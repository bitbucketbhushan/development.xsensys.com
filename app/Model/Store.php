<?php

class Store extends AppModel
{
    
    var $name = "Store";
    
    var $validate = array(
                          
        'store_name' => array(                                
            'notEmpty' => array(                                
                'rule' => array( 'notEmpty' ),
                'required' => true,
                'message' => 'Please fill store name!'                                
            )                                                        
        ),         
        'company_id' => array(            
            'rule' => array( 'checkChooseStatus' ),
            'required' => true,
            'message' => 'Please choose status here!'            
        )
    );
    
    var $belongsTo = array(
        
        'Company' => array(
                               
            'className' => 'Company',
            'foreignKey' => 'company_id'
                               
        )
    );
    
    public function checkChooseStatus()
    {
        if( $this->data['Store']['company_id'] === "")
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
}

?>
