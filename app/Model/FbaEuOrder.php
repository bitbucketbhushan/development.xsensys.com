<?php
class FbaEuOrder extends AppModel
{
    var $name = "FbaEuOrder";
	
    var $hasMany = array(
		'FbaEuOrderItem' => array(
			
			'className' => 'FbaEuOrderItem',
			'foreignKey' => 'fba_order_inc_id'
		)
    
    );
        
}

?>
