<?php

class ConversionRule extends AppModel
{
    
    var $name = "ConversionRule";
    var $validate = array(
				'conversion_rate' => array(                                
				'notEmpty' => array(                                
					'rule' => array( 'numeric' ),
					'required' => true,
					'message' => 'Please fill conversion rate value in number formate!'                                
					)
				)
			);    
}

?>
