<?php

class Company extends AppModel
{
    
    var $name = "Company";
    
    var $validate = array(
                          
        'company_name' => array(                                
            'notEmpty' => array(                                
                'rule' => array( 'notEmpty' ),
                'required' => true,
                'message' => 'Please fill company name!'                                
            )                                                        
        )
    );
    
    
    var $hasMany = array(
        
        'Store' => array(
                               
            'className' => 'Store',
            'foreignKey' => 'company_id'
                               
        )
    );
}

?>
