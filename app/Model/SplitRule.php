<?php

class SplitRule extends AppModel
{
    
    var $name = "SplitRule";
	var $validate = array(
				'max_value' => array(                                
				'notEmpty' => array(                                
					'rule' => array( 'numeric' ),
					'required' => true,
					'message' => 'Please fill max value in number formate!'                                
					)
				),
				'min_value' => array(
					'notEmpty' => array(                                
					'rule' => array( 'numeric' ),
					'required' => true,
					'message' => 'Please fill mix value in number formate!' 
				)
			)
		);    
}

?>
