<?php
class FbaShipmentItem extends AppModel
{
    var $name = "FbaShipmentItem";
    
    var $belongsTo = array(
        'FbaShipment'   => array(

            'className'  => 'FbaShipment',
            'foreignKey' => 'id',
       
        )
    );
    
} 

?>
