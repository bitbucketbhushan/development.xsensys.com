<?php

class CompanyLocation extends AppModel
{
    
    var $name = "CompanyLocation";
    
    var $validate = array(
                          
        'country_name' => array(                                
            'notEmpty' => array(                                
                'rule' => array( 'notEmpty' ),
                'required' => true,
                'message' => 'Please fill country name!'                                
            )                                                        
        )
    );
    
    
    var $hasMany = array(
        
        'Store' => array(
                               
            'className' => 'Store',
            'foreignKey' => 'company_id'
                               
        )
    );
}

?>
