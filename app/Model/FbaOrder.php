<?php
class FbaOrder extends AppModel
{
    var $name = "FbaOrder";
	
    var $hasMany = array(
		'FbaOrderItem' => array(
			
			'className' => 'FbaOrderItem',
			'foreignKey' => 'fba_order_inc_id'
		)
    
    );
        
}

?>
