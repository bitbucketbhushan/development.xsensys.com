<?php
class PackageEnvelope extends AppModel
{
    
    var $name = "PackageEnvelope";
    
    var $belongsTo = array(
                           
        'Packagetype'  => array(
                           
            'className' => 'Packagetype',
            'foreignKey' => 'Packagetype_id'
                           
        )                       
                           
    );
    
    var $virtualFields = array('envelopeName' => 'CONCAT("ID" , PackageEnvelope.id, " - ", PackageEnvelope.envelope_name)');
    
     var $validate = array(
     
     'envelope_name' => array(
		'notEmpty' => array(
		'rule' => array( 'notEmpty' ),
		'required' => true,
		'message' => 'Please fill Package Type !'
		)
	),
	'envelop_cost' => array(
		'notEmpty' => array(
		'rule' => array( 'notempty' ),
		'required' => true,
		'message' => 'Please fill cost !'
		)
	),
	'packagetype_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please select Package Type !',
				'allowEmpty' => false
			)
		),
		'envelope_length' => array(
			'notEmpty' => array(
				'rule' => array('numeric'),
				'message' => 'Please fill length in number !',
				'allowEmpty' => false
			)
		),
		'envelope_width' => array(
			'notEmpty' => array(
				'rule' => array('numeric'),
				'message' => 'Please fill width in number !',
				'allowEmpty' => false
			)
		),
		'envelope_height' => array(
			'notEmpty' => array(
				'rule' => array('numeric'),
				'message' => 'Please fill height in number !',
				'allowEmpty' => false
			)
		),
		'envelope_weight' => array(
			'notEmpty' => array(
				'rule' => array('numeric'),
				'message' => 'Please fill weight in number !',
				'allowEmpty' => false
			)
		)
     
     );
    

}
?>
