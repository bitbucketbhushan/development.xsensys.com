<?php
class FbaShipmentBox extends AppModel
{
    var $name = "FbaShipmentBox";
    
    var $belongsTo = array(
        'FbaShipmentPacking'   => array(

            'className'  => 'FbaShipmentPacking',
            'foreignKey' => 'id',
       
        )
    );
    
}

?>
