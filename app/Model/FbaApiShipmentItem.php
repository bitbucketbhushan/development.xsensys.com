<?php
class FbaApiShipmentItem extends AppModel
{
    var $name = "FbaApiShipmentItem";
    
    var $belongsTo = array(
        'FbaApiShipment'   => array(

            'className'  => 'FbaApiShipment',
            'foreignKey' => 'id',
       
        )
    );
    
}

?>
