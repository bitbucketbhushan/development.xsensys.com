<?php

class SplitsController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Splits";
    
    var $components = array('Session','Upload','Common','Auth','Paginator');
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
    var $virtualFields;
   
   	public function index()
	{
			$this->layout = 'index';
			$this->loadModel( 'MergeUpdate' );
			
			$order_detail = array();
			if(isset($_REQUEST['searchkey']))
			{ 
				$order_detail	=	$this->MergeUpdate->find( 'all', array( 'conditions' => array( 'MergeUpdate.order_id' => $_REQUEST['searchkey'] ) ) );
			}
			
			$this->set('order_detail', $order_detail);
	} 
	 
	public function VerifyOrder()
	{
			$this->layout = 'index';
			$this->loadModel( 'Product' );
			$this->loadModel( 'MergeUpdate' );
			$html = '';
			foreach($this->request->data['sku'] as $count => $data)
			{ 
 				$height = [];
				$weight = [];
				$skus = explode(',', $data);
				if(count($skus) > 1){
					foreach($skus as $k => $v){
						$x = explode('XS-', $v); 
						if(count($x) > 1){
							$qty = $x[0];
							$sku = 'S-'.$x[1];	
							$detail	=	$this->Product->find( 'first', array( 'conditions' => ['product_sku' => $sku],'fields'=>['ProductDesc.barcode','ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight'] ) );
							$length = $detail['ProductDesc']['length'] ;
							$width = $detail['ProductDesc']['width'] ;
							$height[] = $detail['ProductDesc']['height'] * $qty;
							$weight[] = $detail['ProductDesc']['weight'] * $qty ;
  						}
					}
				}else {
						$x = explode('XS-', $data); 
						if(count($x) > 1){
							$qty = $x[0];
							$sku = 'S-'.$x[1];	
							$detail	=	$this->Product->find( 'first', array( 'conditions' => ['product_sku' => $sku],'fields'=>['ProductDesc.barcode','ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight'] ) );
							$length = $detail['ProductDesc']['length'] ;
							$width = $detail['ProductDesc']['width'] ;
							$height[] = $detail['ProductDesc']['height'] * $qty;
							$weight[] = $detail['ProductDesc']['weight'] * $qty;									
						}						 
				} 
						 
				$_height = array_sum($height);
				$_weight = array_sum($weight);
							
				$html .= $this->request->data['searchkey'] .'-'.($count+1).'Dim'.$length.' x'.$width.' x_height'.$_height.'x_weight'.$_weight .'<br>';
 			}
			echo $html;
		 exit;
	} 
	
	public function showSubOrder() 
	{
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		
		//pr($this->request->data);
		
		$orderid	=	'1735426';//$this->request->data['id'];
		//$orderid	=	$this->request->data['id'];
		//$skuqty		=	'2';//$thi2->request->data['skuqty'];
		$sku 		=	'2XS-TEP006SP,1XS-TEP004SP';
		//$sku 		=	$this->request->data['skuqty'];
		//$n_o_sku 	= 	explode('XS-',$sku);
		$o_detail	=	$this->MergeUpdate->find( 'first', array( 'conditions' => array( 'MergeUpdate.order_id' => $orderid ) ) );
		$suborder 	=   $this->MergeUpdate->find( 'count', array( 'conditions' => array( 'MergeUpdate.order_id' => $orderid ) ) );
		$o_all_skus	=	explode( ',', $o_detail['MergeUpdate']['sku']);
		$t_qty = 0;
		pr($o_all_skus);
		$osku = array();
		foreach( $o_all_skus as $o_all_sku )
		{
			$o_sku_qty	=	explode( 'XS-', $o_all_sku );
			$c_skus	=	explode(',',$sku);
			foreach( $c_skus as $c_sku)
			{
				$c_sku_qty	=	explode( 'XS-', $c_sku ); 
				if('S-'.$o_sku_qty[1] == 'S-'.$c_sku_qty[1])
				{
					$o_o_qty = $o_sku_qty[0] - $c_sku_qty[0];
					if( !empty($osku)){
						if($o_o_qty != 0 ){ 
							$osku[]  .=	','.$o_o_qty.'XS-'.$o_sku_qty[1];
						}
					} else {
						if($o_o_qty != 0 ){ 
							$osku[] 	=	$o_o_qty.'XS-'.$o_sku_qty[1];	
						}
					}
				} 
				else
				{	if( !empty($osku)){ echo "11111";
						if($o_sku_qty[0] != 0){ echo "222222";
								$osku[]  .=	','.$o_sku_qty[0].'XS-'.$o_sku_qty[1];
							}
					} else { echo "333333";
						if($o_sku_qty[0] != 0){ 
							
							$osku[] 	=	$o_sku_qty[0].'XS-'.$o_sku_qty[1];
						}
					}
				} 
			}
		}
		pr($osku);
		exit;
		
		foreach( $o_all_skus as $all_sku )
		{
			$o_qty_sku	=	explode( 'XS-',$all_sku );
			if('S-'.$n_o_sku[1] == 'S-'.$o_qty_sku[1])
			{
				$t_qty 			= 	$t_qty + $o_qty_sku[0];
				$p_q_p 			=   ($o_detail['MergeUpdate']['price']/$t_qty);
				$n_o_p			=	$p_q_p * $n_o_sku[0];
				
				$p_q_w 			=   ($o_detail['MergeUpdate']['packet_weight']/$t_qty);
				$n_o_w			=	$p_q_w * $n_o_sku[0];
				
				$r_o_qty		=	$o_qty_sku[0] - $n_o_sku[0];	
				$r_o_p			=	$p_q_p * $r_o_qty;
				$r_o_w			=	$p_q_w * $r_o_qty;
			}
		}
		
			$new_o_data['MergeUpdate']['order_id'] 					= $orderid;
			$new_o_data['MergeUpdate']['product_order_id_identify'] = $orderid.'-'.($suborder+1);
			$new_o_data['MergeUpdate']['sku'] 						= $sku;
			$new_o_data['MergeUpdate']['quantity'] 					= $t_qty ;
			$new_o_data['MergeUpdate']['price'] 					= $n_o_p ;
			$new_o_data['MergeUpdate']['packet_weight'] 			= $n_o_w ;
			
			$rem_o_data['MergeUpdate']['order_id'] 					= $orderid;
			$rem_o_data['MergeUpdate']['product_order_id_identify'] = $o_detail['MergeUpdate']['product_order_id_identify'];
			$rem_o_data['MergeUpdate']['sku'] 						= $r_o_qty.'XS-'.$n_o_sku[1];
			$rem_o_data['MergeUpdate']['quantity'] 					= $r_o_qty;
			$rem_o_data['MergeUpdate']['price'] 					= $r_o_p ;
			$rem_o_data['MergeUpdate']['packet_weight'] 			= $r_o_w ;
			
			$new_order	=	json_encode( array( 'mainorder' => $rem_o_data, 'neworder' => $new_o_data ) );	
			echo $new_order;	
			exit;	 
	}
   
    
    
    
}
?>
