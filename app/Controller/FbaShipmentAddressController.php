<?php
//error_reporting(0);
class FbaShipmentAddressController extends AppController
{
    
    var $name = "FbaShipmentAddress";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('printTest'));		
			
    }
 	
   public function index() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Shipment Pickup Address" );	
		$this->loadModel('FbaApiShipmentAddress');
		 
		$this->paginate = array('order'=>'FbaApiShipmentAddress.id DESC','limit' => 20 );
		$all_address = $this->paginate('FbaApiShipmentAddress');
		 
		$this->set( 'all_address', $all_address );				
		
	} 
	public function ShipmentAddress($id = 0)  
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Shipments" );	
		$this->loadModel('FbaApiShipmentAddress');  
		 
		$this->paginate = array('order'=>'FbaApiShipmentAddress.id DESC','limit' => 20 );
		$all_address = $this->paginate('FbaApiShipmentAddress');
		 
		$this->set( 'all_address', $all_address );	
		$address = array();
		if($id > 0){
			$address = $this->FbaApiShipmentAddress->find('first',array('conditions' => array('id' => $id)));
		}
		$this->set( 'address', $address );				
		
	} 
	
	public function ShipmentAddressDelete( $id = 0){ 
	
		$this->loadModel('FbaApiShipmentAddress'); 
		
		$this->FbaApiShipmentAddress->deleteAll(array('FbaApiShipmentAddress.id' => $id), false);
		
		$this->Session->setFlash("Address Deleted.", 'flash_success');
		
		$this->redirect($this->referer());	  
	}
	
	public function SaveShipmentAddress(){
		
		$this->loadModel('FbaApiShipmentAddress');  
		$error = array();
		if($this->request->data['address_line_1'] == ''){
			$error[] = 'Please enter address.';
		}
		
		if($this->request->data['city'] == ''){
			$error[] = 'Please enter city.';
		}
		
		if($this->request->data['country_code'] == ''){
			$error[] = 'Please enter country code.';
		}
		
		if($this->request->data['state_or_province_code'] == ''){
			$error[] = 'Please enter state/province code.';
		}
		
		if($this->request->data['postal_code'] == ''){
			$error[] = 'Please enter postal code.';
		}
		
		if(count($error) > 0){
			$msg ='';
			foreach($error as $err){
				$msg .= $err."<br>";
			}
			$this->Session->setFlash($msg, 'flash_danger');
		}else{
			$this->FbaApiShipmentAddress->saveAll( $this->request->data ); 
			if(isset($this->request->data['id'])){
				$this->Session->setFlash(" Address Updated.", 'flash_success');
			}else{
				$this->Session->setFlash(" Address added.", 'flash_success');
			}
		}
		
		$this->redirect($this->referer());	 
						
	}
	 
}