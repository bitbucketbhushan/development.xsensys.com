<?php
//error_reporting(0);
//ini_set('memory_limit', '-1');
	App::uses('Folder', 'Utility');
	App::uses('File', 'Utility');
class CustomfilesController extends AppController
{	
	var $name = "Customfiles";
	var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
	public function index()
	{
		echo "ihihi";
		exit;
	}
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('getBrtSheetManifest','getPhoneNumbreList','getDataForBrtPalletPrintPdf','getBarcodeOutside','generateBrtLabels','getBrtBarcodeData'));
    }
	
 

	public function getBrtManifestFilesCustom()
	{ 
		$this->layout = 'index';

		if(isset($_POST['parcel_no'])) {
			
		$this->loadModel('BrtSerialNo');

		$parcelNos = $this->request->data['parcel_no']; 
		$orders = array();

		// $parcelNos = explode(',',$parcelNos);

		$parcelNos = array_map('trim', explode(',', $parcelNos));

		$brtdata = $this->BrtSerialNo->find('all', array( 
			'conditions' => array( 'BrtSerialNo.parcel_no'=>$parcelNos),
			'order' => 'parcel_no DESC')
		);
        if(count($brtdata) >0 ) {
			foreach($brtdata as $brtord) { 
			   $orders[] = $brtord['BrtSerialNo']['split_order_id'];
			}  
	    }
        if(count($orders) > 0 ) {
			$brt ='';
			$result	= $this->getBrtSheetManifest($orders);
			if($result == 'ok'){
				$result	= $this->getPhoneNumbreList($orders);
				if($result == 'ok'){
					$result	= $this->getDataForBrtPalletPrintPdf($orders);
					if($result == 'ok'){
							$this->Session->setFlash('Brt Manifest Files generated.', 'flash_success');	
							$brt = '?brt=1';	
					} else {
						$this->Session->setFlash($result, 'flash_danger');
					}
				}else{
					$this->Session->setFlash($result, 'flash_danger');
				}
			}else{			
				$this->Session->setFlash($result, 'flash_danger');
			}
			$this->Session->setFlash('All Brt files are generated.', 'flash_success');
	    } else {
			$this->Session->setFlash('No order id found.', 'flash_error');
		}
		
	    }  
	}
	
	public function getBrtSheetManifest($orders = array())
	{
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('ItalyCode');
		$this->loadModel('BrtSerialNo');
		$start_date = date('Y-m-d H:i:s');
		$end_date   = date('Y-m-d H:i:s');
		
		$italyOrders = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => $orders ) ) );
																		
		if(count($italyOrders) > 0)
		{																		
		$fp = fopen(WWW_ROOT."img/FNVAB00R.csv","w");
		$sep = ";";
		$header = "VABCCM".$sep."VABLNP".$sep."VABAAS".$sep."VABMGS".$sep."VABNRS".$sep."VABNSP".$sep."VABCBO".$sep."VABLNA".$sep."VABRSD".$sep."VABRD2".$sep."VABIND".$sep."VABCAD".$sep."VABLOD".$sep."VABPRD".$sep."VABNZD".$sep."VABGC1".$sep."VABGC2".$sep."VABCTR".$sep."VABTSP".$sep."VABIAS".$sep."VABVAS".$sep."VABNAS".$sep."VABNCL".$sep."VABPKB".$sep."VABVLB".$sep."VABQFT".$sep."VABCAS".$sep."VABTIC".$sep."VABVCA".$sep."VABGCA".$sep."VABRMN".$sep."VABRMA".$sep."VABNCD".$sep."VABNCA".$sep."VABXCO".$sep."VABNOT".$sep."VABNT2".$sep."VABZNC".$sep."VABCTM".$sep."VABFFD".$sep."VABDCR".$sep."VABTCR".$sep."VABHCR".$sep."VABCTS".$sep."VABFTM".$sep."VABVMD".$sep."VABVAD".$sep."VABGMA".$sep."VABGGA".$sep."VABGVA".$sep."VABTC1".$sep."VABTC2".$sep."VABSCL".$sep."VABANT".$sep."VABRMO".$sep."VABCMO".$sep."VABNMO\r\n";
		unlink(WWW_ROOT."img/FNVAB00R.csv");
		file_put_contents(WWW_ROOT."img/FNVAB00R.csv",$header);
			foreach( $italyOrders as $italyOrder )
			{
					$italyOrder['MergeUpdate']['order_id'];
					$orderdetails = $this->MergeUpdate->find('all', array('conditions'=>array('MergeUpdate.product_order_id_identify'=>$italyOrder['MergeUpdate']['product_order_id_identify'])));
					$italyopen	=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id'=>$italyOrder['MergeUpdate']['order_id'])));
				foreach($orderdetails as $orderdetail)
					{
						$brtdata	=	$this->BrtSerialNo->find('first', array( 'conditions' => array( 'BrtSerialNo.split_order_id'=>$orderdetail['MergeUpdate']['product_order_id_identify']),'order' => 'parcel_no DESC'));
						
						if(!empty($brtdata))
						{
							$split_order_id		= 	$orderdetail['MergeUpdate']['product_order_id_identify'];
							$serial_no			=	$brtdata['BrtSerialNo']['serial_no'];
							$parcel_no			=	$brtdata['BrtSerialNo']['parcel_no'];
							$split_order_id		=	$brtdata['BrtSerialNo']['split_order_id'];
							$iso_country		=	$brtdata['BrtSerialNo']['iso_country'];
							$province_recipient	=	$brtdata['BrtSerialNo']['province_recipient'];
							$po_arrival			=	$brtdata['BrtSerialNo']['po_arrival'];
							$po_arrival_des		=	$brtdata['BrtSerialNo']['po_arrival_des'];
							$po_departure_des	=	$brtdata['BrtSerialNo']['po_departure_des'];
							$arrival_terminal	=	$brtdata['BrtSerialNo']['arrival_terminal'];
							$zone_mark_neck		=	$brtdata['BrtSerialNo']['zone_mark_neck'];
							$barcode			=	$brtdata['BrtSerialNo']['barcode'];
							
							$store =	$italyopen['OpenOrder']['sub_source'];
							 
							if($store == 'CostBreaker_IT' || $store == 'CostBreaker_DE' || $store == 'CostBreaker_FR' || $store == 'CostBreaker_ES'){
							 	$acnumber = '1664097';
							} else if($store == 'Rainbow_Retail_IT' || $store == 'Rainbow Retail' || $store == 'Rainbow_Retail_ES' || $store == 'RAINBOW RETAIL DE'){
								$acnumber = '1664098';
							} else if($store == 'Tech_Drive_ES' || $store == 'Tech_Drive_UK' || $store == 'Tech_Drive_DE' || $store == 'Tech_Drive_FR'){
								$acnumber = '1664099';
							} else {
								$acnumber = '1664070';
							}
							
							
							$general_info		=	unserialize( $italyopen['OpenOrder']['general_info'] );
							$shipping_info		=	unserialize( $italyopen['OpenOrder']['shipping_info'] );
							$customer_info		=	unserialize( $italyopen['OpenOrder']['customer_info'] );
							$totals_info		=	unserialize( $italyopen['OpenOrder']['totals_info'] );
							$items				=	unserialize( $italyopen['OpenOrder']['items'] );
							$orderidwithout		=	str_replace('-','',$orderdetail['MergeUpdate']['product_order_id_identify']);
							$orderidwith		=	$orderdetail['MergeUpdate']['product_order_id_identify'];
							
							$vitrual_delete		=	'';
							$departure_depot					=	'';
							$shipment_year						=	date("Y");
							$shipment_monty_day					=	date("md");
							
							$address1 = rtrim($this->getcommaremovedata($customer_info->Address->Address1),' ');
							$address2 = rtrim($this->getcommaremovedata($customer_info->Address->Address2),' ');
							
							$consignee_address 					= 	rtrim($address1.' '.$address2,' ');
							$consigee_cap_zip_code 				= 	str_pad($customer_info->Address->PostCode,5,"0",STR_PAD_LEFT);
							$consignee_city						=	rtrim($this->getcommaremovedata(htmlspecialchars($customer_info->Address->Town)),' ');//city;
							$consignee_province_abbreviation	=	$this->getcommaremovedata(htmlspecialchars($customer_info->Address->Region));//provience;
							$name								=	rtrim($this->getcommaremovedata($customer_info->Address->FullName),' ');
							$email								=	$this->getcommaremovedata($customer_info->Address->EmailAddress);
							$comany 							= 	$this->getcommaremovedata($customer_info->Address->Company); 
							$weight								=	ceil($orderdetail['MergeUpdate']['packet_weight']);
							$consignee_country					=	'';
							$phonenumber						=	str_replace('+','',filter_var($customer_info->Address->PhoneNumber, FILTER_SANITIZE_NUMBER_INT));
						 
							//str_pad($parcel_no,7,"0",STR_PAD_LEFT)1664070
							//$sno	=	substr($parcel_no, -2);
							
							  $content = $acnumber.$sep.'166'.$sep.$shipment_year.$sep.$shipment_monty_day.$sep.'67'.$sep.$parcel_no.$sep.'1'.$sep.$po_arrival.$sep.$name.$sep.''.$sep.$consignee_address.$sep.$consigee_cap_zip_code.$sep.$consignee_city.$sep.$province_recipient.$sep.''.$sep.''.$sep.''.$sep.'300'.$sep.'c'.$sep.''.$sep.''.$sep.'Goods'.$sep.'1'.$sep.$weight.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.$orderidwithout.$sep.$orderidwith.$sep.str_pad($parcel_no,7,"0",STR_PAD_LEFT).$sep.str_pad($parcel_no,7,"0",STR_PAD_LEFT).$sep.''.$sep.''.$sep.''.$sep.$zone_mark_neck.$sep.'2'.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep."\r\n";
							 
							file_put_contents(WWW_ROOT."img/FNVAB00R.csv",$content, FILE_APPEND | LOCK_EX);
							$this->MergeUpdate->updateAll(array('MergeUpdate.brt' => '2'), array('MergeUpdate.id' => $orderdetail['MergeUpdate']['id']));
						}
				  }
			}
			 
			$uploadUrl 		= 	WWW_ROOT .'img/FNVAB00R.csv';
			$folderName = 'Service Manifest -'. date("d.m.Y");
			$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
			copy( $uploadUrl, WWW_ROOT .'img/cut_off/'.$folderName.'/FNVAB00R_'.date('m-d-Y H:i:s').'.csv');  
			$status = 'ok';
			} else {
				$status = 'There is no data for FNVAB00R.';
			}
			return $status;
	}
		
	public function getPhoneNumbreList($orders = array())
	{
	
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('ItalyCode');
		$this->loadModel('BrtSerialNo');
		
		$italyOrders = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => $orders ) ) );
	 																			
		if(count($italyOrders) > 0)
		{
		$fp = fopen(WWW_ROOT."img/FNVAT00R.csv","w");
		$sep = ";";
		$header = "VATCCM".$sep."VATLNP".$sep."VATNRS".$sep."VATNSP".$sep."VATNOT_B".$sep."VATNOT_S\r\n";
		unlink(WWW_ROOT."img/FNVAT00R.csv");
		file_put_contents(WWW_ROOT."img/FNVAT00R.csv",$header);
		foreach( $italyOrders as $italyOrder )
			{
					$italyOrder['MergeUpdate']['order_id'];
					$orderdetails = $this->MergeUpdate->find('all', array('conditions'=>array('MergeUpdate.product_order_id_identify'=>$italyOrder['MergeUpdate']['product_order_id_identify'])));
					$italyopen	=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id'=>$italyOrder['MergeUpdate']['order_id'])));
				foreach($orderdetails as $orderdetail)
					{
						$brtdata	=	$this->BrtSerialNo->find('first', array( 'conditions' => array( 'BrtSerialNo.split_order_id'=>$orderdetail['MergeUpdate']['product_order_id_identify']),'order' => 'parcel_no DESC'));
						
						if(!empty($brtdata))
						{
							$customer_info				=	unserialize( $italyopen['OpenOrder']['customer_info'] );
							$consignee_country			=	'';
							$number						=	str_replace('-','',str_replace('+','',filter_var($customer_info->Address->PhoneNumber, FILTER_SANITIZE_NUMBER_INT)));
							$concode					=	substr($number, 0, 2);
							
							if(strlen($number) < 13 && $concode != '39'){
								$number =  '+39'.$number;	
							} 
							 
							 $store =	$italyopen['OpenOrder']['sub_source'];
							 
							if($store == 'CostBreaker_IT' || $store == 'CostBreaker_DE' || $store == 'CostBreaker_FR' || $store == 'CostBreaker_ES'){
							 	$acnumber = '1664097';
							} else if($store == 'Rainbow_Retail_IT' || $store == 'Rainbow Retail' || $store == 'Rainbow_Retail_ES' || $store == 'RAINBOW RETAIL DE'){
								$acnumber = '1664098';
							} else if($store == 'Tech_Drive_ES' || $store == 'Tech_Drive_UK' || $store == 'Tech_Drive_DE' || $store == 'Tech_Drive_FR'){
								$acnumber = '1664099';
							} else {
								$acnumber = '1664070';
							}
							
							$split_order_id		= 	$orderdetail['MergeUpdate']['product_order_id_identify'];
							$serial_no			=	$brtdata['BrtSerialNo']['serial_no'];
							$parcel_no			=	$brtdata['BrtSerialNo']['parcel_no'];
							$sno	=	substr($parcel_no, -2);
							$content = $acnumber .$sep.'166'.$sep.'67'.$sep.$parcel_no.$sep.$number.$sep.$number."   SN\r\n";
							file_put_contents(WWW_ROOT."img/FNVAT00R.csv",$content, FILE_APPEND | LOCK_EX);
							$this->MergeUpdate->updateAll(array('MergeUpdate.brt_phone' => '2'), array('MergeUpdate.id' => $orderdetail['MergeUpdate']['id']));
						}
				
					 }
				}
			$folderName = 'Service Manifest -'. date("d.m.Y");
			$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
			$uploadUrl 		= 	WWW_ROOT .'img/FNVAT00R.csv';
			copy($uploadUrl,WWW_ROOT .'img/cut_off/'.$folderName.'/FNVAT00R_'.date('m-d-Y H:i:s').'.csv');  
				$status = 'ok';
			} else {
				$status = 'There is no data for FNVAT00R.';
			}
			return $status;		
			
			
	}
	
	public function getDataForBrtPalletPrintPdf($orders = array())
	{
		
		$this->layout  = '';
		$this->autoRender = false;
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('ItalyCode');
		$this->loadModel('BrtSerialNo');
		
		
		$italyOrders = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => $orders ) ) );									
		
		if(count($italyOrders) > 0)
		{
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper(array(0, 0, '794', '1122' ), 'portrait');
	
		$imgPath = WWW_ROOT .'css/';
		$html = '<style>table, th, td { border: 1px solid black; }</style><body>';
		$html .= '<div id="label">';
		$html .= '<div class="container">';
   	  	$html .= '<div class="header row" style="width:100%; border:1px solid #000000; margin:20px 0 10px 0; padding:10px; text-align: center;"><strong style="font-size: 32px;" >Brt Export Sheet</strong><br><strong>ESL Limited</strong><br><strong>'.date('m-d-Y H:i:s').'</strong></div>';
		$html .= '<table class="header row" style="border:1px solid #000000; margin:0px 0 10px 0;" width=100%>';
      	$html .='<tr style="text-align: left;"><th width="20%" >Our Ref:</th><th width="20%" >Consignee Name</th><th width="30%">Consignee Address</th><th width="15%">Consignee PostCode</th><th width="15%">Parcel No</th></tr>';
	  
	  	foreach($italyOrders as $italyOrder) {
		
			$or_id				=	$italyOrder['MergeUpdate']['order_id'];
			$spo_id				=	$italyOrder['MergeUpdate']['product_order_id_identify'];
			$italyopen			=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id'=>$or_id)));
			$brtdata			=	$this->BrtSerialNo->find('first', array( 'conditions' => array( 'BrtSerialNo.split_order_id'=>$spo_id),'order' => 'parcel_no DESC'));
			$customer_info		=	unserialize( $italyopen['OpenOrder']['customer_info'] );
			$consignee_address 	= 	$customer_info->Address->Address1;
			$zip_code 			= 	str_pad($customer_info->Address->PostCode,5,"0",STR_PAD_LEFT);
			$name				=	$customer_info->Address->FullName;
			$parcel_no			=	'--';
			if( !empty( $brtdata ) )
				$parcel_no			=	$brtdata['BrtSerialNo']['parcel_no'];
				
			$html .='<tr><td width="20%">'.$spo_id.'</td><td width="20%">'.$name.'</td><td width="20%">'.$consignee_address.'</td><td width="20%">'.filter_var($zip_code, FILTER_SANITIZE_NUMBER_INT).'</td><td width="20%">'.$parcel_no.'</td></tr>';
			//$this->MergeUpdate->updateAll(array('brt_exprt' => 2), array('product_order_id_identify' => $spo_id));
	  	}
   	  	$html .='</table></div></body>';
		$html.= '<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
		//exit;
		$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
		$dompdf->render();
		//$dompdf->stream("ExportLabelBrt.pdf");
		
		$imgPath 	= 	WWW_ROOT .'img/'; 
		$name		=   'ExportLabelBrt.pdf';
		file_put_contents($imgPath.$name, $dompdf->output());
		
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
		$uploadUrl 		= 	WWW_ROOT .'img/ExportLabelBrt.pdf';
		copy($uploadUrl,WWW_ROOT .'img/cut_off/'.$folderName.'/ExportLabelBrt_'.date('m-d-Y H:i:s').'.pdf');  
		$uploadUrl 		= 	WWW_ROOT .'img/ExportLabelBrt.pdf';		
		$status = 'ok';
		} else {
		$status = 'There is no data for ExportLabelBrt.';
		}
		return $status;
		
		
	}
	
	public function getcommaremovedata($string = null){
			
		$special_chars = array(';'=>' ','"'=>' ',"'"=>' ','\n'=>'',','=>' ');
		$str = strtr( $string,$special_chars );
		return  $str;
	}
	
	
	
	public function getBarcodeOutside2()
			 {  
				  $this->layout = '';
				  $this->autoRender = false;
				  $this->loadModel( 'OpenOrder' );
				  $this->loadModel( 'AssignService' );
				  $this->loadModel( 'MergeUpdate' );
				
				  //$uploadUrl = $this->getUrlBase();
				  $imgPath = WWW_ROOT .'img/orders/barcode/';   
				  //$allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.status' => '0')));
				  
				  $allSplitOrders = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify IN' => array('1219062-1','1219056-1','1219007-1','1219034-1','1219035-1','1219027-1','1219065-1','1219029-1','1218608-1','1219019-1','1219016-1','1219040-1') ) ) );
				  
				  
			 	require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
				require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
				require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
				//$font = new BCGFontFile(APP .'Vendor/barcodegen/font/Arial.ttf', 13);
				$colorFront = new BCGColor(0, 0, 0);
				$colorBack = new BCGColor(255, 255, 255);
					
				  if( count($allSplitOrders) > 0 )	
				  {
					  foreach( $allSplitOrders as $allSplitOrder )
					  {					  
						  $id 			= 		$allSplitOrder['MergeUpdate']['id'];
						  $openorderid	=	 	$allSplitOrder['MergeUpdate']['product_order_id_identify'];
						  $barcodeimage	=	$openorderid.'.png';
						  
						  	$orderbarcode=$openorderid;
							$code128 = new BCGcode128();
							$code128->setScale(2);
							$code128->setThickness(20);
							$code128->setForegroundColor($colorFront);
							$code128->setBackgroundColor($colorBack);
							$code128->setLabel(false);
							$code128->parse($orderbarcode);
												
							//Drawing Part
							$imgOrder128=$orderbarcode.".png";
							$imgOrder128path=$imgPath.'/'.$orderbarcode.".png";
							$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
							$drawing128->setBarcode($code128);
							$drawing128->draw();
							$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);
						  
						  if( $allSplitOrder['MergeUpdate']['product_order_id_identify'] != "" )
						  {
							  //$content = file_get_contents($uploadUrl.$openorderid);
							  //file_put_contents($imgPath.$barcodeimage, $content);
							  
							  $data['MergeUpdate']['id'] 	=  $id;
							  $data['MergeUpdate']['order_barcode_image'] 	=  $barcodeimage;
							  //$this->MergeUpdate->save($data);
						  }
					  }
				  }
				  
				  $this->redirect( Router::url( $this->referer(), true ) );
			  
			 }
			 
			public function  saveOpenOrderCustom()
			{
				$this->layout = '';
				$this->autoRender = false;
				$this->loadModel('OpenOrder');
				$this->loadModel('AssignService');
				$this->loadModel('Customer');
				$this->loadModel('OrderItem');
				$this->loadModel('Product');
				$this->loadModel('UnprepareOrder');
				
				
				App::import('Vendor', 'Linnworks/src/php/Auth');
				App::import('Vendor', 'Linnworks/src/php/Factory');
				App::import('Vendor', 'Linnworks/src/php/Orders');
			
				$username = Configure::read('linnwork_api_username');
				$password = Configure::read('linnwork_api_password');
				
				$token = Configure::read('access_new_token');
				$applicationId = Configure::read('application_id');
				$applicationSecret = Configure::read('application_secret');
				
				//$multi = AuthMethods::Multilogin($username, $password);		
				$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	
		
				$token = $auth->Token;	
				$server = $auth->Server;		
				
				//$arr = array('1209325','1209410','1209497','1209583','1210036','1210247','1210267','1210269','1210317','1210406','1210637','1210692','1210710','1210753');
				$arr = array('1210046');
				
				foreach($arr as $pkOrderId){
					$orders = array();  
					$order	= OrdersMethods::GetOrderDetailsByNumOrderId($pkOrderId,$token, $server);
					App::import('Controller', 'Cronjobs');
					$obj = new CronjobsController(); 
					
					//$locationName = 'CostBreaker_ES';
					echo "<br>".$locationName = $order->GeneralInfo->SubSource;
					$orders[] = $order->OrderId ;
					pr($orders);
					$results[0] = $order;
					pr($results);
					$obj->saveOpenOrder( $results , $locationName , $orders );		
				}
				
			}
			
			public function getBarcodeOutside()
			 { 
				  
				  $this->layout = '';
				  $this->autoRender = false;
				  $this->loadModel( 'OpenOrder' );
				  $this->loadModel( 'AssignService' );
				  $this->loadModel( 'MergeUpdate' );
				
				  //$uploadUrl = $this->getUrlBase();
				  $imgPath = $_SERVER['DOCUMENT_ROOT'] .'/app/webroot/img/orders/barcode/';   
				  //$allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.status' => '0')));
				  
				  $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.status' => '0'),'fields'=> array('id','product_order_id_identify','order_barcode_image')));
				  
				  
			 	require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
				require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
				require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
				//$font = new BCGFontFile(APP .'Vendor/barcodegen/font/Arial.ttf', 13);
				$colorFront = new BCGColor(0, 0, 0);
				$colorBack = new BCGColor(255, 255, 255);
					
				  if( count($allSplitOrders) > 0 )	
				  {
					  foreach( $allSplitOrders as $allSplitOrder )
					  {					  
						  $id 			=   $allSplitOrder['MergeUpdate']['id'];
						  $openorderid	=	$allSplitOrder['MergeUpdate']['product_order_id_identify'];
						  $barcodeimage	=	$openorderid.'.png';
						echo $imgPath.$barcodeimage;
						echo "<br>";
						  if(!file_exists($imgPath.$barcodeimage)) 
						  {
							  
								$orderbarcode=$openorderid;
								$code128 = new BCGcode128();
								$code128->setScale(2);
								$code128->setThickness(20);
								$code128->setForegroundColor($colorFront);
								$code128->setBackgroundColor($colorBack);
								$code128->setLabel(false);
								$code128->parse($orderbarcode);
													
								//Drawing Part
								$imgOrder128=$orderbarcode.".png";
								$imgOrder128path=$imgPath.'/'.$orderbarcode.".png";
								$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
								$drawing128->setBarcode($code128);
								$drawing128->draw();
								$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);
							  
								    
								  $data['MergeUpdate']['id'] 	=  $id;
								echo   $data['MergeUpdate']['order_barcode_image'] 	=  $barcodeimage;
								echo "<br>";
								  $this->MergeUpdate->save($data);
								  
								}
					  }
				  }
				  
				exit;
			  
			 }
			 
			 
			 
			 public function generateBrtLabels($order_id = null)
			{		
			$this->loadModel('OpenOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('ItalyCode');
			$this->loadModel('BrtSerialNo');
			$italyOrders = $this->OpenOrder->find('all', array('conditions' => array('OpenOrder.num_order_id' => '1219038' ) ) );
			
			$brtData = array();
			if( count($italyOrders) > 0 ){
				foreach( $italyOrders as $italyOrder )
				{
					
					$italyOrder['OpenOrder']['num_order_id'];
					$orderdetails	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $italyOrder['OpenOrder']['num_order_id']) ) );
					pr($orderdetails);
					
					if( count($orderdetails) > 0 ){
						$series_no = 1;
						foreach($orderdetails as $orderdetail)
						{					
							//$brtSerialNo = $this->BrtSerialNo->find('first', array('order' => 'id desc') );
							
							$parcel_no = '11855';
							$store =	$italyOrder['OpenOrder']['sub_source'];
							 
							if($store == 'CostBreaker_IT' || $store == 'CostBreaker_DE' || $store == 'CostBreaker_FR' || $store == 'CostBreaker_ES'){
							 	$store = 'COST-DROPPER/ESL';
							} else if($store == 'Rainbow_Retail_IT' || $store == 'Rainbow Retail' || $store == 'Rainbow_Retail_ES' || $store == 'RAINBOW RETAIL DE'){
								$store = 'RAINBOW RETAIL/ESL';
							} else if($store == 'Tech_Drive_ES' || $store == 'Tech_Drive_UK' || $store == 'Tech_Drive_DE' || $store == 'Tech_Drive_FR'){
								$store = 'TECHDRIVEE/ESL';
							} else {
								$store = 'ESL Limited';
							}
							
							
							$general_info	=	unserialize( $italyOrder['OpenOrder']['general_info'] );
							$shipping_info	=	unserialize( $italyOrder['OpenOrder']['shipping_info'] );
							$customer_info	=	unserialize( $italyOrder['OpenOrder']['customer_info'] );
							$totals_info	=	unserialize( $italyOrder['OpenOrder']['totals_info'] );
							$items			=	unserialize( $italyOrder['OpenOrder']['items'] ); 
							
							
							$consignee_address 		= 	$customer_info->Address->Address1.' '.$customer_info->Address->Address2;
							$consigee_cap_zip_code  = 	$customer_info->Address->PostCode;
							$consignee_city			=	$customer_info->Address->Town;//city;					
							$name					=	$customer_info->Address->FullName;
							$email					=	$customer_info->Address->EmailAddress;
							$postcode 				=   str_pad($customer_info->Address->PostCode,5,"0",STR_PAD_LEFT); 
							$comany 				= 	($customer_info->Address->Company != '') ? $customer_info->Address->Company : $name; 
							$weight					= 	$orderdetail['MergeUpdate']['packet_weight'];
							$split_order_id			= 	$orderdetail['MergeUpdate']['product_order_id_identify'];
							
							$getcountery		    =	$this->ItalyCode->find('first', array( 'conditions' => array( 'ItalyCode.postal_code' => $postcode ) ) );
							if(count($getcountery) == 1)
							$state_abravation	=	$getcountery['ItalyCode']['state_abbreviation'];
							
							//echo "<br>";
							
							$data['TipoLancio'] =  "";
							$data['RicercaPOArrivo']= "S";
							$data['AnnoSpedizione']= date('Y');
							$data['CodiceProdotto']= $split_order_id;
							//$data['DestinatarioCap']= $consigee_cap_zip_code;
							$data['DestinatarioCap']= trim(filter_var($postcode, FILTER_SANITIZE_NUMBER_INT));
							//$data['DestinatarioIndirizzo']= $this->replaceFrenchChar(utf8_encode($consignee_address));
							$data['DestinatarioIndirizzo']= utf8_encode($consignee_address);
							$data['DestinatarioLocalita']= $consignee_city;
							$data['DestinatarioNazione']= "";
							$data['DestinatarioProvincia']= $state_abravation;
							$data['DestinatarioRagioneSociale']= $this->replaceFrenchChar($comany);
							$data['FilialeSegnaCollo'] = "166";
							$data['MeseGiornoSpedizione'] = date('md');
							$data['MittenteNazione']= "";
							$data['MittenteProvincia']= "JE";
							//$data['MittenteRagioneSociale']= "ESL Jersey";
							$data['MittenteRagioneSociale']= $store;
							$data['Network']= "";
							$data['NumeroSegnaCollo'] = $parcel_no;//str_pad($parcel_no,7,"0",STR_PAD_LEFT); 
							//$data['NumeroSerie']= $series_no;
							$data['NumeroSerie']= '67';
							$data['NumSegnaColloDi']= "1";
							$data['Peso']= ceil($weight);
							$data['POPartenza']= "166";
							$data['Volume']= 0;
							$data['Ristampa']= "R";
							$data['PrimaConsegnaParticolare']= "";
							$data['SecondaConsegnaParticolare']= "";
							$data['TipoPorto']= "F";
							$data['TipoServizioBolle']= "C";
							$data['TipoStampante']= "1"; 
							//pr(json_encode($data)); 							 
							//exit;
						   $brtData = $this->getBrtBarcodeData($data);
						   //pr( $brtData);
						   //exit;
						   if($brtData != 'error')
						   {
							  $curl = curl_init();
			
							  curl_setopt_array($curl, array(
							  CURLOPT_URL => "http://31.14.134.40/api/LabelProcessor?LabelFormat=4",
							  CURLOPT_RETURNTRANSFER => true,
							  CURLOPT_ENCODING => "",
							  CURLOPT_MAXREDIRS => 10,
							  CURLOPT_TIMEOUT => 30,
							  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							  CURLOPT_CUSTOMREQUEST => "POST",
							  CURLOPT_POSTFIELDS => "[".json_encode($data)."]",
							  CURLOPT_HTTPHEADER => array(
								"cache-control: no-cache",
								"content-type: application/json",
								"password: Test01",					 
								"token: eyJ1bmlxdWVfbmFtZSI6InRvZGQiLCJ",
								"username: Test"
							  ),
							));
							
							$response = curl_exec($curl);
							$err = curl_error($curl);
							curl_close($curl);
							
							if ($err) {
								//echo "cURL Error #:" . $err;  
								file_put_contents(WWW_ROOT.'/logs/brtlog.log', "\nLabel Error\t".date('d-m-y H:i:s')."\t".$split_order_id."\n". $err."\n", FILE_APPEND | LOCK_EX);	
							} else {						
								$obj = json_decode($response) ;	 
								if(isset($obj->Message)){	
										$mailBody = '<p><strong>'.$obj->ExceptionMessage.' </strong></p>';
										App::uses('CakeEmail', 'Network/Email');
										$email = new CakeEmail('');
										$email->emailFormat('html');
										$email->from('info@euracogroup.co.uk');
										//$email->to( array('avadhesh@euracogroup.co.uk','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));					  
										$getBase = Router::url('/', true);
										$email->subject('BRT orders issue in Xsensys' );
										$email->send( $mailBody );	
									//$obj->ExceptionMessage						
									file_put_contents(WWW_ROOT.'/logs/brtlog.log', "\nLabel Error\t".date('d-m-y H:i:s')."\t".$split_order_id."\n". $response . "\n", FILE_APPEND | LOCK_EX);	
								}else{	
								
								/*-------------------------Fetch Barcode from BRT----------------------*/
									//$brtData = $this->getBrtBarcodeData($data);
									//pr($brtData);
								/*------------------------End Fetch Barcode from BRT---------------------*/
									if(count($brtData) > 0){
										$brtData['serial_no']	   =  $series_no;
										$brtData['parcel_no'] 	   =  $parcel_no;
										$brtData['split_order_id'] =  $split_order_id;
										$brtData['added_date'] 	   =  date('Y-m-d') ; 
										
										$brtbarcode	=	$brtData['barcode'];
										
										$labelPdfPath = WWW_ROOT .'img/brt/label_'.$split_order_id.'.pdf'; 			 
										$datas = base64_decode( $response ); 				
										file_put_contents($labelPdfPath , $datas); 
										file_put_contents(WWW_ROOT.'/logs/brt_labels_'.date('dmy').'.log', "\n".date('d-m-y H:i:s')."\t".$split_order_id."\n". $response."\n", FILE_APPEND | LOCK_EX);	
										//$this->MergeUpdate->updateAll(array('brt' => 1, 'brt_phone' => 1, 'brt_exprt' => '1', 'brt_barcode' => $brtbarcode, 'track_id' => $brtbarcode, 'reg_post_number' => $brtbarcode), array('product_order_id_identify' => $split_order_id));	
										//$this->BrtSerialNo->saveAll($brtData);   

					
										}	 
									 }
										 
							}	
																					  
						}	
					 }							 
				}
			}
	  }
	//exit;
	}
	
	public function getBrtBarcodeData($data)
	{
		$split_order_id = $data['CodiceProdotto'];
		$brtData = array();
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://31.14.134.40/api/Barcode?LabelFormat=1",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "[".json_encode($data)."]",
		  CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: application/json",
			"password: Test01",					
			"token: eyJ1bmlxdWVfbmFtZSI6InRvZGQiLCJ",
			"username: Test"
		  ),
		));
		
		$bcode_response = curl_exec($curl);
		$bcode_err = curl_error($curl);
		
		curl_close($curl);
		
		if ($bcode_err) {		  
			file_put_contents(WWW_ROOT.'/logs/brtlog.log', "\nBarcode Error\t".date('d-m-y H:i:s')."\t".$split_order_id."\n". $bcode_err . "\n", FILE_APPEND | LOCK_EX);	
		} else {
			  $bcode_obj = json_decode($bcode_response) ; 
			  if(isset($bcode_obj->Message)){	
			  		$mailBody = '<p><strong>'.$bcode_obj->ExceptionMessage.' </strong></p>';
					App::uses('CakeEmail', 'Network/Email');
					$email = new CakeEmail('');
					$email->emailFormat('html');
					$email->from('info@euracogroup.co.uk');
					$email->to( array('avadhesh@euracogroup.co.uk','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));					  
					$getBase = Router::url('/', true);
					$email->subject('BRT orders issue in Xsensys' );
					$email->send( $mailBody );	
				  file_put_contents(WWW_ROOT.'/logs/brtlog.log', "\nBarcode Error\t".date('d-m-y H:i:s')."\t".$split_order_id."\n". $bcode_response . "\n", FILE_APPEND | LOCK_EX);	
			  }else{			
					$destinations = $bcode_obj->destinations[0];	
					if($destinations->POArrivo != '0' || $destinations->POArrivoDes != '' || $destinations->POPartenzaDes != '' || $destinations->TerminalArrivo != '0' || $destinations->DestinatarioProvincia != '')
					{
						$brtData['iso_country']  = $destinations->NazioneISO;	
						$brtData['province_recipient'] = $destinations->DestinatarioProvincia;
						$brtData['po_arrival'] = $destinations->POArrivo;
						$brtData['po_arrival_des'] = $destinations->POArrivoDes;
						$brtData['po_departure_des'] = $destinations->POPartenzaDes;
						$brtData['arrival_terminal'] = $destinations->TerminalArrivo;
						$brtData['zone_mark_neck'] = $destinations->ZonaSegnaCollo;
						$brtData['barcode']  = $bcode_obj->Barcodes[0];	
					 } else {
						$mailBody = '<p><strong>Please check Xsensys Orders :- '.$split_order_id.'</strong></p>';
						App::uses('CakeEmail', 'Network/Email');
						$email = new CakeEmail('');
						$email->emailFormat('html');
						$email->from('info@euracogroup.co.uk');
						$email->to( array('avadhesh.kumar@jijgroup.com','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));	
						$email->subject('BRT orders issue in Xsensys' );
						$email->send( $mailBody );
						$brtData = 'error';	
						exit;
					}	  			 
			 }
		}
		return $brtData;							  
	}
	
	
	public function replaceFrenchChar($string = null){
			
		$unwanted_array = array('�'=>'S', '�'=>'s', '�'=>'Z', '�'=>'z', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E','�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', 'N�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U','�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c','�'=>'e', '�'=>'e', '�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n','n�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o','�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'b', '�'=>'y','�'=>'u','�'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}
	
	 public function setStoreSku()
	 {
		$this->layout = 'index';
		 
		if(isset($_POST['skus'])) {

			$skus = $this->request->data['skus']; 
			// $skusList = explode(',',$skus);
			$skusList = array_map('trim', explode(',', $skus));

			$this->loadModel('Product');
			$this->loadModel('Store');
			$this->loadModel('SkuPercentRecord');
			
			/*$getStockDetails	=	$this->Product->find('all', 
										array( 
											'fields' => array( 
														'Product.current_stock_level, 
														Product.product_sku, 
														Product.store_name,
														ProductDesc.barcode,
														Product.product_name,
														ProductDesc.barcode' 
													) 
											)
										);*/
			$getStockDetails	=	$this->Product->find('all', 
										array( 
											'fields' => array( 
														'Product.current_stock_level, 
														Product.product_sku,
														Product.store_name,
														ProductDesc.barcode,
														Product.product_name,
														ProductDesc.barcode' 
													),
											'conditions' => array('Product.product_sku' => $skusList )
											)
										);
			
			
			foreach( $getStockDetails as $getStockDetailValue)
			{
				$storeIds	=	 explode(',', $getStockDetailValue['Product']['store_name']);
				foreach( $storeIds as $storeIdValue )
				{
					$getStoreName	=	$this->Store->find( 'first', array( 
									'conditions' => array( 'Store.id' => $storeIdValue),
									'fields' => array( 'Store.store_name, Store.id') 
									)
								);
					$storeNameAlies = str_replace('.','_',str_replace(' ','_',$getStoreName['Store']['store_name']));		
					$getResult	=	$this->SkuPercentRecord->find('first', array( 'conditions' => array( 'SkuPercentRecord.store_id' => $getStoreName['Store']['id'], 'SkuPercentRecord.sku' => $getStockDetailValue['Product']['product_sku']) ));
					//echo $getStockDetailValue['Product']['product_sku']."<br>";
						if( !empty( $getResult['SkuPercentRecord']['id'] ) && count($getResult['SkuPercentRecord']['id']) > 0  )
						{
							$data['SkuPercentRecord']['id'] 						= $getResult['SkuPercentRecord']['id'];
							$data['SkuPercentRecord']['store_id'] 					= $getStoreName['Store']['id'];
							$data['SkuPercentRecord']['store_name'] 				= $getStoreName['Store']['store_name'];
							$data['SkuPercentRecord']['store_alies'] 				= $storeNameAlies;
							$data['SkuPercentRecord']['sku'] 						= $getStockDetailValue['Product']['product_sku'];
							$data['SkuPercentRecord']['current_stock'] 				= $getStockDetailValue['Product']['current_stock_level'];
							$data['SkuPercentRecord']['product_title'] 				= $getStockDetailValue['Product']['product_name'];
							$data['SkuPercentRecord']['barcode'] 					= ($getStockDetailValue['ProductDesc']['barcode'] != '') ? $getStockDetailValue['ProductDesc']['barcode'] : '0';
							$this->SkuPercentRecord->saveAll( $data );
						}
						else
						{
							$data['SkuPercentRecord']['id'] 						= 	'';
							$data['SkuPercentRecord']['percentage'] 				= 	'50';
							$data['SkuPercentRecord']['store_id'] 					= $getStoreName['Store']['id'];
							$data['SkuPercentRecord']['store_name'] 				= $getStoreName['Store']['store_name'];
							$data['SkuPercentRecord']['store_alies'] 				= $storeNameAlies;
							$data['SkuPercentRecord']['sku'] 						= $getStockDetailValue['Product']['product_sku'];
							$data['SkuPercentRecord']['current_stock'] 				= $getStockDetailValue['Product']['current_stock_level'];
							$data['SkuPercentRecord']['product_title'] 				= $getStockDetailValue['Product']['product_name'];
							$data['SkuPercentRecord']['barcode'] 					= ($getStockDetailValue['ProductDesc']['barcode'] != '') ? $getStockDetailValue['ProductDesc']['barcode'] : '0';
							$this->SkuPercentRecord->saveAll( $data );
						}  
				}
			}
			$this->Session->setFlash('Store skus set successfully.', 'flash_success');
	 } 
	}
	
	
	public function generateManifestAfter()
	{
		
		$this->layout = '';
		$this->autoRender = false;          
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'ManifestBackup' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'Customer' );
		$this->loadModel( 'Category' );					
		
		
		
		$isoCode = Configure::read('customIsoCodes');
		$glbalSortingCounter = 0;
		
		$serviceProvider = 'PostNL';//$this->request->data['serviceProvider'];
		date_default_timezone_set('Europe/Jersey');
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');                          
		
		//Set and create Active Sheet for single workbook with singlle sheet
		$objPHPExcel = new PHPExcel();       
		$objPHPExcel->createSheet();
		
		//Column Create                              
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'LineNo');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Option');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'LineIdentifier');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'GroupageManifestNo');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Consignor');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'ConsignorAddress1');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'ConsignorAddress2');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'ConsignorPostCode');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'ConsigneeName');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'ConsigneeAddress1');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('K1', 'ConsigneeAddress2');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('L1', 'ConsigneeGSTNumber');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('M1', 'ConsigneePostCode');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('N1', 'ConsigneeCountry');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('O1', 'NoOfUnits');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('P1', 'GrossMass');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Description');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('R1', 'Value');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('S1', 'ValueCurr');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('T1', 'ForwardingAgent');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('U1', 'ForwardingAgentAddress1');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('V1', 'ForwardingAgentAddress2');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('W1', 'ForwardingAgentCountry');                                                                  
		$objPHPExcel->getActiveSheet()->setCellValue('X1', 'CommunityStatus');   
					
		$manifests	=	$this->ServiceCounter->find('all',array( 'conditions' => array( 'service_provider' => array($serviceProvider,'Royalmail') ) ) );
		$inc = 1;$cnt = 2;$e = 0;
		if(!empty($manifests))
		{
			foreach($manifests as $manifest)
			{
				if($manifest['ServiceCounter']['service_provider'] == 'PostNL' || $manifest['ServiceCounter']['service_provider'] == 'Royalmail'){
					if($manifest['ServiceCounter']['order_ids'] != ''){
						$orderIds = explode( ',' , $manifest['ServiceCounter']['order_ids']);
						foreach($orderIds as $k => $v)
						{
							$totalIds[]	=	$v;
						}
					}
				}
			}
		
			$inc = 1;$cnt = 2;$k=0;	$combineSku = '';
			foreach( $totalIds as $ids )
			{
				$params = array('conditions' => array('MergeUpdate.id' => $ids),
					'fields' => array(
						'MergeUpdate.id',
						'MergeUpdate.order_id',
						'MergeUpdate.product_order_id_identify',
						'MergeUpdate.quantity',
						'MergeUpdate.sku',
						'MergeUpdate.price',
						'MergeUpdate.packet_weight',
						'MergeUpdate.envelope_weight'
					)
				);
				
				$mergeOrder = json_decode(json_encode($this->MergeUpdate->find('all', $params)),0);
				$getSku = explode( ',' , $mergeOrder[0]->MergeUpdate->sku);
				
				$packageWeight = $mergeOrder[0]->MergeUpdate->envelope_weight;                    
				$totalPriceValue = 0;$massWeight = 0;$totalUnits = 0;$combineTitle = '';$combineCategory = '';$calculateWeight = 0;
				$getSpecifier = explode('-', $mergeOrder[0]->MergeUpdate->product_order_id_identify);
                $setSpaces = '';
                $identifier = $getSpecifier[1];
				$em = 0;
				while( $em < $identifier ){
					$setSpaces .= $setSpaces.' ';						
					$em++;	
				}
				$j = 0;
				while( $j <= count($getSku)-1 )
					{
						$newSku = explode( 'XS-' , $getSku[$j] );
						$setNewSku = 'S-'.$newSku[1];
						$this->Product->bindModel(array('hasOne' => array('Category' => array('foreignKey' => false,'conditions' => array('Category.id = Product.category_id'),
							   'fields' => array('Category.id,Category.category_name')))));
					
						$productSku = $this->Product->find('first' ,array('conditions' => array('Product.product_sku' => $setNewSku)));
						if( $combineTitle == '' )
						{
							$combineTitle		= $newSku[0] .'X' .substr($productSku['Product']['product_name'],0,25);	
							$totalUnits 		= $totalUnits + $newSku[0];     
                            $calculateWeight 	= ($newSku[0] * $productSku['ProductDesc']['weight']);
							$massWeight 		= $massWeight + $calculateWeight;		
							$combineCategory 	= $productSku['Category']['category_name'];				
						}
						else
						{
							$combineTitle .= ',' .  $newSku[0] . 'X' .substr($productSku['Product']['product_name'],0,25);	
							$totalUnits 		= 	$totalUnits + $newSku[0];
                            $calculateWeight 	= 	($newSku[0] * $productSku['ProductDesc']['weight']);
							$massWeight 		= 	$massWeight + $calculateWeight;                                                        
							$combineCategory .= ',' . $productSku['Category']['category_name'];				
						}
						if( $combineSku == '' )
							$combineSku = $setNewSku;	
						else
							$combineSku .= ',' . $setNewSku;	
						
						$j++;	
					}
					$massWeight = $packageWeight + $massWeight; 
                  	$objPHPExcel->getActiveSheet()->setCellValue('A'.$cnt, $inc );
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$cnt, 'N' );
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$cnt, $mergeOrder[0]->MergeUpdate->product_order_id_identify );
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$cnt, '' );
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$cnt, 'ESL Limited' );
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$cnt, 'Unit 4 Airport Cargo Centre' );
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$cnt, 'L\'avenue De La Comune; Jersey' );
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$cnt, 'JE3 7BY' );
				
					$paramsConsignee = array('conditions' => array('OpenOrder.num_order_id' => $mergeOrder[0]->MergeUpdate->order_id),
											'fields' => array(
												'OpenOrder.num_order_id',
												'OpenOrder.id',
												'OpenOrder.general_info',
												'OpenOrder.shipping_info',
												'OpenOrder.customer_info',
												'OpenOrder.totals_info'	));
					$getConsigneeDetailFromLinnworksOrder = json_decode(json_encode($this->OpenOrder->find( 'first', $paramsConsignee )),0);					
					$congineeInfo 	= unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->customer_info);					
					$totalInfo 		= unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->totals_info);
					$postcountry 	= $congineeInfo->Address->Country;
					$previousDate	=  date('Y-m-d h:i:s', strtotime('-10 days'));
					$exteraword = '';
					 if($identifier >= 2)
						{
						  $getCustomerDetail =	$this->Customer->find('all', array( 
						  'conditions' => array('Customer.country'=>$postcountry,'and'=>array('Customer.date >'=>'2018-01-01 00:00:00','Customer.date <' => $previousDate)),
									 			'order' => 'rand()','limit' => 1,));
							$customerName		=	$getCustomerDetail[0]['Customer']['name'].' ';
							$customerAddress1	=	$getCustomerDetail[0]['Customer']['address1'].' ';
							$customerAddress2	=	$getCustomerDetail[0]['Customer']['address2'].' ';
						}
						else
						{
							$customerName 		= $congineeInfo->Address->FullName;
							$customerAddress1 	= $congineeInfo->Address->Address1;
							$customerAddress2 	= $congineeInfo->Address->Address2;
						}
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$cnt, str_replace(',',';', $customerName) );
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$cnt, str_replace(',',';', $customerAddress1) );
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$cnt, '' );
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$cnt, '' );
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$cnt, str_replace(',',';', $congineeInfo->Address->PostCode) );
					$setSpaces = '';
					$country = '';					
					foreach( $isoCode as $index => $value ){
						if( $index == $congineeInfo->Address->Country ){
							$country = $value;
						}
					}
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$cnt, $country );
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$cnt, $totalUnits );
					$objPHPExcel->getActiveSheet()->setCellValue('P'.$cnt, $massWeight );
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$cnt, str_replace(',',';', $combineCategory ) );
					$currencyMatter = $totalInfo->Currency;
					$globalCurrencyConversion = 1;
					if( $currencyMatter == "EUR" ){
						$globalCurrencyConversion = 1;
					} else {
						$globalCurrencyConversion = 1.38;
					}
					$totalValue = 0;
					$setPrice = $mergeOrder[0]->MergeUpdate->price;
					if( ( $setPrice * $globalCurrencyConversion) > 21.99 ){
						$totalValue = number_format($this->getrand(), 2, '.', ''); 
					} else {
						$totalValue = number_format(( $setPrice * $globalCurrencyConversion ), 2, '.', '');
					}
					
					$objPHPExcel->getActiveSheet()->setCellValue('R'.$cnt, $totalValue );
					$objPHPExcel->getActiveSheet()->setCellValue('S'.$cnt, 'EUR' );
					$objPHPExcel->getActiveSheet()->setCellValue('T'.$cnt, 'ESL Limited' );
					$objPHPExcel->getActiveSheet()->setCellValue('U'.$cnt, 'Unit 4 Airport Cargo Centre' );
					$objPHPExcel->getActiveSheet()->setCellValue('V'.$cnt, "L'avenue De La Comune; JE3 7BY" );
					$objPHPExcel->getActiveSheet()->setCellValue('W'.$cnt, "JE" );
					$objPHPExcel->getActiveSheet()->setCellValue('X'.$cnt, "T2" );
					
					$combineSku = '';$totalUnits = 0;$massWeight = 0;$combineCategory = '';
					$inc++;	$cnt++;	$k++;	
		   }
		}
		
		$uploadUrl = WWW_ROOT .'img/' .$serviceProvider.'_'.date('d-m-Y').'B.csv';                                          
		$uploadUrI = Router::url('/', true) . 'img/' .$serviceProvider.'_'.date('d-m-Y').'B.csv';                                          
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
		$objWriter->save($uploadUrl);
		$result	=	Router::url('/', true)."img/".$serviceProvider.'_'.date('d-m-Y')."B.csv";
			
		header('Content-Description: File Transfer');
		header('Content-Type: application/force-download');
		header('Content-Disposition: attachment; filename='.basename($result));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public'); 
		readfile($result);
		exit;
	}
	
	
	public function getrand()
	   {
		   (float)$min=20.10;
		   (float)$max=21.99;
		   return ($min + ($max - $min) * (mt_rand() / mt_getrandmax()));	    	
	   }
	   
	public function generateChannelskuFeedfile()
	{
		$this->loadMOdel('SkuAsin');
		$this->loadMOdel('Product');
		$store = 'CostBreaker_DE';
		$sep = "\t";
		$gethannelskus	=	$this->SkuAsin->find('all');
		//$gethannelskus	=	$this->SkuAsin->find('all', array( 'conditions' => array( 'channel' => $store ) ) );
		$path = WWW_ROOT.'/img/';
		$filename = 'StockFeed_'.$store.'.txt';
		$header = "Product Sku".$sep."Channel Sku".$sep."Asin".$sep."Original Stock".$sep."channel_sku_per".$sep.$store."\r\n";
		file_put_contents($path.$filename,$header, LOCK_EX);
		foreach($gethannelskus as $gethannelsku)
		{
			$sku 			= 	$gethannelsku['SkuAsin']['sku'];
			$channel_sku 	= 	$gethannelsku['SkuAsin']['channel_sku'];
			$asin 			= 	$gethannelsku['SkuAsin']['asin'];
			$product	=	$this->Product->find('first', array('conditions'=>array('product_sku'=>$sku ),'fields'=>array('Product.current_stock_level','ProductDesc.product_type','ProductDesc.product_identifier','ProductDesc.product_defined_skus')));
			if(count($product) > 0)
			{
					if($product['ProductDesc']['product_type'] == 'Single')
					{
						$current_stock		=	$product['Product']['current_stock_level'];
						$cs_per				=	$gethannelsku['SkuAsin']['percentage'];
						$stock_percentage 	= 	floor(($current_stock * $cs_per)/100);
						$content = $sku.$sep.$channel_sku.$sep.$asin.$sep.$current_stock.$sep.$cs_per.$sep.$stock_percentage."\r\n";
						file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);	
					}
					else if( $product['ProductDesc']['product_type'] == 'Bundle' )
					{
							if($product['ProductDesc']['product_identifier'] == 'Single')
							{
								$qty 			= 	explode('-',$gethannelsku['SkuAsin']['sku']);
								if(count($qty) == '3'){
									$stock = $this->Product->find('first',array('conditions'=>array('product_sku'=>'S-'.$qty[1]),'fields'=>array('Product.current_stock_level')));
									$current_stock		=	floor($stock['Product']['current_stock_level']/$qty[2]);
								}
								$cs_per				=	$gethannelsku['SkuAsin']['percentage'];
								$stock_percentage 	= 	floor(($current_stock * $cs_per)/100);
								$content = $sku.$sep.$channel_sku.$sep.$asin.$sep.$current_stock.$sep.$cs_per.$sep.$stock_percentage."\r\n";
								file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);	
							}
							else if($product['ProductDesc']['product_identifier'] == 'Multiple')
							{
								$pr_dif_skus	=	explode(':', $product['ProductDesc']['product_defined_skus']);
								$p_stock = array();
								foreach( $pr_dif_skus as $pr_dif_sku )
								{
									$stock_ds = $this->Product->find('first',array('conditions'=>array('product_sku'=>$pr_dif_sku),'fields'=>array('Product.current_stock_level')));
									if(count($stock_ds) == 1){
										$p_stock[]		=	$stock_ds['Product']['current_stock_level'];
									} else {
										$p_stock[]		=	'0';
									}
								}
								$m_stock 		=	min($p_stock);
								$cs_per				=	$gethannelsku['SkuAsin']['percentage'];
								$stock_percentage 	= 	floor(($m_stock * $cs_per)/100);
								$current_stock 		= 	implode(';', $p_stock);
								$content = $sku.$sep.$channel_sku.$sep.$asin.$sep.$current_stock.$sep.$cs_per.$sep.$stock_percentage."\r\n";
								file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);	
							}
					}
			}
		} exit;
	}
	
	public function setpercentage()
	{
		$this->loadMOdel('SkuAsin');
		$this->loadMOdel('Product');
		
		$products	=	$this->Product->find('all', array( 'fields' => 'product_sku'));
		foreach($products as $product)
		{
			$sku			=	$product['Product']['product_sku'];
			$getchannelskus	=	$this->SkuAsin->find('all', array( 'conditions' => array( 'SkuAsin.sku' => $sku ) ) );	
			if(count($getchannelskus) > 0)
			{
				foreach( $getchannelskus as $getchannelsku )
				{
					$salesums[$getchannelsku['SkuAsin']['sku']][] = $getchannelsku['SkuAsin']['total_sale'];
				}
				$count = array_sum($salesums[$getchannelsku['SkuAsin']['sku']]);
				foreach( $getchannelskus as $asin )
				{
					$c_sku['id']			=	 $asin['SkuAsin']['id'];
					//$c_sku['channel_sku']	=	 $asin['SkuAsin']['channel_sku'];
					if($asin['SkuAsin']['total_sale'] > 0){
						$c_sku['percentage']	=	floor(($asin['SkuAsin']['total_sale']/$count)*100);
					} else {
						$c_sku['percentage']	=	'0';
					}
					$this->SkuAsin->saveAll( $c_sku );
				}
			}	
		}
	}
	
}

?>
