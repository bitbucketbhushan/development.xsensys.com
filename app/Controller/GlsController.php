<?php
//error_reporting(0);
class GlsController extends AppController
{
    
	var $name = "Gls";    
	var $components = array('Session', 'Common', 'Upload','Auth');    
	var $helpers = array('Html','Form','Common','Session');
	var $uidcliente = "6BAB7A53-3B6D-4D5A-9450-702D2FAC0B11";
	
	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->layout = false;
		$this->Auth->Allow(array('applyGls','getSlipLabel'));			
	}
	public function getTest()
	{
		$url='http://cost-dropper.com/amazon/Inventoryuploadfyndiq.csv';
		$xml = file_get_contents($url);
		pr($xml);
		exit;
	}
	public function getCurrencyRate()
	{	 			
		$this->loadModel('CurrencyExchangeRate');	
		
		$curencies = array('INR','EUR','USD');
		$url='http://www.floatrates.com/daily/gbp.xml';
		$xml = file_get_contents($url);
		$curArray = json_decode(json_encode(simplexml_load_string($xml)),true);
		if(count($curArray) > 0 ){
			$this->CurrencyExchangeRate->query("TRUNCATE currency_exchange_rates");
			foreach($curArray['item'] as $val) {
				if (in_array($val['targetCurrency'],$curencies)) {			
					$curency['base_currency'] = 'GBP';
					$curency['exchange_rate'] = $val['exchangeRate'];	
					$curency['target_currency'] = $val['targetCurrency'];					
					$this->CurrencyExchangeRate->saveAll($curency);	
				}
			}		
		}
		
		$curency = $this->CurrencyExchangeRate->find("all"); 
		foreach($curency as $val){
			$curencyArr[$val['CurrencyExchangeRate']['target_currency']] = $val['CurrencyExchangeRate']['exchange_rate'];
		}
				
		return $curencyArr; 			
 	}
 	public function applyGls($order_id = null){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'OrderLocation' );
		$this->loadModel( 'PostalServiceDesc' );
		$this->loadModel( 'GlsSpain' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'Country' );	
 		$dhl 			  = 2.05;//eur 
 		$currency 		  = $this->getCurrencyRate();
		$exchange_rate	  = $currency ['EUR'];  
		$return 		  = array();
		$msg 			  = '';
		 
		$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id,'MergeUpdate.status' => 0 , 'MergeUpdate.gls_shipment_ref IS NULL ', 'MergeUpdate.delevery_country' => 'Spain')));	
		
  		$inc_id = 0;
		$mdata = [];
		$all_skus = [];
		$length = $width = 0; $_height = array(); $_weight = array(); $data = array();$_price = []; 
		if(count($orders) >0){
			
			foreach($orders  as $orderItem){
 				 				 				
				$_quantity[] 	  	= $orderItem['MergeUpdate']['quantity'];
				$_price[] 	  		= $orderItem['MergeUpdate']['price'];
				$_packet_weight[]	= $orderItem['MergeUpdate']['packet_weight'];				
				$packet_length 		= $orderItem['MergeUpdate']['packet_length'];
				$packet_width 		= $orderItem['MergeUpdate']['packet_width'];
				$_packet_height[]	= $orderItem['MergeUpdate']['packet_height'];
				
				$order_id 	   		= $orderItem['MergeUpdate']['order_id'];
				if($orderItem['MergeUpdate']['order_id'].'-1' == $orderItem['MergeUpdate']['product_order_id_identify']){
					$inc_id = $orderItem['MergeUpdate']['id'];
				}
  				
 				$pos    = strpos($orderItem['MergeUpdate']['sku'],",");
				$all_skus[] = $orderItem['MergeUpdate']['sku'];
				if ($pos === false) {
					$val  = $orderItem['MergeUpdate']['sku'];
					$s    = explode("X", $val);
					$_qty = $s[0]; 
					$_sku = $s[1];	
 					 					
					$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name','ProductDesc.barcode')));
					if(count($product) > 0){
						$_weight[] 	= $product['ProductDesc']['weight'] * $_qty;		
						$_height[] 	= $product['ProductDesc']['height'];		
						$length 	= $product['ProductDesc']['length'];
						$width 		= $product['ProductDesc']['width']; 
						$barcode	= $product['ProductDesc']['barcode'];
					 }
					 
					 $mdata[] = ['sku'=>$_sku, 'quantity' => $_qty, 'order_id' => $orderItem['MergeUpdate']['order_id'],'barcode'=>$barcode,'data_con'=>3];
					
				}else{			
					$sks = explode(",",$orderItem['MergeUpdate']['sku']);
					$_weight = array();					 
					foreach($sks as $val){
						$s = explode("X", $val);
						$_qty = $s[0]; 
						$_sku = $s[1]; 
						$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name','ProductDesc.barcode')));
						if(count($product) > 0)	{
							$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
							$_height[] = $product['ProductDesc']['height'];
							$length = $product['ProductDesc']['length'];	
							$width  = $product['ProductDesc']['width']; 
							$barcode	= $product['ProductDesc']['barcode'];
						}
						
						$mdata[] = ['sku'=>$_sku, 'quantity' => $_qty, 'order_id' => $orderItem['MergeUpdate']['order_id'],'barcode'=>$barcode,'data_con'=>3];				
					}							 			
				}
 			}	
			
		 
			$price 	 	 	= array_sum($_price);
			$quantity 	  	= array_sum($_quantity);
			$packet_weight	= array_sum($_packet_weight);
			$packet_height	= array_sum($_packet_height);
 			
			if($packet_weight == 0){
				$packet_weight = array_sum($_weight);	
				$data['packet_weight'] = $packet_weight;
			}
			if($packet_height == 0){
				$packet_height = array_sum($_height);
				$data['packet_height'] = $packet_height;
			}
			if($packet_length == 0){
				$packet_length = $length; 
				$data['packet_length'] = $packet_length; 
			}
			if($packet_width == 0){
				$packet_width = $width;  
				$data['packet_width'] = $packet_width;
			}	 
			/*-------------------gls Mail Calculations------------------------*/		
			 $final_dim = array();
		 
			 $dim = array($packet_width,$packet_height,$packet_length);
			 asort($dim);
			 $final_dim = array_values($dim) ;							
			 $filterResults = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'GLS' ),'order'=>'PostalServiceDesc.per_item ASC'));
		 
 			$all_gls    = array(); 
			$all_postnl = array(); 
			$glsFee = 0; $postNLFee = 0;
			
			if(count($filterResults) > 0)
			{
				foreach($filterResults as $rv){
			 
					$per_item 		  = $rv['PostalServiceDesc']['per_item'];
					$per_kg 		  = $rv['PostalServiceDesc']['per_kilo'];
					$psd_id 		  = $rv['PostalServiceDesc']['id'];
					$all_gls[$psd_id] = (($per_item + ($per_kg * $packet_weight)) / $exchange_rate ) + ($dhl * $packet_weight);
														 
				} 
				//pr($all_gls);
				$glsFee   = min($all_gls);	
				$post_id  = array_search($glsFee, $all_gls); 
				$post_gls = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));	
				 
				//echo $glsFee;						 
				
				/*-------------------PostNL Calculations------------------------*/
				$postNlPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey',
								'Location.county_name' => 'Spain', 'PostalServiceDesc.max_weight >=' => $packet_weight, 'PostalServiceDesc.max_length >=' => $packet_length, 'PostalServiceDesc.max_width >=' => $packet_width, 'PostalServiceDesc.max_height >=' => $packet_height, 'PostalServiceDesc.courier' => 'Belgium Post')));
								
				$all_postnl = array();
				foreach($postNlPostal as $pv){
					$per_item 	= $pv['PostalServiceDesc']['per_item'];
					$per_kg 	= $pv['PostalServiceDesc']['per_kilo'];
					$weightKilo = $pv['PostalServiceDesc']['max_weight'];						
					$psd_id 	= $pv['PostalServiceDesc']['id'];
				
					$all_postnl[$psd_id] = (($per_item + ($per_kg * $packet_weight)) / $exchange_rate ) + ($dhl * $packet_weight);
				} 
				
				if(count($all_postnl) >0){	
					$postNLFee  = min($all_postnl);	
				}
				// pr($postNlPostal);
				 // pr($all_postnl);
				 
				  /* echo $postNLFee .' glsFee='.$glsFee ." = ".$order_id;
				   echo "<br>";
						 
 					pr($all_skus);
					pr($final_dim);
					echo "packet_weight=".$packet_weight; */
					
 					$order_location = $this->OrderLocation->find('all',array('conditions' => array('order_id' => $order_id,'status' =>'active')));
					
					if(count($order_location) > 0){
 						$this->OrderLocation->updateAll(array( 'OrderLocation.status' =>"'deleted'"), array( 'OrderLocation.order_id' => $order_id,'status' =>'active' ) ); 	
  						
					}
					foreach($mdata as $md){
						$this->order_location($md);
					}
					 
					
					$item_sku 	 = [];
					$open_order  = $this->OpenOrder->find('first',array('conditions' => array('num_order_id' => $order_id)));
					$items 		 = unserialize($open_order['OpenOrder']['items']);
					if(count($items) > 0){
 						foreach($items as $v){
							$item_skus[] 	= $v->Quantity.'X'.$v->SKU;
							$item_barcode[] = $v->BarcodeNumber;//['quantity' => $v->Quantity,'barcode'=>$v->BarcodeNumber];
 						}
					}
					 
					 
 					
				/*-------------------Compare And Replace------------------------*/
				if($postNLFee == 0 || $postNLFee > $glsFee) {
					
					if($inc_id > 0){
 						
						$_orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id)));
						foreach($_orders  as $orderItem){
							if($orderItem['MergeUpdate']['id'] == $inc_id){
								$data['sku'] 			= implode(",",$item_skus);
								$data['barcode'] 		= implode(",",$item_barcode);	
								$data['price'] 			= $price;	
								$data['quantity'] 		= $quantity;	
								 
								$data['packet_weight'] 		= $packet_weight;						 
								$data['service_name'] 		= $post_gls['PostalServiceDesc']['service_name'];
								$data['track_id'] 			= '';
								$data['reg_post_number'] 	= '';
								$data['reg_num_img'] 		= '';						 
								$data['provider_ref_code'] 	= $post_gls['PostalServiceDesc']['provider_ref_code'];
								$data['service_id'] 		= $post_gls['PostalServiceDesc']['id'];
								$data['service_provider'] 	= $post_gls['PostalServiceDesc']['courier'];								 
								$data['template_id'] 		= $post_gls['PostalServiceDesc']['template_id'];  
								$data['id'] 				= $inc_id;
								
								file_put_contents(WWW_ROOT .'logs/pnl_to_gls_'.date('ymd').'.log' , $orderItem['MergeUpdate']['product_order_id_identify']."\t".$orderItem['MergeUpdate']['service_id']."\n",  FILE_APPEND|LOCK_EX);	
								 
								$this->loadModel( 'MergeUpdate' );				 
								$this->MergeUpdate->saveAll( $data );									
								$msg .= $orderItem['MergeUpdate']['product_order_id_identify']."\t<br>";
							}else{
								$msg .= $orderItem['MergeUpdate']['product_order_id_identify']." is deleted.<br>";
 							 	$this->MergeUpdate->query( "DELETE from merge_updates where product_order_id_identify = '".$orderItem['MergeUpdate']['product_order_id_identify']."'" );
								
							}
						}
					}
						 
				}else{
					 $msg =  ' No order found in condition - 1.<br>';
				}
				/*------------------end--------------------------------*/
			}else{
				 $msg = ' This order may have over weight or any other issue.';
 			}
				 
		 }else{
			 $msg =  ' No order found.<br>';
		}
		
		file_put_contents(WWW_ROOT."logs/gls_app_by_order_".date('dmy').".log", date('Y-m-d H:i:s')."\t". $order_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);	
  	}
	
	public function order_location($data = array()){
		
		$this->loadModel( 'OrderLocation' );
		$this->loadModel( 'BinLocation' );		
		$this->loadModel( 'CheckIn');	
		 
		if(count($data) > 0){
				
			$quantity = $data['quantity'];					
			$sku      = $data['sku'];
			$barcode  = $data['barcode'];
			$order_id = $data['order_id']; 
			$split_order_id = $data['order_id'].'-1';
			
			$data_con = 3;
			if(isset($data['data_con'])){
				$data_con = $data['data_con']; 
			}
					
			$po_name = array(); $posIds = array(); $pos = array(); $binname = array(); 
			$available_qty = 0; $available_qty_bin = 0;	
			
			$local_barcode = $this->Components->load('Common')->getLocalBarcode($barcode);
			
			$poDetail = $this->CheckIn->find( 'all', 
				array( 'conditions' => array( 'CheckIn.barcode IN' => array($barcode,$local_barcode), 'CheckIn.selling_qty != CheckIn.qty_checkIn','po_name !=""'),
						'order' => 'CheckIn.date  ASC ' ) );
			  			
			if(count($poDetail) > 0){
				$qt_count = 0; 				
				foreach($poDetail as $po){
					$poQty = $po['CheckIn']['qty_checkIn'] - $po['CheckIn']['selling_qty'];
					if($poQty > 0){						
						 for($i=0; $i <= $quantity ;$i++){						 
							 if($qt_count >= $quantity ){
								 break;
							 }else if(($poQty - $i)  > 0){							
								$po_name[$po['CheckIn']['id']][] = $po['CheckIn']['po_name'];	
								$available_qty++;							
								$qt_count++;
							}												
						 }						
					}
				}
			}
			 
			
			$getStock = $this->BinLocation->find( 'all', array( 'conditions' => array( 'BinLocation.barcode' => $barcode),'order' => 'priority ASC' ) );
			$bin_name = array(); $finalData['bin_location'] = "";
			if(count($getStock) > 0){
 				$qt_count = 0 ;
				foreach($getStock as $val){
 					$binData['id'] = $val['BinLocation']['id'];					
					 for($i=0; $i <= $quantity ;$i++){						 
						 if($qt_count >= $quantity ){
							 break;
						 }else if(($val['BinLocation']['stock_by_location'] - $i)  > 0){							
							$bin_name[$val['BinLocation']['id']][] = $val['BinLocation']['bin_location'];	
							$available_qty_bin++;						
							$qt_count++;
						}												
					 }						 
 				}				
 			}
		 					
			 
			if(count($po_name) > 0){
				foreach(array_keys($po_name) as $k){
					$qts   = count($po_name[$k]);	
					$pos[] = $po_name[$k][0];
					$posIds[] = $k;
					//$this->CheckIn->updateAll( array('CheckIn.selling_qty' =>  "CheckIn.selling_qty + $qts"),array('CheckIn.id' => $k));
				}
			}
				
			if(count($bin_name) > 0)
			{
				foreach(array_keys($bin_name) as $k){					
					$qts	   = count($bin_name[$k]);	
					$binname[] = $bin_name[$k][0];	
					
					$finalData['available_qty_check_in'] = $available_qty;
					$finalData['available_qty_bin'] = $qts;
					$finalData['quantity']      	= $quantity;
					$finalData['order_id']			= $order_id;	
					$finalData['split_order_id']	= $split_order_id;					
					$finalData['sku']	   			= $sku;								
					$finalData['barcode']  			= $barcode;	
					$finalData['po_name']  			= implode(",",$pos);	
					$finalData['po_id']    			= implode(",",$posIds);	
					$finalData['bin_location']  	= $bin_name[$k][0];	
					$finalData['data_con']  		= $data_con;					
					$this->OrderLocation->saveAll( $finalData );
					
					/*$this->BinLocation->updateAll( array('BinLocation.stock_by_location' =>  "BinLocation.stock_by_location - $qts"),array('BinLocation.id' => $k));*/
				}
			} else {
					
						$finalData['available_qty_check_in'] = $available_qty;
						$finalData['available_qty_bin'] = '0';
						$finalData['quantity']      	= $quantity;
						$finalData['order_id']			= $order_id;
						$finalData['split_order_id']	= $split_order_id;						
						$finalData['sku']	   			= $sku;								
						$finalData['barcode']  			= $barcode;	
						$finalData['po_name']  			= implode(",",$pos);	
						$finalData['po_id']    			= implode(",",$posIds);	
						$finalData['bin_location']  	= 'No Location';
						$finalData['data_con']  		= $data_con;					
						$this->OrderLocation->saveAll( $finalData );
				}	
			 
 			}						
	}
	
	public function applyGls_123($order_id = null){
		 
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'PostalServiceDesc' );	 
		$this->loadModel( 'GlsSpain' );
		$this->loadModel( 'Country' );
		 
 		$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id,'MergeUpdate.status' =>1 ,   'MergeUpdate.delevery_country' => 'Spain')));	
		 
		if(count($orders) > 0){
			
			App::import( 'Controller' , 'Cronjobs' );		
			$objController 	= new CronjobsController();
			$getorderDetail	= $objController->getOpenOrderById($order_id);
			$cInfo = $getorderDetail['customer_info']; 			
			$TotalsInfo = $getorderDetail['totals_info'];
			
 			foreach($orders  as $orderItems){
			 
			$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $orderItems['MergeUpdate']['product_order_id_identify'],'MergeUpdate.status' => 1 ,'MergeUpdate.delevery_country' => 'Spain')));	
					 
				if(count($orderItem) > 0)
				{
  					$source_coming 	 = $orderItem['MergeUpdate']['source_coming'];
					$quantity 	   	 = $orderItem['MergeUpdate']['quantity'];
					$packet_weight 	 = $orderItem['MergeUpdate']['packet_weight']; 
					$packet_length 	 = $orderItem['MergeUpdate']['packet_length']; 
					$packet_height 	 = $orderItem['MergeUpdate']['packet_height']; 
					$packet_width 	 = $orderItem['MergeUpdate']['packet_width']; 
					$order_id 	  	 = $orderItem['MergeUpdate']['order_id'];
					$inc_order_id 	 = $orderItem['MergeUpdate']['id'];
					$split_order_id  = $orderItem['MergeUpdate']['product_order_id_identify'];
					$country 		 = $cInfo->Address->Country;
					$weight		   	 = $packet_weight;
				 				
					$dim = array($packet_width,$packet_height,$packet_length);
					asort($dim);
					$final_dim = array_values($dim) ;	
					
 					$filterResults = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'GLS' ),'order'=>'PostalServiceDesc.per_item ASC'));
					$all_postal = array();
					 
					foreach($filterResults as $pv){
				 
						$per_item 	= $pv['PostalServiceDesc']['per_item'];
						$per_kg 	= $pv['PostalServiceDesc']['per_kilo'];
						$psd_id 	= $pv['PostalServiceDesc']['id'];
						$all_postal[ $psd_id ] = ( $per_item + ($per_kg * $packet_weight));
									 
					} 
					
					$postalFee  = min($all_postal);	
					$post_id    = array_search($postalFee, $all_postal); 
					$postal_service = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));				  		
					$Delivery_Name = utf8_decode(substr($cInfo->Address->FullName,0,78)); 
					$Delivery_Address = utf8_decode(substr(($cInfo->Address->Address1.' '.$cInfo->Address->Address2),0,78));
					 
					$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
					$randomNum = substr(str_shuffle("0123456789"), 0, 5);
					$ShipmentReference = $randomNum.'-'. $split_order_id;
					
 					$sender = 'FRESHER BUSINESS LIMITED C/O G.L. Systems'; 
 					if(strpos($source_coming,'CostBreaker')!== false){
						$sender = 'EURACO GROUP LTD C/O G.L. Systems'; 
					}else if(strpos($source_coming,'Marec')!== false){					 
						$sender = 'ESL LIMITED C/O G.L. Systems'; 
					}else if(strpos($source_coming,'RAINBOW')!== false){						
						$sender = 'FRESHER BUSINESS LIMITED C/O G.L. Systems'; 
					}else if(strpos($source_coming,'Tech_Drive')!== false){						
						$sender = 'TD Supplies  C/O G.L. Systems'; 
					}elseif(strpos($source_coming,'BBD')!== false){
						$sender = 'JIJ Hongkong Ltd C/O G.L. Systems'; 
					}
 					
 					$URL = "https://wsclientes.asmred.com/b2b.asmx?wsdl";
				 
 					$XML= '<?xml version="1.0" encoding="utf-8"?>
					<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
					<soap12:Body>
					<GrabaServicios  xmlns="http://www.asmred.com/">
					<docIn>
					<Servicios uidcliente="'.$this->uidcliente.'" xmlns="http://www.asmred.com/">
					<Envio>
					<Fecha>' . date('d/m/Y'). '</Fecha>
					<Servicio>1</Servicio>
					<Horario>3</Horario>
					<Bultos>1</Bultos>
					<Peso>' . ceil($weight) . '</Peso>
					<Portes>P</Portes>
					<Importes>
					  <Reembolso>0</Reembolso>
					</Importes>
					<Remite>
					  <Nombre>'.$sender.'</Nombre>
					  <Direccion>Street / Fontaneros 3</Direccion>
					  <Poblacion>San Fernando de Henares</Poblacion>					 
					  <CP>28330</CP>
					</Remite>
					<Destinatario>
					  <Nombre>' . $Delivery_Name. '</Nombre>
					  <Direccion>' . $Delivery_Address. '</Direccion>
					  <Poblacion>' . ($cInfo->Address->Town) . '</Poblacion>
					  <Pais>' .  $country_data['Country']['iso_2']. '</Pais>
					  <CP>' . $cInfo->Address->PostCode . '</CP>
					  <Telefono>' . $cInfo->Address->PhoneNumber . '</Telefono>
					  <Email>' . $cInfo->Address->EmailAddress . '</Email>
					  <NIF></NIF>
					  <Observaciones>' . $split_order_id . '</Observaciones>
					</Destinatario>
					<Referencias>
					  <Referencia tipo="C">' . $ShipmentReference. '</Referencia>
					   <Referencia tipo="0">'.$randomNum.'</Referencia> 
					</Referencias>
					</Envio>
					</Servicios>
					</docIn>
					</GrabaServicios>
					</soap12:Body>
					</soap12:Envelope>';
					
					file_put_contents(WWW_ROOT."logs/gls_apply_req_".date('dmy').".txt", $XML ."\n", FILE_APPEND|LOCK_EX);
					
					$ch = curl_init();
					
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_HEADER, FALSE);
					curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
					curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
					curl_setopt($ch, CURLOPT_URL, $URL );
					curl_setopt($ch, CURLOPT_POSTFIELDS, $XML );
					curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=UTF-8"));
 					
					$postResult = curl_exec($ch);
					curl_close($ch);
					//echo 'postResult: ' . $postResult . '<br><br>';
					
					file_put_contents(WWW_ROOT."logs/gls_apply_res_".date('dmy').".txt", $postResult ."\n", FILE_APPEND|LOCK_EX);
					
					$xml = simplexml_load_string($postResult, NULL, NULL, "http://http://www.w3.org/2003/05/soap-envelope");
					$xml->registerXPathNamespace('asm', 'http://www.asmred.com/');
					$arr = $xml->xpath("//asm:GrabaServiciosResponse/asm:GrabaServiciosResult");
					$ret = $arr[0]->xpath("//Servicios/Envio");
					
					$return = $ret[0]->xpath("//Servicios/Envio/Resultado/@return");
					
					$gls = array();
					if($return[0] == 0){
						$cb = $ret[0]->xpath("//Servicios/Envio/@codbarras");
						$gls['shipment_barcode'] = $cb[0]["codbarras"];						
						$uid = $ret[0]->xpath("//Servicios/Envio/@uid");						 
						$gls['shipment_uid'] = $uid[0]["uid"];
						$codexp = $ret[0]->xpath("//Servicios/Envio/@codexp");						 
						$gls['shipment_number'] = $codexp[0]["codexp"]; 
						
						$glsRef = array();	
						$glsRef['id'] 					= $orderItem['MergeUpdate']['id'];
   						$glsRef['service_name'] 		= $postal_service['PostalServiceDesc']['service_name'];
						$glsRef['track_id'] 			= '';
						$glsRef['reg_post_number'] 		= '';
						$glsRef['reg_num_img'] 			= '';
						//$glsRef['postal_service'] 	= 'Standard';
						$glsRef['provider_ref_code'] 	= $postal_service['PostalServiceDesc']['provider_ref_code'];
						$glsRef['service_id'] 			= $postal_service['PostalServiceDesc']['id'];
						$glsRef['service_provider'] 	= $postal_service['PostalServiceDesc']['courier'];								 
						$glsRef['template_id'] 			= $postal_service['PostalServiceDesc']['template_id'];  
						$glsRef['gls_shipment_ref'] 	= $ShipmentReference;
						$glsRef['gls_shipment_barcode'] = $gls['shipment_barcode']; 
						
 						$this->MergeUpdate->saveAll($glsRef);
						
						$gls['split_order_id']  	= $split_order_id;
						$gls['shipment_reference'] 	= $randomNum;
						$this->GlsSpain->saveAll( $gls ); 
						$this->glsLabel($ShipmentReference,$split_order_id);
						//$this->getSlipLabel($ShipmentReference,$split_order_id);
						 
					}else{
						$err = $arr[0]->xpath("//Servicios/Envio/Errores/Error") ;
						file_put_contents(WWW_ROOT."logs/".$ShipmentReference."-err.txt", $postResult);
						$getBase = Router::url('/', true);
						$details['split_order_id'] = $split_order_id;	
						$details['error_msg'] = $err[0];						 
						$details['error_file'] ='<a href="'.$getBase.'logs/'.$ShipmentReference.'-err.txt">'.$ShipmentReference.'</a>' ;
						$this->sendErrorMail($details);
						$this->lockOrder($details);
					}					  
					
				}
			}
		}
	}
 	
	public function glsLabel($shipment_reference = '',$split_order_id = ''){
		   
		$url = "https://wsclientes.asmred.com/b2b.asmx"; 
		$XML='<?xml version="1.0" encoding="utf-8"?>
			<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
			<soap12:Body>
			<EtiquetaEnvio xmlns="http://www.asmred.com/">
				<codigo>'.$shipment_reference.'</codigo>
				<tipoEtiqueta>PDF</tipoEtiqueta>
			</EtiquetaEnvio>
			</soap12:Body>
			</soap12:Envelope>';
		
		file_put_contents(WWW_ROOT."logs/glsLabel-req-".date('dmy').".log", $XML ."\n", FILE_APPEND|LOCK_EX);
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $XML);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=UTF-8"));
		
		$postResult = curl_exec($ch);
		file_put_contents(WWW_ROOT."logs/glsLabel-res-".date('dmy').".log", $postResult ."\n", FILE_APPEND|LOCK_EX);
		$msg = '';
		if (curl_errno($ch)) {
			$msg = 'The GLS WS could not be called.<br>';
		}
		
		$result = strpos($postResult, '<base64Binary>');
		
		if($result == false){
			$msg .= 'No tags have been returned.';
		}
		else {
			 
 			$xml = simplexml_load_string($postResult, NULL, NULL, "http://http://www.w3.org/2003/05/soap-envelope");
			$xml->registerXPathNamespace("asm","http://www.asmred.com/");
			$arr = $xml->xpath("//asm:EtiquetaEnvioResponse/asm:EtiquetaEnvioResult/asm:base64Binary");
		
			$Tot = sizeof($arr);
		
				for( $Num=0 ; $Num <= sizeof($arr)-1 ; $Num++ ) {
					$descodificar = base64_decode($arr[$Num]);
					$NumBul = $Num + 1;
					$Nombre =  $split_order_id . ".pdf";
					file_put_contents(WWW_ROOT."gls/labels/label_".$split_order_id.".pdf", $descodificar);	
					$msg = 'ok'; 
			}
		}
		return $msg;   
	}
	
	public function glsCancelShipment($ref_barcode = ''){
		
 		$url = "https://wsclientes.asmred.com/b2b.asmx";				 
  	    $XML= '<?xml version="1.0" encoding="utf-8"?>
			 <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:asm="http://www.asmred.com/">
			 <soap:Header/>	
			  <soap:Body>			
			 <asm:Anula>
				<asm:docIn>
					<Servicios uidcliente="'.$this->uidcliente.'">
						<Envio codbarras="'.$ref_barcode.'" />					
					</Servicios>
			   </asm:docIn>  
			   </asm:Anula>
			</soap:Body>
			</soap:Envelope>';
		
		 file_put_contents(WWW_ROOT."logs/gls-cancel-shipment-req-".date('dmy').".log", $XML ."\n", FILE_APPEND|LOCK_EX);
	 
 		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $XML);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=UTF-8"));
		
		echo $postResult = curl_exec($ch);
		
		file_put_contents(WWW_ROOT."logs/gls-cancel-shipment-res-".date('dmy').".log", $postResult ."\n", FILE_APPEND|LOCK_EX);
		exit;
	}
	
	public function glsTracking(){
	
 		$Busca = "41038-1750260-1";
		$URL= "https://wsclientes.asmred.com/b2b.asmx?wsdl";
	
		$XML= '<?xml version="1.0" encoding="utf-8"?>
			   <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
				 <soap12:Body>
				   <GetExpCli xmlns="http://www.asmred.com/">
					 <codigo>' . $Busca . '</codigo>
					 <uid>' . $this->uidcliente.'</uid>
				   </GetExpCli>
				 </soap12:Body>
			   </soap12:Envelope>';
	
		file_put_contents(WWW_ROOT."logs/glsTracking-req-".date('dmy').".log", $XML ."\n", FILE_APPEND|LOCK_EX);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($ch, CURLOPT_URL, $URL );
		curl_setopt($ch, CURLOPT_POSTFIELDS, $XML );
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=UTF-8"));
	
		$postResult = curl_exec($ch);
		curl_close($ch);
	
		file_put_contents(WWW_ROOT."logs/glsTracking-res-".date('dmy').".log", $postResult ."\n", FILE_APPEND|LOCK_EX);
		//echo 'uidCliente: ' . $uidCliente . '<br><br>';;
		//echo 'xml: ' . $XML . '<br><br>';
		//echo 'postResult: ' . $postResult . '<br><br>';
	
		$xml = simplexml_load_string($postResult, NULL, NULL, "http://http://www.w3.org/2003/05/soap-envelope");
		$xml->registerXPathNamespace('asm', 'http://www.asmred.com/');
		$arr = $xml->xpath("//asm:GetExpCliResponse/asm:GetExpCliResult");
	
		if (sizeof($arr) == 0){
		   echo '<font size="5">Posiblemente el "UID Cliente" est� mal informado.</font>';
	   }
		else {
		   $ret2 = $arr[0]->xpath("//expediciones/exp");
		   
		   if ($ret2 == null){
			  echo '<font size="5">Ning�n env�o encontrado con la referencia: ' . $Busca . '</font>';
			  echo '<font size="5">No shipment found with reference: ' . $Busca . '</font>';
		  } else {
		   
			  
			  echo '<font size="7">' . sizeof($ret2) . '</font> shipments found with reference: ' . $Busca . '<br><br><br>';
			  
			  $Num = 0;
			  foreach ($ret2 as $ret) {
				 
				 $Num2 = $Num + 1;
			  
				 //=================================================================================================================
				 // RECOGER DATOS DE EXPEDICION.
				 // GET SHIPMENT DATA.
				 //=================================================================================================================
				 $expedicion       = $ret[0]->xpath("//expediciones/exp/expedicion");
				 $albaran          = $ret[0]->xpath("//expediciones/exp/albaran");
				 $codexp           = $ret[0]->xpath("//expediciones/exp/codexp");
				 $codbar           = $ret[0]->xpath("//expediciones/exp/codbar");
				 $uidExp           = $ret[0]->xpath("//expediciones/exp/uidExp");
																						
				 $codplaza_cli     = $ret[0]->xpath("//expediciones/exp/codplaza_cli");
				 $codcli           = $ret[0]->xpath("//expediciones/exp/codcli");
				 $nmCliente        = $ret[0]->xpath("//expediciones/exp/nmCliente");
																						
				 $fecha            = $ret[0]->xpath("//expediciones/exp/fecha");
				 $FPEntrega        = $ret[0]->xpath("//expediciones/exp/FPEntrega");
																						
				 $nombre_org       = $ret[0]->xpath("//expediciones/exp/nombre_org");
				 $nif_org          = $ret[0]->xpath("//expediciones/exp/nif_org");
				 $calle_org        = $ret[0]->xpath("//expediciones/exp/calle_org");
				 $localidad_org    = $ret[0]->xpath("//expediciones/exp/localidad_org");
				 $cp_org           = $ret[0]->xpath("//expediciones/exp/cp_org");
				 $tfno_org         = $ret[0]->xpath("//expediciones/exp/tfno_org");
				 $departamento_org = $ret[0]->xpath("//expediciones/exp/departamento_org");
				 $codpais_org      = $ret[0]->xpath("//expediciones/exp/codpais_org");
																						
				 $nombre_dst       = $ret[0]->xpath("//expediciones/exp/nombre_dst");
				 $nif_dst          = $ret[0]->xpath("//expediciones/exp/nif_dst");
				 $calle_dst        = $ret[0]->xpath("//expediciones/exp/calle_dst");
				 $localidad_dst    = $ret[0]->xpath("//expediciones/exp/localidad_dst");
				 $cp_dst           = $ret[0]->xpath("//expediciones/exp/cp_dst");
				 $tfno_dst         = $ret[0]->xpath("//expediciones/exp/tfno_dst");
				 $departamento_dst = $ret[0]->xpath("//expediciones/exp/departamento_dst");
				 $codpais_dst      = $ret[0]->xpath("//expediciones/exp/codpais_dst");
																						
				 $codServicio      = $ret[0]->xpath("//expediciones/exp/codServicio");
				 $codHorario       = $ret[0]->xpath("//expediciones/exp/codHorario");
				 $servicio         = $ret[0]->xpath("//expediciones/exp/servicio");
				 $horario          = $ret[0]->xpath("//expediciones/exp/horario");
																						
				 $tipo_portes      = $ret[0]->xpath("//expediciones/exp/tipo_portes");
				 $bultos           = $ret[0]->xpath("//expediciones/exp/bultos");
				 $kgs              = $ret[0]->xpath("//expediciones/exp/kgs");
				 $vol              = $ret[0]->xpath("//expediciones/exp/vol");
				 $Observacion      = $ret[0]->xpath("//expediciones/exp/Observacion");
				 $dac              = $ret[0]->xpath("//expediciones/exp/dac");
				 $retorno          = $ret[0]->xpath("//expediciones/exp/retorno");
																						
				 $borrado          = $ret[0]->xpath("//expediciones/exp/borrado");
				 $codestado        = $ret[0]->xpath("//expediciones/exp/codestado");
				 $estado           = $ret[0]->xpath("//expediciones/exp/estado");
				 $incidencia       = $ret[0]->xpath("//expediciones/exp/incidencia");
			  
				 //=================================================================================================================
				 // MOSTRAR DATOS DE EXPEDICION.
				 // SHOW EXPEDITION DATA.
				 //=================================================================================================================
				 echo '<strong><font size="5">ENVIO N�MERO (SHIPMENT NUMBER) ' . $Num2 . '</font></strong><br><br>';
				 
				 echo'<BLOCKQUOTE>';
				 echo '<strong>Datos de identificaci�n (Identification information)</strong><br>';
				 echo '<table border="1" bgcolor="0#b0">';
				 echo '<tr>';
				 echo '<td><strong>Env�o</strong></td>';
				 echo '<td><strong>Expedici�n</strong></td>';
				 echo '<td><strong>Albar�n</strong></td>';
				 echo '<td><strong>Cod.Exp</strong></td>';
				 echo '<td><strong>Cod.Barras</strong></td>';
				 echo '<td><strong>Uid.Exp</strong></td>';
				 echo '</tr>';
				 echo '<tr>';
				 echo '<td>' . '#' . $Num2 . '</td>';
				 echo '<td>' . $expedicion[$Num] . '</td>';
				 echo '<td>' . $albaran[$Num] . '</td>';
				 echo '<td>' . $codexp[$Num] . '</td>';
				 echo '<td>' . $codbar[$Num] . '</td>';
				 echo '<td>' . $uidExp[$Num] . '</td>';
				 echo '</tr></table><br>';
			  
				 echo '<strong>Datos de env�o (Shipment information)</strong><br>';
				 echo '<table border="1" bgcolor="0#b0">';
				 echo '<tr>';
				 echo '<td><strong>Plaza Paga</strong></td>';
				 echo '<td><strong>Cod.Cliente</strong></td>';
				 echo '<td><strong>Nom.Cliente</strong></td>';
				 echo '<td><strong>Fec.Env�o</strong></td>';
				 echo '<td><strong>Fec.Prev.Entrega</strong></td>';
				 echo '</tr>';
				 echo '<tr>';
				 echo '<td>' . $codplaza_cli[$Num] . '</td>';
				 echo '<td>' . $codcli[$Num] . '</td>';
				 echo '<td>' . $nmCliente[$Num] . '</td>';
				 echo '<td>' . $fecha[$Num] . '</td>';
				 echo '<td>' . $FPEntrega[$Num] . '</td>';
				 echo '</tr></table><br>';
			  
				 echo '<strong>Datos de Remitente (Sender information)</strong><br>';
				 echo '<table border="1" bgcolor="#f#f0">';
				 echo '<tr>';
				 echo '<td><strong>Nombre</strong></td>';
				 echo '<td><strong>Nif</strong></td>';
				 echo '<td><strong>Direcci�n</strong></td>';
				 echo '<td><strong>Localidad</strong></td>';
				 echo '<td><strong>Cod.Postal</strong></td>';
				 echo '<td><strong>Tel�fono</strong></td>';
				 echo '<td><strong>Departamento</strong></td>';
				 echo '<td><strong>CodPais</strong></td>';
				 echo '</tr>';
				 echo '<tr>';
				 echo '<td>' . $nombre_org[$Num] . '</td>';
				 echo '<td>' . $nif_org[$Num] . '</td>';
				 echo '<td>' . $calle_org[$Num] . '</td>';
				 echo '<td>' . $localidad_org[$Num] . '</td>';
				 echo '<td>' . $cp_org[$Num] . '</td>';
				 echo '<td>' . $tfno_org[$Num] . '</td>';
				 echo '<td>' . $departamento_org[$Num] . '</td>';
				 echo '<td>' . $codpais_org[$Num] . '</td>';
				 echo '</tr></table><br>';
			  
				 echo '<strong>Datos de Destinatario (Consignee information)</strong><br>';
				 echo '<table border="1" bgcolor="#f#f0">';
				 echo '<tr>';
				 echo '<td><strong>Nombre</strong></td>';
				 echo '<td><strong>Nif</strong></td>';
				 echo '<td><strong>Direcci�n</strong></td>';
				 echo '<td><strong>Localidad</strong></td>';
				 echo '<td><strong>Cod.Postal</strong></td>';
				 echo '<td><strong>Tel�fono</strong></td>';
				 echo '<td><strong>Departamento</strong></td>';
				 echo '<td><strong>CodPais</strong></td>';
				 echo '</tr>';
				 echo '<tr>';
				 echo '<td>' . $nombre_dst[$Num] . '</td>';
				 echo '<td>' . $nif_dst[$Num] . '</td>';
				 echo '<td>' . $calle_dst[$Num] . '</td>';
				 echo '<td>' . $localidad_dst[$Num] . '</td>';
				 echo '<td>' . $cp_dst[$Num] . '</td>';
				 echo '<td>' . $tfno_dst[$Num] . '</td>';
				 echo '<td>' . $departamento_dst[$Num] . '</td>';
				 echo '<td>' . $codpais_dst[$Num] . '</td>';
				 echo '</tr></table><br>';
			  
				 echo '<strong>Otros datos (another information)</strong><br>';
				 echo '<table border="1" bgcolor="#c#c#c">';
				 echo '<tr>';
				 echo '<td><strong>Cod.Servicio</strong></td>';
				 echo '<td><strong>Nom.Servicio</strong></td>';
				 echo '<td><strong>Cod.Horario</strong></td>';
				 echo '<td><strong>Nom.Horario</strong></td>';
				 echo '<td><strong>Portes</strong></td>';
				 echo '<td><strong>Bultos</strong></td>';
				 echo '<td><strong>Kgs</strong></td>';
				 echo '<td><strong>Volumen</strong></td>';
				 echo '<td><strong>Observaciones</strong></td>';
				 echo '<td><strong>RCS</strong></td>';
				 echo '<td><strong>Retorno</strong></td>';
				 echo '<td><strong>Borrado</strong></td>';
				 echo '<td><strong>Cod.Estado.Actual</strong></td>';
				 echo '<td><strong>Nom.Estado.Actual</strong></td>';
				 echo '<td><strong>Nom.Incidencia.Actual</strong></td>';
				 echo '</tr>';
				 echo '<tr>';
				 echo '<td>' . $codServicio[$Num] . '</td>';
				 echo '<td>' . $servicio[$Num] . '</td>';
				 echo '<td>' . $codHorario[$Num] . '</td>';
				 echo '<td>' . $horario[$Num] . '</td>';
				 echo '<td>' . $tipo_portes[$Num] . '</td>';
				 echo '<td>' . $bultos[$Num] . '</td>';
				 echo '<td>' . $kgs[$Num] . '</td>';
				 echo '<td>' . $vol[$Num] . '</td>';
				 echo '<td>' . $Observacion[$Num] . '</td>';
				 echo '<td>' . $dac[$Num] . '</td>';
				 echo '<td>' . $retorno[$Num] . '</td>';
				 echo '<td>' . $borrado[$Num] . '</td>';
				 echo '<td>' . $codestado[$Num] . '</td>';
				 echo '<td>' . $estado[$Num] . '</td>';
				 echo '<td>' . $incidencia[$Num] . '</td>';
				 echo '</tr></table><br>';
			  
				 //=================================================================================================================
				 // MOSTRAR TRACKING.
				 // SHOW TRACKING.
				 //=================================================================================================================
				 echo '<strong>Historial (Tracking)</strong><br>';
				 echo '<table border="1" bgcolor="0#f0">';
				 echo '<tr>';
				 echo '<td><strong>Fecha</strong></td>';
				 echo '<td><strong>Tipo</strong></td>';
				 echo '<td><strong>C�digo</strong></td>';
				 echo '<td><strong>Descripci�n</strong></td>';
				 echo '<td><strong>Cod.Agencia</strong></td>';
				 echo '<td><strong>Nom.Agencia</strong></td>';
				 echo '</tr>';
			  
				 $ret3 = $ret2[$Num]->xpath("tracking_list/tracking");
				 $Num3 = 0;
				 foreach ($ret3 as $ret) {
					$TrkFecha   = $ret[0]->xpath("//expediciones/exp/tracking_list/tracking/fecha");
					$TrkTipo    = $ret[0]->xpath("//expediciones/exp/tracking_list/tracking/tipo");
					$TrkCodigo  = $ret[0]->xpath("//expediciones/exp/tracking_list/tracking/codigo");
					$TrkDesc    = $ret[0]->xpath("//expediciones/exp/tracking_list/tracking/evento");
					$TrkCodAge  = $ret[0]->xpath("//expediciones/exp/tracking_list/tracking/plaza");
					$TrkNomAge  = $ret[0]->xpath("//expediciones/exp/tracking_list/tracking/nombreplaza");
					
					echo '<tr>';
					echo '<td>' . $TrkFecha[$Num3]  . '</td>';
					echo '<td>' . $TrkTipo[$Num3]   . '</td>';
					echo '<td>' . $TrkCodigo[$Num3] . '</td>';
					echo '<td>' . $TrkDesc[$Num3]   . '</td>';
					echo '<td>' . $TrkCodAge[$Num3] . '</td>';
					echo '<td>' . $TrkNomAge[$Num3] . '</td>';
					echo '</tr>';
					
					$Num3 = $Num3 + 1;
				 }
				 echo '</table><br>';
				 
				 //=================================================================================================================
				 // MOSTRAR DIGITALIZACIONES.
				 // SHOW DIGITIZATIONS.
				 //=================================================================================================================
				 echo '<strong>Digitalizaciones (images)</strong><br>';
				 echo '<table border="1" bgcolor="#a#f#f">';
				 echo '<tr>';
				 echo '<td><strong>Fecha</strong></td>';
				 echo '<td><strong>C�digo</strong></td>';
				 echo '<td><strong>Tipo</strong></td>';
				 echo '<td><strong>Imagen</strong></td>';
				 echo '<td><strong>Observaciones</strong></td>';
				 echo '</tr>';
				 
				 $ret4 = $ret2[$Num]->xpath("digitalizaciones/digitalizacion");
				 $Num4 = 0;
			  
				 foreach ($ret4 as $ret) {
					$DigFecha   = $ret[0]->xpath("//expediciones/exp/digitalizaciones/digitalizacion/fecha");
					$DigCodigo  = $ret[0]->xpath("//expediciones/exp/digitalizaciones/digitalizacion/codtipo");
					$DigTipo    = $ret[0]->xpath("//expediciones/exp/digitalizaciones/digitalizacion/tipo");
					$DigImag    = $ret[0]->xpath("//expediciones/exp/digitalizaciones/digitalizacion/imagen");
					$DigObserv  = $ret[0]->xpath("//expediciones/exp/digitalizaciones/digitalizacion/observaciones");
			  
					echo '<tr>';
					echo '<td>' . $DigFecha[$Num4]  . '</td>';
					echo '<td>' . $DigCodigo[$Num4] . '</td>';
					echo '<td>' . $DigTipo[$Num4]   . '</td>';
					echo '<td>' . '<a href="' . $DigImag[$Num4] . '">Ver pod</a>'  . '</td>';
					echo '<td>' . $DigObserv[$Num4] . '</td>';
					echo '</tr>';
			  
					$Num4 = $Num4 + 1;
				 }
				 echo '</table><br>';
				 echo'</BLOCKQUOTE>';
			  
				 $Num = $Num + 1;
			  }
			  echo '</table>';
		   }
		}
	 exit;
	}
	
	public function glsManifest(){
 		
		$this->loadModel( 'MergeUpdate' ); 
 		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'GlsSpain' );
  					
		$orders = $this->MergeUpdate->find('all', array('conditions' => array(	'MergeUpdate.status' => 1, 
																				'MergeUpdate.delevery_country'=>'Spain',
																				'MergeUpdate.scan_status' => 1,
																				'MergeUpdate.sorted_scanned' => 1,
																				'MergeUpdate.manifest_status' => 2,
																				'MergeUpdate.gls_shipment_barcode IS NOT NULL',
																				'MergeUpdate.gls_shipment_ref IS NOT NULL'
																				
																				) ) );
																				
																				
																								
		$orders = $this->MergeUpdate->find('all', array( 'conditions' => array('gls_shipment_ref IS NOT NULL','gls_shipment_barcode IS NOT NULL')) );
		
		if(count($orders) > 0){
			
				$filename = date('Ymd_His')."_gls";
			
			//file_put_contents(WWW_ROOT."gls/manifests/".$filename, "Expedicion,Doc,Ref. GLS,Ref. Client,Date,Service,Hour,Parcels, Kgs, Refnd, Portes,DAC, Ret., Addressee, Department, Address, Location, Country\n");	
			 
				//ob_clean();                                                         
				App::import('Vendor', 'PHPExcel/IOFactory');
				App::import('Vendor', 'PHPExcel');                          
 				 
				$objPHPExcel = new PHPExcel();       
				$objPHPExcel->createSheet();
 				$objPHPExcel->setActiveSheetIndex(0);
				$inc = 1;
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Customer Number:');  
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, '1083');  
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->getFont()->setSize(18)->setBold(true)->getColor()->setRGB('0000FF');	
				
				$inc++;	
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Custome Name:');
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, 'EURACO GROUP LTD'); 				
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->getFont()->setSize(16)->setBold(true)->getColor()->setRGB('008000');	
				
				$inc++;	
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Manifiesto cierre:');  
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, date('d/m/Y H:i:s'));  
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->getFont()->setSize(12)->setBold(true)->getColor()->setRGB('808080');		
				
				$inc++;
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Number of Shipment:'.count($orders));   
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->getFont()->setSize(12)->setBold(true)->getColor()->setRGB('808080');		
				
				$inc++;
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Number of bultos:'.count($orders));   
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->getFont()->setSize(12)->setBold(true)->getColor()->setRGB('808080');		
				$bborder = array(
				  'borders' => array(
						'bottom' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)																
				  )
				);
				$tborder = array(
				  'borders' => array(
						'top' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)																
				  )
				);
				
				$inc++;	$inc++;	
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->applyFromArray($tborder);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Expedicion');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, 'Doc');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, 'Ref. GLS');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 'Ref. Client');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, 'Date');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 'Service');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 'Hour');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 'Parcels');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 'Kgs');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 'Refnd');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 'Portes');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 'DAC');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 'Ret.');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 'Addressee');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 'Department');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, 'Address');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 'Location');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, 'Country');                                                                
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->getFont()->setSize(12)->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->applyFromArray($bborder);		
				foreach($orders as $val){
					$inc++;
					$glsinfo = $this->GlsSpain->find('first', array( 'conditions' => array('shipment_barcode'=>$val['MergeUpdate']['gls_shipment_barcode'],'split_order_id'=>$val['MergeUpdate']['product_order_id_identify'])) );
					
					$sopen = $this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id'=>$val['MergeUpdate']['order_id'])));
					
					$general_info		=	unserialize( $sopen['OpenOrder']['general_info'] );
					$shipping_info		=	unserialize( $sopen['OpenOrder']['shipping_info'] );
					$customer_info		=	unserialize( $sopen['OpenOrder']['customer_info'] );
					$totals_info		=	unserialize( $sopen['OpenOrder']['totals_info'] );
					$items				=	unserialize( $sopen['OpenOrder']['items'] );
					
					$consignee_name 	=	$customer_info->Address->FullName;
					$address1 			= 	$customer_info->Address->Address1;
					$address2 			= 	$customer_info->Address->Address2;
					$consigee_zip_code 	= 	str_pad($customer_info->Address->PostCode,5,"`0",STR_PAD_LEFT);
					$consignee_city 	=	$customer_info->Address->Town; 
					$consignee_country	=   $customer_info->Address->Country;
								
					$consignee_address  = 	rtrim($address1.' '.$address2,' ')." ".$consigee_zip_code ." ".$consignee_city;
					
					if($customer_info->Address->Region != ''){
						$consignee_address .= 	" ". $customer_info->Address->Region;
					}
					
					$expedicion = '771-'.$glsinfo['GlsSpain']['shipment_number'];
					
					//file_put_contents(WWW_ROOT."gls/manifests/".$filename, "{$expedicion},Doc,".$val['MergeUpdate']['gls_shipment_barcode'].",".$val['MergeUpdate']['gls_shipment_ref'].",".$val['MergeUpdate']['merge_order_date'].", ".				$val['MergeUpdate']['service_name'].",0,1,1,,P,,,{$consignee_name},,{$consignee_address},,{$consignee_country}"."\n", FILE_APPEND|LOCK_EX);
					
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $expedicion);                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, '');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $val['MergeUpdate']['gls_shipment_barcode']);                                             
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $val['MergeUpdate']['gls_shipment_ref']);                                                 
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, date('d/m/Y',strtotime($val['MergeUpdate']['merge_order_date'])));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $val['MergeUpdate']['service_name']);                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, '0');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, '0');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, '0');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, '0');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 'P');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, '0');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, '0');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, $consignee_name);                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, '');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, $consignee_address);                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, '');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, $consignee_country); 
				 
			}	
			
 			$uploadUrl = WWW_ROOT .'gls/manifests/'.$filename.'.xlsx';  
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
			$objWriter->save($uploadUrl);		 
		}
		exit;
	}	
	
   	private function lockOrder($details = null){
	
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderNote' );	
		$this->loadModel( 'RoyalmailError' );
		$md = $this->MergeUpdate->find('first', array( 'conditions' => array('product_order_id_identify' => $details['split_order_id']),'fields'=>'order_id' ) );
		if(count($md) > 0){
			$lock_note = $details['error_msg'];
			
			$firstName = $_SESSION['Auth']['User']['first_name'];
			$lastName = $_SESSION['Auth']['User']['last_name'];
 			$mdata['status']    = 3;
 			$this->MergeUpdate->updateAll($mdata, array( 'product_order_id_identify' => $details['split_order_id'] ) );
			$this->OpenOrder->updateAll(array('OpenOrder.status' => 3), array('OpenOrder.num_order_id' => $md['MergeUpdate']['order_id']));
			
			$noteDate['order_id'] = $md['MergeUpdate']['order_id'];
			$noteDate['note'] = $lock_note;
			$noteDate['type'] = 'Lock';
			$noteDate['user'] = $firstName.' '.$lastName;
			$noteDate['date'] = date('Y-m-d H:i:s');
			$this->OrderNote->saveAll( $noteDate );			 
		}
	}	
	
	public function getGlsLabel( $split_id = null, $shipment_reference = null )
	{
 		$url = "https://wsclientes.asmred.com/b2b.asmx"; 
 		// Ahora podemos obtener la etiqueta codificada en base64
	/*	$XML='<?xml version="1.0" encoding="utf-8"?>
			<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
			<soap12:Body>
			<GetPlazaXCP xmlns="http://www.asmred.com/"> 
				<codPais>34</codPais>
				<cp>46530</cp>
			</GetPlazaXCP>
			</soap12:Body>
			</soap12:Envelope>';
			*/
			
			$XML='<?xml version="1.0" encoding="utf-8"?>
			<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
			<soap12:Body>
			<EtiquetaEnvio xmlns="http://www.asmred.com/">
				<codigo>'.$shipment_reference.'</codigo>
				<tipoEtiqueta>PNG</tipoEtiqueta>
			</EtiquetaEnvio>
			</soap12:Body>
			</soap12:Envelope>';
		
		 
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $XML);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=UTF-8"));
		
		$postResult = curl_exec($ch);
 
		$msg = '';
		if (curl_errno($ch)) {
			$msg = 'No se pudo llamar al WS de GLS<br>';
		}
		
		$result = strpos($postResult, '<base64Binary>');
		
		if($result == false){
			$msg = 'No se ha retornado ninguna etiqueta.';
		}
		else {
			  
 			$xml = simplexml_load_string($postResult, NULL, NULL, "http://http://www.w3.org/2003/05/soap-envelope");
			$xml->registerXPathNamespace("asm","http://www.asmred.com/");
			$arr = $xml->xpath("//asm:EtiquetaEnvioResponse/asm:EtiquetaEnvioResult/asm:base64Binary");
		
			$Tot = sizeof($arr);
		
			for( $Num=0 ; $Num <= sizeof($arr)-1 ; $Num++ ) {
				
				$descodificar = base64_decode($arr[$Num]);
				$NumBul = $Num + 1;		
				file_put_contents( WWW_ROOT .'logs/'.$split_id.'.png', trim($descodificar));
				$msg = 'ok';
			}
		}
		
		file_put_contents( WWW_ROOT .'logs/getGlsLabel_'.date('dmy').'.log', $split_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);		
		return $msg; 
	}
		
	private function sendErrorMail($details = null){
	
		 
		$subject   = $details['split_order_id'].' GLS Shipping orders issue in Xsensys';
		$mailBody  = '<p><strong>Error :'.$details['error_msg'].'</strong></p>';
  		$mailBody .= '<p>Error file : '.$details['error_file'].'</p>'; 
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('');
		$email->emailFormat('html');
		$email->from('info@euracogroup.co.uk');
		//$email->to( array('avadhesh.kumar@jijgroup.com','shashi@euracogroup.co.uk','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));	
		$email->to( array('avadhesh.kumar@jijgroup.com'));
		$email->subject( $subject );
	  	$email->send( $mailBody );
	}
	
	public function replaceFrenchChar($string = null){
			
		$unwanted_array = array('�'=>'S', '�'=>'s', '�'=>'Z', '�'=>'z', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E','�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', 'N�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U','�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c','�'=>'e', '�'=>'e', '�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n','n�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o','�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'b', '�'=>'y','�'=>'u','�'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}
	
	public function fileFormat(){
		
		$file = 'Inventoryupload24072019.csv';
		$data =	explode("\n",file_get_contents(WWW_ROOT."logs/".$file));
		
		foreach($data as $val){
			$row ='';
			foreach(explode(",",$val) as $v){
				$row.= utf8_encode($this->replaceFrenchChar($v)).",";
			}
			$row = rtrim($row,",");
			file_put_contents(WWW_ROOT."logs/ii_order.csv", $row ."\n", FILE_APPEND|LOCK_EX);
		}
 
		exit;
	}	
	 
}

 
?>