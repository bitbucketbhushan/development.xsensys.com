<?php
//error_reporting(0); 
class FbaEuOrdersController extends AppController
{  
    
    var $name = "FbaEuOrders";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('fetchOrders','fetchOrderItems'));		
			
    }
 	
  	public function index() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","Europe FBA Orders" );	
 		$this->loadModel('FbaEuOrder'); 
		 
		$this->paginate = array('order'=>'FbaEuOrder.purchase_date DESC','limit' => 100 );
		$all_orders = $this->paginate('FbaEuOrder');
		 
		$this->set( 'all_orders', $all_orders );	
		
	} 
	
	public function search() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Orders" );	
		
		$this->loadModel('FbaEuOrder');
		$this->loadModel('FbaEuOrderItem');
  
			
		$all_orders =  $this->FbaEuOrder->find('all', array(
			'joins' => array(
				array(
					'table' => 'fba_eu_order_items',
					'alias' => 'FbaEuOrderItem',
					'type' => 'INNER',		
					'conditions' => array(
						 'FbaEuOrder.amazon_order_id = FbaEuOrderItem.amazon_order_id',		
						 'FbaEuOrder.id = FbaEuOrderItem.fba_order_inc_id'						    
					)
				)			
			),
			'conditions' =>  array('OR' =>
						 array(
						 'FbaEuOrderItem.amazon_order_id' => trim($_REQUEST['searchkey']),
						 'FbaEuOrderItem.seller_sku' => trim($_REQUEST['searchkey'])
						 ) 
					 ) ,
		   'group' => 'FbaEuOrder.amazon_order_id',
		 ) 
		);
		//echo $_REQUEST['searchkey'];	
		// pr ($all_orders);
		 //exit;
		$this->set( 'all_orders', $all_orders ); 
	} 
	
	public function getSales() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Orders" );	
		$this->loadModel('FbaEuOrder'); 
		$this->loadModel('FbaEuOrderItem'); 
		
		 
		$order_items = array();
		$orders = array();
		$all_orders = array();
		$amazon_order_ids = array();
		$all_master_skus = array();
		if(isset($_REQUEST["report_year"])){
			$sdate = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-01';
			$edate = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-31';
		}else{
			$sdate = date('Y-m').'-01';
			$edate = date('Y-m-d', strtotime('last day of this month'));
		}
		
		$this->FbaEuOrder->unbindModel( array('hasMany' => array( 'FbaEuOrderItem' ) ) );
		
		$orders = $this->FbaEuOrder->find('all', array('conditions'=>array('FbaEuOrder.order_status !='=>'Canceled','FbaEuOrder.purchase_date BETWEEN ? AND ? ' => array($sdate,$edate)),'fields'=>array('amazon_order_id','purchase_date'),'order'=>'FbaEuOrder.id DESC') );
		
		if(count($orders ) > 0){
		
			foreach($orders as $order){
				$amazon_order_ids[] =	$order['FbaEuOrder']['amazon_order_id'];
			}
		 
			if(count($amazon_order_ids) > 0){
				
				 if(count($amazon_order_ids) == 1){
					 
					$this->paginate = array('conditions'=>array('FbaEuOrderItem.amazon_order_id ' => $amazon_order_ids[0]),
			'fields'=>array('id','amazon_order_id','asin','seller_sku','master_sku','quantity_ordered','quantity_shipped','item_price'),
			'group' =>'master_sku', 'order'=>'master_sku','limit' => 50 );
				 
				 }else{
				
					$this->paginate = array('conditions'=>array('FbaEuOrderItem.amazon_order_id IN ' => $amazon_order_ids),
			'fields'=>array('id','amazon_order_id','asin','seller_sku','master_sku','quantity_ordered','quantity_shipped','item_price'),
			'group' =>'master_sku','order'=>'master_sku','limit' => 50 );
			
				}			
			}
			
			$all_master_skus = $this->paginate('FbaEuOrderItem');
		}
		 
		$this->set( 'all_master_skus', $all_master_skus ); 
		
	}
	
	public function getSalesReport($report_year = null, $report_month = null) 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Orders" );	
		$this->loadModel('FbaEuOrder'); 
		$this->loadModel('FbaEuOrderItem'); 
		
		 
		$order_items = array();
		$amazon_order_ids = array();
		
		$sdate = $report_year.'-'.$report_month.'-01';
		$edate = $report_year.'-'.$report_month.'-31';
			
			
		$this->FbaEuOrder->unbindModel( array('hasMany' => array( 'FbaEuOrderItem' ) ) );
		
		$orders = $this->FbaEuOrder->find('all', array('conditions'=>array('FbaEuOrder.order_status !='=>'Canceled','FbaEuOrder.purchase_date BETWEEN ? AND ? ' => array($sdate, $edate )),'fields'=>array('amazon_order_id','purchase_date')) );

 
		foreach($orders as $order){
			$amazon_order_ids[] =	$order['FbaEuOrder']['amazon_order_id'];
		}
	 	
		$this->FbaEuOrderItem->unbindModel( array( 'belongsTo' => array( 'FbaEuOrder' ) ) );
		
		 if(count($amazon_order_ids) > 0){
			
			 if(count($amazon_order_ids) == 1){
					
				 $order_items = $this->FbaEuOrderItem->find('all', 
					array(
					'conditions'=>array('FbaEuOrderItem.amazon_order_id ' => $amazon_order_ids[0]), 
					'group' => 'FbaEuOrderItem.seller_sku',
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);	
			 
			 }else{
					
			 $order_items = $this->FbaEuOrderItem->find('all', 
					array(
					'conditions'=>array('FbaEuOrderItem.amazon_order_id IN ' => $amazon_order_ids),
					'group' => 'FbaEuOrderItem.seller_sku', 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);	
			}			
		}
	
		$sep = ",";
		$content = 'SKU';
		$file_name = 'fba_sale_'.$report_year.$report_month.'.csv'; 
		$days_of_month = cal_days_in_month(CAL_GREGORIAN, $report_month , $report_year);
		
		for ($i = 1; $i <= $days_of_month; $i++)
		{
			$date = $report_year.'-'.$report_month.'-'.str_pad($i, 2, "0", STR_PAD_LEFT);
			$d 	= date('d M Y', strtotime($date));
			$content .= $sep . $d;		
		}
		
		file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");
		
		if(count($order_items) > 0){
			foreach($order_items as $order){
				$content = $order['FbaEuOrderItem']['seller_sku'];  
				for ($i = 1; $i <= $days_of_month; $i++)
				{								
					$date = $report_year.'-'.$report_month.'-'.str_pad($i, 2, "0", STR_PAD_LEFT);					 	
					$sale = $this->getSkuSale($order['FbaEuOrderItem']['seller_sku'],$date) ? $this->getSkuSale($order['FbaEuOrderItem']['seller_sku'],$date) : '-';			
					$content .= $sep . $sale;
				}
				
				file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n", FILE_APPEND | LOCK_EX);				 
			}
		} 
		$result =  WWW_ROOT.'logs/'.$file_name; 
		header('Content-Description: File Transfer');
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename='.basename($result));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($result));   
		readfile($result); 
		exit;
	}
	
	public function getSkuSale($seller_sku = null,$date = null){
	
		$this->loadModel('FbaEuOrder'); 
		$this->loadModel('FbaEuOrderItem'); 
		$amazon_order_ids = array(); $order_items = array();
		$this->FbaEuOrder->unbindModel( array('hasMany' => array( 'FbaEuOrderItem' ) ) );
		
		$orders = $this->FbaEuOrder->find('all', array('conditions'=>array('FbaEuOrder.order_status !='=>'Canceled','FbaEuOrder.purchase_date LIKE ' => $date.'%'),'fields'=>array('amazon_order_id','purchase_date')) );

 
		foreach($orders as $order){
			$amazon_order_ids[] =	$order['FbaEuOrder']['amazon_order_id'];
		}
	 	
 		$this->FbaEuOrderItem->unbindModel( array( 'belongsTo' => array( 'FbaEuOrder' ) ) );
		
		 if(count($amazon_order_ids) > 0){
			
			 if(count($amazon_order_ids) == 1){
					
				 $order_items = $this->FbaEuOrderItem->find('all', 
					array(
					'conditions'=>array('FbaEuOrderItem.amazon_order_id ' => $amazon_order_ids[0],'seller_sku' => $seller_sku), 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);	
			 
			 }else{
					
			 $order_items = $this->FbaEuOrderItem->find('all', 
					array(
					'conditions'=>array('FbaEuOrderItem.amazon_order_id IN ' => $amazon_order_ids,'seller_sku' => $seller_sku), 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);	
			}			
		}
	
		$sales_qty = 0;
		if(count($order_items) > 0){
			foreach($order_items as $order){
				$sales_qty += $order['FbaEuOrderItem']['quantity_ordered'];
			}
		} 
		return  $sales_qty;
		 
	} 
	public function getSellerSkuSale($master_sku = null,$date = null){
	
		$this->loadModel('FbaEuOrder'); 
		$this->loadModel('FbaEuOrderItem'); 
		$amazon_order_ids = array(); $order_items = array();
		$this->FbaEuOrder->unbindModel( array('hasMany' => array( 'FbaEuOrderItem' ) ) );
		
		
		if(isset($_REQUEST["report_year"])){
			$sdate = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-01';
			$edate = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-31';
		}else{
			$sdate = date('Y-m').'-01';
			$edate = date('Y-m-d', strtotime('last day of this month'));
		}
		
		$this->FbaEuOrder->unbindModel( array('hasMany' => array( 'FbaEuOrderItem' ) ) );
		
		$orders = $this->FbaEuOrder->find('all', array('conditions'=>array('FbaEuOrder.order_status !='=>'Canceled','FbaEuOrder.purchase_date BETWEEN ? AND ? ' => array($sdate,$edate)),'fields'=>array('amazon_order_id','purchase_date'),'order'=>'FbaEuOrder.id DESC') );
		
		 
 
		foreach($orders as $order){
			$amazon_order_ids[] =	$order['FbaEuOrder']['amazon_order_id'];
		}
	 	
 		$this->FbaEuOrderItem->unbindModel( array( 'belongsTo' => array( 'FbaEuOrder' ) ) );
		
		 if(count($amazon_order_ids) > 0){
			
			 if(count($amazon_order_ids) == 1){
					
				 $order_items = $this->FbaEuOrderItem->find('all', 
					array(
					'conditions'=>array('FbaEuOrderItem.amazon_order_id ' => $amazon_order_ids[0],'master_sku' => $master_sku), 
					'group' => 'seller_sku',
					'fields'=>array('amazon_order_id','asin','seller_sku','master_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);	
			 
			 }else{
					
			 $order_items = $this->FbaEuOrderItem->find('all', 
					array(
					'conditions'=>array('FbaEuOrderItem.amazon_order_id IN ' => $amazon_order_ids,'master_sku' => $master_sku), 
					'group' => 'seller_sku',
					'fields'=>array('amazon_order_id','asin','seller_sku','master_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);	
			}			
		}
	
		$sales_qty = 0; $arr = array();
		if(count($order_items) > 0){
			foreach($order_items as $order){
			
			$arr[] = array('master_sku' => $order['FbaEuOrderItem']['master_sku'],'qty_ordered' => $order['FbaEuOrderItem']['quantity_ordered'],'seller_sku' => $order['FbaEuOrderItem']['seller_sku'] );
			
				//$sales_qty += $order['FbaEuOrderItem']['quantity_ordered'];
			}
		} 
		return  $arr;
		 
	} 
	
	public function getDateSale($date = null){
	
		$this->loadModel('FbaEuOrder'); 
		$this->loadModel('FbaEuOrderItem'); 
		$amazon_order_ids = array(); $order_items = array();
		$this->FbaEuOrder->unbindModel( array('hasMany' => array( 'FbaEuOrderItem' ) ) );
		
		$orders = $this->FbaEuOrder->find('all', array('conditions'=>array('FbaEuOrder.order_status !='=>'Canceled','FbaEuOrder.purchase_date LIKE ' => $date.'%'),'fields'=>array('amazon_order_id','purchase_date')) );

 
		foreach($orders as $order){
			$amazon_order_ids[] =	$order['FbaEuOrder']['amazon_order_id'];
		}
	
		$this->FbaEuOrderItem->unbindModel( array( 'belongsTo' => array( 'FbaEuOrder' ) ) );
		
		 if(count($amazon_order_ids) > 0){
			
			 if(count($amazon_order_ids) == 1){
				 $order_items = $this->FbaEuOrderItem->find('all', 
					array(
					'conditions'=>array('FbaEuOrderItem.amazon_order_id ' => $amazon_order_ids[0]), 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);
			 
			 }else{
				$order_items = $this->FbaEuOrderItem->find('all', 
					array(
					'conditions'=>array('FbaEuOrderItem.amazon_order_id IN  ' => $amazon_order_ids), 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);
			}			
		}
	
		$sales_qty = 0;
		
		if(count($order_items) > 0){
			foreach($order_items as $order){
				$sales_qty += $order['FbaEuOrderItem']['quantity_ordered'];
			}
		} 
		return  $sales_qty;
		 
	
	}
	
	public function getSaleReport($month = null, $year = null){
	
		$this->loadModel('FbaEuOrder'); 
		$this->loadModel('FbaEuOrderItem'); 
		$amazon_order_ids = array(); $order_items = array();
		$this->FbaEuOrder->unbindModel( array('hasMany' => array( 'FbaEuOrderItem' ) ) );
		
		$orders = $this->FbaEuOrder->find('all', array('conditions'=>array('FbaEuOrder.order_status !='=>'Canceled','FbaEuOrder.purchase_date LIKE ' => $date.'%'),'fields'=>array('amazon_order_id','purchase_date')) );

 
		foreach($orders as $order){
			$amazon_order_ids[] =	$order['FbaEuOrder']['amazon_order_id'];
		}
	
		$this->FbaEuOrderItem->unbindModel( array( 'belongsTo' => array( 'FbaEuOrder' ) ) );
		
		 if(count($amazon_order_ids) > 0){
			
			 if(count($amazon_order_ids) == 1){
				 $order_items = $this->FbaEuOrderItem->find('all', 
					array(
					'conditions'=>array('FbaEuOrderItem.amazon_order_id ' => $amazon_order_ids[0]), 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);
			 
			 }else{
				$order_items = $this->FbaEuOrderItem->find('all', 
					array(
					'conditions'=>array('FbaEuOrderItem.amazon_order_id IN  ' => $amazon_order_ids), 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);
			}			
		}
	
		$sales_qty = 0;
		
		if(count($order_items) > 0){
			foreach($order_items as $order){
				$sales_qty += $order['FbaEuOrderItem']['quantity_ordered'];
			}
		} 
		return  $sales_qty;	
	}
	
	public function uploadSave() 
    {	
 		
		$this->layout = 'index';		 
		$this->loadModel('FbaEuOrder');	 
		$this->loadModel('FbaEuOrderItem');	
		 
		$filetemppath =	$this->data['orders']['uploadfile']['tmp_name']; 
		$pathinfo     = pathinfo($this->data['orders']['uploadfile']['name']);
             
		$fileNameWithoutExt = $pathinfo['filename'];
		$fileExtension = strtolower($pathinfo['extension']);
		
		if($fileExtension == 'csv')
		{
 			$filename = $fileNameWithoutExt.'_'.date('Ymd_his').'_'.$this->Session->read('Auth.User.username').".".$fileExtension;		
			$upload_file = WWW_ROOT. 'fba_eu_orders'.DS.$filename ; 
			move_uploaded_file($filetemppath,$upload_file); 
			
			$row = 0; 
			$handle = fopen($upload_file, "r");
			$cols   = array_flip(fgetcsv($handle));
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			  
			$row++;			
 			// pr($data);
			if($data[$cols['fulfillment-channel']] == 'AFN')
			{
 				$savedata = array();					
				$savedata['last_update_date'] 	= gmdate("Y-m-d H:i:s", strtotime($data[$cols['reporting-date']])); 
				$savedata['purchase_date'] 		= gmdate("Y-m-d H:i:s", strtotime($data[$cols['purchase-date']]));
				$savedata['order_status'] 		= '';
				$savedata['fulfillment_channel']= $data[$cols['fulfillment-channel']];
				$savedata['sales_channel'] 		= $data[$cols['sales-channel']];					
				$savedata['ship_service_level'] = $data[$cols['ship-service-level']];
				$savedata['number_of_items'] 	= 0;
				$savedata['order_type']			= 'StandardOrder';
				$savedata['earliest_ship_date'] = $data[$cols['estimated-arrival-date']];
				$savedata['is_prime'] 			= 'false';
				$savedata['is_replacement_order']='false';	
 				 
				$savedata['name']				= $data[$cols['recipient-name']];
				$savedata['address_line_1'] 	= $data[$cols['ship-address-1']];
				if(!empty($data[$cols['ship-address-3']])){
					$savedata['address_line_2'] 	= $data[$cols['ship-address-2']].' '.$data[$cols['ship-address-3']];
				}else{
					$savedata['address_line_2'] 	= $data[$cols['ship-address-2']];
				}
				
				$savedata['city'] 				= $data[$cols['ship-city']];
				$savedata['region'] 			= $data[$cols['ship-state']];
				$savedata['post_code'] 			= $data[$cols['ship-postal-code']];
				$savedata['country_code'] 		= $data[$cols['ship-country']];
				//$savedata['order_total_amount'] = $data[$cols['purchase']] ;
				//$savedata['order_total_currency']= $data[$cols['purchase']] ;
				$savedata['buyer_email'] 		= $data[$cols['buyer-email']];
				$savedata['buyer_name'] 		= $data[$cols['buyer-name']]; 	
				$savedata['file_name'] 			= $filename;
						 						  
				/* echo "<pre>";
				 print_r($savedata);
				 echo "</pre>";*/               
			 	$fba_order = $this->FbaEuOrder->find('first', array('conditions'=>array('FbaEuOrder.amazon_order_id' =>$data[$cols['amazon-order-id']])) );
					
				if(count($fba_order) > 0){
					$fba_order_inc_id = $fba_order['FbaEuOrder']['id'];
 					$savedata['id'] = $fba_order['FbaEuOrder']['id'];	
					$this->FbaEuOrder->saveAll( $savedata );					 
					 
				}else{
					$savedata['seller_order_id'] 	= $data[$cols['amazon-order-item-id']];				
					$savedata['amazon_order_id'] 	= $data[$cols['amazon-order-id']];
					$savedata['added_date'] 		= date('Y-m-d H:i:s');							
					$this->FbaEuOrder->saveAll( $savedata );
					$fba_order_inc_id	 = $this->FbaEuOrder->getLastInsertId(); 	 									
				}    
				
				$saveItems = [];
				
				$master = $this->getSkuMapping($data[$cols['sku']]); 
			 
				$saveItems['fba_order_inc_id'] 	= $fba_order_inc_id;						 
 				$saveItems['amazon_order_id'] 	= $data[$cols['amazon-order-id']];
				//$saveItems['asin'] 				= $data[$cols['amazon-order-id']];
				$saveItems['seller_sku']		= $data[$cols['sku']];
			 	$saveItems['master_sku']		= $master['sku']; 
				$saveItems['order_item_id'] 	= $data[$cols['shipment-item-id']];	
				$saveItems['title'] 			= $data[$cols['product-name']];	
				$saveItems['quantity_ordered'] 	= $data[$cols['quantity-shipped']];	
				$saveItems['quantity_shipped'] 	= $data[$cols['quantity-shipped']];	
				$saveItems['number_of_items'] 	= $data[$cols['quantity-shipped']];	
  				$saveItems['currency_code'] 	= $data[$cols['currency']];	
				$saveItems['item_price'] 		= $data[$cols['item-price']];
				$saveItems['shipping_price'] 	= $data[$cols['shipping-price']];		
				$saveItems['item_tax'] 			= $data[$cols['item-tax']];		
				$saveItems['shipping_tax'] 		= $data[$cols['shipping-tax']];		
				$saveItems['shipping_discount']	= $data[$cols['ship-promotion-discount']];		

 				$fba_order_item = $this->FbaEuOrderItem->find('first', array('conditions'=>array('FbaEuOrderItem.amazon_order_id' => $data[$cols['amazon-order-id']],'order_item_id' => $data[$cols['shipment-item-id']])) );
				
				if(count($fba_order_item) > 0){
					$saveItems['id'] = $fba_order_item['FbaEuOrderItem']['id'];						
				}else{					
					$saveItems['added_date'] 		= date('Y-m-d H:i:s');														
				}		
						
				$this->FbaEuOrderItem->saveAll( $saveItems );                               
			
					//file_put_contents(WWW_ROOT .'logs/rm_track_id_'.date('ymd').'.log' , $data[2]."\t".$data[1]."\n",  FILE_APPEND|LOCK_EX);					
				}					
			   		
			}
			
			fclose($handle);
			
			if($row > 0){
				$this->Session->setFlash($row . " orders updated.", 'flash_success');
			} 
			
		 }
		 else{
			 $this->Session->setFlash("Invalid file format, Please upload CSV file.", 'flash_danger');
		 }
		 
		 $this->redirect($this->referer());
 		 
 	}
 
	
	public function getSkuMapping($seller_sku = null)
	{		
		$this->loadModel( 'Skumapping' );	
		$_items = $this->Skumapping->find('first', array('conditions'=>array('Skumapping.channel_sku'=>$seller_sku)));
		if(count($_items) > 0){
			return  array('sku'=>$_items['Skumapping']['sku'],'barcode'=>$_items['Skumapping']['barcode'],'channel_name'=>$_items['Skumapping']['channel_name']);
		}else{
			return 0;
		}
	}
	 
}