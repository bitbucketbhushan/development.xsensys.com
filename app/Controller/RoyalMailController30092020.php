<?php
error_reporting(1);
class RoyalMailController extends AppController
{
    
    var $name = "RoyalMail";    
    var $components = array('Session', 'Common', 'Upload','Auth');    
    var $helpers = array('Html','Form','Common','Session');
   
   public function beforeFilter()
   {
		parent::beforeFilter();
		$this->layout = false;
		$this->Auth->Allow(array('applyRoyalMail','createManifestCron','printManifest','reprintLabel','RoyalmailErrors'));
		$this->common = $this->Components->load('Common'); 
		//customApply
    }
    
	private function royalMailAuth()
	{
		$data = Configure::read( 'royalmail_auth' );
		$x_rmg_password = $data['x_rmg_password'];			
		$nonce_date_pwd = pack("H*", sha1($x_rmg_password));			
		$data['x_rmg_password'] = base64_encode($nonce_date_pwd); 
		return $data;	
	} 
		
	public function index(){
		$this->layout = 'index';
		
		$cover_img = WWW_ROOT .'img/orders/barcode/3151532-1.png';
		
 		$newwidth  = 500; $width = 224; $height = 59;
		$newheight = 200;
		$src = imagecreatefrompng($cover_img);
		$dst = imagecreatetruecolor($newwidth, $newheight);
		imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagepng($dst,WWW_ROOT .'royalmail/labels/barcode3151532-1.png');
		imagedestroy($dst);
		imagedestroy($src);

		$dest_img = WWW_ROOT .'royalmail/labels/3151532-1.png'; 
		$cover_img = WWW_ROOT .'royalmail/labels/barcode3151532-1.png';
		$dest = imagecreatefrompng($dest_img);
		$src = imagecreatefrompng($cover_img);
		
 		imagecopymerge($dest, $src, 500, 1530, 0, 0, $newwidth , $newheight, 100); //have to play with these numbers for it to work for you, etc.
		//imagecopymerge  int $dst_x , int $dst_y , int $src_x , int $src_y , int $src_w , int $src_h , int $pct ) 
		//header('Content-Type: image/png');
		imagepng($dest,WWW_ROOT .'royalmail/labels/3151532-1R.png');
		//file_put_contents(WWW_ROOT .'royalmail/labels/3151532-1rrrr.png' ,imagepng($dest));			
		imagedestroy($dest);
		imagedestroy($src);
exit;
	} 
	public function uploadManifest(){
		$this->layout = 'index';
	} 
	
	public function uploadManifestSave(){
		
		$this->layout = 'index';		 
		$this->loadModel('MergeUpdate');	 
		 
		$filetemppath =	$this->data['rm_manifest']['uploadfile']['tmp_name'];
		$pathinfo     = pathinfo($this->data['rm_manifest']['uploadfile']['name']);
             
		$fileNameWithoutExt = $pathinfo['filename'];
		$fileExtension = strtolower($pathinfo['extension']);
		
		if($fileExtension == 'csv')
		{
 			$filename = $fileNameWithoutExt.'_'.date('ymd_his').'_'.$this->Session->read('Auth.User.username').".".$fileExtension;		
			$upload_file = WWW_ROOT. 'royalmail'.DS.'rm_tracking_ids'.DS.$filename ; 
			move_uploaded_file($filetemppath,$upload_file); 
			
			$row = 0; 
			$handle = fopen($upload_file, "r");
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			  
			   $row++;			
			   if($row > 1){
					//echo "<br>". $data[1];
					//echo " = ". $data[2];			    
					$dataUpdate['track_id']   = "'".$data[1]."'";				
					$conditions = array('MergeUpdate.product_order_id_identify' => $data[2]);	
					$this->MergeUpdate->updateAll( $dataUpdate, $conditions ); 
					file_put_contents(WWW_ROOT .'logs/rm_track_id_'.date('ymd').'.log' , $data[2]."\t".$data[1]."\n",  FILE_APPEND|LOCK_EX);					
				}					
 			}
			
			fclose($handle);
			
			if($row > 0){
				$this->Session->setFlash($row . " items track_id updated.", 'flash_success');
			} 
 		 }
		 else{
			 $this->Session->setFlash("Invalid file format, Please upload CSV file.", 'flash_danger');
		 }
		 
		$this->redirect($this->referer());
 		 
	} 
	
	public function testDim(){
		$this->loadModel( 'ProductDesc' );
		$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name')));
		if(count($product) > 0)	{
			$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
			$_height[] = $product['ProductDesc']['height'];
			$length = $product['ProductDesc']['length'];	
			$width  = $product['ProductDesc']['width']; 
			}
	}
	
	public function applyRoyalMail($order_id = null){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'PostalServiceDesc' );
		$royal_sur_charge = 0.40;
		$fuel_percent     = 3;
		$dhl 			  = 1.20;
		$dhl_additional_cost = 0.42;
			
		$currency 		  = $this->getCurrencyRate();
		$exchange_rate	  = $currency ['EUR'];  
		$return 		  = array();
		$msg 			  = '';
		 
		$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id,'MergeUpdate.status' => 0 , 'MergeUpdate.royalmail' => 0, 'MergeUpdate.delevery_country' => 'United Kingdom')));	
		
		if(count($orders) >0){
		
			foreach($orders  as $orderItems){
			 
			$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $orderItems['MergeUpdate']['product_order_id_identify'],'MergeUpdate.status' => 0 ,'MergeUpdate.royalmail' => 0,'MergeUpdate.delevery_country' => 'United Kingdom')));	
					
				if(count($orderItem) > 0)
				{
						$this->loadModel( 'Product' );
						$this->loadModel( 'ProductDesc' );
						$this->loadModel( 'Country' );	
					
						$quantity 	   = $orderItem['MergeUpdate']['quantity'];
						$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
						$packet_length = $orderItem['MergeUpdate']['packet_length'];
						$packet_width  = $orderItem['MergeUpdate']['packet_width'];
						$packet_height = $orderItem['MergeUpdate']['packet_height'];
						$order_id 	   = $orderItem['MergeUpdate']['order_id'];
						$sub_source    = $orderItem['MergeUpdate']['source_coming'];
						$ebay_source   = '';
						if(strpos(strtolower($sub_source), 'ebay') !== false){
							$ebay_source = 'ebay';
						}
 						
						$_height = array(); $_weight = array(); $length = $width = 0; $data = array();
						
						$pos    = strpos($orderItem['MergeUpdate']['sku'],",");
						if ($pos === false) {
							$val  = $orderItem['MergeUpdate']['sku'];
							$s    = explode("X", $val);
							$_qty = $s[0]; 
							$_sku = $s[1];		
							$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name')));
							if(count($product) > 0){
								$_weight[] = $product['ProductDesc']['weight'] * $_qty;		
								$_height[] = $product['ProductDesc']['height'];		
								$length = $product['ProductDesc']['length'];
								$width = $product['ProductDesc']['width']; 
							 }
							
						}else{			
							$sks = explode(",",$orderItem['MergeUpdate']['sku']);
							$_weight = array();					 
							foreach($sks as $val){
								$s = explode("X", $val);
								$_qty = $s[0]; 
								$_sku = $s[1]; 
								$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name')));
								if(count($product) > 0)	{
									$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
									$_height[] = $product['ProductDesc']['height'];
									$length = $product['ProductDesc']['length'];	
									$width  = $product['ProductDesc']['width']; 
								}				
							}							 			
						}
						 
						if($packet_weight == 0){
							$packet_weight = array_sum($_weight);	
							$data['packet_weight'] = $packet_weight;
						}
						if($packet_height == 0){
							$packet_height = array_sum($_height);
							$data['packet_height'] = $packet_height;
						}
						if($packet_length == 0){
							$packet_length = $length; 
							$data['packet_length'] = $packet_length; 
						}
						if($packet_width == 0){
							$packet_width = $width;  
							$data['packet_width'] = $packet_width;
						}
						
						 
 						/*-------------------Royal Mail Calculations------------------------*/		
						 $final_dim = array();
						 if($packet_weight > 2){
							 $royalMailPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
									  
						 }else{
							 $dim = array($packet_width,$packet_height,$packet_length);
							 asort($dim);
							 $final_dim = array_values($dim) ;							
							
							 $royalMailPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
						 }
						 
						$all_royal = array(); 
						$all_postnl = array(); 
						$royalFee = 0; $postNLFee = 0;
						if(count($royalMailPostal) > 0)
						{
							foreach($royalMailPostal as $rv){
						 
								$per_item 			= $rv['PostalServiceDesc']['per_item'];
								$psd_id 			= $rv['PostalServiceDesc']['id'];
								
								$fuel_surcharge	 	= ($per_item  * $fuel_percent) / 100;
								$additional_fee 	= ($dhl + $dhl_additional_cost) * $packet_weight ;							
								$all_royal[$psd_id] = ($per_item + $fuel_surcharge + $additional_fee) ;	 
									 
							} 
							//pr($all_royal);
							$royalFee   = min($all_royal);	
							$post_id    = array_search($royalFee, $all_royal); 
							$post_royal = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));	
							
							//echo $royalFee;						 
							
							/*-------------------PostNL Calculations------------------------*/
							$postNlPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey',
											'Location.county_name' => 'United Kingdom', 'PostalServiceDesc.max_weight >=' => $packet_weight, 'PostalServiceDesc.max_length >=' => $packet_length, 'PostalServiceDesc.max_width >=' => $packet_width, 'PostalServiceDesc.max_height >=' => $packet_height, 'PostalServiceDesc.courier' => 'Belgium Post')));
											
							$all_postnl = array();
							foreach($postNlPostal as $pv){
								$per_item 	= $pv['PostalServiceDesc']['per_item'];
								$per_kg 	= $pv['PostalServiceDesc']['per_kilo'];
								$weightKilo = $pv['PostalServiceDesc']['max_weight'];						
								$psd_id 	= $pv['PostalServiceDesc']['id'];
							
								$all_postnl[$psd_id] = (($per_item + ($per_kg * $orderItem['MergeUpdate']['packet_weight'])) / $exchange_rate ) + ($dhl * $orderItem['MergeUpdate']['packet_weight']);
							} 
							
							if(count($all_postnl) >0){	
								$postNLFee  = min($all_postnl);	
							}
							// pr($postNlPostal);
							 // pr($all_postnl);
							//  echo $postNLFee .' royalFee='.$royalFee ." = ".$orderItem['MergeUpdate']['product_order_id_identify'];
									 
							 $weight = $packet_weight * 1000;
							
							/*-------------------Compare And Replace------------------------*/
							if(($postNLFee > $royalFee) || ($weight	>= 2000) || $ebay_source == 'ebay') {
										
									 //echo $orderItem['MergeUpdate']['product_order_id_identify'].'=='.$postNLFee .'>'. $royalFee;exit;
									 //$data['royalmail'] 			= '1';
									 
									if($orderItem['MergeUpdate']['packet_weight'] == 0){
										$data['packet_weight'] 			= $packet_weight;
									}
									$data['service_name'] 		= $post_royal['PostalServiceDesc']['service_name'];
									$data['track_id'] 			= '';
									$data['reg_post_number'] 	= '';
									$data['reg_num_img'] 		= '';
									$data['postal_service'] 	= 'Standard';
									$data['provider_ref_code'] 	= $post_royal['PostalServiceDesc']['provider_ref_code'];
									$data['service_id'] 		= $post_royal['PostalServiceDesc']['id'];
									$data['service_provider'] 	= $post_royal['PostalServiceDesc']['courier'];								 
									$data['template_id'] 		= $post_royal['PostalServiceDesc']['template_id'];  
									$data['id'] 				= $orderItem['MergeUpdate']['id'];
									
									//file_put_contents(WWW_ROOT."logs/royalmail_".$orderItem['MergeUpdate']['product_order_id_identify'].".log",print_r($_details,true));
									 
									file_put_contents(WWW_ROOT .'logs/pnl_to_rm_app_'.date('ymd').'.log' , $orderItem['MergeUpdate']['product_order_id_identify']."\t".$orderItem['MergeUpdate']['service_id']."\n",  FILE_APPEND|LOCK_EX);	
									
									$this->loadModel( 'MergeUpdate' );				 
									$this->MergeUpdate->saveAll( $data );									
								echo	$msg .= $orderItem['MergeUpdate']['product_order_id_identify']."\t";
									 
							}else{
								 $msg =  ' No order found in condition - 1.<br>';
							}
							/*------------------end--------------------------------*/
						}else{
							 $msg = ' This order may have over weight or any other issue.';
							
						}
				 
			  }
				else{
				 $msg =  ' No order found.<br>';
				}
				
				file_put_contents(WWW_ROOT."logs/royalmail_app_by_order_".date('Ymd').".log", date('Y-m-d H:i:s')."\t". $order_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);	
				 
			}
		}
	}
	
	public function applyRoyalMailByOrder($split_order_id = null){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' ); 
		$msg = '';
		 
		$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_order_id,'MergeUpdate.status' => 0, 'MergeUpdate.royalmail' => 0,'MergeUpdate.service_provider' => 'Royalmail')));	
		//'MergeUpdate.royalmail' => 0,
		//'MergeUpdate.status' => 0 
 		    
		 //966596 966653
		if(count($orderItem) > 0)
		{
			$this->loadModel( 'Product' );
			$this->loadModel( 'ProductDesc' );
			$this->loadModel( 'Country' );	
		
			$quantity 	   = $orderItem['MergeUpdate']['quantity'];
			$packet_weight = $orderItem['MergeUpdate']['packet_weight']; 
			$order_id 	   = $orderItem['MergeUpdate']['order_id'];
			$weight		   = $packet_weight; 
			
			$quantity 	   = $orderItem['MergeUpdate']['quantity'];
			$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
			$packet_length = $orderItem['MergeUpdate']['packet_length'];
			$packet_width  = $orderItem['MergeUpdate']['packet_width'];
			$packet_height = $orderItem['MergeUpdate']['packet_height'];
			$order_id 	   = $orderItem['MergeUpdate']['order_id'];
			$category_name = '';$product_name = '';
			$_height = array(); $_weight = array(); $length = $width = 0; $data = array(); 
					
			$pos    = strpos($orderItem['MergeUpdate']['sku'],",");
			
			if ($pos === false) {
				$val  = $orderItem['MergeUpdate']['sku'];
				$s    = explode("X", $val);
				$_qty = $s[0]; 
				$_sku = $s[1];		
				$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.product_name','Product.category_name')));
				if(count($product) > 0){
					$_weight[] = $product['ProductDesc']['weight'] * $_qty;		
					$_height[] = $product['ProductDesc']['height'];		
					$length = $product['ProductDesc']['length'];
					$width = $product['ProductDesc']['width'];	
					$category_name = $product['Product']['category_name'];	
					$product_name = $product['Product']['product_name'];										
				}
				
			}
			else{			
				$sks = explode(",",$orderItem['MergeUpdate']['sku']);
				$_weight = array();					 
				foreach($sks as $val){
					$s = explode("X", $val);
					$_qty = $s[0]; 
					$_sku = $s[1]; 
					$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.product_name','Product.category_name')));
					if(count($product) > 0)	{
						$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
						$_height[] = $product['ProductDesc']['height'];
						$length = $product['ProductDesc']['length'];	
						$width  = $product['ProductDesc']['width']; 
						$category_name = $product['Product']['category_name'];	
						$product_name = $product['Product']['product_name'];	
					}				
				}		
							
			}
			
			if($packet_weight == 0){
				$packet_weight = array_sum($_weight);	
				$data['packet_weight'] = $packet_weight;
			}
			if($packet_height == 0){
				$packet_height = array_sum($_height);
				$data['packet_height'] = $packet_height;
			}
			if($packet_length == 0){
				$packet_length = $length; 
				$data['packet_length'] = $packet_length; 
			}
			if($packet_width == 0){
				$packet_width = $width;  
				$data['packet_width'] = $packet_width;
			}
			 
		 
			//echo $orderItem['MergeUpdate']['product_order_id_identify'].'=='.$postNLFee .'>'. $royalFee;exit;
			App::import( 'Controller' , 'Cronjobs' );		
			$objController 	= new CronjobsController();
			$getorderDetail	= $objController->getOpenOrderById($order_id);
			 
			$cInfo = $getorderDetail['customer_info'];
			$order->TotalsInfo = $getorderDetail['totals_info'];
			
			$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
			
			$Address1 = $cInfo->Address->Address1;
			$Address2 = $cInfo->Address->Address2;
			$type = 2; 
			if($orderItem['MergeUpdate']['service_name'] == 'royalmail_24'){
				$type = 1;
			}
			$service_format = 'P';
			if($orderItem['MergeUpdate']['service_name'] == 'royalmail_LL_48'){
				/*  F	Inland Large Letter, L	Inland Letter, N	Inland format Not Applicable, P	Inland Parcel */
				$service_format = 'F';
			}
			$_details = array('id'=> $orderItem['MergeUpdate']['id'],
				'merge_id'=> $order_id,
				'split_order_id'=> $orderItem['MergeUpdate']['product_order_id_identify'],
				'sub_total' => number_format($order->TotalsInfo->Subtotal,2),
				'order_total'=> number_format($order->TotalsInfo->TotalCharge,2),
				'postage_cost' => number_format($order->TotalsInfo->PostageCost,2),
				'order_currency'=>$order->TotalsInfo->Currency,			
				'weight'=>  $weight ,
				'Company'=>$cInfo->Address->Company,
				'FullName'=> $this->replaceFrenchChar($cInfo->Address->FullName),
				'Address1' =>$this->replaceFrenchChar($Address1),
				'Address2' =>$this->replaceFrenchChar($Address2),
				'Town' =>$this->replaceFrenchChar($cInfo->Address->Town),
				'Region' =>$this->replaceFrenchChar($cInfo->Address->Region),
				'PostCode' =>$cInfo->Address->PostCode,
				'CountryName'=>$cInfo->Address->Country,
				'CountryCode' =>$country_data['Country']['iso_2'],
				'PhoneNumber'=>$cInfo->Address->PhoneNumber	,
				'sub_source' => $getorderDetail['sub_source'],
  				'weight'=>  $packet_weight ,
				'packet_height'=>  $packet_height ,
				'packet_length'=>  $packet_length ,
				'packet_width'=>  $packet_width ,
				'packet_weight'=>  $packet_weight ,
				'category_name'=>  $category_name ,
				'product_name'=>  $product_name , 
				'type' 		=> $type,
				'service_format' => $service_format,
				'item_count'=> 1,
				'building_name' => '',
				'building_number' => 0,
			); 		 
			 
			
			$data['royalmail'] 			= '1';			 
			$data['id'] 				= $orderItem['MergeUpdate']['id'];
			
			//file_put_contents(WWW_ROOT."logs/royalmail_".$orderItem['MergeUpdate']['product_order_id_identify'].".log",print_r($_details,true));
			
			file_put_contents(WWW_ROOT .'logs/pnl_to_rm_'.date('ymd').'.log' , $orderItem['MergeUpdate']['product_order_id_identify']."\t".$orderItem['MergeUpdate']['service_id']."\n",  FILE_APPEND|LOCK_EX);	
			
			$this->royalMailServiceStart($_details, $data);
			$msg .= $orderItem['MergeUpdate']['product_order_id_identify']."\t";
		
		}
		else{
			$msg =  ' No order found in condition - 1.<br>';
		}
		 
		file_put_contents(WWW_ROOT."logs/royalmail_by_order_".date('Ymd').".log", date('Y-m-d H:i:s')."\t". $split_order_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);	
	}
	
	public function customV3Apply($split_order_id = null){
	
		//echo 'date_default_timezone_set: ' . date_default_timezone_get() . '<br />';echo date('Y-m-d H:i:s'); 
	 	//  die('Uncomment Code.');
		
		$split_order_id = '3029395-1';   
		//$_postcode = 'PO37 6RQ'; 
		//$_town = 'BFPO ';  
		 			
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'PostalServiceDesc' );
		$royal_sur_charge = 0.40;
		$fuel_percent     = 3;
		$dhl 			  = 1.20;
		$dhl_additional_cost = 0.42;
			
		$currency 		  = $this->getCurrencyRate();
		$exchange_rate	  = $currency ['EUR'];  
		$return 		  = array();
		$msg 			  = '';
		 
		$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_order_id,'MergeUpdate.delevery_country' => 'United Kingdom')));	
 		 
 		if(count($orderItem) > 0)
		{
				$this->loadModel( 'Product' );
				$this->loadModel( 'ProductDesc' );
				$this->loadModel( 'Country' );	
  			
				$quantity 	   = $orderItem['MergeUpdate']['quantity'];
				$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
				$packet_length = $orderItem['MergeUpdate']['packet_length'];
				$packet_width  = $orderItem['MergeUpdate']['packet_width'];
				$packet_height = $orderItem['MergeUpdate']['packet_height'];
				$order_id 	   = $orderItem['MergeUpdate']['order_id'];
				$category_name = '';$product_name = '';
				$_height = array(); $_weight = array(); $length = $width = 0; $data = array(); 
						
				$pos    = strpos($orderItem['MergeUpdate']['sku'],",");
				
				if ($pos === false) {
					$val  = $orderItem['MergeUpdate']['sku'];
					$s    = explode("X", $val);
					$_qty = $s[0]; 
					$_sku = $s[1];		
					$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.product_name','Product.category_name')));
					if(count($product) > 0){
						$_weight[] = $product['ProductDesc']['weight'] * $_qty;		
						$_height[] = $product['ProductDesc']['height'];		
						$length = $product['ProductDesc']['length'];
						$width = $product['ProductDesc']['width'];	
						$category_name = $product['Product']['category_name'];	
						$product_name = $product['Product']['product_name'];										
					}
					
				}
				else{			
					$sks = explode(",",$orderItem['MergeUpdate']['sku']);
					$_weight = array();					 
					foreach($sks as $val){
						$s = explode("X", $val);
						$_qty = $s[0]; 
						$_sku = $s[1]; 
						$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.product_name','Product.category_name')));
						if(count($product) > 0)	{
							$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
							$_height[] = $product['ProductDesc']['height'];
							$length = $product['ProductDesc']['length'];	
							$width  = $product['ProductDesc']['width']; 
							$category_name = $product['Product']['category_name'];	
							$product_name = $product['Product']['product_name'];	
						}				
					}		
								
				}
				
				if($packet_weight == 0){
					$packet_weight = array_sum($_weight);	
					$data['packet_weight'] = $packet_weight;
				}
				if($packet_height == 0){
					$packet_height = array_sum($_height);
					$data['packet_height'] = $packet_height;
				}
				if($packet_length == 0){
					$packet_length = $length; 
					$data['packet_length'] = $packet_length; 
				}
				if($packet_width == 0){
					$packet_width = $width;  
					$data['packet_width'] = $packet_width;
				}
				 
				/*-------------------Royal Mail Calculations------------------------*/		
				
 				 //echo $orderItem['MergeUpdate']['product_order_id_identify'].'=='.$postNLFee .'>'. $royalFee;exit;
				App::import( 'Controller' , 'Cronjobs' );		
				$objController 	= new CronjobsController();
				$getorderDetail	= $objController->getOpenOrderById($order_id);
				$cInfo = $getorderDetail['customer_info'];
				$order->TotalsInfo = $getorderDetail['totals_info'];
				
				$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
				
				$Address1 = $cInfo->Address->Address1;
				$Address2 = $cInfo->Address->Address2;
				$type = 2; 
				if($post_royal['PostalServiceDesc']['service_name'] == 'royalmail_24'){
					$type = 1;
				}
				
				$service_format = 'P';
				if($post_royal['PostalServiceDesc']['service_name'] == 'royalmail_LL_48'){
					/*  F	Inland Large Letter, L	Inland Letter, N	Inland format Not Applicable, P	Inland Parcel */
					$service_format = 'F';
				}
				
				if(isset($_postcode) && $_postcode !=''){
					$postcode = $_postcode;									 
				}else{
					$postcode = $cInfo->Address->PostCode;
				}
				
				if(isset($_town) && $_town != ''){
					$Town = $_town ;
				}else if($cInfo->Address->Town){
					$Town = utf8_decode($cInfo->Address->Town);
				}else{
					$Town = utf8_decode($cInfo->Address->Address3);
				}
				
				$_details = array('merge_id'=> $order_id,
					'split_order_id'=> $orderItem['MergeUpdate']['product_order_id_identify'],
					'sub_total' => number_format($order->TotalsInfo->Subtotal,2),
					'order_total'=> number_format($order->TotalsInfo->TotalCharge,2),
					'postage_cost' => number_format($order->TotalsInfo->PostageCost,2),
					'order_currency'=>$order->TotalsInfo->Currency,			
					'Company'=>$cInfo->Address->Company,
					'FullName'=> $this->replaceFrenchChar($cInfo->Address->FullName),
					'Address1' =>$this->replaceFrenchChar($Address1),
					'Address2' =>$this->replaceFrenchChar($Address2),
					'Town' =>$this->replaceFrenchChar($Town),
					'Region' => $this->replaceFrenchChar($cInfo->Address->Region),
					'PostCode' =>$postcode,//$cInfo->Address->PostCode,
					'CountryName'=>$cInfo->Address->Country,
					'country'=>$cInfo->Address->Country,
					'CountryCode' =>$country_data['Country']['iso_2'],
					'PhoneNumber'=>$cInfo->Address->PhoneNumber	,
					'email' => $cInfo->Address->EmailAddress,
					'sub_source' => $getorderDetail['sub_source'],
					'weight'=>  $packet_weight ,
					'packet_height'=>  $packet_height ,
					'packet_length'=>  $packet_length ,
					'packet_width'=>  $packet_width ,
					'packet_weight'=>  $packet_weight ,
					'category_name'=>  $category_name ,
					'product_name'=>  $product_name , 
					'type' 		=> $type,
					'service_format' => $service_format,
					'item_count'=> 1,
					'building_name' => '',
					'building_number' => 0,
	
				);
			
				$data['royalmail'] 			= '1';
				$data['service_name'] 		= $post_royal['PostalServiceDesc']['service_name'];
				$data['track_id'] 			= '';
				$data['reg_post_number'] 	= '';
				$data['reg_num_img'] 		= '';
				$data['postal_service'] 	= 'Standard';
				$data['provider_ref_code'] 	= $post_royal['PostalServiceDesc']['provider_ref_code'];
				$data['service_id'] 		= $post_royal['PostalServiceDesc']['id'];
				$data['service_provider'] 	= $post_royal['PostalServiceDesc']['courier'];								 
				$data['template_id'] 		= $post_royal['PostalServiceDesc']['template_id'];  
				$data['id'] 				= $orderItem['MergeUpdate']['id'];
				pr($_details);
				
			echo	$this->royalMailServiceStart($_details, $data);
			echo	$msg .= $orderItem['MergeUpdate']['product_order_id_identify']."\t";
							 
  		  }
		else{
			echo $msg =  ' No order found.<br>';
		}
		
  	}	
	
	public function customApply($split_order_id = null){
		//echo 'date_default_timezone_set: ' . date_default_timezone_get() . '<br />';echo date('Y-m-d H:i:s'); 
	 	//  die('Uncomment Code.');
		
		$split_order_id = '3151532-1';   
		//$_postcode = 'PO37 6RQ'; 
		//$_town = 'BFPO ';  
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'PostalServiceDesc' );
		$royal_sur_charge = 0.40;
		$fuel_percent     = 3;
		$dhl 			  = 1.20;
		$dhl_additional_cost = 0.42;
			
		$currency 		  = $this->getCurrencyRate();
		$exchange_rate	  = $currency ['EUR'];  
		$return 		  = array();
		$msg 			  = '';
		$ch_rm = $this->Royalmail->find('first',array('conditions' => array('Royalmail.split_order_id' => $split_order_id)));	 
		
		
		if(count($ch_rm ) == 0){ 
		$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_order_id,'MergeUpdate.delevery_country' => 'United Kingdom')));	
		
		 
		//'MergeUpdate.royalmail' => 0,
		//'MergeUpdate.status' => 0  
 		   
		 //966596 966653
		
		if(count($orderItem) > 0)
		{
				$this->loadModel( 'Product' );
				$this->loadModel( 'ProductDesc' );
				$this->loadModel( 'Country' );	
  			
				$quantity 	   = $orderItem['MergeUpdate']['quantity'];
				$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
				$packet_length = $orderItem['MergeUpdate']['packet_length'];
				$packet_width  = $orderItem['MergeUpdate']['packet_width'];
				$packet_height = $orderItem['MergeUpdate']['packet_height'];
				$order_id 	   = $orderItem['MergeUpdate']['order_id'];
				
 				$category_name = '';$product_name = '';
				$_height = array(); $_weight = array(); $length = $width = 0; $data = array(); 
						
				$pos    = strpos($orderItem['MergeUpdate']['sku'],",");
				
				if ($pos === false) {
					$val  = $orderItem['MergeUpdate']['sku'];
					$s    = explode("X", $val);
					$_qty = $s[0]; 
					$_sku = $s[1];		
					$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.product_name','Product.category_name')));
					if(count($product) > 0){
						$_weight[] = $product['ProductDesc']['weight'] * $_qty;		
						$_height[] = $product['ProductDesc']['height'];		
						$length = $product['ProductDesc']['length'];
						$width = $product['ProductDesc']['width'];	
						$category_name = $product['Product']['category_name'];	
						$product_name = $product['Product']['product_name'];										
					}
					
				}
				else{			
					$sks = explode(",",$orderItem['MergeUpdate']['sku']);
					$_weight = array();					 
					foreach($sks as $val){
						$s = explode("X", $val);
						$_qty = $s[0]; 
						$_sku = $s[1]; 
						$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.product_name','Product.category_name')));
						if(count($product) > 0)	{
							$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
							$_height[] = $product['ProductDesc']['height'];
							$length = $product['ProductDesc']['length'];	
							$width  = $product['ProductDesc']['width']; 
							$category_name = $product['Product']['category_name'];	
							$product_name = $product['Product']['product_name'];	
						}				
					}		
								
				}
				
				if($packet_weight == 0){
					$packet_weight = array_sum($_weight);	
					$data['packet_weight'] = $packet_weight;
				}
				if($packet_height == 0){
					$packet_height = array_sum($_height);
					$data['packet_height'] = $packet_height;
				}
				if($packet_length == 0){
					$packet_length = $length; 
					$data['packet_length'] = $packet_length; 
				}
				if($packet_width == 0){
					$packet_width = $width;  
					$data['packet_width'] = $packet_width;
				}
				 
				/*-------------------Royal Mail Calculations------------------------*/		
				
				 if($packet_weight > 2){
					 $royalMailPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
						   
							 
				 }else{
					 $dim = array($packet_width,$packet_height,$packet_length);
					 asort($dim);
					 $final_dim = array_values($dim) ;							
					
					 $royalMailPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
					 
				 }
				 
				$all_royal = array(); 
				$all_postnl = array(); 
				$royalFee = 0; $postNLFee = 0;
				if(count($royalMailPostal) > 0)
				{
					foreach($royalMailPostal as $rv){
				 
						$per_item 			= $rv['PostalServiceDesc']['per_item'];
						$psd_id 			= $rv['PostalServiceDesc']['id'];
						
						$fuel_surcharge	 	= ($per_item  * $fuel_percent) / 100;
						$additional_fee 	= ($dhl + $dhl_additional_cost) * $packet_weight ;							
						$all_royal[$psd_id] = ($per_item + $fuel_surcharge + $additional_fee) ;	 
							 
					} 
					 
					$royalFee   = min($all_royal);	
					$post_id    = array_search($royalFee, $all_royal); 
					$post_royal = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));	
					
					//echo $royalFee;						 
					
					/*-------------------PostNL Calculations------------------------*/
					$postNlPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey',
									'Location.county_name' => 'United Kingdom', 'PostalServiceDesc.max_weight >=' => $packet_weight, 'PostalServiceDesc.max_length >=' => $packet_length, 'PostalServiceDesc.max_width >=' => $packet_width, 'PostalServiceDesc.max_height >=' => $packet_height, 'PostalServiceDesc.courier' => 'Belgium Post')));
					 
					foreach($postNlPostal as $pv){
						$per_item 	= $pv['PostalServiceDesc']['per_item'];
						$per_kg 	= $pv['PostalServiceDesc']['per_kilo'];
						$weightKilo = $pv['PostalServiceDesc']['max_weight'];						
						$psd_id 	= $pv['PostalServiceDesc']['id'];
					
						$all_postnl[$psd_id] = (($per_item + ($per_kg * $orderItem['MergeUpdate']['packet_weight'])) / $exchange_rate ) + ($dhl * $orderItem['MergeUpdate']['packet_weight']);
					} 
					
					$postNLFee  = min($all_postnl);	
					//echo $postNLFee .'== '.$royalFee ." = ".$orderItem['MergeUpdate']['product_order_id_identify'];
						 	 
					$weight	= $packet_weight * 1000;	
					 
					/*-------------------Compare And Replace------------------------*/
					if(($royalFee) || ($weight	>= 2000)) {
								
							 //echo $orderItem['MergeUpdate']['product_order_id_identify'].'=='.$postNLFee .'>'. $royalFee;exit;
							App::import( 'Controller' , 'Cronjobs' );		
							$objController 	= new CronjobsController();
							$getorderDetail	= $objController->getOpenOrderById($order_id);
							$cInfo = $getorderDetail['customer_info'];
							$order->TotalsInfo = $getorderDetail['totals_info'];
							
							$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
							
							$Address1 = $cInfo->Address->Address1;
							$Address2 = $cInfo->Address->Address2;
							$type = 2; 
							if($post_royal['PostalServiceDesc']['service_name'] == 'royalmail_24'){
								$type = 1;
							}
							
							$service_format = 'P';
							if($post_royal['PostalServiceDesc']['service_name'] == 'royalmail_LL_48'){
								/*  F	Inland Large Letter, L	Inland Letter, N	Inland format Not Applicable, P	Inland Parcel */
								$service_format = 'F';
							}
							
							if(isset($_postcode) && $_postcode !=''){
								$postcode = $_postcode;									 
							}else{
								$postcode = $cInfo->Address->PostCode;
							}
							
							if(isset($_town) && $_town != ''){
								$Town = $_town ;
							}else if($cInfo->Address->Town){
								$Town = utf8_decode($cInfo->Address->Town);
							}else{
								$Town = utf8_decode($cInfo->Address->Address3);
							}
							
							$_details = array('merge_id'=> $order_id,
								'split_order_id'=> $orderItem['MergeUpdate']['product_order_id_identify'],
								'sub_total' => number_format($order->TotalsInfo->Subtotal,2),
								'order_total'=> number_format($order->TotalsInfo->TotalCharge,2),
								'postage_cost' => number_format($order->TotalsInfo->PostageCost,2),
								'order_currency'=>$order->TotalsInfo->Currency,			
 								'Company'=>$cInfo->Address->Company,
								'FullName'=> $this->replaceFrenchChar($cInfo->Address->FullName),
								'Address1' =>$this->replaceFrenchChar($Address1),
								'Address2' =>$this->replaceFrenchChar($Address2),
								'Town' =>$this->replaceFrenchChar($Town),
								'Region' => $this->replaceFrenchChar($cInfo->Address->Region),
								'PostCode' =>$postcode,//$cInfo->Address->PostCode,
								'CountryName'=>$cInfo->Address->Country,
								'country'=>$cInfo->Address->Country,
								'CountryCode' =>$country_data['Country']['iso_2'],
								'PhoneNumber'=>$cInfo->Address->PhoneNumber	,
								'email' => $cInfo->Address->EmailAddress,
								'sub_source' => $getorderDetail['sub_source'],
								'weight'=>  $packet_weight ,
								'packet_height'=>  $packet_height ,
								'packet_length'=>  $packet_length ,
								'packet_width'=>  $packet_width ,
								'packet_weight'=>  $packet_weight ,
								'category_name'=>  $category_name ,
								'product_name'=>  $product_name ,
								'type' 		=> $type,
								'service_format' => $service_format,
								'item_count'=> 1,
								'building_name' => '',
								'building_number' => 0,
							);
						
							$data['royalmail'] 			= '1';
							$data['service_name'] 		= $post_royal['PostalServiceDesc']['service_name'];
							$data['track_id'] 			= '';
							$data['reg_post_number'] 	= '';
							$data['reg_num_img'] 		= '';
							$data['postal_service'] 	= 'Standard';
							$data['provider_ref_code'] 	= $post_royal['PostalServiceDesc']['provider_ref_code'];
							$data['service_id'] 		= $post_royal['PostalServiceDesc']['id'];
							$data['service_provider'] 	= $post_royal['PostalServiceDesc']['courier'];								 
							$data['template_id'] 		= $post_royal['PostalServiceDesc']['template_id'];  
							$data['id'] 				= $orderItem['MergeUpdate']['id'];
							pr($_details);
							
	 						//exit;
							//file_put_contents(WWW_ROOT."logs/royalmail_".$orderItem['MergeUpdate']['product_order_id_identify'].".log",print_r($_details,true));
							 
							file_put_contents(WWW_ROOT .'logs/pnl_to_rm_'.date('ymd').'.log' , $orderItem['MergeUpdate']['product_order_id_identify']."\t".$orderItem['MergeUpdate']['service_id']."\n",  FILE_APPEND|LOCK_EX);	
							
						echo	$this->royalMailServiceStart($_details, $data);
						echo	$msg .= $orderItem['MergeUpdate']['product_order_id_identify']."\t";
							 
					}else{
						echo $msg =  ' No order found in condition - 1.<br>';
					}
					/*------------------end--------------------------------*/
				}else{
				echo	 $msg = ' This order may have over weight or any other issue.';
					
				}
		 
		  }
		  else{
			echo $msg =  ' No order found.<br>';
			}
		
		file_put_contents(WWW_ROOT."logs/royalmail_by_order_".date('Ymd').".log", date('Y-m-d H:i:s')."\t". $split_order_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);	
		}else{
			echo 'already exit. please cancel https://app.rmdmo.royalmail.com before generate new.';
		}
	}
	public function CheckPostage($order_id = null){
		 
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'PostalServiceDesc' );
		$royal_sur_charge = 0.40;
		$fuel_percent     = 3;
		$dhl 			  = 1.20;
		$dhl_additional_cost = 0.42;
			
		$currency 		  = $this->getCurrencyRate();
		$exchange_rate	  = $currency ['EUR'];  
		$return 		  = array();
		$msg 			  = '';
		 
		$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id, 'MergeUpdate.delevery_country' => 'United Kingdom')));	
		 
		if(count($orders) >0){
		
			foreach($orders  as $orderItems){
			 
			$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $orderItems['MergeUpdate']['product_order_id_identify'],'MergeUpdate.delevery_country' => 'United Kingdom')));	
					
				if(count($orderItem) > 0)
				{
						$this->loadModel( 'Product' );
						$this->loadModel( 'ProductDesc' );
						$this->loadModel( 'Country' );	
					
						$quantity 	   = $orderItem['MergeUpdate']['quantity'];
						$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
						$packet_length = $orderItem['MergeUpdate']['packet_length'];
						$packet_width  = $orderItem['MergeUpdate']['packet_width'];
						$packet_height = $orderItem['MergeUpdate']['packet_height'];
						$order_id 	   = $orderItem['MergeUpdate']['order_id'];
 						
						$_height = array(); $_weight = array(); $length = $width = 0; $data = array();
						
						$pos    = strpos($orderItem['MergeUpdate']['sku'],",");
						if ($pos === false) {
							$val  = $orderItem['MergeUpdate']['sku'];
							$s    = explode("X", $val);
							$_qty = $s[0]; 
							$_sku = $s[1];		
							$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name')));
							if(count($product) > 0){
								$_weight[] = $product['ProductDesc']['weight'] * $_qty;		
								$_height[] = $product['ProductDesc']['height'];		
								$length = $product['ProductDesc']['length'];
								$width = $product['ProductDesc']['width']; 
							 }
							
						}else{			
							$sks = explode(",",$orderItem['MergeUpdate']['sku']);
							$_weight = array();					 
							foreach($sks as $val){
								$s = explode("X", $val);
								$_qty = $s[0]; 
								$_sku = $s[1]; 
								$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name')));
								if(count($product) > 0)	{
									$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
									$_height[] = $product['ProductDesc']['height'];
									$length = $product['ProductDesc']['length'];	
									$width  = $product['ProductDesc']['width']; 
								}				
							}							 			
						}
						 
						if($packet_weight == 0){
							$packet_weight = array_sum($_weight);	
							$data['packet_weight'] = $packet_weight;
						}
						if($packet_height == 0){
							$packet_height = array_sum($_height);
							$data['packet_height'] = $packet_height;
						}
						if($packet_length == 0){
							$packet_length = $length; 
							$data['packet_length'] = $packet_length; 
						}
						if($packet_width == 0){
							$packet_width = $width;  
							$data['packet_width'] = $packet_width;
						}
						
						echo "packet_weight=".$packet_weight; 
 						/*-------------------Royal Mail Calculations------------------------*/		
						 $final_dim = array();
						 if($packet_weight > 2){
							 $royalMailPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
									  
						 }else{
							 $dim = array($packet_width,$packet_height,$packet_length);
							 asort($dim);
							 $final_dim = array_values($dim) ;							
							
							 $royalMailPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
						 }
					//	pr( $final_dim);pr( $royalMailPostal);
						$all_royal = array(); 
						$all_postnl = array(); 
						$royalFee = 0; $postNLFee = 0;
						if(count($royalMailPostal) > 0)
						{
							foreach($royalMailPostal as $rv){
						 
								$per_item 			= $rv['PostalServiceDesc']['per_item'];
								$psd_id 			= $rv['PostalServiceDesc']['id'];
								
								$fuel_surcharge	 	= ($per_item  * $fuel_percent) / 100;
								$additional_fee 	= ($dhl + $dhl_additional_cost) * $packet_weight ;							
								$all_royal[$psd_id] = ($per_item + $fuel_surcharge + $additional_fee) ;	 
									 
							} 
							//pr($all_royal);
							$royalFee   = min($all_royal);	
							$post_id    = array_search($royalFee, $all_royal); 
							$post_royal = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));	
							
							//echo $royalFee;						 
							
							/*-------------------PostNL Calculations------------------------*/
							$postNlPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey',
											'Location.county_name' => 'United Kingdom', 'PostalServiceDesc.max_weight >=' => $packet_weight, 'PostalServiceDesc.max_length >=' => $packet_length, 'PostalServiceDesc.max_width >=' => $packet_width, 'PostalServiceDesc.max_height >=' => $packet_height, 'PostalServiceDesc.courier' => 'Belgium Post')));
											
							$all_postnl = array();
							foreach($postNlPostal as $pv){
								$per_item 	= $pv['PostalServiceDesc']['per_item'];
								$per_kg 	= $pv['PostalServiceDesc']['per_kilo'];
								$weightKilo = $pv['PostalServiceDesc']['max_weight'];						
								$psd_id 	= $pv['PostalServiceDesc']['id'];
							
								$all_postnl[$psd_id] = (($per_item + ($per_kg * $orderItem['MergeUpdate']['packet_weight'])) / $exchange_rate ) + ($dhl * $orderItem['MergeUpdate']['packet_weight']);
							} 
							
							if(count($all_postnl) >0){	
								$postNLFee  = min($all_postnl);	
								$postNL_id    = array_search($postNLFee, $all_postnl); 
								 	
							}
							// pr($postNlPostal);
							 // pr($all_postnl);
							//  echo $postNLFee .' royalFee='.$royalFee ." = ".$orderItem['MergeUpdate']['product_order_id_identify'];
									 
							 $weight = $packet_weight * 1000;
							 echo 'postNLFee='.$postNLFee.' id='. $postNL_id.' <br> royalFee ='.$royalFee .' id='.$post_id;
							/*-------------------Compare And Replace------------------------*/
							if(($postNLFee > $royalFee) || ($weight	>= 2000)) { 
								 $msg =  'condition ';
							}else{
								 $msg =  ' No order found in condition - 1.<br>';
							}
							/*------------------end--------------------------------*/
						}else{
							 $msg = ' This order may have over weight or any other issue.';
							
						}
				 
			  }
				else{
				 $msg =  ' No order found.<br>';
				}
 				 
			}
		}
	}
 	public function CheckManifest()
	{
		$this->layout = '';
		$this->autoRander = false;
		 
		$this->loadModel('Royalmail');
		$this->loadModel('MergeUpdate');
		$path = WWW_ROOT.'/logs/';
	 	$upload_file = WWW_ROOT. 'royalmail'.DS.'rm_tracking_ids'.DS.'ShipmentsDownload_12_09_2019.csv' ; 
			 
		$row = 0; 
		$handle = fopen($upload_file, "r");
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		  
		   $row++;			
		   if($row > 1){  
		   
			$order = $this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => trim($data[2]),'service_provider'=>'Royalmail','status' => 1),'fields'=>['track_id','product_order_id_identify','manifest_date']));
	 
 			if(count($order) > 0){
			
				
				$rm = $this->Royalmail->find('first', array('conditions' => array( 'split_order_id' => trim($data[2]),'manifest_table_id'=>0),'fields'=>['manifest_table_id','manifest_date']));
				if(count($rm) > 0){
					 echo " RM=".$data[2];
					 echo "<br>";
 				
					if( $order['MergeUpdate']['manifest_date'] == ''){
						echo $manifest_date   = "'". date('Y-m-d H:i:s', strtotime(str_replace("/","-", $data[4])))."'";   
						$this->Royalmail->updateAll(['manifest_table_id'=>1,'manifest_date'=>$manifest_date],['split_order_id'=> $order['MergeUpdate']['product_order_id_identify']]);	
						echo "==".$order['MergeUpdate']['product_order_id_identify'];
						echo "<br>";
						 
					}else{
						$manifest_date = "'". $order['MergeUpdate']['manifest_date']."'";      
						$this->Royalmail->updateAll(['manifest_table_id'=>1,'manifest_date'=>$manifest_date],['split_order_id'=> $order['MergeUpdate']['product_order_id_identify']]);	
						
					}
					file_put_contents(WWW_ROOT .'logs/rm_manifest_check_'.date('dmy').'.log' , $data[2]."\t".$data[1]."\t".$manifest_date."\n",  FILE_APPEND|LOCK_EX);		}	
			} 
			else{
				echo "Open order: ".trim($data[2]) ; 
				echo "<br>";
			}
					
		   }					
		}
		
		fclose($handle);
  		exit;	
	 }
		  	
 	public function royalMailServiceStart($details = null, $mergeupdate = null)
	{ 		  	
		$data = $this->royalMailGetToken($details);	
		// pr($data);exit;
		if(isset($data['token'])){		
			$this->royalMailCreateShipment($details, $mergeupdate, $data['token']);
		} 
	}
	
	
	public function royalMailGetToken($details = null){
		//$auth = $this->royalMailAuth();
		/*$x_ibm_client_id = $auth['x_ibm_client_id'];
		$x_ibm_client_secret = $auth['x_ibm_client_secret'];
		$x_rmg_user_name = $auth['x_rmg_user_name'];
		$x_rmg_password = $auth['x_rmg_password'];*/
		
		$filename =  WWW_ROOT.'logs/rm_token.txt';
		
		if (file_exists($filename)) {
			$token_time =  strtotime('+3 hours', filemtime($filename));		
			if($token_time > strtotime(date('Y-m-d H:i:s'))){
				 $tokenArray['token'] =  file_get_contents($filename);
				 return $tokenArray;
				 exit;
			}
		}
	
		$data = Configure::read( 'royalmail_auth' );
		$x_ibm_client_id = $data['x_ibm_client_id'];
		$x_ibm_client_secret = $data['x_ibm_client_secret'];
		$x_rmg_user_name = $data['x_rmg_user_name'];
		$x_rmg_password = $data['x_rmg_password'];	
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.royalmail.net/shipping/v3/token",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_SSL_VERIFYHOST=> false,
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLINFO_HEADER_OUT => true,			  
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_HTTPHEADER => array(
			"accept: application/json", 				 
			"x-ibm-client-id: $x_ibm_client_id",
			"x-ibm-client-secret: $x_ibm_client_secret",
			"x-rmg-security-password: $x_rmg_password",
			"x-rmg-security-username: $x_rmg_user_name" 
			
			),
		)); 
	
		$response = curl_exec($curl);
		$err  = curl_error($curl);	
		$info = curl_getinfo($curl);
		// pr($info );	 exit;
		curl_close($curl);		
		$tokenArray = array();
		$responsePath = WWW_ROOT .'logs/royalmail_token_error_'.date('F').'.log'; 	
		if ($err) {			
					
			file_put_contents($responsePath, "\n------".date("d-m-Y H:i:s")."-----\n" . $err, FILE_APPEND|LOCK_EX);
			/***********Send Error Email***************/
			$details['error_code'] = 'curl_error';
			$details['error_msg']  = $err;
			$this->sendErrorMail($details);
			
		} else {
			$res = json_decode($response);
		
			if(isset($res->httpCode)){				
				$tokenArray['error'] = $res->httpCode;
				 
				file_put_contents($responsePath, "\n------".date("d-m-Y H:i:s")."-----\n" . $response, FILE_APPEND|LOCK_EX);
				
				/***********Send Error Email***************/				
				if(isset($res->errors)){
					$details['error_code'] = $res->errors[0]->errorCode;
					$details['error_msg']  = $res->errors[0]->errorDescription.' Cause:'.$res->errors[0]->errorCause;
				}else{
					$details['error_code'] = $res->httpCode;
					$details['error_msg']  = $res->httpMessage;
				}
				$this->sendErrorMail($details);
				
			}else{ 
				$tokenArray['token'] = $res->token;		
				file_put_contents($filename,$tokenArray['token'], LOCK_EX); 				 
			}
		}
	 
	return $tokenArray;
	} 
	
	public function royalMailCreateShipment( $details = null, $mergeupdate = null, $token = null ){
		
		$this->loadModel( 'Royalmail' );
		 
 		$data = $this->royalDataFormat($details);
  		$auth = Configure::read( 'royalmail_auth' );
		$x_ibm_client_id = $auth['x_ibm_client_id'];
	
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.royalmail.net/shipping/v3/shipments",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLINFO_HEADER_OUT => true,
		CURLOPT_POSTFIELDS => json_encode($data),
		CURLOPT_HTTPHEADER => array(
			"accept: application/json",
			"content-type: application/json",
			"x-ibm-client-id: $x_ibm_client_id",
			"x-rmg-auth-token: $token" 
			),
		));
		echo "=================================================<br>";
		echo  json_encode($data);
		echo "=================================================<br>";
		echo $response = curl_exec($curl);
		echo "======<br><br><br>";
		$err  = curl_error($curl);
		$info = curl_getinfo($curl);
 		curl_close($curl);
		$path = WWW_ROOT .'logs/royalmail_shipment_err_'.date('dmy').'.log';
		if ($err) {		 
			file_put_contents($path , $details['split_order_id']."\t".$err. "\n", FILE_APPEND | LOCK_EX);
			/***********Send Error Email***************/
			$details['error_code'] = 'curl_error';
			$details['error_msg']  = $err;
			$this->sendErrorMail($details);
			$this->royalMailCancelShipmentOfError($details['split_order_id']);
		} else {		
		  $res = json_decode($response); 
		  pr($res );
 		
		  if(isset($res->HttpStatusCode) && $res->HttpStatusDescription == 'OK'){
 				
				foreach($res->Packages as $val){
					  
					$details['trackingNumber'] = $val->UniqueId;
					/*$image2DMatrix = base64_decode( $val->Primary2DBarcodeImage );
					$imgPath = WWW_ROOT .'royalmail/barcode/2d_matrix_'.$details['split_order_id'].'.png'; 			 
					file_put_contents($imgPath , $image2DMatrix);*/
					$LabelImages = base64_decode( $res->LabelImages );
					$imgPath = WWW_ROOT .'royalmail/labels/'.$details['split_order_id'].'.png'; 			 
					file_put_contents($imgPath , $LabelImages); 
					 
 					if(filesize($imgPath) > 100){
						$this->loadModel( 'MergeUpdate' );	
						$mergeupdate['track_id'] =  $val->UniqueId;
						$mergeupdate['reg_post_number'] =  $val->UniqueId;
						$mergeupdate['reg_num_img'] = $val->UniqueId.'.png';				 
						$this->MergeUpdate->saveAll( $mergeupdate );					
						//$this->labelHtml($details);
						
						$details['shipment_number'] 	= $val->UniqueId;
						$dataArray['FormattedUniqueId'] = $val->UniqueId;//$val->FormattedUniqueId;
						$dataArray['split_order_id']  	= $details['split_order_id']; 
						$dataArray['shipment_number'] 	= $val->UniqueId;
						$dataArray['details'] 		  	= json_encode($details);
						$dataArray['added_date'] 		= date('Y-m-d H:i:s');
						$dataArray['shipping_date'] 	= date('Y-m-d H:i:s');
						
						$conditions = array('Royalmail.split_order_id' => $details['split_order_id']);	
						
						if ($this->Royalmail->hasAny($conditions)){
							foreach($dataArray as $field => $rval){
								 $dataUpdate[$field] = "'".$rval."'";										
							}					
							$this->Royalmail->updateAll( $dataUpdate, $conditions ); 
						}else{ 
							$this->Royalmail->saveAll($dataArray);
						}
					
					}else{
						$this->lockOrder($details);	
						$this->Royalmail->updateAll( array( 'label_print' => 2 ), array( 'split_order_id' => $details['split_order_id'] ) );
						$details['error_code'] = 'label issue';
						$details['error_msg']  = 'Barcode Image Issue. Please re-print lables.';
						$this->sendErrorMail($details);		
					}				 
		  			
 			   }
  		   
		  }else{
				//echo "<br>".$res->httpMessage;
				//echo  $res->moreInformation;
				$infoPath = WWW_ROOT .'logs/royalmail/info_'.$details['split_order_id'].'.log'; 
				file_put_contents($infoPath , json_encode($data));
				$responsePath = WWW_ROOT .'logs/royalmail/error_'.$details['split_order_id'].'.log'; 
				file_put_contents($responsePath , $response);
				$responsePath = WWW_ROOT .'logs/royalmail/details_'.$details['split_order_id'].'.log'; 
				file_put_contents($responsePath , print_r($details,true));
				file_put_contents($path , $details['split_order_id']."\t". $response. "\n", FILE_APPEND | LOCK_EX);
				/***********Send Error Email***************/
				if(isset($res->errors)){
					$details['error_code'] = $res->errors[0]->errorCode;
					$details['error_msg']  = $res->errors[0]->errorDescription.' Cause:'.$res->errors[0]->errorCause;
				}else{
					$details['error_code'] = $res->httpCode;
					$details['error_msg']  = $res->httpMessage;
				}
				/*--------------Lock Order------------*/
				$this->lockOrder($details);				 
				/*-----------------------------------*/
				$this->sendErrorMail($details);				
								
		   } 
	 	}
	 
	}	

	public function royalMailCancelShipment($split_order_id = null, $is_ajax = 0 ){
		
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'MergeUpdate' );
		$result = $this->Royalmail->find('first', array('conditions' => array( 'split_order_id' => $split_order_id )));
		
		if(count($result) > 0){
		
			$shipment_number = $result['Royalmail']['shipment_number'];			
			$details = json_decode($result['Royalmail']['details'],true);
			 
			$tdata  = $this->royalMailGetToken();	
			$token = $tdata['token'];
									
			$auth = Configure::read( 'royalmail_auth' );
			$x_ibm_client_id = $auth['x_ibm_client_id']; 
			
 		  	$curl = curl_init();
			
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.royalmail.net/shipping/v3/shipments/cancel" ,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "PUT",	
			CURLOPT_POSTFIELDS => "[{\"ShipmentId\":\"{$shipment_number}\",\"ReasonForCancellation\":\"UploadedInError\"}]" ,
			CURLINFO_HEADER_OUT => true,	 
			CURLOPT_HTTPHEADER => array(
			    "accept: application/json",
				"content-type: application/json",
 				"x-ibm-client-id: $x_ibm_client_id", 
				"x-rmg-auth-token: $token"
				),
			));
			
			$response = curl_exec($curl);
			$err      = curl_error($curl);
			$info     = curl_getinfo($curl);
 			curl_close($curl);
			 
			if ($err) {			
				$responsePath = WWW_ROOT .'logs/royalmail/cancel_'.$split_order_id.'.log'; 
				file_put_contents($responsePath , $err);
				/***********Send Error Email***************/
				$details['error_code'] = 'curl_error_in_shipment_cancellation';
				$details['error_msg']  = $err;
				$this->sendErrorMail($details);
				$msg['msg']    = $err;	
				$msg['status'] = 'error';	
					
			} else {
				//echo $response; 
				$res = json_decode($response);   
				if(isset($res->HttpStatusCode)){
					if($res->HttpStatusCode > 200){
						$responsePath = WWW_ROOT .'logs/royalmail/cancel_'.$split_order_id.'.log'; 
						file_put_contents($responsePath , $response);
						/***********Send Error Email***************/
						$details['error_code'] = $res->HttpStatusCode;
						$details['error_msg']  = $res->Message;
						
						$this->sendErrorMail($details);	
						$msg['msg']    = $res->Message;	
						$msg['status'] = 'error';		
					}else{					
						$this->Royalmail->query( "delete from royalmails where split_order_id = '".$split_order_id."'" );
						
						$this->MergeUpdate->updateAll( array( 'royalmail' => 0 ), array( 'product_order_id_identify' => $split_order_id ) );
						$msg['msg']    = 'Shipment number cancelled';
						$msg['status'] = 'ok';	
						file_put_contents( WWW_ROOT .'logs/royalmail/cancel_'.date('dmY').'.log', "\n".$split_order_id."\t".date('Y-m-d H:i:s'),FILE_APPEND|LOCK_EX);				
					}
				}			
			}
		}else{
			$responsePath = WWW_ROOT .'logs/royalmail/cancel_'.$split_order_id.'.log'; 
			file_put_contents($responsePath ,'shipment_number not found');
			$msg['msg'] = 'Shipment number not found';
			$msg['status'] = 'error';	
		}
		if($is_ajax > 0){
			return $msg;
			//exit; 
		} 
		
	}
	
	public function royalMailCancelShipmentOfError($split_order_id = null){
		
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'MergeUpdate' );
		$result = $this->Royalmail->find('first', array('conditions' => array( 'split_order_id' => $split_order_id )));
		$msg = [];
		if(count($result) > 0){
		
			$shipment_number = $result['Royalmail']['shipment_number'];			
			$details = json_decode($result['Royalmail']['details'],true);
			 
			$data  = $this->royalMailGetToken();	
			$token = $data['token'];
									
			$auth = $this->royalMailAuth();
			$x_ibm_client_id     = $auth['x_ibm_client_id'];
			$x_ibm_client_secret = $auth['x_ibm_client_secret']; 
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.royalmail.net/shipping/v3/".$shipment_number,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "DELETE",	
			CURLINFO_HEADER_OUT => true,	 
			CURLOPT_HTTPHEADER => array(
			"accept: application/json",
				"content-type: application/json",
				"Accept-Encoding: gzip,deflate",
				"Host: api.royalmail.net",
				"Connection: Keep-Alive",
				"User-Agent: ".$_SERVER['HTTP_USER_AGENT'],
				"x-ibm-client-id: $x_ibm_client_id",
				"x-ibm-client-secret: $x_ibm_client_secret",
				"x-rmg-auth-token: $token"
				),
			));
			
			$response = curl_exec($curl);
			$err      = curl_error($curl);
			$info     = curl_getinfo($curl);
			 
			curl_close($curl);
			//echo $response; 
			if ($err) {			
				$responsePath = WWW_ROOT .'logs/royalmail_error_cancel_'.date('dmy').'.log'; 
				file_put_contents($responsePath , $err);
				/***********Send Error Email***************/
				$details['error_code'] = 'curl_error_in_shipment_cancellation';
				$details['error_msg']  = $err;
				$this->sendErrorMail($details);
				$msg['msg']    = $err;	
				$msg['status'] = 'error';	
					
			} else {
				 
				$res = json_decode($response); 
				if(isset($res->httpCode)){
					$responsePath = WWW_ROOT .'logs/royalmail_error_cancel_'.date('dmy').'.log'; 
					file_put_contents($responsePath , $response);
					/***********Send Error Email***************/
					
					$details['error_code'] = $res->errors[0]->errorCode;
					$details['error_msg']  = $res->errors[0]->errorDescription.' Cause:'.$res->errors[0]->errorCause.' Resolution:'.$res->errors[0]->errorResolution;
					$this->sendErrorMail($details);	
					$msg['msg']    = $res->errors[0]->errorDescription;	
				 	$msg['status'] = 'error';		
				}else{					
					//$this->Royalmail->query( "delete from royalmails where split_order_id = '".$split_order_id."'" );
					
					//$this->MergeUpdate->updateAll( array( 'royalmail' => 0 ), array( 'product_order_id_identify' => $split_order_id ) );
					//$msg['msg']    = 'Shipment number cancelled';
					//$msg['status'] = 'ok';	
					//file_put_contents( WWW_ROOT .'logs/royalmail_cancel_'.date('Ymd').'.log', "\n".$split_order_id."\t".date('Y-m-d H:i:s'),FILE_APPEND|LOCK_EX);				
				}			
			}
		}else{
			$responsePath = WWW_ROOT .'logs/royalmail_error_cancel_'.date('dmy').'.log'; 
			file_put_contents($responsePath ,'shipment_number not found');
			$msg['msg'] = 'Shipment number not found';
			$msg['status'] = 'error';	
		}
		// print_r($msg);
		//return $msg;
		//exit;
		 
		
	}
	
	public function royalMailUpdateShipment(){ 
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'MergeUpdate' );
		
		/*$all_orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status' => 1, 'MergeUpdate.pick_list_status' => 1,'MergeUpdate.royalmail' => 1,'MergeUpdate.scan_status' => 0, 'MergeUpdate.sorted_scanned' => 0,'MergeUpdate.manifest_status' => 0),'fields' => array('MergeUpdate.product_order_id_identify')));	*/
		
		$all_orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.royalmail' => 1,'MergeUpdate.scan_status' => 0, 'MergeUpdate.sorted_scanned' => 0),'fields' => array('MergeUpdate.status','MergeUpdate.product_order_id_identify')));	
		
 		if(count($all_orders) > 0){
			foreach( $all_orders as $result )
			{
				if($result['MergeUpdate']['status'] == 2){
					$this->royalMailCancelShipment($result['MergeUpdate']['product_order_id_identify']);
				}else{
					$this->updateShipment($result['MergeUpdate']['product_order_id_identify']);
				}
			}
		}
	}
		
	public function updateShipment($split_order_id = null){
 		
		$this->loadModel('Royalmail');
		
		$result = $this->Royalmail->find('first', array('conditions' => array( 'split_order_id' => $split_order_id )));
		
		if(count($result) > 0){
			
			$shipping_date = $this->nextShipmentDate(date('Y-m-d'));
			
			if($shipping_date  !=  $result['Royalmail']['shipping_date']){
				 	
 			  	$shipment_number = $result['Royalmail']['shipment_number'];	
				$split_order_id  = $result['Royalmail']['split_order_id'];				 
						
				$auth = Configure::read( 'royalmail_auth' );
				$x_ibm_client_id = $auth['x_ibm_client_id'];
				
				$tData  = $this->royalMailGetToken();	
				$token = $tData['token'];
				
				$curl = curl_init();
				
				curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.royalmail.net/shipping/v3/shipments/defer",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "PUT",
				CURLINFO_HEADER_OUT => true,
				CURLOPT_POSTFIELDS =>  "[{\"ShipmentId\":\"{$shipment_number}\",\"ShipmentDate\":\"{$shipping_date}\"}]",
				CURLOPT_HTTPHEADER => array(
				"accept: application/json",
					"content-type: application/json", 
					"x-ibm-client-id: $x_ibm_client_id", 
					"x-rmg-auth-token: $token"
					),
				)); 
				
				$response = curl_exec($curl);
				 
				$err = curl_error($curl);
				$info = curl_getinfo($curl);			
				curl_close($curl);
				
				if ($err) {		 
					$responsePath = WWW_ROOT .'logs/royalmail_shipment_update_'.$split_order_id.'.log'; 
					file_put_contents($responsePath , $err);
					/***********Send Error Email***************/
					$details['error_code'] = 'curl_error';
					$details['error_msg']  = $err;
					$this->sendErrorMail($details);
				} else {		
				  $res = json_decode($response);
				  if(isset($res->HttpStatusCode) ){
					  if($res->HttpStatusCode > 200){
							//echo "<br>".$res->httpMessage;
							//echo  $res->moreInformation;
							$responsePath = WWW_ROOT .'logs/royalmail_shipment_update_'.$split_order_id.'.log'; 
							file_put_contents($responsePath , $response);
							/***********Send Error Email***************/
							$details['error_code'] = $res->HttpStatusCode;
							$details['error_msg']  = $res->Message;
							$this->sendErrorMail($details);		
											
					   }else{					
							$this->Royalmail->updateAll( array('update_date' => "'".date('Y-m-d H:i:s')."'"),array('shipment_number' => $shipment_number));	   
							$responsePath = WWW_ROOT .'logs/royalmail_shipment_update_'.$split_order_id.'.log'; 
							file_put_contents($responsePath , date('Y-m-d H:i:s')."\t".$response);					
					   }
					}
				}
			}
		}
	} 
	
	public function createManifest(){
	
		/**
		*Update all order shipment that are in picklist and not processed till now.
		*/
		$this->royalMailUpdateShipment();
		 
		
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'RoyalmailManifest' ); 
		 	
		$auth = Configure::read( 'royalmail_auth' );
		$x_ibm_client_id = $auth['x_ibm_client_id'];
		
		$tData  = $this->royalMailGetToken();	
		$token = $tData['token'];
		
	 	
		$curl = curl_init();
		
		curl_setopt_array($curl, array(

		CURLOPT_URL => "https://api.royalmail.net/shipping/v3/manifests",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "{\"PostingLocation\":\"9000485960\"}",
		CURLOPT_HTTPHEADER => array(
		"accept: application/json",
		"content-type: application/json",
		"x-ibm-client-id: $x_ibm_client_id", 
		"x-rmg-auth-token: $token"
		),
		));
		
	echo	$response = curl_exec($curl);
		$err = curl_error($curl);		
		curl_close($curl);
		
		$responsePath = WWW_ROOT .'logs/royalmail/manifest_'.date('mdY').'.log'; 
		file_put_contents($responsePath , $response, FILE_APPEND|LOCK_EX);
		$responsePath = WWW_ROOT .'logs/royalmail_manifest_error_'.date('mdY').'.log'; 
		if ($err) {		 
				
				file_put_contents($responsePath , $err);
				/***********Send Error Email***************/
				$details['error_code'] = 'curl_error';
				$details['error_msg']  = $err;
				$this->sendErrorMail($details,1);
				$this->Session->setFlash($err, 'flash_danger');	
		}else {			
			 $res = json_decode($response); 	 pr($res);		
			 if(isset($res->HttpStatusCode)){
				 if($res->HttpStatusCode > 200){		
					 
					file_put_contents($responsePath , $response);		
					/***********Send Error Email***************/
					$details['error_code'] = $res->HttpStatusCode;
					$details['error_msg']  = $res->Message;
					
					$this->sendErrorMail($details,1);					
					$this->Session->setFlash($res->errors[0]->errorDescription, 'flash_danger');	 		
									
			   }else{ 
				
					foreach($res->Manifests as $val){
					
						$dataArray['batch_number']  = $val->ManifestNumber; 
						$dataArray['count']			= $val->TotalPackages;	
						$dataArray['total_weight']	= $val->TotalWeight;								
						$dataArray['is_printed']    = 1;	
						$dataArray['added_date']    = date("Y-m-d H:i:s");						 
						$this->RoyalmailManifest->saveAll($dataArray);
						$manifest_table_id = $this->RoyalmailManifest->getLastInsertId();
						
						$labelPdfPath = WWW_ROOT .'royalmail/manifests/'.date('Ymd').'_'.$val->ManifestNumber.'.pdf'; 			 
						file_put_contents($labelPdfPath , base64_decode($res->ManifestImage));
						
 						$rresult = $this->Royalmail->find('first', array('conditions' => array( 'shipment_number' => $val->shipmentNumber )));
						
						if(count($rresult) > 0){
							$this->Royalmail->updateAll( array('manifest_table_id' => $manifest_table_id,'service_code'=>"'".$val->ServiceCode."'",'manifest_date' => "'".$dataArray['added_date']."'"),array('shipment_number' => $val->shipmentNumber));
							$this->updateMergeUpdateTable($val->shipmentNumber);				
						} 				
					}
				
				} 
			}
		}
		//$this->redirect($this->referer());
	}
	 
	public function printManifest(){}
	
	public function createManifestCron(){
		
		$this->loadModel( 'RoyalmailManifest' ); 
		$result = $this->RoyalmailManifest->find('first', array('order'=>'id DESC'));
		$manifest_date = date('Y-m-d',strtotime($result['RoyalmailManifest']['added_date']));
		 
		if(date('Y-m-d') > $manifest_date){
			$this->createManifest();
			$royalmail_manifest_cron = date('Y-m-d H:i:s')."\tin_cron_codition\t";			
		}else{
			$royalmail_manifest_cron = date('Y-m-d H:i:s')."\tnot_in_cron_codition\t";	
		}
		
		$path = WWW_ROOT .'logs/royalmail/manifest_cron_'.date('Ymd').'.log'; 		
		file_put_contents($path , $royalmail_manifest_cron."\n",  FILE_APPEND|LOCK_EX);
		
		mail('avadhesh.kumar@jijgroup.com','Royalmail',$royalmail_manifest_cron);
		echo 'createManifestCron';
 		exit;		 
	}
	
	public function exportManifest(){
		
		$this->loadModel( 'RoyalmailManifest' ); 
		$result = $this->RoyalmailManifest->find('all', array('limit'=>4,'order'=>'id DESC'));
		return $result;
	}
	
	public function reprintLabel($split_order_id = null){
	     die('stop');
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'MergeUpdate' );
	 
 		if($split_order_id){
			$results = $this->Royalmail->find('all', array('conditions' => array('split_order_id' => $split_order_id,'manifest_table_id' => 0 ) ) );
		}else{
			$results = $this->Royalmail->find('all', array('conditions' => array('split_order_id  IN' => array('1477071-1','1477599-1','1478303-1','1478863-1'),'manifest_table_id' => 0) ) );	
			//$results = $this->Royalmail->find('all', array('conditions' => array('label_print' => 2,'manifest_table_id' => 0) ) );	
		}
		 
		$file_to_save = WWW_ROOT .'logs/royalmail/reprint_'.date('Ymd').'.log';	
				 
		if(count($results) > 0){
		
			$data = $this->royalMailGetToken();	
		
			if(isset($data['token'])){	
				
				foreach($results as $result){	
				
					$this->unLockOrder($result['Royalmail']['split_order_id']);
					
					$details = json_decode($result['Royalmail']['details'],true);
					
					$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $result['Royalmail']['split_order_id']),'fields'=>array('id')));	
					
					$mergeupdate['id']		= $orderItem['MergeUpdate']['id'];
					$mergeupdate['royalmail'] = 1;
					
					$this->royalMailGenerateLabel($details, $mergeupdate, $data['token']);
					 
					$this->Royalmail->updateAll( array('label_print' => 1),array('split_order_id' => $result['Royalmail']['split_order_id']));
					
					
					file_put_contents($file_to_save, $result['Royalmail']['split_order_id']."\n" , FILE_APPEND | LOCK_EX );
				} 
			
			} 	
				 
		}else{ 
			file_put_contents($file_to_save, date('Y-m-d H:i:s')."\tCron:no order found to re-Print\n" , FILE_APPEND | LOCK_EX );
			 
		}
		
		exit;
	}
	
	private function royalDataFormat($details = null){
	
		date_default_timezone_set('Europe/Jersey');
		
		$this->loadModel( 'RoyalmailManifest' ); 
		
		$result = $this->RoyalmailManifest->find('first', array('order'=>'id DESC'));
		
		$manifest_date = date('Y-m-d',strtotime($result['RoyalmailManifest']['added_date']));
		
		$shipping_date = date('Y-m-d');
		
		/*if(($manifest_date == date('Y-m-d')) || (date('H') >= '14')){
			$shipping_date = $this->nextShipmentDate(date('Y-m-d'));
		}*/   
		if($manifest_date == date('Y-m-d')){
			$shipping_date = $this->nextShipmentDate(date('Y-m-d'));
		}
		$store_country = strtoupper($details['CountryCode']);
		$country = $details['country'] ;
		if($store_country != ''){
			$country = $store_country;
		}
	 	$sub_source = strtolower($details['sub_source']); 
			 
		$weight = $details['packet_weight'] > 0 ? $details['packet_weight'] : 10;
		if(strpos($sub_source,'cost')!== false){
			$box_number = 'Box 824';
			$phone_number = '+443301170104';
			$email = 'accounts@euracogroup.co.uk';
			$VAT_Number = 'GB 304984295';  
			if(in_array($country ,['United Kingdom','GB','UK'])){  
				$VAT_Number   = 'GB 304984295'; 
			}else if(in_array($country ,['Germany','DE'])){
				$VAT_Number   = 'DE 321777974'; 
			}else if(in_array($country ,['France','FR'])){
				$VAT_Number   = 'FR 59850070509'; 
			}else if(in_array($country ,['Spain','ES'])){
				$VAT_Number   = 'ES N6061518D'; 
			}else if(in_array($country ,['Italy','IT'])){
				$VAT_Number   = 'IT 10905930961'; 
			}			 
			 
		}
		else if(strpos($sub_source,'marec')!== false){
 			$box_number = 'Box 825';
			$phone_number = '+443301170238';
			$email = 'accounts@esljersey.com'; 
			
			$VAT_Number = 'GB 307817693';  
			if(in_array($country ,['United Kingdom','GB','UK'])){  
				$VAT_Number   = 'GB 307817693'; 
			}else if(in_array($country ,['Germany','DE'])){  
				$VAT_Number   = 'DE 323086773'; 
			}else if(in_array($country ,[ 'France','FR'])){  
				$VAT_Number   = 'FR 07879900934';  
			}else if(in_array($country ,['Italy','IT'])){  
				$VAT_Number   = 'IT 11062310963'; 
			} 
		 }	
		else if(strpos($sub_source,'rainbow')!== false){
			$box_number = 'Box 826';
			$phone_number = '0123456789';
			$email = 'accounts@fresherbusiness.co.uk'; 
			
			$VAT_Number = 'GB 318649182';  
			if(in_array($country ,['United Kingdom','GB','UK'])){    
				$VAT_Number   = 'GB 318649182'; 
			}else if(in_array($country ,['Germany','DE'])){ 
				$VAT_Number   = 'DE 327173988'; 
			}else if(in_array($country ,['France','FR'])){ 
				$VAT_Number   = 'FR 48880490255';  
			}else if(in_array($country ,['Italy','IT'])){ 
				$VAT_Number   = 'IT 11061000961'; 
			} 
		}
		
 
 
	$data = 	array (
  'Shipper' => 
	  array (
		 'ShipperReference' => $details['split_order_id'],
		//'ShipperReference2' => $details['merge_id'], 
		'CompanyName' => 'C/o GLM',
		'ContactName' => 'C/o GLM',
		'AddressLine1' => $box_number,
		'AddressLine2' =>  '',
		'AddressLine3' => '',
		'Town' => 'Greenford', 
		'CountryCode' => 'GB',
		'Postcode' => 'UB18 7NL',
		'PhoneNumber' => $phone_number,
		'EmailAddress' => $email,
		'VatNumber' => $VAT_Number,
	  ),
  'Destination' => 
	  array (
		/*<!--$details['PostCode']-->*/
		'CompanyName' =>  $details['Company'],
		'ContactName' =>  $details['FullName'],
		'AddressLine1' => $details['Address1'],
		'AddressLine2' => $details['Address2'],
		'AddressLine3' => '',
		'Town' => $details['Town'], 
		'County' => $details['Region'],
		'CountryCode' => $details['CountryCode'],
		'Postcode' => trim($details['PostCode']),
		'PhoneNumber' => $details['PhoneNumber'],
		'EmailAddress' => $details['email'] 
	  ),
  'ShipmentInformation' => 
  array (
    'ShipmentDate' => $shipping_date,
    'ServiceCode' => 'CRL'.$details['type'],
    'ServiceOptions' => 
    array (
      'PostingLocation' => '9000485960',
      'ServiceLevel' => '01',
      'ServiceFormat' => $details['service_format'],
      //'Safeplace' => 'Front Porch',
      'SaturdayGuaranteed' => false, 
      'LocalCollect' => false,
      'RecordedSignedFor' => false,
    ),
    'TotalPackages' => 1,
    'TotalWeight' => $weight,
    'WeightUnitOfMeasure' => 'KG', //Grams
    'Product' => 'NDX',  //*DOX** - Documents Only **NDX** - All other shipment product types",
    'DescriptionOfGoods' => $details['category_name'],
    'ReasonForExport' => 'Sale of goods',
    'Value' => $details['order_total'],
    'Currency' => 'GBP',
    'LabelFormat' => 'PNG',//PDF", "PNG", "DATASTREAM"
    'SilentPrintProfile' => '',
    'ShipmentAction' => 'Process',//Process Allocate  Create
    'Packages' => 
    array (
      0 => 
      array (
        'PackageOccurrence' => 1,
        'Weight' => $weight,
        'Length' =>  $details['packet_length'],
        'Width' =>  $details['packet_width'],
        'Height' =>  $details['packet_height'],
      ),
    ),
    'Items' => 
    array (
      0 => 
      array (
        'Quantity' => 1,
        'Description' => $details['product_name'],
        'Value' => $details['order_total'],
        'Weight' => $weight,
        'PackageOccurrence' => 1,
        ),
    ),
  ),
);

	return $data;
	}	
	
  
 	public function labelHtml( $details = null ){		
		  		
		$this->loadModel( 'RoyalmailOutwardInwardCode' );
		$imgPath = WWW_ROOT.'royalmail/barcode/2d_matrix_'.$details['split_order_id'].'.png';
		
		if(file_exists($imgPath) &&  filesize($imgPath) > 100 )
		{
				$this->getBarcode( $details['split_order_id'] );
				$FormattedUniqueId = '';
				if(isset($details['FormattedUniqueId']) && $details['FormattedUniqueId'] != ''){
					$FormattedUniqueId = $details['FormattedUniqueId'];
					 
				}else{
					$this->loadModel( 'Royalmail' );
					$for = $this->Royalmail->find('first',array('conditions' => array('split_order_id' => $details['split_order_id'])));
					if(count($for) > 0){
						$FormattedUniqueId = $for['Royalmail']['FormattedUniqueId'];
 					}
				}
				
				list($pc_district) = explode(" ",$details['PostCode']);
				
				$outwardInward = $this->RoyalmailOutwardInwardCode->find('first',array('conditions' => array('pc_district' => $pc_district)));
				if(count($outwardInward) > 0){
					$outward_code = $outwardInward['RoyalmailOutwardInwardCode']['outward_code'];
					$inward_code = $outwardInward['RoyalmailOutwardInwardCode']['inward_code'];
				}
				
				$service = '48';
				if($details['type'] == 1 ){
					$service = '24';
				}
				
				$box_number = 824; 
				if(strpos($details['sub_source'],'CostBreaker')!== false){
					$box_number = 824;
				}
				else if(strpos($details['sub_source'],'RAINBOW')!== false){
					$box_number = 825;;
				}else if(strpos($details['sub_source'],'Marec')!== false){
					$box_number = 826;
				}	
				elseif(strpos($details['sub_source'],'BBD')!== false){
					$box_number = 827;
				}
				else if(strpos($details['sub_source'],'Tech_Drive')!== false){
					$box_number = 828;
				} 
				 
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF(); 
				//$dompdf->set_paper('A4', 'portrait');
				$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');
				/**************** for tempplate *******************/
				$html = '<!DOCTYPE html>';
				$html .= '<html>'; 
				$html .= '<head>';
				$html .= '<title>Label</title>'; 
				$html .= '<style>.container{margin:0px auto;}</style>'; 
				$html .= '</head>'; 
				$html .= '<body>';
				 
				$html .= '<div class="container"  style="width:371px; height:auto;  border:1px solid #000;  margin-left:-40px;margin-top:-39px;">';
				
				$html .= '<table  width="100%" cellpadding="0" cellspacing="10">';
				$html .= '<tbody>';
				$html .= '<tr>';
				$html .= '<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/Royal_Mail.png" width="115px"></td>';
				$html .= '<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/'.$service.'.jpg" width="50px"></td>';
				$html .= '<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/royal.png" width="100px" height="70px"></td>';
				$html .= '</tr>';
				$html .= '</tbody>';
				$html .= '</table>';
				 
				$html .= '<table  width="100%" cellpadding="0" cellspacing="0" style="border-top:1px solid #000">';
				$html .= '<tbody>';
				$html .= '<tr>';
				$html .= '<td valign="middle" align="left" width="25%" style="text-align: center;font-size: 21px;font-weight: bold;font-family:Chevin Bold ">'.$outward_code.'</td>';
				$html .= '<td valign="middle" align="left" width="25%" style="border-left:1px solid #000;background: #000;color: #fff;text-align: center;font-size: 21px;font-weight: bold;font-family:Chevin Bold ;">'.$inward_code.'</td>';
				$html .= '<td valign="middle" align="left"></td>';
				$html .= '<td valign="middle" align="left" width="20%" style="border-left:1px solid #000"><table align="center"  width="100%" cellpadding="0" cellspacing="0"><tr><td style="border-bottom:1px solid #000" align="center">Parcel</td></tr><tr><td align="center">'.($details['weight']*1000).'g</td></tr></table></td>';
				$html .= '</tr>';
				$html .= '</tbody>';
				$html .= '</table>';
				
				$html .= '<table width="100%" cellpadding="0" cellspacing="10" style="border-bottom:1px solid #000; border-top:1px solid #000;">';
				$html .= '<tbody>';
				$html .= '<tr><td valign="top" align="left">'.$FormattedUniqueId.'</td></tr>';
				$html .= '<tr>';
				$html .= '<td valign="top" width="50%" align="left" >';
				$html .= '<img src="'. WWW_ROOT .'royalmail/barcode/2d_matrix_'.$details['split_order_id'].'.png" width="83px" height="83px" style="margin-left:10px;">';
				$html .= '</td>';
			
				$html .= '</tr>';
				$html .= '</tbody>';
				$html .= '</table>';
				
				$html .= '<table width="100%"  cellpadding="0" cellspacing="10" >';
				$html .= '<tbody>';
				$html .= '<tr>';
				$html .= '<td valign="top" width="75%">';
				$html .= '<div style="font-size:10pt; !important; font-family:Ariel regular light; margin-left:10px;">'; 
				$html .='<strong>'. ucwords(strtolower($details['FullName'])).'</strong><br>';
				
				if(strlen($details['Address1']) > 25 || strlen($details['Address2']) > 25){
				
					$lines = explode("\n", wordwrap(htmlentities($details['Address1']) .' '.htmlentities($details['Address2']), '25'));
					if(isset($lines[0]) && $lines[0] != ''){
						$html .=  $lines[0].'<br>';
					}
					if(isset($lines[1]) && $lines[1] != ''){
						$html .=  $lines[1].'<br>';
					}
					if(isset($lines[2]) && $lines[2] != ''){
						$html .=  $lines[2].'<br>';
					}
				
				}else{
					$html .= ucfirst($details['Address1']).'<br>';
					if($details['Address2']) $html .= ucfirst($details['Address2']).'<br>';
				}
				
				
				$html .= ucfirst($details['Town']).'<br>';
				$html .= $details['PostCode'];
				$html .= '</div>';
				$html .= '</td>'; 
				
				$html .= '<td valign="bottom" width="25%" align="right" style="writing-mode: tb-rl;
-webkit-transform: rotate(90deg);
-moz-transform: rotate(90deg);
-o-transform: rotate(90deg);
-ms-transform: rotate(90deg);
transform: rotate(180deg);
white-space: nowrap;
display: block;
bottom: 0;
width: 20px;
height: 20px;">AVADHESH';
				$html .= '<img src="'.WWW_ROOT.'royalmail/img/'.$box_number.'.png" style="height:100px;float:right;">';
				$html .= ' </td>';
				$html .= ' </tr>';
				$html .= '</tbody>';   
				$html .= ' </table>';
				
				 
				$html .= '<table width="100%" style="border-top:1px solid #000;" >';
				$html .= '<tbody>';
				$html .= '<tr>';
				$html .= '<td valign="top" align="center" style="padding-top:5px;"><p style="font-family:chevin; font-size:12px;">Customer Reference:'.$details['split_order_id'].'</p>';
				
				$html .= '</td>';
				$html .= '</tr>';
				$html .= '</tbody>';
				$html .= '</table>';
			 
				$html .= ' </div>';
				 
				$html .= ' <div  style="text-align:center;width:100%;padding-top:25px;">';
				$html .= '<img src="'.WWW_ROOT .'img/orders/barcode/'.$details['split_order_id'].'.png" style="text-align:center">';
				$html .= ' </div>';
				
				$html .= '</body>';
				$html .= '</html>';
				 echo $html;
			 
 					
				$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
				$dompdf->render();
				//$dompdf->stream();			
								
				$file_to_save = WWW_ROOT .'royalmail/labels/label_'.$details['split_order_id'].'.pdf';	
				 
				file_put_contents($file_to_save, $dompdf->output());  
				unset($html);
				unset($dompdf);
		}	
		
	}
	
	public function customLabelHtml( $details = null ){		
		//die('Uncomment Code.');
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Country' );	
		$order_ids = array(3152138); 
		 
		foreach($order_ids as $order_id){
			 
			$orderItems = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id )));
				foreach($orderItems as $val){
					App::import( 'Controller' , 'Cronjobs' );		
					$objController 	= new CronjobsController();
					$getorderDetail	=	$objController->getOpenOrderById($order_id);
					$cInfo = $getorderDetail['customer_info'];
					$order->TotalsInfo = $getorderDetail['totals_info'];
					
					$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
					
				 	$Address1 = $cInfo->Address->Address1;
					$Address2 = $cInfo->Address->Address2;
					 
					
					$details['sub_source'] = $getorderDetail['sub_source'];			 
				 
					$details['FullName'] =  $this->replaceFrenchChar($cInfo->Address->FullName);
					$details['Company'] =  '';
					$details['building_name'] =  '';
					$details['building_number'] = '0';
					  
					$details['Address1'] = $this->replaceFrenchChar($Address1);
					$details['Address2'] = $this->replaceFrenchChar($Address2);
					$details['Town'] = $this->replaceFrenchChar($cInfo->Address->Town);
					$details['PostCode'] = $cInfo->Address->PostCode;
					$details['CountryCode'] = $country_data['Country']['iso_2'];
					$details['split_order_id'] = $val['MergeUpdate']['product_order_id_identify']; 
					if($val['MergeUpdate']['service_name'] == 'royalmail_24'){
						$details['type'] = 1 ;
					} else{
						$details['type'] = 2 ;
					}
					
				
					  $this->labelHtml($details);
				} 
		} 
		exit;
	}
	private function lockOrder($details = null){
	
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderNote' );	
		$this->loadModel( 'RoyalmailError' );
		$md = $this->MergeUpdate->find('first', array( 'conditions' => array('product_order_id_identify' => $details['split_order_id']),'fields'=>'order_id' ) );
		if(count($md) > 0){
			$lock_note = $details['error_msg'];
			
			$firstName = 'RoyalMail';
			$lastName = 'Cron';
			$mdata['royalmail'] = 0;
			$mdata['status']    = 3;
			$mdata['rm_error_msg'] = "'".$lock_note."'";
			 
			$this->MergeUpdate->updateAll($mdata, array( 'product_order_id_identify' => $details['split_order_id'] ) );
			$this->OpenOrder->updateAll(array('OpenOrder.status' => 3), array('OpenOrder.num_order_id' => $md['MergeUpdate']['order_id']));
			
			$noteDate['order_id'] = $md['MergeUpdate']['order_id'];
			$noteDate['note'] = $lock_note;
			$noteDate['type'] = 'Lock';
			$noteDate['user'] = $firstName.' '.$lastName;
			$noteDate['date'] = date('Y-m-d H:i:s');
			$this->OrderNote->saveAll( $noteDate ); 
			$roe['split_order_id']  = $details['split_order_id'];
			$roe['error_msg'] 		= $lock_note;
			$this->RoyalmailError->saveAll( $roe ); 
		}
	}	
	
	public function unLockOrder($split_order_id = null)
	{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );			 
			$md = $this->MergeUpdate->find('first', array( 'conditions' => array('product_order_id_identify' => $split_order_id),'fields'=>'order_id' ) );
			if(count($md) > 0)
			{
				$this->MergeUpdate->query( "delete from royalmail_errors where split_order_id = '{$split_order_id}'" );
				
				$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 0,'MergeUpdate.rm_error_msg' => '""'  ) , array( 'MergeUpdate.product_order_id_identify' => $split_order_id ) );
				$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 0 ) , array( 'OpenOrder.num_order_id' => $md['MergeUpdate']['order_id'] ) );
			}
			 
	}
	
	private function sendErrorMail($details = null, $is_manifest = 0){
	
		if($is_manifest > 0){
			$subject   = 'RoyalMail create manifest issue in Xsensys';
			$mailBody  = '<p><strong>Create manifest have below issue please review and solve it.</strong></p>';
		}else{
			$subject   = $details['split_order_id'].' RoyalMail orders issue in Xsensys';
			$mailBody  = '<p><strong>'.$details['split_order_id'].' have below issue please review and solve it.</strong></p>';
		}	
		
		$mailBody .= '<p>Error Code : '.$details['error_code'].'</p>';
		$mailBody .= '<p>Error Message : '.$details['error_msg'].'</p>';
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('');
		$email->emailFormat('html');
		$email->from('info@euracogroup.co.uk');
		//$email->to( array('avadhesh.kumar@jijgroup.com','shashi@euracogroup.co.uk','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));	
		if($details['error_code'] == 'E1001'){
			$email->to( array('avadhesh.kumar@jijgroup.com','abhishek@euracogroup.co.uk','deepak@euracogroup.com','vikas.kumar@euracogroup.co.uk','ankit.nagar@euracogroup.co.uk'));
   		}else if(in_array($details['error_code'],array('E0015','E0007','E0005','500'))){
			$email->to( array('avadhesh.kumar@jijgroup.com','shashi@euracogroup.co.uk','abhishek@euracogroup.co.uk','deepak@euracogroup.com','vikas.kumar@euracogroup.co.uk','ankit.nagar@euracogroup.co.uk'));	
		}else if(in_array($details['error_code'],array('E1138','E1136','E1135'))){
			$email->to( array('test.kumar@jijgroup.com'));	
		}else{
			$email->to( array('avadhesh.kumar@jijgroup.com'));	
		}	
		
		 			  
		$getBase = Router::url('/', true);
		$email->subject( $subject );
	  	$email->send( $mailBody );
	}
	 
	private function getBarcode( $split_order_id ){ 
		
		$imgPath = WWW_ROOT .'img/orders/barcode/';   
 		
		if(!file_exists($imgPath.$split_order_id.'.png')){
		
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
			
			$colorFront = new BCGColor(0, 0, 0);
			$colorBack = new BCGColor(255, 255, 255); 
			
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->setLabel(false);
			$code128->parse($split_order_id);
			
			//Drawing Part
			$imgOrder128 = $split_order_id.".png";
			$imgOrder128path = $imgPath.$split_order_id.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);  
		}
		      
	}		
	 	 
	private function updateMergeUpdateTable($shipment_number = null){
		
		$this->loadModel('MergeUpdate');
		$this->loadModel('Royalmail');
		
		$result = $this->Royalmail->find('first', array('conditions' => array( 'shipment_number' => $shipment_number )));	
			
		$this->MergeUpdate->updateAll( array('royalmail'=>2,'manifest_date' => "'".date('Y-m-d H:i:s')."'"),array('product_order_id_identify' => $result['Royalmail']['split_order_id']));	
	}
	
	private function nextShipmentDate($shipping_date = null)
	{   
		$sunday   = date('Y-m-d', strtotime('sunday this week'));
		$holidays = array($sunday,"2017-08-04","2017-08-28","2017-12-25","2017-12-26","2018-01-01","2018-03-30","2018-04-02","2018-05-07","2018-05-09","2018-05-28","2018-08-27","2018-08-27","2018-12-25","2018-12-26"); 		 
		
		$shipping_date = date('Y-m-d',strtotime($shipping_date . "+1 day"));			
		if(in_array($shipping_date, $holidays)){				
			$shipping_date = $this->nextShipmentDate($shipping_date);
		} 	
				
		return  $shipping_date;
	}
	
	
	public function applyPostNL(){
		exit;
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'PostalServiceDesc' );
		$royal_sur_charge = 0.40;
		$fuel_percent     = 3;
		$dhl 			  = 1.20;
		$dhl_additional_cost = 0.42;
			
		$currency 		  = $this->getCurrencyRate();
		$exchange_rate	  = $currency ['EUR'];  
		$return 		  = array();
		$count = 0;
		//$orderItems = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status IN' =>array(0,1),'MergeUpdate.delevery_country' => 'United Kingdom'),'limit'=>'200','Order BY id ASC'));	
		 
		 $orderItems = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status' => 0,'MergeUpdate.product_order_id_identify IN ' => array('1044828-1','1044834-1','1044851-1','1044845-1','1044857-1','1044857-2','1044924-1','1044925-1','1044878-1','1044850-1','1036515-1'))));	
		
		//pr( $orderItems);exit; 
		 //966596 966653
		if(count($orderItems) > 0)
		{
 			$this->loadModel( 'Product' );
			$this->loadModel( 'ProductDesc' );
			$this->loadModel( 'Country' );	
			
			foreach($orderItems as $orderItem ) 
			{
				$quantity 	   = $orderItem['MergeUpdate']['quantity'];
				$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
				$packet_length = $orderItem['MergeUpdate']['packet_length'];
				$packet_width  = $orderItem['MergeUpdate']['packet_width'];
				$packet_height = $orderItem['MergeUpdate']['packet_height'];
				$order_id 	   = $orderItem['MergeUpdate']['order_id'];
				
				/*-------------------Royal Mail Calculations------------------------*/		
				
				 if($packet_weight > 2){
					 $royalMailPostal = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
							  
							 
				 }else{
					 $dim = array($packet_width,$packet_height,$packet_length);
					 asort($dim);
					 $final_dim = array_values($dim) ;							
					
					 $royalMailPostal = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
				 }
				$postNLFee = 0;  $all_postnl = array();
				if(count($royalMailPostal) > 0)
				{
					$per_item = $royalMailPostal['PostalServiceDesc']['per_item'];
					
					
					$fuel_surcharge = ($per_item  * $fuel_percent) / 100;
					$additional_fee = ($dhl + $dhl_additional_cost) * $packet_weight ;							
					$royalFee       = ($per_item + $fuel_surcharge + $additional_fee) ;	
					
					//echo $royalFee;		
					//$royalFee = $royalMailPostal['PostalServiceDesc']['per_item'] + ($packet_weight * $royal_sur_charge);
					
					/*-------------------PostNL Calculations------------------------*/
					$postNlPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey',
									'Location.county_name' => 'United Kingdom', 'PostalServiceDesc.max_weight >=' => $packet_weight, 'PostalServiceDesc.max_length >=' => $packet_length, 'PostalServiceDesc.max_width >=' => $packet_width, 'PostalServiceDesc.max_height >=' => $packet_height, 'PostalServiceDesc.courier' => 'Belgium Post')));
					 
					foreach($postNlPostal as $pv){
						$per_item 	= $pv['PostalServiceDesc']['per_item'];
						$per_kg 	= $pv['PostalServiceDesc']['per_kilo'];
						$weightKilo = $pv['PostalServiceDesc']['max_weight'];
						$postalid 	= $pv['PostalServiceDesc']['id'];
					
						$all_postnl[$postalid] = (($per_item + ($per_kg * $orderItem['MergeUpdate']['packet_weight'])) / $exchange_rate ) + ($dhl * $orderItem['MergeUpdate']['packet_weight']);
					} 
					
					//pr($all_postnl);
					if(count($all_postnl) > 0){
						$postNLFee  = min($all_postnl);	
					
						$postnl_id = array_search($postNLFee, $all_postnl); 
						if($postnl_id != $orderItem['MergeUpdate']['service_id']){
							 echo " order_id==".$orderItem['MergeUpdate']['product_order_id_identify']; 
							 echo " postnl_id==".$postnl_id;
							 echo " service_id= ".$orderItem['MergeUpdate']['service_id'];
							 echo "</br>";
						 }
						//echo " postNLFee==".$postNLFee;
						//echo " royalFee==".$royalFee;
						 	
					}				 
  					
					/*-------------------Compare And Replace------------------------*/
					//if(($postNLFee > 0) && ($postNLFee > $royalFee)) {
					if($postnl_id != $orderItem['MergeUpdate']['service_id']){
							$count++;
							 
							
							$post_nl = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $postnl_id)));
						 
							$split_order_id 	   = $orderItem['MergeUpdate']['product_order_id_identify']; 
							
							echo " split_order_id=".$split_order_id;
							echo " postNLFee=".$postNLFee;
							echo " royalFee=".$royalFee;
							echo "===". $royalMailPostal['PostalServiceDesc']['id'];
							echo "</br>";	
							 
							$data['royalmail'] 			= 0;
							$data['service_name'] 		= $post_nl['PostalServiceDesc']['service_name'];
							$data['track_id'] 			= '';
							$data['reg_post_number'] 	= '';
							$data['provider_ref_code'] 	= $post_nl['PostalServiceDesc']['provider_ref_code'];
							$data['service_id'] 		= $post_nl['PostalServiceDesc']['id'];
							$data['service_provider'] 	= 'PostNL';								 
							$data['template_id'] 		= $post_nl['PostalServiceDesc']['template_id'];  
							$data['id'] 				= $orderItem['MergeUpdate']['id'];
							
							$responsePath = WWW_ROOT .'logs/royalmail_to_postnl_'.date('F').'.log'; 
							//file_put_contents($responsePath , $split_order_id."\n",  FILE_APPEND|LOCK_EX);	
							//$this->royalMailCancelShipment($split_order_id);
							//$this->MergeUpdate->saveAll($data);
							 pr($data);
							 
							 
					}else{
						//echo $order_id. ' No order found in condition.<br>';
					}
					/*------------------end--------------------------------*/
				}else{
					echo $msg = 'This order may have over weight or any other issue.'; 
				}
			}
		//return $return;
	  }
	  else{
		echo 'No order found.';
		}
		echo "count=".$count;
	}
	
	public function CancelShipment($shipment_number = null){
		
		 
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'MergeUpdate' );
		$result = $this->Royalmail->find('first', array('conditions' => array( 'shipment_number' => $shipment_number )));
		
		if(count($result) > 0){
		
			$shipment_number = $result['Royalmail']['shipment_number'];			
			$details = json_decode($result['Royalmail']['details'],true);
			 
			$data  = $this->royalMailGetToken();	
			$token = $data['token'];
									
			$auth = $this->royalMailAuth();
			$x_ibm_client_id     = $auth['x_ibm_client_id'];
			$x_ibm_client_secret = $auth['x_ibm_client_secret']; 
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.royalmail.net/shipping/v3/".$shipment_number,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "DELETE",	
			CURLINFO_HEADER_OUT => true,	 
			CURLOPT_HTTPHEADER => array(
			"accept: application/json",
				"content-type: application/json",
				"Accept-Encoding: gzip,deflate",
				"Host: api.royalmail.net",
				"Connection: Keep-Alive",
				"User-Agent: ".$_SERVER['HTTP_USER_AGENT'],
				"x-ibm-client-id: $x_ibm_client_id",
				"x-ibm-client-secret: $x_ibm_client_secret",
				"x-rmg-auth-token: $token"
				),
			));
			
			$response = curl_exec($curl);
			$err      = curl_error($curl);
			$info     = curl_getinfo($curl);
			 
			curl_close($curl);
			
			pr($info);
		}
		exit;
	}
	
	public function getCurrencyRate()
	{	 			
		$this->loadModel('CurrencyExchangeRate');	
		
		$curencies = array('INR','EUR','USD');
		$url='http://www.floatrates.com/daily/gbp.xml';
		$xml = file_get_contents($url);
		$curArray = json_decode(json_encode(simplexml_load_string($xml)),true);
		if(count($curArray) > 0 ){
			$this->CurrencyExchangeRate->query("TRUNCATE currency_exchange_rates");
			foreach($curArray['item'] as $val) {
				if (in_array($val['targetCurrency'],$curencies)) {			
					$curency['base_currency'] = 'GBP';
					$curency['exchange_rate'] = $val['exchangeRate'];	
					$curency['target_currency'] = $val['targetCurrency'];					
					$this->CurrencyExchangeRate->saveAll($curency);	
				}
			}		
		}
		
		$curency = $this->CurrencyExchangeRate->find("all"); 
		foreach($curency as $val){
			$curencyArr[$val['CurrencyExchangeRate']['target_currency']] = $val['CurrencyExchangeRate']['exchange_rate'];
		}
				
		return $curencyArr; 			
 	}
	
	public function RoyalmailErrors(){
		
 		$this->loadModel('RoyalmailError' ); 
		$this->loadModel('Royalmail');	
		
		$added_date   = date('Y-m-d H:i:s', strtotime('-2 days'));
		$rm_result 	  = $this->Royalmail->find('all',array('conditions' => array('manifest_table_id' => 0,'added_date < ' => $added_date)));
		//pr($rm_result );
		$mailBody 	  = '';
		$path = WWW_ROOT.'/logs/';
		$file = date('dmy_h').'_rm_not_manifested.log';
		
		if(count($rm_result) > 0){
		
			$mailBody 	= "<a href='".Router::url('/', true)."logs/".$file."'>Downloda ".$file ."</a>\n";
			$mailBody  .= "<p>------------------Orders Not Manifested Yet----------------------------</p>\n";
			foreach($rm_result as $val){
				$mailBody  .= "<p>".$val['Royalmail']['split_order_id']."\t\t".$val['Royalmail']['added_date']."</p>\n";
				file_put_contents($path.$file,$val['Royalmail']['split_order_id']."\r\n", FILE_APPEND | LOCK_EX);
			}
			$mailBody  .= "<p>----------------------------------------------------</p>\n";
		}
		
		$result = $this->RoyalmailError->find('all');
		 			 
		if(count($result) > 0){
		 	 
			foreach($result as $v){
				$mailBody  .= "<p>".$v['RoyalmailError']['split_order_id']."\t".$v['RoyalmailError']['error_msg']."\t". $v['RoyalmailError']['timestamp']."</p>\n";
				
			
			}
		}
		
		if($mailBody != ''){
			$subject   = 'RoyalMail Error in Xsensys';			  
			App::uses('CakeEmail', 'Network/Email');
			$email = new CakeEmail('');
			$email->emailFormat('html');
			$email->from('info@euracogroup.co.uk');
 			$email->to( array('avadhesh.kumar@jijgroup.com','amit@euracogroup.co.uk'));	
 			$getBase = Router::url('/', true);
			$email->subject( $subject );
			$email->send( $mailBody );
			echo "RoyalmailErrors";
		}
 	 
		exit;
 	}
	
	public function server($string = null){
		echo date('Y-m-d H:i:s');
	}
	
	private function replaceFrenchChar($string = null){
  		return iconv('UTF-8','ASCII//TRANSLIT',$string);	 
	}	
			
	private function replaceFrenchChar30AUG($string = null){
			
		$unwanted_array = array('�'=>'S', '�'=>'s', '�'=>'Z', '�'=>'z', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E','�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', 'N�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U','�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c','�'=>'e', '�'=>'e', '�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n','n�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o','�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'b', '�'=>'y','�'=>'u','�'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}	
	 
}


/*curl -O http://xsensys.com/RoyalMail/printManifest &>/dev/null 2>&1
curl -O http://wms.esljersey.com/RoyalMail/printManifest &>/dev/null 2>&1
*/
?>