<?php
//error_reporting(0);
ini_set('memory_limit', '2048M');
class LocationswapsController extends AppController
{
    
    var $name = "Locationswaps";
    
    var $components = array('Session','Upload','Common','Paginator');
    
    var $helpers = array('Html','Form','Session','Common' , 'Soap' , 'Paginator');
    
   	public function index()
	{
		$this->layout = 'index';
		
	}
	
	public function getskulocation()
	{
		$this->loadModel('Product');
		$this->loadModel('BinLocation');
		
		//$sku 			=	'S-0080D2952';//$this->request->data['sku'];
		$sku 			=	trim($this->request->data['sku']);
		$getbarcode		=	$this->Product->find('first', array('conditions' => array('product_sku' => $sku), 'fields' => array( 'ProductDesc.barcode' ) )  );
		$barcode		=	$getbarcode['ProductDesc']['barcode'];
		$locationDetail	=	$this->BinLocation->find('all', array( 'conditions' => array( 'barcode' => $barcode ) ) );
		echo json_encode(array('status' => '1', 'data' => $locationDetail));		
		exit;
	}
	
	public function swaplocationqty()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('BinLocation');
		
		$location		=	$this->request->data['location'];
		$barcode		=	$this->request->data['barcode'];
		$locatiostock	=	$this->request->data['locationstock'];
		$selectedloc	=	$this->request->data['selectedloc'];
		$swapqty		=	$this->request->data['swapqty'];
		
		$checkbinlocation	=	$this->BinLocation->find('first', array( 'conditions' => array( 'barcode' => $barcode, 'bin_location' => $location ) ) );
		$idone	=	$checkbinlocation['BinLocation']['id'];
		$checkbinlocation['BinLocation']['barcode'];
		$checkbinlocation['BinLocation']['bin_location'];
		$checkbinlocation['BinLocation']['stock_by_location'];
		$checkbinlocation['BinLocation']['priority'];
		$checkbinlocation['BinLocation']['timestamp'];
		if($checkbinlocation['BinLocation']['stock_by_location'] >= $swapqty)
		{
			$locationone					=	$checkbinlocation['BinLocation']['stock_by_location'] - $swapqty;
			$data['id']				   		= 	$idone;
			$data['stock_by_location'] 		= 	$locationone;
			$this->BinLocation->saveAll($data);
			$selectedlocation	=	$this->BinLocation->find('first', array( 'conditions' => array( 'barcode' => $barcode, 'bin_location' => $selectedloc ) ) );
			$idtwo				=	$selectedlocation['BinLocation']['id'];
			$selectedlocation['BinLocation']['barcode'];
			$selectedlocation['BinLocation']['bin_location'];
			$selectedlocation['BinLocation']['stock_by_location'];
			$selectedlocation['BinLocation']['priority'];
			$selectedlocation['BinLocation']['timestamp'];
			$locationtwo				   =	$selectedlocation['BinLocation']['stock_by_location'] + $swapqty;
			$dataswap['id']				   = 	$idtwo;
			$dataswap['stock_by_location'] = 	$locationtwo;
			$this->BinLocation->saveAll($dataswap);
			$locationDetail	=	$this->BinLocation->find('all', array( 'conditions' => array( 'barcode' => $barcode ) ) );
			
			$swapqty_log = "Barcode=".$barcode."SwapQty=".$swapqty."\tFrom Location=".$checkbinlocation['BinLocation']['bin_location']."\tBefore From Location Qty=".$checkbinlocation['BinLocation']['stock_by_location']."\tTo Location=".$selectedlocation['BinLocation']['bin_location']."\tAfter From Location Qty=".$locationone."\tBefore To Location Qty=".$selectedlocation['BinLocation']['stock_by_location']."\tAfter To Location Qty=".$locationtwo."\tUser=".$this->Session->read('Auth.User.username')."\t".date('Y-m-d H:i:s')."\n";
			file_put_contents(WWW_ROOT."img/SwapLocationQty.log", $swapqty_log, FILE_APPEND | LOCK_EX);	
			
			echo json_encode(array('status' => '1', 'data' => $locationDetail));		
			exit;
		}
		else
		{
			echo json_encode(array('status' => '0', 'data' => 'Location value should be greater the or equal to swap value.'));		
			exit;
		}
	}
	public function swapLocation()
	{
		$this->layout = 'index';
		$this->loadModel('BinLocation');
		$bin_locat = $this->BinLocation->find('all',array('GROUP BY'=>'bin_location','order'=>'bin_location ASC'));
		if(count($bin_locat) > 0){
			foreach( $bin_locat as $bin ){
				if($bin['BinLocation']['bin_location']){
				$bin_locations[$bin['BinLocation']['bin_location']] = $bin['BinLocation']['bin_location'];
				}
			}
		}	
		$bin_locations = $this->msortnew($bin_locations, array('bin_location'));
		$this->set('getalllocations', $bin_locations);
	}
	
	public function msortnew($array, $key, $sort_flags = SORT_REGULAR) {
  	  if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    // @TODO This should be fixed, now it will be sorted as string
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
            asort($mapping, $sort_flags);
			natsort($mapping);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
	}
	
        	
}

?>
