<?php
error_reporting(0);
class NewInvoiceController extends AppController
{
    var $name = "NewInvoice";
    
    var $helpers = array('Html','Form','Session');
	
	var $COST_STORE_NAME	  = 'EURACO GROUP LIMITED';//CostDroper
	var $COST_STORE_ADDRESS	  = '49 Oxford Road St Helier <br>Jersey JE2 4LJ';
	var $COST_REG_NUMBER 	  = '109464';
	
	var $RAIN_STORE_NAME	  = 'FRESHER BUSINESS LIMITED';//Rainbow
	var $RAIN_STORE_ADDRESS   = 'Beachside Business Centre<br>Rue Du Hocq St Clement<br>Jersey JE2 6LF';
	var $RAIN_REG_NUMBER 	  = '108507';
	
	var $MARE_STORE_NAME	  = 'ESL LIMITED'; //Marec
	var $MARE_STORE_ADDRESS   = 'Unit 4 Airport Cargo Centre<br>L\'avenue De La Commune St Peter<br>Jersey JE3 7BY';
	var $MARE_REG_NUMBER 	  = '118438';
    
	public function beforeFilter()
    {
	 	parent::beforeFilter();
		$this->layout = false;  
		$this->Auth->Allow(array('Generate','GenerateB2b','cronEuFBAInvoice','GenerateB2Cslip')); 		
		//$this->GlobalBarcode = $this->Components->load('Common'); 
		$this->Common = $this->Components->load('Common'); 
 	} 
	
	public function index()
    {
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
		$html .= '<body>'.$htmlTemplate.'</body>';
				
		$name	= 'Invoice.pdf';							
		$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
     
		echo $r = 'Юлия Свеженцева';
		echo "<br>=====";
		echo utf8_encode($r);
		echo "<br>=====";
		echo mb_convert_encoding($r, 'HTML-ENTITIES','UTF-8');
		exit;
		
	}
	
   	public function GenerateB2b($amazon_order_id = null)
    { 
				 
 		$order = $this->getOrderByNumId_openorder( $amazon_order_id );	
		
		if(is_object($order)){
					 
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();	
			
			$result 		=	'';
			$htmlTemplate	=	'';	
	
			if(strtotime($order->GeneralInfo->ReceivedDate) > strtotime('2019-09-30 23:59:59')){
				 $htmlTemplate	=	$this->getB2Bhtml($order);	
			}else{
				 $htmlTemplate	=	$this->getB2Bhtml30($order);	
			}	
			$SubSource		=	$order->GeneralInfo->SubSource;
			$ReferenceNum   =   $order->GeneralInfo->ReferenceNum;
		
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4))); 
			/**************** for tempplate *******************/
			$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				 <meta content="" name="description"/>
				 <meta content="" name="author"/>
				 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
			$html .= '<body>'.$htmlTemplate.'</body>';
			//$html =	$htmlTemplate;
			$name	= $amazon_order_id.'.pdf';							
			$dompdf->load_html(($html), Configure::read('App.encoding'));
			$dompdf->render();			
							
			$file_to_save = WWW_ROOT .'b2b_invoices/'.$name;
			 
			//save the pdf file on the server
			file_put_contents($file_to_save, $dompdf->output()); 
			  
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="'.$name.'"'); 
			header('Content-Length: ' . filesize($file_to_save));
			header("Cache-control: private"); 
			readfile($file_to_save);
			exit;
		
		}else{
			header("Location:".Router::url('/', true)."InvoiceArchive/GenerateB2b/". $amazon_order_id); 
			exit;
			//echo 'Invalid Order.';
		}		 
			
	}
	
	public function GenerateB2Cslip($amazon_order_id = null,$pdf = 0)
    { 
  		$order = $this->getOrderByNumId_openorder( $amazon_order_id );	
  		$htmlTemplate = '';
		if(is_object($order)){
					 
 			$htmlTemplate	=	$this->getB2Chtml($order);	
		 	//$htmlTemplate	=	$this->getB2ChtmlEpson($order);		
			/*$SubSource		=	$order->GeneralInfo->SubSource;
			$ReferenceNum   =   $order->GeneralInfo->ReferenceNum;
		
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4))); 
			/**************** for tempplate *******************/
			if($pdf > 0){
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();	
			 
				//$html =	$htmlTemplate; 
			//	$label 	= $this->getJpLabel(); 
				$cssPath = WWW_ROOT .'css/';
				$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
								 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
								 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
								 <meta content="" name="description"/>
								 <meta content="" name="author"/>';
				
				$html .= '<table width="100%" style="height:1050px;"> 
						<tr><td style="padding-top:20px; padding:5px 0px 5px 5px; height:720px;" colspan="2" valign="top" >'.$htmlTemplate.'</td></tr>
						<tr><td style="padding:2px;valign:bottom;" valign="bottom">'.$label.'</td><td></td></tr> </table>';
						$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				  					
				$name	= $amazon_order_id.'.pdf';							
				$dompdf->load_html(($html), Configure::read('App.encoding'));
				$dompdf->render();			
								
				$file_to_save = WWW_ROOT .'b2b_invoices/'.$name;
				 
				//save the pdf file on the server
				file_put_contents($file_to_save, $dompdf->output()); 
				header('Content-type: application/pdf');
				header('Content-Disposition: inline; filename="'.$name.'"'); 
				header('Content-Length: ' . filesize($file_to_save));
				header("Cache-control: private"); 
				readfile($file_to_save);
				exit;
			}
		}
 		 return $htmlTemplate;
	}
	
	public function GenerateB2CEpson($amazon_order_id = null,$pdf = 0)
    { 
  		$order = $this->getOrderByNumId_openorder( $amazon_order_id );	
  		$htmlTemplate = '';
		if(is_object($order)){
					 
 			//$htmlTemplate	=	$this->getB2Chtml($order);	
		 	$htmlTemplate	=	$this->getB2ChtmlEpson($order);		
			/*$SubSource		=	$order->GeneralInfo->SubSource;
			$ReferenceNum   =   $order->GeneralInfo->ReferenceNum;
		
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4))); 
			/**************** for tempplate *******************/
			if($pdf > 0){
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();	
					 $html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
					 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
					 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
					 <meta content="" name="description"/>
					 <meta content="" name="author"/>
					 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
				echo $html .= '<body>'.$htmlTemplate.'</body>';
					
				echo $name	= $amazon_order_id.'T.pdf';		
				$dompdf->set_paper(array(0, 0, '288', '500' ), 'portrait');					
				$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
				$dompdf->render();			
								
				$file_to_save = WWW_ROOT .'b2b_invoices/invoice_'.$name;
				 
				//save the pdf file on the server
				file_put_contents($file_to_save, $dompdf->output()); 
				
		
				$name	= $amazon_order_id.'.pdf';							
				$dompdf->load_html(($html), Configure::read('App.encoding'));
				$dompdf->render();			
			 
				 
				//save the pdf file on the server
				file_put_contents($file_to_save, $dompdf->output()); 
				header('Content-type: application/pdf');
				header('Content-Disposition: inline; filename="'.$name.'"'); 
				header('Content-Length: ' . filesize($file_to_save));
				header("Cache-control: private"); 
				readfile($file_to_save);
				exit;
			}
		}
 		 return $htmlTemplate;
	}
	
	public function GenerateB2CslipTest($amazon_order_id = null,$pdf = 0)
    { 
  		$order = $this->getOrderByNumId_openorder( $amazon_order_id );	
  		$htmlTemplate = '';
		if(is_object($order)){
					 
 		echo	$htmlTemplate	=	$this->getB2Chtml($order);		
			/*$SubSource		=	$order->GeneralInfo->SubSource;
			$ReferenceNum   =   $order->GeneralInfo->ReferenceNum;
		
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4))); 
			/**************** for tempplate *******************/
			if($pdf > 0){
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();	
			 
				//$html =	$htmlTemplate; 
				$label 	= $this->getJpLabel(); 
				$cssPath = WWW_ROOT .'css/';
				$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
								 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
								 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
								 <meta content="" name="description"/>
								 <meta content="" name="author"/>';
				
				$html .= '<table width="100%" style="height:1050px;"> 
						<tr><td style="padding-top:20px; padding:5px 0px 5px 5px; height:720px;" colspan="2" valign="top" >'.$htmlTemplate.'</td></tr>
						<tr><td style="padding:2px;valign:bottom;" valign="bottom">'.$label.'</td><td></td></tr> </table>';
						$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				  					
				$name	= $amazon_order_id.'.pdf';							
				$dompdf->load_html(($html), Configure::read('App.encoding'));
				$dompdf->render();			
								
				$file_to_save = WWW_ROOT .'b2b_invoices/'.$name;
				 
				//save the pdf file on the server
				file_put_contents($file_to_save, $dompdf->output()); 
				header('Content-type: application/pdf');
				header('Content-Disposition: inline; filename="'.$name.'"'); 
				header('Content-Length: ' . filesize($file_to_save));
				header("Cache-control: private"); 
				readfile($file_to_save);
				exit;
			}
		}
 		 return $htmlTemplate;
	}
	public function getJpLabel( $split_id = '3850387-1' , $ce = 0)
		{ 
 			$r_label = WWW_ROOT .'jerseypost/labels/rotate_'.$split_id.'.jpg';
			if(!file_exists($r_label)){
				$filename = WWW_ROOT.'jerseypost/labels/'.$split_id.'.jpg';
				$degrees = 270;  		
				$source = imagecreatefromjpeg($filename);		
				$rotate = imagerotate($source, $degrees, 0);		
				
				imagejpeg($rotate,$r_label);
				// Free the memory
				imagedestroy($source);
				imagedestroy($rotate); 
			
			}
			$label  = '<img src="'.$r_label .'" width="540px">';
			
 			return $label ; 
			exit;
 		}
	
	public function GenerateB2c($amazon_order_id = null)
    { 
				 
 		$order = $this->getOrderByNumId_openorder( $amazon_order_id );	
		
		if(is_object($order)){
					 
			if($order->GeneralInfo->Source != 'AMAZON'){
				echo 'You can not generate VAT Invoice for <strong>'.$order->GeneralInfo->Source.'</strong>.';
				exit;
			}
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();	
			
			$result 		=	'';
			$htmlTemplate	=	'';	
	
			$htmlTemplate	=	$this->getB2Chtml($order);		
			$SubSource		=	$order->GeneralInfo->SubSource;
			$ReferenceNum   =   $order->GeneralInfo->ReferenceNum;
		
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4))); 
			/**************** for tempplate *******************/
			$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				 <meta content="" name="description"/>
				 <meta content="" name="author"/>
				 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
			$html .= '<body>'.$htmlTemplate.'</body>';
			//echo $html;
			//exit;
			$name	= $amazon_order_id.'.pdf';							
			$dompdf->load_html(($html), Configure::read('App.encoding'));
			$dompdf->render();			
							
			$file_to_save = WWW_ROOT .'b2c_invoices/'.$name;
			 
			//save the pdf file on the server
			file_put_contents($file_to_save, $dompdf->output()); 
			  
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="'.$name.'"'); 
			header('Content-Length: ' . filesize($file_to_save));
			header("Cache-control: private"); 
			readfile($file_to_save);
			exit;
		
		}else{
			header("Location:".Router::url('/', true)."InvoiceArchive/GenerateB2c/". $amazon_order_id); 
			exit;
			//echo 'Invalid Order.';
		}		 
			
	}
	
	public function getB2BInvoice($amazon_order_id = null){ 
				 
 		$order = $this->getOrderByNumId_openorder( $amazon_order_id );	
		   		 
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();	
 		
		$result 		=	'';
		$htmlTemplate	=	'';	

		if(strtotime($order->GeneralInfo->ReceivedDate) > strtotime('2019-09-30 23:59:59')){
			 $htmlTemplate	=	$this->getB2Bhtml($order);	
		}else{
			 $htmlTemplate	=	$this->getB2Bhtml30($order);	
		}
				
		$SubSource		=	$order->GeneralInfo->SubSource;
		$ReferenceNum   =   $order->GeneralInfo->ReferenceNum;
	
		$result			=	ucfirst(strtolower(substr($SubSource, 0, 4)));
		ob_clean();
		/**************** for tempplate *******************/
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
	 	$html .= '<body>'.$htmlTemplate.'</body>';
	 	$html =	$htmlTemplate;
		$name	= $amazon_order_id.'.pdf';							
		$dompdf->load_html(($html), Configure::read('App.encoding'));
		$dompdf->render();			
						
		$file_to_save = WWW_ROOT .'b2b_invoices/'.$name;
		 
		//save the pdf file on the server
		file_put_contents($file_to_save, $dompdf->output()); 
		//copy($file_to_save,WWW_ROOT .'b2b_invoices/26092019/'.$amazon_order_id.'.pdf');
		
  		//exit;		 
			
	}
	
	public function getEuropeOrderByNumId($amazon_order_id = null)
	{
		$this->loadModel('FbaEuOrder');	 
		$this->loadModel('FbaEuOrderItem');	
		$data 	= [];
		$order 	= $this->FbaEuOrder->find('first', array('conditions'=>array('amazon_order_id' => $amazon_order_id)));
		if(count($order ) > 0)
		{
 			$data['NumOrderId']    = substr($amazon_order_id,-7);
			$general_info['ReferenceNum'] = $order['FbaEuOrder']['amazon_order_id'];
			$general_info['Source'] = $order['FbaEuOrder']['sales_channel'];
			$general_info['SubSource'] = 'not_found';
			$general_info['ReceivedDate'] = $order['FbaEuOrder']['purchase_date']; 
			$general_info['DespatchByDate'] = $order['FbaEuOrder']['earliest_ship_date'];
			
			$customer_info['ChannelBuyerName'] = $order['FbaEuOrder']['buyer_name'];
			$customer_info['Address']['EmailAddress'] = $order['FbaEuOrder']['buyer_email'];
			$customer_info['Address']['Address1'] = $order['FbaEuOrder']['address_line_1'];
			$customer_info['Address']['Address2'] = $order['FbaEuOrder']['address_line_2'];
			$customer_info['Address']['Address3'] ='';
			$customer_info['Address']['Town'] = $order['FbaEuOrder']['city'];			
			$customer_info['Address']['Region'] = $order['FbaEuOrder']['region'];
			$customer_info['Address']['PostCode'] = $order['FbaEuOrder']['post_code'];
			$customer_info['Address']['Country'] = $order['FbaEuOrder']['country_code'];
			$customer_info['Address']['FullName'] = $order['FbaEuOrder']['name'];
			$customer_info['Address']['PhoneNumber'] = '';
			
			$customer_info['BillingAddress']['EmailAddress'] = $order['FbaEuOrder']['buyer_email'];
			$customer_info['BillingAddress']['Address1'] = $order['FbaEuOrder']['bill_address_1'];
			$customer_info['BillingAddress']['Address2'] = $order['FbaEuOrder']['bill_address_2'];
			$customer_info['BillingAddress']['Address3'] = $order['FbaEuOrder']['bill_address_3'];
			$customer_info['BillingAddress']['Town'] = $order['FbaEuOrder']['bill_city'];			
			$customer_info['BillingAddress']['Region'] = $order['FbaEuOrder']['bill_state'];
			$customer_info['BillingAddress']['PostCode'] = $order['FbaEuOrder']['bill_postal_code'];
			$customer_info['BillingAddress']['Country'] = $order['FbaEuOrder']['bill_country'];
			$customer_info['BillingAddress']['FullName'] = $order['FbaEuOrder']['name'];
			$customer_info['BillingAddress']['PhoneNumber'] = $order['FbaEuOrder']['buyer_phone_number'];
		 
			//pr($order['FbaEuOrderItem']);
			
			$sub_total = 0; $shipping_price = 0; $item_tax = 0; $shipping_promotion = 0;
			$gift_wrap = 0; $item_promotion = 0; $currency = '';
			foreach($order['FbaEuOrderItem'] as $i => $v){ 
				
				$items[$i]['ItemId'] = $v['order_item_id'];
				$items[$i]['ItemNumber'] =  $v['order_item_id'];
				$items[$i]['SKU'] = $v['master_sku'];
				$items[$i]['ItemSource'] = 'AMAZON';
				$items[$i]['Title'] = $v['title'];
				$items[$i]['Quantity'] = $v['quantity_shipped'];
				$items[$i]['CategoryName'] = 'Default';
				$items[$i]['PricePerUnit'] = $v['item_price'];
				$items[$i]['item_tax'] = $v['item_tax'];
				$items[$i]['shipping_price'] = $v['shipping_price'];
				$items[$i]['shipping_discount'] = $v['shipping_discount'];
				$items[$i]['item_promotion_discount'] = $v['item_promotion_discount'];
				$items[$i]['gift_wrap_price'] = $v['gift_wrap_price'];
				$items[$i]['gift_wrap_tax'] = $v['gift_wrap_tax'];		
 				$items[$i]['UnitCost'] = 0;
				$items[$i]['Discount'] = '0';
				$items[$i]['Tax'] = '0';
				$items[$i]['Cost'] = $v['item_price'];
				$items[$i]['CostIncTax'] = $v['item_price']  ;	
				$items[$i]['ChannelSKU'] = $v['seller_sku'];
				$items[$i]['ChannelTitle'] = $v['title'];
				if(strpos($v['seller_sku'],"FREURAFBA") !== false){
					$general_info['SubSource'] = 'CostBreaker_FRFBA';
				}else if(strpos($v['seller_sku'],"ESEURAFBA") !== false){
					$general_info['SubSource'] = 'CostBreaker_ESFBA';
				}else if(strpos($v['seller_sku'],"DEEURAFBA") !== false){
					$general_info['SubSource'] = 'CostBreaker_DEFBA';
				}else if(strpos($v['seller_sku'],"ITEURAFBA") !== false){
					$general_info['SubSource'] = 'CostBreaker_ITFBA';
				}else if(strpos($v['seller_sku'],"UKEURAFBA") !== false){
					$general_info['SubSource'] = 'CostBreaker_UKFBA';
				}
				
				else if(strpos($v['seller_sku'],"MRDEFBA") !== false){
					$general_info['SubSource'] = 'Marec_DEFBA';
				}else if(strpos($v['seller_sku'],"MRUKFBA") !== false){
					$general_info['SubSource'] = 'Marec_UKFBA';
				}else if(strpos($v['seller_sku'],"MRITFBA") !== false){
					$general_info['SubSource'] = 'Marec_ITFBA';
				}else if(strpos($v['seller_sku'],"MRFRFBA") !== false){
					$general_info['SubSource'] = 'Marec_FRFBA';
				}
				
				else if(strpos($v['seller_sku'],"RAINUKFBA") !== false){
					$general_info['SubSource'] = 'Rainbow_UK_FBA';
				} 
 				
				$currency = $v['currency_code']; 
				$sub_total += $v['item_price'];
				$shipping_price += $v['shipping_price'];
				$item_tax += $v['item_tax'];
				$gift_wrap += ($v['gift_wrap_price'] + $v['gift_wrap_tax']);
				$item_promotion += $v['item_promotion_discount'];
				$shipping_promotion += $v['shipping_discount'];
			}
	
  		
		$totals_info['Subtotal'] = $sub_total;
		$totals_info['PostageCost'] = $shipping_price;
		$totals_info['Tax'] = $item_tax;		 
		$totals_info['TotalCharge'] = $sub_total + $shipping_price + $item_tax + $gift_wrap + $shipping_promotion + $item_promotion;
		$totals_info['TotalDiscount'] = '0';
		$totals_info['Currency'] = $currency;
		$totals_info['CountryTaxRate'] = '0';
		$totals_info['gift_wrap'] = $gift_wrap;
		$totals_info['shipping_promotion'] = $shipping_promotion;
		$totals_info['item_promotion'] = $item_promotion;
		
		
		$data['GeneralInfo']   = json_decode(json_encode($general_info));
		$data['CustomerInfo']   = json_decode(json_encode($customer_info));
		$data['TotalsInfo']   = json_decode(json_encode($totals_info));
		$data['Items']   = json_decode(json_encode($items));
		
 		} 
		/*pr($data);
		exit;*/
		return json_decode(json_encode( $data ),0);	
		
	}
	
	 
	public function getEuFBAInvoice($order_id = null, $option = null){
	
   		$order = $this->getEuropeOrderByNumId( $order_id );	
  		 
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();	
 		
		$result 		=	'';
		$htmlTemplate	=	''; 

		$htmlTemplate	=	$this->getFbaEuhtml($order);		
		$SubSource		=	$order->GeneralInfo->SubSource;
	
		$result			=	ucfirst(strtolower(substr($SubSource, 0, 4)));
		ob_clean();
		/**************** for tempplate *******************/
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
	 	$html .= '<body>'.$htmlTemplate.'</body>';
	 	$html =	$htmlTemplate;
		$name	= $order_id.'.pdf';							
		$dompdf->load_html(($html), Configure::read('App.encoding'));
		$dompdf->render();			
						
		$file_to_save = WWW_ROOT .'fba_eu_invoice/'.$name;
		 
		//save the pdf file on the server
		file_put_contents($file_to_save, $dompdf->output()); 
		

		if( $option != 'cron'){
			header("Location:". Router::url('/', true).'fba_eu_invoice/'.$name);
 			exit;	
		}	 
			
	}
	
	public function getOrderByNumId_openorder($pkOrderId = null)
	{
		$this->loadModel('OpenOrder');	
		$data 	= [];
		$order 	= $this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id' => $pkOrderId)));
		
		if(count($order ) == 0)
		{
			$order 	= $this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.amazon_order_id' => $pkOrderId)));
		} 
		
		if(count($order ) > 0)
		{
			$data['NumOrderId']    = $order['OpenOrder']['num_order_id'];
			$data['GeneralInfo']   = unserialize($order['OpenOrder']['general_info']);
			$data['ShippingInfo']  = unserialize($order['OpenOrder']['shipping_info']);
			$data['CustomerInfo']  = unserialize($order['OpenOrder']['customer_info']);
			$data['TotalsInfo']    = unserialize($order['OpenOrder']['totals_info']);
			$data['Items'] 		   = unserialize($order['OpenOrder']['items']);
			$data['vat_number']    = $order['OpenOrder']['vat_number']; 
			$data['buyer_vat_number']= $order['OpenOrder']['buyer_vat_number']; 
			$data['invoice_id']		= $order['OpenOrder']['invoice_id']; 
			$data['credit_note']	= $order['OpenOrder']['credit_note_id']; 
			$data['ioss_no']	    = $order['OpenOrder']['ioss_no']; 
			 
			if(isset($data['CustomerInfo']->BillingAddress) && count($data['CustomerInfo']->BillingAddress) > 0){
				
			}else{
				$data['CustomerInfo']->BillingAddress  = $data['CustomerInfo']->Address;
			}		 
 		} 
		return json_decode(json_encode( $data ),0);	
		
	}
		
	public function getOrderByNumId($pkOrderId = null)
	{
		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
	
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
		//$multi = AuthMethods::Multilogin($username, $password);		
		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

		$token = $auth->Token;	
		$server = $auth->Server;		
		$order	= OrdersMethods::GetOrderDetailsByNumOrderId($pkOrderId,$token, $server);
		//pr($order	);
		//exit;
		return $order;	
		
	}
 
  	 
  	public function sendEmail( $data = array() ){ 
 			 
			//$to = $data['to'];	
			$from_email = $data['from_email'];
			$to = 'avadhesh.kumar@jijgroup.com'; 
			 
			$_path =  WWW_ROOT .'b2b_invoices/'. $data['file'];   
	   
			App::uses('CakeEmail', 'Network/Email');
			$email = new CakeEmail('');
			$email->from($from_email);	
			$email->to( array('avadhesh.kumar@jijgroup.com'));
			//$email->to( array('avadhesh.kumar@jijgroup.com','pappu.k@euracogroup.co.uk'));
			$email->subject($data['subject']);
			$email->attachments(array(
				    $data['file'] => array(
					'file' => $_path,
					'mimetype' => 'application/pdf',
					'contentId' => 'Euraco'
				)
			));
			
			$email->send( (utf8_decode($data['message'])));
		
			return $from_email;
				
			 
	}
	
 	
    	public function getFbaEuhtml($order = null){ 
	
 		$this->loadModel('ProductHscode'); 
		$this->loadModel('MergeUpdate');
		$this->loadModel('SourceList');	
		$this->loadModel('Country');	
		$this->loadModel('Product');	
		
		 
		$SubSource		  = $order->GeneralInfo->SubSource;
 		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  		 		
			 	
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		$BILLING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName));
		$SHIPPING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName));
		
	 	$Country  = ''; $is_european_country = 0;
		$BillinCountryCode  =  strtolower($order->CustomerInfo->BillingAddress->Country);
		$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $order->CustomerInfo->BillingAddress->Country],'fields'=>['name','country_group']]);
		if(count( $cresult ) > 0){	
			$BillinCountry  =  $cresult['Country']['name'];
			
			if(in_array($cresult['Country']['name'],['Switzerland','Bosnia and Herzegovina','Kosovo','North Macedonia'])){		
				$is_european_country++;
			}else if(in_array($cresult['Country']['country_group'],['rest_of_europe','rest_of_eu','united kingdom','france','italy','germany','spain'])){
				$is_european_country++;
			} 
		}
		
		$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $order->CustomerInfo->Address->Country],'fields'=>['name']]);
		if(count( $cresult ) > 0){	
			$Country  =  $cresult['Country']['name'];
		}
		
		if($BILLING_ADDRESS == ''){
			
 			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS  = ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$BillinCountry;
			}
		}	
		if($SHIPPING_ADDRESS == ''){
 			
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS = ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $Country;
			}			
		}		
		
		$EmailAddress	  = $order->CustomerInfo->Address->EmailAddress;
		$exp = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = $exp[$index];
		  		
		$country_code	  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 
		$ReferenceNum	  = $order->GeneralInfo->ReferenceNum; 
 		$GIFT_WRAP		  = $order->TotalsInfo->gift_wrap;
 		$ITEM_PROMOTION	  = $order->TotalsInfo->item_promotion;
		$SHIP_PROMOTION	  = $order->TotalsInfo->shipping_promotion;
		
 		$ORDER_DATE	  	 = date('d-m-Y', strtotime($order->GeneralInfo->ReceivedDate));
		$INVOICE_NO	 	 = 'INV-'.$OrderId ;
		$INVOICE_DATE 	 = $ORDER_DATE;//date('d-m-Y');
		
 		$templateHtml 	  = NULL;
  		$tax_rate = 0; 	  $vat_amount = 0;
  		$currency_code = $this->getCurrencyCode($Currency);
		
		$VAT_Number = '';
		if($result == 'cost'){
			$EORI_Number = 'GB019020854000';
			$UK_VAT_Number = 'GB 304984295'; 
			$DE_VAT_Number = 'DE 321777974'; 
			$FR_VAT_Number = 'FR 59850070509'; 
			$IT_VAT_Number = 'IT 10905930961'; 
			$ES_VAT_Number = 'ES N6061518D'; 
			$CZ_VAT_Number = 'CZ 684972917';
			
			$STORE_NAME	   = $this->COST_STORE_NAME;
		    $STORE_ADDRESS = $this->COST_STORE_ADDRESS; 			 
		}
		
		if($result == 'mare'){
			$EORI_Number = 'GB019434034000';
			$UK_VAT_Number = 'GB 307817693'; 
			$DE_VAT_Number = 'DE 323086773';
			$IT_VAT_Number = 'IT 11062310963'; // 16 Dec 2019
			$FR_VAT_Number = 'FR 07879900934';  
		
			$STORE_NAME	   = $this->MARE_STORE_NAME;
		    $STORE_ADDRESS = $this->MARE_STORE_ADDRESS;
  		}	
		
		if(in_array($result,['rain','rrre'])){
 			$EORI_Number = 'GB318649182000';
			$UK_VAT_Number = 'GB 318649182'; 
			$IT_VAT_Number = 'IT 11061000961';
			$DE_VAT_Number = 'DE 327173988';
			$FR_VAT_Number = '';			
		  
			$STORE_NAME	   = $this->RAIN_STORE_NAME;
		    $STORE_ADDRESS = $this->RAIN_STORE_ADDRESS;
 		}	 
		 
 		if($store_country == 'gb' || $store_country == 'uk'){  
			$VAT_Number   = $UK_VAT_Number;
		}else if($store_country == 'de'){
			$VAT_Number   = $DE_VAT_Number; 
		}else if($store_country == 'fr'){
			$VAT_Number   = $FR_VAT_Number;  
		}else if($store_country == 'it'){
			$VAT_Number   = $IT_VAT_Number; 			 
		}else if($store_country == 'es'){
			$VAT_Number   = $ES_VAT_Number; 			 
		}
		 
		$txt = '';
		if($is_european_country > 0){
			$txt = 'Vatable Transaction'; 
		}else{
			$txt = 'Non Taxable Supply (Intra Community Transaction)'; 
		} 
 
  		$templateHtml  ='<head><meta charset="UTF-8"><title>Invoice</title></head>';
		$templateHtml  .='<style>*{font-family:arial; font-size:12px;}</style>';
		$templateHtml  .='<div style="height:950px;margin-bottom:10px;">';						   
		$templateHtml  .='<table border="0" width="700px" align="center" cellpadding="0" cellspacing="0" style="margin:0px auto;">';
		$templateHtml  .='<tbody>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> Invoice </td>';
		$templateHtml  .='  </tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td valign="middle" align="left" width="50%">';
				$templateHtml  .='<p style="font-size:18px ;font-weight:bold;">'.$BILLING_ADDRESS_NAME.'</p>';
				$templateHtml  .='<p>'.$BILLING_ADDRESS.'</p>'; 
 			$templateHtml  .='</td>';
			$templateHtml  .='<td valign="top" align="center" style="background:#dfeced; padding:10px;">';
			  $templateHtml  .='<div  style="border:1px solid #bdc9c9; padding:5px;">';
				$templateHtml  .='<table>';
				  
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td style="font-size:30px ;font-weight:bold;"> Paid</td>';
					 $templateHtml  .='</tr>';
					$templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2">';
						$templateHtml  .='<p>Sold by <strong> '.$STORE_NAME.'</strong></p>';
						if($VAT_Number){
							$templateHtml  .='<p>VAT #<strong>&nbsp;&nbsp;&nbsp;&nbsp;'.$VAT_Number.'</strong></p>'; 
						}
 						 
						$templateHtml  .='</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2" width="100%" height="1px" style="width:100%; height:1px; background:#bdc9c9;"> </td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong>Invoice date / Delivery date</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_DATE.'</td>';
					 $templateHtml  .='</tr>';
					  $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong><strong>Invoice #</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_NO.'</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong><strong>Total payable</strong></td>';
						$templateHtml  .='<td> '.$currency_code.number_format($TOTAL,2).'</td>';
					 $templateHtml  .='</tr>'; 
				  
			   $templateHtml  .='</table>';
			   $templateHtml  .='</div>';
			   
			$templateHtml  .='</td>';
		 $templateHtml  .='</tr>';
		 
		 if($exp[$index] == 'fr'){
			 $amazon_url = 'www.amazon.fr';
		 }else if($exp[$index] == 'es'){
			 $amazon_url = 'www.amazon.es';
		 }else if($exp[$index] == 'it'){
			 $amazon_url = 'www.amazon.it';
		 }else if($exp[$index] == 'de'){
			 $amazon_url = 'www.amazon.de';
		 }else{
			 $amazon_url = 'www.amazon.co.uk';
		 }
		 
		 $templateHtml  .='<tr>';
		/*	$templateHtml  .='<td colspan="2" style="padding:10px 0px;"> For questions about your order, visit <a href="https://'.$amazon_url.'/contact-us">'.$amazon_url.'/contact-us</a></td>';*/
			$templateHtml  .='<td colspan="2" style="padding:10px 0px;"> For questions about your order, visit '.$amazon_url.'/contact-us</td>';
		 $templateHtml  .='</tr>';
		
		
		  $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="padding:5px 0px;"><strong>Tranction Type:</strong>'.$txt.'</td>';
		 $templateHtml  .='</tr>';
		 
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="background:#ddd; height:2px;"> </td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="height:10px;"> </td>';
		 $templateHtml  .='</tr>'; 
		 
		 $templateHtml  .='<tr><td colspan="2">';
		 $templateHtml  .='<table border="0" width="700px" align="center" cellpadding="0" cellspacing="0" >';
			
				$templateHtml  .='<tr>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">BILLING ADDRESS </th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">DELIVERY ADDRESS</th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">SOLD BY </th>';
			   $templateHtml  .='</tr>'; 
			   
			  $templateHtml  .='<tr>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$BILLING_ADDRESS_NAME.'</strong><br>'.$BILLING_ADDRESS.'</p>'; 
				  $templateHtml  .='</td>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$SHIPPING_ADDRESS_NAME.'</strong><br>'.$SHIPPING_ADDRESS.'</p>';
				  $templateHtml  .='</td>';
				 $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$STORE_NAME.'</strong><br>'.$STORE_ADDRESS;
					 if($VAT_Number){
						 $templateHtml  .='<br><strong>'.$VAT_Number.'</strong>';
					 }
					 $templateHtml  .='</p>';
					  
				  $templateHtml  .='</td>'; 
			  $templateHtml  .='</tr>';
				$templateHtml  .='<tr><td colspan="3" style="background:#ddd; height:1px;"> </td></tr>';
				$templateHtml  .='<tr>';
				   $templateHtml  .='<td colspan="3">';
					 $templateHtml  .='<p><strong>Order information</strong></p>';
					 $templateHtml  .='<p>Order date '.$ORDER_DATE.'';
					 $templateHtml  .='<br>Order # '.$ReferenceNum.'</p>';
				   $templateHtml  .='</td>';
				$templateHtml  .='</tr>';
		  
		  
		 $templateHtml  .='</table>';
		 $templateHtml  .='</td></tr>'; 
		$templateHtml  .='</table>';
		$templateHtml  .='</div>';
		
		$templateHtml  .='<div style="height:950px;margin-bottom:-1px;padding-top:100px;">';
		$templateHtml  .='<table width="700px" cellpadding="0" cellspacing="0" align="center">';
		$templateHtml  .='<tbody>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="5"></td>';
			$templateHtml  .='<td  style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> &nbsp; </td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="5"></td>';
			$templateHtml  .='<td  style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> Invoice</td>';
		 $templateHtml  .='</tr>';
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="4"></td>';
			$templateHtml  .='<td  colspan="2" style="font-weight:bold; padding:7px 5px; text-align:right"> Invoice # '.$INVOICE_NO.'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="6" style="font-size:24px; font-weight:bold; border-top:2px solid #ddd; border-bottom:2px solid #ddd; padding:7px 5px;"> Invoice Details</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<th valign="top" style="text-align:left; padding:10px 5px;" width="50%"> Description </th>';
			$templateHtml  .='<th valign="top" style="text-align:left; padding:10px 5px;" width="5%"> Qty </th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="10%"> Unit price<br>(excl. VAT)</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="11%"> VAT rate</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="12%"> Unit price<br> (incl. VAT)</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="12%"> Item subtotal<br> (incl. VAT)</th>';
		 $templateHtml  .='</tr>';
		 
		$total_item_vat_amount = 0;
		$total_item_cost 	   = 0; 
 		$tax_rate = 0;
		
		if($store_country == 'uk'){
			$store_country = 'gb';
		} 
		
		if($is_european_country > 0 ){
			$shipping_per_item = $POSTAGE / count($items); 
			$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $store_country)));
			if(count( $thsresult ) > 0){				
				$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
			}
		} 
		
		foreach($items as $count => $item){   
	
  			 $PricePerUnit = ($item->PricePerUnit + $item->item_tax) ;
			 
			 $item_vat_amount = $PricePerUnit - ($PricePerUnit/(1 + ($tax_rate/100)));
			 $item_cost = $PricePerUnit - $item_vat_amount;
			
			 $total_item_vat_amount  +=$item_vat_amount;
			 $total_item_cost  += $item_cost ;
			 
			 $per_unit_cost  = $item_cost / $item->Quantity;
			 $per_unit_price  = $PricePerUnit / $item->Quantity;
		
 			$templateHtml  .='<tr style="background:#ddd;">';
				$templateHtml  .='<td style="padding:10px 5px;">'.$item->Title.'</td>';
				$templateHtml  .='<td style="padding:10px 5px;">'.$item->Quantity.'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($per_unit_cost,2).'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($per_unit_price,2).'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($item->CostIncTax,2).'</td>';
			 $templateHtml  .='</tr>';
			 
		 } 
  	 
 		 $postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
 		 $postage_cost		 = $POSTAGE - $postage_vat_amount;
		 
		 $templateHtml  .='<tr style="background:#ddd;">';

			$templateHtml  .='<td style="padding:10px 5px;">Shipping Charge</td>';
			$templateHtml  .='<td style="padding:10px 5px;"></td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($postage_cost,2).' </td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
		 $templateHtml  .='</tr>';
 		 
 		  $gift_vat_amount  = $GIFT_WRAP - ($GIFT_WRAP/(1 + ($tax_rate/100)));
 		  $gift_cost		= $GIFT_WRAP - $gift_vat_amount; 
		 
		  $templateHtml  .='<tr style="background:#ddd;">';

			$templateHtml  .='<td style="padding:10px 5px;">Gift Wrap</td>';
			$templateHtml  .='<td style="padding:10px 5px;"></td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($gift_cost,2).' </td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($GIFT_WRAP,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($GIFT_WRAP,2).'</td>';
		 $templateHtml  .='</tr>';
		 
		  $ip_vat_amount  = $ITEM_PROMOTION - ($ITEM_PROMOTION/(1 + ($tax_rate/100)));
 		  $ip_cost		= $ITEM_PROMOTION - $ip_vat_amount; 
		 
		  $templateHtml  .='<tr style="background:#ddd;">';
 			$templateHtml  .='<td style="padding:10px 5px;">Item Promotion</td>';
			$templateHtml  .='<td style="padding:10px 5px;"></td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($ip_cost,2).' </td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($ITEM_PROMOTION,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($ITEM_PROMOTION,2).'</td>';
		 $templateHtml  .='</tr>';
		 
		 $sp_vat_amount  = $SHIP_PROMOTION - ($SHIP_PROMOTION/(1 + ($tax_rate/100)));
 		 $sp_cost		 = $SHIP_PROMOTION - $sp_vat_amount; 
		 
		  $templateHtml  .='<tr style="background:#ddd;">';
 			$templateHtml  .='<td style="padding:10px 5px;">Shipping Promotion</td>';
			$templateHtml  .='<td style="padding:10px 5px;"></td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($sp_cost,2).' </td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($SHIP_PROMOTION,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($SHIP_PROMOTION,2).'</td>';
		 $templateHtml  .='</tr>';
		
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			//$templateHtml  .='<td></td>';
			$templateHtml  .='<td colspan="4"><table width="100%"><tr><td style="font-size:24px; font-weight:bold;" align="left">
			Invoice total</td><td style="font-size:24px; font-weight:bold;"align="right"> '.$currency_code.'&nbsp;'. number_format($TOTAL,2) .'</td></tr></table></td>';
			
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';		 
			$templateHtml  .='<td colspan="4" style="height:2px; background:#ddd;"></td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="6" height="10px;"></td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th style="text-align:left; font-weight:bold; padding:10px 5px;">VAT rate </th>';
			$templateHtml  .='<th style="font-weight:bold; padding:10px 5px;" align="right">Item subtotal<br>(excl. VAT)</th>';
			$templateHtml  .='<th style="font-weight:bold; padding:10px 5px;" align="right">VAT subtotal</th>';
		 $templateHtml  .='</tr>';
		 
		 $total_item_vat_amount = $total_item_vat_amount + $postage_vat_amount  + $gift_vat_amount + $ip_vat_amount + $sp_vat_amount;
		 $total_item_cost  = $total_item_cost + $postage_cost + $gift_cost + $ip_cost + $sp_cost ;
			 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px"> '.$tax_rate.'%</td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($total_item_cost,2).'</td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($total_item_vat_amount,2).'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td style="padding:10px 5px; font-weight:bold;">Total</td>';
			$templateHtml  .='<td style="padding:10px 5px; font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format($total_item_cost,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;  font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format($total_item_vat_amount ,2).'</td>';
		 $templateHtml  .='</tr>';
		$templateHtml  .='</tbody>';
		$templateHtml  .='</table>';
		$templateHtml  .='</div>';
		$templateHtml  .='</body>';
		 
 		return $templateHtml;	
	
 	}
	
	public function getInvoiceId($company = '')
     { 
			$this->loadModel('InvoiceCreditnoteNo');
 			$new_invoice_no = '';      
			$list = $this->InvoiceCreditnoteNo->find('first', array('conditions' => array('company'=>strtoupper($company)), 'fields' => array('id','invoice_start_id','invoice_id') ) );
			if(count($list) > 0){
				$id = $list['InvoiceCreditnoteNo']['id'];
 				if(empty($list['InvoiceCreditnoteNo']['invoice_id'])){
					$new_invoice_no = $list['InvoiceCreditnoteNo']['invoice_start_id'] + 1;
				}else{
					$new_invoice_no = $list['InvoiceCreditnoteNo']['invoice_id'] + 1;
 				}
				$this->InvoiceCreditnoteNo->query("UPDATE `invoice_creditnote_nos` SET `invoice_id` = '".$new_invoice_no."' WHERE `id` = {$id}"); 
			}
			
			return $new_invoice_no;
     }
	 
 	public function getB2Chtml($order = null){ 
	
 		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdate');
		$this->loadModel('OpenOrder');
		$this->loadModel('SourceList');	
		$this->loadModel('Country');	
		$this->loadModel('Product');	
		$this->loadModel('OrderLocation');
		$this->loadModel('PurchaseOrder');
		$this->loadModel('ProductBarcode');	 
		$this->loadModel('ResponsiblePersonDetail' );
		 
		$Source		  	  = $order->GeneralInfo->Source;
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  	
 		$saved_vat_number = $order->vat_number;  	 		
		$saved_invoice_id = $order->invoice_id;  
		$ioss_no		  = $order->ioss_no;  
		$buyer_vat_no     = $order->buyer_vat_number;	 		
			
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		$BILLING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName));
		$SHIPPING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName));
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
 			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS  = ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if(isset($order->CustomerInfo->BillingAddress->Address2) && $order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if(isset($order->CustomerInfo->BillingAddress->Address3) && $order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode));
			} 
			if(isset($order->CustomerInfo->BillingAddress->Region) && $order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ', '.ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region));
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
 				$BILLING_ADDRESS .=	$order->CustomerInfo->BillingAddress->Country ? '<br>'.$order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
 			
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS = ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode));
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ', '.ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region));
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
 				$SHIPPING_ADDRESS .=  $order->CustomerInfo->Address->Country ? '<br>'.$order->CustomerInfo->Address->Country : '';
			}			
		}		
		 
		$EmailAddress	  = $order->CustomerInfo->Address->EmailAddress;
		$exp   = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = $exp[$index];
		$country_code   =	$store_country;
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$_POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 
		$ReferenceNum	  = $order->GeneralInfo->ReferenceNum; 
 			
		$ORDER_DATE	  	 = date('d-m-Y', strtotime($order->GeneralInfo->ReceivedDate));
		$INVOICE_NO	 	 = 'INV-'.$OrderId ;
		$INVOICE_DATE 	 = $ORDER_DATE;//date('d-m-Y');
 		
		$templateHtml 	  = NULL;
   
		//echo "</pre>";
		$item_lvcr_limit  = '21.9'; //EUR
		$lvcr_limit_shipping  = '150';		
		$POSTAGE = $_POSTAGE;
 		
		 
 		if(empty($country_code) || $country_code == 'com'){
		 	$country_code = $this->getCountryCode($Country);
		}		 
 		$store_country = [];
		$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $country_code],'fields'=>['name','custom_name']]);
		if(count( $cresult ) > 0){	
			$store_country  = [$cresult['Country']['name'], $cresult['Country']['custom_name']];
		}else{
		 	$country_code = $this->getCountryCode($Country);
			$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $country_code],'fields'=>['name','custom_name']]);
			if(count( $cresult ) > 0){	
				$store_country  = [$cresult['Country']['name'], $cresult['Country']['custom_name']];
			}
		} 	
			 
		
		$tax_rate = 20; $vat_amount = 0;
		 
 		$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
		if(count( $thsresult ) > 0){				
			$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
		} 
		$currency_code = $this->getCurrencyCode($Currency);
		 
		$VAT_Number = ''; $company = ''; $IOSS_Number = 'IOSS:123456789';
		
 		if(in_array(strtolower($SubSource),['costbreaker_uk','costbreaker_de','costbreaker_es','costbreaker_fr','costbreaker_it','costbreaker_nl','marec_uk','marec_de','marec_es','marec_fr','marec_it','marec_nl','rainbow retail','rainbow retail de','rainbow_retail_fr','rainbow_retail_it'])){
			$IOSS_Number = 'IOSS:IM4420001201';//Amazon Europe 			
		}else if(strtolower($SubSource) == 'costbreaker_usabuyer'){
			/* 06-AUG-2021  Hi Avadhesh Ji,
			For USA and CANADA can we make the changes like this  - 
			1) Orders sold on USA store should have no IOSS no on Label and API
			2) Orders for USA but sold in Europe  we can use the IOSS no in Order*/
			$IOSS_Number = '';
			$euc = $this->Country->find('first',['conditions' => ['OR'=>['name' => $Country,'custom_name' => $Country],'eu_country' => 1],'fields'=>['name']]);
			if(count($euc) > 0){
				$IOSS_Number = 'IOSS:IM4420001234';//Amazon US 	
			}		
		}else if(strtolower($SubSource) == 'costbreaker_ca'){
			$IOSS_Number = '';
			$euc = $this->Country->find('first',['conditions' => ['OR'=>['name' => $Country,'custom_name' => $Country],'eu_country' => 1],'fields'=>['name']]);
			if(count($euc) > 0){
				$IOSS_Number = 'IOSS:IM4420001223';//Amazon Canada  
			}			
		}else if(strtolower($SubSource) == 'costdropper'){
			$IOSS_Number = 'IOSS:IM2760000924';//Real DE				
		}else if(strtolower($SubSource) == 'cdiscount' || strtolower($SubSource) == 'costbreaker'){
			$IOSS_Number = 'IOSS:IM2500000295';
 		}
		
 		if($result == 'cost' || in_array( strtolower($SubSource),['ebay2','onbuy'])){ 
			$company 	=  'CostBreaker'; 
			$company_name = 'EURACO';
			$EORI_Number = 'GB019020854000';
			$UK_VAT_Number = 'GB 304984295'; 
			$DE_VAT_Number = 'DE 321777974'; 
			$FR_VAT_Number = 'FR 59850070509'; 
			$IT_VAT_Number = 'IT 10905930961'; 
			$CZ_VAT_Number = 'CZ 684972917';  
			$ES_VAT_Number = 'ES N6061518D';

 			if(strtolower($SubSource) == 'costdropper'){
				$IOSS_Number = 'IOSS:IM2760000924';//Real DE				
			} 
			if(strtolower($SubSource) == 'cdiscount' || strtolower($SubSource) == 'costbreaker'){
				$IOSS_Number = 'IOSS:IM2500000295';
				$FR_VAT_Number = 'FR 34424059822';
			}

			$STORE_NAME	   = $this->COST_STORE_NAME;
		    $STORE_ADDRESS = $this->COST_STORE_ADDRESS; 
			$input = array();
			if($Country == 'United Kingdom'){
 				$VAT_Number   = $UK_VAT_Number;
  			}else if($Country == 'Germany'){
				$VAT_Number   = $DE_VAT_Number;
			}else if($Country == 'France'){
				$VAT_Number   = $FR_VAT_Number;
			}else if($Country == 'Italy'){
				$VAT_Number   = $IT_VAT_Number;
 			}else if($Country == 'Spain'){
				$VAT_Number   = $ES_VAT_Number;
 			}else{
 				$input = array($UK_VAT_Number,$FR_VAT_Number,$CZ_VAT_Number);
			}
			if(count($input) > 0){
 				  $rand_keys 	  = array_rand($input);
 				  $VAT_Number     = $input[$rand_keys]; 
			}
 		}
		
		if($result == 'mare' || strtolower($SubSource) == 'ebay5'){
			$company 	=  'Marec';  
			$company_name = 'ESL';
			$EORI_Number = 'GB019434034000';
			$UK_VAT_Number = 'GB 307817693'; 
			$DE_VAT_Number = 'DE 323086773';
			$IT_VAT_Number = 'IT 11062310963'; // 16 Dec 2019
			$FR_VAT_Number = 'FR 07879900934'; 
		
			$STORE_NAME	   = $this->MARE_STORE_NAME;
		    $STORE_ADDRESS = $this->MARE_STORE_ADDRESS;
			$input = array();
			if($Country == 'United Kingdom'){
 				$VAT_Number = $UK_VAT_Number;	
 			}else if($Country == 'Germany'){
				$VAT_Number = $DE_VAT_Number;	
			}else if($Country == 'France'){
 				$VAT_Number = $FR_VAT_Number;	
 			}else if($Country == 'Italy'){
 				$VAT_Number = $IT_VAT_Number;	
  			}else if($Country == 'Spain'){
 				$VAT_Number   = $UK_VAT_Number;
  			}else{
				$input = array($UK_VAT_Number,$DE_VAT_Number);
			}
			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}			 
		}	
 		if(in_array($result,['rain','rrre']) || strtolower($SubSource) == 'ebay0'){
 			$company 	= 'Rainbow Retail'; 
			$company_name = 'FRESHER';
			$EORI_Number = 'GB318649182000';
			$UK_VAT_Number = 'GB 318649182'; 
			$IT_VAT_Number = 'IT 11061000961';
			$DE_VAT_Number = 'DE 327173988';
			$FR_VAT_Number = '';
		  
			$STORE_NAME	   = $this->RAIN_STORE_NAME;
		    $STORE_ADDRESS = $this->RAIN_STORE_ADDRESS;
			
			$input = array();
			if($Country == 'United Kingdom'){
 				$VAT_Number = $UK_VAT_Number;	
 			}else if($Country == 'Germany'){
 				$VAT_Number = $DE_VAT_Number;	
			}else if($Country == 'France'){
				$VAT_Number = $UK_VAT_Number;	
			}else if($Country == 'Italy' ){
 				$VAT_Number = $IT_VAT_Number;	
  			}else{
				$input = array($UK_VAT_Number);
			}
			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}
		}	
		if($Country =='United Kingdom'){
			$IOSS_Number = 'OrderId:'.$OrderId.'-1';
		}
		
		if(empty($saved_invoice_id)){	
	 		$INVOICE_NO = substr($company_name,0,3).'-IN-'.$this->getInvoiceId($company_name); 
			$this->OpenOrder->updateAll(array('invoice_id' => "'".$INVOICE_NO."'"), array('num_order_id' => $OrderId));
	 	}else{
			$INVOICE_NO = $saved_invoice_id;
		}	
		
		if(empty($saved_vat_number)){
			$this->OpenOrder->updateAll(array('vat_number' => "'".$VAT_Number."'"), array('num_order_id' => $OrderId));
	 	}else{
			$VAT_Number = $saved_vat_number;
		}
		 
		$ebay_sub_total = 0;  
		if(in_array(strtolower($SubSource),['ebay0','ebay2','ebay5'])){
 			$ebay_sub_total = $TOTAL;
			$tot =  0;// ($TOTAL * $tax_rate / 100);
		 	$TOTAL = $TOTAL + $tot;
 		}
		 
		$txt = 'Taxable Supply'; 
		
		$total_item_vat_amount = 0;
		$total_item_cost 	   = 0;
 		$shipping_per_order	   = 0;   
 		$prhtml = '';
		$spd =	$this->MergeUpdate->find('all', array('conditions' => array( 'MergeUpdate.order_id' => $OrderId )));
		if(count($spd) > 0){
			$_total = $TOTAL / count($spd);
			$shipping_per_order = $POSTAGE/count($spd);
		}else{
			$_total = $TOTAL;
			$shipping_per_order = $POSTAGE;
		}
		if(empty($split_order_id)){
			$split_order_id = $OrderId.'-1';
		}	
  			
		$splitOrderDetail = $this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $split_order_id )));
		$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
 		$po_total =0;
		foreach( $skus as $_sku )
		{
			$newSku 	= explode( 'XS-', $_sku);  
			$getsku 	= 'S-'.$newSku[1];
			$qty 		= $newSku[0];
			$pos 		= $this->PurchaseOrder->find( 'first', array( 'conditions' => array('purchase_sku' => $getsku,'price >'=> 0 ),'order'=> 'id desc' ) );

			if(count($pos) > 0){
				$po_total += ($pos['PurchaseOrder']['price']*$qty);
			}  
				 
		}
		$total_vat_excl = 0;
		foreach( $skus as $_sku )
		{
			$newSku = explode( 'XS-', $_sku); 
			 
 			$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
			$getsku 	= 'S-'.$newSku[1];
			$qty 		= $newSku[0];
			$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
			 
			$list = $this->ProductBarcode->find('first', array('conditions' => array('global_barcode'=> trim($getOrderDetail['ProductDesc']['barcode'])),  'fields' => array( 'barcode'), 'order' => 'id DESC' ) );
			
			 if(count($list) > 0){
				 $local_barcode = $list['ProductBarcode']['barcode'];
			 }else{
				 $local_barcode = $getOrderDetail['ProductDesc']['barcode'];
			 }  
			 
			$pos = $this->PurchaseOrder->find( 'first', array( 'conditions' => array('purchase_sku' => $getsku,'price >'=> 0 ),'order'=> 'id desc' ) );
			 
 			$catName     = $getOrderDetail['Product']['category_name'];
			$Title       = $getOrderDetail['Product']['product_name'];
			if(strlen($getOrderDetail['Product']['product_name']) > 60){
				$Title = substr($getOrderDetail['Product']['product_name'],0,60).'...';
			}
			
			$bin_loc = [];
			$bins = $this->OrderLocation->find( 'all', array( 'conditions' => array('sku' => $getsku,'picklist_inc_id'=>$splitOrderDetail['MergeUpdate']['picklist_inc_id'] ) ) ); 
			if(count($bins) > 0){
				foreach($bins as $bin){ 
					$bin_loc[$bin['OrderLocation']['bin_location']] = $bin['OrderLocation']['bin_location'];
				}
			}
			/*
			if($shipping_per_item > 0){
				$shipping_per_item = ($shipping_per_item / $item->Quantity);	
			}
			$PricePerUnit = $item->PricePerUnit - $shipping_per_item;
			 
			 $item_vat_amount = $PricePerUnit - ($PricePerUnit/(1 + ($tax_rate/100)));
			 $item_cost = $PricePerUnit - $item_vat_amount;
			
			 $total_item_vat_amount  +=$item_vat_amount;
			 $total_item_cost  += $item_cost * $item->Quantity;*/
			 
			 
			 /*Amazon ASIN price = product price 50 + shipping 20
		Cost of A - 5
		Cost of B - 10
		Cost of C - 15
		Total cost aBC = 30
		Sale price for A = (5/30)*50 = 8.33
		Sale price for B = (10/30)*50= 16.67
		Sale price for C= 50 - 8.33 -16.67 = 25*/
			 $_item_cost = ($TOTAL / count($skus));				  
			 $PricePerUnit = number_format(($TOTAL / $po_total)*$pos['PurchaseOrder']['price'],2);
			 
			 // $PricePerUnit =($_item_cost / $qty);
			 //($_item_cost / $qty);
			 $item_vat_amount = $PricePerUnit - ($PricePerUnit/(1 + ($tax_rate/100)));
			 $item_cost = $PricePerUnit - $item_vat_amount;
			 $total_vat_excl += ($item_cost * $qty) ;
			 $total_item_vat_amount  +=($item_vat_amount * $qty);
			 $total_item_cost  += $PricePerUnit * $qty;
		
			// $templateHtml  .='<tr><td>'.$Title.'</td><td>'. implode( ",",$bin_loc) .'</td><td>'.$getsku.'</td><td>'.$qty.'</td><td>'.$currency_code.'&nbsp;'.number_format($item_cost,2).'</td><td>'.$tax_rate.'%</td><td>'.$currency_code.'&nbsp;'.number_format($PricePerUnit,2).'</td><td>'.$currency_code.'&nbsp;'.number_format(($PricePerUnit *$qty),2).'</td></tr>';
		
		  
			$prhtml  .='<tr>';
				$prhtml  .='<td style="border:1px solid #ccc;font-size:11px;">'.$Title.'</td>';
				//$prhtml  .='<td style="border:1px solid #ccc">'.implode( ",",$bin_loc).'</td>';
				$prhtml  .='<td style="border:1px solid #ccc">'.$local_barcode.'</td>';						
				$prhtml  .='<td align="center" style="border:1px solid #ccc">'.$qty.'</td>';
				$prhtml  .='<td align="right" style="border:1px solid #ccc">'.$currency_code.'&nbsp;'.number_format($item_cost,2).'</td>';
				$prhtml  .='<td align="right" style="border:1px solid #ccc">'.$tax_rate.'%</td>';
				$prhtml  .='<td align="right" style="border:1px solid #ccc">'.$currency_code.'&nbsp;'.number_format($PricePerUnit,2).'</td>';
				$prhtml  .='<td align="right" style="border:1px solid #ccc">'.$currency_code.'&nbsp;'.number_format(($PricePerUnit *$qty),2).'</td>';
			 $prhtml  .='</tr>';  
			 
		}
		 
		$templateHtml  ='<head><meta charset="UTF-8"><title>Invoice</title></head>';
		$templateHtml  .='<style>*{font-family:Verdana; font-size:12px;}</style>';
 		$templateHtml  .='<table border="0" width="100%"  align="left" cellpadding="0" cellspacing="0" style="margin:2px;">';
    	 $templateHtml  .='<tr>';
			$templateHtml  .='<td style="font-size:14px; font-weight:bold; padding:3px 2px; text-align:left" >Slip/Invoice </td>';
			$templateHtml  .='<td style="font-size:14px; font-weight:bold; padding:3px 2px; text-align:right">'.$IOSS_Number.'&nbsp;&nbsp;&nbsp;&nbsp;</td>';
		$templateHtml  .='  </tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td valign="top" align="left" width="50%">';
				$templateHtml  .= $BILLING_ADDRESS_NAME.'<br>'.$BILLING_ADDRESS;
				//$templateHtml  .='<p>'.$BILLING_ADDRESS.'</p>';
				/*if($VAT_Number){
					$templateHtml  .='<p><strong>'.$VAT_Number.'</strong></p>';
				}*/
 			$templateHtml  .='</td>';
			$templateHtml  .='<td valign="top" align="center" style="padding:4px;background:#dfeced;">';//background:#dfeced; 
			  $templateHtml  .='<div  style="padding:1px;">';//border:1px solid #bdc9c9; 
				$templateHtml  .='<table>';
				  
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2" style="font-size:11px ;font-weight:bold;"> Paid</td>';
					 $templateHtml  .='</tr>';
					$templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2">';
						$templateHtml  .='Sold by <strong> '.$STORE_NAME.'</strong>';
						if($VAT_Number){
							$templateHtml  .='<br>VAT #<strong>'.$VAT_Number.'</strong>'; 
						}
 						 
						$templateHtml  .='</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2" width="100%" height="1px" style="width:100%; height:1px; background:#bdc9c9;"> </td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong>Invoice date / Delivery date</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_DATE.'</td>';
					 $templateHtml  .='</tr>';
					  $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong>Invoice #</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_NO.'</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong>Total payable</strong></td>';
						$templateHtml  .='<td> '.$currency_code.$total_item_cost.'</td>';
					 $templateHtml  .='</tr>'; 
				  
			   $templateHtml  .='</table>';
			   $templateHtml  .='</div>';
			   
			$templateHtml  .='</td>';
		 $templateHtml  .='</tr>';
		 
		 if($exp[$index] == 'fr'){
			 $amazon_url = 'www.amazon.fr';
		 }else if($exp[$index] == 'es'){
			 $amazon_url = 'www.amazon.es';
		 }else if($exp[$index] == 'it'){
			 $amazon_url = 'www.amazon.it';
		 }else if($exp[$index] == 'de'){
			 $amazon_url = 'www.amazon.de';
		 }else{
			 $amazon_url = 'www.amazon.co.uk';
		 }
		
		 if($order->GeneralInfo->Source == 'AMAZON'){
			 $templateHtml  .='<tr>';		 
				$templateHtml  .='<td colspan="2" style="padding:2px 0px;"> For questions about your order, visit '.$amazon_url.'/contact-us</td>';
			 $templateHtml  .='</tr>';
		 }
		 
 		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="padding:2px 0px;"><strong>Tranction Type : </strong>'.$txt.' <strong>&nbsp;&nbsp; Order Ref# &nbsp; </strong>'.$Source.' '.$ReferenceNum.'&nbsp;&nbsp;<strong>Order Id#</strong>&nbsp;'.$OrderId.'</td>';
		 $templateHtml  .='</tr>';
		 
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="background:#ddd; height:1px;"> </td>';
		 $templateHtml  .='</tr>';
		  
		 
		 $templateHtml  .='<tr><td colspan="2">';
		 $templateHtml  .='<table border="0" align="center" cellpadding="0" cellspacing="0" >';

			
				$templateHtml  .='<tr>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">BILLING ADDRESS </th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">DELIVERY ADDRESS</th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">SOLD BY </th>';
			   $templateHtml  .='</tr>'; 
			   
			  $templateHtml  .='<tr>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<strong>'.$BILLING_ADDRESS_NAME.'</strong><br>'.$BILLING_ADDRESS; 
				  $templateHtml  .='</td>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<strong>'.$SHIPPING_ADDRESS_NAME.'</strong><br>'.$SHIPPING_ADDRESS;
				  $templateHtml  .='</td>';
				 $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<strong>'.$STORE_NAME.'</strong><br>'.$STORE_ADDRESS;
					 if($VAT_Number){
						 $templateHtml  .='<br><strong>'.$VAT_Number.'</strong>';
					 }
					  
				  $templateHtml  .='</td>'; 
			 $templateHtml  .='</tr>';
			/*$templateHtml  .='<tr><td colspan="3" style="background:#ddd; height:1px;"> </td></tr>';
			$templateHtml  .='<tr><td colspan="3"><strong>Order information</strong></td></tr>';
			$templateHtml  .='<tr><td colspan="2">Order date '.$ORDER_DATE.'</td><td>Order # '.$ReferenceNum.'</td></tr>';
 		  */
		 $templateHtml  .='</table>';
		 $templateHtml  .='</td></tr>'; 
		$templateHtml  .='</table>';
 		
 		$templateHtml  .='<table border="0" width="100%" cellpadding="1" cellspacing="0" align="left" style="margin:2px;">';
 		  		  
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="7" style="font-size:16px; font-weight:bold; border-top:1px solid #ddd; border-bottom:1px solid #ddd; padding:1px 3px;"> Invoice Details</td>';
		 $templateHtml  .='</tr>';
		 
		 
		  $templateHtml  .='<tr>';
			$templateHtml  .='<th valign="top"> Description </th>';
			//$templateHtml  .='<th valign="top" width="15%"> Bin Location </th>';
			$templateHtml  .='<th valign="top" width="15%"> Barcode </th>';
			$templateHtml  .='<th valign="top" width="8%"> Qty </th>';
			$templateHtml  .='<th valign="top" align="right" width="8%">Unit price<br>(excl.VAT)</th>';
			$templateHtml  .='<th valign="top" align="right" width="8%">VAT rate</th>';
			$templateHtml  .='<th valign="top" align="right" width="8%">Unit price<br>(incl.VAT)</th>';
			$templateHtml  .='<th valign="top" align="right" width="8%">Subtotal<br>(incl.VAT)</th>';
		 $templateHtml  .='</tr>';
	
		/*----*/	
		
		$templateHtml .= $prhtml;
		 
		
		 $postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
		 $postage_cost = $POSTAGE - $postage_vat_amount;
	 	 
		   $templateHtml  .='<tr style="background:#ddd;">';

			$templateHtml  .='<td style="padding:5px 2px;">Shipping Charge</td>';
			$templateHtml  .='<td style="padding:5px 2px;"></td>';
			$templateHtml  .='<td style="padding:5px 2px;"></td>';
			//$templateHtml  .='<td style="padding:5px 2px;"></td>';
			$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format($postage_cost,2).' </td>';
			$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
			$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
		 $templateHtml  .='</tr>';
		
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="4"></td>';
			$templateHtml  .='<td colspan="2" style="font-size:12px; font-weight:bold; padding:10px 0px;">Invoice total</td>';
			$templateHtml  .='<td style="font-size:14px; font-weight:bold; padding:10px 0px;" align="right">'.$currency_code.'&nbsp;'.number_format($total_item_cost,2).'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="4"></td>';
			$templateHtml  .='<td colspan="3" style="height:1px; background:#ddd;"></td>';
		 $templateHtml  .='</tr>';
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<th colspan="5" align="right"> VAT rate </th>';
			$templateHtml  .='<th align="right">Item subtotal (excl. VAT)</th>';
			$templateHtml  .='<th align="right">VAT subtotal</th>';
		 $templateHtml  .='</tr>';
		$templateHtml  .='<tr>';
 			$templateHtml  .='<td colspan="4"></td>';
 			$templateHtml  .='<td style="background:#ddd;padding:5px 2px"> '.$tax_rate.'%</td>';
			
			/*if($ebay_sub_total > 0){
				$templateHtml  .='<td style="background:#ddd;padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format($ebay_sub_total,2).'</td>';
				$templateHtml  .='<td style="background:#ddd;padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format((number_format($total_item_cost,2) - $ebay_sub_total),2).'</td>';
 			}else{*/
				$templateHtml  .='<td style="background:#ddd;padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_vat_excl + $postage_cost),2).'</td>';
				$templateHtml  .='<td style="background:#ddd;padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_vat_amount + $postage_vat_amount),2).'</td>';
 			//}
			
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
 			$templateHtml  .='<td colspan="4"></td>';
 			$templateHtml  .='<td style="padding:5px 2px; font-weight:bold;">Total</td>';
			/*if($ebay_sub_total > 0){
				$templateHtml  .='<td style="padding:5px 2px; font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format($ebay_sub_total,2).'</td>';
				$templateHtml  .='<td style="padding:5px 2px; font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format((number_format($total_item_cost,2) - $ebay_sub_total),2).'</td>';
 			}else{*/
				$templateHtml  .='<td style="padding:5px 2px; font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format((($total_item_cost - $total_item_vat_amount) + $postage_cost),2).'</td>';
				$templateHtml  .='<td style="padding:5px 2px; font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_vat_amount + $postage_vat_amount),2).'</td>';
 		//	}
		 $templateHtml  .='</tr>';  
 		
 		
		/*--------------Ink------------------*/
		$this->loadMOdel( 'InkOrderLocation' );	 
		$order_ink	    = $this->InkOrderLocation->find('first', array('conditions' => array('order_id' => $OrderId,'split_order_id'=>$OrderId.'-1','status'=>'active')));		
		
		
		if(count($order_ink) > 0 ){			 
		//	$templateHtml .=  '<tr><td colspan="8"><div style="color:green;font-size:12px;"><center>#2 Envelope Required.</center></div><br><img src="'.WWW_ROOT.'img/ink_xl2.jpg" height="100"></td></tr>';
		} /*---------------EC MARK---------------*/
				
 		$person = [];  
		if($Country != 'United Kingdom' ){
			$get_sp_order = $this->MergeUpdate->find('first', array('conditions' => array('order_id' => $OrderId),'fields'=>['sku']));
			if(count( $get_sp_order ) > 0){				
 				$skus = explode( ',', $get_sp_order['MergeUpdate']['sku']);
				foreach($skus as $k => $v){
					$x = explode('XS-', $v); 
					$_sku = 'S-'.$x[1];
					$getRes	= $this->ResponsiblePersonDetail->find('first', array('conditions' => array('sku' => $_sku, 'store_name' => $company)));
					if(count($getRes) > 0){
						$person[$getRes['ResponsiblePersonDetail']['brand_name']] = $getRes['ResponsiblePersonDetail']['person_information'];
					}
					 
				}
			}		
		}
		
		if(count($person) > 0){
 			$person_info = ''; $b = 0;
			foreach($person as $brand => $pinfo){
				if(($b > 1) && ($b % 2 == 0)){
					$person_info .= '<br>';
				}
				$person_info .= $pinfo;
				$b++;
			}
			 
			$templateHtml .= '<tr><td colspan="8"><div style="font-size:11px; border:1px solid; text-align:left;padding:5px;">This product is CE marked products in accordance with European health, safety, and environmental protection standards for products sold within the European Economic Area (EEA).Authorised representative European Union:'. ($person_info).'</div></td></tr>';
 			 
		}
		
		
		$templateHtml  .='</table>'; 
		return $templateHtml;	
	
	
	}
	
	public function getB2ChtmlEpson($order = null){ 
	
 		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdate');
		$this->loadModel('OpenOrder');
		$this->loadModel('SourceList');	
		$this->loadModel('Country');	
		$this->loadModel('Product');	
		$this->loadModel('OrderLocation');
		$this->loadModel('PurchaseOrder');
		$this->loadModel('ProductBarcode');	 
		$this->loadModel('ResponsiblePersonDetail' );
		 
		$Source		  	  = $order->GeneralInfo->Source;
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  	
 		$saved_vat_number = $order->vat_number;  	 		
		$saved_invoice_id = $order->invoice_id;  
		$ioss_no		  = $order->ioss_no;  
		$buyer_vat_no     = $order->buyer_vat_number;	 		
			
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		$BILLING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName));
		$SHIPPING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName));
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
 			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS  = ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if(isset($order->CustomerInfo->BillingAddress->Address2) && $order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if(isset($order->CustomerInfo->BillingAddress->Address3) && $order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode));
			} 
			if(isset($order->CustomerInfo->BillingAddress->Region) && $order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ', '.ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region));
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
 				$BILLING_ADDRESS .=	$order->CustomerInfo->BillingAddress->Country ? '<br>'.$order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
 			
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS = ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode));
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ', '.ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region));
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
 				$SHIPPING_ADDRESS .=  $order->CustomerInfo->Address->Country ? '<br>'.$order->CustomerInfo->Address->Country : '';
			}			
		}		
		 
		$EmailAddress	  = $order->CustomerInfo->Address->EmailAddress;
		$exp   = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = $exp[$index];
		$country_code   =	$store_country;
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$_POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 
		$ReferenceNum	  = $order->GeneralInfo->ReferenceNum; 
 			
		$ORDER_DATE	  	 = date('d-m-Y', strtotime($order->GeneralInfo->ReceivedDate));
		$INVOICE_NO	 	 = 'INV-'.$OrderId ;
		$INVOICE_DATE 	 = $ORDER_DATE;//date('d-m-Y');
 		
		$templateHtml 	  = NULL;
   
		//echo "</pre>";
		$item_lvcr_limit  = '21.9'; //EUR
		$lvcr_limit_shipping  = '150';		
		$POSTAGE = $_POSTAGE;
 		
		 
 		if(empty($country_code) || $country_code == 'com'){
		 	$country_code = $this->getCountryCode($Country);
		}		 
 		$store_country = [];
		$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $country_code],'fields'=>['name','custom_name']]);
		if(count( $cresult ) > 0){	
			$store_country  = [$cresult['Country']['name'], $cresult['Country']['custom_name']];
		}else{
		 	$country_code = $this->getCountryCode($Country);
			$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $country_code],'fields'=>['name','custom_name']]);
			if(count( $cresult ) > 0){	
				$store_country  = [$cresult['Country']['name'], $cresult['Country']['custom_name']];
			}
		} 	
			 
		
		$tax_rate = 20; $vat_amount = 0;
		 
 		$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
		if(count( $thsresult ) > 0){				
			$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
		} 
		$currency_code = $this->getCurrencyCode($Currency);
		 
		$VAT_Number = ''; $company = ''; $IOSS_Number = 'IOSS:123456789';
		
 		if(in_array(strtolower($SubSource),['costbreaker_uk','costbreaker_de','costbreaker_es','costbreaker_fr','costbreaker_it','costbreaker_nl','marec_uk','marec_de','marec_es','marec_fr','marec_it','marec_nl','rainbow retail','rainbow retail de','rainbow_retail_fr','rainbow_retail_it'])){
			$IOSS_Number = 'IOSS:IM4420001201';//Amazon Europe 			
		}else if(strtolower($SubSource) == 'costbreaker_usabuyer'){
			/* 06-AUG-2021  Hi Avadhesh Ji,
			For USA and CANADA can we make the changes like this  - 
			1) Orders sold on USA store should have no IOSS no on Label and API
			2) Orders for USA but sold in Europe  we can use the IOSS no in Order*/
			$IOSS_Number = '';
			$euc = $this->Country->find('first',['conditions' => ['OR'=>['name' => $Country,'custom_name' => $Country],'eu_country' => 1],'fields'=>['name']]);
			if(count($euc) > 0){
				$IOSS_Number = 'IOSS:IM4420001234';//Amazon US 	
			}		
		}else if(strtolower($SubSource) == 'costbreaker_ca'){
			$IOSS_Number = '';
			$euc = $this->Country->find('first',['conditions' => ['OR'=>['name' => $Country,'custom_name' => $Country],'eu_country' => 1],'fields'=>['name']]);
			if(count($euc) > 0){
				$IOSS_Number = 'IOSS:IM4420001223';//Amazon Canada  
			}			
		}else if(strtolower($SubSource) == 'costdropper'){
			$IOSS_Number = 'IOSS:IM2760000924';//Real DE				
		}else if(strtolower($SubSource) == 'cdiscount' || strtolower($SubSource) == 'costbreaker'){
			$IOSS_Number = 'IOSS:IM2500000295';
 		}
		
 		if($result == 'cost' || in_array( strtolower($SubSource),['ebay2','onbuy'])){ 
			$company 	=  'CostBreaker'; 
			$company_name = 'EURACO';
			$EORI_Number = 'GB019020854000';
			$UK_VAT_Number = 'GB 304984295'; 
			$DE_VAT_Number = 'DE 321777974'; 
			$FR_VAT_Number = 'FR 59850070509'; 
			$IT_VAT_Number = 'IT 10905930961'; 
			$CZ_VAT_Number = 'CZ 684972917';  
			$ES_VAT_Number = 'ES N6061518D';

 			if(strtolower($SubSource) == 'costdropper'){
				$IOSS_Number = 'IOSS:IM2760000924';//Real DE				
			} 
			if(strtolower($SubSource) == 'cdiscount' || strtolower($SubSource) == 'costbreaker'){
				$IOSS_Number = 'IOSS:IM2500000295';
				$FR_VAT_Number = 'FR 34424059822';
			}

			$STORE_NAME	   = $this->COST_STORE_NAME;
		    $STORE_ADDRESS = $this->COST_STORE_ADDRESS; 
			$input = array();
			if($Country == 'United Kingdom'){
 				$VAT_Number   = $UK_VAT_Number;
  			}else if($Country == 'Germany'){
				$VAT_Number   = $DE_VAT_Number;
			}else if($Country == 'France'){
				$VAT_Number   = $FR_VAT_Number;
			}else if($Country == 'Italy'){
				$VAT_Number   = $IT_VAT_Number;
 			}else if($Country == 'Spain'){
				$VAT_Number   = $ES_VAT_Number;
 			}else{
 				$input = array($UK_VAT_Number,$FR_VAT_Number,$CZ_VAT_Number);
			}
			if(count($input) > 0){
 				  $rand_keys 	  = array_rand($input);
 				  $VAT_Number     = $input[$rand_keys]; 
			}
 		}
		
		if($result == 'mare' || strtolower($SubSource) == 'ebay5'){
			$company 	=  'Marec';  
			$company_name = 'ESL';
			$EORI_Number = 'GB019434034000';
			$UK_VAT_Number = 'GB 307817693'; 
			$DE_VAT_Number = 'DE 323086773';
			$IT_VAT_Number = 'IT 11062310963'; // 16 Dec 2019
			$FR_VAT_Number = 'FR 07879900934'; 
		
			$STORE_NAME	   = $this->MARE_STORE_NAME;
		    $STORE_ADDRESS = $this->MARE_STORE_ADDRESS;
			$input = array();
			if($Country == 'United Kingdom'){
 				$VAT_Number = $UK_VAT_Number;	
 			}else if($Country == 'Germany'){
				$VAT_Number = $DE_VAT_Number;	
			}else if($Country == 'France'){
 				$VAT_Number = $FR_VAT_Number;	
 			}else if($Country == 'Italy'){
 				$VAT_Number = $IT_VAT_Number;	
  			}else if($Country == 'Spain'){
 				$VAT_Number   = $UK_VAT_Number;
  			}else{
				$input = array($UK_VAT_Number,$DE_VAT_Number);
			}
			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}			 
		}	
		
 		if(in_array($result,['rain','rrre']) || strtolower($SubSource) == 'ebay0'){
 			$company 	= 'Rainbow Retail'; 
			$company_name = 'FRESHER';
			$EORI_Number = 'GB318649182000';
			$UK_VAT_Number = 'GB 318649182'; 
			$IT_VAT_Number = 'IT 11061000961';
			$DE_VAT_Number = 'DE 327173988';
			$FR_VAT_Number = '';
		  
			$STORE_NAME	   = $this->RAIN_STORE_NAME;
		    $STORE_ADDRESS = $this->RAIN_STORE_ADDRESS;
			
			$input = array();
			if($Country == 'United Kingdom'){
 				$VAT_Number = $UK_VAT_Number;	
 			}else if($Country == 'Germany'){
 				$VAT_Number = $DE_VAT_Number;	
			}else if($Country == 'France'){
				$VAT_Number = $UK_VAT_Number;	
			}else if($Country == 'Italy' ){
 				$VAT_Number = $IT_VAT_Number;	
  			}else{
				$input = array($UK_VAT_Number);
			}
			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}
		}	
	
		if($Country =='United Kingdom'){
			$IOSS_Number = 'OrderId:'.$OrderId.'-1';
		}
		
		if(empty($saved_invoice_id)){	
	 		$INVOICE_NO = substr($company_name,0,3).'-IN-'.$this->getInvoiceId($company_name); 
			$this->OpenOrder->updateAll(array('invoice_id' => "'".$INVOICE_NO."'"), array('num_order_id' => $OrderId));
	 	}else{
			$INVOICE_NO = $saved_invoice_id;
		}	
		
		if(empty($saved_vat_number)){
			$this->OpenOrder->updateAll(array('vat_number' => "'".$VAT_Number."'"), array('num_order_id' => $OrderId));
	 	}else{
			$VAT_Number = $saved_vat_number;
		}
		 
		$ebay_sub_total = 0;  
		if(in_array(strtolower($SubSource),['ebay0','ebay2','ebay5'])){
 			$ebay_sub_total = $TOTAL;
			$tot =  0;// ($TOTAL * $tax_rate / 100);
		 	$TOTAL = $TOTAL + $tot;
 		}
		 
		$txt = 'Taxable Supply'; 
		
		$total_item_vat_amount = 0;
		$total_item_cost 	   = 0;
 		$shipping_per_order	   = 0;   
 		$prhtml = '';
		$spd =	$this->MergeUpdate->find('all', array('conditions' => array( 'MergeUpdate.order_id' => $OrderId )));
		if(count($spd) > 0){
			$_total = $TOTAL / count($spd);
			$shipping_per_order = $POSTAGE/count($spd);
		}else{
			$_total = $TOTAL;
			$shipping_per_order = $POSTAGE;
		}
		if(empty($split_order_id)){
			$split_order_id = $OrderId.'-1';
		}	
  			
		$splitOrderDetail = $this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $split_order_id )));
		$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
 		$po_total =0;
		foreach( $skus as $_sku )
		{
			$newSku 	= explode( 'XS-', $_sku);  
			$getsku 	= 'S-'.$newSku[1];
			$qty 		= $newSku[0];
			$pos 		= $this->PurchaseOrder->find( 'first', array( 'conditions' => array('purchase_sku' => $getsku,'price >'=> 0 ),'order'=> 'id desc' ) );

			if(count($pos) > 0){
				$po_total += ($pos['PurchaseOrder']['price']*$qty);
			}  
				 
		}
		$total_vat_excl = 0;
		foreach( $skus as $_sku )
		{
			$newSku = explode( 'XS-', $_sku); 
			 
 			$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
			$getsku 	= 'S-'.$newSku[1];
			$qty 		= $newSku[0];
			$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
			 
			$list = $this->ProductBarcode->find('first', array('conditions' => array('global_barcode'=> trim($getOrderDetail['ProductDesc']['barcode'])),  'fields' => array( 'barcode'), 'order' => 'id DESC' ) );
			
			 if(count($list) > 0){
				 $local_barcode = $list['ProductBarcode']['barcode'];
			 }else{
				 $local_barcode = $getOrderDetail['ProductDesc']['barcode'];
			 }  
			 
			$pos = $this->PurchaseOrder->find( 'first', array( 'conditions' => array('purchase_sku' => $getsku,'price >'=> 0 ),'order'=> 'id desc' ) );
			 
 			$catName     = $getOrderDetail['Product']['category_name'];
			$Title       = $getOrderDetail['Product']['product_name'];
			if(strlen($getOrderDetail['Product']['product_name']) > 60){
				$Title = substr($getOrderDetail['Product']['product_name'],0,60).'...';
			}
			
			$bin_loc = [];
			$bins = $this->OrderLocation->find( 'all', array( 'conditions' => array('sku' => $getsku,'picklist_inc_id'=>$splitOrderDetail['MergeUpdate']['picklist_inc_id'] ) ) ); 
			if(count($bins) > 0){
				foreach($bins as $bin){ 
					$bin_loc[$bin['OrderLocation']['bin_location']] = $bin['OrderLocation']['bin_location'];
				}
			}
 			 
			 $_item_cost = ($TOTAL / count($skus));				  
			 $PricePerUnit = number_format(($TOTAL / $po_total)*$pos['PurchaseOrder']['price'],2);
			 
			 // $PricePerUnit =($_item_cost / $qty);
			 //($_item_cost / $qty);
			 $item_vat_amount = $PricePerUnit - ($PricePerUnit/(1 + ($tax_rate/100)));
			 $item_cost = $PricePerUnit - $item_vat_amount;
			 $total_vat_excl += ($item_cost * $qty) ;
			 $total_item_vat_amount  +=($item_vat_amount * $qty);
			 $total_item_cost  += $PricePerUnit * $qty;
 			 		
 			$prhtml  .='<tr>';
				$prhtml  .='<td valign="top" style="border-bottom:1px solid #000;">'.$Title.'</td>';
  				$prhtml  .='<td valign="top" style="border-bottom:1px solid #000;">'.$qty.'</td>';
				$prhtml  .='<td valign="top" style="border-bottom:1px solid #000;" align="right">'.$currency_code.'&nbsp;'.number_format($item_cost,2).'</td>';
				$prhtml  .='<td valign="top" style="border-bottom:1px solid #000;" align="right">'.$tax_rate.'%</td>';
				$prhtml  .='<td valign="top" style="border-bottom:1px solid #000;" align="right">'.$currency_code.'&nbsp;'.number_format(($PricePerUnit *$qty),2).'&nbsp;</td>';
			 $prhtml  .='</tr>';  
			 
		}
		 
		
		$templateHtml  ='<table width="100%" style="font-family:calibri; text-align:left; border:1px solid #000;" cellspacing="0" cellpadding="3">';
		$templateHtml  .='<tr>';
		$templateHtml  .='<td valign="top" style="border-right:none;">Slip/Invoice</td>';   
		$templateHtml  .='<td valign="top" align="right" style="text-align:left;">'.$IOSS_Number.'</td>';   
		$templateHtml  .='</tr>'; 
		$templateHtml  .='<tr>';
		$templateHtml  .='<td  valign="top" style="border-right:none;">
		<strong>'.$STORE_NAME.'</strong><br>
		<strong>VAT</strong> #'.$VAT_Number.'<br>
		<!--<strong>Invoice date / Delivery date</strong> '.$INVOICE_DATE.'<br>-->
		<strong>Invoice</strong> # INV-'.$INVOICE_NO.'<br>
		<!--<strong>Total payable</strong> '.$currency_code.$total_item_cost.'-->
 		<strong>Order date: </strong> '.$INVOICE_DATE.'<br>
		<strong>Order#</strong> '.$ReferenceNum.'

		</td>';
		   
		$templateHtml  .='<td valign="top" align="right" style="text-align:left;"><strong>'.$SHIPPING_ADDRESS_NAME.'</strong><br>'.$SHIPPING_ADDRESS.'</td>';   
		$templateHtml  .='</tr>';
		   
		$templateHtml  .='<tr><td colspan="2">Tranction Type : '.$txt.'</td></tr>';
		$templateHtml  .='</table>';
		
   		$templateHtml  .='<table width="100%" style="font-family:calibri; text-align:left; border:1px solid #000;"  cellspacing="0" cellpadding="3">';
		$templateHtml  .='<tr>';
		$templateHtml  .='<td valign="top"><strong>BILLING ADDRESS</strong><br> '.$BILLING_ADDRESS_NAME.$BILLING_ADDRESS.'</td>';
		$templateHtml  .='<td valign="top"><strong>DELIVERY ADDRESS</strong><br>'.$SHIPPING_ADDRESS_NAME.$SHIPPING_ADDRESS.'</td>';
  		$templateHtml  .='</tr>';
		
		$templateHtml  .='</table>';
		
		$templateHtml  .='<table width="100%" style="font-family:calibri; text-align:left; border:1px solid #000;" cellspacing="0" cellpadding="0">';
		$templateHtml  .=' <tr>
		  <th valign="top" style="border-bottom:1px solid #000;">Description</th>
		  <th valign="top" style="border-bottom:1px solid #000;">Qty</th>
		  <th valign="top" style="border-bottom:1px solid #000;" align="right">Unit Price (excl. VAT)</th>
		  <th valign="top" style="border-bottom:1px solid #000;" align="right">Vat</th>
		  <th valign="top" style="border-bottom:1px solid #000;" align="right">Total (incl. VAT)&nbsp;</th>
		  </tr>';
 		$templateHtml .= $prhtml;
	
 	 $postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
	 $postage_cost = $POSTAGE - $postage_vat_amount;
		 
	 $templateHtml  .='<tr>';
		$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;">Shipping Charge</td>';
		$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;"></td>';
		$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;" align="right">'.$currency_code.'&nbsp;'.number_format($postage_cost,2).' </td>';
		$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;" align="right">'.$tax_rate.'%</td>';
		$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'&nbsp;</td>';
	 $templateHtml  .='</tr>';
			 
	   $templateHtml  .='<tr>';
				$templateHtml  .='<td></td>';
				$templateHtml  .='<td colspan="3" style="font-size:14px; font-weight:bold;">Grand Total</td>';
				$templateHtml  .='<td style="font-size:14px; font-weight:bold;padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'. number_format($total_item_cost,2).'</td>';
			 $templateHtml  .='</tr>';
		 $templateHtml  .='</table>';	  
 		
		/*--------------Ink------------------*/
		$this->loadMOdel( 'InkOrderLocation' );	 
		$order_ink	    = $this->InkOrderLocation->find('first', array('conditions' => array('order_id' => $OrderId,'split_order_id'=>$OrderId.'-1','status'=>'active')));		
		
		
		if(count($order_ink) > 0 ){			 
		//	$templateHtml .=  '<tr><td colspan="8"><div style="color:green;font-size:12px;"><center>#2 Envelope Required.</center></div><br><img src="'.WWW_ROOT.'img/ink_xl2.jpg" height="100"></td></tr>';
		} /*---------------EC MARK---------------*/
				
 		$person = [];  
		if($Country != 'United Kingdom' ){
			$get_sp_order = $this->MergeUpdate->find('first', array('conditions' => array('order_id' => $OrderId),'fields'=>['sku']));
			if(count( $get_sp_order ) > 0){				
 				$skus = explode( ',', $get_sp_order['MergeUpdate']['sku']);
				foreach($skus as $k => $v){
					$x = explode('XS-', $v); 
					$_sku = 'S-'.$x[1];
					$getRes	= $this->ResponsiblePersonDetail->find('first', array('conditions' => array('sku' => $_sku, 'store_name' => $company)));
					if(count($getRes) > 0){
						$person[$getRes['ResponsiblePersonDetail']['brand_name']] = $getRes['ResponsiblePersonDetail']['person_information'];
					}
					 
				}
			}		
		}
		
		if(count($person) > 0){
 			$person_info = ''; $b = 0;
			foreach($person as $brand => $pinfo){
				if(($b > 1) && ($b % 2 == 0)){
					$person_info .= '<br>';
				}
				$person_info .= $pinfo;
				$b++;
			}
			 
			$templateHtml .= '<tr><td colspan="5"><div style="font-size:11px; border:1px solid; text-align:left;padding:5px;">This product is CE marked products in accordance with European health, safety, and environmental protection standards for products sold within the European Economic Area (EEA).Authorised representative European Union:'. ($person_info).'</div></td></tr>';
 			 
		}
		
		
		$templateHtml  .='</table>'; 
		return $templateHtml;	
	
	
	}
	
	public function testinv() 
	{	
	
 					
					$html ='<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
								 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
								 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
								 <meta content="" name="description"/>
								 <meta content="" name="author"/> 
								 <table style="height:1090px;   border:1px solid #CCC;"  > 
   <tr>
      <td style="padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >
         <table border="0"  align="center" cellpadding="0" cellspacing="0" style="margin:0px auto;">
            <tbody>
               <tr>
                  <td colspan="2" style="font-size:14px; font-weight:bold; padding:3px 2px; text-align:right"> Invoice </td>
               </tr>
               <tr>
                  <td valign="middle" align="left" width="50%">
                     <p style="font-size:12px ;font-weight:bold;">Marzaroli Martine</p>
                     <p>12 rue des Acacias<br>Nanterre<br>92000<br>France</p>
                  </td>
                  <td valign="top" align="center" style="padding:5px;background:#dfeced;">
                         <table>
                           <tr>
                              <td style="font-size:12px ;font-weight:bold;"> Paid</td>
                           </tr>
                           <tr>
                              <td colspan="2">Sold by <strong> FRESHER BUSINESS LIMITED</strong><br>VAT #<strong>GB 318649182</strong></td>
                           </tr>
                           <tr>
                              <td colspan="2" width="100%" height="1px" style="width:100%; height:1px; background:#bdc9c9;"> </td>
                           </tr>
                           <tr>
                              <td> <strong>Invoice date / Delivery date</strong></td>
                              <td> 12-11-2020</td>
                           </tr>
                           <tr>
                              <td> <strong><strong>Invoice #</strong></td>
                              <td> INV-3283949</td>
                           </tr>
                           <tr>
                              <td> <strong><strong>Total payable</strong></td>
                              <td> &euro;39.6</td>
                           </tr>
                        </table>
                   </td>
               </tr>
               <tr>
                  <td colspan="2" style="padding:2px 0px;"> For questions about your order, visit www.amazon.fr/contact-us</td>
               </tr>
               <tr>
                  <td colspan="2" style="padding:2px 0px;"><strong>Tranction Type : </strong>Non Taxable Supply( Export/Intra Community Transactions)</td>
               </tr>
               <tr>
                  <td colspan="2" style="background:#ddd; height:1px;"> </td>
               </tr>
               <tr>
                  <td colspan="2">
                     <table border="0" width="700px" align="center" cellpadding="0" cellspacing="0" >
                        <tr>
                           <td valign="top"><strong>Marzaroli Martine</strong><br>12 rue des Acacias<br>Nanterre<br>92000<br>France</td>
                           <td valign="top"><strong>Marzaroli Martine</strong><br>12 rue des Acacias<br>Nanterre<br>92000<br>France</td>
                           <td valign="top"><strong>FRESHER BUSINESS LIMITED</strong><br>Beachside Business Centre<br>Rue Du Hocq St Clement<br>Jersey JE2 6LF<br><strong>GB 318649182</strong></td>
                        </tr>
                     </table>
                  </td>
               </tr>
         </table>
         <table cellpadding="0" cellspacing="0" align="center">
            <tbody>
               <tr>
                  <td colspan="6" style="font-size:16px; font-weight:bold; border-top:2px solid #ddd; border-bottom:2px solid #ddd; padding:7px 5px;"> Invoice Details</td>
               </tr>
            </tbody>
         </table>
       </td>
   </tr>
   <tr><td><img src="http://development.xsensys.com/postnl/labels/label_3283949-1.jpg" width="300"></td></tr>
</table>  
   
 ';

	require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				
				$cssPath = WWW_ROOT .'css/';
					$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
					  echo $html;
				//	$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
					$dompdf->load_html($html, Configure::read('App.encoding'));
					$dompdf->render();
					$imgPath = WWW_ROOT .'img/printPDF/Bulk/'; 
					$path = Router::url('/', true).'img/printPDF/Bulk/';
					$name	=	'Label_Slip_3283949-1.pdf';
					unlink($imgPath.$name);   
					file_put_contents($imgPath.$name, $dompdf->output());
					
exit;
    }
  	
	public function dateFormat($date = null, $country = null) 
	{
		$date = '2019-03-05';
		$country = 'fr';
		if($country == 'fr'){
		
 				$order_date = date('D. M d Y', strtotime($date));
				$english_days = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
				$french_days = array('lun', 'mar', 'mer', 'jeu', 'ven', 'sam', 'dim');
				//$french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
				$english_months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
				//$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
				$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
				
			
		echo	str_replace($english_months, $french_months, str_replace($english_days, $french_days, $order_date )  );
			
 		}
		
	
		//return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date) ) ) );
	}
	
	public function getStoreData($SubSource = null, $country = null){
		$store_name = '';
		$store = strtolower(substr($SubSource, 0, 4));
		$europe = array('uk','es','it','fr','de');
		if($store == 'cost' && in_array($country,$europe)){
			$store_name = 'Cost-Dropper';
		}else if($store == 'mare' && in_array($country,$europe)){
			$store_name = 'EbuyerExpress';
		}else if(in_array($store,['rain','rrre']) && in_array($country,$europe)){
			$store_name = 'RRRetail';
		}else if($store == 'bbd_' && in_array($country,$europe)){
			$store_name = 'BBDEU';
		}else if($store == 'cost' && $country == 'ca'){
			$store_name = 'CanadaEmporium';
		}else if($store == 'cost' && $country == 'com'){
			$store_name = 'USABuyer';
		}
		return $store_name;
	}
	
	public function getListingId($SubSource = null, $seller_sku = null){
	
 		$store = strtolower(substr($SubSource, 0, 4));
		
		if($store == 'cost'){
			$url = 'http://cost-dropper.com/Webservice/getListingId'; 
		}else if($store == 'mare'){
			$url = 'http://ebuyer-express.com/Webservice/getListingId'; 
		}
		
		$channel_sku['seller_sku'] = $seller_sku;
		
		$curl = curl_init(); 
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FAILONERROR, true); 
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
		curl_setopt($curl, CURLOPT_POST, count($channel_sku));
		curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($channel_sku));  
		$result = curl_exec($curl); 						 
	 	$error = curl_error($curl); 
		$info = curl_getinfo($curl);
		curl_close($curl);	
		$data = json_decode($result);
		 
		if(count($data) < 1){
			$data = '0612UINKSC5';
		}
		 
 		return $data ;
	}
	
	public function getDHLInvoice($order_id = null){
				 
		$this->loadModel('MergeUpdate');
		$waybill_number = '';
		$result = $this->MergeUpdate->find('first', array('conditions' => array('order_id' => $order_id),'fields'=>['track_id']));
		if(count($result) > 0){
			$waybill_number = $result['MergeUpdate']['track_id'];
		}
		//$order = $this->getOrderByNumId_openorder( $order_id );	
		$order = $this->getOrderByNumIdDhl( $order_id );	
		 
 
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();	
		$result 		=	'';
		$htmlTemplate	=	'';	

		$htmlTemplate	=	$this->getTemplateDHL($order,$waybill_number);		
		$SubSource		=	$order->GeneralInfo->SubSource;
	
		 $result			=	ucfirst(strtolower(substr($SubSource, 0, 4)));
		
		/**************** for tempplate *******************/
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
	 	$html .= '<body>'.$htmlTemplate.'</body>';
	 		
		$name	= $order_id.'.pdf';							
		$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
		$dompdf->render();			
						
		$file_to_save = WWW_ROOT .'img/dhl/invoice_'.$name;
		 
		//save the pdf file on the server
		file_put_contents($file_to_save, $dompdf->output()); 
		
		/*-----------------DHL AMAZON SLIP----------------------*/	
	 	$this->getDHLSlip($order_id);
		
 		exit;		 
			
	}	
	
	public function Address(){
	
		$this->loadModel('InvoiceAddress');

		$data['order_id'] 	= $this->request->data['order_id'];
		$field 				= $this->request->data['field'];
		$data[$field] 		= $this->request->data['val'];
				
		if($this->Auth->user('username')){
			$data['username']  = $this->Auth->user('username');
		}
		
		$conditions = array('InvoiceAddress.order_id' => $this->request->data['order_id']);
														
		if ($this->InvoiceAddress->hasAny($conditions)){
			foreach($data as $field => $val){
				$DataUpdate[$field] = "'".$val."'";								
			}
			$this->InvoiceAddress->updateAll( $DataUpdate, $conditions );
			$msg['msg'] = 'updated';
		}else{
			$this->InvoiceAddress->saveAll($data);
			$msg['msg'] = 'saved';			
		}
	echo json_encode($msg);
	exit;
	
	}
	
	public function getCountryCode($Country = null){
		$code = '';
		if($Country == 'United Kingdom'){
			$code = 'gb';
		}else if($Country == 'Germany'){
			$code = 'de';
		}else if($Country == 'France'){
			$code = 'fr';
		}else if($Country == 'Italy'){
			$code = 'it';
		}else if($Country == 'Spain'){
			$code = 'es';
		}else if($Country == 'Australia'){
			$code = 'au';
		}else if($Country == 'Canada'){
			$code = 'ca';
		}else if($Country == 'United States'){
			$code = 'us';
		}
		return $code;
	}
	
	public function getCurrencyCode($currency  = null){
		$code = $currency;
		if($currency  == 'GBP'){
			$code = '&pound;';
		}else if($currency  == 'EUR'){
			$code = '&euro;';
		}else if($currency  == 'USD'){
			$code = '$';
		}else if($currency  == 'CAD'){
			$code = 'CAD$';
		} 
		return $code;
	}
	
	public function getThermalInvoice($openOrderId = null){
				 
		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdate');
		$this->loadModel('OpenOrder');
		$this->loadModel('SourceList');	
		$this->loadModel('Country');	
		$this->loadModel('Product');
		$this->loadModel('ResponsiblePersonDetail');
		
 		$order = $this->getOrderByNumId_openorder($openOrderId);		
 		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  	
		$saved_vat_number = $order->vat_number;  
		
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
	
		$SHIPPING_ADDRESS = '';		$BILLING_ADDRESS = '';
	 	$BILLING_ADDRESS_NAME  	=  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName));
		$SHIPPING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName));
  		if($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
 		if($BILLING_ADDRESS == ''){
			
 			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS  = ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode));
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ', '.ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region));
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
 				$BILLING_ADDRESS .=	$order->CustomerInfo->BillingAddress->Country ? '<br>'.$order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
 		if($SHIPPING_ADDRESS == ''){
 			
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS = ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode));
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ', '.ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region));
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
 				$SHIPPING_ADDRESS .=  $order->CustomerInfo->Address->Country ? '<br>'.$order->CustomerInfo->Address->Country : '';
			}			
		}	
		
		$EmailAddress	  = $order->CustomerInfo->Address->EmailAddress;
		$exp = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = $exp[$index];
		$country_code  = $store_country; 		
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$_POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 
		$ReferenceNum	  = $order->GeneralInfo->ReferenceNum; 
 			
		$ORDER_DATE	  	 = date('d-m-Y', strtotime($order->GeneralInfo->ReceivedDate));
		$INVOICE_NO	 	 = 'INV-'.$OrderId ;
		$INVOICE_DATE 	 = $ORDER_DATE; 
		
 		$templateHtml 	  = NULL;
   
 		$item_lvcr_limit  = '21.9'; //EUR
		$lvcr_limit_shipping  = '150';		
		$final_total =  $TOTAL ;
	 
 		if(empty($country_code) || $country_code == 'com'){
		 	$country_code = $this->getCountryCode($Country);
		} 
		$store_country = [];
		$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $country_code],'fields'=>['name','custom_name']]);
		if(count( $cresult ) > 0){	
			$store_country  = [$cresult['Country']['name'], $cresult['Country']['custom_name']];
		}
		
		$tax_rate = 0; $vat_amount = 0;
		 
 		$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
		if(count( $thsresult ) > 0){				
			$tax_rate = $thsresult['ProductHscode']['tax_rate'];
		} 
		 
	 	$currency_code = $this->getCurrencyCode($Currency);
		 
		$VAT_Number = ''; $company = ''; $IOSS_Number = '123456789';
		if($result == 'cost' || strtolower($SubSource) == 'ebay2'){
			$company 	=  'CostBreaker'; 
			$EORI_Number = 'GB019020854000';
			$UK_VAT_Number = 'GB 304984295'; 
			$DE_VAT_Number = 'DE 321777974'; 
			$FR_VAT_Number = 'FR 59850070509'; 
			$IT_VAT_Number = 'IT 10905930961'; 
			$CZ_VAT_Number = 'CZ 684972917';  
			$ES_VAT_Number = 'ES N6061518D';

 			if(strtolower($SubSource) == 'costdropper' ){
				$IOSS_Number = 'IM2760000924';//Real DE				
			} 
			if(strtolower($SubSource) == 'cdiscount' || strtolower($SubSource) == 'costbreaker'){
				$IOSS_Number = 'IM2500000295';
				$FR_VAT_Number = 'FR 34424059822';
			}
			$STORE_NAME	   = $this->COST_STORE_NAME;
		    $STORE_ADDRESS = $this->COST_STORE_ADDRESS; 
			$input = array();
			if($Country == 'United Kingdom'){
 				$VAT_Number   = $UK_VAT_Number;
  			}else if($Country == 'Germany'){
				$VAT_Number   = $DE_VAT_Number;
			}else if($Country == 'France'){
				$VAT_Number   = $FR_VAT_Number;
			}else if($Country == 'Italy'){
				$VAT_Number   = $IT_VAT_Number;
 			}else if($Country == 'Spain'){
				$VAT_Number   = $ES_VAT_Number;
 			}else{
 				$input = array($UK_VAT_Number,$FR_VAT_Number,$CZ_VAT_Number);
			}
			if(count($input) > 0){
 				  $rand_keys 	  = array_rand($input);
 				  $VAT_Number     = $input[$rand_keys]; 
			}
		
		}
		if($result == 'mare' || strtolower($SubSource) == 'ebay5'){
			$company 	=  'Marec';  
			$EORI_Number = 'GB019434034000';
			$UK_VAT_Number = 'GB 307817693'; 
			$DE_VAT_Number = 'DE 323086773';
			$IT_VAT_Number = 'IT 11062310963'; // 16 Dec 2019
			$FR_VAT_Number = 'FR 07879900934'; 
		
			$STORE_NAME	   = $this->MARE_STORE_NAME;
		    $STORE_ADDRESS = $this->MARE_STORE_ADDRESS;
			$input = array();
			if($Country == 'United Kingdom'){
 				$VAT_Number = $UK_VAT_Number;	
 			}else if($Country == 'Germany'){
				$VAT_Number = $DE_VAT_Number;	
			}else if($Country == 'France'){
 				$VAT_Number = $FR_VAT_Number;	
 			}else if($Country == 'Italy'){
 				$VAT_Number = $IT_VAT_Number;	
  			}else if($Country == 'Spain'){
 				$VAT_Number   = $UK_VAT_Number;
  			}else{
				$input = array($UK_VAT_Number,$DE_VAT_Number);
			}
			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}			 
		}	
 		if(in_array($result,['rain','rrre']) || strtolower($SubSource) == 'ebay0'){
 			$company 	= 'Rainbow Retail'; 
			$EORI_Number = 'GB318649182000';
			$UK_VAT_Number = 'GB 318649182'; 
			$IT_VAT_Number = 'IT 11061000961';
			$DE_VAT_Number = 'DE 327173988';
			$FR_VAT_Number = '';
		  
			$STORE_NAME	   = $this->RAIN_STORE_NAME;
		    $STORE_ADDRESS = $this->RAIN_STORE_ADDRESS;
			
			$input = array();
			if($Country == 'United Kingdom'){
 				$VAT_Number = $UK_VAT_Number;	
 			}else if($Country == 'Germany'){
 				$VAT_Number = $DE_VAT_Number;	
			}else if($Country == 'France'){
				$VAT_Number = $UK_VAT_Number;	
			}else if($Country == 'Italy' ){
 				$VAT_Number = $IT_VAT_Number;	
  			}else{
				$input = array($UK_VAT_Number);
			}
			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}
		}	
		 
		if(empty($saved_vat_number)){
			$this->OpenOrder->updateAll(array('vat_number' => "'".$VAT_Number."'"), array('num_order_id' => $OrderId));
	 	}else{
			$VAT_Number = $saved_vat_number;
		}
		$txt = 'Taxable Supply'; 
		  
		 
								
$templateHtml  ='<table width="100%" style="font-family:calibri; text-align:left; border:1px solid #000;" cellspacing="0" cellpadding="3">';
 $templateHtml  .='<tr>';
$templateHtml  .='<td valign="top" style="border-right:none;">Slip/Invoice</td>';   
$templateHtml  .='<td valign="top" align="right" style="text-align:left;">IOSS:'.$IOSS_Number.'</td>';   
$templateHtml  .='</tr>'; 
$templateHtml  .='<tr>';
$templateHtml  .='<td  valign="top" style="border-right:none;">
<strong>'.$STORE_NAME.'</strong><br>
<strong>VAT</strong> #'.$VAT_Number.'<br>
<strong>Invoice date / Delivery date</strong> '.$INVOICE_DATE.'<br>
<strong>Invoice</strong> # INV-'.$INVOICE_NO.'<br>
<strong>Total payable</strong> '.$currency_code.$TOTAL.'
</td>';
   
$templateHtml  .='<td valign="top" align="right" style="text-align:left;"><strong>'.$SHIPPING_ADDRESS_NAME.'</strong><br>'.$SHIPPING_ADDRESS.'</td>';   
$templateHtml  .='</tr>';
   
$templateHtml  .='<tr><td colspan="2">Tranction Type : '.$txt.'</td></tr>';
$templateHtml  .='</table>';
  
$templateHtml  .='<table width="100%" style="font-family:calibri; text-align:left; border:1px solid #000;"  cellspacing="0" cellpadding="3">';
$templateHtml  .='<tr>';
$templateHtml  .='<td valign="top"><strong>BILLING ADDRESS</strong><br> '.$BILLING_ADDRESS_NAME.$BILLING_ADDRESS.'</td>';
$templateHtml  .='<td valign="top"><strong>DELIVERY ADDRESS</strong><br>'.$SHIPPING_ADDRESS_NAME.$SHIPPING_ADDRESS.'</td>';
 
					 
$templateHtml  .='<td valign="top"><strong>Order information</strong><br><strong>Order date</strong>'.$ORDER_DATE.'<br><strong>Order</strong>#'.$ReferenceNum.'</td>';
$templateHtml  .='</tr>';

$templateHtml  .='</table>';

$templateHtml  .='<table width="100%" style="font-family:calibri; text-align:left; border:1px solid #000;" cellspacing="0" cellpadding="3">';
$templateHtml  .=' <tr>
  <th  valign="top" style="border-bottom:1px solid #000;">Description</th>
  <th  valign="top" style="border-bottom:1px solid #000;">Qty</th>
  <th  valign="top" style="border-bottom:1px solid #000;">Unit Price</th>
  <th  valign="top" style="border-bottom:1px solid #000;">Vat</th>
  <th  valign="top" style="border-bottom:1px solid #000;">Total</th>
  </tr>';
  
  
	$total_item_vat_amount = 0;
	$total_item_cost 	   = 0;
	$shipping_per_item 	   = 0;
	
	if($lvcr_limit_shipping >= $final_total){
		$shipping_per_item = $POSTAGE / count($items);
	}
	
	 foreach($items as $count => $item){ 
		 
		if(strlen($item->Title) > 10){
			$Title = $item->Title; 
		}else{
			$Title = $item->ChannelTitle;
		} 
		 
		if($shipping_per_item > 0){
			$shipping_per_item = ($shipping_per_item / $item->Quantity);	
		}
		$PricePerUnit = $item->PricePerUnit - $shipping_per_item;
		 
		 $item_vat_amount = $PricePerUnit - ($PricePerUnit/(1 + ($tax_rate/100)));
		 $item_cost = $PricePerUnit - $item_vat_amount;
		
		 $total_item_vat_amount  +=$item_vat_amount;
		 $total_item_cost  += $item_cost * $item->Quantity;
		  
		$templateHtml  .='<tr>';
			$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;">'.$Title.'</td>';
			$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;">'.$item->Quantity.'</td>';
			$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;" align="right">'.$currency_code.'&nbsp;'.number_format($item_cost,2).'</td>';
			$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;" align="right">'.$tax_rate.'%</td>';
			//$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format($PricePerUnit,2).'</td>';
			$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;" align="right">'.$currency_code.'&nbsp;'.number_format(($item->CostIncTax - 
			($shipping_per_item *$item->Quantity)),2).'</td>';
		 $templateHtml  .='</tr>';
		 
	 } 
	
	 $postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
	 $postage_cost = $POSTAGE - $postage_vat_amount;
	 
	 $templateHtml  .='<tr>';
 		$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;">Shipping Charge</td>';
		$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;"></td>';
		$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;" align="right">'.$currency_code.'&nbsp;'.number_format($postage_cost,2).' </td>';
		$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;" align="right">'.$tax_rate.'%</td>';
		//$templateHtml  .='<td style="padding:10px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
		$templateHtml  .='<td style="padding:5px 2px;border-bottom:1px solid #000;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
	 $templateHtml  .='</tr>';
  		 
   $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
  			$templateHtml  .='<td colspan="3" style="font-size:14px; font-weight:bold;">Grand Total</td>';
			$templateHtml  .='<td style="font-size:14px; font-weight:bold;padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'. number_format($TOTAL,2).'</td>';
		 $templateHtml  .='</tr>';
	 $templateHtml  .='</table>';	 
	 
	$this->loadMOdel( 'InkOrderLocation' );
 	$order_ink	    = $this->InkOrderLocation->find('first', array('conditions' => array('order_id' => $OrderId,'split_order_id' => $OrderId.'-1','status' => 'active')));
	
	if(count($order_ink) > 0 ){ 					 
		$templateHtml 	.=  '<div style="color:green;font-size:14px;"><center>#2 Envelope Required.</center></div><br><img src="'.WWW_ROOT.'img/ink_xl2.jpg" height="130">';
	}
		 
	  /*---------------EC MARK---------------*/
				
 		$person = [];  
		if($Country != 'United Kingdom' ){
			$get_sp_order = $this->MergeUpdate->find('first', array('conditions' => array('order_id' => $OrderId),'fields'=>['sku']));
			if(count( $get_sp_order ) > 0){				
 				$skus = explode( ',', $get_sp_order['MergeUpdate']['sku']);
				foreach($skus as $k => $v){
					$x = explode('XS-', $v); 
					$_sku = 'S-'.$x[1];
					$getRes	= $this->ResponsiblePersonDetail->find('first', array('conditions' => array('sku' => $_sku, 'store_name' => $company)));
					if(count($getRes) > 0){
						$person[$getRes['ResponsiblePersonDetail']['brand_name']] = $getRes['ResponsiblePersonDetail']['person_information'];
					}
					 
				}
			}		
		}
		
		if(count($person) > 0){
 			$person_info = ''; $b = 0;
			foreach($person as $brand => $pinfo){
				if(($b > 1) && ($b % 2 == 0)){
					$person_info .= '<br>';
				}
				$person_info .= $pinfo;
				$b++;
			}
			 
			$templateHtml .= '<div style="font-size:11px; border:1px solid; text-align:left;padding:5px;">This product is CE marked products in accordance with European health, safety, and environmental protection standards for products sold within the European Economic Area (EEA).Authorised representative European Union:'. ($person_info).'</div>';
 			 
		}
     //echo $templateHtml;
	// exit;
 	//	return $templateHtml;
 	 
		 require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();	 
		 
		 $html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
	 	echo $html .= '<body>'.$templateHtml.'</body>';
	 		
		echo $name	= $openOrderId.'.pdf';		
		$dompdf->set_paper(array(0, 0, '288', '500' ), 'portrait');					
		$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
		$dompdf->render();			
						
		$file_to_save = WWW_ROOT .'logs/invoice_'.$name;
		 
		//save the pdf file on the server
		file_put_contents($file_to_save, $dompdf->output());  
 		exit;		 
			
	}	

 	
	public function replaceFrenchChar($string = null){
			
		$unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E','Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Nº'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U','Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c','è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n','nº'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o','ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y','ü'=>'u','º'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}	
	public function getJpLabelx( $split_id = '2660473-1' )
		{
 			 
 				$label  = '<td valign="top" style="padding:0px 0px 5px 12px;">						
				<div style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
				transform-origin: 50% 50%;height:270px; width:450px;">
				<img src="'.Router::url('/', true) .'jerseypost/labels/'.$split_id.'.jpg" width="350px" style="margin-left: -110px; margin-right: -70px;margin-top: -70px;"></div></td><td></td>';
			 
						
 			echo $label ; 
			exit;
 		}
  	
}

?>

