<?php

class SkureportsController extends AppController
{
    var $name = "Skureports";
	var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
	public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->Auth->Allow(array('update_amazon_order_id'));
	}
	
	 public function index( $sku = null )
    {
		$this->layout = 'index';
		$this->loadModel('Product');
		$this->Product->unbindModel(array( 'hasMany' => array( 'ProductLocation' )));
		
		if( $sku != '' )
		{
			$sku = str_replace('_h_','#',$sku);
			$this->paginate = array(
								'conditions' => array('Product.product_sku' => $sku),
                        		'fields' => array('Product.product_sku','Product.category_name','ProductDesc.brand','ProductDesc.user_id','ProductDesc.purchase_user_id'),
								'limit' => 750			
			);
		}
		else
		{
		$this->paginate = array(
                        'fields' => array('Product.product_sku','Product.category_name','ProductDesc.brand','ProductDesc.user_id','ProductDesc.purchase_user_id'),
						'limit' => 750			
			);
		}
		$getAllProducts = $this->paginate('Product');
		//$getAllProducts = $this->Product->find( 'all', array('fields' => array( 'Product.product_sku','Product.category_name','ProductDesc.brand','ProductDesc.user_id' )));
		$this->set( 'getAllProducts', $getAllProducts );
	}
	
	
	
	public function getMappedAsin()
	{
		  $sku = $this->request->data['sku'];
		
		  //$sku = array('S-5222B005');
		
		
		  /******************Get Mapped Asin Record from AWS*******/
		  $url = 'http://techdrive.biz/api-getasin.php'; 
		  
		  $curl = curl_init(); 
		  curl_setopt($curl, CURLOPT_HEADER, false);
		  curl_setopt($curl, CURLOPT_URL, $url);
		  curl_setopt($curl, CURLOPT_FAILONERROR, true);   
		  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
		  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
		  //curl_setopt($curl, CURLOPT_POST, count($orderIds));
		  curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($sku));  
		  $result = curl_exec($curl);        
		  $error = curl_error($curl); 
		  $info = curl_getinfo($curl);
		  curl_close($curl);
		  pr($result);
	}
	
	public function getAllMappedAsin()
	{
		$this->loadModel('Product');
		$this->Product->unbindModel(array('hasOne' => array('ProductDesc'), 'hasMany' => array( 'ProductLocation' )));
		$getAllProductSku	=	$this->Product->find( 'list', array( 'fields' => array( 'Product.product_sku' ) ) );
		pr($getAllProductSku);
		exit;
		$url = 'http://techdrive.biz/api-getasin.php'; 
		  
	  	$curl = curl_init(); 
	  	curl_setopt($curl, CURLOPT_HEADER, false);
	  	curl_setopt($curl, CURLOPT_URL, $url);
	  	curl_setopt($curl, CURLOPT_FAILONERROR, true);   
	  	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
	  	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
	  	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
	  	//curl_setopt($curl, CURLOPT_POST, count($orderIds));
	  	curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($getAllProductSku));  
	  	$result = curl_exec($curl);        
	  	$error = curl_error($curl); 
	  	$info = curl_getinfo($curl);
	  	curl_close($curl);
	  	pr($result);
		
		
	}
	
	public function getMappedSkuAsin_old( $sku = null )
	{
		$this->layout = 'index';
		$this->loadModel('Skumapping');
		$this->loadModel('Product');
		$sku = str_replace('_h_','#',$sku);
		
		$getSelectedAsins 	=	$this->Skumapping->find('all', array( 'conditions' => array( 'Skumapping.sku' =>  $sku ) ) );
		
		$this->Product->unbindModel(array( 'hasMany' => array( 'ProductLocation' )));
		$getProduct 		=	$this->Product->find('first', array( 'conditions' => array( 'Product.product_sku' =>  $sku ),'fields' => array( 'Product.product_sku', 'ProductDesc.barcode','Product.current_stock_level', 'Product.product_name' ) ) );
		$this->set( 'getSelectedAsins' , $getSelectedAsins );
		$this->set( 'getProduct' , $getProduct );
		
	}
	
	
	public function getMappedSkuAsin( $sku = null, $startDate = null, $endDate = null )
	{
		$this->layout = 'index';
		$this->loadModel('Skumapping');
		$this->loadModel('ChannelDetail');
		$this->loadModel('Product');
		$sku = str_replace('_h_','#',$sku);
		if($startDate == '')
			$startDate = date('Y-m-d');
		if($endDate == '')
			$endDate = date('Y-m-d');
		
		$getSelectedAsins 	=	$this->Skumapping->find('all', array( 'conditions' => array( 'Skumapping.sku' =>  $sku ) ) );
		$getAllOrders = $this->ChannelDetail->find( 'all', array( 
																'conditions' => array( 'ChannelDetail.sku' =>  $sku, 'order_date >=' =>  $startDate.' 00:00:00', 'order_date <=' => $endDate.' 23:59:59'),
																'fields' => array('SUM(quantity) AS Qty','sub_source','channelSKU','sku','order_date'),
																'group'=>array('channelSKU')
																 )  );
		/*$getAllOrders = $this->ChannelDetail->find( 'all', array( 
																'conditions' => array( 'ChannelDetail.sku' =>  $sku, 'order_date >=' =>  $startDate.' 00:00:00', 'order_date <=' => $endDate.'23:59:59'),
																 )  );*/
		
		$this->Product->unbindModel(array( 'hasMany' => array( 'ProductLocation' )));
		$getProduct 		=	$this->Product->find('first', array( 'conditions' => array( 'Product.product_sku' =>  $sku ),'fields' => array( 'Product.product_sku', 'ProductDesc.barcode','Product.current_stock_level', 'Product.product_name' ) ) );
		$this->set( 'getSelectedAsins' , $getSelectedAsins );
		$this->set( 'getProduct' , $getProduct );
		$this->set( 'getAllOrders' , $getAllOrders );
		
	}
	
	public function getOpenOrderDetail()
	{
		$this->layout = '';
		$this->loadModel('ChannelDetail');
		
		$asin		=	$this->request->data['asin'];
		$sku 		= 	$this->request->data['sku'];
		$channelsku = 	$this->request->data['channelSku'];
		$channel	=	$this->request->data['channelName'];
		
		$getChannelSkuDetail = $this->ChannelDetail->find('all', array( 'conditions' => array( 'sub_source' => $channel, 'channelSKU' => $channelsku, 'sku' => $sku  ),'fields' => array( 'SUM( quantity ) AS Qty', 'SUM( totalCharge ) AS TotalCharge' ) ) ); 
		
		if($getChannelSkuDetail[0][0]['Qty'] > 0 )
		{
			echo $getChannelSkuDetail[0][0]['Qty'].'####'.$getChannelSkuDetail[0][0]['TotalCharge'];
			exit;
		}
		else
		{
			echo '--';
			exit;
		}
		
		
	}
	
	public function getQty( $sku = null, $channelsku = null, $channel = null, $startDate = null, $endDate = null )
	{
	
		$this->loadModel('ChannelDetail');
		 
	if($startDate)
		$startDate = date("Y-d-m", strtotime($startDate));
	if($endDate)
		$endDate = date("Y-d-m", strtotime($endDate));
		
		if( isset($startDate)  &&  isset($endDate) && $startDate != '' && $endDate != '')
		{
		
			$getChannelSkuDetail = $this->ChannelDetail->find('all', array( 'conditions' => array( 'sub_source' => $channel, 'channelSKU' => $channelsku, 'sku' => $sku,'order_date >=' =>  $startDate, 'order_date <=' => $endDate),'fields' => array( 'SUM( quantity ) AS Qty' ) ) ); 
		}
		else
		{
			$getChannelSkuDetail = $this->ChannelDetail->find('all', array( 'conditions' => array( 'sub_source' => $channel, 'channelSKU' => $channelsku, 'sku' => $sku  ),'fields' => array( 'SUM( quantity ) AS Qty' ) ) ); 
		}
		if($getChannelSkuDetail[0][0]['Qty'] > 0 )
		{
			return $getChannelSkuDetail[0][0]['Qty'];
			exit;
		}
		else
		{
			return '0';
			exit;
		}
	}
	
	public function getAmount( $sku = null, $channelsku = null, $channel = null, $startDate = null, $endDate = null )
	{
		
		
		$this->loadModel('ChannelDetail');
		if($startDate)
			$startDate = date("Y-d-m", strtotime($startDate));
		if($endDate)
			$endDate = date("Y-d-m", strtotime($endDate));
		
		if( isset($startDate)  &&  isset($endDate) && $startDate != '' && $endDate != '')
		{
			$getChannelSkuDetail = $this->ChannelDetail->find('all', array( 'conditions' => array( 'sub_source' => $channel, 'channelSKU' => $channelsku, 'sku' => $sku,'order_date >=' =>  $startDate, 'order_date <=' => $endDate),'fields' => array( 'SUM( totalCharge ) AS TotalCharge' ) ) ); 
		}
		else
		{
		$getChannelSkuDetail = $this->ChannelDetail->find('all', array( 'conditions' => array( 'sub_source' => $channel, 'channelSKU' => $channelsku, 'sku' => $sku  ),'fields' => array( 'SUM( totalCharge ) AS TotalCharge' ) ) ); 
		}
		if($getChannelSkuDetail[0][0]['TotalCharge'] > 0 )
		{
			return $getChannelSkuDetail[0][0]['TotalCharge'];
			exit;
		}
		else
		{
			return '0';
			exit;
		}
	}
	
	public function getPoRate( $sku = null )
	{
		$this->loadModel('PurchaseOrder');
		$getPurchaseDetail	= $this->PurchaseOrder->find('all', array('conditions'=> array('PurchaseOrder.purchase_sku' => $sku ), 'order' => array('PurchaseOrder.date DESC') ) );
		if(count($getPurchaseDetail) > 1)
		{
			$new_date = date('Y-m-d', strtotime( $getPurchaseDetail[0]['PurchaseOrder']['date'] )); 
			$podetail = '<b>'.$getPurchaseDetail[0]['PurchaseOrder']['price'] . '</b> [ ' . $getPurchaseDetail[0]['PurchaseOrder']['po_name'] .' ] [ '. $new_date.' ]'; 
			return $podetail;
			exit;
		}
		else
		{
			return '----';
			exit; 
		}
	}
	
	public function getProductForAssign()
	{
		$this->layout = 'index';
		$this->loadModel('Product');
		$this->loadModel('User');
		$this->Product->unbindModel(array( 'hasMany' => array( 'ProductLocation' )));
		
		$this->paginate = array(
								'conditions' => array('OR' => array('ProductDesc.user_id' => '0', 'ProductDesc.purchase_user_id' => 0  ) ),
								'fields' => array( 'Product.product_sku','Product.product_name','ProductDesc.product_id','ProductDesc.user_id','ProductDesc.purchase_user_id' ),
								'limit' => '2000'
								);
		$getAllProducts = $this->paginate('Product');
		$getAllUsers = $this->User->find('list', array('conditions' => array( 'country' => '3' ), 'fields' => array( 'first_name' ) ) );
		$this->set( 'getAllProducts', $getAllProducts );
		$this->set( 'getAllUsers', $getAllUsers );
		
	}
	
	public function getUserName( $id = null )
	{
		$this->loadModel('User');
		$getUserDetail	=	$this->User->find('first', array('conditions' => array( 'User.id' =>  $id ) ) );
		if(count($getUserDetail) == 1)
		{
			$userName	=	$getUserDetail['User']['first_name'].' '.$getUserDetail['User']['last_name'];
			return $userName;
			exit;
		}
		else
		{
			return '----';
			exit;
		}
		
	}
	
	public function saveUserWithSku()
	{
		$this->loadModel('ProductDesc');
		$productId	=	$this->request->data['productID'];
		$userID		=	$this->request->data['userID'];
		$userName	=	$this->request->data['userName'];
		
		$result = $this->ProductDesc->updateAll(array('ProductDesc.user_id' => $userID), array('ProductDesc.product_id' => $productId));
		if($result)
		{
			echo "1";
			exit;
		}
		else
		{
			echo "2";
			exit;
		}
	}
	
	public function savePurchaseUserWithSku()
	{
		$this->loadModel('ProductDesc');
		$productId	=	$this->request->data['productID'];
		$userID		=	$this->request->data['userID'];
		$userName	=	$this->request->data['userName'];
		
		$result = $this->ProductDesc->updateAll(array('ProductDesc.purchase_user_id' => $userID), array('ProductDesc.product_id' => $productId));
		if($result)
		{
			echo "1";
			exit;
		}
		else
		{
			echo "2";
			exit;
		}
		
	}
	
	public function generateSkuCsv( $sku = null )
	{
		$this->layout = '';
		$this->autorender = false;
		$this->loadModel('Skumapping');//channel_details
		$this->loadModel('ChannelDetail');
		
		$sku = str_replace('_h_','#',$sku);
		$path = WWW_ROOT.'img/Sku_report';
		$header = "Sku,Channel Sku,Channel,Qty,Amount\r\n";
		file_put_contents($path."/sku_asin.csv",$header, LOCK_EX);
		$getAllAsins	=	$this->ChannelDetail->find('all', array( 'conditions' => array( 'ChannelDetail.sku' =>  $sku) ) );
		
		foreach( $getAllAsins as $getAllAsin )
		{
			$sku		=	$getAllAsin['ChannelDetail']['sku'];
			$channelSku	=	$getAllAsin['ChannelDetail']['channelSKU'];
			$subsource	=	$getAllAsin['ChannelDetail']['sub_source'];
			$qty		=	$getAllAsin['ChannelDetail']['quantity'];
			$amount		=	$getAllAsin['ChannelDetail']['totalCharge'];
			
			$content = $sku.",".$channelSku.",".$subsource.",".$qty.",".$amount."\r\n";
			file_put_contents($path."/sku_asin.csv",$content, FILE_APPEND | LOCK_EX);
		}
		$result	=	Router::url('/', true)."img/Sku_report/sku_asin.csv";
		
	   header('Content-Description: File Transfer');
	   header('Content-Type: application/force-download');
	   header('Content-Disposition: attachment; filename='.basename($result));
	   header('Content-Transfer-Encoding: binary');
	   header('Expires: 0');
	   header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	   header('Pragma: public'); 
	   readfile($result);
		exit;
	}
	
	
	public function generateSkuCsv130417( $sku = null )
	{
		$this->layout = '';
		$this->autorender = false;
		$this->loadModel('Skumapping');
		
		$sku = str_replace('_h_','#',$sku);
		$path = WWW_ROOT.'img/Sku_report';
		$header = "Sku,Asin,Channel Sku,Channel,Qty,Amount\r\n";
		file_put_contents($path."/sku_asin.csv",$header, LOCK_EX);
		$getAllAsins	=	$this->Skumapping->find('all', array( 'conditions' => array( 'Skumapping.sku' =>  $sku) ) );
		foreach( $getAllAsins as $getAllAsin )
		{
			$sku		=	$getAllAsin['Skumapping']['sku'];
			$asin		=	$getAllAsin['Skumapping']['asin'];
			$chanalSku	=	$getAllAsin['Skumapping']['channel_sku'];
			$channel	=	$getAllAsin['Skumapping']['channel_name'];
			$Qty		=	$this->getQty( $sku, $chanalSku, $channel );
			$amount		=	$this->getAmount( $sku, $chanalSku, $channel );
			$content = $sku.",".$asin.",".$chanalSku.",".$channel.",".$Qty.",".$amount."\r\n";
			file_put_contents($path."/sku_asin.csv",$content, FILE_APPEND | LOCK_EX);
		}
		$result	=	Router::url('/', true)."img/Sku_report/sku_asin.csv";
		
	    header('Content-Description: File Transfer');
	   header('Content-Type: application/force-download');
	   header('Content-Disposition: attachment; filename='.basename($result));
	   header('Content-Transfer-Encoding: binary');
	   header('Expires: 0');
	   header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	   header('Pragma: public'); 
	   readfile($result);
		exit;
	}
	
	public function currentDataCsv( $sku = null, $startDate = null, $endDate = null )
	{
		$this->layout = '';
		$this->autorender = false;
		$this->loadModel('ChannelDetail');
		
		$sku = str_replace('_h_','#',$sku);
		if($startDate == '')
			$startDate = date('Y-m-d');
		if($endDate == '')
			$endDate = date('Y-m-d');
		
		
		$getAllOrders = $this->ChannelDetail->find( 'all', array( 
										'conditions' => array( 'ChannelDetail.sku' =>  $sku, 'order_date >=' =>  $startDate.' 00:00:00', 'order_date <=' => $endDate.' 23:59:59'),
										'fields' => array('SUM(quantity) AS quantity','sub_source','channelSKU','sku','order_date','listing_user','purchase_user'),
										'group'=>array('channelSKU')
										 )  );
		
		$path = WWW_ROOT.'img/Sku_report';
		$header = "Sku,Channel Sku,Channel,Qty,Listing User,Purchase User,Order Date\r\n";
		file_put_contents($path."/Channel_Sku_Report.csv",$header, LOCK_EX);
		
		foreach( $getAllOrders as $getAllOrder )
		{
			$sku			=	$getAllOrder['ChannelDetail']['sku'];
			$chanalSku		=	$getAllOrder['ChannelDetail']['channelSKU'];
			$channel		=	$getAllOrder['ChannelDetail']['sub_source'];
			$Qty			=	$getAllOrder[0]['quantity'];
			$listingUser 	= 	$this->getUserName( $getAllOrder['ChannelDetail']['listing_user'] );
			$purchaseUser 	= 	$this->getUserName( $getAllOrder['ChannelDetail']['purchase_user'] );
			$orderDate		=	$getAllOrder['ChannelDetail']['order_date'];
			$content 		=  $sku.",".$chanalSku.",".$channel.",".$Qty.",".$listingUser.",".$purchaseUser.",".$orderDate."\r\n";
			file_put_contents($path."/Channel_Sku_Report.csv",$content, FILE_APPEND | LOCK_EX);
		}
		$result	=	Router::url('/', true)."img/Sku_report/Channel_Sku_Report.csv";
		
	   header('Content-Description: File Transfer');
	   header('Content-Type: application/force-download');
	   header('Content-Disposition: attachment; filename='.basename($result));
	   header('Content-Transfer-Encoding: binary');
	   header('Expires: 0');
	   header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	   header('Pragma: public'); 
	   readfile($result);
		exit;
	}
	
	public function getAllChannelSkuOrders( $user = null, $startDate = null, $endDate = null )
	{
		$this->layout = 'index';
		$this->loadModel('ChannelDetail');
		$this->loadModel('Product');
		
		$this->loadModel('User');
		$getAllUsers = $this->User->find('list', array('conditions' => array( 'country' => '3' ), 'fields' => array('User.first_name' ) ) );
		
		if($startDate == '')
			$startDate = date('Y-m-d');
		if($endDate == '')
			$endDate = date('Y-m-d');
		
		if( empty($user) || $user == '' )
		{
			
			$this->paginate = array(
									'conditions' => array( 'order_date >=' =>  $startDate.' 00:00:00', 'order_date <=' => $endDate.' 23:59:59'),
									'fields' => array('SUM(quantity) AS quantity','sub_source','channelSKU','sku','order_date','listing_user','purchase_user'),
									'group'=>array('channelSKU'),
									'limit' => 500			
								  );
			$getAllSkuCount = $this->ChannelDetail->find( 'all', array( 'fields' => array( 'SUM( quantity ) AS Qty' ) )  );
			$getAllOrders = $this->paginate('ChannelDetail');
		}
		else if( !empty($user) && $user != '' )
		{
			if( $user == 'All Users' )
			{
				$this->paginate = array(
									'conditions' => array( 'order_date >=' =>  $startDate.' 00:00:00', 'order_date <=' => $endDate.' 23:59:59'),
									'fields' => array('SUM(quantity) AS quantity','sub_source','channelSKU','sku','order_date','listing_user','purchase_user'),
									'group'=>array('channelSKU'),
									'limit' => 500			
								  );
			}
			else
			{
				$this->paginate = array(
									'conditions' => array( 'OR' => array('listing_user' =>  $user, 'purchase_user' =>  $user), 'order_date >=' =>  $startDate.' 00:00:00', 'order_date <=' => $endDate.' 23:59:59'),
									'fields' => array('SUM(quantity) AS quantity','sub_source','channelSKU','sku','order_date','listing_user','purchase_user'),
									'group'=>array('channelSKU'),
									'limit' => 500			
								  );
			}
			$getAllSkuCount = $this->ChannelDetail->find( 'all', array( 'fields' => array( 'SUM( quantity ) AS Qty' ) )  );
			$getAllOrders = $this->paginate('ChannelDetail');
		}
		else
		{
			
			$this->paginate = array(
									'conditions' => array( 'order_date >=' =>  $startDate.' 00:00:00', 'order_date <=' => $endDate.' 23:59:59'),
									'fields' => array('SUM(quantity) AS quantity','sub_source','channelSKU','sku','order_date','listing_user','purchase_user'),
									'group'=>array('channelSKU'),
									'limit' => 500			
								  );
			$getAllSkuCount = $this->ChannelDetail->find( 'all', array( 'fields' => array( 'SUM( quantity ) AS Qty' ) )  );
			$getAllOrders = $this->paginate('ChannelDetail');
		
		}
		
		$this->set('getAllSkuCount', $getAllSkuCount);
		$this->set('getAllOrders', $getAllOrders);
		$this->set('getAllUsers', $getAllUsers);
	}
	
	public function getUserids( $name = null )
	{
		$this->loadModel('User');
		
		$getUserDetail	=	$this->User->find('first', array('conditions' => array( 'User.first_name' =>  $name ) ) );
		if(count($getUserDetail) == 1)
		{
			$userName	=	$getUserDetail['User']['id'];
			return $userName;
			exit;
		}
		else
		{
			return '0';
			exit;
		}
	}
	
	
	public function currentDataCsvAllUser( $username = null, $startDate = null, $endDate = null )
	{
		$this->layout = '';
		$this->autorender = false;
		$this->loadModel('ChannelDetail');
		if($username != 'All Users'){
			echo $user = $this->getUserids( $username );
		}
		if($startDate == '')
			$startDate = date('Y-m-d');
		if($endDate == '')
			$endDate = date('Y-m-d');
		
		if( empty($user) || $user == '' || $user == 'All Users')
		{
			$getAllOrders = $this->ChannelDetail->find( 'all', array( 
																'conditions' => array( 'order_date >=' =>  $startDate.' 00:00:00', 'order_date <=' => $endDate.' 23:59:59'),
																 )  );
		}
		else if( !empty($user) && $user != '' )
		{
			if( $user == 'All Users' )
			{
				$getAllOrders = $this->ChannelDetail->find( 'all', array( 
																'conditions' => array( 'order_date >=' =>  $startDate.' 00:00:00', 'order_date <=' => $endDate.' 23:59:59'),
																 )  );
			}
			else
			{
			
			$getAllOrders = $this->ChannelDetail->find( 'all', array( 
																'conditions' => array( 'OR' => array('listing_user' =>  $user, 'purchase_user' =>  $user), 'order_date >=' =>  $startDate.' 00:00:00', 'order_date <=' => $endDate.' 23:59:59'),
																 )  );
			}
		}
		else
		{
			$getAllOrders = $this->ChannelDetail->find( 'all', array( 
																'conditions' => array( 'order_date >=' =>  $startDate.' 00:00:00', 'order_date <=' => $endDate.' 23:59:59'),
																 )  );
		
		}
		$path = WWW_ROOT.'img/Sku_report';
		$header = "Sku,Channel Sku,Channel,Qty,Listing User,Purchase User,Order Date\r\n";
		file_put_contents($path."/Select_Order_Sku_Report.csv",$header, LOCK_EX);
		
		foreach( $getAllOrders as $getAllOrder )
		{
			$sku			=	$getAllOrder['ChannelDetail']['sku'];
			$chanalSku		=	$getAllOrder['ChannelDetail']['channelSKU'];
			$channel		=	$getAllOrder['ChannelDetail']['sub_source'];
			$Qty			=	$getAllOrder['ChannelDetail']['quantity'];
			$listingUser 	= 	$this->getUserName( $getAllOrder['ChannelDetail']['listing_user'] );
			$purchaseUser 	= 	$this->getUserName( $getAllOrder['ChannelDetail']['purchase_user'] );
			$orderDate		=	$getAllOrder['ChannelDetail']['order_date'];
			$content = $sku.",".$chanalSku.",".$channel.",".$Qty.",".$listingUser.",".$purchaseUser.",".$orderDate."\r\n";
			file_put_contents($path."/Select_Order_Sku_Report.csv",$content, FILE_APPEND | LOCK_EX);
		}
		$result	=	Router::url('/', true)."img/Sku_report/Select_Order_Sku_Report.csv";
		
	   header('Content-Description: File Transfer');
	   header('Content-Type: application/force-download');
	   header('Content-Disposition: attachment; filename='.basename($result));
	   header('Content-Transfer-Encoding: binary');
	   header('Expires: 0');
	   header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	   header('Pragma: public'); 
	   readfile($result);
		exit;
	}
	
	
	
	public function getSkuCountReport()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ChannelDetail');
		
		$getAllProducts 	= 	$this->Product->find('list', array( 'fields' => array( 'Product.product_sku' ) ) );
		
		for ($i = 6; $i >= 0; $i--) {
				if( $i == 0 ){
					$timestamp[] = date('Y-m-d');
				} else {
					$timestamp[] = date('Y-m-d', strtotime('-'.$i.' day'));
				}
			}
		
		$path = WWW_ROOT.'img/Sku_report';
		$header = "Sku,Channel Sku,".$timestamp[0].",".$timestamp[1].",".$timestamp[2].",".$timestamp[3].",".$timestamp[4].",".$timestamp[5].",".$timestamp[6]."\r\n";
		file_put_contents($path."/Sku_channel_count.csv",$header, LOCK_EX);
		
		foreach($getAllProducts as $getAllProduct)
		{
			$getAllChannels	=	$this->ChannelDetail->find('all', array( 'conditions' => array( 'ChannelDetail.sku' => $getAllProduct ),'fields' => array( 'DISTINCT ChannelDetail.channelSKU','sub_source' ) ) );
			foreach($getAllChannels as $getAllChannel)
			{
				for ($i = 6; $i >= 0 ; $i--) {
				$timestamp = date('Y-m-d', strtotime('-'.$i.' day'));
				$channelSku = $getAllChannel['ChannelDetail']['channelSKU'];
				$dayQtyChannels	=	$this->ChannelDetail->find('all', array( 
																	  'conditions' => array( 
																	  						'ChannelDetail.sku' => $getAllProduct, 'ChannelDetail.channelSKU' => $channelSku, 
																							'order_date >=' =>  $timestamp.' 00:00:00', 'order_date <=' => $timestamp.' 23:59:59'
																							),
																	  'fields' => array( 'SUM(quantity) as Qty') ) );
																	  
				$channelName 	= 	$getAllChannel['ChannelDetail']['sub_source'];
				$sku			=	$getAllProduct;
				$chanalSku		=	$channelSku;
				$Qty[]			= 	($dayQtyChannels[0][0]['Qty'] != '') ? $dayQtyChannels[0][0]['Qty'] : '0';
				}
				
				$content = $sku.",".$chanalSku.",".$Qty[0].",".$Qty[1].",".$Qty[2].",".$Qty[3].",".$Qty[4].",".$Qty[5].",".$Qty[6]."\r\n";
				unset($Qty);
				file_put_contents($path."/Sku_channel_count.csv",$content, FILE_APPEND | LOCK_EX);
			}
		}
		
	   $result	=	Router::url('/', true)."img/Sku_report/Sku_channel_count.csv";
	   header('Content-Description: File Transfer');
	   header('Content-Type: application/force-download');
	   header('Content-Disposition: attachment; filename='.basename($result));
	   header('Content-Transfer-Encoding: binary');
	   header('Expires: 0');
	   header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	   header('Pragma: public'); 
	   readfile($result);
	   exit;	
	}
	
	public function uploadAssignUser()
	{
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		//$uploadfileName			=	 'User_assigned_list.csv';
		$uploadfileName			=	 'assign_user_update.csv';
		App::import('Vendor', 'PHPExcel/IOFactory');
		$objPHPExcel = new PHPExcel();
		$objReader= PHPExcel_IOFactory::createReader('CSV');
		$objReader->setReadDataOnly(true);				
		$objPHPExcel=$objReader->load('files/'.$uploadfileName );
		$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
		$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
		$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
		$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
		
		for($i=2;$i<=$lastRow;$i++) 
			{
					$sku 			=  	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
					$checkSku		=	$this->Product->find('first', array( 'conditions' => array( 'Product.product_sku' => $sku ) ) );
					if( $checkSku['ProductDesc']['id'] )
					{
						$data['user_id'] 			=   $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
						$data['purchase_user_id'] 	=   $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
						//pr($data);
						//exit;
						//$this->ProductDesc->updateAll( $data, array('ProductDesc.id' => $checkSku['ProductDesc']['id'] ) );
					}
			}
	}
	
	public function uploadAmazonFile()
	{
		 $this->layout = '';
         $this->autoRender = false;
		 $this->loadModel( 'AmazonRecord' );
		 date_default_timezone_set('Europe/Jersey');
		 
		 $fileExtension = explode('.', $_FILES['upload_file']['name']);
		 
		 if( $fileExtension[1] == 'txt' )
		 {
			 $filename = WWW_ROOT. 'img'.DS.'Sku_report/amazon_file/'.$_FILES['upload_file']['name']; 
			 move_uploaded_file($_FILES['upload_file']['tmp_name'],$filename);  
			
			 $mainarrays = array();
			 $i = 0; $j = 0;
			 foreach (file($filename) as $k => $row) 
			 { 
		          $fileData = array();
				  $mainarrays = explode("\t", $row); 
				  if( $k > 0 )
				  {
					 $checkOrder = $this->AmazonRecord->find('first', array( 'conditions' => array( 'AmazonRecord.order_id' => $mainarrays[0], 'AmazonRecord.order_item_id' => $mainarrays[1] ) ) );
					 
					 if( count( $checkOrder ) > 0 )
						 {			 
							 $j++;
							 $fileData['id']					=	$checkOrder['AmazonRecord']['id'];
						 }
						 else
						 {
							 $i++; 
						 }
					 	
						 $fileData['order_id']				=	$mainarrays[0];
						 $fileData['order_item_id']			=	$mainarrays[1];
						 $fileData['purchase_dates']		=	$mainarrays[2];
						 $fileData['payments_date']			=	$mainarrays[3];
						 $fileData['reporting_date']		=	$mainarrays[4];
						 $fileData['promise_date']			=	$mainarrays[5];
						 $fileData['days_past_promise']		=	$mainarrays[6];
						 $fileData['buyer_email']			=	utf8_encode(($mainarrays[7]));
						 $fileData['buyer_name']			=	utf8_encode(($mainarrays[8]));
						 $fileData['buyer_phone_number']	=	$mainarrays[9];
						 $fileData['sku']					=	$mainarrays[10];
						//  $fileData['sku']					=	$mainarrays[10];
						 $fileData['product_name']			=	utf8_encode(($mainarrays[11]));
						 $fileData['quantity_purchased']	=	$mainarrays[12];
						 $fileData['quantity_shipped']		=	$mainarrays[13];
						 $fileData['quantity_to_ship']		=	$mainarrays[14];
						 $fileData['ship_service_level']	=	$mainarrays[15];
						 $fileData['recipient_name']		=	utf8_encode(($mainarrays[16]));
						 $fileData['ship_address_1']		=	utf8_encode(($mainarrays[17]));
						 $fileData['ship_address_2']		=	utf8_encode(($mainarrays[18]));
						 $fileData['ship_address_3']		=	utf8_encode(($mainarrays[19]));
						 $fileData['ship_city']				=	utf8_encode(($mainarrays[20]));
						 $fileData['ship_state']			=	utf8_encode(($mainarrays[21]));
						 $fileData['ship_postal_code']		=	utf8_encode(($mainarrays[22]));
						 $fileData['ship_country']			=	$mainarrays[23];
						 $fileData['sales_channel']			=	trim($mainarrays[24]);
						 //$fileData['sales_channel']			=	trim($mainarrays[24]);
						 $fileData['upload_date']			=	date('Y-m-d');
						 $this->AmazonRecord->saveAll( $fileData );
				 
				 			
				  }  
			 }
			 echo $i." Order Inserted ".$j." Order Updated";
			 exit;
		 }
		 else if( $fileExtension[1] == 'xlsx' )
		 {
		 	$filename = WWW_ROOT.'img'.DS.'Sku_report/amazon_file/'.$_FILES['upload_file']['name']; 
			move_uploaded_file($_FILES['upload_file']['tmp_name'],$filename); 
		 	
			App::import('Vendor', 'PHPExcel/IOFactory');
			$objPHPExcel 	= 	new PHPExcel();
			$objReader		= 	PHPExcel_IOFactory::createReader('Excel2007');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel	=	$objReader->load($filename );
			$objWorksheet	=	$objPHPExcel->setActiveSheetIndex(0);
			$lastRow 		= 	$objPHPExcel->getActiveSheet()->getHighestRow();
			$colString		=	$highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber 		= 	PHPExcel_Cell::columnIndexFromString($colString);
			$k = 0; $j = 0;
			for($i=2;$i<=$lastRow;$i++) 
				{
					$fileData = array();
					$channelName = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
					$status = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
					
					if( $channelName == 'Cdiscount' && $status == 'Confirmed -to be dispatched')
					{
						
							$checkOrder = $this->AmazonRecord->find('first', array( 'conditions' => array( 'AmazonRecord.order_id' => $objWorksheet->getCellByColumnAndRow(1,$i)->getValue(), 'AmazonRecord.sku' => $objWorksheet->getCellByColumnAndRow(10,$i)->getValue() ) ) );
					 
							if( count( $checkOrder ) > 0 )
							 {			 
								 $j++;
								 $fileData['id']				=	$checkOrder['AmazonRecord']['id'];
							 }
							 else
							 {
								 $k++; 
							 }
							 
							 $fileData['order_id']				=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
							 $fileData['order_item_id']			=	'-';
							 $fileData['purchase_date']			=	date('Y-m-d', strtotime(str_replace('/','-',$objWorksheet->getCellByColumnAndRow(2,$i)->getValue())));
							 $fileData['payments_date']			=	date('Y-m-d', strtotime(str_replace('/','-',$objWorksheet->getCellByColumnAndRow(2,$i)->getValue())));
							 $fileData['reporting_date']		=	'';
							 $fileData['promise_date']			=	'';
							 $fileData['days_past_promise']		=	'-';
							 $fileData['buyer_email']			=	'-';
							 $fileData['buyer_name']			=	'-';
							 $fileData['buyer_phone_number']	=	$objWorksheet->getCellByColumnAndRow(22,$i)->getValue();
							 $fileData['sku']					=	$objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
							 $fileData['product_name']			=	$objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
							 $fileData['quantity_purchased']	=	$objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
							 $fileData['quantity_shipped']		=	$objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
							 $fileData['quantity_to_ship']		=	$objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
							 $fileData['ship_service_level']	=	$objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
							 $fileData['recipient_name']		=	$objWorksheet->getCellByColumnAndRow(17,$i)->getValue();
							 $fileData['ship_address_1']		=	$objWorksheet->getCellByColumnAndRow(17,$i)->getValue();
							 $fileData['ship_address_2']		=	$objWorksheet->getCellByColumnAndRow(17,$i)->getValue();
							 $fileData['ship_address_3']		=	'-';
							 $fileData['ship_city']				=	$objWorksheet->getCellByColumnAndRow(19,$i)->getValue();
							 $fileData['ship_state']			=	'-';
							 $fileData['ship_postal_code']		=	$objWorksheet->getCellByColumnAndRow(18,$i)->getValue();
							 $fileData['ship_country']			=	$objWorksheet->getCellByColumnAndRow(20,$i)->getValue();
							 $fileData['sales_channel']			=	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
							 $fileData['upload_date']			=	date('Y-m-d');
							 $this->AmazonRecord->saveAll( $fileData );
					}
				}
			 echo $k." Order Inserted ".$j." Order Updated";
			 exit;
		 } 
		 else 
		 {
		 	echo "Please upload valid file.";
			exit;
		 }
	}
	
	public function uploadAmazonFile_old()
	{
		 $this->layout = '';
         $this->autoRender = false;
		 $this->loadModel( 'AmazonRecord' );
		 
		 $filename = WWW_ROOT. 'img'.DS.'Sku_report/amazon_file/'.$_FILES['upload_file']['name']; 
		 move_uploaded_file($_FILES['upload_file']['tmp_name'],$filename);  
		
		 $mainarrays = array();
		
		 $i = 0; $j = 0;
		 foreach (file($filename) as $k => $row) 
		 { 
			 $fileData = array();
			  $mainarrays = explode("\t", $row); 
			  if( $k > 0 )
			  {
			  
			  $checkOrder = $this->AmazonRecord->find('first', array( 'conditions' => array( 'AmazonRecord.order_id' => $mainarrays[0], 'AmazonRecord.order_item_id' => $mainarrays[1] ) ) );
			 if( count( $checkOrder ) > 0 )
			 {			 
			 	 $j++;
			 	 $fileData['id']					=	$checkOrder['AmazonRecord']['id'];
			 }
			 else
			 {
			 	 $i++; 
			 }
				 $fileData['order_id']				=	$mainarrays[0];
				 $fileData['order_item_id']			=	$mainarrays[1];
				 $fileData['purchase_date']			=	$mainarrays[2];
				 $fileData['payments_date']			=	$mainarrays[3];
				 $fileData['reporting_date']		=	$mainarrays[4];
				 $fileData['promise_date']			=	$mainarrays[5];
				 $fileData['days_past_promise']		=	$mainarrays[6];
				 $fileData['buyer_email']			=	utf8_encode(($mainarrays[7]));
				 $fileData['buyer_name']			=	utf8_encode(($mainarrays[8]));
				 $fileData['buyer_phone_number']	=	$mainarrays[9];
				 $fileData['sku']					=	$mainarrays[10];
				 $fileData['product_name']			=	utf8_encode(($mainarrays[11]));
				 $fileData['quantity_purchased']	=	$mainarrays[12];
				 $fileData['quantity_shipped']		=	$mainarrays[13];
				 $fileData['quantity_to_ship']		=	$mainarrays[14];
				 $fileData['ship_service_level']	=	$mainarrays[15];
				 $fileData['recipient_name']		=	utf8_encode(($mainarrays[16]));
				 $fileData['ship_address_1']		=	utf8_encode(($mainarrays[17]));
				 $fileData['ship_address_2']		=	utf8_encode(($mainarrays[18]));
				 $fileData['ship_address_3']		=	utf8_encode(($mainarrays[19]));
				 $fileData['ship_city']				=	utf8_encode(($mainarrays[20]));
				 $fileData['ship_state']			=	utf8_encode(($mainarrays[21]));
				 $fileData['ship_postal_code']		=	utf8_encode(($mainarrays[22]));
				 $fileData['ship_country']			=	$mainarrays[23];
				 $fileData['sales_channel']			=	trim($mainarrays[24]);
				 $fileData['upload_date']			=	date('Y-m-d');
				 //$this->AmazonRecord->create();
				 $this->AmazonRecord->saveAll( $fileData );
				
				
				
			  }  
		 }
		 
		 echo $i." Order Inserted ".$j." Order Updated";
		 exit;
	}
	
	public function showAmazonRecords( $channel = null, $selectedDate = null )
	{
		$this->layout = 'index';
		$this->loadModel( 'AmazonRecord' );
		date_default_timezone_set('Europe/Jersey');
		if($selectedDate == ''){
			$selectedDate = date('Y-m-d');
		}
		
		if( $channel != '')
		{
			if($channel == 'All Channel' )
			{
				$getAllRecords	=	$this->AmazonRecord->find('all',array('limit' => 500));
			}
			else
			{
				$getAllRecords	=	$this->AmazonRecord->find('all', array( 'conditions' => array( 'sales_channel like ' => "%".$channel."%" ,'upload_date' => $selectedDate),'limit' => 500 ) );
			}
		}
		else
		{
			$getAllRecords	=	$this->AmazonRecord->find('all', array( 'conditions' => array( 'upload_date' => $selectedDate),'limit' => 500 ) );
		}
		
		
		$getChannelName = 	$this->AmazonRecord->find('list', array( 'group' => array('sales_channel'), 'fields' => array( 'sales_channel'),'limit' => 500 ) );
		$this->set( 'getAllRecords', $getAllRecords );
		$this->set( 'getChannelName', $getChannelName );
	}
	
	public function uploadMasterSkuAndChannelSku()
	{
		$this->loadModel('MasterSku');
		$uploadfileName = '';
		$filename = WWW_ROOT.'img'.DS.'Sku_report/master_sku/'.$_FILES['upload_sku_file']['name']; 
		
		$nameExplode 	= 	 explode('.', $filename);
		$nameNew 		= 	 trim(end( $nameExplode ));
		
		if( $nameNew != 'xls' ){
				echo 'Please Upload xlsx file';
				exit;			
			}
		
		move_uploaded_file($_FILES['upload_sku_file']['tmp_name'],$filename);  
		$uploadfileName			=	 $filename;
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		$objPHPExcel = new PHPExcel();
		$objReader= PHPExcel_IOFactory::createReader('Excel5');
		$objReader->setReadDataOnly(true);				
		$objPHPExcel=$objReader->load($uploadfileName );
		$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
		$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
		$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
		$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
		$j = 0; $k = 0;
		for($i=2;$i<=$lastRow;$i++) 
			{
				$data 		= array();
				$dataUpdate = array();
				$checkResult	=	$this->MasterSku->find('first', array( 'conditions' => array( 
																			'MasterSku.channelSku' => $objWorksheet->getCellByColumnAndRow(3,$i)->getValue(),
																			'MasterSku.asin' => $objWorksheet->getCellByColumnAndRow(4,$i)->getValue(),
																			 ) ) );
																			 
				
 				$sku = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$title = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
				if(count($checkResult) == 0 )
				{
					$channel = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
					$asin = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
					
					if($channel !='' && $sku != '' && $asin != '' && $title != ''){
						$data['channel'] 			=  		$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
						$data['sku']				=		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
						$data['title']				=	 	$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
						$data['channelsku']			=	 	$objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
						$data['asin']				=	 	$objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
						$this->MasterSku->saveAll( $data );
						$j++;
					}	
				} else {
				
					$dataUpdate['id'] 			=  		$checkResult['MasterSku']['id'];
					$dataUpdate['channel'] 		=  		$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
					$dataUpdate['sku']			=		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
					$dataUpdate['title']		=	 	$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
					$dataUpdate['channelsku']	=	 	$objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
					$dataUpdate['asin']			=	 	$objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
					$this->MasterSku->saveAll( $dataUpdate );
					$k++;
				}		
			}
		 echo $j." Channel Sku Inserted ".$k." Channel Sku Updated";
		 exit;
	}
	
	public function checkChennalSku( $channel = null )
	{
		$this->layout = 'index';
		$this->loadModel('AmazonRecord');
		$this->loadModel('MasterSku');
		$date = date('Y-m-d');
		
		if( $channel != ''){
			if($channel == 'All Channel' )
			{
				$getOpenOrders	=	$this->AmazonRecord->find('all',array('conditions' => array('upload_date' => $date)));
			}
			else
			{
				$getOpenOrders	=	$this->AmazonRecord->find('all', array( 'conditions' => array( 'sales_channel like ' => "%".$channel."%",'upload_date' => $date) ) );
			}
		} else {
			$getOpenOrders	=	$this->AmazonRecord->find('all', array('conditions' => array('upload_date' => $date)));
		}
		
		$i = 0;
		if(!empty($getOpenOrders))
		{
			foreach( $getOpenOrders as $getOpenOrder )
			{
				$cahnnelSku	= $getOpenOrder['AmazonRecord']['sku'];
				$getChannelSkuRecord	=	$this->MasterSku->find('first', array( 'conditions' => array( 'MasterSku.channelsku' =>  $cahnnelSku ) ) );
				$openOrderData[$i]['open_order_channelsku']		=	$getOpenOrder['AmazonRecord']['sku'];
				$openOrderData[$i]['open_order_product_title']	=	$getOpenOrder['AmazonRecord']['product_name'];
				$openOrderData[$i]['sales_channel']				=	$getOpenOrder['AmazonRecord']['sales_channel'];
				
				if( count($getChannelSkuRecord) == 1 )
				{
					$openOrderData[$i]['channel']				=	$getChannelSkuRecord['MasterSku']['channel'];
					$openOrderData[$i]['martersku_channelsku']	=	$getChannelSkuRecord['MasterSku']['channelsku'];
					$openOrderData[$i]['martersku_title']		=	$getChannelSkuRecord['MasterSku']['title'];
					$openOrderData[$i]['martersku_sku']			=	$getChannelSkuRecord['MasterSku']['sku'];
					$openOrderData[$i]['asin']					=	$getChannelSkuRecord['MasterSku']['asin'];
				}
				else
				{
					$openOrderData[$i]['channel']				=	'-';
					$openOrderData[$i]['martersku_channelsku']	=	'-';
					$openOrderData[$i]['martersku_title']		=	'-';
					$openOrderData[$i]['martersku_sku']			=	'-';
					$openOrderData[$i]['asin']					=	'-';
				}
				$i++;	
			}
			$this->set( 'getOpenOrders', $openOrderData );
		}
		$getChannelName = 	$this->AmazonRecord->find('list', array( 'group' => array('sales_channel'), 'fields' => array( 'sales_channel') ) );
		$this->set( 'getChannelName', $getChannelName );
		
		
	}
	
	public function openOrderStatusfile( $channel = null )
	{
		$this->layout = '';
		$this->autorender = false;
		date_default_timezone_set('Europe/Jersey');
		$date = date('Y-m-d');
		$this->loadModel('AmazonRecord');
		$this->loadModel('MasterSku');
		$this->loadModel('OpenOrder');

		if( $channel != ''){
			if($channel == 'All Channel' )
			{
				$getOpenOrders	=	$this->AmazonRecord->find('all',array( 'conditions' => array( 'upload_date' => $date)));
			}
			else
			{
				$getOpenOrders	=	$this->AmazonRecord->find('all', array( 'conditions' => array( 'sales_channel like ' => "%".$channel."%" ,'upload_date' => $date) ) );
			}
		} else {
			$getOpenOrders	=	$this->AmazonRecord->find('all',array( 'conditions' => array( 'upload_date' => $date)));
		}
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderID'); 
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'SKU'); 
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Master Stock Asin');       
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Master Channel Sku');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'OpenOrder Channel Sku');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Master Stock Title');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'OpenOrder Title');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Match Status');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Order Status');
		$i = 2;
		foreach( $getOpenOrders as $getOpenOrder )
		{
			$cahnnelSku				= 	$getOpenOrder['AmazonRecord']['sku'];
			$orderID				= 	$getOpenOrder['AmazonRecord']['order_id'];
			$getChannelSkuRecord	=	$this->MasterSku->find('first', array( 'conditions' => array( 'MasterSku.channelsku' =>  $cahnnelSku ) ) );
			$sku					=	$getOpenOrder['AmazonRecord']['sku'];
			$openOrderTitle			=	$getOpenOrder['AmazonRecord']['product_name'];
			$saleChannel			=	$getOpenOrder['AmazonRecord']['sales_channel'];
			//$orderStatus 			= 	$getOpenOrder['AmazonRecord']['status'];
			$orderStatus 			= 	$getOpenOrder['AmazonRecord']['status'] ? $getOpenOrder['AmazonRecord']['status'] : 'Please check' ;
			
			if( count($getChannelSkuRecord) == 1 )
			{
				$masterSkuChannel				=	$getChannelSkuRecord['MasterSku']['channel'];
				$masterChannelSku				=	$getChannelSkuRecord['MasterSku']['channelsku'];
				$masterSkuTitle					=	$getChannelSkuRecord['MasterSku']['title'];
				$masterSku						=	$getChannelSkuRecord['MasterSku']['sku'];
				$masterAsin						=	$getChannelSkuRecord['MasterSku']['asin'];
			}
			else
			{
				$masterSkuChannel				=	'-';
				$masterChannelSku				=	'-';
				$masterSkuTitle					=	'-';
				$masterSku						=	'-';
				$masterAsin						=	'-';
			}
			similar_text($masterSkuTitle, $openOrderTitle, $percent);
			if($percent == 100){
					$status = 'Matched';
				} else { 
					$status = 'Not Matched';
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'fcb2b2'))));
				}
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $orderID); 
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $masterSku); 
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $masterAsin);       
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $masterChannelSku);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $sku);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $masterSkuTitle);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $openOrderTitle);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $status.' [ '.number_format($percent,2).' ] ');
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $orderStatus );
			$i++;
	   }
		
		$objPHPExcel->getActiveSheet(0);
		$getBase 		= 	Router::url('/', true);
		$uploadUrl 		= 	WWW_ROOT .'img/Sku_report/master_sku/open_order_result.xlsx';
		$uploadRemote 	= 	$getBase.'img/Sku_report/master_sku/open_order_result.xlsx';
		$objWriter 		= 	PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save($uploadUrl);
		$file = 'img/Sku_report/master_sku/open_order_result.xlsx';
		
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
	}
	
	public function openOrderDayPast( $channel = null )
	{
		$this->layout = '';
		$this->autorender = false;
		date_default_timezone_set('Europe/Jersey');
		$selectedDate = date('Y-m-d');
		
		$this->loadModel('AmazonRecord');
		$this->loadModel('MasterSku');

		if( $channel != ''){
			if($channel == 'All Channel' )
			{
				$getOpenOrders	=	$this->AmazonRecord->find('all', array( 'conditions' => array( 'upload_date' => $selectedDate) ));
			}
			else
			{
				$getOpenOrders	=	$this->AmazonRecord->find('all', array( 'conditions' => array( 'sales_channel like ' => "%".$channel."%", 'upload_date' => $selectedDate ) ) );
			}
		} else {
			$getOpenOrders	=	$this->AmazonRecord->find('all', array( 'conditions' => array( 'upload_date' => $selectedDate) ));
		}
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Order ID'); 
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Sku');    
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Title');   
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Day Past Promise');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Quantity');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Channel');
		$i = 2;
		foreach( $getOpenOrders as $getOpenOrder )
		{
			$orderId			=	$getOpenOrder['AmazonRecord']['order_id'];
			$dayPastPromise		=	$getOpenOrder['AmazonRecord']['days_past_promise'];
			$channelSku			=	$getOpenOrder['AmazonRecord']['sku'];
			$title				=	$getOpenOrder['AmazonRecord']['product_name'];
			$quantity			=	$getOpenOrder['AmazonRecord']['quantity_purchased'];
			$channel			=	$getOpenOrder['AmazonRecord']['sales_channel'];
			if($dayPastPromise >= -1){
				$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'fcb2b2'))));
				}
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $orderId); 
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $channelSku);       
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $dayPastPromise);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $title);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $quantity);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $channel);
			$i++;
		}
		
		$objPHPExcel->getActiveSheet(0);
		$getBase 		= 	Router::url('/', true);
		$uploadUrl 		= 	WWW_ROOT .'img/Sku_report/master_sku/days_past_promise.xlsx';
		$uploadRemote 	= 	$getBase.'img/Sku_report/master_sku/days_past_promise.xlsx';
		$objWriter 		= 	PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save($uploadUrl);
		$file = 'img/Sku_report/master_sku/days_past_promise.xlsx';
		
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
	}
	
	public function showPopupForAddSku()
	{
		$this->render( 'getAddPopup' );
	}
	
	public function addMasterSkuAsin()
	{
		$this->layout = '';
		$this->autorender = false;
		$this->loadModel('MasterSku');
		
		$data['sku'] 		= 	$this->request->data['sku'];
		$data['channel'] 	=	$this->request->data['channel'];
		$data['channelsku'] =	$this->request->data['channelSku'];
		$data['asin'] 		=	$this->request->data['asin'];
		$data['title'] 		=	$this->request->data['title'];
		
		$this->MasterSku->saveAll($data);
		echo "Master Sku Save Successfully";
		exit;
	}
	
	public function getEditPopup()
	{
		$this->layout = '';
		$this->autorender = false;
		$data['channelSku']		=	$this->request->data['channelSku'];
		$data['channelTitle']	=	$this->request->data['channelTitle'];
		$this->set( 'data', $data );
		$this->render( 'getEditPopupShow' );
	}
	
	public function saveAddMasterSku()
	{
		$this->layout = '';
		$this->autorender = false;
		$this->loadModel('MasterSku');
	
		$channelSku	=	$this->request->data['channelSku'];
		$title		=	$this->request->data['title'];
		$channel	=	$this->request->data['channel'];
		$asin	=	$this->request->data['asin'];
		
		$checkResult = $this->MasterSku->find('first', array( 'conditions' => array( 'MasterSku.channelsku' => $channelSku, 'MasterSku.asin' => $asin ) ) );
		if( count($checkResult) == 1 )
		{	
			$this->MasterSku->updateAll( array('MasterSku.title' => $title), array( 'MasterSku.channelsku' => $channelSku ) );
			echo "Channel Sku Update Successfully";
			exit;
		}
		else
		{
			$data['channelsku'] =  $channelSku;
			$data['title'] 		=  $title;
			$data['channel']	=  $channel;
			$data['asin']		=  $asin;
			$this->MasterSku->saveAll( $data );
			echo "Channel Sku save Successfully";
			exit;
		}
		
	}
	
	public function assignStatus()
	{
		date_default_timezone_set('Europe/Jersey');
		$date = date('Y-m-d');
		$this->loadModel('AmazonRecord');
		$this->loadModel('OpenOrder');
		$getDatas = $this->AmazonRecord->find( 'all', array( 'conditions' => array('AmazonRecord.upload_date' =>  $date),'fields' => array( 'AmazonRecord.order_id','AmazonRecord.id' )) );
		$amazon_record = array();
		if( count($getDatas) > 0 )
		{
		
			foreach( $getDatas as $getData ){
				$order_ids[] = $getData['AmazonRecord']['order_id'];
				//$amazon_record[$getData['AmazonRecord']['order_id']] = $getData['AmazonRecord']['id'];
				
				$amazon_item_record[$getData['AmazonRecord']['order_id']][] = $getData['AmazonRecord']['id']; 
				
			}
			 
			$getdetail = $this->OpenOrder->find( 'all', array(
						'conditions' => array( 'OpenOrder.amazon_order_id IN ' => $order_ids),
						'fields' => array( 'OpenOrder.status','OpenOrder.amazon_order_id'  )	
						)
					);
				 
			$open_ord_ids = array(); $processed_ord_ids = array(); $cancelled_ord_ids = array(); $locked_ord_ids = array();
			
			if( count($getdetail) > 0 )
			{
				foreach($getdetail  as $getdetails)
				{
					$status = $getdetails['OpenOrder']['status'];
					$amazon_order_id = $getdetails['OpenOrder']['amazon_order_id'];
					/*if(isset($amazon_record[$amazon_order_id]) && $status == 0 ) {						
						$open_ord_ids[] = $amazon_record[$amazon_order_id];
					}elseif ( isset($amazon_record[$amazon_order_id]) && $status == 1 ) {					
						$processed_ord_ids[] = $amazon_record[$amazon_order_id];
					}elseif(isset($amazon_record[$amazon_order_id]) &&  $status == 2  ) {						
						$cancelled_ord_ids[] = $amazon_record[$amazon_order_id];
					}elseif( isset($amazon_record[$amazon_order_id]) && $status == 3  ) {						
						$locked_ord_ids[] = $amazon_record[$amazon_order_id];
					}*/
					
					if(isset($amazon_item_record[$amazon_order_id]) && $status == 0 ) {
						$open_ord_ids[] = $amazon_item_record[$amazon_order_id];
					}elseif ( isset($amazon_item_record[$amazon_order_id]) && $status == 1 ) {					
						$processed_ord_ids[] = $amazon_item_record[$amazon_order_id];
					}elseif(isset($amazon_item_record[$amazon_order_id]) &&  $status == 2  ) {						
						$cancelled_ord_ids[] = $amazon_item_record[$amazon_order_id];
					}elseif( isset($amazon_item_record[$amazon_order_id]) && $status == 3  ) {						
						$locked_ord_ids[] = $amazon_item_record[$amazon_order_id];
					}
					
				}
			}
 			$open_inc_ids = array();
 			if(count($open_ord_ids) > 0){
				foreach($open_ord_ids as $val => $inc_id){
					foreach($inc_id as $ids){
						$open_inc_ids[] = $ids;
					}
				}
				if( count($open_inc_ids) > 0){			
					if( count($open_inc_ids) == 1){			
						$this->AmazonRecord->updateAll(array('status' => '\'Open\''),  array('id' => $open_inc_ids[0]));
					}else{
						//$this->AmazonRecord->query('Update amazon_records set status = "Open"  where id IN (' .implode(",",$open_ord).')' );					
						 $this->AmazonRecord->updateAll(array('status' => '\'Open\''),  array('id IN ' => $open_inc_ids));
					}
				}
				
			}
			
			$processed_inc_ids = array();
			if(count($processed_ord_ids) > 0){
				foreach($processed_ord_ids as $inc_id){
					foreach($inc_id as $ids){
						$processed_inc_ids[] = $ids;
					} 
				}
				if( count($processed_inc_ids) > 0){				
					if( count($processed_inc_ids) == 1){			
						$this->AmazonRecord->updateAll(array('status' => '\'Processed\''),  array('id' => $processed_inc_ids[0]));
					}else{					
						 $this->AmazonRecord->updateAll(array('status' => '\'Processed\''),  array('id IN ' => $processed_inc_ids));
					}
				}
			}
			
			$cancelled_inc_ids = array();
			if(count($cancelled_ord_ids) > 0){
				foreach($cancelled_ord_ids as $inc_id){
					foreach($inc_id as $ids){
						$cancelled_inc_ids[] = $ids;
					} 
				}
				if( count($cancelled_inc_ids) > 0){
					if( count($cancelled_inc_ids) == 1){			
						$this->AmazonRecord->updateAll(array('status' => '\'Cancelled\''),  array('id' => $cancelled_inc_ids[0]));
					}else{				
						 $this->AmazonRecord->updateAll(array('status' => '\'Cancelled\''),  array('id IN ' => $cancelled_inc_ids));
					}
				}
			}
			
			$locked_inc_ids = array();
			if(count($locked_ord_ids) > 0){
				foreach($locked_ord_ids as $inc_id){
					foreach($inc_id as $ids){
						$locked_inc_ids[] = $ids;
					}  
					if( count($locked_inc_ids) > 0){
						if( count($locked_inc_ids) == 1){			
							$this->AmazonRecord->updateAll(array('status' => '\'Locked\''),  array('id' => $locked_inc_ids[0]));
						}else{				
							 $this->AmazonRecord->updateAll(array('status' => '\'Locked\''),  array('id IN ' => $locked_inc_ids));
						}
					}
				}
			}
			
			echo "Status Assign Successfully.";
		} else {
			echo "There is no order for assign status.";
		}
		exit;
	}
	
	public function update_amazon_order_id()
	{
		
		$this->loadModel('OpenOrder');
	
		$getdetails		=	$this->OpenOrder->find( 'all', array(
				'conditions' => array( 'OpenOrder.amazon_order_id IS NULL' ),
				'fields' => array( 'OpenOrder.general_info','OpenOrder.id'),
				'order'=>'id DESC',
				'limit'=> 50000
				)
			);
		if(count($getdetails) > 0){		
			foreach($getdetails as $getdetail){
				$info = unserialize($getdetail['OpenOrder']['general_info']);
				$data['id'] = $getdetail['OpenOrder']['id'];
				$data['amazon_order_id'] = $info->ReferenceNum;
				$this->OpenOrder->saveAll($data);
			}
		}else{
			echo 'no order found.';
		}	
		exit;
		
	}
	
	public function assignStatus_060318()
	{
		date_default_timezone_set('Europe/Jersey');
		$date = date('Y-m-d');
		$this->loadModel('AmazonRecord');
		$this->loadModel('OpenOrder');
		$getDatas = $this->AmazonRecord->find( 'all', array( 'conditions' => array('AmazonRecord.upload_date' =>  $date) ) );
		if( count($getDatas) > 0 )
		{
			foreach( $getDatas as $getData )
			{
				$orderid		=	$getData['AmazonRecord']['order_id'];
				$getdetails	=	$this->OpenOrder->find( 'first', array(
						'conditions' => array( 'OpenOrder.general_info like ' => "%{$orderid}%" ),
						'fields' => array( 'OpenOrder.status' )	
						)
					);
				if( count($getdetails) > 0 )
				{
					$status = $getdetails['OpenOrder']['status'];
					if( $status == 0 ) {
						$value = "Open";
					} elseif ( $status == 1 ) { 
						$value = "Processed";
					} elseif( $status == 2  ) {
						$value = "Cancelled";
					} elseif( $status == 3  ) {
						$value = "Locked";
					}
				}
				else
				{
					$value = "Please check";
				}
				$data['id'] 	= 	$getData['AmazonRecord']['id'];
				$data['status']	=	$value;
				$this->AmazonRecord->saveAll( $data );
				
			}
			echo "Status Assign Successfully.";
			exit;
		} else {
			echo "There is no order for assign status.";
			exit;
			}
		
	}
	
	
	public function processedOrderReport()
	{
		$this->layout = 'index';
		$this->loadModel('User');
		$getUsers 	= 	$this->User->find('all', array( 'conditions' => array( 'User.country' => '1' ), 'fields' => array( 'DISTINCT User.first_name', 'User.last_name') ) );
		$this->set( 'getUsers', $getUsers );
	}
	
	public function getOperaterDetail()
	{
		$this->layout ='index';
		$this->autorender = false;
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('User');
		
		$this->request->data['getFrom'];
		$this->request->data['getEnd'];
		
		$getFrom 	= date('Y-m-d',strtotime($this->request->data['getFrom']));
		$getEnd 	= date('Y-m-d',strtotime($this->request->data['getEnd']));
		
		$path = WWW_ROOT.'img/Sku_report/user_report';
		$header = "OrderID,Split Order ID, User Name, System, Process Date\r\n";
		file_put_contents($path."/user_report.csv",$header, LOCK_EX);
		
		$getOpenOrders = $this->OpenOrder->find( 'all', array( 
																'conditions' => array( 
																				'OpenOrder.status' => '1', 
																				'OpenOrder.process_date >=' => $getFrom.' 00:00:00', 
																				'OpenOrder.process_date <=' => $getEnd.' 23:59:59' 
																					),
																'fields' => array('OpenOrder.num_order_id', 'OpenOrder.process_date')
															 ) 
												);
		
		foreach( $getOpenOrders as $getOpenOrder )
		{
			$processDate	=	$getOpenOrder['OpenOrder']['process_date'];
			$getSplitOrders	= 	$this->MergeUpdate->find('list', array(
																	'conditions' => array(
																					'MergeUpdate.status' => '1',
																					'MergeUpdate.order_id' => $getOpenOrder['OpenOrder']['num_order_id'],
																						  ),
																	'fields' => array('product_order_id_identify')
																	) 
													 );
			foreach($getSplitOrders as $orderId => $orderValue)
			{
				$getOrders	= $this->MergeUpdate->find('first', array(
																	'conditions' => array(
																					'MergeUpdate.product_order_id_identify' => $orderValue,
																						  ),
																	'fields' => array('order_id','product_order_id_identify','user_id')
																	) 
													 );
				$order 			= 	$getOrders['MergeUpdate']['order_id'];
				$splitorderid 	= 	$getOrders['MergeUpdate']['product_order_id_identify'];
				$userid			=	$getOrders['MergeUpdate']['user_id'];
				$userdetail		=	$this->getUserDetail( $userid );
				$firstName		=	($userdetail['User']['first_name'] != '')? $userdetail['User']['first_name'] : '--';
				$lastName		=	($userdetail['User']['last_name'] != '' )?$userdetail['User']['last_name'] : '--';
				$userName		=	$firstName.' '.$lastName;
				$pcName			=	($userdetail['User']['pc_name'] != '') ? $userdetail['User']['pc_name'] : '---';
				
				$content = $order.",".$splitorderid.",".$userName.",".$pcName.",".$processDate."\r\n";
				file_put_contents($path."/user_report.csv",$content, FILE_APPEND | LOCK_EX);
			}
		}
		echo "1";
		exit;
	}
	
	public function getUserDetail( $userID = null )
	{
		$userDeatil = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userID ), 'fields' => array( 'first_name', 'last_name', 'pc_name' ) ) );
		if( count($userDeatil) == 1 ){
				$data['User']['first_name'] = $userDeatil['User']['first_name'];
				$data['User']['last_name'] = $userDeatil['User']['last_name'];
				$data['User']['pc_name'] = $userDeatil['User']['pc_name'];
			} else {
				$data['User']['first_name'] = '--';
				$data['User']['last_name'] = '--';
				$data['User']['pc_name'] = '--';
			}
			return $data;
	}
	
	public function skuSellGraph( $sku = null) 
	{
		$this->layout ='index';
		$this->autorender = false;
	}
	
	public function getGraphData()
	{
		$this->layout ='';
		$this->autoRender = false;
		$this->layout = 'index';
		$this->loadModel('Skumapping');
		$this->loadModel('ChannelDetail');
		$this->loadModel('Product');
		$sku  = $this->request->data['sku'];
		$sku = trim(str_replace('_h_','#',$sku));
		//$sku = 'S-100151#ASB';
		
		$getSelectedAsins 	=	$this->Skumapping->find('all', array( 'conditions' => array( 'Skumapping.sku' =>  "$sku" ) ) );
		$getAllOrders = $this->ChannelDetail->find( 'all', array( 
																'conditions' => array( 'ChannelDetail.sku' =>  "$sku"),
																'fields' => array('SUM(quantity) AS Qty','sub_source','channelSKU','sku','DATE(order_date) OrderDate'),
																'group'	 => array('OrderDate'),
																'order' => 'OrderDate DESC',
																'limit' => '30'
																 )  );
		$qty = array();
		$orderDate = array();
		foreach($getAllOrders as $getAllOrder)
		{
			$qty[]			=	(int)$getAllOrder[0]['Qty'];
			$orderDate[] 	= 	date('d',strtotime($getAllOrder[0]['OrderDate']));
		}									
		echo json_encode(array( 'quantity' => array_reverse($qty), 'orderdate' => array_reverse($orderDate)));
		exit; 
		
	}
	
	public function showMasterAsin( $skuasin = null )
	{
		$this->layout = 'index';
		$this->loadModel('MasterSku');
		
		if( $skuasin != '' )
		{
			$skuasin = str_replace('_h_','#',$skuasin);
			$this->paginate = array(
								'conditions' => array('OR'=> array('MasterSku.sku' => $skuasin,'MasterSku.channelsku' => $skuasin,'MasterSku.asin' => $skuasin)),
                        		'limit' => 750			
			);
		}
		else
		{
			$this->paginate = array('limit' => 750);
		}
		
		//$this->paginate = 	array('limit' => 500);
		$getAllSkuAsins = 	$this->paginate('MasterSku');
		$this->set( 'getAllSkuAsins', $getAllSkuAsins );
	}
	
	public function deleteSkuAsin()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('MasterSku');
		$id	=	$this->request->data['id'];
		$this->MasterSku->delete( $id );
		echo "1";
		exit;
	}
	
	public function editSkuAsinPopup()
	{
		$this->loadModel('MasterSku');
		$id		=	$this->request->data['id'];
		$getMasterSku	=	$this->MasterSku->find('first', array('conditions' => array( 'MasterSku.id' => $id ) ) );
		$this->set('getMasterSku', $getMasterSku);
		$this->render( 'getskuEditPopup' );
	}
	
	public function editMasterSkuAsin()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('MasterSku');
		
		$marterData['id']			=	$this->request->data['id'];
		$marterData['sku']			=	$this->request->data['sku'];
		$marterData['channel']		=	$this->request->data['channel'];
		$marterData['channelsku']	=	$this->request->data['channelSku'];
		$marterData['asin']			=	$this->request->data['asin'];
		$marterData['title']		=	$this->request->data['title'];
		
		$this->MasterSku->saveAll($marterData);
		
		echo $this->request->data['id'];
		exit;
		
	}
	
	public function generateUserReport()
	{
		$this->loadModel('Product');
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Sku'); 
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Title');    
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Category');   
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Brand');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Listing User');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Purchase User');
		$getAllProducts	=	$this->Product->find('all', array( 'conditions' => array( 'is_deleted' => 0 ) ) );
	
		$i = 2;
		foreach( $getAllProducts as $getAllProduct )
		{
			$sku		=	$getAllProduct['Product']['product_sku'];
			$name		=	$getAllProduct['Product']['product_name'];
			$category	=	$getAllProduct['Product']['category_name'];
			$brand		=	$getAllProduct['ProductDesc']['brand'];

			$listingUser	=	$this->getUserName( $getAllProduct['ProductDesc']['user_id'] );
			$purchaseUser	=	$this->getUserName( $getAllProduct['ProductDesc']['purchase_user_id'] );
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $sku); 
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $name);       
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $category);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $brand);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $listingUser);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $purchaseUser);
			$i++;
		}
		
		$objPHPExcel->getActiveSheet(0);
		$getBase 		= 	Router::url('/', true);
		$uploadUrl 		= 	WWW_ROOT .'img/Sku_report/master_sku/user_report.xlsx';
		$uploadRemote 	= 	$getBase.'img/Sku_report/master_sku/user_report.xlsx';
		$objWriter 		= 	PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save($uploadUrl);
		$file = 'img/Sku_report/master_sku/user_report.xlsx';
		
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
		
	}
	
	
	    
}

?>
