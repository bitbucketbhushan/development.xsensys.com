<?php
class SortMdArrayController extends AppController
 {
 
	public $sort_order = 'asc'; // default
	public $sort_key = 'position'; // default
	var $name = "SortMdArray";
	var $components = array('Session','Upload','Common','Auth');
    var $helpers = array('Html','Form','Common','Session');
	 
	public function sortByKey(&$array) {       
		usort($array, array(__CLASS__, 'sortByKeyCallback'));     
	}
	 
	public function sortByKeyCallback($a, $b) {
	$return='';
		if($this->sort_order == 'asc') {
			$return = $a[$this->sort_key] - $b[$this->sort_key];
		} else if($this->sort_order == 'desc') {
			$return = $b[$this->sort_key] - $a[$this->sort_key];
		}
		return $return;
	} 
	
	public function getPaginate($item_per_page = NULL, $current_page = NULL, $total_records = NULL, $total_pages = NULL, $data_name = false, $data_value=false, $sort_by=false,$url = NULL )
		{
			$pagination = '';
			if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
				$pagination .= '<ul class="pagination">';
				
				$right_links    = $current_page + 5; 
				$previous       = $current_page - 5; //previous link 
				$next           = $current_page + 1; //next link
				$first_link     = true; //boolean var to decide our first link
				
				if($current_page > 1){
					$previous_link = ($previous > 0)? $previous : 1;
					$pagination .= '<li class="first"><a href="'.$url.'1" data-page="1" data-name="'.$data_name.'" order-by="'.$sort_by.'" data-value="'.$data_value.'" title="First">&laquo;</a></li>'; //first link
					$pagination .= '<li><a href="'.$url.$previous_link.'" data-page="'.$previous_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Previous">&lt;</a></li>'; //previous link
						for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
							if($i > 0){
								$pagination .= '<li><a href="'.$url.$i.'" data-page="'.$i.'"  data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Page'.$i.'">'.$i.'</a></li>';
							}
						}   
					$first_link = false; //set first link to false
				}
				
				if($first_link){ //if current active page is first link
					$pagination .= '<li class="first active"><span>'.$current_page.'</span></li>';
				}elseif($current_page == $total_pages){ //if it's the last active link
					$pagination .= '<li class="last active"><span>'.$current_page.'</span></li>';
				}else{ //regular current link
					$pagination .= '<li class="active"><span>'.$current_page.'</span></li>';
				}
						
				for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
					if($i<=$total_pages){
						$pagination .= '<li><a href="'.$url.$i.'" data-page="'.$i.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Page '.$i.'">'.$i.'</a></li>';
					}
				}
				if($current_page < $total_pages){ 
						$next_link = ($i > $total_pages) ? $total_pages : $i;
						$pagination .= '<li><a href="'.$url.$next_link.'" data-page="'.$next_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'" data-value="'.$data_value.'" title="Next">&gt;</a></li>'; //next link
						$pagination .= '<li class="last"><a href="'.$url.$next_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
				}
				
				$pagination .= '</ul>'; 
			}
			return $pagination; //return pagination links
		}
} 
 