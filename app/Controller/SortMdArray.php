<?php
class SortMdArray {
	public $sort_order = 'asc'; // default
	public $sort_key = 'position'; // default
	 
	public function sortByKey(&$array) {       
		usort($array, array(__CLASS__, 'sortByKeyCallback'));     
	}
	 
	function sortByKeyCallback($a, $b) {
	$return='';
		if($this->sort_order == 'asc') {
			$return = $a[$this->sort_key] - $b[$this->sort_key];
		} else if($this->sort_order == 'desc') {
			$return = $b[$this->sort_key] - $a[$this->sort_key];
		}
		return $return;
	} 
} 
 