<?php

class ProcessordersController extends AppController
{
    
    var $name = "Processorders";
    
    var $components = array('Session','Upload','Common','Auth');
   
    var $helpers = array('Html','Form','Common','Session');
    
    public function index()
    {
       
		$this->loadModel( 'MergeUpdate' );
		$this->layout = 'index';
		$perPage	=	Configure::read( 'perpage' );
		
		$emails = array('jake.shaw@euracogroup.co.uk','mmarecky82@gmail.com','lalit.prasad@jijgroup.com','shashi.b.kumar@jijgroup.com','amit.gaur@jijgroup.com','avadhesh.kumar@jijgroup.com');
		
		if(in_array($this->Session->read('Auth.User.email'),$emails)){
		
		if(isset($_REQUEST['searchkey']) && $_REQUEST['searchkey']){
				$conditions =  array('MergeUpdate.status' => '1', 'MergeUpdate.product_order_id_identify LIKE ' => $_REQUEST['searchkey'].'%') ;
			}else{
				$conditions =  array('MergeUpdate.status' => '1')  ;
			}
			
		}else{
			if(isset($_REQUEST['searchkey']) && $_REQUEST['searchkey']){
				$conditions =  array('MergeUpdate.status' => '1','MergeUpdate.user_id' => $this->Session->read('Auth.User.id'), 'MergeUpdate.product_order_id_identify LIKE ' => $_REQUEST['searchkey'].'%') ;
			}else{
				$conditions =  array('MergeUpdate.status' => '1', 'MergeUpdate.user_id' => $this->Session->read('Auth.User.id'))  ;
			}
		}
	  		
		$this->paginate = array(
						'conditions' => $conditions,
                        'fields' => array( 
                                'MergeUpdate.*'
                            ),
						'order' => 'process_date DESC'	,
						'limit' => $perPage			
		);
		$Processorders = $this->paginate('MergeUpdate'); 
		$this->set( 'Processorders', $Processorders );
	
    }
    
    public function showAllProcessOrders() 
    {
		$this->layout = "index";
		
		$this->loadModel( 'OpenOrder' );
		$perPage	=	Configure::read( 'perpage' );

		$this->paginate = array(
						'conditions' => array(
							'OpenOrder.status' => '1'
							),
                        'fields' => array( 
                                'OpenOrder.*'
                            ),
						'limit' => $perPage			
		);
		 
		//$this->Product->recursive = 7;
		
		// we are using the 'Product' model
		$getAllProcessOrders = $this->paginate('OpenOrder');
		
		$i = 0;
		if( count( $getAllProcessOrders ) > 0 )
		{
			foreach($getAllProcessOrders as $getAllProcessOrder )
			{
				$processOrderDetail[$i]['order_id']			= $getAllProcessOrder['OpenOrder']['order_id'];
				$processOrderDetail[$i]['num_order_id']		= $getAllProcessOrder['OpenOrder']['num_order_id'];
				$processOrderDetail[$i]['general_info']		= unserialize($getAllProcessOrder['OpenOrder']['general_info']);
				$processOrderDetail[$i]['shipping_info']	= unserialize($getAllProcessOrder['OpenOrder']['shipping_info']);
				$processOrderDetail[$i]['customer_info']	= unserialize($getAllProcessOrder['OpenOrder']['customer_info']);
				$processOrderDetail[$i]['totals_info']		= unserialize($getAllProcessOrder['OpenOrder']['totals_info']);
				$processOrderDetail[$i]['totals_info']		= unserialize($getAllProcessOrder['OpenOrder']['totals_info']);
				$processOrderDetail[$i]['items']			= unserialize($getAllProcessOrder['OpenOrder']['items']);
				$processOrderDetail[$i]['date']				= $getAllProcessOrder['OpenOrder']['date'];
				$i++;
			}
			$this->set('processOrderDetails', $processOrderDetail);
		}
		$this->set( 'countprocess', count($this->OpenOrder->find( 'all', array( 'conditions' => array('OpenOrder.status' => '1' ) ) ) ) );
		$this->set('title', 'Processed Orders');
		
	}
	public function getUserDetail( $id = null )
	{
		$this->loadModel( 'User' );
		$userDetail	=	$this->User->find( 'first',  array( 'conditionds' => array('User.id' => $id) ) );
		$fullName	=	$userDetail['User']['first_name'].' '.$userDetail['User']['last_name'];
		if( $fullName )
		{
			return $fullName;
		}
		else
		{
			return "---";
		}
	}
	
	public function showAllsplitOrders()
	{
		$this->loadModel( 'MergeUpdate' );
		$this->layout = 'index';
		$perPage	=	Configure::read( 'perpage' );
		
		$this->paginate = array(
						'conditions' => array(
							'MergeUpdate.status' => '1'
							),
                        'fields' => array( 
                                'MergeUpdate.*'
                            ),
						'limit' => $perPage			
		);
		$getAllSplitOrders = $this->paginate('MergeUpdate');
		$this->set( 'countSplitOrder', count($this->MergeUpdate->find( 'all', array( 'conditions' =>array('MergeUpdate.status' => '1') ) ) ) );
		$this->set( 'title', 'Split Order' );
		$this->set( 'getAllSplitOrders', $getAllSplitOrders );
	}
	
	public function showAllUnprepareOrders()
	{
		
		$this->layout = "index";
		
		$this->loadModel( 'UnprepareOrder' );
		$perPage	=	Configure::read( 'perpage' );

		$this->paginate = array(
						'conditions' => array(
								'status' => '0'
							),
					    'fields' => array( 
                                'UnprepareOrder.*'
                            ),
						'limit' => $perPage			
		);
		 
		//$this->Product->recursive = 7;
		
		// we are using the 'Product' model
		$getAllProcessOrders = $this->paginate('UnprepareOrder');
		$i = 0;
		if( count( $getAllProcessOrders ) > 0 )
		{
			foreach($getAllProcessOrders as $getAllProcessOrder )
			{
				$processOrderDetail[$i]['order_id']			= $getAllProcessOrder['UnprepareOrder']['order_id'];
				$processOrderDetail[$i]['num_order_id']		= $getAllProcessOrder['UnprepareOrder']['num_order_id'];
				$processOrderDetail[$i]['general_info']		= unserialize($getAllProcessOrder['UnprepareOrder']['general_info']);
				$processOrderDetail[$i]['shipping_info']	= unserialize($getAllProcessOrder['UnprepareOrder']['shipping_info']);
				$processOrderDetail[$i]['customer_info']	= unserialize($getAllProcessOrder['UnprepareOrder']['customer_info']);
				$processOrderDetail[$i]['totals_info']		= unserialize($getAllProcessOrder['UnprepareOrder']['totals_info']);
				$processOrderDetail[$i]['totals_info']		= unserialize($getAllProcessOrder['UnprepareOrder']['totals_info']);
				$processOrderDetail[$i]['items']			= unserialize($getAllProcessOrder['UnprepareOrder']['items']);
				$processOrderDetail[$i]['date']				= $getAllProcessOrder['UnprepareOrder']['date'];
				$processOrderDetail[$i]['unprepare_status']	= $getAllProcessOrder['UnprepareOrder']['unprepare_status'];
				$i++;
			}
			$this->set('processOrderDetails', $processOrderDetail);
		}
		$this->set( 'countprocess', count($this->UnprepareOrder->find( 'all' )));
		$this->set('title', 'Unprepare Orders');
	}
	
	

    
    
}

?>
