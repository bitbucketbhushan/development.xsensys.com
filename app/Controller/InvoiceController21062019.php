<?php
class InvoiceController extends AppController
{
     var $name = "Invoice";
    
    var $helpers = array('Html','Form','Session');
    
	public function beforeFilter()
    {
		parent::beforeFilter();
		$this->layout = false; 
		$this->Auth->Allow(array('Generate'));			
				
	}
	
	public function index()
    {
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
		$html .= '<body>'.$htmlTemplate.'</body>';
				
		$name	= 'Invoice.pdf';							
		$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
     
		echo $r = 'Юлия Свеженцева';
		echo "<br>=====";
		echo utf8_encode($r);
		echo "<br>=====";
		echo mb_convert_encoding($r, 'HTML-ENTITIES','UTF-8');
		exit;
		
	}
	
    public function Generate($OrderId = null)
    {
        
		$this->layout = '';
		$this->autoRender = false;	
		
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();	
		$result 		=	'';
		$htmlTemplate	=	'';				
				
		//$OrderId	= 	$this->request->data['order_id'];
		$order			=	$this->getOrderByNumId( $OrderId );	
		//	pr($order);exit;
		if(is_object($order)){			
			$htmlTemplate	=	$this->getTemplate($order);
			$SubSource		=	$order->GeneralInfo->SubSource;
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4)));
			
			/**************** for tempplate *******************/
			$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				 <meta content="" name="description"/>
				 <meta content="" name="author"/>
				 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
			$html .= '<body>'.$htmlTemplate.'</body>';
					
			$name	= 'Invoice.pdf';							
			$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
			//$html_t = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");
			//$dompdf->load_html($html_t);
			
			//$dompdf->load_html(iconv("UTF-8", "CP1252", $html));
			 
			$dompdf->render();			
			//$dompdf->stream($name);
							
			$file_to_save = WWW_ROOT .'img/invoice_template/'.$name;
			//save the pdf file on the server
			file_put_contents($file_to_save, $dompdf->output()); 
			//print the pdf file to the screen for saving
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="'.$name.'"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . filesize($file_to_save));
			header('Accept-Ranges: bytes');
			readfile($file_to_save);
		
		}else{
			echo 'Invalid Order.';
		}
		//echo json_encode($msg);	
		exit;
		
    }
		
	public function getOrderByNumId_openorder($pkOrderId = null)
	{
		$this->loadModel('OpenOrder');	
		$order 	=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id' => $pkOrderId)));
		$data['NumOrderId']    = $pkOrderId;
		$data['GeneralInfo']   = unserialize($order['OpenOrder']['general_info']);
		$data['ShippingInfo']  = unserialize($order['OpenOrder']['shipping_info']);
		$data['CustomerInfo']  = unserialize($order['OpenOrder']['customer_info']);
		$data['TotalsInfo']    = unserialize($order['OpenOrder']['totals_info']);
		$data['Items'] 		   = unserialize($order['OpenOrder']['items']);
		
		if( count($data['CustomerInfo']->BillingAddress) == 0){
			$data['CustomerInfo']->BillingAddress  = $data['CustomerInfo']->Address;
		}		 
		return json_decode(json_encode( $data ),0);	
		
	}
		
	public function getOrderByNumId($pkOrderId = null)
	{
		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
	
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
		//$multi = AuthMethods::Multilogin($username, $password);		
		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

		$token = $auth->Token;	
		$server = $auth->Server;		
		$order	= OrdersMethods::GetOrderDetailsByNumOrderId($pkOrderId,$token, $server);
		//pr($order	);
		//exit;
		return $order;	
		
	}
	
	public function getOrderByNumIdDhl($pkOrderId = null)
	{
		 
		//App::import('Vendor', 'Linnworks/src/php/Orders');
		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
		
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
		//$multi = AuthMethods::Multilogin($username, $password);		
		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

		$token = $auth->Token;	
		$server = $auth->Server;		
		$order	= OrdersMethods::GetOrderDetailsByNumOrderId($pkOrderId,$token, $server);
		
		return $order;	
		
	}
	public function getTemplate($order = null )
	{
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;
		
		
		$this->loadModel('InvoiceAddress');		
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
			$BILLING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName)).'<br>';
			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$order->CustomerInfo->BillingAddress->Country ? $order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
			
			$SHIPPING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName)).'<br>';
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $order->CustomerInfo->Address->Country ? $order->CustomerInfo->Address->Country : '';
			}			
		}		
		
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal.'&nbsp;'.$Currency;
		$POSTAGE		  = $order->TotalsInfo->PostageCost.'&nbsp;'.$Currency;
		$TAX			  = $order->TotalsInfo->Tax.'&nbsp;'.$Currency;
		$TOTAL			  = $order->TotalsInfo->TotalCharge.'&nbsp;'.$Currency;
		$INVOICE_DATE	  = date('Y-M-d', strtotime($order->GeneralInfo->ReceivedDate));
		$templateHtml 	  = NULL;
					
		if($result == 'cost'){
			$templateHtml ='<!-- CostBreaker Invoice -->';			
			$templateHtml .='<div id="label">';
			$templateHtml .='<div class="container">';	
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top" style="font-size:15px;">
				EURACO GROUP LIMITED, 36 HARBOUR REACH<br>LA RUE DE CARTERET, ST HELIER<br>JERSEY, JE2 4HR</td>';
			$templateHtml .='<td  width="25%" valign="top" >';
			$templateHtml .='<span style="font-weight:bold; font-size:30px">INVOICE</span><br>';
			$templateHtml .='<b>Invoice Number:</b>'.$OrderId.'<br><b>Date:</b> '.$INVOICE_DATE;
			$templateHtml .='</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';	
			$templateHtml .='<td width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';	
			$templateHtml .='</tr>';	
			$templateHtml .='</table>';
	
			$templateHtml  .='<div class="tablesection row" style="margin:5px 0 0 0; border-bottom:0px;height:700px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
				foreach($items as $item){
					
				  $templateHtml .='<tr>
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$item->Title.'</td>
					   <td style="" valign="top" >'.$item->Quantity.'</td>
					   <td style="" valign="top" >'.$item->PricePerUnit.'</td>
					   <td style="" valign="top" >'.$item->TaxRate.'</td>
					   <td style="" valign="top" >'.$item->Tax.'</td>
					   <td style="" valign="top" >'.$item->Cost.'</td>
					   <td style="" valign="top" >'.$item->CostIncTax.'</td>
					</tr>';            
       
				}
			$templateHtml .= '</table>';
	
			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" rowspan="4" width="60%"></th>';
			$templateHtml .= '<th style="" align="left" width="25%">SUB TOTAL:</th>';	
			$templateHtml .= '<td style="" align="right" width="15%">'.$SUB_TOTAL.'</td>';
			$templateHtml .= '</tr>';
	
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" >POSTAGE(Ex TAX):</th>';
			$templateHtml .='<td style="" align="right">'.$POSTAGE.'</td>';	
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" >TAX:</th>';	
			$templateHtml .='<td style="" align="right">'.$TAX.'</td>';	
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" >TOTAL:</th>';	
			$templateHtml .='<td style="" align="right">'.$TOTAL.'</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';			
			$templateHtml .='</div>';
			
			$templateHtml .='<div class="footer row" style="text-align:center; border-bottom:0px;">
			<b>Thank you for your purchase. Happy Shopping!</div>';
			$templateHtml .='</div>';
		
		}
		elseif($result == 'mare'){
			$templateHtml ='<!-- eBuyer Express(Marec) Invoice -->';			
			$templateHtml .='<div id="label" style="margin-top:20px;">';
			$templateHtml .='<div class="container">';			
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top"><img src=http://xsensys.com/img/ebuyerexpress.jpg width="380px"></td>';
			$templateHtml .='<td  width="25%" valign="top">
			THIRD FLOOR<br>
			40 ESPLANADE<br>
			ST HELIER UNITED KINGDOM<br>
			JE4 9RJ			
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="37%"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml .='<td valign="top" width="38%"><h4><u>Billing Address:</u></h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';

			$templateHtml .='<td  width="25%" valign="top">
			<table>
			<tr><td><b>Invoice ID:</b></td>	<td>'.$OrderId.'</td></tr>
			<tr><td><b>Invoice Date:</b></td><td>'.$INVOICE_DATE.'</td></tr>
			</table>
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<div style="font-weight:bold; font-size:30px; text-align:center">INVOICE</div>';
			$templateHtml .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:600px">';
			
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
			foreach($items as $count => $item){
		    
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				}else{
					$b = '';
				}					
				
			  $templateHtml .='<tr>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->SKU.'</td>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->Title.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Quantity.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->PricePerUnit.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->TaxRate.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Tax.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Cost.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->CostIncTax.'</td>
				</tr>';            
       
			}
			$templateHtml .= '</table>';
			
			$templateHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$templateHtml .='<tr>
			<th style="border-top:2px solid #000;" align="left" rowspan="4" width="60%"></th>
			<th style="border-top:2px solid #000;" align="left" width="25%">SUB TOTAL:</th>
			<td style="border-top:2px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>
			</tr>';
			
			$templateHtml .='<tr>
			<th style="" align="left">POSTAGE(Ex TAX):</th>
			<td style="" align="right">'.$POSTAGE.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TAX:</th>
			<td style="" align="right">'.$TAX.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TOTAL:</th>
			<td style="" align="right">'.$TOTAL.'</td>
			</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='</div>';
			
			$templateHtml .='<div class="footer row" style="font-size:15px; text-align:center; border-bottom:0px;" >
			<h2>THANK YOU FOR YOUR PURCHASE</h2> 
			<b>Please take a moment to leave us positive feedback.</b><br>
			If however, for any reason you are not entirely happy with your
			order, please contact us<br>to allow us to rectify any issues before
			leaving feedback.</div>';			
			$templateHtml .='</div>';
					
		}
		elseif($result == 'rain'){
			
			$templateHtml ='<!-- Rainbow Invoice -->';			
			$templateHtml .='<div id="label">';
			$templateHtml .='<div class="container">';
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="60%" style="text-align:center; font-weight:bold; font-size:28px">INVOICE</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="50%" valign="top">';
			$templateHtml .='<img src=http://xsensys.com/img/rainbow_logo.png width="250px">';
			$templateHtml .='</td>';			
			$templateHtml .='<td  width="50%" valign="top" style="text-align:right">
				<b>FRESHER BUSINESS LIMITED</b><br>
				BEACHSIDE BUSINESS CENTRE<br>
				RUE DU HOCQ<br>
				ST CLEMENT<br>
				JERSEY<br>
				JE2 6LF
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<table cellpadding="3" style="background-color:#333; color:#fff; margin-bottom:20px;" class="topborder bottomborder"><tr>';
			$templateHtml .='<td><span class="bold">Date:</span> '.$INVOICE_DATE.'</td>';
			$templateHtml .='<td align="right"><span class="bold">Invoice ID:</span> '.$OrderId.'</td>';
			$templateHtml .='</tr></table>';
			
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  valign="top" width="50%"><h4 class="topborder bottomborder" style="background-color:#333; color:#fff; padding:3px">
			Billing Address:</h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml .='<td  width="50%" valign="top"><h4 class="topborder bottomborder" style="background-color:#333; color:#fff; padding:3px">
			Delivery Address:</h4>'.$SHIPPING_ADDRESS.'</td>';
			
			$templateHtml .='</tr>';		
			$templateHtml .='</table>';
	
			$templateHtml .='<div class="tablesection row" style="margin:5px 0 0 0; border-bottom:0px;height:680px">';
			$templateHtml .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse: collapse; margin:0px;">';
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" class="topborder bottomborder" width="13%">SKU</th>';
			$templateHtml .='<th style="" align="left" valign="top" width="19%" class="topborder bottomborder">Item</th>';
			$templateHtml .='<th style="" valign="top" class="topborder bottomborder" width="10%">Quantity</th>';
			$templateHtml .='<th style="" align="left" class="topborder bottomborder" width="8%">Unit</th>';
			$templateHtml .='<th style="" align="left" valign="top" width="10%" class="topborder bottomborder">Tax Rate</th>';
			$templateHtml .='<th style="" valign="top" class="topborder bottomborder" width="8%">Tax</th>';
			$templateHtml .='<th style="" align="left" class="topborder bottomborder" width="12%">Cost (ex, Tax)</th>';
			$templateHtml .='<th style="" align="left" valign="top" width="10%" class="topborder bottomborder">Line Cost</th>';
			$templateHtml .='</tr>';
				foreach($items as $item){
				  $templateHtml .='<tr>
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$item->Title.'</td>
					   <td style="" valign="top" >'.$item->Quantity.'</td>
					   <td style="" valign="top" >'.$item->PricePerUnit.'</td>
					   <td style="" valign="top" >'.$item->TaxRate.'</td>
					   <td style="" valign="top" >'.$item->Tax.'</td>
					   <td style="" valign="top" >'.$item->Cost.'</td>
					   <td style="" valign="top" >'.$item->CostIncTax.'</td>
					</tr>';            
				
				}
			$templateHtml .='</table>';
		
			$templateHtml .='<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" width="60%" class="topborder"></th>';
			$templateHtml .='<th style="" align="left" width="25%" class="topborder">SUB TOTAL:</th>';
			$templateHtml .='<td style="" align="right" width="15%" class="topborder">'.$SUB_TOTAL.'</td>';
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td></td>';
			$templateHtml .='<th style="" align="left">POSTAGE(Ex TAX):</th>';
			$templateHtml .='<td style="" align="right">'.$POSTAGE.'</td>';
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td></td>';
			$templateHtml .='<th style="" align="left" >TAX:</th>';
			$templateHtml .='<td style="" align="right">'.$TAX.'</td>';
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td></td>';
			$templateHtml .='<th style="" align="left" class="topborder bottomborder">TOTAL:</th>';
			$templateHtml .='<td style="" align="right" class="topborder bottomborder">'.$TOTAL.'</td>';
			$templateHtml .='</tr>';			
			$templateHtml .='</table>';			
			$templateHtml .='</div>';
	
			$templateHtml  .= '<div class="footer row" style="text-align:center; border-bottom:0px;" >
				<b>Thank you for your purchase. Happy Shopping!<br>
				Rainbow Retail</b>';	
			 $templateHtml  .= '</div>';
		}
		elseif($result == 'bbd_'){		
			$templateHtml ='<!-- BBD_EU Invoice -->';			
			$templateHtml .='<div id="label" style="margin-top:20px;">';
			$templateHtml .='<div class="container">';			
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top"><img src=http://xsensys.com/img/bbdEu.png></td>';
			$templateHtml .='<td  width="25%" valign="top">			
								Unit A1 21/F, Officeplus Among Kok<br>
								998 Canton Road<br>
								Hong Kong<br>
								KL<br>
								0000			
							</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="37%"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml .='<td valign="top" width="38%"><h4><u>Billing Address:</u></h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';

			$templateHtml .='<td  width="25%" valign="top">
			<table>
			<tr><td><b>Invoice ID:</b></td>	<td>'.$OrderId.'</td></tr>
			<tr><td><b>Invoice Date:</b></td><td>'.$INVOICE_DATE.'</td></tr>
			</table>
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<div style="font-weight:bold; font-size:30px; text-align:center">INVOICE</div>';
			$templateHtml .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:600px">';
			
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
			foreach($items as $count => $item){
		    
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				}else{
					$b = '';
				}					
				
			  $templateHtml .='<tr>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->SKU.'</td>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->Title.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Quantity.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->PricePerUnit.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->TaxRate.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Tax.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Cost.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->CostIncTax.'</td>
				</tr>';            
       
			}
			$templateHtml .= '</table>';
			
			$templateHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$templateHtml .='<tr>
			<th style="border-top:2px solid #000;" align="left" rowspan="4" width="60%"></th>
			<th style="border-top:2px solid #000;" align="left" width="25%">SUB TOTAL:</th>
			<td style="border-top:2px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>
			</tr>';
			
			$templateHtml .='<tr>
			<th style="" align="left">POSTAGE(Ex TAX):</th>
			<td style="" align="right">'.$POSTAGE.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TAX:</th>
			<td style="" align="right">'.$TAX.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TOTAL:</th>
			<td style="" align="right">'.$TOTAL.'</td>
			</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='</div>';
			
			$templateHtml .='<div class="footer row" style="font-size:15px; text-align:center; border-bottom:0px;" >
			<h2>THANK YOU FOR YOUR PURCHASE</h2> 
			<b>Please take a moment to leave us positive feedback.</b><br>
			If however, for any reason you are not entirely happy with your
			order, please contact us<br>to allow us to rectify any issues before
			leaving feedback.</div>';			
			$templateHtml .='</div>';
					
		}
		elseif($result == 'tech'){
			$templateHtml  ='<!-- TechDrive Invoice -->';
			$templateHtml  .='<div id="label">';
			$templateHtml  .='<div class="container">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px"></table>';
			$templateHtml  .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  width="60%" valign="top">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml  .='<td  width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			$templateHtml  .='</td>';
			$templateHtml  .='<td  width="40%" valign="top" align="center">
			<img src=http://xsensys.com/img/techDriveSupplies2.jpg width="280px"></td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			
			$templateHtml  .='<table>';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td width="80%"><span style="font-weight:bold; font-size:32px">INVOICE #'.$OrderId.'</span><br></td>';
			$templateHtml  .='<td width="20%" align="right"><b>Date:</b> '.$INVOICE_DATE.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';

			$templateHtml  .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:650px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:3px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
				foreach($items as $item){
					
				  $templateHtml .='<tr style="background-color:#f2f2f2">
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$item->Title.'</td>
					   <td style="" valign="top" >'.$item->Quantity.'</td>
					   <td style="" valign="top" >'.$item->PricePerUnit.'</td>
					   <td style="" valign="top" >'.$item->TaxRate.'</td>
					   <td style="" valign="top" >'.$item->Tax.'</td>
					   <td style="" valign="top" >'.$item->Cost.'</td>
					   <td style="" valign="top" >'.$item->CostIncTax.'</td>
					</tr>';            
       
				}
			$templateHtml .= '</table>';		 
		
			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-top:1px solid #000;border-bottom:3px solid #000;" align="left" rowspan="4" width="60%">';		
			$templateHtml .= '<div style="border-bottom:0px;" ><b>Thank you for your purchase. Happy Shopping!<br>TechDrive Supplies</b></div>';
			$templateHtml .= '</th>';
			$templateHtml .= '<th style="border-top:1px solid #000;" align="left" width="25%">SUB TOTAL:</th>';
			$templateHtml .= '<td style="border-top:1px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>';
			$templateHtml .= '</tr>';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >POSTAGE(Ex TAX):</th>';
			$templateHtml .= '<td style="" align="right">'.$POSTAGE.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >TAX:</th>';
			$templateHtml .= '<td style="" align="right">'.$TAX.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-bottom:3px solid #000;" align="left" >TOTAL:</th>';
			$templateHtml .= '<td style="border-bottom:3px solid #000;" align="right">'.$TOTAL.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '</table>';
			$templateHtml .= '</div>';
				
			$templateHtml .= '<div class="row" style="text-align:center; border-bottom:0px;margin-left:20%; padding:0px" >';
			$templateHtml .= '<div style="width:70%; text-align:center; border:2px solid #000000; padding:10px;" >';
	
			$templateHtml .= '<table style="padding:0; text-align:center"><tr>
			<td width="15%"><img src="http://xsensys.com/img/happy_g.png" width="80px"></td>
			<td width="85%" align="center"><span style="font-size:24px">Happy with your order?</span> <br>
			<span style="font-size:10px;">Please take a moment to leave us positive feedback.</span><br>
			<img src="http://xsensys.com/img/5star_g.png" width="120px"><br></td></tr>
			</table>';
			$templateHtml .= '<div style="text-align:left; font-size:10px;">
			If however, for any reason you are not entirely happy with your order, please contact us to allow us to rectify any issues before leaving feedback.
			</div>';
			
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
		
		}		
		elseif($result == 'ebay'){			
			$templateHtml .='<!-- eBuyersDirect Invoice -->';			
			$templateHtml .='<div id="label" style="margin-top:20px;">';
			$templateHtml .='<div class="container">';			
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="50%" valign="top">
			4 Norwood Court<br>
La Rue Militaire<br>
St John, Jersey Channel Islands JE3 4DP<br>
United Kingdom
			</td>';
			$templateHtml .='<td  width="50%" valign="top" align="right">
			<img src=http://xsensys.com/img/ebuyerDirect.png width="300px">
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<hr style="color:#6bc15f">';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>'; 
			
			//mb_convert_encoding($SHIPPING_ADDRESS,'HTML-ENTITIES','UTF-8')			
			$t = mb_convert_encoding($SHIPPING_ADDRESS, "windows-1251", "utf-8");		
			$templateHtml .='<td  valign="top" width="37%"><h4>Delivery Address:</h4>'.$t.'</td>';
			$templateHtml .='<td  valign="top" width="38%"><h4>Billing Address:</h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';
			
			$templateHtml .='<td  width="25%" valign="top">
			<table>
			<tr>
			<td><b>Invoice ID:</b></td>
			<td align="right">'.$OrderId.'</td>
			</tr>
			<tr>
			<td><b>Invoice Date:</b></td>
			<td align="right">'.$INVOICE_DATE.'</td>
			</tr>
			</table>
			<div style="font-size:44px; font-weight:bold; text-align:right">INVOICE</div>
			</td>';
			
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			
			$templateHtml .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:625px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
			foreach($items as $count => $item){
		    
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				}else{
					$b = '';
				}					
				
			  $templateHtml .='<tr>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->SKU.'</td>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->Title.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Quantity.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->PricePerUnit.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->TaxRate.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Tax.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Cost.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->CostIncTax.'</td>
				</tr>';            
       
			}
			$templateHtml .= '</table>';

			$templateHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$templateHtml .='<tr>
			<th style="border-top:2px solid #000;" align="left" rowspan="4" width="60%"></th>
			<th style="border-top:2px solid #000;" align="left" width="25%">SUB TOTAL:</th>
			<td style="border-top:2px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>
			</tr>';

			$templateHtml .='<tr>
			<th style="" align="left" >POSTAGE(Ex TAX):</th>
			<td style="" align="right">'.$POSTAGE.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TAX:</th>
			<td style="" align="right">'.$TAX.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TOTAL:</th>
			<td style="" align="right">'.$TOTAL.'</td>
			</tr>';
			$templateHtml .='</table>';
			$templateHtml .='</div>';
			
			$templateHtml .='<hr style="color:#6bc15f">';
			$templateHtml .='<div class="footer row" style="font-size:15px; text-align:left; border-bottom:0px;" >
			<span style="font-size:28px; font-weight:bold">THANK YOU</span> <i>for using ebuyersDirect-2u</i><br><br>
			<b>Please take a moment to leave us positive feedback.</b><br>
			If however, for any reason you are not entirely happy with your
			order, please contact us to allow us to rectify<br>any issues before
			leaving feedback.</div>';
			$templateHtml .='</div>';
		}
		elseif($result == 'http'){		
			$templateHtml  ='<!-- storeforlife Invoice -->';			
			$templateHtml  .='<div id="label">';
			$templateHtml  .='<div class="container">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px"></table>';
			$templateHtml  .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  width="60%" valign="top">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml  .='<td  width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			$templateHtml  .='</td>';
			$templateHtml  .='<td  width="40%" valign="top" align="center">
			<img src="http://xsensys.com/img/sfl_logo.png"><br>			
			36 HARBOUR REACH, LA RUE DE CARTERET<br> ST HELIER, JERSEY, JE2 4HR</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			
			$templateHtml  .='<table>';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td width="80%"><span style="font-weight:bold; font-size:32px">INVOICE #'.$OrderId.'</span><br></td>';
			$templateHtml  .='<td width="20%" align="right"><b>Date:</b> '.$INVOICE_DATE.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';

			$templateHtml  .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:650px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:3px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
				foreach($items as $item){
					
				  $templateHtml .='<tr style="background-color:#f2f2f2">
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$item->Title.'</td>
					   <td style="" valign="top">'.$item->Quantity.'</td>
					   <td style="" valign="top">'.$item->PricePerUnit.'</td>
					   <td style="" valign="top">'.$item->TaxRate.'</td>
					   <td style="" valign="top">'.$item->Tax.'</td>
					   <td style="" valign="top">'.$item->Cost.'</td>
					   <td style="" valign="top">'.$item->CostIncTax.'</td>
					</tr>';            
       
				}
			$templateHtml .= '</table>';
		 
		
			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-top:1px solid #000;border-bottom:3px solid #000;" align="left" rowspan="4" width="60%">';		
			$templateHtml .= '<div style="border-bottom:0px;" ><b>Thank you for your purchase. Happy Shopping!<br>StoreForLife</b></div>';
			$templateHtml .= '</th>';
			$templateHtml .= '<th style="border-top:1px solid #000;" align="left" width="25%">SUB TOTAL:</th>';
			$templateHtml .= '<td style="border-top:1px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>';
			$templateHtml .= '</tr>';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >POSTAGE(Ex TAX):</th>';
			$templateHtml .= '<td style="" align="right">'.$POSTAGE.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >TAX:</th>';
			$templateHtml .= '<td style="" align="right">'.$TAX.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-bottom:3px solid #000;" align="left" >TOTAL:</th>';
			$templateHtml .= '<td style="border-bottom:3px solid #000;" align="right">'.$TOTAL.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '</table>';
			$templateHtml .= '</div>';
				
			$templateHtml .= '<div class="row" style="text-align:center; border-bottom:0px;margin-left:20%; padding:0px" >';
			$templateHtml .= '<div style="width:70%; text-align:center; border:2px solid #000000; padding:10px;" >';
	
			$templateHtml .= '<table style="padding:0; text-align:center"><tr>
			<td width="15%"><img src="http://xsensys.com/img/happy_g.png" width="80px"></td>
			<td width="85%" align="center"><span style="font-size:24px">Happy with your order?</span> <br>
			<span style="font-size:10px;">Please take a moment to leave us positive feedback.</span><br>
			<img src="http://xsensys.com/img/5star_g.png" width="120px"><br></td>
			</tr>
			</table>';
			$templateHtml .= '<div style="text-align:left; font-size:10px;">
			If however, for any reason you are not entirely happy with your order, please contact us to allow us to rectify any issues before leaving feedback.
			</div>';

			
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
			
		}
				
				
		return $templateHtml;	
	}
	
	public function getTemplateDHL($order = null,$waybill_number = null )
	{
		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdate');
		$this->loadModel('Product');	
		
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  		 		
			
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
			$BILLING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName)).'<br>';
			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$order->CustomerInfo->BillingAddress->Country ? $order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
			
			$SHIPPING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName)).'<br>';
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $order->CustomerInfo->Address->Country ? $order->CustomerInfo->Address->Country : '';
			}			
		}		
		
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 
		 
			
		$INVOICE_DATE	  = date('Y-M-d', strtotime($order->GeneralInfo->ReceivedDate));
		$templateHtml 	  = NULL;
		
		/*
			Date 11 MAR 2019 on recommendations of Lalit.
			For Over LVCR Limit in Invoice Format need to remove Tax Rate and Tax column and need to modify heading
			
		*/
		$lvcr_limit = 17.99; //gbp
		
		$final_total = $TOTAL ;
		if($Currency == 'GBP'){
			$final_total = $TOTAL * (1.122);
		}
		if($final_total < $lvcr_limit){
			$POSTAGE = 4;
		}
		$ShipmentWeight  = 0.0;
		$swresult = $this->MergeUpdate->find('first',['conditions' => ['order_id' => $OrderId],'fields'=>['packet_weight','envelope_weight']]);
		if(count( $swresult ) > 0){				
			$ShipmentWeight = $swresult['MergeUpdate']['packet_weight'] + $swresult['MergeUpdate']['envelope_weight'];
		}
		$country_code = $this->getCountryCode($Country);
		$tax_rate = ''; $vat_amount = 0;
		$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
		if(count( $thsresult ) > 0){				
			$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
		}
		
		$currency_code = $this->getCurrencyCode($Currency);
								
		if($result == 'cost'){
			
			$EORI_Number = 'GB019020854000';
			$UK_VAT_Number = 'GB 304984295'; 
			$DE_VAT_Number = 'DE 321777974';
			$FR_VAT_Number = ''; 
				
			//$templateHtml ='<!-- CostBreaker Invoice -->';			
			$templateHtml  ='<div id="label">';
			$templateHtml .='<div class="container">';	
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top" style="font-size:15px;">
				EURACO GROUP LIMITED, 36 HARBOUR REACH<br>LA RUE DE CARTERET, ST HELIER<br>JERSEY, JE2 4HR</td>';
			$templateHtml .='<td  width="25%" valign="top" >';
			$templateHtml .='<span style="font-weight:bold; font-size:20px">INVOICE(CIF)</span>';
			$templateHtml .='<br><b>Invoice Number:</b>'.$OrderId;
			$templateHtml .='<br><b>EORI Number:</b>'.$EORI_Number;
			if($Country == 'United Kingdom' && $UK_VAT_Number != ''){
				$templateHtml .='<br><b>VAT Number:</b>'.$UK_VAT_Number;
			}else if($Country == 'Germany' && $DE_VAT_Number != '' ){
				$templateHtml .='<br><b>VAT Number:</b>'.$DE_VAT_Number;
			}
			if($waybill_number != ''){
				$templateHtml .='<br><b>WayBill Number:</b>'.$waybill_number;
			}
			
			$templateHtml .='<br><b>Date:</b>'.$INVOICE_DATE;
			$templateHtml .='</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';	
			$templateHtml .='<td width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';	
			$templateHtml .='</tr>';	
			$templateHtml .='</table>';
	
			$templateHtml  .='<div class="tablesection row" style="margin:5px 0 0 0; border-bottom:0px;height:700px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">';
  			$country_of_origin = '';
			foreach($items as $count => $item){ 
					
				$Title = $item->Title;
				if(strlen($item->Title) < 10){
					$Title = $item->ChannelTitle;
				}
				$pro_weight = 0;  
				$pro_res = $this->Product->find('first', array('conditions' => ['Product.product_sku' => $item->SKU],'fields' => ['Product.country_of_origin','ProductDesc.weight']));
 				if(count( $pro_res ) > 0){				
					$pro_weight = $pro_res['ProductDesc']['weight'];
					$country_of_origin = $pro_res['Product']['country_of_origin'];
					 
				}
				$hs_code = '';
				$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $item->SKU,'country_code' => $country_code)));
				if(count( $hsresult ) > 0){				
					$hs_code = $hsresult['ProductHscode']['hs_code'];
				}
				if($count == 0){	
					  $templateHtml  .='<tr>';
						    $templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Item</th>';
							
							$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Qty</th>';
							$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Weight</th>';
						    if($hs_code != ''){
						   		 $templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">HSCode</th>';
							}
							 if($country_of_origin != ''){
						   		 $templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Country of origin</th>';
							}
							$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Price</th>';
							$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Tax Rate</th>';
  						   $templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Tax</th>
						   <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Line Total</th>
						</tr>';
				}
				
				  $item_vat_amount = $item->Cost - ($item->Cost/(1 + ($tax_rate/100)));
				  $item_cost = $item->Cost - $item_vat_amount;
				  
				  $templateHtml .='<tr>';
  				  $templateHtml .='   <td align="left" valign="top" class="">'. $Title  .'</td>'; 			  
				  $templateHtml .='   <td valign="top">'.$item->Quantity.'</td>'; 
				  $templateHtml .='   <td valign="top">'.($pro_weight * $item->Quantity).'</td>'; 	
  				  if($hs_code != ''){
					$templateHtml .='   <td valign="top">'.$hs_code.'</td>'; 
				  }	
				  if($country_of_origin != ''){
					$templateHtml .='   <td valign="top">'.$country_of_origin.'</td>'; 
				  }		  
				  $templateHtml .='   <td valign="top">'.number_format($item_cost,2).'&nbsp;'.$currency_code.'</td>'; 
				  $templateHtml .='   <td valign="top">'.$tax_rate.'%</td>'; 				  
				  $templateHtml .='   <td valign="top">'.number_format($item_vat_amount,2).'&nbsp;'.$currency_code.'</td>'; 
				  $templateHtml .='   <td valign="top">'.number_format($item->Cost,2).'&nbsp;'.$currency_code.'</td>'; 
				$templateHtml .='</tr>'; 
        
			}
			
			 $postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
			 $postage_cost = $POSTAGE - $postage_vat_amount;
			  
			$templateHtml .='<tr>';
			$templateHtml .='<td align="left" valign="top" class="">Shipping & Handling Fee</td>'; 
  			$templateHtml .='<td valign="top">&nbsp;</td>'; 
			$templateHtml .='<td valign="top">&nbsp;</td>'; 
			if($hs_code != ''){
				$templateHtml .='<td valign="top">&nbsp;</td>'; 
			}
			if($country_of_origin != ''){
				$templateHtml .='<td valign="top">&nbsp;</td>'; 
			}
			$templateHtml .='<td valign="top">'.number_format($postage_cost,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='<td valign="top">'.$tax_rate.'%</td>'; 				  
			$templateHtml .='<td valign="top">'.number_format($postage_vat_amount,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='<td valign="top">'.number_format($POSTAGE,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='</tr>'; 
			
			$final_total =  $item_cost + $postage_cost + $item_vat_amount + $postage_vat_amount;
			
			$templateHtml .='<tr>';
				$templateHtml .='<td align="left" valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000"><strong>Total</strong></td>'; 
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>'; 
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>'; 
				
				if($hs_code != ''){
					$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>'; 
				}
				if($country_of_origin != ''){
					$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>'; 
				}
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">'.number_format(($item_cost + $postage_cost),2).'&nbsp;'.$currency_code.'</td>'; 
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">'.$tax_rate.'%</td>'; 				  
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">'.number_format(($item_vat_amount + $postage_vat_amount),2).'&nbsp;'.$currency_code.'</td>'; 
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">'.number_format($final_total,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='</tr>'; 
 					
			$templateHtml .= '</table>';
			
			 
 			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
 			$templateHtml .='<tr>';			 
			$templateHtml .='<td align="left"><strong>Shipment Weight</strong>: '. number_format($ShipmentWeight,2) .'Kg</td>';	
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';	
			$templateHtml .='<td align="left" width="33%">'; 
			$templateHtml .='<div style="font-size:14px;"><strong>Declaration:</strong></div>
			<div class="row" style="font-size:12px;">
We hereby certify that the information contained <br>in this invoice is true and correct and that
the <br>contents of this shipment are as started above.</div>';
			$templateHtml .='</td>';
			$templateHtml .='</tr>';
 			$templateHtml .='</table>';	
 			$templateHtml .='</div>';
 
			$templateHtml .='<div class="footer row" style="text-align:center; border-bottom:0px;">
			<b>Thank you for your purchase. Happy Shopping!</div>';
		 echo	$templateHtml .='</div>'; 
		
		}
		elseif($result == 'mare'){
			
			$EORI_Number = 'GB019434034000';
			$UK_VAT_Number = 'GB 307817693'; 
			$DE_VAT_Number = '';
			$FR_VAT_Number = '';
			
			$templateHtml ='<!-- eBuyer Express(Marec) Invoice -->';			
			$templateHtml .='<div id="label" style="margin-top:20px;">';
			$templateHtml .='<div class="container">';			
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top"><img src="'.WWW_ROOT.'img/ebuyerexpress.jpg" width="380px"></td>';
			$templateHtml .='<td  width="25%" valign="top">
			ESL LIMITED<br>
			THIRD FLOOR<br>
			40 ESPLANADE<br>
			ST HELIER, ST CLEMENT<br>
			JE4 9RJ			
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="35%"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml .='<td valign="top" width="35%"><h4><u>Billing Address:</u></h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';
			 
 			$templateHtml .='<td  width="29%" valign="top">';
			$templateHtml .='<table>';
			$templateHtml .='<tr><td><b>EORI No:</b></td>	<td>'.$EORI_Number.'</td></tr>';
			$templateHtml .='<tr><td><b>Invoice(CIF):</b></td>	<td>'.$OrderId.'</td></tr>';
			if($Country == 'United Kingdom' && $UK_VAT_Number != '' ){
				$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$UK_VAT_Number.'</td></tr>';
			}else if($Country == 'Germany' && $DE_VAT_Number != ''){
				$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$DE_VAT_Number.'</td></tr>';
			}
			if($waybill_number != ''){
				$templateHtml .='<tr><td><b>WayBill No:</b></td>	<td>'.$waybill_number.'</td></tr>';
			}
			
			$templateHtml .='<tr><td><b>Invoice Date:</b></td><td>'.$INVOICE_DATE.'</td></tr>';
			$templateHtml .='</table>';
			$templateHtml .='</td>'; 			
 			
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
 			 
			$templateHtml .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:500px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">';
 			$country_of_origin = '';
			foreach($items as $count => $item){
		    	$b = '';
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				} 
									
				$pro_weight = 0; 
				$pro_res = $this->Product->find('first', array('conditions' => ['Product.product_sku' => $item->SKU],'fields' => ['Product.country_of_origin','ProductDesc.weight']));
 				if(count( $pro_res ) > 0){				
					$pro_weight = $pro_res['ProductDesc']['weight'];
					$country_of_origin = $pro_res['Product']['country_of_origin'];
 				} 
				
				$hs_code = '';
			    $hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $item->SKU,'country_code' => $country_code)));
				if(count( $hsresult ) > 0){				
					$hs_code = $hsresult['ProductHscode']['hs_code'];
				}
				
				if( $count == 0){
					
					$templateHtml  .='<tr>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">SKU</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Item</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Qty</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Weight</th>';
					if( $hs_code != ''){
						$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">HSCode</th>';
					}
					if($country_of_origin != ''){						 
						$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Country of origin</th>';
					}
 					
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Price</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Tax Rate</th>';				
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Tax</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Line Total</th>';
					$templateHtml  .='</tr>';
				}
				
				$Title = $item->Title;
				if(strlen($item->Title) < 10){
					$Title = $item->ChannelTitle;
				}
					  
				$templateHtml .='<tr>';
					$templateHtml .=' <td style="'.$b.'" align="left" valign="top">'.$item->SKU.'</td>';
					$templateHtml .=' <td style="'.$b.'" align="left" valign="top">'.$Title.'</td>';
					$templateHtml .=' <td style="'.$b.'" valign="top">'.$item->Quantity.'</td>';
					$templateHtml .=' <td style="'.$b.'" valign="top">'.($pro_weight * $item->Quantity).'</td>';
					if($hs_code != ''){
						$templateHtml .=' <td style="'.$b.'" align="left" valign="top" >'.$hs_code.'</td>';
					}
					if($country_of_origin != ''){
						$templateHtml .='<td style="'.$b.'" align="left" valign="top" >'.$country_of_origin.'</td>'; 
					}
					
					$item_vat_amount = $item->Cost - ($item->Cost/(1 + ($tax_rate/100)));
				    $item_cost = $item->Cost - $item_vat_amount;
				  
 					$templateHtml .=' <td style="'.$b.'" valign="top">'.number_format($item_cost,2).'&nbsp;'.$currency_code.'</td>';	
					$templateHtml .=' <td style="'.$b.'" valign="top">'.$tax_rate.'%</td>';				 
					$templateHtml .=' <td style="'.$b.'" valign="top" >'.number_format($item_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
					$templateHtml .=' <td style="'.$b.'" valign="top" >'.number_format($item->Cost,2).'&nbsp;'.$currency_code.'</td>';
				$templateHtml .='</tr>';            
       
			}
 			
			$postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
			$postage_cost = $POSTAGE - $postage_vat_amount;
			
			$b_bottom = 'border-bottom:2px solid #000;';  
			$templateHtml .='<tr>';
				$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top" colspan="2">SHIPPING HANDLING FEE</td>'; 
   				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				if($hs_code != ''){
					$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
				if($country_of_origin != ''){
					$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format($postage_cost,2).'&nbsp;'.$currency_code.'</td>';	
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.$tax_rate.'%</td>';				 
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top" >'.number_format($postage_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top" >'.number_format($POSTAGE,2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='</tr>';  
			
 			$final_total =  $item_cost + $postage_cost + $item_vat_amount + $postage_vat_amount;
			  
			$templateHtml .='<tr>';
				$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top" colspan="2"><strong>TOTAL</strong></td>'; 
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				if($hs_code != ''){
					$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
				if($country_of_origin != ''){
					$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}  				 
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format(($item_cost+$postage_cost),2).'&nbsp;'.$currency_code.'</td>';	
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.$tax_rate.'%</td>';					 
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format(($item_vat_amount+$postage_vat_amount),2).'&nbsp;'.$currency_code.'</td>';
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format($final_total,2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='</tr>';  
			    
			  
			$templateHtml .= '</table>';
			
			$templateHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$templateHtml .='<tr>';
			$templateHtml .='<td align="left"><strong>Shipment Weight</strong>: '. number_format($ShipmentWeight,2) .'Kg</td>';	
 			$templateHtml .='</tr>';	
			 $templateHtml .='<tr>';	
			$templateHtml .='<td align="left" width="33%">'; 
			$templateHtml .='<div style="font-size:14px;"><strong>Declaration:</strong></div>
			<div class="row" style="font-size:12px;">
We hereby certify that the information contained <br>in this invoice is true and correct and that
the <br>contents of this shipment are as started above.</div>';
			$templateHtml .='</td>';
			$templateHtml .='</tr>';
			
			$templateHtml .='</table>';
			
			$templateHtml .='</div>';
			
 			$templateHtml .='<div class="footer row" style="font-size:15px; text-align:center; border-bottom:0px;" >
			<h2>THANK YOU FOR YOUR PURCHASE</h2> 
			<b>Please take a moment to leave us positive feedback.</b><br>
			If however, for any reason you are not entirely happy with your
			order, please contact us<br>to allow us to rectify any issues before
			leaving feedback.</div>';			
		 	$templateHtml .='</div>';
					
		}
		elseif($result == 'rain'){
		
			$EORI_Number = 'GB318649182000';
			$UK_VAT_Number = 'GB 318649182'; 
			$DE_VAT_Number = '';
			$FR_VAT_Number = '';     
			
			//$templateHtml ='<!-- Rainbow Invoice -->';			
			$templateHtml ='<div id="label">';
			$templateHtml .='<div class="container">';
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="60%" style="text-align:center; font-weight:bold; font-size:28px">INVOICE(CIF)</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="50%" valign="top">';
			$templateHtml .='<img src="'.WWW_ROOT.'img/rainbow_logo.png" width="250px">';
			$templateHtml .='</td>';			
			$templateHtml .='<td  width="50%" valign="top" style="text-align:right">
				<b>FRESHER BUSINESS LIMITED</b><br>
				BEACHSIDE BUSINESS CENTRE<br>
				RUE DU HOCQ<br>
				ST CLEMENT<br>
				JERSEY<br>
				JE2 6LF
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<table cellpadding="3" style="background-color:#333; color:#fff; margin-bottom:20px;" class="topborder bottomborder"><tr>';
			
			$templateHtml .='<td><span class="bold">Invoice ID:</span> '.$OrderId.'</td>';		
			$templateHtml .='<td><span class="bold">EORI No:'.$EORI_Number.'</td>';
			if($Country == 'United Kingdom' && $UK_VAT_Number != ''){
				$templateHtml .='<td><b>VAT No:</b>'.$UK_VAT_Number.'</td>';
			}else if($Country == 'Germany' && $DE_VAT_Number != '' ){
				$templateHtml .='<td><b>VAT No:</b>'.$DE_VAT_Number.'</td>';
			}
			if($waybill_number != ''){
				$templateHtml .='<td><b>WayBill No:</b>'.$waybill_number.'</td>';
			}
			$templateHtml .='<td><span class="bold">Date:</span> '.$INVOICE_DATE.'</td>';
			
			$templateHtml .='</tr></table>';
			
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  valign="top" width="50%"><h4 class="topborder bottomborder" style="background-color:#333; color:#fff; padding:3px">
			Billing Address:</h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml .='<td  width="50%" valign="top"><h4 class="topborder bottomborder" style="background-color:#333; color:#fff; padding:3px">
			Delivery Address:</h4>'.$SHIPPING_ADDRESS.'</td>';
			
			$templateHtml .='</tr>';		
			$templateHtml .='</table>';
	
			$templateHtml .='<div class="tablesection row" style="margin:5px 0 0 0; border-bottom:0px;height:600px">';
			$templateHtml .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse: collapse; margin:0px;">';
			$country_of_origin = '';
			
			foreach($items as $count => $item){
				
					$Title = $item->Title;
					if(strlen($item->Title) < 10){
						$Title = $item->ChannelTitle;
					}
					
					$pro_weight = 0; 
					$pro_res = $this->Product->find('first', array('conditions' => ['Product.product_sku' => $item->SKU],'fields' => ['Product.country_of_origin','ProductDesc.weight']));
					if(count( $pro_res ) > 0){				
						$pro_weight = $pro_res['ProductDesc']['weight'];
						$country_of_origin = $pro_res['Product']['country_of_origin'];
					} 
					$hs_code = '';
					$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $item->SKU,'country_code' => $country_code)));
					if(count( $hsresult ) > 0){				
						$hs_code = $hsresult['ProductHscode']['hs_code'];
					}
				
					if($count == 0){
						$templateHtml .='<tr>';
						$templateHtml .='<th align="left" class="topborder bottomborder">SKU</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Item</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Qty</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Weight</th>';
						if($hs_code !='' ){				
							$templateHtml .='<th align="left" class="topborder bottomborder">HSCode</th>';
						}
						if($country_of_origin !='' ){				
							$templateHtml .='<th align="left" class="topborder bottomborder">Country of origin</th>';
						}						
						$templateHtml .='<th align="left" class="topborder bottomborder">Price</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Tax Rate</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Tax</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Line Cost</th>';
						$templateHtml .='</tr>';
					}
				
					$templateHtml .='<tr>';
					$templateHtml .='   <td align="left">'.$item->SKU.'</td>';
					$templateHtml .='   <td align="left">'.$Title.'</td>';
					$templateHtml .='   <td align="left">'.$item->Quantity.'</td>';
					$templateHtml .='   <td align="left">'.($pro_weight * $item->Quantity).'</td>';
					if($hs_code !='' ){	
						$templateHtml .='   <td align="left">'.$hs_code.'</td>';					
					}
					if($country_of_origin !='' ){
						$templateHtml .='   <td align="left">'.$country_of_origin.'</td>';	
					}  
 					$item_vat_amount = $item->Cost - ($item->Cost/(1 + ($tax_rate/100)));
					$item_cost = $item->Cost - $item_vat_amount;
  
  					$templateHtml .='<td style="" valign="top">'.number_format($item_cost,2).'&nbsp;'.$currency_code.'</td>';
  					$templateHtml .='<td style="" valign="top">'.$tax_rate.'%</td>';
					$templateHtml .='<td style="" valign="top">'.number_format($item_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
 					$templateHtml .='<td style="" valign="top">'.number_format($item->Cost,2).'&nbsp;'.$currency_code.'</td>'; 
					$templateHtml .='</tr>';            
				
			}
			
			$postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
			$postage_cost = $POSTAGE - $postage_vat_amount;
			
			$templateHtml .='<tr>';
			$templateHtml .='<td align="left" colspan="2">SHIPPING & HANDLING FEE</td>'; 
 			$templateHtml .='<td align="left">&nbsp;</td>';
			$templateHtml .='<td align="left">&nbsp;</td>';
			if($hs_code !='' ){	
				$templateHtml .='<td align="left">&nbsp;</td>';					
			}
			if($country_of_origin !='' ){
				$templateHtml .='<td align="left">&nbsp;</td>';		
			}
  			$templateHtml .='<td align="left">'.number_format($postage_cost,2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='<td align="left">'.$tax_rate.'%</td>';
			$templateHtml .='<td align="left">'.number_format($postage_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='<td align="left">'.number_format($POSTAGE,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='</tr>'; 
			
			$final_total =  $item_cost + $postage_cost + $item_vat_amount + $postage_vat_amount;
			
			$templateHtml .='<tr>';
			$templateHtml .='   <td align="left" colspan="2" style="border-top:1px solid #000;border-bottom:1px solid"><strong>TOTAL</strong></td>'; 
			
			$templateHtml .='   <td style="border-top:1px solid #000;border-bottom:1px solid">&nbsp;</td>';
			$templateHtml .='   <td style="border-top:1px solid #000;border-bottom:1px solid">&nbsp;</td>';
			if($hs_code !='' ){	
				$templateHtml .='   <td style="border-top:1px solid #000;border-bottom:1px solid">&nbsp;</td>';					
			}
			if($country_of_origin !='' ){
				$templateHtml .='   <td style="border-top:1px solid #000;border-bottom:1px solid">&nbsp;</td>';	
			}
			$templateHtml .='<td style="border-top:1px solid #000;border-bottom:1px solid">'.number_format(($item_cost + $postage_cost),2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='<td style="border-top:1px solid #000;border-bottom:1px solid">'.$tax_rate.'%</td>';
			$templateHtml .='<td style="border-top:1px solid #000;border-bottom:1px solid">'.number_format(($item_vat_amount + $postage_vat_amount),2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='<td style="border-top:1px solid #000;border-bottom:1px solid">'.number_format($final_total,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='</tr>';  
 					
			$templateHtml .='</table>';
		
			$templateHtml .='<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;">';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td align="left"><strong>Shipment Weight</strong>: '. number_format($ShipmentWeight,2) .'Kg</td>';	
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td>';
			$templateHtml .='<div style="font-size:14px;"><strong>Declaration:</strong></div>
			<div class="row" style="font-size:12px;">We hereby certify that the information contained <br>in this invoice is true and correct and that
the <br>contents of this shipment are as started above.</div>';
			 $templateHtml .='<tr>';
			 $templateHtml .='</tr>';
			 			
			$templateHtml .='</table>';			
			$templateHtml .='</div>';

			

			$templateHtml  .= '<div class="footer row" style="text-align:center; border-bottom:0px;" >
				<b>Thank you for your purchase. Happy Shopping!<br>
				Rainbow Retail</b>';	
			 $templateHtml  .= '</div>';
		}
		else{
 		
 			$itmsHtml  ='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:500px">';
			$itmsHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">';
 			$country_of_origin = '';
			foreach($items as $count => $item){
		    	$b = '';
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				} 					
				$pro_weight = 0; 
				$pro_res = $this->Product->find('first', array('conditions' => ['Product.product_sku' => $item->SKU],'fields' => ['Product.country_of_origin','ProductDesc.weight']));
				if(count( $pro_res ) > 0){				
					$pro_weight = $pro_res['ProductDesc']['weight'];
					$country_of_origin = $pro_res['Product']['country_of_origin'];
				} 
				$hs_code = '';
			    $hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $item->SKU,'country_code' => $country_code)));
				if(count( $hsresult ) > 0){				
					$hs_code = $hsresult['ProductHscode']['hs_code'];
				}
				
				if( $count == 0){
					
					$itmsHtml  .='<tr>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">SKU</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Item</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Qty</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Weight</th>';
					if( $hs_code != ''){
						$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">HSCode</th>';
					}
					if( $country_of_origin != ''){
						$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Country of origin</th>';
					}
 					
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Price</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Tax Rate</th>';				
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Tax</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Line Total</th>';
					$itmsHtml  .='</tr>';
				}
				
				$Title = $item->Title;
				if(strlen($item->Title) < 10){
					$Title = $item->ChannelTitle;
				}
					  
				$itmsHtml .='<tr>';
					$itmsHtml .=' <td style="'.$b.'" align="left" valign="top">'.$item->SKU.'</td>';
					$itmsHtml .=' <td style="'.$b.'" align="left" valign="top">'.$Title.'</td>';
					$itmsHtml .=' <td style="'.$b.'" valign="top">'.$item->Quantity.'</td>';
					$itmsHtml .=' <td style="'.$b.'" valign="top">'.($pro_weight * $item->Quantity).'</td>';
					if($hs_code != ''){
						$itmsHtml .=' <td style="'.$b.'" align="left" valign="top" >'.$hs_code.'</td>';
					}
					if($country_of_origin != ''){
						$itmsHtml .=' <td style="'.$b.'" align="left" valign="top" >'.$country_of_origin.'</td>';
					}
					
					$item_vat_amount = $item->Cost - ($item->Cost/(1 + ($tax_rate/100)));
				    $item_cost = $item->Cost - $item_vat_amount;
				  
 					$itmsHtml .=' <td style="'.$b.'" valign="top">'.number_format($item_cost,2).'&nbsp;'.$currency_code.'</td>';	
					$itmsHtml .=' <td style="'.$b.'" valign="top">'.$tax_rate.'%</td>';				 
					$itmsHtml .=' <td style="'.$b.'" valign="top" >'.number_format($item_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
					$itmsHtml .=' <td style="'.$b.'" valign="top" >'.number_format($item->Cost,2).'&nbsp;'.$currency_code.'</td>';
				$itmsHtml .='</tr>';            
       
			}
 			
			$postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
			$postage_cost = $POSTAGE - $postage_vat_amount;
			
			$b_bottom = 'border-bottom:2px solid #000;';  
			$itmsHtml .='<tr>';
				$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top" colspan="2">SHIPPING HANDLING FEE</td>'; 
   				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				if($hs_code != ''){
					$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}if($country_of_origin != ''){
					$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format($postage_cost,2).'&nbsp;'.$currency_code.'</td>';	
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.$tax_rate.'%</td>';				 
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top" >'.number_format($postage_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top" >'.number_format($POSTAGE,2).'&nbsp;'.$currency_code.'</td>';
			$itmsHtml .='</tr>';  
			
 			$final_total =  $item_cost + $postage_cost + $item_vat_amount + $postage_vat_amount;
			  
			$itmsHtml .='<tr>';
				$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top" colspan="2"><strong>TOTAL</strong></td>'; 
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				if($hs_code != ''){
					$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
				if($country_of_origin != ''){
					$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
  				 
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format(($item_cost+$postage_cost),2).'&nbsp;'.$currency_code.'</td>';	
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.$tax_rate.'%</td>';					 
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format(($item_vat_amount+$postage_vat_amount),2).'&nbsp;'.$currency_code.'</td>';
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format($final_total,2).'&nbsp;'.$currency_code.'</td>';
			$itmsHtml .='</tr>';  
			    
			  
			$itmsHtml .= '</table>';
			
			$itmsHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$itmsHtml .='<tr>';
			$itmsHtml .='<td align="left"><strong>Shipment Weight</strong>: '. number_format($ShipmentWeight,2) .'Kg</td>';	
 			$itmsHtml .='</tr>';	
			 $itmsHtml .='<tr>';	
			$itmsHtml .='<td align="left" width="33%">'; 
			$itmsHtml .='<div style="font-size:14px;"><strong>Declaration:</strong></div>
			<div class="row" style="font-size:12px;">
We hereby certify that the information contained <br>in this invoice is true and correct and that
the <br>contents of this shipment are as started above.</div>';
			$itmsHtml .='</td>';
			$itmsHtml .='</tr>';
			
			$itmsHtml .='</table>';
			
			$itmsHtml .='</div>';
			
 			 
 		
			 if($result == 'tech'){
  			
				$EORI_Number = '';
				$UK_VAT_Number = ''; 
				$DE_VAT_Number = '';
				$FR_VAT_Number = '';
			
				//$templateHtml  ='<!-- TechDrive Invoice -->';
				$templateHtml   ='<div id="label">';
				$templateHtml  .='<div class="container">';
				$templateHtml  .='<table class="header row" style="border-bottom:0px"></table>';
				$templateHtml  .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td  width="60%" valign="top">';
				$templateHtml  .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';
				$templateHtml  .='<td  width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
				$templateHtml  .='</td>';
				$templateHtml  .='<td width="40%" valign="top" align="center"><img src="'.WWW_ROOT.'img/techDriveSupplies2.jpg" width="280px"></td>';
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
				
				$templateHtml  .='<table>';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td><span style="font-weight:bold; font-size:18px">INVOICE(CIF) #'.$OrderId.'</span><br></td>';
  				$templateHtml .='<td><b>EORI No:</b></td>	<td>'.$EORI_Number.'</td>';
				$templateHtml .='<td><b>Invoice(CIF):</b></td>	<td>'.$OrderId.'</td>';
				if($Country == 'United Kingdom' && $UK_VAT_Number != '' ){
					$templateHtml .='<td><b>VAT No:</b></td>	<td>'.$UK_VAT_Number.'</td>';
				}else if($Country == 'Germany' && $DE_VAT_Number != ''){
					$templateHtml .='<td><b>VAT No:</b></td>	<td>'.$DE_VAT_Number.'</td>';
				}
				if($waybill_number != ''){
					$templateHtml .='<td><b>WayBill No:</b></td>	<td>'.$waybill_number.'</td>';
				}
				$templateHtml  .='<td><b>Date:</b> '.$INVOICE_DATE.'</td>';
			
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
	
				$templateHtml  .= $itmsHtml;
  					
				$templateHtml .= '<div class="row" style="text-align:center; border-bottom:0px;margin-left:20%; padding:0px" >';
				$templateHtml .= '<div style="width:70%; text-align:center; border:2px solid #000000; padding:10px;" >';
 				$templateHtml .= '<div style="text-align:left; font-size:10px;">
				If however, for any reason you are not entirely happy with your order, please contact us to allow us to rectify any issues before leaving feedback.
				</div>';
				
				$templateHtml .= '</div>';
				$templateHtml .= '</div>';
				$templateHtml .= '</div>';
			
			}		
			elseif($result == 'bbd_'){	
			
				$EORI_Number = '';
				$UK_VAT_Number = ''; 
				$DE_VAT_Number = '';
				$FR_VAT_Number = '';
					
				//$templateHtml ='<!-- BBD_EU Invoice -->';			
				$templateHtml  ='<div id="label" style="margin-top:20px;">';
				$templateHtml .='<div class="container">';			
				$templateHtml .='<table class="header row" style="border-bottom:0px">';
				$templateHtml .='</table>';
				$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
				$templateHtml .='<tr>';
				$templateHtml .='<td  width="75%" valign="top"><img src="'.WWW_ROOT.'img/bbdEu.png"></td>';
				$templateHtml .='<td  width="25%" valign="top">			
									Unit A1 21/F, Officeplus Among Kok<br>
									998 Canton Road<br>
									Hong Kong<br>
									KL<br>
									0000			
								</td>';
				$templateHtml .='</tr>';
				$templateHtml .='</table>';
				
				$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
				$templateHtml .='<tr>';
				$templateHtml .='<td valign="top" width="37%"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
				$templateHtml .='<td valign="top" width="38%"><h4><u>Billing Address:</u></h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';
	
				$templateHtml .='<td  width="25%" valign="top">';
				$templateHtml .='<table>';
				$templateHtml .='<tr><td><b>Invoice(CIF):</b></td>	<td>'.$OrderId.'</td></tr>';
				
 				if($Country == 'United Kingdom' && $UK_VAT_Number != '' ){
					$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$UK_VAT_Number.'</td></tr>';
				}else if($Country == 'Germany' && $DE_VAT_Number != ''){
					$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$DE_VAT_Number.'</td></tr>';
				}
				if($waybill_number != ''){
					$templateHtml .='<tr><td><b>WayBill No:</b></td>	<td>'.$waybill_number.'</td></tr>';
				}
				$templateHtml .='<tr><td><b>Invoice Date:</b></td><td>'.$INVOICE_DATE.'</td></tr>';
				
				$templateHtml .='</table>';
				$templateHtml .='</td>';
				$templateHtml .='</tr>';
				$templateHtml .='</table>';
				
				$templateHtml .= $itmsHtml;
 				
 				$templateHtml .='<div class="footer row" style="font-size:15px; text-align:center; border-bottom:0px;" >
				<h2>THANK YOU FOR YOUR PURCHASE</h2> 
				<b>Please take a moment to leave us positive feedback.</b><br>
				If however, for any reason you are not entirely happy with your
				order, please contact us<br>to allow us to rectify any issues before
				leaving feedback.</div>';			
				$templateHtml .='</div>';
						
			}
			elseif($result == 'ebay'){			
				//$templateHtml  ='<!-- eBuyersDirect Invoice -->';			
				$templateHtml  ='<div id="label" style="margin-top:20px;">';
				$templateHtml .='<div class="container">';			
				$templateHtml .='<table class="header row" style="border-bottom:0px">';
				$templateHtml .='</table>';
				$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
				$templateHtml .='<tr>';
				$templateHtml .='<td  width="50%" valign="top">
				4 Norwood Court<br>
	La Rue Militaire<br>
	St John, Jersey Channel Islands JE3 4DP<br>
	United Kingdom
				</td>';
				$templateHtml .='<td  width="50%" valign="top" align="right">
				<img src="'.WWW_ROOT.'img/ebuyerDirect.png" width="300px">
				</td>';
				$templateHtml .='</tr>';
				$templateHtml .='</table>';
				$templateHtml .='<hr style="color:#6bc15f">';
				
				$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
				$templateHtml .='<tr>'; 
				
				//mb_convert_encoding($SHIPPING_ADDRESS,'HTML-ENTITIES','UTF-8')			
				$t = mb_convert_encoding($SHIPPING_ADDRESS, "windows-1251", "utf-8");		
				$templateHtml .='<td  valign="top" width="37%"><h4>Delivery Address:</h4>'.$t.'</td>';
				$templateHtml .='<td  valign="top" width="38%"><h4>Billing Address:</h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';
				
				$templateHtml .='<td  width="25%" valign="top">';
				$templateHtml .='<table>';
				$templateHtml .='<tr><td><b>Invoice ID:</b></td><td align="right">'.$OrderId.'</td></tr>';
				if($Country == 'United Kingdom' && $UK_VAT_Number != '' ){
					$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$UK_VAT_Number.'</td></tr>';
				}else if($Country == 'Germany' && $DE_VAT_Number != ''){
					$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$DE_VAT_Number.'</td></tr>';
				}
				if($waybill_number != ''){
					$templateHtml .='<tr><td><b>WayBill No:</b></td>	<td>'.$waybill_number.'</td></tr>';
				}
				
				$templateHtml .='<tr><td><b>Invoice Date:</b></td><td align="right">'.$INVOICE_DATE.'</td></tr>';
				
				
				$templateHtml .='</table>				 
				</td>';
				
 				$templateHtml .='</tr>';
				$templateHtml .='</table>';
				
 				$templateHtml .= $itmsHtml; 
				 
				
				$templateHtml .='<hr style="color:#6bc15f">';
				$templateHtml .='<div class="footer row" style="font-size:15px; text-align:left; border-bottom:0px;" >
				<span style="font-size:28px; font-weight:bold">THANK YOU</span> <i>for using ebuyersDirect-2u</i><br><br>
				<b>Please take a moment to leave us positive feedback.</b><br>
				If however, for any reason you are not entirely happy with your
				order, please contact us to allow us to rectify<br>any issues before
				leaving feedback.</div>';
				$templateHtml .='</div>';
			}
			elseif($result == 'http'){		
			
				//$templateHtml  ='<!-- storeforlife Invoice -->';			
				$templateHtml 	='<div id="label">';
				$templateHtml  .='<div class="container">';
				$templateHtml  .='<table class="header row" style="border-bottom:0px"></table>';
				$templateHtml  .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td  width="60%" valign="top">';
				$templateHtml  .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';
				$templateHtml  .='<td  width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
				$templateHtml  .='</td>';
				$templateHtml  .='<td  width="40%" valign="top" align="center">
				<img src="'.WWW_ROOT.'img/sfl_logo.png"><br>			
				36 HARBOUR REACH, LA RUE DE CARTERET<br> ST HELIER, JERSEY, JE2 4HR</td>';
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
				
				$templateHtml  .='<table>';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td width="80%"><span style="font-weight:bold; font-size:32px">INVOICE #'.$OrderId.'</span><br></td>';
				$templateHtml  .='<td width="20%" align="right"><b>Date:</b> '.$INVOICE_DATE.'</td>';
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
	
				$templateHtml  .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:650px">';
				$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
				<tr>
				   <th style="border-top:3px solid #000;" align="left" class=" " width="14%">SKU</th>
				   <th style="border-top:3px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
				   <th style="border-top:3px solid #000;" valign="top" class=" " width="8%">Quantity</th>
				   <th style="border-top:3px solid #000;" align="left" class=" " width="6%">Unit</th>';
				   
				   if($final_total < $lvcr_limit){
					 $templateHtml  .='<th style="border-top:3px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>';
					 $templateHtml  .='<th style="border-top:3px solid #000;" valign="top" class=" " width="5%">Tax</th>';
				   }
				   
				   $templateHtml  .='<th style="border-top:3px solid #000;" align="left" class=" " width="12%">Cost</th>
				   <th style="border-top:3px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
				</tr>';
					foreach($items as $item){
					
					$Title = $item->Title;
					if(strlen($item->Title) < 10){
						$Title = $item->ChannelTitle;
					}
						
					  $templateHtml .='<tr style="background-color:#f2f2f2">
						   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
						   <td style="" align="left" valign="top" class="">'.$Title.'</td>
						   <td style="" valign="top">'.$item->Quantity.'</td>
						   <td style="" valign="top">'.$item->PricePerUnit.'</td>';
						   if($final_total < $lvcr_limit){
								$templateHtml .=' <td style="" valign="top">'.$item->TaxRate.'</td>';
								$templateHtml .=' <td style="" valign="top">'.$item->Tax.'</td>';
						   }
							$templateHtml .=' <td style="" valign="top">'.$item->Cost.'</td>
						   <td style="" valign="top">'.$item->CostIncTax.'</td>
						</tr>';            
		   
					}
				$templateHtml .= '</table>';
			 
			
				$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
		
				$templateHtml .= '<tr>';
				$templateHtml .= '<th style="border-top:1px solid #000;border-bottom:3px solid #000;" align="left" rowspan="4" width="60%">';		
				$templateHtml .= '<div style="border-bottom:0px;" ><b>Thank you for your purchase. Happy Shopping!<br>StoreForLife</b></div>';
				$templateHtml .= '</th>';
				$templateHtml .= '<th style="border-top:1px solid #000;" align="left" width="25%">SUB TOTAL:</th>';
				$templateHtml .= '<td style="border-top:1px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>';
				$templateHtml .= '</tr>';
		
				$templateHtml .= '<tr>';
				$templateHtml .= '<th style="" align="left" >POSTAGE(Inc VAT):</th>';
				$templateHtml .= '<td style="" align="right">'.$POSTAGE.'</td>';
				$templateHtml .= '</tr>';
				
				/*$templateHtml .= '<tr>';
				$templateHtml .= '<th style="" align="left" >TAX:</th>';
				$templateHtml .= '<td style="" align="right">'.$TAX.'</td>';
				$templateHtml .= '</tr>';*/
			 
				$templateHtml .= '<tr>';
				$templateHtml .= '<th style="border-bottom:3px solid #000;" align="left" >TOTAL INCLUDING VAT:</th>';
				$templateHtml .= '<td style="border-bottom:3px solid #000;" align="right">'.$TOTAL.'</td>';
				$templateHtml .= '</tr>';
				
				$templateHtml .= '</table>';
				$templateHtml .= '</div>';
					
				$templateHtml .= '<div class="row" style="text-align:center; border-bottom:0px;margin-left:20%; padding:0px" >';
				$templateHtml .= '<div style="width:70%; text-align:center; border:2px solid #000000; padding:10px;" >';
		
				$templateHtml .= '<table style="padding:0; text-align:center"><tr>
				<td width="15%"><img src="'.WWW_ROOT.'img/happy_g.png" width="80px"></td>
				<td width="85%" align="center"><span style="font-size:24px">Happy with your order?</span> <br>
				<span style="font-size:10px;">Please take a moment to leave us positive feedback.</span><br>
				<img src="'.WWW_ROOT.'img/5star_g.png" width="120px"><br></td>
				</tr>
				</table>';
				$templateHtml .= '<div style="text-align:left; font-size:10px;">
				If however, for any reason you are not entirely happy with your order, please contact us to allow us to rectify any issues before leaving feedback.
				</div>';
				
				$templateHtml .= '</div>';
				$templateHtml .= '</div>';
				$templateHtml .= '</div>';
				
			}
		}
		
				
		return $templateHtml;	
	}
	 
	
	public function getDHLSlip($order = null){
	
		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdate');
		$this->loadModel('Product');	
		$this->loadModel('Skumapping');
		
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  		 		
			
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
			$BILLING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName)).'<br>';
			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$order->CustomerInfo->BillingAddress->Country ? $order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
			
			$SHIPPING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName)).'<br>';
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).', ';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $order->CustomerInfo->Address->Country ? $order->CustomerInfo->Address->Country : '';
			}			
		}		
		 
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 		
		$ReferenceNum     = $order->GeneralInfo->ReferenceNum;
		$PostalServiceName= $order->ShippingInfo->PostalServiceName ;
		$BuyerName		  = $order->CustomerInfo->ChannelBuyerName;
		$EmailAddress	  = $order->CustomerInfo->Address->EmailAddress;
		//qgwn9gw0ybrk78b@marketplace.amazon.com
		//dbjggbqmqzd8tw7@marketplace.amazon.co.uk
		//zyp9sfsnj55pyqz@marketplace.amazon.ca
		$exp = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = $exp[$index];
		$storeName = $this->getStoreData($SubSource, $store_country);
		
		//pr($exp);
		//pr( $order); 	
		$INVOICE_DATE	  = date('Y-M-d', strtotime($order->GeneralInfo->ReceivedDate));
		//$order_date	  	  = date('D, M d, Y', strtotime($order->GeneralInfo->ReceivedDate));
		$templateHtml 	  = NULL;
		
	  	$lvcr_limit = 17.99; //gbp
		
		$final_total = $TOTAL ;
		if($Currency == 'GBP'){
			$final_total = $TOTAL * (1.122);
		}
		if($final_total < $lvcr_limit){
			$POSTAGE = 4;
		}
		$ShipmentWeight  = 0.0;
		$swresult = $this->MergeUpdate->find('first',['conditions' => ['order_id' => $OrderId],'fields'=>['packet_weight','envelope_weight']]);
		if(count( $swresult ) > 0){				
			$ShipmentWeight = $swresult['MergeUpdate']['packet_weight'] + $swresult['MergeUpdate']['envelope_weight'];
		}
		$country_code = $this->getCountryCode($Country);
		$tax_rate = ''; $vat_amount = 0;
		$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
		if(count( $thsresult ) > 0){				
			$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
		}
		
		$currency_code = $this->getCurrencyCode($Currency);
		
		$TXT_SHIP_TO = 'Ship To';
		$TXT_ORDER_ID = 'Order ID';
		$TXT_THANKS = 'Thank you for buying from '.$storeName.' on Amazon Marketplace.';
		$TXT_SHIP_ADDRESS = 'Shipping Address';		
		$TXT_ORDER_DATE = 'Order Date';
		$TXT_SHIP_SERVICE = 'Shipping Service';
		$TXT_BUYER_NAME = 'Buyer Name';
		$TXT_SELLER_NAME = 'Seller Name'; 
		
		$TXT_QUANTITY = 'Quantity';
		$TXT_PRODUCT_DETAILS = 'Product Details';
		$TXT_UNIT_PRICE = 'Unit price';
		$TXT_ORDER_TOTALS = 'Order Totals';
		
		$TXT_SKU = 'SKU';
		$TXT_ASIN = 'ASIN';
		$TXT_CONDITION = 'Condition';
		$TXT_LISTING_ID = 'Listing ID';
		$TXT_ORDER_ITEM_ID = 'Order Item ID';
 		$condition = 'New';
		
		$TXT_ITEM_SUB_TOTAL = 'Item subtotal';
		$TXT_ITEM_TOTAL = 'Item total';
		$TXT_GRAND_TOTAL = 'Grand total';
		
		$RET_MESSAGE  = ' <tr>
			  <td style="padding:5px;">
				 <strong>Returning your item:</strong>
				 <p>Go to "Your Account" on Amazon.com, click "Your Orders" and then click the "seller profile" link for this order to get
					information about the return and refund policies that apply.</br>
					Visit <a href="https://www.amazon.com/returns" style="text-decoration:none;">https://www.amazon.com/returns</a> to print a return shipping label. Please have your order ID ready.
				 </p>
			  </td>
		   </tr>';
		$RET_MESSAGE .= ' <tr>
			  <td style="padding:5px;">
				 <p><strong>Thanks for buying on Amazon Marketplace.</strong> To provide feedback for the seller please visit <a href="http://www.amazon.com/feedback"  style="text-decoration:none;">www.amazon.com/feedback</a>. To
					contact the seller, go to Your Orders in Your Account. Click the seller\'s name under the appropriate product. Then, in the
					"Further Information" section, click "Contact the Seller."
				 </p>
			  </td>
		   </tr>';
		 
		$order_date 	= date('D, M d, Y', strtotime($order->GeneralInfo->ReceivedDate));
		$english_days   = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');			 
		$english_months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
  		 						
		if($store_country == 'fr'){
			$TXT_SHIP_TO = utf8_decode('Adresse d\'expédition');
			$TXT_ORDER_ID = utf8_decode('Numéro de la commande');
			$TXT_THANKS = 'Merci pour votre achat avec le vendeur '.$storeName.' sur Amazon.fr';
			$TXT_SHIP_ADDRESS = utf8_decode('Adresse d\'expédition');
			$TXT_ORDER_DATE = 'Date de commande';
			$TXT_SHIP_SERVICE = 'Service de livraison';
			$TXT_BUYER_NAME = 'Nom de l\'acheteur';
			$TXT_SELLER_NAME = 'Nom du vendeur';
			
			$TXT_QUANTITY =  utf8_decode('Quantité');
			$TXT_PRODUCT_DETAILS = utf8_decode('Détails de l\'article');
			$TXT_UNIT_PRICE = utf8_decode('Prix de l\'unité');
			$TXT_ORDER_TOTALS = 'Total des commandes';
			
			$TXT_SKU = 'SKU';
			$TXT_ASIN = 'ASIN';
			$TXT_CONDITION = utf8_decode('État');
			$TXT_LISTING_ID = utf8_decode('ID de l\'offre');
			$TXT_ORDER_ITEM_ID = utf8_decode('ID de l\'article commandé');
			$condition = 'Neuf';
			
			$TXT_ITEM_SUB_TOTAL = 'Sous-total de l\'article';
			$TXT_ITEM_SHIPPING = utf8_decode('Total de l\'expédition');
			$TXT_ITEM_TOTAL = 'Total de l\'article';
			$TXT_GRAND_TOTAL = 'Total';
			 
 			$_order_date = date('D. M d Y', strtotime($order->GeneralInfo->ReceivedDate));		
			$french_days = array('lun', 'mar', 'mer', 'jeu', 'ven', 'sam', 'dim');			 
			$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
			$order_date = str_replace($english_months, $french_months, str_replace($english_days, $french_days, $_order_date ) );
			
 	  
			$RET_MESSAGE  = utf8_decode(' <tr>
						  <td style="padding:5px;">							 
							 <p><strong>Merci de votre achat sur Amazon Marketplace.</strong>Pour évaluer votre vendeur, veuillez vous rendre sur <a href="https://www.amazon.fr/feedback" style="text-decoration:none;">www.amazon.fr/feedback</a>.	Pour contacter votre vendeur, veuillez cliquez sur « Votre compte » puis « Vos commandes ».
Localisez la commande puis cliquez sur « Contactez le vendeur » à droite du produit.</p>
						  </td>
					   </tr>');
					   
 		}else if($store_country == 'es'){
			$TXT_SHIP_TO = 'Enviar a';
			$TXT_ORDER_ID =  ('Nº de pedido');
			$TXT_THANKS = 'Gracias por comprar en '.$storeName.' en Marketplace de Amazon.';
			$TXT_SHIP_ADDRESS = utf8_decode('Dirección de envío');
			$TXT_ORDER_DATE = 'Date de commande';
			$TXT_SHIP_SERVICE = 'Service de livraison';
			$TXT_BUYER_NAME = 'Nom de l\'acheteur';
			$TXT_SELLER_NAME = 'Nom du vendeur';
			
			$TXT_QUANTITY =  utf8_decode('Quantité');
			$TXT_PRODUCT_DETAILS = utf8_decode('Détails de l\'article');
			$TXT_UNIT_PRICE = utf8_decode('Prix de l\'unité');
			$TXT_ORDER_TOTALS = 'Total des commandes';
			
			$TXT_SKU = 'SKU';
			$TXT_ASIN = 'ASIN';
			$TXT_CONDITION = utf8_decode('État');
			$TXT_LISTING_ID = utf8_decode('ID de l\'offre');
			$TXT_ORDER_ITEM_ID = utf8_decode('ID de l\'article commandé');
			$condition = 'Neuf';
			
			$TXT_ITEM_SUB_TOTAL = 'Sous-total de l\'article';
			$TXT_ITEM_SHIPPING = utf8_decode('Total de l\'expédition');
			$TXT_ITEM_TOTAL = 'Total de l\'article';
			$TXT_GRAND_TOTAL = 'Total';
			
			$_order_date = date('D, d M Y', strtotime($order->GeneralInfo->ReceivedDate));				 
			$french_days = array('lun.', 'mar.', 'mié.', 'jue.', 'vie.', 'sáb.', 'dom.');	
			$french_months = array('enero', 'feb.', 'marzo', 'abr.', 'may.', 'jun.', 'jul.', 'agosto', 'sept.', 'oct.', 'nov.', 'dic.');
			$order_date = str_replace($english_months, $french_months, str_replace($english_days, $french_days, $_order_date ) );
		
 			$RET_MESSAGE  = ' <tr>
						  <td style="padding:5px;">
							 <strong>Gracias por comprar en Amazon.</strong>
							 <p>Para enviar tus comentarios al vendedor, visita <a href="https://www.amazon.es/feedback" style="text-decoration:none;">www.amazon.es/feedback</a>
							</p>
							<p>Si quieres ponerte en contacto con el vendedor, accede a “Mi cuenta” (en la parte superior derecha de cualquier página de
Amazon) y haz clic en “Mis pedidos”. Localiza el pedido y haz clic en “Contactar con el vendedor”.</p>
						  </td>
					   </tr>';
 		}else if($store_country == 'it'){
			
			$TXT_SHIP_TO = 'Spedire a';
			$TXT_ORDER_ID = 'Numero dell\'ordine';
			$TXT_THANKS = 'Ti ringraziamo per aver acquistato da '.$storeName.' su Amazon.';
			$TXT_SHIP_ADDRESS = 'Indirizzo di spedizione';		
			$TXT_ORDER_DATE = 'Data ordine';
			$TXT_SHIP_SERVICE = 'Tipo di spedizione';
			$TXT_BUYER_NAME = 'Nome acquirente';
			$TXT_SELLER_NAME = 'Nome venditore'; 
			
			$TXT_QUANTITY = 'Quantità';
			$TXT_PRODUCT_DETAILS = 'Dettagli prodotto';
			$TXT_UNIT_PRICE = 'Prezzo unitario';
			$TXT_ORDER_TOTALS = 'Totale ordine';
			
			$TXT_SKU = 'SKU';
			$TXT_ASIN = 'ASIN';
			$TXT_CONDITION = 'Condizione';
			$TXT_LISTING_ID = 'Numero offerta';
			$TXT_ORDER_ITEM_ID = 'N. prodotto';
			$condition = 'Nuovo';
			
			$TXT_ITEM_SUB_TOTAL = 'Subtotale articoli';
			$TXT_ITEM_TOTAL = 'Tot. articolo';
			$TXT_GRAND_TOTAL = 'Tot.';
			
			$_order_date = date('D. M d Y', strtotime($order->GeneralInfo->ReceivedDate));		
			$it_days = array('lun', 'mar', 'mer', 'jeu', 'ven', 'sam', 'dim');  
			$it_months = array('genn.', 'febbr.', 'mar.', 'apr.', 'giugno', 'luglio', 'ag.', 'sett.', 'ott.', 'octobre', 'nov.', 'dic');
			$order_date = str_replace($english_months, $it_months, str_replace($english_days, $it_days, $_order_date ) );
 			
			$RET_MESSAGE  = ' <tr>
						  <td style="padding:5px;">
							 <strong>Grazie per aver comprato nel Marketplace di Amazon.</strong>
							 <p>Per fornire il tuo feedback sul venditore, visita la pagina seguente:
<a href="https://www.amazon.it/feedback" style="text-decoration:none;">www.amazon.it/feedback</a>. Se desideri contattare il venditore, seleziona il link “Il mio account”, che trovi in alto a destra su
ogni pagina di Amazon.it, e accedi alla sezione “I miei ordini”. Individua l\'ordine in questione e clicca su “Contatta il
venditore”</p>
						  </td>
					   </tr>';
		
 		}else if($store_country == 'de'){
		
			$TXT_SHIP_TO = 'Liefern an';
			$TXT_ORDER_ID =  ('Bestellnummer');
			$TXT_THANKS = 'Vielen Dank für Ihren Einkauf bei '.$storeName.' auf Amazon.de Marketplace.';
			$TXT_SHIP_ADDRESS = ('Lieferanschrift');
			$TXT_ORDER_DATE = 'Bestellt am';
			$TXT_SHIP_SERVICE = 'Versandart';
			$TXT_BUYER_NAME = 'Name des Käufers';
			$TXT_SELLER_NAME = 'Name des Verkäufers';
			
			$TXT_QUANTITY =  ('Menge');
			$TXT_PRODUCT_DETAILS = ('Produktdetails');
			$TXT_UNIT_PRICE =  ('Preis pro Einheit');
			$TXT_ORDER_TOTALS = 'Gesamtbestellsumme';
			
			$TXT_SKU = 'SKU';
			$TXT_ASIN = 'ASIN';
			$TXT_CONDITION = ('Zustand');
			$TXT_LISTING_ID = ('Angebotsnummer');
			$TXT_ORDER_ITEM_ID = ('Bestellposten-ID');
			$condition = 'Neu';
			
			$TXT_ITEM_SUB_TOTAL = 'Zwischensumme Artikel';
			$TXT_ITEM_SHIPPING = utf8_decode('Total de l\'expédition');
			$TXT_ITEM_TOTAL = 'Gesamtbetrag Artikel';
			$TXT_GRAND_TOTAL = 'Gesamtbetrag';
			
			$_order_date = date('D. M d Y', strtotime($order->GeneralInfo->ReceivedDate));		
			$de_days = array('Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So');				 
			$de_months = array('Jan.', 'Feb.', 'März', 'Apr.', 'Mai', 'Juni', 'Juli', 'Aug.', 'Sept.', 'Okt.', 'Nov.', 'Dez.');
			$order_date = str_replace($english_months, $de_months, str_replace($english_days, $de_days, $_order_date ) );
		 
 	 		$RET_MESSAGE  = ' <tr>
						  <td style="padding:5px;">
							 <strong>Vielen Dank für Ihren Einkauf bei Amazon Marketplace!</strong>
							 <p>Unter <a href="https://www.amazon.de/feedback" style="text-decoration:none;">www.amazon.de/feedback</a> können Sie Ihre Bewertung für diesen Verkäufer abgeben. Wenn Sie den Verkäufer kontaktieren möchten, gehen Sie über "Mein Konto" auf "Meine
Bestellungen". Suchen Sie die Bestellung und klicken Sie dort bitte auf die Schaltfläche "Verkäufer kontaktieren" um zum
Kontaktformular zu gelangen.
							 </p>
						  </td>
					   </tr>';
					 
 		}
		
		$html = '<!DOCTYPE HTML>';
		$html .= '<html lang="en-US">';
		   $html .= '<head>';
			  $html .= '<meta charset="UTF-8">';
			  $html .= '<title>Pay Slip</title>';
		  $html .= ' </head>';
		   $html .= '<body>
			  <div style="width:650px; height:auto; margin:20px auto;">
				 <table width="100%" style="border-bottom:2px dashed #000;font-family:arial;">
					<tbody>
					   <tr>';
						   $html .= '<td width="100%" value="top" align="left" style="padding:5px;">
							 '.$TXT_SHIP_TO.':<br>
							 <strong>'.$SHIPPING_ADDRESS.'</strong>
						  </td>';
					    $html .= '</tr>
					</tbody>
				 </table>';
				 $html .= '<table width="100%">
					<tbody>
					   <tr>';
						  $html .= '<td width="100%" valign="top" align="left" style="padding:5px;">
							 <strong>'.$TXT_ORDER_ID.': '.$ReferenceNum.'.</strong><br>
							 <div style="font-family:arial;font-size: 12px;">'.$TXT_THANKS.'</div>';
						  $html .= '</td>
					   </tr>
					</tbody>
				 </table>';
				 $html .= '<table width="100%" style="border:1px solid #000;font-family:arial;font-size: 12px;">
					<tbody>
					   <tr>';
						  $html .= '<td width="35%" valign="top" align="left" style="padding:5px;">
 							  <strong>'.$TXT_SHIP_ADDRESS.':</strong> <br>
							  	'.$SHIPPING_ADDRESS.'							  
						  </td>';
						 $html .= ' <td width="25%" valign="top" align="left" style="padding:5px;">							  
								'.$TXT_ORDER_DATE.':<br>
								'.$TXT_SHIP_SERVICE.':<br>
								'.$TXT_BUYER_NAME.':<br>
								'.$TXT_SELLER_NAME.':
 						  </td>';
						 
						 $html .= '<td width="25%" valign="top" align="left" style="padding:5px;">
							    '.$order_date.'<br>
								'.$PostalServiceName.'<br>
								'.$BuyerName.'<br>
								'.$storeName.'
							  
						  </td>';
						 $html .= ' <td width="25%" valign="top" align="left">   
						  </td>';
					   $html .= '</tr>
					</tbody>
				 </table>';
				 $html .= '<table width="100%" border="1" style="margin-top:10px; margin-bottom:2px; border-collapse: collapse;font-family:arial;font-size: 12px;">
					<tbody>';
				 $html .= '<tr>
						  <th style="padding:2px;" align="left">'.$TXT_QUANTITY.'</th>
						  <th style="padding:2px;" align="left">'.$TXT_PRODUCT_DETAILS.'</th>
						  <th style="padding:2px;" align="left">'.$TXT_UNIT_PRICE.'</th>
						  <th style="padding:2px;">'.$TXT_ORDER_TOTALS.'</th>
					   </tr>';					  		 
				$total = 0;
				
				foreach($items as $count => $item){ 
					
					$Title = $item->Title;
					if(strlen($item->Title) < 10){
						$Title = $item->ChannelTitle;
					}
					$asin = 'B00BPPPV8U';
					$asin_data = $this->Skumapping->find('first', array('conditions' => array('Skumapping.channel_sku' => $item->ChannelSKU),'fields'=>['asin']));
					if(count($asin_data) > 0){
						$asin = $asin_data['Skumapping']['asin'];
					}
					$ListingId = $this->getListingId($SubSource,$item->ChannelSKU);
					
					
 					$html .= '<tr>';
					$html .= '<td width="10%" valign="top" align="center" style="padding:2px;">  
							 <strong>'.$item->Quantity.'</strong>
						  </td>';
						  
					$html .= '<td width="40%" valign="top" align="left" style="padding:2px;">
						 <strong>'. $Title.'</strong>
						 <ul style="list-style:none; margin-left:0px; padding-left:0px;">
							<li><strong>'.$TXT_SKU.':</strong> '.$item->ChannelSKU.'</li>
							<li><strong>'.$TXT_ASIN.':</strong> '.$asin.'</li>
							<li><strong>'.$TXT_CONDITION.':</strong> '.$condition.'</li>
							<li><strong>'.$TXT_LISTING_ID.':</strong> '.$ListingId.'</li>
							<li><strong>'.$TXT_ORDER_ITEM_ID.':</strong>'.$item->ItemNumber.'</li>
						 </ul>
					  </td>'; 					  
					    
 		 
					$html .= '<td width="15%" valign="top" align="left" align="center" style="padding:5px;"> '; 
					if( $Currency == 'EUR'){
						$html .= '<strong> '. number_format($item->UnitCost,2,',','').' '.$currency_code.' </strong>';
					}else{
						$html .= '<strong> '.$currency_code.$item->UnitCost.' </strong>';
					}
							 
					$html .= ' </td>';
					$html .= '<td width="35%" style="padding-left:5px;">
					<table width="100%" style="font-weight:bold;">';
					
					if( $Currency == 'EUR'){
						$html .= '<tr><td style="padding-top:1px;">'.$TXT_ITEM_SUB_TOTAL.'</td><td align="right">'.number_format($order->TotalsInfo->Subtotal,2,',','').' '.$currency_code.'</td></tr>';
					}else{
						$html .= '<tr><td style="padding-top:1px;">'.$TXT_ITEM_SUB_TOTAL.'</td><td align="right">'.$currency_code.$order->TotalsInfo->Subtotal.'</td></tr>';
					}
					
 					if($order->TotalsInfo->PostageCost > 0){
						if( $Currency == 'EUR'){
							$html .= '<tr><td>'.$TXT_ITEM_SHIPPING.'</td><td align="right">'.number_format($order->TotalsInfo->PostageCost,2,',','').' '.$currency_code.'</td></tr>';
 						}else{
							$html .= '<tr><td>'.$TXT_ITEM_SHIPPING.'</td><td align="right">'.$currency_code.$order->TotalsInfo->PostageCost.'</td></tr>';
						}
					
					}
					
					if( $Currency == 'EUR'){
						$html .= '<tr><td>'.$TXT_ITEM_TOTAL.'</td><td align="right">'.number_format($order->TotalsInfo->TotalCharge,2,',','').' '.$currency_code.'</td></tr>';
					}else{
						$html .= '<tr><td>'.$TXT_ITEM_TOTAL.'</td><td align="right">'.$currency_code.$order->TotalsInfo->TotalCharge.'</td></tr>';
					}
					
					$html .= '</table>'; 
							
					$html .= '</td>';
					$html .= '</tr>';
						  
				 $total  = $order->TotalsInfo->TotalCharge;
        
			}
  		
				$html .= '	</tbody>
				 </table>';
				$html .= ' <table width="100%">';
				$html .= '	<tbody>';
				$html .= '	   <tr>';
				$html .= '		  <td width="60%"> </td>';
				if( $Currency == 'EUR'){
					$html .= '<td align="right" valign="top" width="40%" style="font-family:arial;font-size:12px;"> <strong>'.$TXT_GRAND_TOTAL.': '.number_format($total,2,',','').' '.$currency_code.'</strong> </td>';
				}else{
					$html .= '<td align="right" valign="top" width="40%" style="font-family:arial;font-size:12px;"> <strong>'.$TXT_GRAND_TOTAL.': '.$currency_code.$total.'</strong> </td>';
				}
				$html .= '	   </tr>';
				$html .= '	</tbody>';
				$html .= '</table>';
				$html .= ' <table style="border-bottom:2px dashed #000;font-family:arial;font-size: 12px;">';
				$html .= '	<tbody>';
				
				$html .= $RET_MESSAGE;
				
				$html .= ' 
					</tbody>
				 </table>
			  </div>
		   </body>
		</html>';
		echo $html; 
		//// <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		
		$_html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			';
	 	$_html .= '<body>'.$html.'</body>';
	 		
		$name	= $OrderId.'.pdf';							
		$dompdf->load_html(utf8_encode($_html), Configure::read('App.encoding'));
		$dompdf->render();			
						
		$file_to_save = WWW_ROOT .'img/dhl/invoice_amz_'.$name;
		 
		//save the pdf file on the server
		file_put_contents($file_to_save, $dompdf->output()); 
		//exit;
	}
	
	public function getDHLSlipFr($order = null){
	
		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdate');
		$this->loadModel('Product');	
		$this->loadModel('Skumapping');
		
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  		 		
			
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
			$BILLING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName)).'<br>';
			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$order->CustomerInfo->BillingAddress->Country ? $order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
			
			$SHIPPING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName)).'<br>';
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).', ';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $order->CustomerInfo->Address->Country ? $order->CustomerInfo->Address->Country : '';
			}			
		}		
		 
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 		
		$ReferenceNum     = $order->GeneralInfo->ReferenceNum;
		$PostalServiceName= $order->ShippingInfo->PostalServiceName ;
		$BuyerName		  = $order->CustomerInfo->ChannelBuyerName;
		$EmailAddress	  = $order->CustomerInfo->Address->EmailAddress;
		//qgwn9gw0ybrk78b@marketplace.amazon.com
		//dbjggbqmqzd8tw7@marketplace.amazon.co.uk
		//zyp9sfsnj55pyqz@marketplace.amazon.ca
		$exp = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = $exp[$index];
		$storeName = $this->getStoreData($SubSource, $store_country);
		
		//pr($exp);
		//pr( $order); 	
		$INVOICE_DATE	  = date('Y-M-d', strtotime($order->GeneralInfo->ReceivedDate));
		$order_date	  	  = date('D, M d, Y', strtotime($order->GeneralInfo->ReceivedDate));
		$templateHtml 	  = NULL;
		
	  	$lvcr_limit = 17.99; //gbp
		
		$final_total = $TOTAL ;
		if($Currency == 'GBP'){
			$final_total = $TOTAL * (1.122);
		}
		if($final_total < $lvcr_limit){
			$POSTAGE = 4;
		}
		$ShipmentWeight  = 0.0;
		$swresult = $this->MergeUpdate->find('first',['conditions' => ['order_id' => $OrderId],'fields'=>['packet_weight','envelope_weight']]);
		if(count( $swresult ) > 0){				
			$ShipmentWeight = $swresult['MergeUpdate']['packet_weight'] + $swresult['MergeUpdate']['envelope_weight'];
		}
		$country_code = $this->getCountryCode($Country);
		$tax_rate = ''; $vat_amount = 0;
		$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
		if(count( $thsresult ) > 0){				
			$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
		}
		
		$currency_code = $this->getCurrencyCode($Currency);
		
		$html = '<!DOCTYPE HTML>';
		$html .= '<html lang="en-US">';
		   $html .= '<head>';
			  $html .= '<meta charset="UTF-8">';
			  $html .= '<title>Pay Slip</title>';
		  $html .= ' </head>';
		   $html .= '<body>
			  <div style="width:650px; height:auto; margin:20px auto;">
				 <table width="100%" style="border-bottom:2px dashed #000;font-family:arial;">
					<tbody>
					   <tr>';
						   $html .= '<td width="100%" value="top" align="left" style="padding:5px;">
							 Ship To:<br>
							 <strong>'.$SHIPPING_ADDRESS.'</strong>
						  </td>';
					    $html .= '</tr>
					</tbody>
				 </table>';
				 $html .= '<table width="100%">
					<tbody>
					   <tr>';
						  $html .= '<td width="100%" valign="top" align="left" style="padding:5px;">
							 <strong>Order ID: '.$ReferenceNum.'.</strong><br>
							 <div style="font-family:arial;font-size: 12px;">Thank you for buying from USABuyer on Amazon Marketplace.</div>';
						  $html .= '</td>
					   </tr>
					</tbody>
				 </table>';
				 $html .= '<table width="100%" style="border:1px solid #000;font-family:arial;font-size: 12px;">
					<tbody>
					   <tr>';
						  $html .= '<td width="35%" valign="top" align="left" style="padding:5px;">
 							  <strong>Shipping Address:</strong> <br>
							  	'.$SHIPPING_ADDRESS.'							  
						  </td>';
						 $html .= ' <td width="25%" valign="top" align="left" style="padding:5px;">							  
								Order Date:<br>
								Shipping Service:<br>
								Buyer Name:<br>
								Seller Name:
							 
						  </td>';
						 $html .= '<td width="25%" valign="top" align="left" style="padding:5px;">
							    '.$order_date.'<br>
								'.$PostalServiceName.'<br>
								'.$BuyerName.'<br>
								'.$storeName.'
							  
						  </td>';
						 $html .= ' <td width="25%" valign="top" align="left">   
						  </td>';
					   $html .= '</tr>
					</tbody>
				 </table>';
				 $html .= '<table width="100%" border="1" style="margin-top:10px; margin-bottom:20px; border-collapse: collapse;font-family:arial;font-size: 12px;">
					<tbody>';
				 $html .= '<tr>
						  <th style="padding:2px;">Quantity</th>
						  <th style="padding:2px;">Product Details</th>
						  <th style="padding:2px;">Unit price</th>
						  <th style="padding:2px;">Order Totals</th>
					   </tr>';
				
				$total = 0;
				
				foreach($items as $count => $item){ 
					
					$Title = $item->Title;
					if(strlen($item->Title) < 10){
						$Title = $item->ChannelTitle;
					}
					$asin = 'B00BPPPV8U';
					$asin_data = $this->Skumapping->find('first', array('conditions' => array('Skumapping.channel_sku' => $item->ChannelSKU),'fields'=>['asin']));
					if(count($asin_data) > 0){
						$asin = $asin_data['Skumapping']['asin'];
					}
					$ListingId = $this->getListingId($SubSource,$item->ChannelSKU);
					
					
 					$html .= '<tr>';
					$html .= '<td width="10%" valign="top" align="center" style="padding:2px;">  
							 <strong>'.$item->Quantity.'</strong>
						  </td>';
						  
					$html .= '<td width="40%" valign="top" align="left" style="padding:2px;">
						 <strong>'. $Title.'</strong>
						 <ul style="list-style:none; margin-left:0px; padding-left:0px;">
							<li><strong>SKU:</strong> '.$item->ChannelSKU.'</li>
							<li><strong>ASIN:</strong> '.$asin.'</li>
							<li><strong>Condition:</strong> New</li>
							<li><strong>Listing ID:</strong> '.$ListingId.'</li>
							<li><strong>Order Item ID:</strong>'.$item->ItemNumber.'</li>
						 </ul>
					  </td>';
					$html .= '<td width="10%" valign="top" align="left" style="padding:5px;">   
							 <strong> '.$currency_code.$item->UnitCost.' </strong>
						  </td>';
					$html .= '<td width="40%" valign="top" align="center">
							 <ul style="list-style:none; font-weight:bold; ">
								<li style="margin-bottom:5px;"><span style="border-bottom:2px solid #ddd ">Item subtotal '.$currency_code.$item->Cost.'</span></li>
								<li><span style="border-bottom:2px solid #ddd;">Item total '.$currency_code.$item->Cost.'</span></li>
							 </ul>
						  </td>';
					$html .= '</tr>';
						  
				 $total += $item->Quantity * $item->UnitCost;
        
			}
  			
				$html .= '	</tbody>
				 </table>';
				$html .= ' <table width="100%">';
				$html .= '	<tbody>';
				$html .= '	   <tr>';
				$html .= '		  <td width="60%"> </td>';
				$html .= '		  <td align="right" valign="top" width="40%" style="font-family:arial;"> <strong>Grand total: '.$currency_code.$total.'</strong> </td>';
				$html .= '	   </tr>';
				$html .= '	</tbody>';
				$html .= '</table>';
				$html .= ' <table style="border-bottom:2px dashed #000;font-family:arial;font-size: 12px;">';
				$html .= '	<tbody>';
					 $html .= ' <tr>
						  <td style="padding:5px;">
							 <strong >Returning your item:</strong>
							 <p>Go to "Your Account" on Amazon.com, click "Your Orders" and then click the "seller profile" link for this order to get
								information about the return and refund policies that apply.</br>
								Visit <a href="https://www.amazon.com/returns" style="text-decoration:none;">https://www.amazon.com/returns</a> to print a return shipping label. Please have your order ID ready.
							 </p>
						  </td>
					   </tr>';
					$html .= ' <tr>
						  <td style="padding:5px;">
							 <p><strong>Thanks for buying on Amazon Marketplace.</strong> To provide feedback for the seller please visit <a href="http://www.amazon.com/feedback"  style="text-decoration:none;">www.amazon.com/feedback</a>. To
								contact the seller, go to Your Orders in Your Account. Click the seller\'s name under the appropriate product. Then, in the
								"Further Information" section, click "Contact the Seller."
							 </p>
						  </td>
					   </tr>';
					$html .= '  </tr>
					   </tr>
					</tbody>
				 </table>
			  </div>
		   </body>
		</html>';
		echo $html; 
		//// <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		
		$_html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			';
	 	$_html .= '<body>'.$html.'</body>';
	 		
		$name	= $OrderId.'.pdf';							
		$dompdf->load_html(utf8_encode($_html), Configure::read('App.encoding'));
		$dompdf->render();			
						
		$file_to_save = WWW_ROOT .'img/dhl/invoice_amz_'.$name;
		 
		//save the pdf file on the server
		file_put_contents($file_to_save, $dompdf->output()); 
		//exit;
	}
	public function dateFormat($date = null, $country = null) 
	{
		$date = '2019-03-05';
		$country = 'fr';
		if($country == 'fr'){
		
 				$order_date = date('D. M d Y', strtotime($date));
				$english_days = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
				$french_days = array('lun', 'mar', 'mer', 'jeu', 'ven', 'sam', 'dim');
				//$french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
				$english_months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
				//$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
				$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
				
			
		echo	str_replace($english_months, $french_months, str_replace($english_days, $french_days, $order_date )  );
			
 		}
		
	
		//return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date) ) ) );
	}
	
	public function getStoreData($SubSource = null, $country = null){
		$store_name = '';
		$store = strtolower(substr($SubSource, 0, 4));
		$europe = array('uk','es','it','fr','de');
		if($store == 'cost' && in_array($country,$europe)){
			$store_name = 'Cost-Dropper';
		}else if($store == 'mare' && in_array($country,$europe)){
			$store_name = 'EbuyerExpress';
		}else if($store == 'rain' && in_array($country,$europe)){
			$store_name = 'RRRetail';
		}else if($store == 'bbd_' && in_array($country,$europe)){
			$store_name = 'BBDEU';
		}else if($store == 'cost' && $country == 'ca'){
			$store_name = 'CanadaEmporium';
		}else if($store == 'cost' && $country == 'com'){
			$store_name = 'USABuyer';
		}
		return $store_name;
	}
	
	public function getListingId($SubSource = null, $seller_sku = null){
	
 		$store = strtolower(substr($SubSource, 0, 4));
		
		if($store == 'cost'){
			$url = 'http://cost-dropper.com/Webservice/getListingId'; 
		}else if($store == 'mare'){
			$url = 'http://ebuyer-express.com/Webservice/getListingId'; 
		}
		
		$channel_sku['seller_sku'] = $seller_sku;
		
		$curl = curl_init(); 
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FAILONERROR, true); 
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
		curl_setopt($curl, CURLOPT_POST, count($channel_sku));
		curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($channel_sku));  
		$result = curl_exec($curl); 						 
	 	$error = curl_error($curl); 
		$info = curl_getinfo($curl);
		curl_close($curl);	
		$data = json_decode($result);
		if($data == ''){
			$data = '0612UINKSC5';
		}
 		return $data ;
	}
	
	public function getDHLInvoice($order_id = null){
				 
		$this->loadModel('MergeUpdate');
		$waybill_number = '';
		$result = $this->MergeUpdate->find('first', array('conditions' => array('order_id' => $order_id),'fields'=>['track_id']));
		if(count($result) > 0){
			$waybill_number = $result['MergeUpdate']['track_id'];
		}
		
		$order = $this->getOrderByNumIdDhl( $order_id );	
		 
 
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();	
		$result 		=	'';
		$htmlTemplate	=	'';	

		$htmlTemplate	=	$this->getTemplateDHL($order,$waybill_number);		
		$SubSource		=	$order->GeneralInfo->SubSource;
	
		$result			=	ucfirst(strtolower(substr($SubSource, 0, 4)));
		
		/**************** for tempplate *******************/
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
	 	$html .= '<body>'.$htmlTemplate.'</body>';
	 		
		$name	= $order_id.'.pdf';							
		$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
		$dompdf->render();			
						
		$file_to_save = WWW_ROOT .'img/dhl/invoice_'.$name;
		 
		//save the pdf file on the server
		file_put_contents($file_to_save, $dompdf->output()); 
		
		/*-----------------DHL AMAZON SLIP----------------------*/	
		$this->getDHLSlip($order);
		
 		exit;		 
			
	}	
	public function Address(){
	
		$this->loadModel('InvoiceAddress');

		$data['order_id'] 	= $this->request->data['order_id'];
		$field 				= $this->request->data['field'];
		$data[$field] 		= $this->request->data['val'];
				
		if($this->Auth->user('username')){
			$data['username']  = $this->Auth->user('username');
		}
		
		$conditions = array('InvoiceAddress.order_id' => $this->request->data['order_id']);
														
		if ($this->InvoiceAddress->hasAny($conditions)){
			foreach($data as $field => $val){
				$DataUpdate[$field] = "'".$val."'";								
			}
			$this->InvoiceAddress->updateAll( $DataUpdate, $conditions );
			$msg['msg'] = 'updated';
		}else{
			$this->InvoiceAddress->saveAll($data);
			$msg['msg'] = 'saved';			
		}
	echo json_encode($msg);
	exit;
	
	}
	public function getCountryCode($Country = null){
		$code = '';
		if($Country == 'United Kingdom'){
			$code = 'gb';
		}else if($Country == 'Germany'){
			$code = 'de';
		}else if($Country == 'France'){
			$code = 'fr';
		}else if($Country == 'Italy'){
			$code = 'it';
		}else if($Country == 'Spain'){
			$code = 'es';
		}else if($Country == 'Australia'){
			$code = 'au';
		}else if($Country == 'Canada'){
			$code = 'ca';
		}else if($Country == 'United States'){
			$code = 'us';
		}
		return $code;
	}
	public function getCurrencyCode($currency  = null){
		$code = $currency;
		if($currency  == 'GBP'){
			$code = '&pound;';
		}else if($currency  == 'EUR'){
			$code = '&euro;';
		}else if($currency  == 'USD'){
			$code = '&dollar;;';
		}else if($currency  == 'CAD'){
			$code = '&dollar;';
		} 
		return $code;
	}
	public function replaceFrenchChar($string = null){
			
		$unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E','Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Nº'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U','Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c','è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n','nº'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o','ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y','ü'=>'u','º'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}	
	
  	
}

?>

