﻿<?php
error_reporting(1);
ini_set( 'memory_limit' , '2048M' );
class LinnworksapisController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Linnworksapis";
    
    var $components = array('Session','Upload','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
    var $virtualFields;
    
    public function beforeFilter()
	{ 
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->Auth->Allow(array('cancelOrder'));
	   $this->GlobalBarcode = $this->Components->load('Common'); /*Apply code for Global Barcode*/
	   //die('maintenance going on!');
	}
	
    public function index()
	{
			$this->layout = "index";
			$this->set('role', 'Linnworks API');
	}
	
	 public function getCategory()
		{
			/* method's ctp file get category */
			$this->layout = "index";
			$this->set('title', 'Category (Linnworks API)');
		}
	
	 public function getorderStatus()
		{
			/* method's ctp file get order status */
			$this->layout = "index";
			$this->set('title', 'Status (Linnworks API)');

		}
	
	 public function getPostalServices()
		{
			/* method's ctp file get postal services */
			$this->layout = "index";
			$this->set('title', 'Postal Service (Linnworks API)');
		}
	
	 public function getLocations()
		{
			/* method's ctp file get location */
			$this->layout = "index";
			$this->set('title', 'Location (Linnworks API)');
		}
	
	 public function getStockItem()
		{
			/* method's ctp file get stock item */
			$this->layout = "index";
			$this->set('title', 'Stock Items (Linnworks API)');
		}
	public function getOrder( $orderID = null, $pkOrderId = null)
		{
			
			$this->layout = "index";
			if($orderID == 'deleteOrder')
			{
				$delete = 'orderdeleted';
				$this->Session->setflash(  "Order Deleted Successful.",  'flash_success' );
			}
			else
			{
				$delete = '';
			}
			$this->set(compact('orderID', 'pkOrderId','delete'));
		}

	public function getFilterOrder()
	{
		if($this->request->data)
		{
			$data	=	$this->request->data;
			$this->set('data' , $data);
		}
		$this->layout = "index";
		$this->set('title', 'Filter order (Linnworks API)');
	}


	public function downloadExcel()
	{
		$this->autoRender = false;
		$this->layout = '';
		
		$data['Linnworksapi']['order_type'] 	= 	$this->request->data['Linnworksapis']['order_type'];
		$data['Linnworksapi']['location'] 		= 	$this->request->data['Linnworksapis']['location'];
		$data['Linnworksapi']['source'] 		= 	$this->request->data['Linnworksapis']['source'];
		$data['Linnworksapi']['subsource'] 		= 	$this->request->data['Linnworksapis']['subsource'];
		$data['Linnworksapi']['datefrom'] 		= 	$this->request->data['Linnworksapis']['datefrom'];
		$data['Linnworksapi']['dateto'] 		= 	$this->request->data['Linnworksapis']['dateto'];
		$data['Linnworksapi']['orderid'] 		= 	$this->request->data['Linnworksapis']['orderid'];
		App::import('Helper', 'Soap');
		$SoapHelper = new SoapHelper( new View(null) );
		$getData	=	$SoapHelper->getFilteredOrder( $data );
		
		App::import('Helper', 'Number');
		$numberHelper = new NumberHelper( new View(null) );
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');
		$objPHPExcel = new PHPExcel();   
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderItemNumber');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Name');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Address');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Postcode');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Country');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Item Count');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Contents');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Total Packet Value');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Weight');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'HS');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Deposit');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Invoice Number');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Bag barcode');
		
		$i = 2;
		foreach($getData->GetFilteredOrdersResponse->GetFilteredOrdersResult->Orders->Order as $order)
				{
					$contents = array();
					$orderItems = array();
					foreach($order->OrderItems->OrderItem as $item)
						{ 
							$contents[] =	$item->Qty.' X '.$item->ItemTitle;
							$orderItems[] =	$item->OrderItemNumber;
						}
		
					$content = implode(" \n", $contents);
					$orderItem = implode(" \n", $orderItems);
					
					$itemCount	=	count($order->OrderItems->OrderItem);
					
					
					
					$address	=	$order->ShippingAddress->Address1.','.
									$order->ShippingAddress->Address2.','.
									$order->ShippingAddress->Address3;
									
					$address = explode(',', $address);
					$address = implode(" \n ", $address);
		
						
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i.'', $orderItem);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i.'', $order->ShippingAddress->Name);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i.'', $address);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i.'', $order->ShippingAddress->PostCode );
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i.'', $order->ShippingAddress->CountryCode);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i.'', $itemCount);
	
		$totlaCost =	$numberHelper->currency( $order->TotalCost, 'EUR' );
		
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$i.'', $content );
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i.'', $totlaCost);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$i.'', '5');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$i.'', '5');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i.'', '5');
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$i.'', '5');
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$i.'', '5');
		
		$i++;
				}
		
		
		
		if($this->request->data['Linnworksapis']['order_type'] == 0)
		{
			$objPHPExcel->getActiveSheet()->setTitle('Open Order');
			$objPHPExcel->createSheet();
			$name = 'Open Order';
		}
		if($this->request->data['Linnworksapis']['order_type'] == 1)
		{
			$objPHPExcel->getActiveSheet()->setTitle('Processed Order');
			$objPHPExcel->createSheet();
			$name = 'Procesed Order';
		}
		if($this->request->data['Linnworksapis']['order_type'] == 2)
		{
			$objPHPExcel->getActiveSheet()->setTitle('Cancelled Order');
			$objPHPExcel->createSheet();
			$name = 'Cancelled Order';
		}
		
		
		header('Content-Encoding: UTF-8');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment;filename="'.$name.'.csv"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		
		$objWriter->save('php://output');
	}
	
	function compareByName($a = null, $b = null)
	{
	  return strcmp($a["binrack"], $b["binrack"]);
	}
	
	function build_sorter($key)
	{
		return function ($a, $b) use ($key) {
			return strnatcmp($a[$key], $b[$key]);
		};
	}
	
	function aasort (&$array, $key)
	{
		$sorter=array();
		$ret=array();
		reset($array);
		foreach ($array as $ii => $va) {
			$sorter[$ii]=$va[$key];
			
			echo $sorter[$ii] .'='. $va[$key];
			echo "<br>"; 
		}
		asort($sorter);
		foreach ($sorter as $ii => $va) {
			$ret[$ii]=$array[$ii];
		}
		$array=$ret;
	}
	
	/**
	 * Sort a 2 dimensional array based on 1 or more indexes.
	 * 
	 * msort() can be used to sort a rowset like array on one or more
	 * 'headers' (keys in the 2th array).
	 * 
	 * @param array        $array      The array to sort.
	 * @param string|array $key        The index(es) to sort the array on.
	 * @param int          $sort_flags The optional parameter to modify the sorting 
	 *                                 behavior. This parameter does not work when 
	 *                                 supplying an array in the $key parameter. 
	 * 
	 * @return array The sorted array.
	 */
	function msort($array, $key, $sort_flags = SORT_REGULAR) {
		if (is_array($array) && count($array) > 0) {
			if (!empty($key)) {
				$mapping = array();
				foreach ($array as $k => $v) {
					$sort_key = '';
					if (!is_array($key)) {
						$sort_key = $v[$key];
					} else {
						// @TODO This should be fixed, now it will be sorted as string
						foreach ($key as $key_key) {
							$sort_key .= $v[$key_key];
						}
						$sort_flags = SORT_STRING;
					}
					$mapping[$k] = $sort_key;
				}
				asort($mapping, $sort_flags);
				$sorted = array();
				foreach ($mapping as $k => $v) {
					$sorted[] = $array[$k];
				}
				return $sorted;
			}
		}
		return $array;
	}
	
	//New pick list generation with location update with inventory
	public function generatePickList_tip()
	{
		
		$this->layout = '';
		$this->autoRander = false;
		/* start for pick list */
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderItem' );
		$this->loadModel( 'MergeOrder' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		
		$param = array(
				'conditions' => array(
					'MergeUpdate.pick_list_status' => '0',					
					'MergeUpdate.status' => '0',					
						'OR' => array(
							array( 'MergeUpdate.linn_fetch_orders' => '1' ),
							array( 'MergeUpdate.linn_fetch_orders' => '4' )	
						)
				)
			);
		//$pickListItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.status' => 0)));			
		$pickListItems 	=	$this->MergeUpdate->find('all', $param );			
		
		foreach( $pickListItems as $pickListItem )
		{
			$stringitem[]	 =  $pickListItem['MergeUpdate']['product_order_id_identify'];
		}
		$stringitem = implode('---', $stringitem);
		$idsArray	=	explode('---', $stringitem);
		
		foreach( $idsArray as $orderid )
		{
		
			$orderItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.product_order_id_identify' => $orderid)));			
			
			foreach($orderItems as $k => $v)
			{
				
				$splitSku = explode(",", $v['MergeUpdate']['sku']);
				$jk = 0;while( $jk <= count( $splitSku )-1 )
				{
					
					$splitNow = explode( "XS-", $splitSku[$jk] );
					//pr($splitNow);
					
					$id = 'S-'.$splitNow[1];
					$result[$id]['quantity'][] = $splitNow[0];
					
					$skuText = $id;				
					$skuLocation = $this->getSkuLocation( $skuText );
					//$result[$id]['bin'][0] = $skuLocation['Product']['bin_rack'];
					$result[$id]['bin'][0] = '';
					
					//Update bin status
					//$this->MergeUpdate->updateAll(array('MergeUpdate.pick_list_status' => '1'), array('MergeUpdate.id' => $v['MergeUpdate']['id']));
					
					//Update bin status
					$this->MergeUpdate->updateAll(array('MergeUpdate.pick_list_status' => '1'), array('MergeUpdate.id' => $v['MergeUpdate']['id']));
					
				$jk++;	
				}				
			}
		}
		//pr($result);
		//exit;
		$new = array();
		foreach($result as $key => $value)
		{
			$new[] = array('sku' => $key, 'quanity' => array_sum($value['quantity']), 'binrack' => $value['bin'][0]);
		}
		
		$new = Set::sort($new, '{n}.binrack', 'ASC');
		
		
		
		/* dome pdf vendor */
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper('A4', 'portrait');
		/* Html for print pick list */
		$date = date("m/d/Y");
		$j=0;
		foreach($new as $sku)
		{
			$j = $j+$sku['quanity'];
		}
			
		$html = '<h3>PickList :- '.$date.'</h3><hr><h3>Total Item  : - '.$j.'</h3>
				<table border="1" width="100%" style="font-size:12px; margin-top:10px;">
				<tr>
				<th width="10%" align="center">Bin/Rack</th>
				<th width="10%" align="center">SKU</th>
				<th width="28%" align="center">Qty / Item Title</th>
				<th width="10%" align="center">Barcode</th>
				<th width="12%" align="center">Other</th>
				</tr>';

		
		$i = 0;
		$this->loadModel( 'BinLocation' );
		foreach($new as $sku)
		{
			$skuWith	=	explode(',', $sku['sku']);
			
			$product	=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku['sku'])));
			if(count($product) > 0)
			{
				
				$getBinLocations 	=	$this->BinLocation->find('all', array( 'conditions' => array( 'BinLocation.barcode' => $product['ProductDesc']['barcode']) ));
				
				if( $getBinLocations )
				{
					foreach( $getBinLocations as $getBinLocation)
					{
						$bin[]	 =  $getBinLocation['BinLocation']['bin_location'];
					}
					$binlocation = implode(',', $bin);
					$skulocations[$i]['binrack'] = $binlocation;
					unset( $bin );
				}
				else
				{
					$skulocations[$i]['binrack'] = "No bin location.";
				}
				
				if(count($product) > 0)
				{
					$title			=	$product['Product']['product_name'];
					$skulocations[$i]['barcode']	=	$product['ProductDesc']['barcode'];
				}
				else
				{
					$title							=	"Please check inventory";
					$skulocations[$i]['binrack']	=	"Please check inventory";
				}
				$skulocations[$i]['sku'] = $sku['sku'];
				$skulocations[$i]['quantity'] = $sku['quanity'];
				$skulocations[$i]['title'] = $title;
			$i++;
			}
		}
		//pr($skulocations);
		//exit;
		
		//pr($skulocations);
		$skulocate[] = array();
		$jk = 0;while( $jk <= count($skulocations)-1 )
		{
			$binLocate = $skulocations[$jk]['binrack'];
			$binExplode = explode( ',', $binLocate );
			$binText = $binExplode[0];			
			$skulocate[$jk] = $binText;			
		$jk++;	
		}
		
		natsort( $skulocate );		 
		//$skulocate = Set::sort($skulocate, '{n}.binrack', 'ASC');
		
		/*************************************************************************************************/
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'SKU');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Barcode');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Location');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Stock');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Location Quantity');
		
		$inc = 2;
		foreach( $skulocate as $skulocateIndex => $skulocateValue )
		{
			$barcode	=	$barcode = $skulocations[$skulocateIndex]['barcode'];
			
			$getBinLocations		=	$this->BinLocation->find( 'all', array( 'conditions' => array('BinLocation.barcode' =>  $barcode )) );
			$totalLocationQty = 0;
			foreach( $getBinLocations as $getBinLocation )
			{
				
				$totalLocationQty	=	$totalLocationQty + $getBinLocation['BinLocation']['stock_by_location'];
			}
			
			$getProductDescs	=	$this->Product->find( 'first', array( 'conditions' => array( 'ProductDesc.barcode' => $barcode ) ) );
			$stockLevel			=	$getProductDescs['Product']['current_stock_level'];
			
			if( $stockLevel != $totalLocationQty )
			{
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProductDescs['Product']['product_sku'] );
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $barcode );
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getBinLocation['BinLocation']['bin_location']);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockLevel );
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $totalLocationQty );
				$inc++;
			}
		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/checkinventery/checkinventery.csv';
		$uploadRemote = $getBase.'img/checkinventery/checkinventery.csv';
		$path = $getBase.'img/checkinventery/';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		
		$filename = 'checkinventery.csv';
		$mailto	=	'amit.gaur@jijgroup.com';
		$from_mail	=	'webmaster@xsensys.com';
		$from_name = 'webManter';
		$replyto ='';
		$subject = 'reconcilation report';
		$message = 'reconcilation report';
		
		$this->mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message);
		
		/**************************************************************************************************/
		
		
		
		foreach( $skulocate as $skulocateIndex => $skulocateValue )
		{
			$multiSku = $skulocations[$skulocateIndex]['binrack'];
			$getLumtiSku = explode( ',',$multiSku );
			
			$quantityOfLocation = $skulocations[$skulocateIndex]['quantity'];
			$barcode = $skulocations[$skulocateIndex]['barcode'];
			$getNewLocationIndex = '';
			
			if( count($getLumtiSku) > 1 )
			{
				
				$this->loadModel( 'BinLocation' );
				$param = array(
					'conditions' => array(
						'BinLocation.barcode' => $barcode,
						'BinLocation.stock_by_location >' => 0
					)
				);
				$getLocation = $this->BinLocation->find( 'all' , $param );
				$getMaxLocationQuantity = array();
				$getMaxLocation = array();
				$getLocationIndex = array();
				foreach( $getLocation as $index => $value )
				{
					
					$getMaxLocationQuantity[] = $value['BinLocation']['stock_by_location'];
					$getMaxLocation[] = $value['BinLocation']['bin_location'];
					$getLocationIndex[] = $value['BinLocation']['id'];
					
					if( $value['BinLocation']['stock_by_location'] >= $quantityOfLocation )
					{
						$getNewLocationIndex = $value['BinLocation']['bin_location'];						
						break;
					}
					
				}
				
				if( $getNewLocationIndex == '' )
				{
					$returnThis = array_search(max($getMaxLocationQuantity),$getMaxLocationQuantity);
					
					//Get Max location and quantity also
					$getLocationOfSku = $getMaxLocation[$returnThis];
					$getLocationOfQuantity = $getMaxLocationQuantity[$returnThis];
					
					unset($getMaxLocation[$returnThis]);
					sort($getMaxLocation);
					//pr($getMaxLocation);
					
					unset($getMaxLocationQuantity[$returnThis]);
					sort($getMaxLocationQuantity);
					//pr($getMaxLocationQuantity);
					
					$maxQuantityPreffered = $getLocationOfQuantity;
					$locationArray = array();
					$quantityArray = array();
					for( $ist = 0;$ist < count($getMaxLocationQuantity); $ist++ )
					{
						
						$maxQuantityPreffered = $maxQuantityPreffered + $getMaxLocationQuantity[$ist];						
						$locationArray[$ist] = $getMaxLocation[$ist];
						$quantityArray[$ist] = $getMaxLocationQuantity[$ist];
						
						if( $maxQuantityPreffered >= $quantityOfLocation )							
						{	
							$quantValue = $getMaxLocationQuantity[$ist];							
							$totalQuantityNow = $maxQuantityPreffered - ($maxQuantityPreffered - $quantityOfLocation);
							
							$quantityArray[$ist] = $quantValue - ($maxQuantityPreffered - $quantityOfLocation);
							break;
						}
						else
						{
							$totalQuantityNow = $maxQuantityPreffered;
						}
						
					}
					
					$prefferedGetLocation = '(<b>'.$getLocationOfQuantity.'</b>)'.$getLocationOfSku;
					
					$implodeOtherLocation = '';
					for( $intO = 0; $intO < count($locationArray); $intO++ )
					{
						if( $quantityArray[$intO] > 0 )
						{
							if( $implodeOtherLocation == '' )
							{
								$implodeOtherLocation = '(<b>'.$quantityArray[$intO].'</b>)'.$locationArray[$intO];
							}
							else
							{
								$implodeOtherLocation .= ' <br /> ' . '(<b>'.$quantityArray[$intO].'</b>)'.$locationArray[$intO];
							}
						}
					}
					
					$prefferedGetLocation = $prefferedGetLocation.'<br />'.$implodeOtherLocation;
					
					$setLocation = explode(',',str_replace('</b>)','#',str_replace('(<b>','#',str_replace('<br />', ',' ,$prefferedGetLocation))));
					
					for( $k = 0; $k < count( $setLocation ); $k++ )
					{
						$locationFind = explode('#',$setLocation[$k]);
						$getStock = $this->BinLocation->find( 'first', array( 'conditions' => array( 'BinLocation.barcode' => $skulocations[$skulocateIndex]['barcode'] , 'BinLocation.bin_location' => $locationFind[2] ) ) );
						
						$currentLocationStock = $locationFind[1];
						$getDeductStock = $getStock['BinLocation']['stock_by_location'];
						
						$getMinLevel = $getDeductStock - $currentLocationStock;
						if( $getMinLevel == 0 )
						{
							$data['BinLocation']['id'] = $getStock['BinLocation']['id'];
							$data['BinLocation']['stock_by_location'] = 0;
							$this->BinLocation->saveAll( $data );
							
							$this->loadModel( 'FreeLocation' );
							$dataFree['FreeLocation']['bin_location'] = $getStock['BinLocation']['bin_location'];
							$this->FreeLocation->saveAll( $dataFree );
							unset($dataFree);
							$dataFree = '';
							
							$this->BinLocation->delete( $getStock['BinLocation']['id'] );				
							
							//pr($data);
						}
						else
						{
							$data['BinLocation']['id'] = $getStock['BinLocation']['id'];
							$data['BinLocation']['stock_by_location'] = $getMinLevel;
							$this->BinLocation->saveAll( $data );
							//pr($data);
						}
						
						
					}
					
					$html.=	'<tr>
					<td>'.$getLocationOfSku.'</td>
					<td>'.$skulocations[$skulocateIndex]['sku'].'</td>
					<td align="left"><b>'.$skulocations[$skulocateIndex]['quantity'] .'</b> X '.substr($skulocations[$skulocateIndex]['title'], 0, 42 ).'</td>
					<td>'.$skulocations[$skulocateIndex]['barcode'].'</td>
					<td>'.str_replace(',','<br />',$prefferedGetLocation).'</td>
					</tr>';
					
					$implodeOtherLocation = '';
					
					//Unset everything
					unset($quantityArray);
					$quantityArray = '';
					
					unset($locationArray);
					$locationArray = '';
					
					unset($getMaxLocation);
					$getMaxLocation = '';
					
					unset($getMaxLocationQuantity);
					$getMaxLocationQuantity = '';
					
					unset($getLocationIndex);
					$getLocationIndex = '';
				}
				else
				{
					$html.=	'<tr>
					<td>'.$getNewLocationIndex.'</td>
					<td>'.$skulocations[$skulocateIndex]['sku'].'</td>
					<td align="left"><b>'.$skulocations[$skulocateIndex]['quantity'] .'</b> X '.substr($skulocations[$skulocateIndex]['title'], 0, 42 ).'</td>
					<td>'.$skulocations[$skulocateIndex]['barcode'].'</td>
					<td>'.$getNewLocationIndex.'</td>
					</tr>';
					
					$getStock = $this->BinLocation->find( 'first', array( 'conditions' => array( 'BinLocation.barcode' => $skulocations[$skulocateIndex]['barcode'] , 'BinLocation.bin_location' => $skulocations[$skulocateIndex]['binrack'] ) ) );
						
					$currentLocationStock = $skulocations[$skulocateIndex]['quantity'];
					$getDeductStock = $getStock['BinLocation']['stock_by_location'];
					
					$getMinLevel = $getDeductStock - $currentLocationStock;
					if( $getMinLevel == 0 )
					{
						$data['BinLocation']['id'] = $getStock['BinLocation']['id'];
						$data['BinLocation']['stock_by_location'] = 0;
						$this->BinLocation->saveAll( $data );
						
						$this->loadModel( 'FreeLocation' );
						$dataFree['FreeLocation']['bin_location'] = $getStock['BinLocation']['bin_location'];
						$this->FreeLocation->saveAll( $dataFree );
						unset($dataFree);
						$dataFree = '';
						
						$this->BinLocation->delete( $getStock['BinLocation']['id'] );	
												
					}
					else
					{
						$data['BinLocation']['id'] = $getStock['BinLocation']['id'];
						$data['BinLocation']['stock_by_location'] = $getMinLevel;
						$this->BinLocation->saveAll( $data );
					}
					
				}	
				
				
			}
			else
			{
				$html.=	'<tr>
				<td>'.$skulocations[$skulocateIndex]['binrack'].'</td>
				<td>'.$skulocations[$skulocateIndex]['sku'].'</td>
				<td align="left"><b>'.$skulocations[$skulocateIndex]['quantity'] .'</b> X '.substr($skulocations[$skulocateIndex]['title'], 0, 42 ).'</td>
				<td>'.$skulocations[$skulocateIndex]['barcode'].'</td>
				<td>'.$skulocations[$skulocateIndex]['binrack'].'</td>
				</tr>';
				
				$getStock = $this->BinLocation->find( 'first', array( 'conditions' => array( 'BinLocation.barcode' => $skulocations[$skulocateIndex]['barcode'] , 'BinLocation.bin_location' => $skulocations[$skulocateIndex]['binrack'] ) ) );
						
				$currentLocationStock = $skulocations[$skulocateIndex]['quantity'];
				$getDeductStock = $getStock['BinLocation']['stock_by_location'];
				
				$getMinLevel = $getDeductStock - $currentLocationStock;
				if( $getMinLevel == 0 )
				{
					$data['BinLocation']['id'] = $getStock['BinLocation']['id'];
					$data['BinLocation']['stock_by_location'] = 0;
					$this->BinLocation->saveAll( $data );	
					
					$this->loadModel( 'FreeLocation' );
					$dataFree['FreeLocation']['bin_location'] = $getStock['BinLocation']['bin_location'];
					$this->FreeLocation->saveAll( $dataFree );
					unset($dataFree);
					$dataFree = '';
					
					$this->BinLocation->delete( $getStock['BinLocation']['id'] );	
								
				}
				else
				{
					$data['BinLocation']['id'] = $getStock['BinLocation']['id'];
					$data['BinLocation']['stock_by_location'] = $getMinLevel;
					$this->BinLocation->saveAll( $data );	
				}
				
			}	
			
		}
		
		/*$html .= '</table>';
		
		$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
		$dompdf->render();
		
		date_default_timezone_set('Europe/Jersey');
		$today = date("d_m_Y_H_i");
		//$dompdf->stream("Pick_List_(".$date.").pdf");
		
		$imgPath = WWW_ROOT .'img/printPickList/'; 
		$path = Router::url('/', true).'img/printPickList/';
		$name	=	'Pick_List_'.$name.'_'.$today.'.pdf';
		
		file_put_contents($imgPath.$name, $dompdf->output());
		echo $serverPath  	= 	$path.$name ;
		exit;*/
		
		$html .= '</table>';
		$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
		$dompdf->render();
		
		date_default_timezone_set('Europe/Jersey');
		$today = date("d_m_Y_H_i");
		//$dompdf->stream("Pick_List_(".$date.").pdf");
		
		$imgPath = WWW_ROOT .'img/printPickList/'; 
		$path = Router::url('/', true).'img/printPickList/';
		$name	=	'Pick_List_'.$name.'_'.$today.'.pdf';
		
		file_put_contents($imgPath.$name, $dompdf->output());
		
		//Remove if any location has 0 data
		//$this->BinLocation->query( 'Delete from bin_locations where stock_by_location = 0' );
		
		echo $serverPath  	= 	$path.$name ;
		exit;
				
	}
	
	public function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {
			$file = $path.$filename;
			$file_size = filesize($file);
			$handle = fopen($file, "r");
			$content = fread($handle, $file_size);
			fclose($handle);
			$content = chunk_split(base64_encode($content));
			$uid = md5(uniqid(time()));
			$header = "From: ".$from_name." <".$from_mail.">\r\n";
			$header .= "Reply-To: ".$replyto."\r\n";
			$header .= "MIME-Version: 1.0\r\n";
			$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
			$header .= "This is a multi-part message in MIME format.\r\n";
			$header .= "--".$uid."\r\n";
			$header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
			$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
			$header .= $message."\r\n\r\n";
			$header .= "--".$uid."\r\n";
			$header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
			$header .= "Content-Transfer-Encoding: base64\r\n";
			$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
			$header .= $content."\r\n\r\n";
			$header .= "--".$uid."--";
			if (mail($mailto, $subject, "", $header)) {
				echo "mail send ... OK"; // or use booleans here
			} else {
				echo "mail send ... ERROR!";
			}
		}
	
	
	private function updateAbortFile()
	{
		
		$this->layout = '';
		$this->autoRender = false;
		
		//Authorization
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		$AbortFilePath = WWW_ROOT .'img/Abort/Ajax/Route/'; 
		$dir = new Folder($AbortFilePath, true, 0755);
		$fileName = "abortPicklist.txt";
		file_put_contents($AbortFilePath . $fileName, "1");
		
	//now get
	return (string)file_put_contents($AbortFilePath . $fileName);
	}
	
	//original method invocation and generate the file 
	public function generatePickList()
	{
		
		$this->layout = '';
		$this->autoRander = false;
		
		//abort status
		$fileStatus = $this->updateAbortFile();
		
		/* start for pick list */
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderItem' );
		$this->loadModel( 'MergeOrder' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		
		$param = array(
				'conditions' => array(
					'MergeUpdate.pick_list_status' => '0',					
					'MergeUpdate.status' => '0',		
						'OR' => array(
							array( 'MergeUpdate.linn_fetch_orders' => '1' ),
							array( 'MergeUpdate.linn_fetch_orders' => '4' )	
						)
				)
			);
		//$pickListItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.status' => 0)));			
		$pickListItems 	=	$this->MergeUpdate->find('all', $param );			
		
		foreach( $pickListItems as $pickListItem )
		{
			$stringitem[]	 =  $pickListItem['MergeUpdate']['product_order_id_identify'];
		}
		
		if( count( $stringitem ) > 0 )
		{
			//manage userd - id with name
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
			
			$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
			
			//manage order ids
			$this->customLogGlobal( 'Pick List [ ' . $username . '__' . date( 'Y-m-d j:i:s' ) . ' ]' , implode( "," , $stringitem ) );
		}
		
		$stringitem = implode('---', $stringitem);
		$idsArray	=	explode('---', $stringitem);
		
		foreach( $idsArray as $orderid )
		{
		
			$orderItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.product_order_id_identify' => $orderid)));			
			
			foreach($orderItems as $k => $v)
			{
				
				$splitSku = explode(",", $v['MergeUpdate']['sku']);
				$jk = 0;while( $jk <= count( $splitSku )-1 )
				{
					
					$splitNow = explode( "XS-", $splitSku[$jk] );
					//pr($splitNow);
					
					$id = 'S-'.$splitNow[1];
					$result[$id]['quantity'][] = $splitNow[0];
					
					$skuText = $id;				
					$skuLocation = $this->getSkuLocation( $skuText );
					//$result[$id]['bin'][0] = $skuLocation['Product']['bin_rack'];
					$result[$id]['bin'][0] = '';
					
					//Update bin status
					//$this->MergeUpdate->updateAll(array('MergeUpdate.pick_list_status' => '1'), array('MergeUpdate.id' => $v['MergeUpdate']['id']));
					
					//Update bin status
					$this->MergeUpdate->updateAll(array('MergeUpdate.pick_list_status' => '1'), array('MergeUpdate.id' => $v['MergeUpdate']['id']));
					
				$jk++;	
				}				
			}
		}
		//pr($result);
		//exit;
		$new = array();
		foreach($result as $key => $value)
		{
			$new[] = array('sku' => $key, 'quanity' => array_sum($value['quantity']), 'binrack' => $value['bin'][0]);
		}
		
		$new = Set::sort($new, '{n}.binrack', 'ASC');
		
		
		
		/* dome pdf vendor */
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper('A4', 'portrait');
		/* Html for print pick list */
		$date = date("m/d/Y H:i:s");
		$j=0;
		foreach($new as $sku)
		{
			$j = $j+$sku['quanity'];
		}
			
		$html = '<h3>PickList :- '.$date.'</h3><hr><h3>Total Item  : - '.$j.'</h3>
				<table border="1" width="100%" style="font-size:12px; margin-top:10px;">
				<tr>
				<th width="10%" align="center">Bin/Rack</th>
				<th width="10%" align="center">SKU</th>
				<th width="28%" align="center">Qty / Item Title</th>
				<th width="10%" align="center">Barcode</th>
				<th width="12%" align="center">Other</th>
				</tr>';
		
		$i = 0;
		$this->loadModel( 'BinLocation' );
		foreach($new as $sku)
		{
			$skuWith	=	explode(',', $sku['sku']);
			
			$product	=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku['sku'])));
			if(count($product) > 0)
			{
				
				$getBinLocations 	=	$this->BinLocation->find('all', array( 'conditions' => array( 'BinLocation.barcode' => $product['ProductDesc']['barcode']) ));
				
				if( $getBinLocations )
				{
					$stockLocation = array();$stockCounter = 0;foreach( $getBinLocations as $getBinLocation)
					{
						if( $getBinLocation['BinLocation']['stock_by_location'] > 0 )
						{
							$stockCounter = $stockCounter + $getBinLocation['BinLocation']['stock_by_location'];
							$stockLocation[] = $getBinLocation['BinLocation']['bin_location'];
						}
						
						$bin[]	 =  $getBinLocation['BinLocation']['bin_location'];
					}
					$binlocation = implode(',', $bin);					
					unset( $bin );
					
					if( $stockCounter == 0 )
					{
						$getSpeacialBibLocation	= $this->BinLocation->find('first', array( 'conditions' => array( 'BinLocation.barcode' => $product['ProductDesc']['barcode']) ));
						$skulocations[$i]['binrack'] = $getSpeacialBibLocation['BinLocation']['bin_location'];
					}					
					else
					{
					    $skulocations[$i]['binrack'] = implode( $stockLocation , ',' ); //$binlocation;
					}    
					$stockCounter = 0; 
					unset($stockLocation);
					$stockLocation = '';
					   		
				}
				else
				{
					$skulocations[$i]['binrack'] = "No bin location.";
				}
				
				if(count($product) > 0)
				{
					$title			=	$product['Product']['product_name'];
					$skulocations[$i]['barcode']	=	$product['ProductDesc']['barcode'];
				}
				else
				{
					$title							=	"Please check inventory";
					$skulocations[$i]['binrack']	=	"Please check inventory";
				}
				$skulocations[$i]['sku'] = $sku['sku'];
				$skulocations[$i]['quantity'] = $sku['quanity'];
				$skulocations[$i]['title'] = $title;
			$i++;
			}
		}
		
		//pr($skulocations);
		$skulocate[] = array();
		$jk = 0;while( $jk <= count($skulocations)-1 )
		{
			$binLocate = $skulocations[$jk]['binrack'];
			$binExplode = explode( ',', $binLocate );
			$binText = $binExplode[0];			
			$skulocate[$jk] = $binText;			
		$jk++;	 
		}
		
		natsort( $skulocate );		 
		//$skulocate = Set::sort($skulocate, '{n}.binrack', 'ASC');
		
		foreach( $skulocate as $skulocateIndex => $skulocateValue )
		{
			$multiSku = $skulocations[$skulocateIndex]['binrack'];
			$getLumtiSku = explode( ',',$multiSku );
			
			$quantityOfLocation = $skulocations[$skulocateIndex]['quantity'];
			$barcode = $skulocations[$skulocateIndex]['barcode'];
			$getNewLocationIndex = '';
			
			if( count($getLumtiSku) > 1 )
			{
				
				$this->loadModel( 'BinLocation' );
				$param = array(
					'conditions' => array(
						'BinLocation.barcode' => $barcode,
						'BinLocation.stock_by_location >' => 0
					)
				);
				$getLocation = $this->BinLocation->find( 'all' , $param );
				
				$getMaxLocationQuantity = array();
				$getMaxLocation = array();
				$getLocationIndex = array();
				
				foreach( $getLocation as $index => $value )
				{
					
					$getMaxLocationQuantity[] = $value['BinLocation']['stock_by_location'];
					$getMaxLocation[] = $value['BinLocation']['bin_location'];
					$getLocationIndex[] = $value['BinLocation']['id'];
					
					if( $value['BinLocation']['stock_by_location'] >= $quantityOfLocation )
					{
						$getNewLocationIndex = $value['BinLocation']['bin_location'];						
						break;
					}
					
				}
				
				if( $getNewLocationIndex == '' )
				{
					
					$returnThis = array_search(max($getMaxLocationQuantity),$getMaxLocationQuantity);
					
					//Get Max location and quantity also
					$getLocationOfSku = $getMaxLocation[$returnThis];
					
					$getLocationOfQuantity = $getMaxLocationQuantity[$returnThis];
					
					unset($getMaxLocation[$returnThis]);
					$getMaxLocation = array_merge($getMaxLocation);
					
					unset($getMaxLocationQuantity[$returnThis]);
					$getMaxLocationQuantity = array_merge($getMaxLocationQuantity);
					
					$maxQuantityPreffered = $getLocationOfQuantity; //18
					$locationArray = array();
					$quantityArray = array();
					
					$inc = 0;for( $ist = 0;$ist < count($getMaxLocationQuantity); $ist++ )
					{
						
						$maxQuantityPreffered = $maxQuantityPreffered + $getMaxLocationQuantity[$ist];						
						$locationArray[$ist] = $getMaxLocation[$ist];
						$quantityArray[$ist] = $getMaxLocationQuantity[$ist];
						
						if( $maxQuantityPreffered >= $quantityOfLocation )							
						{	
							$quantValue = $getMaxLocationQuantity[$ist];							
							$totalQuantityNow = $maxQuantityPreffered - ($maxQuantityPreffered - $quantityOfLocation);
							
							$quantityArray[$ist] = $quantValue - ($maxQuantityPreffered - $quantityOfLocation);
							
							$inc++;
							break;
						}
						else
						{
							$inc++;
							$totalQuantityNow = $maxQuantityPreffered;
						}
						
					}
					
					$quantityArray[$inc+1] = $getLocationOfQuantity;
					$locationArray[$inc+1] = $getLocationOfSku;
					
					$quantityArray = array_merge($quantityArray);
					$locationArray = array_merge($locationArray);
							
					//pr($quantityArray);
					//pr($locationArray);
					
					//exit;
					//$prefferedGetLocation = '(<b>'.$getLocationOfQuantity.'</b>)'.$getLocationOfSku;
					
					$implodeOtherLocation = '';
					for( $intO = 0; $intO < count($locationArray); $intO++ )
					{
						if( $quantityArray[$intO] > 0 )
						{
							if( $implodeOtherLocation == '' )
							{
								$implodeOtherLocation = '(<b>'.$quantityArray[$intO].'</b>)'.$locationArray[$intO];
							}
							else
							{
								$implodeOtherLocation .= ' <br /> ' . '(<b>'.$quantityArray[$intO].'</b>)'.$locationArray[$intO];
							}
						}
					}
					
					$prefferedGetLocation = $implodeOtherLocation;
					
					$setLocation = explode(',',str_replace('</b>)','#',str_replace('(<b>','#',str_replace('<br />', ',' ,$prefferedGetLocation))));
					
					$setLocation = $quantityArray;
					
					for( $k = 0; $k < count( $setLocation ); $k++ )
					{
						$locationFind = $locationArray[$k];
						$getStock = $this->BinLocation->find( 'first', array( 'conditions' => array( 'BinLocation.barcode' => $skulocations[$skulocateIndex]['barcode'] , 'BinLocation.bin_location' => $locationFind ) ) );
						
						$currentLocationStock = $quantityArray[$k];
						$getDeductStock = $getStock['BinLocation']['stock_by_location'];
						
						$getMinLevel = $getDeductStock - $currentLocationStock;
						if( $getMinLevel == 0 )
						{
							$data['BinLocation']['id'] = $getStock['BinLocation']['id'];
							$data['BinLocation']['stock_by_location'] = 0;
							$this->BinLocation->saveAll( $data );
							
							$this->loadModel( 'FreeLocation' );
							$dataFree['FreeLocation']['bin_location'] = $getStock['BinLocation']['bin_location'];
							//$this->FreeLocation->saveAll( $dataFree );
							unset($dataFree);
							$dataFree = '';
							
							//$this->BinLocation->delete( $getStock['BinLocation']['id'] );				
							
							//pr($data);
						}
						else
						{
							$data['BinLocation']['id'] = $getStock['BinLocation']['id'];
							$data['BinLocation']['stock_by_location'] = $getMinLevel;
							$this->BinLocation->saveAll( $data );
							//pr($data);
						}
						
						
					}
					
					$html.=	'<tr>
					<td>'.$getLocationOfSku.'</td>
					<td>'.$skulocations[$skulocateIndex]['sku'].'</td>
					<td align="left"><b>'.$skulocations[$skulocateIndex]['quantity'] .'</b> X '.substr($skulocations[$skulocateIndex]['title'], 0, 42 ).'</td>
					<td>'.$skulocations[$skulocateIndex]['barcode'].'</td>
					<td>'.str_replace(',','<br />',$prefferedGetLocation).'</td>
					</tr>';
					
					$implodeOtherLocation = '';
					
					//Unset everything
					unset($quantityArray);
					$quantityArray = '';
					
					unset($locationArray);
					$locationArray = '';
					
					unset($getMaxLocation);
					$getMaxLocation = '';
					
					unset($getMaxLocationQuantity);
					$getMaxLocationQuantity = '';
					
					unset($getLocationIndex);
					$getLocationIndex = '';
				}
				else
				{
					$html.=	'<tr>
					<td>'.$getNewLocationIndex.'</td>
					<td>'.$skulocations[$skulocateIndex]['sku'].'</td>
					<td align="left"><b>'.$skulocations[$skulocateIndex]['quantity'] .'</b> X '.substr($skulocations[$skulocateIndex]['title'], 0, 42 ).'</td>
					<td>'.$skulocations[$skulocateIndex]['barcode'].'</td>
					<td>'.$getNewLocationIndex.'</td>
					</tr>';
					
					$getStock = $this->BinLocation->find( 'first', array( 'conditions' => array( 'BinLocation.barcode' => $skulocations[$skulocateIndex]['barcode'] , 'BinLocation.bin_location' => $getNewLocationIndex ) ) );						
					$currentLocationStock = $skulocations[$skulocateIndex]['quantity'];
					$getDeductStock = $getStock['BinLocation']['stock_by_location'];
					
					$getMinLevel = $getDeductStock - $currentLocationStock;
					if( $getMinLevel == 0 )
					{
						$data['BinLocation']['id'] = $getStock['BinLocation']['id'];
						$data['BinLocation']['stock_by_location'] = 0;
						$this->BinLocation->saveAll( $data );
						
						$this->loadModel( 'FreeLocation' );
						$dataFree['FreeLocation']['bin_location'] = $getStock['BinLocation']['bin_location'];
						//$this->FreeLocation->saveAll( $dataFree );
						unset($dataFree);
						$dataFree = '';		
						//pr($data);
						//$this->BinLocation->delete( $getStock['BinLocation']['id'] );				
								
					}
					else
					{
						$data['BinLocation']['id'] = $getStock['BinLocation']['id'];
						$data['BinLocation']['stock_by_location'] = $getMinLevel;
						$this->BinLocation->saveAll( $data );
						//pr($data);
					}
					
				}	
				
				
			}
			else
			{
				
				$html.=	'<tr>
				<td>'.$skulocations[$skulocateIndex]['binrack'].'</td>
				<td>'.$skulocations[$skulocateIndex]['sku'].'</td>
				<td align="left"><b>'.$skulocations[$skulocateIndex]['quantity'] .'</b> X '.substr($skulocations[$skulocateIndex]['title'], 0, 42 ).'</td>
				<td>'.$skulocations[$skulocateIndex]['barcode'].'</td>
				<td>'.$skulocations[$skulocateIndex]['binrack'].'</td>
				</tr>';
				
				$getStock = $this->BinLocation->find( 'first', array( 'conditions' => array( 'BinLocation.barcode' => $skulocations[$skulocateIndex]['barcode'] , 'BinLocation.bin_location' => $skulocations[$skulocateIndex]['binrack'] ) ) );
					
				$currentLocationStock = $skulocations[$skulocateIndex]['quantity'];
				$getDeductStock = $getStock['BinLocation']['stock_by_location'];
				
				$getMinLevel = $getDeductStock - $currentLocationStock;
				if( $getMinLevel == 0 )
				{
					$data['BinLocation']['id'] = $getStock['BinLocation']['id'];
					$data['BinLocation']['stock_by_location'] = 0;
					$this->BinLocation->saveAll( $data );	
					
					$this->loadModel( 'FreeLocation' );
					$dataFree['FreeLocation']['bin_location'] = $getStock['BinLocation']['bin_location'];
					//$this->FreeLocation->saveAll( $dataFree );
					unset($dataFree);
					$dataFree = '';		
					//pr($data);
					//$this->BinLocation->delete( $getStock['BinLocation']['id'] );				
							
				}
				else
				{
					$data['BinLocation']['id'] = $getStock['BinLocation']['id'];
					$data['BinLocation']['stock_by_location'] = $getMinLevel;
					$this->BinLocation->saveAll( $data );	
					//pr($data);
				}
				
			}	
			
		}
		
		$html .= '</table>';
		$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
		$dompdf->render();
		
		date_default_timezone_set('Europe/Jersey');
		$today = date("d_m_Y_H_i");
		//$dompdf->stream("Pick_List_(".$date.").pdf");
		
		$imgPath = WWW_ROOT .'img/printPickList/'; 
		$path = Router::url('/', true).'img/printPickList/';
		$name	=	'Pick_List_'.$name.'_'.$today.'.pdf';
		
		file_put_contents($imgPath.$name, $dompdf->output());
		
		//now abort file status down
		$this->updateAbortFileAtLast();
		$fileStatus = 0;
		echo $serverPath  	= 	$path.$name ;
		exit;
				
	}
	
	private function updateAbortFileAtLast()
	{
		
		$this->layout = '';
		$this->autoRender = false;
		
		//Authorization
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		$AbortFilePath = WWW_ROOT .'img/Abort/Ajax/Route/'; 
		$dir = new Folder($AbortFilePath, true, 0755);
		$fileName = "abortPicklist.txt";
		file_put_contents($AbortFilePath . $fileName, "0");
		
	}
	
	public function generatePickList_old090216()
	{
		
		$this->layout = '';
		$this->autoRander = false;
		/* start for pick list */
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderItem' );
		$this->loadModel( 'MergeOrder' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		
		$param = array(
				'conditions' => array(
					'MergeUpdate.pick_list_status' => '0',					
					'MergeUpdate.status' => '0',					
						'OR' => array(
							array( 'MergeUpdate.linn_fetch_orders' => '1' ),
							array( 'MergeUpdate.linn_fetch_orders' => '4' )	
						)
				)
			);
		//$pickListItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.status' => 0)));			
		$pickListItems 	=	$this->MergeUpdate->find('all', $param );			
		
		foreach( $pickListItems as $pickListItem )
		{
			$stringitem[]	 =  $pickListItem['MergeUpdate']['product_order_id_identify'];
		}
		$stringitem = implode('---', $stringitem);
		$idsArray	=	explode('---', $stringitem);
		
		foreach( $idsArray as $orderid )
		{
		
			$orderItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.product_order_id_identify' => $orderid)));			
			
			foreach($orderItems as $k => $v)
			{
				
				$splitSku = explode(",", $v['MergeUpdate']['sku']);
				$jk = 0;while( $jk <= count( $splitSku )-1 )
				{
					
					$splitNow = explode( "XS-", $splitSku[$jk] );
					//pr($splitNow);
					
					$id = 'S-'.$splitNow[1];
					$result[$id]['quantity'][] = $splitNow[0];
					
					$skuText = $id;				
					$skuLocation = $this->getSkuLocation( $skuText );
					//$result[$id]['bin'][0] = $skuLocation['Product']['bin_rack'];
					$result[$id]['bin'][0] = '';
					
					//Update bin status
					$this->MergeUpdate->updateAll(array('MergeUpdate.pick_list_status' => '1'), array('MergeUpdate.id' => $v['MergeUpdate']['id']));
					
				$jk++;	
				}				
			}
		}
		//pr($result);
		//exit;
		$new = array();
		foreach($result as $key => $value)
		{
			$new[] = array('sku' => $key, 'quanity' => array_sum($value['quantity']), 'binrack' => $value['bin'][0]);
		}
		
		$new = Set::sort($new, '{n}.binrack', 'ASC');
		
		
		
		/* dome pdf vendor */
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper('A4', 'portrait');
		/* Html for print pick list */
		$date = date("m/d/Y");
		$j=0;
		foreach($new as $sku)
			{
				$j = $j+$sku['quanity'];
			}
			
		$html = '<h3>PickList :- '.$date.'</h3><hr><h3>Total Item  : - '.$j.'</h3>
				<table border="1" width="100%" style="font-size:12px; margin-top:10px;">
				<tr>
				<th width="10%" align="center">Bin/Rack</th>
				<th width="10%" align="center">SKU</th>
				<th width="28%" align="center">Qty / Item Title</th>
				<th width="10%" align="center">Barcode</th>
				<th width="12%" align="center">Other</th>
				</tr>';
		$i = 0; 
		
		
		
		$this->loadModel( 'BinLocation' );
		foreach($new as $sku)
		{
			$skuWith	=	explode(',', $sku['sku']);
			
			$product	=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku['sku'])));
			if(count($product) > 0)
			{
			$getBinLocations 	=	$this->BinLocation->find('all', array( 'conditions' => array( 'BinLocation.barcode' => $product['ProductDesc']['barcode']) ));
			
			if( $getBinLocations )
			{
				foreach( $getBinLocations as $getBinLocation)
				{
					$bin[]	 =  $getBinLocation['BinLocation']['bin_location'];
				}
				$binlocation = implode(',', $bin);
				$skulocations[$i]['binrack'] = $binlocation;
				unset( $bin );
			}
			else
			{
				$skulocations[$i]['binrack'] = "No bin location.";
			}
			
			if(count($product) > 0)
			{
				$title			=	$product['Product']['product_name'];
				$skulocations[$i]['barcode']	=	$product['ProductDesc']['barcode'];
			}
			else
			{
				$title							=	"Please check inventory";
				$skulocations[$i]['binrack']	=	"Please check inventory";
			}
			$skulocations[$i]['sku'] = $sku['sku'];
			$skulocations[$i]['quantity'] = $sku['quanity'];
			$skulocations[$i]['title'] = $title;
			$i++;
			}
		}
		//pr($skulocations);
		//exit;
		
		//pr($skulocations);
		$skulocate[] = array();
		$jk = 0;while( $jk <= count($skulocations)-1 )
		{
			$binLocate = $skulocations[$jk]['binrack'];
			$binExplode = explode( ',', $binLocate );
			$binText = $binExplode[0];			
			$skulocate[$jk] = $binText;			
		$jk++;	
		}
		
		natsort( $skulocate );		 
		//$skulocate = Set::sort($skulocate, '{n}.binrack', 'ASC');
		
		foreach( $skulocate as $skulocateIndex => $skulocateValue )
		{
			$multiSku = $skulocations[$skulocateIndex]['binrack'];
			$getLumtiSku = explode( ',',$multiSku );
			
			$quantityOfLocation = $skulocations[$skulocateIndex]['quantity'];
			$barcode = $skulocations[$skulocateIndex]['barcode'];
			$getNewLocationIndex = '';
			
			if( count($getLumtiSku) > 1 )
			{
				
				$this->loadModel( 'BinLocation' );
				$param = array(
					'conditions' => array(
						'BinLocation.barcode' => $barcode
					)
				);
				$getLocation = $this->BinLocation->find( 'all' , $param );
				$getMaxLocationQuantity = array();
				$getMaxLocation = array();
				
				foreach( $getLocation as $index => $value )
				{
					
					$getMaxLocationQuantity[] = $value['BinLocation']['stock_by_location'];
					$getMaxLocation[] = $value['BinLocation']['bin_location'];
					
					if( $value['BinLocation']['stock_by_location'] >= $quantityOfLocation )
					{
						$getNewLocationIndex = $value['BinLocation']['bin_location'];						
						break;
					}
					
				}
				
				if( $getNewLocationIndex == '' )
				{
					//pr($getMaxLocationQuantity);
					$returnThis = array_search(max($getMaxLocationQuantity),$getMaxLocationQuantity);
					
					//Get Max location and quantity also
					$getLocationOfSku = $getMaxLocation[$returnThis];
					$getLocationOfQuantity = $getMaxLocationQuantity[$returnThis];
					
					unset($getMaxLocation[$returnThis]);
					sort($getMaxLocation);
					//pr($getMaxLocation);
					
					unset($getMaxLocationQuantity[$returnThis]);
					sort($getMaxLocationQuantity);
					//pr($getMaxLocationQuantity);
					
					$maxQuantityPreffered = $getLocationOfQuantity;
					$locationArray = array();
					$quantityArray = array();
					for( $ist = 0;$ist < count($getMaxLocationQuantity); $ist++ )
					{
						
						/*if( $maxQuantityPreffered + $getMaxLocationQuantity[$ist] <= $quantityOfLocation )
						{
							$maxQuantityPreffered = $maxQuantityPreffered + $getMaxLocationQuantity[$ist];
							$locationArray[$ist] = $getMaxLocation[$ist];							
						}*/						
						
						$maxQuantityPreffered = $maxQuantityPreffered + $getMaxLocationQuantity[$ist];
						
						$locationArray[$ist] = $getMaxLocation[$ist];
						$quantityArray[$ist] = $getMaxLocationQuantity[$ist];
						
						if( $maxQuantityPreffered >= $quantityOfLocation )							
						{	
							$quantValue = $getMaxLocationQuantity[$ist];							
							$totalQuantityNow = $maxQuantityPreffered - ($maxQuantityPreffered - $quantityOfLocation);
							
							$quantityArray[$ist] = $quantValue - ($maxQuantityPreffered - $quantityOfLocation);
							break;
						}
						else
						{
							$totalQuantityNow = $maxQuantityPreffered;
						}
						
					}
					
					$prefferedGetLocation = '(<b>'.$getLocationOfQuantity.'</b>)'.$getLocationOfSku;
					$implodeOtherLocation = '';
					for( $intO = 0; $intO < count($locationArray); $intO++ )
					{
						
						if( $implodeOtherLocation == '' )
						{
							$implodeOtherLocation = '(<b>'.$quantityArray[$intO].'</b>)'.$locationArray[$intO];
						}
						else
						{
							$implodeOtherLocation .= ' <br /> ' . '(<b>'.$quantityArray[$intO].'</b>)'.$locationArray[$intO];
						}
						
					}
					
					$prefferedGetLocation = $prefferedGetLocation.'<br />'.$implodeOtherLocation;
					
					$html.=	'<tr>
					<td>'.$getLocationOfSku.'</td>
					<td>'.$skulocations[$skulocateIndex]['sku'].'</td>
					<td align="left"><b>'.$skulocations[$skulocateIndex]['quantity'] .'</b> X '.substr($skulocations[$skulocateIndex]['title'], 0, 42 ).'</td>
					<td>'.$skulocations[$skulocateIndex]['barcode'].'</td>
					<td>'.str_replace(',','<br />',$prefferedGetLocation).'</td>
					</tr>';
					
					$implodeOtherLocation = '';

					//Unset everything
					unset($quantityArray);
					$quantityArray = '';
					
					unset($locationArray);
					$locationArray = '';
					
					unset($getMaxLocation);
					$getMaxLocation = '';
					
					unset($getMaxLocationQuantity);
					$getMaxLocationQuantity = '';
					
				}
				else
				{
					$html.=	'<tr>
					<td>'.$getNewLocationIndex.'</td>
					<td>'.$skulocations[$skulocateIndex]['sku'].'</td>
					<td align="left"><b>'.$skulocations[$skulocateIndex]['quantity'] .'</b> X '.substr($skulocations[$skulocateIndex]['title'], 0, 42 ).'</td>
					<td>'.$skulocations[$skulocateIndex]['barcode'].'</td>
					<td>'.$getNewLocationIndex.'</td>
					</tr>';
				}	
				
				
			}
			else
			{
				$html.=	'<tr>
				<td>'.$skulocations[$skulocateIndex]['binrack'].'</td>
				<td>'.$skulocations[$skulocateIndex]['sku'].'</td>
				<td align="left"><b>'.$skulocations[$skulocateIndex]['quantity'] .'</b> X '.substr($skulocations[$skulocateIndex]['title'], 0, 42 ).'</td>
				<td>'.$skulocations[$skulocateIndex]['barcode'].'</td>
				<td>'.$skulocations[$skulocateIndex]['binrack'].'</td>
				</tr>';
			}
				
			
		}
		
		/*foreach( $skuNewlocations as $skulocation)
		{
			$html.=	'<tr>
						<td>'.$skulocation['binrack'].'</td>
						<td>'.$skulocation['sku'].'</td>
						<td align="left"><b>'.$skulocation['quantity'] .'</b> X '.substr($skulocation['title'], 0, 55 ).'</td>
						<td>'.$skulocation['barcode'].'</td>
						</tr>';
			
		}*/
		
		$html .= '</table>';
		$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
		$dompdf->render();
		
		date_default_timezone_set('Europe/Jersey');
		$today = date("d_m_Y_H_i");
		//$dompdf->stream("Pick_List_(".$date.").pdf");
		
		$imgPath = WWW_ROOT .'img/printPickList/'; 
		$path = Router::url('/', true).'img/printPickList/';
		$name	=	'Pick_List_'.$name.'_'.$today.'.pdf';
		
		file_put_contents($imgPath.$name, $dompdf->output());
		echo $serverPath  	= 	$path.$name ;
		exit;
				
	}
	
	public function generatePickList_3_2_2016()
	{
		
		$this->layout = '';
		$this->autoRander = false;
		/* start for pick list */
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderItem' );
		$this->loadModel( 'MergeOrder' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		
		$param = array(
				'conditions' => array(
					'MergeUpdate.pick_list_status' => '0',					
					'MergeUpdate.status' => '0',					
						'OR' => array(
							array( 'MergeUpdate.linn_fetch_orders' => '1' ),
							array( 'MergeUpdate.linn_fetch_orders' => '4' )	
						)
				)
			);
		//$pickListItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.status' => 0)));			
		$pickListItems 	=	$this->MergeUpdate->find('all', $param );			
		//pr($pickListItems);
		foreach( $pickListItems as $pickListItem )
		{
			$stringitem[]	 =  $pickListItem['MergeUpdate']['product_order_id_identify'];
		}
		$stringitem = implode('---', $stringitem);
		$idsArray	=	explode('---', $stringitem);
		
		foreach( $idsArray as $orderid )
		{
		
			$orderItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.product_order_id_identify' => $orderid)));			
			
			foreach($orderItems as $k => $v)
			{
				
				$splitSku = explode(",", $v['MergeUpdate']['sku']);
				$jk = 0;while( $jk <= count( $splitSku )-1 )
				{
					
					$splitNow = explode( "XS-", $splitSku[$jk] );
					//pr($splitNow);
					
					$id = 'S-'.$splitNow[1];
					$result[$id]['quantity'][] = $splitNow[0];
					
					$skuText = $id;				
					$skuLocation = $this->getSkuLocation( $skuText );
					//$result[$id]['bin'][0] = $skuLocation['Product']['bin_rack'];
					$result[$id]['bin'][0] = '';
					
					//Update bin status
					$this->MergeUpdate->updateAll(array('MergeUpdate.pick_list_status' => '1'), array('MergeUpdate.id' => $v['MergeUpdate']['id']));
					
				$jk++;	
				}				
			}
		}
		//pr($result);
		//exit;
		$new = array();
		foreach($result as $key => $value)
		{
			$new[] = array('sku' => $key, 'quanity' => array_sum($value['quantity']), 'binrack' => $value['bin'][0]);
		}
		
		$new = Set::sort($new, '{n}.binrack', 'ASC');
		
		
		
		/* dome pdf vendor */
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper('A4', 'portrait');
		/* Html for print pick list */
		$date = date("m/d/Y");
		$j=0;
		foreach($new as $sku)
			{
				$j = $j+$sku['quanity'];
			}
			
		$html = '<h3>PickList :- '.$date.'</h3><hr><h3>Total Item  : - '.$j.'</h3>
				<table border="1" width="100%" style="font-size:12px; margin-top:10px;">
				<tr>
				<th width="10%" align="center">Bin/Rack</th>
				<th width="15%" align="center">SKU</th>
				<th width="35%" align="center">Qty / Item Title</th>
				<th width="10%" align="center">Barcode</th>
				</tr>';
		$i = 0; 
		
		
		
		$this->loadModel( 'BinLocation' );
		foreach($new as $sku)
		{
			$skuWith	=	explode(',', $sku['sku']);
			
			$product	=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku['sku'])));
			if(count($product) > 0)
			{
			$getBinLocations 	=	$this->BinLocation->find('all', array( 'conditions' => array( 'BinLocation.barcode' => $product['ProductDesc']['barcode']) ));
			
			if( $getBinLocations )
			{
				foreach( $getBinLocations as $getBinLocation)
				{
					$bin[]	 =  $getBinLocation['BinLocation']['bin_location'];
				}
				$binlocation = implode(',', $bin);
				$skulocations[$i]['binrack'] = $binlocation;
				unset( $bin );
			}
			else
			{
				$skulocations[$i]['binrack'] = "No bin location.";
			}
			
			if(count($product) > 0)
			{
				$title			=	$product['Product']['product_name'];
				$skulocations[$i]['barcode']	=	$product['ProductDesc']['barcode'];
			}
			else
			{
				$title							=	"Please check inventory";
				$skulocations[$i]['binrack']	=	"Please check inventory";
			}
			$skulocations[$i]['sku'] = $sku['sku'];
			$skulocations[$i]['quantity'] = $sku['quanity'];
			$skulocations[$i]['title'] = $title;
				/*$html.=	'<tr>
						<td>'.$sku['binrack'].'</td>
						<td>'.$sku['sku'].'</td>
						<td align="left"><b>'.$sku['quanity'] .'</b> X '.substr($title, 0, 55 ).'</td>
						<td>'.$barcode.'</td>
						</tr>';*/
				$i++;
			}
		}
		//pr($skulocations);
		//exit;
		
		//pr($skulocations);
		$skulocate[] = array();
		$jk = 0;while( $jk <= count($skulocations)-1 )
		{
			$binLocate = $skulocations[$jk]['binrack'];
			$binExplode = explode( ',', $binLocate );
			$binText = $binExplode[0];			
			$skulocate[$jk] = $binText;			
		$jk++;	
		}
		
		natsort( $skulocate );		 
		//$skulocate = Set::sort($skulocate, '{n}.binrack', 'ASC');
		
		foreach( $skulocate as $skulocateIndex => $skulocateValue )
		{
			
			$html.=	'<tr>
						<td>'.$skulocations[$skulocateIndex]['binrack'].'</td>
						<td>'.$skulocations[$skulocateIndex]['sku'].'</td>
						<td align="left"><b>'.$skulocations[$skulocateIndex]['quantity'] .'</b> X '.substr($skulocations[$skulocateIndex]['title'], 0, 55 ).'</td>
						<td>'.$skulocations[$skulocateIndex]['barcode'].'</td>
						</tr>';
		}
		
		/*foreach( $skuNewlocations as $skulocation)
		{
			$html.=	'<tr>
						<td>'.$skulocation['binrack'].'</td>
						<td>'.$skulocation['sku'].'</td>
						<td align="left"><b>'.$skulocation['quantity'] .'</b> X '.substr($skulocation['title'], 0, 55 ).'</td>
						<td>'.$skulocation['barcode'].'</td>
						</tr>';
			
		}*/
		
		
		$html .= '</table>';
		$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
		$dompdf->render();
		
		date_default_timezone_set('Europe/Jersey');
		$today = date("d_m_Y_H_i");
		//$dompdf->stream("Pick_List_(".$date.").pdf");
		
		$imgPath = WWW_ROOT .'img/printPickList/'; 
		$path = Router::url('/', true).'img/printPickList/';
		$name	=	'Pick_List_'.$name.'_'.$today.'.pdf';
		
		file_put_contents($imgPath.$name, $dompdf->output());
		echo $serverPath  	= 	$path.$name ;
		exit;
				
	}
	
	/*
	 * 
	 * Params, Get Bin location
	 * 
	 */ 
	private function getSkuLocation( $sku = null )
	{
		$this->loadModel( 'Product' );	
		$params = array(
			'conditions' => array(
				'Product.product_sku' => $sku
			),
			'fields' => array(
				'Product.bin_rack'
			)
		);
		$skuLocation = $this->Product->find( 'first' , $params );
	return $skuLocation;	
	}
	
	
	public function generatePickList_old040115()
	{
				/* start for pick list */
				$this->loadModel( 'OpenOrder' );
				$this->loadModel( 'OrderItem' );
				$this->loadModel( 'Product' );
				$orderItems 	=	$this->OrderItem->find('all', array('conditions' => array('OrderItem.update_quantity' => '0')));
								
				foreach($orderItems as $k => $v) {
					$id = $v['OrderItem']['sku'];
					$result[$id][] = $v['OrderItem']['quantity'];
				}

				$new = array();
				foreach($result as $key => $value) {
					$new[] = array('sku' => $key, 'quanity' => array_sum($value));
				}
				/* dome pdf vendor */
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				/* Html for print pick list */
				$date = date("m/d/Y");
				$j=0;
				foreach($new as $sku)
					{
						$j = $j+$sku['quanity'];
					}
					
				$html = '<h2>PickList :- '.$date.'</h2><hr><h2>Total SKU  : - '.$j.'</h2>
						<table border="1" width="100%" >
						<tr>
						<th width="5%" align="center">S.No</th>
						<th width="25%" align="center">SKU</th>
						<th width="60%" align="center">Qty / Item Title</th>
						<th width="25%" align="center">Barcode</th>
						</tr>';
				$i = 1; 
				foreach($new as $sku)
					{
						$product	=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku['sku'])));
						if(count($product) > 0)
						{
							$title		=	$product['Product']['product_name'];
							$barcode	=	$product['ProductDesc']['barcode'];
						}
						else
						{
							$title		=	"Please check inventory";
							$barcode	=	"Please check inventory";
						}
							$html.=	'<tr>
									<td align="center">'.$i.'</td>
									<td>'.$sku['sku'].'</td>
									<td align="left"><b>'.$sku['quanity'] .'</b> X '.substr($title, 0, 40 ).'</td>
									<td>'.$barcode.'</td>
									</tr>';
							$i++;
					}
				$html .= '</table>';
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				$dompdf->stream("PickList(".$date.").pdf");
				
		}
	
		//For unpaid / pending / help ?
		public function getUnpaidOpenOrder() 
		{
			/* for view of only order */
			$this->layout = 'index';
			$this->loadModel('Order');
			$this->loadModel('Item');
		
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeOrder' );
			$this->loadModel( 'MergeUpdate' );
			
			App::uses('Folder', 'Utility');

			App::uses('File', 'Utility');
			$imgPath = WWW_ROOT .'img/printPickList/'; 
			$dir = new Folder($imgPath, true, 0755);
			$files = $dir->find('.*\.pdf');
			$files = Set::sort($files, '{n}', 'DESC');
			$this->set('files', $files);
			
			$param = array(
				'conditions' => array(
					'OpenOrder.status' => '0',					
						'OR' => array(
							array( 'OpenOrder.linn_fetch_orders' => '0' ),
							array( 'OpenOrder.linn_fetch_orders' => '2' ),
							array( 'OpenOrder.linn_fetch_orders' => '3' )		
						)
				)
			);			
			$getdetails	=	$this->OpenOrder->find('all', $param);
			
			$hiddenArray = array();
			$i = 0;
			foreach($getdetails as $getdetail)
			{
				$id 			= $getdetail['OpenOrder']['num_order_id'];								
				$itemDetails	=	$this->getBundleOrderList( $id );						
				if( count($itemDetails) > 0 )									
				{
					$results[$i]['NumOrderId']		=	 $getdetail['OpenOrder']['num_order_id'];
					$results[$i]['OrderId']			=	 $getdetail['OpenOrder']['order_id'];
					$results[$i]['Items'] =	$itemDetails;				
					$results[$i]['GeneralInfo']		=	 unserialize($getdetail['OpenOrder']['general_info']);
					$results[$i]['ShippingInfo']	=	 unserialize($getdetail['OpenOrder']['shipping_info']);
					$results[$i]['CustomerInfo']	=	 unserialize($getdetail['OpenOrder']['customer_info']);
					$results[$i]['TotalsInfo']		=	 unserialize($getdetail['OpenOrder']['totals_info']);
					$results[$i]['FolderName']		=	 unserialize($getdetail['OpenOrder']['folder_name']);
					//$results[$i]['Items']			=	 unserialize($getdetail['OpenOrder']['items']);
				}
				else
				{
					$hiddenArray[] = $id;
				}				
			$i++;
			}
			if( isset($results)  )
			{
				$results	=	json_decode(json_encode($results),0);
				$this->set('results', $results);
			}
			
			/* code send for save order short detail in data base*/			
			$express1	=	array();
			$express2	=	array();
			$standerd1	=	array();
			$standerd2	=	array();
			$tracked1	=	array();
			$tracked2	=	array();
				
			if( isset($results) )
			 {
				 $i = 0;
				 $k = 0;
				$customPostal = '';
				foreach($results as $data)
				{
					/*if( $data->ShippingInfo->PostalServiceName == "Standard_Jpost" )
					{
						$customPostal = "Standard";
					}
					else
					{
						$customPostal = $data->ShippingInfo->PostalServiceName;
					}*/
					
					if( isset($data->ShippingInfo->PostalServiceName) &&  $data->ShippingInfo->PostalServiceName == 'Express' && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group A')
					{
						$express1[]	=	$data;
					}
					else if( isset($data->ShippingInfo->PostalServiceName) &&  $data->ShippingInfo->PostalServiceName == 'Express' && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group B' )
					{
						$express2[]	=	$data ;						
					}
					if( isset($data->ShippingInfo->PostalServiceName) && ( $data->ShippingInfo->PostalServiceName == 'Standard_Jpost' || $data->ShippingInfo->PostalServiceName == 'Standard' || $data->ShippingInfo->PostalServiceName == 'Default') && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group A')
					{
						$standerd1[]	=	 $data;						
					}
					else if( isset($data->ShippingInfo->PostalServiceName) && ( $data->ShippingInfo->PostalServiceName == 'Standard_Jpost' || $data->ShippingInfo->PostalServiceName == 'Standard' || $data->ShippingInfo->PostalServiceName == 'Default' ) && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group B' )
					{
						$standerd2[]	=	 $data;
					} 
					if( isset($data->ShippingInfo->PostalServiceName) && $data->ShippingInfo->PostalServiceName == 'Tracked' && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group A')
					{
						$tracked1[]	=	 $data;
					}
					else if( isset($data->ShippingInfo->PostalServiceName) && $data->ShippingInfo->PostalServiceName == 'Tracked' && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group B')
					{
						$tracked2[]	=	 $data;
					}
					$i++;
				}
			}
			
			/* for custom postal service */
			$this->loadModel( 'MergeUpdate' );
			$popupOrders = json_decode(json_encode($this->MergeUpdate->find('all')),0);      
			$this->set('popupOrders', $popupOrders );
			
			$this->set(compact('express1','express2','standerd1','standerd2','tracked1','tracked2'));
		}
		
		public function getClickForOrder()
		{
			/* for view of only order */
			$this->layout = 'openorder';
			
			$this->loadModel('Order');
			$this->loadModel('Item');
		
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeOrder' );
			$this->loadModel( 'MergeUpdate' );
			
			App::uses('Folder', 'Utility');
			App::uses('File', 'Utility');
			$imgPath = WWW_ROOT .'img/printPickList/'; 
			$dir = new Folder($imgPath, true, 0755);
			$files = $dir->find('.*\.pdf');
			$files = Set::sort($files, '{n}', 'DESC');
			$this->set('files', $files);
			
			$perPage	=	 Configure::read( 'perpage' );
			
			$this->paginate = array(
                        'conditions' => array(
							'OpenOrder.status' => '0',					
								'OR' => array(
									array( 'OpenOrder.linn_fetch_orders' => '1' ),
									array( 'OpenOrder.linn_fetch_orders' => '4' )	
								)
						),
			'limit' => $perPage			
			);
			 
			$getdetails = $this->paginate('OpenOrder');
			
			
			
			/*$param = array(
				'conditions' => array(
					'OpenOrder.status' => '0',					
						'OR' => array(
							array( 'OpenOrder.linn_fetch_orders' => '1' ),
							array( 'OpenOrder.linn_fetch_orders' => '4' )	
						)
				)
			);		
				
			$getdetails	=	$this->OpenOrder->find('all', $param);*/
			
			$hiddenArray = array();
			$i = 0;
			foreach($getdetails as $getdetail)
			{
				$id 			= $getdetail['OpenOrder']['num_order_id'];								
				$itemDetails	=	$this->getBundleOrderList( $id );						
				if( count($itemDetails) > 0 )									
				{
					$results[$i]['NumOrderId']		=	 $getdetail['OpenOrder']['num_order_id'];
					$results[$i]['OrderId']			=	 $getdetail['OpenOrder']['order_id'];
					$results[$i]['Items'] =	$itemDetails;				
					$results[$i]['GeneralInfo']		=	 unserialize($getdetail['OpenOrder']['general_info']);
					$results[$i]['ShippingInfo']	=	 unserialize($getdetail['OpenOrder']['shipping_info']);
					$results[$i]['CustomerInfo']	=	 unserialize($getdetail['OpenOrder']['customer_info']);
					$results[$i]['TotalsInfo']		=	 unserialize($getdetail['OpenOrder']['totals_info']);
					$results[$i]['FolderName']		=	 unserialize($getdetail['OpenOrder']['folder_name']);
					//$results[$i]['Items']			=	 unserialize($getdetail['OpenOrder']['items']);
				}
				else
				{
					$hiddenArray[] = $id;
				}				
			$i++;
			}
			
			if( isset($results)  )
			{
				$results	=	json_decode(json_encode($results),0);
				$this->set('results', $results);
			}
			
			/* code send for save order short detail in data base*/			
			$express1	=	array();
			$express2	=	array();
			$standerd1	=	array();
			$standerd2	=	array();
			$tracked1	=	array();
			$tracked2	=	array();
				
			if( isset($results) )
			 {
				 $i = 0;
				 $k = 0;
				$customPostal = '';
				foreach($results as $data)
				{
					/*if( $data->ShippingInfo->PostalServiceName == "Standard_Jpost" )
					{
						$customPostal = "Standard";
					}
					else
					{
						$customPostal = $data->ShippingInfo->PostalServiceName;
					}*/
					
					if( isset($data->ShippingInfo->PostalServiceName) &&  $data->ShippingInfo->PostalServiceName == 'Express' && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group A')
					{
						$express1[]	=	$data;
					}
					else if( isset($data->ShippingInfo->PostalServiceName) &&  $data->ShippingInfo->PostalServiceName == 'Express' && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group B' )
					{
						$express2[]	=	$data ;						
					}
					if( isset($data->ShippingInfo->PostalServiceName) && ( $data->ShippingInfo->PostalServiceName == 'Standard_Jpost' || $data->ShippingInfo->PostalServiceName == 'Standard' || $data->ShippingInfo->PostalServiceName == 'Default') && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group A')
					{
						$standerd1[]	=	 $data;						
					}
					else if( isset($data->ShippingInfo->PostalServiceName) && ( $data->ShippingInfo->PostalServiceName == 'Standard_Jpost' || $data->ShippingInfo->PostalServiceName == 'Standard' || $data->ShippingInfo->PostalServiceName == 'Default' ) && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group B' )
					{
						$standerd2[]	=	 $data;
					} 
					if( isset($data->ShippingInfo->PostalServiceName) && $data->ShippingInfo->PostalServiceName == 'Tracked' && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group A')
					{
						$tracked1[]	=	 $data;
					}
					else if( isset($data->ShippingInfo->PostalServiceName) && $data->ShippingInfo->PostalServiceName == 'Tracked' && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group B')
					{
						$tracked2[]	=	 $data;
					}
					$i++;
				}
			}
			
			/* for custom postal service */
			$this->loadModel( 'MergeUpdate' );
			$popupOrders = json_decode(json_encode($this->MergeUpdate->find('all')),0);      
			$this->set('popupOrders', $popupOrders );
			
			$this->set(compact('express1','express2','standerd1','standerd2','tracked1','tracked2'));
			
			$this->render( 'get_order_by_angular' );
			
		}
		
		public function GetExpressOrder() 
		{
			/* for view of only order */
			$this->layout = 'index';
			$this->loadModel('Order');
			$this->loadModel('Item');
		
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeOrder' );
			$this->loadModel( 'MergeUpdate' );
			
			App::uses('Folder', 'Utility');
			App::uses('File', 'Utility');
			$imgPath = WWW_ROOT .'img/printPickList/'; 
			$dir = new Folder($imgPath, true, 0755);
			$files = $dir->find('.*\.pdf');
			$files = Set::sort($files, '{n}', 'DESC');
			$this->set('files', $files);
			//'OpenOrder.general_info like ' => "%{$searchKeyNote}%"
			$usertype = $this->Session->read('Auth.User.service_pkst');
			$conditions = array(
							'conditions' => array(
								'OpenOrder.status' => '0',	
								'OpenOrder.shipping_info like' => '%Express%',	
									'OR' => array(
										array( 'OpenOrder.linn_fetch_orders' => '1' ),
										array( 'OpenOrder.linn_fetch_orders' => '4' )	
									)
							),
				'order' => array('OpenOrder.open_order_date' => 'ASC')			
				);
			
			 
			$getdetails = $this->OpenOrder->find('all', $conditions);
			//pr($getdetails);
			
			$hiddenArray = array();
			$i = 0;
			foreach($getdetails as $getdetail)
			{
				$id 			= $getdetail['OpenOrder']['num_order_id'];								
				$itemDetails	=	$this->getBundleOrderList( $id );						
				if( count($itemDetails) > 0 )									
				{
					$results[$i]['NumOrderId']		=	 $getdetail['OpenOrder']['num_order_id'];
					$results[$i]['OrderId']			=	 $getdetail['OpenOrder']['order_id'];
					$results[$i]['sub_source']		=	 $getdetail['OpenOrder']['sub_source'];
					$results[$i]['Items']			=	 $itemDetails;				
					$results[$i]['GeneralInfo']		=	 unserialize($getdetail['OpenOrder']['general_info']);
					$results[$i]['ShippingInfo']	=	 unserialize($getdetail['OpenOrder']['shipping_info']);
					$results[$i]['CustomerInfo']	=	 unserialize($getdetail['OpenOrder']['customer_info']);
					$results[$i]['TotalsInfo']		=	 unserialize($getdetail['OpenOrder']['totals_info']);
					$results[$i]['FolderName']		=	 unserialize($getdetail['OpenOrder']['folder_name']);
				}
				else
				{
					$hiddenArray[] = $id;
				}				
			$i++;
			}
			
			if( isset($results)  )
			{
				$results	=	json_decode(json_encode($results),0);
				$this->set('results', $results);
			}
			
			
			$express1	=	array();
			$express2	=	array();
			if( isset($results) )
			 {
				 $i = 0;
				 $k = 0;
				$customPostal = '';
				foreach($results as $data)
				{
						if( isset($data->ShippingInfo->PostalServiceName) &&  $data->ShippingInfo->PostalServiceName == 'Express' && count($data->Items) > 0 )
						{
							$express1[]	=	$data;
						}
					$i++;
				}
			}
			
			$this->loadModel( 'OrderLocation' );
			$oversell_items = '';
							
			$oversellItems = $this->OrderLocation->find('all', array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.available_qty_bin' => '0'),'order' => 'bin_location ASC') );	
			if(count($oversellItems) > 0)
			{
				foreach($oversellItems as $v){
					$oversell_items[$v['OrderLocation']['order_id']] = $v['OrderLocation']['order_id'];
				}
			}
			$this->set(compact('express1','express2','oversell_items'));
		}
		
		public function checkOverSell($split_order_id = NULL)
		{
			$this->loadModel( 'OrderLocation' );
							
			$oversellItems = $this->OrderLocation->find('count', array('conditions' => array( 'OrderLocation.status' => 'active', 'OrderLocation.available_qty_bin' => '0', 'split_order_id'=>$split_order_id),'order' => 'bin_location ASC') );	
			
			return $oversellItems;
		
		}
		
		// For open orders only
		public function getOpenOrder()
		{
			/* for view of only order */
			$this->layout = 'index';
			$this->loadModel('Order');
			$this->loadModel('Item');
		
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeOrder' );
			$this->loadModel( 'MergeUpdate' );
			
			App::uses('Folder', 'Utility');
			App::uses('File', 'Utility');
			$imgPath = WWW_ROOT .'img/printPickList/'; 
			$dir = new Folder($imgPath, true, 0755);
			$files = $dir->find('.*\.pdf');
			$files = Set::sort($files, '{n}', 'DESC');
			$this->set('files', $files);
			
			$usertype = $this->Session->read('Auth.User.service_pkst');
			//$perPage	=	 Configure::read( 'perpage' );
			
			
				$this->paginate = array(
							'conditions' => array(
								'OpenOrder.status' => '0',	
									'OR' => array(
										array( 'OpenOrder.linn_fetch_orders' => '1' ),
										array( 'OpenOrder.linn_fetch_orders' => '4' )	
									)
							),
				'limit' => '25',
				'order' => array('OpenOrder.open_order_date' => 'ASC')			
				);
			
			 
			$getdetails = $this->paginate('OpenOrder');
			
			
			
			/*$param = array(
				'conditions' => array(
					'OpenOrder.status' => '0',					
						'OR' => array(
							array( 'OpenOrder.linn_fetch_orders' => '1' ),
							array( 'OpenOrder.linn_fetch_orders' => '4' )	
						)
				)
			);		
				
			$getdetails	=	$this->OpenOrder->find('all', $param);*/
			
			$hiddenArray = array();
			$i = 0;
			foreach($getdetails as $getdetail)
			{
				$id 			= $getdetail['OpenOrder']['num_order_id'];								
				$itemDetails	=	$this->getBundleOrderList( $id );						
				if( count($itemDetails) > 0 )									
				{
					$results[$i]['NumOrderId']		=	 $getdetail['OpenOrder']['num_order_id'];
					$results[$i]['OrderId']			=	 $getdetail['OpenOrder']['order_id'];
					$results[$i]['sub_source']		=	 $getdetail['OpenOrder']['sub_source'];
					$results[$i]['Items']			=	 $itemDetails;				
					$results[$i]['GeneralInfo']		=	 unserialize($getdetail['OpenOrder']['general_info']);
					$results[$i]['ShippingInfo']	=	 unserialize($getdetail['OpenOrder']['shipping_info']);
					$results[$i]['CustomerInfo']	=	 unserialize($getdetail['OpenOrder']['customer_info']);
					$results[$i]['TotalsInfo']		=	 unserialize($getdetail['OpenOrder']['totals_info']);
					$results[$i]['FolderName']		=	 unserialize($getdetail['OpenOrder']['folder_name']);
					//$results[$i]['Items']			=	 unserialize($getdetail['OpenOrder']['items']);
				}
				else
				{
					$hiddenArray[] = $id;
				}				
			$i++;
			}
			
			if( isset($results)  )
			{
				$results	=	json_decode(json_encode($results),0);
				$this->set('results', $results);
			}
			
			/* code send for save order short detail in data base*/			
			$express1	=	array();
			$express2	=	array();
			$standerd1	=	array();
			$standerd2	=	array();
			$tracked1	=	array();
			$tracked2	=	array();
				
			if( isset($results) )
			 {
				 $i = 0;
				 $k = 0;
				$customPostal = '';
				foreach($results as $data)
				{
					/*if( $data->ShippingInfo->PostalServiceName == "Standard_Jpost" )
					{
						$customPostal = "Standard";
					}
					else
					{
						$customPostal = $data->ShippingInfo->PostalServiceName;
					}*/
					
					
						if( isset($data->ShippingInfo->PostalServiceName) &&  $data->ShippingInfo->PostalServiceName == 'Express' && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group A')
						{
							$express1[]	=	$data;
						}
						else if( isset($data->ShippingInfo->PostalServiceName) &&  $data->ShippingInfo->PostalServiceName == 'Express' && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group B' )
						{
							$express2[]	=	$data ;						
						}
						if( isset($data->ShippingInfo->PostalServiceName) && ( $data->ShippingInfo->PostalServiceName == 'Standard_Jpost' || $data->ShippingInfo->PostalServiceName == 'Standard' || $data->ShippingInfo->PostalServiceName == 'Default') && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group A')
						{
							$standerd1[]	=	 $data;						
						}
						else if( isset($data->ShippingInfo->PostalServiceName) && ( $data->ShippingInfo->PostalServiceName == 'Standard_Jpost' || $data->ShippingInfo->PostalServiceName == 'Standard' || $data->ShippingInfo->PostalServiceName == 'Default' ) && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group B' )
						{
							$standerd2[]	=	 $data;
						} 
						if( isset($data->ShippingInfo->PostalServiceName) && $data->ShippingInfo->PostalServiceName == 'Tracked' && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group A')
						{
							$tracked1[]	=	 $data;
						}
						else if( isset($data->ShippingInfo->PostalServiceName) && $data->ShippingInfo->PostalServiceName == 'Tracked' && count($data->Items) > 0 && $data->Items[$k]->MergeUpdate->order_split == 'Group B')
						{
							$tracked2[]	=	 $data;
						}
					
					$i++;
				}
			}
			
			/* for custom postal service */
			//$this->loadModel( 'MergeUpdate' );
			//$popupOrders = json_decode(json_encode($this->MergeUpdate->find('all')),0);      
			//$this->set('popupOrders', $popupOrders );
			
			$this->loadModel( 'OrderLocation' );
			$oversell_items = '';
							
			$oversellItems = $this->OrderLocation->find('all', array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.available_qty_bin' => '0'),'order' => 'bin_location ASC') );	
			if(count($oversellItems) > 0)
			{
				foreach($oversellItems as $v){
					$oversell_items[$v['OrderLocation']['order_id']] = $v['OrderLocation']['order_id'];
				}
			}
			$this->set(compact('express1','express2','standerd1','standerd2','tracked1','tracked2','oversell_items'));
			
			//$this->set(compact('express1','express2','standerd1','standerd2','tracked1','tracked2'));
			
			//$this->render( 'get_order_by_angular' );
			
		}
		
		
		public function cancelOrder_old200416()
		   {
			 
			App::import('Vendor', 'linnwork/api/Auth');
			App::import('Vendor', 'linnwork/api/Factory');
			App::import('Vendor', 'linnwork/api/Orders');
			
			$username	=	Configure::read('linnwork_api_username');
			$password	=	Configure::read('linnwork_api_password');
			
			$multi = AuthMethods::Multilogin($username, $password);
			
			$auth = AuthMethods::Authorize($username, $password, $multi[0]->Id);	

			$token 			= 	$auth->Token;	
			$server 		= 	$auth->Server;
			
			$id				=	$this->request->data['openorderid'];
			$refaundNote	=	$this->request->data['refundNote'];
			$refaundAmount	=	$this->request->data['refundamount'];
			$warehoiuseId	=	$this->request->data['warehohuseId'];
			$this->loadModel('OpenOrder');
			$this->loadModel('OrderItem');
			$this->loadModel('Product');
			$this->loadModel('CancelOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('ScanOrder');
			$getOpernOrder	=	$this->getOpenOrderById( $id );
			
			$order			=	$this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $id)));
			$orderid		=	$order['OpenOrder']['num_order_id'];
			$pkid			=	$order['OpenOrder']['order_id'];
			$orderItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.order_id' => $id)));
			
			foreach( $orderItems as $getItem )
			   {
				   $splitID = $getItem['MergeUpdate']['product_order_id_identify'];
				   $skus	=	 explode( ',', $getItem['MergeUpdate']['sku']);
				     foreach($skus as $sku)
					   {
						   $newSku			=	 explode( 'XS-', $sku);
						  
						   $Quantity 			=	$newSku[0];
						   $sku 				=	'S-'.$newSku[1];
						   $product				=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
						   $currentStickLevel	=	$product['Product']['current_stock_level'];
						   $qtyForUpdate		=	(int)$Quantity + (int)$currentStickLevel;
						   $result				=	$this->Product->updateAll(array('Product.current_stock_level' => $qtyForUpdate), array('Product.product_sku' => $sku));
						   $updateresult		=	$this->OpenOrder->updateAll(array('OpenOrder.status' => '2'), array('OpenOrder.num_order_id' => $id));
							if($updateresult)
							{
								$conditionsformergeupdate = array (
											'MergeUpdate.order_id' => $id
										);
								$conditionsforscanorder = array (
											'ScanOrder.split_order_id' => $splitID
										);
								$resultCancelOrder		=	$this->CancelOrder->saveAll($order['OpenOrder']);
								$resultMergeUpdate  	= 	$this->MergeUpdate->deleteAll($conditionsformergeupdate);
								$resultScanOrder 		= 	$this->ScanOrder->deleteAll($conditionsforscanorder);
							}
					   }
					  
					   
			   }
   
				$orderId	=	$pkid;
				$fulFilment	=	$warehoiuseId;
				$refund		=	$refaundAmount;
				$note 		= 	$refaundNote;
				
				$result = OrdersMethods::CancelOrder($orderId,$fulFilment,$refund,$note,$token, $server);
				
				/*if($result)
				{
					echo "1";
					exit;
				}
				else
				{
					echo "2";
					exit;
				}*/
				if( $resultCancelOrder	&& $resultMergeUpdate && $resultScanOrder)
				{
					
					App::import('Controller', 'Products');
					$productModel = new ProductsController();
					$productModel->productVirtualStockCsvGeneration();
					echo "1";
					exit;
				}
				else
				{
					
					App::import('Controller', 'Products');
					$productModel = new ProductsController();
					$productModel->productVirtualStockCsvGeneration();
					echo "2";
					exit;
				}
			    
		   }
		   
	   public function cancelOrder()
	   {
		 
		/*App::import('Vendor', 'linnwork/api/Auth');
		App::import('Vendor', 'linnwork/api/Factory');
		App::import('Vendor', 'linnwork/api/Orders');
		
		$username	=	Configure::read('linnwork_api_username');
		$password	=	Configure::read('linnwork_api_password');
		
		$multi = AuthMethods::Multilogin($username, $password);
		
		$auth = AuthMethods::Authorize($username, $password, $multi[0]->Id);	

		$token 			= 	$auth->Token;	
		$server 		= 	$auth->Server;*/
		
		App::import('Vendor', 'Linnworks/src/php/Classes/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
	
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
		$multi = AuthMethods::Multilogin($username, $password);
		
		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

		$token = $auth->Token;	
		$server = $auth->Server;
		
		//$id				=	$this->request->data['openorderid'];
		//$refaundNote	=	$this->request->data['refundNote'];
		//$refaundAmount	=	$this->request->data['refundamount'];
		//$warehoiuseId	=	$this->request->data['warehohuseId'];
		
		$this->loadModel('OpenOrder');
		$this->loadModel('OrderItem');
		$this->loadModel('Product');
		$this->loadModel('CancelOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('ScanOrder');
		$this->loadModel('BinLocation');
		
		//$getOpernOrder	=	$this->getOpenOrderById( $id );
		
		$order			=	$this->OpenOrder->find('first', array('conditions' => array('OpenOrder.status' => 2 , 'OpenOrder.cancel_status' => 5) , 'order' => array( 'OpenOrder.id DESC' )));
		
		//pr($order); exit;
		
		//update
		$this->OpenOrder->updateAll(array( 'OpenOrder.cancel_status' => 4 ) , array('OpenOrder.id' => $order['OpenOrder']['id'] ));
		
		$orderid		=	$order['OpenOrder']['num_order_id'];
		
		$id = $orderid;
		
		$pkid			=	$order['OpenOrder']['order_id'];
		$orderItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.order_id' => $id)));
		
		/*$mailFormate = '<html>
		
							<head>
							<style>
							table {
								border-collapse: collapse;
								width: 100%;
							}

							th, td {
								text-align: left;
								padding: 8px;
							}
							
							tr:hover {background-color: #f5f5f5}
							
							tr:nth-child(even){background-color: #f2f2f2}

							th {
								background-color: #4CAF50;
								color: white;
							}
							</style>
							</head>											If, you have already picked those below items, Kindly put back onto shelf!
										<br><br>
										<table border="1" style="border: 1px solid #c2c2c2;">
												<tr>
													<th>OrderID</th>
													<th>Sub-Order</th>
													<th>SKU</th>
													<th>Title</th>
													<th>Barcode</th>
													<th>Location</th>
													<th>Quantity</th>
												</tr>';*/
		$outerCheckForPickLIst = 0;
		
		foreach( $orderItems as $getItem )
		   {
			   
			   
			   $outerCheckForPickLIst = $getItem['MergeUpdate']['pick_list_status'];
			   
			   $splitID = $getItem['MergeUpdate']['product_order_id_identify'];
			   $skus	=	 explode( ',', $getItem['MergeUpdate']['sku']);
				 foreach($skus as $sku)
				   {
					   $newSku			=	 explode( 'XS-', $sku);
					  
					   $Quantity 			=	$newSku[0];
					   $sku 				=	'S-'.$newSku[1];
					   $product				=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
					   $currentStickLevel	=	$product['Product']['current_stock_level'];
					   $qtyForUpdate		=	(int)$Quantity + (int)$currentStickLevel;
					   
					   /*if( $currentStickLevel <= 0 )
					   {
						   $newValue = 0;
					   }
					   else
					   {
						   $newValue = $currentStickLevel;
					   }*/
					   
					   $ordertype = 'Cancel';
					   $data['Product']['product_sku']		=	$product['Product']['product_sku'];
					   $data['ProductDesc']['barcode']		=	$product['ProductDesc']['barcode'];
					   
					   $barcodeVal = $product['ProductDesc']['barcode'];
					   $data['Product']['CurrentStock']		=	$qtyForUpdate;
					   
						$productData	=	json_decode(json_encode($data,0));
						App::import('Controller', 'Cronjobs');
						$productCant = new CronjobsController();
						//$productCant->storeInventoryRecord( $productData, $Quantity, $id, $ordertype, $barcodeVal );
					   
					   //$result				=	$this->Product->updateAll(array('Product.current_stock_level' => $qtyForUpdate), array('Product.product_sku' => $sku));
					   $updateresult		=	$this->OpenOrder->updateAll(array('OpenOrder.status' => '2'), array('OpenOrder.num_order_id' => $id));
						if($updateresult)
						{
							$conditionsformergeupdate = array (
										'MergeUpdate.order_id' => $id
									);
							$conditionsforscanorder = array (
										'ScanOrder.split_order_id' => $splitID
									);
							$resultCancelOrder		=	$this->CancelOrder->saveAll($order['OpenOrder']);
							$resultMergeUpdate  	= 	$this->MergeUpdate->deleteAll($conditionsformergeupdate);
							$resultScanOrder 		= 	$this->ScanOrder->deleteAll($conditionsforscanorder);
							$this->deleteRegisteredNumber( $splitID );
						}
				   }
				   
				  /*if( $getItem['MergeUpdate']['pick_list_status'] == 1 )
				  {
					  $barcode = $product['ProductDesc']['barcode'];
					  $product['ProductDesc']['barcode'];
					  //find the all bin location of barcode
					  $getBinLocations 	=	$this->BinLocation->find( 'list', array( 'conditions' => array( 'BinLocation.barcode' => $barcode ), 'fields' => array( 'BinLocation.stock_by_location' ) ) );
					  $i = 0;
					  // min value for all location 
					  $binkey=array_keys($getBinLocations, min($getBinLocations));
					  $binLocationId	=	$binkey[0];
					 
					  // find the current value of location  
					  $getlocationQty	=	$this->BinLocation->find('first', array( 'conditions' => array( 'BinLocation.id' => $binLocationId )) );
					  $locationQty	=	$Quantity + $getlocationQty['BinLocation']['stock_by_location'];
					 
					  // update the location quantity 
					  $updateresult		=	$this->BinLocation->updateAll(array('BinLocation.stock_by_location' => $locationQty), array('BinLocation.id' => $binLocationId));
					  $mailFormate.=			  '<tr>
													<td>'.$id.'</td>
													<td>'.$splitID.'</td>
													<td>'.$sku.'</td>
													<td>'.$product['Product']['product_name'].'</td>
													<td>'.$product['ProductDesc']['barcode'].'</td>
													<td>'.$getlocationQty['BinLocation']['bin_location'].'</td>
													<td>'.$Quantity.'</td>
												</tr>';
												
						//Putback items for shelf ensuring from warehouse keeper
						$this->loadModel( 'PutbackItem' );							
						$putBackItem = array();
						$authDetails = $this->Session->read('Auth.User'); 
						$putBackItem['user_id']	= $authDetails['id'];											
						$putBackItem['OrderID']	= $id;											
						$putBackItem['Sub-Order']= $splitID;												
						$putBackItem['SKU']		= $sku;										
						$putBackItem['Title']	= $product['Product']['product_name'];								
						$putBackItem['Barcode']	= $product['ProductDesc']['barcode'];					
						$putBackItem['Location']= $getlocationQty['BinLocation']['bin_location'];			
						$putBackItem['Quantity']= $Quantity;	
						$putBackItem['action_type']= 'canceled';	
						$this->PutbackItem->saveAll( $putBackItem );
					
						//Send notification						
						$sendNotifyArray[] = $authDetails['first_name'];
						$sendNotifyArray[] = $splitID;						
						$sendNotifyArray[] = $putBackItem['action_type'];
						
						App::import('Controller','MyExceptions');
						$myExceptionObj = new MyExceptionsController(new View());                        
						$myExceptionObj->send_cancel_delete_order_notification(  $sendNotifyArray  );
						
				  }*/
		   }
		   
		  // $mailFormate.=  '</table></body></html>';			   			   
		   /*if( $outerCheckForPickLIst == 1 )
		   {
			   //send the mail of operater 
			   App::uses('CakeEmail', 'Network/Email');
			   $email = new CakeEmail('');
			   $email->emailFormat('html');
			   $email->from('info@euracogroup.co.uk');
			   $email->to( array('ashish.gupta@jijgroup.com','je-operations@esljersey.com','amit.gaur@jijgroup.com'));						  
			   $getBase = Router::url('/', true);
				$email->subject('Order cancel after generate pick list');
				$email->send( $mailFormate );
				
		   }*/
		   
		   $this->writeLog($pkid .'__'. $orderid); 
		   
		   $sourceSub = $order['OpenOrder']['sub_source'];
		   $this->loadModel( 'SourceList' );
		   $sourceParam = array(
			'conditions' => array(
				'SourceList.LocationName' => $sourceSub
			)
		   );
		    $sourceNameLovationText = $this->SourceList->find( 'first' , $sourceParam );
		    $locatioNtext = $sourceNameLovationText['SourceList']['StockLocationId'];
			$orderId	=	$pkid;
			$fulFilment	=	$locatioNtext;
			$refund		=	'0.00';
			$note 		= 	'refund';
			
			
			$result = OrdersMethods::CancelOrder($orderId,$fulFilment,$refund,$note,$token, $server);
			echo $result; exit;
			/*if($result)
			{
				echo "1";
				exit;
			}
			else
			{
				echo "2";
				exit;
			}
			
			if( $resultCancelOrder	&&  $resultMergeUpdate && $resultScanOrder)
			{
				
				App::import('Controller', 'Products');
				$productModel = new ProductsController();
				$productModel->productVirtualStockCsvGeneration();
				echo "1";
				exit;
			}
			else
			{
				
				App::import('Controller', 'Products');
				$productModel = new ProductsController();
				$productModel->productVirtualStockCsvGeneration();
				echo "2";
				exit;
			}
			
			App::import('Controller', 'Products');
			$productModel = new ProductsController();
			$productModel->productVirtualStockCsvGeneration();*/
			
	   }
		 
		 public function writeLog($numOrderId = null) 
			{
				
				$this->layout = '';
				$this->autoRender = false;
				
				//Cake Log
				App::uses('CakeLog', 'Log');
				CakeLog::config('default', array(
					'engine' => 'File'
				));
				
				//create and setup the logs
				CakeLog::write('Euraco Group_[Cancel_Order_'.date( 'd-m-Y A' ).']', $numOrderId);
				
			}
			
		public function deleteOrder_old200416()
		{
			$this->layout = '';
			$this->autoRender = false;
		
			$id				=	$this->request->data['openorderid'];
			
			$this->loadModel('OpenOrder');
			$this->loadModel('OrderItem');
			$this->loadModel('Product');
			$this->loadModel('CancelOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('ScanOrder');
			
			$getOpernOrder	=	$this->getOpenOrderById( $id );
			
			$order			=	$this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $id)));
			$orderid		=	$order['OpenOrder']['num_order_id'];
			$pkid			=	$order['OpenOrder']['order_id'];
			$orderItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.order_id' => $id)));
			
			foreach( $orderItems as $getItem )
			   {
				   $splitID = $getItem['MergeUpdate']['product_order_id_identify'];
				   $skus	=	 explode( ',', $getItem['MergeUpdate']['sku']);
				 
				     foreach($skus as $sku)
					   {
						   $newSku			=	 explode( 'XS-', $sku);
						  
						   $Quantity 			=	$newSku[0];
						   $sku 				=	'S-'.$newSku[1];
						   $product				=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
						   $currentStickLevel	=	$product['Product']['current_stock_level'];
						   $qtyForUpdate		=	(int)$Quantity + (int)$currentStickLevel;
						   $ordertype = 'Cancel';
						   $data['Product']['product_sku']		=	$product['Product']['product_sku'];
						   $data['ProductDesc']['barcode']		=	$product['ProductDesc']['barcode'];
						   $data['Product']['CurrentStock']		=	$currentStickLevel;
						   
						    $productData	=	json_decode(json_encode($data,0));
							App::import('Controller', 'Cronjobs');
							$productCant = new CronjobsController();
							$productCant->storeInventoryRecord( $productData, $Quantity, $id, $ordertype );
						  
						  	$conditionsformergeupdate = array (
										'MergeUpdate.order_id' => $id
									);
							$conditionsforscanorder = array (
										'ScanOrder.split_order_id' => $splitID
									);
							$conditionsforopenorder = array (
										'OpenOrder.num_order_id' => $id
									);
									
							
							$this->OpenOrder->query("delete from open_orders where num_order_id = '{$id}'");
							//$resultdeleteOrder		=	$this->OpenOrder->delete( $conditionsforopenorder );
							$resultMergeUpdate  	= 	$this->MergeUpdate->deleteAll( $conditionsformergeupdate );
							$resultScanOrder 		= 	$this->ScanOrder->deleteAll( $conditionsforscanorder );
							
					   }
			   }
			   echo "1";
			   exit;
  	  }
	  
	  public function deleteOrderbyID( )
		{
			
			$orderids = array('1216225','1213015','1212840','1212841','1212712','1212441','1212238','1212240','1211809','1210442','1210144','1210145','1210084','1209748','1209749','1209675','1209473','1209404','1209404','1209279','1209023','1209023','1208870','1208870','1208806','1208602','1208347','1208244','1207972','1216211','1216212','1216138','1216116','1216076','1216022','1216023','1215983','1216024','1216025','1216026','1215984','1215916','1215917','1215871','1215870','1215794','1215794','1215744','1215744','1215745','1215695','1215621','1215621','1215576','1215447','1215448','1215377','1215378','1215379','1215340','1215230','1215231','1215101','1215102','1215055','1214985','1214928','1214929','1214881','1214765','1214766','1214767','1214768','1214713','1214713','1214658','1214659','1214660','1214600','1214600','1214601','1214602','1214434','1214433','1214435','1214436','1214378','1214320','1214146','1214146','1214147','1214086','1214088','1214089','1214017','1213953','1213914','1213862','1213863','1213775','1213701','1213702','1213667','1213668','1213669','1213517','1210026','1210027','1210027','1209528','1209402','1209232','1209022','1208977','1208388','1208348','1208349','1207768','1213505','1213459','1213428','1213403','1213404','1213405','1213405','1213074','1213013','1213014','1213016','1212839','1212842','1212711','1212713','1212547','1212548','1212549','1212442','1212442','1212239','1212171','1212172','1212073','1212074','1211990','1211866','1211867','1211868','1211869','1211010','1210659','1210553','1210461','1203917','1203917','1203917','1203917','1210378','1210337','1210083','1209889','1209890','1209805','1209676','1209231','1209231','1209102','1208653','1207929','1207929','1207818');
			$this->loadModel('OpenOrder');
			foreach( $orderids as $orderid){
				$order			=	$this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $orderid,'status' => 0)));
				
				if(count( $order ) > 0){
					echo '==<br>';
					$this->deleteOrder($order['OpenOrder']['num_order_id']);
				} else {
					echo  "Not deleted " .$orderid;
				}
			}
			
			
		}
  	  
  	   public function deleteOrder( $orderId = null )
		{
			$this->layout = '';
			$this->autoRender = false;
			
			if( isset($orderId) && $orderId > 0 )
			{
				$id	= $orderId;
			}
			else
			{
				$id	= $this->request->data['openorderid'];
			}
			
			$this->loadModel('OpenOrder');
			$this->loadModel('OrderItem');
			$this->loadModel('Product');
			$this->loadModel('CancelOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('ScanOrder');
			$this->loadModel('BinLocation');
			$this->loadModel('OrderLocation');
			$this->loadModel('CheckIn');
			$this->loadMOdel('InkOrderLocation' ); 
			
			$getOpernOrder	=	$this->getOpenOrderById( $id );
			
			$order			=	$this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $id)));
			$orderid		=	$order['OpenOrder']['num_order_id'];
			$pkid			=	$order['OpenOrder']['order_id'];
			$orderItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.order_id' => $id)));
			$pick_list_status = 0; 
			
			/*--------------Ink order delete---------------*/	
			$order_ink = $this->InkOrderLocation->find( 'first', array( 'conditions' => array( 'order_id' => $orderid,'status' => 'active' ) ) );
			 
			if(count($order_ink) > 0){ 
			
				/*foreach($order_ink as $val)
				{*/
				
				  	$val 		= $order_ink;
					
 					$barcode 	= $val['InkOrderLocation']['barcode'];
					$sku		= $val['InkOrderLocation']['sku'];
					$ink_sku 	= $val['InkOrderLocation']['ink_sku'];
					$location 	= $val['InkOrderLocation']['bin_location'];
					$quantity 	= $val['InkOrderLocation']['quantity'];
					$qty_bin 	= $val['InkOrderLocation']['available_qty_bin'];						
					$po_id	 	= explode(",",$val['InkOrderLocation']['po_id'])[0];
 					  
							 
					$this->BinLocation->updateAll(	array('BinLocation.stock_by_location' => "BinLocation.stock_by_location + $qty_bin"),
														array('BinLocation.bin_location' => $location, 'BinLocation.barcode' => $barcode));
						 					
						 
					$this->CheckIn->updateAll( array('CheckIn.selling_qty' => "CheckIn.selling_qty - $qty_bin"),array('CheckIn.id' => $po_id));							  
					$product =	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $ink_sku)));
					  
					if( count( $product ) > 0 )
					{
							$ordertype = 'Delete';
							
							$currentStickLevel	=	$product['Product']['current_stock_level'];
							$qtyForUpdate		=	(int)$currentStickLevel + (int)$qty_bin;								
							
							$data['Product']['product_sku']		=	$product['Product']['product_sku'];
							$data['ProductDesc']['barcode']		=	$product['ProductDesc']['barcode'];
							$data['Product']['CurrentStock']	=	$currentStickLevel;
							
							$productData	=	json_decode(json_encode($data,0));
							App::import('Controller', 'Cronjobs');
							$productCant = new CronjobsController();
							$productCant->storeInventoryRecord( $productData, $qty_bin, $id, $ordertype, $barcode);
							
							$this->Product->updateAll(array('Product.current_stock_level' => $qtyForUpdate), array('Product.product_sku' => $ink_sku));
					}
					
					$this->InkOrderLocation->updateAll( array( 'InkOrderLocation.status' =>"'deleted'") , array( 'order_id' => $orderid ) );
					$val['username'] = ( $this->Session->read('Auth.User.username') != '' ) ? $this->Session->read('Auth.User.username') : '_'; 
					file_put_contents(WWW_ROOT .'logs/ink_order_delete_'.date('dmY').'.log', print_r($val,true), FILE_APPEND | LOCK_EX);
				//}
						
			}
			/*--------------End ink order delete---------------*/
			
			if( (count($orderItems) == 0) || (empty($orderItems) || $orderItems == '') )
			{
				
				$order_details	= $this->OrderLocation->find('all', array('conditions' => array('OrderLocation.order_id' => $orderid,'OrderLocation.status' => 'active')));
			
				if(count($order_details) > 0){
				
					$count = 0;			
					
					foreach($order_details as $val)
					{
				
						$barcode 	= $val['OrderLocation']['barcode'];
						$sku		= $val['OrderLocation']['sku'];
						$location 	= $val['OrderLocation']['bin_location'];
						$quantity 	= $val['OrderLocation']['quantity'];
						$qty_bin 	= $val['OrderLocation']['available_qty_bin'];	
						//$qty_chin 	= $val['OrderLocation']['available_qty_check_in'];
						$po_id	 	= explode(",",$val['OrderLocation']['po_id'])[0];	
								
						
						if($location != 'No Location' && $barcode != '')
						{ echo "AVAAVA";
							$this->BinLocation->updateAll(	array('BinLocation.stock_by_location' => "BinLocation.stock_by_location + $qty_bin"),
														array('BinLocation.bin_location' => $location, 'BinLocation.barcode' => $barcode));
						}					
						 
						$this->CheckIn->updateAll( array('CheckIn.selling_qty' => "CheckIn.selling_qty - $qty_bin"),array('CheckIn.id' => $po_id));							  
						$product =	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
						// look for sku if exist  
						if( count( $product ) > 0 )
						{
							$ordertype = 'Delete';
							
							$currentStickLevel	=	$product['Product']['current_stock_level'];
							$qtyForUpdate		=	(int)$currentStickLevel + (int)$qty_bin;								
							
							$data['Product']['product_sku']		=	$product['Product']['product_sku'];
							$data['ProductDesc']['barcode']		=	$product['ProductDesc']['barcode'];
							$data['Product']['CurrentStock']	=	$currentStickLevel;
							
							$productData	=	json_decode(json_encode($data,0));
							App::import('Controller', 'Cronjobs');
							$productCant = new CronjobsController();
							$productCant->storeInventoryRecord( $productData, $qty_bin, $id, $ordertype, $barcode);
							
							$this->Product->updateAll(array('Product.current_stock_level' => $qtyForUpdate), array('Product.product_sku' => $sku));
								
								
							}		
					}
				$this->OrderLocation->updateAll( array( 'OrderLocation.status' =>"'deleted'") , array( 'OrderLocation.order_id' => $orderid ) );
			   }
				$this->OpenOrder->query("delete from open_orders where num_order_id = '{$id}'");	
				
				echo "1";				
				exit;							
			}
			else
			{
			
					/*-------RoyalMail Changes 13032018---------*/
					App::Import('Controller', 'RoyalMail'); 
					$royal = new RoyalMailController;
					/*----------------End---------------------*/
						$mailFormate = '<html>			
								<head>
								<style>
								table {
									border-collapse: collapse;
									width: 100%;
								}
								th, td {
									text-align: left;
									padding: 8px;
								}								
								tr:hover {background-color: #f5f5f5}
								tr:nth-child(even){background-color: #f2f2f2}
								th {
									background-color: #4CAF50;
									color: white;
								}
								</style>
								</head>
								If, you have already picked those below items, Kindly put back onto shelf!
											<br><br>
											<table border="1" style="border: 1px solid #c2c2c2;">
													<tr>
														<th>OrderID</th>
														<th>Sub-Order</th>
														<th>SKU</th>
														<th>Title</th>
														<th>Barcode</th>
														<th>Location</th>
														<th>Quantity</th>
													</tr>';
			
			
			$order_details	= $this->OrderLocation->find('all', array('conditions' => array('OrderLocation.order_id' => $orderid,'OrderLocation.status' => 'active')));
			
			if(count($order_details) > 0){
				
				$count = 0;			
				
				foreach($order_details as $val){
				
					$barcode 	= $val['OrderLocation']['barcode'];
					$sku		= $val['OrderLocation']['sku'];
					$location 	= $val['OrderLocation']['bin_location'];
					$quantity 	= $val['OrderLocation']['quantity'];
					$qty_bin 	= $val['OrderLocation']['available_qty_bin'];	
					//$qty_chin 	= $val['OrderLocation']['available_qty_check_in'];
					$po_id	 	= explode(",",$val['OrderLocation']['po_id'])[0];	
							
					
					if($location != 'No Location')
					{
						$this->BinLocation->updateAll(	array('BinLocation.stock_by_location' => "BinLocation.stock_by_location + $qty_bin"),
													array('BinLocation.bin_location' => $location, 'BinLocation.barcode' => $barcode));
					}					
					 
					$this->CheckIn->updateAll( array('CheckIn.selling_qty' => "CheckIn.selling_qty - $qty_bin"),array('CheckIn.id' => $po_id));							  
					$product =	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
					// look for sku if exist  
					if( count( $product ) > 0 )
					{
						$ordertype = 'Delete';
						
						$currentStickLevel	=	$product['Product']['current_stock_level'];
						$qtyForUpdate		=	(int)$currentStickLevel + (int)$qty_bin;								
						
						$data['Product']['product_sku']		=	$product['Product']['product_sku'];
						$data['ProductDesc']['barcode']		=	$product['ProductDesc']['barcode'];
						$data['Product']['CurrentStock']	=	$currentStickLevel;
						
						$productData	=	json_decode(json_encode($data,0));
						App::import('Controller', 'Cronjobs');
						$productCant = new CronjobsController();
						$productCant->storeInventoryRecord( $productData, $qty_bin, $id, $ordertype, $barcode);
						
						$result =	$this->Product->updateAll(array('Product.current_stock_level' => $qtyForUpdate), array('Product.product_sku' => $sku));
					
						
						foreach( $orderItems as $getItem )
						{
							$splitID = $getItem['MergeUpdate']['product_order_id_identify'];
							$this->deleteRegisteredNumber( $splitID );								
							$pick_list_status 	= $getItem['MergeUpdate']['pick_list_status'];			
						}
						 
						$mailFormate.='<tr>
											<td>'.$id.'</td>
											<td>'.$splitID.'</td>
											<td>'.$sku.'</td>
											<td>'.$product['Product']['product_name'].'</td>
											<td>'.$product['ProductDesc']['barcode'].'</td>
											<td>'.$location.'</td>
											<td>'.$qty_bin.'</td>
										</tr>';
							//Putback items for shelf ensuring from warehouse keeper
							$this->loadModel( 'PutbackItem' );							
							$putBackItem = array();
							$authDetails = $this->Session->read('Auth.User'); 
							$putBackItem['user_id']	= $authDetails['id'];											
							$putBackItem['OrderID']	= $id;											
							$putBackItem['Sub-Order']= $splitID;												
							$putBackItem['SKU']		= $sku;										
							$putBackItem['Title']	= $product['Product']['product_name'];								
							$putBackItem['Barcode']	= $product['ProductDesc']['barcode'];					
							$putBackItem['Location']= $location;			
							$putBackItem['Quantity']= $qty_bin;	
							$putBackItem['action_type']= 'deleted';	
							if( $sku != '' )
							{
								$this->PutbackItem->saveAll( $putBackItem );
								
								//Send notification						
								$sendNotifyArray[] = $authDetails['first_name'];
								$sendNotifyArray[] = $splitID;						
								$sendNotifyArray[] = 'deleted';
								
								App::import('Controller','MyExceptions');
								$myExceptionObj = new MyExceptionsController(new View());                        
								$myExceptionObj->send_cancel_delete_order_notification(  $sendNotifyArray  );
							}
							
					}	
					
					foreach( $orderItems as $getItem )
					{
						$splitID = $getItem['MergeUpdate']['product_order_id_identify'];
						$this->deleteRegisteredNumber( $splitID );	
						
						/*Cancel Royal mail Shipment.*/
						if($getItem['MergeUpdate']['royalmail'] == 1){
							$royal->royalMailCancelShipment($splitID,1); 			
						} 
						
						$conditionsformergeupdate = array ('MergeUpdate.order_id' => $id);
						$resultMergeUpdate  	= 	$this->MergeUpdate->deleteAll( $conditionsformergeupdate );
						 
						$conditionsforscanorder = array ('ScanOrder.split_order_id' => $splitID);
						$resultScanOrder 		= 	$this->ScanOrder->deleteAll( $conditionsforscanorder );
							
					}
															
					$this->OpenOrder->query("delete from open_orders where num_order_id = '{$id}'");							
					
					
				}
				$this->OrderLocation->updateAll( array( 'OrderLocation.status' =>"'deleted'") , array( 'OrderLocation.order_id' => $orderid ) );
			}else{
				/*************In Case when order is not in order_location table but in Open_order and Merge_update table**************************/
				if(count($orderItems) > 0){
						foreach( $orderItems as $getItem )
						{
							$splitID = $getItem['MergeUpdate']['product_order_id_identify'];
							$this->deleteRegisteredNumber( $splitID );	
							/*Cancel Royal mail Shipment.*/
							if($getItem['MergeUpdate']['royalmail'] == 1){
								$royal->royalMailCancelShipment($splitID,1); 			
							}
							$conditionsformergeupdate = array ('MergeUpdate.order_id' => $id);
							$resultMergeUpdate  	= 	$this->MergeUpdate->deleteAll( $conditionsformergeupdate );
							 
							$conditionsforscanorder =  array ('ScanOrder.split_order_id' => $splitID);
							$resultScanOrder 		=  $this->ScanOrder->deleteAll( $conditionsforscanorder );
							$pick_list_status 	    =  $getItem['MergeUpdate']['pick_list_status'];			
						}
						
						
					$product =	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
					$this->OpenOrder->query("delete from open_orders where num_order_id = '{$id}'");
					echo "1";	
					exit;
				}			
			}			
			
			
			   
			$mailFormate.=  '</table></body></html>';
			   
			 
			   
			   if( $pick_list_status == 1 )
			   {  
				   /* send the mail of operater */
					App::uses('CakeEmail', 'Network/Email');
					$email = new CakeEmail('');
					$email->emailFormat('html');
					$email->from('info@euracogroup.co.uk');
					$email->to( array('avadhesh.jij@rediffmail.com','je-operations@esljersey.com'));					  
					$getBase = Router::url('/', true);
					$email->subject('Xsensys : Order delete after generate pick list');
					$email->send( $mailFormate );
			   }
			   echo "1";
			   exit;	
			}
			
  	  }
	  
  	  public function deleteOrder010617( $orderId = null )
		{
			$this->layout = '';
			$this->autoRender = false;
			
			if( isset($orderId) && $orderId > 0 )
			{
				$id	= $orderId;
			}
			else
			{
				$id	= $this->request->data['openorderid'];
			}
			
			$this->loadModel('OpenOrder');
			$this->loadModel('OrderItem');
			$this->loadModel('Product');
			$this->loadModel('CancelOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('ScanOrder');
			$this->loadModel('BinLocation');
			
			$getOpernOrder	=	$this->getOpenOrderById( $id );
			
			$order			=	$this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $id)));
			$orderid		=	$order['OpenOrder']['num_order_id'];
			$pkid			=	$order['OpenOrder']['order_id'];
			$orderItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.order_id' => $id)));
			
			if( (count($orderItems) == 0) || (empty($orderItems) || $orderItems == '') )
			{
				$this->OpenOrder->query("delete from open_orders where num_order_id = '{$id}'");
				echo "1";
				exit;							
			}
			else
			{
				$mailFormate = '<html>
			
								<head>
								<style>
								table {
									border-collapse: collapse;
									width: 100%;
								}

								th, td {
									text-align: left;
									padding: 8px;
								}
								
								tr:hover {background-color: #f5f5f5}
								
								tr:nth-child(even){background-color: #f2f2f2}

								th {
									background-color: #4CAF50;
									color: white;
								}
								</style>
								</head>											If, you have already picked those below items, Kindly put back onto shelf!
											<br><br>
											<table border="1" style="border: 1px solid #c2c2c2;">
													<tr>
														<th>OrderID</th>
														<th>Sub-Order</th>
														<th>SKU</th>
														<th>Title</th>
														<th>Barcode</th>
														<th>Location</th>
														<th>Quantity</th>
													</tr>';
			
			
			foreach( $orderItems as $getItem )
			   {
				   $splitID = $getItem['MergeUpdate']['product_order_id_identify'];
				   $skus	=	 explode( ',', $getItem['MergeUpdate']['sku']);
				     foreach($skus as $sku)
					   {
						   $newSku			=	 explode( 'XS-', $sku);
						  
						   $Quantity 			=	$newSku[0];
						   $sku 				=	'S-'.$newSku[1];
						   $product				=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
						  // look for sku if exist  
						  if( count( $product ) > 0 )
						  {
							   $currentStickLevel	=	$product['Product']['current_stock_level'];
							   
							   /*if( $currentStickLevel <= 0 )
							   {
								   $newValue = 0;
							   }
							   else
							   {
								   $newValue = $currentStickLevel;
							   }*/
							   
							   
							   $qtyForUpdate		=	(int)$Quantity + (int)$currentStickLevel;
							   $ordertype = 'Delete';
							   $data['Product']['product_sku']		=	$product['Product']['product_sku'];
							   $data['ProductDesc']['barcode']		=	$product['ProductDesc']['barcode'];
							   $barcodeVal = $product['ProductDesc']['barcode'];;
							   $data['Product']['CurrentStock']		=	$qtyForUpdate;
							   
								$productData	=	json_decode(json_encode($data,0));
								App::import('Controller', 'Cronjobs');
								$productCant = new CronjobsController();
								$productCant->storeInventoryRecord( $productData, $Quantity, $id, $ordertype, $barcodeVal);
							  
							   $result =	$this->Product->updateAll(array('Product.current_stock_level' => $qtyForUpdate), array('Product.product_sku' => $sku));
					      }
							$conditionsformergeupdate = array (
										'MergeUpdate.order_id' => $id
									);
							$conditionsforscanorder = array (
										'ScanOrder.split_order_id' => $splitID
									);
							$conditionsforopenorder = array (
										'OpenOrder.num_order_id' => $id
									);
									
							$this->OpenOrder->query("delete from open_orders where num_order_id = '{$id}'");							
							$resultMergeUpdate  	= 	$this->MergeUpdate->deleteAll( $conditionsformergeupdate );
							$resultScanOrder 		= 	$this->ScanOrder->deleteAll( $conditionsforscanorder );
							$this->deleteRegisteredNumber( $splitID );
						
							
					   }
					    if( $getItem['MergeUpdate']['pick_list_status'] == 1 && $sku != '' )
						{
						  $barcode = $product['ProductDesc']['barcode'];
						  $product['ProductDesc']['barcode'];
						  /* find the all bin location of barcode*/
						  $getBinLocations 	=	$this->BinLocation->find( 'list', array( 'conditions' => array( 'BinLocation.barcode' => $barcode ), 'fields' => array( 'BinLocation.stock_by_location' ) ) );
						  $i = 0;
						  /* min value for all location */
						  $binkey=array_keys($getBinLocations, min($getBinLocations));
						  $binLocationId	=	$binkey[0];
						 
						  /* find the current value of location  */
						  $getlocationQty	=	$this->BinLocation->find('first', array( 'conditions' => array( 'BinLocation.id' => $binLocationId )) );
						  $locationQty	=	$Quantity + $getlocationQty['BinLocation']['stock_by_location'];
						 
						  /* update the location quantity */
						  $updateresult		=	$this->BinLocation->updateAll(array('BinLocation.stock_by_location' => $locationQty), array('BinLocation.id' => $binLocationId));
						  
						  $mailFormate.=			  '<tr>
														<td>'.$id.'</td>
														<td>'.$splitID.'</td>
														<td>'.$sku.'</td>
														<td>'.$product['Product']['product_name'].'</td>
														<td>'.$product['ProductDesc']['barcode'].'</td>
														<td>'.$getlocationQty['BinLocation']['bin_location'].'</td>
														<td>'.$Quantity.'</td>
													</tr>';
							//Putback items for shelf ensuring from warehouse keeper
							$this->loadModel( 'PutbackItem' );							
							$putBackItem = array();
							$authDetails = $this->Session->read('Auth.User'); 
							$putBackItem['user_id']	= $authDetails['id'];											
							$putBackItem['OrderID']	= $id;											
							$putBackItem['Sub-Order']= $splitID;												
							$putBackItem['SKU']		= $sku;										
							$putBackItem['Title']	= $product['Product']['product_name'];								
							$putBackItem['Barcode']	= $product['ProductDesc']['barcode'];					
							$putBackItem['Location']= $getlocationQty['BinLocation']['bin_location'];			
							$putBackItem['Quantity']= $Quantity;	
							$putBackItem['action_type']= 'deleted';	
							if( $sku != '' )
							{
								$this->PutbackItem->saveAll( $putBackItem );
								
								//Send notification						
								$sendNotifyArray[] = $authDetails['first_name'];
								$sendNotifyArray[] = $splitID;						
								$sendNotifyArray[] = 'deleted';
								
								App::import('Controller','MyExceptions');
								$myExceptionObj = new MyExceptionsController(new View());                        
								$myExceptionObj->send_cancel_delete_order_notification(  $sendNotifyArray  );
							}
													
					  }
			   }
			   
			    $mailFormate.=  '</table></body></html>';
			   
			   
			   
			   if( $getItem['MergeUpdate']['pick_list_status'] == 1 )
			   {
				   /* send the mail of operater */
					App::uses('CakeEmail', 'Network/Email');
					$email = new CakeEmail('');
					$email->emailFormat('html');
					$email->from('info@euracogroup.co.uk');
					$email->to( array('avadhesh.jij@rediffmail.com','je-operations@esljersey.com'));					  
					$getBase = Router::url('/', true);
					$email->subject('Xsensys : Order delete after generate pick list');
					$email->send( $mailFormate );
			   }
			   echo "1";
			   exit;	
			}
			
  	  }
	  
	  public function deleteRegisteredNumber( $splitOrderID )
		{
			$this->loadModel( 'RegisteredNumber' );
			$checkNumber	=	$this->RegisteredNumber->find('first', array( 'conditions' => array( 'split_order_id' =>  $splitOrderID ) ) );
			if( count($checkNumber) > 0 )
			{
				$data['id'] 			= 	$checkNumber['RegisteredNumber']['id'];
				$data['split_order_id'] = 	'';
				$this->RegisteredNumber->saveAll( $data );
			}
			
		}
  	  
		
		public function getBundleOrderList( $bundleId = null )
		{
			
			/* for custom postal service */
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'Template' );
			$this->loadModel( 'PackagingSlip' );
			
			$this->MergeUpdate->bindModel(
				array(
					'hasOne' => array(
						'PackagingSlip' => array(
							'foreignKey' => false,
							'conditions' => array('PackagingSlip.id = MergeUpdate.packSlipId'),
							'fields' => array('PackagingSlip.id,PackagingSlip.name')
						),
						'Template' => array(
							'foreignKey' => false,
							'conditions' => array('Template.id = MergeUpdate.lable_id'),
							'fields' => array('Template.id,Template.label_name')
						)
					)
				)
			);
			
		 $orderItem	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.order_id' => $bundleId, 'MergeUpdate.status' => 0)));			 			 			 						
		 return $orderItem;	 
		}
		
		public function getItemListForGlobal( $bundleId = null )
		{
			
			/* for custom postal service */
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'Template' );
			$this->loadModel( 'PackagingSlip' );
			
			$this->MergeUpdate->bindModel(
				array(
					'hasOne' => array(
						'PackagingSlip' => array(
							'foreignKey' => false,
							'conditions' => array('PackagingSlip.id = MergeUpdate.packSlipId'),
							'fields' => array('PackagingSlip.id,PackagingSlip.name')
						),
						'Template' => array(
							'foreignKey' => false,
							'conditions' => array('Template.id = MergeUpdate.lable_id'),
							'fields' => array('Template.id,Template.label_name')
						)
					)
				)
			);
			
		 $orderItem	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.order_id' => $bundleId)));			 			 			 						
		 return $orderItem;	 	 
			
		}
		
		public function getOrderDetail( $id=null )
		{
			$this->layout = 'index';
			$this->loadModel( 'OpenOrder' );
			$getdetails	=	$this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $id)));
			$itemDetails	=	$this->getAllOrderItem( $id );
			$itemMergeUpdteDetails	=	$this->getAllMergeUpdateItem( $id );
			
			$data['OrderId']		=	$getdetails['OpenOrder']['order_id'];
			$data['NumOrderId']		=	$getdetails['OpenOrder']['num_order_id'];
			$data['GeneralInfo']	=	unserialize($getdetails['OpenOrder']['general_info']);
			$data['ShippingInfo']	=	unserialize($getdetails['OpenOrder']['shipping_info']);
			$data['CustomerInfo']	=	unserialize($getdetails['OpenOrder']['customer_info']);
			$data['TotalsInfo']		=	unserialize($getdetails['OpenOrder']['totals_info']);
			$data['FolderName']		=	unserialize($getdetails['OpenOrder']['folder_name']);
			$data['Items']			=	$itemMergeUpdteDetails;//unserialize($getdetails['OpenOrder']['items']);
	
			$getDetail				=	json_decode(json_encode($data));
			$this->set('getDetail', $getDetail);
		}
		
		   public function barcodeScane()
		   {
			   $this->layout = "index";
		   }
		   
		    /*
		    * function use for get search list after scanning the barcode
		    * 
		    * */
		   public function getuserAllids()
		   {
		   	 $this->loadModel('User');
			$firstName = ( $_SESSION['Auth']['User']['first_name'] != '' ) ? $_SESSION['Auth']['User']['first_name'] : '_';
		   $lastName = ( $_SESSION['Auth']['User']['last_name'] != '' ) ? $_SESSION['Auth']['User']['last_name'] : '_';
		   $pcName = ( $_SESSION['Auth']['User']['pc_name'] != '' ) ? $_SESSION['Auth']['User']['pc_name'] : '_';
		   $user_id = ( $_SESSION['Auth']['User']['id'] != '' ) ? $_SESSION['Auth']['User']['id'] : '_';
		   $user_name = $firstName.' '.$lastName;
		   $findusers = $this->User->find( 'all', array( 'conditions' => array( 'first_name' => $firstName, 'last_name' => $lastName ) ) );
			 $user_ids = array(0,24,29,30,31,40,48,124,149,150,197,208,13,42,43,45,100,122,151,196,207,235,286,303);
			   foreach( $findusers as $finduser )
			   {
			   		$user_ids[] = $finduser['User']['id'];
			   }
			   return  $user_ids;
		   }
		   
		   public function getsearchlist()
		   {
				$this->autoRender = false;
				$this->layout = '';
				$this->loadModel('ScanOrder');
				$this->loadModel('OrderLocation');
				$this->loadModel('OpenOrder');
				$this->loadModel('OrderItem');
				$this->loadModel('MergeUpdate');
				$this->loadModel('Product');
				$this->loadModel('User');
				$this->loadModel('DynamicPicklist');
				$express1	=	array();
				$express2	=	array();
				$standerd1	=	array();
				$standerd2	=	array();
				$tracked1	=	array();
				$tracked2	=	array();
				$dp_ids  = [];
				
				$barcode		=	 trim($this->request->data['barcode']);
				/*************************************Apply code for Global Barcode*********************************************/
				$barcodeg	=	$this->GlobalBarcode->getGlobalBarcode($barcode);
				/**************************************End Apply code for Global Barcode***********************************************/
				$remaning_qty = $cancel_qty = $locked_qty = 0;
			    
				$reparams	=	array(
							'joins' => array(
									array(
										'table' => 'merge_updates',
										'alias' => 'MergeUpdate',
										'type' => 'INNER',
										'conditions' => array(
											'MergeUpdate.product_order_id_identify = ScanOrder.split_order_id',
											'MergeUpdate.status' => [0,2,3] 
											
										)
									)
								),
							'conditions' => array(  'ScanOrder.barcode' => [$barcode,$barcodeg] ),
							'fields' => array( 'MergeUpdate.product_order_id_identify','MergeUpdate.status', 'MergeUpdate.pick_list_status',  'MergeUpdate.reverce_picklist_status','ScanOrder.quantity','ScanOrder.sku','ScanOrder.barcode','ScanOrder.update_quantity' ) 
						);
 				$getremaningresult 	=	$this->ScanOrder->find( 'all', $reparams );
				// pr($getremaningresult 	);
				if(count($getremaningresult)> 0){
 					
					foreach($getremaningresult as $ord){
						if($ord['MergeUpdate']['status'] == 0 && $ord['MergeUpdate']['pick_list_status'] == 1){
 							$remaning_qty += ($ord['ScanOrder']['quantity'] - $ord['ScanOrder']['update_quantity']);
						}
						if($ord['MergeUpdate']['status'] == 2){
 							$cancel_qty += $ord['ScanOrder']['quantity'];
						}
						if($ord['MergeUpdate']['status'] == 3){
 							$locked_qty += $ord['ScanOrder']['quantity'];
						}
					}
				}
					   
			   $dpusers = $this->DynamicPicklist->find( 'all', array('fields' =>['id','assign_by_user'], 'conditions' => ['packing_user_email' => $this->Session->read('Auth.User.email'),'completed_mark_user' => NULL ] ) );
			  
			   foreach( $dpusers as $dpu )
			   {
			   		$dp_ids[] = $dpu['DynamicPicklist']['id'];
			   }
			   
			   $firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			   $lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			   $pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
			   $user_id = ( $this->Session->read('Auth.User.id') != '' ) ? $this->Session->read('Auth.User.id') : '_';
			   $user_name = $firstName.' '.$lastName;
			   $findusers = $this->User->find( 'all', array( 'conditions' => array( 'first_name' => $firstName, 'last_name' => $lastName ) ) );			 
			   $user_ids = array(0,23,24,29,30,31,40,48,124,149,150,197,208,590);
			  
			   foreach( $findusers as $finduser )
			   {
			   		$user_ids[] = $finduser['User']['id'];
			   }
   			   $usertype = $this->Session->read('Auth.User.service_pkst');
			   
			   if($barcode)
			   {
				   $new	=	array( $barcode );
				   
				   $param  = array(	'OR' => array(
										array( 'MergeUpdate.linn_fetch_orders' => '1' ),
										array( 'MergeUpdate.linn_fetch_orders' => '4' )	
								)
					);	
			//	   $getItems = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcode .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1','MergeUpdate.out_sell_marker' => '0', $param )));
				  if( count($user_ids) > 1 ){
				   $getItems_local = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcode .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1',  'MergeUpdate.linn_fetch_orders IN' => array(1,4), 'MergeUpdate.user_name IN' => $user_ids ) ) );
				   
				   	$getItems_global = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcodeg .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1', 'MergeUpdate.linn_fetch_orders IN' => array(1,4), 'MergeUpdate.user_name IN' => $user_ids ) ) );
				   } else {
				   	$getItems_local = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcode .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1',  'MergeUpdate.linn_fetch_orders IN' => array(1,4), 'MergeUpdate.user_name ' => 0 ) ) );
				   
				   	$getItems_global = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcodeg .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1', 'MergeUpdate.linn_fetch_orders IN' => array(1,4), 'MergeUpdate.user_name ' => 0 ) ) );
				   
				   }
				   $getItems = array_merge($getItems_local,$getItems_global);
				  
				   if(count( $getItems) < 1){  
					   $getItems_local = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcode .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1',  'MergeUpdate.linn_fetch_orders IN' => array(1,4), 'MergeUpdate.user_id ' => $user_ids ) ) );
					   
					   $getItems_global = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcodeg .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1', 'MergeUpdate.linn_fetch_orders IN' => array(1,4), 'MergeUpdate.user_id ' => $user_ids ) ) );
				   
				   
				 		$getItems = array_merge($getItems_local,$getItems_global);
				  }
				  
				  
				   
				   $e = 0;
				   foreach( $getItems as $getItem )
				   {
					   $id		=	 $getItem['MergeUpdate']['order_id'];
					   $skus	=	 explode( ',', $getItem['MergeUpdate']['sku']);
					   $barcode	=	 explode( ',', $getItem['MergeUpdate']['barcode']);
					   $assing_user	=	 $getItem['MergeUpdate']['assign_user'];
					   
					   $i = 0;
					   foreach($skus as $sku)
					   {
						   $newSku[$i]			=	 explode( 'X', $sku);
						   $newSku[$i][2]		=	 $barcode[$i];
						   $getProductdesc		=	$this->Product->find('first', array( 'conditions' => array('Product.product_sku' => $newSku[$i][1])));
						   $title	=	 $getProductdesc['Product']['product_name'];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['split_qty']		=	$newSku[$i][0];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['split_sku']		=	$newSku[$i][1];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['split_barcode']	=	$newSku[$i][2];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['title']			=	$title;
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['assign_user']		=	$assing_user;
						   $i++;
					   }
					   $e++;
				   }
			   }
			   
				if(isset($getItems))
				{
					foreach($getItems as $itemdetail)
					{
						if($itemdetail['MergeUpdate']['postal_service'] == 'Express'){
						
							if(empty($itemdetail['MergeUpdate']['user_name'])){
								if(count($itemdetail['MergeUpdate']['ItemList']) == 1){
									$express1[]	= $itemdetail;
								}
								else if(count($itemdetail['MergeUpdate']['ItemList']) > 1){
									$express2[]	= $itemdetail;
								}
							}else if( $usertype == 1 || $itemdetail['MergeUpdate']['user_name'] == $user_id){
								if(count($itemdetail['MergeUpdate']['ItemList']) == 1){
									$express1[]	= $itemdetail;
								}
								else if(count($itemdetail['MergeUpdate']['ItemList']) > 1){
									$express2[]	= $itemdetail;
								}
							}
 						
						}  else {
 								
								if(($itemdetail['MergeUpdate']['postal_service'] == 'Default' || $itemdetail['MergeUpdate']['postal_service']== 'Standard' || $itemdetail['MergeUpdate']['postal_service'] == 'Standard_Jpost' ) && count($itemdetail['MergeUpdate']['ItemList']) == 1)
								{
									$standerd1[]	=	$itemdetail;
								}
								if($itemdetail['MergeUpdate']['postal_service'] == 'Tracked' && count($itemdetail['MergeUpdate']['ItemList']) == 1)
								{
									 $tracked1[] = $itemdetail;
								}
								
								if(($itemdetail['MergeUpdate']['postal_service'] == 'Default' || $itemdetail['MergeUpdate']['postal_service'] == 'Standard' || $itemdetail['MergeUpdate']['postal_service'] == 'Standard_Jpost' ) && count($itemdetail['MergeUpdate']['ItemList']) > 1)
								{
									$standerd2[]	=	$itemdetail;
								};
								
 								if($itemdetail['MergeUpdate']['postal_service'] == 'Tracked' && count($itemdetail['MergeUpdate']['ItemList']) > 1)
								{
									 $tracked2[] = $itemdetail;
								}
						 }
					}
				}
				 
			   $this->set(compact('express1','standerd1','tracked1','express2','standerd2','tracked2','remaning_qty','cancel_qty','locked_qty'));
			   echo $this->render('scansearch');
			   exit;
		   }
		   
		    public function dispatchConsole()
			{
				$this->layout = 'index';
			}
			
			/********************* function use for confirm barcode *********************************/
		   public function confirmBarcode( $id = null)
		   {
			   $this->layout='index';
			   $this->loadModel('OpenOrder');
			   $this->loadModel('MergeUpdate');
			   $this->loadModel('Product');
			   $this->loadModel('Product');
			   $this->loadModel('PackageEnvelope');
			   $this->loadModel('Packagetype');
			  
			  
			   $getItems = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.id' => $id )));
			   
			   /*****************Apply RoyalMail 16042018*******************/	
			   if($getItems[0]['MergeUpdate']['status'] == 0 && $getItems[0]['MergeUpdate']['service_provider'] == 'Royalmail' && $getItems[0]['MergeUpdate']['royalmail'] == 0 && $getItems[0]['MergeUpdate']['delevery_country'] == 'United Kingdom'){
					//App::Import('Controller', 'RoyalMail'); 
					//$royal = new RoyalMailController;
					//$royal->applyRoyalMailByOrder($getItems[0]['MergeUpdate']['product_order_id_identify']);
				}
				
			   $getOpenOrderDetails = $this->OpenOrder->find( 'first', array( 'conditions' => array( 'OpenOrder.num_order_id' =>  $getItems[0]['MergeUpdate']['order_id']) ) );
			   $items	=	unserialize( $getOpenOrderDetails['OpenOrder']['items'] );
			   $customerInfo	=	unserialize( $getOpenOrderDetails['OpenOrder']['customer_info'] );
			   
			   $j = 0;
			   $product_type = '';
			   foreach( $items as $item )
			   {
			   		$singalskus			=	explode('-',$item->SKU);
					if($singalskus[0] == 'B' && count($singalskus) > '3')
					{
						$product_type = 'Bundle';
						while( $i < count($singalskus)-1 )
						{
							$margeskus[ 'S-'.$singalskus[$i] ]   = $item->ChannelTitle;
							$channels[ 'S-'.$singalskus[$i] ]   = $item->ChannelSKU;
							$i++;
						}
						foreach($channels as $k =>$v)
						{
						$channel_sku[$k] = $v;
						}
						foreach($margeskus as $key => $value)
						{
							$order_sku_detail[$key] 	 = $value; 
							$j++;
						}
					}
					elseif($singalskus[0] == 'B' && count($singalskus) == '3')
					{
						$product_type = 'Bundle';
						$order_sku_detail['S-'.$singalskus[1]] 	 =  $item->ChannelTitle; 
						$channel_sku['S-'.$singalskus[1]] = $item->ChannelSKU;
					}
					else
					{
						$product_type = 'Single';
						$order_sku_detail[$item->SKU] 	 =  $item->ChannelTitle; 
						$channel_sku[$item->SKU] = $item->ChannelSKU;
					}
			  }
			  
			   
			   $getPackaeClass =array();
			   $e = 0;
				   foreach( $getItems as $getItem )
				   {
					   
					   $id		=	 $getItem['MergeUpdate']['order_id'];
					   $skus	=	 explode( ',', $getItem['MergeUpdate']['sku']);
					   $barcode	=	 explode( ',', $getItem['MergeUpdate']['barcode']);
					   
					   $i = 0;
					   foreach($skus as $sku)
					   {
						   $newSku[$i]	=	 explode( 'X', $sku);
						   $newSku[$i][2]		=	 $barcode[$i];
						   $getProductdesc	=	$this->Product->find('first', array( 'conditions' => array('Product.product_sku' => $newSku[$i][1])));
						   $title	=	 $getProductdesc['Product']['product_name'];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['split_qty']		=	$newSku[$i][0];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['split_sku']		=	$newSku[$i][1];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['split_barcode']	=	$newSku[$i][2];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['title']			=	$title;
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['source_title']		=	$order_sku_detail[$newSku[$i][1]];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['channel_sku']		=	$channel_sku[$newSku[$i][1]];
						   $i++;
					   }
					   $e++;
					   
					   $getPackageClassId = $this->Packagetype->find('first', array('conditions' => array( 'Packagetype.package_type_name' => $getItem['MergeUpdate']['packaging_type'] )));
					 
					   $getPackaeClass['getPackageClassId'] = $getPackageClassId['Packagetype']['id'];
					   
				   }
				   
			   $packageEnvelop	=	$this->Packagetype->find('list', array('fields' =>array('Packagetype.package_type_name') ));
			   
			   $envelop	=	$this->PackageEnvelope->find('list', array('fields' =>array('PackageEnvelope.id','envelopeName') ));
			   $type		=	$product_type;
			   $this->set(compact('type', $type ));
			   $this->set(compact('envelop', $envelop ));
			   $this->set(compact('packageEnvelop', $packageEnvelop ));
			   $this->set(compact('getItems', $getItems ));
			   $this->set(compact('getPackaeClass', $getPackaeClass ));
			   $this->set(compact('customerInfo', $customerInfo ));
			   
		   }
		   /*
		    * Use for custom packaging assign
		    * 
		    * */
		    
		    public function customServicePackaging()
		    {
				$this->autorender = false;
			    $this->layout = '';
				
				$this->loadModel('MergeUpdate');
				$this->loadModel('PackageEnvelope');
			    $this->loadModel('Packagetype');
				
				$id			=	 	$this->request->data['orderId'];
			    $packClass	=	 	$this->request->data['packClass'];
			    $envID		=	 	$this->request->data['envID'];
			    $envName	=	 	$this->request->data['envName'];
			    
			    /*$this->PackageEnvelope->unbindModel(
					array(
						'belongsTo' => array(
							'Packagetype'
						)
					)
			    );*/
			    
			    $getpackagedetail =	$this->PackageEnvelope->find('first', array( 'conditions' =>array( 'PackageEnvelope.id' =>  $envID)));
			   
			    $data['MergeUpdate']['id'] = $id;
			    $data['MergeUpdate']['envelope_id'] = $getpackagedetail['PackageEnvelope']['id'];
			    $data['MergeUpdate']['envelope_name'] = $getpackagedetail['PackageEnvelope']['envelope_name'];
			    $data['MergeUpdate']['envelop_cost'] = $getpackagedetail['PackageEnvelope']['envelop_cost'];
			    $data['MergeUpdate']['envelope_weight'] = $getpackagedetail['PackageEnvelope']['envelope_weight'];
			    $data['MergeUpdate']['packaging_type'] = $getpackagedetail['Packagetype']['package_type_name'];
			       
			    if( $this->MergeUpdate->saveAll( $data ) )
			    {
					echo "1";
				}
				else
				{
					echo "0";
				}
			    
			    exit;
			}
			
			/*
			 * 
			 * Params, Get All Variants  
			 * 
			 * 
			 */ 
		   public function getAllVariantById()
		   {
				$this->autorender = false;
			    $this->layout = '';
				
				$this->loadModel('PackageEnvelope');
			    $this->loadModel('Packagetype');
			   
			    $pkClassId = $this->request->data['packClassId'];
				$param = array(
					'conditions' => array(
						'PackageEnvelope.packagetype_id' => $pkClassId
					),
					'fields' => array(
						'PackageEnvelope.envelope_name'
					)
				);
				
				$envelop = $this->PackageEnvelope->find('list' , $param );
				$this->set('envelop', $envelop);
				$this->render( 'variants' );
		   }	 	
		    
		   public function confirmBarcode_old( $id = null)
		   {
			   $this->layout='index';
			   $this->loadModel('OpenOrder');
			   $orderDI		=	$id;
			   $getOrderDeatils	=	$this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $id )));
			   $itemDetails	=	$this->getAllOrderItem( $id );
			  
			   $data['Id']				=	$getOrderDeatils['OpenOrder']['id'];
			   $data['OrderId']			=	$getOrderDeatils['OpenOrder']['order_id'];
			   $data['NumOrderId']		=	$getOrderDeatils['OpenOrder']['num_order_id'];
			   $data['GeneralInfo']		=	unserialize($getOrderDeatils['OpenOrder']['general_info']);
			   $data['ShippingInfo']	=	unserialize($getOrderDeatils['OpenOrder']['shipping_info']);
			   $data['CustomerInfo'] 	=	unserialize($getOrderDeatils['OpenOrder']['customer_info']);
			   $data['FolderName']		=	unserialize($getOrderDeatils['OpenOrder']['folder_name']);
			   //$data['Items']			=	unserialize($getOrderDeatils['OpenOrder']['items']);
			   $data['Items']			=	$itemDetails;
			   $data['ScanningQty']		=	$getOrderDeatils['OpenOrder']['scanning_qty'];
			   $data['Status']			=	$getOrderDeatils['OpenOrder']['status'];
			   
			   $detail					=	json_decode(json_encode($data), TRUE);
			   $this->set(compact('detail', 'itemDetails'));
			   
		   }
		   
		   /*************************************************************************************************/
		   public function completethebarcode()
		   {
			   $this->autorender = false;
			   $this->layout = '';
			   $this->loadModel('OpenOrder');
			   $this->loadModel('OrderItem');
			   $this->loadModel('MergeUpdate');
			   $this->loadModel('Product');
			   $this->loadModel('ScanOrder');
			   $firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			   $lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			   $pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
			   $userid = ( $this->Session->read('Auth.User.id') != '' ) ? $this->Session->read('Auth.User.id') : '_';
			   $username = $firstName.' '.$lastName;
			   
			   $barcode			=	 	$this->request->data['barcode'];
			   $id				=	 	$this->request->data['id'];
			   $splitOrderId	=	 	$this->request->data['splitOrderId'];
			  
			   $this->loadModel( 'RoyalmailError' ); 
			   $rm_error	=	$this->RoyalmailError->find('first', array('conditions' => array('RoyalmailError.split_order_id' => $splitOrderId )));
				if(count($rm_error) == 0){
				   
				   /*************************************Apply code for Global Barcode*********************************************/
				   $barcode		=	$this->GlobalBarcode->getGlobalBarcode($barcode);
				   /*************************************Apply code for Global Barcode***********************************************/
				   
					$check_user		=	$this->ScanOrder->find('first', array('conditions' => array('ScanOrder.user_name !=' => "",'ScanOrder.split_order_id' => $splitOrderId )));
				   
				   if(count($check_user) > 0 && ($check_user['ScanOrder']['user_name'] != $username))
					{
					   $userdetail		=	$this->ScanOrder->find('first', array('conditions' => array('ScanOrder.split_order_id' => $splitOrderId)));
					   $pr_user			=	$userdetail['ScanOrder']['user_name'];
					   $pr_pc			=	$userdetail['ScanOrder']['pc_name'];
					   echo (json_encode(array('status' => '0', 'userdetail' => $pr_user.' [ '.$pr_pc.' ] ', 'id' => $id)));
					   exit;
					} 
				   
				   $quantity	=	$this->ScanOrder->find('all', array('conditions' => array('ScanOrder.order_id' => $id, 'ScanOrder.barcode' => $barcode)));
				   /*Apply code for Global Barcode*/
				   if(count($quantity) < 1 ) {
						$quantity = $this->ScanOrder->find('all', array('conditions' => array('ScanOrder.order_id' => $id, 'ScanOrder.barcode' => $this->request->data['barcode'])));
					}
					
					$itemID		=	$quantity[0]['ScanOrder']['id'];
					$totalqty	=	$quantity[0]['ScanOrder']['quantity'];
					$upqty		=	$quantity[0]['ScanOrder']['update_quantity'];
					$sku		=	$quantity[0]['ScanOrder']['sku'];
					$lockqty = 0;
				   if($splitOrderId == '2276333-1'){
					   //echo "totalqty=".$totalqty;  echo "upqty=" ;
					   $totalqty =18;
					   // $upqty = 1;
				   }
				   
				   if($totalqty  != $upqty)
				   {
					   $upqty	=	$upqty+1;
					   $this->ScanOrder->updateAll( array( 'ScanOrder.update_quantity'=>$upqty), array( 'ScanOrder.id' => $itemID) );
					   $this->ScanOrder->updateAll( array( 'ScanOrder.user_name' => "'".$username."'", 'ScanOrder.pc_name' => "'".$pcName."'"), array( 'ScanOrder.split_order_id' => $splitOrderId ) );
					   $this->MergeUpdate->updateAll( array( 'MergeUpdate.assign_user' => "'".$username."'", 'MergeUpdate.user_name' => $userid, 'MergeUpdate.pc_name' => "'".$pcName."'"), array( 'MergeUpdate.product_order_id_identify' => $splitOrderId ) );
					   
					   $detail	=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
					   $detail['Product']['lock_qty']	=	$detail['Product']['lock_qty'] - 1;
					   //$this->Product->updateAll( array( 'Product.lock_qty'=>$detail['Product']['lock_qty']), array( 'Product.product_sku' => $sku) );
					   //$mergeUpdate['id'] = $id;
					   //$mergeUpdate['assign_user'] = $username;
					   $items	=	$this->ScanOrder->find('all', array('conditions' => array('ScanOrder.order_id' => $id)));
					   //$this->MergeUpdate->find( $mergeUpdate );
					   $scanQty = 0;
						   foreach($items as $item )
						   {
							   $scanQty	=	$scanQty +  $item['ScanOrder']['update_quantity'];
						   }
					   $data['status'] = 1;
					   $data['msg'] = $scanQty;
					   echo (json_encode(array('status' => '1', 'msg' => $scanQty, 'id' => $id)));
					   exit;
					}
					else
					{
						echo "2";
						exit;
					}
				}else{
					echo "2";
					exit;
					
				}
				exit;
		   }
		   
		   public function getQuantity( $orderId = null, $splitOrderId = null)
		   {
			   $this->loadMOdel( 'ScanOrder' );
			   $itemdetails	=	$this->ScanOrder->find('all', array('conditions' => array('ScanOrder.order_id' => $orderId, 'ScanOrder.split_order_id' => $splitOrderId)));
			   $quantity = 0;
			   foreach( $itemdetails as $itemdetail )
			   {
				   $quantity	=	$quantity + $itemdetail['ScanOrder']['update_quantity'];
			   }
			   return $quantity;
		   }
		   
		   public function getPkOrderid( $id = null )
		   {
			   $this->loadMOdel( 'OpenOrder' );
			   $itemdetails	=	$this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $id)));
			   $pkOrderid	=	$itemdetails['OpenOrder']['order_id'];
			   return $pkOrderid;
		   }
		   
		   /************************** Function use for asign postal tracking ID*********************************/
		   public function asigntrackid()
		   {
			    $this->layout = '';
			    $this->autoRender = false;
			    $this->loadModel('Order');
			    $this->loadModel('OpenOrder');
			    $this->loadModel('MergeUpdate');
				$this->loadModel('Item');
				
				$data['track_id']		=	$this->request->data['trackingBarcode'];
				$data['id']				=	$this->request->data['pkorder'];
				$result	=	$this->MergeUpdate->saveAll( $data );
				if(isset($result))
				{
					echo "1";
					exit;
				}
				else
				{
					echo "2";
					exit;
				}
			   exit;
		   }
		   
		   public function checkBarcodeForSortingOperator()
		   {
			$this->autoRender = false;
			$this->layout = '';

			$this->loadModel('MergeUpdate');

			$express1 = array();
			$express2 = array();
			$standerd1 = array();
			$standerd2 = array();
			$tracked1 = array();
			$tracked2 = array();

			$barcode  =  $this->request->data['barcode']; 
			
			if($barcode)
			{
				$results = $this->MergeUpdate->find('all', 
						array('conditions' => array('MergeUpdate.product_order_id_identify'=>$barcode, 
													'MergeUpdate.status' => '2' , 
													'MergeUpdate.sorted_scanned' => '2' , 
													'MergeUpdate.scan_status' => '2')));
				if(count($results) == 1)
				{
					echo "1";
					exit;
				}
													
			}

			if($barcode)
			{
			
				$results = $this->MergeUpdate->find('all', 
						array('conditions' => array('OR' => array('MergeUpdate.product_order_id_identify'=>$barcode, 'MergeUpdate.brt_barcode'=>$barcode ), 
													'MergeUpdate.status' => '1' , 
													'MergeUpdate.sorted_scanned' => '0' , 
													'MergeUpdate.scan_status' => '0')));
			}
			
			$orderid	=	explode('-',$barcode);
			$check_spain 	= 	$this->MergeUpdate->find('all', array('conditions' => array('OR' => array('MergeUpdate.order_id'=>$orderid[0] ), 
													'MergeUpdate.delevery_country' => 'Spain',
													)));
			if( count( $check_spain ) > 1 )
			{
				foreach($check_spain as $val) {
					if(date('Y-m-d',strtotime($val['MergeUpdate']['scan_date'])) == date('Y-m-d'))
					{
						echo "5";
						exit;
					}
				}
			}
			// Update tables and create manifest according to services those alloted already with manifest feild
			if( count($results) > 0 )
			{
				$i = 0;
				foreach($results as $result)
				{
						$openOrderId = $result['MergeUpdate']['id'];
						$serviceName = $result['MergeUpdate']['service_name'];
						$serviceProvider = $result['MergeUpdate']['service_provider'];
						$serviceCode = $result['MergeUpdate']['provider_ref_code'];
						$country = $result['MergeUpdate']['delevery_country'];
						$serviceid = $result['MergeUpdate']['service_id'];

					// Update Service table and applied counter value
					$this->loadModel( 'ServiceCounter' );
					if($serviceProvider == 'DHL'){
					
						$getCounterValue = $this->ServiceCounter->find( 'first' , 
							array( 'conditions' => array('ServiceCounter.service_id' => $serviceid )
						) );
					
					}else{
						$getCounterValue = $this->ServiceCounter->find( 'first' , 
						array( 'conditions' => 
						array( 
						 //'ServiceCounter.service_name' => $serviceName,
						 //'ServiceCounter.service_provider' => $serviceProvider,
						 //'ServiceCounter.service_code' => $serviceCode,
						  'ServiceCounter.destination' => $country,
						  'ServiceCounter.service_id' => $serviceid          
						)
						) );
					}
					
					// Manage Counter Section
					$counter = 0;
					if( $getCounterValue['ServiceCounter']['counter'] > 0 ):
					$counter = $getCounterValue['ServiceCounter']['counter'];
					$counter = $counter + 1;
					else:
					$counter = $counter + 1;
					endif;
					
					// Manage Order Id's also
					$orderCommaSeperated = '';
					if( $getCounterValue['ServiceCounter']['order_ids'] != '' ):
					$orderCommaSeperated = $getCounterValue['ServiceCounter']['order_ids'];
					$orderCommaSeperated = $orderCommaSeperated.','.$openOrderId;
					else:
					$orderCommaSeperated = $openOrderId;
					endif;

					 // Now, Updating Services ...... 
					 if($getCounterValue['ServiceCounter']['id'] > 0){
						$this->request->data['ServiceCounter']['ServiceCounter']['id'] = $getCounterValue['ServiceCounter']['id'];      
						$this->request->data['ServiceCounter']['ServiceCounter']['counter'] = $counter;
						$this->request->data['ServiceCounter']['ServiceCounter']['order_ids'] = $orderCommaSeperated;         
						$this->ServiceCounter->saveAll( $this->request->data['ServiceCounter'] );
					 }else{
					 	$_data = array();    
 						$_data['destination'] = $country;
						$_data['service_name'] = $serviceName;
						$_data['service_provider'] = $serviceProvider;
						$_data['service_code'] = $serviceCode;
						$_data['service_id'] = $serviceid;
						$_data['counter'] = $counter;
						$_data['order_ids'] = $orderCommaSeperated;         
						$this->ServiceCounter->saveAll( $_data );
					 }
					 
					 // Now, Updating Open Order ...... 
					$this->request->data['MergeUpdate']['MergeUpdate']['id'] = $openOrderId;            
					$this->request->data['MergeUpdate']['MergeUpdate']['sorted_scanned'] = 1;         
					$this->request->data['MergeUpdate']['MergeUpdate']['scan_status'] = 1;  
					$this->request->data['MergeUpdate']['MergeUpdate']['scan_date'] = date('Y-m-d H:i:s');         
					$this->MergeUpdate->saveAll( $this->request->data['MergeUpdate'] );
					
					//one step more to store and calculate the soring items by user logged
					// Get Session User
				   $user_id = $this->Session->read('Auth.User.id');
				   
				   // Call Query
				   $paramOperator = array(
						'conditions' => array(
										'SortingoperatortimeCalculation.user_id' => $user_id,
										'SortingoperatortimeCalculation.today_date' => date( 'Y-m-d' )
						)
					);
				   
				   // Query Here
				   $this->loadModel('SortingoperatortimeCalculation');
				   $getOperator = $this->SortingoperatortimeCalculation->find( 'first', $paramOperator );
				   
				   //Update first out time
				   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['id'] = $getOperator['SortingoperatortimeCalculation']['id'];
				   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['user_id'] = $user_id;
				   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['out_time'] = date('Y-m-d G:i:s');
				   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['no_of_orders_sorted'] = $getOperator['SortingoperatortimeCalculation']['no_of_orders_sorted'] + 1;				   
				   $this->SortingoperatortimeCalculation->saveAll( $this->request->data['SortingoperatortimeCalculation'] ); 
				   
				   // Query Here
				   $getOperator = $this->SortingoperatortimeCalculation->find( 'first', $paramOperator ); 
				   
				   // Calculation
				   $logout = strtotime($getOperator['SortingoperatortimeCalculation']['out_time']);
				   $login  = strtotime($getOperator['SortingoperatortimeCalculation']['in_time']);
				   $diff   = $logout - $login;                               
				   $timeCalculate = round( $diff / 3600 ) ." hour ". round($diff/60)." minutes ".($diff%60)." seconds";
				   
				   // Yes, calculate the time but we need to check one more time is that today or not
				   $dayString = $getOperator['SortingoperatortimeCalculation']['in_time'];
				   $dayStringSub = substr($dayString, 0, 10);

				   $isToday = ( strtotime('now') >= strtotime($dayStringSub . " 00:00") 
																  && strtotime('now') <  strtotime($dayStringSub . " 23:59") );
				   
					// Today
				   if( $isToday == 1 ):
												
						// Update a row again and again
						//Update calculate time
					   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['id'] = $getOperator['SortingoperatortimeCalculation']['id'];
					   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['user_id'] = $user_id;
					   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['total_time'] = $timeCalculate;
					   //$this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['no_of_orders_sorted'] = $glbalSortingCounter;
					   $this->SortingoperatortimeCalculation->saveAll( $this->request->data['SortingoperatortimeCalculation'] ); 
					else:
					
						// Insert new row, If today is not
						//Insert calculate time                                      
					    $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['user_id'] = $user_id;
					    $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['total_time'] = $timeCalculate;
					    //$this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['no_of_orders_sorted'] = $glbalSortingCounter;
					    $this->SortingoperatortimeCalculation->saveAll( $this->request->data['SortingoperatortimeCalculation'] ); 
												
				   endif; 
				   
					 // Manifest Creation
					 //$this->createManifestAccordingToServiceIfYes();
					 
					$i++;
				} 
				
				$strRep = str_replace(')','-',str_replace('(','-',str_replace('<','-',str_replace(' ','-',$serviceProvider.$serviceName.$serviceCode.$country))));
				$strFormat = preg_replace('/[^a-zA-Z0-9\']/', '-', $strRep);                
			return strtolower($strFormat);exit;
			}
			else
			{
				//Check scan status
				$results = json_decode(json_encode($this->MergeUpdate->find('all', 
							array('conditions' => array('MergeUpdate.product_order_id_identify'=>$barcode, 
														'MergeUpdate.status' => '1' )))),0);
														
				if( count($results) > 0 )
				{
					if( $results[0]->MergeUpdate->scan_status == 1 )
					{
						echo "scanned"; exit; 
					}
				}
				else
				{
					//Response Blank         
					echo "none"; exit;	
				}				
			}
		}  
		
		public function getScannedLocation()
		{
			
			$this->autoRender = false;
			$this->layout = '';

			$this->loadModel('MergeUpdate');
			$barcode  =  $this->request->data['barcode']; 
			
			$results = $this->MergeUpdate->find('all', 
			array('conditions' => array('MergeUpdate.product_order_id_identify'=>$barcode, 
										'MergeUpdate.status' => '1' , 
										'MergeUpdate.sorted_scanned' => '1' , 
										'MergeUpdate.scan_status' => '1')));
	
			foreach($results as $result)
			{
				$openOrderId = $result['MergeUpdate']['id'];
				$serviceName = $result['MergeUpdate']['service_name'];
				$serviceProvider = $result['MergeUpdate']['service_provider'];
				$serviceCode = $result['MergeUpdate']['provider_ref_code'];
				$country = $result['MergeUpdate']['delevery_country'];
				 
			}
		
		$strRep = str_replace(')','-',str_replace('(','-',str_replace('<','-',str_replace(' ','-',$serviceProvider.$serviceName.$serviceCode.$country))));
		$strFormat = preg_replace('/[^a-zA-Z0-9\']/', '-', $strRep);                	
		return strtolower($strFormat);exit;
		}
		
		
		
		
		public function generatepdfdirect($openOrderId = '3283949',$paperHeight = 500, $paperWidth  =288 )
		{
			
				$this->loadModel( 'PackagingSlip' );
				$this->loadModel( 'MergeUpdate' );
				$this->loadModel( 'Product' );
				$this->loadMOdel( 'CategoryContant' );
				
				$id				=	$this->request->data['pkorderid'];
				if(isset($this->request->data['openOrderId'])){
					$openOrderId	=	$this->request->data['openOrderId'];
					$splitOrderId	=	$this->request->data['splitOrderId'];
				}else{
					$splitOrderId	=  $openOrderId.'-1';
				}
			
				$this->layout = '';
				$this->autoRender = false;
				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				
				$dompdf->set_paper(array(0, 0, 245, 450), 'portrait');
				$order	=	$this->getOpenOrderById( $openOrderId );
				
 				$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
				$itemPrice		=	$getSplitOrder['MergeUpdate']['price'];
				$itemQuantity	=	$getSplitOrder['MergeUpdate']['quantity'];
				$BarcodeImage	=	$getSplitOrder['MergeUpdate']['order_barcode_image'];
				$dhlSlipData    = 	array();
				if($order['customer_info']->Address->Country == 'United Kingdom'){
 				 	$serverPath		= Router::url('/', true).'jerseypost/labels/Label_'.$splitOrderId.'.pdf';
 				 	// $printerId	    = $this->getBrother();
					$printerId	    = $this->getZebra();
 					$sendData = array(
						'printerId' => $printerId,
						'title' => 'JP Label:'.$splitOrderId,
						'contentType' => 'pdf_uri',
						'content' => $serverPath,
						'source' => 'Direct'					
					);
				}
				else if($getSplitOrder['MergeUpdate']['service_provider'] == 'DHL'){
					$serverPath	= Router::url('/', true).'img/dhl/invoice_'.$splitOrderId.'.pdf';
					//$printerId	    =	$this->getEpson();
					$printerId	    =	$this->getBrother();
					$sendData = array(
						'printerId' => $printerId,
						'title' => 'DHL Invoice:'.$splitOrderId,
						'contentType' => 'pdf_uri',
						'content' => $serverPath,
						'source' => 'Direct'					
					);
					
					if(file_exists(WWW_ROOT .'img/dhl/invoice_amz_'.$splitOrderId.'.pdf')){					
						$dhl_invouce_path = Router::url('/', true).'img/dhl/invoice_amz_'.$splitOrderId.'.pdf';
						$dhlSlipData = array(
							'printerId' => $printerId,
							'title' => 'DHL Amz Invoice:'.$splitOrderId,
							'contentType' => 'pdf_uri',
							'content' => $dhl_invouce_path,
							'source' => 'Direct'					
						);
					} 
					 
				}
				else{
				
				$k = '10';
				$companyname = '';
				$returnaddress = '';
				switch ($k) {
								case 0:
									echo "";
									break;
								case 1:
									echo "";
									break;
								case 2:
									echo "";
									break;
								default:
								   $companyname 	 =  'Jij Group';
								   $returnaddress 	 =  'Address Line 1 <br> Address Line 2 <br>Some Town Some Region 123 ABC <br>United Country';
							}
				
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$subsource			=		$order['sub_source'];
				
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$address			=		'';
				$address			.=		($company != '') ? $company.'<br>' : '' ; 
				$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
				$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
				$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
				$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
				$address			.=		($town != '') ? $town.'<br>' : '';
				$address			.=		($resion != '') ? $resion.'<br>' : '';
				$address			.=		($postcode != '') ? $postcode.'<br>' : '';
				$address			.=		($country != '' ) ? $country.'<br>' : '';
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				
				
				//$gethtml 	=	$this->PackagingSlip->find('all');
				//pr($gethtml);
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= $postcode;
				$setRepArray[] 					= $country;
				$setRepArray[] 					= $phone;
				$setRepArray[] 					= $ordernumber;
				$setRepArray[] 					= $courier;
				$setRepArray[] 					= $recivedate[0];
				$i = 1;
				$str = '';
				$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
				$totalGoods = 0;
				$productInstructions = ''; $is_bulk_sku	= 0;
				$ref_code = trim($getSplitOrder['MergeUpdate']['provider_ref_code']);
				$codearray = array('DP1', 'FR7');
				if( in_array($ref_code, $codearray)) 
				{
					//pr($items);
					$per_item_pcost 	= $postagecost/count($items);
					$j = 1;
					foreach($items as $item){
					$item_title 		= 	$item->Title;
					$quantity			=	$item->Quantity;
					$price_per_unit		=	$item->PricePerUnit;
					$str .= '<tr>
								<td style="border:1px solid #000;" align="left" valign="top" width="10%">'.$quantity.'</td>
								<td style="border:1px solid #000;" valign="top" width="20%">'.substr($item_title, 0, 15 ).'</td>
								<td style="border:1px solid #000;" valign="top" width="15%">'.$quantity * $price_per_unit.'</td>
								<td style="border:1px solid #000;" valign="top" width="15%">'.$per_item_pcost.'</td>
								<td style="border:1px solid #000;" valign="top" width="20%">'.(($price_per_unit * $quantity) + $per_item_pcost).'</td>
							</tr>';
					$j++;
					}
					$str .=	'<tr><td></td><td></td><td></td><td style="border:1px solid #000;">Total</td>
								<td style="border:1px solid #000;" valign="top" width="20%">'.$totalcharge.'</td>
							</tr>';
					
				} 
				else 
				{
					foreach( $skus as $sku )
					{
					
						$newSkus[]				=	 explode( 'XS-', $sku);
						$cat_name = array();
						$sub_can_name = array();
						foreach($newSkus as $newSku)
							{
								
								$getsku = 'S-'.$newSku[1];
								$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
								
								$contentCat 	= 	$getOrderDetail['Product']['category_name'];
								$cat_name[] 	= 	$getOrderDetail['Product']['category_name'];
								$contentSubCat	=	$getOrderDetail['Product']['sub_category'];
								$sub_can_name[] =	$getOrderDetail['Product']['sub_category'];
								$productBarcode	=	$getOrderDetail['ProductDesc']['barcode'];
								$is_bulk_sku	=	$getOrderDetail['Product']['is_bulk_sku'];
								$getContent	= $this->CategoryContant->find( 'first', array( 
																	'conditions'=>array( 'CategoryContant.sub_category'=>$contentSubCat,'CategoryContant.sub_source'=>$subsource) ) );
								if(count($getContent) > 0) {
									$productBarcode			=	$getOrderDetail['ProductDesc']['barcode'];
									$getbarcode				= 	$this->getBarcode( $productBarcode );
									$productInstructions 	=  '<p style="font-size:14px;">'.$getContent['CategoryContant']['content'].'</p>';
								}
								$title	=	$getOrderDetail['Product']['product_name'];
								$totalGoods = $totalGoods + $newSku[0];
								$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
										<td valign="top" class="rightborder bottomborder">'.substr($title, 0, 15 ).'</td>
										<td valign="top" class="center norightborder bottomborder">'.$newSku[0].'</td>';
								$str .= '</tr>';
								$i++;
							}
							unset($newSkus);
						/*$newSkus[]				=	 explode( 'XS-', $sku);
						foreach($newSkus as $newSku)
							{
								
								$getsku = 'S-'.$newSku[1];
								$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
								$title	=	$getOrderDetail['Product']['product_name'];

								$totalGoods = $totalGoods + $newSku[0];
								$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
										<td valign="top" class="rightborder bottomborder">'.substr($title, 0, 15 ).'</td>
										<td valign="top" class="center norightborder bottomborder">'.$newSku[0][0].'</td>';
								$str .= '</tr>';
								$i++;
							}
							unset($newSkus);*/
					}
				}
				
				/**********************************************/
				
				//$gethtml 		=	$this->PackagingSlip->find('first', array( 'conditions' => array('PackagingSlip.name' => $subsource) ));
				if($subsource == 'Marec_FR' || $subsource == 'Marec_DE' || $subsource == 'Marec_IT' || $subsource == 'Marec_ES' || $subsource == 'Marec_uk'){
					$company 	=  'Marec';
				} else if($subsource == 'CostBreaker_UK' || $subsource == 'CostBreaker_DE' || $subsource == 'CostBreaker_FR' || $subsource == 'CostBreaker_ES' || $subsource == 'CostBreaker_IT' || $subsource == 'Costdropper' || $subsource == 'Cdiscount_costbreaker_FR'|| $subSource == 'costbreaker'){
					$company 	=  'CostBreaker';
				} else if($subsource == 'Tech_Drive_UK' || $subsource == 'Tech_Drive_FR' || $subsource == 'Tech_drive_ES' || $subsource == 'Tech_drive_DE' || $subsource == 'Tech_Drive_IT' ){
					$company 	= 'Tech Drive Supplies';
				} else if($subsource == 'RAINBOW RETAIL DE' || $subsource == 'Rainbow Retail' || $subsource == 'Rainbow_Retail_ES' || $subsource == 'Rainbow_Retail_IT' || $subsource == 'Rainbow_Retail_FR'){
					$company 	= 'Rainbow Retail';
				}
				$countryArray = Configure::read('customCountry');
				$countryArray = Configure::read('customCountry');
				if( ($country == 'Germany' || $country == 'France') && in_array($ref_code, $codearray))
				{
					$gethtml =	$this->PackagingSlip->find('first', array(
											'conditions' => array( 
													'PackagingSlip.name' => 'deutsche_post',
													'PackagingSlip.custom_slip_status' => 1
													)
												)
											);
				} 
				else  
				{
					if($country == 'Germany')
					{
						$getSourceName = $this->getCompanyNameBySubsource( $subsource );
						
							$gethtml =	$this->PackagingSlip->find('first', array(
													'conditions' => array( 
															'PackagingSlip.company_name' => $getSourceName,
															'PackagingSlip.custom_slip_status' => 1
															)
														)
													);
					}
					else
					{
						$gethtml =	$this->PackagingSlip->find('first', array(
												'conditions' => array( 
														'PackagingSlip.store_name' => $subsource,
														'PackagingSlip.custom_slip_status' => 0
														)
													)
												);
					
					}
				}
				
				
				/*$gethtml =	$this->PackagingSlip->find('first', array(
											'conditions' => array( 
													'PackagingSlip.store_name' => $subsource,
													)
												)
											);*/
				
				
				 
				$html 			=	$gethtml['PackagingSlip']['html'];				
				$paperHeight    =   $gethtml['PackagingSlip']['paper_height'];
				$paperWidth  	=   $gethtml['PackagingSlip']['paper_width'];
				$barcodeHeight  =   $gethtml['PackagingSlip']['barcode_height'];
				$barcodeWidth   =   $gethtml['PackagingSlip']['barcode_width'];
				$paperMode      =   $gethtml['PackagingSlip']['paper_mode'];
				
			//	pr(	$gethtml);
				/**********************************************/
				//array(0, 0, $paperWidth, $paperHeight )
				$dompdf->set_paper(array(0, 0, $paperWidth, $paperHeight), $paperMode);
				// $dompdf->set_paper(array(0, 0, 8.5 * 72, 13.5 * 72), "landscape");
				 
				$totalitem = $i - 1;
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totalGoods;
				$setRepArray[]	=	 $subtotal;
				$Path 			= 	'/wms/img/client/';
				$img			=	 '<img src=http://localhost/wms/img/client/demo.png height="50" width="50">';
				$setRepArray[]	=	 $img;
				$setRepArray[]	=	 $postagecost;
				$setRepArray[]	=	 $tax;
				$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
				//$setRepArray[]	=	 $totalamount;
				$setRepArray[]	=	 $itemPrice;
				$setRepArray[]	=	 $address;
				$barcodePath  	=  WWW_ROOT.'img/orders/barcode/';
				$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.' width='. $barcodeWidth .'>';
				$barcodenum		=	explode('.', $barcode);
				
				$setRepArray[] 	=  $barcodeimg;
				$setRepArray[] 	=  $paymentmethod;
				$setRepArray[] 	=  $barcodenum[0];
				$setRepArray[] 	=  '';
				
				
				
				$img 			=	$gethtml['PackagingSlip']['image_name'];
				$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
				$setRepArray[] 	=  $returnaddress;
				$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
				$setRepArray[] 	= $splitOrderId;
				$setRepArray[] 	= 	utf8_decode( $productInstructions );
				$prodBarcodePath  	=  Router::url('/', true).'img/product/barcodes/';
				$setRepArray[] 	=  '<img src='.$prodBarcodePath.$productBarcode.'.png width='. $barcodeWidth .'>';
				$setRepArray[] 	=  $totalamount;
				//$setRepArray[] 	=  '';
				
				/*--------------Ink------------------*/
				$this->loadMOdel( 'InkOrderLocation' );
				$ink_envelope   = 0;
				$order_ink	    = $this->InkOrderLocation->find('first', array('conditions' => array('order_id' => $openOrderId,'split_order_id' => $openOrderId.'-1','status' => 'active')));
				
				if(count($order_ink) > 0 ){ 					 
					$setRepArray[] 	=  '<div style="color:green;font-size:14px;"><center>#2 Envelope Required.</center></div><br><img src="'.WWW_ROOT.'img/ink_xl2.jpg" height="130">';
				}
				//pr($setRepArray);
				/*--------------End of Ink------------------*/
				/*-----------------------ResponsiblePersonDetail---------------------*/
				$this->loadModel('ResponsiblePersonDetail');
				$person = [];  
				if($country != 'United Kingdom' ){
					$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
					foreach($skus as $k => $v){
						$x = explode('XS-', $v); 
						$_sku = 'S-'.$x[1];
						$getRes	= $this->ResponsiblePersonDetail->find('first', array('conditions' => array('sku' => $_sku, 'store_name' => $company)));
						 
						if(count($getRes) > 0){
							$person[$getRes['ResponsiblePersonDetail']['brand_name']] = $getRes['ResponsiblePersonDetail']['person_information'];
						}
						 
					}		
				}
				if(count($person) > 0){
 					$person_info = ''; $b = 0;
					foreach($person as $brand => $pinfo){
 						$b++;
						if($b > 1){$person_info .= '<br>';}
						$person_info .= '<br>'.nl2br($pinfo);
						
					}
 					$html 	.= '<div style="font-size:10px; border:1px solid; text-align:left;padding:5px;">This product is CE marked products in accordance with European health, safety, and environmental protection standards for products sold within the European Economic Area (EEA).<br> 
					We hereby providing details of authorised representative established in European union:<br> 
					<div width="100%" style="font-size:11px;">'. ($person_info).'</div></div>';
					 
 				}
				/*-------END---------------- */
				
				App::Import('Controller', 'NewInvoice'); 
				$newIn = new NewInvoiceController;
				$html = $newIn->getThermalInvoice($openOrderId);
				
				if( $company ==  'Marec' && $is_bulk_sku == 1)
				{	
					App::import('Controller', 'DynamicPicklist');
					$d_obj = new DynamicPicklistController();
					$get_country = array('marec_fr'=>'FR','marec_de'=>'DE','marec_it'=>'IT','marec_es'=>'ES','marec_uk'=>'UK');
					$subsource_lower  =  strtolower($subsource);
					$source_country  = $get_country[$subsource_lower];
					$html 	.= $d_obj->BulkSkuNote($source_country);			 
				}
				
				
				
		
				$imgPath = WWW_ROOT .'css/';
				
				$html2 = '<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				<meta content="" name="description"/>
				<meta content="" name="author"/><body>'.$html;
				if( $company ==  'Marec' && in_array(  'Mobile Accessories', $cat_name ))
				{
					//Commented on 30 Jan 2020 on the recmondation of Shashi
					/*$source	=	explode('_', $subsource)[1];
					App::import('Controller', 'Bulks');
					$bulk_obj = new BulksController();
					$html2 .= '<div style="page-break-before: always; margin-top: 60px; margin-left: 10px;">'.$bulk_obj->getMobileAssHtml( $source, $sub_can_name).'<div>';*/
				}
				$html2 .= '</body>';
				
				'</body>';
				
				$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
				//$html 		= 	$this->setReplaceValue( $setRepArray, $html2 );				 
				
				/*$html   	= str_replace("http://xsensys.com/",WWW_ROOT,$html );
				$html   	= str_replace("http://78007e9327.nxcli.net/",WWW_ROOT,$html );
				$html   	= str_replace("https://xsensys.com/",WWW_ROOT,$html );
				$html   	= str_replace("https://78007e9327.nxcli.net/",WWW_ROOT,$html );*/
			 	//echo $html2;
				//exit;
				//$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->load_html($html2, Configure::read('App.encoding'));
				$dompdf->render();
				//$dompdf->stream("OrderId-$splitOrderId.pdf");
				
				$imgPath = WWW_ROOT .'img/printPDF/'; 
				$path = Router::url('/', true).'img/printPDF/';
			echo	$name	=	'Packagin_Slip_'.$splitOrderId.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				exit;
				$serverPath  	= 	$path.$name ;
				$printerId	=	$this->getEpson();
				
				
				$sendData = array(
					'printerId' => $printerId,
					'title' => 'Packagin Slip:'.$splitOrderId,
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'					
				);
			}
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				$d = $Coreprinter->toPrint( $sendData );
				if(count($dhlSlipData) > 0 ){
					$d = $Coreprinter->toPrint( $dhlSlipData );
				}
				pr($d);
				exit;
			
		}
		
		public function getBarcode( $barcode = null )
		{
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 

			$barcodePath 		= 	WWW_ROOT .'img/product/barcodes/';   
			$barcodecorreosPath = 	WWW_ROOT .'img/orders/correos_barcode/';  
			
			$colorFront 		= 	new BCGColor(0, 0, 0);
			$colorBack 			= 	new BCGColor(255, 255, 255);
			
			// Barcode Part
			$orderbarcode=$barcode;
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->clearLabels( true );
			$code128->SetLabel(false);
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->parse($orderbarcode);
			// Drawing Part
			$imgOrder128=$orderbarcode;
			$imgOrder128path=$barcodePath.'/'.$imgOrder128.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			############ Routing Barcode #######
			
		}	
	
		
		public function getCompanyNameBySubsource( $subSource )
		{
			$this->loadModel( 'PackagingSlip' );
			$getCompanyName = $this->PackagingSlip->find('first', array('conditions' => array('PackagingSlip.store_name' => $subSource ) ) );
			if(!empty($getCompanyName))
			{
				return $getCompanyName['PackagingSlip']['company_name'];
			}
			
		}
		
		function setReplaceValue( $setRepArray, $htmlString )
		{
	
			$constArray = array();
			$constArray[] = '_ADDRESS1_';
			$constArray[] = '_ADDRESS2_';
			$constArray[] = '_ADDRESS3_';
			$constArray[] = '_TOWN_';
			$constArray[] = '_RESION_';
			$constArray[] = '_POSTCODE_';
			$constArray[] = '_COUNTRY_';
			$constArray[] = '_PHONE_';
			$constArray[] = '_ORDERNUMBER_';
			$constArray[] = '_COURIER_';
			$constArray[] = '_RECIVEDATE_';
			$constArray[] = '_ORDERSUMMARY_';
			$constArray[] = '_TOTALITEM_';
			$constArray[] = '_SUBTOTAL_';
			$constArray[] = '_IMAGE_';
			$constArray[] = '_POSTAGECOST_';
			$constArray[] = '_TAX_';
			$constArray[] = '_TOTALAMOUNT_';
			$constArray[] = '_ADDRESS_';
			$constArray[] = '_BARCODE_';
			$constArray[] = '_PAYMENTMETHOD_';
			$constArray[] = '_BARCODENUMBER_';
			$constArray[] = '_COMPANYNAME_';
			$constArray[] = '_RETURNADDRESS_';
			$constArray[] = '_LOGO_';
			$constArray[] = '_SPLITORDERID_';
			$constArray[] = '_PRODUCTINSTRUCTION_';
			$constArray[] =	'_PRODUCTBARCODEIMAGE_';
			$constArray[] =	'_TOTALCOST_';
			$constArray[] =	'_DYURL_';
			$constArray[] =	'_CE_';
			$constArray[] =	'_INK_';
			
			$getRep = $htmlString;
			$getRep = str_replace( $constArray[0],$setRepArray[0],$getRep );
			$getRep = str_replace( $constArray[1],$setRepArray[1],$getRep );
			$getRep = str_replace( $constArray[2],$setRepArray[2],$getRep );
			$getRep = str_replace( $constArray[3],$setRepArray[3],$getRep );
			$getRep = str_replace( $constArray[4],$setRepArray[4],$getRep );
			$getRep = str_replace( $constArray[5],$setRepArray[5],$getRep );
			$getRep = str_replace( $constArray[6],$setRepArray[6],$getRep );
			$getRep = str_replace( $constArray[7],$setRepArray[7],$getRep );
			$getRep = str_replace( $constArray[8],$setRepArray[8],$getRep );
			$getRep = str_replace( $constArray[9],$setRepArray[9],$getRep );
			$getRep = str_replace( $constArray[10],$setRepArray[10],$getRep );
			$getRep = str_replace( $constArray[11],$setRepArray[11],$getRep );
			$getRep = str_replace( $constArray[12],$setRepArray[12],$getRep );
			$getRep = str_replace( $constArray[13],$setRepArray[13],$getRep );
			$getRep = str_replace( $constArray[14],$setRepArray[14],$getRep );
			$getRep = str_replace( $constArray[15],$setRepArray[15],$getRep );
			$getRep = str_replace( $constArray[16],$setRepArray[16],$getRep );
			$getRep = str_replace( $constArray[17],$setRepArray[17],$getRep );
			$getRep = str_replace( $constArray[18],$setRepArray[18],$getRep );
			$getRep = str_replace( $constArray[19],$setRepArray[19],$getRep );
			$getRep = str_replace( $constArray[20],$setRepArray[20],$getRep );
			$getRep = str_replace( $constArray[21],$setRepArray[21],$getRep );
			$getRep = str_replace( $constArray[22],$setRepArray[22],$getRep );
			$getRep = str_replace( $constArray[23],$setRepArray[23],$getRep );
			$getRep = str_replace( $constArray[24],$setRepArray[24],$getRep );
			$getRep = str_replace( $constArray[25],$setRepArray[25],$getRep );
			$getRep = str_replace( $constArray[26],$setRepArray[26],$getRep );
			$getRep = str_replace( $constArray[27],$setRepArray[27],$getRep );
			$getRep = str_replace( $constArray[28],$setRepArray[28],$getRep );
			$getRep = str_replace( $constArray[29],$setRepArray[29],$getRep );
			$getRep = str_replace( $constArray[30],$setRepArray[30],$getRep );
			$getRep = str_replace( $constArray[31],$setRepArray[31],$getRep );
			return $getRep;
			
		}
		
		
		
		public function generatepdfdirect_old130416()
		{
			
				$this->loadModel( 'PackagingSlip' );
				$this->loadModel( 'MergeUpdate' );
				$this->loadModel( 'Product' );
				$id				=	$this->request->data['pkorderid'];
				$openOrderId	=	$this->request->data['openOrderId'];
				$splitOrderId	=	$this->request->data['splitOrderId'];
			
				$this->layout = '';
				$this->autoRender = false;
				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				
				$dompdf->set_paper(array(0, 0, 245, 450), 'portrait');
				$order	=	$this->getOpenOrderById( $openOrderId );
				
				
				$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
				$itemPrice		=	$getSplitOrder['MergeUpdate']['price'];
				$itemQuantity	=	$getSplitOrder['MergeUpdate']['quantity'];
				$BarcodeImage	=	$getSplitOrder['MergeUpdate']['order_barcode_image'];
				
				
				$k = '10';
				$companyname = '';
				$returnaddress = '';
				switch ($k) {
								case 0:
									echo "";
									break;
								case 1:
									echo "";
									break;
								case 2:
									echo "";
									break;
								default:
								   $companyname 	 =  'Jij Group';
								   $returnaddress 	 =  'Address Line 1 <br> Address Line 2 <br>Some Town Some Region 123 ABC <br>United Country';
							}
				
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$subsource			=		$order['general_info']->SubSource;
				
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$address			=		'';
				$address			.=		($company != '') ? $company.'<br>' : '' ; 
				$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
				$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
				$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
				$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
				$address			.=		($town != '') ? $town.'<br>' : '';
				$address			.=		($resion != '') ? $resion.'<br>' : '';
				$address			.=		($postcode != '') ? $postcode.'<br>' : '';
				$address			.=		($country != '' ) ? $country.'<br>' : '';
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				
				
				//$gethtml 	=	$this->PackagingSlip->find('all');
				//pr($gethtml);
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= $postcode;
				$setRepArray[] 					= $country;
				$setRepArray[] 					= $phone;
				$setRepArray[] 					= $ordernumber;
				$setRepArray[] 					= $courier;
				$setRepArray[] 					= $recivedate[0];
				$i = 1;
				$str = '';
				$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
				$totalGoods = 0;
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					foreach($newSkus as $newSku)
						{
							
							$getsku = 'S-'.$newSku[1];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title	=	$getOrderDetail['Product']['product_name'];
							$totalGoods = $totalGoods + $newSku[0];
							$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
									<td valign="top" class="rightborder bottomborder">'.substr($title, 0, 15 ).'</td>
									<td valign="top" class="center norightborder bottomborder">'.$newSku[0][0].'</td>';
									//<td valign="top" class="right rightborder bottomborder">'.$itemPrice.'</td>
									//<td valign="top" class="right bottomborder norightborder">'.$itemQuantity.'</td>
							$str .= '</tr>';
							$i++;
						}
						unset($newSkus);
				}
				
				$totalitem = $i - 1;
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totalGoods;
				$setRepArray[]	=	 $subtotal;
				$Path 			= 	'/wms/img/client/';
				$img			=	 '<img src=http://localhost/wms/img/client/demo.png height="50" width="50">';
				$setRepArray[]	=	 $img;
				$setRepArray[]	=	 $postagecost;
				$setRepArray[]	=	 $tax;
				$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
				//$setRepArray[]	=	 $totalamount;
				$setRepArray[]	=	 $itemPrice;
				$setRepArray[]	=	 $address;
				$barcodePath  	=  Router::url('/', true).'img/orders/barcode/';
				$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.'>';
				$barcodenum		=	explode('.', $barcode);
				
				$setRepArray[] 	=  $barcodeimg;
				$setRepArray[] 	=  $paymentmethod;
				$setRepArray[] 	=  $barcodenum[0];
				$setRepArray[] 	=  '';
				$gethtml 		=	$this->PackagingSlip->find('first', array( 'conditions' => array('PackagingSlip.name' => $subsource) ));
				$html 			=	$gethtml['PackagingSlip']['html'];
				$img 			=	$gethtml['PackagingSlip']['image_name'];
				$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
				$setRepArray[] 	=  $returnaddress;
				$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
				$setRepArray[] 	= $splitOrderId;
				
				$imgPath = WWW_ROOT .'css/';
				
				$html2 = '<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				<meta content="" name="description"/>
				<meta content="" name="author"/><body>'.$html.'</body>';
				
				$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
				$html 		= 	$this->setReplaceValue( $setRepArray, $html2 );
				//echo $html;
				//exit;
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				$dompdf->stream("OrderId-$splitOrderId.pdf");
				
				$imgPath = WWW_ROOT .'img/printPDF/'; 
				$path = Router::url('/', true).'img/printPDF/';
				$name	=	'Packagin_Slip_'.$splitOrderId.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				$serverPath  	= 	$path.$name ;
				$printerId	=	$this->getEpson();
				
				
				$sendData = array(
					'printerId' => $printerId,
					'title' => 'Now Print',
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'					
				);
				
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				$d = $Coreprinter->toPrint( $sendData );
				pr($d); exit;
			
		}
	 
		
		   
		public function generatepdfdirect_old()
		{
			
				$this->loadModel( 'PackagingSlip' );
				$this->loadModel( 'MergeUpdate' );
				$id				=	$this->request->data['pkorderid'];
				$openOrderId	=	$this->request->data['openOrderId'];
				$splitOrderId	=	$this->request->data['splitOrderId'];
			
				$this->layout = '';
				$this->autoRender = false;
				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				
				$dompdf->set_paper(array(0, 0, 245, 450), 'portrait');
				$order	=	$this->getOpenOrderById( $openOrderId );
				$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
				$itemPrice		=	$getSplitOrder['MergeUpdate']['price'];
				$itemQuantity	=	$getSplitOrder['MergeUpdate']['quantity'];
				
				
				$k = '10';
				$companyname = '';
				$returnaddress = '';
				switch ($k) {
								case 0:
									echo "";
									break;
								case 1:
									echo "";
									break;
								case 2:
									echo "";
									break;
								default:
								   $companyname 	 =  'Jij Group';
								   $returnaddress 	 =  'Address Line 1 <br> Address Line 2 <br>Some Town Some Region 123 ABC <br>United Country';
							}
				
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$subsource			=		$order['general_info']->SubSource;
				
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$address			=		'';
				$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
				$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
				$address			.=		(isset($town)) ? $town.'<br>' : '';
				$address			.=		(isset($resion)) ? $resion.'<br>' : '';
				$address			.=		(isset($postcode)) ? $postcode.'<br>' : '';
				$address			.=		(isset($country)) ? $country.'<br>' : '';
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				
				
				//$gethtml 	=	$this->PackagingSlip->find('all');
				//pr($gethtml);
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= $postcode;
				$setRepArray[] 					= $country;
				$setRepArray[] 					= $phone;
				$setRepArray[] 					= $ordernumber;
				$setRepArray[] 					= $courier;
				$setRepArray[] 					= $recivedate[0];
				$i = 1;
				$str = '';
				foreach($items as $item)
				{
					$str .= '<tr>
							<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
							<td valign="top" class="rightborder bottomborder">'.substr($item->Title, 0, 10 ).'</td>
							<td valign="top" class="center rightborder bottomborder">'.$itemQuantity.'</td>
							<td valign="top" class="right rightborder bottomborder">'.$itemPrice.'</td>
							<td valign="top" class="right bottomborder norightborder">'.$itemQuantity.'</td>
							</tr>';
					$i++;
				}
				$totalitem = $i - 1;
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totalitem;
				$setRepArray[]	=	 $subtotal;
				$Path 			= 	'/img/client/';
				$img			=	 '<img src=http://localhost/img/client/demo.png height="50" width="50">';
				$setRepArray[]	=	 $img;
				$setRepArray[]	=	 $postagecost;
				$setRepArray[]	=	 $tax;
				$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
				//$setRepArray[]	=	 $totalamount;
				$setRepArray[]	=	 $itemPrice;
				$setRepArray[]	=	 $address;
				$barcodePath  	=  Router::url('/', true).'img/orders/barcode/';
				$barcodeimg 	=  '<img src='.$barcodePath.$barcode.'>';
				$barcodenum		=	explode('.', $barcode);
				
				$setRepArray[] 	=  $barcodeimg;
				$setRepArray[] 	=  $paymentmethod;
				$setRepArray[] 	=  $barcodenum[0];
				$setRepArray[] 	=  $companyname;
				$setRepArray[] 	=  $returnaddress;
				$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/sfl.png height = 36 >';
				$setRepArray[] 	= $splitOrderId;
				
				$imgPath = WWW_ROOT .'css/';
				$gethtml 	=	$this->PackagingSlip->find('first', array( 'conditions' => array('PackagingSlip.name' => $subsource) ));
				$html 		=	$gethtml['PackagingSlip']['html'];
				$html2 = '<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				<meta content="" name="description"/>
				<meta content="" name="author"/><body>'.$html.'</body>';
				
				$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
				$html 		= 	$this->setReplaceValue( $setRepArray, $html2 );
				
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				$dompdf->stream("OrderId-$splitOrderId.pdf");
				
				$imgPath = WWW_ROOT .'img/printPDF/'; 
				$path = Router::url('/', true).'img/printPDF/';
				$name	=	'Packagin_Slip_'.$splitOrderId.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				$serverPath  	= 	$path.$name ;
				$printerId	=	$this->getEpson();
								
				$sendData = array(
					'printerId' => $printerId,
					'title' => 'Now Print',
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'					
				);
				
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				$d = $Coreprinter->toPrint( $sendData );
				pr($d); exit;
			
		}
		
		function setReplaceValue_old( $setRepArray, $htmlString )
		{
	
			$constArray = array();
			$constArray[] = '_ADDRESS1_';
			$constArray[] = '_ADDRESS2_';
			$constArray[] = '_ADDRESS3_';
			$constArray[] = '_TOWN_';
			$constArray[] = '_RESION_';
			$constArray[] = '_POSTCODE_';
			$constArray[] = '_COUNTRY_';
			$constArray[] = '_PHONE_';
			$constArray[] = '_ORDERNUMBER_';
			$constArray[] = '_COURIER_';
			$constArray[] = '_RECIVEDATE_';
			$constArray[] = '_ORDERSUMMARY_';
			$constArray[] = '_TOTALITEM_';
			$constArray[] = '_SUBTOTAL_';
			$constArray[] = '_IMAGE_';
			$constArray[] = '_POSTAGECOST_';
			$constArray[] = '_TAX_';
			$constArray[] = '_TOTALAMOUNT_';
			$constArray[] = '_ADDRESS_';
			$constArray[] = '_BARCODE_';
			$constArray[] = '_PAYMENTMETHOD_';
			$constArray[] = '_BARCODENUMBER_';
			$constArray[] = '_COMPANYNAME_';
			$constArray[] = '_RETURNADDRESS_';
			$constArray[] = '_LOGO_';
			$constArray[] = '_SPLITORDERID_';
			
			
			$getRep = $htmlString;
			$getRep = str_replace( $constArray[0],$setRepArray[0],$getRep );
			$getRep = str_replace( $constArray[1],$setRepArray[1],$getRep );
			$getRep = str_replace( $constArray[2],$setRepArray[2],$getRep );
			$getRep = str_replace( $constArray[3],$setRepArray[3],$getRep );
			$getRep = str_replace( $constArray[4],$setRepArray[4],$getRep );
			$getRep = str_replace( $constArray[5],$setRepArray[5],$getRep );
			$getRep = str_replace( $constArray[6],$setRepArray[6],$getRep );
			$getRep = str_replace( $constArray[7],$setRepArray[7],$getRep );
			$getRep = str_replace( $constArray[8],$setRepArray[8],$getRep );
			$getRep = str_replace( $constArray[9],$setRepArray[9],$getRep );
			$getRep = str_replace( $constArray[10],$setRepArray[10],$getRep );
			$getRep = str_replace( $constArray[11],$setRepArray[11],$getRep );
			$getRep = str_replace( $constArray[12],$setRepArray[12],$getRep );
			$getRep = str_replace( $constArray[13],$setRepArray[13],$getRep );
			$getRep = str_replace( $constArray[14],$setRepArray[14],$getRep );
			$getRep = str_replace( $constArray[15],$setRepArray[15],$getRep );
			$getRep = str_replace( $constArray[16],$setRepArray[16],$getRep );
			$getRep = str_replace( $constArray[17],$setRepArray[17],$getRep );
			$getRep = str_replace( $constArray[18],$setRepArray[18],$getRep );
			$getRep = str_replace( $constArray[19],$setRepArray[19],$getRep );
			$getRep = str_replace( $constArray[20],$setRepArray[20],$getRep );
			$getRep = str_replace( $constArray[21],$setRepArray[21],$getRep );
			$getRep = str_replace( $constArray[22],$setRepArray[22],$getRep );
			$getRep = str_replace( $constArray[23],$setRepArray[23],$getRep );
			$getRep = str_replace( $constArray[24],$setRepArray[24],$getRep );
			$getRep = str_replace( $constArray[25],$setRepArray[25],$getRep );
			return $getRep;
			
		}
		public function getOpenOrderById( $numOrderId = null )
			{
				
				$this->loadModel('OpenOrder');
				$order = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $numOrderId )));
				$data['id']					=	 $order['OpenOrder']['id'];
				$data['order_id']			=	 $order['OpenOrder']['order_id'];
				$data['num_order_id']		=	 $order['OpenOrder']['num_order_id'];
				$data['sub_source']		=	 $order['OpenOrder']['sub_source'];
				$data['general_info']		=	 unserialize($order['OpenOrder']['general_info']);
				$data['shipping_info']		=	 unserialize($order['OpenOrder']['shipping_info']);
				$data['customer_info']		=	 unserialize($order['OpenOrder']['customer_info']);
				$data['totals_info']		=	 unserialize($order['OpenOrder']['totals_info']);
				$data['folder_name']		=	 unserialize($order['OpenOrder']['folder_name']);
				$data['items']				=	 unserialize($order['OpenOrder']['items']);
				$data['assigned_service']	=	 $order['AssignService']['assigned_service'];
				$data['assign_barcode']		=	 $order['AssignService']['assign_barcode'];
				$data['manifest']			=	 $order['AssignService']['manifest'];
				$data['cn22']				=	 $order['AssignService']['cn22'];
				$data['courier']			=	 $order['AssignService']['courier'];
				$data['template_id']		=	 $order['OpenOrder']['template_id'];
				return $data;
				
			}
			
		
		
		public function getBrother()
		{
			$this->layout = '';
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name']; //getenv('COMPUTERNAME');
			$paramsEpson = array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name LIKE' => 'Brother%'
				)
			);
			
			//EPSONTM-T88V			
			$pcDetailEpson = json_decode(json_encode($this->PcStore->find('all', $paramsEpson)),0);			
 			$brotherT88 = $pcDetailEpson[0]->PcStore->printer_id;
			
			
		return $brotherT88;	
		}
		
		public function domeshippingnew()
		{
				
				$this->layout = '';
				$this->autoRender = false;
				$this->loadModel('Template');
				$this->loadModel('MergeUpdate');
				$this->loadModel('Product');
				
				$id				=	$this->request->data['pkorderid'];
				$openOrderId	=	$this->request->data['openOrderId'];
				$splitOrderId	=	$this->request->data['splitOrderId'];
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				$order			= $this->getOpenOrderById( $openOrderId );
			 	$Country 		= $order['customer_info']->Address->Country;
					
				if($splitOrderDetail['MergeUpdate']['brt'] == 1 ){
					$serverPath	= Router::url('/', true).'img/brt/label_'.$splitOrderId.'.pdf';					
					$printerId  = $this->getZebra();
					$sendData   = array(
						'printerId' => $printerId,
						'title' => 'Xsen BRT '.$splitOrderId,
						'contentType' => 'pdf_uri',
						'content' => $serverPath,
						'source' => 'Direct'					
					);
					
				}else{
				
					
					App::Import('Controller', 'NewInvoice'); 
					$newIn = new NewInvoiceController;
					$slip = $newIn->GenerateB2CEpson($openOrderId);	
 					
					require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
					spl_autoload_register('DOMPDF_autoload'); 
					$dompdf = new DOMPDF();	
				 
					//$html =	$htmlTemplate; 
					//$label 	= $this->getJpLabel(); 
					$cssPath = WWW_ROOT .'css/';
					 $html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
					 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
					 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
					 <meta content="" name="description"/>
					 <meta content="" name="author"/>
					 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
				     $html .= '<body>'.$slip.'</body>';
					//echo $html;exit;					
					$name	= 'Packagin_Label_'.$splitOrderId.'.pdf';					
				//	$dompdf->load_html(($html), Configure::read('App.encoding'));
					
					$dompdf->set_paper(array(0, 0, '288', '500' ), 'portrait');					
					$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
					$dompdf->render();
					 			
									
					$file_to_save = WWW_ROOT .'b2b_invoices/'.$name;					 
					//save the pdf file on the server
					file_put_contents($file_to_save, $dompdf->output());  
					
					file_put_contents(WWW_ROOT."logs/domeshippingnew-".date('dmy').".log", $this->request->data['pkorderid']."\t".$this->request->data['openOrderId']."\t".$this->request->data['splitOrderId']."\n",FILE_APPEND | LOCK_EX);
					
					$imgPath = WWW_ROOT .'b2b_invoices/'; 
					$path = Router::url('/', true).'b2b_invoices/';
					
					$serverPath  	= 	$path.$name ;
					$printerId		=	$this->getEpson();
					//$printerId		=	$this->getBrother();
					
					$sendData = array(
						'printerId' => $printerId,
						'title' => 'Packagin Label:'.$splitOrderId,
						'contentType' => 'pdf_uri',
						'content' => $serverPath,
						'source' => 'Direct'
					);
				
				}
				
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				$d = $Coreprinter->toPrint( $sendData );
				pr($d); 
				exit;
		}
	
		public function getCorreosBarcode( $postcode = null, $splitOrderID = null )
		{
			
			
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 

			$barcodePath 		= 	WWW_ROOT .'img/orders/barcode/';   
			$barcodecorreosPath = 	WWW_ROOT .'img/orders/correos_barcode/';  
			
			$colorFront 		= 	new BCGColor(0, 0, 0);
			$colorBack 			= 	new BCGColor(255, 255, 255);
			
			// Barcode Part
			$orderbarcode=$splitOrderID;
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->clearLabels();
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->parse($orderbarcode);
			// Drawing Part
			$imgOrder128=$orderbarcode;
			$imgOrder128path=$barcodePath.'/'.$imgOrder128.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			############ Routing Barcode #######
			//$destination = $countryCode;
			//$origin ="90019";
			//$routingcode=$destination.$origin;
			
			 
			 
			$routingcode = $this->getCorreosCalculatedBarcode($postcode);
			$routingcode128 = new BCGcode128();
			$routingcode128->setScale(2);
			$routingcode128->setThickness(40);
			$routingcode128->setForegroundColor($colorFront);
			$routingcode128->setBackgroundColor($colorBack);
			$routingcode128->parse(array(CODE128_B, $routingcode));
			//$routingcode128->parse($routingcode);
			// Drawing Part
			$routingimg =$routingcode;
			$routingdrawing128path=$barcodecorreosPath.'/'.$routingimg.".png";
			$routingdrawing128 = new BCGDrawing($routingdrawing128path, $colorBack);
			$routingdrawing128->setBarcode($routingcode128);
			$routingdrawing128->draw();
			$routingdrawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			return $routingcode;
		}
		
		public function getCorreosCalculatedBarcode($postcode = null)
		 {
			$sum_accii = 0;
			
			$code = 'ORDCP'.$postcode; 		
			for($i=0; $i < strlen($code); $i++){
			  $sum_accii += ord($code[$i]);		 
			}
			$remainder = fmod($sum_accii,23);
			$table = array('T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E');
			$control_character = $table[$remainder];
			return	$code.$control_character;			
		} 
		
		public function getCorriosBarcode_160418( $countryCode = null, $splitOrderID = null )
		{
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 

			$barcodePath 		= 	WWW_ROOT .'img/orders/barcode/';   
			$barcodecorreosPath = 	WWW_ROOT .'img/orders/correos_barcode/';  
			
			$colorFront 		= 	new BCGColor(0, 0, 0);
			$colorBack 			= 	new BCGColor(255, 255, 255);
			
			// Barcode Part
			$orderbarcode=$splitOrderID;
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->clearLabels();
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->parse($orderbarcode);
			// Drawing Part
			$imgOrder128=$orderbarcode;
			$imgOrder128path=$barcodePath.'/'.$imgOrder128.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			############ Routing Barcode #######
			$destination = $countryCode;
			$origin ="90019";
			$routingcode=$destination.$origin;
			$routingcode128 = new BCGcode128();
			$routingcode128->setScale(2);
			$routingcode128->setThickness(40);
			$routingcode128->setForegroundColor($colorFront);
			$routingcode128->setBackgroundColor($colorBack);
			$routingcode128->parse(array(CODE128_C, $routingcode));
			//$routingcode128->parse($routingcode);
			// Drawing Part
			$routingimg =$routingcode;
			$routingdrawing128path=$barcodecorreosPath.'/'.$routingimg.".png";
			$routingdrawing128 = new BCGDrawing($routingdrawing128path, $colorBack);
			$routingdrawing128->setBarcode($routingcode128);
			$routingdrawing128->draw();
			$routingdrawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
		}	
		public function getCountryAddress( $addressArray )
		{
			
			switch ($addressArray['country']) 
			{
			case "United States":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<div style="font-size:14px;"><b>'.ucwords($addressArray['company']).'</b><br>' : '<div style="font-size:14px;">' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords(strtolower($addressArray['fullname'])).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords(strtolower($addressArray['address1'])).'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords(strtolower($addressArray['address2'])).'<br>' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords(strtolower($addressArray['address3'])).'<br>' : '' ;
					
					$address			.=		($addressArray['town'] != '' )     ? ' '.strtoupper($addressArray['town']).' ' : '';
					$address			.=		($addressArray['resion'] != '' )   ? ucwords($addressArray['resion']).' ' : '';
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).'<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br>' : '';
					return $address;
					break;
			case "Canada":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<div style="font-size:14px;"><b>'.ucwords($addressArray['company']).'</b><br>' : '<div style="font-size:14px;">' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords($addressArray['fullname']).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1']).'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).'<br>' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']) : '' ;
					$address			.=		($addressArray['town'] != '' )     ? strtoupper($addressArray['town']).' ' : '';
					$address			.=		($addressArray['resion'] != '' )   ? strtoupper($addressArray['resion']).' ' : '';						
					
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).'<br>' : '';
					//$address			.=		($addressArray['postcode'] != '' ) ? '<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br></div>' : '';
					return $address;
					break;
			case "Germany":
			
					if(strtolower($addressArray['sub_source']) == 'costdropper'){
  					
						$address			 =		'';
						$address			.=		($addressArray['company'] != '')   ? '<b>'.ucwords($addressArray['company']).'</b><br>' : '' ; 
						$address			.=		($addressArray['fullname'] != '')  ? ucwords(strtolower($addressArray['fullname'])).'<br>' : '' ; 
						
						if($addressArray['address1'] != '' && $addressArray['address2'] != ''){
							$address			.=		ucwords($addressArray['address2']).' ' . ucwords($addressArray['address1']) ; 
						}
 						if($addressArray['address1'] == '' && $addressArray['address2'] != ''){						
							$address			.=		ucwords($addressArray['address2']) ; 					
						}
						if($addressArray['address1'] != '' && $addressArray['address2'] == ''){						
							$address			.=		ucwords($addressArray['address1']) ; 					
						}
						$address 			.= 		'<br>';  				 
						$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).' ' : '';
						$address			.=		($addressArray['town'] != '' )     ? strtoupper($addressArray['town']).'<br>' : '';
						$address			.=		'DEUTSCHLAND';//($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br>' : '';
 				
					}else{
							$address			 =		'';
							$address			.=		($addressArray['company'] != '')   ? '<b>'.ucwords($addressArray['company']).'</b><br>' : '' ; 
							$address			.=		($addressArray['fullname'] != '')  ? ucwords(strtolower($addressArray['fullname'])).'<br>' : '' ; 
							$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1']).'<br>' : '' ; 
							
							if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
								$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
								$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']).'<br>' : '' ;						
							}
							
							if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
								$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).'<br>' : '' ;
							}
							
							if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
								$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']).'<br>' : '' ;
							}
							
							//$address			.=		($addressArray['resion'] != '' )   ? ucwords(strtoupper($addressArray['resion'])).'<br>' : '';
							$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).' ' : '';
							$address			.=		($addressArray['town'] != '' )     ? strtoupper($addressArray['town']).'<br>' : '';
							$address			.=		'DEUTSCHLAND';//($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br>' : '';
						}
					return $address;
					break;
			case "Italy":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<b>'.strtoupper($addressArray['company']).'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '')  ? strtoupper(strtolower($addressArray['fullname'])).'<br>' : '' ; 
				     $address			.=		($addressArray['address1'] != '' ) ? strtoupper($addressArray['address1']).'<br>' : '' ; 
					
					 if($addressArray['address2'] != '' && $addressArray['address3'] != '') 
					 {
						$address			.=		($addressArray['address2'] != '' )   ? strtoupper($addressArray['address2']).'<br>' : '';
						$address			.=		($addressArray['address3'] != '' )   ? strtoupper($addressArray['address3']).'<br>' : '';
					}
					else if($addressArray['address2'] != '' && $addressArray['address3'] == '')
					{
						
						$address			.=		($addressArray['address2'] != '' )   ? strtoupper($addressArray['address2']).'<br>' : '';
						
					}
					else if($addressArray['address2'] == '' && $addressArray['address3'] != '')
					{
						
						$address			.=		($addressArray['address3'] != '' )   ? strtoupper($addressArray['address3']).'<br>' : '';
						
					}
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).' ' : ' ';
					$address			.=		($addressArray['town'] != '' )     ? strtoupper($addressArray['town']).' ' : ' ';
					if($addressArray['resion'] != '' )
					{
						
						$address			.=		($addressArray['resion'] != '' )   ? strtoupper($addressArray['resion']) : '';
					}
					else
					{
						$address			.=		"";
					}	
					
					
					$address			.=		($addressArray['country'] != '' )  ? "<br>".strtoupper($addressArray['country']).'<br>' : '';
					return $address;
					break;
			case "France":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<b>'.ucwords($addressArray['company']).'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords(strtolower($addressArray['fullname'])).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? strtoupper($addressArray['address1']).'<br>' : '' ; 
					
					if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']).'<br>' : '' ;						
					}
					
					if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).'<br>' : '' ;
					}
					
					if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']).'<br>' : '' ;
					}
					
					$address			.=		($addressArray['resion'] != '' )   ? strtoupper($addressArray['resion']).'<br>' : '';
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).' ' : '';
					$address			.=		($addressArray['town'] != '' )     ? strtoupper($addressArray['town']).'<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br>' : '';
					return $address;
					break;
			case "Spain":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<b>'.ucwords($addressArray['company']).'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords($addressArray['fullname']).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1']).'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).'<br>' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords($addressArray['address3']).'<br>' : '' ;
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']) : '';					
					$address			.=		($addressArray['town'] != '' )     ? ' '.strtoupper($addressArray['town']) : '';
					$address			.=		($addressArray['resion'] != '' )   ? '<br> '. strtoupper($addressArray['resion']) : '';					
					$address			.=		($addressArray['postcode'] != '' ) ? '<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? $addressArray['country'].'<br>' : '';
					return $address;
					break;
			case "Denmark":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? '<b>'.$addressArray['company'].'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '') 	? $addressArray['fullname'].'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1'].'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3'].'<br>' : '' ;
					
					$address			.=		($addressArray['postcode'] != '' ) 	? $addressArray['postcode'].' ' : '';
					$address			.=		($addressArray['town'] != '' ) 		? $addressArray['town'].'<br>' : '';
					$address			.=		($addressArray['resion'] != '' ) 	? $addressArray['resion'].'<br>' : '';
					$address			.=		($addressArray['country'] != '' ) 	? $addressArray['country'].'<br>' : '';
					return $address;
					break;
			case "Austria":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? '<b>'.$addressArray['company'].'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '') 	? $addressArray['fullname'].'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1'].'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3'].'<br>' : '' ;
					$address			.=		($addressArray['resion'] != '' ) 	? $addressArray['resion'].'<br>' : '';
					
					$address			.=		($addressArray['postcode'] != '' ) 	? strtoupper($addressArray['postcode']).' ' : '';
					$address			.=		($addressArray['town'] != '' ) 		? strtoupper($addressArray['town']).'<br>' : '';
					
					$address			.=		($addressArray['country'] != '' ) 	? strtoupper($addressArray['country']).'<br>' : '';
					return $address;
					break;
			default:
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? '<b>'.$addressArray['company'].'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '') 	? $addressArray['fullname'].'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1'].'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3'].'<br>' : '' ;
					$address			.=		($addressArray['town'] != '' ) 		? $addressArray['town'].'<br>' : '';
					$address			.=		($addressArray['resion'] != '' ) 	? $addressArray['resion'].'<br>' : '';
					$address			.=		($addressArray['postcode'] != '' ) 	? $addressArray['postcode'].'<br>' : '';
					$address			.=		($addressArray['country'] != '' ) 	? $addressArray['country'].'<br>' : '';
					return $address;
					break;
			}
			exit;
		}
		
		
		
		function setReplaceValueLabel( $setRepArray, $htmlString )
		{
	
			$constArray = array();
			$constArray[] = '_ADDRESS1_';
			$constArray[] = '_ADDRESS2_';
			$constArray[] = '_ADDRESS3_';
			$constArray[] = '_TOWN_';
			$constArray[] = '_RESION_';
			$constArray[] = '_ADDRESS_';
			$constArray[] = '_COUNTRY_';
			$constArray[] = '_BARCODEIMAGE_';
			$constArray[] = '_BARCODENUMBER_';
			$constArray[] = '_ORDERDETAIL_';
			$constArray[] = '_TAX_';
			$constArray[] = '_DATE_';
			$constArray[] = '_CURRENTDATE_';
			$constArray[] = '_LOGO_';
			$constArray[] = '';
			$constArray[] = '_SIGNATURE_';
			$constArray[] = '_SPLITORDERID_';
			$constArray[] = '_COUNTRYCODE_';
			$constArray[] = '_TOTALWEIGHT_';
			$constArray[] = '_TOTALVALUE_';
			$constArray[] = '_TRACKINGNUMBER_';
			$constArray[] = '_SUBSOURCE_';
			$constArray[] = '_CARTAS_';
			$constArray[] = '_REGBARCODENUMBER_';
			$constArray[] = '_CORRIOSBARCODE_';
			$constArray[] = '_PHONENUMBER_';
			$constArray[] = '_RETURNADD_';
			$constArray[] = '_DYURL_';
			$constArray[] = '_STORELOGO_';
			
			$getRep = $htmlString;
			$getRep = str_replace( $constArray[0],$setRepArray[0],$getRep );
			$getRep = str_replace( $constArray[1],$setRepArray[1],$getRep );
			$getRep = str_replace( $constArray[2],$setRepArray[2],$getRep );
			$getRep = str_replace( $constArray[3],$setRepArray[3],$getRep );
			$getRep = str_replace( $constArray[4],$setRepArray[4],$getRep );
			$getRep = str_replace( $constArray[5],$setRepArray[5],$getRep );
			$getRep = str_replace( $constArray[6],$setRepArray[6],$getRep );
			$getRep = str_replace( $constArray[7],$setRepArray[7],$getRep );
			$getRep = str_replace( $constArray[8],$setRepArray[8],$getRep );
			$getRep = str_replace( $constArray[9],$setRepArray[9],$getRep );
			$getRep = str_replace( $constArray[10],$setRepArray[10],$getRep );
			$getRep = str_replace( $constArray[11],$setRepArray[11],$getRep );
			$getRep = str_replace( $constArray[12],$setRepArray[12],$getRep );
			$getRep = str_replace( $constArray[13],$setRepArray[13],$getRep );
			$getRep = str_replace( $constArray[14],$setRepArray[14],$getRep );
			$getRep = str_replace( $constArray[15],$setRepArray[15],$getRep );
			$getRep = str_replace( $constArray[16],$setRepArray[16],$getRep );
			$getRep = str_replace( $constArray[17],$setRepArray[17],$getRep );
			$getRep = str_replace( $constArray[18],$setRepArray[18],$getRep );
			$getRep = str_replace( $constArray[19],$setRepArray[19],$getRep );
			$getRep = str_replace( $constArray[20],$setRepArray[20],$getRep );
			$getRep = str_replace( $constArray[21],$setRepArray[21],$getRep );
			$getRep = str_replace( $constArray[22],$setRepArray[22],$getRep );
			$getRep = str_replace( $constArray[23],$setRepArray[23],$getRep );
			$getRep = str_replace( $constArray[24],$setRepArray[24],$getRep );
			$getRep = str_replace( $constArray[25],$setRepArray[25],$getRep );
			$getRep = str_replace( $constArray[26],$setRepArray[26],$getRep );
			$getRep = str_replace( $constArray[27],$setRepArray[27],$getRep );
			$getRep = str_replace( $constArray[28],$setRepArray[28],$getRep );
			return $getRep;
			
		}
		
  
		
		public function getZebra()
		{
			$this->layout = '';
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );						
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name']; //getenv('COMPUTERNAME');
			$paramsZDesigner = array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name' => 'ZDesigner'
				)
			);
			//ZDesigner
			 
			$pcDetailZDesigner = json_decode(json_encode($this->PcStore->find('all', $paramsZDesigner)),0);						
			$zDesignerPrinter = $pcDetailZDesigner[0]->PcStore->printer_id;
		
		return $zDesignerPrinter;	
		}
		
		 
		public function getEpson()
		{
			$this->layout = '';
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name']; //getenv('COMPUTERNAME');
			$paramsEpson = array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name LIKE' => 'EPSONTM-T88V%'
				)
			);
			
			//EPSONTM-T88V			
			$pcDetailEpson = json_decode(json_encode($this->PcStore->find('all', $paramsEpson)),0);			
			$epsonT88 = $pcDetailEpson[0]->PcStore->printer_id;
			
		return $epsonT88;	
		}	
		
		public function processorder_new_040216()
		{
			$this->layout = '';
			$this->autoRander = false;
		
			$this->loadModel('OpenOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('ProcessOrder');
		
			$id 			=  $this->request->data['id'];
			$userid 		=  $this->request->data['userID'];
			$openOrderId 	=  $this->request->data['openOrderId'];
			$splitOrderId 	=  $this->request->data['splitOrderId'];
			
			/* update the status after process */
			$result	=	$this->MergeUpdate->updateAll(array('MergeUpdate.status' => '1', 'MergeUpdate.user_id' => $userid), array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId));
			
			/* Update Sorting status for sorting operator */
			App::import('Controller', 'Cronjobs');
			$cronjobtModel = new CronjobsController();
			$cronjobtModel->call_service_counter();
		
			/* update the status after process */
			$result	=	$this->MergeUpdate->updateAll(array('MergeUpdate.manifest_status' => '2', 'MergeUpdate.user_id' => $userid), array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId));
			$this->OpenOrder->updateAll(array('OpenOrder.status' => '1'), array('OpenOrder.num_order_id' => $openOrderId));
			
			if($result)
			{
				$getmergeOrders	=	$this->MergeUpdate->find( 'all', array('conditions' => array( 'MergeUpdate.order_id' =>  $openOrderId )) );
				$checkStatus = 0;
				$checkNonProcessStatus = 0;
					foreach( $getmergeOrders as $getmergeOrder )
					{
						if( $getmergeOrder['MergeUpdate']['status'] == 0 )
						{
							$checkNonProcessStatus++;
						}
						else
						{
							$checkStatus++;
						}
					}
					if($checkNonProcessStatus == 0 && $checkStatus > 0)
					{
						App::import('Vendor', 'linnwork/api/Auth');
						App::import('Vendor', 'linnwork/api/Factory');
						App::import('Vendor', 'linnwork/api/Orders');
				
						$username	=	Configure::read('linnwork_api_username');
						$password	=	Configure::read('linnwork_api_password');
					
						$multi = AuthMethods::Multilogin($username, $password);
					
						$auth = AuthMethods::Authorize($username, $password, $multi[0]->Id);	

						$token 			= 	$auth->Token;	
						$server 		= 	$auth->Server;
						$scanPerformed 	= 	true;
						
						$getOpenOrder	=	$this->OpenOrder->find( 'first', array('conditions' => array('OpenOrder.num_order_id' => $openOrderId )) );
						$data['ProcessOrder']['linn_fetch_orders']		=	$getOpenOrder['OpenOrder']['linn_fetch_orders'];
						$data['ProcessOrder']['sorted_scanned']			=	$getOpenOrder['OpenOrder']['sorted_scanned'];
						$data['ProcessOrder']['destination']			=	$getOpenOrder['OpenOrder']['destination'];
						$data['ProcessOrder']['order_id']				=	$getOpenOrder['OpenOrder']['order_id'];
						$data['ProcessOrder']['num_order_id']			=	$getOpenOrder['OpenOrder']['num_order_id'];
						$data['ProcessOrder']['general_info']			=	$getOpenOrder['OpenOrder']['general_info'];
						$data['ProcessOrder']['shipping_info']			=	$getOpenOrder['OpenOrder']['shipping_info'];
						$data['ProcessOrder']['customer_info']			=	$getOpenOrder['OpenOrder']['customer_info'];
						$data['ProcessOrder']['totals_info']			=	$getOpenOrder['OpenOrder']['totals_info'];
						$data['ProcessOrder']['folder_name']			=	$getOpenOrder['OpenOrder']['folder_name'];
						$data['ProcessOrder']['items']					=	$getOpenOrder['OpenOrder']['items'];
						$data['ProcessOrder']['service_name']			=	$getOpenOrder['OpenOrder']['service_name'];
						$data['ProcessOrder']['service_provider']		=	$getOpenOrder['OpenOrder']['service_provider'];
						$data['ProcessOrder']['service_code']			=	$getOpenOrder['OpenOrder']['service_code'];
						$data['ProcessOrder']['scanning_qty']			=	$getOpenOrder['OpenOrder']['scanning_qty'];
						$data['ProcessOrder']['status']					=	$getOpenOrder['OpenOrder']['status'];
						$data['ProcessOrder']['manifest']				=	$getOpenOrder['OpenOrder']['manifest'];
						$data['ProcessOrder']['cn22']					=	$getOpenOrder['OpenOrder']['cn22'];
						$data['ProcessOrder']['track_id']				=	$getOpenOrder['OpenOrder']['track_id'];
						$data['ProcessOrder']['error_code']				=	$getOpenOrder['OpenOrder']['error_code'];
						$data['ProcessOrder']['template_id']			=	$getOpenOrder['OpenOrder']['template_id'];
						$data['ProcessOrder']['user_id']				=	$userid;
						$data['ProcessOrder']['product_type']			=	$getOpenOrder['OpenOrder']['product_type'];
						$data['ProcessOrder']['product_defined_skus']	=	$getOpenOrder['OpenOrder']['product_defined_skus'];
						
						$this->ProcessOrder->saveAll($data);
						$result = OrdersMethods::ProcessOrder($orderId,$scanPerformed,$token, $server);	
						
						echo "1";
						exit;
					}
					else
					{
						echo "2";
						exit;
					}
			}
			else
			{
				echo "3";
				exit;
			}
		}
		
		
		public function processorder()
		{
			$this->layout = '';
			$this->autoRander = false;
			/*$this->loadModel('Product');*/
			$this->loadModel('OpenOrder');
			$this->loadModel('MergeUpdate');
			$id 			=  $this->request->data['id'];
			$userid 		=  $this->request->data['userID'];
			$openOrderId 	=  $this->request->data['openOrderId'];
			$splitOrderId 	=  $this->request->data['splitOrderId'];
			
			
			
			//echo "In ";
			/* Update Sorting status for sorting operator */
			App::import('Controller', 'Cronjobs');
			$cronjobtModel = new CronjobsController();
			$cronjobtModel->call_service_counter( $splitOrderId );
			
			//echo "Out ";
			/* update the status after process */
			
			/* update the status after process */
			$console = 'normal_console';
			$result	=	$this->MergeUpdate->updateAll(array('MergeUpdate.status' => '1', 'MergeUpdate.user_id' => $userid, 'MergeUpdate.dispatch_console' => "'".$console."'",'MergeUpdate.manifest_status' => '2'), array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId));
			
			/*$result	=	$this->MergeUpdate->updateAll(array('MergeUpdate.manifest_status' => '2', 'MergeUpdate.user_id' => $userid,
			'MergeUpdate.process_date' => "'".date('Y-m-d H:i:s')."'"), array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId));*/
			
			
			if($result)
			{
				$getmergeOrders	=	$this->MergeUpdate->find( 'all', array('conditions' => array( 'MergeUpdate.order_id' =>  $openOrderId ),'fields'=>['status']) );
				
				 
				$checkStatus = 0;
				$checkNonProcessStatus = 0;
					foreach( $getmergeOrders as $getmergeOrder )
					{
						if( $getmergeOrder['MergeUpdate']['status'] == 0 )
						{
							$checkNonProcessStatus++;
						}
						else
						{
							$checkStatus++;
						}
					}
					if($checkNonProcessStatus == 0 && $checkStatus > 0)
					{
						/*App::import('Vendor', 'linnwork/api/Auth');
						App::import('Vendor', 'linnwork/api/Factory');
						App::import('Vendor', 'linnwork/api/Orders');
				
						$username	=	Configure::read('linnwork_api_username');
						$password	=	Configure::read('linnwork_api_password');
					
						$multi = AuthMethods::Multilogin($username, $password);
					
						$auth = AuthMethods::Authorize($username, $password, $multi[0]->Id);	

						$token 			= 	$auth->Token;	
						$server 		= 	$auth->Server;
						$scanPerformed 	= 	true;
						$orderId		=	$this->request->data['id'];
						$result = OrdersMethods::ProcessOrder($orderId,$scanPerformed,$token, $server);	
						*/
					//	$this->OpenOrder->updateAll(array('OpenOrder.status' => '1'), array('OpenOrder.num_order_id' => $openOrderId));
						$this->OpenOrder->updateAll(array('OpenOrder.status' => '1','OpenOrder.process_date' => "'".date('Y-m-d H:i:s')."'" ), array('OpenOrder.num_order_id' => $openOrderId));
						
						echo "1";
						exit;
					}
					else
					{
						echo "2";
						exit;
					}
			}
			else
			{
				echo "3";
				exit;
			}
		}
		public function getProductDetail( $sku =null )
		{
			$productDetail	=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
			return $productDetail;
		}
		
		 public function ShowCustomers()
		 {
				$this->layout = 'index';
				$this->loadModel('Customer');
			 
				$perPage	=	 Configure::read( 'perpage' );
			
				$this->paginate = array('limit' => $perPage	);
				$customers = $this->paginate('Customer');
				$this->set( 'customers' , $customers);
			
				/*$this->layout = 'index';
				$this->loadModel('Customer');
				$customers	=	$this->Customer->find('all');
				$this->set( 'customers' , $customers);*/
		 }
		 public function checkInventery( $sku = null )
		 {
			 $this->layout = '';
			 $this->autoRender = false;
			 $this->loadModel('Product');
			 $checkinventery	=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku )));
			 if(!empty($checkinventery) )
			 {
				 return '1';
			 }
			 else
			 {
				 return '2';
			 }
		 }
		 public function getAllOrderItem( $id = null )
		 {
			 $this->loadModel( 'OrderItem' );
			 //$orderItem	=	$this->OrderItem->find('all', array('conditions' => array('OrderItem.order_id' => $id) , 'group' => array( 'OrderItem.sku' )));			 			 
			 $orderItem	=	$this->OrderItem->find('all', array('conditions' => array('OrderItem.order_id' => $id)));			 			 
			 
			 /*if( count( $orderItem1 ) > 0 )
			 {
				 //Means order_id, no splitting
				 return $orderItem1;
			 }
			 else
			 {
				 //Means order_id, splitting
				 return $orderItem2;
			 }*/
			 
		 return $orderItem;	 
		 }
		 public function getAllMergeUpdateItem( $id )
		 {
			 $this->loadModel( 'MergeUpdate' );
			 $orderItem	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.order_id' => $id)));			 			 
			 return $orderItem;	 
			 
		 }
		 
		 public function changeCurrency( $total = null, $simple = null )
		   {
			   $this->loadModel( 'CurrencyExchange' );
			   $currency	=	$this->CurrencyExchange->find('first', array('order' => array('CurrencyExchange.date DESC')));
			   $rate	=	$currency['CurrencyExchange']['rate'];
			   if($simple == 'GBP')
			   {
				 $total = (1/$rate) * $total;
			   }
			     return $total; 
				
		   }
		   
		    public function checkPickList( $id = null )
			   {
				   $this->loadModel( 'OrderItem' );
				   $this->loadModel( 'MergeUpdate' );
				   $detail	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.product_order_id_identify' => $id)));
				   return $detail['MergeUpdate']['pick_list_status'];
			   }	
			   

		    public function getorderItem( $id )
			   {
				   $this->loadModel( 'OrderItem' );
				   $detail	=	$this->OrderItem->find('all', array('conditions' => array('OrderItem.order_id' => $id)));
				   return  $detail;
			   }
			public function getTitle( $sku )
			{
				$this->loadModel( 'Product' );
				$skus	=	 explode( ',', $sku);
				     foreach($skus as $sku)
					   {
						   $newSku			=	explode( 'XS-', $sku);
						   $sku 			=	'S-'.$newSku[1];
						   $product			=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
						   $title[] 			= 	$product['Product']['product_name'];
					   }
				return $title;
			}
			
			
			
			public function cancelOrderPopup(  )
			{
				$this->layout = "";
                $this->autoRander = false;
                $this->loadModel('OpenOrder');
                $orderId	=	$this->request->data['orderid'];
                
                $getdetails	=	$this->OpenOrder->find( 'all', array('conditions'=>array('OpenOrder.num_order_id' => $orderId)));
				$i = 0;
				
				foreach($getdetails as $getdetail)
				{
					$id 			= 	$getdetail['OpenOrder']['num_order_id'];								
					$itemDetails	=	$this->getBundleOrderList( $id );						
					
					if( count($itemDetails) > 0 )									
					{
						$results[$i]['NumOrderId']		=	 $getdetail['OpenOrder']['num_order_id'];
						$results[$i]['OrderId']			=	 $getdetail['OpenOrder']['order_id'];
						$results[$i]['Items'] =	$itemDetails;				
						$results[$i]['GeneralInfo']		=	 unserialize($getdetail['OpenOrder']['general_info']);
						$results[$i]['ShippingInfo']	=	 unserialize($getdetail['OpenOrder']['shipping_info']);
						$results[$i]['CustomerInfo']	=	 unserialize($getdetail['OpenOrder']['customer_info']);
						$results[$i]['TotalsInfo']		=	 unserialize($getdetail['OpenOrder']['totals_info']);
						$results[$i]['FolderName']		=	 unserialize($getdetail['OpenOrder']['folder_name']);
						//$results[$i]['Items']			=	 unserialize($getdetail['OpenOrder']['items']);
					}
					else
					{
						$hiddenArray[] = $id;
					}				
				$i++;
				}
				if( isset($results)  )
				{
					$results	=	json_decode(json_encode($results),0);
					$this->set('results', $results);
				}
			    $this->render('cancel_order_popup');
			}
			
			public function assignServicePopup()
			{
				$this->layout = "";
                $this->autoRander = false;
				$this->loadModel( 'MergeUpdate' );
				$splitOrderId	=	$this->request->data['orderid'];
				$param = array( 
						'conditions' => array(
							'MergeUpdate.product_order_id_identify' => $splitOrderId 
							) 
						);
				$popupOrders = json_decode(json_encode($this->MergeUpdate->find('all', $param )),0);      
				$this->set('popupOrders', $popupOrders );
			    $this->render('assign_service_popup');
			}
			
			public function assignPackagingSlipPopup()
			{
				$this->layout = "";
                $this->autoRander = false;
				$this->loadModel( 'Store' );
				$this->loadModel( 'Template' );
				$splitOrderId	=	explode('###',$this->request->data['orderid']);
				
				$param = array( 
						'conditions' => array(
							'Store.status' => '1'
							),
						'fields' => array('Store.id','Store.store_name') 
						);
				$popupOrders = $this->Store->find('list', $param );
				$this->set('popupOrders', $popupOrders );
				$this->set('splitOrderId', $splitOrderId[1] );
				$this->set('mergeUpdateOrderId', $splitOrderId[0] );
				
				$getTemplates	=	$this->Template->find('all', array('fields' => array( 'Template.id, Template.label_name, Template.location_name' )));
				foreach( $getTemplates as $getTemplateValue )
				{
					$data[ $getTemplateValue['Template']['id'] ] = $getTemplateValue['Template']['label_name'].'('. $getTemplateValue['Template']['location_name'] .')';
				}
				$getTemplates	=	$data;
				
				$this->set('getTemplate', $getTemplates);
			    $this->render('assign_packaging_popup');
			}
			
			public function getLocation( $orderID = null )
			{
				$this->loadModel( 'MergeUpdate' );
				$this->loadModel( 'BinLocation' );
				$getmegreupdateDetails  =  $this->MergeUpdate->find( 'all', array( 'conditions' => array( 'MergeUpdate.order_id' => $orderID ) ) ); 
				foreach( $getmegreupdateDetails as $getmegreupdateDetail )
				{
					$barcodes	=	$getmegreupdateDetail['MergeUpdate']['barcode'];
				}
				 
				$explodeBarcodes = 		explode( ',', $barcodes );
				$i = 0;
				foreach( $explodeBarcodes as $explodeBarcode )
				{
					$getBinLocations			=	$this->BinLocation->find( 'all', array( 'conditions' => array( 'BinLocation.barcode' =>  $explodeBarcode ) ) );
					$j = 0;
					foreach( $getBinLocations as $getBinLocation )
					{
						$data[$i][$j]['id']				=	$getBinLocation['BinLocation']['id'];
						$data[$i][$j]['bin_location']	=	$getBinLocation['BinLocation']['bin_location'];
						$data[$i][$j]['barcode']		=	$getBinLocation['BinLocation']['barcode'];
						$data[$i][$j]['quantity']		=	$getBinLocation['BinLocation']['stock_by_location'];
						$j++;
					}
					$i++;
				}
				return $data;
				exit;
			}
			
			
			public function getOpenOrderSearchResult()
			{
				
				$this->layout = "";
                $this->autoRander = false;
                $this->loadModel('OpenOrder');
                //$orderId	=	explode( '-', trim($this->request->data['searchKey']));
                /*$getdetails	=	$this->OpenOrder->find( 'all', array(
					'conditions'=>array(
						'OpenOrder.num_order_id' => $orderId[0], 'OpenOrder.status' => '0'
						)
					)
				);*/
				
				$searchKeyNote = $this->request->data['searchKey'];
				$getdetails	=	$this->OpenOrder->find( 'all', array(
					'conditions' => array(						
						'OR' => array(
								array( 'OpenOrder.num_order_id' => $searchKeyNote ),
								array( 'OpenOrder.general_info like ' => "%{$searchKeyNote}%" )	
							)
						)
					)
				);
				
				$i = 0;
				if( count($getdetails) > 0 )
				{
					foreach($getdetails as $getdetail)
					{
						$id 			= 	$getdetail['OpenOrder']['num_order_id'];								
						$itemDetails	=	$this->getBundleOrderList( $id );						
						
						if( count($itemDetails) > 0 )									
						{
							$results[$i]['NumOrderId']		=	 $getdetail['OpenOrder']['num_order_id'];
							$results[$i]['OrderId']			=	 $getdetail['OpenOrder']['order_id'];
							$results[$i]['Items'] 			=	 $itemDetails;				
							$results[$i]['GeneralInfo']		=	 unserialize($getdetail['OpenOrder']['general_info']);
							$results[$i]['ShippingInfo']	=	 unserialize($getdetail['OpenOrder']['shipping_info']);
							$results[$i]['CustomerInfo']	=	 unserialize($getdetail['OpenOrder']['customer_info']);
							$results[$i]['TotalsInfo']		=	 unserialize($getdetail['OpenOrder']['totals_info']);
							$results[$i]['FolderName']		=	 unserialize($getdetail['OpenOrder']['folder_name']);
						}
						else
						{
							$hiddenArray[] = $id;
							if( $getdetail['OpenOrder']['status'] == 2 )
							{
								echo "2";
								exit;
							}
							
						}				
					$i++;
					}
					if( isset($results)  )
					{
						$results	=	json_decode(json_encode($results),0);
						$this->set('results', $results);
					}
					$this->render('open_order_search_result');
				}
				else
				{
					echo "1";
					exit;
				}
		}
		
		//Global Search
		public function searchAnyOrderInList()
		{
			
			$this->layout = "";
			$this->autoRander = false;
			$this->loadModel('OpenOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('OrderNote');
			$this->loadModel('OrderLocation');
			
			
			$searchKeyNote = trim($this->request->data['searchKey']);
			/*if($this->GlobalBarcode->checkCostBreakerOrders($searchKeyNote) > 0){
			    echo "1";
				exit;
			}*/
			 
			$getdetails	=	$this->OpenOrder->find( 'all', array(
				'conditions' => array(
						'OpenOrder.num_order_id' => $searchKeyNote
					)
				)
			);
			
			
			
			$oversell_items = '';
					
			$oversellItems = $this->OrderLocation->find('all', array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.available_qty_bin' => '0'),'fields' => array('OrderLocation.order_id'),'order' => 'bin_location ASC') );	
			if(count($oversellItems) > 0)
			{
				foreach($oversellItems as $v){
					$oversell_items[$v['OrderLocation']['order_id']] = $v['OrderLocation']['order_id'];
				}
			}
			$this->set('oversell_items', $oversell_items);
			
			
			if( count($getdetails) == 0 )
			{
				$getdetails	=	$this->OpenOrder->find( 'all', array(
					'conditions' => array(
							'OpenOrder.amazon_order_id like ' => "{$searchKeyNote}%"
						)
					)
				);
			}
			if( count($getdetails) == 0 )
			{
				
				$getdetails	=	$this->OpenOrder->find( 'all', array(
					'conditions' => array(
							array( 'OpenOrder.general_info like ' => "%{$searchKeyNote}%" )	
						)
					)
				); 
				
			}
			 
			$i = 0;
			if( count($getdetails) > 0 )
			{
				foreach($getdetails as $getdetail)
				{
					$order_note		=   [];
					$id 			= 	$getdetail['OpenOrder']['num_order_id'];	
					$order_note		=	$this->OrderNote->find( 'first' , array( 'conditions' => array( 'order_id' => $id ), 'order' => 'date DESC' ) );					
					 	
					$this->set('order_note', $order_note);	 
						
					$itemDetails	=	$this->getItemListForGlobal( $id );						
					
					if( count($itemDetails) > 0 )									
					{
						$results[$i]['NumOrderId']		=	 $getdetail['OpenOrder']['num_order_id'];
						$results[$i]['amazon_order_id']	=	 $getdetail['OpenOrder']['amazon_order_id'];
						$results[$i]['OrderId']			=	 $getdetail['OpenOrder']['order_id'];
						$results[$i]['OpenStatus']		=	 $getdetail['OpenOrder']['status'];
						$results[$i]['sub_source']		=	 $getdetail['OpenOrder']['sub_source'];
						$results[$i]['linn_fetch_orders']	=	 $getdetail['OpenOrder']['linn_fetch_orders'];
						$results[$i]['date']		    =	 $getdetail['OpenOrder']['date'];
						$results[$i]['process_date']	=	 $getdetail['OpenOrder']['process_date'];
						$results[$i]['Items'] 			=	 $itemDetails;				
						$results[$i]['GeneralInfo']		=	 unserialize($getdetail['OpenOrder']['general_info']);
						$results[$i]['ShippingInfo']	=	 unserialize($getdetail['OpenOrder']['shipping_info']);
						$results[$i]['CustomerInfo']	=	 unserialize($getdetail['OpenOrder']['customer_info']);
						$results[$i]['TotalsInfo']		=	 unserialize($getdetail['OpenOrder']['totals_info']);
						$results[$i]['FolderName']		=	 unserialize($getdetail['OpenOrder']['folder_name']);
						$results[$i]['OpenItems']		=	 unserialize($getdetail['OpenOrder']['items']);
						$results[$i]['parent_id']		=	 $getdetail['OpenOrder']['parent_id'];
						$results[$i]['notes']			=	 $getdetail['OpenOrder']['notes'];
						$results[$i]['ioss_no']			=	 $getdetail['OpenOrder']['ioss_no'];
					}
					else
					{
						$hiddenArray[] = $id;
						//Cancel order popup required
						$results[$i]['NumOrderId']		=	 $getdetail['OpenOrder']['num_order_id'];
						$results[$i]['amazon_order_id']	=	 $getdetail['OpenOrder']['amazon_order_id'];
						$results[$i]['OrderId']			=	 $getdetail['OpenOrder']['order_id'];
						$results[$i]['OpenStatus']		=	 $getdetail['OpenOrder']['status'];
						$results[$i]['date']		    =	 $getdetail['OpenOrder']['date'];						
						$results[$i]['GeneralInfo']		=	 unserialize($getdetail['OpenOrder']['general_info']);
						$results[$i]['ShippingInfo']	=	 unserialize($getdetail['OpenOrder']['shipping_info']);
						$results[$i]['CustomerInfo']	=	 unserialize($getdetail['OpenOrder']['customer_info']);
						$results[$i]['TotalsInfo']		=	 unserialize($getdetail['OpenOrder']['totals_info']);	
						$results[$i]['OpenItems']		=	 unserialize($getdetail['OpenOrder']['items']);
						$results[$i]['IsResend']		=	 $getdetail['OpenOrder']['is_resend'];	
						$results[$i]['parent_id']		=	 $getdetail['OpenOrder']['parent_id'];	
						$results[$i]['notes']			=	 $getdetail['OpenOrder']['notes'];		
						$results[$i]['ioss_no']			=	 $getdetail['OpenOrder']['ioss_no'];				
						
						$this->render('open_order_search_result');
					}				
				$i++;
				}
				if( isset($results)  )
				{
					$results	=	json_decode(json_encode($results),0);
					
					$numID	=	$getdetail['OpenOrder']['num_order_id'];
					
					$getOrderDate	=	$this->MergeUpdate->find( 'first', array(
							'conditions' => array(
									'MergeUpdate.order_id' => $numID
								),
							'fields' => array('MergeUpdate.order_date')
							)
						);
					
					$pDate = $getOrderDate['MergeUpdate']['order_date'];
					
					//changes by amit rawat
					$orderRelation = $this->OpenOrder->find('first',array(
						'conditions' => array(
							'OpenOrder.parent_id' => $numID
						),
						'fields' => array('OpenOrder.num_order_id'),
					));
					
					$childOrderId='';
                    if(!empty($orderRelation['OpenOrder']['num_order_id'])) {   
						$childOrderId=$orderRelation['OpenOrder']['num_order_id'];
					}
					$this->set('childorderid',$childOrderId);

					$this->set('results', $results);
					$this->set('pDate', $pDate);
				}
				$this->render('open_order_search_result');
			}
			else 
			{
				//echo '1';
				//exit;
					
				App::import( 'Controller' , 'ArchiveSearch' );
				$ArchiveSearch = new ArchiveSearchController();
				$darray = $ArchiveSearch->searchOrder( $searchKeyNote );
				if(count($darray) > 0){
					$this->set('childorderid',$darray['childorderid']);
					//changes by amit rawat
					$this->set('results', $darray['results']);
					$this->set('pDate', $darray['pDate']);
					$this->set('archive', 'Archive');						
					$this->render('open_order_search_result');
				}else{
					echo '1';
					exit;
				}
			}
		}
		
		public function searchAnyOrderInList_new()
		{
			
			$this->layout = "";
			$this->autoRander = false;
			$this->loadModel('OpenOrder');
			
			//$orderId	=	explode( '-', trim($this->request->data['searchKey']));
			//$getdetails	=	$this->OpenOrder->find( 'all', array('conditions'=>array('OpenOrder.num_order_id' => $orderId[0], 'OpenOrder.status' => '0')));
			
			$searchKeyNote = $this->request->data['searchKey'];
			$getdetails	=	$this->OpenOrder->find( 'all', array(
				'conditions' => array(
					'OR' => array(
							array( 'OpenOrder.num_order_id' => $searchKeyNote ),
							array( 'OpenOrder.general_info like ' => "%{$searchKeyNote}%" )	
						)
					)
				)
			);
			
			pr($getdetails); exit;
			
			$i = 0;
			if( count($getdetails) > 0 )
			{
				foreach($getdetails as $getdetail)
				{
					$id 			= 	$getdetail['OpenOrder']['num_order_id'];								
					$itemDetails	=	$this->getBundleOrderList( $id );						
					if( count($itemDetails) > 0 )									
					{
						$results[$i]['NumOrderId']		=	 $getdetail['OpenOrder']['num_order_id'];
						$results[$i]['OrderId']			=	 $getdetail['OpenOrder']['order_id'];
						$results[$i]['OpenStatus']		=	 $getdetail['OpenOrder']['status'];
						$results[$i]['date']		    =	 $getdetail['OpenOrder']['date'];
						$results[$i]['Items'] 			=	 $itemDetails;				
						$results[$i]['GeneralInfo']		=	 unserialize($getdetail['OpenOrder']['general_info']);
						$results[$i]['ShippingInfo']	=	 unserialize($getdetail['OpenOrder']['shipping_info']);
						$results[$i]['CustomerInfo']	=	 unserialize($getdetail['OpenOrder']['customer_info']);
						$results[$i]['TotalsInfo']		=	 unserialize($getdetail['OpenOrder']['totals_info']);
						$results[$i]['FolderName']		=	 unserialize($getdetail['OpenOrder']['folder_name']);
					}
					else
					{
						$hiddenArray[] = $id;
					}				
				$i++;
				}
				if( isset($results)  )
				{
					$results	=	json_decode(json_encode($results),0);
					$this->set('results', $results);
				}
				$this->render('open_order_search_result');
			}
			else
			{
				echo "1";
				exit;
			}
		}
		
		public function assignPackagingSlip()
		{
			$this->layout = "";
            $this->autoRander = false;
            $this->loadModel('MergeUpdate');
            $this->loadModel('OpenOrder');
            $this->loadModel('Template');
            $this->loadModel('PostalProvider');
                
			$splitOrder			=	$this->request->data['orderId'];
			$storeId			=	$this->request->data['StoreId'];
			$storeString		=	$this->request->data['StoreIdText'];
			$mergeUpdateId		=	$this->request->data['mergeUpdate'];
			$templateStoreId	=	$this->request->data['TemplateStoreId'];
			$numOrderId = explode( '-', $this->request->data['orderId'] );
			$getTemplate	=	$this->Template->find( 'first', array( 'conditions' => array( 'Template.id' => $templateStoreId ), 'fields' =>array('Template.label_name') ) );
			
			$this->PostalProvider->unbindModel(
					array(
						'hasMany' => array(
							'PostalServiceDesc'
						)
					)
			    );
			
			$getProdiderId	=	$this->PostalProvider->find( 'first', array( 'conditions' => array( 'PostalProvider.provider_name' => $getTemplate['Template']['label_name'] ), 'fields' =>array('PostalProvider.id') ) );
			
			$data['MergeUpdate']['id'] 			=  $mergeUpdateId;
			$data['MergeUpdate']['packSlipId'] 	=  $storeId;
			$data['MergeUpdate']['template_id'] =  $templateStoreId;
			$data['MergeUpdate']['label_id'] 	=  $getProdiderId['PostalProvider']['id'];
			
			$this->MergeUpdate->saveAll($data);
			$this->OpenOrder->updateAll(array('OpenOrder.sub_source' => "'".$storeString."'"), array('OpenOrder.num_order_id' => $numOrderId[0]));
			echo "1";
			exit;
		}
		
		public function getdeleveryLabel()
		{
			$this->autorender = false;
			$this->layout = '';
			
			$this->loadModel( 'Store' );
			$this->loadModel( 'Template' );
			
			$id = $this->request->data['storeId'];
			
			
			$getTemplates	=	$this->Template->find('all', array('conditions' => array('Template.store_id' => $id), 'fields' => array( 'Template.id, Template.label_name, Template.location_name' )));
			
			foreach( $getTemplates as $getTemplateValue )
			{
			
				$data[ $getTemplateValue['Template']['id'] ] = $getTemplateValue['Template']['label_name'].'('. $getTemplateValue['Template']['location_name'] .')';
			}
			
			$getTemplates	=	$data;
			
			$this->set('getTemplate', $getTemplates);
			$this->render( 'storestemplate' );
		}
		
		public function setPostalservice()
		{
				$this->autorender = false;
				$this->layout = '';
				$this->loadModel( 'OpenOrder');
				$this->loadModel( 'MergeUpdate' );
				$this->loadModel( 'PostalServiceDesc' );
				$this->loadModel( 'Template');
				$this->loadModel( 'Product' );	
				$this->loadModel( 'ServiceChangeLog' );
				$this->loadModel( 'OrderLocation' );
				
				$data['id']			=	$this->request->data['id'];
				$data['service_id']	=	$this->request->data['serviceID'];
				$data['customMark']	=	$this->request->data['customMark'];
				
				if( $data['customMark'] == 'true' )
				{
					$customServiceMarked = 1; 
				}
				else
				{
					$customServiceMarked = 0;
				}
				
				$deliveryMatrix = $this->PostalServiceDesc->find( 'all' , array( 'conditions' => array( 'PostalServiceDesc.id' => $data['service_id'] ) ) );
				$mergeupdatedata = $this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.id' => $data['id'] ) ) );
				$delcon	=	trim($mergeupdatedata['MergeUpdate']['delevery_country']);
				 
				
				$length = 0 ; $width =0; $_height = [];$_weight = []; $searcharrayNew = [];
				if($this->Session->read('Auth.User.username') == 'avadhesh'){
				
					if($mergeupdatedata['MergeUpdate']['delevery_country'] == 'null'){
						
						$this->loadModel( 'Country' );
						
						$order_id	=	$mergeupdatedata['MergeUpdate']['order_id'];
						$open 		= 	$this->OpenOrder->find('first', array('conditions' => array( 'num_order_id' => $order_id ) ) );
						$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $open['OpenOrder']['destination'])));
 						if(count($country_data) > 0 ){
							$searcharrayNew['country_code'] = $country_data['Country']['iso_2'];
						}
						$searcharrayNew['delevery_country'] = $open['OpenOrder']['destination'];
						$delcon		=	$open['OpenOrder']['destination'];	
					}
					
					if($mergeupdatedata['MergeUpdate']['packet_weight'] == 0 && $mergeupdatedata['MergeUpdate']['packet_length'] == 0){
					
						foreach(explode(",",$mergeupdatedata['MergeUpdate']['sku']) as $_sku){
							$exp 		= 	explode("XS-",$_sku);
							$sku 		= 	'S-'.$exp[1];
							$product	=   $this->Product->find('first', array('conditions' => array( 'product_sku' => $sku),'fields'=>['ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.product_sku']  ) );
							
							$length    = $product['ProductDesc']['length'];
							$width     = $product['ProductDesc']['width'];
							$_height[] = $product['ProductDesc']['height'];
							$_weight[] = $product['ProductDesc']['weight'];
						}	
						$searcharrayNew['packet_weight']  = array_sum($_weight) ;
						$searcharrayNew['packet_length']  = $length;
						$searcharrayNew['packet_width']   = $width;
						$searcharrayNew['packet_height']  =  array_sum($_height) ;
						
					}
				}
				
				if(count($deliveryMatrix) == 0)
				 { 
					echo json_encode(array( 'status' => '2', 'data' => '' ));
					exit;
				 }
			    if($deliveryMatrix[0]['Location']['county_name'] != $delcon && $deliveryMatrix[0]['Location']['county_name'] != 'Blended')
				 {
				 	if($this->Session->read('Auth.User.username') != 'avadhesh'){
						echo json_encode(array( 'status' => '3','msg' => 'county name not found.delcon='.$delcon.'#'.$deliveryMatrix[0]['Location']['county_name'] , 'data' => '' ));
						exit;
					}
				 }
				 if($deliveryMatrix[0]['PostalServiceDesc']['provider_ref_code'] == 'Parcelforce')
				 {
				 	echo json_encode(array( 'status' => '3', 'data' => '' ));
					exit;
				 }  
				 
				if( count($deliveryMatrix) > 0 )
				 { 					  
					   
					   $searcharrayNew['id'] = $data['id'];
					   $searcharrayNew['service_id'] = $deliveryMatrix[0]['PostalServiceDesc']['id'];
					   $searcharrayNew['service_name']  = $deliveryMatrix[0]['PostalServiceDesc']['service_name'];
					   $searcharrayNew['provider_ref_code']  = $deliveryMatrix[0]['PostalServiceDesc']['provider_ref_code'];
					   $searcharrayNew['service_provider']  = ($deliveryMatrix[0]['PostalProvider']['provider_name'] == 'Belgium Post') ? 'PostNL' : $deliveryMatrix[0]['PostalProvider']['provider_name'];
					   $searcharrayNew['postal_service']  = $deliveryMatrix[0]['ServiceLevel']['service_name'];
					   
					   if($mergeupdatedata['MergeUpdate']['pick_list_status'] > 0){
							
							$searcharrayNew['picked_date']  = '';	
							$searcharrayNew['pick_list_status']  = 0;						
							$searcharrayNew['picklist_username'] = '';						 
							$searcharrayNew['picklist_inc_id']   = 0;
							
							$OrderLocation = array();
							
							$OrderLocation['pick_list']			=	0;
							$OrderLocation['picklist_inc_id'] 	=	0;
							$OrderLocation['picked_date']		=	'""'; 
							$upd_conditions  = array('split_order_id' => $mergeupdatedata['MergeUpdate']['product_order_id_identify'],'status' => 'active') ;
							$this->OrderLocation->updateAll( $OrderLocation,$upd_conditions);
						}
						  
					   $searcharrayNew['custom_service_status']  = 1;
					   $searcharrayNew['custom_service_marked']  = $customServiceMarked;
					   
					   $postalProvider = ( $deliveryMatrix[0]['PostalProvider']['provider_name'] == 'Belgium Post' ) ? 'PostNL' : $deliveryMatrix[0]['PostalProvider']['provider_name'];
					   //get template id accordign to service provider
					   $getTemplate = $this->Template->find( 'first' , array( 'conditions' => array( 'Template.label_name' => $postalProvider ) ));
					   $getTemplateId = $getTemplate['Template']['id'];
					   
					   $searcharrayNew['lable_id']  = $getTemplateId; 
					   $result = $this->MergeUpdate->saveAll( $searcharrayNew );
					   
					   $ServiceChangeLog = array();
					   $ServiceChangeLog['split_order_id']			=	$mergeupdatedata['MergeUpdate']['product_order_id_identify'];
					   $ServiceChangeLog['picklist_inc_id']			=	$mergeupdatedata['MergeUpdate']['picklist_inc_id'];
					   $ServiceChangeLog['old_service_name']	    =	$mergeupdatedata['MergeUpdate']['service_name'];
					   $ServiceChangeLog['old_service_provider']	=	$mergeupdatedata['MergeUpdate']['service_provider'];
 					   $ServiceChangeLog['new_service_name']		=	$searcharrayNew['service_name'];
					   $ServiceChangeLog['new_service_provider']	=	$searcharrayNew['service_provider'];
					   $ServiceChangeLog['username']				=	$this->Session->read('Auth.User.username');
					   $this->ServiceChangeLog->saveAll( $ServiceChangeLog );
					   
  					   
					    $assign_service_log = "Order id=".$data['id']."\tService id=".$data['service_id']."\tService name=".$searcharrayNew['postal_service']."\tService Provider=".$searcharrayNew['service_provider']."\tService code=".$searcharrayNew['provider_ref_code']."\tUser=".$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name')."\t".date('Y-m-d H:i:s')."\n";
					   file_put_contents(WWW_ROOT."logs/assign_service_".date('Ymd').".log", $assign_service_log, FILE_APPEND | LOCK_EX);
					   
					   echo json_encode(array( 'status' => '1', 'data' => $searcharrayNew ));
					   exit;
				}
				else
				{
				   echo "2";
				   exit;
				}
				
				
				/*$getPostelservice	=	$this->PostalServiceDesc->find('all', array( 'conditions' => array( 'PostalServiceDesc.id' => $this->request->data['serviceID'] ) ));
				
				if( count( $getPostelservice ) > 0 )
				{
					$this->MergeUpdate->saveAll($data);
					echo "1";
					exit;
				}
				else
				{
					echo "2";
					exit;
				}*/
				
			}	
		
		
		public function getSpecificOrderById( $bundleId = null )
		{
			
			/* for custom postal service */
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'Template' );
			$this->loadModel( 'PackagingSlip' );
			
			$this->MergeUpdate->bindModel(
				array(
					'hasOne' => array(
						'PackagingSlip' => array(
							'foreignKey' => false,
							'conditions' => array('PackagingSlip.id = MergeUpdate.packSlipId'),
							'fields' => array('PackagingSlip.id,PackagingSlip.name')
						),
						'Template' => array(
							'foreignKey' => false,
							'conditions' => array('Template.id = MergeUpdate.lable_id'),
							'fields' => array('Template.id,Template.label_name')
						)
					)
				)
			);
		 $orderItem	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => $bundleId, 'MergeUpdate.status' => 0)));			 			 			 						
		 return $orderItem;	 	 
			
		}
		
		/*
			 * 
			 * 
			 * Params, Custom order process
			 * 
			 */
			 public function customOrderProcess()
			{
				$this->layout = "";
                $this->autoRander = false;
                $this->loadModel('OpenOrder');
                $orderId	=	$this->request->data['orderid'];
                
                $getdetails	=	$this->OpenOrder->find( 'all', array('conditions'=>array('OpenOrder.num_order_id' => $orderId)));
                
				$i = 0;
				
				foreach($getdetails as $getdetail)
				{
					
					$id 			= 	$getdetail['OpenOrder']['num_order_id'];								
					$itemDetails	=	$this->getSpecificOrderById( $orderId );						
					
					if( count($itemDetails) > 0 )									
					{
						$results[$i]['NumOrderId']		=	 $getdetail['OpenOrder']['num_order_id'];
						$results[$i]['OrderId']			=	 $getdetail['OpenOrder']['order_id'];
						$results[$i]['Items'] =	$itemDetails;				
						$results[$i]['GeneralInfo']		=	 unserialize($getdetail['OpenOrder']['general_info']);
						$results[$i]['ShippingInfo']	=	 unserialize($getdetail['OpenOrder']['shipping_info']);
						$results[$i]['CustomerInfo']	=	 unserialize($getdetail['OpenOrder']['customer_info']);
						$results[$i]['TotalsInfo']		=	 unserialize($getdetail['OpenOrder']['totals_info']);
						$results[$i]['FolderName']		=	 unserialize($getdetail['OpenOrder']['folder_name']);
						//$results[$i]['Items']			=	 unserialize($getdetail['OpenOrder']['items']);
					}
					else
					{
						$hiddenArray[] = $id;
					}				
				$i++;
				}
				if( isset($results)  )
				{
					$results	=	json_decode(json_encode($results),0);
					$this->set('results', $results);
				}
			    $this->render('custom_process_order');
			}
			
			/*
			 * 
			 * 
			 * Params, Custom order process without process marked into innworks because it would be update by cron
			 * But for now, system will mark processed without adding into sorting or manifest
			 * 
			 * 
			 */
			 public function customProcessMarked()
			 {
				 
				$this->layout = "";
                $this->autoRander = false;
                $this->loadModel('OpenOrder');
                $this->loadModel( 'MergeUpdate' );
                
                $orderId	=	$this->request->data['orderid'];
                
                //Custom marked into models
                $setMGUpdate['scan_status'] = '1';
                $setMGUpdate['sorted_scanned'] = '1';
                $setMGUpdate['manifest_status'] = '2';
                $setMGUpdate['status'] = '1';
                $setMGUpdate['custom_marked'] = '1';
                
                $this->MergeUpdate->updateAll( $setMGUpdate , array( 'MergeUpdate.product_order_id_identify' => $orderId ) );
                
                $expId = explode('-', $orderId);
                $param = array(
					'conditions' => array(
						'MergeUpdate.status' => 0,
						'MergeUpdate.order_id' => $expId[0]
					)
                );
                
                $getLists = $this->MergeUpdate->find('first', $param);
                if( count( $getLists ) == 0 )
                {					
					//Update OpenOrder
					//$this->OpenOrder->updateAll( array('OpenOrder.status' => '1'), array( 'OpenOrder.num_order_id' => $expId[0] ) );					
					$ordId = $expId[0];
					//$this->OpenOrder->query( "Update open_orders as OpenOrder set OpenOrder.status = 1 where OpenOrder.num_order_id = '{$ordId}'" );
					
					//Updated 19-01-2017
					$this->OpenOrder->updateAll(array('OpenOrder.status' => '1','OpenOrder.process_date' => "'".date('Y-m-d H:i:s')."'" ), array('OpenOrder.num_order_id' => $ordId));
					
					//reserver inventory now on the basis of selection
					//$this->reserveInevntoryByOrderId( $orderId )
					
					echo "1";
					exit;
				}
				else
				{
					echo "2";
					exit;	
				}
			} 
			
			/*
			 * 
			 * 
			 * Inventroy manage at time of custom processing on the basis of user selection customer / staff
			 * 
			 * 
			 */ 
			
			public function reserveInevntoryByOrderId__( $orderId = null )
			{
				
				$this->layout = "";
                $this->autoRander = false;
                $this->loadModel('OpenOrder');
                $this->loadModel( 'MergeUpdate' );
                
                //$orderId	=	$this->request->data['orderid'];
                
                $orderId = '101257-1';
                $param = array(
					'conditions' => array(
						//'MergeUpdate.status' => 0,
						'MergeUpdate.product_order_id_identify' => $orderId
					)
                );
                
                $getLists = $this->MergeUpdate->find('first', $param);
                pr($getLists); exit;
                
				
			} 
			
			public function reserveInventoryForUnknown( $orderItems = null , $type = null , $NumOrderId = null , $check = null )
			{
				
				$this->loadModel( 'Product' );
				$this->loadModel( 'OversellRecord' );						
				
				$orderSku = array();
				$ik = 0;foreach( $orderItems as $index => $value )
				{
					
					$sku = explode( '-' , $value->SKU);
					if( count( $sku ) == 2 ) //For single
					{
						$orderSku[$ik][0] = $value->SKU;
						$orderSku[$ik][1] = $value->Quantity;
						$ik++;
					}
					else if( count( $sku ) == 3 ) // For Bundle (Single)
					{
						$orderSku[$ik][0] = 'S-'.$sku[1];
						$orderSku[$ik][1] = $value->Quantity * ($sku[2]);
						$ik++;
					}
					else if( count( $sku ) > 3 ) // For Bundle (Bundle)
					{
						
						$inc = 1;$ij = 0;while( $ij < count($sku)-2 )
						{
							
							$orderSku[$ik][0] = 'S-'.$sku[$inc];
							$orderSku[$ik][1] = $value->Quantity;
						
						$inc++;
						$ij++;
						$ik++;	
						}
					}					
				}
				
				//pr($orderSku);
				//Update in queue inventory
				foreach( $orderSku as $orderSkuIndex => $orderSkuValue )
				{
					
					$paramInner = array(
						 'conditions' => array(
							 'Product.product_sku' => $orderSkuValue[0]
						 ),
						 'fields' => array(
							 'Product.current_stock_level as CurrentStock',
							 'Product.id',
							 'Product.product_sku',
							 'ProductDesc.barcode'
						 )  
					   );
					   $openOrderRow = json_decode(json_encode($this->Product->find('first' , $paramInner)),0);
					   //pr($openOrderRow);
					   
					   //Update inventory now very easy
					   $id = $openOrderRow->Product->id;  
					   //$data['Product']['id'] = $openOrderRow->Product->id;
					   //$data['Product']['current_stock_level'] = $calculateStock;
					   
					   $orderSkuStock = $orderSkuValue[1];
					   
					   //inventory manipulation					   
					   if( $type == 2 )
					   {
					   
							$ckSku = $orderSkuValue[0];
							if( $check == 1 )
							{
								
								$param = array(
							
									'conditions' => array(
									
										'OversellRecord.sku' => $ckSku,
										'OversellRecord.num_order_id' => $NumOrderId
									
									)
								
								);
								$count = $this->OversellRecord->find( 'count' , $param );
								
								if( $count == 0 )
									$calculateStock = $openOrderRow->Product->CurrentStock + $orderSkuStock;	
							}
							else
							{
								$calculateStock = $openOrderRow->Product->CurrentStock + $orderSkuStock;	
							}
							
							if( $id > 0  )	
							{
							   if( $calculateStock <= 0 )
							   {
								   $this->Product->query( "UPDATE products set products.current_stock_level = 0 where id = {$id}" );
							   }
							   else
							   {
									$this->Product->query( "UPDATE products set products.current_stock_level = {$calculateStock} where id = {$id}" );
							   }
							}
							
					   }	
					   else
					   {  
						  
						   if( $openOrderRow->Product->CurrentStock <= 0 )
						   {
							  $rest = 0 - $orderSkuStock;
						   }
						   else if( ($openOrderRow->Product->CurrentStock < $orderSkuStock) && ($openOrderRow->Product->CurrentStock <= 0) )
						   {
							  $rest = 0 - $orderSkuStock;
						   }  
						   else if( ($openOrderRow->Product->CurrentStock < $orderSkuStock) && ($openOrderRow->Product->CurrentStock > 0) )
						   {
							  $rest = $openOrderRow->Product->CurrentStock - $orderSkuStock;
						   }
						   else if( $openOrderRow->Product->CurrentStock > $orderSkuStock )
						   {
							  $rest = $openOrderRow->Product->CurrentStock - $orderSkuStock;
						   }	  
						   
						   //Over sell check up
						   if( $openOrderRow->Product->CurrentStock <= 0 ) 
						   {
								//store record future reference  //$NumOrderId							
								$data['OversellRecord']['OversellRecord']['num_order_id']		= $NumOrderId;
								$data['OversellRecord']['OversellRecord']['sku']				= $orderSkuValue[0];
								$data['OversellRecord']['OversellRecord']['original_qty']		= $orderSkuValue[1];
								$data['OversellRecord']['OversellRecord']['marker']			= 3;							
								$this->OversellRecord->saveAll( $data['OversellRecord'] );
						   }
							
						   if( $id > 0  )	
						   {
							   if( $rest <= 0 )
							   {
								   $this->Product->query( "UPDATE products set products.current_stock_level = 0 where id = {$id}" );
							   }
							   else
							   {
									$this->Product->query( "UPDATE products set products.current_stock_level = {$rest} where id = {$id}" );
							   }
							}
							
					   }
		
					}
					
				}
			
			
			public function cc()
			{
				//Send notification						
				$sendNotifyArray[] = 'Userxx';//$authDetails['first_name'];
				$sendNotifyArray[] = '110011-1'; //$splitID;
				//$putBackItem['action_type']
				$sendNotifyArray[] = 'Cancel'; //$splitID;
				
				App::import('Controller','MyExceptions');
				$myExceptionObj = new MyExceptionsController(new View());                        
				$myExceptionObj->send_cancel_delete_order_notification(  $sendNotifyArray  );
				exit;
			}
			
			public function customLogGlobal( $logType = null , $msg = null )
			 {
				  //CakeLog::config($logType, array('engine' => 'FileLog'));
				  //CakeLog::write($logType, 'Result Array is :: ' . print_r($msg , true));
				  
				  App::uses('Folder', 'Utility');
				  App::uses('File', 'Utility');
				  
				  $folderPath = WWW_ROOT .'img/Picklist_BK';
				  $dir = new Folder($folderPath, true, 0755);
				  
				  $imgPath = $folderPath .'/'. date('Y-m-d') .'__'. time() .'_Picklist_BK.txt';    				  
				  file_put_contents($imgPath, $msg);
				  
			 }
			
			public function incompleteOrder()
			{
				$this->layout = "index";
				$this->loadModel( 'OpenOrder' );
				$this->loadModel( 'MergeUpdate' );
				$params = array( 
								'conditions' => array( 'OpenOrder.status' => '0' , 'OpenOrder.order_status' => '1' ),
								);
				
				$getIncompleteDetails	=	$this->OpenOrder->find( 'all', $params );
				
				$i = 0;
				foreach( $getIncompleteDetails as $index => $getAllProcessOrder )
				{
					
					$processOrderDetail[$i]['order_id']			= $getAllProcessOrder['OpenOrder']['order_id'];
					$processOrderDetail[$i]['num_order_id']		= $getAllProcessOrder['OpenOrder']['num_order_id'];
					$processOrderDetail[$i]['general_info']		= unserialize($getAllProcessOrder['OpenOrder']['general_info']);
					$processOrderDetail[$i]['shipping_info']	= unserialize($getAllProcessOrder['OpenOrder']['shipping_info']);
					$processOrderDetail[$i]['customer_info']	= unserialize($getAllProcessOrder['OpenOrder']['customer_info']);
					$processOrderDetail[$i]['totals_info']		= unserialize($getAllProcessOrder['OpenOrder']['totals_info']);
					$processOrderDetail[$i]['totals_info']		= unserialize($getAllProcessOrder['OpenOrder']['totals_info']);
					$processOrderDetail[$i]['items']			= unserialize($getAllProcessOrder['OpenOrder']['items']);
					$processOrderDetail[$i]['date']				= $getAllProcessOrder['OpenOrder']['date'];
					$i++;
					unset( $value );
				}
				//pr($processOrderDetail);
				//exit;
				$this->set( 'getdetails', $processOrderDetail );
				
			}
			
		public function deleteIncompleteOrders( $id = null )
		{
			$this->loadModel( 'OpenOrder' );
			
			$delete = array(
								'OpenOrder.num_order_id' => $id ,
								'OpenOrder.order_status' => 1,
								'OpenOrder.status' => 0  
							);
			
			$this->OpenOrder->deleteAll( $delete );
			
			$this->Session->setFlash($id . ' :: was incomplete and deleted now. Please check after few min!.', 'flash_success');
			$this->redirect(array('controller'=>'Linnworksapis','action' => 'incompleteOrder'));
			
		}
		
		public function getStoreOrderCount()
		{
			$this->layout = "";
            $this->autoRender = false;
                
			$this->loadModel( 'OpenOrder' );
			
			$sumQuantity	=	$this->OpenOrder->find( 'all', array(
									'conditions' => array( 'OpenOrder.status' => '0' ),
									'fields' => array( 'count(OpenOrder.sub_source)   AS Quantity' , 'OpenOrder.sub_source'),
									'group' => 'OpenOrder.sub_source'
									 )
								);
			
			return $sumQuantity;
		}
		public function getUserDetail( $id )
		{
			$this->loadModel('User');
			$getUserDetail = $this->User->find('first', array('conditions' => array('User.id' => $id) ) );
			
			if( count($getUserDetail) == 1 )
			{
					echo $getUserDetail['User']['first_name'].' '.$getUserDetail['User']['last_name'].' [ '.$getUserDetail['User']['username'].' ] '.' [ '.$getUserDetail['User']['pc_name'].' ]';
			}
			else
			{
				 echo "----";
			}
		}
		
		public function getUserByEmail( $email )
		{
			$this->loadModel('User');
			$getUserDetail = $this->User->find('first', array('conditions' => array('User.email' => $email) ) );
			return $getUserDetail;
			 
		}
		
		public function getDpDetail( $picklist_inc_id )
		{
 			$this->loadModel('DynamicPicklist');
			$dp_res = $this->DynamicPicklist->find( 'first' , array( 'conditions' => array( 'id' => $picklist_inc_id )) );	
 			return $dp_res;
		}	
		
	/*public function orderCancelCustom()
	{
		$orderids = array(813544,814364,814365,814407,814438,814395,814439,814397,814424,814401,814423,814399,814425,814405,814394,814398,814430,814431,814454,814432,814400,814433,814404,814406,814281,814381,814264,814382,814383,814266,814267,814385,814386,814269,814271,814387,814388,814274,814276,814390,814391,814371,814372,814280,814247,814402,814376,814378,813855,814420,814246,814117,814416,814417,814419,814123,814458,814208,814459,814460,808389,813448);
		foreach( $orderids as $orderid )
		{
			$this->cancelOrderXsensys($orderid);
		}
	}*/
	
	public function cancelOrderXsensys( $order_id = null )
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'BinLocation' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'CancelSku' );
		$this->loadModel( 'OrderLocation' );
		$this->loadModel( 'CheckIn' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderNote' );
		$this->loadModel( 'ScanOrder' );
		$partial_fulfillment = 'No';
		$count = 0;  
		$order_id = trim($this->request->data['openorderid']);
		$cancel_note = trim($this->request->data['cancelNote']);
		
		$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		
		/****Cancel Royal mail Shipment*****/
		$order_merge	= $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.order_id' => $order_id,'MergeUpdate.royalmail' => 1),'fields' => array('MergeUpdate.product_order_id_identify')));
		if(count($order_merge) > 0){
			App::Import('Controller', 'RoyalMail'); 
			$royal = new RoyalMailController;	
			foreach( $order_merge as $_result )
			{
				$royal->royalMailCancelShipment($_result['MergeUpdate']['product_order_id_identify'],1); 			
			} 
		}
		/**********END OF ROYAL MAIL***********/
		
		$order_details	= $this->OrderLocation->find('all', array('conditions' => array('OrderLocation.order_id' => $order_id, 'OrderLocation.status' => 'active')));
		
		
		if($order_id){
			$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 2, 'MergeUpdate.reverce_picklist_status' => 1 ) , array( 'MergeUpdate.order_id' => $order_id ) );
			$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 2 , 'OpenOrder.cancel_status' => 5 ) , array( 'OpenOrder.num_order_id' => $order_id ) );
			$count++; 
		}
		
		if(count($order_details) > 0){
			$count = 0;
		
			foreach($order_details as $val){
			
				if($val['OrderLocation']['bin_location'] == 'No Location')
				{
					$partial_fulfillment += $val['OrderLocation']['quantity'];
				}
				else
				{
					$barcode 	= $val['OrderLocation']['barcode'];
					$sku		= $val['OrderLocation']['sku'];
					$location 	= $val['OrderLocation']['bin_location'];
					$quantity 	= $val['OrderLocation']['quantity'];
					$qty_bin 	= $val['OrderLocation']['available_qty_bin'];
					//$qty_chin 	= $val['OrderLocation']['available_qty_check_in'];
					$po_id	 	= $val['OrderLocation']['po_id'];					
					
					$getBinLocation	=	$this->BinLocation->find('first', array( 
																'conditions' => array(
																		'BinLocation.barcode' => $barcode,
																		'BinLocation.bin_location' => $location),
																		));
					$locationCurrentQty = 0;
					$totallocationQty   = 0;
					if(count($getBinLocation) > 0){
						$locationCurrentQty = $getBinLocation['BinLocation']['stock_by_location'];
						$totallocationQty = (int)$locationCurrentQty + (int)$qty_bin;
						$this->BinLocation->updateAll(
													array('BinLocation.stock_by_location' => $totallocationQty),
													array('BinLocation.bin_location' => $location,'BinLocation.barcode' => $barcode)
												  );
												  
						$this->CheckIn->updateAll( array('CheckIn.selling_qty' => "CheckIn.selling_qty - $qty_bin"),array('CheckIn.id' => $po_id));	
					}							  
					$getCurrentQty = $this->Product->find('first', array( 
																	'conditions' => array( 'Product.product_sku' => $sku ),
																	'fields' => array( 'Product.current_stock_level' )
																	 ) );
					$currentQty		=	$getCurrentQty['Product']['current_stock_level'];					
					$totalcurrentQty =  (int)$currentQty + (int)$qty_bin ;
					
					$this->Product->updateAll(
												array('Product.current_stock_level' => $totalcurrentQty),
												array('Product.product_sku' => $sku)
											  );
											  
					$cancelData['order_id']				= $order_id;
					$cancelData['sku']					= $sku;	
					$cancelData['barcode']				= $barcode;	
					$cancelData['bin_location']			= $location;	
					$cancelData['qty']					= $qty_bin;	
					$cancelData['current_qty']			= $currentQty;
					$cancelData['after_qty']			= $totalcurrentQty;	
					$cancelData['current_location_qty'] = $locationCurrentQty;
					$cancelData['after_location_qty']	= $totallocationQty;
					$cancelData['user_name']			= $firstName.' '.$lastName;
					$cancelData['date_timef']			= date( 'Y-m-d H:i:s' );
					//pr($cancelData);
					$this->CancelSku->saveAll( $cancelData );
					 
					$data['Product']['product_sku']		=	$sku;
					$data['ProductDesc']['barcode']		=	$barcode;
					$data['Product']['CurrentStock']	=	$currentQty;
					$productData	=	json_decode(json_encode($data,0));					
					
					App::import('Controller', 'Cronjobs');
					$productCant = new CronjobsController();
					$productCant->storeInventoryRecord( $productData, $qty_bin, $order_id, 'Cancel With Location', $barcode );
					$getspids	=	$this->MergeUpdate->find( 'all', array('conditions' => array( 'MergeUpdate.order_id' => $order_id ), 'fields' => array( 'product_order_id_identify') ) );
					
					foreach( $getspids as $getspid ) 
					{
						$conditionsforscanorder = array ('ScanOrder.split_order_id' => $getspid['MergeUpdate']['product_order_id_identify']);
						$this->ScanOrder->deleteAll($conditionsforscanorder);
					}
					
					$count++;
					}
			
			}		
					
			$this->OrderLocation->updateAll( array( 'OrderLocation.status' => "'canceled'") , array( 'OrderLocation.order_id' => $order_id ) );
			
			$noteDate['order_id'] = $order_id;
			$noteDate['note'] = $cancel_note;
			$noteDate['type'] = 'Cancel';
			$noteDate['user'] = $firstName.' '.$lastName;
			$noteDate['date'] = date('Y-m-d H:i:s');
			$this->OrderNote->saveAll( $noteDate );
			
		}
		$msg['partial_fulfillment'] = $partial_fulfillment;
		$msg['delete_count'] = $count;			
		echo json_encode($msg);
		//exit;
	}
		//Avadhesh this is using po and order location	
	public function cancelOrderNotePopup()
	{
		$this->layout = '';
		$this->autoRander = false;
		$order_id = trim($this->request->data['orderid']);
		$this->set('orderid', $order_id);
		$this->render( 'notePopup' );
	}
	
	public function holdCancelPopup()
	{
		$this->layout = '';
		$this->autoRander = false;
		$order_id = trim($this->request->data['orderid']);
		$this->set('orderid', $order_id);
		$this->render( 'cancelHeldPopup' );
	}
		
	public function createPickList()
	{
		
		$this->layout = '';
		$this->autoRander = false;		
		
		/* start for pick list */
		
		$this->loadModel( 'OrderLocation' );		
		$this->loadModel( 'MergeUpdate' );	
		$this->loadModel( 'UnprepareOrder' );	
		$this->loadModel( 'Product' );
		$this->loadModel( 'MergeUpdate' );
		
		$products = array();
		
		//$arr = array(421644,421672,421655,421673,421667,421669,421671,421670,421674,421676,422617,421582,421601,423132,421501,421185,421563,421203);
		
		$_product	= $this->Product->find('all',array('fields'=>array('product_sku','product_name'),'order'=>'Product.id DESC'));
		foreach( $_product as $pro ){
			$products[$pro['Product']['product_sku']] =$pro['Product']['product_name'];
		}
		
		$all_order = array();
		$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id'),'conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.status' => '0','MergeUpdate.service_provider !=' => 'DHL')));	
		
		foreach($orderItems as $item_order ){
			$all_order[] = $item_order['MergeUpdate']['order_id'];
		}
		
		if(count($all_order) > 1){
			$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id IN' => $all_order),'order' => 'bin_location ASC');				
		}else{
			$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id ' => $all_order[0]),'order' => 'bin_location ASC');	
		}		
		//$param = array('conditions' => array('OrderLocation.pick_list' => '0' ),'order' => 'bin_location ASC');				
		$pickListItems 	=	$this->OrderLocation->find('all', $param );			
		if( count( $pickListItems ) > 0 )
		{
		
			//abort status
			$fileStatus = $this->updateAbortFile();
			//manage userd - id with name
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
			
			$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
			$picklist_username = $firstName .' '. $lastName;
			$orderids = array(); $data = array(); $tq = 0; $taq = 0;
			foreach( $pickListItems as $val )
			{
				$tq += $val['OrderLocation']['quantity'];	
				$taq += $val['OrderLocation']['available_qty_bin'];	
				$orderids[] = $val['OrderLocation']['order_id'];			
				$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																	'sku'=>trim($val['OrderLocation']['sku']),
																	'barcode' => $val['OrderLocation']['barcode'],
																	'quantity' => $val['OrderLocation']['quantity'],
																	'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
			}
						
			/* dome pdf vendor */
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();
			$dompdf->set_paper('A4', 'portrait');
			/* Html for print pick list */
			$date = date("m/d/Y H:i:s");	
			$finArray = array();
			$html = '<h3>PickList :- '.$date.'</h3><hr><h3>Total Item  : - '.$tq.'</h3>
					<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
					<tr>
					<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
					<th style="border:1px solid;" width="10%" align="center">SKU</th>
					<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
					<th style="border:1px solid;" width="10%" align="center">Barcode</th>
					</tr>';
			
			$data = $this->msortnew($data, array('bin_location'));
			$tdata = array();
			foreach($data as $a){
				$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
										
			}			
			
			foreach($tdata as $sku => $d){
					$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
					/*foreach($d as $a){
						$bn[$a['bin_location']][] = $a['quantity'];	
						$bna[$a['bin_location']][] = $a['available_qty_bin'];	
						$bar[$a['barcode']] = $a['barcode'];	
					}*/
					foreach($d as $_ar){
						$bn[$_ar['bin_location']][] = $_ar['quantity'];	
						$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
						//$bar[$_ar['barcode']] = $_ar['barcode'];	
						$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
						
						if(count($local_barcode) > 0){
							$bar[$local_barcode[0]] = $local_barcode[0]; 
						}else{
							$bar[$_ar['barcode']] = $_ar['barcode'];
						}
					}
						
					$bins = array_keys($bn);  $oversell = array();
					foreach($bins as $b){
						if($b !=''){
							if(array_sum($bn[$b]) == array_sum($bna[$b])){
								$bin_loc[]= $b."(".array_sum($bna[$b]).")";
							}else{
								$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
							}
						}
						$qty[]= array_sum($bn[$b]);
						$avaqty[]= array_sum($bna[$b]);
						
					}	
					$oversell_str = '';
					if(count($oversell) > 0){
						$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
					}					
					$html.=	'<tr>
						<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
						<td style="border:1px solid;">'.$sku.'</td>
						<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.substr($products[$sku], 0, 40 ).'</td>
						<td style="border:1px solid;">'.implode(",",$bar).'</td>							
						</tr>';
						
			}	
		
			$html .= '</table>';			 
			$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
			$dompdf->render();
 			  			
			date_default_timezone_set('Europe/Jersey');
			$today = date("d_m_Y_H_i");
			//$dompdf->stream("Pick_List_(".$date.").pdf");
			
			$imgPath = WWW_ROOT .'img/printPickList/'; 
			$path = Router::url('/', true).'img/printPickList/';
			$name	=	'Pick_List__'.$today.'.pdf';
			
			file_put_contents($imgPath.$name, $dompdf->output());
			
			if(count($orderids) > 0){
				if(count($orderids) > 1){
					$this->MergeUpdate->updateAll(array('MergeUpdate.picklist_username' => "'".$picklist_username."'", 'MergeUpdate.pick_list_status' => '1','MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.order_id IN ' => $orderids));
					$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.order_id IN ' => $orderids));						
				}else{
					$this->MergeUpdate->updateAll(array('MergeUpdate.picklist_username' => "'".$picklist_username."'", 'MergeUpdate.pick_list_status' => '1','MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.order_id = ' => $orderids[0]));
					$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.order_id = ' => $orderids[0]));
				}
			}
			
			$this->customLogGlobal( 'Pick List [ ' . $username . '__' . date( 'Y-m-d j:i:s' ) . ' ]' , implode( "," , $orderids ) );
			
			
			$folderPath = WWW_ROOT .'logs';
			$dir = new Folder($folderPath, true, 0755);			
			$_Path = $folderPath .'/'. date('dmy_Hi').'_Picklist.log';    				  
			file_put_contents($_Path, $username."\n".implode( "," , $orderids ));
				  
				  
			//now abort file status down
			$this->updateAbortFileAtLast();
			$fileStatus = 0;
			$msg['pdfurl'] = $serverPath  	= 	$path.$name ;
			$msg['msg'] = 'ok';
			echo json_encode($msg);
			exit;
				
		}else{
			$msg['msg'] = 'error';
			echo json_encode($msg);			
			 exit;
		}
		
	}
		
	public function createPickList130617()
	{
		
		$this->layout = '';
		$this->autoRander = false;		
		
		/* start for pick list */
		
		$this->loadModel( 'OrderLocation' );		
		$this->loadModel( 'MergeUpdate' );	
		$this->loadModel( 'UnprepareOrder' );	
		$this->loadModel( 'Product' );
		
		$products = array();
		
		//$arr = array(421644,421672,421655,421673,421667,421669,421671,421670,421674,421676,422617,421582,421601,423132,421501,421185,421563,421203);
		
		$_product	= $this->Product->find('all',array('fields'=>array('product_sku','product_name'),'order'=>'Product.id DESC'));
		foreach( $_product as $pro ){
			$products[$pro['Product']['product_sku']] =$pro['Product']['product_name'];
		}
		
		$_unpOrders	= $this->UnprepareOrder->find('all',array('fields'=>array('num_order_id'),'order'=>'UnprepareOrder.id DESC'));
		foreach($_unpOrders as $unp_order ){
			$unprepare_order[$unp_order['UnprepareOrder']['num_order_id']] = $unp_order['UnprepareOrder']['num_order_id'];
		}
		if(count($unprepare_order) > 1){
			$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id NOT IN' => $unprepare_order),'order' => 'bin_location ASC');				
		}else{
			$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id !=' => $unprepare_order[0]),'order' => 'bin_location ASC');	
		}
		
		//$param = array('conditions' => array('OrderLocation.pick_list' => '0' ),'order' => 'bin_location ASC');				
		$pickListItems 	=	$this->OrderLocation->find('all', $param );			
	
		if( count( $pickListItems ) > 0 )
		{
		
			//abort status
			$fileStatus = $this->updateAbortFile();
			//manage userd - id with name
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
			
			$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
			
			$orderids = array(); $data = array(); $tq = 0; $taq = 0;
			foreach( $pickListItems as $val )
			{
				$tq += $val['OrderLocation']['quantity'];	
				$taq += $val['OrderLocation']['available_qty_bin'];	
				$orderids[] = $val['OrderLocation']['order_id'];			
				$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																	'sku'=>trim($val['OrderLocation']['sku']),
																	'barcode' => $val['OrderLocation']['barcode'],
																	'quantity' => $val['OrderLocation']['quantity'],
																	'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
			}
						
			/* dome pdf vendor */
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();
			$dompdf->set_paper('A4', 'portrait');
			/* Html for print pick list */
			$date = date("m/d/Y H:i:s");	
			$finArray = array();
			$html = '<h3>PickList :- '.$date.'</h3><hr><h3>Total Item  : - '.$tq.'</h3>
					<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
					<tr>
					<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
					<th style="border:1px solid;" width="10%" align="center">SKU</th>
					<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
					<th style="border:1px solid;" width="10%" align="center">Barcode</th>
					</tr>';
			
			$data = $this->msortnew($data, array('bin_location'));
			$tdata = array();
			foreach($data as $a){
				$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
										
			}			
			
			foreach($tdata as $sku => $d){
					$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
					foreach($d as $a){
						$bn[$a['bin_location']][] = $a['quantity'];	
						$bna[$a['bin_location']][] = $a['available_qty_bin'];	
						$bar[$a['barcode']] = $a['barcode'];	
					}
					$bins = array_keys($bn);  $oversell = array();
					foreach($bins as $b){
						if($b !=''){
							if(array_sum($bn[$b]) == array_sum($bna[$b])){
								$bin_loc[]= $b."(".array_sum($bna[$b]).")";
							}else{
								$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
							}
						}
						$qty[]= array_sum($bn[$b]);
						$avaqty[]= array_sum($bna[$b]);
						
					}	
					$oversell_str = '';
					if(count($oversell) > 0){
						$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
					}					
					$html.=	'<tr>
						<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
						<td style="border:1px solid;">'.$sku.'</td>
						<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.substr($products[$sku], 0, 40 ).'</td>
						<td style="border:1px solid;">'.implode(",",$bar).'</td>							
						</tr>';
						
			}	
		
			 $html .= '</table>';			 
			$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
			$dompdf->render();
			
			date_default_timezone_set('Europe/Jersey');
			$today = date("d_m_Y_H_i");
			//$dompdf->stream("Pick_List_(".$date.").pdf");
			
			$imgPath = WWW_ROOT .'img/printPickList/'; 
			$path = Router::url('/', true).'img/printPickList/';
			$name	=	'Pick_List__'.$today.'.pdf';
			
			file_put_contents($imgPath.$name, $dompdf->output());
			
			if(count($orderids) > 0){
				if(count($orderids) > 1){
					$this->MergeUpdate->updateAll(array('MergeUpdate.pick_list_status' => '1'), array('MergeUpdate.order_id IN ' => $orderids));
					$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1'), array('OrderLocation.order_id IN ' => $orderids));
				}else{
					$this->MergeUpdate->updateAll(array('MergeUpdate.pick_list_status' => '1'), array('MergeUpdate.order_id = ' => $orderids[0]));
					$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1'), array('OrderLocation.order_id = ' => $orderids[0]));
				}
			}
			$this->customLogGlobal( 'Pick List [ ' . $username . '__' . date( 'Y-m-d j:i:s' ) . ' ]' , implode( "," , $orderids ) );
			
			
			$folderPath = WWW_ROOT .'logs';
			$dir = new Folder($folderPath, true, 0755);			
			$_Path = $folderPath .'/'. date('dmy_Hi').'_Picklist.log';    				  
			file_put_contents($_Path, $username."\n".implode( "," , $orderids ));
				  
				  
			//now abort file status down
			$this->updateAbortFileAtLast();
			$fileStatus = 0;
			$msg['pdfurl'] = $serverPath  	= 	$path.$name ;
			$msg['msg'] = 'ok';
			echo json_encode($msg);
			exit;
				
		}else{
			$msg['msg'] = 'error';
			echo json_encode($msg);			
			 exit;
		}
		
	}
	public function msortnew($array, $key, $sort_flags = SORT_REGULAR) {
    if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    // @TODO This should be fixed, now it will be sorted as string
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
            asort($mapping, $sort_flags);
			natsort($mapping);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
	}
	
	public function royalMailErrorCheck($order_id = 1)
		{
			 $this->loadModel( 'RoyalmailError' ); 
			 $error_msg = null;
			 $rm_error	= $this->RoyalmailError->find('first', array('conditions' => array("RoyalmailError.split_order_id LIKE" => $order_id.'%'  )));
			 if(count($rm_error) > 0){
				 $error_msg = $rm_error['RoyalmailError']['error_msg'];
			 }
			 return $error_msg;
		 }
}
?>
