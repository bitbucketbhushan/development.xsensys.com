<?php
error_reporting(1); 
class OrderSplitController extends AppController
{  
    
    var $name = "OrderSplit";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('fetchOrders','fetchOrderItems'));		
			
    }
 	
  	public function index() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","Orders" );	
		
		$this->loadModel('OpenOrdersGeneralInfo'); 
		$this->loadModel('OpenOrdersItem');
		$this->paginate = array('order'=>'OpenOrdersGeneralInfo.id DESC','limit' => 2 );
		$all_orders = $this->paginate('OpenOrdersGeneralInfo');
		
		foreach($all_orders as $o ){ 
			$orders = $this->OpenOrdersItem->find('all', array('conditions'=>array('num_order_id'=>$o['OpenOrdersGeneralInfo']['num_order_id'])) );
			$order_item = array(); 
			foreach($orders as $or ){
				$order_item[$or['OpenOrdersItem']['order_id']][] = array('sku'=>$or['OpenOrdersItem']['sku'],'quantity'=>$or['OpenOrdersItem']['quantity'],'unit_cost'=>$or['OpenOrdersItem']['unit_cost']);
			
				/*if( $or['OpenOrdersItem']['quantity'] > 1){
					$sp = $this->splitOrder($or);
					pr($sp);
				}else{
				
				$price  = $or['OpenOrdersItem']['unit_cost'] * $or['OpenOrdersItem']['quantity'];;
				$spllit[$or['OpenOrdersItem']['order_id'].'-1'] = array($or['OpenOrdersItem']['sku'],$price,$or['OpenOrdersItem']['quantity']);
				pr($spllit);
				
				}*/
				 
			}
			
			$sp = $this->splitOrder($order_item);	pr($sp);
		}
		exit;
	} 
	
	public function splitOrder($orders) 
    { 
		pr($orders);
		$spllit = array();
		$spllits = array();
		foreach($orders as $order_id => $skudata ){
			
			$spllits = array();
			$skus = array();
			$price = $q=0;$count = 1;
			 
			/*for($s = 0; $s < count($skudata); $s++){  $q=1; 
				for($i = 1; $i <= $skudata[$s]['quantity']; $i++){
				
					$price = $price + $skudata[$s]['unit_cost'] ;
					if( $price <= 54.20){ 
						$q++;
						$count++;
						$split_id 			 = $order_id .'-'.$count;
						$spllits[$split_id] = array($skudata[$s]['sku'],$skudata[$s]['unit_cost']*$q,$q);
						
					} else{
						$count = 0;
						$q 	   = 1;
						$price = 0;
						$spllit[] = array($skudata[$s]['sku'],$skudata[$s]['unit_cost']*$q,$q);
					}
				}
				
				
				
				
		 }
				 
		 pr($spllits);	echo"=========<br>======"; pr($spllit);		
		 
		  exit;*/ 
			  foreach($skudata as $y => $sk ){
				 $price = 0;
				 $q = 1; 
				for($i = 1; $i <= $sk['quantity']; $i++){
					
					$price = $price + $sk['unit_cost'] ;
					
					if( $price <= 54.20){ 
						$split_id 			 = $order_id .'-'.$count;
						$spllits[$count][$y] = array($sk['sku'],$sk['unit_cost']*$q,$q);
						$q++;
					} else{
						
						$q = 1;
						$price = 0;
						$spllit[$count][$y] = array($sk['sku'],$sk['unit_cost']*$q,$q);
						$count++;
					}
				 }
			 } 
			 /*foreach($skudata as  $or ){
				 
				for($i = 1; $i <= $or['quantity']; $i++){
				 			 
						$skus[] = array('sku'=>$or['sku'],'unit_cost'=>$or['unit_cost']);
					 
				}
			}
			 pr($skus);$q = 0;
			 foreach($skus as  $sk ){
				$price = $price + $sk['unit_cost'] ;
				if($price > 0 && $price <= 54.20){ 
					$q++;
				 
					$split_id 			 = $order_id .'-'.$count;
					$spllits[$split_id][$sk['sku']] = array($sk['sku'],$sk['unit_cost']*$q,$q);
					
				} else{
					$count++;
					$q = 0;
					$price = 0;
				}
			 }*/ 
 			 
		
		   foreach($spllits as $sk ){ 
		   		 foreach($sk as $p ){ 
				 $t[]=  $p ;
				 }
		   }   
		   foreach($spllit as $sk ){ 
		   		 foreach($sk as $p ){ 
  					 $t[]=  $p ;
				 }
		   } 
		  }$count=1;$price = 0;
				 $q = 1; 
		    foreach($t as $y => $sk ){
				 
				 
					
					$price = $price + $sk['1'] ;
					
					if( $price <= 54.20){ 
						$split_id 			 = $order_id .'-'.$count;
						$_spllits[$split_id][] = array($sk['0'],$sk['1'],$sk['2']);
						$q++;
					} else{
						
						$q = 1;
						$price = 0;
						$_spllits[$split_id][] = array($sk['0'],$sk['1'],$sk['2']);
						$count++;
					}
				  
			 } 
			  pr($_spllits);	echo"=========<br>======"; 
		 // pr($tt);
		// array_push($t, $tt);  
		 pr($t);		
		
		return $spllit;  
				
	} 
	
	public function getSales() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Orders" );	
		$this->loadModel('FbaOrder'); 
		$this->loadModel('FbaOrderItem'); 
		
		 
		$order_items = array();
		$orders = array();
		$all_orders = array();
		$amazon_order_ids = array();
		$all_master_skus = array();
		if(isset($_REQUEST["report_year"])){
			$sdate = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-01';
			$edate = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-31';
		}else{
			$sdate = date('Y-m').'-01';
			$edate = date('Y-m-d', strtotime('last day of this month'));
		}
		
		$this->FbaOrder->unbindModel( array('hasMany' => array( 'FbaOrderItem' ) ) );
		
		$orders = $this->FbaOrder->find('all', array('conditions'=>array('FbaOrder.order_status !='=>'Canceled','FbaOrder.purchase_date BETWEEN ? AND ? ' => array($sdate,$edate)),'fields'=>array('amazon_order_id','purchase_date'),'order'=>'FbaOrder.id DESC') );
		
		if(count($orders ) > 0){
		
			foreach($orders as $order){
				$amazon_order_ids[] =	$order['FbaOrder']['amazon_order_id'];
			}
		 
			if(count($amazon_order_ids) > 0){
				
				 if(count($amazon_order_ids) == 1){
					 
					$this->paginate = array('conditions'=>array('FbaOrderItem.amazon_order_id ' => $amazon_order_ids[0]),
			'fields'=>array('id','amazon_order_id','asin','seller_sku','master_sku','quantity_ordered','quantity_shipped','item_price'),
			'group' =>'master_sku', 'order'=>'master_sku','limit' => 50 );
				 
				 }else{
				
					$this->paginate = array('conditions'=>array('FbaOrderItem.amazon_order_id IN ' => $amazon_order_ids),
			'fields'=>array('id','amazon_order_id','asin','seller_sku','master_sku','quantity_ordered','quantity_shipped','item_price'),
			'group' =>'master_sku','order'=>'master_sku','limit' => 50 );
			
				}			
			}
			
			$all_master_skus = $this->paginate('FbaOrderItem');
		}
		 
		$this->set( 'all_master_skus', $all_master_skus ); 
		
	}
	
	public function getSalesReport($report_year = null, $report_month = null) 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Orders" );	
		$this->loadModel('FbaOrder'); 
		$this->loadModel('FbaOrderItem'); 
		
		 
		$order_items = array();
		$amazon_order_ids = array();
		
		$sdate = $report_year.'-'.$report_month.'-01';
		$edate = $report_year.'-'.$report_month.'-31';
			
			
		$this->FbaOrder->unbindModel( array('hasMany' => array( 'FbaOrderItem' ) ) );
		
		$orders = $this->FbaOrder->find('all', array('conditions'=>array('FbaOrder.order_status !='=>'Canceled','FbaOrder.purchase_date BETWEEN ? AND ? ' => array($sdate, $edate )),'fields'=>array('amazon_order_id','purchase_date')) );

 
		foreach($orders as $order){
			$amazon_order_ids[] =	$order['FbaOrder']['amazon_order_id'];
		}
	 	
		$this->FbaOrderItem->unbindModel( array( 'belongsTo' => array( 'FbaOrder' ) ) );
		
		 if(count($amazon_order_ids) > 0){
			
			 if(count($amazon_order_ids) == 1){
					
				 $order_items = $this->FbaOrderItem->find('all', 
					array(
					'conditions'=>array('FbaOrderItem.amazon_order_id ' => $amazon_order_ids[0]), 
					'group' => 'FbaOrderItem.seller_sku',
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);	
			 
			 }else{
					
			 $order_items = $this->FbaOrderItem->find('all', 
					array(
					'conditions'=>array('FbaOrderItem.amazon_order_id IN ' => $amazon_order_ids),
					'group' => 'FbaOrderItem.seller_sku', 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);	
			}			
		}
	
		$sep = ",";
		$content = 'SKU';
		$file_name = 'fba_sale_'.$report_year.$report_month.'.csv'; 
		$days_of_month = cal_days_in_month(CAL_GREGORIAN, $report_month , $report_year);
		
		for ($i = 1; $i <= $days_of_month; $i++)
		{
			$date = $report_year.'-'.$report_month.'-'.str_pad($i, 2, "0", STR_PAD_LEFT);
			$d 	= date('d M Y', strtotime($date));
			$content .= $sep . $d;		
		}
		
		file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");
		
		if(count($order_items) > 0){
			foreach($order_items as $order){
				$content = $order['FbaOrderItem']['seller_sku'];  
				for ($i = 1; $i <= $days_of_month; $i++)
				{								
					$date = $report_year.'-'.$report_month.'-'.str_pad($i, 2, "0", STR_PAD_LEFT);					 	
					$sale = $this->getSkuSale($order['FbaOrderItem']['seller_sku'],$date) ? $this->getSkuSale($order['FbaOrderItem']['seller_sku'],$date) : '-';			
					$content .= $sep . $sale;
				}
				
				file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n", FILE_APPEND | LOCK_EX);				 
			}
		} 
		$result =  WWW_ROOT.'logs/'.$file_name; 
		header('Content-Description: File Transfer');
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename='.basename($result));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($result));   
		readfile($result); 
		exit;
	}
	
	public function getSkuSale($seller_sku = null,$date = null){
	
		$this->loadModel('FbaOrder'); 
		$this->loadModel('FbaOrderItem'); 
		$amazon_order_ids = array(); $order_items = array();
		$this->FbaOrder->unbindModel( array('hasMany' => array( 'FbaOrderItem' ) ) );
		
		$orders = $this->FbaOrder->find('all', array('conditions'=>array('FbaOrder.order_status !='=>'Canceled','FbaOrder.purchase_date LIKE ' => $date.'%'),'fields'=>array('amazon_order_id','purchase_date')) );

 
		foreach($orders as $order){
			$amazon_order_ids[] =	$order['FbaOrder']['amazon_order_id'];
		}
	 	
 		$this->FbaOrderItem->unbindModel( array( 'belongsTo' => array( 'FbaOrder' ) ) );
		
		 if(count($amazon_order_ids) > 0){
			
			 if(count($amazon_order_ids) == 1){
					
				 $order_items = $this->FbaOrderItem->find('all', 
					array(
					'conditions'=>array('FbaOrderItem.amazon_order_id ' => $amazon_order_ids[0],'seller_sku' => $seller_sku), 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);	
			 
			 }else{
					
			 $order_items = $this->FbaOrderItem->find('all', 
					array(
					'conditions'=>array('FbaOrderItem.amazon_order_id IN ' => $amazon_order_ids,'seller_sku' => $seller_sku), 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);	
			}			
		}
	
		$sales_qty = 0;
		if(count($order_items) > 0){
			foreach($order_items as $order){
				$sales_qty += $order['FbaOrderItem']['quantity_ordered'];
			}
		} 
		return  $sales_qty;
		 
	} 
	public function getSellerSkuSale($master_sku = null,$date = null){
	
		$this->loadModel('FbaOrder'); 
		$this->loadModel('FbaOrderItem'); 
		$amazon_order_ids = array(); $order_items = array();
		$this->FbaOrder->unbindModel( array('hasMany' => array( 'FbaOrderItem' ) ) );
		
		
		if(isset($_REQUEST["report_year"])){
			$sdate = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-01';
			$edate = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-31';
		}else{
			$sdate = date('Y-m').'-01';
			$edate = date('Y-m-d', strtotime('last day of this month'));
		}
		
		$this->FbaOrder->unbindModel( array('hasMany' => array( 'FbaOrderItem' ) ) );
		
		$orders = $this->FbaOrder->find('all', array('conditions'=>array('FbaOrder.order_status !='=>'Canceled','FbaOrder.purchase_date BETWEEN ? AND ? ' => array($sdate,$edate)),'fields'=>array('amazon_order_id','purchase_date'),'order'=>'FbaOrder.id DESC') );
		
		 
 
		foreach($orders as $order){
			$amazon_order_ids[] =	$order['FbaOrder']['amazon_order_id'];
		}
	 	
 		$this->FbaOrderItem->unbindModel( array( 'belongsTo' => array( 'FbaOrder' ) ) );
		
		 if(count($amazon_order_ids) > 0){
			
			 if(count($amazon_order_ids) == 1){
					
				 $order_items = $this->FbaOrderItem->find('all', 
					array(
					'conditions'=>array('FbaOrderItem.amazon_order_id ' => $amazon_order_ids[0],'master_sku' => $master_sku), 
					'group' => 'seller_sku',
					'fields'=>array('amazon_order_id','asin','seller_sku','master_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);	
			 
			 }else{
					
			 $order_items = $this->FbaOrderItem->find('all', 
					array(
					'conditions'=>array('FbaOrderItem.amazon_order_id IN ' => $amazon_order_ids,'master_sku' => $master_sku), 
					'group' => 'seller_sku',
					'fields'=>array('amazon_order_id','asin','seller_sku','master_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);	
			}			
		}
	
		$sales_qty = 0; $arr = array();
		if(count($order_items) > 0){
			foreach($order_items as $order){
			
			$arr[] = array('master_sku' => $order['FbaOrderItem']['master_sku'],'qty_ordered' => $order['FbaOrderItem']['quantity_ordered'],'seller_sku' => $order['FbaOrderItem']['seller_sku'] );
			
				//$sales_qty += $order['FbaOrderItem']['quantity_ordered'];
			}
		} 
		return  $arr;
		 
	} 
	
	public function getDateSale($date = null){
	
		$this->loadModel('FbaOrder'); 
		$this->loadModel('FbaOrderItem'); 
		$amazon_order_ids = array(); $order_items = array();
		$this->FbaOrder->unbindModel( array('hasMany' => array( 'FbaOrderItem' ) ) );
		
		$orders = $this->FbaOrder->find('all', array('conditions'=>array('FbaOrder.order_status !='=>'Canceled','FbaOrder.purchase_date LIKE ' => $date.'%'),'fields'=>array('amazon_order_id','purchase_date')) );

 
		foreach($orders as $order){
			$amazon_order_ids[] =	$order['FbaOrder']['amazon_order_id'];
		}
	
		$this->FbaOrderItem->unbindModel( array( 'belongsTo' => array( 'FbaOrder' ) ) );
		
		 if(count($amazon_order_ids) > 0){
			
			 if(count($amazon_order_ids) == 1){
				 $order_items = $this->FbaOrderItem->find('all', 
					array(
					'conditions'=>array('FbaOrderItem.amazon_order_id ' => $amazon_order_ids[0]), 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);
			 
			 }else{
				$order_items = $this->FbaOrderItem->find('all', 
					array(
					'conditions'=>array('FbaOrderItem.amazon_order_id IN  ' => $amazon_order_ids), 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);
			}			
		}
	
		$sales_qty = 0;
		
		if(count($order_items) > 0){
			foreach($order_items as $order){
				$sales_qty += $order['FbaOrderItem']['quantity_ordered'];
			}
		} 
		return  $sales_qty;
		 
	
	}
	
	public function getSaleReport($month = null, $year = null){
	
		$this->loadModel('FbaOrder'); 
		$this->loadModel('FbaOrderItem'); 
		$amazon_order_ids = array(); $order_items = array();
		$this->FbaOrder->unbindModel( array('hasMany' => array( 'FbaOrderItem' ) ) );
		
		$orders = $this->FbaOrder->find('all', array('conditions'=>array('FbaOrder.order_status !='=>'Canceled','FbaOrder.purchase_date LIKE ' => $date.'%'),'fields'=>array('amazon_order_id','purchase_date')) );

 
		foreach($orders as $order){
			$amazon_order_ids[] =	$order['FbaOrder']['amazon_order_id'];
		}
	
		$this->FbaOrderItem->unbindModel( array( 'belongsTo' => array( 'FbaOrder' ) ) );
		
		 if(count($amazon_order_ids) > 0){
			
			 if(count($amazon_order_ids) == 1){
				 $order_items = $this->FbaOrderItem->find('all', 
					array(
					'conditions'=>array('FbaOrderItem.amazon_order_id ' => $amazon_order_ids[0]), 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);
			 
			 }else{
				$order_items = $this->FbaOrderItem->find('all', 
					array(
					'conditions'=>array('FbaOrderItem.amazon_order_id IN  ' => $amazon_order_ids), 
					'fields'=>array('amazon_order_id','asin','seller_sku','quantity_ordered','quantity_shipped','item_price')
					)
				);
			}			
		}
	
		$sales_qty = 0;
		
		if(count($order_items) > 0){
			foreach($order_items as $order){
				$sales_qty += $order['FbaOrderItem']['quantity_ordered'];
			}
		} 
		return  $sales_qty;	
	}
	
	
	public function fetchOrders( ) 
	{	
		
		$this->loadModel('FbaOrder');	 
		
		$url = 'http://cost-dropper.com/Webservice/getFbaOrders'; 
		$curl = curl_init(); 
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FAILONERROR, true); 
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
		
		$result = curl_exec($curl); 						 
		$error = curl_error($curl); 
		$info = curl_getinfo($curl);
		curl_close($curl);	
		$res = json_decode($result,0);
		 
		//pr($res);exit;
	
		foreach($res as $val)
		{			
			//pr($order);
			$order = $val->FbaOrder;
			$savedata = array();					
			$savedata['last_update_date'] 	= gmdate("Y-m-d H:i:s", strtotime($order->last_update_date)); 
			$savedata['purchase_date'] 		= gmdate("Y-m-d H:i:s", strtotime($order->purchase_date));
			$savedata['order_status'] 		= $order->order_status;
			$savedata['fulfillment_channel']= $order->fulfillment_channel;
			$savedata['sales_channel'] 		= $order->sales_channel;					
			$savedata['ship_service_level'] = $order->ship_service_level;
			$savedata['number_of_items'] 	= $order->number_of_items;
			$savedata['order_type']			= $order->order_type;
			$savedata['earliest_ship_date'] = $order->earliest_ship_date;
			$savedata['is_prime'] 			= $order->is_prime;
			$savedata['is_replacement_order']= $order->is_replacement_order;	
			
			 
			$savedata['name']				= $order->name;
			$savedata['address_line_1'] 	= $order->address_line_1;
			$savedata['address_line_2'] 	= $order->address_line_2;
			$savedata['city'] 				= $order->city;
			$savedata['region'] 			= $order->region;
			$savedata['post_code'] 			= $order->post_code;
			$savedata['country_code'] 		= $order->country_code;
			$savedata['order_total_amount'] = $order->order_total_amount;
			$savedata['order_total_currency']= $order->order_total_currency;
			$savedata['buyer_email'] 		= $order->buyer_email;
			$savedata['buyer_name'] 		= $order->buyer_name; 	
			 	  			  			  
			/* echo "<pre>";
			 print_r($savedata);
			 echo "</pre>";   */            
			$fba_order = $this->FbaOrder->find('first', array('conditions'=>array('FbaOrder.amazon_order_id' => $order->amazon_order_id)) );
				
			if(count($fba_order) > 0){
				if($fba_order['FbaOrder']['order_status'] != $order->order_status){
					$savedata['id'] = $fba_order['FbaOrder']['id'];	
					$this->FbaOrder->saveAll( $savedata );	
					$this->fetchOrderItems($val->FbaOrderItem,$fba_order['FbaOrder']['id']);	   												 
				}
			}else{
				$savedata['seller_order_id'] 	= $order->seller_order_id;				
				$savedata['amazon_order_id'] 	= $order->amazon_order_id;
				$savedata['added_date'] 		= date('Y-m-d H:i:s');							
				$this->FbaOrder->saveAll( $savedata );
				$fba_order_inc_id	 = $this->FbaOrder->getLastInsertId();
				$this->fetchOrderItems($val->FbaOrderItem,$fba_order_inc_id);	 									
			}                                  
		}
		exit;
	} 
	
	public function fetchOrderItems($OrderItem = null, $fba_order_inc_id = 0) 
	{ 	
			if(isset($OrderItem)){
				$this->loadModel('FbaOrderItem');	
				foreach($OrderItem as $items)
				{			
 					$savedata = array();	
					$master = $this->getSkuMapping($items->seller_sku);				
					$savedata['amazon_order_id'] 	= $items->amazon_order_id;
					$savedata['asin'] 				= $items->asin;
					$savedata['seller_sku']			= $items->seller_sku;
					$savedata['master_sku']			= $master['sku']; 
					$savedata['order_item_id'] 		= $items->order_item_id;	
					$savedata['title'] 				= $items->title;	
					$savedata['quantity_ordered'] 	= $items->quantity_ordered;	
					$savedata['quantity_shipped'] 	= $items->quantity_shipped;	
					$savedata['number_of_items'] 	= $items->number_of_items;	
					
					if(isset($items->item_price)){
						$savedata['currency_code'] 	= $items->currency_code;	
						$savedata['item_price'] 	= $items->item_price;	
					}
					if(isset($items->shipping_price)){
						//$savedata['number_of_items'] 	= $items->ShippingPrice->CurrencyCode;	
						$savedata['shipping_price'] 	= $items->shipping_price;	
					}
					if(isset($items->item_tax)){
						//$savedata['number_of_items'] 	= $items->ItemTax->CurrencyCode;	
						$savedata['item_tax'] 	= $items->item_tax;
					}
					if(isset($items->shipping_tax)){
							//$savedata['number_of_items'] 	= $items->ShippingTax->CurrencyCode;	
						$savedata['shipping_tax'] 	= $items->shipping_tax;
					}
					if(isset($items->shipping_discount)){
						//$savedata['number_of_items'] 	= $items->ShippingDiscount->CurrencyCode;	
						$savedata['shipping_discount'] 	= $items->shipping_discount;
					}
					
					$savedata['promotion_ids'] 		= $items->promotion_ids;
					if($fba_order_inc_id > 0){
						$savedata['fba_order_inc_id'] 	= $fba_order_inc_id;	
					}	
					 
					
					$fba_order_item = $this->FbaOrderItem->find('first', array('conditions'=>array('FbaOrderItem.amazon_order_id' => $items->amazon_order_id,'order_item_id' => $items->order_item_id)) );
					
					if(count($fba_order_item) > 0){
						$savedata['id'] = $fba_order_item['FbaOrderItem']['id'];						
					}else{					
						$savedata['added_date'] 		= date('Y-m-d H:i:s');														
					}				
					$this->FbaOrderItem->saveAll( $savedata );	 
				}						
		 	}	
	}
	
	public function getSkuMapping($seller_sku = null)
	{		
		$this->loadModel( 'Skumapping' );	
		$_items = $this->Skumapping->find('first', array('conditions'=>array('Skumapping.channel_sku'=>$seller_sku)));
		if(count($_items) > 0){
			return  array('sku'=>$_items['Skumapping']['sku'],'barcode'=>$_items['Skumapping']['barcode']);
		}else{
			return 0;
		}
	}
	 
}