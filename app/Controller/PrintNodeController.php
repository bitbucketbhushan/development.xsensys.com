<?php
class PrintNodeController extends AppController
{
    
    var $name = "PrintNode";
	 var $components = array('Session','Upload','Common','Auth');
    var $helpers = array('Html','Form');   
	
	 public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('getPrinters'));
			//die('maintenance going on!');
    }
	public function changePrinter()
    { 
		$this->layout = "";	 
		$this->loadModel( 'PcStore' );

		if(isset($this->request->data['printer_id']) && $this->request->data['printer_id'] != ''){
			
			
			$key = base64_encode('72b154e71a740ba5ca548ca045c38c9f789e0284');
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.printnode.com/printers",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  
			  CURLOPT_HTTPHEADER => array(
				"Accept: */*",
				"Accept-Encoding: gzip, deflate",
				"Authorization: Basic {$key}",
				"Cache-Control: no-cache",
				"Connection: keep-alive",
				"Content-Type: application/json",
				"Host: api.printnode.com",
				"cache-control: no-cache"
			  ),
			));
			
			$response = curl_exec($curl);
		 	$err = curl_error($curl);
			
			curl_close($curl);
			
		 	$this->PcStore->query("TRUNCATE pc_stores");
			$mesg = 'Printer is changed.'; $found = 0;
			foreach(json_decode($response,1) as $p){
			
				if($this->request->data['printer_id'] == $p['id'] &&  $p['computer']['state'] == 'connected'){
					$_SESSION['printer_id'] = $this->request->data['printer_id'];
					$this->Session->write('printer_id',$this->request->data['printer_id']);
					$found++;
				}
				$sdata['pc_name'] = $p['computer']['name'];
				$sdata['pc_id'] = $p['computer']['id'];
				$sdata['printer_id'] = $p['id'];
				$sdata['printer_name'] = $p['name'];
				$sdata['inet_address'] = $p['computer']['inet'];
				$sdata['pc_state'] = $p['computer']['state'];
				$sdata['printer_state'] = $p['state'];
				$sdata['added_date'] = date('Y-m-d H:i:s');
				 
			 	$this->PcStore->saveAll($sdata);
			
			}
			
			if($found == 0){
				$mesg = 'This printer is not available!';
			}
			
			$msg['msg'] = $mesg  ;
			echo json_encode($msg);
		
	
		}
		exit;
 	}
	
	public function getPrinters()
    { 
	
		$this->layout = "";	 
	
		$this->loadModel( 'PcStore' );
		
			
			$key = base64_encode('72b154e71a740ba5ca548ca045c38c9f789e0284');
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.printnode.com/printers?dir=asc&limit=200",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  
			  CURLOPT_HTTPHEADER => array(
				"Accept: */*",
				"Accept-Encoding: gzip, deflate",
				"Authorization: Basic {$key}",
				"Cache-Control: no-cache",
				"Connection: keep-alive",
				"Content-Type: application/json",
				"Host: api.printnode.com",
				"cache-control: no-cache"
			  ),
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
		 	$this->PcStore->query("TRUNCATE pc_stores");
			//echo count(json_decode($response,1));
			foreach(json_decode($response,1) as $p){
				/*echo $p['state'];
				echo " = " . $p['name'];
				echo " = " . $p['id'];
				echo " = " . $p['description'];
				echo "<br>";*/
				//SELECT *  FROM `users` WHERE `username` LIKE '%-1' AND `country` = 1 AND `is_deleted` = 0
				 
				$sdata['pc_name'] = $p['computer']['name'];
				$sdata['pc_id'] = $p['computer']['id'];
				$sdata['printer_id'] = $p['id'];
				$sdata['printer_name'] = $p['name'];
				$sdata['inet_address'] = $p['computer']['inet'];
				$sdata['pc_state'] = $p['computer']['state'];
				$sdata['printer_state'] = $p['state'];
				$sdata['timestamp'] = date('Y-m-d H:i:s');
  				//	pr($sdata);
			 	$this->PcStore->saveAll($sdata);
			
			}
			//pr(json_decode($response,1));
		 exit;
	} 
	
	public function getcomputers()
    { 
	
		$this->layout = "";	 
	
		$this->loadModel( 'PcStore' );
		
			
			$key = base64_encode('72b154e71a740ba5ca548ca045c38c9f789e0284');
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.printnode.com/computers",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  
			  CURLOPT_HTTPHEADER => array(
				"Accept: */*",
				"Accept-Encoding: gzip, deflate",
				"Authorization: Basic {$key}",
				"Cache-Control: no-cache",
				"Connection: keep-alive",
				"Content-Type: application/json",
				"Host: api.printnode.com",
				"cache-control: no-cache"
			  ),
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			//$this->PcStore->query("TRUNCATE pc_stores");
			
		echo count(json_decode($response,1));
		
			foreach(json_decode($response,1) as $p){
				/*echo $p['state'];
				echo " = " . $p['name'];
				echo " = " . $p['id'];
				echo " = " . $p['description'];
				echo "<br>";*/
				//SELECT *  FROM `users` WHERE `username` LIKE '%-1' AND `country` = 1 AND `is_deleted` = 0
				 
				$sdata['pc_name'] = $p['computer']['name'];
				$sdata['pc_id'] = $p['computer']['id'];
				$sdata['printer_id'] = $p['id'];
				$sdata['printer_name'] = $p['name'];
				$sdata['inet_address'] = $p['computer']['inet'];
				$sdata['pc_state'] = $p['computer']['state'];
				$sdata['printer_state'] = $p['state'];
				$sdata['added_date'] = date('Y-m-d H:i:s');
				pr($sdata);
			//	$this->PcStore->saveAll($sdata);
			
			}
			//pr(json_decode($response,1));
		//exit;
	} 
		
	public function RefreshPrinters()
    { 
 		$this->layout = "";	 
 		$this->loadModel( 'PcStore' );
		
		$key = base64_encode('P66MAw5r6wzAKvBwMPRa5_BkjoCA8mYl7fgK4WWTfJs');
		
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.printnode.com/printers",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  
		  CURLOPT_HTTPHEADER => array(
			"Accept: */*",
			"Accept-Encoding: gzip, deflate",
			"Authorization: Basic {$key}",
			"Cache-Control: no-cache",
			"Connection: keep-alive",
			"Content-Type: application/json",
			"Host: api.printnode.com",
			"cache-control: no-cache"
		  ),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		
		$this->PcStore->query("TRUNCATE pc_stores");
	
		foreach(json_decode($response,1) as $p){
			$sdata['pc_name'] = $p['computer']['name'];
			$sdata['pc_id'] = $p['computer']['id'];
			$sdata['printer_id'] = $p['id'];
			$sdata['printer_name'] = $p['name'];
			$sdata['inet_address'] = $p['computer']['inet'];
			$sdata['pc_state'] = $p['computer']['state'];
			$sdata['printer_state'] = $p['state'];
			$sdata['added_date'] = date('Y-m-d H:i:s');
			$this->PcStore->saveAll($sdata);
		
		}
		$this->Session->setflash( 'List of printer refreshed successfully.', 'flash_success'); 
 		$this->redirect($this->referer());	
	}		
}

?>
