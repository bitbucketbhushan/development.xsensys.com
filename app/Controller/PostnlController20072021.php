<?php
error_reporting(1);
class PostnlController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Postnl";
    var $components = array('Session','Upload','Common','Auth','Paginator');
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
	public function beforeFilter()
    {
		parent::beforeFilter();
		$this->layout = false;
		$this->Auth->Allow(array('Barcode'));
		$this->Common = $this->Components->load('Common');   
    }
	public function index($order_id)
    {		
		$this->loadModel('OpenOrder');
		$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id,'OpenOrder.destination NOT IN' => ['Italy','United Kingdom','Netherlands','Brazil'] ) ) );
		
		$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
		$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
		$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
	//	pr($general_info);	
		pr($customer_info);	
		//pr($totals_info);	
	echo	 $_address1 = $customer_info->Address->Address1;
		 echo " == ";
	echo	  $this->replaceChar(ltrim($_address1,","));
				
		exit;
	}
	
    public function Barcode($Type='UE', $Serie='00000000-99999999', $Range='NL' )
    {
			$this->layout = '';
			$this->autoRender = false;
			$auth = Configure::read( 'postnl' );
			$CustomerCode = $auth['customer_code'];
			$CustomerNumber = $auth['customer_number'];
			$api_key = $auth['api_key'];
			$server_url = $auth['server_url'];
			// $url = "https://api-sandbox.postnl.nl/shipment/v1_1/barcode?CustomerCode={$CustomerCode}&CustomerNumber={$CustomerNumber}&Type=3S&Serie=0000000-9999999";
			$url = $server_url."/shipment/v1_1/barcode?CustomerCode={$CustomerCode}&CustomerNumber={$CustomerNumber}&Type={$Type}&Serie={$Serie}&Range={$Range}";
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(				 
				"apikey: {$api_key}",
				"cache-control: no-cache"
			  ),
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  //echo "cURL Error #:" . $err;
			  	$data['error_code'] = $err;
				$data['error_msg']  = 'Barcode not generated';
				$this->sendErrorMail($data);
			} else {
			  $r = json_decode($response,1);
			  return $r['Barcode'];
			}
		exit;  
	}
 
 	public function Labelling($order_id)
 	{
		$this->loadModel('MergeUpdate'); 
		$this->loadModel('OpenOrder');
		$this->loadModel('Country');
		$this->loadModel('Product');
		$this->loadModel('ProductHscode');  
		$country_of_origin = 'CN';
		$hs_code = '9603298000';//'8470300000';
		$product_name = '';		
 			
		$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $order_id, 'MergeUpdate.service_provider' => 'PostNL','MergeUpdate.brt' => 0,'MergeUpdate.country_code NOT IN' => ['IT','NL']),'fields'=>array('sku','product_order_id_identify','order_id','postnl_barcode','country_code','provider_ref_code','status','postal_service') ) );
		
		/*if(count($orders) == 0){
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id,'OpenOrder.destination NOT IN' => ['Italy','Netherlands'] ) ) );
			$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
		}*/ 
		
  //pr($orders	);
 /*$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $order_id, 'service_provider'=>'PostNL','delevery_country !='=>'Netherlands'),'fields'=>array('sku','product_order_id_identify','order_id','postnl_barcode','country_code','provider_ref_code') ) );
 */
		foreach($orders	 as $v){
			$provider_ref_code =  $v['MergeUpdate']['provider_ref_code'];
			
 			$this->getBarcodeOutside($v['MergeUpdate']['product_order_id_identify']);
 					
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $v['MergeUpdate']['order_id'],'OpenOrder.destination NOT IN' => ['Italy','Netherlands'] ) ) );
			if(count($openOrder) > 0){
			
				$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
				$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
				$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
				// pr( unserialize( $openOrder['OpenOrder']['items']));
				
				$totals_info->Subtotal;
				$totals_info->PostageCost;
				$totals_info->Tax;		
				$totals_info->Currency; 
				
				$country	=	$this->Country->find('first', array( 'conditions' => array('name' =>trim($customer_info->Address->Country)) ) );
				if(count($country) == 0){
					$country	=	$this->Country->find('first', array( 'conditions' => array('custom_name' =>trim($customer_info->Address->Country)) ) );
				}
				
				if(count($country) > 0){
					$postnl_barcode =  $v['MergeUpdate']['postnl_barcode']; 
					
					$pos = strpos($v['MergeUpdate']['sku'],",");
					
					if ($pos === false) {
							$val  = $v['MergeUpdate']['sku'];			
							$s    = explode("XS-", $val);
							$_qqty = $s[0];
							$_sku = "S-".$s[1];		
								
							$product = $this->Product->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.product_name','Product.country_of_origin')));
							
							$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => $_sku,'country_code' =>  $country['Country']['iso_2'])));
							if(count($hscode) == 0){
								$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' =>  $country['Country']['iso_2'])));
							}
							if(count($hscode) == 0){
								$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' => 'GB')));
							}
							if(count($hscode) > 0){
								if($hscode['ProductHscode']['hs_code'] != ''){
									$hs_code = $hscode['ProductHscode']['hs_code'];
								}
							}
							
							if($product['Product']['country_of_origin'] != ''){
								$c_code= $this->Country->find('first',array('conditions' => array('Country.name' => $product['Product']['country_of_origin']),'fields'=>array('Country.iso_2'))); 
  								if(count($c_code) > 0){
  									$country_of_origin = $c_code['Country']['iso_2'];
								}
							}
							
							$product_name = $product['Product']['product_name'];
							$length = $product['ProductDesc']['length'] / 10;
							$width  = $product['ProductDesc']['width'] / 10;
							$height = $product['ProductDesc']['height'] / 10;
							$weight = ($product['ProductDesc']['weight'] * $_qqty ) * 1000;
									
						}else{	
							$_length = []; $_width = []; $_height = []; $_weight = [];		
							$sks = explode(",",$v['MergeUpdate']['sku']);
							foreach($sks as $val){
								$s    = explode("XS-", $val);
								$_qqty = $s[0];
								$_sku = "S-".$s[1];	
								
								$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => $_sku,'country_code' =>  $country['Country']['iso_2'])));
								if(count($hscode) == 0){
									$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' =>  $country['Country']['iso_2'])));
								}
								if(count($hscode) == 0){
									$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' => 'GB')));
								}
								if(count($hscode) > 0){
									if($hscode['ProductHscode']['hs_code'] != ''){
										$hs_code = $hscode['ProductHscode']['hs_code'];
									}
								}
								
								$product = $this->Product->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.product_name','Product.country_of_origin')));
								if($product['Product']['country_of_origin'] != ''){
									$c_code= $this->Country->find('first',array('conditions' => array('Country.name' => $product['Product']['country_of_origin']),'fields'=>array('Country.iso_2'))); 
									if(count($c_code) > 0){
										$country_of_origin = $c_code['Country']['iso_2'];
									}
								}
								$product_name = $product['Product']['product_name'];
								$_length[] = $product['ProductDesc']['length'];
								$_width[]  = $product['ProductDesc']['width'];
								$_height[] = $product['ProductDesc']['height'];
								$_weight[] = $product['ProductDesc']['weight'] * $_qqty;					
							}		
							$length	= array_sum($_length) / 10;
							$width	= array_sum($_width) / 10;
							$height	= array_sum($_height) / 10;
							$weight	= array_sum($_weight) * 1000;	 
					}
					$data['total_charge']    	=  $totals_info->TotalCharge ;
					$data['hs_code']  		 	=  $hs_code ;
					$data['country_of_origin']  =  $country_of_origin ;
					$data['product_name']   	=  $product_name ;
					$data['eu_country']   		=  $country['Country']['eu_country'];
					$data['provider_ref_code']  =  $provider_ref_code ;
					$data['phone_number']   	=  $customer_info->Address->PhoneNumber ;
					$data['postnl_barcode'] 	=  $v['MergeUpdate']['postnl_barcode'];	
					//$weight 					=  str_replace(".","",$weight);
					$weight 					=  explode(".",$weight)[0];
					$data['weight'] 			=  str_pad($weight,4,'0',STR_PAD_LEFT) ;	
					$data['country_code'] 		=  $country['Country']['iso_2'] ;	
					$data['country'] 			=  $customer_info->Address->Country;
					$data['sub_source'] 		=  strtolower($openOrder['OpenOrder']['sub_source']) ;
					$data['split_order_id'] 	=  $v['MergeUpdate']['product_order_id_identify'] ;		
					$data['status']				=  $v['MergeUpdate']['status'] ;
					$data['postal_service']		=  $v['MergeUpdate']['postal_service'] ;	
					$this->getLabel($data,$customer_info);
				
				}
				else{
					 file_put_contents(WWW_ROOT.'logs/postnl_api_country_'.date('dmY').'.log','Case_1'."\t".$data['split_order_id']."\t".$customer_info->Address->Country."\r\n", FILE_APPEND | LOCK_EX);	
					 
					$data['error_code'] = 'country not found';
					$data['error_msg']  = $customer_info->Address->Country;
 					$this->sendErrorMail($data);
			
				}
 			} 
			// exit;
		}
		
		return 1;
	}  
	
 	public function getLabel($data, $customer_info)
	{
		$this->loadModel('PostnlBarcode');
		$this->loadModel('MergeUpdate');	  
		
		$EmailAddress = $customer_info->Address->EmailAddress;
 		$exp = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = strtoupper($exp[$index]);
		
		$sender = $this->getSenderInfo($data,$store_country);
 		/*$CustomerCode = 'HLRQ';
		$CustomerNumber = '10472686';
		$api_key = 'Z7J23nyK3BOEPkR3pgIVAAcOKheVCTDV';*/
 		$auth = Configure::read( 'postnl' );
		$CustomerCode = $auth['customer_code'];
		$CustomerNumber = $auth['customer_number'];
		$api_key = $auth['api_key'];
 		$server_url = $auth['server_url'];
		
	 	//$ProductCodeDelivery = '6945';//'4944';
		if($data['phone_number'] != ''){ 		
			$pdata['Shipments'][0]['Contacts'][0]['TelNr'] =  $data['phone_number'];	
			$pdata['Shipments'][0]['Contacts'][0]['SMSNr'] =  $data['phone_number'];							 
		}
		
		$barcode_type = 'UE'; $ProductCodeDelivery = '6905';
		if($data['provider_ref_code'] == 'sizeg-boxable'){
			$ProductCodeDelivery = '6945';
		}
		if(in_array($data['provider_ref_code'],['sizeg-reg','sizee-reg'])){
			$ProductCodeDelivery = '6908';
			$barcode_type = 'RI';
 		}
 		if($data['provider_ref_code'] == 'sizee-nonboxable'){
			$ProductCodeDelivery = '6905';
		}
		
		if($data['eu_country'] == 0){
			$ProductCodeDelivery = '6908';

			$barcode_type = 'RI';
		}
		/*pr($data);
		echo $barcode_type; 
		echo ' = ';
		echo $ProductCodeDelivery;
		exit;*/
		
		if(is_null($data['postnl_barcode']) || $data['postnl_barcode'] == ''){
			$postnl_barcode =  $this->Barcode($barcode_type);
			$this->MergeUpdate->updateAll( array( 'MergeUpdate.postnl_barcode' => "'".$postnl_barcode."'") , array( 'MergeUpdate.product_order_id_identify' => $data['split_order_id'] ) );	
		}else{
 			$postnl_barcode =  $data['postnl_barcode'];
		}
		 //|| in_array($data['split_order_id'],['325988712072020-1','320649412072020-1','3354815-1'])
		$customs =	array();						 
		if($data['eu_country'] == 0 || in_array($data['split_order_id'],['330668112052020-1','323710212052020-1','329867112062020-1','319999712062020-1','320649412072020-1','325988712072020-1','3394947-1'])){
 			$customs = array (
					'Content' => 
					array (
					  0 => 
					  array (
						'CountryOfOrigin' =>  $data['country_of_origin'],
						'Description' =>  substr($data['product_name'],0,30),
						'HSTariffNr' =>  substr($data['hs_code'],0,6),
						'Quantity' => '1',
						'Value' => $data['total_charge'],
						'Weight' => $data['weight'],
					  ),
					),	
					'Currency' => 'EUR',				 
					'HandleAsNonDeliverable' => 'false',
					'Invoice' => 'true',
					'InvoiceNr' => $data['split_order_id'],
					'TransactionCode' => '011',
					'TransactionDescription' => 'Sale of Goods',
					'ShipmentType' => 'Commercial Goods',
 				  ) ;
		}
		
		preg_match_all('/([\d]+)/', $customer_info->Address->Address1, $match);
    	$house_nr = $match[0][0];
		$_address1 = str_replace($house_nr,' ', $this->replaceChar($customer_info->Address->Address1));
		$_address1 = ltrim($_address1,",");
		$address1 = ''; $address2  = ''; $address3 = '';
		$lines = explode("\n", wordwrap(htmlentities($_address1).' '.htmlentities($this->replaceChar($customer_info->Address->Address2)).' '.htmlentities($this->replaceChar($customer_info->Address->Address3)), '30'));
		
		if(isset($lines[0]) && $lines[0] != ''){
			$address1  =  trim($lines[0]) ;
		}
		if(isset($lines[1]) && $lines[1] != ''){
			$address2 =  trim($lines[1]) ;
		}
		if(isset($lines[2]) && $lines[2] != ''){
			$address3 = trim($lines[2]);
		}
		
		 $data['address1'] 		= $address1;
		 $data['address2'] 		= $address2;
		 $data['address3'] 		= $address3;
		 $contacts = [];
		 if(isset($customer_info->Address->PhoneNumber) && strlen($customer_info->Address->PhoneNumber) > 8){
  			 preg_match_all('/([\d]+)/', $customer_info->Address->PhoneNumber, $telmatch);
 			 $tel ='';
			 for($i = 0; $i< count($telmatch[0]); $i++){
				$tel .= $telmatch[0][$i];
 			 }
  			 $country_pcode = '';
			 if($customer_info->Address->Country == 'France'){
				 $country_pcode = '+33';
			 }else if($customer_info->Address->Country == 'Germany'){
				 $country_pcode = '+49';
			 }else if($customer_info->Address->Country == 'Spain'){
				 $country_pcode = '+34';
			 }else if($customer_info->Address->Country == 'Italy'){
				 $country_pcode = '+39';
			 }else if($customer_info->Address->Country == 'United Kingdom'){
				 $country_pcode = '+44';
			 }
			 if($country_pcode != ''){
				 $customer_phone_number = $country_pcode.substr($tel,-10);
			 }else{
			 	 $customer_phone_number = $tel;
			 }
			 
 			 $contacts = array(
						'ContactType' => 01,
						'Email' =>$customer_info->Address->EmailAddress,
						'TelNr' =>$customer_phone_number,
						'SMSNr' =>$customer_phone_number
					);
 		 }else{
		  $contacts = array(
						'ContactType' => 01,
						'Email' =>$customer_info->Address->EmailAddress 
					);
		 }
   
		//echo "<br>"; 
			$pdata = array(
			'Customer' => array(
					'Address' => array(
							'AddressType' => '02',
 							'Buildingname' => $sender['Building'],
							'City' => $sender['FromCity'],
							'CompanyName' => $sender['FromCompany'] ,
							'Countrycode' => $sender['FromCountryCode'],							 
							'Street' =>  $sender['FromAddress'],
							'Zipcode' => $sender['FromPostCode']
						),
					'Currency' => 'EUR',
					'CollectionLocation' => '888888',
					'ContactPerson' => $sender['FromPersonName'],
					'CustomerCode' => $CustomerCode ,
					'CustomerNumber' => $CustomerNumber,
					'Email' => $sender['email'],
					'Name' => $sender['FromPersonName']
				),
		
			'Message' => array(
					'MessageID' => $data['split_order_id'],
					'MessageTimeStamp' => date('d-m-Y H:i:s'),
					'Printertype' => 'GraphicFile|JPG 300 dpi'
				),
			
			'Shipments' => array(
					'0' => array(
							'Addresses' => array(
										'0' => array(  
  											'AddressType' => '01',
											'HouseNr' => $house_nr,
											'City' => $this->replaceChar($customer_info->Address->Town),
											'Countrycode' =>$data['country_code'], 
											'Name' => $this->replaceChar($customer_info->Address->FullName),
											'Region' =>$this->replaceChar($customer_info->Address->Region),
											'Street' => $this->getCountryAddress($data),
											'Zipcode' => $customer_info->Address->PostCode
										)
		
								),
		
							'Barcode' => $postnl_barcode ,
							'Contacts' => array(
									'0' => $contacts
 								),
		
							'Dimension' => array(
									'Weight' => $data['weight']
								),
							'Reference' =>$data['split_order_id'],
							'ProductCodeDelivery' => $ProductCodeDelivery  
						)		
				) 
			);
			
			if(count($customs) > 0){
 				 $pdata['Shipments'][0]['Customs'] =  $customs;
			} 
			 
			if($data['country'] == 'Brazil'){ 
 				$pdata['ImporterReferenceCode'] = $sender['vat_number'];
			}
			pr($pdata);
			$pdata['LabelSignature'] = base64_encode(file_get_contents(Router::url('/', true).'img/signature.gif'));
			
 			$savedata['split_order_id'] = $data['split_order_id'];
			$savedata['barcode'] 		= $postnl_barcode;
			$savedata['customer'] 		= json_encode($pdata['Customer']);
			$savedata['message'] 		= json_encode($pdata['Message']);
			$savedata['shipments'] 		= json_encode($pdata['Shipments']) ;
			$savedata['customs'] 		= count($customs);
			$savedata['timestamp'] 		= date('Y-m-d H:i:s');
			 
 			/*pr($savedata);
			exit;*/
			//pr($data);pr($pdata);
			$pnl = $this->PostnlBarcode->find('first',array('conditions' => array('split_order_id' => $data['split_order_id']),'fields'=>array('id','barcode')));
			if(count($pnl) > 0){
				$savedata['id'] =  $pnl['PostnlBarcode']['id'] ;
			} 
			
			$this->PostnlBarcode->saveAll($savedata); 
			 
			file_put_contents(WWW_ROOT.'postnl/request/request_'.$data['split_order_id'].'.json',json_encode($pdata));
  		 
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $server_url."/shipment/v2_2/label",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>json_encode($pdata),
			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"apikey: {$api_key}",
				"cache-control: no-cache"
			  ),
			));
			
			$response = curl_exec($curl);
			file_put_contents(WWW_ROOT.'postnl/response/response_'.$data['split_order_id'].'.json',$response);
			
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  //echo "cURL Error #:" . $err;
 			  $data['error_code'] = $err;
			  file_put_contents(WWW_ROOT.'logs/postnl_api_'.date('dmY').'.log','Case_1'."\t".$data['split_order_id']."\t".$data['error_code']."\r\n", FILE_APPEND | LOCK_EX);	
			  $this->sendErrorMail($data);
			} else {
				 // echo $response; 
				  $res = json_decode($response,1);
				  if(isset($res['Errors']) && count($res['Errors']) > 0){
 					 $data['error_code'] = $res['Errors'][0]['Error'] ;
					 $data['error_msg'] = $res['Errors'][0]['Description'] ;
					 file_put_contents(WWW_ROOT.'logs/postnl_api_'.date('dmY').'.log','Case_2'."\t".$data['split_order_id']."\t".$data['error_code']."\r\n", FILE_APPEND | LOCK_EX);	
					 $this->lockOrder($data);	
					 $this->sendErrorMail($data);
				  }elseif(isset($res['ResponseShipments']['0']['Errors']) && count($res['ResponseShipments']['0']['Errors']) > 0){ 
				  	$data['error_code'] = $res['ResponseShipments']['0']['Errors'][0]['Code'] ;
					$data['error_msg'] = $res['ResponseShipments']['0']['Errors'][0]['Description'] ;
					
					file_put_contents(WWW_ROOT.'logs/postnl_api_'.date('dmY').'.log','Case_3'."\t".$data['split_order_id']."\t".$data['error_code']."\t".$data['error_msg']."\r\n", FILE_APPEND | LOCK_EX);	
					
					$this->lockOrder($data);	
					$this->sendErrorMail($data); 
				  }else{  
				 
				 	if($data['status'] == 3){ 
						$this->unLockOrder($data['split_order_id']);
					}
				  
					$content = base64_decode($res['ResponseShipments']['0']['Labels']['0']['Content']);
					 
					$imgPath = WWW_ROOT .'postnl/labels/'.$data['split_order_id'].'.jpg'; 				 
					file_put_contents($imgPath,$content); 
 					$main_height = 1560; $main_left = 500;
					/*if($barcode_type == 'RI'){
						$main_height = 1690;
						$main_left = 50;
					}*/
					
					/*--------------------------Re-Size Order Barcode----------------------------*/
					$cover_img = WWW_ROOT .'img/orders/barcode/'.$data['split_order_id'].'.png';
					$new_ord_barcode = WWW_ROOT .'logs/barcode_'.$data['split_order_id'].'.png';
					$width = 224; 
					$height = 59;
					$newwidth  = 410; 
					$newheight = 190;
					$src = imagecreatefrompng($cover_img);
					$dst = imagecreatetruecolor($newwidth, $newheight);
					imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
					imagepng($dst,$new_ord_barcode);
					imagedestroy($dst);
					imagedestroy($src);
					/*--------------------------END Re-Size Order Barcode----------------------------*/
					/*--------------------------Append Order Barcode----------------------------*/
					$dest = imagecreatefromjpeg($imgPath);
					$src = imagecreatefrompng($new_ord_barcode);
					$postnl_label = WWW_ROOT .'postnl/labels/label_'.$data['split_order_id'].'.jpg';
 					imagecopymerge($dest, $src, $main_left, $main_height, 0, 0, $newwidth , $newheight, 100); //have to play with these numbers for it to work for you, etc. 
					imagejpeg($dest,$postnl_label);
							
					imagedestroy($dest);
					imagedestroy($src);
					/*-------------------------End of Append Order Barcode----------------------------*/
					if(strtolower($data['postal_service']) != 'standard'){
			 			$this->MergeUpdate->updateAll( array( 'MergeUpdate.track_id' => "'".$postnl_barcode."'",'MergeUpdate.reg_post_number' => "'".$postnl_barcode."'",'MergeUpdate.reg_num_img' => "'".$postnl_barcode.".png'") , array( 'MergeUpdate.product_order_id_identify' => $data['split_order_id'] ) );	
					}
			  		
				//	$this->getSingleLabel($data['split_order_id']);
			  
					 
				/*	require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
					spl_autoload_register('DOMPDF_autoload'); 
					$dompdf = new DOMPDF();
					//$dompdf->set_paper('A4', 'portrait'); 
					$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
						<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
						<meta content="" name="description"/>
						<meta content="" name="author"/>';
						$html .= '<table style="width:330px; border:1px solid #CCC;"> 
						<tr><td valign="top"><div>
								<img src="'.WWW_ROOT.'postnl/labels/'.$data['split_order_id'].'.jpg" width="99%"></div><div><center><img src="'.WWW_ROOT.'img/orders/barcode/'.$data['split_order_id'].'.png" style="height:50px;"></center></div></td></tr> 								
						</table>';
						
					$cssPath = WWW_ROOT .'css/';
					$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
					//echo $html;
					$dompdf->load_html($html, Configure::read('App.encoding'));
					$dompdf->render();
					$imgPath = WWW_ROOT .'postnl/labels/'; 
					$name	=	'Label_'.$data['split_order_id'].'.pdf';
					unlink($imgPath.$name); 
					file_put_contents($imgPath.$name, $dompdf->output());*/
 					// exit;
				 }
			} 
			return 1;
 	}
	
	public function getSingleLabel($split_order_id = '3387605-1')
 	{
		$this->autoRender = false;
		$this->layout = ''; 
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		//$dompdf->set_paper('A4', 'portrait'); 
		$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			<meta content="" name="description"/>
			<meta content="" name="author"/>';
			$html .= '<table style="width:400px;"> 
			<tr><td valign="top"><div style="margin-left:-38px;">
					<img src="'.WWW_ROOT.'postnl/labels/'.$split_order_id.'.jpg" width="100%"></div><div><center><img src="'.WWW_ROOT.'img/orders/barcode/'.$split_order_id.'.png" style="height:50px;"></center></div></td></tr> 								
			</table>';
			
		$cssPath = WWW_ROOT .'css/';
		$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
		//echo $html;
		$dompdf->load_html($html, Configure::read('App.encoding'));
		$dompdf->render();
		$imgPath = WWW_ROOT .'postnl/labels/'; 
		$name	=	'Label_'.$split_order_id.'.pdf';
		unlink($imgPath.$name); 
		file_put_contents($imgPath.$name, $dompdf->output());
		return 1;
	}
	
	public function genLabel()
 	{
		$this->loadModel('MergeUpdate'); 
					
		$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.service_provider' =>  'PostNL','MergeUpdate.order_date >' => '2020-12-27 00:00:00'),'fields'=>array('id','sku','product_order_id_identify','order_id','postnl_barcode','country_code','provider_ref_code','status') ) );
		foreach($orders	 as $v){
			echo	$splitOrderID =  $v['MergeUpdate']['product_order_id_identify'];
			$this->getSingleLabel($splitOrderID);
			echo "<br>";
		}
	}
		
	public function jpLabelling($order_id)
 	{
		$this->loadModel('MergeUpdate'); 
		$this->loadModel('OpenOrder');
		$this->loadModel('Country');
		$this->loadModel('Product');
		$this->loadModel('ProductHscode');
		$this->loadModel('PostalServiceDesc');   
		$country_of_origin = 'CN';
		$hs_code = '9603298000';//'8470300000';
		$product_name = '';		
 		App::import('Controller', 'Cronjobs');
		$obj = new CronjobsController();
		
		//exit;
				
		$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.status' => 0, 'MergeUpdate.order_id' => $order_id,'MergeUpdate.brt' => 0,'MergeUpdate.country_code NOT IN' => ['IT','NL']),'fields'=>array('id','sku','product_order_id_identify','order_id','postnl_barcode','country_code','provider_ref_code','status') ) );
		
		// $splitOrderID = null, $ids = null, $productOrderId = null
		
		foreach($orders	 as $v){
			$splitOrderID =  $v['MergeUpdate']['provider_ref_code'];
			 
			
			$sdata = $this->getSplitOrderDetails($v['MergeUpdate']['product_order_id_identify']);
			
			$dim = array($sdata['packet_width'],$sdata['packet_height'],$sdata['packet_length']);
			 asort($dim);
			 $final_dim = array_values($dim) ;	
			 	 
			$whPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $sdata['packet_weight'],'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'Belgium Post'  ),'order'=>'PostalServiceDesc.per_item ASC'));
			 
			
			$all_whistl = array();  
			$whistlFee = 0; 
			if(count($whPostal) > 0)
			{
				foreach($whPostal as $rv){
					$per_item 			= $rv['PostalServiceDesc']['per_item'];
					$psd_id 			= $rv['PostalServiceDesc']['id'];								 						
					$all_whistl[$psd_id] = $per_item ;	 
				} 
				 
				$whistlFee   = min($all_whistl);	
				$post_id    = array_search($whistlFee, $all_whistl); 
				$post_whistl = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));	
				
			  
				if($v['MergeUpdate']['packet_weight'] == 0){
					$data['packet_weight']  = $sdata['packet_weight'];
				}
				
				 
				$data['service_name'] 		= $post_whistl['PostalServiceDesc']['service_name'];
				$data['provider_ref_code'] 	= $post_whistl['PostalServiceDesc']['provider_ref_code'];
				$data['service_id'] 		= $post_whistl['PostalServiceDesc']['id'];
				$data['service_provider'] 	= 'PostNL';								 
				$data['id'] 				= $v['MergeUpdate']['id'];
				pr($data);
				//file_put_contents(WWW_ROOT."logs/royalmail_".$orderItem['MergeUpdate']['product_order_id_identify'].".log",print_r($_details,true));
				 
			 
				
				$this->loadModel( 'MergeUpdate' );				 
				$this->MergeUpdate->saveAll( $data );									
				echo $msg .= $orderItem['MergeUpdate']['product_order_id_identify']."\t";
						 
				}
			else{
				 $msg =  'This order may have over weight or any other issue.<br>';
			}
	 
		}
 		
	 return 1;
	}
		
	private function getSplitOrderDetails($split_order_id = null){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
	    $data = array();
  		$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_order_id)));	
				
		if(count($orderItem) > 0)
		{
			$this->loadModel( 'Product' );
			$this->loadModel( 'ProductDesc' );
			$this->loadModel( 'Country' );	
		
 			$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
			$packet_length = $orderItem['MergeUpdate']['packet_length'];
			$packet_width  = $orderItem['MergeUpdate']['packet_width'];
			$packet_height = $orderItem['MergeUpdate']['packet_height']; 
			
			$_height = array(); $_weight = array(); $length = $width = 0; 
			
			$pos = strpos($orderItem['MergeUpdate']['sku'],",");
			if ($pos === false) {
				$val  = $orderItem['MergeUpdate']['sku'];
				$s    = explode("X", $val);
				$_qty = $s[0]; 
				$_sku = $s[1];		
  				$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.product_name','Product.category_name')));
				if(count($product) > 0){
					$_weight[] = $product['ProductDesc']['weight'] * $_qty;		
					$_height[] = $product['ProductDesc']['height'];		
					$length = $product['ProductDesc']['length'];
					$width = $product['ProductDesc']['width'];	
					$data['category_name'] = $product['Product']['category_name'];	
					$data['product_name'] = $product['Product']['product_name'];										
				}
  				
			}else{			
				$sks = explode(",",$orderItem['MergeUpdate']['sku']);
				$_weight = array();					 
				foreach($sks as $val){
					$s = explode("X", $val);
					$_qty = $s[0]; 
					$_sku = $s[1]; 
					$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.product_name','Product.category_name')));
					if(count($product) > 0)	{
						$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
						$_height[] = $product['ProductDesc']['height'];
						$length = $product['ProductDesc']['length'];	
						$width  = $product['ProductDesc']['width']; 
						$data['category_name'] = $product['Product']['category_name'];	
						$data['product_name'] = $product['Product']['product_name'];	
					}				
				}							 			
			}
			 
			if($packet_weight == 0){
				$packet_weight = array_sum($_weight);
			}
			if($packet_height == 0){
				$packet_height = array_sum($_height);
			}
			if($packet_length == 0){

				$packet_length = $length; 
			}
			if($packet_width == 0){
				$packet_width = $width;  
			}
				
			$data['packet_weight'] = $packet_weight; // KG to Gram
			$data['packet_height'] = $packet_height;
			$data['packet_length'] = $packet_length; 
			$data['packet_width'] = $packet_width;
   		}
		
		return $data;
		
 	}	
	
	public function customLabelling($order_id)
 	{
		$this->loadModel('MergeUpdate'); 
		$this->loadModel('OpenOrder');
		$this->loadModel('Country');
		$this->loadModel('Product');
		$this->loadModel('ProductHscode');  
		$country_of_origin = 'CN';
		$hs_code = '9603298000';//'8470300000';
		$product_name = '';		
		//'MergeUpdate.service_provider' => ['Belgium Post','PostNL'],
 		//	$this->jpLabelling($order_id);
		$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $order_id, 'MergeUpdate.brt' => 0,'MergeUpdate.country_code NOT IN' => ['IT','NL']),'fields'=>array('sku','product_order_id_identify','order_id','postnl_barcode','country_code','provider_ref_code','status','postal_service') ) );
		
		if(count($orders) == 0){
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id,'OpenOrder.destination NOT IN' => ['Italy','Netherlands'] ) ) );
			 
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
		echo 	$customer_info->Address->Country;
			pr($openOrder);
			exit;
		} 
		
     
 /*$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $order_id, 'service_provider'=>'PostNL','delevery_country !='=>'Netherlands'),'fields'=>array('sku','product_order_id_identify','order_id','postnl_barcode','country_code','provider_ref_code') ) );
 */
		foreach($orders	 as $v){
			$provider_ref_code =  $v['MergeUpdate']['provider_ref_code'];
			
 			$this->getBarcodeOutside($v['MergeUpdate']['product_order_id_identify']);
 					
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $v['MergeUpdate']['order_id'],'OpenOrder.destination NOT IN' => ['Italy','Netherlands'] ) ) );
			if(count($openOrder) > 0){
			
				$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
				$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
				$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
				// pr( unserialize( $openOrder['OpenOrder']['items']));
				
				$totals_info->Subtotal;
				$totals_info->PostageCost;
				$totals_info->Tax;		
				$totals_info->Currency; 
				
				$country	=	$this->Country->find('first', array( 'conditions' => array('name' =>trim($customer_info->Address->Country)) ) );
				if(count($country) == 0){
					$country	=	$this->Country->find('first', array( 'conditions' => array('custom_name' =>trim($customer_info->Address->Country)) ) );
				}
				
				if(count($country) > 0){
					$postnl_barcode =  $v['MergeUpdate']['postnl_barcode']; 
					
					$pos = strpos($v['MergeUpdate']['sku'],",");
					
					if ($pos === false) {
							$val  = $v['MergeUpdate']['sku'];			
							$s    = explode("XS-", $val);
							$_qqty = $s[0];
							$_sku = "S-".$s[1];		
								
							$product = $this->Product->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.product_name','Product.country_of_origin')));
							
							$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => $_sku,'country_code' =>  $country['Country']['iso_2'])));
							if(count($hscode) == 0){
								$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' =>  $country['Country']['iso_2'])));
							}
							if(count($hscode) == 0){
								$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' => 'GB')));
							}
							if(count($hscode) > 0){
								if($hscode['ProductHscode']['hs_code'] != ''){
									$hs_code = $hscode['ProductHscode']['hs_code'];
								}
							}
							
							if($product['Product']['country_of_origin'] != ''){
								$c_code= $this->Country->find('first',array('conditions' => array('Country.name' => $product['Product']['country_of_origin']),'fields'=>array('Country.iso_2'))); 
  								if(count($c_code) > 0){
  									$country_of_origin = $c_code['Country']['iso_2'];
								}
							}
							
							$product_name = $product['Product']['product_name'];
							$length = $product['ProductDesc']['length'] / 10;
							$width  = $product['ProductDesc']['width'] / 10;
							$height = $product['ProductDesc']['height'] / 10;
							$weight = ($product['ProductDesc']['weight'] * $_qqty ) * 1000;
									
						}else{			
						
							$_length = []; $_width = []; $_height = []; $_weight = [];
 							$sks = explode(",",$v['MergeUpdate']['sku']);
							foreach($sks as $val){
								$s    = explode("XS-", $val);
								$_qqty = $s[0];
								$_sku = "S-".$s[1];	
								
								$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => $_sku,'country_code' =>  $country['Country']['iso_2'])));
								if(count($hscode) == 0){
									$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' =>  $country['Country']['iso_2'])));
								}
								if(count($hscode) == 0){
									$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' => 'GB')));
								}
								if(count($hscode) > 0){
									if($hscode['ProductHscode']['hs_code'] != ''){
										$hs_code = $hscode['ProductHscode']['hs_code'];
									}
								}
								
								$product = $this->Product->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.product_name','Product.country_of_origin')));
								if($product['Product']['country_of_origin'] != ''){
									$c_code= $this->Country->find('first',array('conditions' => array('Country.name' => $product['Product']['country_of_origin']),'fields'=>array('Country.iso_2'))); 
									if(count($c_code) > 0){
										$country_of_origin = $c_code['Country']['iso_2'];
									}
								}
								$product_name = $product['Product']['product_name'];
								$_length[] = $product['ProductDesc']['length'];
								$_width[]  = $product['ProductDesc']['width'];
								$_height[] = $product['ProductDesc']['height'];
								$_weight[] = $product['ProductDesc']['weight'] * $_qqty;	
								 				
							}		
							$length	= array_sum($_length) / 10;
							$width	= array_sum($_width) / 10;
							$height	= array_sum($_height) / 10;
							$weight	= array_sum($_weight) * 1000;	 
					}
					$data['total_charge']    	=  $totals_info->TotalCharge ;
					$data['hs_code']  		 	=  $hs_code ;
					$data['country_of_origin']  =  $country_of_origin ;
					$data['product_name']   	=  $product_name ;
					$data['eu_country']   		=  $country['Country']['eu_country'];
					$data['provider_ref_code']  =  $provider_ref_code ;
					$data['phone_number']   	=  $customer_info->Address->PhoneNumber ;
					$data['postnl_barcode'] 	=  $v['MergeUpdate']['postnl_barcode'];	
					$weight 					=  explode(".",$weight)[0];
					$data['weight'] 			=  str_pad($weight,4,'0',STR_PAD_LEFT) ;	
					$data['country_code'] 		=  $country['Country']['iso_2'] ;	
					$data['country'] 			=  $customer_info->Address->Country;
					$data['sub_source'] 		=  strtolower($openOrder['OpenOrder']['sub_source']) ;
					$data['split_order_id'] 	=  $v['MergeUpdate']['product_order_id_identify'] ;	
					$data['status']				=  $v['MergeUpdate']['status'] ;		
					$data['postal_service']		=  $v['MergeUpdate']['postal_service'] ;
					
					$res = $this->getLabel($data,$customer_info);
					pr($res);$this->getSingleLabel($v['MergeUpdate']['product_order_id_identify']);
					pr($data);
				
				}
				else{
					 file_put_contents(WWW_ROOT.'logs/postnl_api_country_'.date('dmY').'.log','Case_1'."\t".$data['split_order_id']."\t".$customer_info->Address->Country."\r\n", FILE_APPEND | LOCK_EX);	
					 
					$data['error_code'] = 'country not found';
					$data['error_msg']  = $customer_info->Address->Country;
 					$this->sendErrorMail($data);
			
				}
 			} 
			// exit;
		}
		
		return 1; exit;
	} 
	
	private function replaceChar($string = null){
  		//return iconv('UTF-8','ASCII//TRANSLIT',$string);	 
		 
			
		$unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E','Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Nº'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U','Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c','è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n','nº'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o','ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y','ü'=>'u','º'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	 
	}
		
	private function sendErrorMail($details = null){
		
		$web_store = Configure::read( 'web_store' );
		$subject   = 'PostNL API issue in '.$web_store;
		$mailBody  = '<p><strong>PostNL API have some issue please review and solve it.</strong></p>';
		
		if(isset($details['split_order_id'])){
			$subject   = $details['split_order_id'].' PostNL API orders issue in '.$web_store;
			$mailBody  = '<p><strong>'.$details['split_order_id'].' have below issue please review and solve it.</strong></p>';
		}	
		if(isset($details['error_code'])){
			$mailBody .= '<p>Error Code : '.$details['error_code'].'</p>';
			$mailBody .= '<p>Error Message : '.$details['error_msg'].'</p>';
		}
		 
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('');
		$email->emailFormat('html');
		$email->from('postnl@'.$web_store); 	
 		$email->to( array('avadhesh.kumar@jijgroup.com','pappu.k@euracogroup.co.uk'));	 
		$email->subject( $subject );
	  	$email->send( $mailBody );
	}
	
	private function lockOrder($details = null){
 		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderNote' ); 
		$md = $this->MergeUpdate->find('first', array( 'conditions' => array('product_order_id_identify' => $details['split_order_id']),'fields'=>'order_id' ) );
		if(count($md) > 0){
			$lock_note = $details['error_msg'];
			
			$firstName = 'PostNL API';
			$lastName = 'Cron';
			$mdata['postnl_barcode'] = '\'\'';
			$mdata['status']    = 3;
 			 
			$this->MergeUpdate->updateAll($mdata, array( 'product_order_id_identify' => $details['split_order_id'] ) );
			$this->OpenOrder->updateAll(array('OpenOrder.status' => 3), array('OpenOrder.num_order_id' => $md['MergeUpdate']['order_id']));
			
			$noteDate['order_id'] = $md['MergeUpdate']['order_id'];
			$noteDate['note'] = $lock_note;
			$noteDate['type'] = 'Lock';
			$noteDate['user'] = $firstName.' '.$lastName;
			$noteDate['date'] = date('Y-m-d H:i:s');
			$this->OrderNote->saveAll( $noteDate );  
		}
	}	
	
	public function unLockOrder($split_order_id = null)
	{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );			 
			$md = $this->MergeUpdate->find('first', array( 'conditions' => array('product_order_id_identify' => $split_order_id),'fields'=>'order_id' ) );
			if(count($md) > 0)
			{
 				$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 0) , array( 'MergeUpdate.product_order_id_identify' => $split_order_id ) );
				$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 0 ) , array( 'OpenOrder.num_order_id' => $md['MergeUpdate']['order_id'] ) );
			}
			 
	}
	public function testLabel()
	{				
		
		$this->loadModel( 'MergeUpdate' );
		$data['split_order_id'] = '3032615-1'; 
		$or = explode("-",$data['split_order_id']);  
		$barcode_type = 'RI';
		
		$main_height = 1550; $main_left = 500;
		if($barcode_type == 'RI'){
			$main_height = 1670;
			$main_left = 50;
		}
		
		//	$this->MergeUpdate->updateAll(array('service_provider' => '\'BRT\'', 'brt' => 1, 'brt_phone' => 1, 'brt_exprt' => '1'), array('product_order_id_identify' =>'2865407-1'));	
							 
		 $slip = $this->getSlip($data['split_order_id'],$or[0]);	
			
		// $file_name = 'label_'.$filename.'.pdf';
		
		// $response = file_get_contents(WWW_ROOT.'logs/response_'.$data['split_order_id'].'.json');
		 // $res = json_decode($response,1);
		// pr($res);
		 // $content = base64_decode($res['ResponseShipments']['0']['Labels']['0']['Content']);
			 
			 
			 
			 
		 $imgPath = WWW_ROOT .'logs/'.$data['split_order_id'].'.jpg'; 	
		  //file_put_contents(WWW_ROOT.'logs/response_'.$data['split_order_id'].'.json',$response);
		 // file_put_contents($imgPath,$content);
		
		
		/*--------------------------Re-Size Order Barcode----------------------------*/
		$cover_img = WWW_ROOT .'img/orders/barcode/'.$data['split_order_id'].'.png';
		$new_ord_barcode = WWW_ROOT .'logs/barcode_'.$data['split_order_id'].'.png';
		$width = 224; 
		$height = 59;
		$newwidth  = 400; 
		$newheight = 150;
		$src = imagecreatefrompng($cover_img);
		$dst = imagecreatetruecolor($newwidth, $newheight);
		imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagepng($dst,$new_ord_barcode);
		imagedestroy($dst);
		imagedestroy($src);
		/*--------------------------END Re-Size Order Barcode----------------------------*/
		/*--------------------------Append Order Barcode----------------------------*/
		$dest = imagecreatefromjpeg($imgPath);
		$src = imagecreatefrompng($new_ord_barcode);
		
		imagecopymerge($dest, $src, $main_left, $main_height, 0, 0, $newwidth , $newheight, 100); //have to play with these numbers for it to work for you, etc. 
		imagejpeg($dest,WWW_ROOT .'logs/label_'.$data['split_order_id'].'.jpg');
				
		imagedestroy($dest);
		imagedestroy($src);
		/*-------------------------End of Append Order Barcode----------------------------*/
		
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper('A4', 'portrait'); 
		
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
					 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
					 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
					 <meta content="" name="description"/>
					 <meta content="" name="author"/>';
			$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
				<tr><td style="height:600px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
			<tr><td valign="top" style="padding:0px 0px 5px 12px;">						
			<div style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
					transform-origin: 50% 50%;height:270px; width:450px;">
					<img src="'.Router::url('/', true).'logs/'.$data['split_order_id'].'.jpg" width="350px" style="margin-left: -110px; margin-right: -110px;margin-top: -70px;"></div></td><td><img src="'.Router::url('/', true).'img/orders/barcode/'.$data['split_order_id'].'.png" style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
					transform-origin: 50% 50%;height:60px; margin-left: -28px;" width="224px"> </td> 
					
					 
					</tr>
					 
					
			</table>';
			
		$cssPath = WWW_ROOT .'css/';
		$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
		echo $html;
		$dompdf->load_html($html, Configure::read('App.encoding'));
		$dompdf->render();
		$imgPath = WWW_ROOT .'logs/'; 
		$name	=	'Label_Slip_'.$data['split_order_id'].'.pdf';
		unlink($imgPath.$name); 
		file_put_contents($imgPath.$name, $dompdf->output());
		
		
		exit;
		
	}
	
	public function testSingleLabel()
	{				
		
		$this->loadModel( 'MergeUpdate' );
		$data['split_order_id'] = '3032615-1'; 
		$or = explode("-",$data['split_order_id']);  
		$barcode_type = 'RI';
		
		$main_height = 1550; $main_left = 500;
		if($barcode_type == 'RI'){
			$main_height = 1670;
			$main_left = 50;
		}
  		 $imgPath = WWW_ROOT .'logs/'.$data['split_order_id'].'.jpg'; 	
		  //file_put_contents(WWW_ROOT.'logs/response_'.$data['split_order_id'].'.json',$response);
		 // file_put_contents($imgPath,$content);
		
		
 
		
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		//$dompdf->set_paper('A4', 'portrait'); 
		$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');
	 	$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			<meta content="" name="description"/>
			<meta content="" name="author"/>';
			$html .= '<table style="width:330px; border:1px solid #CCC;"> 
			<tr><td valign="top"><div>
					<img src="'.Router::url('/', true).'logs/'.$data['split_order_id'].'.jpg" width="99%"></div><div><center><img src="'.Router::url('/', true).'img/orders/barcode/'.$data['split_order_id'].'.png" style="height:50px;"></center></div></td></tr> 								
			</table>';
			
		$cssPath = WWW_ROOT .'css/';
		$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
		echo $html;
		$dompdf->load_html($html, Configure::read('App.encoding'));
		$dompdf->render();
		$imgPath = WWW_ROOT .'logs/'; 
		$name	=	'Label__'.$data['split_order_id'].'.pdf';
		unlink($imgPath.$name); 
		file_put_contents($imgPath.$name, $dompdf->output()); 
		exit;
		
	}
	
 	public function getSlip($splitOrderId = null, $openOrderId = null)
	{
	
		//echo 'xxxxxxxxxxxxxxx';exit;
		$this->loadModel('OrderLocation');
		$this->loadModel('MergeUpdate');
		$this->loadModel('CategoryContant');
		$this->loadModel('Product');
		
		$this->GlobalBarcode = $this->Components->load('Common');		
		App::import('Controller', 'Linnworksapis');
		$obj = new LinnworksapisController();
		$order	=	$obj->getOpenOrderById( $openOrderId );
		
		$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
		$itemPrice			=		$getSplitOrder['MergeUpdate']['price'];
		$itemQuantity		=		$getSplitOrder['MergeUpdate']['quantity'];
		$BarcodeImage		=		$getSplitOrder['MergeUpdate']['order_barcode_image'];
		$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
		$assignedservice	=	 	$order['assigned_service'];
		$courier			=	 	$order['courier'];
		$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
		$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
		$barcode			=	 	$order['assign_barcode'];
		$subtotal			=		$order['totals_info']->Subtotal;
		$subsource			=		$order['sub_source'];
		$totlacharge		=		$order['totals_info']->TotalCharge;
		$ordernumber		=		$order['num_order_id'];
		$fullname			=		$order['customer_info']->Address->FullName;
		$address1			=		$order['customer_info']->Address->Address1;
		$address2			=		$order['customer_info']->Address->Address2;
		$address3			=		$order['customer_info']->Address->Address3;
		/*$address1 			= 		wordwrap($order['customer_info']->Address->Address1, 30, "\n<br>", false);	
		$address2 			= 		wordwrap($order['customer_info']->Address->Address2, 30, "\n<br>", false);	
		$address3			=		$order['customer_info']->Address->Address3;
		if(strlen($order['customer_info']->Address->Address3) > 30)	{
			$address3 		= 		wordwrap($order['customer_info']->Address->Address3, 30, "\n<br>", false);
		}*/
		$town				=	 	$order['customer_info']->Address->Town;
		$resion				=	 	$order['customer_info']->Address->Region;
		$postcode			=	 	$order['customer_info']->Address->PostCode;
		$country			=	 	$order['customer_info']->Address->Country;
		$phone				=	 	$order['customer_info']->Address->PhoneNumber;
		$company			=	 	$order['customer_info']->Address->Company;
		$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
		$postagecost		=	 	$order['totals_info']->PostageCost;
		$tax				=	 	$order['totals_info']->Tax;
		$barcode  			=   	$order['assign_barcode'];
		$items				=	 	$order['items'];
		$address			=		'';
		$address			.=		($company != '') ? $company.'<br>' : '' ; 
		$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
		$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
		$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
		$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
		$address			.=		($town != '') ? $town.'<br>' : '';
		$address			.=		($resion != '') ? $resion.'<br>' : '';
		$address			.=		($postcode != '') ? $postcode.'<br>' : '';
		$address			.=		($country != '' ) ? $country.'<br>' : '';
		
		$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forl�-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia');
				
		$cana_provinces = array('AB' =>'Alberta','BC'=>'British Columbia','MB'=>'Manitoba','NB'=>'New Brunswick','NL'=>'Newfoundland and Labrador','NS'=>'Nova Scotia','NT'=>'Northwest Territories','NU'=>'Nunavut','ON'=>'Ontario','PE'=>'Prince Edward Island','QC'=>'Quebec','SK'=>'Saskatchewan','YT'=>'Yukon');
		
		$addData['sub_source'] 	= 	$order['sub_source'];
		$addData['company'] 	= 	$company;
		$addData['fullname'] 	= 	$fullname;
		$addData['address1'] 	= 	$address1;
		$addData['address2'] 	= 	$address2;
		$addData['address3'] 	= 	$address3;
		$addData['town'] 		= 	$town;
		if( $country == 'Italy' )
		{
			if(array_key_exists(strtoupper($resion),$provinces)){
				$addData['resion'] 		= 	$resion;
			}else {
				$addData['resion'] 		= 	array_search(ucfirst($resion), $provinces);
			}
		}
		else if( $country == 'Canada' )
		{
			if(array_key_exists(strtoupper($resion),$cana_provinces)){
				$addData['resion'] 		= 	$resion;
			}else {
				$addData['resion'] 		= 	array_search(ucfirst($resion), $cana_provinces);
			}
		}
		else {
			$addData['resion'] 		= 	$resion;
		}
		
		$addData['postcode'] 	= 	$postcode;
		$addData['country'] 	= 	$country;
				
		$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);//CostBreaker
		
		$subsource_lower = strtolower($subsource) ;
		$marecArray = array('marec_de','marec_es','marec_fr','marec_it','marec_uk','marec_nl');
		$costbreakerArray = array('costbreaker_de','costbreaker_nl','costbreaker_es','costbreaker_fr','costbreaker_it','costbreaker_uk','costbreaker','costbreaker_ca','costbreaker_usabuyer','onbuy','flubit','euraco.fyndiq','costdropper');
		$techdriveArray = array('tech_drive_de','tech_drive_es','tech_drive_fr','tech_drive_it','tech_drive_uk');				
		$rainbowArray = array('rainbow retail','rainbow retail de','rainbow_retail_es','rainbow_retail_it','rainbow_retail_fr');
		$bbdArray = array('bbd_eu_de');	
		
		if(in_array($subsource_lower,$marecArray)){
			$company 	=  'Marec';
		}else if(in_array($subsource_lower,$costbreakerArray)){
			$company 	=  'CostBreaker';
		}else if(in_array($subsource_lower,$techdriveArray)){
			$company 	= 'Tech Drive Supplies';
		}else if(in_array($subsource_lower,$rainbowArray)){
			$company 	= 'Rainbow Retail';
		}else if(in_array($subsource_lower,$bbdArray)){
			$company 	= 'BBD';
		}else if( $subsource == 'EBAY2' ){
			$company 	= 'EBAY2';
		}else if( $subsource == 'EBAY0' ){
			$company 	= 'EBAY0';
		}else if( $subsource == 'EBAY5' ){
			$company 	= 'Marec';
		}
		  
		
		if($splitOrderId == '2311053-1'){
			//echo $company  ;exit;
		}
		
		$i = 1;
		$str = '';
		$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
		$totalGoods = 0;
		$productInstructions = '';
		$ref_code = trim($getSplitOrder['MergeUpdate']['provider_ref_code']);
		$codearray = array('DP1', 'FR7');
		
		$this->loadModel( 'BulkSlip' );
		if($subsource_lower == 'costbreaker_ca'){
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => 'CostBreaker_CA' ) ) );		
		}elseif($subsource_lower == 'costbreaker_usabuyer'){
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => 'CostBreaker_USA' ) ) );		
		}else{
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => $company ) ) );
		}
		 
		
		$p_barcode	 = '';
		$is_bulk_sku = 0;
		if( in_array($ref_code, $codearray)) 
		{
			
			$per_item_pcost 	= $postagecost/count($items);
			$j = 1;
			foreach($items as $item){
				$item_title 		= 	$item->Title;
				$quantity			=	$item->Quantity;
				$price_per_unit		=	$item->PricePerUnit;
				$str .= '<tr>
							<td style="border:1px solid #000;" align="left" valign="top" width="10%">'.$quantity.'</td>
							<td style="border:1px solid #000;" valign="top" width="20%">'.substr($item_title, 0, 30 ).'</td>
							<td style="border:1px solid #000;" valign="top" width="15%">'.$quantity * $price_per_unit.'</td>
						<td style="border:1px solid #000;" valign="top" width="15%">'.$per_item_pcost.'</td>
						<td style="border:1px solid #000;" valign="top" width="20%">'.(($price_per_unit * $quantity) + $per_item_pcost).'</td>
					</tr>';
				$j++;
			}
			 
			$str .=	'<tr><td></td><td></td><td></td><td style="border:1px solid #000;">Total</td>
						<td style="border:1px solid #000;" valign="top" width="20%">'.$totalcharge.'</td>
					</tr>';
			 
			
		} 
		else 
		{
			
			foreach( $skus as $sku )
			{
			
				$newSkus[]				=	 explode( 'XS-', $sku);
				$cat_name = array();
				$sub_can_name = array();
				foreach($newSkus as $newSku)
					{
						
						$getsku = 'S-'.$newSku[1];
						$getOrderDetail = 	$this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
						$OrderLocation 	= 	$this->OrderLocation->find( 'first', array( 'conditions' => array('order_id' => $openOrderId, 'sku' => $getsku ) )  );
						if( count($OrderLocation) > 0 ){
							$o_location 	=	$OrderLocation['OrderLocation']['bin_location'];
						} else {
							$o_location 	=	'Check Location' ;
						}
						
						$contentCat 	= 	$getOrderDetail['Product']['category_name'];
						$cat_name[] 	= 	$getOrderDetail['Product']['category_name'];
						$contentSubCat	=	$getOrderDetail['Product']['sub_category'];
						$sub_can_name[] =	$getOrderDetail['Product']['sub_category'];
						$productBarcode	=	$getOrderDetail['ProductDesc']['barcode'];
						$is_bulk_sku	=	$getOrderDetail['Product']['is_bulk_sku'];
						
						$getContent	= $this->CategoryContant->find( 'first', array( 
															'conditions'=>array('CategoryContant.sub_category'=>$contentSubCat,'CategoryContant.sub_source'=>$subsource)));
						$productBarcode			=	$getOrderDetail['ProductDesc']['barcode'];
						if(count($getContent) > 0) {
							$getbarcode				= 	$this->getBarcode( $productBarcode );
							$productInstructions 	=  '<p style="font-size:14px;">'.$getContent['CategoryContant']['content'].'</p>';
						}
						
						$product_local_barcode = $this->GlobalBarcode->getLocalBarcode($getOrderDetail['ProductDesc']['barcode']);
						
						$title	=	$getOrderDetail['Product']['product_name'];
						$totalGoods = $totalGoods + $newSku[0];
						$str .= '<tr>
								<td valign="top" class="rightborder bottomborder">'.$i.'</td>
								<td valign="top" class="center rightborder bottomborder">'.$newSku[1].'</td>
								<td valign="top" class="rightborder bottomborder">'.$title.'</td>
								<td valign="top" class="rightborder bottomborder" >'.$product_local_barcode.'</td>
								<td valign="top" class="rightborder bottomborder">'.$o_location.'</td>
								<td valign="top" class="center norightborder bottomborder">'.$newSku[0].'</td>';
						$str .= '</tr>';
						$i++;
						
					}
					unset($newSkus);
			}
			
		}
				
						
		/*$html 			=	$gethtml['PackagingSlip']['html'];
		$paperHeight    =   $gethtml['PackagingSlip']['paper_height'];
		$paperWidth  	=   $gethtml['PackagingSlip']['paper_width'];
		$barcodeHeight  =   $gethtml['PackagingSlip']['barcode_height'];
		$barcodeWidth   =   $gethtml['PackagingSlip']['barcode_width'];
		$paperMode      =   $gethtml['PackagingSlip']['paper_mode'];*/
		//
		if($splitOrderId == '1668658-1'){
			//pr($company );
		}
		$setRepArray = array();
		$setRepArray[] 					= $address1;
		$setRepArray[] 					= $address2;
		$setRepArray[] 					= $address3;
		$setRepArray[] 					= $town;
		$setRepArray[] 					= $resion;
		$setRepArray[] 					= $postcode;
		$setRepArray[] 					= $country;
		$setRepArray[] 					= $phone;
		$setRepArray[] 					= $ordernumber;
		$setRepArray[] 					= $courier;
		$setRepArray[] 					= $recivedate[0];
		$totalitem = $i - 1;
		$setRepArray[]	=	 $str;
		$setRepArray[]	=	 $totalGoods;
		$setRepArray[]	=	 $subtotal;
		$Path 			= 	'/wms/img/client/';
		$img			=	 '';
		$setRepArray[]	=	 $img;
		$setRepArray[]	=	 $postagecost;
		$setRepArray[]	=	 $tax;
		$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
		//$setRepArray[]	=	 $totalamount;
		$setRepArray[]	=	 $itemPrice;
		
		$address		= $obj->getCountryAddress( $addData  );
		
		$setRepArray[]	=	 $address;
		$barcodePath  	=  WWW_ROOT.'img/orders/barcode/';
		$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.' width='. $barcodeWidth .'>';
		$barcodenum		=	explode('.', $barcode);
		
		$setRepArray[] 	=  $barcodeimg;
		$setRepArray[] 	=  $paymentmethod;
		$setRepArray[] 	=  $barcodenum[0];
		$setRepArray[] 	=  '';
		$img 			=	$gethtml['PackagingSlip']['image_name'];
		$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
		$setRepArray[] 	=  $returnaddress;
		$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
		$setRepArray[] 	=  $splitOrderId;
		$setRepArray[] 	= 	utf8_decode( $productInstructions );
		//$prodBarcodePath  	=  Router::url('/', true).'img/product/barcodes/';			
		$prodBarcodePath  	= WWW_ROOT.'img/product/barcodes/';
		$setRepArray[] 	=  '<img src='.$prodBarcodePath.$productBarcode.'.png width='. $barcodeWidth .'>';
		$setRepArray[] 	=  $totalamount;
		$setRepArray[]	=	WWW_ROOT;
		/*--------------Ink------------------*/
		$this->loadMOdel( 'InkOrderLocation' );	 
		$order_ink	    = $this->InkOrderLocation->find('first', array('conditions' => array('order_id' => $openOrderId,'split_order_id'=>$openOrderId.'-1','status'=>'active')));		
		if(count($order_ink) > 0 ){			 
			$setRepArray[] 	=  '<div style="color:green;font-size:14px;"><center>#2 Envelope Required.</center></div><br><img src="'.WWW_ROOT.'img/ink_xl2.jpg" height="130">';
		}
		/*--------------End of Ink------------------*/
		$imgPath = WWW_ROOT .'css/';
		$html2 			= 	$gethtml['BulkSlip']['html'];
		//pr($gethtml);
		//$html2 			= 	'';
		if($splitOrderId == '1668658-1'){
			 //echo $html2;exit;
		}
		if( $company ==  'Marec' && in_array(  'Mobile Accessories', $cat_name ))
		{
			//$this->MobileAsseInstruction( $splitOrderId, $subsource  );
		}
		
		
		$get_country = array('marec_fr'=>'FR','marec_de'=>'DE','marec_it'=>'IT','marec_es'=>'ES','marec_uk'=>'UK');
		$source_country  = $get_country[$subsource_lower];
  			
		if( $company ==  'Marec' && $is_bulk_sku == 1)
		{	
			//$html2 	.= $this->BulkSkuNote($source_country);			 
		}
		
		$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
		$html 		= 	$obj->setReplaceValue( $setRepArray, $html2 );
		return $html;
		
	}
	
	public function getAddress()
	{
	
		$this->loadModel('MergeUpdate'); 
		$this->loadModel('OpenOrder');
			
		$orders	=	$this->MergeUpdate->find('all', array('conditions'=> array('order_id'=>['3021618','2821505','3023714']),'fields'=>array('sku','product_order_id_identify','order_id','postnl_barcode','country_code','provider_ref_code','source_coming','delevery_country') ) );
 		
		//echo count($orders);
 		//echo '<br>';
 		unlink(WWW_ROOT.'logs/adr10.csv');
 
		foreach($orders	 as $v){
		
			$provider_ref_code =  $v['MergeUpdate']['provider_ref_code'];
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $v['MergeUpdate']['order_id'] ) ) );
			$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
			// pr($customer_info);
			 $sep = "\t";
			 $content = $customer_info->Address->Address1.$sep.
			 $customer_info->Address->Address2.$sep.
			 $customer_info->Address->Address3.$sep.
			 $customer_info->Address->Town.$sep.
			 $customer_info->Address->Region.$sep.
			 $customer_info->Address->Country;
			 
			 $addressArray['sub_source'] =  $v['MergeUpdate']['source_coming'];
			 $addressArray['country'] = $v['MergeUpdate']['delevery_country'];
			 $addressArray['address1'] = $customer_info->Address->Address1;
			 $addressArray['address2'] = $customer_info->Address->Address2;
			 $addressArray['address3'] = $customer_info->Address->Address3;
			 $addressArray['company'] = $customer_info->Address->Company;
			  
			 
			//echo $v['MergeUpdate']['order_id'] .' ==   ';
			 $adr = $this->getCountryAddress( $addressArray );
			//echo "<br>";
			//echo $content ;
			$content .= $sep.$adr;
			//echo "<br>";
			
		//$content = $v['CustomerOrder']['order_id'].$sep. $v['CustomerOrder']['sku'].$sep. $v['CustomerOrder']['barcode'].$sep. $v['CustomerOrder']['quantity'].$sep. $v['CustomerOrder']['unit_price'].$sep.$v['CustomerOrder']['status'].$sep.$v['CustomerOrder']['created_date'].$sep.$created_by .$sep.$customer_name;
 			
 			file_put_contents(WWW_ROOT.'logs/adr.csv',$content."\r\n", FILE_APPEND | LOCK_EX);	
			 

			
		}
		
		
		//$addressArray
	}
	
	public function getCountryAddress( $addressArray )
	{
			
			switch ($addressArray['country']) 
			{
			case "United States":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? ucwords(strtolower($addressArray['address1']))."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords(strtolower($addressArray['address2']))."\n" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords(strtolower($addressArray['address3']))."\n" : '' ; 
					return $address;
					break;
			case "Canada":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1'])."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']) : '' ;
					return $address;
					break;
			case "Germany":
			
					if(strtolower($addressArray['sub_source']) == 'costdropper'){
  					
						$address			 =		'';
						if($addressArray['address1'] != '' && $addressArray['address2'] != ''){
							$address			.=		ucwords($addressArray['address2']).' ' . ucwords($addressArray['address1']) ; 
						}
 						if($addressArray['address1'] == '' && $addressArray['address2'] != ''){						
							$address			.=		ucwords($addressArray['address2']) ; 					
						}
						if($addressArray['address1'] != '' && $addressArray['address2'] == ''){						
							$address			.=		ucwords($addressArray['address1']) ; 					
						}
						
					}else{
							$address			 =		'';
							$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1'])."\n" : '' ; 
							
							if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
								$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
								$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;						
							}
							
							if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
								$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ;
							}
							
							if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
								$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;
							}
 						}
					return $address;
					break;
				case "Spain":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? ucwords($addressArray['company'])."\n" : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1'])."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords($addressArray['address3'])."\n" : '' ;
					return $address;
					break;
			
			case "Italy":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? strtoupper($addressArray['address1'])."\n" : '' ; 
					
					 if($addressArray['address2'] != '' && $addressArray['address3'] != ''){
						$address			.=		($addressArray['address2'] != '' )   ? strtoupper($addressArray['address2'])."\n" : '';
						$address			.=		($addressArray['address3'] != '' )   ? strtoupper($addressArray['address3'])."\n" : '';
					}
					else if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
 						$address			.=		($addressArray['address2'] != '' )   ? strtoupper($addressArray['address2'])."\n" : '';
 					}
					else if($addressArray['address2'] == '' && $addressArray['address3'] != ''){
 						$address			.=		($addressArray['address3'] != '' )   ? strtoupper($addressArray['address3'])."\n" : '';
 					}
					return $address;
					break;
			case "France":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? strtoupper($addressArray['address1'])."\n" : '' ; 
					
					if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;						
					}
					
					if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ;
					}
					
					if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;
					}
					
					return $address;
					break;
			case "Denmark":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? $addressArray['company']."\n" : '' ;  
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1']."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3']."\n" : '' ; 
					return $address;
					break;
			case "Austria":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? $addressArray['company']."\n" : '' ; 
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1']."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3']."\n" : '' ; 
					return $address;
					break;
			default:
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1']."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3']."\n" : '' ;
					return $address;
					break;
			}
			exit;
		}
		
 	public function getSenderInfo($data  = [], $store_country = '')
	{ 
 		$country = $data['country'] ;
		if($store_country != ''){
			$country = $store_country;
		}
 		$sub_source = $data['sub_source'];
		
  		if(strpos($sub_source,'marec')!== false){
  		  	$senderData['FromPhoneNumber'] = '+443301170238';  
			$senderData['email'] = 'accounts@esljersey.com';  
		  	$senderData['FromCompany']	   =  'ESL LIMITED';
			$senderData['Building']	   	   =  '7115' ;
			$senderData['FromAddress']	   =  'Postbus 7115' ; 
			$senderData['FromPostCode']    =	'3109 AC';
			
 			$VAT_Number = 'GB 307817693';  
			if(in_array($country ,['United Kingdom','GB','UK'])){  
				$VAT_Number   = 'GB 307817693'; 
			}else if(in_array($country ,['Germany','DE'])){  
				$VAT_Number   = 'DE 323086773'; 
			}else if(in_array($country ,[ 'France','FR'])){  
				$VAT_Number   = 'FR 07879900934';  
			}else if(in_array($country ,['Italy','IT'])){  
				$VAT_Number   = 'IT 11062310963'; 
			} 
			$senderData['vat_number'] 	   =	$VAT_Number;
		 }	
		else if(strpos($sub_source,'rainbow')!== false){
 			$senderData['FromPhoneNumber'] = '0123456789';
			$senderData['email'] = 'accounts@fresherbusiness.co.uk'; 			
		  	$senderData['FromCompany']	   =  'FRESHER BUSINESS LIMITED';
			$senderData['Building']	   	   =  '7098' ;
			$senderData['FromAddress']	   =  'Postbus 7098' ; 
			$senderData['FromPostCode']    =	'3109 AB';
			 
 			$VAT_Number = 'GB 318649182';  
			if(in_array($country ,['United Kingdom','GB','UK'])){    
				$VAT_Number   = 'GB 318649182'; 
			}else if(in_array($country ,['Germany','DE'])){ 
				$VAT_Number   = 'DE 327173988'; 
			}else if(in_array($country ,['France','FR'])){ 
				$VAT_Number   = 'FR 48880490255';  
			}else if(in_array($country ,['Italy','IT'])){ 
				$VAT_Number   = 'IT 11061000961'; 
			} 
			$senderData['vat_number'] 	   =	$VAT_Number;
		}else{
 		  	$senderData['FromCompany']	   =  'EURACO GROUP LTD';			
			$senderData['Building']	   	   =  '7124' ;
			$senderData['FromAddress']	   =  'Postbus 7124' ; 
			$senderData['FromPhoneNumber'] =  '+443301170104';
			$senderData['email'] 		   =  'accounts@euracogroup.co.uk';
			$senderData['FromPostCode']    =	'3109 AC';
			
			$VAT_Number = 'GB 304984295';  
			if(in_array($country ,['United Kingdom','GB','UK'])){  
				$VAT_Number   = 'GB 304984295'; 
			}else if(in_array($country ,['Germany','DE'])){
				$VAT_Number   = 'DE 321777974'; 
			}else if(in_array($country ,['France','FR'])){
				$VAT_Number   = 'FR 59850070509'; 
			}else if(in_array($country ,['Spain','ES'])){
				$VAT_Number   = 'ES N6061518D'; 
			}else if(in_array($country ,['Italy','IT'])){
				$VAT_Number   = 'IT 10905930961'; 
			}	
			$senderData['vat_number'] 	   =	$VAT_Number;
		}
		$senderData['FromPersonName']  =  'C/O Postbus';
 		$senderData['FromCity'] 	   =	'Schiedam'; 		
		$senderData['FromCountryCode'] =	'NL'; 
  		 
  		return $senderData;	
	}
	
	public function getSenderInfo20102020($data  = [], $store_country = '')
	{ 
 		$country = $data['country'] ;
		if($store_country != ''){
			$country = $store_country;
		}
 		$sub_source = $data['sub_source'];
		
  		if(strpos($sub_source,'marec')!== false){
			$FromCompany = 'ESL LIMITED';
			$FromPersonName = 'ESL LIMITED';
			/*-------Updated on 12-02-2020-----*/
			$Building = 'Unit 4 Airport Cargo Centre';
			$FromAddress = 'L\'avenue De La Commune';
			$FromCity = 'St Peter';
			$FromDivision = 'JE';
			$FromPostCode = 'JE3 7BY';
			$FromCountryCode = 'GB';
			$FromCountryName = 'JERSEY';
			$FromPhoneNumber = '+443301170238';
			$email = 'accounts@esljersey.com'; 
			
			$VAT_Number = 'GB 307817693';  
 			if(in_array($country ,['United Kingdom','GB','UK'])){  
				$VAT_Number   = 'GB 307817693'; 
    		}else if(in_array($country ,['Germany','DE'])){  
 				$VAT_Number   = 'DE 323086773'; 
			}else if(in_array($country ,[ 'France','FR'])){  
				$VAT_Number   = 'FR 07879900934';  
			}else if(in_array($country ,['Italy','IT'])){  
				$VAT_Number   = 'IT 11062310963'; 
			} 
		 }	
		else if(strpos($sub_source,'rainbow')!== false){
			$FromCompany = 'FRESHER BUSINESS LIMITED';
			$FromPersonName = 'C/O ProFulfillment and Logistics';
			/*-------Updated on 12-02-2020-----*/
			$Building = 'Unit 4 Airport Cargo Centre';
			$FromAddress = 'L\'avenue De La Commune';
			$FromCity = 'St Peter';
			$FromDivision = 'JE';
			$FromPostCode = 'JE3 7BY';
			$FromCountryCode = 'GB';
			$FromCountryName = 'JERSEY';
			$FromPhoneNumber = '0123456789';
			$email = 'accounts@fresherbusiness.co.uk'; 
			
			$VAT_Number = 'GB 318649182';  
 			if(in_array($country ,['United Kingdom','GB','UK'])){    
				$VAT_Number   = 'GB 318649182'; 
    		}else if(in_array($country ,['Germany','DE'])){ 
 				$VAT_Number   = 'DE 327173988'; 
			}else if(in_array($country ,['France','FR'])){ 
				$VAT_Number   = 'FR 48880490255';  
			}else if(in_array($country ,['Italy','IT'])){ 
				$VAT_Number   = 'IT 11061000961'; 
			} 
		}else{
			$FromCompany = 'EURACO GROUP LTD';
			/*-------Updated on 12-02-2020-----*/
			$FromPersonName = 'C/O ProFulfillment and Logistics';
			$Building = 'Unit 4 Airport Cargo Centre';
			$FromAddress = 'L\'avenue De La Commune';
			$FromCity = 'St Peter';
			$FromDivision = 'JE';
			$FromPostCode = 'JE3 7BY';
			$FromCountryCode = 'GB';
			$FromCountryName = 'JERSEY';
			$FromPhoneNumber = '+44 3301170104';
			$email = 'accounts@euracogroup.co.uk';
			 
 			$VAT_Number = 'GB 304984295';  
 			if(in_array($country ,['United Kingdom','GB','UK'])){  
				$VAT_Number   = 'GB 304984295'; 
    		}else if(in_array($country ,['Germany','DE'])){
 				$VAT_Number   = 'DE 321777974'; 
			}else if(in_array($country ,['France','FR'])){
				$VAT_Number   = 'FR 59850070509'; 
			}else if(in_array($country ,['Spain','ES'])){
				$VAT_Number   = 'ES N6061518D'; 
			}else if(in_array($country ,['Italy','IT'])){
				$VAT_Number   = 'IT 10905930961'; 
			}	
		}
		
		$senderData['email']	  	   =    $email;
		$senderData['FromCompany']	   =    $FromCompany;
		$senderData['FromPersonName']  =	$FromPersonName;
		$senderData['Building']	   	   =	$Building ;
		$senderData['FromAddress']	   =	$FromAddress ; 
		$senderData['FromCity'] 	   =	$FromCity;
		$senderData['FromDivision']    =	$FromDivision;
		$senderData['FromPostCode']    =	$FromPostCode;
		$senderData['FromCountryCode'] =	$FromCountryCode;
		$senderData['FromCountryName'] =	$FromCountryName;
		$senderData['FromPhoneNumber'] =	$FromPhoneNumber;
		$senderData['vat_number'] 	   =	$VAT_Number;

 		return $senderData;	
	}
	
 	public function getBarcodeOutside($spilt_order_id  = null)
	{ 
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'AssignService' );
		$this->loadModel( 'MergeUpdate' );
		
		//$uploadUrl = $this->getUrlBase();
		$imgPath = WWW_ROOT .'img/orders/barcode/';   
		
		
		// $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.status' => '0')));
		
		if($spilt_order_id != ''){
		 $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => $spilt_order_id)));
		}else{
		 $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.status' => '0')));
		}
		
		
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
		//$font = new BCGFontFile(APP .'Vendor/barcodegen/font/Arial.ttf', 13);
		$colorFront = new BCGColor(0, 0, 0);
		$colorBack = new BCGColor(255, 255, 255);
		
		if( count($allSplitOrders) > 0 )	
		{
		  foreach( $allSplitOrders as $allSplitOrder )
		  {					  
			  $id 			= 		$allSplitOrder['MergeUpdate']['id'];
			  $openorderid	=	 	$allSplitOrder['MergeUpdate']['product_order_id_identify'];
			  $barcodeimage	=	$openorderid.'.png';
			  
				$orderbarcode=$openorderid;
				$code128 = new BCGcode128();
				$code128->setScale(2);
				$code128->setThickness(20);
				$code128->setForegroundColor($colorFront);
				$code128->setBackgroundColor($colorBack);
				$code128->setLabel(false);
				$code128->parse($orderbarcode);
									
				//Drawing Part
				$imgOrder128=$orderbarcode.".png";
				$imgOrder128path=$imgPath.'/'.$orderbarcode.".png";
				$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
				$drawing128->setBarcode($code128);
				$drawing128->draw();
				$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);
			  
			  if( $allSplitOrder['MergeUpdate']['product_order_id_identify'] != "" )
			  {
				  //$content = file_get_contents($uploadUrl.$openorderid);
				  //file_put_contents($imgPath.$barcodeimage, $content);
				  
				  $data['MergeUpdate']['id'] 	=  $id;
				  $data['MergeUpdate']['order_barcode_image'] 	=  $barcodeimage;
				  $this->MergeUpdate->save($data);
			  }
		  }
		}
		
		
	}
			 
	 public function updateManifestDate($merge_id)
	   {
		  $this->loadModel('MergeUpdate'); 
		  date_default_timezone_set('Europe/Jersey');
		  $firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		  $lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		  $manifest_username = $firstName.' '.$lastName;
		  $data['id'] = $merge_id;   
		  $data['manifest_date'] = date('Y-m-d H:i:s');   
		  $data['manifest_username'] 	= $manifest_username;   
		  $this->MergeUpdate->saveAll( $data);	    	
	   }
	   
     public function addressInfo($order_id)
	   {
			$this->loadModel('OpenOrder'); 
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id ) ) );
			$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
			$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
			pr($customer_info);
			
			
			preg_match_all('/([\d]+)/', $customer_info->Address->Address1, $match);

    		//pr($match);
			echo $match[0][0];
  			echo $int = (int) filter_var($customer_info->Address->Address1, FILTER_SANITIZE_NUMBER_INT);
			echo ' = '.str_replace($int,' ', $customer_info->Address->Address1);
			
			echo ' <br> ';
			echo $int = (int) filter_var($customer_info->Address->PhoneNumber, FILTER_SANITIZE_NUMBER_INT);
			preg_match_all('/([\d]+)/', $customer_info->Address->PhoneNumber, $telmatch);
 			echo '<br>';
    		// pr($match);
			 $tel ='';
			 for($i = 0; $i< count($telmatch[0]); $i++){
				$tel .= $telmatch[0][$i];
 			 }
  			 $country_pcode = '+';
			 if($customer_info->Address->Country == 'France'){
				 $country_pcode = '+33';
			 }else if($customer_info->Address->Country == 'Germany'){
				 $country_pcode = '+49';
			 }else if($customer_info->Address->Country == 'Germany'){
				 $country_pcode = '+49';
			 }
			 echo $country_pcode.substr($tel,-10);
			exit;
	}
	
}
?>
