<?php
class ArchiveSearchController extends AppController
{
    var $name = "ArchiveSearch";    
    var $components = array('Session','Upload','Common','Auth');    
    var $helpers = array('Html','Form','Common','Session');
    
    public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->Auth->Allow(array('getPcName'));
	}
  
    public function index()
    {
		$this->layout = '';
		$this->autoRander = false;
		print 'Index';
 		exit;
	}
	public function searchOrder($searchKeyNote = null)
	{ 
		$this->loadModel('OpenOrdersArchive');
		$this->loadModel('MergeUpdatesArchive');
		$this->loadModel('OrderNote');
		
		 
		$getdetails	=	$this->OpenOrdersArchive->find( 'all', array(
			'conditions' => array(
					'OpenOrdersArchive.num_order_id' => $searchKeyNote
				)
			)
		);
 		 
		$oversell_items = '';
		$darray['oversell_items'] = $oversell_items;
 		
		if( count($getdetails) == 0 )
		{
			
			$getdetails	=	$this->OpenOrdersArchive->find( 'all', array(
				'conditions' => array(
						array( 'OpenOrdersArchive.general_info like ' => "%{$searchKeyNote}%" )	
					)
				)
			);
			
		}
		
		$i = 0;
		if( count($getdetails) > 0 )
		{
			foreach($getdetails as $getdetail)
			{
				$id 			= 	$getdetail['OpenOrdersArchive']['num_order_id'];	
			 
				$itemDetails	=	$this->getItemList( $id );						
				
				if( count($itemDetails) > 0 )									
				{
					$results[$i]['NumOrderId']		=	 $getdetail['OpenOrdersArchive']['num_order_id'];
					$results[$i]['amazon_order_id']	=	 $getdetail['OpenOrdersArchive']['amazon_order_id'];
					$results[$i]['OrderId']			=	 $getdetail['OpenOrdersArchive']['order_id'];
					$results[$i]['OpenStatus']		=	 $getdetail['OpenOrdersArchive']['status'];
					$results[$i]['sub_source']		=	 $getdetail['OpenOrdersArchive']['sub_source'];
					$results[$i]['linn_fetch_orders']		=	 $getdetail['OpenOrdersArchive']['linn_fetch_orders'];
					$results[$i]['date']		    =	 $getdetail['OpenOrdersArchive']['date'];
					$results[$i]['process_date']	=	 $getdetail['OpenOrdersArchive']['process_date'];
					$results[$i]['Items'] 			=	 $itemDetails;				
					$results[$i]['GeneralInfo']		=	 unserialize($getdetail['OpenOrdersArchive']['general_info']);
					$results[$i]['ShippingInfo']	=	 unserialize($getdetail['OpenOrdersArchive']['shipping_info']);
					$results[$i]['CustomerInfo']	=	 unserialize($getdetail['OpenOrdersArchive']['customer_info']);
					$results[$i]['TotalsInfo']		=	 unserialize($getdetail['OpenOrdersArchive']['totals_info']);
					$results[$i]['FolderName']		=	 unserialize($getdetail['OpenOrdersArchive']['folder_name']);
					$results[$i]['OpenItems']		=	 unserialize($getdetail['OpenOrdersArchive']['items']);
					$results[$i]['parent_id']		=	 $getdetail['OpenOrdersArchive']['parent_id'];
					$results[$i]['notes']			=	 $getdetail['OpenOrdersArchive']['notes'];
	
					// changes by amit rawat
					 
					 
					$results[$i]['parent_id']		=	 $getdetail['OpenOrdersArchive']['parent_id'];
					// changes by amit rawat
				}
				else
				{
					$hiddenArray[] = $id;
					//Cancel order popup required
					$results[$i]['NumOrderId']		=	 $getdetail['OpenOrdersArchive']['num_order_id'];
					$results[$i]['amazon_order_id']	=	 $getdetail['OpenOrdersArchive']['amazon_order_id'];
					$results[$i]['OrderId']			=	 $getdetail['OpenOrdersArchive']['order_id'];
					$results[$i]['OpenStatus']		=	 $getdetail['OpenOrdersArchive']['status'];
					$results[$i]['date']		    =	 $getdetail['OpenOrdersArchive']['date'];						
					$results[$i]['GeneralInfo']		=	 unserialize($getdetail['OpenOrdersArchive']['general_info']);
					$results[$i]['ShippingInfo']	=	 unserialize($getdetail['OpenOrdersArchive']['shipping_info']);
					$results[$i]['CustomerInfo']	=	 unserialize($getdetail['OpenOrdersArchive']['customer_info']);
					$results[$i]['TotalsInfo']		=	 unserialize($getdetail['OpenOrdersArchive']['totals_info']);	
 					$results[$i]['OpenItems']		=	 unserialize($getdetail['OpenOrdersArchive']['items']);
 					$results[$i]['notes']			=	 $getdetail['OpenOrdersArchive']['notes'];					
 					// changes by amit rawat
					$results[$i]['IsResend']		=	 $getdetail['OpenOrdersArchive']['is_resend'];	 
											
					$results[$i]['parent_id']		=	 $getdetail['OpenOrdersArchive']['parent_id'];
					// changes by amit rawat
					//$this->render('open_order_search_result');
				}				
			$i++;
			}
			if( isset($results)  )
			{
				$results	=	json_decode(json_encode($results),0);
				
				$numID	=	$getdetail['OpenOrdersArchive']['num_order_id'];
				
				$getOrderDate	=	$this->MergeUpdatesArchive->find( 'first', array(
						'conditions' => array(
								'MergeUpdatesArchive.order_id' => $numID
							)
						)
					);
				
				$pDate = $getOrderDate['MergeUpdatesArchive']['order_date'];
	//changes by amit rawat
				$orderRelation = $this->OpenOrdersArchive->find('first',array(
					'conditions' => array(
						'OpenOrdersArchive.parent_id' => $numID
					),
					'fields' => array('OpenOrdersArchive.num_order_id'),
				));
				
				$childOrderId='';
				if(!empty($orderRelation['OpenOrdersArchive']['num_order_id'])) {   
					$childOrderId=$orderRelation['OpenOrdersArchive']['num_order_id'];
				}
				$darray['childorderid'] = $childOrderId;
				//changes by amit rawat
				$darray['results'] = $results;
				$darray['pDate'] = $pDate;
			}
			 
		}
		else
		{
			$darray = [];
			//exit;
		}
		return $darray;
	}
		
	 public function getItemList( $bundleId = null )
	 {
			
			/* for custom postal service */
			$this->loadModel( 'MergeUpdatesArchive' );
			$this->loadModel( 'Template' );
			$this->loadModel( 'PackagingSlip' );
			
			$this->MergeUpdatesArchive->bindModel(
				array(
					'hasOne' => array(
						'PackagingSlip' => array(
							'foreignKey' => false,
							'conditions' => array('PackagingSlip.id = MergeUpdatesArchive.packSlipId'),
							'fields' => array('PackagingSlip.id,PackagingSlip.name')
						),
						'Template' => array(
							'foreignKey' => false,
							'conditions' => array('Template.id = MergeUpdatesArchive.lable_id'),
							'fields' => array('Template.id,Template.label_name')
						)
					)
				)
			);
			 
		// $orderItem	=	$this->MergeUpdatesArchive->find('all', array('conditions' => array('MergeUpdatesArchive.order_id' => $bundleId)));			 			 			 				
		 
		$sql  = "SELECT  * FROM merge_updates_archives as MergeUpdate WHERE order_id = '".$bundleId."' ";	
		$orderItem = $this->MergeUpdatesArchive->query($sql) or die($sql.$conn->error);
		
				
		 return $orderItem;	 	 
			
	}
	
	public function searchOrder__($order_id = null)
	{ 
		 
	
		$servername = "localhost";
		$username = "abd56b95_nextmp";
		$password = "AcedCountGyroLilies39";
		$dbname = "abd56b95_archive_wms";
		
		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		
		$sql  = "SELECT  num_order_id,order_id,status,sub_source,linn_fetch_orders,date,process_date, general_info,shipping_info,customer_info,totals_info,parent_id,items FROM open_orders_23012020 WHERE num_order_id = '".$order_id."' ";	
		$_results = $conn->query($sql) or die($sql.$conn->error);
		
		 
		if($_results->num_rows > 0){
			$obj = $_results->fetch_object();	
			
			$i= 0;
			$itemDetails = '';;
			$results[$i]['NumOrderId']		=	 $obj->num_order_id;
			$results[$i]['OrderId']			=	 $obj->order_id;
			$results[$i]['OpenStatus']		=	 $obj->status;
			$results[$i]['sub_source']		=	 $obj->sub_source;
			$results[$i]['linn_fetch_orders']=	 $obj->linn_fetch_orders;
			$results[$i]['date']		    =	 $obj->date;
			$results[$i]['process_date']	=	 $obj->process_date;
			$results[$i]['Items'] 			=	 $itemDetails;				
			$results[$i]['GeneralInfo']		=	 unserialize($obj->general_info);
			$results[$i]['ShippingInfo']	=	 unserialize($obj->shipping_info);
			$results[$i]['CustomerInfo']	=	 unserialize($obj->customer_info);
			$results[$i]['TotalsInfo']		=	 unserialize($obj->totals_info); 
			// changes by amit rawat
			 
			 
			$results[$i]['parent_id']		=	 $obj->parent_id;
				
			//echo '<tr><td align="center"><a href="'.$url.'/Invoice/Generate/'.$obj->num_order_id.'?'.$myarray.'" target="_blank">Generate Invoice : '.$obj->num_order_id.'</a></td></tr>';
		}else{	
			$sql  = "SELECT num_order_id,order_id,status,sub_source,linn_fetch_orders,date,process_date, general_info,shipping_info,customer_info,totals_info,parent_id,items FROM open_orders_23012020 WHERE general_info LIKE '%".$order_id."%' ";	
			$_results = $conn->query($sql) or die($sql.$conn->error);
			if($_results->num_rows > 0){ 
				$obj = $_results->fetch_object();	
				$gnfo = unserialize($obj->general_info);
				 
				if( $obj->sub_source == 'costbreaker' || $gnfo->ReferenceNum == $order_id){  
					
				
					$i= 0;
					$itemDetails = '';;
					$results[$i]['NumOrderId']		=	 $obj->num_order_id;
					$results[$i]['OrderId']			=	 $obj->order_id;
					$results[$i]['OpenStatus']		=	 $obj->status;
					$results[$i]['sub_source']		=	 $obj->sub_source;
					$results[$i]['linn_fetch_orders']=	 $obj->linn_fetch_orders;
					$results[$i]['date']		    =	 $obj->date;
					$results[$i]['process_date']	=	 $obj->process_date;
					$results[$i]['Items'] 			=	 $itemDetails;				
					$results[$i]['GeneralInfo']		=	 unserialize($obj->general_info);
					$results[$i]['ShippingInfo']	=	 unserialize($obj->shipping_info);
					$results[$i]['CustomerInfo']	=	 unserialize($obj->customer_info);
					$results[$i]['TotalsInfo']		=	 unserialize($obj->totals_info); 
					// changes by amit rawat
					 
					$results[$i]['parent_id']		=	 $obj->parent_id;
					// changes by amit rawat
					//$this->render('open_order_search_result');
				//	echo '<tr><td align="center"><a href="'.$url.'/Invoice/Generate/'.$obj->num_order_id.'?'.$myarray.'" target="_blank">Generate Invoice : '.$obj->num_order_id.'</a></td></tr>';
				}else{
				  // getInvoice_2019($order_id);
				}
			}else{
				   //getInvoice_2019($order_id);
			}
		}
	
		 
		$oversell_items = array() ;
	 
		$darray['oversell_items'] = $oversell_items;
		 
		$note ='';
		$darray['order_note'] = $note;
		
		$i = 0;
			 
			
			if( isset($results)  )
			{
				$results	=	json_decode(json_encode($results),0);
				
				//$numID	=	$getdetail['OpenOrder']['num_order_id'];
				
				/*$getOrderDate	=	$this->MergeUpdate->find( 'first', array(
						'conditions' => array(
								'MergeUpdate.order_id' => $numID
							)
						)
					);*/
				
				$pDate = '2020-01-11';//$getOrderDate['MergeUpdate']['order_date'];
	//changes by amit rawat
				/*$orderRelation = $this->OpenOrder->find('first',array(
					'conditions' => array(
						'OpenOrder.parent_id' => $numID
					),
					'fields' => array('OpenOrder.num_order_id'),
				));
				
				$childOrderId='';
				if(!empty($orderRelation['OpenOrder']['num_order_id'])) {   
					$childOrderId=$orderRelation['OpenOrder']['num_order_id'];
				}*/
				
				$childOrderId = '';
				$darray['childorderid'] = $childOrderId;
				//changes by amit rawat
				$darray['results'] =  $results;  
				$darray['pDate'] =  $pDate;
			}
			 
			 return $darray;
		 
	}
}

?>
