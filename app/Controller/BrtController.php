<?php
//error_reporting(0);
	App::uses('Folder', 'Utility');
	App::uses('File', 'Utility');
class BrtController extends AppController
{
    
    var $name = "Brt";
    var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
    
    public function index()
    {
		echo "Index";
		exit;
	}
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('generateBrtLabels','getBrtSheetCustom','getBrtSheet'));
    }
	
	public function generateBrtLabels($order_id = null)
	{		
			$this->loadModel('OpenOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('ItalyCode');
			$this->loadModel('BrtSerialNo');
			$italyOrders = $this->OpenOrder->find('all', array('conditions' => array('OpenOrder.num_order_id' => $order_id ) ) );
			
			$brtData = array();
			if( count($italyOrders) > 0 ){
				foreach( $italyOrders as $italyOrder )
				{
					
					$italyOrder['OpenOrder']['num_order_id'];
					$orderdetails	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $italyOrder['OpenOrder']['num_order_id']) ) );
					
					if( count($orderdetails) > 0 ){
						$series_no = 1;
						foreach($orderdetails as $orderdetail)
						{					
							$brtSerialNo = $this->BrtSerialNo->find('first', array('order' => 'id desc') );
							$parcel_no   =  '0000001';
							//0000001 to 9999999
							if(count($brtSerialNo) > 0){
								//$series_no = $brtSerialNo['BrtSerialNo']['serial_no'];	
								$parcel_no = $brtSerialNo['BrtSerialNo']['parcel_no'] + 1;
									
							}else{
								$parcel_no = $parcel_no + 1;
							}						
							
							$store =	$italyOrder['OpenOrder']['sub_source'];
							 
							if($store == 'CostBreaker_IT' || $store == 'CostBreaker_DE' || $store == 'CostBreaker_FR' || $store == 'CostBreaker_ES'){
							 	$store = 'COST-DROPPER/ESL';
							} else if($store == 'Rainbow_Retail_IT' || $store == 'Rainbow Retail' || $store == 'Rainbow_Retail_ES' || $store == 'RAINBOW RETAIL DE'){
								$store = 'RAINBOW RETAIL/ESL';
							} else if($store == 'Tech_Drive_ES' || $store == 'Tech_Drive_UK' || $store == 'Tech_Drive_DE' || $store == 'Tech_Drive_FR'){
								$store = 'TECHDRIVEE/ESL';
							} else {
								$store = 'ESL Limited';
							}
							
							
							$general_info	=	unserialize( $italyOrder['OpenOrder']['general_info'] );
							$shipping_info	=	unserialize( $italyOrder['OpenOrder']['shipping_info'] );
							$customer_info	=	unserialize( $italyOrder['OpenOrder']['customer_info'] );
							$totals_info	=	unserialize( $italyOrder['OpenOrder']['totals_info'] );
							$items			=	unserialize( $italyOrder['OpenOrder']['items'] ); 
							
							
							$consignee_address 		= 	$customer_info->Address->Address1.' '.$customer_info->Address->Address2;
							$consigee_cap_zip_code  = 	$customer_info->Address->PostCode;
							$consignee_city			=	$customer_info->Address->Town;//city;					
							$name					=	$customer_info->Address->FullName;
							$email					=	$customer_info->Address->EmailAddress;
							$postcode 				=   str_pad($customer_info->Address->PostCode,5,"0",STR_PAD_LEFT); 
							$comany 				= 	($customer_info->Address->Company != '') ? $customer_info->Address->Company : $name; 
							$weight					= 	$orderdetail['MergeUpdate']['packet_weight'];
							$split_order_id			= 	$orderdetail['MergeUpdate']['product_order_id_identify'];
							
							$getcountery		    =	$this->ItalyCode->find('first', array( 'conditions' => array( 'ItalyCode.postal_code' => $postcode ) ) );
							if(count($getcountery) == 1)
							$state_abravation	=	$getcountery['ItalyCode']['state_abbreviation'];
							
							//echo "<br>";
							
							$data['TipoLancio'] =  "";
							$data['RicercaPOArrivo']= "S";
							$data['AnnoSpedizione']= date('Y');
							$data['CodiceProdotto']= $split_order_id;
							//$data['DestinatarioCap']= $consigee_cap_zip_code;
							$data['DestinatarioCap']= trim(filter_var($postcode, FILTER_SANITIZE_NUMBER_INT));
							//$data['DestinatarioIndirizzo']= $this->replaceFrenchChar(utf8_encode($consignee_address));
							$data['DestinatarioIndirizzo']= utf8_encode($consignee_address);
							$data['DestinatarioLocalita']= $consignee_city;
							$data['DestinatarioNazione']= "";
							$data['DestinatarioProvincia']= $state_abravation;
							$data['DestinatarioRagioneSociale']= $this->replaceFrenchChar($comany);
							$data['FilialeSegnaCollo'] = "166";
							$data['MeseGiornoSpedizione'] = date('md');
							$data['MittenteNazione']= "";
							$data['MittenteProvincia']= "JE";
							//$data['MittenteRagioneSociale']= "ESL Jersey";
							$data['MittenteRagioneSociale']= $store;
							$data['Network']= "";
							$data['NumeroSegnaCollo'] = $parcel_no;//str_pad($parcel_no,7,"0",STR_PAD_LEFT); 
							//$data['NumeroSerie']= $series_no;
							$data['NumeroSerie']= '67';
							$data['NumSegnaColloDi']= "1";
							$data['Peso']= ceil($weight);
							$data['POPartenza']= "166";
							$data['Volume']= 0;
							$data['Ristampa']= "R";
							$data['PrimaConsegnaParticolare']= "";
							$data['SecondaConsegnaParticolare']= "";
							$data['TipoPorto']= "F";
							$data['TipoServizioBolle']= "C";
							$data['TipoStampante']= "1"; 
							//pr(json_encode($data)); 							 
							//exit;
						   $brtData = $this->getBrtBarcodeData($data);
						   if($brtData != 'error')
						   {
							  $curl = curl_init();
			
							  curl_setopt_array($curl, array(
							  CURLOPT_URL => "http://31.14.134.40/api/LabelProcessor?LabelFormat=4",
							  CURLOPT_RETURNTRANSFER => true,
							  CURLOPT_ENCODING => "",
							  CURLOPT_MAXREDIRS => 10,
							  CURLOPT_TIMEOUT => 30,
							  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							  CURLOPT_CUSTOMREQUEST => "POST",
							  CURLOPT_POSTFIELDS => "[".json_encode($data)."]",
							  CURLOPT_HTTPHEADER => array(
								"cache-control: no-cache",
								"content-type: application/json",
								"password: Test01",					 
								"token: eyJ1bmlxdWVfbmFtZSI6InRvZGQiLCJ",
								"username: Test"
							  ),
							));
							
							$response = curl_exec($curl);
							$err = curl_error($curl);
							curl_close($curl);
							
							if ($err) {
								//echo "cURL Error #:" . $err;  
								file_put_contents(WWW_ROOT.'/logs/brtlog.log', "\nLabel Error\t".date('d-m-y H:i:s')."\t".$split_order_id."\n". $err."\n", FILE_APPEND | LOCK_EX);	
							} else {						
								$obj = json_decode($response) ;	 
								if(isset($obj->Message)){	
										$mailBody = '<p><strong>'.$obj->ExceptionMessage.' </strong></p>';
										App::uses('CakeEmail', 'Network/Email');
										$email = new CakeEmail('');
										$email->emailFormat('html');
										$email->from('info@euracogroup.co.uk');
										$email->to( array('avadhesh@euracogroup.co.uk','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));					  
										$getBase = Router::url('/', true);
										$email->subject('BRT orders issue in Xsensys' );
										$email->send( $mailBody );	
									//$obj->ExceptionMessage						
									file_put_contents(WWW_ROOT.'/logs/brtlog.log', "\nLabel Error\t".date('d-m-y H:i:s')."\t".$split_order_id."\n". $response . "\n", FILE_APPEND | LOCK_EX);	
								}else{	
								
								/*-------------------------Fetch Barcode from BRT----------------------*/
									//$brtData = $this->getBrtBarcodeData($data);
									//pr($brtData);
								/*------------------------End Fetch Barcode from BRT---------------------*/
									if(count($brtData) > 0){
										$brtData['serial_no']	   =  $series_no;
										$brtData['parcel_no'] 	   =  $parcel_no;
										$brtData['split_order_id'] =  $split_order_id;
										$brtData['added_date'] 	   =  date('Y-m-d') ; 
										
									echo	$brtbarcode	=	$brtData['barcode'];
										
										$labelPdfPath = WWW_ROOT .'img/brt/label_'.$split_order_id.'.pdf'; 			 
										$datas = base64_decode( $response ); 				
										file_put_contents($labelPdfPath , $datas); 
										file_put_contents(WWW_ROOT.'/logs/brt_labels_'.date('dmy').'.log', "\n".date('d-m-y H:i:s')."\t".$split_order_id."\n". $response."\n", FILE_APPEND | LOCK_EX);	
																				
										$this->MergeUpdate->updateAll(array('service_provider' => '\'BRT\'', 'brt' => 1, 'brt_phone' => 1, 'brt_exprt' => '1', 'brt_barcode' => $brtbarcode, 'track_id' => $brtbarcode, 'reg_post_number' => $brtbarcode), array('product_order_id_identify' => $split_order_id));	
										$this->BrtSerialNo->saveAll($brtData);   
					
										}	 
									 }
										 
							}	
																					  
						}	
					 }							 
				}
			}
	  }
	//exit;
	}
	
	public function getBrtBarcodeData($data)
	{
		$split_order_id = $data['CodiceProdotto'];
		$brtData = array();
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://31.14.134.40/api/Barcode?LabelFormat=1",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "[".json_encode($data)."]",
		  CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: application/json",
			"password: Test01",					
			"token: eyJ1bmlxdWVfbmFtZSI6InRvZGQiLCJ",
			"username: Test"
		  ),
		));
		
		$bcode_response = curl_exec($curl);
		$bcode_err = curl_error($curl);
		
		curl_close($curl);
		
		if ($bcode_err) {		  
			file_put_contents(WWW_ROOT.'/logs/brtlog.log', "\nBarcode Error\t".date('d-m-y H:i:s')."\t".$split_order_id."\n". $bcode_err . "\n", FILE_APPEND | LOCK_EX);	
		} else {
			  $bcode_obj = json_decode($bcode_response) ; 
			  if(isset($bcode_obj->Message)){	
			  		$mailBody = '<p><strong>'.$bcode_obj->ExceptionMessage.' </strong></p>';
					App::uses('CakeEmail', 'Network/Email');
					$email = new CakeEmail('');
					$email->emailFormat('html');
					$email->from('info@euracogroup.co.uk');
					$email->to( array('avadhesh@euracogroup.co.uk','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));					  
					$getBase = Router::url('/', true);
					$email->subject('BRT orders issue in Xsensys' );
					$email->send( $mailBody );	
				  file_put_contents(WWW_ROOT.'/logs/brtlog.log', "\nBarcode Error\t".date('d-m-y H:i:s')."\t".$split_order_id."\n". $bcode_response . "\n", FILE_APPEND | LOCK_EX);	
			  }else{			
					$destinations = $bcode_obj->destinations[0];	
					if($destinations->POArrivo != '0' || $destinations->POArrivoDes != '' || $destinations->POPartenzaDes != '' || $destinations->TerminalArrivo != '0' || $destinations->DestinatarioProvincia != '')
					{
						$brtData['iso_country']  = $destinations->NazioneISO;	
						$brtData['province_recipient'] = $destinations->DestinatarioProvincia;
						$brtData['po_arrival'] = $destinations->POArrivo;
						$brtData['po_arrival_des'] = $destinations->POArrivoDes;
						$brtData['po_departure_des'] = $destinations->POPartenzaDes;
						$brtData['arrival_terminal'] = $destinations->TerminalArrivo;
						$brtData['zone_mark_neck'] = $destinations->ZonaSegnaCollo;
						$brtData['barcode']  = $bcode_obj->Barcodes[0];	
					 } else {
						$mailBody = '<p><strong>Please check Xsensys Orders :- '.$split_order_id.'</strong></p>';
						App::uses('CakeEmail', 'Network/Email');
						$email = new CakeEmail('');
						$email->emailFormat('html');
						$email->from('info@euracogroup.co.uk');
						$email->to( array('avadhesh.kumar@jijgroup.com','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));	
						$email->subject('BRT orders issue in Xsensys' );
						$email->send( $mailBody );
						$brtData = 'error';	
						exit;
					}	  			 
			 }
		}
		return $brtData;							  
	}
	
	public function getBrtSheet()
	{
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$italyOrders = $this->OpenOrder->find('all', array('conditions' => array('OpenOrder.status' => 0, 'OpenOrder.destination' => 'Italy') ) );
				
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'VABCCM'); 
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'VABCTR'); 
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'VABLNP'); 
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'VABCBO'); 
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'VABTSP'); 	
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'VABNSP'); 
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'VABRMN'); 
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'VABRSD'); 
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'VABRD2'); 
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'VABIND'); 
		$objPHPExcel->getActiveSheet()->setCellValue('K1', 'VABCAD'); 
		$objPHPExcel->getActiveSheet()->setCellValue('L1', 'VABLOD'); 
		$objPHPExcel->getActiveSheet()->setCellValue('M1', 'VABPRD'); 
		$objPHPExcel->getActiveSheet()->setCellValue('N1', 'VABNZD'); 
		$objPHPExcel->getActiveSheet()->setCellValue('O1', 'VABNCL'); 
		$objPHPExcel->getActiveSheet()->setCellValue('P1', 'VABPKB'); 
		$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'VABNRC'); 
		$objPHPExcel->getActiveSheet()->setCellValue('R1', 'VABTRC'); 
		$objPHPExcel->getActiveSheet()->setCellValue('S1', 'VABCAS'); 
		$objPHPExcel->getActiveSheet()->setCellValue('T1', 'VABTIC');
		$objPHPExcel->getActiveSheet()->setCellValue('U1', 'VABEMD'); 
		$objPHPExcel->getActiveSheet()->setCellValue('V1', 'VABCEL'); 
		$objPHPExcel->getActiveSheet()->setCellValue('W1', 'VABTNO'); 
		$i = 2;
		foreach( $italyOrders as $italyOrder )
		{
				
				$italyOrder['OpenOrder']['num_order_id'];
				$orderdetails	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $italyOrder['OpenOrder']['num_order_id']) ) );
				
				$general_info	=	unserialize( $italyOrder['OpenOrder']['general_info'] );
				$shipping_info	=	unserialize( $italyOrder['OpenOrder']['shipping_info'] );
				$customer_info	=	unserialize( $italyOrder['OpenOrder']['customer_info'] );
				$totals_info	=	unserialize( $italyOrder['OpenOrder']['totals_info'] );
				$items			=	unserialize( $italyOrder['OpenOrder']['items'] );
				
				$customer_info->Address->Address1;
				$customer_info->Address->PostCode;
				$customer_info->Address->Region;//provience
				$customer_info->Address->Town;//city
				$customer_info->Address->Company;//Company
				$customer_info->Address->PhoneNumber;
				$random_number = mt_rand(100000, 999999);
				$unique_number = mt_rand(100, 999);
				//exit;
				
				$vitrual_delete	=	'';
				$sender_customer_code				=	$random_number;
				$departure_depot					=	'';
				$shipment_year						=	 date("Y");
				$shipment_monty_day					=	 date("m/d");
				$consignee_address 					= 	$customer_info->Address->Address1;;
				$consigee_cap_zip_code 				= 	$customer_info->Address->PostCode;;
				$consignee_city						=	$customer_info->Address->Town;//city;
				$consignee_province_abbreviation	=	$customer_info->Address->Region;//provience;
				$name								=	$customer_info->Address->FullName;
				$email								=	$customer_info->Address->EmailAddress;
				$consignee_country	=	'';
				$service_type	=	'E';
				$shipment_insurance_amount	=	'';
				$currency_shipment_insurance_amount	=	'';
				$sender_parcel_type = '';
				$number_of_parcel = '1';
				$weight_in_kg =	'';
				$volume_in_m = '';//if known
				$quantity_tobe_invoiced	=	'';
				$cash_on_delivery	=	'No';
				$cod_payment_type	=	'No';
				$cod_currency	=	'No';
				$cod_particularity_management_code	=	'';
				$numericsender_reference='';
				$alphabeticsender_reference	=	'';
				$num_percel_num_form	=	'';
				$num_percel_num_to	=	'';
				$parcel_cumulative	=	'';
				$note	=	'';
				$extension_of_note	=	'';
				$delivery_zone	=	'';
				$parcels_handling_code = '';
				$hold_for_pickup	=	'';
				$delivery_date_required	=	'';
				$delivery_type	=	'';
				$delivery_time_required = '';
				$taxation_code	=	'';
				$flag_sender_tariff_code_force = '';
				$declared_parcel_value	=	'';
				$currency_declared_parcel_value	=	'';
				$particularities_delivery_management_code	=	'';
				$particularities_holdonstock_mamagement_code	=	'';
				$various_particularities_management_code	=	'';
				$first_particular_deliver	=	'';
				$second_particular_deliver	=	'';
				$social_code	=	'';
				$type_and_quantity_pallets	=	'';
				$original_sender_company_name	=	'';
				$zipcode_original_sender	=	'';
				$country_original_sender	=	'';
				
				foreach($orderdetails as $orderdetail)
				{
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $sender_customer_code); 
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '0'); 
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '0');       
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '1');
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, 'C');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $orderdetail['MergeUpdate']['product_order_id_identify']); 
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $orderdetail['MergeUpdate']['product_order_id_identify']); 
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $customer_info->Address->Company);       
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, ''); //EXTENSION OF CONSEGNEE COMPANY NAME 
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $consignee_address);//CONSIGNEE ADDRESS
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $consigee_cap_zip_code);//CONSIGNEE CAP/ZIP CODE
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $consignee_city); 
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $consignee_province_abbreviation);       
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '');//country if italy then empty
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '1');
					$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, str_replace('.', ',', $orderdetail['MergeUpdate']['packet_weight'])); //weight in KG
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $name); 
					$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $customer_info->Address->PhoneNumber);       
					$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, '');//cod amount
					$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '');//C.O.D. PAYMENT TYPE
					$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, $email);// NOt Avaliable
					$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, $customer_info->Address->PhoneNumber); //Not Available
					$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, '');// Not available    
					$i++;  
				}
		}
		
		$objPHPExcel->getActiveSheet(0);
		$getBase 		= 	Router::url('/', true);
		$uploadUrl 		= 	WWW_ROOT .'img/brt_shipment.csv';
		$uploadRemote 	= 	$getBase.'img/brt_shipment.csv';
		$objWriter 		= 	PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'img/brt_shipment.csv';
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
		
	}
	
	
	public function getBrtSheetCustom()
	{
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('ItalyCode');
		//$italyOrders = $this->OpenOrder->find('all', array('conditions' => array('OpenOrder.destination' => 'Italy', 'OpenOrder.num_order_id IN' => array(815315,816344,814361,816568,816345,816464,816323,813075,816375,816218) ) ) );
		
		$fp = fopen(WWW_ROOT."img/brt_shipment.csv","w");
		$header = "VABCCM;VABCTR;VABLNP;VABCBO;VABTSP;VABNSP;VABRMN;VABRSD;VABRD2;VABIND;VABCAD;VABLOD;VABPRD;VABNZD;VABNCL;VABPKB;VABNRC;VABTRC;VABCAS;VABTIC;VABEMD;VABCEL;VABTNO\r\n";
		unlink(WWW_ROOT."img/brt_shipment.csv");
		file_put_contents(WWW_ROOT."img/brt_shipment.csv",$header, FILE_APPEND | LOCK_EX);
		
		//file_put_contents(WWW_ROOT."img/brt_shipment.csv",$content, FILE_APPEND | LOCK_EX);
		$i = 2;$j = 0;
		foreach( $italyOrders as $italyOrder )
		{
				$italyOrder['OpenOrder']['num_order_id'];
				$orderdetails	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $italyOrder['OpenOrder']['num_order_id']) ) );
				
			foreach($orderdetails as $orderdetail)
				{
					$general_info	=	unserialize( $italyOrder['OpenOrder']['general_info'] );
					$shipping_info	=	unserialize( $italyOrder['OpenOrder']['shipping_info'] );
					$customer_info	=	unserialize( $italyOrder['OpenOrder']['customer_info'] );
					$totals_info	=	unserialize( $italyOrder['OpenOrder']['totals_info'] );
					$items			=	unserialize( $italyOrder['OpenOrder']['items'] );
					//$random_number = mt_rand(100000, 999999);
					//$unique_number = mt_rand(100, 999);
				
					if((strlen($customer_info->Address->PostCode) < 5) ){
						$postcode = '0'.$customer_info->Address->PostCode;
					} else { 
						$postcode = $customer_info->Address->PostCode;
					}
					
					$getcountery		=	$this->ItalyCode->find('first', array( 'conditions' => array( 'ItalyCode.postal_code' => $postcode ) ) );
					if(count($getcountery) == 1)
					$state_abravation	=	$getcountery['ItalyCode']['state_abbreviation'];
					
					$vitrual_delete	=	'';
					//$sender_customer_code				=	$random_number;
					$departure_depot					=	'';
					$shipment_year						=	 date("Y");
					$shipment_monty_day					=	 date("m/d");
					$consignee_address 					= 	$customer_info->Address->Address1;
					$consigee_cap_zip_code 				= 	$customer_info->Address->PostCode;
					$consignee_city						=	$customer_info->Address->Town;//city;
					$consignee_province_abbreviation	=	$customer_info->Address->Region;//provience;
					$name								=	$customer_info->Address->FullName;
					$email								=	$customer_info->Address->EmailAddress;
					$comany 							= 	($customer_info->Address->Company != '') ? $customer_info->Address->Company : 'ESL';
					//$weight	=	str_replace('.', ',', $orderdetail['MergeUpdate']['packet_weight']);
					$weight				=	$orderdetail['MergeUpdate']['packet_weight'];
					$consignee_country	=	'';
					$weightformate		=	number_format($weight, 1, ',', '');
					
					$weightformate		=	($weightformate == '0,0') ? '0,1' : $weightformate;
					
					$content = '1663925'.";".'300'.";".'166'.";".'1'.";".'c'.";".str_replace('-','',$orderdetail['MergeUpdate']['product_order_id_identify']).";".str_replace('-','',$orderdetail['MergeUpdate']['product_order_id_identify']).";".$comany.";".''.";".$consignee_address.";".$consigee_cap_zip_code.";".$consignee_city.";".$state_abravation.";".''.";".'1'.";".$weightformate.";".$name.";".str_replace('+','',filter_var($customer_info->Address->PhoneNumber, FILTER_SANITIZE_NUMBER_INT)).";".''.";".''.";".$email.";".str_replace('+','',filter_var($customer_info->Address->PhoneNumber, FILTER_SANITIZE_NUMBER_INT)).";".''."\r\n";
					file_put_contents(WWW_ROOT."img/brt_shipment.csv",$content, FILE_APPEND | LOCK_EX);
					
					
			}
			
			$j++;
		}
				
		$getBase 		= 	Router::url('/', true);
		$uploadUrl 		= 	WWW_ROOT .'img/brt_shipment.csv';
		$uploadRemote 	= 	$getBase.'img/brt_shipment.csv';
		$file = 'img/brt_shipment.csv';
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
		
	}  
	
	public function getBrtManifestFiles()
	{
		 $brt ='';
		 $result	= $this->getBrtSheetManifest();
		 
		if($result == 'ok'){
			 $result	= $this->getPhoneNumbreList();
			if($result == 'ok'){
				 $result	= $this->getDataForBrtPalletPrintPdf();
				if($result == 'ok'){
						$this->Session->setFlash('Brt Manifest Files generated.', 'flash_success');	
						$brt = '?brt=1';	
				} else {
					$this->Session->setFlash($result, 'flash_danger');
				}
			}else{
				$this->Session->setFlash($result, 'flash_danger');
			}
		}else{			
			$this->Session->setFlash($result, 'flash_danger');
		}
		
		$this->redirect('/JijGroup/System/Manifest/Create');
	}
	
	
	public function getBrtSheetManifest()
	{
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('ItalyCode');
		$this->loadModel('BrtSerialNo');
		$start_date = date('Y-m-d H:i:s');
		$end_date   = date('Y-m-d H:i:s');
		$italyOrders = $this->MergeUpdate->find('all', array('conditions' => array(
																					'MergeUpdate.status' => 1, 
																					'MergeUpdate.delevery_country'=>'Italy',
																					'MergeUpdate.brt' => 1,
																					'MergeUpdate.manifest_status' => 2,
																					'MergeUpdate.scan_status' => 1,
																					'MergeUpdate.sorted_scanned' => 1
																					) ) );
																					
		/*$italyOrders = $this->MergeUpdate->find('all', array('conditions' => array(
																					'MergeUpdate.status' => 1, 
																					'MergeUpdate.delevery_country'=>'Italy', 
																					'MergeUpdate.brt >' => 0,
																					'MergeUpdate.manifest_date LIKE' => '2018-02-07%',
																					'MergeUpdate.scan_status' => 1,
																					'MergeUpdate.sorted_scanned' => 1
																					) ) );*/
	 																			
																					
		if(count($italyOrders) > 0)
		{																		
		$fp = fopen(WWW_ROOT."img/FNVAB00R.csv","w");
		$sep = ";";
		$header = "VABCCM".$sep."VABLNP".$sep."VABAAS".$sep."VABMGS".$sep."VABNRS".$sep."VABNSP".$sep."VABCBO".$sep."VABLNA".$sep."VABRSD".$sep."VABRD2".$sep."VABIND".$sep."VABCAD".$sep."VABLOD".$sep."VABPRD".$sep."VABNZD".$sep."VABGC1".$sep."VABGC2".$sep."VABCTR".$sep."VABTSP".$sep."VABIAS".$sep."VABVAS".$sep."VABNAS".$sep."VABNCL".$sep."VABPKB".$sep."VABVLB".$sep."VABQFT".$sep."VABCAS".$sep."VABTIC".$sep."VABVCA".$sep."VABGCA".$sep."VABRMN".$sep."VABRMA".$sep."VABNCD".$sep."VABNCA".$sep."VABXCO".$sep."VABNOT".$sep."VABNT2".$sep."VABZNC".$sep."VABCTM".$sep."VABFFD".$sep."VABDCR".$sep."VABTCR".$sep."VABHCR".$sep."VABCTS".$sep."VABFTM".$sep."VABVMD".$sep."VABVAD".$sep."VABGMA".$sep."VABGGA".$sep."VABGVA".$sep."VABTC1".$sep."VABTC2".$sep."VABSCL".$sep."VABANT".$sep."VABRMO".$sep."VABCMO".$sep."VABNMO\r\n";
		unlink(WWW_ROOT."img/FNVAB00R.csv");
		file_put_contents(WWW_ROOT."img/FNVAB00R.csv",$header);
			foreach( $italyOrders as $italyOrder )
			{
					$italyOrder['MergeUpdate']['order_id'];
					$orderdetails = $this->MergeUpdate->find('all', array('conditions'=>array('MergeUpdate.product_order_id_identify'=>$italyOrder['MergeUpdate']['product_order_id_identify'])));
					$italyopen	=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id'=>$italyOrder['MergeUpdate']['order_id'])));
				foreach($orderdetails as $orderdetail)
					{
						$brtdata	=	$this->BrtSerialNo->find('first', array( 'conditions' => array( 'BrtSerialNo.split_order_id'=>$orderdetail['MergeUpdate']['product_order_id_identify'])));
						
						if(!empty($brtdata))
						{
							$split_order_id		= 	$orderdetail['MergeUpdate']['product_order_id_identify'];
							$serial_no			=	$brtdata['BrtSerialNo']['serial_no'];
							$parcel_no			=	$brtdata['BrtSerialNo']['parcel_no'];
							$split_order_id		=	$brtdata['BrtSerialNo']['split_order_id'];
							$iso_country		=	$brtdata['BrtSerialNo']['iso_country'];
							$province_recipient	=	$brtdata['BrtSerialNo']['province_recipient'];
							$po_arrival			=	$brtdata['BrtSerialNo']['po_arrival'];
							$po_arrival_des		=	$brtdata['BrtSerialNo']['po_arrival_des'];
							$po_departure_des	=	$brtdata['BrtSerialNo']['po_departure_des'];
							$arrival_terminal	=	$brtdata['BrtSerialNo']['arrival_terminal'];
							$zone_mark_neck		=	$brtdata['BrtSerialNo']['zone_mark_neck'];
							$barcode			=	$brtdata['BrtSerialNo']['barcode'];
							
							$store =	$italyopen['OpenOrder']['sub_source'];
							 
							if($store == 'CostBreaker_IT' || $store == 'CostBreaker_DE' || $store == 'CostBreaker_FR' || $store == 'CostBreaker_ES'){
							 	$acnumber = '1664097';
							} else if($store == 'Rainbow_Retail_IT' || $store == 'Rainbow Retail' || $store == 'Rainbow_Retail_ES' || $store == 'RAINBOW RETAIL DE'){
								$acnumber = '1664098';
							} else if($store == 'Tech_Drive_ES' || $store == 'Tech_Drive_UK' || $store == 'Tech_Drive_DE' || $store == 'Tech_Drive_FR'){
								$acnumber = '1664099';
							} else {
								$acnumber = '1664070';
							}
							
							
							$general_info		=	unserialize( $italyopen['OpenOrder']['general_info'] );
							$shipping_info		=	unserialize( $italyopen['OpenOrder']['shipping_info'] );
							$customer_info		=	unserialize( $italyopen['OpenOrder']['customer_info'] );
							$totals_info		=	unserialize( $italyopen['OpenOrder']['totals_info'] );
							$items				=	unserialize( $italyopen['OpenOrder']['items'] );
							$orderidwithout		=	str_replace('-','',$orderdetail['MergeUpdate']['product_order_id_identify']);
							$orderidwith		=	$orderdetail['MergeUpdate']['product_order_id_identify'];
							
							$vitrual_delete		=	'';
							$departure_depot					=	'';
							$shipment_year						=	date("Y");
							$shipment_monty_day					=	date("md");
							
							$address1 = rtrim($this->getcommaremovedata($customer_info->Address->Address1),' ');
							$address2 = rtrim($this->getcommaremovedata($customer_info->Address->Address2),' ');
							
							$consignee_address 					= 	rtrim($address1.' '.$address2,' ');
							$consigee_cap_zip_code 				= 	str_pad($customer_info->Address->PostCode,5,"0",STR_PAD_LEFT);
							$consignee_city						=	rtrim($this->getcommaremovedata(htmlspecialchars($customer_info->Address->Town)),' ');//city;
							$consignee_province_abbreviation	=	$this->getcommaremovedata(htmlspecialchars($customer_info->Address->Region));//provience;
							$name								=	rtrim($this->getcommaremovedata($customer_info->Address->FullName),' ');
							$email								=	$this->getcommaremovedata($customer_info->Address->EmailAddress);
							$comany 							= 	$this->getcommaremovedata($customer_info->Address->Company); 
							$weight								=	ceil($orderdetail['MergeUpdate']['packet_weight']);
							$consignee_country					=	'';
							$phonenumber						=	str_replace('+','',filter_var($customer_info->Address->PhoneNumber, FILTER_SANITIZE_NUMBER_INT));
						 
							//str_pad($parcel_no,7,"0",STR_PAD_LEFT)1664070
							//$sno	=	substr($parcel_no, -2);
							
							  $content = $acnumber.$sep.'166'.$sep.$shipment_year.$sep.$shipment_monty_day.$sep.'67'.$sep.$parcel_no.$sep.'1'.$sep.$po_arrival.$sep.$name.$sep.''.$sep.$consignee_address.$sep.$consigee_cap_zip_code.$sep.$consignee_city.$sep.$province_recipient.$sep.''.$sep.''.$sep.''.$sep.'300'.$sep.'c'.$sep.''.$sep.''.$sep.'Goods'.$sep.'1'.$sep.$weight.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.$orderidwithout.$sep.$orderidwith.$sep.str_pad($parcel_no,7,"0",STR_PAD_LEFT).$sep.str_pad($parcel_no,7,"0",STR_PAD_LEFT).$sep.''.$sep.''.$sep.''.$sep.$zone_mark_neck.$sep.'2'.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep."\r\n";
							 
							 file_put_contents(WWW_ROOT."img/FNVAB00R.csv",$content, FILE_APPEND | LOCK_EX);
							$this->MergeUpdate->updateAll(array('MergeUpdate.brt' => '2'), array('MergeUpdate.id' => $orderdetail['MergeUpdate']['id']));
						}
				  }
			}
			 
			$uploadUrl 		= 	WWW_ROOT .'img/FNVAB00R.csv';
			$folderName = 'Service Manifest -'. date("d.m.Y");
			$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
			copy( $uploadUrl, WWW_ROOT .'img/cut_off/'.$folderName.'/FNVAB00R_'.date('m-d-Y H:i:s').'.csv');  
			$status = 'ok';
			} else {
			$status = 'There is no data for FNVAB00R.';
			}
			return $status;
				
			/*header('Content-Description: File Transfer');
			header('Content-Type: application/force-download');
			header('Content-Disposition: attachment; filename='.basename($uploadUrl));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($uploadUrl));   
			readfile($uploadUrl);
			exit;*/
	}
		
	public function getPhoneNumbreList()
	{
	
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('ItalyCode');
		$this->loadModel('BrtSerialNo');
		$italyOrders = $this->MergeUpdate->find('all', array('conditions' => array(
																					'MergeUpdate.status' => 1, 
																					'MergeUpdate.delevery_country'=>'Italy',
																					'MergeUpdate.manifest_status' => 2,
																					'MergeUpdate.scan_status' => 1,
																					'MergeUpdate.sorted_scanned' => 1,
																					'MergeUpdate.brt' => 2,
																					'MergeUpdate.brt_phone' => 1,
																					) ) );
																					
																					
																					
		/*$italyOrders = $this->MergeUpdate->find('all', array('conditions' => array(
																					'MergeUpdate.status' => 1, 
																					'MergeUpdate.delevery_country'=>'Italy', 
																					'MergeUpdate.brt >' => 0,
																					'MergeUpdate.manifest_date LIKE' => '2018-02-06%',
																					'MergeUpdate.scan_status' => 1,
																					'MergeUpdate.sorted_scanned' => 1 
																					) ) );*/
	 																			
		if(count($italyOrders) > 0)
		{
		$fp = fopen(WWW_ROOT."img/FNVAT00R.csv","w");
		$sep = ";";
		$header = "VATCCM".$sep."VATLNP".$sep."VATNRS".$sep."VATNSP".$sep."VATNOT_B".$sep."VATNOT_S\r\n";
		unlink(WWW_ROOT."img/FNVAT00R.csv");
		file_put_contents(WWW_ROOT."img/FNVAT00R.csv",$header);
		foreach( $italyOrders as $italyOrder )
			{
					$italyOrder['MergeUpdate']['order_id'];
					$orderdetails = $this->MergeUpdate->find('all', array('conditions'=>array('MergeUpdate.product_order_id_identify'=>$italyOrder['MergeUpdate']['product_order_id_identify'])));
					$italyopen	=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id'=>$italyOrder['MergeUpdate']['order_id'])));
				foreach($orderdetails as $orderdetail)
					{
						$brtdata	=	$this->BrtSerialNo->find('first', array( 'conditions' => array( 'BrtSerialNo.split_order_id'=>$orderdetail['MergeUpdate']['product_order_id_identify'])));
						
						if(!empty($brtdata))
						{
							$customer_info				=	unserialize( $italyopen['OpenOrder']['customer_info'] );
							$consignee_country			=	'';
							$number						=	str_replace('-','',str_replace('+','',filter_var($customer_info->Address->PhoneNumber, FILTER_SANITIZE_NUMBER_INT)));
							$concode					=	substr($number, 0, 2);
							
							if(strlen($number) < 13 && $concode != '39'){
								$number =  '+39'.$number;	
							} 
							 
							 $store =	$italyopen['OpenOrder']['sub_source'];
							 
							if($store == 'CostBreaker_IT' || $store == 'CostBreaker_DE' || $store == 'CostBreaker_FR' || $store == 'CostBreaker_ES'){
							 	$acnumber = '1664097';
							} else if($store == 'Rainbow_Retail_IT' || $store == 'Rainbow Retail' || $store == 'Rainbow_Retail_ES' || $store == 'RAINBOW RETAIL DE'){
								$acnumber = '1664098';
							} else if($store == 'Tech_Drive_ES' || $store == 'Tech_Drive_UK' || $store == 'Tech_Drive_DE' || $store == 'Tech_Drive_FR'){
								$acnumber = '1664099';
							} else {
								$acnumber = '1664070';
							}
							
							$split_order_id		= 	$orderdetail['MergeUpdate']['product_order_id_identify'];
							$serial_no			=	$brtdata['BrtSerialNo']['serial_no'];
							$parcel_no			=	$brtdata['BrtSerialNo']['parcel_no'];
							$sno	=	substr($parcel_no, -2);
							$content = $acnumber .$sep.'166'.$sep.'67'.$sep.$parcel_no.$sep.$number.$sep.$number."   SN\r\n";
							file_put_contents(WWW_ROOT."img/FNVAT00R.csv",$content, FILE_APPEND | LOCK_EX);
							$this->MergeUpdate->updateAll(array('MergeUpdate.brt_phone' => '2'), array('MergeUpdate.id' => $orderdetail['MergeUpdate']['id']));
						}
				
					 }
				}
			$folderName = 'Service Manifest -'. date("d.m.Y");
			$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
			$uploadUrl 		= 	WWW_ROOT .'img/FNVAT00R.csv';
			copy($uploadUrl,WWW_ROOT .'img/cut_off/'.$folderName.'/FNVAT00R_'.date('m-d-Y H:i:s').'.csv');  
				$status = 'ok';
			} else {
				$status = 'There is no data for FNVAT00R.';
			}
			return $status;		
			/*	
			header('Content-Description: File Transfer');
			header('Content-Type: application/force-download');
			header('Content-Disposition: attachment; filename='.basename($uploadUrl));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($uploadUrl));   
			readfile($uploadUrl);
			exit;*/
			
	}
	
	public function getDataForBrtPalletPrintPdf()
	{
		
		$this->layout  = '';
		$this->autoRender = false;
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('ItalyCode');
		$this->loadModel('BrtSerialNo');
		$italyOrders = $this->MergeUpdate->find('all', array('conditions' => array(
																					'MergeUpdate.status' => 1, 
																					'MergeUpdate.delevery_country'=>'Italy',
																					'MergeUpdate.brt_exprt' => 1,
																					'MergeUpdate.manifest_status' => 2,
																					'MergeUpdate.scan_status' => 1,
																					'MergeUpdate.sorted_scanned' => 1
																					) ) );
																					
																					
		/*$italyOrders = $this->MergeUpdate->find('all', array('conditions' => array(
																					'MergeUpdate.status' => 1, 
																					'MergeUpdate.delevery_country'=>'Italy', 
																					'MergeUpdate.brt >' => 0,
																					'MergeUpdate.manifest_date LIKE' => '2018-02-07%',
																					'MergeUpdate.scan_status' => 1,
																					'MergeUpdate.sorted_scanned' => 1 
																					) ) );	*/																
																			
		if(count($italyOrders) > 0)
		{
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper(array(0, 0, '794', '1122' ), 'portrait');
			
				$imgPath = WWW_ROOT .'css/';
				$html = '<style>table, th, td { border: 1px solid black; }</style><body>';
				$html .= '<div id="label">';
				$html .= '<div class="container">';
				$html .= '<div class="header row" style="width:100%; border:1px solid #000000; margin:20px 0 10px 0; padding:10px; text-align: center;"><strong style="font-size: 32px;" >Brt Export Sheet</strong><br><strong>ESL Limited</strong><br><strong>'.date('m-d-Y H:i:s').'</strong></div>';
				$html .= '<table class="header row" style="border:1px solid #000000; margin:0px 0 10px 0;" width=100%>';
				$html .='<tr style="text-align: left;"><th width="20%" >Our Ref:</th><th width="20%" >Consignee Name</th><th width="30%">Consignee Address</th><th width="15%">Consignee PostCode</th><th width="15%">Parcel No</th></tr>';
			  
				foreach($italyOrders as $italyOrder) {
				
					$or_id				=	$italyOrder['MergeUpdate']['order_id'];
					$spo_id				=	$italyOrder['MergeUpdate']['product_order_id_identify'];
					$italyopen			=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id'=>$or_id)));
					$brtdata			=	$this->BrtSerialNo->find('first', array( 'conditions' => array( 'BrtSerialNo.split_order_id'=>$spo_id)));
					$customer_info		=	unserialize( $italyopen['OpenOrder']['customer_info'] );
					$consignee_address 	= 	$customer_info->Address->Address1;
					$zip_code 			= 	str_pad($customer_info->Address->PostCode,5,"0",STR_PAD_LEFT);
					$name				=	$customer_info->Address->FullName;
					$parcel_no			=	'--';
					if( !empty( $brtdata ) )
						$parcel_no			=	$brtdata['BrtSerialNo']['parcel_no'];
						
					$html .='<tr><td width="20%">'.$spo_id.'</td><td width="20%">'.$name.'</td><td width="20%">'.$consignee_address.'</td><td width="20%">'.filter_var($zip_code, FILTER_SANITIZE_NUMBER_INT).'</td><td width="20%">'.$parcel_no.'</td></tr>';
					$this->MergeUpdate->updateAll(array('brt_exprt' => 2), array('product_order_id_identify' => $spo_id));
				}
				$html .='</table></div></body>';
				$html.= '<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
				
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				$dompdf->stream("ExportLabelBrt.pdf");
				
				$imgPath 	= 	WWW_ROOT .'img/'; 
				$name		=   'ExportLabelBrt.pdf';
				file_put_contents($imgPath.$name, $dompdf->output());
				
				$folderName = 'Service Manifest -'. date("d.m.Y");
				$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
				$uploadUrl 		= 	WWW_ROOT .'img/ExportLabelBrt.pdf';
				copy($uploadUrl,WWW_ROOT .'img/cut_off/'.$folderName.'/ExportLabelBrt_'.date('m-d-Y H:i:s').'.pdf');  
				$uploadUrl 		= 	WWW_ROOT .'img/ExportLabelBrt.pdf';		
				$status = 'ok';
			}else {
			 	$status = 'There is no data for ExportLabelBrt.';
			}
		return $status;
		
		/*header('Content-Description: File Transfer');
		header('Content-Type: application/force-download');
		header('Content-Disposition: attachment; filename='.basename($uploadUrl));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($uploadUrl));   
		readfile($uploadUrl);
		exit;*/
	}
	
	public function getcommaremovedata($string = null){
			
		$special_chars = array(';'=>' ','"'=>' ',"'"=>' ','\n'=>'',','=>' ');
		$str = strtr( $string,$special_chars );
		return  $str;
	}
	
	public function replaceFrenchChar($string = null){
			
		$unwanted_array = array('�'=>'S', '�'=>'s', '�'=>'Z', '�'=>'z', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E','�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', 'N�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U','�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c','�'=>'e', '�'=>'e', '�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n','n�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o','�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'b', '�'=>'y','�'=>'u','�'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}
	
	
}

?>
