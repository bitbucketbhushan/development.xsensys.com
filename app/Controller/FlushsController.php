<?php
error_reporting(0);
ini_set('memory_limit', '2048M');
class FlushsController extends AppController
{
    
    var $name = "Flushs";
    
    var $components = array('Session','Upload','Common','Paginator');
    
    var $helpers = array('Html','Form','Session','Common' , 'Soap' , 'Paginator');
    
    public function beforeFilter()
	{
	    parent::beforeFilter();
	    $this->layout = false;
	    $this->Auth->Allow(array('productVirtualStockCsvGeneration', 'prepareVirtualStock', 'getStockDataForCsv' ,'batchProcessed' , 'prepareExcel'));
	}
    
    public function skuLocation()
	{
		$this->layout = 'index';
		$this->loadModel( 'Brand' );
		
		$this->loadModel( 'PackageEnvelope' );
		$this->loadModel( 'Packagetype' );
		$packageType = $this->Packagetype->find('list', array('fields' => 'Packagetype.id, Packagetype.package_type_name'));
		
		$brandName	=	$this->Brand->find('list', array('fields' => 'brand_name, brand_name'));
		$this->set( 'brandName', $brandName);
		
		//Rack Section
		$this->loadModel( 'Rack' );
		
		//Ground
		$floorRacks = $this->Rack->find('list' , array( 'fields' => array( 'Rack.floor_name' ) , 'order' => array( 'Rack.id ASC' ) , 'group' => array( 'Rack.floor_name' ) ) );
		
		//Racks
		$rack = $rackRacks = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , array( 'fields' => array( 'Rack.id' , 'Rack.rack_floorName' ) , 'conditions' => array( 'Rack.floor_name' => $floorRacks ) , 'group' => array( 'Rack.rack_floorName' ) , 'order' => array( 'Rack.id ASC' ) ) );		
		
		//Level
		$rackLevel = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , 
				array( 
					'fields' => array( 'Rack.id' , 'Rack.level_association' ) , 
					'conditions' => array( 'Rack.floor_name' => $floorRacks , 'Rack.rack_floorName' => $rack ) , 'group' => array( 'Rack.level_association' ) , 'order' => array( 'Rack.id ASC' ) 
					) 
				);
		
		//Section
		$rackSection = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , 
				array( 
					'fields' => array( 'Rack.id' , 'Rack.rack_level_section' ) , 
					'conditions' => array( 'Rack.floor_name' => $floorRacks , 'Rack.rack_floorName' => $rack , 'Rack.level_association' => $rackLevel ) , 'order' => array( 'Rack.id ASC' ) 
					) 
				);
				
		$this->set( 'setNewGroundArray' , $floorRacks );
		$this->set( 'rack' , $rack );
		$this->set( 'rackLevel' , $rackLevel );
		$this->set( 'rackSection' , $rackSection );
		$this->set( 'packageType', $packageType);
	}
    
    public function flushLocation()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel( 'BinLocation' );	
		$this->loadModel( 'CheckIn' );	
		
		/*pr($this->request->data);
		pr($this->BinLocation->find('all')); exit;*/
		
		$barcode = $this->request->data['barcode'];
		
		
		pr($this->request->data); exit;
		
		
		//Delete Specific barcode rows
		//$this->BinLocation->query( "delete from bin_locations where barcode = '{$barcode}'" );
		
              
		$product_id = $this->request->data['product_id'];
		$descId = $this->request->data['id'];
		$length = $this->request->data['getLength'];
		$width = $this->request->data['getWidth'];
		$height = $this->request->data['getHeight'];
		$weight = $this->request->data['getWeight'];
		$variant_envelope_name = $this->request->data['variant_envelope_name'];
		$variant_envelope_id = $this->request->data['variant_envelope_id'];
		$brand = $this->request->data['brand'];
		$bin = $this->request->data['bin_sku'];
		$binAllocation = $this->request->data['binAllocation'];
		$pageName = $this->request->data['pageName'];
		$currentStock = $this->request->data['currentStock'];
		$userId = $this->request->data['userId'];
		$pcName = $this->request->data['pcName'];
		
		$getStockSum = array_sum( explode(",",$this->request->data['getStockByLocation']) );
		
		//Unbinding Techniques 
		$this->ProductDesc->unbindModel( array( 'belongsTo' => array( 'Product' ) ) );
				
		$this->ProductDesc->updateAll(
			array(
				'length' => $length,
				'width' => $width,
				'height' => $height,
				'weight' => $weight,
				'variant_envelope_name' => "'".$variant_envelope_name."'",
				'variant_envelope_id' => $variant_envelope_id,
				'brand' => "'".$brand."'",
				'bin' => "'".$bin."'"
			),
			array(
				'ProductDesc.id' => $descId
			)
		);
		
		//Save only
		$getLocation = explode( ',',$this->request->data['getLocation'] );
		$getStock = explode( ',',$this->request->data['getStockByLocation'] );
		
		if( count($getLocation) > 0 )
		{
                    
			$stockCount = 0;$e = 0;while( $e < count( $getLocation ) )
			{
				if(  $getLocation[$e] != '' && $getStock[$e] != '' )
				{
                                    
					$this->request->data['BinLocationPreffered']['BinLocation']['barcode'] = $this->request->data['barcode'];
					$this->request->data['BinLocationPreffered']['BinLocation']['bin_location'] = $getLocation[$e];
					$this->request->data['BinLocationPreffered']['BinLocation']['stock_by_location'] = $getStock[$e];
					$stockCount = $stockCount + $getStock[$e];
                                        
					$this->BinLocation->saveAll( $this->request->data['BinLocationPreffered'] );
				}
			$e++;	
			}
		}
		
	   /*$this->Product->updateAll(
				array(
						'current_stock_level' => "'".$stockCount."'"
				),
				array(
						'Product.id' => $product_id
				)
		);*/
		
		$this->Product->query( "UPDATE products set current_stock_level = '{$stockCount}' where id = {$product_id}" );
                
		if( $stockCount > 0 ) 
		{
			//Update Product current stock
			/*$this->Product->updateAll(
				array(
					'current_stock_level' => "'".$stockCount."'"
				),
				array(
					'Product.id' => $product_id
				)
			);*/
                    
                    //$this->Product->query( "UPDATE products set current_stock_level = '{$stockCount}' where id = {$product_id}" );
		}
		
		/*
		 * 
		 * CheckIn record manipulation
		 * 
		 */ 
		$checkIn['sku']				= $bin;
		$checkIn['barcode']			= $barcode;
		$checkIn['qty_checkIn']		= $getStockSum;
		$checkIn['custam_page_name']= $pageName;
		$checkIn['current_stock']	= $currentStock;
		$checkIn['user_id']			= $userId;
		$checkIn['pc_name']			= $pcName;
		
		$this->CheckIn->saveAll( $checkIn );
		unset($checkIn);
		$checkIn = '';
		
		//Get All related data
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		$this->loadModel('BinLocation');
		
		/*$this->ProductDesc->bindModel( array( 
			'hasMany' => array(        
				'BinLocation' => array(									   
					'className' => 'BinLocation',
					'foreignKey' => 'barcode'									   
					)
				) 
			) 
		);*/
		
		$barcode	=	$this->request->data['barcode'];
		
		$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array('ProductDesc.barcode' => $barcode)
								)
							);
		
		$this->ProductDesc->unbindModel( array(
			'hasOne' => array(
				'Product'
			)
		) );
		
		/*$binLocation =	$this->BinLocation->find('all', array(
									'joins' => array(
										array(
											'table' => 'product_descs',
											'alias' => 'ProductDesc',
											'type' => 'INNER',
											'conditions' => array(
												'ProductDesc.barcode = BinLocation.barcode'
											)
										)
									)
								)
							);*/
		$binLocation =	$this->BinLocation->find('all', array( 'conditions' => array(
                                                                        'BinLocation.barcode' => $barcode
                                                                    )
                                                                    
								)
							);
                
		$data['id'] 				= 	$productdeatil['ProductDesc']['id'];
		$data['stock'] 				= 	$productdeatil['Product']['current_stock_level'];
		$data['product_id'] 				= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 				= 	$productdeatil['Product']['product_name'];
		$data['sku'] 				= 	$productdeatil['Product']['product_sku'];
		$data['shortdescription'] 	= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 	= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 				= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 			= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 				= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 			= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 			= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 			= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		$data['bin_combined_text'] 			= 	$productdeatil['ProductDesc']['bin_combined_text'];
		
		//Get and set Bin Locations
		$binLocate = 0;
		foreach($binLocation as $binLocationIndex => $binLocationValue)
		{
			$binData[$binLocate]['id']					=	$binLocationValue['BinLocation']['id'];	
			$binData[$binLocate]['barcode']				=	$binLocationValue['BinLocation']['barcode'];	
			$binData[$binLocate]['bin_location']		=	$binLocationValue['BinLocation']['bin_location'];	
			$binData[$binLocate]['stock_by_location']	=	$binLocationValue['BinLocation']['stock_by_location'];	
		$binLocate++;
		}
		$data['binLocationArray'] = $binData;
		
		$imageUrl	=	Router::url('/', true).'img/product/';
		
		$i = 0;
		foreach($productdeatil['ProductImage'] as $image)
		{
			if($image['image_name'] != '')
			{
				$imagedata[$i]['imageid']		=	$image['id'];
				$imagedata[$i]['imagename']		=	$imageUrl.$image['image_name'];
				$imagedata[$i]['selectedimage']	=	$image['selected_image'];
			}
			$i++;
		}
		
		$j = 0;
		foreach( $productdeatil['ProductLocation'] as $productLocation)
		{
			$productLocation['warehouse_id'];
			$locationData[$j]['binrackid']	=	$productLocation['bin_rack_address'];
			$warehouseDetail	=	$this->Warehouse->find('first', array('conditions' => array('Warehouse.id' => $productLocation['warehouse_id'])));
			$locationData[$j]['warehousename']	=	$warehouseDetail['Warehouse']['warehouse_name'];
			$locationData[$j]['city']			=	$warehouseDetail['WarehouseDesc']['city_id'];
			$locationData[$j]['warehousetype']	=	$warehouseDetail['WarehouseDesc']['warehouse_type'];
			$j++;
		}
		$data['status'] 				= 'success';	
                
		echo (json_encode(array('status' => '1', 'data' => $data)));		
		exit;	 
	}
    	
}

?>
