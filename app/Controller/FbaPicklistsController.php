<?php
error_reporting(1);
class FbaPicklistsController extends AppController
{    
    var $name = "FbaPicklists";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow();			
    }
 	
   public function index() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Shipments" );		
	}
	 
	public function createPickList( $shipment_inc_id = null )
	{
		
		$this->layout = '';
		$this->autoRander = false;		
		
		/* start for pick list */
		
	 	$this->loadModel('FbaShipmentItemsPo');					 
		$this->loadModel('FbaBundleSku'); 	
		$this->loadModel('FbaItemLocation'); 
		$this->loadModel('Product'); 
 		
		$shipment_po_items = $this->FbaShipmentItemsPo->find('all', array('conditions'=>array('FbaShipmentItemsPo.shipment_inc_id'=>$shipment_inc_id))); 
	
		if(count($shipment_po_items) > 0){
		
			$products = array();
			$_product	= $this->Product->find('all',array('fields'=>array('product_sku','product_name','ProductDesc.barcode'),'order'=>'Product.id DESC'));
				 
			foreach( $_product as $pro ){
				$products[$pro['Product']['product_sku']] = array('product_name'=>$pro['Product']['product_name'],'barcode'=>$pro['ProductDesc']['barcode']);
			}	 
		 
			//$shipment_id   = $shipment_po_items['FbaShipmentsPo']['shipment_id'];
			//$ship_table_id = $shipment_po_items['FbaShipmentsPo']['id']; 
			
			foreach($shipment_po_items as $v){	
				$val = $v['FbaShipmentItemsPo'];
				$shipment_id = $val['shipment_id'];
				$qty = 0; 
				 
				if(substr($val['master_sku'], 0, 2) == 'B-'){
					$exp  = explode('-',$val['master_sku']);
					$count  = count($exp);
					$qindex = $count  - 1; 
					$bunq   = $exp[$qindex];
					
					if($count == 3 ){
						$item_qty = $val['shipped_qty'] * $bunq;
					}else if($count > 3 ){
						$item_qty = $val['shipped_qty'];
					}					
					
					for($i = 1; $i < $qindex; $i++){
						
						$poItemData = array();
						
						$msku 							= 'S-'.$exp[$i];						
						$poItemData['sku']				= $msku;						
						$poItemData['quantity']  		= $item_qty;
						$poItemData['fnsku']    		= $val['fnsku'];				
						$poItemData['shipment_id']  	= $shipment_id;
						$poItemData['shipment_inc_id']  = $val['shipment_inc_id'];						
						$poItemData['merchant_sku']  	= $val['merchant_sku'];
						$poItemData['sku_type']  		= '2';
						
						if(isset($products[$msku])){
							$poItemData['barcode'] = $products[$msku]['barcode'];//Global barcode	
						}else{
							$poItemData['barcode']  =  ''; 	
						} 
						 						 
						$this->manageQuantity($poItemData);
					}
				
				}else{										
						$poItemData = array();
						
						if(isset($products[$val['master_sku']])){
							$poItemData['barcode'] =  $products[$val['master_sku']]['barcode'];	//Global barcode						
						}else{
							$poItemData['barcode'] = '';
						}
						$poItemData['sku']				= $val['master_sku'];
						$poItemData['quantity']  		= $val['shipped_qty'];
						$poItemData['fnsku']    		= $val['fnsku'];				
						$poItemData['shipment_id']  	= $shipment_id;
						$poItemData['shipment_inc_id'] 	= $val['shipment_inc_id'];
						$poItemData['merchant_sku']  	= $val['merchant_sku'];
						$poItemData['sku_type']  		= '1';
						$this->manageQuantity($poItemData);
				}
			}			
			//pr($poItemData);
			//exit;
			 
			 $pickListItems = $this->FbaItemLocation->find('all', array('conditions'=>array('FbaItemLocation.shipment_id' => $shipment_id ) ) ); 
			
			if( count( $pickListItems ) > 0 )
			{ 
				//manage userd - id with name
				$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
				$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
				$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
				
				$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
				
				$data = array(); $tq = 0; $taq = 0;
				$itemArray = array(); 
				foreach( $pickListItems as $val )
				{
					$key = $val['FbaItemLocation']['fnsku'].'_'.$val['FbaItemLocation']['sku'];
					if(in_array($key, $itemArray)){
						$quantity = 0;							
					}else{
						$itemArray[$key] = $key ;
						$quantity = $val['FbaItemLocation']['quantity'];
					} 
					
					$tq += $quantity;	
					$taq += $val['FbaItemLocation']['available_qty_bin'];
					$fnskuArray[] = $val['FbaItemLocation']['fnsku'];
					$data[] = array('bin_location' => $val['FbaItemLocation']['bin_location'],
																		'sku'=>trim($val['FbaItemLocation']['sku']),
																		'barcode' => $val['FbaItemLocation']['barcode'],
																		'quantity' => $quantity,
																		'available_qty_bin' => $val['FbaItemLocation']['available_qty_bin']);
				}
				 							
				/* dome pdf vendor */
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				/* Html for print pick list */
				$date = date("m/d/Y H:i:s");	
				$finArray = array();
				$html = '<h3>PickList :- '.$date.'</h3><hr><h3>Total Item  : - '.$tq.'</h3>
						<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
						<tr>
						<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
						<th style="border:1px solid;" width="10%" align="center">SKU</th>
						<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
						<th style="border:1px solid;" width="10%" align="center">Barcode</th>
						</tr>';
				
				$data = $this->msortnew($data, array('bin_location'));
				$tdata = array();
				foreach($data as $a){
					$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
											
				}			
				
				foreach($tdata as $sku => $d){
						$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
						foreach($d as $a){
							$bn[$a['bin_location']][] = $a['quantity'];	
							$bna[$a['bin_location']][] = $a['available_qty_bin'];	
							//$bar[$a['barcode']] = $a['barcode'];
							$local_barcode = $this->Components->load('Common')->getLocalBarcode($a['barcode']);
							
							if($local_barcode != ''){
								$bar[$local_barcode] = $local_barcode; 
							}else{
								$bar[$a['barcode']] = $a['barcode'];
							}
						}
						
						$bins = array_keys($bn);  $oversell = array();
						foreach($bins as $b){
							if($b !=''){
								if(array_sum($bn[$b]) == array_sum($bna[$b])){
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								}else{
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
									$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
								}
							}
							$qty[]= array_sum($bn[$b]);
							$avaqty[]= array_sum($bna[$b]);
							
						}	
						
						$oversell_str = '';
						if(count($oversell) > 0 && array_sum($oversell) > 0){
							$oversell_str =	'<br>Shortage of quantity('.array_sum($oversell).')';
						}		
						$product_name = '';		
						if(isset($products[$sku])){
							$product_name =  substr($products[$sku]['product_name'], 0, 40 ) ;
						}	
						$html.=	'<tr>
							<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
							<td style="border:1px solid;">'.$sku.'</td>
							<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$product_name.'</td>
							<td style="border:1px solid;">'.implode(",",$bar).'</td>							
							</tr>';
							
				}	
			
				$html .= '</table>';			 
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				
				date_default_timezone_set('Europe/Jersey');

				$imgPath = WWW_ROOT .'fba_shipments/picklists/'; 
				$name	 = 'Fba_Pick_List__'.date("d_m_Y_H_i").'.pdf';				
				file_put_contents($imgPath.$name, $dompdf->output());
				
 				$folderPath = WWW_ROOT .'logs'.DS.'FbaPicklist_'.date('dmy_Hi').'.log'; 
				file_put_contents($folderPath, $username."\n".implode( "," , $fnskuArray ));
			
				$this->FbaShipmentItemsPo->updateAll( array('FbaShipmentItemsPo.picklist' =>  "'".$name."'"),array('FbaShipmentItemsPo.shipment_inc_id'=>$shipment_inc_id));				
				 
				
				$this->Session->setflash(  "Picklist generated successfully.",  'flash_success' );
			}else{
				$this->Session->setflash(  "Items not found.",  'flash_danger' );
			} 
		}
		else{
			$this->Session->setflash(  "Please complete shipment.",  'flash_danger' );
		}
		
		$this->redirect( $this->referer());	
	}
	public function deletePicklist($shipment_inc_id = 0){
		
		$this->loadModel('FbaItemLocation');
		$this->loadModel('BinLocation');		
		$this->loadModel('CheckIn');
		$this->loadModel('Product');
		$this->loadModel('InventoryRecord');	
 		$items = $this->FbaItemLocation->find('all', array('conditions' => array('shipment_inc_id' => $shipment_inc_id) ) );
		
		if(count($items) > 0){
 			foreach($items as $itm){
				 
				$check_in 	 = $itm['FbaItemLocation']['available_qty_check_in'];	
				$qty_bin 	 = $itm['FbaItemLocation']['available_qty_bin'];	
				$po_id     	 = $itm['FbaItemLocation']['po_id'];				
				$sku     	 = $itm['FbaItemLocation']['sku'];
				$fnsku    	 = $itm['FbaItemLocation']['fnsku'];
				$barcode  	 = $itm['FbaItemLocation']['barcode'];
				$shipment_id = $itm['FbaItemLocation']['shipment_id']; 
				$shipment_inc_id = $itm['FbaItemLocation']['shipment_inc_id']; 
				
				file_put_contents(WWW_ROOT.'logs/deletePicklist_'.date('Ymd').'.log', $check_in."\t". $qty_bin."\t". $po_id."\t". $sku."\t". $fnsku."\t". $barcode."\t". $shipment_id."\t". $shipment_inc_id."\t". $this->Session->read('Auth.User.username')."\t". date('Y-m-d H:i:s'), FILE_APPEND|LOCK_EX);
				
				$poDetail = $this->CheckIn->find( 'first', array( 'conditions' => array( 'CheckIn.id' => $po_id)) );
				if(count($poDetail) > 0){
					$this->CheckIn->updateAll( array('CheckIn.selling_qty' =>  "CheckIn.selling_qty - $check_in"),array('CheckIn.id' => $po_id));
				}			
				
				$getStock = $this->BinLocation->find( 'first', array( 'conditions' => array( 'BinLocation.barcode' => $barcode),'order' => 'priority ASC' ) );
				$bin_name = '';	
				if($getStock > 0){		
					$this->BinLocation->updateAll( array('BinLocation.stock_by_location' =>  "BinLocation.stock_by_location + $qty_bin"),array('BinLocation.id' => $getStock['BinLocation']['id']));
					$bin_name = $getStock['BinLocation']['stock_by_location'];
				}
							
				$current_qty = 0; $current_stock = 0;
				$_product = $this->Product->find('first',array('conditions'=>array('Product.product_sku' => $sku), 'fields' => array('current_stock_level'),'order'=>'Product.id DESC'));
				
				if(count($_product) > 0){
					$current_stock = $_product['Product']['current_stock_level'];														
					$current_qty   = $_product['Product']['current_stock_level'] + $qty_bin;
					$this->Product->updateAll( array('Product.current_stock_level' => $current_qty),array('Product.product_sku' => $sku));
				}
				
				$inv_data = array();
				$inv_data['action_type']  	 	= 'FBA_Delete_Picklist';
				$inv_data['sku'] 		  	 	= $sku;					
				$inv_data['barcode']	  	 	= $barcode;
				$inv_data['currentStock'] 		= $current_stock;
				$inv_data['quantity'] 	   		= $qty_bin;
				$inv_data['status'] 	   		= 'fba';
				$inv_data['after_maniplation'] 	= $current_qty;
				$inv_data['location'] 			= $bin_name;
				$inv_data['split_order_id']		= $shipment_id.'_'.$fnsku;
				$inv_data['date'] 		   		= date('Y-m-d' );
				$this->InventoryRecord->saveAll( $inv_data );
							 			
			}
			
			$sql = "DELETE FROM `fba_item_locations` WHERE `shipment_inc_id` = $shipment_inc_id";
			$this->FbaItemLocation->query($sql);
			$sql = "DELETE FROM `fba_shipment_items` WHERE `shipment_inc_id` = $shipment_inc_id";
			$this->FbaItemLocation->query($sql);
			$sql = "DELETE FROM `fba_shipments` WHERE `id` = $shipment_inc_id";
			$this->FbaItemLocation->query($sql);
			
			$sql = "DELETE FROM `fba_shipment_boxes` WHERE `shipment_id` = '".$shipment_id."'";
			$this->FbaItemLocation->query($sql);
			$sql ="DELETE FROM `fba_shipment_box_items` WHERE `shipment_id` = '".$shipment_id."'";
			$this->FbaItemLocation->query($sql);
			$sql = "DELETE FROM `fba_shipment_items_pos` WHERE `shipment_inc_id` = $shipment_inc_id";
			$this->FbaItemLocation->query($sql);

 		}					
	}
	
	public function manageQuantity($data = array()){
		
		$this->loadModel('FbaItemLocation');
		$this->loadModel('BinLocation');		
		$this->loadModel('CheckIn');
		$this->loadModel('InventoryRecord');	
		
		if(count($data) > 0){
			 
			$quantity 	 = $data['quantity'];					
			$sku     	 = $data['sku'];
			$fnsku    	 = $data['fnsku'];
			$barcode  	 = $data['barcode'];
			$shipment_id = $data['shipment_id']; 
			$shipment_inc_id = $data['shipment_inc_id']; 
			
			
			$getItem = $this->FbaItemLocation->find('first', array('conditions' => array('shipment_id' => $shipment_id, 'fnsku' => $fnsku, 'sku' => $sku) ) );
 			if(count($getItem) == 0){
						
				$po_name = array(); $posIds = array(); $pos = array(); $binname = array(); 
				$available_qty = 0; $available_qty_bin = 0;	
				
				$poDetail = $this->CheckIn->find( 'all', 
					array( 'conditions' => array( 'CheckIn.barcode' => $barcode, 'CheckIn.selling_qty != CheckIn.qty_checkIn','po_name !=""'),
							'order' => 'CheckIn.date  ASC ' ) );
				
				if(count($poDetail) > 0){
					$qt_count = 0; 				
					foreach($poDetail as $po){
						$poQty = $po['CheckIn']['qty_checkIn'] - $po['CheckIn']['selling_qty'];
						if($poQty > 0){						
							 for($i=0; $i <= $quantity ;$i++){						 
								 if($qt_count >= $quantity ){
									 break;
								 }else if(($poQty - $i)  > 0){							
									$po_name[$po['CheckIn']['id']][] = $po['CheckIn']['po_name'];	
									$available_qty++;							
									$qt_count++;
								}												
							 }						
						}
					}
				}
				 
				
				$getStock = $this->BinLocation->find( 'all', array( 'conditions' => array( 'BinLocation.barcode' => $barcode),'order' => 'priority ASC' ) );
				//pr($getStock );
				$bin_name = array();
				$finalData['bin_location'] = '';
				if(count($getStock) > 0){
					$qt_count = 0 ;
					foreach($getStock as $val){
						 $binData['id'] = $val['BinLocation']['id'];					
						 for($i=0; $i <= $quantity ;$i++){						 
							 if($qt_count >= $quantity ){
								 break;
							 }else if(($val['BinLocation']['stock_by_location'] - $i)  > 0){							
								$bin_name[$val['BinLocation']['id']][] = $val['BinLocation']['bin_location'];	
								$available_qty_bin++;						
								$qt_count++;
							}												
						 }							
					}				
				}							
							
				
				if(count($po_name) > 0){
					foreach(array_keys($po_name) as $k){
						$qts   = count($po_name[$k]);	
						$pos[] = $po_name[$k][0];
						$posIds[] = $k;
						if($shipment_id != 'TEST_SHIPMENT'){
					 		$this->CheckIn->updateAll( array('CheckIn.selling_qty' =>  "CheckIn.selling_qty + $qts"),array('CheckIn.id' => $k));
						}
					}
				}
								
				if(count($bin_name) > 0)
				{
					foreach(array_keys($bin_name) as $k){					
						$qts = count($bin_name[$k]);	
						$binname[] = $bin_name[$k][0];	
						$finalData = array();						 
						$finalData['available_qty_check_in'] = $available_qty;
						$finalData['available_qty_bin'] = $qts;
						$finalData['quantity']      	= $quantity;
						$finalData['shipment_id']		= $shipment_id;	
						$finalData['shipment_inc_id']	= $shipment_inc_id;	
						$finalData['merchant_sku']  	= $data['merchant_sku'];
						$finalData['sku_type']			= $data['sku_type']; 				
						$finalData['sku']	   			= $sku;	
						$finalData['fnsku']	   			= $fnsku;								
						$finalData['barcode']  			= $barcode;	
						$finalData['po_name']  			= implode(",",$pos);	
						$finalData['po_id']    			= implode(",",$posIds);	
						$finalData['bin_location']  	= $bin_name[$k][0];		
						$finalData['username'] 			= $this->Session->read('Auth.User.username').'_'.$this->Session->read('Auth.User.pc_name') ;
						$finalData['added_date'] 		= date('Y-m-d H:i:s');		
						$this->FbaItemLocation->saveAll( $finalData );
					
						if($shipment_id != 'TEST_SHIPMENT'){
							$this->BinLocation->updateAll( array('BinLocation.stock_by_location' =>  "BinLocation.stock_by_location - $qts"),array('BinLocation.id' => $k));
						 }
						/**
						Updating current stock of product.
						*/
						$current_qty = 0;
						$_product = $this->Product->find('first',array('conditions'=>array('Product.product_sku' => $sku), 'fields' => array('current_stock_level'),'order'=>'Product.id DESC'));
						
						if(count($_product) > 0){
						
							$current_stock = $_product['Product']['current_stock_level'];														
							$current_qty   = $_product['Product']['current_stock_level'] - $qts;
							if($current_qty < 0){
								$current_qty = 0;
							}
							
							if($shipment_id != 'TEST_SHIPMENT'){
								$this->Product->updateAll( array('Product.current_stock_level' => $current_qty),array('Product.product_sku' => $sku));
							}
							
							$inv_data = array();
							$inv_data['action_type']  	 	= 'FBA_Inventory_Deduction';
							$inv_data['sku'] 		  	 	= $sku;					
							$inv_data['barcode']	  	 	= $barcode;
							$inv_data['currentStock'] 		= $current_stock;
							$inv_data['quantity'] 	   		= $qts;
							$inv_data['status'] 	   		= 'fba';
							$inv_data['after_maniplation'] 	= $current_qty;
							$inv_data['location'] 			= $bin_name[$k][0];
							$inv_data['split_order_id']		= $shipment_id.'_'.$fnsku;
							$inv_data['date'] 		   		= date('Y-m-d' );
				 			if($shipment_id != 'TEST_SHIPMENT'){
								$this->InventoryRecord->saveAll( $inv_data );
							}
						
						}
						// end of stock updation.						
			
					}
				} 
				else {
						$finalData = array();
						$finalData['available_qty_check_in'] = $available_qty;
						$finalData['available_qty_bin'] = '0';
						$finalData['quantity']      	= $quantity;
						$finalData['shipment_id']		= $shipment_id;	
						$finalData['shipment_inc_id']	= $shipment_inc_id;	
						$finalData['merchant_sku']  	= $data['merchant_sku'];
						$finalData['sku_type']			= $data['sku_type']; 						
						$finalData['fnsku']	   			= $fnsku;	
						$finalData['sku']	   			= $sku;								
						$finalData['barcode']  			= $barcode;	
						$finalData['po_name']  			= implode(",",$pos);	
						$finalData['po_id']    			= implode(",",$posIds);	
						$finalData['bin_location']  	= 'No_Location';			
						$finalData['username'] 			= $this->Session->read('Auth.User.username').'_'.$this->Session->read('Auth.User.pc_name') ;
						$finalData['added_date'] 		= date('Y-m-d H:i:s');			
						$this->FbaItemLocation->saveAll( $finalData );
				}
				
			}	
		}					
	}
		
 	public function customQtyManage($shipment_id = null) 
    {  
		die('code is commented');
		$this->loadModel('FbaItemLocation');
		$this->loadModel('BinLocation');		
		$this->loadModel('CheckIn');
		$this->loadModel('Product');
		$this->loadModel('InventoryRecord'); 
		
		$pickListItems = $this->FbaItemLocation->find('all', array('conditions'=>array('FbaItemLocation.shipment_id' => $shipment_id ) ) ); 
		//pr($pickListItems );
		
		 foreach($pickListItems as $val){
			
			$quantity 		 = $val['FbaItemLocation']['quantity'];					
			$sku     		 = $val['FbaItemLocation']['sku'];
			$fnsku    		 = $val['FbaItemLocation']['fnsku'];
			$barcode  	 	 = $val['FbaItemLocation']['barcode'];
			 
			$shipment_id 	 = $val['FbaItemLocation']['shipment_id']; 
			$shipment_inc_id = $val['FbaItemLocation']['shipment_inc_id']; 
						
			$po_name = array(); $posIds = array(); $pos = array(); $binname = array(); 
			$available_qty = 0; $available_qty_bin = 0;	
			
			$poDetail = $this->CheckIn->find( 'all', 
				array( 'conditions' => array( 'CheckIn.barcode' => $barcode, 'CheckIn.selling_qty != CheckIn.qty_checkIn','po_name !=""'),
						'order' => 'CheckIn.date  ASC ' ) );
			
			if(count($poDetail) > 0){
				$qt_count = 0; 				
				foreach($poDetail as $po){
					$poQty = $po['CheckIn']['qty_checkIn'] - $po['CheckIn']['selling_qty'];
					if($poQty > 0){						
						 for($i=0; $i <= $quantity ;$i++){						 
							 if($qt_count >= $quantity ){
								 break;
							 }else if(($poQty - $i)  > 0){							
								$po_name[$po['CheckIn']['id']][] = $po['CheckIn']['po_name'];	
								$available_qty++;							
								$qt_count++;
							}												
						 }						
					}
				}
			}
			 
			
			$getStock = $this->BinLocation->find( 'all', array( 'conditions' => array( 'BinLocation.barcode' => $barcode),'order' => 'priority ASC' ) );
			//pr($getStock );
			$bin_name = array();
			$finalData['bin_location'] = '';
			if(count($getStock) > 0){
				$qt_count = 0 ;
				foreach($getStock as $val){
					 $binData['id'] = $val['BinLocation']['id'];					
					 for($i=0; $i <= $quantity ;$i++){						 
						 if($qt_count >= $quantity ){
							 break;
						 }else if(($val['BinLocation']['stock_by_location'] - $i)  > 0){							
							$bin_name[$val['BinLocation']['id']][] = $val['BinLocation']['bin_location'];	
							$available_qty_bin++;						
							$qt_count++;
						}												
					 }							
				}				
			}							
						
			
			if(count($po_name) > 0){
				foreach(array_keys($po_name) as $k){
					$qts   = count($po_name[$k]);	
					$pos[] = $po_name[$k][0];
					$posIds[] = $k;
					//$this->CheckIn->updateAll( array('CheckIn.selling_qty' =>  "CheckIn.selling_qty + $qts"),array('CheckIn.id' => $k));
				}
			}
			 		
			if(count($bin_name) > 0)
			{
				foreach(array_keys($bin_name) as $k){					
					$qts = count($bin_name[$k]);	
					$binname[] = $bin_name[$k][0];	
					$finalData = array();						 
					
					//$this->BinLocation->updateAll( array('BinLocation.stock_by_location' =>  "BinLocation.stock_by_location - $qts"),array('BinLocation.id' => $k));
					/**
					Updating current stock of product.
					*/
					$current_qty = 0;
					$_product = $this->Product->find('first',array('conditions'=>array('Product.product_sku' => $sku), 'fields' => array('current_stock_level'),'order'=>'Product.id DESC'));
					
					if(count($_product) > 0){
					
						$current_stock = $_product['Product']['current_stock_level'];														
						$current_qty   = $_product['Product']['current_stock_level'] - $qts;
						if($current_qty < 0){
							$current_qty = 0;
						}
						
						//$this->Product->updateAll( array('Product.current_stock_level' => $current_qty),array('Product.product_sku' => $sku));
						
						$inv_data = array();
						$inv_data['action_type']  	 	= 'FBA_Inventory_Deduction';
						$inv_data['sku'] 		  	 	= $sku;					
						$inv_data['barcode']	  	 	= $barcode;
						$inv_data['currentStock'] 		= $current_stock;
						$inv_data['quantity'] 	   		= $qts;
						$inv_data['status'] 	   		= 'fba';
						$inv_data['after_maniplation'] 	= $current_qty;
						$inv_data['location'] 			= $bin_name[$k][0];
						$inv_data['split_order_id']		= $shipment_id.'_'.$fnsku;
						$inv_data['date'] 		   		= date('Y-m-d' );
						 pr($inv_data);
						//$this->InventoryRecord->saveAll( $inv_data );
					
					}
					// end of stock updation.						
		
				}
			}  
		}	
	}
	
	public function downlodPicklist($file_name = null) 
    {  
			$file_path =  WWW_ROOT.'fba_shipments/picklists/'.$file_name; 
			if(file_exists($file_path)){
				header('Content-Description: File Transfer');
				header('Content-Type: application/pdf');
				header('Content-Disposition: attachment; filename='.basename($file_path));
				header('Content-Transfer-Encoding: binary');
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				header('Content-Length: ' . filesize($file_path));   
				readfile($file_path); 
				exit;
			}else{
				$this->Session->setflash(  "Picklist file is not found.",  'flash_danger' );
				$this->redirect( $this->referer());	
			}
	}
	
	public function msortnew($array, $key, $sort_flags = SORT_REGULAR) {
   	 if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    // @TODO This should be fixed, now it will be sorted as string
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
            asort($mapping, $sort_flags);
			natsort($mapping);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
	}
} 