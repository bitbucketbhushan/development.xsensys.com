<?php
error_reporting(1);
class CustomprintsController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Customprints";
    
    var $components = array('Session','Upload','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
    var $virtualFields;
    
    public function index()
    {
		$this->layout = 'index';
		$this->autoRender = false;
		
		$this->render( '/Products/custom_label' );
	}
    
   
		
    public function getOpenOrderById( $numOrderId = null )
	{
		
		$this->loadModel('OpenOrder');
		$order = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $numOrderId )));
		$data['id']					=	 $order['OpenOrder']['id'];
		$data['order_id']			=	 $order['OpenOrder']['order_id'];
		$data['num_order_id']		=	 $order['OpenOrder']['num_order_id'];
		$data['sub_source']			=	 $order['OpenOrder']['sub_source'];
		$data['general_info']		=	 unserialize($order['OpenOrder']['general_info']);
		$data['shipping_info']		=	 unserialize($order['OpenOrder']['shipping_info']);
		$data['customer_info']		=	 unserialize($order['OpenOrder']['customer_info']);
		$data['totals_info']		=	 unserialize($order['OpenOrder']['totals_info']);
		$data['folder_name']		=	 unserialize($order['OpenOrder']['folder_name']);
		$data['items']				=	 unserialize($order['OpenOrder']['items']);
		$data['assigned_service']	=	 $order['AssignService']['assigned_service'];
		$data['assign_barcode']		=	 $order['AssignService']['assign_barcode'];
		$data['manifest']			=	 $order['AssignService']['manifest'];
		$data['cn22']				=	 $order['AssignService']['cn22'];
		$data['courier']			=	 $order['AssignService']['courier'];
		$data['template_id']		=	 $order['OpenOrder']['template_id'];
		return $data;
		
	}

	
	
	
	public function customLabelPrint( $splitOrderId = null )
			{
				
				$this->layout = '';
				$this->autoRender = false;
				$this->loadModel('Template');
				$this->loadModel('MergeUpdate');
				$this->loadModel('Product');
				
				$splitOrderId =  trim($this->request->data['orderid']);
				$orderArray = explode( '-', $splitOrderId );
				$id				=	$orderArray[0];
				$openOrderId	=	$orderArray[0];
				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
							
				$order				=		$this->getOpenOrderById( $openOrderId );
				$postalservices		=		$order['shipping_info']->PostalServiceName;
				$destination		=		$order['customer_info']->Address->Country;
				$total				=		$order['totals_info']->TotalCharge;
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$currency			=	 	$order['totals_info']->Currency;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$templateId			=	 	$order['template_id'];
				$subSource			=	 	$order['sub_source'];
				
				$str = ''; 
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				$totlaWeight	=	0;
				$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
				$weight = 0;
				/*foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					
					foreach($newSkus as $newSku)
						{
							
							$totalvalue = $splitOrderDetail['MergeUpdate']['price'];
							$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
							$getsku = 'S-'.$newSku[1];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title	=	$getOrderDetail['Product']['product_name'];
							$weight	=	$weight + $getOrderDetail['ProductDesc']['weight'];
							$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.substr($title, 0, 25 ).'</td>
									<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
							$str .= '</tr>';
						}
							unset($newSkus);
				}*/
				
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					
					foreach($newSkus as $newSku)
						{
							
							$totalvalue = $splitOrderDetail['MergeUpdate']['price'];
							$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
							$getsku = 'S-'.$newSku[1];
							$qty = $newSku[0];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title		=	$getOrderDetail['Product']['product_name'];
							$weight		=	$weight + $getOrderDetail['ProductDesc']['weight'];
							$catName	=	$getOrderDetail['Product']['category_name'];
							$allcat[]	=	$getOrderDetail['Product']['category_name'];
							if($country == 'United States')
							{
								$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$catName.'</td>
									<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';
							}
							elseif($country == 'Spain')
							{
								$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$catName.'</td>
									<td valign="top" class="center noleftborder rightborder bottomborder">'.$qty.'</td>
									<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';
							}
							else
							{
								$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.substr($title, 0, 25 ).'</td>
										<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';
							}
						}
							unset($newSkus);
				}
				
				$addData['company'] 	= 	$company;
				$addData['fullname'] 	= 	$fullname;
				$addData['address1'] 	= 	$address1;
				$addData['address2'] 	= 	$address2;
				$addData['address3'] 	= 	$address3;
				$addData['town'] 		= 	$town;
				$addData['resion'] 		= 	$resion;
				$addData['postcode'] 	= 	$postcode;
				$addData['country'] 	= 	$country;
				
				$address 			= 		$this->getCountryAddress( $addData );	
				
				/*$address			=		'';
				$address			.=		($company != '') ? '<b>'.$company.'</b><br>' : '' ; 
				$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
				$address			.=		($address1 != '' ) ? $address1.'<br>' : '' ; 
				$address			.=		($address2 != '' ) ? $address2.',' : '' ; 
				$address			.=		($address3 != '' ) ? $address3.'<br>' : '' ;
				$address			.=		($town != '' ) ? $town.'<br>' : '';
				$address			.=		($resion != '' ) ? $resion.'<br>' : '';
				$address			.=		($postcode != '' ) ? $postcode.'<br>' : '';
				$address			.=		($country != '' ) ? $country.'<br>' : '';
				$CARTAS 			 = ' ';
				if(trim($country) == 'Spain'){
					$address		     =		'';
					$address			.=		($company != '')   ? '<b>'.ucwords($company).'</b><br>' : '' ; 
					$address			.=		($fullname != '')  ? ucwords($fullname).'<br>' : '' ; 
					$address			.=		($address1 != '' ) ? ucwords($address1).'<br>' : '' ; 
					$address			.=		($address2 != '' ) ? ucwords($address2) : '' ; 
					$address			.=		($address3 != '' ) ? ','.ucwords($address3).'<br>' : '<br>' ;
					$address			.=		($postcode != '' ) ? strtoupper($postcode) : '';					
					$address			.=		($town != '' )     ? ' '.strtoupper($town) : '';
					$address			.=		($resion != '' )   ? '<br> '. strtoupper($resion) : '';					
					$address			.=		($postcode != '' ) ? '<br>' : '';
					$address			.=		($country != '' )  ? $country.'<br>' : '';
					
					$CARTAS = 'CARTAS '.substr($postcode, 0, 1);
					if(substr($postcode, 0, 1) == 0){
						$CARTAS = 'CARTAS '.substr($postcode, 1, 1);
					}					
				}*/
				
				$CARTAS = 'CARTAS '.substr($postcode, 0, 1);
					if(substr($postcode, 0, 1) == 0){
						$corrCode = $this->getCorriosBarcode( filter_var($postcode, FILTER_SANITIZE_NUMBER_INT), $splitOrderId);
						$CARTAS = 'CARTAS '.substr($postcode, 1, 1);
					}
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				$currentdate		=	 	date("j, F  Y");
				$BarcodeImage		=		$splitOrderDetail['MergeUpdate']['order_barcode_image'];
				$corriosImage		=		filter_var($postcode, FILTER_SANITIZE_NUMBER_INT).'90019.png';
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= str_replace( 'Spain' , 'ESPAÑA', $address); 
				$setRepArray[] 					= $country;
				$barcode  						= $order['assign_barcode'];
				$barcodenum						= explode('.', $barcode);
				$barcodePath  					= Router::url('/', true).'img/orders/barcode/';
				$barcodeCorriosPath  			= Router::url('/', true).'img/orders/correos_barcode/';
				
				$barcodenum						= explode('.', $barcode);
			
				/**************** for tempplate *******************/
				$countryArray = Configure::read('customCountry');
				/*$labelDetail =	$this->Template->find('first', array(
											'conditions' => array( 
													'Template.location_name' => $country,
													'Template.store_name' => $subSource,
													'Template.status' => '1'
													)
												)
											);
				
				if( empty( $labelDetail ) )
				{
					if( in_array( $country , $countryArray ) && $country != 'United Kingdom' )
						{
							$labelDetail =	$this->Template->find('first', array(
													'conditions' => array( 
															'Template.location_name' => 'Rest Of EU',
															'Template.store_name' => $subSource,
															'Template.status' => '1'
															)
														)
													);
						}
					else
						{
							$labelDetail =	$this->Template->find('first', array(
													'conditions' => array( 
															'Template.location_name' => 'Other',
															'Template.store_name' => $subSource,
															'Template.status' => '1'
															)
														)
													);
						}
				}*/
				
				
				if( $splitOrderDetail['MergeUpdate']['custom_service_marked'] == 1 || (  $splitOrderDetail['MergeUpdate']['postal_service'] == 'Express' || $splitOrderDetail['MergeUpdate']['postal_service'] == 'Tracked' || $splitOrderDetail['MergeUpdate']['postal_service'] == 'Standard_Jpost') )
				{
					$labelDetail =	$this->Template->find('first', array(
												'conditions' => array(
														'Template.location_name' => 'Other',
														'Template.store_name' => $subSource, 
														'Template.label_name' => $splitOrderDetail['MergeUpdate']['service_provider'],
														'Template.status' => '1'
														)
													)
												);
					if( empty($labelDetail) )
					{
						$labelDetail =	$this->Template->find('first', array(
												'conditions' => array( 
														'Template.location_name' => $country,
														'Template.store_name' => $subSource,
														'Template.status' => '1'
														)
													)
												);
					}
				}
				else
				{
					$labelDetail =	$this->Template->find('first', array(
												'conditions' => array( 
														'Template.location_name' => $country,
														'Template.store_name' => $subSource,
														'Template.status' => '1'
														)
													)
												);
					
					if( empty( $labelDetail ) )
					{
						if( in_array( $country , $countryArray ) && $country != 'United Kingdom' )
							{
								$labelDetail =	$this->Template->find('first', array(
														'conditions' => array( 
																'Template.location_name' => 'Rest Of EU',
																'Template.store_name' => $subSource,
																'Template.status' => '1'
																)
															)
														);
							}
						else
							{
								$labelDetail =	$this->Template->find('first', array(
														'conditions' => array( 
																'Template.location_name' => 'Other',
																'Template.store_name' => $subSource,
																'Template.status' => '1'
																)
															)
														);
							}
					}
				}
				
				$html           =  $labelDetail['Template']['html'];
				$paperHeight    =  $labelDetail['Template']['paper_height'];
				$paperWidth  	=  $labelDetail['Template']['paper_width'];
				$barcodeHeight  =  $labelDetail['Template']['barcode_height'];
				$barcodeWidth   =  $labelDetail['Template']['barcode_width'];
				$paperMode      =  $labelDetail['Template']['paper_mode'];
				/********************************/
				
				$dompdf->set_paper(array( 0, 0, $paperWidth, $paperHeight ), $paperMode );
				//$barcodeimg = '<img src='.$barcodePath.$BarcodeImage.'>';
				$barcodeimg = '<img src='.$barcodePath.$BarcodeImage.' width = '.$barcodeWidth.' >';
			
				$setRepArray[] 	=    $barcodeimg;
				$setRepArray[] 	=    $barcodenum[0];
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totlaWeight;
				$setRepArray[]	=	 $tax;
				$setRepArray[]	=	 $currentdate;
				$logopath		=	 Router::url('/', true).'img/';
				$setRepArray[]	=	 '<img src="'.$logopath.'logo.jpg" >';
				$logo			=	 '<img src="'.$logopath.'logo.jpg" >';
				$signature		=	 '<img src="'.$logopath.'sig.jpg" >';
				
				$setRepArray[]	=	 $logo;
				$setRepArray[]	=	 $signature;
				$setRepArray[]	=	 $splitOrderId;
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				
				$tempId			=	$splitOrderDetail['MergeUpdate']['lable_id'];
				$countryCode	=	$splitOrderDetail['MergeUpdate']['country_code'];
				
				$setRepArray[]	=	 $countryCode;
				$setRepArray[]	=	 $weight;
				$conversionRate	=	Configure::read( 'conversionRate' );
				if($currency == 'GBP')
				{
					$totalvalue = $totalvalue;
				}
				else
				{
					$totalvalue = number_format($totalvalue/$conversionRate, 2, '.' ,'');
				}
				
				if($totalvalue >= 22  && $labelDetail['Template']['label_name'] == 'Jersey Post'){
					$numbers = array("18.11","18.32","18.52","18.72","18.94","19.27","19.45","19.62","19.82","20.11","20.31","20.71","21.22","21.41", "21.61","21.51");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				
				$setRepArray[]	=	 $totalvalue;
				if($splitOrderDetail['MergeUpdate']['track_id'] != '')
				{
					$trackingnumber = $splitOrderDetail['MergeUpdate']['track_id'];
				}
				else
				{
					$trackingnumber = 'A'.mt_rand(100000, 999999);
				}
				
				$setRepArray[]	=	 $trackingnumber;
				$setRepArray[]	=	 $subSource;
				$setRepArray[]	=	 $CARTAS;
				$setRepArray[]	=	 $splitOrderDetail['MergeUpdate']['reg_num_img'];
				$setRepArray[]	=	 '<img src='.$barcodeCorriosPath.$corriosImage.'>';
			
				$cssPath = WWW_ROOT .'css/';
				$imgPath = Router::url('/', true) .'img/';
			
			    $html = '<meta charset="utf-8">
						 <meta http-equiv="X-UA-Compatible" content="IE=edge">
						 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
						 <meta content="" name="description"/>
						 <meta content="" name="author"/><body>'.$html.'</body>';
				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				
				$html 	= $this->setReplaceValueLabel( $setRepArray, $html );
				//echo $html;
				//exit;
				//$html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				$dompdf->stream("OrderIdslip-$splitOrderId.pdf");
				
				$imgPath = WWW_ROOT .'img/printPDF/'; 
				$path = Router::url('/', true).'img/printPDF/';
				$date = new DateTime();
				$timestamp = $date->getTimestamp();
				$name	=	'Packagin_Label_'.$splitOrderId.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				$serverPath  	= 	$path.$name;
				$printerId		=	$this->getZebra();
				
				$sendData = array(
					'printerId' => $printerId,
					'title' => 'Now Print',
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'
				);
				
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				$d = $Coreprinter->toPrint( $sendData );
				pr($d); exit;
		}
		
		
		function setReplaceValueLabel( $setRepArray, $htmlString )
		{
	
			$constArray = array();
			$constArray[] = '_ADDRESS1_';
			$constArray[] = '_ADDRESS2_';
			$constArray[] = '_ADDRESS3_';
			$constArray[] = '_TOWN_';
			$constArray[] = '_RESION_';
			$constArray[] = '_ADDRESS_';
			$constArray[] = '_COUNTRY_';
			$constArray[] = '_BARCODEIMAGE_';
			$constArray[] = '_BARCODENUMBER_';
			$constArray[] = '_ORDERDETAIL_';
			$constArray[] = '_TAX_';
			$constArray[] = '_DATE_';
			$constArray[] = '_CURRENTDATE_';
			$constArray[] = '_LOGO_';
			$constArray[] = '';
			$constArray[] = '_SIGNATURE_';
			$constArray[] = '_SPLITORDERID_';
			$constArray[] = '_COUNTRYCODE_';
			$constArray[] = '_TOTALWEIGHT_';
			$constArray[] = '_TOTALVALUE_';
			$constArray[] = '_TRACKINGNUMBER_';
			$constArray[] = '_SUBSOURCE_';
			$constArray[] = '_CARTAS_';
			$constArray[] = '_REGBARCODENUMBER_';
			$constArray[] = '_CORRIOSBARCODE_';
			$constArray[] = '_PHONENUMBER_';
			$constArray[] = '_FREZILEITEM_';
			
			$getRep = $htmlString;
			$getRep = str_replace( $constArray[0],$setRepArray[0],$getRep );
			$getRep = str_replace( $constArray[1],$setRepArray[1],$getRep );
			$getRep = str_replace( $constArray[2],$setRepArray[2],$getRep );
			$getRep = str_replace( $constArray[3],$setRepArray[3],$getRep );
			$getRep = str_replace( $constArray[4],$setRepArray[4],$getRep );
			$getRep = str_replace( $constArray[5],$setRepArray[5],$getRep );
			$getRep = str_replace( $constArray[6],$setRepArray[6],$getRep );
			$getRep = str_replace( $constArray[7],$setRepArray[7],$getRep );
			$getRep = str_replace( $constArray[8],$setRepArray[8],$getRep );
			$getRep = str_replace( $constArray[9],$setRepArray[9],$getRep );
			$getRep = str_replace( $constArray[10],$setRepArray[10],$getRep );
			$getRep = str_replace( $constArray[11],$setRepArray[11],$getRep );
			$getRep = str_replace( $constArray[12],$setRepArray[12],$getRep );
			$getRep = str_replace( $constArray[13],$setRepArray[13],$getRep );
			$getRep = str_replace( $constArray[14],$setRepArray[14],$getRep );
			$getRep = str_replace( $constArray[15],$setRepArray[15],$getRep );
			$getRep = str_replace( $constArray[16],$setRepArray[16],$getRep );
			$getRep = str_replace( $constArray[17],$setRepArray[17],$getRep );
			$getRep = str_replace( $constArray[18],$setRepArray[18],$getRep );
			$getRep = str_replace( $constArray[19],$setRepArray[19],$getRep );
			$getRep = str_replace( $constArray[20],$setRepArray[20],$getRep );
			$getRep = str_replace( $constArray[21],$setRepArray[21],$getRep );
			$getRep = str_replace( $constArray[22],$setRepArray[22],$getRep );
			$getRep = str_replace( $constArray[23],$setRepArray[23],$getRep );
			$getRep = str_replace( $constArray[24],$setRepArray[24],$getRep );
			$getRep = str_replace( $constArray[25],$setRepArray[25],$getRep );
			$getRep = str_replace( $constArray[26],$setRepArray[26],$getRep );
			return $getRep;
			
		}
		
		public function getZebra()
		{
			$this->layout = '';
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );						
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name']; //getenv('COMPUTERNAME');
			$paramsZDesigner = array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name' => 'ZDesigner'
				)
			);
			//ZDesigner
			$pcDetailZDesigner = json_decode(json_encode($this->PcStore->find('all', $paramsZDesigner)),0);						
			$zDesignerPrinter = $pcDetailZDesigner[0]->PcStore->printer_id;
		
		return $zDesignerPrinter;	
		}
		
		
		public function customSlipPrint( $splitOrderId = null )
		{
			
				$this->loadModel( 'PackagingSlip' );
				$this->loadModel( 'MergeUpdate' );
				$this->loadModel( 'Product' );
				$this->loadMOdel( 'CategoryContant' );
				
				$splitOrderId =  trim($this->request->data['orderid']);
				$orderArray = explode( '-', $splitOrderId );
				$id				=	$orderArray[0];
				
				$openOrderId	=	$orderArray[0];
				$splitOrderId	=	$splitOrderId;
		
				$this->layout = '';
				$this->autoRender = false;
				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				
				$dompdf->set_paper(array(0, 0, 245, 450), 'portrait');
				$order	=	$this->getOpenOrderById( $openOrderId );
				
				
				$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
				$itemPrice		=	$getSplitOrder['MergeUpdate']['price'];
				$itemQuantity	=	$getSplitOrder['MergeUpdate']['quantity'];
				$BarcodeImage	=	$getSplitOrder['MergeUpdate']['order_barcode_image'];
				
				
				$k = '10';
				$companyname = '';
				$returnaddress = '';
				switch ($k) {
								case 0:
									echo "";
									break;
								case 1:
									echo "";
									break;
								case 2:
									echo "";
									break;
								default:
								   $companyname 	 =  'Jij Group';
								   $returnaddress 	 =  'Address Line 1 <br> Address Line 2 <br>Some Town Some Region 123 ABC <br>United Country';
							}
				
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$subsource			=		$order['sub_source'];
				
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$address			=		'';
				$address			.=		($company != '') ? $company.'<br>' : '' ; 
				$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
				$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
				$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
				$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
				$address			.=		($town != '') ? $town.'<br>' : '';
				$address			.=		($resion != '') ? $resion.'<br>' : '';
				$address			.=		($postcode != '') ? $postcode.'<br>' : '';
				$address			.=		($country != '' ) ? $country.'<br>' : '';
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= $postcode;
				$setRepArray[] 					= $country;
				$setRepArray[] 					= $phone;
				$setRepArray[] 					= $ordernumber;
				$setRepArray[] 					= $courier;
				$setRepArray[] 					= $recivedate[0];
				$i = 1;
				$str = '';
				$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
				$totalGoods = 0;
				$productInstructions = '';
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					foreach($newSkus as $newSku)
						{
							
							$getsku = 'S-'.$newSku[1];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							
							$contentCat 	= 	$getOrderDetail['Product']['category_name'];
							$contentSubCat	=	$getOrderDetail['Product']['sub_category'];
							$productBarcode	=	$getOrderDetail['ProductDesc']['barcode'];
							$getContent	= $this->CategoryContant->find( 'first', array( 
																'conditions'=>array( 'CategoryContant.sub_category'=>$contentSubCat,'CategoryContant.sub_source'=>$subsource) ) );
							if(count($getContent) > 0) {
								$productBarcode			=	$getOrderDetail['ProductDesc']['barcode'];
								$getbarcode				= 	$this->getBarcode( $productBarcode );
								$productInstructions 	=  '<p style="font-size:14px;">'.$getContent['CategoryContant']['content'].'</p>';
							}
							$title	=	$getOrderDetail['Product']['product_name'];
							$totalGoods = $totalGoods + $newSku[0];
							$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
									<td valign="top" class="rightborder bottomborder">'.substr($title, 0, 15 ).'</td>
									<td valign="top" class="center norightborder bottomborder">'.$newSku[0][0].'</td>';
							$str .= '</tr>';
							$i++;
						}
						unset($newSkus);
					
				}
				
				/**********************************************/
				$countryArray = Configure::read('customCountry');
				
				if($country == 'Germany')
				{
					$getSourceName = $this->getCompanyNameBySubsource( $subsource );
					
					$gethtml =	$this->PackagingSlip->find('first', array(
											'conditions' => array( 
													'PackagingSlip.store_name' => $subsource,
													'PackagingSlip.custom_slip_status' => 0
													)
												)
											);
					
				
				}
				else
				{
					$gethtml =	$this->PackagingSlip->find('first', array(
											'conditions' => array( 
													'PackagingSlip.store_name' => $subsource,
													'PackagingSlip.custom_slip_status' => 0
													)
												)
											);
				
				}
				
				/*$gethtml =	$this->PackagingSlip->find('first', array(
											'conditions' => array( 
													'PackagingSlip.store_name' => $subsource,
													)
												)
											);*/
				
				
				$html 			=	$gethtml['PackagingSlip']['html'];
				$paperHeight    =   $gethtml['PackagingSlip']['paper_height'];
				$paperWidth  	=   $gethtml['PackagingSlip']['paper_width'];
				$barcodeHeight  =   $gethtml['PackagingSlip']['barcode_height'];
				$barcodeWidth   =   $gethtml['PackagingSlip']['barcode_width'];
				$paperMode      =   $gethtml['PackagingSlip']['paper_mode'];
				
				
				/**********************************************/
				
					$dompdf->set_paper(array(0, 0, $paperWidth, $paperHeight ), $paperMode);
				
				$totalitem = $i - 1;
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totalGoods;
				$setRepArray[]	=	 $subtotal;
				$Path 			= 	'/wms/img/client/';
				$img			=	 '<img src=http://localhost/wms/img/client/demo.png height="50" width="50">';
				$setRepArray[]	=	 $img;
				$setRepArray[]	=	 $postagecost;
				$setRepArray[]	=	 $tax;
				$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
				$setRepArray[]	=	 $itemPrice;
				$setRepArray[]	=	 $address;
				$barcodePath  	=  Router::url('/', true).'img/orders/barcode/';
				$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.' width='. $barcodeWidth .'>';
				$barcodenum		=	explode('.', $barcode);
				
				$setRepArray[] 	=  $barcodeimg;
				$setRepArray[] 	=  $paymentmethod;
				$setRepArray[] 	=  $barcodenum[0];
				$setRepArray[] 	=  '';
				
				
				
				$img 			=	$gethtml['PackagingSlip']['image_name'];
				$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
				$setRepArray[] 	=  $returnaddress;
				$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
				$setRepArray[] 	= 	$splitOrderId;
				$setRepArray[] 	= 	utf8_decode( $productInstructions );
				$prodBarcodePath  	=  Router::url('/', true).'img/product/barcodes/';
				$setRepArray[] 	=  '<img src='.$prodBarcodePath.$productBarcode.'.png width='. $barcodeWidth .'>';
				
				$imgPath = WWW_ROOT .'css/';
				
				$html2 = '<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				<meta content="" name="description"/>
				<meta content="" name="author"/><body>'.$html;
				if( $productInstructions != '' )
				{
					$html2 .= '_PRODUCTINSTRUCTION_';
				} else {
					$html2 .= '</div>';
				}
				
				'</body>';
				
				$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
				$html 		= 	$this->setReplaceValue( $setRepArray, $html2 );
				//$html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				$dompdf->stream("OrderId-$splitOrderId.pdf");
				
				$imgPath = WWW_ROOT .'img/printPDF/'; 
				$path = Router::url('/', true).'img/printPDF/';
				$name	=	'Packagin_Slip_'.$splitOrderId.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				$serverPath  	= 	$path.$name ;
				$printerId	=	$this->getEpson();
				
				
				$sendData = array(
					'printerId' => $printerId,
					'title' => 'Now Print',
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'					
				);
				
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				$d = $Coreprinter->toPrint( $sendData );
				pr($d); exit;
			
		}
		
		function setReplaceValue( $setRepArray, $htmlString )
		{
	
			$constArray = array();
			$constArray[] = '_ADDRESS1_';
			$constArray[] = '_ADDRESS2_';
			$constArray[] = '_ADDRESS3_';
			$constArray[] = '_TOWN_';
			$constArray[] = '_RESION_';
			$constArray[] = '_POSTCODE_';
			$constArray[] = '_COUNTRY_';
			$constArray[] = '_PHONE_';
			$constArray[] = '_ORDERNUMBER_';
			$constArray[] = '_COURIER_';
			$constArray[] = '_RECIVEDATE_';
			$constArray[] = '_ORDERSUMMARY_';
			$constArray[] = '_TOTALITEM_';
			$constArray[] = '_SUBTOTAL_';
			$constArray[] = '_IMAGE_';
			$constArray[] = '_POSTAGECOST_';
			$constArray[] = '_TAX_';
			$constArray[] = '_TOTALAMOUNT_';
			$constArray[] = '_ADDRESS_';
			$constArray[] = '_BARCODE_';
			$constArray[] = '_PAYMENTMETHOD_';
			$constArray[] = '_BARCODENUMBER_';
			$constArray[] = '_COMPANYNAME_';
			$constArray[] = '_RETURNADDRESS_';
			$constArray[] = '_LOGO_';
			$constArray[] = '_SPLITORDERID_';
			$constArray[] = '_PRODUCTINSTRUCTION_';
			$constArray[] =	'_PRODUCTBARCODEIMAGE_';
			$constArray[] =	'_TOTALCOST_';
			$constArray[] =	'_INK_';
			
			$getRep = $htmlString;
			$getRep = str_replace( $constArray[0],$setRepArray[0],$getRep );
			$getRep = str_replace( $constArray[1],$setRepArray[1],$getRep );
			$getRep = str_replace( $constArray[2],$setRepArray[2],$getRep );
			$getRep = str_replace( $constArray[3],$setRepArray[3],$getRep );
			$getRep = str_replace( $constArray[4],$setRepArray[4],$getRep );
			$getRep = str_replace( $constArray[5],$setRepArray[5],$getRep );
			$getRep = str_replace( $constArray[6],$setRepArray[6],$getRep );
			$getRep = str_replace( $constArray[7],$setRepArray[7],$getRep );
			$getRep = str_replace( $constArray[8],$setRepArray[8],$getRep );
			$getRep = str_replace( $constArray[9],$setRepArray[9],$getRep );
			$getRep = str_replace( $constArray[10],$setRepArray[10],$getRep );
			$getRep = str_replace( $constArray[11],$setRepArray[11],$getRep );
			$getRep = str_replace( $constArray[12],$setRepArray[12],$getRep );
			$getRep = str_replace( $constArray[13],$setRepArray[13],$getRep );
			$getRep = str_replace( $constArray[14],$setRepArray[14],$getRep );
			$getRep = str_replace( $constArray[15],$setRepArray[15],$getRep );
			$getRep = str_replace( $constArray[16],$setRepArray[16],$getRep );
			$getRep = str_replace( $constArray[17],$setRepArray[17],$getRep );
			$getRep = str_replace( $constArray[18],$setRepArray[18],$getRep );
			$getRep = str_replace( $constArray[19],$setRepArray[19],$getRep );
			$getRep = str_replace( $constArray[20],$setRepArray[20],$getRep );
			$getRep = str_replace( $constArray[21],$setRepArray[21],$getRep );
			$getRep = str_replace( $constArray[22],$setRepArray[22],$getRep );
			$getRep = str_replace( $constArray[23],$setRepArray[23],$getRep );
			$getRep = str_replace( $constArray[24],$setRepArray[24],$getRep );
			$getRep = str_replace( $constArray[25],$setRepArray[25],$getRep );
			$getRep = str_replace( $constArray[26],$setRepArray[26],$getRep );
			$getRep = str_replace( $constArray[27],$setRepArray[27],$getRep );
			$getRep = str_replace( $constArray[28],$setRepArray[28],$getRep );
			$getRep = str_replace( $constArray[29],$setRepArray[29],$getRep );
			return $getRep;
			
		}
		
		public function getEpson()
		{
			$this->layout = '';
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name']; //getenv('COMPUTERNAME');
			$paramsEpson = array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name' => 'EPSONTM-T88V'
				)
			);
			
			//EPSONTM-T88V			
			$pcDetailEpson = json_decode(json_encode($this->PcStore->find('all', $paramsEpson)),0);			
			$epsonT88 = $pcDetailEpson[0]->PcStore->printer_id;
			
		return $epsonT88;	
		}
		
		public function varifyLabelPrint()
		{
			//$splitOrderId =	'2044634-1';
			$this->layout = '';
			$this->autoRender = false;
			$this->loadModel('Template');
			$this->loadModel('MergeUpdate');
			$this->loadModel('Product');
			
			if(isset($this->request->data['orderid']) && $this->request->data['orderid'] !='' ){
				$splitOrderId = trim($this->request->data['orderid']);
			}
			
			$orderArray 	=   explode( '-', $splitOrderId );
			$id				=	$orderArray[0];
			$openOrderId	=	$orderArray[0];
			
			$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
			$order			=	$this->getOpenOrderById( $openOrderId );
			$Country = $order['customer_info']->Address->Country;
			
			if($splitOrderDetail['MergeUpdate']['service_provider'] == 'DHL'){				
				echo Router::url('/', true).'img/dhl/label_'.$splitOrderId.'.pdf';
				$order_id = trim(explode("-",$splitOrderId)[0]);
				App::import( 'Controller' , 'Invoice' );		
				$_invoice = new InvoiceController();					
				$_invoice->getDHLSlip($order_id); 		
				//http://xsensys.com/img/dhl/label_'.$splitOrderId.'.pdf'
			}else if($splitOrderDetail['MergeUpdate']['service_provider'] == 'Royalmail' ){
				echo Router::url('/', true).'royalmail/labels/label_'.$splitOrderId.'.pdf';		
			}else if($splitOrderDetail['MergeUpdate']['service_provider'] == 'Jersey Post' && !in_array($Country,['United Kingdom','Jersey','Guernsey','Isle of Man']) ){
				echo Router::url('/', true).'jerseypost/labels/Label_'.$splitOrderId.'.pdf';
			}else if(strtolower($splitOrderDetail['MergeUpdate']['service_provider']) == 'whistl'){
				App::import('Controller', 'Whistl');
				$whObj = new WhistlController(); 
				$whObj->genrateLabel( $splitOrderId );
				echo Router::url('/', true).'whistl/Label_'.$splitOrderId.'.pdf';
			}else if($splitOrderDetail['MergeUpdate']['service_provider'] == 'PostNL' && $splitOrderDetail['MergeUpdate']['postnl_barcode'] != '' ){
				App::import('Controller', 'Postnl');
				$pObj = new PostnlController(); 
				$pObj->getSingleLabel( $splitOrderId );
				echo Router::url('/', true).'postnl/labels/Label_'.$splitOrderId.'.pdf';		
			}else if($splitOrderDetail['MergeUpdate']['service_provider'] == 'GLS' ){
				echo Router::url('/', true).'gls/labels/label_'.$splitOrderId.'.pdf';		
			}else if($splitOrderDetail['MergeUpdate']['brt'] == 1 ){
				echo Router::url('/', true).'img/brt/label_'.$splitOrderId.'.pdf';		
			}else{
				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
							
				$order				=		$this->getOpenOrderById( $openOrderId );
				$postalservices		=		$order['shipping_info']->PostalServiceName;
				$destination		=		$order['customer_info']->Address->Country;
				$total				=		$order['totals_info']->TotalCharge;
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$totlacharge		=		$order['totals_info']->TotalCharge;
				
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company; 
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$currency			=	 	$order['totals_info']->Currency;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$templateId			=	 	$order['template_id'];
				$subSource			=	 	$order['sub_source'];
				$str = ''; 
				$totlaWeight		=		0;
				
				if($splitOrderId == '2609354-1'){
					$address1 =  'Mr Rebholtz Jérôme - 4 A RUE LIVIO';
				}else if($splitOrderId == '2609353-1'){
					$address1 =  'Mrs TERROLLES Frederique - 25 route de Billom';
				}else if($splitOrderId == '2609355-1'){
					$address1 =  'Mr GUEGUEN Francois - KERANSQUER KERNEVEL';
				}else if($splitOrderId == '2609353-1'){
					//$address1 =  'Mr LE NOAN Andre - 271 CHEMIN DE CHEZ PALLUD';
				}
				 
				/*$address1 ='9, ruelle de la prison';
				$address2 = $address3 = $resion= ''; 
		*/        
		
				$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
				$weight = 0;
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					
					foreach($newSkus as $newSku)
						{
							
							$totalvalue = $total;
							$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
							$getsku = 'S-'.$newSku[1];
							$qty = $newSku[0];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title	=	$getOrderDetail['Product']['product_name'];
							$weight	=		$weight + ($newSku[0] * $getOrderDetail['ProductDesc']['weight']);
							$catName	=	$getOrderDetail['Product']['category_name'];
							if($country == 'United States')
							{
								$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.$catName.'</td>
									<td valign="top" class="center norightborder bottomborder">'.$weight.'</td>';
								$str .= '</tr>';
							}
							else if($country == 'Canada')
							{
								$str .= '<tr>
									<td valign="top" class="leftborder rightborder bottomborder">'.$catName.'</td>
									<td valign="top" class="leftborder rightborder bottomborder">'.$newSku[0].'</td>
									<td valign="top" class="center rightborder bottomborder">'.$weight.'</td>';
								$str .= '</tr>';
							}
							elseif($country == 'Spain')
							{
								/*$tw = $newSku[0] * $getOrderDetail['ProductDesc']['weight'];
								$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$catName.'</td>
									<td valign="top" class="center noleftborder rightborder bottomborder">'.$qty.'</td>
									<td valign="top" class="center norightborder bottomborder">'.$newSku[0] * $getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';*/
								
								$str .='<tr>
											<td  width="60%"  style="border:1px solid #000; padding:5px;">'.$catName.'</td>
											<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty * $getOrderDetail['ProductDesc']['weight'].'</td>
											<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty.'</td>';
								$str .= '</tr>';
							}
							else
							{
								$tw = $newSku[0] * $getOrderDetail['ProductDesc']['weight'];
								$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.substr($title, 0, 25 ).'</td>
										<td valign="top" class="center norightborder bottomborder">'.$newSku[0] * $getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';
							}
						}
							unset($newSkus);
				}
				
				
				$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forlì-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia','MI'=>'Milano','NA'=>'Napoli','TO'=>'Torino');
				
				
				$cana_provinces = array('AB' =>'Alberta','BC'=>'British Columbia','MB'=>'Manitoba','NB'=>'New Brunswick','NL'=>'Newfoundland and Labrador','NS'=>'Nova Scotia','NT'=>'Northwest Territories','NU'=>'Nunavut','ON'=>'Ontario','PE'=>'Prince Edward Island','QC'=>'Quebec','SK'=>'Saskatchewan','YT'=>'Yukon');
				
				$addData['sub_source'] 	= 	$order['sub_source'];
				$addData['company'] 	= 	$company;
				$addData['fullname'] 	= 	$fullname;
				$addData['address1'] 	= 	$address1;
				$addData['address2'] 	= 	$address2;
				$addData['address3'] 	= 	$address3;
				$addData['town'] 		= 	$town;
				
				if( $country == 'Italy' )
				{
					if(array_key_exists(strtoupper($resion),$provinces)){
						$addData['resion'] 		= 	$resion;
					}else {
						$addData['resion'] 		= 	array_search(ucfirst($resion), $provinces);
					}
				}
				else if( $country == 'Canada' )
				{
					if(array_key_exists(strtoupper($resion),$cana_provinces)){
						$addData['resion'] 		= 	$resion;
					}else {
						$addData['resion'] 		= 	array_search(ucfirst($resion), $cana_provinces);
					}
				} else {
						$addData['resion'] 		= 	$resion;
				}
				$addData['postcode'] 	= 	$postcode;
				$addData['country'] 	= 	$country;
				 
				$address 			= 		$this->getCountryAddress( $addData );	
				//echo $address;
				//exit;
				/*$address			 =		'';
				$address			.=		($company != '') ? '<b>'.$company.'</b><br>' : '' ; 
				$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
				$address			.=		($address1 != '' ) ? $address1.'<br>' : '' ; 
				$address			.=		($address2 != '' ) ? $address2.',' : '' ; 
				$address			.=		($address3 != '' ) ? $address3.'<br>' : '' ;
				$address			.=		($town != '' ) ? $town.'<br>' : '';
				$address			.=		($resion != '' ) ? $resion.'<br>' : '';
				$address			.=		($postcode != '' ) ? $postcode.'<br>' : '';
				$address			.=		($country != '' ) ? $country.'<br>' : '';
				$CARTAS              = 		'';
				if(trim($country) == 'Spain'){
					$address		     =		'';
					$address			.=		($company != '')   ? '<b>'.ucwords($company).'</b><br>' : '' ; 
					$address			.=		($fullname != '')  ? ucwords($fullname).'<br>' : '' ; 
					$address			.=		($address1 != '' ) ? ucwords($address1).'<br>' : '' ; 
					$address			.=		($address2 != '' ) ? ucwords($address2) : '' ; 
					$address			.=		($address3 != '' ) ? ','.ucwords($address3).'<br>' : '<br>' ;
					$address			.=		($postcode != '' ) ? strtoupper($postcode) : '';					
					$address			.=		($town != '' )     ? ' '.strtoupper($town) : '';
					$address			.=		($resion != '' )   ? '<br> '. strtoupper($resion) : '';					
					$address			.=		($postcode != '' ) ? '<br>' : '';
					$address			.=		($country != '' )  ? $country.'<br>' : '';
					
					$CARTAS = 'CARTAS '.substr($postcode, 0, 1);
					if(substr($postcode, 0, 1) == 0){
						$CARTAS = 'CARTAS '.substr($postcode, 1, 1);
					}					
				}*/
				
				$CARTAS = ''; $routingcode ='';
				if(trim($country) == 'Spain') { 
					$CARTAS = 'CARTAS '.substr($postcode, 0, 1);
					$routingcode = $this->getCorreosBarcode( $postcode, $splitOrderId);
					if(substr($postcode, 0, 1) == 0){
						$CARTAS = 'CARTAS '.substr($postcode, 1, 1);
					}	
				}
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				$currentdate		=	 	date("j, F  Y");
				$BarcodeImage		=		$splitOrderDetail['MergeUpdate']['order_barcode_image'];
				//corriosImage		=		filter_var($postcode, FILTER_SANITIZE_NUMBER_INT).'90019.png';
				$corriosImage		=		$routingcode.'.png';
			
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= str_replace( 'Spain' , 'ESPAÑA', $address); 
				$setRepArray[] 					= $country;
				$barcode  						= $order['assign_barcode'];
				$barcodenum						= explode('.', $barcode);
				$barcodePath  					= WWW_ROOT.'img/orders/barcode/';
				$barcodeCorriosPath  			= WWW_ROOT.'img/orders/correos_barcode/';
				$barcodenum						= explode('.', $barcode);
			
				/**************** for tempplate *******************/
				if($subSource == 'Marec_FR' || $subSource == 'Marec_DE' || $subSource == 'Marec_IT' || $subSource == 'Marec_ES' || $subSource == 'Marec_UK'){
					$company 	=  'Marec';
				} else if($subSource == 'CostBreaker_UK' || $subSource == 'CostBreaker_DE' || $subSource == 'CostBreaker_FR' || $subSource == 'CostBreaker_ES' || $subSource == 'CostBreaker_IT'|| $subSource == 'costbreaker' || $subSource == 'CostBreaker_NL' ){
					$company 	=  'CostBreaker';
				} else if($subSource == 'Tech_Drive_UK' || $subSource == 'Tech_Drive_FR' || $subSource == 'Tech_drive_ES' || $subSource == 'Tech_drive_DE' || $subSource == 'Tech_Drive_IT' ){
					$company 	= 'Tech Drive Supplies';
				} else if($subSource == 'RAINBOW RETAIL DE' || $subSource == 'Rainbow Retail' || $subSource == 'Rainbow_Retail_ES' || $subSource == 'Rainbow_Retail_IT' || $subSource == 'Rainbow_Retail_FR'){
					$company 	= 'Rainbow Retail';
				}
				else if($subSource == 'Onbuy' || $subSource == 'Flubit' || $subSource == 'Euraco.fyndiq' || $subSource == 'Costdropper_Fnac'){
					$company 	=  'CostBreaker';
					$subSource  =  'CostBreaker_UK';
				}else if($subSource == 'Costdropper'){
					$company 	=  'CostBreaker';
					$subSource  =  'Real.de';
				}
				
				$countryArray = Configure::read('customCountry');
				$ref_code = trim($splitOrderDetail['MergeUpdate']['provider_ref_code']);
				$codearray = array("GYE", "FRU");
				$spaincode = array("mad_air","mad_road","cat_air","cat_road","sp_air","sp_road");
				
				if( $country == 'Spain' && empty($splitOrderDetail['MergeUpdate']['reg_post_number']))
				{ 
					$labelDetail =	$this->Template->find('first', array(
														'conditions' => array(
																'Template.location_name' => 'Rest Of EU',
																'Template.store_name' => $subSource
																)));
				}
				else if($subSource == 'Real.de'){
					
					$labelDetail =	$this->Template->find('first', array(
										'conditions' => array( 
										'Template.location_name' => $country,
										'Template.store_name' => $subSource,
										'Template.status' => '1',
										'Template.reg_post' => '0'
										)
								)
							); 
				}
				else
				{ 
					if(in_array($ref_code, $codearray))
					{ 
							$labelDetail =	$this->Template->find('first', array(
															'conditions' => array(
																	'Template.label_name' => 'deutsche_post',
																	'Template.location_name' => $country,
																	'Template.company_name' => $company
																	)));  
					} 
					else 
					{ 
						if(  !empty($splitOrderDetail['MergeUpdate']['reg_post_number']) &&  !empty($splitOrderDetail['MergeUpdate']['reg_num_img']) )
						{
							 
							$labelDetail =	$this->Template->find('first', array(
														'conditions' => array(
																'Template.location_name' => $country,
																'Template.store_name' => $subSource,
																'Template.reg_post' => '1'
																
																)
															)
														);
														
									if( empty( $labelDetail ) )
									{
										 
											$labelDetail =	$this->Template->find('first', array(
																	'conditions' => array( 
																			'Template.location_name' => 'Rest Of EU',
																			//'Template.store_name' => $subSource,
																			'Template.status' => '1',
																			'Template.reg_post' => '1'
																			)
																		)
																);
									 
								} 
						}
						else
						{
							if( $splitOrderDetail['MergeUpdate']['custom_service_marked'] == 1 || (  $splitOrderDetail['MergeUpdate']['postal_service'] == 'Express' || $splitOrderDetail['MergeUpdate']['postal_service'] == 'Tracked' || $splitOrderDetail['MergeUpdate']['postal_service'] == 'Standard_Jpost') )
							{
								$labelDetail =	$this->Template->find('first', array(
															'conditions' => array(
																	'Template.location_name' => 'Other',
																	'Template.store_name' => $subSource, 
																	'Template.label_name' => $splitOrderDetail['MergeUpdate']['service_provider'],
																	'Template.status' => '1',
																	'Template.reg_post' => '0'
																	)
																)
															);
								if( empty($labelDetail) )
								{
									$labelDetail =	$this->Template->find('first', array(
															'conditions' => array( 
																	'Template.location_name' => $country,
																	'Template.store_name' => $subSource,
																	'Template.status' => '1',
																	'Template.reg_post' => '0'
																	)
																)
															);
								}
								 
							}
							else
							{ 
								$labelDetail =	$this->Template->find('first', array(
															'conditions' => array( 
																	'Template.location_name' => $country,
																	'Template.store_name' => $subSource,
																	'Template.label_name' => $splitOrderDetail['MergeUpdate']['service_provider'],
																	'Template.status' => '1',
																	'Template.reg_post' => '0'
																	)
																)
															);
								
								if( empty( $labelDetail ) )
								{  
									if( in_array( $country , $countryArray ) && $country != 'United Kingdom' )
										{
											if( $splitOrderDetail['MergeUpdate']['service_provider'] == 'Jersey Post'){
												$labelDetail =	$this->Template->find('first', array(
																	'conditions' => array( 
																			'Template.location_name' => 'Other',
																			'Template.store_name' => $subSource,
																			'Template.label_name' => $splitOrderDetail['MergeUpdate']['service_provider'],
																			'Template.status' => '1',
																			'Template.reg_post' => '0'
																			)
																		)
																	);
											} else {
											
											 
												$labelDetail =	$this->Template->find('first', array(
																	'conditions' => array( 
																			'Template.location_name' => 'Rest Of EU',
																			'Template.store_name' => $subSource,
																			'Template.status' => '1',
																			'Template.reg_post' => '0'
																			)
																		)
																	);
											}
										}
									else
										{
											$labelDetail =	$this->Template->find('first', array(
																	'conditions' => array( 
																			'Template.location_name' => ['Rest Of EU','Other'],
																			'Template.label_name' => $splitOrderDetail['MergeUpdate']['service_provider'],
																			'Template.store_name' => $subSource,
																			'Template.status' => '1',
																			'Template.reg_post' => '0'
																			)
																		)
																	);
										}
								}
							}
						}
					 }
				}
					
					/*$labelDetail =	$this->Template->find('first', array(
																	'conditions' => array( 
																			'Template.id' => 96 
																			)
																		)
																	);*/
														
																		//pr($labelDetail);exit;		 							 
				$html           =  $labelDetail['Template']['html'];  
				$paperHeight    =  $labelDetail['Template']['paper_height'];
				$paperWidth  	=  $labelDetail['Template']['paper_width'];
				$barcodeHeight  =  $labelDetail['Template']['barcode_height'];
				$barcodeWidth   =  $labelDetail['Template']['barcode_width'];
				$paperMode      =  $labelDetail['Template']['paper_mode'];
				/********************************/
				 if($splitOrderDetail['MergeUpdate']['service_provider'] == 'PostNL' && $splitOrderDetail['MergeUpdate']['postal_service'] == 'Tracked'){
			
					/*require_once(APP . 'Vendor' . DS . 'code39' . DS . 'Barcode39.php'); 	
					$text = 'RK020423785JE';
					$barimg = WWW_ROOT."code39barcode/".$text.".png";
					$bc = new barcode() ;
					$result = $bc->code39($text, $height = "100", $widthScale = "2",$barimg); 
					
					*/
					
					App::import( 'Controller' , 'GenerateBarcodes');
					$gbarcode = new GenerateBarcodesController();
					$barcodeimage	=	$BarcodeImage;
					$gbarcode->GenerateCode($splitOrderId, 'code128','orders/barcode');
					$barcodeimg = '<img src="'.$barcodePath.$BarcodeImage.'" width = "'.$barcodeWidth.'" >';
				}else{
					$barcodeimg = '<img src="'.$barcodePath.$BarcodeImage.'" alt="Barcode">';
				}
				
				//pr($labelDetail);exit;
				//$dompdf->set_paper(array( 0, 0, 288, 500 ), 'portrait' );
				
				$dompdf->set_paper(array(0, 0, $paperWidth, $paperHeight ), $paperMode);
			
				$setRepArray[] 	=    $barcodeimg;
				$setRepArray[] 	=    $barcodenum[0];
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totlaWeight;
				//$setRepArray[]	=	 $weight;
				$setRepArray[]	=	 $tax;
				$setRepArray[]	=	 $currentdate;
				$logopath		=	 Router::url('/', true).'img/';
				$setRepArray[]	=	 '<img src="'.$logopath.'logo.jpg" >';
				$logo			=	 '<img src="'.$logopath.'logo.jpg" >';
				$signature		=	 '<img src="'.$logopath.'sig.jpg" >';
				
				$setRepArray[]	=	 $logo;
				$setRepArray[]	=	 $signature;
				$setRepArray[]	=	 $splitOrderId;
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				
				$tempId			=	$splitOrderDetail['MergeUpdate']['lable_id'];
				$countryCode	=	$splitOrderDetail['MergeUpdate']['country_code'];
				
				$setRepArray[]	=	 $countryCode;
				//$setRepArray[]	=	 $tw;
				$setRepArray[]	=	 $weight;
				$conversionRate	=	Configure::read( 'conversionRate' );
				
				if($currency == 'GBP')
				{
					$totalvalue = $totalvalue;
				}
				elseif($country == 'Spain' && $currency == 'EUR')
				{
					$totalvalue = $totalvalue;
				}
				elseif($country == 'United States')
				{
					$totalvalue = $totalvalue;
				}
				else
				{
					$totalvalue = number_format($totalvalue/$conversionRate, 2, '.' ,'');
				}
				
				if($totalvalue >= 1000  && $labelDetail['Template']['label_name'] == 'Jersey Post' && $country == 'United States' ){
					$numbers = array("97.11","98.32","98.52","99.72","99.94","150.27","200.45","270.62","370.82","422.11","472.31","530.71");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				else if($totalvalue >= 22  && $labelDetail['Template']['label_name'] == 'Jersey Post' && $country != 'United States'){
					$numbers = array("18.11","18.32","18.52","18.72","18.94","19.27","19.45","19.62","19.82","20.11","20.31","20.71","21.22","21.41", "21.61","21.51");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				if( $totalvalue == 0 )
				{
					$resendnumbers = array("18.11","18.32","18.52","18.72","18.94","19.27","19.45","19.62","19.82","20.11","20.31");
					shuffle($resendnumbers);				
					$totalvalue = $resendnumbers[0];
				} 
				
				if($totalvalue >= 22){
					$numbers = array("18.11","18.32","18.52","18.72","18.94","19.27","19.45","19.62","19.82","20.11","20.31","20.71","21.22","21.41", "21.61","21.51");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				$setRepArray[]	=	 $totalvalue;
				
				if($splitOrderDetail['MergeUpdate']['track_id'] != '')
				{
					$trackingnumber = $splitOrderDetail['MergeUpdate']['track_id'];
				}
				else
				{
					$trackingnumber = 'A'.mt_rand(100000, 999999);
				}
				
				$setRepArray[]	=	 $trackingnumber;
				$setRepArray[]	=	 $subSource;
				$setRepArray[]	=	 $CARTAS;
				$setRepArray[]	=	 $splitOrderDetail['MergeUpdate']['reg_num_img'];
				$setRepArray[]	=	 '<img src='.$barcodeCorriosPath.$corriosImage.' width=250>';
				$setRepArray[]	=	($phone != '') ? 'Telefono del cliente:'.$phone : '';
				//pr($setRepArray);
				$cssPath = WWW_ROOT .'css/';
				$imgPath = Router::url('/', true) .'img/';
			
				$html = '<meta charset="utf-8">
						 <meta http-equiv="X-UA-Compatible" content="IE=edge">
						 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
						 <meta content="" name="description"/>
						 <meta content="" name="author"/><body>'.$html.'</body>';
				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				$html 	= $this->setReplaceValueLabel( $setRepArray, $html );
				//$html   = str_replace("http://xsensys.com","http://xsensys.com",$html );
				 
				
				$dompdf->load_html($html, Configure::read('App.encoding'));
				//$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
			
				$imgPath = WWW_ROOT .'img/printPDF/'; 
				$path = Router::url('/', true).'img/printPDF/';
				unlink($imgPath.$name);
				$date = new DateTime();
				$timestamp = $date->getTimestamp();
				$name	=	'Packagin_Label_'.$splitOrderId.'.pdf';
				file_put_contents($imgPath.$name, $dompdf->output());
				echo $path.$name;
				}
			//exit;
		}





		//getCountryAddressLabel
		public function getCountryAddress( $addressArray )
		{
			//pr($addressArray);
			switch ($addressArray['country']) {
			case "United Kingdom":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<b>'.ucwords($addressArray['company']).'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords(strtolower($addressArray['fullname'])).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords(strtolower($addressArray['address1'])).'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords(strtolower($addressArray['address2'])) : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords(strtolower($addressArray['address3'])).'' : '' ;
					$address			.=		($addressArray['town'] != '' )     ? ' '.strtoupper($addressArray['town']).'<br>' : '';
					$address			.=		($addressArray['resion'] != '' )   ? ucwords(strtolower($addressArray['resion'])).'<br>' : '';
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).'<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br>' : '';
					return $address;
					break;
			case "United States":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<div style="font-size:17px;"><b>'.ucwords($addressArray['company']).'</b><br>' : '<div style="font-size:17px;">' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords($addressArray['fullname']).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1']).'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).'<br>' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']) : '' ;
					$address			.=		($addressArray['town'] != '' )     ? strtoupper($addressArray['town']).' ' : '';
					$address			.=		($addressArray['resion'] != '' )   ? strtoupper($addressArray['resion']).' ' : '';						
					
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).'<br>' : '';
					//$address			.=		($addressArray['postcode'] != '' ) ? '<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br></div>' : '';
					return $address;
					break;
			case "Canada":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<div style="font-size:15px;"><b>'.ucwords($addressArray['company']).'</b><br>' : '<div style="font-size:15px;">' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords($addressArray['fullname']).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1']).'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).'<br>' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']) : '' ;
					$address			.=		($addressArray['town'] != '' )     ? strtoupper($addressArray['town']).' ' : '';
					$address			.=		($addressArray['resion'] != '' )   ? strtoupper($addressArray['resion']).' ' : '';						
					
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).'<br>' : '';
					//$address			.=		($addressArray['postcode'] != '' ) ? '<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br></div>' : '';
					return $address;
					break;
			case "France":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<b>'.ucwords($addressArray['company']).'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords(strtolower($addressArray['fullname'])).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? strtoupper($addressArray['address1']).'<br>' : '' ; 
					
					if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']).'<br>' : '' ;						
					}
					
					if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).'<br>' : '' ;
					}
					
					if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']).'<br>' : '' ;
					}
					
					$address			.=		($addressArray['resion'] != '' )   ? strtoupper($addressArray['resion']).'<br>' : '';
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).' ' : '';
					$address			.=		($addressArray['town'] != '' )     ? strtoupper($addressArray['town']).'<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br>' : '';
					return $address;
					break;
			case "Germany":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<b>'.ucwords($addressArray['company']).'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords(strtolower($addressArray['fullname'])).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords(strtolower($addressArray['address1'])).'<br>' : '' ; 
					
					if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']).'<br>' : '' ;						
					}
					
					if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).'<br>' : '' ;
					}
					
					if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']).'<br>' : '' ;
					}
					
					//$address			.=		($addressArray['resion'] != '' )   ? ucwords(strtolower($addressArray['resion'])).'<br>' : '';
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).' ' : '';
					$address			.=		($addressArray['town'] != '' )     ? strtoupper($addressArray['town']).'<br>' : '';
					$address			.=		'DEUTSCHLAND';//($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br>' : '';
					return $address;
					break;
			case "Italy":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<b>'.strtoupper($addressArray['company']).'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '')  ? strtoupper(strtolower($addressArray['fullname'])).'<br>' : '' ; 
				     $address			.=		($addressArray['address1'] != '' ) ? strtoupper($addressArray['address1']).'<br>' : '' ; 
					
					 if($addressArray['address2'] != '' && $addressArray['address3'] != '') 
					 {
						$address			.=		($addressArray['address2'] != '' )   ? strtoupper($addressArray['address2']).'<br>' : '';
						$address			.=		($addressArray['address3'] != '' )   ? strtoupper($addressArray['address3']).'<br>' : '';
					}
					else if($addressArray['address2'] != '' && $addressArray['address3'] == '')
					{
						
						$address			.=		($addressArray['address2'] != '' )   ? strtoupper($addressArray['address2']).'<br>' : '';
						
					}
					else if($addressArray['address2'] == '' && $addressArray['address3'] != '')
					{
						
						$address			.=		($addressArray['address3'] != '' )   ? strtoupper($addressArray['address3']).'<br>' : '';
						
					}
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).' ' : ' ';
					$address			.=		($addressArray['town'] != '' )     ? strtoupper($addressArray['town']).' ' : ' ';
					if($addressArray['resion'] != '' )
					{
						
						$address			.=		($addressArray['resion'] != '' )   ? strtoupper($addressArray['resion']) : '';
					}
					else
					{
						$address			.=		"";
					}	
					
					
					$address			.=		($addressArray['country'] != '' )  ? "<br>".strtoupper($addressArray['country']).'<br>' : '';
					return $address;
					break;
			case "Spain":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<b>'.ucwords($addressArray['company']).'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords($addressArray['fullname']).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1']).'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."<br>" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords($addressArray['address3']).'<br>' : '' ;
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode'])."" : '<br>';					
					$address			.=		($addressArray['town'] != '' )     ? ' '.strtoupper($addressArray['town']) : '';
					$address			.=		($addressArray['resion'] != '' )   ? '<br> '. strtoupper($addressArray['resion']) : '';					
					$address			.=		($addressArray['postcode'] != '' ) ? '<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? $addressArray['country'].'<br>' : '';
					return $address;
					break;
			case "Denmark":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? '<b>'.$addressArray['company'].'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '') 	? $addressArray['fullname'].'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1'].'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3'].'<br>' : '' ;
					
					$address			.=		($addressArray['postcode'] != '' ) 	? $addressArray['postcode'].' ' : '';
					$address			.=		($addressArray['town'] != '' ) 		? $addressArray['town'].'<br>' : '';
					$address			.=		($addressArray['resion'] != '' ) 	? $addressArray['resion'].'<br>' : '';
					$address			.=		($addressArray['country'] != '' ) 	? $addressArray['country'].'<br>' : '';
					return $address;
					break;
			case "Austria":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? '<b>'.$addressArray['company'].'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '') 	? $addressArray['fullname'].'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1'].'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3'].'<br>' : '' ;
					$address			.=		($addressArray['resion'] != '' ) 	? $addressArray['resion'].'<br>' : '';
					
					$address			.=		($addressArray['postcode'] != '' ) 	? strtoupper($addressArray['postcode']).' ' : '';
					$address			.=		($addressArray['town'] != '' ) 		? strtoupper($addressArray['town']).'<br>' : '';
					
					$address			.=		($addressArray['country'] != '' ) 	? strtoupper($addressArray['country']).'<br>' : '';
					return $address;
					break;
			default:
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? '<b>'.$addressArray['company'].'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '') 	? $addressArray['fullname'].'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1'].'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3'].'<br>' : '' ;
					$address			.=		($addressArray['town'] != '' ) 		? $addressArray['town'].'<br>' : '';
					$address			.=		($addressArray['resion'] != '' ) 	? $addressArray['resion'].'<br>' : '';
					$address			.=		($addressArray['postcode'] != '' ) 	? $addressArray['postcode'].'<br>' : '';
					$address			.=		($addressArray['country'] != '' ) 	? $addressArray['country'].'<br>' : '';
					return $address;
					break;
			}
			exit;
		}
		
		
		public function getCountryAddress290317( $addressArray )
		{
			
			switch ($addressArray['country']) {
			case "United Kingdom":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<b>'.ucwords($addressArray['company']).'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords(strtolower($addressArray['fullname'])).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords(strtolower($addressArray['address1'])).'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords(strtolower($addressArray['address2'])).'' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ' ,'.ucwords(strtolower($addressArray['address3'])).'<br>' : '' ;
					$address			.=		($addressArray['resion'] != '' )   ? ucwords(strtolower($addressArray['resion'])).'<br>' : '';
					$address			.=		($addressArray['town'] != '' )     ? ' '.strtoupper($addressArray['town']).'<br>' : '';
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).'<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br>' : '';
					return $address;
					break;
			case "Germany":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<b>'.ucwords($addressArray['company']).'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords(strtolower($addressArray['fullname'])).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1']).'<br>' : '' ; 
					
					if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']).'<br>' : '' ;						
					}
					
					if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).'<br>' : '' ;
					}
					
					if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']).'<br>' : '' ;
					}
					
					$address			.=		($addressArray['resion'] != '' )   ? ucwords(strtoupper($addressArray['resion'])).'<br>' : '';
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).' ' : '';
					$address			.=		($addressArray['town'] != '' )     ? strtoupper($addressArray['town']).'<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br>' : '';
					return $address;
					break;
			case "Spain":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<b>'.ucwords($addressArray['company']).'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords($addressArray['fullname']).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1']).'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).'<br>' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords($addressArray['address3']).'<br>' : '' ;
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']) : '';					
					$address			.=		($addressArray['town'] != '' )     ? ' '.strtoupper($addressArray['town']) : '';
					$address			.=		($addressArray['resion'] != '' )   ? '<br> '. strtoupper($addressArray['resion']) : '';					
					$address			.=		($addressArray['postcode'] != '' ) ? '<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? $addressArray['country'].'<br>' : '';
					return $address;
					break;
			case "United States":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? '<div style="font-size:17px;"><b>'.ucwords($addressArray['company']).'</b><br>' : '<div style="font-size:17px;">' ; 
					$address			.=		($addressArray['fullname'] != '')  ? ucwords($addressArray['fullname']).'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1']).'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).'<br>' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']) : '' ;
					$address			.=		($addressArray['town'] != '' )     ? strtoupper($addressArray['town']).' ' : '';
					$address			.=		($addressArray['resion'] != '' )   ? strtoupper($addressArray['resion']).' ' : '';						
					
					$address			.=		($addressArray['postcode'] != '' ) ? strtoupper($addressArray['postcode']).'<br>' : '';
					//$address			.=		($addressArray['postcode'] != '' ) ? '<br>' : '';
					$address			.=		($addressArray['country'] != '' )  ? strtoupper($addressArray['country']).'<br></div>' : '';
					return $address;
					break;
			default:
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? '<b>'.$addressArray['company'].'</b><br>' : '' ; 
					$address			.=		($addressArray['fullname'] != '') 	? $addressArray['fullname'].'<br>' : '' ; 
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1'].'<br>' : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3'].'<br>' : '' ;
					$address			.=		($addressArray['town'] != '' ) 		? $addressArray['town'].'<br>' : '';
					$address			.=		($addressArray['resion'] != '' ) 	? $addressArray['resion'].'<br>' : '';
					$address			.=		($addressArray['postcode'] != '' ) 	? $addressArray['postcode'].'<br>' : '';
					$address			.=		($addressArray['country'] != '' ) 	? $addressArray['country'].'<br>' : '';
					return $address;
					break;
			}
			exit;
		}
	
		
		public function varifyLabelSlip()
		{
			
				$this->loadModel( 'PackagingSlip' );
				$this->loadModel( 'MergeUpdate' );
				$this->loadModel( 'Product' );
				$this->loadMOdel( 'CategoryContant' );
				
				
				//	$splitOrderId =  '1580047-1';//
				$splitOrderId =  trim($this->request->data['orderid']);
				$orderArray = explode( '-', $splitOrderId );
				$id				=	$orderArray[0];
				
				$openOrderId	=	$orderArray[0];
				$splitOrderId	=	$splitOrderId;
		
				$this->layout = '';
				$this->autoRender = false;
				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				
				$dompdf->set_paper(array(0, 0, 245, 450), 'portrait');
				$order	=	$this->getOpenOrderById( $openOrderId );
 				
				$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
				$itemPrice		=	$getSplitOrder['MergeUpdate']['price'];
				$itemQuantity	=	$getSplitOrder['MergeUpdate']['quantity'];
				$BarcodeImage	=	$getSplitOrder['MergeUpdate']['order_barcode_image'];
				
				
				$k = '10';
				$companyname = '';
				$returnaddress = '';
				switch ($k) {
								case 0:
									echo "";
									break;
								case 1:
									echo "";
									break;
								case 2:
									echo "";
									break;
								default:
								   $companyname 	 =  'Jij Group';
								   $returnaddress 	 =  'Address Line 1 <br> Address Line 2 <br>Some Town Some Region 123 ABC <br>United Country';
							}
				
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$subsource			=		$order['sub_source'];
				
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				
				$addData['company'] 	= 	$company;
				$addData['fullname'] 	= 	$fullname;
				$addData['address1'] 	= 	$address1;
				$addData['address2'] 	= 	$address2;
				$addData['address3'] 	= 	$address3;
				$addData['town'] 		= 	$town;
				$addData['resion'] 		= 	$resion;
				$addData['postcode'] 	= 	$postcode;
				$addData['country'] 	= 	$country;
				
				$address 			= 		$this->getCountryAddress( $addData );
				
				/*$address			=		'';
				$address			.=		($company != '') ? $company.'<br>' : '' ; 
				$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
				$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
				$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
				$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
				$address			.=		($town != '') ? $town.'<br>' : '';
				$address			.=		($resion != '') ? $resion.'<br>' : '';
				$address			.=		($postcode != '') ? $postcode.'<br>' : '';
				$address			.=		($country != '' ) ? $country.'<br>' : '';*/
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= $postcode;
				$setRepArray[] 					= $country;
				$setRepArray[] 					= $phone;
				$setRepArray[] 					= $ordernumber;
				$setRepArray[] 					= $courier;
				$setRepArray[] 					= $recivedate[0];
				$i = 1;
				$str = '';
				$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
				$totalGoods = 0;
				$productInstructions = '';
				$ref_code = trim($getSplitOrder['MergeUpdate']['provider_ref_code']);
				$codearray = array('DP1', 'FR7');
				if( in_array($ref_code, $codearray)) 
				{
					//pr($items);
					$per_item_pcost 	= $postagecost/count($items);
					$j = 1;
					foreach($items as $item){
					$item_title 		= 	$item->Title;
					$quantity			=	$item->Quantity;
					$price_per_unit		=	$item->PricePerUnit;
					$str .= '<tr>
								<td style="border:1px solid #000;" align="left" valign="top" width="10%">'.$quantity.'</td>
								<td style="border:1px solid #000;" valign="top" width="20%">'.substr($item_title, 0, 15 ).'</td>
								<td style="border:1px solid #000;" valign="top" width="15%">'.$quantity * $price_per_unit.'</td>
								<td style="border:1px solid #000;" valign="top" width="15%">'.$per_item_pcost.'</td>
								<td style="border:1px solid #000;" valign="top" width="20%">'.(($price_per_unit * $quantity) + $per_item_pcost).'</td>
							</tr>';
					$j++;
					}
					$str .=	'<tr><td></td><td></td><td></td><td style="border:1px solid #000;">Total</td>
								<td style="border:1px solid #000;" valign="top" width="20%">'.$totalcharge.' EUR</td>
							</tr>';
					
				} 
				else
				{
					$c_ink = 0;
					foreach( $skus as $sku )
					{
						$newSkus[]				=	 explode( 'XS-', $sku);
						foreach($newSkus as $newSku)
							{
								
								$getsku = 'S-'.$newSku[1];
								$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
								$contentCat 	= 	$getOrderDetail['Product']['category_name'];
								$contentSubCat	=	$getOrderDetail['Product']['sub_category'];
								$productBarcode	=	$getOrderDetail['ProductDesc']['barcode'];
								
								/*$check_ink		=	$this->InkSku->find( 'first', array( 'conditions' => array( 'sku' => $getsku ) ) );
								if( count($check_ink) > 0 ){
									$c_ink++;
								}*/
								if($contentCat == 'Mobile Accessories' || $contentCat == 'Electronics'){
								$getContent	= $this->CategoryContant->find( 'first', array( 
																	'conditions'=>array( 'CategoryContant.sub_category'=>$contentSubCat,'CategoryContant.sub_source'=>$subsource) ) );
								
								if(count($getContent) > 0) {
									$productBarcode			=	$getOrderDetail['ProductDesc']['barcode'];
									$getbarcode				= 	$this->getBarcode( $productBarcode );
									$productInstructions 	=  '<p style="font-size:14px;">'.$getContent['CategoryContant']['content'].'</p>';
								}}
								$title	=	$getOrderDetail['Product']['product_name'];
								$totalGoods = $totalGoods + $newSku[0];
								$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
										<td valign="top" class="rightborder bottomborder">'.substr($title, 0, 15 ).'</td>
										<td valign="top" class="center norightborder bottomborder">'.$newSku[0].'</td>';
								$str .= '</tr>';
								$i++;
							}
							unset($newSkus);
					}
				}
					
				/**********************************************/
				$countryArray = Configure::read('customCountry');
				if($subsource == 'Marec_FR' || $subsource == 'Marec_DE' || $subsource == 'Marec_IT' || $subsource == 'Marec_ES' || $subsource == 'Marec_UK'){
					$company 	=  'Marec';
				} else if($subsource == 'CostBreaker_UK' || $subsource == 'CostBreaker_DE' || $subsource == 'CostBreaker_FR' || $subsource == 'CostBreaker_ES' || $subsource == 'CostBreaker_IT' || $subsource == 'Onbuy' || $subsource == 'Fyndiq_Costdropper' || $subsource == 'Flubit'){
					$company 	=  'CostBreaker';
				} else if($subsource == 'Tech_Drive_UK' || $subsource == 'Tech_Drive_FR' || $subsource == 'Tech_drive_ES' || $subsource == 'Tech_drive_DE' || $subsource == 'Tech_Drive_IT' ){
					$company 	= 'Tech Drive Supplies';
				} else if($subsource == 'RAINBOW RETAIL DE' || $subsource == 'Rainbow Retail' || $subsource == 'Rainbow_Retail_ES' || $subsource == 'Rainbow_Retail_IT'){
					$company 	= 'Rainbow Retail';
				}
				
				$ref_code = trim($getSplitOrder['MergeUpdate']['provider_ref_code']);
				$codearray = array('DP1', 'FR7');
				
				if( ($country == 'Germany' || $country == 'France') && in_array($ref_code, $codearray))
				{
					
					$gethtml =	$this->PackagingSlip->find('first', array(
											'conditions' => array( 
													'PackagingSlip.name' => 'deutsche_post',
													'PackagingSlip.company_name' => $company,
													'PackagingSlip.custom_slip_status' => 1
													)
												)
											);
				} 
				if( ($subsource == 'Onbuy'))
				{
					$gethtml =	$this->PackagingSlip->find('first', array(
											'conditions' => array( 
													'PackagingSlip.name' => 'deutsche_post',
													'PackagingSlip.name' => $subsource
													)));
				} 
				else if( ($subsource == 'Flubit'))
				{
					$gethtml =	$this->PackagingSlip->find('first', array(
											'conditions' => array( 
													'PackagingSlip.name' => $subsource,
													)));
				} 
				else 
				{
					
					if($country == 'Germany')
					{
						$getSourceName = $this->getCompanyNameBySubsource( $subsource );
						
							$gethtml =	$this->PackagingSlip->find('first', array(
													'conditions' => array( 
															'PackagingSlip.company_name' => $getSourceName,
															'PackagingSlip.custom_slip_status' => 1
															)
														)
													);
					}
					else
					{
						$gethtml =	$this->PackagingSlip->find('first', array(
												'conditions' => array( 
														'PackagingSlip.store_name' => $subsource,
														'PackagingSlip.custom_slip_status' => 0
														)
													)
												);
					
					}
				}
				
				/*$gethtml =	$this->PackagingSlip->find('first', array(
											'conditions' => array( 
													'PackagingSlip.store_name' => $subsource,
													)
												)
											);*/
				
				$html 			=	$gethtml['PackagingSlip']['html'];
				$paperHeight    =   $gethtml['PackagingSlip']['paper_height'];
				$paperWidth  	=   $gethtml['PackagingSlip']['paper_width'];
				$barcodeHeight  =   $gethtml['PackagingSlip']['barcode_height'];
				$barcodeWidth   =   $gethtml['PackagingSlip']['barcode_width'];
				$paperMode      =   $gethtml['PackagingSlip']['paper_mode'];
				/**********************************************/
				
				$dompdf->set_paper(array(0, 0, $paperWidth, $paperHeight ), $paperMode);
				
				$totalitem = $i - 1;
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totalGoods;
				$setRepArray[]	=	 $subtotal;
				$Path 			= 	'/wms/img/client/';
				$img			=	 '<img src=http://localhost/wms/img/client/demo.png height="50" width="50">';
				$setRepArray[]	=	 $img;
				$setRepArray[]	=	 $postagecost;
				$setRepArray[]	=	 $tax;
				$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
				$setRepArray[]	=	 $itemPrice;
				$setRepArray[]	=	 $address;
				$barcodePath  	=  Router::url('/', true).'img/orders/barcode/';
				$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.' width='. $barcodeWidth .'>';
				$barcodenum		=	explode('.', $barcode);
				
				$setRepArray[] 	=  $barcodeimg;
				$setRepArray[] 	=  $paymentmethod;
				$setRepArray[] 	=  $barcodenum[0];
				$setRepArray[] 	=  '';
				
				
				
				$img 			=	$gethtml['PackagingSlip']['image_name'];
				$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
				$setRepArray[] 	=  $returnaddress;
				$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
				$setRepArray[] 	= $splitOrderId;
				$setRepArray[] 	= utf8_decode( $productInstructions );
				$prodBarcodePath  	=  Router::url('/', true).'img/product/barcodes/';
				$setRepArray[] 	=  '<img src='.$prodBarcodePath.$productBarcode.'.png width='. $barcodeWidth .'>';
				$setRepArray[] 	=  $totalamount;
				
				/*--------------Ink------------------*/

				$this->loadMOdel( 'InkOrderLocation' );
				$ink_envelope   = 0;
				$order_ink	    = $this->InkOrderLocation->find('all', array('conditions' => array('order_id' => $openOrderId)));
				
				if(count($order_ink) > 0 ){
					foreach($order_ink as $_ink){
					 	$ink_envelope = $ink_envelope + $_ink['InkOrderLocation']['quantity'];					 
					}
					$setRepArray[] 	=  '<div style="color:green;font-size:14px;"><center>#'.$ink_envelope.' Envelope Required.</center></div><br><img src="'.Router::url('/', true).'img/ink_xl2.jpg" height="130">';
				}
				/*--------------End of Ink------------------*/
 
				App::Import('Controller', 'NewInvoice'); 
				$newIn = new NewInvoiceController;
				$html = $newIn->getThermalInvoice($openOrderId);
				
				$imgPath = WWW_ROOT .'css/';
				
				$html2 = '<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				<meta content="" name="description"/>
				<meta content="" name="author"/><body>'.$html;
				if( $productInstructions != '' )
				{
					$html2 .= '<div style="page-break-before: always;"></div> _PRODUCTINSTRUCTION_';
				} else {
					$html2 .= '</div>';
				}
				'</body>';
				
				$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
				
			//	$html 		= 	$this->setReplaceValue( $setRepArray, $html2 );
				//echo $html;
				
				$dompdf->load_html(utf8_decode($html2), Configure::read('App.encoding'));
				$dompdf->render();
			
				$imgPath = WWW_ROOT .'img/printPDF/'; 
				$path = Router::url('/', true).'img/printPDF/';
				$date = new DateTime();
				$timestamp = $date->getTimestamp();
				$name	=	'Packagin_Slip_'.$splitOrderId.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				echo $serverPath  	= 	$path.$name;
				exit; 
		}
		

		public function getCompanyNameBySubsource( $subSource )
		{
			$this->loadModel( 'PackagingSlip' );
			$getCompanyName = $this->PackagingSlip->find('first', array('conditions' => array('PackagingSlip.store_name' => $subSource ) ) );
			if(!empty($getCompanyName))
			{
				return $getCompanyName['PackagingSlip']['company_name'];
			}
			
		}
		
		public function getCorreosBarcode( $postcode = null, $splitOrderID = null )
		{
			
			
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 

			$barcodePath 		= 	WWW_ROOT .'img/orders/barcode/';   
			$barcodecorreosPath = 	WWW_ROOT .'img/orders/correos_barcode/';  
			
			$colorFront 		= 	new BCGColor(0, 0, 0);
			$colorBack 			= 	new BCGColor(255, 255, 255);
			
			// Barcode Part
			$orderbarcode=$splitOrderID;
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->clearLabels();
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->parse($orderbarcode);
			// Drawing Part
			$imgOrder128=$orderbarcode;
			$imgOrder128path=$barcodePath.'/'.$imgOrder128.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			############ Routing Barcode #######
			//$destination = $countryCode;
			//$origin ="90019";
			//$routingcode=$destination.$origin;
			
			 
			 
			$routingcode = $this->getCorreosCalculatedBarcode($postcode);
			$routingcode128 = new BCGcode128();
			$routingcode128->setScale(2);
			$routingcode128->setThickness(40);
			$routingcode128->setForegroundColor($colorFront);
			$routingcode128->setBackgroundColor($colorBack);
			$routingcode128->parse(array(CODE128_B, $routingcode));
			//$routingcode128->parse($routingcode);
			// Drawing Part
			$routingimg =$routingcode;
			$routingdrawing128path=$barcodecorreosPath.'/'.$routingimg.".png";
			$routingdrawing128 = new BCGDrawing($routingdrawing128path, $colorBack);
			$routingdrawing128->setBarcode($routingcode128);
			$routingdrawing128->draw();
			$routingdrawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			return $routingcode;
		}	
		
		
		public function getCorriosBarcode_160418( $countryCode = null, $splitOrderID = null )
		{
		
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 

			$barcodePath 		= 	WWW_ROOT .'img/orders/barcode/';   
			$barcodecorreosPath = 	WWW_ROOT .'img/orders/correos_barcode/';  
			
			$colorFront 		= 	new BCGColor(0, 0, 0);
			$colorBack 			= 	new BCGColor(255, 255, 255);
			
			// Barcode Part
			$orderbarcode=$splitOrderID;
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->clearLabels();
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->parse($orderbarcode);
			// Drawing Part
			$imgOrder128=$orderbarcode;
			$imgOrder128path=$barcodePath.'/'.$imgOrder128.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			############ Routing Barcode #######
			$destination = $countryCode;
			$origin ="90019";
			$routingcode=$destination.$origin;
			$routingcode128 = new BCGcode128();
			$routingcode128->setScale(2);
			$routingcode128->setThickness(40);
			$routingcode128->setForegroundColor($colorFront);
			$routingcode128->setBackgroundColor($colorBack);
			$routingcode128->parse(array(CODE128_C, $routingcode));
			//$routingcode128->parse($routingcode);
			// Drawing Part
			$routingimg =$routingcode;
			$routingdrawing128path=$barcodecorreosPath.'/'.$routingimg.".png";
			$routingdrawing128 = new BCGDrawing($routingdrawing128path, $colorBack);
			$routingdrawing128->setBarcode($routingcode128);
			$routingdrawing128->draw();
			$routingdrawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
		}	
		
		public function getBarcode( $barcode = null )
		{
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 

			$barcodePath 		= 	WWW_ROOT .'img/product/barcodes/';   
			$barcodecorreosPath = 	WWW_ROOT .'img/orders/correos_barcode/';  
			
			$colorFront 		= 	new BCGColor(0, 0, 0);
			$colorBack 			= 	new BCGColor(255, 255, 255);
			
			// Barcode Part
			$orderbarcode=$barcode;
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->clearLabels( true );
			$code128->SetLabel(false);
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->parse($orderbarcode);
			// Drawing Part
			$imgOrder128=$orderbarcode;
			$imgOrder128path=$barcodePath.'/'.$imgOrder128.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			############ Routing Barcode #######
			
			$routingcode = $this->getCorreosCalculatedBarcode($postcode);
			$routingcode128 = new BCGcode128();
			$routingcode128->setScale(2);
			$routingcode128->setThickness(40);
			$routingcode128->setForegroundColor($colorFront);
			$routingcode128->setBackgroundColor($colorBack);
			$routingcode128->parse(array(CODE128_B, $routingcode));
			//$routingcode128->parse($routingcode);
			// Drawing Part
			$routingimg =$routingcode;
			$routingdrawing128path=$barcodecorreosPath.'/'.$routingimg.".png";
			$routingdrawing128 = new BCGDrawing($routingdrawing128path, $colorBack);
			$routingdrawing128->setBarcode($routingcode128);
			$routingdrawing128->draw();
			$routingdrawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			return $routingcode;
			
		}	
		
		public function getCorreosCalculatedBarcode($postcode = null)
		 {
			$sum_accii = 0;
			$code = 'ORDCP'.$postcode; 		
			for($i=0; $i < strlen($code); $i++){
			  $sum_accii += ord($code[$i]);		 
			}
			$remainder = fmod($sum_accii,23);
			$table = array('T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E');
			$control_character = $table[$remainder];
			return	$code.$control_character;			
		} 
	
	 
}
?>
