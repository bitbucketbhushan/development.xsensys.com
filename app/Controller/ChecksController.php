<?php

class ChecksController extends AppController
{
    
    var $name = "Checks";
    var $components = array('Session','Upload','Common','Auth');
    var $helpers = array('Html','Form','Common','Session');
    
     public function beforeFilter()
		{
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('saveStockFeed','creatFeedSheet'));
		}
   
	public function virtualStockDetail()
	{
		$this->layout = 'index';
		
		$this->loadModel( 'Product' );
		$this->Product->unBindModel(array('hasMany'=>('ProductLocation')));
		
		$this->paginate = array( 
					'fields'=>array('Product.product_sku','Product.current_stock_level' ),
					'order' => array('Product.current_stock_level DESC'),
					'limit' => 100			
		   );
		
	
								
		$getSkus	=	$this->paginate('Product');
		$results	=	json_decode(json_encode($getSkus),0);
		$this->set('results', $results);
		//$this->render( 'getSkuResult' );
	}
	
	public function getDYMO()
	{
		$this->layout = '';
		$this->autoRander = false;
		
		$this->loadModel( 'PcStore' );						
		
		//get user
		$pcName = $this->Session->read('Auth.User.pc_name');
		
		$param = array(
		
			'conditions' => array(
			
				'PcStore.pc_name' => $pcName,
				'PcStore.printer_name' => 'DYMO LabelWriter 450'
			
			),
			'fields' => array(
			
				'PcStore.printer_id'
			
			)
		
		);
		
		$printer = json_decode(json_encode($this->PcStore->find( 'first' , $param )),0);
		$zDesignerPrinter = $printer->PcStore->printer_id;
	
	return $zDesignerPrinter;	
	}
	
	public function checkBarcode()
	{
			$this->layout = '';
			$this->autoRender = false;
			
			$this->loadModel( 'Product' );
			$barcodeNumber	=	$this->request->data['barcodeNumber'];
		
			//$dt =  date("Ymd") . $barcodeNumber . time();
			$params = array( 'conditions' => array( 
								 'OR' => array(
											array('ProductDesc.barcode' => $barcodeNumber),
											array('ProductDesc.qr_code' => $barcodeNumber),
											)
										)
									);
			
			$getBarcode	=	$this->Product->find( 'first', $params);
			
			if( count( $getBarcode ) > 0 )
			{
				echo "1##$##".$getBarcode['ProductDesc']['barcode'];
				exit;
			}
			else
			{
				echo "2";
				exit;
			}
		
	}
	
	
	public function generateBarcode()
	{
			$this->layout = '';
			$this->autoRender = false;
			
			$this->loadModel( 'Barcode' );
			$this->loadModel( 'Product' );
			$this->loadModel( 'ProductDesc' );
			
			$barcodeNumber			=	$this->request->data['barcodeNumber'];
			$generationQuantity		=	$this->request->data['quantity'];
			$productDescID			=	$this->request->data['productdescId'];
			
			require_once(APP . 'Vendor' . DS . 'src' . DS . 'BarcodeGenerator.php'); 
			require_once(APP . 'Vendor' . DS . 'src' . DS . 'BarcodeGeneratorPNG.php'); 
			require_once(APP . 'Vendor' . DS . 'src' . DS . 'BarcodeGeneratorSVG.php'); 
			require_once(APP . 'Vendor' . DS . 'src' . DS . 'BarcodeGeneratorHTML.php'); 
			
			$generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
			
			$imgPath = WWW_ROOT .'img/GeneratedBarcode/'; 
			$path = Router::url('/', true).'img/GeneratedBarcode/';
			$name = $barcodeNumber.'.png';
			
			$data	=	 'data:image/png;base64,' . base64_encode($generator->getBarcode($barcodeNumber, $generator::TYPE_CODE_128));
			$serverPath  	= 	$path.$barcodeNumber.'.png' ;
			$file = $imgPath.$barcodeNumber.'.png';
			$pdfName = $barcodeNumber;
			
			$data = str_replace('data:image/png;base64,', '', $data);
			$data = str_replace(' ', '+', $data);
			$data = base64_decode($data);
			
			$success = file_put_contents($file, $data);
			
			/*  generate pdf  for barcode */
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();
			$dompdf->set_paper(array(0, 0, 188, 71), 'portrait');
			$cssPath = WWW_ROOT .'css/';
			
				$html = '<meta charset="utf-8">
								 <meta http-equiv="X-UA-Compatible" content="IE=edge">
								 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
								 <meta content="" name="description"/>
								 <meta content="" name="author"/><body>
									<div id="label">
									   <div class="container">
										  <table style="padding:5px 0;">
											 <tr>
												<td valign="top" width="100%" style="font-size:16px; text-align:center;" >
													<img  src="'.$serverPath.'" width="150px" height="37px">
													'.$barcodeNumber.'
												</td>
											 </tr>

											 
										  </table>
									   </div>
									</div></body>';
					
				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
			
				//echo $html;
				//exit;
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				$dompdf->stream($pdfName.'.pdf');
				$imgPath1 = WWW_ROOT .'img/GeneratedBarcode/'; 
				$path1 = Router::url('/', true).'img/GeneratedBarcode/';
				$date = new DateTime();
				$timestamp = $date->getTimestamp();
				$name	=	$pdfName.'.pdf';
							
				file_put_contents($imgPath1.$name, $dompdf->output());
				$serverPath  	= 	$path1.$name ;
				$printerId		=	$this->getDYMO();
				
				$sendData = array(
					'printerId' =>$printerId,
					'title' => 'Now Print',
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'
				);
			for( $i = 1; $i <= $generationQuantity; $i++ )
			{
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				$d = $Coreprinter->toPrintDymo( $sendData );
			}
			
			echo "1";
			exit;
			
	}
	
	
	public function generateBarcode_old_060416()
	{
			$this->layout = '';
			$this->autoRender = false;
			$barcodeNumber	=	$this->request->data['barcodeNumber'];
			
			require_once(APP . 'Vendor' . DS . 'src' . DS . 'BarcodeGenerator.php'); 
			require_once(APP . 'Vendor' . DS . 'src' . DS . 'BarcodeGeneratorPNG.php'); 
			require_once(APP . 'Vendor' . DS . 'src' . DS . 'BarcodeGeneratorSVG.php'); 
			require_once(APP . 'Vendor' . DS . 'src' . DS . 'BarcodeGeneratorHTML.php'); 
			
			$generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
			
			$imgPath = WWW_ROOT .'img/GeneratedBarcode/'; 
			$path = Router::url('/', true).'img/GeneratedBarcode/';
			$name = $barcodeNumber.'.png';
			
			$data	=	 'data:image/png;base64,' . base64_encode($generator->getBarcode($barcodeNumber, $generator::TYPE_CODE_128));
			$data = str_replace('data:image/png;base64,', '', $data);
			$data = str_replace(' ', '+', $data);
			$data = base64_decode($data);
			$file = $imgPath.'generate.png';
			$success = file_put_contents($file, $data);
			
			/*  generate pdf  for barcode */
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();
			$dompdf->set_paper(array(0, 0, 204, 94), 'portrait');
			$cssPath = WWW_ROOT .'css/';
			$serverPath  	= 	$path.'generate.png' ;
			$html = '<meta charset="utf-8">
							 <meta http-equiv="X-UA-Compatible" content="IE=edge">
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/><body>
								<div id="label">
								   <div class="container">
									  <table style="padding:10px 0;">
										 <tr>
										 <td valign="top" width="50%" style="font-size:16px" >
										 <img style="margin-left:20px" src="'.$serverPath.'" width="200px">
										</td>
										 </tr>
									  </table>
								   </div>
								</div></body>';
				
			$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
			//echo $html;
			//exit;
			$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
			$dompdf->render();
			$dompdf->stream("OrderIdslip-$splitOrderId.pdf");
			$imgPath1 = WWW_ROOT .'img/GeneratedBarcode/'; 
			$path1 = Router::url('/', true).'img/GeneratedBarcode/';
			$date = new DateTime();
			$timestamp = $date->getTimestamp();
			$name	=	'Barcode.pdf';
			
			file_put_contents($imgPath1.$name, $dompdf->output());
			$serverPath  	= 	$path1.$name ;
			$printerId		=	$this->getDYMO();
			
			$sendData = array(
				'printerId' => $printerId,
				'title' => 'Now Print',
				'contentType' => 'pdf_uri',
				'content' => $serverPath,
				'source' => 'Direct'
			);
					
			App::import( 'Controller' , 'Coreprinters' );
			$Coreprinter = new CoreprintersController();
			$d = $Coreprinter->toPrint( $sendData );
			pr($d); exit;
		
		
		
	}
	
	public function barcodeCode()
	{
		$this->layout = 'index';
	}
	
	public function getBarcodePopup()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'Product' );
		
		$productId	=	$this->request->data['productId'];
		$productBarcode	=	$this->Product->find( 'first', array( 
								'conditions' => array( 'Product.id' =>  $productId),
								'fields' => array( 'Product.id, ProductDesc.barcode, ProductDesc.id' )
								) );
		$this->set( 'productBarcode', $productBarcode );
		$this->render( 'barcodePopup' );
		
	}
	
	public function getqrcodePopup()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'Product' );
		$productId	=	$this->request->data['productId'];
		
		$productBarcode	=	$this->Product->find( 'first', array( 
								'conditions' => array( 'Product.id' =>  $productId),
								'fields' => array( 'Product.id, ProductDesc.barcode, ProductDesc.qr_code, ProductDesc.id' )
								) );
		$this->set( 'productBarcode', $productBarcode );
		$this->render( 'qrcodePopup' );
		
	}
	
	public function setQrcode()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'ProductDesc' );
		
		$data['ProductDesc']['id'] 				= 	$this->request->data['productId'];
		$data['ProductDesc']['qr_code']			=	$this->request->data['qrcode'];
		
		if($this->ProductDesc->saveAll( $data, array('validate' => false) ) )
		{
			echo "1";
			exit;
		}
		else
		{
			echo "2";
			exit;
		}
		
	}
	
	public function setForSlipAndLabel()
	{
		$this->layout = 'index';
			
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		
		$params	=	array(
							'joins' => array(
									array(
										'table' => 'merge_updates',
										'alias' => 'MergeUpdate',
										'type' => 'INNER',
										'conditions' => array(
											'OpenOrder.num_order_id = MergeUpdate.order_id'
											)
										)
									),
							'conditions' => array( 
										'OpenOrder.date >' => '2016-05-07 00:00:00',
										'OpenOrder.date <=' => '2016-05-09 00:00:00',
										
									),
							'fields' => array( 
										'MergeUpdate.*','OpenOrder.sub_source','OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date'
									)
							);
		
		
			
		$getProdessOrderDetails	=	$this->OpenOrder->find('all', $params );
		$this->set( 'getOrderResult', $getProdessOrderDetails );
	}
	/************Start For Po upload and show detail************/
	
	public function uploadPoView() 
	{
		$this->layout = 'index';

	}
	
	public function UploadsCsvFilesForPo_old300616()
	{
		$this->autoLayout = 'ajax';
		$this->autoRender = false;
		
		$this->loadModel('PurchaseOrder');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Po');
		
		if($this->data['Po']['Import_file']['name'] != '')
		{
				
			$filename = WWW_ROOT. 'files'.DS.$this->request->data['Po']['Import_file']['name']; 
			move_uploaded_file($this->request->data['Po']['Import_file']['tmp_name'],$filename);  
			$name		=	 $this->request->data['Po']['Import_file']['name'];
			
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/'.$name);
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			$nameNew = explode( '.',  $name );
			
			$count=0;
			$poDetail	=	$this->PurchaseOrder->find( 'first', array( 'conditions' => array( 'PurchaseOrder.po_name' => $nameNew[0]) ) );
			
			
			if( count( $poDetail ) > 0 )
			{
				$this->Session->setFlash('PO already exists.', 'flash_danger');
				$this->redirect($this->referer());
			}
			else
			{
				$poData['Po']['po_name'] = $nameNew[0];
				$this->Po->saveAll( $poData );
			}
						 
			for($i=2;$i<=$lastRow;$i++) 
			{
				
				$sku = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
				
				$paramSku = array(
				
					'conditions' => array(
						
						'Product.product_sku' => $sku
						
					),
					'fields' => array(
					
						'ProductDesc.barcode'
					
					)
				
				);
				
				$setResult = json_decode(json_encode($this->Product->find( 'first' , $paramSku )),0);
				//$barcodeStatusValue = $setResult->ProductDesc->barcode;
				if( isset($setResult->ProductDesc->barcode ))
				{
					$barcodeStatusValue = $setResult->ProductDesc->barcode;
				}
				
				$data['PurchaseOrder']['po_name']					=	$nameNew[0];
				$data['PurchaseOrder']['purchase_qty']				=	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$data['PurchaseOrder']['purchase_description']		=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$data['PurchaseOrder']['purchase_code'] 			=	$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
				$data['PurchaseOrder']['purchase_sku'] 				=	$objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
				$data['PurchaseOrder']['purchase_barcode'] 			=	$barcodeStatusValue;
				
				$this->PurchaseOrder->saveAll( $data );
				$count++;
				$this->Session->setFlash($count.' :- SKU Inserted', 'flash_danger');
			}
			$this->redirect($this->referer());
		}
		else
		{
			$this->Session->setFlash('Please Insert CSV File.', 'flash_danger');
			$this->redirect($this->referer());
		}
	}
	
	public function UploadsCsvFilesForPo()
	{
		$this->autoLayout = 'ajax';
		$this->autoRender = false;
		
		$this->loadModel('PurchaseOrder');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Po');
		
		if($this->data['Po']['Import_file']['name'] != '')
		{
			//$invoiceNo	=	$this->request->data['po']['invoice'];
			$filename = WWW_ROOT. 'files'.DS.$this->request->data['Po']['Import_file']['name']; 
			move_uploaded_file($this->request->data['Po']['Import_file']['tmp_name'],$filename);  
			$name			=	 $this->request->data['Po']['Import_file']['name'];
			$nameExplode 	= 	 explode('.', $name);
			$nameNew 		= 	 end( $nameExplode );
			
			if( $nameNew == 'xlsx' || $nameNew == 'xls' )
			{
				$fileType = 'Excel2007';
			}
			else if( $nameNew == 'csv' )
			{
				$fileType = 'CSV';
			}
			else
			{
				$this->Session->setFlash('Please upload valid file( XLSX, XLS, CSV ).', 'flash_danger');
				$this->redirect($this->referer());	
			}
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			
			$objReader= PHPExcel_IOFactory::createReader($fileType);
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/'.$name);
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			$exten = array('.xlsx','.xls','.csv');
			$nameNew = str_replace($exten,'', $name);
			
			
			$count = 0;
			$duplicateCount	=	0;
			$dupSku 		= 	array();
			$caheckSku 		= 	array();
			$notInDb 		= 	0;
			
			$poDetail	=	$this->Po->find('first', array(								
								'conditions' => array('Po.po_name' => $nameNew)
										)
									);
			
			if( count( $poDetail ) > 0 )
			{
				$this->Session->setFlash('PO should be unique.', 'flash_danger');
				$this->redirect($this->referer());
			}
			else
			{
				$poData['Po']['po_name'] = $nameNew;
				$this->Po->saveAll( $poData );
				$poid	=	$this->Po->getLastInsertId();
			}
						 
			for($i=2;$i<=$lastRow;$i++) 
			{
				$sku = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
				
				$paramSku = array(
				
					'conditions' => array(
						
						'Product.product_sku' => $sku
						
					),
					'fields' => array(
					
						'ProductDesc.barcode'
					
					)
				
				);
				
				$setResult = json_decode(json_encode($this->Product->find( 'first' , $paramSku )),0);
				if( count( $setResult ) == 1 )
				{
						if( isset($setResult->ProductDesc->barcode ))
						{
							$barcodeStatusValue = $setResult->ProductDesc->barcode;
						}
						//1.Description, 2.CODE, 3.OUR REF, 4.BARCODE, 5.Qty
						$getSkuDetail =	$this->PurchaseOrder->find( 'first', array( 
																'conditions' => array( 
																			'PurchaseOrder.po_name' => $nameNew, 
																			'PurchaseOrder.purchase_sku' => $objWorksheet->getCellByColumnAndRow(2,$i)->getValue() 
																) 
															  )
															);
						if( count( $getSkuDetail ) != 1 )
						{
							$data['PurchaseOrder']['po_id']						=	$poid;
							$data['PurchaseOrder']['po_name']					=	$nameNew;
							$data['PurchaseOrder']['purchase_qty']				=	$objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
							$data['PurchaseOrder']['purchase_description']		=	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
							$data['PurchaseOrder']['purchase_code'] 			=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
							$data['PurchaseOrder']['purchase_sku'] 				=	$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
							$data['PurchaseOrder']['purchase_barcode'] 			=	$barcodeStatusValue;
							$this->PurchaseOrder->saveAll( $data );
							$count++;
						}
						else
						{
							$duplicateCount++;
							$dupSku[] = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
						}
					}
					else
					{
						$notInDb++;
						$caheckSku[] = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
					}
					$this->Session->setFlash($count.' :- SKU Inserted<br>'.$duplicateCount.' :- SKU Duplicate '.implode(',', $dupSku).'<br>'.$notInDb.' :- SKU Not in DB :-'.implode(',',$caheckSku).'', 'flash_danger');
			}
			$this->redirect($this->referer());
		}
		else
		{
			$this->Session->setFlash('Please Insert CSV File.', 'flash_danger');
			$this->redirect($this->referer());
		}
	}
	
	/*public function UploadsCsvFilesForPo_200716()
	{
		$this->autoLayout = 'ajax';
		$this->autoRender = false;
		
		$this->loadModel('PurchaseOrder');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Po');
		
		if($this->data['Po']['Import_file']['name'] != '')
		{
				
			$filename = WWW_ROOT. 'files'.DS.$this->request->data['Po']['Import_file']['name']; 
			move_uploaded_file($this->request->data['Po']['Import_file']['tmp_name'],$filename);  
			$name			=	 $this->request->data['Po']['Import_file']['name'];
			$nameExplode 	= 	 explode('.', $name);
			$nameNew 		= 	 end( $nameExplode );
			
			if( $nameNew == 'xlsx' || $nameNew == 'xls' )
			{
				$fileType = 'Excel2007';
			}
			else if( $nameNew == 'csv' )
			{
				$fileType = 'CSV';
			}
			else
			{
				$this->Session->setFlash('Please upload valid file( XLSX, XLS, CSV ).', 'flash_danger');
				$this->redirect($this->referer());	
			}
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			
			$objReader= PHPExcel_IOFactory::createReader($fileType);
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/'.$name);
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			$exten = array('.xlsx','.xls','.csv');
			$nameNew = str_replace($exten,'', $name);
			
			
			$count = 0;
			$duplicateCount	=	0;
			$dupSku 		= 	array();
			$caheckSku 		= 	array();
			$notInDb 		= 	0;
			$poDetail		=	$this->PurchaseOrder->find( 'first', array( 'conditions' => array( 'PurchaseOrder.po_name' => $nameNew) ) );
			
			if( count( $poDetail ) > 0 )
			{
				$this->Session->setFlash('PO already exists.', 'flash_danger');
				$this->redirect($this->referer());
			}
			else
			{
				$poData['Po']['po_name'] = $nameNew;
				$this->Po->saveAll( $poData );
			}
						 
			for($i=2;$i<=$lastRow;$i++) 
			{
				$sku = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
				
				$paramSku = array(
				
					'conditions' => array(
						
						'Product.product_sku' => $sku
						
					),
					'fields' => array(
					
						'ProductDesc.barcode'
					
					)
				
				);
				
				$setResult = json_decode(json_encode($this->Product->find( 'first' , $paramSku )),0);
				if( count( $setResult ) == 1 )
				{
						if( isset($setResult->ProductDesc->barcode ))
						{
							$barcodeStatusValue = $setResult->ProductDesc->barcode;
						}
						//1.Description, 2.CODE, 3.OUR REF, 4.BARCODE, 5.Qty
						$getSkuDetail =	$this->PurchaseOrder->find( 'first', array( 
																'conditions' => array( 
																			'PurchaseOrder.po_name' => $nameNew, 
																			'PurchaseOrder.purchase_sku' => $objWorksheet->getCellByColumnAndRow(2,$i)->getValue() 
																) 
															  )
															);
						if( count( $getSkuDetail ) != 1 )
						{
							$data['PurchaseOrder']['po_name']					=	$nameNew;
							$data['PurchaseOrder']['purchase_qty']				=	$objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
							$data['PurchaseOrder']['purchase_description']		=	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
							$data['PurchaseOrder']['purchase_code'] 			=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
							$data['PurchaseOrder']['purchase_sku'] 				=	$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
							$data['PurchaseOrder']['purchase_barcode'] 			=	$barcodeStatusValue;
							$this->PurchaseOrder->saveAll( $data );
							$count++;
						}
						else
						{
							$duplicateCount++;
							$dupSku[] = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
						}
					}
					else
					{
						$notInDb++;
						$caheckSku[] = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
					}
					$this->Session->setFlash($count.' :- SKU Inserted<br>'.$duplicateCount.' :- SKU Duplicate '.implode(',', $dupSku).'<br>'.$notInDb.' :- SKU Not in DB :-'.implode(',',$caheckSku).'', 'flash_danger');
			}
			$this->redirect($this->referer());
		}
		else
		{
			$this->Session->setFlash('Please Insert CSV File.', 'flash_danger');
			$this->redirect($this->referer());
		}
	}*/
	
	
	public function showAllPo()
	{
		$this->layout = 'index';
		$this->loadModel( 'Po' );
		$getAllpo	=	$this->Po->find( 'all', array( 'conditions' => array( 'Po.status' => 1 ) ) );
		
		foreach( $getAllpo as $index => $value )
		{
			if( $value["Po"]["invoice_no"] != '')
			{
				$getpoList[$value["Po"]["id"]] = $value["Po"]["po_name"]." ( ".$value["Po"]["invoice_no"]." )";
			}
			else
			{
				$getpoList[$value["Po"]["id"]] = $value["Po"]["po_name"];
			}
		}
		$this->set( 'getpoList', $getpoList );
	}
	
	/*public function showAllPo()
	{
		$this->layout = 'index';
		$this->loadModel( 'Po' );
		//$getpoList	=	$this->Po->find( 'list', array( 'fields'=> array( 'Po.id','Po.po_name' ) ) );
		//$this->set( 'getpoList', $getpoList );
		$getpoList	=	$this->Po->find( 'list', array( 'conditions' => array( 'Po.status' => 1 ),'fields'=> array( 'Po.id','Po.po_name' ) ) );
		$this->set( 'getpoList', $getpoList );
	}*/
	
	public function getSelectedPo()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('PurchaseOrder');
		$this->loadModel('PoComment');
		$this->loadModel('ScanInvoice');
		$this->loadModel('Po');
		$poName	=	addslashes($this->request->data['poName']);
		$poName	=	explode( '(', $poName );
		
		$getPodetail 		=	$this->PurchaseOrder->find( 'all' , array('conditions' => array( 'PurchaseOrder.po_name' => $poName) ) );
		$getPo 		 		=  $this->Po->find( 'first', array( 'conditions' => array( 'po_name' => $poName[0] ), 'fields' => array( 'Po.id', 'Po.po_name', 'Po.status','Po.invoice_no' ) ) );
		$getComment  		=  $this->PoComment->find( 'all' );
		$PoId	=	$getPo['Po']['id'];
		$PoName	=	$getPo['Po']['po_name'];
		$scanInvoiceData  	=  $this->ScanInvoice->find( 'all', array( 'conditions' => array( 'ScanInvoice.po_id' =>  $PoId) ) );
		
		$this->set( 'scanInvoiceData', $scanInvoiceData);
		$this->set( 'getComment', $getComment);
		$this->set( 'getPodetail',  $getPodetail );
		$this->set( 'getPo',  $getPo );
		
		$this->render( 'getPoResult' );
	
	}
	
	
	
	/*public function getSelectedPo()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('PurchaseOrder');
		$this->loadModel('Po');
		$poName	=	$this->request->data['poName'];
		
		$getPodetail =	$this->PurchaseOrder->find( 'all' , array('conditions' => array( 'PurchaseOrder.po_name' => $poName) ) );
		$getPo = $this->Po->find( 'first', array( 'conditions' => array( 'po_name' => $poName ), 'fields' => array( 'Po.id', 'Po.po_name', 'Po.status' ) ) );
	
		$this->set( 'getPodetail',  $getPodetail );
		$this->set( 'getPo',  $getPo );
		$this->render( 'getPoResult' );
	
	}*/
	
	
	public function prepareCsv()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$getBase = Router::url('/', true);
		
		$this->loadModel('PurchaseOrder');
		$newPoname	=	explode( '(', $this->request->data['poName'] );
		$poName		=	$newPoname[0];
		
		$getPodetails =	$this->PurchaseOrder->find( 'all' , array('conditions' => array( 'PurchaseOrder.po_name' => $poName) ) );
		
		  App::import('Vendor', 'PHPExcel/IOFactory');
		  App::import('Vendor', 'PHPExcel');  
		  $objPHPExcel = new PHPExcel();       
		  
		  //Column Create  
		  $objPHPExcel->setActiveSheetIndex(0);
		  $objPHPExcel->getActiveSheet()->setCellValue('A1', 'PO Name');     
		  $objPHPExcel->getActiveSheet()->setCellValue('B1', 'SKU');
		  $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Code');
		  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Barcode');
		  $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Description');		  
		  $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Qty');		  
		  $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Warehouse In');
		  $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Comment');
		  $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Seller Comment');
		  
		  $inc = 2; 
		  foreach( $getPodetails as $getPodetailIndex => $getPodetailValue )
		  {
			  $inWarehouse = $this->getWerehouseFillQty( $getPodetailValue['PurchaseOrder']['po_name'], $getPodetailValue['PurchaseOrder']['purchase_sku'] );
			  $objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getPodetailValue['PurchaseOrder']['po_name']);     
			  $objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $getPodetailValue['PurchaseOrder']['purchase_sku']);
			  $objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getPodetailValue['PurchaseOrder']['purchase_code']);
			  $objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getPodetailValue['PurchaseOrder']['purchase_barcode']);		  
			  $objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $getPodetailValue['PurchaseOrder']['purchase_description']);		  
			  $objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $getPodetailValue['PurchaseOrder']['purchase_qty']);		  
			  $objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $inWarehouse);
			  $objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $getPodetailValue['PurchaseOrder']['comment']);
			  $objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $getPodetailValue['PurchaseOrder']['seller_comment']);
			  $inc++;
		  }
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:Q1')->getAlignment()->applyFromArray(
		  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:Q1')->getAlignment()->setWrapText(true);
		  $objPHPExcel->getActiveSheet(0)->setTitle('Current Stock');
		  $objPHPExcel->getActiveSheet(0)->getStyle("A1:Q1")->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet(0)
			 ->getStyle('A1:Q1')
			 ->applyFromArray(
			  array(
			   'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EBE5DB')
			   )
			  )
			 );		   
		  
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/poCsv.csv';
		  $uploadRemote = $getBase.'app/webroot/img/stockUpdate/poCsv.csv';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		  $objWriter->save($uploadUrl);		 
		
		  echo $uploadRemote = $getBase.'app/webroot/img/stockUpdate/poCsv.csv';
		  exit; 
		 
	}
	
	
	public function getWerehouseFillQty( $po = null, $sku  = null)
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'CheckIn' );
		$sumQuantity	=	$this->CheckIn->find( 'first', array(
									'conditions' => array( 'CheckIn.po_name' => $po, 'CheckIn.sku' => $sku),
									'fields' => array( 'sum(CheckIn.qty_checkIn)   AS Quantity' , 'CheckIn.sku, CheckIn.barcode, CheckIn.po_name'),
									'group' => 'CheckIn.po_name',
									 )
								);
		if( count( $sumQuantity ) > 1 )
		{						
				$warehouseQuantity = $sumQuantity[0]['Quantity'];
		}
		else
		{
				$warehouseQuantity = '0';
		}
		return $warehouseQuantity;
	}
	
	public function prepareDemoCsv()
	{
		  $this->layout = '';
		  $this->autoRender = false;
		  $getBase = Router::url('/', true);
			
		  App::import('Vendor', 'PHPExcel/IOFactory');
		  App::import('Vendor', 'PHPExcel');  
		  $objPHPExcel = new PHPExcel();       
		  
		  //Column Create  
		  $objPHPExcel->setActiveSheetIndex(0);
		  $objPHPExcel->getActiveSheet()->setCellValue('A1', 'QTY');     
		  $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Description');
		  $objPHPExcel->getActiveSheet()->setCellValue('C1', 'CODE');
		  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'SKU');
		  $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Barcode');		  
	
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/poDemoCsv.csv';
		  $uploadRemote = $getBase.'app/webroot/img/stockUpdate/poDemoCsv.csv';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		  $objWriter->save($uploadUrl);		 
		
		  echo $uploadRemote = $getBase.'app/webroot/img/stockUpdate/poDemoCsv.csv';
		  exit; 
	}
	public function markLoclUnlock()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'Po' );
		$poname	=	$this->request->data['poName'];
		$poname = explode( '(', $poname );
		
		$status	=	$this->request->data['status'];
		
		if($status == 0)
		{
			$this->Po->updateAll( array('Po.status' => '0' ), array('Po.po_name' => $poname[0] ) );
			echo '1';
			exit;
		}
		else
		{
			$this->Po->updateAll( array('Po.status' => '1' ), array('Po.po_name' => $poname[0] ) );
			echo '0';
			exit;
		}
		
		
	}
	
	/************End For Po upload and show detail************/
	
	public function updateInventery()
	{
		$this->layout = 'index';
	}
	
	
	public function updatePackagingIdByBarcodeSearch()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('BinLocation');	
		$this->loadModel('CheckIn');
		$this->loadModel('UpdateInventory');
		
		$barcode = $this->request->data['barcode'];
		
		//Delete Specific barcode rows
		$this->BinLocation->query( "delete from bin_locations where barcode = '{$barcode}'" );
	
		$product_id = $this->request->data['product_id'];
		$descId = $this->request->data['id'];
		$bin = $this->request->data['bin_sku'];
		$binAllocation = $this->request->data['getLocation'];
		$userId = $this->request->data['userId'];
		$pcName = $this->request->data['pcName'];
		$qtyBeforeUpdate = $this->request->data['qtyBeforeUpdate'];
		
		$getStockSum = array_sum( explode(",",$this->request->data['getStockByLocation']) );
		
		//Unbinding Techniques 
		$this->ProductDesc->unbindModel( array( 'belongsTo' => array( 'Product' ) ) );
				
		$this->ProductDesc->updateAll(
			array(
				'bin' => "'".$bin."'"
			),
			array(
				'ProductDesc.id' => $descId
			)
		);
		//Save only
		$getLocation = explode( ',',$this->request->data['getLocation'] );
		$getStock = explode( ',',$this->request->data['getStockByLocation'] );
		
		if( count($getLocation) > 0 )
		{
         	$stockCount = 0;$e = 0;while( $e < count( $getLocation ) )
			{
				if(  $getLocation[$e] != '' && $getStock[$e] != '' )
				{
                                    
					$this->request->data['BinLocationPreffered']['BinLocation']['barcode'] = $this->request->data['barcode'];
					$this->request->data['BinLocationPreffered']['BinLocation']['bin_location'] = $getLocation[$e];
					$this->request->data['BinLocationPreffered']['BinLocation']['stock_by_location'] = $getStock[$e];
					$stockCount = $stockCount + $getStock[$e];
                                        
					$this->BinLocation->saveAll( $this->request->data['BinLocationPreffered'] );
				}
			$e++;	
			}
		}
		
		$this->Product->query( "UPDATE products set current_stock_level = '{$stockCount}' where id = {$product_id}" );
    	
		/*
		 * CheckIn record manipulation
		 */ 
		 
		$updateIn['sku']				= $bin;
		$updateIn['barcode']			= $barcode;
		$updateIn['qty_before_update']	= $qtyBeforeUpdate;
		$updateIn['qty_after_update']	= $getStockSum;
		$updateIn['user_id']			= $userId;
		$updateIn['pc_name']			= $pcName;
		$diff = $updateIn['qty_after_update'] - $updateIn['qty_before_update'];
		$updateIn['qty_diff']			= $diff;
		$this->UpdateInventory->saveAll( $updateIn );
		unset($updateIn);
		$updateIn = '';
		$data['status'] 		= 'success';	
                
		echo (json_encode(array('status' => '1', 'data' => $data)));		
		exit;	 
	}
	
    /************************************ for manage stock *******************************************/
    public function manageStock_old()
    {
		$this->layout = 'index';
		$this->loadModel('SkuPercentRecord');
		$this->loadModel('Store');
		$this->Store->unbindModel(array('belongsTo' => array('Company')), true);
		
		$getStores 	=		$this->Store->find('all', array( 'order' => 'Store.store_name ASC' ));
		$perPage	=	 	'100';//Configure::read( 'perpage' );
		$this->paginate = array(
					  'group' => 'SkuPercentRecord.sku',
                      'limit' => $perPage,			
                      'order' => array('SkuPercentRecord.store_name ASC')			
			);
			
		$getskuDetail = $this->paginate( 'SkuPercentRecord' );
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		$imgPath = WWW_ROOT .'img/storesFeed/'; 
		$dir = new Folder($imgPath, true, 0755);
		$files = $dir->find('.*\.csv');
		$files = Set::sort($files, '{n}', 'DESC');
		$this->set('files', $files);
		$this->set( compact('getskuDetail' , 'getStores' ) );
	}
	
	public function getmargen()
	{
		$this->loadModel( 'SkuPercentRecord' );
		$getresult	=	$this->SkuPercentRecord->find('first', array( 'conditions' => array('SkuPercentRecord.margin_value >' => 0) ));
		return $getresult['SkuPercentRecord']['margin_value'];
		exit;
	}
	
	public function saveStockFeed()
	{
		// use for save all aku with store
		$this->setStoreSku();
		
		// use for save total sold sku
		//$this->getallSoldSku();
		
		// use sold sku for thirty minut
		//$this->getallSoldSkuOfThirtyDay();
		
		//for selected day sku
		//$this->setSelectedDaySoldSku();
	}
	
	
	
	 /* for save all store related sku by cron*/
    public function setStoreSku()
	{
		
		$this->loadModel('Product');
		$this->loadModel('Store');
		$this->loadModel('SkuPercentRecord');
		
		$getStockDetails	=	$this->Product->find('all', 
									array( 
										'fields' => array( 
													'Product.current_stock_level, 
													Product.product_sku, 
													Product.store_name,
													ProductDesc.barcode,
													Product.product_name,
													ProductDesc.barcode' 
												) 
										)
									);
		foreach( $getStockDetails as $getStockDetailValue)
		{
			$storeIds	=	 explode(',', $getStockDetailValue['Product']['store_name']);
			foreach( $storeIds as $storeIdValue )
			{
				$getStoreName	=	$this->Store->find( 'first', array( 
								'conditions' => array( 'Store.id' => $storeIdValue),
								'fields' => array( 'Store.store_name, Store.id') 
								)
							);
				$storeNameAlies = str_replace('.','_',str_replace(' ','_',$getStoreName['Store']['store_name']));		
				$getResult	=	$this->SkuPercentRecord->find('first', array( 'conditions' => array( 'SkuPercentRecord.store_id' => $getStoreName['Store']['id'], 'SkuPercentRecord.sku' => $getStockDetailValue['Product']['product_sku']) ));
				
					if( count($getResult['SkuPercentRecord']['id']) > 0  )
					{
						$data['SkuPercentRecord']['id'] 		=  $getResult['SkuPercentRecord']['id'];
						$data['SkuPercentRecord']['store_id'] 					= $getStoreName['Store']['id'];
						$data['SkuPercentRecord']['store_name'] 				= $getStoreName['Store']['store_name'];
						$data['SkuPercentRecord']['store_alies'] 				= $storeNameAlies;
						$data['SkuPercentRecord']['sku'] 						= $getStockDetailValue['Product']['product_sku'];
						$data['SkuPercentRecord']['current_stock'] 				= $getStockDetailValue['Product']['current_stock_level'];
						$data['SkuPercentRecord']['product_title'] 				= $getStockDetailValue['Product']['product_name'];
						$data['SkuPercentRecord']['barcode'] 					= ($getStockDetailValue['ProductDesc']['barcode'] != '') ? $getStockDetailValue['ProductDesc']['barcode'] : '0';
						$this->SkuPercentRecord->saveAll( $data );
					}
					else
					{
						$data['SkuPercentRecord']['id'] 						= 	'';
						$data['SkuPercentRecord']['percentage'] 				= 	'70';
						$data['SkuPercentRecord']['store_id'] 					= $getStoreName['Store']['id'];
						$data['SkuPercentRecord']['store_name'] 				= $getStoreName['Store']['store_name'];
						$data['SkuPercentRecord']['store_alies'] 				= $storeNameAlies;
						$data['SkuPercentRecord']['sku'] 						= $getStockDetailValue['Product']['product_sku'];
						$data['SkuPercentRecord']['current_stock'] 				= $getStockDetailValue['Product']['current_stock_level'];
						$data['SkuPercentRecord']['product_title'] 				= $getStockDetailValue['Product']['product_name'];
						$data['SkuPercentRecord']['barcode'] 					= ($getStockDetailValue['ProductDesc']['barcode'] != '') ? $getStockDetailValue['ProductDesc']['barcode'] : '0';
						$this->SkuPercentRecord->saveAll( $data );
					}
					
					
					
					
			}
		}
		
		// use for save total sold sku
		$this->getallSoldSku();
	}
	
	/***************************************Use for Total Sold Stock( sold_stock )*************************************/
	public function getallSoldSku()
	{
		
		$this->loadModel( 'SkuPercentRecord' );
		$this->loadModel( 'OpenOrder' );
		
		$getAllStoreSkus	=	$this->SkuPercentRecord->find('all', array( 
																'fields' => array('SkuPercentRecord.sku'),
																'group'=> array( 'SkuPercentRecord.sku' )
																)
															);
		foreach( $getAllStoreSkus as $getAllStoreSku )
		{
			$sku =  $getAllStoreSku['SkuPercentRecord']['sku'];
			
			
			$getdetails	=	$this->OpenOrder->find( 'count' , array( 
																'conditions' => array( 'OpenOrder.items like' =>"%{$sku}%",
																		),
																	)
												  );
			$detail = $getdetails."===".$sku;
			$detailSplit = explode('===', $detail);
			$this->SkuPercentRecord->updateAll( 
											array( 
												'SkuPercentRecord.sold_stock' => $detailSplit[0],  
												),
											array(
												'SkuPercentRecord.sku' => $detailSplit[1],
												)
											);
		}
		
		// use sold sku for thirty minut
		$this->getallSoldSkuOfThirtyDay();
		
	}
	
	/***************************************Use for 30 day Sold Stock( fixed_day_sold )*************************************/
	public function getallSoldSkuOfThirtyDay()
	{
		
		$this->loadModel( 'SkuPercentRecord' );
		$this->loadModel( 'OpenOrder' );
		$getFrom = date('Y-m-d',time() - 30 * 3600 * 24);
		$getEnd = date('Y-m-d',time());
		
		$getAllStoreSkus	=	$this->SkuPercentRecord->find('all', array( 
																'fields' => array('SkuPercentRecord.sku'),
																'group'=> array( 'SkuPercentRecord.sku' )
																)
															);
		foreach( $getAllStoreSkus as $getAllStoreSku )
		{
			$sku =  $getAllStoreSku['SkuPercentRecord']['sku'];
			
			
			$getdetails	=	$this->OpenOrder->find( 'count' , array( 
																'conditions' => array(
																		'OpenOrder.items like' =>"%{$sku}%",
																		'OpenOrder.date >=' => $getFrom,
																		'OpenOrder.date <=' => $getEnd,
																		),
																	)
												  );
			$detail = $getdetails."===".$sku;
			$detailSplit = explode('===', $detail);
			
			$this->SkuPercentRecord->updateAll( 
											array( 
												'SkuPercentRecord.fixed_day_sold' => $detailSplit[0],  
												),
											array(
												'SkuPercentRecord.sku' => $detailSplit[1],
												)
											);
		}
		
		//for selected day sku
		$this->setSelectedDaySoldSku();
	}
	
	
	/*************************************selected day sold ( fixed day sold )********************************************************/
	
	/*************/
	public function setSelectedDaySoldSku()
	{
		
		$this->loadModel( 'SkuPercentRecord' );
		$this->loadModel( 'OpenOrder' );
		$getFrom = date('Y-m-d',time() - 7 * 3600 * 24);
		$getEnd = date('Y-m-d',time());
		
		$getAllStoreSkus	=	$this->SkuPercentRecord->find('all', array( 
																'fields' => array('SkuPercentRecord.sku', 'SkuPercentRecord.store_name'),
																)
															);
		
		foreach( $getAllStoreSkus as $getAllStoreSku )
		{
			$sku 		=  $getAllStoreSku['SkuPercentRecord']['sku'];
			$storeName  =  $getAllStoreSku['SkuPercentRecord']['store_name'];
			
			$getdetails	=	$this->OpenOrder->find( 'count' , array( 
																'conditions' => array(
																		'OpenOrder.items like' =>"%{$sku}%",
																		'OpenOrder.date >=' => $getFrom,
																		'OpenOrder.date <=' => $getEnd,
																		'OpenOrder.sub_source' => $storeName,
																		),
																	)
												  );
			$detail = $getdetails."===".$sku;
			$detailSplit = explode('===', $detail);
			
			$this->SkuPercentRecord->updateAll( 
											array( 
												'SkuPercentRecord.select_day_sold' => $detailSplit[0],  
												),
											array(
												'SkuPercentRecord.sku' => $detailSplit[1],
												'SkuPercentRecord.store_name' => $storeName
												)
											);
		}
		
	}
	
	
	
	/******************************* Save store Sku Per ****************************************/
	
	public function saveStoreSkuPercentage()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'SkuPercentRecord' );
		
		$totaldetail 	=	 	count($this->request->data['skuStoredetail']);
		$bufferValue	=		$this->request->data['bufferValue'];
		$newskuStatus	=		($this->request->data['newskuStatus'] == 'true') ? 1 : 0 ;
		$textString		=		$this->request->data['textString'];
		
		for( $i = 0; $i < $totaldetail; $i++ )
		{
			$splitDetail	=	 explode('####',$this->request->data['skuStoredetail'][$i]);
			if( $splitDetail[0] != 'undefined' && $splitDetail[1] != 'undefined')
			{
				$store		=	explode('__', $splitDetail[1]);
				
				$storeName		=	$store[1];
				$percenatage 	=	$splitDetail[0];
				$sku 			=	$splitDetail[2];
				if( $textString == 'Update' )
				{
					$customMark = 1;
				}
				else
				{
					$customMark = 0;
				}
				$this->SkuPercentRecord->updateAll(
													array(
													'SkuPercentRecord.percentage' => $percenatage,
													'SkuPercentRecord.buffer' => $bufferValue,
													'SkuPercentRecord.custom_mark' => $customMark,
													'SkuPercentRecord.sku_status' => $newskuStatus
													), 
													array(
													 'SkuPercentRecord.sku' => $sku,
													 'SkuPercentRecord.store_name' => $storeName,
													)
												   );
				
			}
		}
		echo "1";
		exit;
	}
	
	
	/******************************************* for save all sku data ***********************************************/
	
	/* for save all data */
	public function saveStoreSkuDetail_old()
	{
		$this->loadModel( 'SkuPercentRecord' );
		$FormOne = $this->request->data['setData'];
		$FormOneValues = array();
		parse_str($FormOne, $FormOneValues);
		
		foreach( $FormOneValues as $index => $value )
		{
			$fullSkuDetail	=	$index.'___'.$value;
			$splitDetail	=	explode('___', $fullSkuDetail);
			
			$sku	=	$splitDetail['0'];
			
			if( $splitDetail['1'] == 'sku_status_selected' && $splitDetail['2'] == '0')
			{
				$this->SkuPercentRecord->updateAll(
												array('SkuPercentRecord.sku_status' => 0), 
												array('SkuPercentRecord.sku' => $sku)
												);
			}
			if( $splitDetail['1'] == 'sku_status_selected' && $splitDetail['2'] == '1')
			{
				$this->SkuPercentRecord->updateAll(
												array('SkuPercentRecord.sku_status' => 1), 
												array('SkuPercentRecord.sku' => $sku)
												);
			}
			else if( $splitDetail['1'] == 'buffer' )
			{
				$buffer	=	$splitDetail['2'];
				$this->SkuPercentRecord->updateAll(
												array('SkuPercentRecord.buffer' => $buffer), 
												array('SkuPercentRecord.sku' => $sku)
												);
			}
			else
			{
				$storeName 	= $splitDetail['1'];
				$percentage = (isset($splitDetail['2'])  && $splitDetail['2'] != '') ? $splitDetail['2'] : 0;
				$this->SkuPercentRecord->updateAll(
												array('SkuPercentRecord.percentage' => $percentage), 
												array('SkuPercentRecord.sku' => $sku,
													  'SkuPercentRecord.store_alies' => $storeName
													 )
												);
			}
		}
		echo "1";
		exit;
	}
	
	public function saveStoreSkuDetail()
	{
		$this->loadModel( 'SkuPercentRecord' );
		$FormOne = $this->request->data['setData'];
		$FormOneValues = array();
		parse_str($FormOne, $FormOneValues);
		//pr($FormOneValues );
		//exit;
		foreach( $FormOneValues as $index => $value )
		{
			$fullSkuDetail	=	$index.'___'.$value;
			$splitDetail	=	explode('___', $fullSkuDetail);
			//pr( $splitDetail );
			//exit;
			$sku	=	$splitDetail['0'];
			
			if( $splitDetail['1'] == 'sku_status_selected' )
			{
				if( $splitDetail['2'] == '0' )
				{
					$status = '0';
				}
				else
				{
					$status = '1';
				}
				$this->SkuPercentRecord->updateAll(
												array('SkuPercentRecord.sku_status' => $status), 
												array('SkuPercentRecord.sku' => $sku)
												);
			}
			else if( $splitDetail['1'] == 'buffer' )
			{
				$buffer	=	$splitDetail['2'];
				$this->SkuPercentRecord->updateAll(
												array('SkuPercentRecord.buffer' => $buffer), 
												array('SkuPercentRecord.sku' => $sku)
												);
			}
			else
			{
				$storeName 	= $splitDetail['1'];
				$percentage = (isset($splitDetail['2'])  && $splitDetail['2'] != '') ? $splitDetail['2'] : 0;
				$this->SkuPercentRecord->updateAll(
												array('SkuPercentRecord.percentage' => $percentage), 
												array('SkuPercentRecord.sku' => $sku,
													  'SkuPercentRecord.store_alies' => $storeName
													 )
												);
			}
		}
		echo "1";
		exit;
	}
	
	/*************************** save open order save qty ****************************************/
	public function getOpenOrderQtyByStore()//$sku, $storeName
	{
		
		
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel('SkuPercentRecord');
		
		//$storeName	=	'Rainbow Retail';//$this->request->data['getStoreSelection'];
		
		$status = array(0,1);
		$params	=	array(
					'joins' => array(
							array(
								'table' => 'merge_updates',
								'alias' => 'MergeUpdate',
								'type' => 'INNER',
								'conditions' => array(
									'OpenOrder.num_order_id = MergeUpdate.order_id'
								)
							),
							array(
								'table' => 'scan_orders',
								'alias' => 'ScanOrder',
								'type' => 'INNER',
								'conditions' => array(
									'MergeUpdate.product_order_id_identify = ScanOrder.split_order_id'
								)
							)
						),
					'conditions' => array( 
									'OpenOrder.status' => $status,
									),
					'fields' => array('SUM(ScanOrder.quantity) AS QUANTITY','ScanOrder.sku','OpenOrder.sub_source'),
					'group' => 'ScanOrder.sku, OpenOrder.sub_source', 
					'order' => array('QUANTITY DESC') 
				);
				
		$getOpenOrderDetails		=	$this->OpenOrder->find( 'all', $params );
		//pr( $getOpenOrderDetails );
		//exit;
		foreach( $getOpenOrderDetails as $getOpenOrderDetailValue )
		{
			
			$soldStock	=	$getOpenOrderDetailValue['0']['QUANTITY'];
			$sku		=	$getOpenOrderDetailValue['ScanOrder']['sku'];
			$subSource	=	$getOpenOrderDetailValue['OpenOrder']['sub_source'];
			
			$this->SkuPercentRecord->updateAll( 
											array( 
												'SkuPercentRecord.open_order_qty' => $soldStock,  
												),
											array(
												'SkuPercentRecord.sku' => $sku,
												'SkuPercentRecord.store_name' => $subSource
												)
											);
		}
	}
			
	/*********************** Save open order bundal qty******************************/
	
	public function bundalOpenOrderQty()
	{
		$this->loadModel('SkuPercentRecord');
		$getBundelDetails =	$this->SkuPercentRecord->find('all', 
											array( 'conditions' => array(
															'SkuPercentRecord.product_type' => 'Bundle',
															),
													'fields' =>  array('SkuPercentRecord.id','SkuPercentRecord.sku','SkuPercentRecord.store_name')
														) 
													);
		foreach(  $getBundelDetails as $getBundelDetail )
		{
				$id 				= $getBundelDetail['SkuPercentRecord']['id'];
				$sku 				= $getBundelDetail['SkuPercentRecord']['sku'];
				$storeName 			= $getBundelDetail['SkuPercentRecord']['store_name'];
				$openOrderqty 		= $this->getOpenOrderQty( $sku, $storeName );
				$this->SkuPercentRecord->updateAll( 
											array( 
												'SkuPercentRecord.open_order_qty' => $openOrderqty,  
												),
											array(
												'SkuPercentRecord.sku' => $sku,
												'SkuPercentRecord.store_name' => $storeName
												)
											);
				
		}									
	}
	
	public function getOpenOrderQty( $sku , $storeName )
	{
			$this->layout = "";
			$this->autoRander = false;
			$this->loadModel('OpenOrder');
			
			$getdetails	=	$this->OpenOrder->find( 'first' , array( 
																'conditions' => array( 'OpenOrder.items like' =>"%{$sku}%",
																					   'OpenOrder.sub_source' =>"{$storeName}",
																					   'OpenOrder.status' =>0
																		),
																'fields' => array('OpenOrder.items','OpenOrder.sub_source') 
																	)
												);
				$bundalQty = 0;	
				if( !empty( $getdetails ) )
				{
						$items['sub_source']	 = $getdetails['OpenOrder']['sub_source'];
						$items['items']			 = unserialize($getdetails['OpenOrder']['items']);
						
						foreach( $items['items'] as $item)
						{
							$bundalQty = $bundalQty + $item->Quantity;
						}
						return $bundalQty;
						
				}
				
				
	}
	
	public function saveMarginValue()
	{
		$this->loadModel('SkuPercentRecord');
		
		$marginValue = $this->request->data['marginValue'];
		//pr( $this->request->data );
		//exit;
		
		//$getresult	=	$this->SkuPercentRecord->find('all', array( 'group' => array('Max(SkuPercentRecord.select_day_sold) AS SelectDayValue') ));
		
		$getBundelDetails =	$this->SkuPercentRecord->find('all', 
											array( 
												'fields' =>  array(
																'SkuPercentRecord.sku'
																),
												'group' => array( 
																'SkuPercentRecord.sku'
																)
															) 
														);
		$i = 0;
		$this->SkuPercentRecord->query( "UPDATE sku_percent_records set margin_value = 0" );
		if( !empty( $getBundelDetails ) )
		{
			$results =array();
			foreach( $getBundelDetails as $getBundelDetail )
			{
				$result = $this->SkuPercentRecord->find('first', array(
								'conditions' => array(
													'SkuPercentRecord.sku' => $getBundelDetail['SkuPercentRecord']['sku'],
													'SkuPercentRecord.select_day_sold !=' => 0,
													'SkuPercentRecord.sku_status' => 0,
													), 
								'fields' => array(
												'MAX(SkuPercentRecord.select_day_sold) AS selectDaySold','SkuPercentRecord.id','SkuPercentRecord.sku','SkuPercentRecord.store_name','SkuPercentRecord.store_alies'
												),
								'order' => array('SkuPercentRecord.id ASC'),
								)
							);
				if($result['SkuPercentRecord']['id'] != '' )
				{
					$results[] 	= $result;
					$data['id'] = $result['SkuPercentRecord']['id'];
					$data['margin_value'] = $marginValue;
					$this->SkuPercentRecord->saveAll( $data );
				}
				$i++;
			}
		}
		
		echo json_encode(array('status' => 1, 'data' => $results));
		exit;
	}
	
	public function removeCustomMark()
	{
		$this->loadModel( 'SkuPercentRecord' );
		$skuAndMark	=	$this->request->data['remove'];
		$splitSku	=	explode('####', $skuAndMark);
		
		if( $splitSku[1] == 1 )
		{
			$this->SkuPercentRecord->updateAll(
									array('SkuPercentRecord.custom_mark ' => 0 ),
									array('SkuPercentRecord.sku ' => $splitSku[0])
								);
		}
		
		exit;
	}
	
	public function creatFeedSheet()
	{
		
	}
	
	/* create the feed csv file for all store  */
	public function creatFeedSheet_old($storeAlias = null)
	{
		$storeAlias = 'CostBreaker_UK';
		
		
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'SkuPercentRecord' );
		$this->loadModel( 'Store' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		
		$getBase = Router::url('/', true);
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		
		$this->Store->unbindModel(array('belongsTo' => array('Company')), true);
		
		$getStores = $this->Store->find( 'all', array( 'conditions' => array( 'Store.status' => '1' , 'Store.store_alias' => $storeAlias ) ));
		//pr( $getStores );
		//exit;
		foreach( $getStores as $getStore )
		{
			$storeName 		= 	$getStore['Store']['store_name'];
			$storeId 		= 	$getStore['Store']['id'];
			$storeStatus 	= 	$getStore['Store']['status'];
			
			$params = array('conditions'=> 
							array( 'SkuPercentRecord.store_id' => $storeId)
							);
			$getAllFeeds	=	$this->SkuPercentRecord->find( 'all', $params );
			
			//Column Create 
			$objPHPExcel = new PHPExcel(); 
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Product Title');     
			$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Sku');
			$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Barcode');
			$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Stock');
			$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Original Stock');
			$objPHPExcel->getActiveSheet()->setCellValue('F1', $storeName);
			    
			$inc = 2; $e = 0;
			$innerSku = array();
			foreach( $getAllFeeds as $getAllFeed )
			{
				$mainSku	 		= $getAllFeed['SkuPercentRecord']['sku'];
				
				$this->Product->unbindModel( array(
										'hasMany' => array(
											'ProductLocation'
											)		
										) 
									 );
									 
									 $this->ProductDesc->unbindModel(
										array(
											'belongsTo' => array(
												'Product'
											)
										)
									);
					
									$params = array(
										'conditions' => array(
											'Product.product_sku' => trim($mainSku)
										),
										'fields' => array(
											//'IF(Product.current_stock_level > 2, Product.current_stock_level , 0) as FindStockThrough__LimitFactor'
											'Product.current_stock_level',
											'ProductDesc.product_type',
											'ProductDesc.product_identifier',
											'ProductDesc.product_defined_skus'
										),
										'order' => 'Product.id ASC'
									);
				$stockDetail = json_decode(json_encode($this->Product->find( 'first' , $params )),0);
				
				
				$openOrderItemCount 	=  $this->getSoldByStoreOpenOrder( $mainSku , $storeName );
				
				//echo $mainSku.'====='.$storeName.'====='.$openOrderItemCount."<br>";
				
				$currentStock 		= $stockDetail->Product->current_stock_level;
				//$currentStock 		= $getAllFeed['SkuPercentRecord']['current_stock'];
				$storeName 			= $getAllFeed['SkuPercentRecord']['store_name'];
				$storeAlies 		= $getAllFeed['SkuPercentRecord']['store_alies'];
				if( $storeAlies == 'http://www_storeforlife_co_uk')
				{
					$storeAlies = 'Store_For_Life';
				}
				
				$percentage 		= $getAllFeed['SkuPercentRecord']['percentage'];
				$buffer 			= $getAllFeed['SkuPercentRecord']['buffer'];
				$productTitle 		= $getAllFeed['SkuPercentRecord']['product_title'];
				$customMark 		= $getAllFeed['SkuPercentRecord']['custom_mark'];
				$skuStatus 			= $getAllFeed['SkuPercentRecord']['sku_status'];
				$barcode 			= $getAllFeed['SkuPercentRecord']['barcode'];
				$productType 		= $stockDetail->ProductDesc->product_type;//$getAllFeed['SkuPercentRecord']['product_type'];
				$productIdentifier 	= $stockDetail->ProductDesc->product_identifier;//$getAllFeed['SkuPercentRecord']['product_identifier'];
				$productDefinedSkus = $stockDetail->ProductDesc->product_defined_skus;//$getAllFeed['SkuPercentRecord']['product_defined_skus'];
				$openOrderQty 		= ($openOrderItemCount > 0) ? $openOrderItemCount : '0';//$getAllFeed['SkuPercentRecord']['open_order_qty'];
				$marginValue 		= ($getAllFeed['SkuPercentRecord']['margin_value'] > 0) ? $getAllFeed['SkuPercentRecord']['margin_value'] : '0';
				
				
				
				 if( strtolower( $productType ) === "single" ):
								$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($productTitle) );    
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($mainSku) );    
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($barcode) );    
										if( $currentStock <= 0 ):
											$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );	
											$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, 0 );
										
											//Set highlight full row
											$objPHPExcel->getActiveSheet(0)->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));					
										
											//For other subsource
											$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
										else:
											$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $currentStock );
											$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $currentStock );							
											//For other subsource
											if( $currentStock == 1 )	
											{
												$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
											}
											else if( $currentStock < 1 )	
											{
												$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
											}
											else
											{
												if($currentStock <= $marginValue)
													{
														if( $marginValue > 0 )
														{
															$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $currentStock + $openOrderQty);
														}
														else
														{
															$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0);
														}
													}
													else
													{	
														if( $skuStatus == 1 )
														{
															$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $currentStock + $openOrderQty);
														}
														else
														{
															$actualQty = floor((($currentStock - $buffer) * $percentage)/100) + $openOrderQty;
															$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc,  $actualQty);
														}
													}
											}
										endif;
						elseif( strtolower( $productType ) === "bundle" ):					
								if( strtolower( $productIdentifier ) === "single" ):
									//Set if same sku is there with single-bundle type
									$this->Product->unbindModel( array(
										'hasOne' => array(
											'ProductDesc' , 'ProductPrice' 
											),
										'hasMany' => array(
											'ProductLocation'
											)		
										) 
									 );
									 
									 $this->ProductDesc->unbindModel(
										array(
											'belongsTo' => array(
												'Product'
											)
										)
									);
					
									$params = array(
										'conditions' => array(
											'Product.product_sku' => trim($productDefinedSkus)
										),
										'fields' => array(
											//'IF(Product.current_stock_level > 2, Product.current_stock_level , 0) as FindStockThrough__LimitFactor'
											'Product.current_stock_level as FindStockThrough__LimitFactor',
											'Product.product_sku'
											
										),
										'order' => 'Product.id ASC'
									);
									$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);
									pr( $stockData );
									
									$mainSkuNew = explode('-',$mainSku );
									pr( $mainSkuNew );
									exit;						
									$getDivisionNumberFromBundleSku = $mainSkuNew[2];						
									$index = 0;
									$getAvailableStockFor_Bundle_WithSameSku = $stockData[0][$index]->FindStockThrough__LimitFactor;
									
									//Set data into file 
									$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes( $productTitle ) );    
									$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes( $mainSku ) );    
									$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes( $barcode ) );
									
									$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );
									
									if( $getAvailableStockFor_Bundle_WithSameSku > 2 ): 
										$getAvailableStockFor_Bundle_WithSameSku = floor($getAvailableStockFor_Bundle_WithSameSku / $getDivisionNumberFromBundleSku) - 2;
										
										//For other subsource
										if( $getAvailableStockFor_Bundle_WithSameSku == 1 )
										{
											$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
										}
										else if( $getAvailableStockFor_Bundle_WithSameSku < 1 )
										{
											$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
										}
										else
										{
											
											if($getAvailableStockFor_Bundle_WithSameSku <= $marginValue)
													{
														if( $marginValue > 0 )
														{
															$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $getAvailableStockFor_Bundle_WithSameSku + $openOrderQty);
														}
														else
														{
															$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0);
														}
													}
													else
													{	
														if( $skuStatus == 1 )
														{
															$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $getAvailableStockFor_Bundle_WithSameSku + $openOrderQty);
														}
														else
														{
															$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor((($getAvailableStockFor_Bundle_WithSameSku - $buffer) * $percentage)/100) + $openOrderQty );
														}
													}
											
											
											//$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor((($getAvailableStockFor_Bundle_WithSameSku)* $percentage)/100) + $openOrderQty);//($stockDataValue-2)* $percentage)/100
										}	
									else:
										
										//Set highlight full row
										$objPHPExcel->getActiveSheet(0)
										->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));						
										
										//For other subsource
										$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
									endif;	

									$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $stockData[0][$index]->FindStockThrough__LimitFactor );						
								
								elseif( strtolower( $productIdentifier ) === "multiple" ):
								
									$stockData = '';
															
									$params = array(
										'conditions' => array(
											'ProductDesc.product_defined_skus' => $productDefinedSkus
										),
										'fields' => array(
											'ProductDesc.product_defined_skus as Diff_Skus',
											'Product.product_sku'
										),
										'order' => 'ProductDesc.id ASC'
									);
									
									$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);	
									$diff_SkuList = explode(':',trim($stockData[0]->ProductDesc->Diff_Skus));					
									
									//Get Diff Sku stock value under min section according to limit factor
									//Set if same sku is there with single-bundle type
									$this->Product->unbindModel( array(
										'hasOne' => array(
											'ProductDesc' , 'ProductPrice' 
											),
										'hasMany' => array(
											'ProductLocation'
											)		
										) 
									 );
									 
									 $this->ProductDesc->unbindModel(
										array(
											'belongsTo' => array(
												'Product'
											)
										)
									);
									
									$params = array(
										'conditions' => array(
											'Product.product_sku' => $diff_SkuList
										),
										'fields' => array(
											//'IF(Product.current_stock_level > 2, (Product.current_stock_level -2) , 0) as FindStockThrough__LimitFactor'
											'Product.id',
											'Product.current_stock_level'
										),
										'order' => 'Product.id ASC'
									);
									
									//Get relevant data
									$getListStockValue = $this->Product->find( 'list' , $params );							
									$stockDataValue = min($getListStockValue);
									
									//Here we have found min lowest stock sku value tha means, need to put it into csv but keep safety margin
									if( $stockDataValue > 2 ):
										
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($productTitle) );    
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($mainSku) );    
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($barcode) );    
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockDataValue );
										$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
										
										//For other subsource
										if( ($stockDataValue-2) == 1 )
										{
											$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
										}
										else if( ($stockDataValue-2) < 1 )
										{
											$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
										}
										else
										{
											$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor(((($stockDataValue-$buffer)* $percentage)/100)+ $openOrderQty));//floor(($getStockDetailValue[0]->Product->AvailableStock * $percentage)/ 100
										}
									elseif( $stockDataValue <= 2 ):
										$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($productTitle) );    
										$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($mainSku) );    
										$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($barcode) );    
										$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockDataValue );
										$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
										$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
										$objPHPExcel->getActiveSheet(0)->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));
									endif;
									
								endif;
								
							endif;
					$e++;$inc++;
					
					
				
			}
					$uploadUrl = WWW_ROOT .'img/storesFeed/StockFeedTest_'.$storeAlies.'.csv';
					$uploadRemote = $getBase.'img/storesFeed/StockFeedTest_'.$storeAlies.'.csv';
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
					$objWriter->save($uploadUrl);
			
				
			
		}
	}
	
	/*public function setSelectedDaySoldSku()
	{
		
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel('SkuPercentRecord');
		
		$getFrom = date('Y-m-d',time() - 30 * 3600 * 24);
		$getEnd = date('Y-m-d',time());
		
		
		//$storeName	=	'Rainbow Retail';//$this->request->data['getStoreSelection'];
		
		$status = array(0,1);
		$params	=	array(
					'joins' => array(
							array(
								'table' => 'merge_updates',
								'alias' => 'MergeUpdate',
								'type' => 'INNER',
								'conditions' => array(
									'OpenOrder.num_order_id = MergeUpdate.order_id'
								)
							),
							array(
								'table' => 'scan_orders',
								'alias' => 'ScanOrder',
								'type' => 'INNER',
								'conditions' => array(
									'MergeUpdate.product_order_id_identify = ScanOrder.split_order_id'
								)
							)
						),
					'conditions' => array( 
									'OpenOrder.status' => $status,
									'OpenOrder.date >=' => $getFrom,
									'OpenOrder.date <=' => $getEnd,
									),
					'fields' => array('SUM(ScanOrder.quantity) AS QUANTITY','ScanOrder.sku','OpenOrder.sub_source'),
					'group' => 'ScanOrder.sku, OpenOrder.sub_source', 
					'order' => array('QUANTITY DESC') 
				);
				
		$getOpenOrderDetails		=	$this->OpenOrder->find( 'all', $params );
		pr( $getOpenOrderDetails );
		exit;
		foreach( $getOpenOrderDetails as $getOpenOrderDetailValue )
		{
			pr( $getOpenOrderDetailValue );
			exit;
			
			$soldStock	=	$getOpenOrderDetailValue['0']['QUANTITY'];
			$sku		=	$getOpenOrderDetailValue['ScanOrder']['sku'];
			$subSource	=	$getOpenOrderDetailValue['OpenOrder']['sub_source'];
			
			$this->SkuPercentRecord->updateAll( 
											array( 
												'SkuPercentRecord.select_day_sold' => $soldStock,  
												),
											array(
												'SkuPercentRecord.sku' => $sku,
												'SkuPercentRecord.store_name' => $subSource
												)
											);
		}
	}*/
	
	/**************************************************************************************/
	
	public function manageStock()
    {
		$this->layout = 'index';
		$this->loadModel('SkuPercentRecord');
		$this->loadModel('Store');
		$this->loadModel('Product');
		
		$this->Store->unbindModel(array('belongsTo' => array('Company')), true);
		
		$getStores 	=		$this->Store->find('all', array( 'conditions' => array('Store.status' => 1), 'order' => 'Store.store_name ASC' ));
		$perPage	=	 	'20';//Configure::read( 'perpage' );
		
		$this->paginate = array(
					  'limit' => $perPage,			
                      'fields' => array('Product.product_sku', 'Product.current_stock_level', 'Product.store_name'),	
         );
         
        $getskusDetail = $this->paginate( 'Product' );
          
		App::uses( 'Folder', 'Utility');
		App::uses( 'File', 'Utility' );
		$imgPath = WWW_ROOT .'img/storesFeed/'; 
		$dir = new Folder($imgPath, true, 0755);
		$files = $dir->find('.*\.csv');
		$files = Set::sort($files, '{n}', 'DESC');
		$this->set('files', $files);
		$this->set( compact('getskusDetail' , 'getStores' ) );
	}
	
	
	/*public function manageStock()
    {
		$this->layout = 'index';
		$this->loadModel('SkuPercentRecord');
		$this->loadModel('Store');
		$this->loadModel('Product');
		
		$this->Store->unbindModel(array('belongsTo' => array('Company')), true);
		
		$getStores 	=		$this->Store->find('all', array( 'conditions' => array('Store.status' => 1), 'order' => 'Store.store_name ASC' ));
		$perPage	=	 	'20';//Configure::read( 'perpage' );
		
		$this->paginate = array(
					  'limit' => $perPage,			
                      'fields' => array('Product.product_sku', 'Product.current_stock_level', 'Product.store_name'),	
         );
         
         $getskusDetail = $this->paginate( 'Product' );
        App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		$imgPath = WWW_ROOT .'img/storesFeed/'; 
		$dir = new Folder($imgPath, true, 0755);
		$files = $dir->find('.*\.csv');
		$files = Set::sort($files, '{n}', 'DESC');
		$this->set('files', $files);
		$this->set( compact('getskusDetail' , 'getStores' ) );
	}*/
	
	public function getAllSkuDetail()
	{
		$this->loadModel('SkuPercentRecord');
		$splitSku	=	explode( '____', $this->request->data['skuString'] );
		$getSkuFeedDeatil = $this->SkuPercentRecord->find( 'all', array( 'conditions' => array( 'SkuPercentRecord.sku' => $splitSku ) ) );
		echo json_encode(array( 'status' => '1', 'data' => $getSkuFeedDeatil ));
		exit;
	}
	
	public function getSoldByStoreOpenOrder( $sku, $storeName )
	{
			
			$this->loadModel('OpenOrder');
			$getdetails	=	$this->OpenOrder->find( 'first' , array( 
																'conditions' => array( 'OpenOrder.items like' =>"%{$sku}%",
																					   'OpenOrder.sub_source' =>"{$storeName}",
																					   'OpenOrder.status' => 0
																		),
																'fields' => array('OpenOrder.items','OpenOrder.sub_source') 
																	)
												);
				$bundalQty = 0;	
				if( !empty( $getdetails ) )
				{
						$items['sub_source']	 = $getdetails['OpenOrder']['sub_source'];
						$items['items']			 = unserialize($getdetails['OpenOrder']['items']);
						
						foreach( $items['items'] as $item)
						{
							$bundalQty = $bundalQty + $item->Quantity;
						}
				}
				else
				{
					$bundalQty = 0;
				}
			return $bundalQty;
	}
	
	public function searchFeedBySku()
	{	
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'Product' );
		$this->loadModel( 'Store' );
		
		$getStores 	=		$this->Store->find('all', array( 'conditions' => array('Store.status' => 1), 'order' => 'Store.store_name ASC' ));
		$searchStringSku =	$this->request->data['searchString'];
		$getSerchResult = $this->Product->find( 'all', array( 
														'conditions' => array( 'Product.product_sku' => $searchStringSku ),
														'fields' => array( 'Product.product_sku', 'Product.current_stock_level') 
														) 
													);
		$this->set('getskusDetail', $getSerchResult);
		$this->set('getStores', $getStores);
		$this->render( 'get_feed_search_result' );
	}
	
	
	/************************300616*********************************/
	
	public function importDataFeedDetail()
	{
		$this->layout = 'index';
		$this->loadModel( 'Product' );
		$this->loadModel( 'SkuPercentRecord' );
		$bufferAndNewArray  = array();
		
		if(!empty( $this->data['FeedUpload']['Import_file'] ) && $this->data['FeedUpload']['Import_file']['name'] != '')
		{
			$exFetchFile	=	explode('/',$this->request->data['FeedUpload']['uploadFile']);
			$exFetchFile	=	end( $exFetchFile );
			$filename = WWW_ROOT. 'files'.DS.$this->request->data['FeedUpload']['Import_file']['name']; 
			move_uploaded_file($this->request->data['FeedUpload']['Import_file']['tmp_name'],$filename);  
			$uploadfileName			=	 $this->request->data['FeedUpload']['Import_file']['name'];
			App::import('Vendor', 'PHPExcel/IOFactory');
				
			$objPHPExcel = new PHPExcel();
			
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/'.$uploadfileName );
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			
			if( $exFetchFile == 'bufferQuantity' )
			{
				
				$bufferAndNewArray = array('sku' =>'Sku', 'buffer' => 'Buffer value', 'sku_status' => 'Sku new status');
				for($i=0;$i < $colNumber;$i++) 
					{
						$dataArray[] = $objWorksheet->getCellByColumnAndRow($i,1)->getValue();
					}
				$this->set( 'dataArray', $dataArray );
				$this->set( 'columnName', $bufferAndNewArray );
				$this->set( 'uploadfileName', $uploadfileName );
				$this->set( 'exFetchFile', $exFetchFile );
				//unset( $bufferAndNewArray );
			}
			
			if( $exFetchFile == 'channelPercentage' )
			{
				
				$bufferAndNewArray = array('sku' =>'Sku', 'store_name' => 'Sub Source', 'percentage' => 'Percentage');
				for($i=0;$i < $colNumber;$i++) 
					{
						$dataArray[] = $objWorksheet->getCellByColumnAndRow($i,1)->getValue();
					}
				$this->set( 'dataArray', $dataArray );
				$this->set( 'columnName', $bufferAndNewArray );
				$this->set( 'uploadfileName', $uploadfileName );
				$this->set( 'exFetchFile', $exFetchFile );
				//unset( $bufferAndNewArray );
			}
			if( $exFetchFile == 'storeMapSku' )
			{
				
				$bufferAndNewArray = array('sku' =>'Sku', 'store_name' => 'Sub Source');
				for($i=0;$i < $colNumber;$i++) 
					{
						$dataArray[] = $objWorksheet->getCellByColumnAndRow($i,1)->getValue();
					}
				$this->set( 'dataArray', $dataArray );
				$this->set( 'columnName', $bufferAndNewArray );
				$this->set( 'uploadfileName', $uploadfileName );
				$this->set( 'exFetchFile', $exFetchFile );
				//unset( $bufferAndNewArray );
			}
			
		}
			
		
	}
	
	public function UploadFeedDetail()
	{
		$this->loadModel( 'SkuPercentRecord' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'Store' );
		
		$uploadfileName			=	 $this->request->data['FeedUpload']['upoadName'];
		$exFileType				=	 $this->request->data['FeedUpload']['uploadType'];
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		$objPHPExcel = new PHPExcel();
		$objReader= PHPExcel_IOFactory::createReader('CSV');
		$objReader->setReadDataOnly(true);				
		$objPHPExcel=$objReader->load('files/'.$uploadfileName );
		$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
		$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
		$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
		$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
		$columnCount 	=	count( $this->request->data['FeedUpload']['column'] );
		$count = 0;
		
		for($i=2;$i<=$lastRow;$i++) 
			{
				if( $exFileType == 'bufferQuantity')
				{
					for($j = 0; $j < $columnCount; $j++)
					{
						$value = $this->request->data['FeedUpload']['column'][$j];
						if( $value == 'sku' )
						{
							$sku	=	$objWorksheet->getCellByColumnAndRow($j,$i)->getValue();
						}
						else if( $value == 'buffer' )
						{
							$buffer  = 	$objWorksheet->getCellByColumnAndRow($j,$i)->getValue();
						}
						else
						{
							$newStatus  = 	$objWorksheet->getCellByColumnAndRow($j,$i)->getValue();
						}
					}
					
					$this->SkuPercentRecord->updateAll( 
															array(
																'SkuPercentRecord.buffer' => $buffer,
																'SkuPercentRecord.sku_status' => $newStatus,
																), 
															array(
																'SkuPercentRecord.sku' => $sku,
																)
														  );
					$count++;
					$msg = "Sku buffer quantity update.";
			  }
			  else if( $exFileType == 'channelPercentage')
			  {
				 
				  	for($j = 0; $j < $columnCount; $j++)
					{
						$value = $this->request->data['FeedUpload']['column'][$j];
						if( $value == 'sku' )
						{
							$sku	=	$objWorksheet->getCellByColumnAndRow($j,$i)->getValue();
						}
						else if( $value == 'store_name' )
						{
							$subSource  = 	$objWorksheet->getCellByColumnAndRow($j,$i)->getValue();
						}
						else if( $value == 'percentage' )
						{
							$skuPercentage  = 	($objWorksheet->getCellByColumnAndRow($j,$i)->getValue() != '') ? $objWorksheet->getCellByColumnAndRow(2,$i)->getValue() : '100';
						}
					}
					
					$this->SkuPercentRecord->updateAll( 
															array(
																'SkuPercentRecord.percentage' => $skuPercentage
																), 
															array(
																'SkuPercentRecord.store_name' => $subSource,
																'SkuPercentRecord.sku' => $sku,
																)
														  );
					$count++;
					$msg = "Sku channel percentage";

			  }
			  else if( $exFileType == 'storeMapSku' )
			  {
				  
				  for($j = 0; $j < $columnCount; $j++)
					{
						$value = $this->request->data['FeedUpload']['column'][$j];
						if( $value == 'sku' )
						{
							$sku	=	$objWorksheet->getCellByColumnAndRow($j,$i)->getValue();
						}
						else if( $value == 'store_name' )
						{
							$subSource  = 	$objWorksheet->getCellByColumnAndRow($j,$i)->getValue();
						}
						
					}
					
					$getId		=	$this->Store->find( 'first', array( 'conditions' => array( 'Store.store_name'=> $subSource), 'fields' => array('Store.id') ) );
				
					$getStoreId	=	$this->Product->find( 'first', array( 'conditions' => array( 'Product.product_sku' => $sku ), 'fields' => array( 'Product.store_name' ) ) );
					
					if( !empty( $getStoreId['Product']['store_name'] ) && $getStoreId['Product']['store_name'] != '' || $getStoreId['Product']['store_name'] != '0')
					{
						
						$a = array();
						$getStore = implode(',',array_unique(explode(',',$getStoreId['Product']['store_name'])));
						$b = array_push($a, $getStore, $getId['Store']['id'] );
						$storeIds	=	implode(',', array_unique($a) );
						$this->Product->updateAll( 
												array(
													'Product.store_name' => "'{$storeIds}'"
													), 
												array(
													'Product.product_sku' => "{$sku}"
													)
											  );
						
					}
					else
					{
						$storeId = $getId['Store']['id'];
						$this->Product->updateAll( 
												array(
													'Product.store_name' => "'{$storeId}'"
													), 
												array(
													'Product.product_sku' => "{$sku}"
													)
											  );
					}
					$count++;
					$msg = "Sku Maped with store";
			  }
			}
			
			$this->Session->setFlash($count.$msg, 'flash_success');
			$this->redirect( array( 'controller' => 'Virtuals', 'action' => 'importDataFeedDetail' ) );
	}
	
	public function exportDataFeedDetail()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'SkuPercentRecord' );
		$this->loadModel( 'Product' );
		
		$this->loadModel( 'Store' );
		
		$getDetails	=	$this->SkuPercentRecord->find('all');
		
		$this->Store->unbindModel(array('belongsTo' => array('Company')), true);
		$getStores	=	$this->Store->find('all', array( 'conditions' => array( 'Store.status' => '1' ) ));
		
		$this->Product->unbindModel( array(
										'hasOne' => array(
											'ProductDesc' , 'ProductPrice' 
											),
										'hasMany' => array(
											'ProductLocation'
											)		
										) 
									 );
									 
		$productSkus = $this->Product->find('all', array( 'fields' => array( 'Product.product_sku', 'Product.current_stock_level' ) ) );
		$alphabet = array('G','H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
									'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN');
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'SKU');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'NEW SKU');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'TOTAL SOLD');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '30 DAY SOLD');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'CURRENT STOCK');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'BUFFER');
		
		$i = 0;
		foreach( $getStores as $getStoresValue)
			{
				$objPHPExcel->getActiveSheet()->setCellValue( $alphabet[$i].'1', $getStoresValue['Store']['store_name']);
				$i++;
			}
		$k = 2;
		foreach( $productSkus as $productSkuValue)
		{
			$getDetails	=	$this->SkuPercentRecord->find('all', array( 'conditions' => array('SkuPercentRecord.sku' => $productSkuValue['Product']['product_sku'] ) ) );
			$inc = 2;
			$currentStock = $productSkuValue['Product']['current_stock_level'];
			foreach( $getDetails as $getDetailValue )
				{
					
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$k, $getDetailValue['SkuPercentRecord']['sku']);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$k, $getDetailValue['SkuPercentRecord']['sku_status']);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$k, $getDetailValue['SkuPercentRecord']['sold_stock']);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$k, $getDetailValue['SkuPercentRecord']['fixed_day_sold']);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$k, $currentStock);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$k, $getDetailValue['SkuPercentRecord']['buffer']);
					$j = 0;
					foreach( $getStores as $getStoresValue)
					{
						if( $getStoresValue['Store']['store_name'] == $getDetailValue['SkuPercentRecord']['store_name'] )
						{
							$objPHPExcel->getActiveSheet()->setCellValue( $alphabet[$j].$k, ($getDetailValue['SkuPercentRecord']['percentage']));
						}
						$j++;
					}
				}
			$inc++;
			$k++;
		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/storesFeed/feedDetail.csv';
		$uploadRemote = $getBase.'img/storesFeed/feedDetail.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'storesFeed/feedDetail.csv';
		
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
		
	}
	
	/************************300616*********************************/
	
	/**************************************** Start Code for po 300616 ***************************************************/
	
	/***************************code for po************************/
	
	
	public function showAllLockedPo()
	{
		$this->layout = 'index';
		$this->loadModel( 'Po' );
		$getAllpo	=	$this->Po->find( 'all', array( 'conditions' => array( 'Po.status' => 0 ) ) );
		
		foreach( $getAllpo as $index => $value )
		{
			if( $value["Po"]["invoice_no"] != '' )
			{
				$getpoList[$value["Po"]["id"]] = $value["Po"]["po_name"]." ( ".$value["Po"]["invoice_no"]." )";
			}
			else
			{
				$getpoList[$value["Po"]["id"]] = $value["Po"]["po_name"];
			}
		}
		
		$this->set( 'getpoList', $getpoList );
	}
	
	/*public function showAllLockedPo()
	{
		$this->layout = 'index';
		$this->loadModel( 'Po' );
		$getpoList	=	$this->Po->find( 'list', array( 'conditions' => array( 'Po.status' => 0 ), 'fields'=> array( 'Po.id','Po.po_name' ) ) );
		$this->set( 'getpoList', $getpoList );
	}*/
	
	public function editPoSkuPopUp()
	{
		$this->layout = '';
		$this->autoRender = false;
		$id = $this->request->data['poSkuId'];
		
		$this->loadModel( 'PurchaseOrder' );
		
		$getPurchaseSkuDetail	=	 $this->PurchaseOrder->find( 'first', array('conditions'=> array( 'PurchaseOrder.id' => $id ) ) );
		
		$this->set( 'getPurchaseSkuDetail', $getPurchaseSkuDetail );
		$this->render('poedit_popup');
	}
	
	public function editPoSku()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'PurchaseOrder' );
		
		//$data['purchase_sku']			=	$this->request->data['productSku'];
		//$data['purchase_code']			=	$this->request->data['productCode'];
		//$data['purchase_description']	=	addslashes($this->request->data['productDesc']);
		$data['purchase_qty']			=	$this->request->data['purchaseQty'];
		$data['id']						=	$this->request->data['poskuID'];
		//$data['purchase_barcode']		=	$this->request->data['productBarcode'];
		// update po sku detail
		
		$this->PurchaseOrder->saveAll( $data );
		echo json_encode(array('status' => 1, 'data' => $data));
		exit;
	}
	
	public function deletePoSkuPopUp()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'PurchaseOrder' );
		$poSkuId	=	$this->request->data['poSkuId'];
		$this->PurchaseOrder->delete( $poSkuId );
		echo $poSkuId;
		exit;
	}
	
	public function addPoSkuPopUp()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'PurchaseOrder' );
		$poName	=	$this->request->data['choosePo'];
		$this->set( 'poName', $poName );
		$this->render('poskuadd_popup');
	}
	
	public function addPoSku() 
	{
			$this->layout = '';
			$this->autoRender = false;
			$this->loadModel( 'PurchaseOrder' );
			$this->loadModel( 'Product' );
			
			$skuDetail['po_name']		=		$this->request->data['poName'];
			$skuDetail['purchase_sku']	=		$this->request->data['productSku'];
			$skuDetail['purchase_code']	=		$this->request->data['productCode'];
			$skuDetail['purchase_qty']	=		$this->request->data['purchaseQty'];
			
			
			
			$getProductdetail =	$this->Product->find('first', array( 
															'conditions' => array( 'Product.product_sku' =>  $this->request->data['productSku']),
															'fields' => array('Product.product_name', 'Product.product_name','ProductDesc.barcode')
															)
														);
			if( count($getProductdetail) > 0 )
			{
				$getpurchaseOrderDetail 	=	$this->PurchaseOrder->find( 'first', array(
																'conditions' => array( 
																'PurchaseOrder.po_name' => $this->request->data['poName'],
																'PurchaseOrder.purchase_sku' => $this->request->data['productSku']
																)
															)
														);
				if( count($getpurchaseOrderDetail) > 0 )
				{
					echo "2";
					exit;
				}
				else
				{
					$skuDetail['purchase_description'] 	=  $getProductdetail['Product']['product_name'];
					$skuDetail['purchase_barcode'] 		=  $getProductdetail['ProductDesc']['barcode'];
					
					$this->PurchaseOrder->saveAll( $skuDetail );
					
					echo "1";
					exit;
				}
			}
			else
			{
				echo "3";
				exit;
			}
	}
	
	public function deletePoAllRecord()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'PurchaseOrder' );
		$this->loadModel( 'Po' );
		
		$poId 			= 	$this->request->data['choosePoId'];
		$poName 		= 	$this->request->data['choosePo'];
		$getPoRecords	=	$this->PurchaseOrder->find( 'all', array( 
														'conditions' => array('PurchaseOrder.po_name' => $poName ),
														'fields'=> ( 'PurchaseOrder.id' ) 
														) 
													);
		if( !empty( $getPoRecords ) )
		{
			foreach( $getPoRecords as $getPoRecordIndex => $getPoRecordValue )
			{
				$id = $getPoRecordValue['PurchaseOrder']['id'];
				$this->PurchaseOrder->delete( $id );
			}
		}
		$this->Po->delete( $poId );
		echo "1";
		exit;
	}
	
	public function getBarcodePrintPopup()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('PurchaseOrder');
		
		$poSkuId		=	$this->request->data['poSkuId'];
		$productBarcode	=	$this->PurchaseOrder->find( 'all', array( 
														'conditions' => array('PurchaseOrder.id' => $poSkuId ),
														'fields'=> array( 'PurchaseOrder.purchase_barcode', 'PurchaseOrder.id' ) 
														) 
													);
		
		$this->set( 'productBarcode', $productBarcode );
		$this->render( 'barcodePrintPopup' );
		
	}
	
	
	/**************************************** End Code for po 300616 ***************************************************/
	/***************************************Start PO ****************************/
	public function saveSkuCount()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'PurchaseOrder' );
		
		
		$saveData['id']				=	$this->request->data['poItemId'];
		$saveData['purchase_count']	=	$this->request->data['poItemQty'];
		
		$result = $this->PurchaseOrder->saveAll( $saveData );
		
		if( $result )
		{
			echo "1";
			exit;
		}
		else
		{
			echo "2";
			exit;
		}
	}
	
	public function saveSkuComment()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'PurchaseOrder' );
		
		$saveData['id']			=	$this->request->data['poSkuId'];
		$saveData['comment_id']	=	$this->request->data['commentId'];
		$saveData['comment']	=	$this->request->data['comment'];
		$result = $this->PurchaseOrder->saveAll( $saveData );
		if( $result )
		{
			echo "1";
			exit;
		}
		else
		{
			echo "2";
			exit;
		}
	}
	
	public function saveInvoiceNumber()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'Po' );
		$this->loadModel( 'PurchaseOrder' );
		
		$poId		=	$this->request->data['poId'];
		$invoiceNo	=	$this->request->data['invoiceNo'];
		
		$poDetail	=	$this->Po->find('first', array(								
								'conditions' => array('Po.invoice_no' => $invoiceNo)
									)
								);
	
		
		if( count( $poDetail ) == 0 )
		{
			$dataForSave['id'] = $poId;
			$dataForSave['invoice_no'] = $invoiceNo;
			$this->Po->saveAll( $dataForSave );
			$this->Session->setFlash('Invoice no. save successfully.', 'flash_success');
			echo "1";
			exit;
		}
		else
		{
			echo "2";
			exit;
		}
	}
	
	public function UploadScanningFilesForPo()
	{
		$this->loadModel( 'ScanInvoice' );
		$this->loadModel( 'Po' );
		$count = 0;
		
		$poId = $this->request->data['Po']['choosePo'];
		$getPoDetail =	$this->Po->find( 'first', array( 'conditions' => array( 'Po.id' =>  $poId ) ) );
		$poName = $getPoDetail['Po']['po_name'];
		
		foreach( $this->request->data['ScanInvoice']['files'] as $value )
		{
			$data['po_id'] 		= 	$poId;
			$data['po_name'] 	= 	$poName;
			$data['file_name']	=	$value['name'];
			$filename = WWW_ROOT. 'img/scanInvoice'.DS.$value['name']; 
			move_uploaded_file( $value['tmp_name'], $filename );  
			$this->ScanInvoice->saveAll( $data );
			$count++;
		}
		$this->Session->setFlash($count.' file uploaded successfully.', 'flash_success');
		$this->redirect($this->referer());
	}
	
	public function selletCommentPopup()
	{
		$this->loadModel( 'PurchaseOrder' );
		
		$poSkuID = $this->request->data['poSkuId'];
		$getPurchaseOrderSkuDetail =	$this->PurchaseOrder->find( 'first', array( 'conditions' => array( 'PurchaseOrder.id' => $poSkuID ) ) );
		$this->set( 'getPurchaseOrderSkuDetail', $getPurchaseOrderSkuDetail );
		$this->render('seller_comment_popup');
	}
	
	public function saveSellerComment()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'PurchaseOrder' );
		
		$sellerData['id'] 				=   $this->request->data['poskuID'];
		$sellerData['seller_comment']	=	$this->request->data['sellerComment'];
		
		$this->PurchaseOrder->saveAll( $sellerData );
		
		echo "1";
		exit;
	}
	
	public function brcodeGenerationPopup()
	{
		$this->loadModel( 'PurchaseOrder' );
		
		$poSkuID	=	$this->request->data['poskuID'];
		
		$getPoSkuDetail =	$this->PurchaseOrder->find( 'first', array( 'conditions' => array( 'PurchaseOrder.id' => $poSkuID ) ) );
		$this->set( 'getPoSkuDetail', $getPoSkuDetail );
		$this->render('barcode_generate_popup');
	}
	public function printSkuBarcode()
	{
		
			$this->layout = '';
			$this->autoRender = false;
			
			$generationQuantity	= $this->request->data['purchaseQty'];
			$barcodeNumber		= $this->request->data['barcode'];
			
			require_once(APP . 'Vendor' . DS . 'src' . DS . 'BarcodeGenerator.php'); 
			require_once(APP . 'Vendor' . DS . 'src' . DS . 'BarcodeGeneratorPNG.php'); 
			require_once(APP . 'Vendor' . DS . 'src' . DS . 'BarcodeGeneratorSVG.php'); 
			require_once(APP . 'Vendor' . DS . 'src' . DS . 'BarcodeGeneratorHTML.php'); 
			
			$generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
			
			$imgPath = WWW_ROOT .'img/GeneratedBarcode/'; 
			$path = Router::url('/', true).'img/GeneratedBarcode/';
			$name = $barcodeNumber.'.png';
			
			$data	=	 'data:image/png;base64,' . base64_encode($generator->getBarcode($barcodeNumber, $generator::TYPE_CODE_128));
			$serverPath  	= 	$path.$barcodeNumber.'.png' ;
			$file = $imgPath.$barcodeNumber.'.png';
			$pdfName = $barcodeNumber;
			
			$data = str_replace('data:image/png;base64,', '', $data);
			$data = str_replace(' ', '+', $data);
			$data = base64_decode($data);
			
			$success = file_put_contents($file, $data);
			
			/*  generate pdf  for barcode */
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();
			$dompdf->set_paper(array(0, 0, 188, 71), 'portrait');
			$cssPath = WWW_ROOT .'css/';
			
				$html = '<meta charset="utf-8">
								 <meta http-equiv="X-UA-Compatible" content="IE=edge">
								 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
								 <meta content="" name="description"/>
								 <meta content="" name="author"/><body>
									<div id="label">
									   <div class="container">
										  <table style="padding:5px 0;">
											 <tr>
												<td valign="top" width="100%" style="font-size:16px; text-align:center;" >
													<img  src="'.$serverPath.'" width="150px" height="37px">
													'.$barcodeNumber.'
												</td>
											 </tr>

											 
										  </table>
									   </div>
									</div></body>';
					
				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
			
				//echo $html;
				//exit;
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				$dompdf->stream($pdfName.'.pdf');
				$imgPath1 = WWW_ROOT .'img/GeneratedBarcode/'; 
				$path1 = Router::url('/', true).'img/GeneratedBarcode/';
				$date = new DateTime();
				$timestamp = $date->getTimestamp();
				$name	=	$pdfName.'.pdf';
							
				file_put_contents($imgPath1.$name, $dompdf->output());
				$serverPath  	= 	$path1.$name ;
				$printerId		=	$this->getDYMO();
				
				$sendData = array(
					'printerId' =>$printerId,
					'title' => 'Now Print',
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'
				);
				
			for( $i = 1; $i <= $generationQuantity; $i++ )
			{
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				$d = $Coreprinter->toPrintDymo( $sendData );
				
				echo $i.'=='.$d.'\n';
				sleep(2);
				
			}
			
			echo "1";
			exit;
		
		
	}
	
	public function getcommentPopup()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'Product' );
		$productId	=	$this->request->data['productId'];
		
		$productBarcode	=	$this->Product->find( 'first', array( 
								'conditions' => array( 'Product.id' =>  $productId),
								'fields' => array( 'Product.id, Product.comment' )
								) );
		$this->set( 'productBarcode', $productBarcode );
		$this->render( 'commentPopup' );
		
	}
	
	public function setProductComment()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'Product' );
		
		$productId		=	$this->request->data['productId'];
		$productComment	=	$this->request->data['productComment'];
		
		$this->Product->updateAll( 
								array(
									'Product.comment' => "'{$productComment}'"
									), 
								array(
									'Product.id' => "{$productId}"
									)
								 );
			
		echo "1";
		exit;
		
	}
	
	public function flushComment()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'Product' );
		$productId		=	$this->request->data['productId'];
		$productComment = '';
		
		$this->Product->updateAll( 
								array(
									'Product.comment' => "'{$productComment}'"
									), 
								array(
									'Product.id' => "{$productId}"
									)
								 );
			
		echo "1";
		exit;
		
	}
	
	public function customInventeryUpdate()
	{
		$this->layout = 'index';
		
	}
	
	public function setCustomStockOnDemand()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'Product' );
		$this->loadModel( 'BinLocation' );
		$this->loadModel( 'CustomInventoryRecord' );
		$this->loadModel( 'InventoryRecord' );
		
		$barcodeSku				=	$this->request->data['barcodeSku'];
		$quantity				=	$this->request->data['quantity'];
		$action					=	$this->request->data['getStoreSelection'];
		$userName				=	$this->request->data['userName'];
		$comment				=	$this->request->data['comment'];
		
		$getProductDetail	=	$this->Product->find( 'first', array( 'conditions' => array(
												'OR'=> array(
												array('Product.product_sku' => $barcodeSku),
												array('ProductDesc.barcode' => $barcodeSku)
													)
												) ) );
												
		if( count( $getProductDetail ) > 0 )
		{
			$product_quantity	=	$getProductDetail['Product']['current_stock_level'];
			$productId			=	$getProductDetail['Product']['id'];
			$barcode			=	$getProductDetail['ProductDesc']['barcode'];
			$productSku			=	$getProductDetail['Product']['product_sku'];
			$currStock			=	$product_quantity;
			if( $product_quantity <= 0 )
			{
				$product_quantity = 0;
			}
			
			if( $quantity <= 0 )
			{
				echo "Kindly input atleast 1 quantity";
				exit;
			}
			
			//process start
			if( $action == 0 )
			{
				$product_quantity = $product_quantity + $quantity;
				$getAction 	=	'Increment';
			}
			else if( $action == 1 )
			{
				$product_quantity = $product_quantity - $quantity;				
				if( $product_quantity <= 0 )
				{
					$product_quantity = 0;
					
				}
				$getAction 	=	'Decrement';				
			}
			else
			{}
			
			$getBinLocationDetail	=	$this->BinLocation->find( 'all', array( 'conditions' => array(
												'BinLocation.barcode'	=>  $barcode
												),
												'order' =>'BinLocation.stock_by_location DESC',
												'limit' => 1
												 ));
			
			if( count($getBinLocationDetail) > 0 )
			{
				
				$this->Product->updateAll( 
					array(
						'Product.current_stock_level' => "'{$product_quantity}'"
						), 
					array(
						'Product.id' => "{$productId}"
						)
					 );
					
				$stockLocationId = $getBinLocationDetail[0]['BinLocation']['id'];
				$stockLocationValue = $getBinLocationDetail[0]['BinLocation']['stock_by_location'];
				$stockLocation = $getBinLocationDetail[0]['BinLocation']['bin_location'];
				if($stockLocationValue <= 0)
				{
					$stockLocationValue = 0;
				}
				
				//process start
				if( $action == 0 )
				{
					$stockLocationValue = $stockLocationValue + $quantity;
				}
				else if( $action == 1 )
				{
					$stockLocationValue = $stockLocationValue - $quantity;
					if( $stockLocationValue <= 0 )
					{
						$stockLocationValue = 0;
					}				
				}
				else
				{}
				
				$this->BinLocation->updateAll( 
					array(
						'BinLocation.stock_by_location' => "'{$stockLocationValue}'"
						), 
					array(
						'BinLocation.id' => "{$stockLocationId}"
						)
					 );
					 
				// save action in table
				$data['current_stock'] 	= 	$product_quantity;
				$data['sku'] 			= 	$productSku;
				$data['quantity'] 		= 	$quantity;
				$data['action']			=	$getAction;
				$data['user_name']		=	$userName;
				$data['comment']		=	$comment;
				$data['action_date'] 	= 	date('Y-m-d H:i:s');
				
				$this->CustomInventoryRecord->saveAll( $data );
				
				
				//action_type, sku, barcode, currentStock, quantity, after_maniplation, split_order_id
				$currentquantity	=	$product_quantity;
				
				$updateData['action_type']			=	$getAction;
				$updateData['sku']					=	$productSku;
				$updateData['barcode']				=	$barcode;
				$updateData['currentStock']			=	$currStock;
				$updateData['quantity']				=	$quantity;
				$updateData['after_maniplation'] 	=	$currentquantity;
				$updateData['split_order_id'] 		= 	'-';
				$updateData['date'] 	= 	date('Y-m-d');
				
				$this->InventoryRecord->saveAll( $updateData );
				
				if( $action == 0 )
				{
					echo "Put item ( " . $quantity . " ) onto this shelf =>> ( " . $stockLocation ." )";
					exit;		
				}
				else
				{
					echo "Items managed";
					exit;		
				}
			}
		}
		else
		{
			echo "No result found.";
			exit;
		}	
	}
	
	/*******************************************End Po*******************************************/
}

?>
