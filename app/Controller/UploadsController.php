<?php
error_reporting(0);
ini_set('max_execution_time', 40);
//ini_set('memory_limit', '-1');
class UploadsController extends AppController
{

    var $name = "Uploads";
    var $components = array('Session','Upload','Common','Auth');
    var $helpers = array('Html','Form','Common','Session');
	public $funcName;
	private static $fnm;
	 
	public function UploadsCsvFilesForInventery()
	{
  		$this->autoLayout = 'upload';
		$this->autoRender = false;
		
		$this->loadModel('Store');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('ProductPrice');
		$this->loadModel('ProductImage');
		$this->loadModel('AttributeOption');
		$this->loadModel('ProductBarcode');
		$this->loadModel('ProductCsv');
		$this->loadModel('SkuFbmFba');
		$this->loadModel('ProductHscode'); 
 		$this->loadModel('SupplierMapping'); 
		$this->loadModel('SupplierDetail');		
		$this->loadModel('Category');
		$this->loadModel('ProductFilesLog');
		$this->loadModel('Skumapping');
 		$this->loadModel('Country');
		//$this->data['Product']['Import_file']['name'];
 		if(empty($this->request->data['template'])){
			$this->Session->setFlash('Please select template type.', 'flash_danger');
			$this->redirect($this->referer());
		}
		
		if($this->data['Product']['Import_file']['name'] != '')
		{
			$path_info = pathinfo($this->request->data['Product']['Import_file']['name']);

			if(strtolower($path_info['extension']) != 'csv'){ 
				$this->Session->setFlash('Please upload csv file.', 'flash_danger');
				$this->redirect($this->referer());
 			} 
 			$username   = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
			$main_file 	= $this->request->data['Product']['Import_file']['name'];	
			$name		= $this->Session->read('Auth.User.first_name').'_'.$this->Session->read('Auth.User.last_name').'_'.date('ymdhis');
 			$filename   =  WWW_ROOT. 'files'.DS.$name.$main_file;
			move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
			 
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel = $objReader->load('files/'.$name.$main_file);
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
			$lastRow    = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	= $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber  = PHPExcel_Cell::columnIndexFromString($colString);
			
  			$countbarcode	=	0;
			$frizileMark	=	0;
 			$update_msg 	=   [];
			$success_msg 	=	[];	
			$error_msg 		=   [];
 			$product_csv_ids= 	[];
			$channelData	=	[];
			$message		= 	[];
			$skus			=	[]	;
			$CountryCodeArr =  $this->getChannelCountry(); 
			$status_file 	=  $this->request->data['template'].'-'.date('Ymdhis').'.csv';
			if($this->request->data['template'] == 'CoreUploadFields'){
				file_put_contents(WWW_ROOT.'logs'.DS.$status_file,"Product sku,Upload Status"."\n", LOCK_EX);

 				for($i=2; $i<=$lastRow; $i++) 
				{ 				 	
					if(in_array(strtolower($objWorksheet->getCellByColumnAndRow(1,$i)->getValue()),['add','edit'])){
						$empty 			=   [];
						$productdata = []; $productdscdata = [];
						$global_barcode = ''; $product_sku = '';
						if(trim($objWorksheet->getCellByColumnAndRow(4,$i)->getValue()) != ''){	
							for($j = 0; $j < 13; $j++) {
								$global_barcode .= mt_rand(1, 9);
							}
							$_xsbarcode = trim($objWorksheet->getCellByColumnAndRow(4,$i)->getValue());
							if(substr($_xsbarcode,0,1) == '~'){
								$xsbarcode = str_replace('~','',$_xsbarcode); 
							}else{
								$xsbarcode = $_xsbarcode; 
							}
 							if(!is_numeric($xsbarcode)) {
								$empty[] = 'UPC/Barcode should be numeric.';
							}
  						}else{
							$empty[] = 'UPC/Barcode can not be empty.';
						}
						
						if(trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue()) != ''){
							$product_sku 		=	trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
							$productdata['product_sku'] = $product_sku;
							$skus[$productdata['product_sku']] = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
							 if(substr($product_sku,0,1) == 'B'){
								 $empty[] = 'Please check product sku.';
							 }
						}else{
							$empty[] = 'Product sku can not be empty.';
						}
 						if(trim($objWorksheet->getCellByColumnAndRow(2,$i)->getValue()) != ''){
							$productdata['product_name']		=	addslashes($objWorksheet->getCellByColumnAndRow(2,$i)->getValue());		
						}else{
							$empty[] = 'Product Title can not be empty.';
						}
 						if(trim($objWorksheet->getCellByColumnAndRow(5,$i)->getValue()) != ''){	
							$productdata['category']			=	addslashes($objWorksheet->getCellByColumnAndRow(5,$i)->getValue());
						}else{
							$empty[] = 'Category can not be empty.';
						}
 						 
						$productdata['product_status'] = 0;
						 
 						if(trim($objWorksheet->getCellByColumnAndRow(13,$i)->getValue()) != ''){	
							$iso_2  = trim($objWorksheet->getCellByColumnAndRow(13,$i)->getValue());
							$getiso = $this->Country->find( 'first' , array( 'conditions' => array( 'iso_2' => trim($iso_2) ) ) );
							if(count($getiso) > 0){
								$productdata['country_of_origin']	=	trim($iso_2);
							}else{
								$empty[] = 'Country of origin should be ISO 2 format.';
							}
						}else{
							$empty[] = 'Country of origin can not be empty.';
						}
 						if(trim($objWorksheet->getCellByColumnAndRow(10,$i)->getValue()) != ''){
							$cell_val = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
							$productdata['route']				=	is_null($cell_val) ? '1' : $cell_val;
						}else{
							$empty[] = 'Dangerous Good? Route can not be empty.';
						}
						
						if(trim($objWorksheet->getCellByColumnAndRow(3,$i)->getValue()) != ''){	
							$productdscdata['brand']			=	addslashes($objWorksheet->getCellByColumnAndRow(3,$i)->getValue());
						}else{
							$empty[] = 'Brand can not be empty.';
						}
						
 						if($objWorksheet->getCellByColumnAndRow(6,$i)->getValue() != ''){
							$productdscdata['weight']			=	$objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
						}
						if($objWorksheet->getCellByColumnAndRow(7,$i)->getValue() != ''){
							$productdscdata['length'] 			=	$objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
						}
						if($objWorksheet->getCellByColumnAndRow(8,$i)->getValue() != ''){
							$productdscdata['width'] 			=	$objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
						}
						if($objWorksheet->getCellByColumnAndRow(9,$i)->getValue() != ''){
							$productdscdata['height']				=	$objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
						}
						if($objWorksheet->getCellByColumnAndRow(11,$i)->getValue() != ''){
							$productdscdata['frezile_mark']		=	$objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
						}else{
							$empty[] = 'Fragile Mark can not be empty.';
						}
						if($objWorksheet->getCellByColumnAndRow(12,$i)->getValue() != ''){
							$hscode								=	$objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
						}else{
							$empty[] = 'HSN code can not be empty.';
						}						 
						if($objWorksheet->getCellByColumnAndRow(14,$i)->getValue() != ''){
							$productdata['export_invoice_title'] =	$objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
						}else{
							$empty[] = 'Export Invoice Title can not be empty.';
						}
	
						$product_type = 'Single';
						$product_identifier = 'Single';
						$product_defined_skus = '';
						//$productdata['product_sku'] = 'B-0621B026-0628B001-0620B001-3';
						//echo $productdata['product_sku'] = 'B-06245#BC-4';
						if(substr($product_sku,0,1) == 'B'){
							$_sk = explode("-",$product_sku);
							$ind = count($_sk) - 1;
							$_skus = [];
							for($b = 1; $b < $ind  ; $b++){
								$_skus[] = 'S-'.$_sk[$b];
							}
							$product_type = 'Bundle';
							if(count($_sk ) > 3){
								$product_identifier = 'Multiple';
							}
							$product_defined_skus = implode(":",$_skus);  
						}
					 
						$productdscdata['product_type']				=	$product_type;//$objWorksheet->getCellByColumnAndRow(31,$i)->getValue();
						$productdscdata['product_defined_skus']		=	$product_defined_skus;//$objWorksheet->getCellByColumnAndRow(32,$i)->getValue();
						$productdscdata['product_identifier']		=	$product_identifier;//$objWorksheet->getCellByColumnAndRow(33,$i)->getValue();
						
 						if(count($empty) == 0){
						
							if(!empty($product_sku) && $hscode != ''){
								$ccode = 'GB';
								$checHs = $this->ProductHscode->find('first', array(
																			'conditions' => array(
																				'sku' => $product_sku,
																				'country_code' => $ccode
																				),
																			 'fields' =>['id']
																			)
																		); 
								
								$store['sku']    		= $product_sku;
								$store['country_code']  = $ccode;
								$store['hs_code']		= $hscode;
								$store['username'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
								if(count($checHs) > 0)
								{
									$store['id']			= 	$checHs['ProductHscode']['id'];
									$store['timestamp']		= 	date('Y-m-d H:i:s');
									$this->ProductHscode->saveAll($store);
								}else{
									$store['added_date']	= 	date('Y-m-d H:i:s');
									$this->ProductHscode->saveAll($store);
								}
							}
									
							// Check also brand is exists or not
							$this->loadModel( 'Brand' );
							$getBrand = $this->Brand->find( 'first' , array( 'conditions' => array( 'Brand.brand_name' => $productdscdata['brand'] ) ) );
							
							if( count( $getBrand ) == 0 )
							{
								$brandName = $productdscdata['brand'];
								$brandAlias = strtolower($brandName);					
								$this->Brand->query( "insert into brands ( brand_name, brand_alias ) values ( '{$brandName}' , '{$brandAlias}' )" );					
								$productdscdata['brand'] = $brandName;
								
							}
							else
							{
								$productdscdata['brand'] = $productdscdata['brand'];
							}
							
							if($objWorksheet->getCellByColumnAndRow(15,$i)->getValue() != ''){
								$supplier_code =  trim($objWorksheet->getCellByColumnAndRow(15,$i)->getValue());
								$getSupp = $this->SupplierDetail->find( 'first' , array( 'conditions' => array( 'supplier_code' => $supplier_code ),'fields' =>['purchaser','id'] ) );
								if(count( $getSupp ) > 0 )
								{
									$getSupm = $this->SupplierMapping->find( 'first' , array( 'conditions' => array( 'sup_code' => $supplier_code,'master_sku' => $product_sku ),'fields' =>['id'] ) );
									if(count($getSupm) == 0){
										$supmap = [];
										$supmap['sup_inc_id'] 	= $getSupp['SupplierDetail']['id'];
										$supmap['sup_code'] 	= $supplier_code;
										$supmap['master_sku'] 	= $product_sku;
										$supmap['supplier_sku'] = trim($objWorksheet->getCellByColumnAndRow(16,$i)->getValue());
										$supmap['added_date'] 	= date('Y-m-d H:i:s'); 
										$supmap['username']	 	= $username;
										$this->SupplierMapping->saveAll($supmap);
									}
 									$productdscdata['purchaser'] = $getSupp['SupplierDetail']['purchaser'];
								}
							}
		
							
							// Check also Category is exist or note
							$getCategory = $this->Category->find( 'first' , array( 'conditions' => array( 'Category.category_name' => $productdata['category'] ) ) );
							if( count( $getCategory ) == 0 )
							{
								$categoryName = $productdata['category'];
								$catAlias = strtolower($categoryName);					
								$this->Category->query( "insert into categories ( category_name, category_alias ) values ( '{$categoryName}' , '{$catAlias}' )" );					
								$catId = $this->Category->getLastInsertId();
								$productdata['category_id'] = $catId;
								$productdata['category_name'] = $categoryName;
							}
							else
							{
								$productdata['category_id'] = $getCategory['Category']['id'];
								$productdata['category_name'] = $productdata['category'];
							}
							
							$productdata['core_update'] = date('Y-m-d H:i:s');
							$productdata['core_upload'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name'); 
							
							$checkproduct		=	$this->Product->find('first', array(
																'conditions' => array('Product.product_sku' => $productdata['product_sku'])
																)
															); 
							if(strtolower($objWorksheet->getCellByColumnAndRow(1,$i)->getValue()) == 'add'){
								$pbarcode =	$this->ProductBarcode->find('first', array(
																'conditions' => array('barcode' => $xsbarcode)
																)
															); 
								if(count($pbarcode) > 0){
									$error_msg[] = $product_sku.' not added. #'.$xsbarcode .' is already exists.';
									file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",Sku not added.".$xsbarcode.' is already exists.'."\n",FILE_APPEND | LOCK_EX);

								}else{
									if(count($checkproduct) == 0)
									{
										try{
											$productdata['added_date'] = date('Y-m-d H:i:s');
											$productdata['added_by']   = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
											if($this->Product->saveAll($productdata)){
												//$this->Product->saveAll($productdata);
 											
												$productid	=	$this->Product->getLastInsertId();
											
												$success_msg[]  = $productdata['product_sku'].' is added.';
												file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$product_sku.' is added.'."\n",FILE_APPEND | LOCK_EX);
		
												$productdscdata['product_id'] = $productid;
												try{
													$productdscdata['barcode'] = $global_barcode;
													$this->ProductDesc->saveAll($productdscdata, array('validate' => false));
												}catch (Exception $e) {
													$error_msg[] = 'Caught exception: '.  $e->getMessage(). "\n";
												} 
												
												$productDescid	=	$this->ProductDesc->getLastInsertId();
													
												$barcodeData = [];
												$barcodeData['global_barcode']  = $global_barcode;
												if($xsbarcode == '' ){	
													$barcodeData['barcode'] = $global_barcode; 
												} 
												else{
													$barcodeData['barcode'] = $xsbarcode;
												}
												/************************Save Global barcode*********************************/
												$barcodeData['username'] 		= $this->Session->read('Auth.User.username');
												$barcodeData['added_date'] 		= date('Y-m-d H:i:s');
												$this->ProductBarcode->saveAll($barcodeData);
												/*****************************************/
											}
										}
										catch (Exception $e) {
											$error_msg[] =  'Caught exception: '.  $e->getMessage(). "\n";
										}
 									}
									else
									{
										$error_msg[] = 'SKU #'.$productdata['product_sku'] .' is already exists.';
										file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$product_sku.' is already exists.'."\n",FILE_APPEND | LOCK_EX);
  									}
								}
							}
							else if(strtolower($objWorksheet->getCellByColumnAndRow(1,$i)->getValue()) == 'edit'){
								if(count($checkproduct) > 0){
									$product_barcode = '';
									$pbarcode =	$this->ProductBarcode->find('first', array(
																	'conditions' => array('barcode' => $xsbarcode)
																	)
																); 
									if(count($pbarcode) > 0){
										$product_barcode = $pbarcode['ProductBarcode']['barcode'];
										if($pbarcode['ProductBarcode']['global_barcode'] != $checkproduct['ProductDesc']['barcode']){
											$error_msg[] = 'Barcode #'.$xsbarcode .' is already exists. Can not update.';
											file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$xsbarcode.' is already exists. Can not update.'."\n",FILE_APPEND | LOCK_EX);
										}
									} 
									if($checkproduct['ProductDesc']['barcode'] != ''){
										$global_barcode = $checkproduct['ProductDesc']['barcode'];
									}
									if($checkproduct['ProductDesc']['barcode'] == '' || $xsbarcode != $product_barcode){
										$barcodeData = [];
										$barcodeData['global_barcode']  = $global_barcode;
										$barcodeData['barcode'] 		= $xsbarcode;									 
										$barcodeData['username'] 		= $this->Session->read('Auth.User.username');
										$barcodeData['added_date'] 		= date('Y-m-d H:i:s');
										$this->ProductBarcode->saveAll($barcodeData);
										$productdscdata['barcode'] = $global_barcode;
									}
									
									$productdata['id'] = $checkproduct['Product']['id'];
									$this->Product->saveAll($productdata);
									
									$productdscdata['product_id'] = $productdata['id'];
									if($checkproduct['ProductDesc']['id'] > 0){
										$productdscdata['id'] = $checkproduct['ProductDesc']['id'];
									}
									 
									$this->ProductDesc->saveAll($productdscdata, array('validate' => false));
									$update_msg[$i]  = $product_sku.' is updated. '; 
 									file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$product_sku.' is updated.'."\n",FILE_APPEND | LOCK_EX);

								}
								else{
									$error_msg[$i] = 'SKU #'.$product_sku .' is not found to edit.';
									file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$product_sku.' is not found to edit.'."\n",FILE_APPEND | LOCK_EX);
								}
  							}
						}
						else{
							$error_msg[] =  'SKU #'.$product_sku.' missing these fields: '.implode(",",$empty) ;
							file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$product_sku.' missing. '.implode(";",$empty)."\n",FILE_APPEND | LOCK_EX);
 						}
					}
					   
				}
				$emsg = ''; $msg = '';
 			 
				if(count($success_msg) > 0){
					$msg .= count($success_msg) .' Products Added.';
				}
				if(count($update_msg) > 0 ){
					$msg .= implode("<br>",$update_msg);//count($update_msg) .' Products Updated.';
				}
				if(count($error_msg) > 0){
					$emsg = implode("<br>",$error_msg);
				}
				 
				$message['success'] = $msg;
				$message['danger'] = $emsg;
				
 			}
			else if($this->request->data['template'] == 'BundleSku'){
				file_put_contents(WWW_ROOT.'logs'.DS.$status_file,"Product sku,Upload Status"."\n", LOCK_EX);

 				for($i=2; $i<=$lastRow; $i++) 
				{ 				 	
					if(in_array(strtolower($objWorksheet->getCellByColumnAndRow(1,$i)->getValue()),['add','edit'])){
						$empty 			=   [];
						$productdata = []; $productdscdata = [];
						$global_barcode = ''; $product_sku = '';
						for($j = 0; $j < 13; $j++) {
							$global_barcode .= mt_rand(1, 9);
						}
						if($objWorksheet->getCellByColumnAndRow(4,$i)->getValue() != ''){	
 							$_xsbarcode = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
 							if(substr($_xsbarcode,0,1) == '~'){
								$xsbarcode = str_replace('~','',$_xsbarcode); 
							}	
 						}else{
							$xsbarcode = $global_barcode;
						}
						
 						if($objWorksheet->getCellByColumnAndRow(0,$i)->getValue() != ''){
							 $product_sku =	trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
							 $productdata['product_sku'] = $product_sku;	
 							 if(substr($product_sku,0,1) == 'S'){
								 $empty[] = $product_sku.' Please use currect sku.';
								 file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",Please use currect sku with\n",FILE_APPEND | LOCK_EX);
							 }else if(count(explode("-",$product_sku)) < 3){
							 	 $empty[] = $product_sku.' Please use sku with qty.';
								 file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",Please use sku with qty\n",FILE_APPEND | LOCK_EX);
 							 }
 							 $skus[$productdata['product_sku']] =	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
						}else{
							$error_msg[] = 'Product sku can not be empty.';
							file_put_contents(WWW_ROOT.'logs'.DS.$status_file,"Product sku can not be empty\n",FILE_APPEND | LOCK_EX);
						} 
						
						$product_type = 'Single';
						$product_identifier = 'Single';
						$product_defined_skus = '';
 						if(substr($productdata['product_sku'],0,1) == 'B'){
							$_sk = explode("-",$productdata['product_sku']);
							$ind = count($_sk) - 1;
							$_skus  = [];  
 									
							for($b = 1; $b < $ind  ; $b++){
								$_skus[] = 'S-'.$_sk[$b];
								$singleproduct =	$this->Product->find('first', array(
																'conditions' => array('Product.product_sku' => 'S-'.$_sk[$b])
																)
															); 
								if(count($singleproduct) == 0){
									$error_msg[] = $productdata['product_sku'].' Single SKU #S-'.$_sk[$b].' is not found.';
									file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",S-".$_sk[$b].' Single sku is not found.'."\n",FILE_APPEND | LOCK_EX);
 								} 
																
							}
							$product_type = 'Bundle';
							if(count($_sk ) > 3){
								$product_identifier = 'Multiple';
							}
							$product_defined_skus = implode(":",$_skus);  
							
							
						}
						
 						if($objWorksheet->getCellByColumnAndRow(2,$i)->getValue() != ''){
							$productdata['product_name']		=	addslashes($objWorksheet->getCellByColumnAndRow(2,$i)->getValue());		
						}else{
							$empty[] = 'Product Title can not be empty.';
						}
 						if($objWorksheet->getCellByColumnAndRow(5,$i)->getValue() != ''){	
							$productdata['category']			=	addslashes($objWorksheet->getCellByColumnAndRow(5,$i)->getValue());
						} 
 						 
						$productdata['product_status'] = 0;
						 
 						if($objWorksheet->getCellByColumnAndRow(13,$i)->getValue() != ''){	
							$iso_2  = $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
							$getiso = $this->Country->find( 'first' , array( 'conditions' => array( 'iso_2' => trim($iso_2) ) ) );
							if(count($getiso) > 0){
								$productdata['country_of_origin']	=	trim($iso_2);
							}else{
								$empty[] = 'Country of origin should be ISO 2 format.';
							}
						}else{
							$empty[] = 'Country of origin can not be empty.';
						}
 						if($objWorksheet->getCellByColumnAndRow(10,$i)->getValue() != ''){
							$cell_val = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
							$productdata['route']				=	is_null($cell_val) ? '1' : $cell_val;
						}else{
							$empty[] = 'Dangerous Good? Route can not be empty.';
						}
						
						if($objWorksheet->getCellByColumnAndRow(3,$i)->getValue() != ''){	
							$productdscdata['brand']			=	addslashes($objWorksheet->getCellByColumnAndRow(3,$i)->getValue());
						}
 						if($objWorksheet->getCellByColumnAndRow(6,$i)->getValue() != ''){
							$productdscdata['weight']			=	$objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
						}
						if($objWorksheet->getCellByColumnAndRow(7,$i)->getValue() != ''){
							$productdscdata['length'] 			=	$objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
						}
						if($objWorksheet->getCellByColumnAndRow(8,$i)->getValue() != ''){
							$productdscdata['width'] 			=	$objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
						}
						if($objWorksheet->getCellByColumnAndRow(9,$i)->getValue() != ''){
							$productdscdata['height']				=	$objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
						}
						if($objWorksheet->getCellByColumnAndRow(11,$i)->getValue() != ''){
							$productdscdata['frezile_mark']		=	$objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
						}else{
							$empty[] = 'Fragile Mark can not be empty.';
						}
						if($objWorksheet->getCellByColumnAndRow(12,$i)->getValue() != ''){
							$hscode								=	$objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
						}else{
							$empty[] = 'HSN code can not be empty.';
						}						 
						if($objWorksheet->getCellByColumnAndRow(14,$i)->getValue() != ''){
							$productdata['export_invoice_title'] =	$objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
						}else{
							$empty[] = 'Export Invoice Title can not be empty.';
						}
	
 						$productdscdata['product_type']				=	$product_type;//$objWorksheet->getCellByColumnAndRow(31,$i)->getValue();
						$productdscdata['product_defined_skus']		=	$product_defined_skus;//$objWorksheet->getCellByColumnAndRow(32,$i)->getValue();
						$productdscdata['product_identifier']		=	$product_identifier;//$objWorksheet->getCellByColumnAndRow(33,$i)->getValue();
						
 						if(count($empty) == 0){
						
							if(!empty($product_sku) && $hscode != ''){
								$ccode = 'GB';
								$checHs = $this->ProductHscode->find('first', array(
																			'conditions' => array(
																				'sku' => $product_sku,
																				'country_code' => $ccode
																				),
																			 'fields' =>['id']
																			)
																		); 
								
								$store['sku']    		= $product_sku;
								$store['country_code']  = $ccode;
								$store['hs_code']		= $hscode;
								$store['username'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
								if(count($checHs) > 0)
								{
									$store['id']			= 	$checHs['ProductHscode']['id'];
									$store['timestamp']		= 	date('Y-m-d H:i:s');
									$this->ProductHscode->saveAll($store);
								}else{
									$store['added_date']	= 	date('Y-m-d H:i:s');
									$this->ProductHscode->saveAll($store);
								}
							}
									
							// Check also brand is exists or not
							$this->loadModel( 'Brand' );
							$getBrand = $this->Brand->find( 'first' , array( 'conditions' => array( 'Brand.brand_name' => $productdscdata['brand'] ) ) );
							
							if( count( $getBrand ) == 0 )
							{
								$brandName = $productdscdata['brand'];
								$brandAlias = strtolower($brandName);					
								$this->Brand->query( "insert into brands ( brand_name, brand_alias ) values ( '{$brandName}' , '{$brandAlias}' )" );					
								$productdscdata['brand'] = $brandName;
								
							}
							else
							{
								$productdscdata['brand'] = $productdscdata['brand'];
							}
							
 							// Check also Category is exist or note
							$getCategory = $this->Category->find( 'first' , array( 'conditions' => array( 'Category.category_name' => $productdata['category'] ) ) );
							if( count( $getCategory ) == 0 )
							{
								$categoryName = $productdata['category'];
								$catAlias = strtolower($categoryName);					
								$this->Category->query( "insert into categories ( category_name, category_alias ) values ( '{$categoryName}' , '{$catAlias}' )" );					
								$catId = $this->Category->getLastInsertId();
								$productdata['category_id'] = $catId;
								$productdata['category_name'] = $categoryName;
							}
							else
							{
								$productdata['category_id'] = $getCategory['Category']['id'];
								$productdata['category_name'] = $productdata['category'];
							}
							
							$productdata['core_update'] = date('Y-m-d H:i:s');
							$productdata['core_upload'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name'); 
							
							$checkproduct =	$this->Product->find('first', array(
																'conditions' => array('Product.product_sku' => $productdata['product_sku'])
																)
															); 
							if(strtolower($objWorksheet->getCellByColumnAndRow(1,$i)->getValue()) == 'add'){
								$pbarcode =	$this->ProductBarcode->find('first', array(
																'conditions' => array('barcode' => $xsbarcode)
																)
															); 
								if(count($pbarcode) > 0){
									$error_msg[] = 'Barcode #'.$xsbarcode .' is already exists.';
									file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$xsbarcode.' is already exists.'."\n",FILE_APPEND | LOCK_EX);

								}else{
									if(count($checkproduct) == 0)
									{
										try{
											$productdata['added_date'] = date('Y-m-d H:i:s');
											$productdata['added_by']   = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
											
											if($this->Product->saveAll($productdata)){
											
												$productid	=	$this->Product->getLastInsertId();
												$success_msg[]  = $productdata['product_sku'].' is added.';
												file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$product_sku.' is added.'."\n",FILE_APPEND | LOCK_EX);
												$productdscdata['product_id'] = $productid;
												try{
													$productdscdata['barcode'] = $global_barcode;
													$this->ProductDesc->saveAll($productdscdata, array('validate' => false));
												}catch (Exception $e) {
													$error_msg[] = 'Caught exception: '.  $e->getMessage(). "\n";
												} 
												
												$productDescid	=	$this->ProductDesc->getLastInsertId();
													
												$barcodeData = [];
												$barcodeData['global_barcode']  = $global_barcode;
												if($xsbarcode == '' ){	
													$barcodeData['barcode'] = $global_barcode; 
												} 
												else{
													$barcodeData['barcode'] = $xsbarcode;
												}
												/************************Save Global barcode*********************************/
												$barcodeData['username'] 		= $this->Session->read('Auth.User.username');
												$barcodeData['added_date'] 		= date('Y-m-d H:i:s');
												$this->ProductBarcode->saveAll($barcodeData);
												/*****************************************/
												}
											}
											catch (Exception $e) {
											$error_msg[] =  'Caught exception: '.  $e->getMessage(). "\n";
										} 
 									}
									else
									{
										$error_msg[] = 'SKU #'.$productdata['product_sku'] .' is already exists.';
										file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$product_sku.' is already exists.'."\n",FILE_APPEND | LOCK_EX);

									}
								}
							}
							else if(strtolower($objWorksheet->getCellByColumnAndRow(1,$i)->getValue()) == 'edit'){
								if(count($checkproduct) > 0){
									$product_barcode = '';
									$pbarcode =	$this->ProductBarcode->find('first', array(
																	'conditions' => array('barcode' => $xsbarcode)
																	)
																); 
									if(count($pbarcode) > 0){
										$product_barcode = $pbarcode['ProductBarcode']['barcode'];
										if($pbarcode['ProductBarcode']['global_barcode'] != $checkproduct['ProductDesc']['barcode']){
											$error_msg[] = 'Barcode #'.$xsbarcode .' is not updated because it is already associated with other product.';
											file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$xsbarcode.' is not updated because it is already associated with other product.'."\n",FILE_APPEND | LOCK_EX);
										}
									} 
									if($checkproduct['ProductDesc']['barcode'] != ''){
										$global_barcode = $checkproduct['ProductDesc']['barcode'];
									}
									if(!empty($checkproduct['ProductDesc']['barcode']) || $xsbarcode != $product_barcode){
										$barcodeData = [];
										$barcodeData['global_barcode']  = $global_barcode;
										$barcodeData['barcode'] 		= $xsbarcode;									 
										$barcodeData['username'] 		= $this->Session->read('Auth.User.username');
										$barcodeData['added_date'] 		= date('Y-m-d H:i:s');
										$this->ProductBarcode->saveAll($barcodeData);
										$productdscdata['barcode'] = $global_barcode;
									}
									
									$productdata['id'] = $checkproduct['Product']['id'];
									$this->Product->saveAll($productdata);
									
									$productdscdata['id'] = $checkproduct['ProductDesc']['id'];
									$this->ProductDesc->saveAll($productdscdata, array('validate' => false));
									$update_msg[]  = $productdata['product_sku'].' is updated. '; 
									file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$product_sku.' is updated.'."\n",FILE_APPEND | LOCK_EX);
								}
								else{
									$error_msg[] = 'SKU #'.$productdata['product_sku'] .' is not found.';
									file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$product_sku.' is not found.'."\n",FILE_APPEND | LOCK_EX);
								}
  							}
						}
						else if(count($empty) > 0){
							$error_msg[] =  'SKU #'.$productdata['product_sku'].' missing these fields: '.implode(",",$empty) ;
							file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".implode(";",$empty)."\n",FILE_APPEND | LOCK_EX);

   						}
					}
 				}
				$emsg = ''; $msg = '';
 			 
				if(count($success_msg) > 0){
					$msg .= count($success_msg) .' Products Added.';
				}
				if(count($update_msg) > 0 ){
					$msg .= implode("<br>",$update_msg);//count($update_msg) .' Products Updated.';
				}
				if(count($error_msg) > 0){
					$emsg = implode("<br>",$error_msg);
				}
				 
				$message['success'] = $msg;
				$message['danger'] = $emsg;
				
 			}
			else if($this->request->data['template'] == 'DetailedProductInformation'){
				file_put_contents(WWW_ROOT.'logs'.DS.$status_file,"Product sku,Upload Status"."\n", LOCK_EX);

 				for($i=2; $i<=$lastRow; $i++) 
				{ 				 	
					$empty 			= [];
					$productdata    = []; $productdscdata = []; 
					
					if(trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue()) != ''){
						$product_sku		=	trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
						$skus[]				=	$product_sku;
						$checkproduct		=	$this->Product->find('first', array(
															'conditions' => array('Product.product_sku' => $product_sku),
															'fields' => array('ProductDesc.id')
															)
														); 
						if(count($checkproduct) > 0)
						{
							$productdscdata['id'] = $checkproduct['ProductDesc']['id'];
						}else{
							$empty[] = $product_sku .' product sku not found.';
						}
						
					}else{
						$empty[] = 'Product sku can not be empty.';
					}
					
					if($objWorksheet->getCellByColumnAndRow(1,$i)->getValue() != ''){
						$productdscdata['short_description']			=	addslashes($objWorksheet->getCellByColumnAndRow(1,$i)->getValue());		
					} 
					if($objWorksheet->getCellByColumnAndRow(2,$i)->getValue() != ''){	
						$productdscdata['long_description']				=	addslashes($objWorksheet->getCellByColumnAndRow(2,$i)->getValue());
					} 
					if($objWorksheet->getCellByColumnAndRow(3,$i)->getValue() != ''){	
						$productdscdata['model_no']			=	$objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
					} 
					if($objWorksheet->getCellByColumnAndRow(4,$i)->getValue() != ''){	
						$productdscdata['manufacturer_part_num']		=	$objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
					} 
					if($objWorksheet->getCellByColumnAndRow(5,$i)->getValue() != ''){
						$productdscdata['color']					=	$objWorksheet->getCellByColumnAndRow(5,$i)->getValue() ;
					} 
					if($objWorksheet->getCellByColumnAndRow(6,$i)->getValue() != ''){	
						$productdscdata['keywords']			=	addslashes($objWorksheet->getCellByColumnAndRow(6,$i)->getValue());
					}
					if($objWorksheet->getCellByColumnAndRow(7,$i)->getValue() != ''){
						$productdscdata['bullet_1'] 		=	$objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
					}
					if($objWorksheet->getCellByColumnAndRow(8,$i)->getValue() != ''){
						$productdscdata['bullet_2'] 		=	$objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
					}
					if($objWorksheet->getCellByColumnAndRow(9,$i)->getValue() != ''){
						$productdscdata['bullet_3']			=	$objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
					}
					if($objWorksheet->getCellByColumnAndRow(10,$i)->getValue() != ''){
						$productdscdata['bullet_4']			=	$objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
					} 
					if($objWorksheet->getCellByColumnAndRow(11,$i)->getValue() != ''){
						$productdscdata['bullet_5']			=	$objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
					} 
					if($objWorksheet->getCellByColumnAndRow(12,$i)->getValue() != ''){
						$productdscdata['image_url_1']		=	$objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
					} 
					if($objWorksheet->getCellByColumnAndRow(13,$i)->getValue() != ''){
						$productdscdata['image_url_2']	=	$objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
					}
					if($objWorksheet->getCellByColumnAndRow(14,$i)->getValue() != ''){
						$productdscdata['image_url_3']	=	$objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
					}
					if($objWorksheet->getCellByColumnAndRow(15,$i)->getValue() != ''){
						$productdscdata['image_url_4']	=	$objWorksheet->getCellByColumnAndRow(15,$i)->getValue();
					}
					if($objWorksheet->getCellByColumnAndRow(16,$i)->getValue() != ''){
						$productdscdata['image_url_5']	=	$objWorksheet->getCellByColumnAndRow(16,$i)->getValue();
					}
					if($objWorksheet->getCellByColumnAndRow(17,$i)->getValue() != ''){
						$productdscdata['image_url_6']	=	$objWorksheet->getCellByColumnAndRow(17,$i)->getValue();
					}
					if($objWorksheet->getCellByColumnAndRow(18,$i)->getValue() != ''){
						$productdscdata['image_url_7']	=	$objWorksheet->getCellByColumnAndRow(18,$i)->getValue();
					}
					
				 	$productdscdata['info_update'] = date('Y-m-d H:i:s');
					$productdscdata['info_upload'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name'); 
							
					if(count($empty) == 0){  
						$rr = $this->ProductDesc->saveAll($productdscdata, array('validate' => false));
						//pr($productdscdata);
						//var_dump($rr);exit;
						$update_msg[]  = $product_sku.' is updated. ';
						file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".$product_sku.' is updated.'."\n",FILE_APPEND | LOCK_EX);

					}else{
						$error_msg[] =  implode("<br>",$empty) ;
						file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".implode(";",$empty)."\n",FILE_APPEND | LOCK_EX);
					}
				}
				
				$emsg = ''; $msg = '';
 			 
				if(count($success_msg) > 0){
					$msg .= count($success_msg) .' Products Added.';
				}
				if(count($update_msg) > 0 ){
					$msg .= count($update_msg) .' Products Updated.';
				}
				if(count($error_msg) > 0){
					$emsg = implode(",",$error_msg);
				}
				 
				$message['success'] = $msg;
				$message['danger'] = $emsg;
 			}
			else if($this->request->data['template'] == 'ChannelSKUMapping'){
  				file_put_contents(WWW_ROOT.'logs'.DS.$status_file,"Product sku,Upload Status"."\n", LOCK_EX);
				$sku_msg_status = [];
				for($i=2; $i<=$lastRow; $i++) 
				{ 				 	
					$empty 			= [];
 					
					if($objWorksheet->getCellByColumnAndRow(0,$i)->getValue() != ''){
						$product_sku		=	trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
						$asin_no			=	trim($objWorksheet->getCellByColumnAndRow(1,$i)->getValue());
						$skus[]				=	$product_sku;
						$checkproduct		=	$this->Product->find('first', array(
															'conditions' => array('Product.product_sku' => $product_sku),
															'fields' => array('ProductDesc.id')
															)
														); 
						if(count($checkproduct) > 0)
						{
							$productdscdata['id'] = $checkproduct['ProductDesc']['id'];
						}else{
							$empty[] = $product_sku .' product sku not found.';
						}
						
					}else{
						$empty[] = 'Product sku can not be empty.';
					}
					
					//-----------Cost
					$store_csv = [];
					$col = 2;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_UK'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
 					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_ES'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}					
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
					$store_csv['CostBreaker_FR'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_DE'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_IT'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_NL'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_CA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_US'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					} //FBA
					 
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_USABuyer'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_UKFBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_DEFBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_ESFBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_FRFBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['CostBreaker_ITFBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					
					//Marec	 
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Marec_DE'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Marec_DEFBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Marec_ES'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Marec_FR'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Marec_FRFBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Marec_IT'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Marec_ITFBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Marec_NL'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Marec_UK'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Marec_UKFBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					 $col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Rainbow Retail'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){				
						$store_csv['RAINBOW RETAIL DE'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Rainbow_DE_FBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Rainbow_FRFBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Rainbow_Retail_ES'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Rainbow_Retail_FR'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Rainbow_Retail_IT'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Rainbow_UK_FBA'] = $fba_csv['FBAUK'][] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
 					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Cdiscount_FR'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Real_DE'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['eBay'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Onbuy'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Flubit'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Fnac'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Fyndiq'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
 					$col++;	
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Rainbow_ES_FBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					$col++;
					if(!is_null($objWorksheet->getCellByColumnAndRow($col,$i)->getValue())){
						$store_csv['Rainbow_IT_FBA'] = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
					}
					
 					
					if(count($empty) == 0){
 						$sku_msg = []; 
 						foreach($store_csv as $channel_title => $val){
							 $channelData = [];
							 if(strtolower($val) == 'y'){
								 $channelData['active'] = array('sku' => $product_sku,
											'channel_title' =>$channel_title,	
											'asin' => $asin_no,
											
										);
							}else if(strtolower($val) == 'n'){
								$channelData['inactive'] = array('sku' => $product_sku,
											'channel_title' =>$channel_title,	
											'asin' => $asin_no,
											
										);
							} 
 							foreach($channelData as $status => $dataLine){
								
								$sup = $this->SupplierMapping->find('first', array('conditions' => array( 'master_sku' => $dataLine['sku']),'fields'=>['sup_code']));
								$supplier = '';
								if(count($sup) > 0){
									$supplier = $sup['SupplierMapping']['sup_code'];
								}
							//	'sku' => $dataLine['sku'],
								$result =  $this->Skumapping->find('first', array('conditions' => array('asin' => $dataLine['asin'],'channel_name' => $dataLine['channel_title']),'fields' => ['id','listing_status','channel_sku','sku'])); 
								
 								if(count($result) > 0){		
									//pr($result);exit;
 									if($result['Skumapping']['sku'] == $dataLine['sku']){
									
										$this->Skumapping->query("UPDATE 
										`skumappings` SET 
										`listing_status` = '".$status."' ,
										`supplier` = '".$supplier."' ,
										`map_change` = '".date("Y-m-d H:i:s")."' ,
										`map_change_by` = '".$username."' 
											WHERE 
										`id` = '".$result['Skumapping']['id']."' "); 
										//`sku` = '".$dataLine['sku']."' AND
										file_put_contents(WWW_ROOT. 'logs'.DS.'Uploads.UpdateMapping_'.date('Ymd').'.log', 
										$dataLine['sku']."\t".$dataLine['asin']."\t".$dataLine['channel_title']."\t".$status."\t".$username."\n", 
										FILE_APPEND|LOCK_EX); 	
										
									//	$all_exist .= $dataLine['sku']."\t".$dataLine['asin']."\t".$dataLine['channel_title']."\t is Already Exists!\n";
									//	$all_exist_count++; 
										$k = $dataLine['sku'].'_'.$dataLine['asin'].'_'.$dataLine['channel_title'];
										$sku_msg_status[$status][] = $k;
										$update_msg[] = $dataLine['sku'].' and '.$dataLine['channel_title'].' is updated. ';	
										$sku_msg[]    = $dataLine['asin'].' & '.$result['Skumapping']['channel_sku'].' with '.$dataLine['channel_title'].' is '. ucfirst($status);					}else{
										$error_msg[]    = $dataLine['asin'].' & '.$dataLine['channel_title'].' is already mapped with '. $result['Skumapping']['sku'];	
									
									}
									
								 }
								else if($status == 'active'){	 
 										$channel_sku  = $this->getAutoChannel($dataLine['channel_title']);	
										$country_code = $CountryCodeArr[$dataLine['channel_title']];	
										$inSql = "INSERT  INTO `skumappings` 
										 (`sku`, `asin`,`channel_name`,`channel_sku`,`country_code`,`added_date`,`username`,`map_change`,`map_change_by`,`supplier`)
										 VALUES   
										('".$dataLine['sku']."','".$dataLine['asin']."','".$dataLine['channel_title']."',
											   '".$channel_sku."','".$country_code."','".date("Y-m-d H:i:s")."','".$username."','".date("Y-m-d H:i:s")."','".$username."','".$supplier."')";
										$this->Skumapping->query($inSql);	
 										$success_msg[]  = $dataLine['sku'].' and '.$dataLine['channel_title'].' is added. ';	
										$sku_msg[]  = $dataLine['asin'].' & '.$channel_sku .' with '.$dataLine['channel_title'].' is added. ';							
								}else{
									$sku_msg[]    = $dataLine['sku'].' & '.$dataLine['asin'].' & '.' with '.$dataLine['channel_title'].' is not created due flag value "n"';
									$sku_msg_status['nf'][] = $dataLine['sku'];			
								}												
 							}
						
						}  
 						file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".implode(", ",$sku_msg)."\n",FILE_APPEND | LOCK_EX);
 					}
					else{
						$error_msg[] =  implode("<br>",$empty) ;
						file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$product_sku.",".implode(", ",$empty)."\n",FILE_APPEND | LOCK_EX);
 					}
				}
				
				$emsg = ''; $msg = '';
 			 
				if(count($success_msg) > 0){
					$msg .= count($success_msg) .' Channel SKU Added.<br>';
					//$msg  = implode("<br>",$success_msg);
				}
				file_put_contents(WWW_ROOT.'logs/sku_msg_status.log',print_r($sku_msg_status,true),FILE_APPEND | LOCK_EX);
 				if(count($sku_msg_status) > 0 ){
					if(isset($sku_msg_status['active'])){
						$msg .= count($sku_msg_status['active']). ' Channel SKU Active.<br>';
					}
					if(isset($sku_msg_status['inactive'])){
						$msg .= count($sku_msg_status['inactive']) .' Channel SKU Inactive.';
					}
					if(isset($sku_msg_status['nf'])){
						$error_msg[] = count($sku_msg_status['nf']) .' Channel SKU not created due to flag value "n".';
					}
				}
				
				/*if(count($update_msg) > 0 ){
					$msg .= count($update_msg) .' Channel SKU Updated.';
					//$msg .= implode("<br>",$update_msg);
				}*/
				if(count($error_msg) > 0){
					$emsg = implode(",",$error_msg);
				}
				 
				$message['success'] = $msg;
				$message['danger'] = $emsg;
 			}
			else if($this->request->data['template'] == 'ChannelSKUManagement'){
  				file_put_contents(WWW_ROOT.'logs'.DS.$status_file,"Channel sku,Upload Status"."\n", LOCK_EX);

				for($i=2; $i<=$lastRow; $i++) 
				{ 				 	
					//ChannelSKU	Asin No	Margin %	Referral Fee	FBA Fees	SmallLightProgramme-FBA	NPA?	Min Fee	Variable Fee 

 					$empty 			= [];
 					$channel_sku	= trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
 					if($channel_sku != ''){
					//	$asin					=	trim($objWorksheet->getCellByColumnAndRow(1,$i)->getValue());
						$margin					=	trim($objWorksheet->getCellByColumnAndRow(1,$i)->getValue());
						$referral_fee			=	trim($objWorksheet->getCellByColumnAndRow(2,$i)->getValue());
						$fba_fee				=	trim($objWorksheet->getCellByColumnAndRow(3,$i)->getValue());
						$light_programme_fba	=	trim($objWorksheet->getCellByColumnAndRow(4,$i)->getValue());
						$is_npa					=	trim($objWorksheet->getCellByColumnAndRow(5,$i)->getValue());
						$min_fee				=	trim($objWorksheet->getCellByColumnAndRow(6,$i)->getValue());
						$variable_fee			=	trim($objWorksheet->getCellByColumnAndRow(7,$i)->getValue());
						$skus[] 				=	$channel_sku;
						$dataLine = []; 
						if($margin < 0 && strtolower($is_npa) != 'y' ){
							$empty[] = $channel_sku. ' Please use margin as(+ve) value OR mark it as NPA and then upload it.';
 						}
						$checkproduct =	$this->Skumapping->find('first', array(
															'conditions' => array('channel_sku' => $channel_sku),
															'fields' => array('id')
															)
														); 
						if(count($checkproduct) > 0){
							if(count($empty) == 0)
							{
								$dataLine['id'] = $checkproduct['Skumapping']['id'];
								$dataLine['change_margin']    = date("Y-m-d H:i:s");
								$dataLine['change_margin_by'] = $username;
										
								if($margin != ''){
									$dataLine['margin'] = $margin;
								}
								if($referral_fee != ''){
									$dataLine['referral_fee'] = $referral_fee;
								}
								if($fba_fee != ''){
									$dataLine['fba_fee'] = $fba_fee;
								}
								
								if($light_programme_fba == 'y'){
									$dataLine['light_programme_fba'] = 'yes';
								}else if($light_programme_fba == 'n'){
									$dataLine['light_programme_fba'] = 'no';
								}
								
								if(strtolower($is_npa) == 'y'){
									$dataLine['stock_status'] = 'npa';
								}elseif(strtolower($is_npa) == 'n'){
									$dataLine['stock_status'] = 'out of stock';
								} 
								if($min_fee != ''){
									$dataLine['min_referral_fee'] = $min_fee;
								}
								if($variable_fee != ''){
									$dataLine['variable_fee'] = $variable_fee;
								}

								$this->Skumapping->saveAll($dataLine);
								file_put_contents(WWW_ROOT. 'logs'.DS.'Uploads.ChannelSKUManagement_'.date('Ymd').'.log', 
								$channel_sku."\t".$margin."\t".$username."\n", 
								FILE_APPEND|LOCK_EX); 	
								$update_msg[]  = $channel_sku.' is updated. ';	
								file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$channel_sku.",updated\n",FILE_APPEND | LOCK_EX);
		
 						}
						}else{
							$empty[] = $channel_sku .' channel sku not found.';
						}
						
					}
					else{
						$empty[] = 'Channel sku can not be empty.';
					}
				 
					 	
					if(count($empty) > 0){
						$error_msg[] =  implode("<br>",$empty) ; 
						file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$channel_sku.",".implode("; ",$empty)."\n",FILE_APPEND | LOCK_EX);
					}
				}
				$emsg = ''; $msg = '';
 			 
 				if(count($update_msg) > 0 ){
					$msg .= count($update_msg) .' Channel SKU Updated.';
					//$msg .= implode("<br>",$update_msg);
				}
				if(count($error_msg) > 0){
					$emsg = implode(",",$error_msg);
				}
				 
				$message['success'] = $msg;
				$message['danger'] = $emsg;
 			}
			else if($this->request->data['template'] == 'DeleteSku'){
  				file_put_contents(WWW_ROOT.'logs'.DS.$status_file,"Product sku,Upload Status"."\n", LOCK_EX);

				for($i=2; $i<=$lastRow; $i++) 
				{ 				 	
					$empty 			= [];
 					
					if($objWorksheet->getCellByColumnAndRow(0,$i)->getValue() != ''){
						$sku			=	trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
						$skus[] 		=	$sku;
						$dataLine = []; 
						
						$checkproduct		=	$this->Product->find('first', array(
															'conditions' => array('product_sku' => $sku),
															'fields' => array('id')
															)
														); 
						if(count($checkproduct) > 0)
						{ 
  							$this->Skumapping->query("UPDATE `skumappings` SET `listing_status` = 'inactive' WHERE `sku` = '".$sku."'"); 
							$this->Skumapping->query("UPDATE `products` SET `is_deleted` = '1', product_status = '1' WHERE `product_sku` = '".$sku."'"); 
							$update_msg[] = $sku .' sku deleted.';
							file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$sku.",deleted\n",FILE_APPEND | LOCK_EX);

 						}
						else{
							$error_msg[] = $sku .' sku not found.';
							file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$sku.",sku not found\n",FILE_APPEND | LOCK_EX);
						}
						
					}
					else{
						$error_msg[] = 'Sku can not be empty.';
						file_put_contents(WWW_ROOT.'logs'.DS.$status_file,"Sku can not be empty\n",FILE_APPEND | LOCK_EX);
 					}
				 
					 	
					if(count($empty) > 0){
						$error_msg[] =  implode("<br>",$empty) ; 
					}
				}
				$emsg = ''; $msg = '';
 			 
 				if(count($update_msg) > 0 ){
					$msg .= count($update_msg) .' SKU deleted.';
					//$msg .= implode("<br>",$update_msg);
				}
				if(count($error_msg) > 0){
					$emsg = implode("<br>",$error_msg);
				}
				 
				$message['success'] = $msg;
				$message['danger'] = $emsg;
 			}
			else if($this->request->data['template'] == 'DeleteChannelSKUMapping'){
  				file_put_contents(WWW_ROOT.'logs'.DS.$status_file,"Channel sku,Upload Status"."\n", LOCK_EX);

				for($i=2; $i<=$lastRow; $i++) 
				{ 				 	
					$empty 			= [];
 					
					if($objWorksheet->getCellByColumnAndRow(0,$i)->getValue() != ''){
						$channel_sku		=	trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
  						$checkproduct		=	$this->Skumapping->find('first', array(
															'conditions' => array('channel_sku' => $channel_sku),
															'fields' => array('id')
															)
														); 
						if(count($checkproduct) > 0)
						{ 
  							$this->Skumapping->query("UPDATE `skumappings` SET `listing_status` = 'deleted' WHERE `channel_sku` = '".$channel_sku."'"); 
 							$update_msg[] = $channel_sku .' Channel sku deleted.';
							file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$channel_sku.",deleted\n",FILE_APPEND | LOCK_EX);

 						}
						else{
							$error_msg[] = $channel_sku .' Channel sku not found.';
							file_put_contents(WWW_ROOT.'logs'.DS.$status_file,$channel_sku.",Channel sku not found\n",FILE_APPEND | LOCK_EX);
						}
						
					}
					else{
						$error_msg[] = 'Channel Sku can not be empty.';
						file_put_contents(WWW_ROOT.'logs'.DS.$status_file,"Channel Sku can not be empty\n",FILE_APPEND | LOCK_EX);
 					}
				 
					 	
					if(count($empty) > 0){
						$error_msg[] =  implode("<br>",$empty) ; 
					}
				}
				$emsg = ''; $msg = '';
 			 
 				if(count($update_msg) > 0 ){
					$msg .= count($update_msg) .' Channel SKU deleted.';
					//$msg .= implode("<br>",$update_msg);
				}
				if(count($error_msg) > 0){
					$emsg = implode("<br>",$error_msg);
				}
				 
				$message['success'] = $msg;
				$message['danger'] = $emsg;
 			}
			$message['info'] = 'Download status file.<a <a href="'.Router::url('/', true).'logs/'.$status_file.'">Download</a>';
			$FilesLog = [];
			$FilesLog['template'] 	   = $this->request->data['template'];
			$FilesLog['sku'] 		   = json_encode($skus);
			$FilesLog['uploaded_file'] = $main_file;
			$FilesLog['file_name'] 	   = $name.$main_file;
			$FilesLog['uploaded_by']   = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
			$FilesLog['added_date']    = date('Y-m-d H:i:s');
		 	$this->ProductFilesLog->saveAll($FilesLog);	
 			
 			$this->Session->setflash( $message, 'flash_all' ); 
  			$this->redirect($this->referer());
		}
		else
		{
			$this->Session->setFlash('Please Insert CSV File.', 'flash_danger');
			$this->redirect($this->referer());
		}
	}
	
	public function UploadsCsvFilesForInventery29DEC2021()
	{
  		$this->autoLayout = 'upload';
		$this->autoRender = false;
		
		$this->loadModel('Store');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('ProductPrice');
		$this->loadModel('ProductImage');
		$this->loadModel('AttributeOption');
		$this->loadModel('ProductBarcode');
		$this->loadModel('ProductCsv');
		$this->loadModel('SkuFbmFba');
		$this->loadModel('ProductHscode'); 
 		$this->loadModel('SupplierMapping'); 
		$this->loadModel('SupplierDetail');		
				//pr($this->request->data);
		// exit;
		$this->data['Product']['Import_file']['name'];
		if($this->data['Product']['Import_file']['name'] != '')
		{
				
				
			$name		= $this->Session->read('Auth.User.first_name').'_'.$this->Session->read('Auth.User.last_name').'_'.$this->request->data['Product']['Import_file']['name'];
 			$filename   =  WWW_ROOT. 'files'.DS.$name;
			move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
			
  			
			/*$filename =  WWW_ROOT. 'files'.DS.$this->request->data['Product']['Import_file']['name'];
			move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
			$name = $this->request->data['Product']['Import_file']['name'];
			*/
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/'.$name);
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			
 			$header	=	array('Product Title','Brand','Short Description','Long Description','Contents/Information','Ingredients','Usage Information', 'Notes', 'Technical Specifications',
			 'UPC/Barcode', 'Image filename', 'Image filename2', 'Image filename3', 'Image filename4', 'Vegan', 'Vegetarian', 'Gluten Free', 'Corn Free', 'Wheat Free', 'Dairy Free', 
			 'Soya Free', 'Non-GM', 'Weight', 'Category', 'Price', 'Length', 'Width', 'Height', 'Status');
			 
			$count	= 0;
			$alreadycount	=	0;
			$countbarcode	=	0;
			$frizileMark	=	0;
 			$update_msg 	=   [];
			$success_msg 	=	[];	
			$error_msg 		=   [];
			$product_csv_ids= 	[];
			$channelData	=	[];
 			$already_map    =  'already_map_'.$this->Session->read('Auth.User.first_name').'_'.$this->Session->read('Auth.User.last_name').'_'.date('ymdHis').'.csv';	
			@unlink(WWW_ROOT. 'logs'.DS.$already_map);
			for($i=2; $i<=$lastRow; $i++) 
			{ 				 	
				$productdata = []; $productdscdata = [];
				$global_barcode = '';
				for($j = 0; $j < 13; $j++) {
					$global_barcode .= mt_rand(1, 9);
				}
				$xsbarcode								=	$objWorksheet->getCellByColumnAndRow(5,$i)->getValue();	
 				$productdata['product_sku']				=	trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
				$productdata['asin_no']					=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$productdata['product_name']			=	addslashes($objWorksheet->getCellByColumnAndRow(2,$i)->getValue());				
				$productdata['category']				=	addslashes($objWorksheet->getCellByColumnAndRow(8,$i)->getValue());
				$productdata['product_status']			=	$objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
				$productdata['country_of_origin']		=	$objWorksheet->getCellByColumnAndRow(20,$i)->getValue();
				$cell_val = $objWorksheet->getCellByColumnAndRow(19,$i)->getValue();
				$productdata['route']					=	is_null($cell_val) ? '1' : $cell_val;
				
 				$productdscdata['brand']				=	addslashes($objWorksheet->getCellByColumnAndRow(3,$i)->getValue());
				$productdscdata['short_description']	=	addslashes($objWorksheet->getCellByColumnAndRow(4,$i)->getValue());
				$productdscdata['long_description']		=	addslashes($objWorksheet->getCellByColumnAndRow(4,$i)->getValue());
				$productdscdata['barcode'] 				=	$global_barcode;//$objWorksheet->getCellByColumnAndRow(6,$i)->getValue();				 
				 
 				$productdscdata['weight']				=	$objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
				$productdscdata['price']				=	$objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
				$productdscdata['length'] 				=	$objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
				$productdscdata['width'] 				=	$objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
				$productdscdata['height']				=	$objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
				
 				$productdscdata['model_no']				=	$objWorksheet->getCellByColumnAndRow(25,$i)->getValue();
				$productdscdata['manufacturer_part_num']=	$objWorksheet->getCellByColumnAndRow(26,$i)->getValue();
				$productdscdata['type']					=	$objWorksheet->getCellByColumnAndRow(28,$i)->getValue();
				$product_type = 'Single';
				$product_identifier = 'Single';
				$product_defined_skus = '';
				//$productdata['product_sku'] = 'B-0621B026-0628B001-0620B001-3';
  				//echo $productdata['product_sku'] = 'B-06245#BC-4';
				if(substr($productdata['product_sku'],0,1) == 'B'){
					$_sk = explode("-",$productdata['product_sku']);
					$ind = count($_sk) - 1;
					$_skus = [];
					for($b = 1; $b < $ind  ; $b++){
						$_skus[] = 'S-'.$_sk[$b];
					}
					$product_type = 'Bundle';
					if(count($_sk ) > 3){
						$product_identifier = 'Multiple';
					}
 					$product_defined_skus = implode(":",$_skus);  
 				}
  				/*
 				 * Params, Inserting Product type and product defined skus list with colon based
 				 */ 
				$productdscdata['product_type']				=	$product_type;//$objWorksheet->getCellByColumnAndRow(31,$i)->getValue();
				$productdscdata['product_defined_skus']		=	$product_defined_skus;//$objWorksheet->getCellByColumnAndRow(32,$i)->getValue();
				$productdscdata['product_identifier']		=	$product_identifier;//$objWorksheet->getCellByColumnAndRow(33,$i)->getValue();
 				$productdscdata['frezile_mark']				=	$objWorksheet->getCellByColumnAndRow(16,$i)->getValue();
				
 				$hscode =[];
				$hscode['GB'] =	$objWorksheet->getCellByColumnAndRow(21,$i)->getValue();
				$hscode['DE'] =	$objWorksheet->getCellByColumnAndRow(22,$i)->getValue();
				$hscode['ES'] =	$objWorksheet->getCellByColumnAndRow(23,$i)->getValue();
				$hscode['FR'] =	$objWorksheet->getCellByColumnAndRow(24,$i)->getValue();
				$hscode['IT'] =	$objWorksheet->getCellByColumnAndRow(25,$i)->getValue();
				$hscode['US'] =	$objWorksheet->getCellByColumnAndRow(26,$i)->getValue();
				$hscode['CA'] =	$objWorksheet->getCellByColumnAndRow(27,$i)->getValue();
				
				$product_csv = [];
 				$product_csv['asin']				=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$product_csv['ref_fee']				=	$objWorksheet->getCellByColumnAndRow(29,$i)->getValue();
				$product_csv['margin']				=	$objWorksheet->getCellByColumnAndRow(30,$i)->getValue();
 				$product_csv['referral_category']	=	$objWorksheet->getCellByColumnAndRow(28,$i)->getValue();
				$store_csv =[]; $fba_csv =[];
 				
			 	//-----------Cost
				$col = 33;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_UK'] = is_null($cell_val) ? 'N' : $cell_val;
				
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_ES'] = is_null($cell_val) ? 'N' : $cell_val;
				
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_FR'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_DE'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_IT'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_NL'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_CA'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_US'] = $fba_csv['FBAUSA'][] = is_null($cell_val) ? 'N' : $cell_val; //FBA
				 
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_USABuyer'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_UKFBA'] = $fba_csv['FBAUK'][] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
 				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_DEFBA'] = $fba_csv['FBADE'][] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_ESFBA'] = $fba_csv['FBAES'][] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_FRFBA'] = $fba_csv['FBAFR'][] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_ITFBA'] = $fba_csv['FBAIT'][] = is_null($cell_val) ? 'N' : $cell_val;
				
				//Marec	 
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_DE'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_DEFBA'] = $fba_csv['FBADE'][] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_ES'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_FR'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_FRFBA'] = $fba_csv['FBAFR'][] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_IT'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_ITFBA'] = $fba_csv['FBAIT'][] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_NL'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_UK'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_UKFBA'] = $fba_csv['FBAUK'][] = is_null($cell_val) ? 'N' : $cell_val;
				 $col++;
  				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow Retail'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();				
  				$store_csv['RAINBOW RETAIL DE'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_DE_FBA'] = $fba_csv['FBADE'][] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_FRFBA'] = $fba_csv['FBAFR'][] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_Retail_ES'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_Retail_FR'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_Retail_IT'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_UK_FBA'] = $fba_csv['FBAUK'][] = is_null($cell_val) ? 'N' : $cell_val;
				
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Cdiscount_FR'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Real_DE'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['eBay'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Onbuy'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Flubit'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Fnac'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Fyndiq'] = is_null($cell_val) ? 'N' : $cell_val;
				
				$col++;	
 				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_ES_FBA'] = $fba_csv['FBAES'][] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_IT_FBA'] = $fba_csv['FBAIT'][] = is_null($cell_val) ? 'N' : $cell_val;
				
				/*---------------------Pankaj SFL fields(23-11-2021)------------------*/
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['color'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['keywords'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['bullet_1'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['bullet_2'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['bullet_3'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['bullet_4'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['bullet_5'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['root_category'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['sub_category'] = is_null($cell_val) ? 'N' : $cell_val;
 				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['ship_height'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['ship_width'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['ship_length'] = is_null($cell_val) ? 'N' : $cell_val;
 				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['ship_weight'] = is_null($cell_val) ? 'N' : $cell_val;
 				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['image_url_1'] = is_null($cell_val) ? 'N' : $cell_val;
 				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['image_url_2'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['image_url_3'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['image_url_4'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['image_url_5'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['image_url_6'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$product_csv['image_url_7'] = is_null($cell_val) ? 'N' : $cell_val;
				
				$fba_csv['FBM'][] = 'Y'; 
					
 				$storeIds = [];
				
				$storedata = $this->Store->find('all', array(
																'conditions' => array('OR' =>[
 																	'store_name' => array_keys($store_csv) ,
																	'store_alias' => array_keys($store_csv)
																])
																)
															); 
															
				
  					
				if(count($storedata) > 0){
					foreach($storedata as $str){
						$storeIds[]  = 	$str['Store']['id'];
					}
 				}  
				
				$productdata['store_name']				=	implode(",",$storeIds); 
				$product_csv['description']				=	addslashes($objWorksheet->getCellByColumnAndRow(4,$i)->getValue());		 	
				$product_csv['store_allowed']			=	json_encode($store_csv);
				$product_csv['referral_category']		=	$objWorksheet->getCellByColumnAndRow(28,$i)->getValue();
				$product_csv['referral_fee']			=	$objWorksheet->getCellByColumnAndRow(29,$i)->getValue();
				$product_csv['margin']					=	$objWorksheet->getCellByColumnAndRow(30,$i)->getValue();
 				$product_csv['supplier']				=	addslashes($objWorksheet->getCellByColumnAndRow(31,$i)->getValue());
				$product_csv['supplier_code']			=	$objWorksheet->getCellByColumnAndRow(32,$i)->getValue();
 				$product_csv['file_name']				= 	$this->request->data['Product']['Import_file']['name'];
				
				foreach($productdscdata as $pd => $v){
					$product_csv[$pd] = $v;
				}
				
				foreach($productdata as $pd => $v){
					$product_csv[$pd] = $v;
				}
				foreach($store_csv as $channel_title => $val){
					if(strtolower($val) == 'y'){
						 $channelData['y'][] = array('sku' => $productdata['product_sku'],
									'channel_title' =>$channel_title,	
									'channel_sku_title'=>$productdata['product_name'],										
									'asin' => $productdata['asin_no'],
									
								);
					}
					if(strtolower($val) == 'x'){
						$channelData['x'][] = array('sku' => $productdata['product_sku'],
									'channel_title' =>$channel_title,	
									'channel_sku_title'=>$productdata['product_name'],										
									'asin' => $productdata['asin_no'],
									
								);
					}
				} 
				foreach($hscode as $ccode => $hs_code){
					$product_csv[$ccode] = $hs_code;
					//---------------Update Store data--------------------
					$store = [];
					if(!empty($product_csv['product_sku'])){
						$checHs = $this->ProductHscode->find('first', array(
																	'conditions' => array(
																		'sku' => $product_csv['product_sku'],
																		'country_code' => $ccode
																		),
																	 'fields' =>['id']
																	)
																); 
						
						$store['sku']    		= $product_csv['product_sku'];
						$store['country_code']  = $ccode;
						$store['hs_code']		= $hs_code;
						$store['username'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
						if(count($checHs) > 0)
						{
							$store['id']			= 	$checHs['ProductHscode']['id'];
							$store['timestamp']		= 	date('Y-m-d H:i:s');
							$this->ProductHscode->saveAll($store);
						}else{
							$store['added_date']	= 	date('Y-m-d H:i:s');
							$this->ProductHscode->saveAll($store);
						}
					}
					//-------End									
				} 
				
				$fba_final = [];
				foreach(array_keys($fba_csv) as $fb){
					 
					if(in_array('Y', $fba_csv[$fb]) || in_array('y',$fba_csv[$fb])){
 						$fba_final[$fb] = 'Y';
					}else{
						$fba_final[$fb] = 'N';
					}
				} 
				 
				foreach($fba_final as $str => $v){
					$product_csv[$str] = $v;
					//---------------Update Store data--------------------
					$store = [];
 					if(!empty($product_csv['product_sku'])){
						$checFbm = $this->SkuFbmFba->find('first', array(
																	'conditions' => array(
																		'sku' => $product_csv['product_sku'],
																		'store' => $str),
																	'fields' => ['id']
																	)
																); 
						$store['store']  = 	$str;
						$store['sku']  = 	$product_csv['product_sku'];
						$store['status'] = 	strtoupper($v);
						if(count($checFbm) > 0)
						{
							$store['id']				= 	$checFbm['SkuFbmFba']['id'];
							$store['updated_date']		= 	date('Y-m-d H:i:s');
							$store['updated_by']		= 	$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
							$this->SkuFbmFba->saveAll($store);
							 
						}else{
							$store['added_date']	= 	date('Y-m-d H:i:s');
							$this->SkuFbmFba->saveAll($store);
						}
					} 
					//-------End									
				}  
				//pr($product_csv);
				
					//----------------- Suppliers
  					$supmaps = $this->SupplierMapping->find( 'first' , array( 'conditions' => array( 'master_sku' => $product_csv['product_sku'], 'supplier_sku' => $product_csv['supplier_code'] ) ) );
 						
					if(count($supmaps) > 0){																				
						$sql = "UPDATE `supplier_mappings` SET `supplier_sku` = '".$product_csv['supplier_code']."' WHERE `master_sku` = '".$product_csv['product_sku']."' AND `sup_code` = '".$product_csv['supplier']."'";											
						file_put_contents(WWW_ROOT."logs/xapi_".date('Ymd').".log", $sql."\n", FILE_APPEND | LOCK_EX);
						try{
							$this->SupplierMapping->query($sql);
							$msg['supplier_upd'][$product_csv['supplier']] = $product_csv['supplier'];
						}catch (Exception $e) {
							$msg['error'][] = 'Supplier exception: '. $e->getMessage(). "\n";
							file_put_contents(WWW_ROOT."logs/xapier_".date('Ymd').".log", $e->getMessage()."\n", FILE_APPEND | LOCK_EX);
						}
							
					}else{			
						$sup = $this->SupplierDetail->find( 'first' , array( 'conditions' => array( 'supplier_code' => $product_csv['supplier']),'fields'=>['id'] ) );
						if(count($sup) > 0){				
							$sql = "INSERT INTO `supplier_mappings` (`sup_inc_id`,`sup_code`,`master_sku`, `supplier_sku`,`price`,`sup_title`)
							 VALUES 
							('".$sup['SupplierDetail']['id']."','".$product_csv['supplier']."','".$product_csv['product_sku']."','".$product_csv['supplier_code']."','".$product_csv['price']."','".$product_csv['product_name']."');";
							
							try{
								$this->SupplierMapping->query($sql);
								file_put_contents(WWW_ROOT."logs/xapi_".date('Ymd').".log", $sql."\n", FILE_APPEND | LOCK_EX);
 							}catch (Exception $e) {
								$msg['error'][] = 'Supplier exception: '. $e->getMessage(). "\n";
								file_put_contents(WWW_ROOT."logs/xapier_".date('Ymd').".log", $e->getMessage()."\n", FILE_APPEND | LOCK_EX);
							}
						}
					}
			
				//-----------------End of Supplier
				
			 
				// Check also brand is exists or not
				$this->loadModel( 'Brand' );
				$getBrand = $this->Brand->find( 'first' , array( 'conditions' => array( 'Brand.brand_name' => $productdscdata['brand'] ) ) );
				
				if( count( $getBrand ) == 0 )
				{
					$brandName = $productdscdata['brand'];
					$brandAlias = strtolower($brandName);					
					$this->Brand->query( "insert into brands ( brand_name, brand_alias ) values ( '{$brandName}' , '{$brandAlias}' )" );					
					$productdscdata['brand'] = $brandName;
					
				}
				else
				{
					$productdscdata['brand'] = $productdscdata['brand'];
				}
				
				// Check also Category is exist or note
				$this->loadModel( 'Category' );
				$getCategory = $this->Category->find( 'first' , array( 'conditions' => array( 'Category.category_name' => $productdata['category'] ) ) );
				if( count( $getCategory ) == 0 )
				{
					$categoryName = $productdata['category'];
					$catAlias = strtolower($categoryName);					
					$this->Category->query( "insert into categories ( category_name, category_alias ) values ( '{$categoryName}' , '{$catAlias}' )" );					
					$catId = $this->Category->getLastInsertId();
					$productdata['category_id'] = $catId;
					$productdata['category_name'] = $categoryName;
				}
				else
				{
					$productdata['category_id'] = $getCategory['Category']['id'];
					$productdata['category_name'] = $productdata['category'];
				}
				
				if(!empty($product_csv['product_sku'])){
					$checPro		=	$this->ProductCsv->find('first', array(
																'conditions' => array('product_sku' => $product_csv['product_sku'],'asin' => $product_csv['asin']),
																'fields' => ['id']
																)
															); 
					$pro_save = 0;
  					 
					try{
						if(count($checPro) > 0)
						{
							$product_csv['id']				= 	$checPro['ProductCsv']['id'];
							$product_csv['local_barcode']   =   $xsbarcode;
							$product_csv['updated_date']	= 	date('Y-m-d H:i:s');
							$product_csv['updated_by']		= 	$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
							$this->ProductCsv->saveAll($product_csv);
							//$update_msg[]  = $product_csv['asin'].' & '.$product_csv['product_sku'];
							$product_csv_ids[$product_csv['id']] = $product_csv['id'];
						}else{
						
							$checAsinPro		=	$this->ProductCsv->find('first', array(
																	'conditions' => array('product_sku' => $product_csv['product_sku']),
																	'fields' => ['id','product_sku']
																	)
																); 
							if(count($checAsinPro) > 0)
							{	$product_csv['id'] = $checAsinPro['ProductCsv']['id'];
								$product_csv['local_barcode'] = $xsbarcode;
								//$this->ProductCsv->saveAll($product_csv);
								$product_csv_ids[$checAsinPro['ProductCsv']['id']] = $checAsinPro['ProductCsv']['id']; 
							}else{								
								$product_csv['local_barcode'] = $xsbarcode;
								$product_csv['added_date']	  = date('Y-m-d H:i:s');
								$product_csv['added_by']	  = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
								$this->ProductCsv->saveAll($product_csv);
 								$pcsv_inc_id    = $this->ProductCsv->getLastInsertId(); 
								$product_csv_ids[$pcsv_inc_id] = $pcsv_inc_id; 
							}
  						}
						
 						$checkproduct		=	$this->Product->find('first', array(
															'conditions' => array('Product.product_sku' => $productdata['product_sku'])
															)
														); 
						
					 
						if(count($checkproduct) == 0)
						{
							//echo '<br>'.$productdata['product_sku'];
							$productdata['product_status'] = 0;
							
							try{
								$this->Product->saveAll($productdata);
 							}
							catch (Exception $e) {
								$error_msg[] =  'Caught exception: '.  $e->getMessage(). "\n";
							} 
							
							$productid	=	$this->Product->getLastInsertId();
							$success_msg[]  = $product_csv['product_sku'].' is added.';
							$productdscdata['product_id'] = $productid;
							try{
								$this->ProductDesc->saveAll($productdscdata, array('validate' => false));
							}catch (Exception $e) {
								$error_msg[] = 'Caught exception: '.  $e->getMessage(). "\n";
							} 
							
							$productDescid	=	$this->ProductDesc->getLastInsertId();
							
							$productprice['product_id'] = $productid;
							$this->ProductPrice->saveAll($productprice, array('validate' => false));
							 
							
							/*  generate barcode  */
							$shiftValue = 8;
							$moveValue = 2;
							//$count = 0;
							if( $xsbarcode == '' )
							{
								$barcode = $productDescid;
								$lengthOfId = strlen($barcode);					
								$mainDiffer = $lengthOfId - $moveValue; // ? - 2 = ?
								$differ = $shiftValue - $mainDiffer; // 8 - ? = ?
								$newSpaceVake = '';
								for( $sk = 0; $sk < $differ; $sk++ ) 
								{
									if( $sk == 0 )
									{
										$newSpaceVake .= '0';
									}
									else
									{
										$newSpaceVake = $newSpaceVake.'0';
									}
								}
								$newBarcode = rand(11,99) . $newSpaceVake . $barcode;
								$newSpaceVake = '';
								$data['ProductDesc']['id'] 				= 	$productDescid;
								$data['ProductDesc']['barcode']			=	$global_barcode;
								$this->ProductDesc->saveAll( $data, array('validate' => false) ); 
								/************************Save Global barcode*********************************/
								$barcodeData = [];
								$barcodeData['global_barcode']  = $global_barcode;
								$barcodeData['barcode'] 		= $newBarcode;
								$barcodeData['username'] 		= $this->Session->read('Auth.User.username');
								$barcodeData['added_date'] 		= date('Y-m-d H:i:s');
								$this->ProductBarcode->saveAll($barcodeData);
							} 
							else {
								/************************Save Global barcode*********************************/
								$barcodeData = [];
								$barcodeData['global_barcode']  = $global_barcode;
								$barcodeData['barcode'] 		= $xsbarcode;
								$barcodeData['username'] 		= $this->Session->read('Auth.User.username');
								$barcodeData['added_date'] 		= date('Y-m-d H:i:s');
								$this->ProductBarcode->saveAll($barcodeData);
							}
							/*****************************************/
							$count++;
						}
						else
						{
							$productdata['product_status'] = 0;
							$productdata['id'] = $checkproduct['Product']['id'];
							$this->Product->saveAll($productdata);
							
 							$productdscdata['id'] = $checkproduct['ProductDesc']['id'];
							$this->ProductDesc->saveAll($productdscdata, array('validate' => false));
 							
							$alreadycount++;
							$update_msg[]  = $product_csv['product_sku'].' is updated. ';
							$getbar = $this->ProductBarcode->find('first',array('conditions' => array('global_barcode' => $checkproduct['ProductDesc']['barcode'] ) ) );
							$barcodeData = [];
							if(count($getbar) > 0){
								$barcodeData['id'] 				= $getbar['ProductBarcode']['id'];
								$barcodeData['global_barcode']  = $checkproduct['ProductDesc']['barcode'];
							}else{
								$barcodeData['global_barcode']  = $global_barcode;
							}	
							$barcodeData['barcode'] 		= $xsbarcode;
 							$barcodeData['username'] 		= $this->Session->read('Auth.User.username');
							//$this->ProductBarcode->saveAll($barcodeData);
							
							$sql = "UPDATE `product_barcodes` SET `barcode` = '".$xsbarcode."' WHERE `global_barcode` = '".$checkproduct['ProductDesc']['barcode']."'";						 							$this->ProductBarcode->query($sql);//28-10-2021
							

							
  						}
					
					}
					catch (Exception $e) {
						$error_msg[] = 'Caught exception: '.  $e->getMessage(). "\n";
					}
				} 
				
  				$frizileMark	=	$this->ProductDesc->find('count', array(
															'conditions' => array('ProductDesc.frezile_mark' => $productdscdata['frezile_mark'])
										)
									);

				
			}  
			
			$channel_msg = $this->CreateChannelSku($channelData);
			 
 			$emsg = ''; $msg = '';
 			 
			if(count($success_msg) > 0){
				$msg .= count($success_msg) .' Products Added.';
			}
			if(count($update_msg) > 0 ){
				$msg .= count($update_msg) .' Products Updated.';
			}
			 
			if(count($channel_msg)){
				if(isset($channel_msg['success'])){
					$msg .= "\n".$channel_msg['success'];
				}
				if(isset($channel_msg['warnings'])){
					$msg .= "\n".$channel_msg['warnings'];
				}
				if(isset($channel_msg['errors'])){
					$emsg .= "\n".$channel_msg['errors'];
				}
				 
				file_put_contents(WWW_ROOT. 'logs'.DS.'Uploads.channel_msg_'.date('Ymd').'.log',print_r($channel_msg,1),FILE_APPEND|LOCK_EX); 				 
			}
			
			//$this->Session->setflash( $msg, 'flash_success' ); 
			
			//$msg = '';
			if(count($error_msg) > 0){
				$emsg .= count($error_msg) .' have issue. may be asin alredy mapped with other sku.';
				
				if(file_exists(WWW_ROOT. 'logs'.DS.$already_map)){
					$emsg .= ' <a href="'.Router::url('/', true).'logs/'.$already_map.'">Download file</a>';
				}
				
 				//$this->Session->setFlash($msg.'<br>', 'flash_danger');
			}
			 
			//$msg .= '<div style="background-color:#FF6633;padding:5px;">'.$emsg.'</div>';
			$message['success'] = $msg;
			$message['danger'] = $emsg;
			$this->Session->setflash( $message, 'flash_all' ); 
			
 			$this->redirect($this->referer());
		}
		else
		{
			$this->Session->setFlash('Please Insert CSV File.', 'flash_danger');
			$this->redirect($this->referer());
		}
	}
	
	public function CreateChannelSku($channelData = []){
		
		$this->loadModel( 'Skumapping' );
		$username     = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
 		$updata = []; $rowdata = [];
		foreach($channelData as $val => $products){  
			foreach($products as $v){  
				if(strtolower($val) == 'y'){
					$rowdata[] = array(	'sku' => $v['sku'],
										'channel_title' =>$v['channel_title'],	
										'channel_sku_title'=>$v['channel_sku_title'],										
										'asin' => $v['asin']
									);
				}
				if(strtolower($val) == 'x'){
					$updata[] = array(	'sku' => $v['sku'],
										'channel_title' =>$v['channel_title'],		
										'channel_sku_title'=>$v['channel_sku_title'],										
										'asin' => $v['asin']
									);
				}
				
 			}
		}
  		 
		$up_count = 0; 
		$not_found_count = 0; 
		$errors = []; $success = []; $warnings = [];
		$msg = [];
 		
 		$warningArr			= array();
		$insert_str 		= ''; 	
 		$all_exist  		= ''; 	
		$asin_exist 		= '';
		$all_exist_count 	= 0;
		$asin_exist_count 	= 0;
		$add_count 			= 0; 
			
  		if (count($rowdata)> 0) 
		{  
			$skuArr 		 = $this->getSkuId();
			$chco 			 = $this->getChannelCountry(); 
			$ChannelIdArr    = $chco['ch_id'];
			$CountryCodeArr  = $chco['country'];
			
			foreach($rowdata as $k => $dataLine) {	
			
				$ch_id  = $ChannelIdArr[$dataLine['channel_title']]; 
				$ms_id 	= $skuArr[$dataLine['sku']];									
				 
				if(is_numeric($ms_id) && $ms_id > 0){						
					$result =  $this->Skumapping->find('first', array('conditions' => array('sku' => $dataLine['sku'],'asin' => $dataLine['asin'],'channel_name' => $dataLine['channel_title']),'fields' => ['channel_sku'])); 
					
					if(count($result) > 0){		
						$this->Skumapping->query("UPDATE 
						`skumappings` SET `listing_status` = 'active' 
							WHERE 
						`sku` = '".$dataLine['sku']."' AND
						`asin` = '".$dataLine['asin']."' AND 
						`channel_name` = '".$dataLine['channel_title']."'"); 
						file_put_contents(WWW_ROOT. 'logs'.DS.'Uploads.UpdateMapping_'.date('Ymd').'.log', 
						$dataLine['sku']."\t".$dataLine['asin']."\t".$dataLine['channel_title']."\t".'active'."\t".$username, 
						FILE_APPEND|LOCK_EX); 	
						
						$all_exist .= $dataLine['sku']."\t".$dataLine['asin']."\t".$dataLine['channel_title']."\t is Already Exists!\n";
						$all_exist_count++; 
 						
					 }else{	
							$result =  $this->Skumapping->find('first', array('conditions' => array('asin' => $dataLine['asin'],'channel_name' => $dataLine['channel_title']),'fields' => ['sku','asin','channel_sku'])); 
							
							 									
							if(count($result) > 0){	
								$this->Skumapping->query("UPDATE 
								`skumappings` SET `listing_status` = 'active' 
									WHERE 
								`asin` = '".$dataLine['asin']."' AND 
								`channel_name` = '".$dataLine['channel_title']."'"); 
								file_put_contents(WWW_ROOT. 'logs'.DS.'Uploads.UpdateMapping_'.date('Ymd').'.log', 
								$dataLine['asin']."\t".$dataLine['channel_title']."\t".'active'."\t".$username."\n", 
								FILE_APPEND|LOCK_EX); 	
 								
								$asin_exist .= $dataLine['asin']." is Already Exists for \t".$dataLine['channel_title']."\n" ;
								$asin_exist_count++;								
							}else{	
  							 
								$channel_sku  = $this->getAutoChannel($dataLine['channel_title']);	
								$country_code = $CountryCodeArr[$dataLine['channel_title']];	
 								$insert_str .= "('".$dataLine['sku']."','".$dataLine['asin']."','".$dataLine['channel_title']."',
											   '".$channel_sku."','".$country_code."','".$dataLine['channel_sku_title']."','".date("Y-m-d H:i:s")."','".$username."'),";	
								
 								$add_count++;	
																
								}												
						}	
				}
				else{
					 $errors[] = $dataLine['sku'].' Invalid Master SKU!';
				}
				
			}	
		}
		
			
 		/*------------------------UPDATE CHANNEL MAPPING---------------*/
		if(count($updata) > 0) 
		{						
   			foreach($updata as $k => $dataLine) {
				$result =  $this->Skumapping->find('first', array(
				'conditions' => array('sku' => $dataLine['sku'],'channel_name' => $dataLine['channel_title']),
				'fields' => ['channel_sku'])
				); 
				if(count($result) > 0){
					$up_count++;
					$this->Skumapping->query("UPDATE 
					`skumappings` SET `listing_status` = 'inactive' 
					WHERE 
					`sku` = '".$dataLine['sku']."' AND 
					`channel_name` = '".$dataLine['channel_title']."'"); 
					file_put_contents(WWW_ROOT. 'logs'.DS.'Uploads.UpdateMapping_'.date('Ymd').'.log', 
					$dataLine['sku']."\t".$dataLine['channel_title']."\t".'inactive'."\t".$username, 
					FILE_APPEND|LOCK_EX); 
				}else{
					$not_found_count++;
				}	
 			}	
			$success[] =  '#'.$up_count.' Record updated.';
		}
				
		if($all_exist_count > 0){
			$warnings[] = '#' .$all_exist_count .' SKU,ASIN ALREADY EXISTS FOR CHANNNEL.<a href="../logs/all_exist.txt" target="_blank">Download Log File</a>';			
			file_put_contents(WWW_ROOT. 'logs'.DS.'all_exist.txt', $all_exist, LOCK_EX); 		
		}							
		if($asin_exist_count > 0){
			$warnings[] = '#' .$asin_exist_count .' ASIN EXISTS ALREADY FOR CHANNNEL.<a href="../logs/asin_exist.txt" target="_blank">Download Log File</a>';			
			file_put_contents(WWW_ROOT. 'logs'.DS.'asin_exist.txt', $asin_exist, LOCK_EX); 		
		}
		
		if($insert_str !=''){
		   $insert_str = rtrim($insert_str,',');							   
		   $inSql = "INSERT INTO `skumappings`(`sku`,`asin`,`channel_name`,`channel_sku`,`country_code`,`channel_sku_title`,`added_date`,`username`)
										   VALUES $insert_str";
		   $this->Skumapping->query($inSql);	
		}	
		
		
		if($add_count){
			$success[] =  '#'.$add_count.' Channel sku added.';
		}
		if($not_found_count){
			$errors[] =  '#'.$not_found_count.' Record not found. Unable to update mapping.';
		}
		 if(count($success)>0){
			$success_msg = '';
			foreach($success as $sLine) {					
				$success_msg .= $sLine."\n";
			} 
			$msg['success'] = $success_msg;			
		}	
		
		if(count($warnings)>0){
			$warnings_msg = '';
			foreach($warnings as $wLine) {					
				$warnings_msg .= $wLine."\n";
			} 
			$msg['warnings'] = $warnings_msg;
		}	
		
		if(count($errors)>0){
			$errors_msg = '';
			foreach($errors as $eLine) {					
				$errors_msg .= $eLine."\n";
			} 
			$msg['errors'] = $errors_msg;			
		}		
							
 		return $msg;
 					 
	}
	
	public function getAutoChannel($channel_title){
		$channels = array();
		$this->loadModel('ProChannel');	
 		$result = $this->ProChannel->find('first', array('conditions' => array('channel_title' => $channel_title),'fields' => ['ch_id','prefix','increment_id']));						
 		$channel_sku = strtoupper($result['ProChannel']['prefix'].$result['ProChannel']['increment_id']);
 		$usql = "UPDATE `pro_channels` SET `increment_id` = increment_id + 1 WHERE `ch_id` = '".$result['ProChannel']['ch_id']."'";
		$this->ProChannel->query($usql);	
		return	$channel_sku;
	}
	public function getSkuId(){
	 
		$this->loadModel('Product');
		$products =  $this->Product->find('all', array('fields' => ['product_sku','id']));
		foreach($products as $pro){
			$data[$pro['Product']['product_sku']] = $pro['Product']['id'];
		}
		return $data;

	}
	
	public function getChannelCountry(){
	
		$data = array();		
		$this->loadModel('ProChannel');	
		$result = $this->ProChannel->find('all', array('fields' => ['ch_id','channel_title','country']));	
 		if(count($result) > 0){
			foreach($result as $val){
 				$data[$val['ProChannel']['channel_title']] = $val['ProChannel']['country'];
			}								
 		}		
		return	$data;	
	}
	
 	public function test()
	{
	
		$asins = ['B0064JWMTS','B0058NRT6W','B0058NS3E4','B0058NSI0I'];
		$url = 'http://www.techdrive.biz/api-get-asin-data-xsensys.php'; 
		$curl = curl_init(); 
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FAILONERROR, true); 
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
		curl_setopt($curl, CURLOPT_POST, count($asins));
		curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($asins));  
		$result = curl_exec($curl); 						 
		$error = curl_error($curl); 
		$info = curl_getinfo($curl);
		curl_close($curl);	
		$_result = json_decode($result,true);	
		 
		print_r($_result);
	//	file_put_contents(WWW_ROOT.'logs/'.$dfile, $asin .",". $location.",". implode(";",$fb)."\n", FILE_APPEND | LOCK_EX);	
		
		exit;
	}	
		
	public function GetAsinStatus()
	{
   
 		$this->autoLayout = 'ajax';
		$this->autoRender = false;
		
		$this->loadModel('Store');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
 		$this->loadModel('ProductCsv');
		$this->loadModel('SkuFbmFba');
  		 
		if($this->data['Product']['Import_file']['name'] != '')
		{
				
			$name		= $this->Session->read('Auth.User.first_name').'_'.$this->Session->read('Auth.User.last_name').'_'.$this->request->data['Product']['Import_file']['name'];
 			$filename   =  WWW_ROOT. 'logs'.DS.$name;
			move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
			
			
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel = $objReader->load('logs/'.$name);
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
		 
			$count	= 0;
			$alreadycount	=	0;
			$countbarcode	=	0;
 			$sheetData 		=	[];	
 			$dfile 			= 	$this->request->data['Product']['Import_file']['name'];
			
			file_put_contents(WWW_ROOT.'logs/'.$dfile, "ASIN,Location,Final Comment\n", LOCK_EX);	
			
			for($i=2; $i<=$lastRow; $i++) 
			{ 				 	
  				$asins[] 	  =	trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
				$sheetData[] = ['asin'=>trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue()),'location'=> $objWorksheet->getCellByColumnAndRow(1,$i)->getValue()]; 
				/*$location =	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				
				$checPro = $this->ProductCsv->find('all', array(
																'conditions' => array('asin' => $asin),
																'fields' => ['id','FBM','FBAUK','FBADE','FBAES','FBAFR','FBAIT','FBANL','FBAPL','FBAUSA','FBACAN']
																)
															); 
				$fb = [];											
				if(count($checPro) > 0){
					foreach($checPro as $val){
					
						foreach(['FBM','FBAUK','FBADE','FBAES','FBAFR','FBAIT','FBANL','FBAPL','FBAUSA','FBACAN'] as $f){
						
							if(isset($val['ProductCsv'][$f]) && $val['ProductCsv'][$f] == 'Y'){
								$fb[$f] =  $f;
							}		
						}
					}
				}
 			 
			 	file_put_contents(WWW_ROOT.'logs/'.$dfile, $asin .",". $location.",". implode(";",$fb)."\n", FILE_APPEND | LOCK_EX);*/											
 			} 
			
			$url = 'http://www.techdrive.biz/api-get-asin-data-xsensys.php'; 
			$curl = curl_init(); 
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_FAILONERROR, true); 
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
			curl_setopt($curl, CURLOPT_POST, count($asins));
			curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($asins));  
			$result = curl_exec($curl); 						 
			$error = curl_error($curl); 
			$info = curl_getinfo($curl);
			curl_close($curl);	
			$_result = json_decode($result,true);	
			 
			foreach($sheetData as $val){		
				$asindata = [];
				if(isset($_result[$val['asin']])){
					$asindata = $_result[$val['asin']];
				}		
				file_put_contents(WWW_ROOT.'logs/'.$dfile, $val['asin'] .",". $val['location'].",". implode(";",$asindata)."\n", FILE_APPEND | LOCK_EX); 	
			}
 			
			 
 			header('Content-Description: File Transfer');
			header('Content-Type: application/force-download');
			header('Content-Disposition: attachment; filename='.basename($dfile));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize(WWW_ROOT.'logs/'.$dfile));
			ob_clean();
			flush();
			readfile(WWW_ROOT.'logs/'.$dfile);
			exit;
		
		
		}
		else
		{
			$this->Session->setFlash('Please Insert CSV File.', 'flash_danger');
			$this->redirect($this->referer());
		}
	}
	
	public function BulkActive()
	{
		$this->autoLayout = 'ajax';
		$this->autoRender = false; 
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc'); 
		$this->loadModel('SkuPercentRecord');
		$this->loadModel('Store');
 
 		$this->data['Product']['Import_file']['name'];
		if($this->data['Product']['Import_file']['name'] != '')
		{
				
				$name	  =	'active_'.$this->Session->read('Auth.User.username').'__'.$this->request->data['Product']['Import_file']['name'];
				$filename = WWW_ROOT. 'files'.DS.$name;
				move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
				
 				App::import('Vendor', 'PHPExcel/IOFactory');
				
				$objPHPExcel = new PHPExcel();
				$objReader= PHPExcel_IOFactory::createReader('CSV');
				$objReader->setReadDataOnly(true);				
				$objPHPExcel=$objReader->load('files/'.$name);
				$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
				$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
				$colString	= $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
				$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
				
				$log__file = WWW_ROOT .'logs/xsensys_sku_not_found_'.date('dmY').".csv";	
 				$updated = 0;
				for($i=2; $i<=$lastRow; $i++) 
				{
					
					$sku =	trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue()); 
					
 					$sql  =   "SELECT `product_sku` FROM `products`  WHERE `product_sku` LIKE '".$sku."' ";
					$results = $this->Product->query($sql);		
					if(count($results) > 0){
						$usql ="UPDATE `products` SET `is_deleted` = 0,`product_status` = 0 WHERE `product_sku` = '".$sku."' "; 
						$this->Product->query($usql); 
  						$updated++;	
					}else{
						echo $sku. "  not found<br />\n";
   						file_put_contents($log__file,$sku."\n",  FILE_APPEND|LOCK_EX);
					}
					
					
					$sql  =   "SELECT `sku` FROM `sku_percent_records`  WHERE `sku` LIKE '".$sku."' ";
					$results = $this->Product->query($sql);	
					if(count($results) > 0){
						$ssql ="UPDATE `sku_percent_records` SET `deleted_status` = 0 WHERE `sku` = '".$sku."'";  
						$this->Product->query($ssql);
					}else{
  	 					echo $sku. "  not found sku_percent_records<br />\n";
   						file_put_contents($log__file,$sku."\n",  FILE_APPEND|LOCK_EX);
					}
					
 					
					$getStoreName	=	$this->Store->find( 'all', array( 
							'conditions' => array( 'Store.status' => 1),
							'fields' => array( 'Store.store_name, Store.id') 
							)
						);
					
					foreach($getStoreName as $_storeName){
					
						$chkStoreRec = $this->SkuPercentRecord->find('first', array('conditions' => 
													array('sku' => $productdata['product_sku'],
														'store_name' => $_storeName['Store']['store_name'])));
					
						$storeNameAlies = str_replace('.','_',str_replace(' ','_',$_storeName['Store']['store_name']));
						$product	=	$this->Product->find('first', 
									array( 
										'fields' => array( 
													'Product.current_stock_level', 
													'Product.product_sku',
													'Product.store_name',
													'Product.product_name',
													'ProductDesc.barcode',
													 
												),
										'conditions' => array('Product.product_sku' => $sku ) ) );
										
						$sprData =[];
						$sprData['store_id'] 		= $_storeName['Store']['id'];
						$sprData['store_name'] 		= $_storeName['Store']['store_name'];
						$sprData['store_alies'] 	= $storeNameAlies;
						$sprData['sku'] 			= $product['Product']['product_sku'];
						$sprData['current_stock'] 	= 1;
						$sprData['product_title'] 	= $product['Product']['product_name'];
						$sprData['barcode'] 		= $product['ProductDesc']['barcode'] ;
						$sprData['deleted_status']	= 0;
						$sprData['percentage']		= 100;
						if(count($chkStoreRec) > 0){
							$sprData['id'] = $chkStoreRec['SkuPercentRecord']['id'];
						}
						$this->SkuPercentRecord->saveAll( $sprData ); 
					
					}
  					 
  				}
				
				$this->Session->setFlash($updated.' : SKU updated.', 'flash_success');
 				$this->redirect($this->referer());
		}
		else
		{ 
			$this->Session->setFlash('Please Insert CSV File.', 'flash_danger');
			$this->redirect($this->referer());
		}
	}
	
	public function BulkInActive()
	{
		$this->autoLayout = 'ajax';
		$this->autoRender = false; 
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc'); 
		$this->loadModel('SkuPercentRecord');
		$this->loadModel('Store');
 
 		$this->data['Product']['Import_file']['name'];
		if($this->data['Product']['Import_file']['name'] != '')
		{
				
				$name	  =	'inactive_'.$this->Session->read('Auth.User.username').'__'.$this->request->data['Product']['Import_file']['name'];
				$filename =  WWW_ROOT. 'files'.DS.$name; 
				move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
				
				App::import('Vendor', 'PHPExcel/IOFactory');
				
				$objPHPExcel = new PHPExcel();
				$objReader= PHPExcel_IOFactory::createReader('CSV');
				$objReader->setReadDataOnly(true);				
				$objPHPExcel=$objReader->load('files/'.$name);
				$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
				$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
				$colString	= $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
				$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
				
				$log__file = WWW_ROOT .'logs/xsensys_sku_not_found_'.date('dmY').".csv";	
 				$updated = 0;
				for($i=2; $i<=$lastRow; $i++) 
				{
					
					$sku =	trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue()); 
					
 					$sql  =   "SELECT `product_sku` FROM `products`  WHERE `product_sku` LIKE '".$sku."' ";
					$results = $this->Product->query($sql);		
					if(count($results) > 0){
						$usql ="UPDATE `products` SET `is_deleted` = 1,`product_status` = 1 WHERE `product_sku` = '".$sku."' "; 
						$this->Product->query($usql); 
  						$updated++;	
					}else{
						echo $sku. "  not found<br />\n";
   						file_put_contents($log__file,$sku."\n",  FILE_APPEND|LOCK_EX);
					}
					
					
					$sql  =   "SELECT `sku` FROM `sku_percent_records`  WHERE `sku` LIKE '".$sku."' ";
					$results = $this->Product->query($sql);	
					if(count($results) > 0){
						$ssql ="UPDATE `sku_percent_records` SET `deleted_status` = 1 WHERE `sku` = '".$sku."'";  
						$this->Product->query($ssql);
					}else{
  	 					echo $sku. "  not found sku_percent_records<br />\n";
   						file_put_contents($log__file,$sku."\n",  FILE_APPEND|LOCK_EX);
					}
					
   				}
				
				$this->Session->setFlash($updated.' : SKU updated.', 'flash_success');
 				$this->redirect($this->referer());
		}
		else
		{ 
			$this->Session->setFlash('Please Insert CSV File.', 'flash_danger');
			$this->redirect($this->referer());
		}
	}
	
	public function DownloadSample()
	{
		$this->layout = '';
		$this->autoRender = false;
		if( $this->request->data['Product']['Import_type'] != '' )
		{
			App::import('Vendor', 'PHPExcel/IOFactory');
			App::import('Vendor', 'PHPExcel');  
			$objPHPExcel = new PHPExcel();     
			$objPHPExcel->setActiveSheetIndex(0);
			$getBase = Router::url('/', true);
			if($this->request->data['Product']['Import_type'] == 'products'){
			
 				$uploadRemote = $getBase.'sample_files/add_products.csv';
				$file = 'add_products.csv';
				$contenttype = "application/force-download";
				header("Content-Type: " . $contenttype);
				header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
				readfile($uploadRemote);
			
			}elseif($this->request->data['Product']['Import_type'] == 'active'){
			
 				$uploadRemote = $getBase.'sample_files/active.csv';
				$file = 'active.csv';
				$contenttype = "application/force-download";
				header("Content-Type: " . $contenttype);
				header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
				readfile($uploadRemote);
			
			}elseif($this->request->data['Product']['Import_type'] == 'inactive'){
 				 
				$uploadRemote = $getBase.'sample_files/inactive.csv';
				$file = 'inactive.csv';
				$contenttype = "application/force-download";
				header("Content-Type: " . $contenttype);
				header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
				readfile($uploadRemote);
			
			}else{
				$strName = "DownloadSample".$this->request->data['Product']['Import_type'];	
				$className = new UploadsController();		
				$className->{"$strName"}();
			}
		}
		else
		{
			$this->Session->setflash( 'Please select file type.', 'flash_danger' );  
			$this->redirect( array( "controller" => "JijGroup/UploadProducts" ) );
		}
	}
	
	public function DownloadSampleUploadStockQuantity()
	{
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$getBase = Router::url('/', true);
		$uploadRemote = $getBase.'files/UploadStockQuantity.csv';
		$file = 'UploadStockQssuantity.csv';
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
	}
	
	public function DownloadSampleInventery()
	{
		echo "Inventery";
	}
	
	public function DownloadSampleBinLocation()
	{
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$getBase = Router::url('/', true);
		$uploadRemote = $getBase.'files/UploadLocation.csv';
		$file = 'UploadLocation.csv';
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
	}

	public function DownloadSampleaAddNewProduct()
	{
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$getBase = Router::url('/', true);
		$uploadRemote = $getBase.'files/AddNewProduct.csv';
		$file = 'AddNewProduct.csv';
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
	}

	
	/*********************************************************************************/
	
	public function UploadCsvFile()
	{
		$this->layout = '';
		$this->autoRender = false;
		$file	=	explode('.' ,$this->data['Product']['Import_file']['name']);
		$fileName = $file[0];
		$strName = "UploadCsvFile".$file[0];
		$filearray = array( 'UploadLocation', 'UploadStockQuantity','AddNewProducts' );
		if( $fileName != '' && in_array( $fileName, $filearray ))
		{
			$className = new UploadsController();		
			$msg	=	$className->{"$strName"}( $this->request->data );
			$this->Session->setflash( $msg, 'flash_success' );       
			$this->redirect( array( "controller" => "JijGroup/UploadProducts" ) );
		}
		else
		{
			$this->Session->setflash( 'Please insert valid file with valid name', 'flash_danger' );  
			$this->redirect( array( "controller" => "JijGroup/UploadProducts" ) );
		}
	}
	
	
	public function UploadCsvFileUploadLocation( $data )
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'BinLocation' );
		$this->loadModel( 'Product' );
		$this->BinLocation->query( 'TRUNCATE bin_locations' );
		
		$filename = WWW_ROOT. 'files'.DS.$data['Product']['Import_file']['name']; 
		move_uploaded_file($data['Product']['Import_file']['tmp_name'],$filename);  
		$name		=	 $data['Product']['Import_file']['name'];
	
		App::import('Vendor', 'PHPExcel/IOFactory');
		$objPHPExcel = new PHPExcel();
		$objReader= PHPExcel_IOFactory::createReader('CSV');
		$objReader->setReadDataOnly(true);
		$objPHPExcel=$objReader->load('files/'. $name );
		$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
		$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
		$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
		$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
		
		$insertIncrement = 0;
		$notinsertIncrement = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$Sku	=	$objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
				if( $Sku != '')
				{
					$getProductDetail 	=	$this->Product->find( 'first', 
											array( 'conditions' => array( 'Product.product_sku' => $Sku ),
													'fields' => array( 'ProductDesc.barcode')
													) 
												);
					if( $objWorksheet->getCellByColumnAndRow(2,$i)->getValue() < 0 )
					{
						$binlocationQty = 0;
					}
					else
					{
						$binlocationQty = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
					}
					
					$barcode						=	$getProductDetail['ProductDesc']['barcode'];
					
					$data['barcode']				=	$barcode;
					$data['bin_location']			=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
					$data['stock_by_location']		=	$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
					
					$result	=	$this->BinLocation->saveAll( $data );
					if($result)
					{
						$insertIncrement++;
					}
					else
					{
						$notinsertIncrement++;
					}
					$msg	=	'(<b>'.$insertIncrement.'</b>) Bin Location Inserted Successfully.';
				}
				else
				{
					$msg	=	'Please fill all Sku.';
				}
			}
		
		return $msg; 
	}
	
	
	 
	
	public function UploadCsvFileUploadStockQuantity( $data )
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'Product' );
		$filename = WWW_ROOT. 'files'.DS.$data['Product']['Import_file']['name']; 
		move_uploaded_file($data['Product']['Import_file']['tmp_name'],$filename);  
		$name		=	 $data['Product']['Import_file']['name'];
	
		App::import('Vendor', 'PHPExcel/IOFactory');
		$objPHPExcel = new PHPExcel();
		$objReader= PHPExcel_IOFactory::createReader('CSV');
		$objReader->setReadDataOnly(true);
		$objPHPExcel=$objReader->load('files/'. $name );
		$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
		$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
		$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
		$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
		$insertIncrement = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$sku		=	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$qty		=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$this->Product->updateAll(array('Product.uploaded_stock' => $qty ,  'Product.current_stock_level' => $qty  ), array('Product.product_sku' => $sku));
				$insertIncrement++;
			}
		$msg	=	'(<b>'.$insertIncrement.'</b>) Sku Updates Successfully.';
		return $msg; 
	}
	
 	public function getProcessedOrder()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'ScanOrder' );
		
		$getData = $this->request->data;		
		
		$getFrom = date('Y-m-d',strtotime($getData['getFrom']));
		$getEnd  = date('Y-m-d',strtotime($getData['getEnd']. "+1 days"));
		$getStoreSelection = '';//$getData['getStoreSelection'];
	
 		 
		$path = WWW_ROOT .'img/upload/';
		$filename = "ProcessedOrder.txt";
		$uploadRemote = $path.$filename;
		
		$header = "OrderId\tSplit Order Id\tProcessedDate\tReferenceNum\tSubSource\tFullName\tCountry\tPostcode\tSKU\tQuantity\tService Name\tProvider_ref_Code\tEnvelope Name\tEnvelope Cost\tOrder weight\tPrice\tCurrency\r\n";
		file_put_contents($path.$filename,$header, LOCK_EX);
		
 		
		$sql = "SELECT OpenOrder.status,OpenOrder.num_order_id,OpenOrder.amazon_order_id,OpenOrder.date,OpenOrder.process_date,OpenOrder.customer_info, OpenOrder.totals_info,OpenOrder.sub_source,MergeUpdate.product_order_id_identify,MergeUpdate.sku,MergeUpdate.service_name,MergeUpdate.provider_ref_code,MergeUpdate.envelope_name,MergeUpdate.envelope_cost,MergeUpdate.packet_weight,MergeUpdate.price,SUM(ScanOrder.quantity) as QTY FROM open_orders OpenOrder
 				JOIN merge_updates MergeUpdate
					ON  OpenOrder.num_order_id = MergeUpdate.order_id AND OpenOrder.status = 1 AND OpenOrder.process_date BETWEEN '".$getFrom."' AND '".$getEnd."'
				JOIN scan_orders ScanOrder
					ON ScanOrder.split_order_id = MergeUpdate.product_order_id_identify
				GROUP BY ScanOrder.split_order_id ORDER BY OpenOrder.process_date  ";
		 
		
		$data = $this->OpenOrder->query($sql);
		
		$inc = 0;
		$content = '';
		
		foreach($data as $getProdessOrderDetail )
		{
			 	$inc++;
			 
				$customerInfo	= unserialize($getProdessOrderDetail['OpenOrder']['customer_info']);
				$totals_info	= unserialize($getProdessOrderDetail['OpenOrder']['totals_info']);
				 
				$quantity		=	$getProdessOrderDetail['0']['QTY'];
				$orderid		=	$getProdessOrderDetail['OpenOrder']['num_order_id'];
				$suborderid		=	$getProdessOrderDetail['MergeUpdate']['product_order_id_identify'];				
				$processdate	=	$getProdessOrderDetail['OpenOrder']['process_date'];
				
				$refnum			=	$getProdessOrderDetail['OpenOrder']['amazon_order_id'];
				
				$subsource		=	$getProdessOrderDetail['OpenOrder']['sub_source'];
				$fullname		=	$customerInfo->Address->FullName;
				$country		=	$customerInfo->Address->Country;
				$postcode		=	$customerInfo->Address->PostCode;
				$sku 			=	$getProdessOrderDetail['MergeUpdate']['sku'];
				$quantity		=	$quantity;
				$servicename	=	$getProdessOrderDetail['MergeUpdate']['service_name'];
				$servicecode	=	$getProdessOrderDetail['MergeUpdate']['provider_ref_code'];
				$envelopname	=	$getProdessOrderDetail['MergeUpdate']['envelope_name'];
				$envelopcost	=	$getProdessOrderDetail['MergeUpdate']['envelope_cost'];
				$packit_weight	=	$getProdessOrderDetail['MergeUpdate']['packet_weight'];
				$price			=	$getProdessOrderDetail['MergeUpdate']['price'];
				
 				$content .= $orderid."\t".$suborderid."\t".$processdate."\t".$refnum."\t".$subsource."\t".$fullname."\t".$country."\t".$postcode."\t".$sku."\t".$quantity."\t".$servicename."\t".$servicecode."\t".$envelopname."\t".$envelopcost."\t".$packit_weight."\t".$totals_info->TotalCharge."\t".$totals_info->Currency."\r\n";
				
				if($inc % 1000 == 0)
				{
					file_put_contents($path.$filename, $content, FILE_APPEND | LOCK_EX);
					$content = '';					
					$inc  ;
				}
			 
		}	
		file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);
		
		
		echo Router::url('/', true).'Uploads/downloadProcessedOrder';	 
	 
		exit;
	}
	
  	public function downloadProcessedOrder()
	{
	
	 	$path = WWW_ROOT .'img/upload/';
		$filename = "ProcessedOrder.txt";
		header('Content-Description: File Transfer');
		header('Content-Type: text/plain');
		header('Content-Disposition: attachment; filename='.basename($filename));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($path.$filename));
		ob_clean();
		flush();
		readfile($path.$filename); 
		
	}
	
	 
	public function getOrdersDownload()
	{
		$this->layout = 'index';
 	}

	public function getOpenOrder()
	{
		
		$this->layout = '';
		$this->autoRender = false;
		
		$getData = $this->request->data;		
		
		$getFrom 	= date('Y-m-d',strtotime($getData['getFrom']));
		$getEnd 	= date('Y-m-d',strtotime($getData['getEnd']. "+1 days"));
		$getStoreSelection = $getData['getStoreSelection'];
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		
		if( isset($getStoreSelection) && $getStoreSelection != '' )
		{
			$param = array(
			
				'conditions' => array( 
					'OpenOrder.status' => 0, 
					//'OpenOrder.linn_fetch_orders !=' => 0,				
					//'OpenOrder.open_order_date >=' => $getFrom,
					//'OpenOrder.open_order_date <=' => $getEnd,
					'OpenOrder.sub_source' => $getStoreSelection
				),
				'fields' => array( 
					'OpenOrder.sub_source','OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date'
				)
			
			);
		}
		else
		{
			
			$param = array(
			
				'conditions' => array( 
					'OpenOrder.status' => 0, 
					//'OpenOrder.linn_fetch_orders !=' => 0,				
					//'OpenOrder.open_order_date >=' => $getFrom,
					//'OpenOrder.open_order_date <=' => $getEnd
				),
				'fields' => array( 
					'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.open_order_date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date'
				)
			
			);
			
		}
		
		$getProdessOrderDetails	=	$this->OpenOrder->find('all', $param );		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderId');   
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Split OrderId');  
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ReceivedDate');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'ReferenceNum');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Source');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'SubSource');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'FullName');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Country');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'SKU');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Quantity');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Service Provider');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Servece code');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', 'OpenOrderDate');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Move Date');
		
		$inc = 2;
		foreach($getProdessOrderDetails as $getProdessOrderDetail )
		{

			$generalInfo	= unserialize($getProdessOrderDetail['OpenOrder']['general_info']);
			$customerInfo	= unserialize($getProdessOrderDetail['OpenOrder']['customer_info']);
			$orderReciveDate	=	$generalInfo->ReceivedDate;

			$numOrderId		=	 $getProdessOrderDetail['OpenOrder']['num_order_id'];
			$orderAfterSpliting	=	 $this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' =>  $numOrderId ) ) );
			if( !empty( $orderAfterSpliting ) )
			{
					foreach( $orderAfterSpliting as $orderAfterSplitingIndex => $orderAfterSplitingValuer )
					{
					
						if(strpos($generalInfo->Source, 'EBAY')=== false){ $ReferenceNum = $generalInfo->ReferenceNum; }
						else{ $ReferenceNum = $generalInfo->SecondaryReference; }
					
					
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['OpenOrder']['num_order_id'] );
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $orderAfterSplitingValuer['MergeUpdate']['product_order_id_identify'] );
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $orderReciveDate );
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $ReferenceNum );
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->Source );
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $generalInfo->SubSource );
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->FullName );
						$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $customerInfo->Address->Country );
						$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $orderAfterSplitingValuer['MergeUpdate']['sku'] );
						$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, $orderAfterSplitingValuer['MergeUpdate']['quantity'] );
						$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, $orderAfterSplitingValuer['MergeUpdate']['service_provider'] );
						$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, $orderAfterSplitingValuer['MergeUpdate']['provider_ref_code'] );
						$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, $getProdessOrderDetail['OpenOrder']['open_order_date']);
						$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, $orderAfterSplitingValuer['MergeUpdate']['order_date'] );
						$inc++;
					}
			}
			else
			{
						if(strpos($generalInfo->Source, 'EBAY')=== false){ $ReferenceNum = $generalInfo->ReferenceNum; }
						else{ $ReferenceNum = $generalInfo->SecondaryReference; }
					 
					 	$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['OpenOrder']['num_order_id'] );
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, '' );
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getProdessOrderDetail['OpenOrder']['open_order_date'] );
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $ReferenceNum );
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->Source );
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $generalInfo->SubSource );
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->FullName );
						$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $customerInfo->Address->Country );
						$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, '' );
						$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, '' );
						$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, '' );
						$inc++;
			}

		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/upload/OpenOrder.csv';
		$uploadRemote = $getBase.'img/upload/OpenOrder.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'OpenOrder.csv';
		
		//$contenttype = "application/force-download";
		//header("Content-Type: " . $contenttype);
		//header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		//readfile($uploadRemote);
		echo $uploadRemote;
		exit;

	}
	
	public function getUnprepareOrder()
	{


$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'UnprepareOrder' );
		$getProdessOrderDetails	=	$this->UnprepareOrder->find('all', array( 'fields' => array( 'UnprepareOrder.status', 'UnprepareOrder.num_order_id','UnprepareOrder.date','UnprepareOrder.general_info','UnprepareOrder.customer_info') ));
	
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderId');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'ReceivedDate');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ReferenceNum');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Source');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'SubSource');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'FullName');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Country');
		$inc = 2;
		foreach($getProdessOrderDetails as $getProdessOrderDetail )
		{

			$generalInfo	= unserialize($getProdessOrderDetail['UnprepareOrder']['general_info']);
			$customerInfo	= unserialize($getProdessOrderDetail['UnprepareOrder']['customer_info']);


			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['UnprepareOrder']['num_order_id'] );
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $generalInfo->ReceivedDate );
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $generalInfo->ReferenceNum );
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $generalInfo->Source );
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->SubSource );
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $customerInfo->Address->FullName );
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->Country );
			$inc++;
		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/upload/UnprepareOrder.csv';
		$uploadRemote = $getBase.'img/upload/UnprepareOrder.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'UnprepareOrder.csv';
		
		//$contenttype = "application/force-download";
		//header("Content-Type: " . $contenttype);
		//header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		//readfile($uploadRemote);
		echo $uploadRemote;
		exit;


	}

	public function getCancelOrder()
	{

		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		$getData = $this->request->data;		
		
		$getFrom = date('Y-m-d',strtotime($getData['getFrom']));
		$getEnd = date('Y-m-d',strtotime($getData['getEnd']. "+1 days"));
		$getStoreSelection = $getData['getStoreSelection'];
		
		if( isset($getStoreSelection) && $getStoreSelection != '' )
		{
			$param = array(
			
				'conditions' => array( 
					'OpenOrder.status' => 2,
					'OpenOrder.date >=' => $getFrom,
					'OpenOrder.date <=' => $getEnd,
					'OpenOrder.sub_source' => $getStoreSelection
				),
				'fields' => array( 
					'OpenOrder.sub_source','OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date'
				)
			
			);
		}
		else
		{
			
			$param = array(
			
				'conditions' => array( 
					'OpenOrder.status' => 2,
					'OpenOrder.date >=' => $getFrom,
					'OpenOrder.date <=' => $getEnd
				),
				'fields' => array( 
					'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date'
				)
			
			);
			
		}
		
		$getProdessOrderDetails	=	$this->OpenOrder->find('all', $param );
		
		//$getProdessOrderDetails	=	$this->OpenOrder->find('all', array( 'conditions' => array( 'OpenOrder.status' => 2), 'fields' => array( 'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info') ));
	
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderId');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'ReceivedDate');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ReferenceNum');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Source');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'SubSource');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'FullName');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Country');
		$inc = 2;
		foreach($getProdessOrderDetails as $getProdessOrderDetail )
		{

			$generalInfo	= unserialize($getProdessOrderDetail['OpenOrder']['general_info']);
			$customerInfo	= unserialize($getProdessOrderDetail['OpenOrder']['customer_info']);

			$numOrderId		=	 $getProdessOrderDetail['OpenOrder']['num_order_id'];
		
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['OpenOrder']['num_order_id'] );
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $getProdessOrderDetail['OpenOrder']['date'] );
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $generalInfo->ReferenceNum );
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $generalInfo->Source );
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->SubSource );
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $customerInfo->Address->FullName );
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->Country );
			$inc++;
			
		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/upload/CancelOrder.csv';
		$uploadRemote = $getBase.'img/upload/CancelOrder.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'CancelOrder.csv';
		
		//$contenttype = "application/force-download";
		//header("Content-Type: " . $contenttype);
		//header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		//readfile($uploadRemote);
		
		echo $uploadRemote;
		exit;

	}
	
	public function serviceCounterBackUp()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'ServiceCounter' );
		$userId	=	$this->request->data['userId'];
		//$servicePost = $this->request->data['servicePost'];
		$serializeData = $this->ServiceCounter->find( 'all' );
		if( count($serializeData) > 0 )
		{
			$manifest = serialize( $serializeData );		
			$this->loadModel('ManifestBackup');
			$data['ManifestBackup']['manifest_bk'] = $manifest;
			$data['ManifestBackup']['service_provider'] = 'All';
			$data['ManifestBackup']['date'] = date('Y-m-d');
			$data['ManifestBackup']['user_id'] = $userId;
			$this->ManifestBackup->saveAll( $data );
		}
		
	}
	
	public function getStores()
	{
		
		$this->loadModel( 'Store' );		
		$this->layout = '';
		$this->autoRender = false;
		
		$sendStores = json_decode(json_encode($this->Store->find('list' , array( 'conditions' => array( 'Store.status' => 1 ) , 'fields' => array( 'Store.store_name', 'Store.store_name' ) ))),0);		
	return $sendStores;
	}
	
	public function updateBarcode()
		{
				$this->autoLayout = 'ajax';
				$this->autoRender = false;
				$this->loadModel('Product');
				$this->loadModel('ProductDesc');
				
				$name = 'GenerateNewBarcode.csv';
				App::import('Vendor', 'PHPExcel/IOFactory');
				
				$objPHPExcel = new PHPExcel();
				$objReader= PHPExcel_IOFactory::createReader('CSV');
				$objReader->setReadDataOnly(true);				
				$objPHPExcel=$objReader->load('img/upload/GenerateNewBarcode.csv');
				$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
				$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
				$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
				$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
				$countInserted = 0;
				$countNotInserted = 0;
				$shiftValue = 8;
				$moveValue = 2;
				$count = 0;
				for($i=2;$i<=$lastRow;$i++) 
				{
					$sku		=	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
					$productDetail = $this->Product->find('first', array( 'conditions' => array( 'Product.product_sku' =>  $sku ) ) );
					
					$barcode = $productDetail['ProductDesc']['id'];
					$lengthOfId = strlen($barcode);					
					$mainDiffer = $lengthOfId - $moveValue; // ? - 2 = ?
					$differ = $shiftValue - $mainDiffer; // 8 - ? = ?
					$newSpaceVake = '';
					for( $sk = 0; $sk < $differ; $sk++ ) 
					{
						if( $sk == 0 )
						{
							$newSpaceVake .= '0';
						}
						else
						{
							$newSpaceVake = $newSpaceVake.'0';
						}
					}
					$newBarcode = rand(11,99) . $newSpaceVake . $barcode;
					$newSpaceVake = '';
					$data['ProductDesc']['id'] 				= 	$productDetail['ProductDesc']['id'];
					$data['ProductDesc']['barcode']			=	$newBarcode;
					$this->ProductDesc->saveAll( $data, array('validate' => false) ); 
					$count++;
				}
				echo $count;
			exit;
			
		
		}
		
    public function getSellReport()
    {
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'ScanOrder' );

		$getData = $this->request->data;
		
		$getFrom = date('Y-m-d',strtotime($getData['getFrom']));
		$getEnd = date('Y-m-d',strtotime($getData['getEnd']));
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'SKU');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Barcode');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Quantity');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Date');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Country');
		
		$param = array(
			'conditions' => array( 
					'ScanOrder.scan_date >=' => $getFrom,
					'ScanOrder.scan_date <=' => $getEnd
				),
				'fields' => array( 'sum(ScanOrder.quantity)   AS Quantity' , 'ScanOrder.id, ScanOrder.split_order_id, ScanOrder.sku, ScanOrder.barcode', 'ScanOrder.scan_date'),
				'group' => array('ScanOrder.sku, ScanOrder.scan_date'),
				'order'=> array( 'ScanOrder.scan_date ASC' )
			);
		
		$soldSkus = $this->ScanOrder->find( 'all', $param );
		
		$inc = 2;
		foreach( $soldSkus as $soldSku )
		{
		
			$order	=	$this->OpenOrder->find('first', array( 'conditions'=> array( 'OpenOrder.num_order_id' => explode("-",$soldSku['ScanOrder']['split_order_id'])[0]) ) );
			$sub_source = ''; $country = '';
			if(count($order)> 0){
				$sub_source = $order['OpenOrder']['sub_source'];
				$customer_info = unserialize($order['OpenOrder']['customer_info']);
				$country = $customer_info->Address->Country;
			}

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $soldSku['ScanOrder']['sku']);     
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $soldSku['ScanOrder']['barcode']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $soldSku[0]['Quantity']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $soldSku['ScanOrder']['scan_date']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $country);
			$inc++;
		}
		
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		$uploadUrl = WWW_ROOT .'img/daily_report/CustomSellReport.csv';
		$uploadRemote = $getBase.'img/daily_report/CustomSellReport.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'CustomSellReport.csv';
		
		echo $uploadRemote;
		exit;
	}
	
	//sku detailed report section
	public function getSku()
	{
		$this->layout = 'index';
	}
	
	public function processForDetailedSku()
	{
		
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'CheckIn' );
		$this->loadModel( 'InventoryRecord' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		
		//sheet generation
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel'); 
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderID'); 
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Action');   
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Sku');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Barcode');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Current Stock');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Quantity');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'After Manipulation');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Status');  
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Date');
		
		$inputSku 	= $this->request->data['inputSku'];
		$getFrom 	= date('Y-m-d' ,strtotime($this->request->data['getFrom']));
		$getEnd 	= date('Y-m-d' ,strtotime($this->request->data['getEnd'] . "+1 days"));
		
		$paramHostory = array(
		
			'conditions' => array(
			
				'InventoryRecord.sku' => $inputSku,
				'InventoryRecord.date >=' => $getFrom,
				'InventoryRecord.date <=' => $getEnd,
			
			)
		
		);
		
		//Processing now
		$getSkuDetailedReport = json_decode(json_encode($this->InventoryRecord->find( 'all', $paramHostory )),0);
		
		if( count( $getSkuDetailedReport ) > 0 )		
		{
			
			//sheet setup
			$inc = 2;
			foreach( $getSkuDetailedReport as $inventoryRow )
			{
				if( $inventoryRow->InventoryRecord->action_type == '' )
				{
					$inventoryRow->InventoryRecord->action_type = 'N/A / Old Data';
				}
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $inventoryRow->InventoryRecord->split_order_id);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $inventoryRow->InventoryRecord->action_type);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $inventoryRow->InventoryRecord->sku);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $inventoryRow->InventoryRecord->barcode);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $inventoryRow->InventoryRecord->currentStock);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $inventoryRow->InventoryRecord->quantity);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $inventoryRow->InventoryRecord->after_maniplation);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $inventoryRow->InventoryRecord->status);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $inventoryRow->InventoryRecord->date);
				$inc++;
			}
			
			//report setup
			$objPHPExcel->getActiveSheet(0);
			$getBase = Router::url('/', true);
			$uploadUrl = WWW_ROOT .'img/daily_report/sku_dtailed_report_by_date.csv';
			$uploadRemote = $getBase.'img/daily_report/sku_dtailed_report_by_date.csv';
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
			$objWriter->save($uploadUrl);
			$file = 'sku_dtailed_report_by_date.csv';
			
			echo $uploadRemote;
			exit;
			
		}
		else
		{
			echo "0";
			exit;
		}
		
		
		
	}
	
	public function updateCategory()
		{
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('Product');
			$this->loadModel('ProductDesc');
			$this->loadModel( 'Category' );
		
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/Category_update.csv');
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			$notFound = 0;
			$count = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$sku	=	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$category = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				
				$getProductDetail	=	$this->Product->find('first', array( 'conditions'=> array( 'Product.product_sku' =>  $sku) ) );
				
				$getCategory = $this->Category->find( 'first' , array( 'conditions' => array( 'Category.category_name' => $category ) ) );
				
				if( !empty( $getProductDetail ))
				{
					if( count( $getCategory ) == 0 )
					{
						$categoryName = $category;
						$catAlias = strtolower($categoryName);					
						$this->Category->query( "insert into categories ( category_name, category_alias ) values ( '{$categoryName}' , '{$catAlias}' )" );					
						$catId = $this->Category->getLastInsertId();
						$productdata['id'] 				= $getProductDetail['Product']['id'];
						$productdata['category_id'] 	= $catId;
						$productdata['category_name'] 	= $categoryName;
					}
					else
					{
						$categoryName 					= $getCategory['Category']['category_name'];
						$productdata['Product']['id'] 				= $getProductDetail['Product']['id'];
						$productdata['Product']['category_id'] 		= $getCategory['Category']['id'];
						$productdata['Product']['category_name'] 	= $categoryName;
					}
					$this->Product->saveAll($productdata, array('validate' => false));
					$count++;
				}
				else
				{
					$notFound++;
				}
			}
			echo "Inserted >> ".$count."<br> NotFound >> ".$notFound; 
		}
		
		
		public function uploadRegisteredTrackingNumber()
		{
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('RegisteredNumber');
			
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/RegisteredNumbetPostnl/POSTNL_Barcodes_011217.csv');
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			$j = 0; $k = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$regData['reg_number']			=	 	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$regData['reg_baecode_number'] 	= 		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$regData['date']				= 		date('Y-m-d H:i:s');
				$chechTrackedID = $this->RegisteredNumber->find( 'first' , array( 'conditions' => array( 'RegisteredNumber.reg_number' => $regData['reg_baecode_number'] ) ) );
				if(count($chechTrackedID) == 0)
				{	
					$j++;
					$this->RegisteredNumber->saveAll( $regData );
				} else { 
					$k++;
				}
			}	
			echo $j."Tracking Id Inserted.".$k." Id Already Inserted";
		}
		
		public function uploadSubCategory()
		{
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('Product');
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/cat_subcat/SubCategory.csv');
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			$j = 0; $k = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$sku							=	 	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$subCategory 					= 		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$checkProduct = $this->Product->find( 'first' , array( 'conditions' => array( 'Product.product_sku' => $sku ) ) );
				
				if(count($checkProduct) == 4)
				{	
					$this->Product->updateAll(array('Product.sub_category' => "'".$subCategory."'" ), array('Product.product_sku' => $sku));
					$j++;
				} else { 
					$k++;
				}
			}
			echo $j.'Updated'.$k.'Not found';	
		}
		
		
		public function updatepoprice()
		{
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('PurchaseOrder');
			
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/PurchaseOrderPriceUpdate/Other PO 2.csv');
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			$j = 0; $k = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$po_name			=	 	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$purchase_sku 	    = 		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$price 	    		= 		$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
				//echo $po_name.'>>>>>'.$purchase_sku.'>>>>>'.$price."<br>";
				$chechposku = $this->PurchaseOrder->find( 'first' , array( 'conditions' => array('PurchaseOrder.po_name'=>$po_name,'PurchaseOrder.purchase_sku'=>$purchase_sku)));
				if(count($chechposku) == 1)
				{	
					$j++;
					$this->PurchaseOrder->updateAll( array('PurchaseOrder.price'=>$price), array('PurchaseOrder.po_name'=>$po_name,'PurchaseOrder.purchase_sku'=>$purchase_sku) );
				} else { 
					$k++;
				}
			}	
			echo $j."Po Sku Price Updated.".$k."Po Sku Price inserted.";
		}
		
		public function assignUserBulk()
		{
			$this->layout = "index";
		}
		
		public function UploadsCsvFilesForAssignUser()
		{
		
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('Product');
			$this->loadModel('ProductDesc');
			
			$this->data['Product']['Import_file']['name'];
			if($this->data['Product']['Import_file']['name'] != '')
				{
					$filename = WWW_ROOT. 'files'.DS.'assign_user_file'.DS.'uploaded'.DS.$this->request->data['Product']['Import_file']['name']; 
					move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
					$name		=	 $this->request->data['Product']['Import_file']['name'];
					App::import('Vendor', 'PHPExcel/IOFactory');
			
						$objPHPExcel = new PHPExcel();
						$objReader= PHPExcel_IOFactory::createReader('CSV');
						$objReader->setReadDataOnly(true);				
						$objPHPExcel=$objReader->load('files/assign_user_file/uploaded/'.$name);
						$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
						$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
						$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
						$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
						$count = 0; $k = 0;
						for($i=2;$i<=$lastRow;$i++) 
						{	
							$sku							=	 	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
							$listing_user 					= 		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
							$purchase_user 					= 		$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
							echo $sku.'>>>>>>>>'.$listionUser.'>>>>>>>>'.$purchaseUser;
							echo "<br>";
							$checkProduct 	= $this->Product->find( 'first' , array( 'conditions' => array( 'Product.product_sku' => $sku ) ) );
							echo count($checkProduct);
							echo "<br>";
							$barcode 		= $checkProduct['ProductDesc']['barcode'];
							if(count($checkProduct) == 4){	
								$this->ProductDesc->updateAll(array('ProductDesc.user_id'=>$listing_user,'ProductDesc.purchase_user_id'=>$purchase_user),array('Product.product_sku' => $sku));
								$count++;
							} else { 
								$k++;
							}
							
						}
				}
				
			$this->Session->setFlash($count.' :- SKU Updated.'.$k.' :- SKU Not Update.', 'flash_danger');		
			$this->redirect($this->referer());
		}
		
	public function updateBulkTitle()
	{
		$this->layout = "index";
	}
	
	public function UploadsCsvFilesForUpdateTitle()
	{
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('Product');
			$this->loadModel('ProductDesc');
			
			$firstName 	= ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName 	= ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$u_name		= strtolower($firstName).'_'.strtolower($lastName).'_'.date('Y-m-d_H:i:s');
			
			$this->data['Product']['Import_file']['name'];
			if($this->data['Product']['Import_file']['name'] != '')
				{
					$filename = WWW_ROOT. 'files'.DS.'update_title'.DS.'uploaded'.DS.$u_name.'_'.$this->request->data['Product']['Import_file']['name']; 
					move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
					$name		=	 $this->request->data['Product']['Import_file']['name'];
					App::import('Vendor', 'PHPExcel/IOFactory');
			
						$objPHPExcel = new PHPExcel();
						$objReader= PHPExcel_IOFactory::createReader('CSV');
						$objReader->setReadDataOnly(true);				
						$objPHPExcel=$objReader->load('files/update_title/uploaded/'.$u_name.'_'.$name);
						$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
						$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
						$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
						$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
						$count = 0; $k = 0;
						for($i=2;$i<=$lastRow;$i++) 
						{	
							$sku							=	 	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
							$title 							= 		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
							$checkProduct 	= $this->Product->find( 'first' , array( 'conditions' => array( 'Product.product_sku' => $sku ) ) );
							$barcode 		= $checkProduct['ProductDesc']['barcode'];
							if(count($checkProduct) == 4){	
								$this->ProductDesc->updateAll(array('Product.product_name'=>$title,'ProductDesc.short_description'=>$title,'ProductDesc.long_description'=>$title),array('Product.product_sku' => $sku));
								$count++;
							} else { 
								$k++;
							}
							
						}
				}
			$this->Session->setFlash($count.' :- SKU Updated.'.$k.' :- SKU Not Update.', 'flash_danger');		
			$this->redirect($this->referer());
		
	}
	
}

?>
