<?php
error_reporting(1);
ini_set( 'memory_limit' , '2048M' );
App::uses('Folder', 'Utility');
App::uses('File', 'Utility'); 

class PicklistController extends AppController
{
    
    var $name = "Picklist";
    var $components = array('Session','Upload','Common','Auth');
    var $helpers = array('Html','Form','Common','Session');
    
    public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('overSellSync'));
			$this->GlobalBarcode = $this->Components->load('Common');
    }
	public function index(){ 
	
		//$this->redirect();	
		$this->redirect(array('action' => 'orderSkus'));
	}
	public function orderSkus($page = NULL)
    { 
		
		
		$this->layout = "index";
		$this->loadModel('CustomPicklistSku');
		$this->loadModel( 'OrderLocation' );		
		$this->loadModel( 'MergeUpdate' );	
		$this->loadModel( 'UnprepareOrder' );	
		$this->loadModel( 'Product' );
		$this->loadModel( 'MergeUpdate' );
		$perPage	=	 Configure::read( 'perpage' );
		/*$this->paginate = array(
                        'fields' => array('product_sku','product_name'			
						),
			'limit' => $perPage,
			'order'=>'Product.product_sku DESC'			
			);
			 
			$products = $this->paginate('Product');
			
		$_product	= $this->Product->find('all',array('fields'=>array('product_sku','product_name'),'order'=>'Product.id DESC'));
		
		foreach( $_product as $pro ){
			$products[$pro['Product']['product_sku']] = $pro['Product']['product_name'];
		}*/
		$selected_skus = array();
		$sel_skus = $this->CustomPicklistSku->find('all');
		if(count($sel_skus) > 0){
			foreach( $sel_skus as $pro ){
				$selected_skus[$pro['CustomPicklistSku']['sku']] = $pro['CustomPicklistSku']['sku'];
			}
		}
		
		$all_order = array();
		$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id'),'conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.status' => '0')));	
		
		foreach($orderItems as $item_order ){
			$all_order[$item_order['MergeUpdate']['order_id']] = $item_order['MergeUpdate']['order_id'];			
		}
		if(count($all_order) > 1){
			$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id IN' => $all_order),'order' => 'bin_location ASC');				
		}else{
			$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id ' => $all_order[0]),'order' => 'bin_location ASC');	
		}		
		
						
		$pickListItems 	=	$this->OrderLocation->find('all', $param );		
	
		//$this->paginate = $param;
			 
		//$pickListItems = $this->paginate('OrderLocation');
				
		if( count( $pickListItems ) > 0 )
		{
			foreach( $pickListItems as $val )
			{
				 $vdata[$val['OrderLocation']['sku']][] = array('bin_location' => $val['OrderLocation']['bin_location'],
																	'order_id'=>trim($val['OrderLocation']['order_id']),
																	'barcode' => $val['OrderLocation']['barcode'],
																	'quantity' => $val['OrderLocation']['quantity'],
																	'available_qty_bin' => $val['OrderLocation']['available_qty_bin'],
																	'move_date' => $val['OrderLocation']['timestamp']);
				 
			}
		}
		 foreach($vdata as $sku => $val){
			 $order_count = 0;
			 foreach($val as $d){
				 $order_count++;
			 
			 
		$data[$sku][] = array( 
								'bin_location' => $d['bin_location'],
								'order_id'=>$d['order_id'] ,
								'barcode' => $d['barcode'],
								'quantity' => $d['quantity'],
								'available_qty_bin' => $d['available_qty_bin'],
								'move_date' => $d['move_date'],
								'order_count' => count($val));
																	  
																	
		} 
			$data[$sku]['order_count'] =   count($val);
			$data[$sku]['sku'] =  $sku;
			
		} 
		$imgPath = WWW_ROOT .'img/printPickList/'; 
		$dir = new Folder($imgPath, true, 0755);
		$files = $dir->find('.*\.pdf');
		$files = Set::sort($files, '{n}', 'DESC');
		 
		$bin_location = $this->bin_locations();
		  
		$this->set( compact('data','selected_skus','page','bin_location','files' )); //pr( $data);
	}
	
	public function selectSku()
    {
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('CustomPicklistSku');
		
		if($_POST['action'] == 'Selected'){
	 		$this->CustomPicklistSku->query("DELETE FROM `custom_picklist_skus` WHERE `sku` = '".$_POST['sku']."'");
			$msg['msg'] = 'deleted';
		}else{
			$this->CustomPicklistSku->query("DELETE FROM `custom_picklist_skus` WHERE `sku` = '".$_POST['sku']."'");
			$this->CustomPicklistSku->saveAll( $_POST );
			$msg['msg'] = 'inserted';
		}
		echo json_encode($msg);
		exit;
	}
	public function flushskus()
    {
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('CustomPicklistSku');	
		$this->CustomPicklistSku->query("TRUNCATE custom_picklist_skus");
		$this->Session->setflash( 'Old selected skus are flushed.', 'flash_success' ); 
		$this->redirect($this->referer());		 
	}	 
	public function search()
    {
		 
		$this->layout = "index";
		$this->loadModel('CustomPicklistSku');
		$this->loadModel( 'OrderLocation' );		
		$this->loadModel( 'MergeUpdate' );			
		$perPage	=	 Configure::read( 'perpage' );
		$data = array();
		$keyword = '';
		if(isset($this->request->query['keyword']) && $this->request->query['keyword']!=''){
			$keyword = $this->request->query['keyword'];
		}
		$selected_skus = array();
		$sel_skus = $this->CustomPicklistSku->find('all');
		if(count($sel_skus) > 0){
			foreach( $sel_skus as $pro ){
				$selected_skus[$pro['CustomPicklistSku']['sku']] = $pro['CustomPicklistSku']['sku'];
			}
		}
		
		$all_order = array();
		$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id'),'conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.status' => '0')));	
		
		foreach($orderItems as $item_order ){
			$all_order[$item_order['MergeUpdate']['order_id']] = $item_order['MergeUpdate']['order_id'];			
		}
		 
		
		if(isset($this->request->query['datefilter']) && $this->request->query['datefilter']!=''){
		
		 $date_range = $this->request->query['datefilter'];
		 $d = explode('-', $date_range);
		 $start = date("Y-m-d H:i:s",strtotime(trim($d[0])));		 
		 $end = date("Y-m-d H:i:s",strtotime(trim($d[1])." 23:59:59" ));
		 
			if(count($all_order) > 1){
				$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id IN' => $all_order,'timestamp BETWEEN ? AND ?' => array($start,$end)),'order' => 'bin_location ASC');	
				if($keyword){
					$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id IN' => $all_order,'timestamp BETWEEN ? AND ?' => array($start,$end), "OrderLocation.sku LIKE '%{$keyword}%'"),'order' => 'bin_location ASC');	
				}
							
			}else{
			
				$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id' => $all_order[0],'timestamp BETWEEN ? AND ?' => array($start,$end)),'order' => 'bin_location ASC');
				if($keyword){
					$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id' => $all_order[0],'timestamp BETWEEN ? AND ?' => array($start,$end), "OrderLocation.sku LIKE '%{$keyword}%'"),'order' => 'bin_location ASC');	
				}
					
			}	  
		 
		}else{
		
			if(count($all_order) > 1){
				$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id IN' => $all_order,"sku LIKE '%{$keyword}%'"),'order' => 'bin_location ASC');				
			}else{
				$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id ' => $all_order[0],"sku LIKE '%{$keyword}%'"),'order' => 'bin_location ASC');	
			}		
		}
				 		
		$pickListItems 	=	$this->OrderLocation->find('all', $param );		
	 
		$vdata ='';
		if(count($pickListItems) > 0){	 
			if( count( $pickListItems ) > 0 )
			{
				foreach( $pickListItems as $val )
				{
					 $vdata[$val['OrderLocation']['sku']][] = array('bin_location' => $val['OrderLocation']['bin_location'],
																		'order_id'=>trim($val['OrderLocation']['order_id']),
																		'barcode' => $val['OrderLocation']['barcode'],
																		'quantity' => $val['OrderLocation']['quantity'],
																		'available_qty_bin' => $val['OrderLocation']['available_qty_bin'],
																		'move_date' => $val['OrderLocation']['timestamp']);
					 
				}
			}
			 foreach($vdata as $sku => $val){
				 $order_count = 0;
				 foreach($val as $d){
					 $order_count++;
				 
				 
				$data[$sku][] = array( 
									'bin_location' => $d['bin_location'],
									'order_id'=>$d['order_id'] ,
									'barcode' => $d['barcode'],
									'quantity' => $d['quantity'],
									'available_qty_bin' => $d['available_qty_bin'],
									'move_date' => $d['move_date'],
									'order_count' => count($val));
																		  
																		
			} 
		 
			$data[$sku]['order_count'] =   count($val);
			$data[$sku]['sku'] =  $sku;			
			}
		}
		 
		$imgPath = WWW_ROOT .'img/printPickList/'; 
		$dir = new Folder($imgPath, true, 0755);
		$files = $dir->find('.*\.pdf');
		$files = Set::sort($files, '{n}', 'DESC');
		$bin_location = $this->bin_locations();
		$this->set( compact( 'data' , 'selected_skus', 'keyword', 'bin_location','files') ); //pr( $data);
	 
	}
	
	public function overSellSync(){
	
	
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('OrderLocation');	
		$this->loadModel('BinLocation');		
		$this->loadModel('CheckIn');
		$this->loadModel('Product');
		
		$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.available_qty_bin' => '0'),
		'order' => 'timestamp  DESC');				
		$OverSell 	=	$this->OrderLocation->find('all', $param );	
		 
		foreach($OverSell as $val){
		
			$po_name = array(); $posIds = array(); $pos = array(); $binname = array(); 
			$available_qty = 0; $available_qty_bin = 0;	$t = array();
		
			$quantity = $val['OrderLocation']['quantity'];					
			$sku      = $val['OrderLocation']['sku'];
			$barcode  = $val['OrderLocation']['barcode'];
			$order_id = $val['OrderLocation']['order_id'];
			$id 	  = $val['OrderLocation']['id'];
				
				
			$poDetail = $this->CheckIn->find( 'all', 
				array( 'conditions' => array( 'CheckIn.barcode' => $barcode, 'CheckIn.selling_qty != CheckIn.qty_checkIn','po_name !=""'),
						'order' => 'CheckIn.date  ASC ' ) );
			
			if(count($poDetail) > 0){
				$qt_count = 0; 				
				foreach($poDetail as $po){
					$poQty = $po['CheckIn']['qty_checkIn'] - $po['CheckIn']['selling_qty'];
					if($poQty > 0){						
						 for($i=0; $i <= $quantity ;$i++){						 
							 if($qt_count >= $quantity ){
								 break;
							 }else if(($poQty - $i)  > 0){							
								$po_name[$po['CheckIn']['id']][] = $po['CheckIn']['po_name'];	
								$available_qty++;							
								$qt_count++;
							}												
						 }						
					}
				}
			}
			 
			
			$getStock = $this->BinLocation->find( 'all', array( 'conditions' => array( 'BinLocation.barcode' => $barcode),'order' => 'priority ASC' ) );
			$bin_name = array(); $finalData['bin_location'] = "";
			if(count($getStock) > 0){
			
				$qt_count = 0 ;
				foreach($getStock as $val){
					 
					$binData['id'] = $val['BinLocation']['id'];					
					 for($i=0; $i <= $quantity ;$i++){						 
						 if($qt_count >= $quantity ){
							 break;
						 }else if(($val['BinLocation']['stock_by_location'] - $i)  > 0){							
							$bin_name[$val['BinLocation']['id']][] = $val['BinLocation']['bin_location'];	
							$available_qty_bin++;						
							$qt_count++;
						}												
					 }	
									 
				}					
			}
			
			
			if(count($po_name) > 0){
				foreach(array_keys($po_name) as $k){
					$qts   = count($po_name[$k]);	
					$pos[] = $po_name[$k][0];
					$posIds[] = $k;
					$this->OrderLocation->updateAll( array('OrderLocation.available_qty_check_in' => $qts),array('id' => $id));		
					$this->CheckIn->updateAll( array('CheckIn.selling_qty' =>  "CheckIn.selling_qty + $qts"),array('CheckIn.id' => $k));
					file_put_contents( WWW_ROOT .'logs/oversellsync.log', date('d-m-y H:i:s')."\tCheckIn\t". $k."\t".$qts."\n", FILE_APPEND | LOCK_EX);	
				}
			}
				
			if(count($bin_name) > 0)
			{
				foreach(array_keys($bin_name) as $k){					
					$qts = count($bin_name[$k]);	
					$binname[] = $bin_name[$k][0];	
					
					$this->OrderLocation->updateAll( array('OrderLocation.bin_location' => "'".$bin_name[$k][0]."'",
														'OrderLocation.available_qty_bin' => "'".$qts."'",
														'OrderLocation.po_name' => "'".implode(',',$pos)."'",
														'OrderLocation.po_id' => "'".implode(',',$posIds)."'"),
													array('id' => $id)
													);
					$this->Product->updateAll( array('current_stock_level' =>  "current_stock_level - $qts"),array('product_sku' => $sku));
					
					$this->BinLocation->updateAll( array('BinLocation.stock_by_location' =>  "BinLocation.stock_by_location - $qts"),array('BinLocation.id' => $k));
					
					 
					file_put_contents( WWW_ROOT .'logs/oversellsync.log', date('d-m-y H:i:s')."\tBinLocation\t". $sku."\t".$qts."\n", FILE_APPEND | LOCK_EX);	
				}
			}						 
		}
		
		$this->Session->setflash( 'Oversell sync process is completed!', 'flash_success'); 
		$this->redirect($this->referer());
		exit;
	}
	
	public function generate_picklist($option = NULL)
    { 		
		$this->layout = '';
		$this->autoRander = false;		
		
		/* start for pick list */
		
		$this->loadModel( 'OrderLocation' );		
		$this->loadModel( 'MergeUpdate' );	
		$this->loadModel( 'UnprepareOrder' );	
		$this->loadModel( 'Product' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'CustomPicklistSku' );
 		
		$all_order = array();
		$param = array();
		
		
		$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id'),'conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.status' => '0','MergeUpdate.service_provider !=' => 'DHL')));	
		
		
		foreach($orderItems as $item_order ){
			$all_order[] = $item_order['MergeUpdate']['order_id'];
		}
		 
		// pr( $date_range = $this->request->query['datefilter']);exit;
		if($option == 'daterange'){
			 if(isset($this->request->query['datefilter']) && $this->request->query['datefilter']!=''){
		
				$all_order = array();
				$date_range = $this->request->query['datefilter'];		
				$d = explode('-', $date_range);
				$start = date("Y-m-d H:i:s",strtotime(trim($d[0])));
				$end = date("Y-m-d H:i:s",strtotime(trim($d[1])." 23:59:59" ));// $end = date("Y-m-d H:i:s",strtotime(trim($d[1])));
				
				$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id'),'conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.status' => '0','order_date BETWEEN ? AND ?' => array($start,$end), 'MergeUpdate.delevery_country' => 'United Kingdom','MergeUpdate.postal_service' => 'Express')));	
				
				/*$orderItems = $this->OpenOrder->find('all', array('fields'=>array('order_id'),'conditions' => array('OpenOrder.status' => '0', 'open_order_date BETWEEN ? AND ?' => array($start,$end))));	
				
				 pr($orderItems );*/
				foreach($orderItems as $item_order ){
					$all_order[] = $item_order['MergeUpdate']['order_id'];
				}
				if(count($all_order) > 0){ 
					if(count($all_order) > 1){
						$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id IN' => $all_order,'available_qty_bin >' => 0),'order' => 'bin_location ASC');				
					}else{
						$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id ' => $all_order[0],'available_qty_bin >' => 0),'order' => 'bin_location ASC');	
					}
				}else{
					$this->Session->setflash( 'There are no orders between this date range!', 'flash_danger' ); 
					$this->redirect($this->referer());	
				}	  
		 	}else{
				$this->Session->setflash( 'Please selected date range or choose other option!', 'flash_danger' ); 
				$this->redirect($this->referer());	
			 }
		}
		else if($option == 'selsku'){
		
			$all_order = array();
			$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id'),'conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.status' => '0')));	
			
			foreach($orderItems as $item_order ){
				$all_order[] = $item_order['MergeUpdate']['order_id'];
			}
			$sel_skus = $this->CustomPicklistSku->find('all');
			if(count($sel_skus) > 0){
				foreach( $sel_skus as $pro ){
					$selected_skus[$pro['CustomPicklistSku']['sku']] = $pro['CustomPicklistSku']['sku'];
				}
				if(count($all_order) > 1){
					if(count($selected_skus) > 1){
						$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id IN' => $all_order,'available_qty_bin >' => 0,'sku IN' =>$selected_skus),'order' => 'bin_location ASC');
					}else{
						$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id IN' => $all_order,'available_qty_bin >' => 0,'sku' =>$selected_skus),'order' => 'bin_location ASC');
					}				
				}else{
					if(count($selected_skus) > 1){
						$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id ' => $all_order[0],'available_qty_bin >' => 0, 'sku IN' =>$selected_skus),'order' => 'bin_location ASC');
					}else{
						$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id ' => $all_order[0],'available_qty_bin >' => 0, 'sku ' =>$selected_skus),'order' => 'bin_location ASC');
					}	
				}
			}else{				
				$this->Session->setflash( 'No selected sku found!', 'flash_danger' ); 
				$this->redirect($this->referer());	
			}
			
		}
		else if($option == 'express'){
		
				$all_order = array();
				$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id'),'conditions' => array('MergeUpdate.status' => '0','MergeUpdate.service_provider' => 'DHL')));	
				 //pr($orderItems);
				foreach($orderItems as $item_order ){
					$all_order[] = $item_order['MergeUpdate']['order_id'];
				}
				if(count($all_order)){
					if(count($all_order) > 1){
						$param = array('conditions' => array('OrderLocation.order_id IN' => $all_order,'available_qty_bin >' => 0),'order' => 'bin_location ASC');				
					}else{
						$param = array('conditions' => array('OrderLocation.order_id ' => $all_order[0],'available_qty_bin >' => 0),'order' => 'bin_location ASC');	
					}
				}else{				
					$this->Session->setflash( 'No DHL orders found!', 'flash_danger' ); 
					$this->redirect($this->referer());	
				}	  
		 	 
		}
		else if($option == 'ukexpress'){
		
				$all_order = array();
				$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id'),'conditions' => array('MergeUpdate.status' => '0','MergeUpdate.delevery_country' => 'United Kingdom','MergeUpdate.postal_service' => 'Express')));	
				 //pr($orderItems);
				foreach($orderItems as $item_order ){
					$all_order[] = $item_order['MergeUpdate']['order_id'];
				}
				if(count($all_order)){
					if(count($all_order) > 1){
						$param = array('conditions' => array('OrderLocation.order_id IN' => $all_order,'available_qty_bin >' => 0),'order' => 'bin_location ASC');				
					}else{
						$param = array('conditions' => array('OrderLocation.order_id ' => $all_order[0],'available_qty_bin >' => 0),'order' => 'bin_location ASC');	
					}
				}else{				
					$this->Session->setflash( 'No UK Express orders found!', 'flash_danger' ); 
					$this->redirect($this->referer());	
				}	  
		 	 
		}
		else{
			if(count($all_order) >= 1){
				$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id IN' => $all_order,'available_qty_bin >' => 0),'order' => 'bin_location ASC');				
			}else{
				$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id ' => $all_order[0],'available_qty_bin >' => 0),'order' => 'bin_location ASC');	
			}
		}
		/*pr($all_order); 
	 	pr($param); 
		exit;*/	 
		//$param = array('conditions' => array('OrderLocation.pick_list' => '0' ),'order' => 'bin_location ASC');				
		if(isset($param)){
			$pickListItems = $this->OrderLocation->find('all', $param );	
				
			if( count( $pickListItems ) > 0 )
			{
				$products = array();
				$_product	= $this->Product->find('all',array('fields'=>array('product_sku','product_name'),'order'=>'Product.id DESC'));
				foreach( $_product as $pro ){
					$products[$pro['Product']['product_sku']] = $pro['Product']['product_name'];
				}
 				//abort status				 
				//manage userd - id with name
				$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
				$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
				$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
				
				$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
				
				$orderids = array(); $data = array(); $tq = 0; $taq = 0;
				foreach( $pickListItems as $val )
				{
					$tq += $val['OrderLocation']['quantity'];	
					$taq += $val['OrderLocation']['available_qty_bin'];	
					$orderids[] = $val['OrderLocation']['order_id'];			
					$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																		'sku'=>trim($val['OrderLocation']['sku']),
																		'barcode' => $val['OrderLocation']['barcode'],
																		'quantity' => $val['OrderLocation']['quantity'],
																		'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
				}
							
				/* dome pdf vendor */
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				/* Html for print pick list */
				$date = date("m/d/Y H:i:s");	
				$finArray = array();
				$html = '<h3>PickList :- '.$date.'</h3><hr><h3>Total Item  : '.$tq.'</h3>
						<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
						<tr>
						<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
						<th style="border:1px solid;" width="10%" align="center">SKU</th>
						<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
						<th style="border:1px solid;" width="10%" align="center">Barcode</th>
						</tr>';
				
				$data = $this->msortnew($data, array('bin_location'));
				$tdata = array();
				foreach($data as $a){
					$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
											
				}			
				
				foreach($tdata as $sku => $d){
						$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
						foreach($d as $_ar){
							$bn[$_ar['bin_location']][] = $_ar['quantity'];	
							$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
							//$bar[$_ar['barcode']] = $_ar['barcode'];	
							$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
							
							if(count($local_barcode) > 0){
								$bar[$local_barcode[0]] = $local_barcode[0]; 
							}else{
								$bar[$_ar['barcode']] = $_ar['barcode'];
							}
						}
						
						$bins = array_keys($bn);  $oversell = array();
						foreach($bins as $b){
							if($b !=''){
								if(array_sum($bn[$b]) == array_sum($bna[$b])){
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								}else{
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
									$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
								}
							}
							$qty[]= array_sum($bn[$b]);
							$avaqty[]= array_sum($bna[$b]);
							
						}	
						$oversell_str = '';
						if(count($oversell) > 0){
							$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
						}
						$sku_title = '';		
						if(isset($products[$sku])){
							$sku_title = $products[$sku];
						}
											
						$html.=	'<tr>
							<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
							<td style="border:1px solid;">'.$sku.'</td>
							<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.substr($products[$sku], 0, 40 ).'</td>
							<td style="border:1px solid;">'.implode(",",$bar).'</td>							
							</tr>';
							
				}	
			
				$html .= '</table>';			 
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
 				  			
				date_default_timezone_set('Europe/Jersey');
				$today = date("d_m_Y_H_i");
				//$dompdf->stream("Pick_List_(".$date.").pdf");
				
				$imgPath = WWW_ROOT .'img/printPickList/'; 
				$path = Router::url('/', true).'img/printPickList/';
				$name	=	'Pick_List__'.$today.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				
				if(count($orderids) > 0){
					if(count($orderids) > 1){
						 $this->MergeUpdate->updateAll(array('MergeUpdate.pick_list_status' => '1','MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'" ), array('MergeUpdate.order_id IN ' => $orderids));
					 	$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.order_id IN ' => $orderids));
					}else{
					 	$this->MergeUpdate->updateAll(array('MergeUpdate.pick_list_status' => '1','MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.order_id = ' => $orderids[0]));
					 	$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.order_id = ' => $orderids[0]));
					}
				}
				// $this->customLogGlobal( 'Pick List [ ' . $username . '__' . date( 'Y-m-d j:i:s' ) . ' ]' , implode( "," , $orderids ) );
				
				
				$folderPath = WWW_ROOT .'logs';
				$dir = new Folder($folderPath, true, 0755);			
				$_Path = $folderPath .'/'. date('dmy_Hi').'_PicklistPC.log';    				  
				file_put_contents($_Path, $username."\n".implode( "," , $orderids ));
					  
 				//now abort file status down
				 
 				header('Content-Encoding: UTF-8');
				header('Content-type: text/csv; charset=UTF-8');
				header('Content-Disposition: attachment;filename="'.$name.'"');
				header("Content-Type: application/octet-stream;");
				header('Cache-Control: max-age=0');
				readfile($imgPath.$name);
				$this->CustomPicklistSku->query("TRUNCATE custom_picklist_skus");
				//exit;
					
			}else{
				$this->Session->setflash( 'No orders are available for pick list!', 'flash_danger'); 
				$this->redirect($this->referer());
			}
		}else{
			$this->Session->setflash( 'Please selected skus before to generate pick list or choose other option!', 'flash_danger' ); 
			$this->redirect($this->referer());	
		}
		 
		exit;
	
				
	}
	
	public function bin_locations()
    {
		//$this->layout = '';
		//$this->autoRender = false;
		$this->loadModel('BinLocation');
		$bin_locat = $this->BinLocation->find('all',array('GROUP BY'=>'bin_location','order'=>'bin_location ASC'));
		if(count($bin_locat) > 0){
			foreach( $bin_locat as $bin ){
				if($bin['BinLocation']['bin_location']){
					$bin_locations[$bin['BinLocation']['bin_location']] = $bin['BinLocation']['bin_location'];
				}
			}
		}	
		
		//$this->msortnew($bin_locations, array('bin_location'));
		
		return $bin_locations ;
		//$this->set( 'bin_locations' , $bin_locations );  
	}
	public function PicklistByLocation()
	{
		//pr($_REQUEST);
	
	 		
		$this->layout = '';
		$this->autoRander = false;		
		
		/* start for pick list */
		
		$this->loadModel( 'OrderLocation' );		
		$this->loadModel( 'MergeUpdate' );	
		$this->loadModel( 'UnprepareOrder' );	
		$this->loadModel( 'Product' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'CustomPicklistSku' );
		
		
		$all_order = array();
		$param = array();
		
		
		$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id'),'conditions' => array('MergeUpdate.pick_list_status' => '0','MergeUpdate.status' => '0')));	
		
		foreach($orderItems as $item_order ){
			$all_order[] = $item_order['MergeUpdate']['order_id'];
		}
		
		$loc =  explode(",",$_REQUEST['loc']);
		if( count($loc) == 1 ){
			$loc[1] = 'ttt';
		} 
		if(count($all_order) > 1){
			$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id IN' => $all_order,'available_qty_bin >' => 0 ,'bin_location IN ' => $loc ),'order' => 'bin_location ASC');				
		}else{
			$param = array('conditions' => array('OrderLocation.pick_list' => '0','OrderLocation.status' => 'active', 'OrderLocation.order_id ' => $all_order[0],'available_qty_bin >' => 0 , 'bin_location IN ' => $loc),'order' => 'bin_location ASC');	
		}
		  				
		//$param = array('conditions' => array('OrderLocation.pick_list' => '0' ),'order' => 'bin_location ASC');				
		if(isset($param) && count($param) ){
			$pickListItems 	=	$this->OrderLocation->find('all', $param );			
			if( count( $pickListItems ) > 0 )
			{ 			
				$products = array(); 		
				$_product	= $this->Product->find('all',array('fields'=>array('product_sku','product_name'),'order'=>'Product.id DESC'));
				foreach( $_product as $pro ){
					$products[$pro['Product']['product_sku']] = $pro['Product']['product_name'];
				}
				//abort status 				 
				//manage userd - id with name
				$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
				$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
				$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
				
				$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
				
				$orderids = array(); $data = array(); $tq = 0; $taq = 0;
				foreach( $pickListItems as $val )
				{
					$tq += $val['OrderLocation']['quantity'];	
					$taq += $val['OrderLocation']['available_qty_bin'];	
					$orderids[] = $val['OrderLocation']['order_id'];			
					$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																		'sku'=>trim($val['OrderLocation']['sku']),
																		'barcode' => $val['OrderLocation']['barcode'],
																		'quantity' => $val['OrderLocation']['quantity'],
																		'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
				}
							
				/* dome pdf vendor */
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				/* Html for print pick list */
				$date = date("m/d/Y H:i:s");	
				$finArray = array();
				$html = '<h3>PickList :- '.$date.'</h3><hr><h3>Total Item  : - '.$tq.'</h3>
						<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
						<tr>
						<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
						<th style="border:1px solid;" width="10%" align="center">SKU</th>
						<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
						<th style="border:1px solid;" width="10%" align="center">Barcode</th>
						</tr>';
				
				$data = $this->msortnew($data, array('bin_location'));
				$tdata = array();
				foreach($data as $a){
					$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
											
				}			
				
				foreach($tdata as $sku => $d){
						$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
						foreach($d as $a){
							$bn[$a['bin_location']][] = $a['quantity'];	
							$bna[$a['bin_location']][] = $a['available_qty_bin'];	
							$bar[$a['barcode']] = $a['barcode'];
							$glb_barcode = $this->GlobalBarcode->getAllBarcode($a['barcode']);
							$bar[$glb_barcode[0]] = $glb_barcode[0];	
						}
						$bins = array_keys($bn);  $oversell = array();
						foreach($bins as $b){
							if($b !=''){
								if(array_sum($bn[$b]) == array_sum($bna[$b])){
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								}else{
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
									$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
								}
							}
							$qty[]= array_sum($bn[$b]);
							$avaqty[]= array_sum($bna[$b]);
							
						}	
						$oversell_str = '';
						if(count($oversell) > 0){
							$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
						}					
						$html.=	'<tr>
							<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
							<td style="border:1px solid;">'.$sku.'</td>
							<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.substr($products[$sku], 0, 40 ).'</td>
							<td style="border:1px solid;">'.implode(",",$bar).'</td>							
							</tr>';
							
				}	
			
				 $html .= '</table>';			 
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				
  				date_default_timezone_set('Europe/Jersey');
				$today = date("d_m_Y_H_i");
				//$dompdf->stream("Pick_List_(".$date.").pdf");
				
				$imgPath = WWW_ROOT .'img/printPickList/'; 
				$path = Router::url('/', true).'img/printPickList/';
				$name	=	'Pick_List__'.$today.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				
				if(count($orderids) > 0){
					if(count($orderids) > 1){
						 $this->MergeUpdate->updateAll(array('MergeUpdate.pick_list_status' => '1','MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'" ), array('MergeUpdate.order_id IN ' => $orderids));
					 	$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.order_id IN ' => $orderids));
					}else{
					 	$this->MergeUpdate->updateAll(array('MergeUpdate.pick_list_status' => '1','MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.order_id = ' => $orderids[0]));
					 	$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.order_id = ' => $orderids[0]));
					}
				}
				//$this->customLogGlobal( 'Pick List [ ' . $username . '__' . date( 'Y-m-d j:i:s' ) . ' ]' , implode( "," , $orderids ) );
				
				
				$folderPath = WWW_ROOT .'logs';
				$dir = new Folder($folderPath, true, 0755);			
				$_Path = $folderPath .'/'. date('dmy_Hi').'_PicklistByLocation.log';    				  
				file_put_contents($_Path, $username."\n".implode( "," , $orderids )); 
					  
				//now abort file status down
 				 
				header('Content-Encoding: UTF-8');
				header('Content-type: text/csv; charset=UTF-8');
				header('Content-Disposition: attachment;filename="'.$name.'"');
				header("Content-Type: application/octet-stream;");
				header('Cache-Control: max-age=0');
				readfile($imgPath.$name);
				$this->CustomPicklistSku->query("TRUNCATE custom_picklist_skus");
				//exit;
					
			}else{
				$this->Session->setflash( 'No orders are available for pick list!', 'flash_danger'); 
				$this->redirect($this->referer());
			}
		}else{
			$this->Session->setflash( 'Please selected skus before to generate pick list or choose other option!', 'flash_danger' ); 
			$this->redirect($this->referer());	
		}
		exit;
	
				
	
	}
	
	public function msortnew($array, $key, $sort_flags = SORT_REGULAR) {
	
	  if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) { //pr($v);
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
			        // @TODO This should be fixed, now it will be sorted as string
                   foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
		    asort($mapping, $sort_flags);
			natsort($mapping);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
	}
	
	public function revercePicklist()
	{
		$this->layout = 'index';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'OrderLocation' );
		$this->loadModel( 'RevercePicklist' );
		
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		$imgPath = WWW_ROOT .'img/HeldOrders/'; 
		$dir = new Folder($imgPath, true, 0755);
		$files = $dir->find('.*\.csv');
		$files = Set::sort($files, '{n}', 'DESC');
		
		$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id'),'conditions' => array('MergeUpdate.reverce_picklist_status IN' => array(1,2)),'MergeUpdate.status' => '2'));	
		
		if(count($orderItems ) > 0){
		foreach($orderItems as $item_order ){
			$all_order[] = $item_order['MergeUpdate']['order_id'];			
		}
		
		if(count($all_order) > 1){
			$param = array('conditions' => array('OrderLocation.pick_list' => '1','OrderLocation.status' => 'canceled', 'OrderLocation.order_id IN' => $all_order),'order' => 'bin_location ASC');				
		}else{
			$param = array('conditions' => array('OrderLocation.pick_list' => '1','OrderLocation.status' => 'canceled', 'OrderLocation.order_id' => $all_order[0]),'order' => 'bin_location ASC');	
		}
		
		$pickListItems 	=	$this->OrderLocation->find('all', $param );
		
		if( count( $pickListItems ) > 0 )
		{
			foreach( $pickListItems as $val )
			{
				 $vdata[$val['OrderLocation']['sku']][] = array('bin_location' => $val['OrderLocation']['bin_location'],
																	'order_id'=>trim($val['OrderLocation']['order_id']),
																	'barcode' => $val['OrderLocation']['barcode'],
																	'quantity' => $val['OrderLocation']['quantity'],
																	'available_qty_bin' => $val['OrderLocation']['available_qty_bin'],
																	'move_date' => $val['OrderLocation']['timestamp']);
				 
			}
			
			foreach($vdata as $sku => $val){
			 $order_count = 0;
			 foreach($val as $d){
				 $order_count++;
			 
		$data[$sku][] = array( 
								'bin_location' => $d['bin_location'],
								'order_id'=>$d['order_id'] ,
								'barcode' => $d['barcode'],
								'quantity' => $d['quantity'],
								'available_qty_bin' => $d['available_qty_bin'],
								'move_date' => $d['move_date'],
								'order_count' => count($val));
																	  
																	
		} 
			$data[$sku]['order_count'] =   count($val);
			$data[$sku]['sku'] =  $sku;
			
		}
		
		$rpicklists	=	$this->RevercePicklist->find('all', array( 'conditions' => array( 'status' => '0' ) ) );
		$this->set( 'rpicklists', $rpicklists );
		$this->set( 'data' , $data );
		$this->set('files', $files);
			
		}
		
	   }	
	}
	
	public function putbacklocation()
	{
		$this->loadModel( 'MergeUpdate' );
		$orderids	=	$this->request->data['orderid'];
		$sku		=	$this->request->data['sku'];
		$qty		=	$this->request->data['qty'];
		$count		=	$this->request->data['count'];
		$det		=	$this->request->data['det'];
		$idsarray	=	explode(',',$orderids);
		
		
		$firstName 	= ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		$lastName 	= ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		$pcName 	= ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
		
		if(count($idsarray) > 1)
		{
			$this->MergeUpdate->updateAll( array('MergeUpdate.reverce_picklist_status' => '3'), array('MergeUpdate.order_id IN' => $idsarray ) );
		} else {
			$this->MergeUpdate->updateAll( array('MergeUpdate.reverce_picklist_status' => '3'), array('MergeUpdate.order_id' => $idsarray[0] ) );
		}
		
		$putback_log = "User Name =".$firstName.' '.$lastName."\tPc Name=".$pcName."\tSKU=".$sku."\tQTY=".$qty."\tOrder Count=".$count."\tLocation Detail=".$det."\tdate=".date('Y-m-d H:i:s')."\n";
		file_put_contents(WWW_ROOT."img/PutBack.log", $putback_log, FILE_APPEND | LOCK_EX);
		echo '1';
		exit;
	}
	
	public function putbacklocationsep()
	{
		$detail		=	$this->request->data['orderedet'];
		$details	=	explode( '####', $detail );
		$orderid	=	$details[0];
		$location	=	$details[1];
		$qty		=	$details[2];
		$sku		=	$details[3];
		$firstName 	= ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		$lastName 	= ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		$pcName 	= ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';$count		=	'1';
		
		//$this->MergeUpdate->updateAll( array('MergeUpdate.reverce_picklist_status' => '2'), array('MergeUpdate.order_id IN' => $orderid ) );
		
		$putback_log = "User Name =".$firstName.' '.$lastName."\tPc Name=".$pcName."\tSKU=".$sku."\tQTY=".$qty."\tOrder Count=".$count."\tLocation Detail=".$orderid.':'.$location.'[ '.$qty.' ]'."\t".date('Y-m-d H:i:s')."\n";
		file_put_contents(WWW_ROOT."img/PutBack.log", $putback_log, FILE_APPEND | LOCK_EX);
		echo '1';
		exit;
		
	}
	
	public function generate_reverce_picklist()
	{
		 		
		$this->layout = '';
		$this->autoRander = false;			
		$this->loadModel( 'OrderLocation' );		
		$this->loadModel( 'MergeUpdate' );	
		$this->loadModel( 'UnprepareOrder' );	
		$this->loadModel( 'Product' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel('CustomPicklistSku');
		$this->loadModel('RevercePicklist');
		date_default_timezone_set('Europe/Jersey');
		
		$firstName 	= ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		$lastName 	= ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		$pcName 	= ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
		
		$products = array();	 
		
		$_product	= $this->Product->find('all',array('fields'=>array('product_sku','product_name'),'order'=>'Product.id DESC'));
		foreach( $_product as $pro ){
			$products[$pro['Product']['product_sku']] =$pro['Product']['product_name'];
		}	 
		
		
		$all_order = array();
		$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id'),'conditions' => array('MergeUpdate.reverce_picklist_status' => '1')));	
	
		foreach($orderItems as $item_order ){
			$all_order[] = $item_order['MergeUpdate']['order_id'];
		}
		if(count($all_order) > 1){
					$param = array('conditions' => array('OrderLocation.pick_list' => '1','OrderLocation.order_id IN' => $all_order),array('order' => 'bin_location ASC'));				
				}else{
					$param = array('conditions' => array('OrderLocation.pick_list' => '1','OrderLocation.order_id ' => $all_order[0]),array('order' => 'bin_location ASC'));	
				}
		
		if(isset($param)){
			$pickListItems 	=	$this->OrderLocation->find('all', $param );	
			if( count( $pickListItems ) > 0 )
			{			
				
				$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
				$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
				$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
				
				$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
				$today = date("d_m_Y_H_i");
				$name  = 'Reverce_Pick_List__'.$today.'.pdf';
				
				$orderids = array(); $data = array(); $tq = 0; $taq = 0;
				foreach( $pickListItems as $val )
				{
					$tq += $val['OrderLocation']['quantity'];	
					$taq += $val['OrderLocation']['available_qty_bin'];	
					$orderids[] = $val['OrderLocation']['order_id'];			
					$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																		'sku'=>trim($val['OrderLocation']['sku']),
																		'barcode' => $val['OrderLocation']['barcode'],
																		'quantity' => $val['OrderLocation']['quantity'],
																		'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
				}
							
				/* dome pdf vendor */
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				/* Html for print pick list */
				$date = date("m/d/Y H:i:s");	
				$insert_str = '';
				$finArray = array();
				$html = '<h3>PickList :- '.$date.'</h3><hr><h3>Total Item  : '.$tq.'</h3>
						<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
						<tr>
						<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
						<th style="border:1px solid;" width="10%" align="center">SKU</th>
						<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
						<th style="border:1px solid;" width="10%" align="center">Barcode</th>
						</tr>';
				
				$data = $this->msortnew($data, array('bin_location'));
				$tdata = array();
				foreach($data as $a){
					$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
											
				}			
				
				foreach($tdata as $sku => $d){
						$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); $save_bin= array();
						foreach($d as $a){
							$bn[$a['bin_location']][] = $a['quantity'];	
							$bna[$a['bin_location']][] = $a['available_qty_bin'];	
							$bar[$a['barcode']] = $a['barcode'];	
						}
						$bins = array_keys($bn);  $oversell = array();
						foreach($bins as $b){
							if($b !=''){
								if(array_sum($bn[$b]) == array_sum($bna[$b])){
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
									$save_bin[]= $b ;
								}else{
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
									$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
									$save_bin[]= $b ;
								}
							}
							$qty[]= array_sum($bn[$b]);
							$avaqty[]= array_sum($bna[$b]);
							
						}	
						$oversell_str = '';
						if(count($oversell) > 0){
							$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
						}		
						$glb_barcode = $this->GlobalBarcode->getAllBarcode($a['barcode']);
						$bar[$glb_barcode[0]] = $glb_barcode[0];
						$html.=	'<tr>
							<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
							<td style="border:1px solid;">'.$sku.'</td>
							<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.substr($products[$sku], 0, 40 ).'</td>
							<td style="border:1px solid;">'.implode(",",$bar).'</td>							
							</tr>';
							
							
							$insert_str .= "('".$sku."','".implode(",",$save_bin)."','". implode(",",$bar)."', '". array_sum($qty) ."','".$oversell_str."','".$name."','".$username."', '".date("Y-m-d H:i:s")."'),";
							
				}	
			
				$html .= '</table>';			 
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render(); 
				
				$imgPath = WWW_ROOT .'img/RevercePickList/'; 
				$path = Router::url('/', true).'img/RevercePickList/';
				
				
				file_put_contents($imgPath.$name, $dompdf->output());
				
				if(count($orderids) > 1){
						$this->MergeUpdate->updateAll(array('MergeUpdate.reverce_picklist_status' => '2'), array('MergeUpdate.order_id IN ' => $orderids));
					 	//$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1'), array('OrderLocation.order_id IN ' => $orderids));
					}else{
					 	$this->MergeUpdate->updateAll(array('MergeUpdate.reverce_picklist_status' => '2'), array('MergeUpdate.order_id = ' => $orderids[0]));
					 	//$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1'), array('OrderLocation.order_id = ' => $orderids[0]));
			}
				//$this->customLogGlobal( 'Pick List [ ' . $username . '__' . date( 'Y-m-d j:i:s' ) . ' ]' , implode( "," , $orderids ) );
				
				$firstName 	= ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
				$lastName 	= ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
				$pcName 	= ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
				
				$this->loadModel( 'RevercePicklist' );	
			 	$data['file_name'] = $name;
				$data['user_name'] = $firstName.' '.$lastName.' [ '.$pcName.' ] ';
				$this->RevercePicklist->saveAll( $data );
				
				$folderPath = WWW_ROOT .'logs';
				$dir = new Folder($folderPath, true, 0755);			
				$_Path = $folderPath .'/'. date('dmy_Hi').'_RevercePicklist.log';    				  
				file_put_contents($_Path, $username."\n".implode( "," , $orderids ));
					  
				$this->generateHeldOrder();
				//now abort file status down
				 
				 
				header('Content-Encoding: UTF-8');
				header('Content-type: text/csv; charset=UTF-8');
				header('Content-Disposition: attachment;filename="'.$name.'"');
				header("Content-Type: application/octet-stream;");
				header('Cache-Control: max-age=0');
				readfile($imgPath.$name);
				//$this->CustomPicklistSku->query("TRUNCATE custom_picklist_skus");
					
			}else{
				$this->Session->setflash( 'No orders are available for pick list!', 'flash_danger'); 
				$this->redirect($this->referer());
			}
		}else{
			$this->Session->setflash( 'Please selected skus before to generate pick list or choose other option!', 'flash_danger' ); 
			$this->redirect($this->referer());	
		}
		exit;
	
	}
	
	public function generateHeldOrder()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('OrderLocation');
		
		$last_updated = date('Y-m-d H:i:s',strtotime("-5 day"));
		$date = date('d-m-Y');
		$getOrders	=	$this->OpenOrder->find('all', array( 'conditions' => array( 'open_order_date >' => $last_updated, 'OpenOrder.status' => 0) )  );
		$fp = fopen(WWW_ROOT."img/HeldOrders/Held_Orders_".$date.".csv","w");
		$sep = ",";
		$header = "Order Id".$sep."SKU".$sep."Location".$sep."Barcode\r\n"; 
		unlink(WWW_ROOT."img/HeldOrders/Held_Orders_".$date.".csv");
		
		file_put_contents(WWW_ROOT."img/HeldOrders/Held_Orders_".$date.".csv",$header);
		
		foreach($getOrders as $getOrder)
		{
			$items					=	unserialize($getOrder['OpenOrder']['items']);
			//pr($getOrder);
			$o_id	=	$getOrder['OpenOrder']['num_order_id'];
			$ord_locations =	$this->OrderLocation->find('all', array('conditions' => array( 'order_id' => $o_id, 'pick_list' => 1) ) );
			$o_sku = array();	$o_barcode = array();$o_location = array();
			if(count($ord_locations) > 0)
			{
				foreach($ord_locations as $ord_location)
				{
					$o_sku[]		=	$ord_location['OrderLocation']['sku'];
					$o_barcode[]	=	$ord_location['OrderLocation']['barcode'];
					$o_location[]	=	$ord_location['OrderLocation']['bin_location'];
				}
				$content = $o_id.$sep.implode(';',$o_sku).$sep.implode(';',$o_location).$sep."'".implode(';',$o_barcode)."\r\n";
				file_put_contents(WWW_ROOT."img/HeldOrders/Held_Orders_".$date.".csv",$content, FILE_APPEND | LOCK_EX);
			}
			
		}
		
	}
	
	
	public function revercepickdone()
	{
		$this->loadModel( 'RevercePicklist' );
		$rid			=	$this->request->data['rpid'];
		$data['id'] 	=   $rid;
		$data['status'] =   '1';
		$this->RevercePicklist->saveAll( $data );
		echo "1";
		exit;
	}
	
	public function getrevercepickliststatus()
	{
		$this->loadModel('MergeUpdate');
		$ordersisd				=	$this->request->data['ids'];
		$ids	=	explode(', ',$ordersisd);
		//pr($ids);
		$nopl = 0;
		$rpl  = 0;
		foreach($ids as $id)
		{
			$revircepichlistststus	=	$this->MergeUpdate->find('first', array('conditions' => array( 'order_id' => $id ),'fields' => array('reverce_picklist_status') ) );
			$revpickstst	=	$revircepichlistststus['MergeUpdate']['reverce_picklist_status'];
			if($revpickstst == 1){
			 $nopl++;
			} else { 
			 $rpl++;
			}
		}
		if($rpl >= 1){
			echo '2';
			exit;
		} else { 
			echo '1';
			exit;
		}
	}
	
	
}