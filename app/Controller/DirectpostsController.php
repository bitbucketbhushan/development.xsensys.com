<?php

class DirectpostsController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Directposts";
    var $components = array('Session','Upload','Common','Auth','Paginator');
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
    public function getClientTime()
      {
		  $this->layout = '';
		  $this->autoRender = false;
		  date_default_timezone_set('Europe/Jersey');
		  $serviceProvider = 'Jersey Post';//$this->request->data['serviceProvider'];
		  if( $serviceProvider == "Jersey Post" ){
			  if( time() > strtotime('3 pm') ){
				  echo '0';
				  exit;
			  } else{
				  echo '0';
				  exit;
			  }
		  }
		  else
		  {			  
			  if( time() > strtotime('1 pm') ){
				  echo '0';
				  exit;
			  } else {
				  echo '0';
				  exit;
			  }
		  }
	  }
	  
	  
	public function setInfoEdoc()
	{
		$this->layout = '';
		$this->autoRender = false;          
		// Get All manifest related services
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'MergeUpdate' );
		$serviceProvider 	= 'Jersey Post';//$this->request->data['serviceProvider'];
		//$service_code 		= 'FR7';
		
		
		date_default_timezone_set('Europe/Jersey');
		$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));
		
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$service = str_replace(' ', '', str_replace(':','_',$serviceProvider.'-'. date("d.m.Y") .'_'. $time_in_12_hour_format));
		$params = array(
			'conditions' => array(
				'ServiceCounter.service_provider' => $serviceProvider,
				'ServiceCounter.service_code IN'=>array('GYE','FRU'),
				'ServiceCounter.counter >' => 0,
				'ServiceCounter.original_counter !=' => ''
			),
			'fields' => array(				
				'ServiceCounter.counter',
				'ServiceCounter.bags',
				'ServiceCounter.service_code',
				'ServiceCounter.order_ids'
			)
		);
		
		$eShipperData = $this->ServiceCounter->find( 'all', $params );	
		foreach( $eShipperData as $value )
		{
			 $ids	=	explode(',' , $value['ServiceCounter']['order_ids']);
			 foreach($ids as $id)
			 {
			 	//$this->setManifestRecordJerseyPost( $id,  $service );
			 }
		}		
		echo (json_encode(array('status' => '1', 'data' => $eShipperData)));
		exit;
		
	}
	
	public function setInfoEdocFrance()
	{
		$this->layout = '';
		$this->autoRender = false;          
		// Get All manifest related services
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'MergeUpdate' );
		$serviceProvider 	= 'Jersey Post';//$this->request->data['serviceProvider'];
		//$service_code 		= 'FR7';
		
		
		date_default_timezone_set('Europe/Jersey');
		$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));
		
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$service = str_replace(' ', '', str_replace(':','_',$serviceProvider.'-'. date("d.m.Y") .'_'. $time_in_12_hour_format));
		$params = array(
			'conditions' => array(
				'ServiceCounter.service_provider' => $serviceProvider,
				'ServiceCounter.destination'=>'France',
				'ServiceCounter.counter >' => 0,
				'ServiceCounter.original_counter !=' => ''
			),
			'fields' => array(				
				'ServiceCounter.counter',
				'ServiceCounter.bags',
				'ServiceCounter.service_code',
				'ServiceCounter.order_ids'
			)
		);
		
		$eShipperData = $this->ServiceCounter->find( 'all', $params );	
		foreach( $eShipperData as $value )
		{
			 $ids	=	explode(',' , $value['ServiceCounter']['order_ids']);
			 foreach($ids as $id)
			 {
			 	//$this->setManifestRecordJerseyPost( $id,  $service );
			 }
		}		
		echo (json_encode(array('status' => '1', 'data' => $eShipperData)));
		exit;
		
	}
	
	public function getGermanyAndFranceManifest()
	{
		$this->layout = '';
		$this->autoRender = false;          
	
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'ManifestRecord' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'Category' );					
		$serviceProvider 	= 	'Jersey Post';//$this->request->data['serviceProvider'];
		$manfestcountry  	= 	$this->request->data['country'];
		
		/*----------If not Scaned--------------*/
		App::Import('Controller', 'Jerseypost'); 
		$jp = new JerseypostController();
		$jp->unmarkedParcels();
				
		if( $manfestcountry == 'germany' ){
			$service_code 		= 'GYE';
		} else if( $manfestcountry == 'france' )
		{
			$service_code 		= 'FRU';
		}
		
		$manifest = json_decode(json_encode($this->ServiceCounter->find( 'all' , 
																				array( 
																					'conditions' => array( 
																							'ServiceCounter.service_provider' => $serviceProvider, 
																							'ServiceCounter.service_code'=>$service_code,
																							'ServiceCounter.order_ids !=' => '', 
																							'ServiceCounter.counter >' => 0, 
																							'ServiceCounter.original_counter >' => 0, 
																						)))),0); 
		//pr($manifest);
		//exit;
		if( count($manifest) > 0 )
		{
			$totalOrders = 0;
			$totalPecises = 0;
			$totalMassWeightNow = 0;

			$inc = 1;$cnt = 6;$e = 0;foreach( $manifest as $manifestIndex => $manifestValue )
			{
				if( $e == 0 )
				{
					App::import('Vendor', 'PHPExcel/IOFactory');
					App::import('Vendor', 'PHPExcel');                          
					$objPHPExcel = new PHPExcel();       
					$objPHPExcel->createSheet();
					$objPHPExcel->setActiveSheetIndex(0);
					$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Item');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Shipper');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Receiver');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Address');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Postcode');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Address');        
					$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Address'); 					
					$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);		                                                           
					$objPHPExcel->getActiveSheet()->setCellValue('G5', 'COO');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Pces');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Weight(g)');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Value');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Shipping Cost');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Total Value');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Cur.');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Commodity');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('O5', 'Inv No.');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('P5', 'Bag-No'); 
					$objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Destination');                                                                  
				}
				
				$orderIds = explode( ',' , $manifestValue->ServiceCounter->order_ids);
				App::import( 'Controller' , 'Cronjobs');
				$objCronjobs = new CronjobsController();
				
				$combineSku = '';				
				$k = 0;while( $k <= count($orderIds)-1 )
				{
					$orderIdSpecified = $orderIds[$k];
					$this->updateManifestDate($orderIdSpecified);
					$params = array(
						'conditions' => array('MergeUpdate.id' => $orderIdSpecified),
						'fields' => array('MergeUpdate.id','MergeUpdate.order_id','MergeUpdate.product_order_id_identify','MergeUpdate.quantity','MergeUpdate.sku',			         								  'MergeUpdate.price','MergeUpdate.packet_weight','MergeUpdate.envelope_weight','MergeUpdate.source_coming'));
					
					$mergeOrder = json_decode(json_encode($this->MergeUpdate->find('all', $params)),0);
					$getSku = explode( ',' , $mergeOrder[0]->MergeUpdate->sku);
				    $packageWeight = $mergeOrder[0]->MergeUpdate->envelope_weight;                    
					$totalPriceValue = 0;$massWeight = 0;$totalUnits = 0;$combineTitle = '';$combineCategory = ''; $calculateWeight = 0;
                    $getSpecifier = explode('-', $mergeOrder[0]->MergeUpdate->product_order_id_identify);
                   
                    $setSpaces = '';
                    $identifier = $getSpecifier[1];
                    $em = 0;while( $em < $identifier ){
						$setSpaces .= $setSpaces.' ';						
						$em++;	
					}
					$j = 0;while( $j <= count($getSku)-1 ){
						$newSku = explode( 'XS-' , $getSku[$j] );
						$setNewSku = 'S-'.$newSku[1];
						$this->Product->bindModel(
							array('hasOne' => array('Category' => array('foreignKey' => false,'conditions' => array('Category.id = Product.category_id'),
							      'fields' => array('Category.id,Category.category_name')))));
					
						$productSku = $this->Product->find('first' ,array('conditions' => array('Product.product_sku' => $setNewSku)));
						if( $combineTitle == '' )
						{
							$combineTitle = substr($productSku['Product']['product_name'],0,25);	
							$totalUnits = $totalUnits + $newSku[0];     
                            $calculateWeight = ($newSku[0] * $productSku['ProductDesc']['weight']);
							$massWeight = $massWeight + $calculateWeight;		
							$combineCategory = $productSku['Category']['category_name'];				
						}
						else
						{
							$combineTitle .= ',' . substr($productSku['Product']['product_name'],0,25);	
							$totalUnits = $totalUnits + $newSku[0];
                            $calculateWeight = ($newSku[0] * $productSku['ProductDesc']['weight']);
							$massWeight = $massWeight + $calculateWeight;                                                        
							$combineCategory .= ',' . $productSku['Category']['category_name'];				
						}
						if( $combineSku == '' )
							$combineSku = $setNewSku;	
						else
							$combineSku .= ',' . $setNewSku;	
						
					$j++;	
					}
					$totalPecises = $totalPecises + $totalUnits;
					$massWeight = $packageWeight + $massWeight; 
					$totalMassWeightNow = $totalMassWeightNow + $massWeight;	
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$cnt, $inc );
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$cnt, 'ESL' );
					$paramsConsignee = array(
						'conditions' => array('OpenOrder.num_order_id' => $mergeOrder[0]->MergeUpdate->order_id	),
						'fields' => array('OpenOrder.num_order_id','OpenOrder.id','OpenOrder.general_info','OpenOrder.shipping_info','OpenOrder.customer_info',									                                          'OpenOrder.totals_info'));
					
					$getConsigneeDetailFromLinnworksOrder = json_decode(json_encode($this->OpenOrder->find( 'first', $paramsConsignee )),0);	
					$generalInfo	=	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->general_info);
					$shippingInfo	=	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->shipping_info);
					$customerInfo	=	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->customer_info);
					$totalsInfo		=	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->totals_info);
					$congineeInfo 	= 	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->customer_info);					
				
					//### Added 30/09/2019 ####
					if(strtolower($mergeOrder[0]->MergeUpdate->source_coming) == 'costdropper'){
 						$Adds = $congineeInfo->Address->Address2 .'  '. $congineeInfo->Address->Address1;
					}else{
						$Adds = $congineeInfo->Address->Address1 .'  '. $congineeInfo->Address->Address2;
					}
					if($congineeInfo->Address->Region){
						$cityInfo = $congineeInfo->Address->Town .' , '. $congineeInfo->Address->Region;
					}else{
						$cityInfo = $congineeInfo->Address->Town;
					}
 					//#### End ###
					
					$PostCode	=	str_pad($congineeInfo->Address->PostCode,5,"0",STR_PAD_LEFT);
					//echo "<br>";
					$totalInfo = unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->totals_info);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$cnt, str_replace(' ',$setSpaces,$congineeInfo->Address->FullName) );
					//### Added 30/09/2019 ####
					//$objPHPExcel->getActiveSheet()->setCellValue('D'.$cnt, str_replace(' ',$setSpaces,$congineeInfo->Address->Address1) );
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$cnt, $Adds );
					
					//#### End ###
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$cnt, $PostCode );
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$cnt, $cityInfo );
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$cnt, 'Jersey' );
					$setSpaces = '';
					$country = '';					
					//$objPHPExcel->getActiveSheet()->setCellValue('H'.$cnt, '1' ); on Recmondation of Lalit 27 02 2020
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$cnt, $totalUnits);
					//$massWeight = ($massWeight * 1000);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$cnt, ($massWeight*1000) );
					$currencyMatter = $totalInfo->Currency;
					$globalCurrencyConversion = 1;
					
					if( $currencyMatter == "EUR" ){
						$globalCurrencyConversion = 1;
					} else {
						$globalCurrencyConversion = 1.38;
					}
					
					$totalValue = 0;
					$setPrice = $mergeOrder[0]->MergeUpdate->price;
					if( ( $setPrice * $globalCurrencyConversion) > 21.99 ){
						$totalValue = number_format($this->getrand(), 2, '.', '');
					} else {
						$totalValue = number_format(( $setPrice * $globalCurrencyConversion ), 2, '.', ''); 
					}
					
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$cnt, $totalValue );
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$cnt, $totalsInfo->PostageCost );
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$cnt, $totalValue + $totalsInfo->PostageCost );
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$cnt, $totalsInfo->Currency);
					$countPostcode = strlen($congineeInfo->Address->PostCode);
					$countChar = $countPostcode - 3;
					
					if( $countPostcode == 5 ){
						$getBagNo = substr( $congineeInfo->Address->PostCode , 0 , 2 );
					} else {
						$getBagNo = substr( $congineeInfo->Address->PostCode , 0 , 1 );
					}
					
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$cnt, $combineCategory );
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$cnt, $mergeOrder[0]->MergeUpdate->product_order_id_identify  );
					$objPHPExcel->getActiveSheet()->setCellValue('p'.$cnt, substr($congineeInfo->Address->PostCode, 0, 2 )  );
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$cnt, $congineeInfo->Address->Country  );
					$combineSku = '';$totalUnits = 0;$massWeight = 0;$combineCategory = '';
					$totalOrders++;$inc++;$cnt++;$k++;	
				}
				
				$serviceData = json_decode(json_encode($this->ServiceCounter->find('first',array('conditions'=>array('ServiceCounter.id'=>$manifestValue->ServiceCounter->id)))),0);							
				$originalCounter = $serviceData->ServiceCounter->original_counter - $serviceData->ServiceCounter->counter;
				$manifestRecord = array(
					'destination' => $manifestValue->ServiceCounter->destination,
					'service_name' => $manifestValue->ServiceCounter->service_name,
					'service_provider' => $manifestValue->ServiceCounter->service_provider,
					'service_code' => $manifestValue->ServiceCounter->service_code,
					'order_ids' => $manifestValue->ServiceCounter->order_ids,
					'date' => date('Y-m-d')
				); 
			
				$this->ManifestRecord->saveAll( $manifestRecord );
				unset($manifestRecord);
				$manifestRecord = '';
				$getOrderId = explode('-',$manifestValue->ServiceCounter->order_ids);
				foreach( $getOrderId as $getOrderIdValue )
				{
					$innerOrderSorted = explode( ',' , $getOrderIdValue );					
					foreach( $innerOrderSorted as $innerValue )
					{
						$this->request->data['MergeUpdate']['MergeUpdate']['id'] = $innerValue;
						$this->request->data['MergeUpdate']['MergeUpdate']['manifest_sheet_marker'] = 1;

						$this->request->data['MergeUpdate']['MergeUpdate']['date'] = date('Y-m-d');
						$this->MergeUpdate->saveAll( $this->request->data['MergeUpdate'] );
					}
				}				
				$this->request->data['ServiceCounter']['ServiceCounter']['id'] = $manifestValue->ServiceCounter->id;
				$this->request->data['ServiceCounter']['ServiceCounter']['original_counter'] = $originalCounter;
				$this->request->data['ServiceCounter']['ServiceCounter']['counter'] = 0;
				$this->request->data['ServiceCounter']['ServiceCounter']['order_ids'] = '';
			 	$this->ServiceCounter->saveAll( $this->request->data['ServiceCounter'] );
				$e++;	
			}
			
			$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 14) );
			
			$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('C2', '                                        LOW VALUE MANIFEST DESTINATION '.strtoupper($manfestcountry));
			$objPHPExcel->getActiveSheet()->mergeCells('C2:M2');
			$objPHPExcel->getActiveSheet()->getStyle('C2:M2')->getFont()->setSize(14);	
			$objPHPExcel->getActiveSheet()->getStyle('C2:M2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('D5', '                Postcode      Address');
			$objPHPExcel->getActiveSheet()->mergeCells('D5:F5');
			$objPHPExcel->getActiveSheet()->getStyle('D5:F5')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('D5:F5')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('F3', 'DATE: '.date('d.m.Y'));
			$objPHPExcel->getActiveSheet()->mergeCells('F3:G3');
			$objPHPExcel->getActiveSheet()->getStyle('F3:G3')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('F3:G3')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('I3', 'ORIGIN:  Jersey');
			$objPHPExcel->getActiveSheet()->getStyle('I3')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('I3')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('N3', 'DESTINATION: '. ucfirst($manfestcountry));
			$objPHPExcel->getActiveSheet()->getStyle('N3')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('N3')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Total No of Items: '. $k );
			$objPHPExcel->getActiveSheet()->getStyle('C4')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('C4')->applyFromArray($styleArray);
		// ### updated 30/09/2019 #######	
			$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Manifestabfertigung ');
			$objPHPExcel->getActiveSheet()->mergeCells('E4:F4');
			$objPHPExcel->getActiveSheet()->getStyle('E4:F4')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('E4:F4')->applyFromArray($styleArray);
		// #### End ###	
			$objPHPExcel->getActiveSheet()->SetCellValue('I4', 'TOTAL PIECES : '. $totalPecises );
			$objPHPExcel->getActiveSheet()->getStyle('I4')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('I4')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('N4', 'TOTAL WEIGHT : '.number_format($totalMassWeightNow, 3, '.', ' ').'kg');
			$objPHPExcel->getActiveSheet()->getStyle('N4')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('N4')->applyFromArray($styleArray);
			
			$objPHPExcel->setActiveSheetIndex(0);                                                                              
			date_default_timezone_set('Europe/Jersey');
			$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));
			
			$folderName = 'Service Manifest -'. date("d.m.Y");
			$service = str_replace(' ', '', str_replace(':','_',$serviceProvider.'_'.$manfestcountry.'-'. date("d.m.Y") .'_'. $time_in_12_hour_format));
			$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
			
			$uploadUrl = WWW_ROOT .'img/cut_off/'. $folderName . '/' .$service.'.xls';                                          
			$uploadUrI = Router::url('/', true) . 'img/cut_off/'. $folderName . '/' .$service.'.xls';                                          
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
			$objWriter->save($uploadUrl);
			echo $uploadUrI; exit;
		}
		else
		{
			echo "blank"; exit;
		}
	}
	
	public function getFranceManifest()
	{
		$this->layout = '';
		$this->autoRender = false;          
	
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'ManifestRecord' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'Category' );					
		$serviceProvider 	= 	'Jersey Post';//$this->request->data['serviceProvider'];
		$manfestcountry  	= 	$this->request->data['country'];
		 
		
		/*----------If not Scaned--------------*/
		App::Import('Controller', 'Jerseypost'); 
		$jp = new JerseypostController();
		$jp->unmarkedParcels();
 		
		if( $manfestcountry == 'france_under' ){
			$service_code 		= 'FRU';
		}else if( $manfestcountry == 'france_over' ){
			$service_code = ['FRO','FRT','FRS'];
		}
		
		$manifest = json_decode(json_encode($this->ServiceCounter->find( 'all' , 
																				array( 
																					'conditions' => array( 
																							'ServiceCounter.service_provider' => $serviceProvider, 
																							'ServiceCounter.service_code'=>$service_code,
																							'ServiceCounter.destination'=>'France',
																							'ServiceCounter.order_ids !=' => '', 
																							'ServiceCounter.counter >' => 0, 
																							'ServiceCounter.original_counter >' => 0, 
																						)))),0); 
		//exit;
		if( count($manifest) > 0 )
		{
			$totalOrders = 0;
			$totalPecises = 0;
			$totalMassWeightNow = 0;

			$inc = 1;$cnt = 6;$e = 0;foreach( $manifest as $manifestIndex => $manifestValue )
			{
				if( $e == 0 )
				{
					App::import('Vendor', 'PHPExcel/IOFactory');
					App::import('Vendor', 'PHPExcel');                          
					$objPHPExcel = new PHPExcel();       
					$objPHPExcel->createSheet();
					$objPHPExcel->setActiveSheetIndex(0);
					$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Item');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Shipper');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Receiver');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Address');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Postcode');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Address');        
					$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Address'); 					
					$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);		                                                           
					$objPHPExcel->getActiveSheet()->setCellValue('G5', 'COO');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Pces');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Weight(g)');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Value');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Shipping Cost');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Total Value');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Cur.');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Commodity');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('O5', 'Inv No.');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('P5', 'Bag-No'); 
					$objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Destination');                                                                  
				}
				
				$orderIds = explode( ',' , $manifestValue->ServiceCounter->order_ids);
				App::import( 'Controller' , 'Cronjobs');
				$objCronjobs = new CronjobsController();
				
				$combineSku = '';				
				$k = 0;while( $k <= count($orderIds)-1 )
				{
					$orderIdSpecified = $orderIds[$k];
					//$this->updateManifestDate($orderIdSpecified);
					$params = array(
						'conditions' => array('MergeUpdate.id' => $orderIdSpecified),
						'fields' => array('MergeUpdate.id','MergeUpdate.order_id','MergeUpdate.product_order_id_identify','MergeUpdate.quantity','MergeUpdate.sku',			         								  'MergeUpdate.price','MergeUpdate.packet_weight','MergeUpdate.envelope_weight','MergeUpdate.source_coming'));
					
					$mergeOrder = json_decode(json_encode($this->MergeUpdate->find('all', $params)),0);
					$getSku = explode( ',' , $mergeOrder[0]->MergeUpdate->sku);
				    $packageWeight = $mergeOrder[0]->MergeUpdate->envelope_weight;                    
					$totalPriceValue = 0;$massWeight = 0;$totalUnits = 0;$combineTitle = '';$combineCategory = ''; $calculateWeight = 0;
                    $getSpecifier = explode('-', $mergeOrder[0]->MergeUpdate->product_order_id_identify);
                   
                    $setSpaces = '';
                    $identifier = $getSpecifier[1];
                    $em = 0;while( $em < $identifier ){
						$setSpaces .= $setSpaces.' ';						
						$em++;	
					}
					$j = 0;while( $j <= count($getSku)-1 ){
						$newSku = explode( 'XS-' , $getSku[$j] );
						$setNewSku = 'S-'.$newSku[1];
						$this->Product->bindModel(
							array('hasOne' => array('Category' => array('foreignKey' => false,'conditions' => array('Category.id = Product.category_id'),
							      'fields' => array('Category.id,Category.category_name')))));
					
						$productSku = $this->Product->find('first' ,array('conditions' => array('Product.product_sku' => $setNewSku)));
						if( $combineTitle == '' )
						{
							$combineTitle = substr($productSku['Product']['product_name'],0,25);	
							$totalUnits = $totalUnits + $newSku[0];     
                            $calculateWeight = ($newSku[0] * $productSku['ProductDesc']['weight']);
							$massWeight = $massWeight + $calculateWeight;		
							$combineCategory = $productSku['Category']['category_name'];				
						}
						else
						{
							$combineTitle .= ',' . substr($productSku['Product']['product_name'],0,25);	
							$totalUnits = $totalUnits + $newSku[0];
                            $calculateWeight = ($newSku[0] * $productSku['ProductDesc']['weight']);
							$massWeight = $massWeight + $calculateWeight;                                                        
							$combineCategory .= ',' . $productSku['Category']['category_name'];				
						}
						if( $combineSku == '' )
							$combineSku = $setNewSku;	
						else
							$combineSku .= ',' . $setNewSku;	
						
					$j++;	
					}
					$totalPecises = $totalPecises + $totalUnits;
					$massWeight = $packageWeight + $massWeight;  
  					$totalMassWeightNow = $totalMassWeightNow + $massWeight;	
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$cnt, $inc );
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$cnt, 'ESL' );
					$paramsConsignee = array(
						'conditions' => array('OpenOrder.num_order_id' => $mergeOrder[0]->MergeUpdate->order_id	),
						'fields' => array('OpenOrder.num_order_id','OpenOrder.id','OpenOrder.general_info','OpenOrder.shipping_info','OpenOrder.customer_info',									                                          'OpenOrder.totals_info'));
					
					$getConsigneeDetailFromLinnworksOrder = json_decode(json_encode($this->OpenOrder->find( 'first', $paramsConsignee )),0);	
					$generalInfo	=	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->general_info);
					$shippingInfo	=	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->shipping_info);
					$customerInfo	=	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->customer_info);
					$totalsInfo		=	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->totals_info);
					$congineeInfo 	= 	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->customer_info);					
				
					//### Added 30/09/2019 ####
					if(strtolower($mergeOrder[0]->MergeUpdate->source_coming) == 'costdropper'){
						$Adds = $congineeInfo->Address->Address2 .'  '. $congineeInfo->Address->Address1;
					}else{
						$Adds = $congineeInfo->Address->Address1 .'  '. $congineeInfo->Address->Address2;
					}
					if($congineeInfo->Address->Region){
						$cityInfo = $congineeInfo->Address->Town .' , '. $congineeInfo->Address->Region;
					}else{
						$cityInfo = $congineeInfo->Address->Town;
					}
					//#### End ###
					
					$PostCode	=	str_pad($congineeInfo->Address->PostCode,5,"0",STR_PAD_LEFT);
					//echo "<br>";
					$totalInfo = unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->totals_info);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$cnt, str_replace(' ',$setSpaces,$congineeInfo->Address->FullName) );
					//### Added 30/09/2019 ####
					//$objPHPExcel->getActiveSheet()->setCellValue('D'.$cnt, str_replace(' ',$setSpaces,$congineeInfo->Address->Address1) );
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$cnt, $Adds );
					
					//#### End ###
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$cnt, $PostCode );
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$cnt, $cityInfo );
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$cnt, 'Jersey' );
					$setSpaces = '';
					$country = '';					
					//$objPHPExcel->getActiveSheet()->setCellValue('H'.$cnt, '1' ); on Recmondation of Lalit 27 02 2020
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$cnt, $totalUnits);
					//$massWeight = ($massWeight * 1000);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$cnt, ($massWeight*1000) );
					$currencyMatter = $totalInfo->Currency;
					$globalCurrencyConversion = 1;
					
					if( $currencyMatter == "EUR" ){
						$globalCurrencyConversion = 1;
					} else {
						$globalCurrencyConversion = 1.38;
					}
					
					$totalValue = 0;
					$setPrice = $mergeOrder[0]->MergeUpdate->price;
					if( ( $setPrice * $globalCurrencyConversion) > 21.99 ){
						$totalValue = number_format($this->getrand(), 2, '.', '');
					} else {
						$totalValue = number_format(( $setPrice * $globalCurrencyConversion ), 2, '.', ''); 
					}
					
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$cnt, $totalValue );
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$cnt, number_format($totalsInfo->PostageCost,2));
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$cnt, number_format(($totalValue + $totalsInfo->PostageCost),2) );
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$cnt, $totalsInfo->Currency);
					$countPostcode = strlen($congineeInfo->Address->PostCode);
					$countChar = $countPostcode - 3;
					
					if( $countPostcode == 5 ){
						$getBagNo = substr( $congineeInfo->Address->PostCode , 0 , 2 );
					} else {
						$getBagNo = substr( $congineeInfo->Address->PostCode , 0 , 1 );
					}
					
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$cnt, $combineCategory );
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$cnt, $mergeOrder[0]->MergeUpdate->product_order_id_identify  );
					$objPHPExcel->getActiveSheet()->setCellValue('p'.$cnt, substr($congineeInfo->Address->PostCode, 0, 2 )  );
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$cnt, $congineeInfo->Address->Country  );
					$combineSku = '';$totalUnits = 0;$massWeight = 0;$combineCategory = '';
					$totalOrders++;$inc++;$cnt++;$k++;	
				
					
				}
				$serviceData = json_decode(json_encode($this->ServiceCounter->find('first',array('conditions'=>array('ServiceCounter.id'=>$manifestValue->ServiceCounter->id)))),0);							
				$originalCounter = $serviceData->ServiceCounter->original_counter - $serviceData->ServiceCounter->counter;
				$manifestRecord = array(
					'destination' => $manifestValue->ServiceCounter->destination,
					'service_name' => $manifestValue->ServiceCounter->service_name,
					'service_provider' => $manifestValue->ServiceCounter->service_provider,
					'service_code' => $manifestValue->ServiceCounter->service_code,
					'order_ids' => $manifestValue->ServiceCounter->order_ids,
					'date' => date('Y-m-d')
				); 
			
			//	$this->ManifestRecord->saveAll( $manifestRecord );
				unset($manifestRecord);
				$manifestRecord = '';
				$getOrderId = explode('-',$manifestValue->ServiceCounter->order_ids);
				foreach( $getOrderId as $getOrderIdValue )
				{
					$innerOrderSorted = explode( ',' , $getOrderIdValue );					
					foreach( $innerOrderSorted as $innerValue )
					{
						$this->request->data['MergeUpdate']['MergeUpdate']['id'] = $innerValue;
						$this->request->data['MergeUpdate']['MergeUpdate']['manifest_sheet_marker'] = 1;

						$this->request->data['MergeUpdate']['MergeUpdate']['date'] = date('Y-m-d');
						$this->MergeUpdate->saveAll( $this->request->data['MergeUpdate'] );
					}
				}				
				$this->request->data['ServiceCounter']['ServiceCounter']['id'] = $manifestValue->ServiceCounter->id;
				$this->request->data['ServiceCounter']['ServiceCounter']['original_counter'] = $originalCounter;
				$this->request->data['ServiceCounter']['ServiceCounter']['counter'] = 0;
				$this->request->data['ServiceCounter']['ServiceCounter']['order_ids'] = '';
			 	//$this->ServiceCounter->saveAll( $this->request->data['ServiceCounter'] );
				$e++;	
			}
			
			$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 14) );
			
			$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('C2', '                                        LOW VALUE MANIFEST DESTINATION '.strtoupper($manfestcountry));
			$objPHPExcel->getActiveSheet()->mergeCells('C2:M2');
			$objPHPExcel->getActiveSheet()->getStyle('C2:M2')->getFont()->setSize(14);	
			$objPHPExcel->getActiveSheet()->getStyle('C2:M2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('D5', '                Postcode      Address');
			$objPHPExcel->getActiveSheet()->mergeCells('D5:F5');
			$objPHPExcel->getActiveSheet()->getStyle('D5:F5')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('D5:F5')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('F3', 'DATE: '.date('d.m.Y'));
			$objPHPExcel->getActiveSheet()->mergeCells('F3:G3');
			$objPHPExcel->getActiveSheet()->getStyle('F3:G3')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('F3:G3')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('I3', 'ORIGIN:  Jersey');
			$objPHPExcel->getActiveSheet()->getStyle('I3')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('I3')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('N3', 'DESTINATION: '. ucfirst($manfestcountry));
			$objPHPExcel->getActiveSheet()->getStyle('N3')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('N3')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Total No of Items: '. $k );
			$objPHPExcel->getActiveSheet()->getStyle('C4')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('C4')->applyFromArray($styleArray);
		// ### updated 30/09/2019 #######	
			$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Manifestabfertigung ');
			$objPHPExcel->getActiveSheet()->mergeCells('E4:F4');
			$objPHPExcel->getActiveSheet()->getStyle('E4:F4')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('E4:F4')->applyFromArray($styleArray);
		// #### End ###	
			$objPHPExcel->getActiveSheet()->SetCellValue('I4', 'TOTAL PIECES : '. $totalPecises );
			$objPHPExcel->getActiveSheet()->getStyle('I4')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('I4')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('N4', 'TOTAL WEIGHT : '.number_format($totalMassWeightNow, 3, '.', ' ').'kg');
			$objPHPExcel->getActiveSheet()->getStyle('N4')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('N4')->applyFromArray($styleArray);
			
			$objPHPExcel->setActiveSheetIndex(0);                                                                              
			date_default_timezone_set('Europe/Jersey');
			$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));
			
			$folderName = 'Service Manifest -'. date("d.m.Y");
			$service = str_replace(' ', '', str_replace(':','_',$serviceProvider.'_'.$manfestcountry.'-'. date("d.m.Y") .'_'. $time_in_12_hour_format));
			$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
			
			$uploadUrl = WWW_ROOT .'img/cut_off/'. $folderName . '/' .$service.'.xls';                                          
			$uploadUrI = Router::url('/', true) . 'img/cut_off/'. $folderName . '/' .$service.'.xls';                                          
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
			$objWriter->save($uploadUrl);
			echo $uploadUrI; exit;
		}
		else
		{
			echo "blank"; exit;
		}
	}
	
	public function getGermanyFranceManifest($manfestcountry = null)
	{
		$this->layout = '';
		$this->autoRender = false;          
	
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'ManifestRecord' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'Category' );					
		$serviceProvider 	= 	'Jersey Post';//$this->request->data['serviceProvider'];
		
		if(isset($this->request->data['country']) && $this->request->data['country'] != ''){
			$manfestcountry  	= 	$this->request->data['country'];
		}
		
		if( $manfestcountry == 'germany' ){
			$service_code 		= 'GYE';
		}else if( $manfestcountry == 'france' ){
			$service_code 		= 'FRU';
		}
		
		$scan_date = date('Y-m-d', strtotime("-10 Days"));
		
		$orderIds = []; $orderData = [];
		
		$result = $this->MergeUpdate->find('all' , array( 'conditions' => array( 'scan_date >' => $scan_date,'manifest_date' => '','provider_ref_code'=>$service_code),'fields'=>['id','order_id','merge_order_date','order_date','provider_ref_code','service_name','scan_date','process_date','delevery_country','service_provider'] ));
		
		foreach($result as $v){
			$orderIds[] = $v['MergeUpdate']['id'];
			$orderData[] = ['order_id'=>$v['MergeUpdate']['order_id'],'merge_order_date'=>$v['MergeUpdate']['merge_order_date'],'provider_ref_code'=>$v['MergeUpdate']['provider_ref_code'],'service_name'=>$v['MergeUpdate']['service_name']];
 			
			$delevery_country = $v['MergeUpdate']['delevery_country'];
			$service_name = $v['MergeUpdate']['service_name'];
				 
		}
 			
 
		if( count($orderIds) > 0 )
		{
				$totalOrders = 0;
				$totalPecises = 0;
				$totalMassWeightNow = 0;
				date_default_timezone_set('Europe/Jersey');
				$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));
				
				$folderName = 'Service Manifest -'. date("d.m.Y");
				$service = str_replace(' ', '', str_replace(':','_',$serviceProvider.'_'.$manfestcountry.'-'. date("d.m.Y") .'_'. $time_in_12_hour_format));
				$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
			
			
				App::import('Vendor', 'PHPExcel/IOFactory');
				App::import('Vendor', 'PHPExcel');                          
				$objPHPExcel = new PHPExcel();       
				$objPHPExcel->createSheet();
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Item');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Shipper');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Receiver');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Address');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Postcode');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Address');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'COO');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Pces');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Weight(g)');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Value');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Shipping Cost');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Total Value');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Cur.');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Commodity');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('O5', 'Inv No.');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('P5', 'Bag-No'); 
				$objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Destination');      

				$inc = 1;$cnt = 6;$e = 0;
				$file_to_save = WWW_ROOT .'img/cut_off/'.$service.'.log';
 				//$orderIds = explode( ',' , $manifestValue->ServiceCounter->order_ids);
				App::import( 'Controller' , 'Cronjobs');
				$objCronjobs = new CronjobsController();
				
				$combineSku = '';				
				$k = 0;while( $k <= count($orderIds)-1 )
				{
					$orderIdSpecified = $orderIds[$k];
					
					$this->updateManifestDate($orderIdSpecified);
					
					//save the pdf file on the server
					file_put_contents($file_to_save, $orderIdSpecified."\n",  FILE_APPEND | LOCK_EX);		
					
					$params = array(
						'conditions' => array('MergeUpdate.id' => $orderIdSpecified),
						'fields' => array('MergeUpdate.id','MergeUpdate.order_id','MergeUpdate.product_order_id_identify','MergeUpdate.quantity','MergeUpdate.sku',			         								  'MergeUpdate.price','MergeUpdate.packet_weight','MergeUpdate.envelope_weight'));
					
					$mergeOrder = json_decode(json_encode($this->MergeUpdate->find('all', $params)),0);
					$getSku = explode( ',' , $mergeOrder[0]->MergeUpdate->sku);
				    $packageWeight = $mergeOrder[0]->MergeUpdate->envelope_weight;                    
					$totalPriceValue = 0;$massWeight = 0;$totalUnits = 0;$combineTitle = '';$combineCategory = ''; $calculateWeight = 0;
                    $getSpecifier = explode('-', $mergeOrder[0]->MergeUpdate->product_order_id_identify);
                   
                    $setSpaces = '';
                    $identifier = $getSpecifier[1];
                    $em = 0;while( $em < $identifier ){
						$setSpaces .= $setSpaces.' ';						
						$em++;	
					}
					$j = 0;while( $j <= count($getSku)-1 ){
						$newSku = explode( 'XS-' , $getSku[$j] );
						$setNewSku = 'S-'.$newSku[1];
						$this->Product->bindModel(
							array('hasOne' => array('Category' => array('foreignKey' => false,'conditions' => array('Category.id = Product.category_id'),
							      'fields' => array('Category.id,Category.category_name')))));
					
						$productSku = $this->Product->find('first' ,array('conditions' => array('Product.product_sku' => $setNewSku)));
						if( $combineTitle == '' )
						{
							$combineTitle = substr($productSku['Product']['product_name'],0,25);	
							$totalUnits = $totalUnits + $newSku[0];     
                            $calculateWeight = ($newSku[0] * $productSku['ProductDesc']['weight']);
							$massWeight = $massWeight + $calculateWeight;		
							$combineCategory = $productSku['Category']['category_name'];				
						}
						else
						{
							$combineTitle .= ',' . substr($productSku['Product']['product_name'],0,25);	
							$totalUnits = $totalUnits + $newSku[0];
                            $calculateWeight = ($newSku[0] * $productSku['ProductDesc']['weight']);
							$massWeight = $massWeight + $calculateWeight;                                                        
							$combineCategory .= ',' . $productSku['Category']['category_name'];				
						}
						if( $combineSku == '' )
							$combineSku = $setNewSku;	
						else
							$combineSku .= ',' . $setNewSku;	
						
					$j++;	
					}
					$totalPecises = $totalPecises + $totalUnits;
					$massWeight = $packageWeight + $massWeight; 
					$totalMassWeightNow = $totalMassWeightNow + $massWeight;	
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$cnt, $inc );
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$cnt, 'ESL' );
					$paramsConsignee = array(
						'conditions' => array('OpenOrder.num_order_id' => $mergeOrder[0]->MergeUpdate->order_id	),
						'fields' => array('OpenOrder.num_order_id','OpenOrder.id','OpenOrder.general_info','OpenOrder.shipping_info','OpenOrder.customer_info',									                                          'OpenOrder.totals_info'));
					
					$getConsigneeDetailFromLinnworksOrder = json_decode(json_encode($this->OpenOrder->find( 'first', $paramsConsignee )),0);	
					$generalInfo	=	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->general_info);
					$shippingInfo	=	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->shipping_info);
					$customerInfo	=	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->customer_info);
					$totalsInfo		=	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->totals_info);
					$congineeInfo 	= 	unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->customer_info);					
					
					$cityInfo = $congineeInfo->Address->Town .' , '. $congineeInfo->Address->Region;
					
					$Adds = $congineeInfo->Address->Address2 .'  '. $congineeInfo->Address->Address1;
					
					
					$PostCode	=	str_pad($congineeInfo->Address->PostCode,5,"0",STR_PAD_LEFT);
					//echo "<br>";
					$totalInfo = unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->totals_info);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$cnt, str_replace(' ',$setSpaces,$congineeInfo->Address->FullName) );
					
					//$objPHPExcel->getActiveSheet()->setCellValue('D'.$cnt, str_replace(' ',$setSpaces,$congineeInfo->Address->Address1) );
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$cnt, $Adds);
					
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$cnt, $PostCode );
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$cnt, $cityInfo );
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$cnt, 'Jersey' );
					$setSpaces = '';
					$country = '';					
					//$objPHPExcel->getActiveSheet()->setCellValue('H'.$cnt, '1' ); on Recmondation of Lalit 27 02 2020
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$cnt, $totalUnits);
					//$massWeight = ($massWeight * 1000);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$cnt, ($massWeight * 1000) );
					$currencyMatter = $totalInfo->Currency;
					$globalCurrencyConversion = 1;
					
					if( $currencyMatter == "EUR" ){
						$globalCurrencyConversion = 1;
					} else {
						$globalCurrencyConversion = 1.38;
					}
					
					$totalValue = 0;
					$setPrice = $mergeOrder[0]->MergeUpdate->price;
					if( ( $setPrice * $globalCurrencyConversion) > 21.99 ){
						$totalValue = number_format($this->getrand(), 2, '.', '');
					} else {
						$totalValue = number_format(( $setPrice * $globalCurrencyConversion ), 2, '.', ''); 
					}
					
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$cnt, $totalValue );
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$cnt, $totalsInfo->PostageCost );
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$cnt, $totalValue + $totalsInfo->PostageCost );
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$cnt, $totalsInfo->Currency);
					$countPostcode = strlen($congineeInfo->Address->PostCode);
					$countChar = $countPostcode - 3;
					
					if( $countPostcode == 5 ){
						$getBagNo = substr( $congineeInfo->Address->PostCode , 0 , 2 );
					} else {
						$getBagNo = substr( $congineeInfo->Address->PostCode , 0 , 1 );
					}
					
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$cnt, $combineCategory );
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$cnt, $mergeOrder[0]->MergeUpdate->product_order_id_identify  );
					$objPHPExcel->getActiveSheet()->setCellValue('p'.$cnt, substr($congineeInfo->Address->PostCode, 0, 2 )  );
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$cnt, $congineeInfo->Address->Country  );
					$combineSku = '';$totalUnits = 0;$massWeight = 0;$combineCategory = '';
					$totalOrders++;$inc++;$cnt++;$k++;	
				}
				
				
 			$styleArray = array('font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 14) );
			
			$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('C2', '                                        LOW VALUE MANIFEST DESTINATION FRANCE');
			$objPHPExcel->getActiveSheet()->mergeCells('C2:M2');
			$objPHPExcel->getActiveSheet()->getStyle('C2:M2')->getFont()->setSize(14);	
			$objPHPExcel->getActiveSheet()->getStyle('C2:M2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('D5', '             Postcode           Address');
			$objPHPExcel->getActiveSheet()->mergeCells('D5:F5');
			$objPHPExcel->getActiveSheet()->getStyle('D5:F5')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('D5:F5')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('F3', 'DATE: '.date('d.m.Y'));
			$objPHPExcel->getActiveSheet()->mergeCells('F3:G3');
			$objPHPExcel->getActiveSheet()->getStyle('F3:G3')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('F3:G3')->applyFromArray($styleArray);
			
			// ### updated 30/09/2019 #######	
			$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Manifestabfertigung ');
			$objPHPExcel->getActiveSheet()->mergeCells('E4:F4');
			$objPHPExcel->getActiveSheet()->getStyle('E4:F4')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('E4:F4')->applyFromArray($styleArray);
		// #### End ###	
			
			$objPHPExcel->getActiveSheet()->SetCellValue('I3', 'ORIGIN:  Jersey');
			$objPHPExcel->getActiveSheet()->getStyle('I3')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('I3')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('N3', 'DESTINATION: '.$congineeInfo->Address->Country);
			$objPHPExcel->getActiveSheet()->getStyle('N3')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('N3')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Total No of Items: '. $k );
			$objPHPExcel->getActiveSheet()->getStyle('C4')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('C4')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('I4', 'TOTAL PIECES : '. $totalPecises );
			$objPHPExcel->getActiveSheet()->getStyle('I4')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('I4')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('N4', 'TOTAL WEIGHT : '.number_format($totalMassWeightNow, 3, '.', ' ').'kg');
			$objPHPExcel->getActiveSheet()->getStyle('N4')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('N4')->applyFromArray($styleArray);
			
			$objPHPExcel->setActiveSheetIndex(0);                                                                              
		
			
			$uploadUrl = WWW_ROOT .'img/cut_off/'. $folderName . '/' .$service.'.xls';                                          
			$uploadUrI = Router::url('/', true) . 'img/cut_off/'. $folderName . '/' .$service.'.xls';                                          
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
			$objWriter->save($uploadUrl);
			
 			$manifestRecord = array(
					'destination' => $delevery_country,
					'service_name' => $service_name,
					'service_provider' => $serviceProvider,
					'service_code' => $service_code,
					'order_ids' => implode(",",$orderIds),
					'date' => date('Y-m-d')
				); 
			
			$this->ManifestRecord->saveAll( $manifestRecord );
		
			echo $uploadUrI; exit;
		}
		else
		{
			echo "blank"; exit;
		}
	}
	
	public function getrand()
	   {
		   (float)$min=20.10;
		   (float)$max=21.99;
		   return ($min + ($max - $min) * (mt_rand() / mt_getrandmax()));	    	
	   }
	
	 public function updateManifestDate($merge_id)
	   {
		  $this->loadModel('MergeUpdate'); 
		  date_default_timezone_set('Europe/Jersey');
		  $firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		  $lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		  $manifest_username = $firstName.' '.$lastName;
		  $data['id'] = $merge_id;   
		  $data['manifest_date'] = date('Y-m-d H:i:s');   
		  $data['manifest_username'] 	= $manifest_username;   
		  $this->MergeUpdate->saveAll( $data);	    	
	   }
    
}
?>
