<?php
error_reporting(0);
class NewInvoiceController extends AppController
{
    var $name = "NewInvoice";
    
    var $helpers = array('Html','Form','Session');
	
	var $COST_STORE_NAME	  = 'EURACO GROUP LIMITED';//CostDroper
	var $COST_STORE_ADDRESS	  = '49 Oxford Road St Helier <br>Jersey JE2 4LJ';
	var $COST_REG_NUMBER 	  = '109464';
	
	var $RAIN_STORE_NAME	  = 'FRESHER BUSINESS LIMITED';//Rainbow
	var $RAIN_STORE_ADDRESS   = 'Beachside Business Centre<br>Rue Du Hocq St Clement<br>Jersey JE2 6LF';
	var $RAIN_REG_NUMBER 	  = '108507';
	
	var $MARE_STORE_NAME	  = 'ESL LIMITED'; //Marec
	var $MARE_STORE_ADDRESS   = 'Unit 4 Airport Cargo Centre<br>L\'avenue De La Commune St Peter<br>Jersey JE3 7BY';
	var $MARE_REG_NUMBER 	  = '118438';
    
	public function beforeFilter()
    {
	 	parent::beforeFilter();
		$this->layout = false; 
		$this->Auth->Allow(array('Generate','GenerateB2b','cronEuFBAInvoice')); 		
		$this->GlobalBarcode = $this->Components->load('Common'); 
	} 
	
	public function index()
    {
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
		$html .= '<body>'.$htmlTemplate.'</body>';
				
		$name	= 'Invoice.pdf';							
		$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
     
		echo $r = 'Юлия Свеженцева';
		echo "<br>=====";
		echo utf8_encode($r);
		echo "<br>=====";
		echo mb_convert_encoding($r, 'HTML-ENTITIES','UTF-8');
		exit;
		
	}
	
   	public function GenerateB2b($amazon_order_id = null)
    { 
				 
 		$order = $this->getOrderByNumId_openorder( $amazon_order_id );	
		
		if(is_object($order)){
					 
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();	
			
			$result 		=	'';
			$htmlTemplate	=	'';	
	
			if(strtotime($order->GeneralInfo->ReceivedDate) > strtotime('2019-09-30 23:59:59')){
				 $htmlTemplate	=	$this->getB2Bhtml($order);	
			}else{
				 $htmlTemplate	=	$this->getB2Bhtml30($order);	
			}	
			$SubSource		=	$order->GeneralInfo->SubSource;
			$ReferenceNum   =   $order->GeneralInfo->ReferenceNum;
		
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4))); 
			/**************** for tempplate *******************/
			$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				 <meta content="" name="description"/>
				 <meta content="" name="author"/>
				 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
			$html .= '<body>'.$htmlTemplate.'</body>';
			//$html =	$htmlTemplate;
			$name	= $amazon_order_id.'.pdf';							
			$dompdf->load_html(($html), Configure::read('App.encoding'));
			$dompdf->render();			
							
			$file_to_save = WWW_ROOT .'b2b_invoices/'.$name;
			 
			//save the pdf file on the server
			file_put_contents($file_to_save, $dompdf->output()); 
			  
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="'.$name.'"'); 
			header('Content-Length: ' . filesize($file_to_save));
			header("Cache-control: private"); 
			readfile($file_to_save);
			exit;
		
		}else{
			header("Location:".Router::url('/', true)."InvoiceArchive/GenerateB2b/". $amazon_order_id); 
			exit;
			//echo 'Invalid Order.';
		}		 
			
	}
	
	public function GenerateB2Cslip($amazon_order_id = '3872643')
    { 
  		$order = $this->getOrderByNumId_openorder( $amazon_order_id );	
  		$htmlTemplate = '';
		if(is_object($order)){
					 
 			$htmlTemplate	=	$this->getB2Chtml($order);		
			/*$SubSource		=	$order->GeneralInfo->SubSource;
			$ReferenceNum   =   $order->GeneralInfo->ReferenceNum;
		
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4))); 
			/**************** for tempplate *******************/
			/*$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				 <meta content="" name="description"/>
				 <meta content="" name="author"/>
				 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
			$html .= '<body>'.$htmlTemplate.'</body>';*/
			 echo $htmlTemplate;//exit;
			 
		}
 		 return $htmlTemplate;
	}
	
	public function GenerateB2c($amazon_order_id = null)
    { 
				 
 		$order = $this->getOrderByNumId_openorder( $amazon_order_id );	
		
		if(is_object($order)){
					 
			if($order->GeneralInfo->Source != 'AMAZON'){
				echo 'You can not generate VAT Invoice for <strong>'.$order->GeneralInfo->Source.'</strong>.';
				exit;
			}
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();	
			
			$result 		=	'';
			$htmlTemplate	=	'';	
	
			$htmlTemplate	=	$this->getB2Chtml($order);		
			$SubSource		=	$order->GeneralInfo->SubSource;
			$ReferenceNum   =   $order->GeneralInfo->ReferenceNum;
		
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4))); 
			/**************** for tempplate *******************/
			$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				 <meta content="" name="description"/>
				 <meta content="" name="author"/>
				 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
			$html .= '<body>'.$htmlTemplate.'</body>';
			//echo $html;
			//exit;
			$name	= $amazon_order_id.'.pdf';							
			$dompdf->load_html(($html), Configure::read('App.encoding'));
			$dompdf->render();			
							
			$file_to_save = WWW_ROOT .'b2c_invoices/'.$name;
			 
			//save the pdf file on the server
			file_put_contents($file_to_save, $dompdf->output()); 
			  
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="'.$name.'"'); 
			header('Content-Length: ' . filesize($file_to_save));
			header("Cache-control: private"); 
			readfile($file_to_save);
			exit;
		
		}else{
			header("Location:".Router::url('/', true)."InvoiceArchive/GenerateB2c/". $amazon_order_id); 
			exit;
			//echo 'Invalid Order.';
		}		 
			
	}
 
	public function getOrderByNumId_openorder($pkOrderId = null)
	{
		$this->loadModel('OpenOrder');	
		$data 	= [];
		$order 	= $this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id' => $pkOrderId)));
		
		if(count($order ) == 0)
		{
			$order 	= $this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.amazon_order_id' => $pkOrderId)));
		} 
		
		if(count($order ) > 0)
		{
			$data['NumOrderId']    = $order['OpenOrder']['num_order_id'];
			$data['GeneralInfo']   = unserialize($order['OpenOrder']['general_info']);
			$data['ShippingInfo']  = unserialize($order['OpenOrder']['shipping_info']);
			$data['CustomerInfo']  = unserialize($order['OpenOrder']['customer_info']);
			$data['TotalsInfo']    = unserialize($order['OpenOrder']['totals_info']);
			$data['Items'] 		   = unserialize($order['OpenOrder']['items']);
			$data['vat_number']    = $order['OpenOrder']['vat_number'];
			 
			if(isset($data['CustomerInfo']->BillingAddress) && count($data['CustomerInfo']->BillingAddress) > 0){
				
			}else{
				$data['CustomerInfo']->BillingAddress  = $data['CustomerInfo']->Address;
			}		 
 		} 
		return json_decode(json_encode( $data ),0);	
		
	}
		
	public function getOrderByNumId($pkOrderId = null)
	{
		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
	
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
		//$multi = AuthMethods::Multilogin($username, $password);		
		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

		$token = $auth->Token;	
		$server = $auth->Server;		
		$order	= OrdersMethods::GetOrderDetailsByNumOrderId($pkOrderId,$token, $server);
		//pr($order	);
		//exit;
		return $order;	
		
	}
 
  	 
   	
 	public function getB2Chtml($order = null){ 
	
 		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdate');
		$this->loadModel('OpenOrder');
		$this->loadModel('SourceList');	
		$this->loadModel('Country');	
		$this->loadModel('Product');	
		$this->loadModel('OrderLocation');
		$this->loadModel('PurchaseOrder');	 
		$this->loadModel('ResponsiblePersonDetail' );
		
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  	
		$saved_vat_number = $order->vat_number;  	 		
			
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		$BILLING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName));
		$SHIPPING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName));
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
 			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS  = ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode));
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ', '.ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region));
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
 				$BILLING_ADDRESS .=	$order->CustomerInfo->BillingAddress->Country ? '<br>'.$order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
 			
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS = ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode));
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ', '.ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region));
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
 				$SHIPPING_ADDRESS .=  $order->CustomerInfo->Address->Country ? '<br>'.$order->CustomerInfo->Address->Country : '';
			}			
		}		
		 
		$EmailAddress	  = $order->CustomerInfo->Address->EmailAddress;
		$exp = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = $exp[$index];
		$country_code   =	$store_country;
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$_POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 
		$ReferenceNum	  = $order->GeneralInfo->ReferenceNum; 
 			
		$ORDER_DATE	  	 = date('d-m-Y', strtotime($order->GeneralInfo->ReceivedDate));
		$INVOICE_NO	 	 = 'INV-'.$OrderId ;
		$INVOICE_DATE 	 = $ORDER_DATE;//date('d-m-Y');
 		
		$templateHtml 	  = NULL;
   
		//echo "</pre>";
		$item_lvcr_limit  = '21.9'; //EUR
		$lvcr_limit_shipping  = '150';		
		$POSTAGE = $_POSTAGE;
 		
		//echo "==TT===";
		// echo "==TT===".($lvcr_limit_shipping  - $final_total);  exit;
		
	/*	$sresult = $this->SourceList->find('first',['conditions' => ['LocationName' => $SubSource],'fields'=>['country_code']]);
		if(count( $sresult ) > 0){	
			$country_code   = $sresult['SourceList']['country_code'];
		}*/
 		if(empty($country_code) || $country_code == 'com'){
		 	$country_code = $this->getCountryCode($Country);
		}		 
 		$store_country = [];
		$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $country_code],'fields'=>['name','custom_name']]);
		if(count( $cresult ) > 0){	
			$store_country  = [$cresult['Country']['name'], $cresult['Country']['custom_name']];
		}else{
		 	$country_code = $this->getCountryCode($Country);
			$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $country_code],'fields'=>['name','custom_name']]);
			if(count( $cresult ) > 0){	
				$store_country  = [$cresult['Country']['name'], $cresult['Country']['custom_name']];
			}
		} 
		 
		
		$tax_rate = 0; $vat_amount = 0;
		 
 		$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
		if(count( $thsresult ) > 0){				
			$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
		} 
		$currency_code = $this->getCurrencyCode($Currency);
		 
		$VAT_Number = ''; $company = ''; $IOSS_Number = 'IOSS:123456789';
		if($result == 'cost' || strtolower($SubSource) == 'ebay2'){
			$company 	=  'CostBreaker'; 
			$EORI_Number = 'GB019020854000';
			$UK_VAT_Number = 'GB 304984295'; 
			$DE_VAT_Number = 'DE 321777974'; 
			$FR_VAT_Number = 'FR 59850070509'; 
			$IT_VAT_Number = 'IT 10905930961'; 
			$CZ_VAT_Number = 'CZ 684972917';  
			$ES_VAT_Number = 'ES N6061518D';

 			if(strtolower($SubSource) == 'costdropper'){
				$IOSS_Number = 'IOSS:IM2760000924';//Real DE				
			} 
			if(strtolower($SubSource) == 'cdiscount'){
				$IOSS_Number = 'IOSS:IM2500000295';
				$FR_VAT_Number = 'FR 34424059822';
			}

			$STORE_NAME	   = $this->COST_STORE_NAME;
		    $STORE_ADDRESS = $this->COST_STORE_ADDRESS; 
			$input = array();
			if($Country == 'United Kingdom'){
 				$VAT_Number   = $UK_VAT_Number;
  			}else if($Country == 'Germany'){
				$VAT_Number   = $DE_VAT_Number;
			}else if($Country == 'France'){
				$VAT_Number   = $FR_VAT_Number;
			}else if($Country == 'Italy'){
				$VAT_Number   = $IT_VAT_Number;
 			}else if($Country == 'Spain'){
				$VAT_Number   = $ES_VAT_Number;
 			}else{
 				$input = array($UK_VAT_Number,$FR_VAT_Number,$CZ_VAT_Number);
			}
			if(count($input) > 0){
 				  $rand_keys 	  = array_rand($input);
 				  $VAT_Number     = $input[$rand_keys]; 
			}
		
		}
		if($result == 'mare' || strtolower($SubSource) == 'ebay5'){
			$company 	=  'Marec';  
			$EORI_Number = 'GB019434034000';
			$UK_VAT_Number = 'GB 307817693'; 
			$DE_VAT_Number = 'DE 323086773';
			$IT_VAT_Number = 'IT 11062310963'; // 16 Dec 2019
			$FR_VAT_Number = 'FR 07879900934'; 
		
			$STORE_NAME	   = $this->MARE_STORE_NAME;
		    $STORE_ADDRESS = $this->MARE_STORE_ADDRESS;
			$input = array();
			if($Country == 'United Kingdom'){
 				$VAT_Number = $UK_VAT_Number;	
 			}else if($Country == 'Germany'){
				$VAT_Number = $DE_VAT_Number;	
			}else if($Country == 'France'){
 				$VAT_Number = $FR_VAT_Number;	
 			}else if($Country == 'Italy'){
 				$VAT_Number = $IT_VAT_Number;	
  			}else if($Country == 'Spain'){
 				$VAT_Number   = $UK_VAT_Number;
  			}else{
				$input = array($UK_VAT_Number,$DE_VAT_Number);
			}
			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}			 
		}	
 		if($result == 'rain' || strtolower($SubSource) == 'ebay0'){
 			$company 	= 'Rainbow Retail'; 
			$EORI_Number = 'GB318649182000';
			$UK_VAT_Number = 'GB 318649182'; 
			$IT_VAT_Number = 'IT 11061000961';
			$DE_VAT_Number = 'DE 327173988';
			$FR_VAT_Number = '';
		  
			$STORE_NAME	   = $this->RAIN_STORE_NAME;
		    $STORE_ADDRESS = $this->RAIN_STORE_ADDRESS;
			
			$input = array();
			if($Country == 'United Kingdom'){
 				$VAT_Number = $UK_VAT_Number;	
 			}else if($Country == 'Germany'){
 				$VAT_Number = $DE_VAT_Number;	
			}else if($Country == 'France'){
				$VAT_Number = $UK_VAT_Number;	
			}else if($Country == 'Italy' ){
 				$VAT_Number = $IT_VAT_Number;	
  			}else{
				$input = array($UK_VAT_Number);
			}
			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}
		}	
		if($Country =='United Kingdom'){
			$IOSS_Number = 'OrderId:'.$OrderId.'-1';
		}
		
		 
		if(empty($saved_vat_number)){
			$this->OpenOrder->updateAll(array('vat_number' => "'".$VAT_Number."'"), array('num_order_id' => $OrderId));
	 	}else{
			$VAT_Number = $saved_vat_number;
		}
		
		if(in_array(strtolower($SubSource),['ebay0','ebay2','ebay5'])){
			echo $TOTAL;
			echo '==';$tot = ($TOTAL * $tax_rate / 100);
			echo $TOTAL = $TOTAL + $tot;
		}
		
		$txt = 'Taxable Supply'; 
		 
		$templateHtml  ='<head><meta charset="UTF-8"><title>Invoice</title></head>';
		$templateHtml  .='<style>*{font-family:Verdana; font-size:12px;}</style>';
 		$templateHtml  .='<table border="0" width="750px"  align="left" cellpadding="0" cellspacing="0" style="margin:2px;">';
    	 $templateHtml  .='<tr>';
			$templateHtml  .='<td style="font-size:14px; font-weight:bold; padding:3px 2px; text-align:left" >Slip/Invoice </td>';
			$templateHtml  .='<td style="font-size:14px; font-weight:bold; padding:3px 2px; text-align:right">'.$IOSS_Number.'&nbsp;&nbsp;&nbsp;&nbsp;</td>';
		$templateHtml  .='  </tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td valign="middle" align="left" width="50%">';
				$templateHtml  .='<p>'.$BILLING_ADDRESS_NAME.'<br>'.$BILLING_ADDRESS.'</p>';
				//$templateHtml  .='<p>'.$BILLING_ADDRESS.'</p>';
				/*if($VAT_Number){
					$templateHtml  .='<p><strong>'.$VAT_Number.'</strong></p>';
				}*/
 			$templateHtml  .='</td>';
			$templateHtml  .='<td valign="top" align="center" style="padding:4px;background:#dfeced;">';//background:#dfeced; 
			  $templateHtml  .='<div  style="padding:1px;">';//border:1px solid #bdc9c9; 
				$templateHtml  .='<table>';
				  
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2" style="font-size:11px ;font-weight:bold;"> Paid</td>';
					 $templateHtml  .='</tr>';
					$templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2">';
						$templateHtml  .='Sold by <strong> '.$STORE_NAME.'</strong>';
						if($VAT_Number){
							$templateHtml  .='<br>VAT #<strong>'.$VAT_Number.'</strong>'; 
						}
 						 
						$templateHtml  .='</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2" width="100%" height="1px" style="width:100%; height:1px; background:#bdc9c9;"> </td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong>Invoice date / Delivery date</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_DATE.'</td>';
					 $templateHtml  .='</tr>';
					  $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong>Invoice #</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_NO.'</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong>Total payable</strong></td>';
						$templateHtml  .='<td> '.$currency_code.$TOTAL.'</td>';
					 $templateHtml  .='</tr>'; 
				  
			   $templateHtml  .='</table>';
			   $templateHtml  .='</div>';
			   
			$templateHtml  .='</td>';
		 $templateHtml  .='</tr>';
		 
		 if($exp[$index] == 'fr'){
			 $amazon_url = 'www.amazon.fr';
		 }else if($exp[$index] == 'es'){
			 $amazon_url = 'www.amazon.es';
		 }else if($exp[$index] == 'it'){
			 $amazon_url = 'www.amazon.it';
		 }else if($exp[$index] == 'de'){
			 $amazon_url = 'www.amazon.de';
		 }else{
			 $amazon_url = 'www.amazon.co.uk';
		 }
		
		 if($order->GeneralInfo->Source == 'AMAZON'){
			 $templateHtml  .='<tr>';		 
				$templateHtml  .='<td colspan="2" style="padding:2px 0px;"> For questions about your order, visit '.$amazon_url.'/contact-us</td>';
			 $templateHtml  .='</tr>';
		 }
		 
 		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="padding:2px 0px;"><strong>Tranction Type : </strong>'.$txt.' <strong>&nbsp;&nbsp; Order Ref# &nbsp; </strong>'.$ReferenceNum.' <strong>Order Id#</strong>&nbsp;'.$OrderId.'</td>';
		 $templateHtml  .='</tr>';
		 
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="background:#ddd; height:1px;"> </td>';
		 $templateHtml  .='</tr>';
		  
		 
		 $templateHtml  .='<tr><td colspan="2">';
		 $templateHtml  .='<table border="0" align="center" cellpadding="0" cellspacing="0" >';

			
				$templateHtml  .='<tr>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">BILLING ADDRESS </th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">DELIVERY ADDRESS</th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">SOLD BY </th>';
			   $templateHtml  .='</tr>'; 
			   
			  $templateHtml  .='<tr>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<strong>'.$BILLING_ADDRESS_NAME.'</strong><br>'.$BILLING_ADDRESS; 
				  $templateHtml  .='</td>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<strong>'.$SHIPPING_ADDRESS_NAME.'</strong><br>'.$SHIPPING_ADDRESS;
				  $templateHtml  .='</td>';
				 $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<strong>'.$STORE_NAME.'</strong><br>'.$STORE_ADDRESS;
					 if($VAT_Number){
						 $templateHtml  .='<br><strong>'.$VAT_Number.'</strong>';
					 }
					  
				  $templateHtml  .='</td>'; 
			 $templateHtml  .='</tr>';
			/*$templateHtml  .='<tr><td colspan="3" style="background:#ddd; height:1px;"> </td></tr>';
			$templateHtml  .='<tr><td colspan="3"><strong>Order information</strong></td></tr>';
			$templateHtml  .='<tr><td colspan="2">Order date '.$ORDER_DATE.'</td><td>Order # '.$ReferenceNum.'</td></tr>';
 		  */
		 $templateHtml  .='</table>';
		 $templateHtml  .='</td></tr>'; 
		$templateHtml  .='</table>';
 		
 		$templateHtml  .='<table border="0" width="750px" cellpadding="1" cellspacing="0" align="left" style="margin:2px;">';
 		  		  
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="8" style="font-size:16px; font-weight:bold; border-top:1px solid #ddd; border-bottom:1px solid #ddd; padding:1px 3px;"> Invoice Details</td>';
		 $templateHtml  .='</tr>';
		 
		 
		  $templateHtml  .='<tr>';
			$templateHtml  .='<th valign="top" width="30%"> Description </th>';
			$templateHtml  .='<th valign="top" width="15%"> Bin Location </th>';
			$templateHtml  .='<th valign="top" width="15%"> SKU </th>';
			$templateHtml  .='<th valign="top" width="8%"> Qty </th>';
			$templateHtml  .='<th valign="top"   align="right" width="8%">Unit price<br>(excl.VAT)</th>';
			$templateHtml  .='<th valign="top"   align="right" width="8%">VAT rate</th>';
			$templateHtml  .='<th valign="top"   align="right" width="8%">Unit price<br>(incl.VAT)</th>';
			$templateHtml  .='<th valign="top"   align="right" width="8%">Subtotal<br>(incl.VAT)</th>';
		 $templateHtml  .='</tr>';
		$total_item_vat_amount = 0;
		$total_item_cost 	   = 0;
 		$shipping_per_order = 0;   
 		
		$spd =	$this->MergeUpdate->find('all', array('conditions' => array( 'MergeUpdate.order_id' => $OrderId )));
		if(count($spd) > 0){
			$_total = $TOTAL / count($spd);
			$shipping_per_order = $POSTAGE/count($spd);
		}else{
			$_total = $TOTAL;
			$shipping_per_order = $POSTAGE;
		}
		if(empty($split_order_id)){
			$split_order_id = $OrderId.'-1';
		}	
  			
		$splitOrderDetail = $this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $split_order_id )));
		$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
 		$po_total =0;
		foreach( $skus as $_sku )
		{
			$newSku 	= explode( 'XS-', $_sku);  
			$getsku 	= 'S-'.$newSku[1];
			$qty 		= $newSku[0];
			$pos 		= $this->PurchaseOrder->find( 'first', array( 'conditions' => array('purchase_sku' => $getsku,'price >'=> 0 ),'order'=> 'id desc' ) );
			if(count($pos) > 0){
				$po_total += ($pos['PurchaseOrder']['price']*$qty);
			}  
				 
		}
		//echo ' po_total '.$po_total .'=='.  $splitOrderDetail['MergeUpdate']['price'];
		if($po_total == 0){
			$po_total = $splitOrderDetail['MergeUpdate']['price'];
 		}
		
		$total_vat_excl = 0;
		foreach( $skus as $_sku )
		{
				$newSku = explode( 'XS-', $_sku); 
				 
				$totalvalue = $total;
				$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
				$getsku 	= 'S-'.$newSku[1];
				$qty 		= $newSku[0];
				$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
				$local_barcode = $getOrderDetail['ProductDesc']['barcode'];
				//echo '=='.$this->GlobalBarcode->getLocalBarcode('4286819746992');
				$pos = $this->PurchaseOrder->find( 'first', array( 'conditions' => array('purchase_sku' => $getsku,'price >'=> 0 ),'order'=> 'id desc' ) );
				if(count($pos) > 0){
					$item_price = $pos['PurchaseOrder']['price']; 
				}else{
					$item_price = $splitOrderDetail['MergeUpdate']['price']/$splitOrderDetail['MergeUpdate']['quantity'];
				}
				
 				$catName     = $getOrderDetail['Product']['category_name'];
				$Title       = substr($getOrderDetail['Product']['product_name'],0,20).'...';
				$bin_loc = [];
				$bins = $this->OrderLocation->find( 'all', array( 'conditions' => array('sku' => $getsku,'picklist_inc_id'=>$splitOrderDetail['MergeUpdate']['picklist_inc_id'] ) ) ); 
				if(count($bins) > 0){
					foreach($bins as $bin){ 
						$bin_loc[$bin['OrderLocation']['bin_location']] = $bin['OrderLocation']['bin_location'];
					}
				}
		 
				 $_item_cost = ($TOTAL / count($skus));				  
				 $PricePerUnit = ($TOTAL / $po_total)*$item_price;
				 
				 // $PricePerUnit =($_item_cost / $qty);
				 //($_item_cost / $qty);
				 $item_vat_amount = $PricePerUnit - ($PricePerUnit/(1 + ($tax_rate/100)));
				 $item_cost = $PricePerUnit - $item_vat_amount;
				 $total_vat_excl += ($item_cost * $qty) ;
				 $total_item_vat_amount  +=($item_vat_amount * $qty);
				 $total_item_cost  += $PricePerUnit * $qty;
		
				// $templateHtml  .='<tr><td>'.$Title.'</td><td>'. implode( ",",$bin_loc) .'</td><td>'.$getsku.'</td><td>'.$qty.'</td><td>'.$currency_code.'&nbsp;'.number_format($item_cost,2).'</td><td>'.$tax_rate.'%</td><td>'.$currency_code.'&nbsp;'.number_format($PricePerUnit,2).'</td><td>'.$currency_code.'&nbsp;'.number_format(($PricePerUnit *$qty),2).'</td></tr>';
		
			  
				$templateHtml  .='<tr>';
					$templateHtml  .='<td style="border:1px solid #ccc">'.$Title.'</td>';
					$templateHtml  .='<td style="border:1px solid #ccc">'.implode( ",",$bin_loc).'</td>';
					$templateHtml  .='<td style="border:1px solid #ccc">'.$getsku.'</td>';						
					$templateHtml  .='<td style="border:1px solid #ccc">'.$qty.'</td>';
					$templateHtml  .='<td style="border:1px solid #ccc">'.$currency_code.'&nbsp;'.number_format($item_cost,2).'</td>';
					$templateHtml  .='<td style="border:1px solid #ccc">'.$tax_rate.'%</td>';
					$templateHtml  .='<td style="border:1px solid #ccc">'.$currency_code.'&nbsp;'.number_format($PricePerUnit,2).'</td>';
					$templateHtml  .='<td style="border:1px solid #ccc">'.$currency_code.'&nbsp;'.number_format(($PricePerUnit *$qty),2).'</td>';
				 $templateHtml  .='</tr>'; 
			 
					 
					 
				 
		}
		
		/* foreach($items as $count => $item){ 
			 
			if(strlen($item->Title) > 10){
				$Title = $item->Title; 
			}else{
				$Title = $item->ChannelTitle;
			} 
			 
			if($shipping_per_item > 0){
				$shipping_per_item = ($shipping_per_item / $item->Quantity);	
			}
			$PricePerUnit = $item->PricePerUnit - $shipping_per_item;
			 
			 $item_vat_amount = $PricePerUnit - ($PricePerUnit/(1 + ($tax_rate/100)));
			 $item_cost = $PricePerUnit - $item_vat_amount;
			
			 $total_item_vat_amount  +=$item_vat_amount;
			 $total_item_cost  += $item_cost * $item->Quantity;
			  
			$templateHtml  .='<tr style="background:#ddd;">';
				$templateHtml  .='<td style="padding:5px 2px;">'.$Title.'</td>';
				$templateHtml  .='<td style="padding:5px 2px;">'.$item->Quantity.'</td>';
				$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format($item_cost,2).'</td>';
				$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$tax_rate.'%</td>';
				$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format($PricePerUnit,2).'</td>';
				$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format(($item->CostIncTax - 
				($shipping_per_item *$item->Quantity)),2).'</td>';
			 $templateHtml  .='</tr>';
			 
		 } */
		
		 $postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
		 $postage_cost = $POSTAGE - $postage_vat_amount;
	 	 
		   $templateHtml  .='<tr style="background:#ddd;">';

			$templateHtml  .='<td style="padding:5px 2px;">Shipping Charge</td>';
			$templateHtml  .='<td style="padding:5px 2px;"></td>';
			$templateHtml  .='<td style="padding:5px 2px;"></td>';
			$templateHtml  .='<td style="padding:5px 2px;"></td>';
			$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format($postage_cost,2).' </td>';
			$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
			$templateHtml  .='<td style="padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
		 $templateHtml  .='</tr>';
		
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="5"></td>';
			$templateHtml  .='<td colspan="2" style="font-size:12px; font-weight:bold; padding:10px 0px;">Invoice total</td>';
			$templateHtml  .='<td style="font-size:14px; font-weight:bold; padding:10px 0px;" align="right">'.$currency_code.'&nbsp;'.number_format($TOTAL,2).'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="4"></td>';
			$templateHtml  .='<td colspan="4" style="height:1px; background:#ddd;"></td>';
		 $templateHtml  .='</tr>';
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<th colspan="6" align="right"> VAT rate </th>';
			$templateHtml  .='<th  align="right">Item subtotal (excl. VAT)</th>';
			$templateHtml  .='<th   align="right">VAT subtotal</th>';
		 $templateHtml  .='</tr>';
		$templateHtml  .='<tr>';
 			$templateHtml  .='<td colspan="5"></td>';
 			$templateHtml  .='<td style="background:#ddd;padding:5px 2px"> '.$tax_rate.'%</td>';
			$templateHtml  .='<td style="background:#ddd;padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_vat_excl + $postage_cost),2).'</td>';
			$templateHtml  .='<td style="background:#ddd;padding:5px 2px;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_vat_amount + $postage_vat_amount),2).'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
 			$templateHtml  .='<td colspan="5"></td>';
 			$templateHtml  .='<td style="padding:5px 2px; font-weight:bold;">Total</td>';
			$templateHtml  .='<td style="padding:5px 2px; font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format((($total_item_cost - $total_item_vat_amount) + $postage_cost),2).'</td>';
			$templateHtml  .='<td style="padding:5px 2px;  font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_vat_amount + $postage_vat_amount),2).'</td>';
		 $templateHtml  .='</tr>';  
 		
 		
		/*--------------Ink------------------*/
		$this->loadMOdel( 'InkOrderLocation' );	 
		$order_ink	    = $this->InkOrderLocation->find('first', array('conditions' => array('order_id' => $OrderId,'split_order_id'=>$OrderId.'-1','status'=>'active')));		
		
		
		if(count($order_ink) > 0 ){			 
		//	$templateHtml .=  '<tr><td colspan="8"><div style="color:green;font-size:12px;"><center>#2 Envelope Required.</center></div><br><img src="'.WWW_ROOT.'img/ink_xl2.jpg" height="100"></td></tr>';
		} /*---------------EC MARK---------------*/
				
 		$person = [];  
		if($Country != 'United Kingdom' ){
			$get_sp_order = $this->MergeUpdate->find('first', array('conditions' => array('order_id' => $OrderId),'fields'=>['sku']));
			if(count( $get_sp_order ) > 0){				
 				$skus = explode( ',', $get_sp_order['MergeUpdate']['sku']);
				foreach($skus as $k => $v){
					$x = explode('XS-', $v); 
					$_sku = 'S-'.$x[1];
					$getRes	= $this->ResponsiblePersonDetail->find('first', array('conditions' => array('sku' => $_sku, 'store_name' => $company)));
					if(count($getRes) > 0){
						$person[$getRes['ResponsiblePersonDetail']['brand_name']] = $getRes['ResponsiblePersonDetail']['person_information'];
					}
					 
				}
			}		
		}
		
		if(count($person) > 0){
 			$person_info = ''; $b = 0;
			foreach($person as $brand => $pinfo){
				if(($b > 1) && ($b % 2 == 0)){
					$person_info .= '<br>';
				}
				$person_info .= $pinfo;
				$b++;
			}
			 
			$templateHtml .= '<tr><td colspan="8"><div style="font-size:11px; border:1px solid; text-align:left;padding:5px;">This product is CE marked products in accordance with European health, safety, and environmental protection standards for products sold within the European Economic Area (EEA).Authorised representative European Union:'. ($person_info).'</div></td></tr>';
 			 
		}
		
		
		$templateHtml  .='</table>';echo $templateHtml;
		return $templateHtml;	
	
	
	}
 
	public function dateFormat($date = null, $country = null) 
	{
		$date = '2019-03-05';
		$country = 'fr';
		if($country == 'fr'){
		
 				$order_date = date('D. M d Y', strtotime($date));
				$english_days = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
				$french_days = array('lun', 'mar', 'mer', 'jeu', 'ven', 'sam', 'dim');
				//$french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
				$english_months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
				//$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
				$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
				
			
		echo	str_replace($english_months, $french_months, str_replace($english_days, $french_days, $order_date )  );
			
 		}
		
	
		//return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date) ) ) );
	}
	
	public function getStoreData($SubSource = null, $country = null){
		$store_name = '';
		$store = strtolower(substr($SubSource, 0, 4));
		$europe = array('uk','es','it','fr','de');
		if($store == 'cost' && in_array($country,$europe)){
			$store_name = 'Cost-Dropper';
		}else if($store == 'mare' && in_array($country,$europe)){
			$store_name = 'EbuyerExpress';
		}else if($store == 'rain' && in_array($country,$europe)){
			$store_name = 'RRRetail';
		}else if($store == 'bbd_' && in_array($country,$europe)){
			$store_name = 'BBDEU';
		}else if($store == 'cost' && $country == 'ca'){
			$store_name = 'CanadaEmporium';
		}else if($store == 'cost' && $country == 'com'){
			$store_name = 'USABuyer';
		}
		return $store_name;
	}
	
 	
	public function Address(){
	
		$this->loadModel('InvoiceAddress');

		$data['order_id'] 	= $this->request->data['order_id'];
		$field 				= $this->request->data['field'];
		$data[$field] 		= $this->request->data['val'];
				
		if($this->Auth->user('username')){
			$data['username']  = $this->Auth->user('username');
		}
		
		$conditions = array('InvoiceAddress.order_id' => $this->request->data['order_id']);
														
		if ($this->InvoiceAddress->hasAny($conditions)){
			foreach($data as $field => $val){
				$DataUpdate[$field] = "'".$val."'";								
			}
			$this->InvoiceAddress->updateAll( $DataUpdate, $conditions );
			$msg['msg'] = 'updated';
		}else{
			$this->InvoiceAddress->saveAll($data);
			$msg['msg'] = 'saved';			
		}
	echo json_encode($msg);
	exit;
	
	}
	
	public function getCountryCode($Country = null){
		$code = '';
		if($Country == 'United Kingdom'){
			$code = 'gb';
		}else if($Country == 'Germany'){
			$code = 'de';
		}else if($Country == 'France'){
			$code = 'fr';
		}else if($Country == 'Italy'){
			$code = 'it';
		}else if($Country == 'Spain'){
			$code = 'es';
		}else if($Country == 'Australia'){
			$code = 'au';
		}else if($Country == 'Canada'){
			$code = 'ca';
		}else if($Country == 'United States'){
			$code = 'us';
		}
		return $code;
	}
	
	public function getCurrencyCode($currency  = null){
		$code = $currency;
		if($currency  == 'GBP'){
			$code = '&pound;';
		}else if($currency  == 'EUR'){
			$code = '&euro;';
		}else if($currency  == 'USD'){
			$code = '$';
		}else if($currency  == 'CAD'){
			$code = 'CAD$';
		} 
		return $code;
	}
 
	public function replaceFrenchChar($string = null){
			
		$unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E','Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Nº'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U','Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c','è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n','nº'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o','ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y','ü'=>'u','º'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}	
 
  	
}

?>

