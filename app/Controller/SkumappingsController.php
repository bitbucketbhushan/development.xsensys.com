<?php
ini_set('max_execution_time', '30'); 
set_time_limit(30);
error_reporting(0);
class SkumappingsController extends AppController
{
    
    var $name = "Skumappings";
    
    var $components = array('Session','Upload','Common','Paginator');
    
    var $helpers = array('Html','Form','Session','Common' , 'Soap' , 'Paginator');
    
    public function beforeFilter()
	{
	    parent::beforeFilter();
	    $this->layout = false;
	    $this->Auth->Allow(array('updateSkuPerformance','getSkusFromAws','getFeed','UpdateStockCron','getSkusFromAwsFba'));
		$this->GlobalBarcode = $this->Components->load('Common'); 
	}
    
    public function index() 
    {	
		$this->layout = "skumapping"; 		
		$this->loadModel('Skumapping');	
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Store');			
		$skus 		  = array();
		$mappped_sku  = array();
		$channel_min  = array();
		
		
		$this->Product->unbindModel( array( 'hasMany' => array( 'ProductLocation' ) ) );	
		$this->Store->unbindModel(array('belongsTo' => array('Company')), true);		
		$getStores 	= $this->Store->find('all', array( 'conditions' => array('Store.status' => 1), 'order' => 'Store.store_name ASC' ));
		$this->set('all_stores', $getStores );
	
		$perPage	=	5;//Configure::read( 'perpage' );	
		
		$this->paginate = array(
					  'limit' => $perPage,	
					  'order' => 'Product.current_stock_level DESC',			
                      'fields' => array('Product.product_sku','ProductDesc.barcode', 'Product.id', 'Product.product_name','Product.current_stock_level'),	
         );		
		$all_product = $this->paginate('Product');		
		 
		
		foreach($all_product as $val){
			$skus[] = $val['Product']['product_sku']; 
		}
		$ch =	'';
		if(isset($_REQUEST['ch']) && ($_REQUEST['ch'] != 'all')){
			 $ch =	"channel_name LIKE '".trim($_REQUEST['ch'])."%'";
		}
		
		if(count($skus) > 0){
			if(count($skus) == 1){
				$conditions 	=  array( 'Skumapping.sku' =>  $skus[0]);
			}else{			
				$conditions 	=  array( 'Skumapping.sku IN' =>  $skus, $ch);
				
			}	
			/************************Get Mapped Skus**********************************/			
				$map 	= $this->Skumapping->find('all', array('conditions' => $conditions));
				foreach($map as $val){
					$mappped_sku[$val['Skumapping']['sku']][] = array('id' => $val['Skumapping']['id'],
															'barcode' => $val['Skumapping']['barcode'],
															'asin' => $val['Skumapping']['asin'],
															'channel_name' => $val['Skumapping']['channel_name'],
															'channel_sku' => $val['Skumapping']['channel_sku'],
															'channel_sku_title' => $val['Skumapping']['channel_sku_title'],
															'qty_percent' => $val['Skumapping']['qty_percent'],
															'buffer' => $val['Skumapping']['buffer'],
															'performance_status' =>$val['Skumapping']['performance_status'],
															'min_qty' => $val['Skumapping']['min_qty'],
															'max_qty' => $val['Skumapping']['max_qty'],
															'amazon_status' => $val['Skumapping']['amazon_status'],
															'xsensys_status' => $val['Skumapping']['xsensys_status']); 
															
					if(isset($_REQUEST['ch']) && ($_REQUEST['ch'] != 'all')){
						$channel_min = array('qty_percent' => $val['Skumapping']['qty_percent'],
											 'buffer' => $val['Skumapping']['buffer'],
											 'min_qty' => $val['Skumapping']['min_qty'],
											 'max_qty' => $val['Skumapping']['max_qty']
											);
											
					}				
				}
			/************************End Get Mapped Skus**********************************/						
		}
		
		
			
		 
		
		$this->set(compact('all_product','mappped_sku','channel_min'));
		
		
		//$this->set('mappped_sku', $skuArray );		
		//$this->set('sku_percent', $percentArray );
		//$this->set('SkuOpenOrder', $this->getOpenOrderSkuQty() );
		$this->render( 'index' );		
		/*$OpenOrderQty = ;
		foreach($OpenOrderQty as $val){
			$percentArray[$val['SkuPercentRecord']['sku']][$val['SkuPercentRecord']['store_name']] = $val['SkuPercentRecord']['buffer'];
		}*/
		
		
	}
	
	public function maps() 
    {	
		$this->layout = "skumapping"; 		
		$this->loadModel('Skumapping');	
  		$this->loadModel('SourceComany');	 
		$this->loadModel('ProChannel');
		$channelArray = []; 
 		$chs 	= $this->ProChannel->find('all', array('fields' => array('channel_type','source','channel_title')));		
		 
		if(count($chs)>0){
 			foreach($chs as $ch){
				$channelArray[$ch['ProChannel']['channel_title']] = ['source'=>$ch['ProChannel']['source'],'channel_type' => $ch['ProChannel']['channel_type']];
 			}
		} 
		
 		$getStores 	= $this->SourceComany->find('all', array(  'order' => 'channel_title ASC' ));
		$this->set('all_stores', $getStores );
	
		$perPage	=	50;
		$conditions = ['listing_status !=' => 'deleted'];
		if(isset($_REQUEST['searchkey']) && ($_REQUEST['searchkey'] != '')){
		  $conditions = ['OR'=>['sku LIKE'=>trim($_REQUEST['searchkey']).'%','asin LIKE'=>trim($_REQUEST['searchkey']).'%','channel_sku LIKE'=>trim($_REQUEST['searchkey']).'%']];
		}
		
		if(isset($_REQUEST['ch']) && ($_REQUEST['ch'] != 'all')){
 			 $this->paginate = array(
					  'conditions' => ['channel_name LIKE'=>trim($_REQUEST['ch']).'%',$conditions],	
					  'limit' => $perPage,	
					  'order' => 'Skumapping.added_date DESC',			
                       
        	 );	
		}else{
		
			$this->paginate = array(
						  'conditions' => [$conditions],	
						  'limit' => $perPage,	
						  'order' => 'Skumapping.added_date DESC',			
						   
			 );		
		 }
		$all_product = $this->paginate('Skumapping');		
		 
  		$this->set(compact('all_product','channelArray'));
 		
	}
    public function Savesku(){
		$this->loadModel( 'Product' );
		$this->loadModel( 'Skumapping' );
		$dataArray = array();
		foreach($this->request->data['values'] as $val){
			$data = explode("=",$val);
			$dataArray[$data[0]] = $data[1];			
		} 
		if($this->request->data['source'] == ''){
			$msg['msg'] = 'Please select a source!';
		}if($this->request->data['sub_source'] == 'all'){
			$msg['msg'] = 'Please select a sub source!';
		}elseif($dataArray['sku'] == ''){
			$msg['msg'] = 'SKU Value is Empty!';
		}elseif($dataArray['asin'] == ''){
			$msg['msg'] = 'ASIN Value is Empty!';
		}else{
		
			$prod = $this->Product->find('first', array('conditions' => array('product_sku' => $dataArray['sku'])));
			if(count($prod) > 0){
			//	$get = $this->Skumapping->find('first', array('conditions' => array('channel_sku' => $dataArray['channel_sku'])));
				$get = $this->Skumapping->find('first', array('conditions' => array('channel_name' => $this->request->data['sub_source'],'asin' => $dataArray['asin'])));
				if(count($get) > 0 ){
					//$msg['msg'] = $dataArray['sku'].' with ' . $dataArray['channel_sku'] . ' Already Exists!';
					$msg['msg'] =  'ASIN "'.$dataArray['asin'].'" already mapped with '. $this->request->data['sub_source']. '.';
					$msg['status'] = 'error';
				}else{			
			
					$dataArray['channel_sku']   = $this->getAutoChannel($this->request->data['sub_source']);
 					$dataArray['source'] 	   = $this->request->data['source']; 
					//$dataArray['channel_type'] = $this->request->data['channel_type']; 
					$dataArray['channel_name'] = $this->request->data['sub_source'];
 					$dataArray['listing_status'] = $this->request->data['listing_status']; 
					$dataArray['stock_status'] = $this->request->data['stock_status'];
					
					$dataArray['added_date']   = date("Y-m-d H:i:s");
					$dataArray['username']     = $this->Session->read('Auth.User.username');	
					$this->Skumapping->saveAll($dataArray);	
					//pr($dataArray);//$dataArray['id'] = $this->Skumapping->getLastInsertId();  
					$msg['msg'] = 'SKU added successfully.';
					$msg['status'] = 'ok';
				}
			}else{
				$msg['msg'] = 'SKU "'.$dataArray['sku'].'" not found. Please add sku before mapping.';
				$msg['status'] = 'error';
			}
		}		
		echo json_encode($msg);
		exit;
	}
	
	public function getAutoChannel($channel_title){
		$channels = array();
		$this->loadModel('ProChannel');	
 		$result = $this->ProChannel->find('first', array('conditions' => array('channel_title' => $channel_title),'fields' => ['ch_id','prefix','increment_id']));						
 		$channel_sku = strtoupper($result['ProChannel']['prefix'].$result['ProChannel']['increment_id']);
 		$usql = "UPDATE `pro_channels` SET `increment_id` = increment_id + 1 WHERE `ch_id` = '".$result['ProChannel']['ch_id']."'";
		$this->ProChannel->query($usql);	
		return	$channel_sku;
	}
	
    public function Prefilled(){
		$this->loadModel( 'ProChannel' );
		$dataArray = [];
  		if($this->request->data['channel'] != ''){
			$get = $this->ProChannel->find('first', array('conditions' => array('channel_title' => $this->request->data['channel'])));
			if(count($get) > 0 ){
 				$dataArray['channel_type'] = $get['ProChannel']['channel_type']; 
				$dataArray['prefix'] =  $get['ProChannel']['prefix']; 
  			} 
		}		
		echo json_encode($dataArray);
		exit;
	}
	
	public function Mapsku(){
	
		$this->loadModel( 'Skumapping' );
		$dataArray = array();
		foreach($this->request->data['values'] as $val){
			$data = explode("=",$val);
			$dataArray[$data[0]] = $data[1];			
		}
		if($this->request->data['channel'] == 'all'){
			$msg['msg'] = 'Please select a channel!';
		}elseif($dataArray['sku'] == ''){
			$msg['msg'] = 'SKU Value is Empty!';
		}elseif($dataArray['asin'] == ''){
			$msg['msg'] = 'ASIN Value is Empty!';
		}elseif($dataArray['channel_sku'] == ''){
			$msg['msg'] = 'Channel SKU is Empty!';
		}else{
			$get = $this->Skumapping->find('first', array('conditions' => array('sku' => $dataArray['sku'],'channel_sku' => $dataArray['channel_sku'])));
			if(count($get) > 0 ){
				$msg['msg'] = $dataArray['sku'].' WITH ' . $dataArray['channel_sku'] . ' Already Exists!';
			}else{			
				$dataArray['channel_name'] = $this->request->data['channel']; 
				$dataArray['added_date']   = date("Y-m-d H:i:s");
				$dataArray['username']     = $this->Session->read('Auth.User.username');	
				$this->Skumapping->saveAll($dataArray);	
				$dataArray['id'] = $this->Skumapping->getLastInsertId(); 
				
				 
				$msg['row'] = '<div class="col-lg-12" id="r_'.$dataArray['id'].'">';
										
				$msg['row'] .= '<div class="col-sm-1"><section><span class="xedit" pk="'.$dataArray['id'].'" data-name="asin" data-title="Asin" data-value="'.$dataArray['asin'].'">'.$dataArray['asin'].'</span></section></div>';
				$msg['row'] .= '<div class="col-sm-2">'.$dataArray['channel_name'].'</div>';
				
				$msg['row'] .= '<div class="col-sm-2"><section><span class="xedit" pk="'.$dataArray['id'].'" data-name="channel_sku" data-title="Channel SKU" data-value="'.$dataArray['channel_sku'].'">'.$dataArray['channel_sku'].'</span></section></div>';
				
				$msg['row'] .= '<div class="col-sm-2"><section><span class="xedit" pk="'.$dataArray['id'].'" data-name="channel_sku_title" data-title="Channel SKU Title" data-value="'.$dataArray['channel_sku_title'].'">'.$dataArray['channel_sku_title'].'</span></section></div>';
				
				$msg['row'] .= '<div class="col-sm-2">';
				$msg['row'] .= '<div class="w10" style="width:25%; float: left;">											
				<section><span class="xedit" pk="'.$dataArray['id'].'" data-name="qty_percent" data-title="Qty Percent" data-value="'.$dataArray['qty_percent'].'">'.$dataArray['qty_percent'].'</span></section></div>';
				$qtyToReport = 0;
																
				$msg['row'] .= '<div class="" style="width:25%; float: left;">'.$qtyToReport .'</div>';
				
				$msg['row'] .= '<div class="" style="width:25%; float: left;"><section><span class="xedit" pk="'.$dataArray['id'].'" data-name="min_qty" data-title="Min Qty" data-value="'.$dataArray['min_qty'].'">'.$dataArray['min_qty'].'</span></section></div>';
				$msg['row'] .= '<div class="" style="width:25%; float: left;"><section><span class="xedit" pk="'.$dataArray['id'].'" data-name="max_qty" data-title="Max Qty" data-value="'.$dataArray['max_qty'].'">'.$dataArray['max_qty'].'</span></section></div>';
						
				$msg['row'] .= '</div>';
											
				$msg['row'] .=  '<div class="col-sm-1"><section><span class="xedit" pk="'.$dataArray['id'].'" data-name="amazon_status" data-title="Amazon Status">0</span></section></div>';
														
				$msg['row'] .=  '<div class="col-sm-1"><section><span class="xedit" pk="'.$dataArray['id'].'" data-name="xsensys_status" data-title="Xsensys Status">0</span></section></div>';
				$msg['row'] .=  '<div class="col-sm-1"><i class="glyphicon glyphicon-remove delete" onclick="delCh('.$dataArray['id'].');"></i></div>';
				
				$msg['row'] .= '</div>	';
											
											
				
			}
		}		
		echo json_encode($msg);
		exit;
	}
	public function UpdateQty() 
    {	
		if($this->request->data['channel'] == 'all'){
			$msg['msg'] = 'Please select a channel!';
		}elseif($this->request->data['qty_percent'] == ''){
			$msg['msg'] = 'qty_percent is Empty!';
		}else{
			$this->loadModel( 'Skumapping' );	
			$conditions = array('Skumapping.channel_name' => $this->request->data['channel']);			
			$vals['qty_percent'] = $this->request->data['qty_percent'];
			$vals['min_qty'] = $this->request->data['min_qty'];
			$vals['max_qty'] = $this->request->data['max_qty'];
			$vals['buffer']  = $this->request->data['buffer'];
			$this->Skumapping->updateAll( $vals, $conditions );	
			$msg['msg'] = 'done!';
		}
		
			echo json_encode($msg);
		exit;
	}
	
	
	
	public function Uploadcsv($id = null) 
    {	
			$this->loadModel( 'Skumapping' );		
			$pathinfo = pathinfo($_FILES["upload"]["name"]);
            if (array_key_exists('filename', $pathinfo))
                $fileName  = $pathinfo['filename'];
            if (array_key_exists('extension', $pathinfo))
                $extension = strtolower($pathinfo['extension']);
								
			$output_dir = WWW_ROOT. 'files'.DS.'mapping'.DS; 
			$msg = array();			
			 if($extension == 'csv'){
				$insert_str	= '';
				$already    = '';
				$al_count   = 0;
				$ins_count  = 0;
				$rowcount   = 0;
				$u          = 0;
				$csvFile	= $_FILES["upload"]["tmp_name"];	
				$handle 	= fopen($csvFile, 'r');
				$cols   	= array_flip(fgetcsv($handle));				
				$chSkus		= array();
				$del_chsku 	= array();
				$user		= $this->Session->read('Auth.User.username');
				
				$channelSku = $this->Skumapping->find('all', array('fields' => array('channel_sku')));
				foreach($channelSku as $val){
					$chSkus[trim($val['Skumapping']['channel_sku'])] = trim($val['Skumapping']['channel_sku']);
				}
				
				//pr($chSkus);
				/*********************Getting Data From CSV***********************************/
				while($data = fgetcsv($handle))
				{	
				
					if(array_key_exists('channel_sku',$cols)  && array_key_exists('delete',$cols) && $data[$cols['delete']] != ''){
							$del_chsku[] = $data[$cols['channel_sku']];								
					}else if(array_key_exists('store_qty',$cols)){					
						/*-------Update Qty  on Store level-------*/
						$channel_name 		= isset($data[$cols['channel_name']]) ? $data[$cols['channel_name']] :'';
						
						$exist_store = $this->Skumapping->find('first', array(
												'fields' => array('channel_sku'),
												'conditions'=>array('Skumapping.channel_name' => $data[$cols['channel_name']])
									)
						);
						if(count($exist_store)>0){
						
							$update = array();													
							
							if(isset($data[$cols['qty_percent']]) && $data[$cols['qty_percent']] != ''){
								$update['qty_percent'] =   $data[$cols['qty_percent']];
							}
							if($data[$cols['min_qty']] != ''){
								$update['min_qty'] =   $data[$cols['min_qty']];
							}
							if($data[$cols['max_qty']] != ''){
								$update['max_qty'] =   $data[$cols['max_qty']];
							}
													
							
																	
							if(count($update) > 0){		
													
								$update['username']		 = $user;	
									
								$conditions = array('Skumapping.channel_name' => $channel_name);					
								if ($this->Skumapping->hasAny($conditions)){
										foreach($update as $field => $val){
											 $DataUpdate[$field] = "'".$val."'";
										 }									
										$this->Skumapping->updateAll( $DataUpdate, $conditions );		
										unset($DataUpdate);
									
								}							
								$u++;		
							}
						}else{
							$msg['msg'] = '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please '.$channel_name.' Check Store!</div>';
						}								
					
					
					}
					else if(array_key_exists('update_qty',$cols)){
						/*-------Update Qty  on Master Sku And Store level-------*/
						$sku				= isset($data[$cols['sku']]) ? $data[$cols['sku']] :'';
						$channel_name 		= isset($data[$cols['channel_name']]) ? $data[$cols['channel_name']] :'';
						
						$exist_sku = $this->Skumapping->find('first', array(
												'fields' => array('channel_sku'),
												'conditions'=>array('Skumapping.sku' => $data[$cols['sku']],'Skumapping.channel_name' => $data[$cols['channel_name']])
									)
						);
						if(count($exist_sku)>0){
						
							$update = array();													
							
							if(isset($data[$cols['asin']]) && $data[$cols['asin']] != ''){
								$update['asin'] =   $data[$cols['asin']];
							}
							if(isset($data[$cols['channel_sku']]) && $data[$cols['channel_sku']] != ''){
								$update['channel_sku'] =   $data[$cols['channel_sku']];
							}
							if(isset($data[$cols['channel_sku_title']]) && $data[$cols['channel_sku_title']] != ''){
								$update['channel_sku_title'] =  addslashes( $data[$cols['channel_sku_title']]);
							}
							if(isset($data[$cols['qty_percent']]) && $data[$cols['qty_percent']] != ''){
								$update['qty_percent'] =   $data[$cols['qty_percent']];
							}
							if($data[$cols['min_qty']] != ''){
								$update['min_qty'] =   $data[$cols['min_qty']];
							}
							if($data[$cols['max_qty']] != ''){
								$update['max_qty'] =   $data[$cols['max_qty']];
							}
							if($data[$cols['amazon_status']] != ''){
								$update['amazon_status'] =   $data[$cols['amazon_status']];
							}
							if($data[$cols['xsensys_status']] != ''){
								$update['xsensys_status'] =   $data[$cols['xsensys_status']];
							}							
							
																	
							if(count($update) > 0){		
													
								$update['username']		 = $user;	
									
								$conditions = array('Skumapping.sku' => $sku,'Skumapping.channel_name' => $channel_name);					
								if ($this->Skumapping->hasAny($conditions)){
										foreach($update as $field => $val){
											 $DataUpdate[$field] = "'".$val."'";
										 }									
										$this->Skumapping->updateAll( $DataUpdate, $conditions );		
										unset($DataUpdate);
									
								}							
								$u++;		
							}
						}else{
							$msg['msg'] = '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please '.$channel_name.' Check Store!</div>';
						}								
					
					
					}
					else if(isset($data[$cols['channel_sku']]) &&  isset($data[$cols['sku']])){
					
						$barcode = isset($data[$cols['barcode']]) ? $data[$cols['barcode']] :'';						 
						if(strlen($barcode) > 1 && strlen($barcode) < 11){
						 $barcode = str_pad($barcode, 11, "0", STR_PAD_LEFT);	
						}
						$sku				= isset($data[$cols['sku']]) ? $data[$cols['sku']] :'';
						$asin				= isset($data[$cols['asin']]) ? $data[$cols['asin']] :'';
						$channel_name 		= isset($data[$cols['channel_name']]) ? $data[$cols['channel_name']] :'';
						$channel_sku 		= isset($data[$cols['channel_sku']]) ? $data[$cols['channel_sku']] :'';
						$channel_sku_title	= isset($data[$cols['channel_sku_title']]) ? $data[$cols['channel_sku_title']] :'';
						$qty_percent 		= isset($data[$cols['qty_percent']]) ? $data[$cols['qty_percent']] :'';
						$min_qty			= isset($data[$cols['min_qty']]) ? $data[$cols['min_qty']] :'';
						$max_qty			= isset($data[$cols['max_qty']]) ? $data[$cols['max_qty']] :'';
						$amazon_status 		= isset($data[$cols['amazon_status']]) ? $data[$cols['amazon_status']] :'';
						$xsensys_status 	= isset($data[$cols['amazon_status']]) ? $data[$cols['xsensys_status']] :'';
							
						if(array_key_exists('update',$cols) && $data[$cols['update']] != ''){
								
							$update = array();			$update[] = " `username` =  '".$user."' ";		
							if($barcode!='')			$update[] = " `barcode` =  '".$barcode."' ";
							if($asin!='')				$update[] = " `asin` =  '".$asin."' ";
							if($channel_sku_title!='') 	$update[] = " `channel_sku_title` =  '".addslashes($channel_sku_title)."' ";
							if($qty_percent!='') 		$update[] = " `qty_percent` =  '".$qty_percent."' ";
							if($min_qty!='')			$update[] = " `min_qty` =  '".$min_qty."' ";
							if($max_qty!='')			$update[] = " `max_qty` =  '".$max_qty."' ";
							if($amazon_status!='')		$update[] = " `amazon_status` =  '".$amazon_status."' ";
							if($xsensys_status!='')		$update[] = " `min_qty` =  '".$xsensys_status."' ";
																	
							$sql_query = '';
							if(count($update)>0){
								$w = 1; $update_col = '';
								$count_lenght  = count($update);
								foreach ($update as $k) {
									$update_col .= $k;
									if ($w < $count_lenght) {
										$update_col .=', ';
									}
									$w++;
								 }											 
								$sql_query = ' SET '.$update_col;
								$this->Skumapping->query("UPDATE `skumappings` $sql_query WHERE `channel_sku` = '".$channel_sku."'");	
								$u++;		
							
							}
										
						}
						else{		
						
							if(isset($data[$cols['asin']]) && $data[$cols['asin']]!=''){
										
								if(in_array($data[$cols['channel_sku']],$chSkus) ){
									$already .= $data[$cols['sku']]."\t" . $data[$cols['channel_sku']] ."\t" . $data[$cols['asin']] . "\tAlready Exists!\n";
									$al_count++;							
								}else{				
									$rowcount ++;							
									$chSkus[$channel_sku] = $channel_sku;								
									$insert_str .= "('".$sku."','".$barcode."','".$asin."', '".$channel_name."','".$channel_sku."','".addslashes($channel_sku_title)."','".$qty_percent."','".$min_qty."','".$max_qty."','".$amazon_status."','".$xsensys_status."','".date("Y-m-d H:i:s")."','".$this->Session->read('Auth.User.username')."'),";
								  
								   $ins_count++;
								   
								   if($rowcount % 1000 == 0){
									$insert_str = rtrim($insert_str,',');
									$this->Skumapping->query("INSERT INTO `skumappings` (`sku`, `barcode`, `asin`, `channel_name`, `channel_sku`, `channel_sku_title`, `qty_percent`, `min_qty`, `max_qty`, `amazon_status`,`xsensys_status`, `added_date`, `username`) VALUES $insert_str");
									unset($insert_str);
									
								   }
								}
							}else{
							 $msg['msg'] = '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please check `sku`,  `asin`, And `channel_sku`!</div>';
							} 
						}					   
					 
					}else{
					 	  $msg['msg'] = '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please Check SKU,   And Channel Sku !</div>';
					 }															   
				}
				/*********************End Data From CSV***********************************/
				
				if(count($del_chsku) > 0){
					$condition = array('channel_sku IN' => $del_chsku);
					$this->Skumapping->deleteAll($condition,false); 
					$msg['msg'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.count($del_chsku).' Asins Deleted.</div>';  
				} 
				if($u > 0){
					$msg['msg'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$u.' Asins Updated.</div>';
				} 
				
				if($insert_str != ''){
					$insert_str = rtrim($insert_str,',');
					$this->Skumapping->query("INSERT INTO `skumappings` (`sku`, `barcode`, `asin`, `channel_name`, `channel_sku`, `channel_sku_title`, `qty_percent`, `min_qty`, `max_qty`, `amazon_status`,`xsensys_status`, `added_date`, `username`) VALUES $insert_str");
					unset($insert_str);
				}
						
				 if (!is_dir($output_dir)) {
					mkdir($output_dir, 0777);         
				 }
				 if($al_count > 0){					 
					file_put_contents($output_dir.'asin_exist_'.$user.'.txt', "SKU\tCHANNL SKU\tASIN\tMESSAGE\n",LOCK_EX); 					
					file_put_contents($output_dir.'asin_exist_'.$user.'.txt', $already, FILE_APPEND | LOCK_EX);
					$already = "<a href='". $this->webroot."files/mapping/asin_exist_".$user.".txt' target='_blank' style='text-decoration:underline;color:#CC0000;'>".$al_count." Asins Already Exist (Download File)!</a>\n";			
				 }
				 if($already != '' && $ins_count == 0 ){				  	
					 $msg['msg'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$ins_count." Asins Added.\n".$already.'</div>';
				 }
				 
				 if($ins_count > 0){
					 move_uploaded_file($_FILES["upload"]["tmp_name"],$output_dir. $fileName.'_'.date('ymd_Hm').'_'.$user.'.'.$extension);
					 $msg['msg'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$ins_count." Asins Added.\n".$already.'</div>';
				 }	
				 
			}else{		 	
				 $msg['msg'] = '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Invalid file type. We accept only CSV file!</div>';
		
			 }
			 // move_uploaded_file($_FILES["upload"]["tmp_name"],$output_dir. $fileName.'_'.date('ymd_Hm').'_.'.$extension);
			 $msg['msg'] = nl2br($msg['msg']);
		echo json_encode($msg);
		exit;
	}
	
	public function UploadMaps($id = null) 
    {	
			$this->loadModel( 'Skumapping' );		
			$pathinfo = pathinfo($_FILES["upload"]["name"]);
            if (array_key_exists('filename', $pathinfo))
                $fileName  = $pathinfo['filename'];
            if (array_key_exists('extension', $pathinfo))
                $extension = strtolower($pathinfo['extension']);
								
			$output_dir = WWW_ROOT. 'files'.DS.'mapping'.DS; 
			$msg = array();			
			 if($extension == 'csv'){
				$insert_str	= '';
				$already    = '';
				$al_count   = 0;
				$ins_count  = 0;
				$rowcount   = 0;
				$u          = 0;
				$csvFile	= $_FILES["upload"]["tmp_name"];	
				$handle 	= fopen($csvFile, 'r');
				$cols   	= array_flip(fgetcsv($handle));				
				$chSkus		= array();
				$del_chsku 	= array();
				$user		= $this->Session->read('Auth.User.username');
				
				$channelSku = $this->Skumapping->find('all', array('fields' => array('channel_sku')));
				foreach($channelSku as $val){
					$chSkus[trim($val['Skumapping']['channel_sku'])] = trim($val['Skumapping']['channel_sku']);
				}
				
				//pr($chSkus);
				/*********************Getting Data From CSV***********************************/
				while($data = fgetcsv($handle))
				{	
				
					 if(isset($data[$cols['channel_sku']]) &&  isset($data[$cols['sku']])){
					
						$barcode = isset($data[$cols['barcode']]) ? $data[$cols['barcode']] :'';						 
						if(strlen($barcode) > 1 && strlen($barcode) < 11){
						 $barcode = str_pad($barcode, 11, "0", STR_PAD_LEFT);	
						}
						$sku				= isset($data[$cols['sku']]) ? $data[$cols['sku']] :'';
						$asin				= isset($data[$cols['asin']]) ? $data[$cols['asin']] :'';
						$channel_name 		= isset($data[$cols['sub_source']]) ? $data[$cols['sub_source']] :'';
						$channel_sku 		= isset($data[$cols['channel_sku']]) ? $data[$cols['channel_sku']] :'';
						$channel_sku_title	= isset($data[$cols['channel_sku_title']]) ? $data[$cols['channel_sku_title']] :'';
  
						$source 			= isset($data[$cols['source']]) ? $data[$cols['source']] :'';
 						$channel_type		= isset($data[$cols['channel_type']]) ? $data[$cols['channel_type']] :'';
						$referral_fee 		= isset($data[$cols['referral_fee']]) ? $data[$cols['referral_fee']] :'';
						$min_referral_fee	= isset($data[$cols['min_referral_fee']]) ? $data[$cols['min_referral_fee']] :'';
						$margin 			= isset($data[$cols['margin']]) ? $data[$cols['margin']] :'';
						$listing_status 	= isset($data[$cols['listing_status']]) ? $data[$cols['listing_status']] :'';
						$stock_status 		= isset($data[$cols['stock_status']]) ? $data[$cols['stock_status']] :'';	

 						if(isset($data[$cols['asin']]) && $data[$cols['asin']]!=''){
									
							if(in_array($data[$cols['channel_sku']],$chSkus) ){
 								
								$update = array();			
								$update[] = " `username` =  '".$user."' ";		
 								if($asin!='')				$update[] = " `asin` =  '".$asin."' ";
								if($channel_sku_title!='') 	$update[] = " `channel_sku_title` =  '".addslashes($channel_sku_title)."' ";
								if($source!='') 			$update[] = " `source` =  '".$source."' ";
								if($channel_type!='') 		$update[] = " `channel_type` =  '".$channel_type."' ";
 								if($referral_fee!='') 		$update[] = " `referral_fee` =  '".$referral_fee."' ";
								if($min_referral_fee!='')	$update[] = " `min_referral_fee` =  '".$min_referral_fee."' ";
								if($margin!='')				$update[] = " `margin` =  '".$margin."' ";
								if($listing_status!='')		$update[] = " `listing_status` =  '".$listing_status."' ";
								if($stock_status!='')		$update[] = " `stock_status` =  '".$stock_status."' ";
																		
								$sql_query = '';
								if(count($update)>0){
									$w = 1; $update_col = '';
									$count_lenght  = count($update);
									foreach ($update as $k) {
										$update_col .= $k;
										if ($w < $count_lenght) {
											$update_col .=', ';
										}
										$w++;
									 }											 
									$sql_query = ' SET '.$update_col;
									$this->Skumapping->query("UPDATE `skumappings` $sql_query WHERE `channel_sku` = '".$channel_sku."'");	
									$u++;		
								
								}
  								 
								$already .= $data[$cols['sku']]."\t" . $data[$cols['channel_sku']] ."\t" . $data[$cols['asin']] . "\tAlready Exists!\n";
								$al_count++;							
							}else{				
								$rowcount ++;							
								$chSkus[$channel_sku] = $channel_sku;								
								$insert_str .= "('".$sku."','".$barcode."','".$asin."', '".$channel_name."','".$channel_sku."','".addslashes($channel_sku_title)."','".$source."','".$channel_type."','".$referral_fee."','".$min_referral_fee."','".$margin."','".$listing_status."','".$stock_status."','".date("Y-m-d H:i:s")."','".$this->Session->read('Auth.User.username')."'),";
							  
							   $ins_count++;
							   
							   if($rowcount % 1000 == 0){
								 
								 $insert_str = rtrim($insert_str,',');
								$this->Skumapping->query("INSERT INTO `skumappings` (`sku`, `barcode`, `asin`, `channel_name`, `channel_sku`, `channel_sku_title`, `source`, `channel_type`, `referral_fee`,`min_referral_fee`,`margin`,`listing_status`, `stock_status`,`added_date`, `username`) VALUES $insert_str");
								unset($insert_str);
								
							   }
							}
						}else{
						 $msg['msg'] = '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please check `sku`,  `asin`, And `channel_sku`!</div>';
						} 
						 					   
					 
					}else{
					 	  $msg['msg'] = '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please Check SKU,   And Channel Sku !</div>';
					 }															   
				}
				/*********************End Data From CSV***********************************/
				
				if(count($del_chsku) > 0){
					$condition = array('channel_sku IN' => $del_chsku);
					$this->Skumapping->deleteAll($condition,false); 
					$msg['msg'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.count($del_chsku).' Asins Deleted.</div>';  
				} 
				if($u > 0){
					$msg['msg'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$u.' Asins Updated.</div>';
				} 
				
				if($insert_str != ''){
					$insert_str = rtrim($insert_str,',');
					$this->Skumapping->query("INSERT INTO `skumappings` (`sku`, `barcode`, `asin`, `channel_name`, `channel_sku`, `channel_sku_title`, `source`, `channel_type`, `referral_fee`,`min_referral_fee`,`margin`,`listing_status`, `stock_status`,`added_date`, `username`) VALUES $insert_str");
					unset($insert_str);
				}
						
				 if (!is_dir($output_dir)) {
					mkdir($output_dir, 0777);         
				 }
				 if($al_count > 0){					 
					file_put_contents($output_dir.'asin_exist_'.$user.'.txt', "SKU\tCHANNL SKU\tASIN\tMESSAGE\n",LOCK_EX); 					
					file_put_contents($output_dir.'asin_exist_'.$user.'.txt', $already, FILE_APPEND | LOCK_EX);
					$already = "<a href='". $this->webroot."files/mapping/asin_exist_".$user.".txt' target='_blank' style='text-decoration:underline;color:#CC0000;'>".$al_count." Asins Already Exist (Download File)!</a>\n";			
				 }
				 if($already != '' && $ins_count == 0 ){				  	
					 $msg['msg'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$ins_count." Asins Added.\n".$already.'</div>';
				 }
				 
				 if($ins_count > 0){
					 move_uploaded_file($_FILES["upload"]["tmp_name"],$output_dir. $fileName.'_'.date('ymd_Hm').'_'.$user.'.'.$extension);
					 $msg['msg'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$ins_count." Asins Added.\n".$already.'</div>';
				 }	
				 
			}else{		 	
				 $msg['msg'] = '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Invalid file type. We accept only CSV file!</div>';
		
			 }
			 // move_uploaded_file($_FILES["upload"]["tmp_name"],$output_dir. $fileName.'_'.date('ymd_Hm').'_.'.$extension);
			$msg['msg'] = nl2br($msg['msg']);
			$this->Session->setFlash($msg['msg'], 'flash_danger');
			$this->redirect($this->referer());
		exit;
	}
	
	public function Search() 
    {	
		
		$this->layout = "wms_ajax"; 		
		$this->loadModel('Skumapping');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		
		$this->Product->unbindModel( array( 'hasMany' => array( 'ProductLocation' ) ) );
			
		$searchkey = trim($this->request->data['searchkey']);	
		
		$getProduct = array();	
		$skuArray 	= array();		
		$perPage	= '800';	
		
		$this->paginate = array(
					  'limit' => $perPage,
					  'conditions' => array( 'Product.product_sku LIKE' => "%{$searchkey}%"  ),			
                      'fields' => array('Product.product_sku','ProductDesc.barcode', 'Product.id', 'Product.product_name','Product.current_stock_level'),	
         );	
		 	
		$getProduct = $this->paginate('Product');			
		
		if(count($getProduct)> 0){				
			$map 	= $this->Skumapping->find('all', array('conditions' => array( "sku LIKE " => "%{$searchkey}%")));
			foreach($map as $val){
				$skuArray[$val['Skumapping']['sku']][] = array('id' => $val['Skumapping']['id'],
															'barcode' => $val['Skumapping']['barcode'],
															'asin' => $val['Skumapping']['asin'],
															'channel_name' => $val['Skumapping']['channel_name'],
															'channel_sku' => $val['Skumapping']['channel_sku'],
															'channel_sku_title' => $val['Skumapping']['channel_sku_title'],
															'buffer' => $val['Skumapping']['buffer'],
															'performance_status' =>$val['Skumapping']['performance_status'],
															'qty_percent' => $val['Skumapping']['qty_percent'],
															'min_qty' => $val['Skumapping']['min_qty'],
															'max_qty' => $val['Skumapping']['max_qty'],
															'amazon_status' => $val['Skumapping']['amazon_status'],
															'xsensys_status' => $val['Skumapping']['xsensys_status']);         
			}	
			
		}else{
		
			$skus = array();  
			$map  = $this->Skumapping->find('all', array('conditions' => array( 'OR' => array('channel_sku LIKE' => "%{$searchkey}%",'asin LIKE' => "%{$searchkey}%"))));
			
			foreach($map as $val){
				$skuArray[$val['Skumapping']['sku']][] = array('id' => $val['Skumapping']['id'],
															'barcode' => $val['Skumapping']['barcode'],
															'asin' => $val['Skumapping']['asin'],
															'channel_name' => $val['Skumapping']['channel_name'],
															'channel_sku' => $val['Skumapping']['channel_sku'],
															'buffer' => $val['Skumapping']['buffer'],
															'performance_status' =>$val['Skumapping']['performance_status'],
															'channel_sku_title' => $val['Skumapping']['channel_sku_title'],
															'qty_percent' => $val['Skumapping']['qty_percent'],
															'min_qty' => $val['Skumapping']['min_qty'],
															'max_qty' => $val['Skumapping']['max_qty'],
															'amazon_status' => $val['Skumapping']['amazon_status'],
															'xsensys_status' => $val['Skumapping']['xsensys_status'] );  
				$skus[] = $val['Skumapping']['sku'];       
			}			
			//$this->set('mappped_sku', $skuArray );
			
			if(count($skus)>0){
					
				if(count($skus) == 1){
					//$getProduct =  $this->Product->find('all', array('conditions' => array( "Product.product_sku " =>  $skus[0] )));	
					$conditions =  array( 'Product.product_sku ' =>  $skus[0]);
				}else{
					//$getProduct =  $this->Product->find('all', array('conditions' => array( "Product.product_sku IN " =>  $skus )));				
					$conditions =  array( 'Product.product_sku  IN' =>  $skus);
				}	
				
				$this->paginate = array(
					  'limit' => $perPage,
					  'conditions' => $conditions,			
                      'fields' => array('Product.product_sku','ProductDesc.barcode', 'Product.id', 'Product.product_name','Product.current_stock_level'),	
					 );		
				$getProduct = $this->paginate('Product');	
			}
		}	
		
		$this->set('all_product', $getProduct );	
		$this->set('mappped_sku', $skuArray );
		$this->render( 'search' );
		//exit;
	} 
	
	public function Delete() 
    {	
		$this->loadModel( 'Skumapping' );	
		$this->Skumapping->query("DELETE FROM `skumappings` WHERE `id` = '".$_POST['id']."'");
		$msg['msg'] = 'DELETED';	
		echo json_encode($msg);		
		exit;
	} 
	
	public function DeleteMap($id = 0) 
    {	
		$this->loadModel( 'Skumapping' );	
		$this->Skumapping->query("DELETE FROM `skumappings` WHERE `id` = '".$id."'");
 		$this->Session->setflash( 'Channel sku deleted successfully!', 'flash_danger' ); 
  		$this->redirect($this->referer());	
		exit;
	}
	 
	public function Update() 
    {	
		$this->loadModel( 'Skumapping' );	
		$this->Skumapping->query("UPDATE `skumappings` SET `". $_POST['name']."` =  '".addslashes($_POST['value'])."' WHERE `id` = '".$_POST['pk']."'");
		$msg['msg'] = 'Updated';	
		echo json_encode($msg);		
		exit;
	}
	public function updateOpenOrderQty()  {	
		$this->loadModel('Skumapping');	
		$PerformingSku = $this->getPerformingSku();
		
		foreach($PerformingSku as $sku){
				foreach($sku as  $v){				
				$skuQty['orders_qty'] = $v['quantity'];
				$conditions = array('Skumapping.channel_sku' => $v['channel_sku']);					
				$this->Skumapping->updateAll( $skuQty, $conditions );
			}	
		}			
	
	}
	
	public function getOpenOrderQty()  {	
		$this->loadModel('OpenOrder');	
		$items  =  $this->OpenOrder->find('all', array(
			    'conditions' => array('OpenOrder.status' => 0),
				'fields' => array(
							'OpenOrder.items', 
							'OpenOrder.num_order_id',
							'OpenOrder.status'
							),
				'order' => array('OpenOrder.num_order_id' => 'ASC') 
				)
		);
		
		$skuArray = array();
		
		foreach($items as $item){
			foreach($item as $val){
				foreach(unserialize($val['items']) as $v){
					$sku = $v->SKU;
					$qty = $v->Quantity;
					if(substr($sku, 0, 2) == 'B-'){
						$list =  explode('-',$sku);
						if(count($list) == 3){
							$bsku = 'S-'.trim($list[1]);		
							$q = (int) (trim($list[2]) * $qty);				
							$skuArray[$bsku][] =
								array('channel_sku' =>  $v->ChannelSKU, 'num_order_id' => $val['num_order_id'],'quantity' => $q,'Bund' => $sku);
						}else{						
						
							for($i = 1; $i < count($list)-1;$i++){
								$bsku = 'S-'.$list[$i];		
								$len = (count($list)-1);
								$q = (int) ($list[$len] * $qty);						
								$skuArray[$bsku][] =
									array('channel_sku' =>  $v->ChannelSKU,'num_order_id' => $val['num_order_id'],'quantity' => $qty,'Bunds' => $sku);
							}
							
						}					
					}else{	
						
							$skuArray[$v->SKU][] =
								array('channel_sku' =>  $v->ChannelSKU,'num_order_id' => $val['num_order_id'],'quantity' => $v->Quantity,'status' => $val['status']); 	
									
					}
				}
			}
		}	
		//pr($skuArray);exit;
		return $skuArray;
		exit;
	}
	public function getOpenOrderSkuQty()  {	
		$this->layout = '';
		$this->autoRender = false;
		$open   = $this->getOpenOrderQty();	
		$skuQty = array();
		foreach($open as $sku => $item){ 
			$qty = 0;		
			foreach($item as $val){
				$qty +=	$val['quantity'];	
			}
			$skuQty[$sku] = $qty;
		}		
		//pr($skuQty);exit; 
		return $skuQty;
		exit;
	}
	public function getFeedCsv()  {	
		
		$this->loadModel('Skumapping');			
		$OpenOrderQty = $this->getOpenOrderSkuQty();
			
		$output_dir = WWW_ROOT. 'logs'.DS.'mapping'.DS; 
		$channel = trim($this->request->data['channel']);
		$msg['msg'] = '';
		if (!is_dir($output_dir)) {
			mkdir($output_dir, 0777);         
		}		
		
		$file = fopen("$output_dir$channel.csv","w") or die("File open error.");
		fputcsv($file,array('channel_sku','asin','buffer','qty_percent','min_qty','max_qty','qtyToReport','CurrentStock','SKU'));  
		
		$map  = $this->Skumapping->find('all', array('conditions' => array( 'channel_name' => "{$channel}")));   
		
			
		foreach($map as $val){
		
			$qtyToReport	= 0; 
			$qty 			= 0;
			$perqty 		= 0;
			$sku 			= $val['Skumapping']['sku'];
			$channel_sku 	= $val['Skumapping']['channel_sku'];
			$currentStock 	= $this->getCurrentStock($sku);			
			$csku = '';									
												
			if(isset($currentStock[$sku]) && ($currentStock[$sku] > 0)){
				$perqty = floor(($currentStock[$sku] - $val['Skumapping']['buffer']) * $val['Skumapping']['qty_percent']/100 );
				$qty = ($currentStock[$sku] - $val['Skumapping']['buffer']);
				if(($qty > 0) && ($qty < $val['Skumapping']['min_qty'])){
				
					if($val['Skumapping']['performance_status']== 'fast_moving'){  
						$qtyToReport = $qty;
					}else if($val['Skumapping']['performance_status']== 'slow_moving'){
						 $qtyToReport = $qty;
					}else{
						$qtyToReport = 0;
					}
					
				}elseif($qty > $val['Skumapping']['max_qty']){
					$qtyToReport = $val['Skumapping']['max_qty'];
				}else{
					 $qtyToReport = $perqty > 0 ? $perqty : 0;
				}
				
				$csku = $currentStock[$sku];
			}
			//echo "<br>".$t++;
			$msg['msg'] = "File generated."; 
			
			
			fputcsv($file,array($val['Skumapping']['channel_sku'],$val['Skumapping']['asin'],$val['Skumapping']['buffer'],$val['Skumapping']['qty_percent'],$val['Skumapping']['min_qty'],$val['Skumapping']['max_qty'],$qtyToReport,$csku,$sku));  
			  
		}	
					
		fclose($file);
		$msg['file'] = "<div class='alert alert-success fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Download <a href='".$this->webroot."logs/mapping/".$channel.".csv'>".$channel.".csv</a></div>"; 
		
	    echo json_encode($msg);
		
		exit;
	}

	public function getMapCsv()  {	
		
		$this->loadModel('Skumapping');			
		//$OpenOrderQty = $this->getOpenOrderSkuQty();
			
		$output_dir = WWW_ROOT. 'logs'.DS.'mapping'.DS; 
		$channel = trim($this->request->data['channel']);
		$msg['msg'] = '';
		if (!is_dir($output_dir)) {
			mkdir($output_dir, 0777);         
		}		
		
		$file = fopen("$output_dir$channel.csv","w") or die("File open error.");
		fputcsv($file,array('sku','channel_sku','asin','title','Source','sub Source','Channel type','Referral fee','Margin','Listing status','Stock status'));  
		if(in_array($channel,['CostBreaker','Marec','Rainbow'])){
			$map  = $this->Skumapping->find('all', array('conditions' => array( 'channel_name LIKE' =>  $channel."%")));
			// array('fields' => array( 'sku','channel_sku','asin','title','source','channel_name','channel_type','referral_fee','margin','listing_status','status'))
		}else{
			$map  = $this->Skumapping->find('all', array('conditions' => array( 'channel_name' => "{$channel}")));
		}   
		
 		foreach($map as $val){
  			$msg['msg'] = "File generated."; 
 			fputcsv($file,array($val['Skumapping']['sku'],$val['Skumapping']['channel_sku'],$val['Skumapping']['asin'],$val['Skumapping']['channel_sku_title'],$val['Skumapping']['source'],$val['Skumapping']['channel_name'],$val['Skumapping']['channel_type'],$val['Skumapping']['referral_fee'],$val['Skumapping']['margin'],$val['Skumapping']['listing_status'],$val['Skumapping']['stock_status']));  
			  
		}	
					
		fclose($file);
		$msg['file'] = "<div class='alert alert-success fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Download <a href='".$this->webroot."logs/mapping/".$channel.".csv'>".$channel.".csv</a></div>"; 
		
	    echo json_encode($msg);
		
		exit;
	}
	
	public function updateSkuPerformance()  {
		$this->layout = '';
		$this->autoRender = false;
		$this->fetchPerformingSku();		 
		$this->loadModel('Skumapping');
		$this->loadModel('OpenOrder');	
		/*------------------Get Medium SKUS---------------------*/
		$start_date = date('Y-m-d H:i:s');
		$end_date   = date('Y-m-d H:i:s', strtotime('-12 days',strtotime(date('Y-m-d H:i:s'))));
		
		$items  =  $this->OpenOrder->find('all', array(
			    'conditions' => array('open_order_date BETWEEN ? AND ?' => array($end_date,$start_date)),
				'fields' => array(
							'OpenOrder.items', 
							'OpenOrder.num_order_id',
							'OpenOrder.sub_source'
							),
				'order' => array('OpenOrder.num_order_id' => 'ASC') 
				)
		);
		$mediumSku = array();
		if(count($items) > 0 ){
			foreach($items as $val){
				$item = unserialize($val['OpenOrder']['items']);
				foreach($item as $v){				
					//Get all sku of 10 days sale.(Medium Moving)
					$mediumSku[] = "'" . $v->ChannelSKU . "'";
				}	
			}
		}			
		/*$map  = $this->Skumapping->find('all'); 
		$slowSku = array();
		foreach($map as $val){			
			$channel_sku = $val['Skumapping']['channel_sku'];
			if(!in_array($channel_sku,$data)){
				$slowSku[] = "'" . $channel_sku . "'";
			}			
		}*/
		/*--------------------------------------------------------------------*/		
		$fastSku = array();
		foreach($this->getPerformingSku() as $channel_sku  => $val){	
			$fastSku[] = "'" . $channel_sku . "'";
		}	
		
		if(count($mediumSku) > 0){
			$this->Skumapping->query("UPDATE `skumappings` SET `performance_status` =  'slow_moving'");
			$this->Skumapping->query("UPDATE `skumappings` SET `performance_status` =  'medium_moving' WHERE `channel_sku` IN(".implode(",",$mediumSku).")");
			//$this->Skumapping->query("UPDATE `skumappings` SET `performance_status` =  'slow_moving' WHERE `channel_sku` IN(".implode(",",$slowSku).")");
		}
		if(count($fastSku) > 0){
			$this->Skumapping->query("UPDATE `skumappings` SET `performance_status` =  'fast_moving' WHERE `channel_sku` IN(".implode(",",$fastSku).")");
		}
		$output_dir = WWW_ROOT. 'logs'.DS; 
		file_put_contents($output_dir.'updateSkuPerformance_'.date('dmy').'.log', "\n*********".date('d-m-Y H:i:s')."***********\n Medium Sku Updated :".count($mediumSku), FILE_APPEND|LOCK_EX);
	} 
	
	public function getPerformingSku()  {	
		 $this->layout = '';
		$this->autoRender = false;
		$this->loadModel('SkuPerforming');
		$stockArray = array();
		$skus 		= $this->SkuPerforming->find('all',array('order' => 'order_count DESC' , 'limit' => 20));		
		if(count($skus)>0){
			foreach($skus as $item){
				$stockArray[$item['SkuPerforming']['channel_sku']] = $item['SkuPerforming']['channel_sku'];
			}
		}			
		return $stockArray;
		
	}	
	
	
	public function fetchPerformingSku()  {	
		
		$this->loadModel('SkuPerforming');
		$this->loadModel('OpenOrder');	
		$start_date = date('Y-m-d H:i:s');
		$end_date   = date('Y-m-d H:i:s', strtotime('-3 days',strtotime(date('Y-m-d H:i:s'))));
		
		$items  =  $this->OpenOrder->find('all', array(
			    'conditions' => array('open_order_date BETWEEN ? AND ?' => array($end_date,$start_date)),
				'fields' => array(
							'OpenOrder.items', 
							'OpenOrder.num_order_id',
							'OpenOrder.sub_source'
							),
				'order' => array('OpenOrder.num_order_id' => 'ASC') 
				)
		);
		
		$this->SkuPerforming->query( 'TRUNCATE sku_performings' );
		
		$data = array();
		foreach($items as $val){
			$item = unserialize($val['OpenOrder']['items']);
			foreach($item as $v){
				$getresult	=	$this->SkuPerforming->find('all', array( 'conditions' => array('SkuPerforming.channel_sku' => $v->ChannelSKU) ));
				 
				if(count($getresult) > 0 ){
					$qty = $this->SkuPerforming->field('quantity') + $v->Quantity;
					$order_count = $this->SkuPerforming->field('order_count') + 1; 					
					$orders = $this->SkuPerforming->field('orders').','.$val['OpenOrder']['num_order_id'];		
					$this->SkuPerforming->updateAll(array('quantity'=> $qty,'orders'=> "'".$orders."'",'order_count'=> $order_count), 
					array('SkuPerforming.channel_sku' => $v->ChannelSKU) );
				}else{
				
					$data['sku'] 		 = $v->SKU;
					$data['channel_sku'] = $v->ChannelSKU;
					$data['store'] 		 = $val['OpenOrder']['sub_source'];					
					$data['quantity'] 	 = $v->Quantity;
					$data['orders'] 	 = $val['OpenOrder']['num_order_id'];		
					$data['order_count'] = 1; 
					$data['added_date']	 = date('Y-m-d H:i:s');	
					$this->SkuPerforming->saveAll($data);		
				}	
			}		
		}		
		
	} 
	
	public function getCurrentStock($skus = null)  {	
	 
		$this->loadModel('Product');
		$stockArray = array();
		
		$this->Product->unbindModel( array( 'hasMany' => array( 'ProductLocation','ProductDesc','ProductPrice' ) ) );	
		
		if(is_array($skus)){		
			if(count($skus) == 1){				
				$conditions =  array( 'Product.product_sku ' =>  $skus[0]);
			}else{			
				$conditions =  array( 'Product.product_sku  IN' =>  $skus);
			}	
		}else{
			$conditions =  array( 'Product.product_sku ' =>  $skus);
		}		
		$Products 	= $this->Product->find('all', array('conditions' => $conditions,'fields' => array('Product.product_sku','Product.current_stock_level')));		
		if(count($Products)>0){
			foreach($Products as $item){
				$stockArray[$item['Product']['product_sku']] = $item['Product']['current_stock_level'];
			}
		}
		return $stockArray;
	}	
	

	public function getFeed()  {	
  
  		$this->layout = '';
		$this->autoRender = false;
		$public_key  = 'qaz1wsx2edc3tfv';
		$private_key = 'plm9ojn8ijb7uhv';
		
		$signature = hash_hmac("sha256", $public_key, $private_key, true);
		$signature = urlencode(base64_encode($signature));

		$dataArray = array();
	 
		if($this->request->data['key'] == $signature){
		
			$channel = $this->request->data['channel'];
			$this->loadModel('Skumapping');	
							
			//$this->updateSkuPerformance(); //Update sku performance 
						
			$output_dir = WWW_ROOT. 'logs'.DS.'feed'.DS; 
			$msg['msg'] = '';
			if (!is_dir($output_dir)) {
				mkdir($output_dir, 0777);         
			}
		
			$file = fopen("$output_dir$channel.csv","w") or die("File open error.");
			fputcsv($file,array('channel_sku','buffer','qtyToReport','CurrentStock','SKU'));  
		
			$map  = $this->Skumapping->find('all', array('conditions' => array( 'channel_name LIKE' => "{$channel}%")));   
	
			
			foreach($map as $val){
			
				$qtyToReport	= 0; 
				$qty 			= 0;
				$perqty 		= 0;
				$sku 			= $val['Skumapping']['sku'];
				$channel_sku 	= $val['Skumapping']['channel_sku'];
				$currentStock 	= $this->getCurrentStock($sku);			
				$csku = '';
				if(isset($currentStock[$sku]) && ($currentStock[$sku] > 0)){
					$pty = floor(($currentStock[$sku] - $val['Skumapping']['buffer']) * $val['Skumapping']['qty_percent']/100 );
					$qty = floor($currentStock[$sku] - $val['Skumapping']['buffer']);
					if(($qty > 0) && ($qty < $val['Skumapping']['min_qty'])){
				
						if($val['Skumapping']['performance_status']== 'fast_moving'){
							$qtyToReport = $qty;
						}else if($val['Skumapping']['performance_status']== 'slow_moving'){
							 $qtyToReport = $qty;
						}else{
							$qtyToReport = 0;
						}
					
					}elseif($qty > $val['Skumapping']['max_qty']){
						$qtyToReport = $val['Skumapping']['max_qty'];
					}else{
						 $qtyToReport = $pty > 0 ? $pty : 0;
					}
					
					$csku = $currentStock[$sku];
				}
				//echo "<br>".$t++;
				$msg['msg'] = "File generated."; 
				fputcsv($file,array($channel_sku,$val['Skumapping']['buffer'],$qtyToReport,$csku,$sku));  
				$dataArray[$channel_sku] = array('qty'=>$qtyToReport,'sku'=>$sku);
				  
			}	
								
			fclose($file);
		}else{
			$dataArray = array('msg'=>'Signature is not valid!');
		}		 
		echo json_encode($dataArray);//$dataArray;
		exit;
	}
	public function getSkusFromAwsFba()  {
	
		$channels = ['CostBreaker_ITFBA','CostBreaker_ESFBA','CostBreaker_UKFBA','CostBreaker_DEFBA','CostBreaker_FRFBA','Marec_DEFBA','Marec_UKFBA','Marec_ITFBA','Marec_FRFBA','Rainbow_UK_FBA','Rainbow_FRFBA','CostBreaker_NLFBA','CostBreaker_PLFBA','Rainbow_DE_FBA'];
		foreach( $channels as $channel ) { 
			$post = array('channel' => $channel);
			$this->getDataAws($post );
			print $channel;
			print "\n<br>";
		} 
		exit;
	}
			
	public function getSkusFromAws($store = NULL)  {	
	
		print "Processing.......\n";
		$this->layout = false;
		$this->loadModel('Store');	
		
		/*if($channel){		
				print "\n";
				print $channel;
				print "\n";
				$post = array('channel' => $channel);
				$this->getDataAws($post);
		
		}else{*/
		$getStores 	= $this->Store->find('all', array( 'conditions' => array('Store.store_name LIKE ?'=> $store.'%'), 'order' => 'Store.store_name DESC' ));
		
		foreach( $getStores as $getStore ) { 
			
			print $channel = trim($getStore['Store']['store_name']);
			print "\n";
			$post = array('channel' => $channel);
			$this->getDataAws($post);
			sleep(1);
			
		}
		//}
		print 'done.';
		exit;
	}
	public function Reconciliation() 
	{
		$this->layout = "skumapping";
		$this->loadModel('ReconciliationLog');	
		$this->paginate = array(
					  'limit' => 30,	
					  'order' => 'id DESC',			
         );		
		$reconciliation = $this->paginate('ReconciliationLog');		
 	 	$this->set('reconciliation', $reconciliation );	
	}
	
	public function UploadReconciliation() 
    {	
			$this->loadModel('Skumapping');	
			$this->loadModel('ReconciliationLog');	

			
 			$pathinfo = pathinfo($_FILES["upload"]["name"]);
            if (array_key_exists('filename', $pathinfo))
                $fileName  = $pathinfo['filename'];
            if (array_key_exists('extension', $pathinfo))
                $extension = strtolower($pathinfo['extension']);
								
			$output_dir = WWW_ROOT. 'mapping_reconciliation'.DS; 
			 
			$msg = array();		
			$status_update = [];
			$mismatch   = 'mismatch_sku_'.date('Y-m-d').'.csv';
			$amazon_txt = 'amazon_'.date('Y-m-d').'.txt';	
			if(in_array($extension,['txt','csv'])){
 				$csvFile			= $_FILES["upload"]["tmp_name"];	
  				$nf_sku_arr			= array();
				$mismatch_sku_arr 	= array();
				$all_ch_skus		= array();
				$mathched_sku_arr	= array();
				$recData            = array();
				$user				= $this->Session->read('Auth.User.username');
				$file_name_system	= $fileName.'_'.date('ymd_His').'_'.$user.'.'.$extension;
				move_uploaded_file($_FILES["upload"]["tmp_name"],$output_dir.$file_name_system );
				$fileData = file_get_contents($output_dir.$file_name_system);
				$data     = explode("\n",$fileData);
 				if($this->data['file_type'] != 'status'){
					
					$text ="sku\tproduct-id\tproduct-id-type\tprice\tminimum-seller-allowed-price\tmaximum-seller-allowed-price\titem-condition\tquantity\tadd-delete\twill-ship-internationally\texpedited-shipping\titem-note\tfulfillment-center-id\tproduct-tax-code\tmerchant_shipping_group_name\n";
					file_put_contents($output_dir.$amazon_txt, $text, LOCK_EX);
					$text = 'Channel SKU,Amazon Status,Xsensys Status'."\n";
					file_put_contents($output_dir.$mismatch, $text, LOCK_EX);
					file_put_contents($output_dir.'notfound_sku_'.date('Y-m-d').'.csv', "Channel SKU\n", LOCK_EX);
					/*********************Getting Data From CSV***********************************/
					foreach($data as $in => $rows)
					{
						if($in > 0){
							$cols = explode("\t",$rows);
  							$channel_sku =  $cols[0] ;
							$all_ch_skus[$channel_sku] = $channel_sku;
							$status = $this->Skumapping->find('first', array( 'conditions' => array('channel_sku'=> $channel_sku),'fields'=>['listing_status','asin','channel_name']));
							if(count($status) > 0){
								if(strtolower($status['Skumapping']['listing_status']) != strtolower($cols[4])){
									$mismatch_sku_arr[$channel_sku] = [$cols[4],$status['Skumapping']['listing_status']];
									$text = $channel_sku.",".$cols[4].",".$status['Skumapping']['listing_status']."\n";
									file_put_contents($output_dir.$mismatch, $text, FILE_APPEND | LOCK_EX);
									$text = $channel_sku."\t".trim($status['Skumapping']['asin'])."\t"."1\t".$cols[2]."\t"."\t"."\t"."11\t"."0\t"."a\t"."n\t"."\t".''."\t"." \t"."\t"."\t\n";
									file_put_contents($output_dir.$amazon_txt, $text,  FILE_APPEND | LOCK_EX);

								}else{
									$mathched_sku_arr[$channel_sku] = [$cols[4],$status['Skumapping']['listing_status']];
								}
							}else{
								$nf_sku_arr[$channel_sku] = $channel_sku;
								$text = $channel_sku."\n";
								file_put_contents($output_dir.'notfound_sku_'.date('Y-m-d').'.csv', $text, FILE_APPEND | LOCK_EX);
							}
  
 						}
					}
					$not_uploaded = [];
					$not_in = $this->Skumapping->find('all', array( 'conditions' => array('channel_name LIKE' =>$this->data['file_type'].'%','channel_sku NOT IN'=> $all_ch_skus),'fields'=>['channel_sku','asin']));
					if(count($$not_in) > 0){
						foreach($not_in as $nt){
							$not_uploaded[$nt['Skumapping']['channel_sku']] = $nt['Skumapping']['channel_sku'];
						}
					}
					$recData['file_name_system']  = $file_name_system;
					$recData['mathched'] 	  = json_encode($mathched_sku_arr);
					$recData['not_mathched']  = json_encode($mismatch_sku_arr);
					$recData['not_in_system'] = json_encode($nf_sku_arr);
					$recData['not_uploaded_marketplace'] =json_encode($not_uploaded);
					$recData['sub_source'] = $this->data['file_type'];
					$recData['username']   = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
					$recData['file_name']  = $fileName.'.'.$extension;
					$recData['added_date'] = date('Y-m-d H:i:s');
					
					$this->ReconciliationLog->saveAll($recData);
					/*********************End Data From CSV***********************************/
				}
 				else if($this->data['file_type'] == 'status'){
   					foreach($data as $in => $rows)
					{
						if($in  > 0){
							$cols = explode(",",$rows);
							$channel_sku =  $cols[0] ;
							$status 	 =  strtolower(trim($cols[1]));
							if(in_array($status,['active','inactive'])){
								$this->Skumapping->query("UPDATE `skumappings` SET `listing_status` = '".$status."' WHERE `channel_sku` = '".$channel_sku."'"); 
								$status_update[] = $channel_sku;
							}else{
								$msg['error'] = 'Upload valid file.';
							}
						}
 						 
					}
 				}
				  
			}else{		 	
				 $msg['msg'] = '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Invalid file type. We accept only TXT file for Amazon and CSV file for status update !</div>';
		
			 }
			 if(isset($msg['error'])){
				 $message['success'] = nl2br($msg['msg'].$msg['error']);
			 }else{
 		 		 $message['success'] = nl2br($msg['msg']);
			 }
			 
			 if(count($status_update) > 0){
			  $message['success'] = count($status_update).' Sku\'s Status updated.';
			 }
			 if($this->data['file_type'] == 'amazon'){
				 $file = "Download <a href='".$this->webroot."logs/".$mismatch."' style='text-decoration:underline'>Status Mis Match</a>"; 
				 $file .= "<br>Download <a href='".$this->webroot."logs/".$amazon_txt."' target='_blank' style='text-decoration:underline'>Amazon txt</a>"; 
				 
				 if(count($nf_sku_arr) > 0){
					$file .= "<br>Download <a href='".$this->webroot."logs/notfound_sku_".date('Y-m-d').".csv' target='_blank' style='text-decoration:underline'>Not found sku</a>"; 
				 }
				 $message['danger'] = $file;
			 }
 			
			
			$this->Session->setFlash($message, 'flash_all');
			$this->redirect($this->referer());
		exit;
	}
	
	public function UpdateStatusSample()  {	
		$file = "status_update.csv";
  		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT."sample_files/".$file));
		header("Content-Type: application/csv");
		readfile(WWW_ROOT."sample_files/".$file);
		exit;
	}
	public function DownloadReconciliationFile($id = 0)  {	
		
		$output_dir = WWW_ROOT. 'mapping_reconciliation'.DS; 
		$this->loadModel( 'ReconciliationLog' );
		$data =  $this->ReconciliationLog->find('first', array('conditions' => array('id' => $id)));

		if(count($data) > 0){
   			$file = $data['ReconciliationLog']['file_name_system'];
			header('Content-Description: File Transfer');
			header('Content-Disposition: attachment; filename='.basename($data['ReconciliationLog']['file_name']));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($output_dir.$file));
			header("Content-Type: text/plain");
			readfile($output_dir.$file);
		}
		exit;
	}
		
	public function DownloadMathched($id = 0)  {	
		
		$output_dir = WWW_ROOT. 'logs'.DS; 
		$this->loadModel( 'ReconciliationLog' );
		$data =  $this->ReconciliationLog->find('first', array('conditions' => array('id' => $id)));

		if(count($data) > 0){
   			$file =  'status-mathched.csv'; 
			file_put_contents($output_dir.$file, "Channel SKU,Amazon Status,Xsensys Status\n", LOCK_EX);
 			foreach(json_decode($data['ReconciliationLog']['mathched'],1) as $chsku => $val){
 			 	file_put_contents($output_dir.$file, $chsku.",".$val[0].",".$val[1]."\n", FILE_APPEND | LOCK_EX);
 			}
 			header('Content-Description: File Transfer');
			header('Content-Disposition: attachment; filename='.basename($file));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($output_dir.$file));
			header("Content-Type: application/csv");
			readfile($output_dir.$file);
		}
		exit;
	}	
	public function DownloadNotMathched($id = 0)  {	
		
		$output_dir = WWW_ROOT. 'logs'.DS; 
		$this->loadModel( 'ReconciliationLog' );
		$data =  $this->ReconciliationLog->find('first', array('conditions' => array('id' => $id)));

		if(count($data) > 0){
   			$file =  'status-mismathched.csv'; 
			file_put_contents($output_dir.$file, "Channel SKU,Amazon Status,Xsensys Status\n", LOCK_EX);
 			foreach(json_decode($data['ReconciliationLog']['not_mathched'],1) as $chsku => $val){
 			 	file_put_contents($output_dir.$file, $chsku.",".$val[0].",".$val[1]."\n", FILE_APPEND | LOCK_EX);
 			}
 			header('Content-Description: File Transfer');
			header('Content-Disposition: attachment; filename='.basename($file));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($output_dir.$file));
			header("Content-Type: application/csv");
			readfile($output_dir.$file);
		}
		exit;
	}	
	public function DownloadNotUploaded($id = 0)  {	
		
		$output_dir = WWW_ROOT. 'logs'.DS; 
		$this->loadModel( 'ReconciliationLog' );
		$data =  $this->ReconciliationLog->find('first', array('conditions' => array('id' => $id)));

		if(count($data) > 0){
   			$file =  'not_uploaded.csv'; 
			file_put_contents($output_dir.$file, "Channel SKU\n", LOCK_EX);
 			foreach(json_decode($data['ReconciliationLog']['not_uploaded_marketplace'],1) as $chsku){
 			 	file_put_contents($output_dir.$file, $chsku."\n", FILE_APPEND | LOCK_EX);
 			}
 			header('Content-Description: File Transfer');
			header('Content-Disposition: attachment; filename='.basename($file));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($output_dir.$file));
			header("Content-Type: application/csv");
			readfile($output_dir.$file);
		}
		exit;
	}	public function DownloadNotInSystem($id = 0)  {	
		
		$output_dir = WWW_ROOT. 'logs'.DS; 
		$this->loadModel( 'ReconciliationLog' );
		$data =  $this->ReconciliationLog->find('first', array('conditions' => array('id' => $id)));

		if(count($data) > 0){
   			$file =  'not_in_system.csv'; 
			file_put_contents($output_dir.$file, "Channel SKU\n", LOCK_EX);
 			foreach(json_decode($data['ReconciliationLog']['not_in_system'],1) as $chsku){
 			 	file_put_contents($output_dir.$file, $chsku."\n", FILE_APPEND | LOCK_EX);
 			}
 			header('Content-Description: File Transfer');
			header('Content-Disposition: attachment; filename='.basename($file));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($output_dir.$file));
			header("Content-Type: application/csv");
			readfile($output_dir.$file);
		}
		exit;
	}	
	
	public function getSource($channel_title = 'Marec_ES')  {	
	 
		$this->loadModel('ProChannel');
		$stockArray = []; 
 		$chs 	= $this->ProChannel->find('first', array('conditions' => ['channel_title'=>$channel_title],'fields' => array('channel_type','source')));		
		 
		if(count($chs)>0){
 			$stockArray['source'] = $chs['ProChannel']['source'];
			$stockArray['channel_type'] = $chs['ProChannel']['channel_type'];
		} 
		//pr($chs);
		//pr($stockArray);
		return $stockArray;
		exit;
	}	
	
  	public function UpdateStockCron(){
		
		$this->loadModel( 'Product' );
		$this->loadModel( 'Skumapping' );
		$this->loadModel( 'FbaUploadInventory' ); 
		$dir = WWW_ROOT. 'logs'.DS; 
		$products =  $this->Product->find('all', array('conditions' => array('Product.is_deleted' => 0),'fields' => ['product_sku','current_stock_level']));
		if(count($products) > 0){
			foreach($products as $p){
				$status = 'out of stock';
				if($p['Product']['current_stock_level'] > 0){
					$status = 'instock';
				}
				$this->Skumapping->query("UPDATE `skumappings` SET `stock_status` = '".$status."' WHERE `sku` = '".$p['Product']['product_sku']."'"); 
 				file_put_contents($dir.'UpdateStockCron_Product_'.date('dmy').'.log', $p['Product']['product_sku']."\t".$status."\n", FILE_APPEND | LOCK_EX);
			 
 			}
		}	
		$fba = $this->FbaUploadInventory->find('all', array('conditions' => array('status' => 0),'fields' => ['seller_sku','total_units']) );
  		if(count($fba) > 0){
			foreach($fba as $f){
				$status = 'out of stock';
				if($f['FbaUploadInventory']['total_units'] > 0){
					$status = 'instock';
				}
				$this->Skumapping->query("UPDATE `skumappings` SET `stock_status` = '".$status."' WHERE `channel_sku` = '".$f['FbaUploadInventory']['seller_sku']."'"); 
 				file_put_contents($dir.'UpdateStockCron_FBA_'.date('dmy').'.log', $f['FbaUploadInventory']['seller_sku']."\t".$status."\n", FILE_APPEND | LOCK_EX);

			}
		}		
		 
		exit;
	}
	
	private function getDataAws($channel = NULL)  {	
		 
		$this->loadModel( 'Skumapping' );	
		$url = 'http://techdrive.biz/api-xsensys-mapping.php'; 
		$curl = curl_init(); 
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FAILONERROR, true); 
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
		curl_setopt($curl, CURLOPT_POST, count($channel));
		curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($channel));  
		$result = curl_exec($curl); 						 
		$error = curl_error($curl); 
		$info = curl_getinfo($curl);
		curl_close($curl);	
		
		$data = json_decode($result);
		if(count($data) > 0 ){
			
			$this->loadModel( 'Skumapping' );		
			
			$msg = array();			
			$insert_str	= '';
			$upd_str    = '';
			$upd_count  = 0;
			$ins_count  = 0;
			$ins_log	= '';						
			$username	= $this->Session->read('Auth.User.username');
			
			/*$channelSku = $this->Skumapping->find('all', array('fields' => array('channel_sku'), 'conditions' => array('channel_name' => $channel['channel'])));
			foreach($channelSku as $val){
				$chSkus[trim($val['Skumapping']['channel_sku'])] = trim($val['Skumapping']['channel_sku']);
			}*/
			
			  
			foreach($data as $val)
			{			
				$barcode = isset($val->barcode) ? $val->barcode :'';						 
				if(strlen($barcode) > 1 && strlen($barcode) < 11){
				 $barcode = str_pad($barcode, 11, "0", STR_PAD_LEFT);	
				}
			
				$sku			=  $val->sku ;
				$asin			=  $val->asin ;
				$channel_name 	=  $val->channel_name ;
				$channel_sku 	=  $val->channel_sku;
 						
				if($sku != '' && $channel_sku != ''){
					$channelSku = $this->Skumapping->find('first', array('fields' => array('channel_sku'), 'conditions' => array('asin' => $asin,'channel_name' => $channel_name)));	
					if(count($channelSku) > 0){
						
						$update = array();			
						$update[] = " `username` =  '".$username."' ";										
						if($barcode!='') 	$update[] = " `barcode` =  '".$barcode."' ";
						if($asin!='') 		$update[] = " `asin` =  '".$asin."' ";
						if($sku!='') 		$update[] = " `sku` =  '".$sku."' ";
						if($channel_name!='') 		$update[] = " `channel_name` =  '".$channel_name."' ";
						
						$sql_query = '';
						
						if(count($update)>0){
							$w = 1; $update_col = '';
							$count_lenght  = count($update);
							foreach ($update as $k) {
								$update_col .= $k;
								if ($w < $count_lenght) {
									$update_col .=', ';
								}
								$w++;
							 }											 
							$sql_query = ' SET '.$update_col;
						 	$this->Skumapping->query("UPDATE `skumappings` $sql_query WHERE `id` = '".$channelSku['Skumapping']['id']."'");	
							
							$upd_count++;	
							//$upd_str .= $val->sku."\t" . $val->channel_sku ."\t" . $val->asin . "\t Updated!\n";	
						}
												
					}
					else{		
					
						$ins_count++;						
						//$chSkus[$channel_sku] = $channel_sku;	
																					
						$insert_str .= "('".$sku."','".$barcode."','".$asin."', '".$channel_name."','".$channel_sku."','".date("Y-m-d H:i:s")."','".$username."'),";
					 	$ins_log  .=  $sku."\t".$barcode."\t".$asin."\t".$channel_name."\t".$channel_sku."\t".date("Y-m-d H:i:s")."\n";
					    if($ins_count % 100 == 0){
							$insert_str = rtrim($insert_str,',');
							$this->Skumapping->query("INSERT INTO `skumappings` (`sku`, `barcode`, `asin`, `channel_name`, `channel_sku`, `added_date`, `username`)
							VALUES $insert_str");
							unset($insert_str);									
					   }
					}
				}else{
					 $msg = 'Please check `sku`And `channel_sku`!';
				} 
			
			}// end foreach
			
			
			if($insert_str != ''){
				$insert_str = rtrim($insert_str,',');
				
				$this->Skumapping->query("INSERT INTO `skumappings` (`sku`, `barcode`, `asin`, `channel_name`, `channel_sku`, `added_date`, `username`)
						VALUES $insert_str");
				unset($insert_str);
			}
			
			$output_dir = WWW_ROOT. 'logs'.DS.'mapping'.DS; 
			if (!is_dir($output_dir)) {
				mkdir($output_dir, 0777);         
			 }
			 if($upd_str !='' ){								
				file_put_contents($output_dir.'UPDATE_mapping_'.$channel_name.'_'.date('dmy').'.log', "\n*********".date('d-m-Y')."***********\n".$upd_str, FILE_APPEND | LOCK_EX);
			}
			if($ins_log !='' ){								
				file_put_contents($output_dir.'INSERT_mapping_'.$channel_name.'_'.date('dmy').'.log', "\n*********".date('d-m-Y')."***********\n".$ins_log, FILE_APPEND | LOCK_EX);
			}			
		}	
		//exit;
		//return $result ;		
	}	
	public function ChannelSkus(){	
			
		$this->layout = "skumapping"; 		
		$this->loadModel('User');	
		$this->loadModel('Skumapping');
  		$this->loadModel('SourceComany');	 
		$this->loadModel('SupplierDetail');
		$this->loadModel('SupplierMapping');
		$this->loadModel('ProductFilesLog');
		
		$purchaserSku = []; $productFiles = []; 
		 
		/*$all_purchaser = $this->User->find('all', array( 'conditions' => array( 'user_type' => 'purchaser' ),'fields'=>['first_name','last_name','username'] ) );
 		$this->set('all_purchaser', $all_purchaser );
		
		$all_supplier = $this->SupplierDetail->find('all', array('fields'=>['supplier_code','supplier_name'] ) );
 		$this->set('all_supplier', $all_supplier );
 
 		$getStores 	= $this->SourceComany->find('all', array(  'order' => 'channel_title ASC' ));
		$this->set('all_stores', $getStores );*/
	
		$perPage	=	50;
		$conditions = ['listing_status !=' => 'deleted'];
		if(isset($_REQUEST['searchkey']) && ($_REQUEST['searchkey'] != '')){
		  $conditions = ['OR'=>['sku LIKE'=>trim($_REQUEST['searchkey']).'%','channel_sku LIKE'=>trim($_REQUEST['searchkey']).'%']];
		}
		
		if(isset($_REQUEST['by']) && ($_REQUEST['by'] == 'channel')){
 			 $this->paginate = array(
					  'conditions' => ['channel_name'=>trim($_REQUEST['ch']),$conditions],	
					  'limit' => $perPage,	
					  'order' => 'Skumapping.added_date DESC',			
                       
        	 );	
		}else if(isset($_REQUEST['by']) && ($_REQUEST['by'] == 'store')){
 			 $this->paginate = array(
					  'conditions' => ['channel_name LIKE'=> trim($_REQUEST['ch']).'%',$conditions],	
					  'limit' => $perPage,	
					  'order' => 'Skumapping.added_date DESC',			
                       
        	 );	
		}else if(isset($_REQUEST['by']) && ($_REQUEST['by'] == 'supplier')){
 			 $this->paginate = array(
					  'conditions' => ['supplier LIKE'=> trim($_REQUEST['ch']).'%',$conditions],	
					  'limit' => $perPage,	
					  'order' => 'Skumapping.added_date DESC',			
                       
        	 );	
		}else if(isset($_REQUEST['by']) && ($_REQUEST['by'] == 'purchaser')){
  			$supplier_code['abc'] = 'abc';  
			$chs = $this->SupplierDetail->find('all', array('conditions' => array( 'purchaser LIKE' => '%'.trim($_REQUEST['ch']).'%'),'fields' => array('supplier_code','purchaser')));
			if(count($chs)>0){
				foreach($chs as $ch){
					$supplier_code[$ch['SupplierDetail']['supplier_code']] = $ch['SupplierDetail']['supplier_code'];
				}
 			 } 		
			 $this->paginate = array(
						  'conditions' => ['supplier'=>$supplier_code,$conditions],	
						  'limit' => $perPage,	
						  'order' => 'Skumapping.added_date DESC',			
						   
				 );	  
   		}		
		else{
		
			$this->paginate = array(
						  'conditions' => [$conditions],	
						  'limit' => $perPage,	
						  'order' => 'Skumapping.added_date DESC',			
						   
			 );		
		 }
		$all_product = $this->paginate('Skumapping');		
		 
		 $skus = [];
		 if(count($all_product) > 0 ){
			foreach($all_product as $sm){
  				$skus[] = $sm['Skumapping']['sku'];
			}	 
		}
		
		$this->loadModel('Product');
		$this->Product->unbindModel( array(
			'hasOne' => array(
				'ProductPrice' 
				),
			'hasMany' => array(
				'ProductLocation'
				)		
			) 
		 );
		 $pro_logs = [];
		 $pro = $this->Product->find('all', array('conditions'=> array('Product.product_sku'=>$skus),'fields' => array('Product.product_sku','Product.core_upload','Product.core_update','ProductDesc.info_upload','ProductDesc.info_update')));
		
		if(count($pro)>0){
			foreach($pro as $v){
				$pro_logs[$v['Product']['product_sku']] = [
															'core_upload'=>$v['Product']['core_upload'],
															'core_update'=>$v['Product']['core_update'],
															'info_upload'=>$v['ProductDesc']['info_upload'],
															'info_update'=>$v['ProductDesc']['info_update']
															];
			}
		} 
		
		$purchasers = [];
		$chs = $this->SupplierDetail->find('all', array('fields' => array('supplier_code','purchaser')));
 		if(count($chs)>0){
 			foreach($chs as $ch){
				$purchasers[$ch['SupplierDetail']['supplier_code']] = $ch['SupplierDetail']['purchaser'];
			}
		}
		$supplierMapping = $this->SupplierMapping->find('all', array('conditions'=> array('master_sku'=>$skus),'fields' => array('sup_code','master_sku')));
		if(count($supplierMapping)>0){
 			foreach($supplierMapping as $sm){
				$sku =  $sm['SupplierMapping']['master_sku'];;
				$purchaserSku[$sku] = $purchasers[$sm['SupplierMapping']['sup_code']];
			}
		}		
  		$this->set(compact('all_product','purchaserSku','pro_logs')); 
	}
	
	public function getMapReportCsv()  {	
		
		$this->loadModel('Skumapping');
		$this->loadModel('Product');	
		$this->loadModel('SupplierDetail');	
		$this->loadModel('SupplierMapping');
		$this->loadModel('ProductFilesLog');
					
 			
		$output_dir = WWW_ROOT. 'logs'.DS; 
		$channel = trim($this->request->data['channel']);
		$by = trim($this->request->data['by']);
		$msg['msg'] = '';
 		$file_name = $channel.'_'.$by.".csv";
		$file = fopen($output_dir.$file_name,"w") or die("File open error.");
		
		 $this->Product->unbindModel( array(
			'hasOne' => array(
				'ProductPrice' 
				),
			'hasMany' => array(
				'ProductLocation'
				)		
			) 
		 );
		 $pro_logs = [];
		 $pro = $this->Product->find('all', array('fields' => array('Product.product_sku','Product.core_upload','Product.core_update','ProductDesc.info_upload','ProductDesc.info_update')));
		
		if(count($pro)>0){
			foreach($pro as $v){
				$pro_logs[$v['Product']['product_sku']] = [
															'core_upload'=>$v['Product']['core_upload'],
															'core_update'=>$v['Product']['core_update'],
															'info_upload'=>$v['ProductDesc']['info_upload'],
															'info_update'=>$v['ProductDesc']['info_update']
															];
			}
		} 
		
		$supplier_purchaser = [];  
		$chs = $this->SupplierDetail->find('all', array('fields' => array('supplier_code','purchaser')));
		if(count($chs)>0){
			foreach($chs as $ch){
				$supplier_purchaser[$ch['SupplierDetail']['supplier_code']] = $ch['SupplierDetail']['purchaser'];
			}
		} 
		
		if($by == 'channel'){ 
			$map  = $this->Skumapping->find('all', array('conditions' => array( 'channel_name' => "{$channel}"))); 
		}else if($by == 'store'){ 
			$map  = $this->Skumapping->find('all', array('conditions' => array( 'channel_name LIKE' => "{$channel}%")));
		}else if($by == 'supplier'){ 
 			$map  = $this->Skumapping->find('all', array('conditions' => array( 'supplier' =>"{$channel}")));
		}else if($by == 'purchaser'){ 
 			$supplier_code['abc'] = 'abc';  
			$chs = $this->SupplierDetail->find('all', array('conditions' => array( 'purchaser LIKE' => "{$channel}%"),'fields' => array('supplier_code','purchaser')));
			if(count($chs)>0){
				foreach($chs as $ch){
					$supplier_code[$ch['SupplierDetail']['supplier_code']] = $ch['SupplierDetail']['supplier_code'];
				}
			}
  			$map  = $this->Skumapping->find('all', array('conditions' => array( 'supplier' => $supplier_code),'fields'=>['sku','channel_sku','supplier','map_change_by','map_change','change_margin','change_margin_by']));
		} 
 
		fputcsv($file,array('Purchaser','SKU','Channel sku','Core Upload Fields','Detailed Product Information','Channel SKU Mapping','Channel SKU Management')); 
 		if(count($map) > 0){
			foreach($map as $val){
				$msg['msg'] = "File generated."; 
				$purchaser = 'NA';
				$sku = $val['Skumapping']['sku'];
				$core_n = $core_d = '';
				$product_info_n = $product_info_d = '';
  				$sku = $val['Skumapping']['sku'];
 				/*$logs = $this->ProductFilesLog->find('first', array('conditions'=> array('sku LIKE'=>'%'.$sku.'%' ,'template'=>'CoreUploadFields'),'order'=>'id DESC')); 	*/			
				if(isset($pro_logs[$sku])){
					$core_n = $pro_logs[$sku]['core_upload'];
					$core_d = $pro_logs[$sku]['core_update'];
					$product_info_n = $pro_logs[$sku]['info_upload'];
					$product_info_d = $pro_logs[$sku]['info_update'];
				}
				/*$logs = $this->ProductFilesLog->find('first', array('conditions'=> array('sku LIKE'=>'%'.$sku.'%' ,'template'=>'DetailedProductInformation'),'order'=>'id DESC'));
				if(count($logs) > 0){
					$product_info_n = $logs['ProductFilesLog']['uploaded_by'];
					$product_info_d = $logs['ProductFilesLog']['added_date'];
				} */
				 
 				if(isset($supplier_purchaser[$val['Skumapping']['supplier']])){
					$purchaser = $supplier_purchaser[$val['Skumapping']['supplier']];
				}
				$channel_mapping = $change_margin = '';
				if($val['Skumapping']['map_change'] != ''){
					$channel_mapping = $val['Skumapping']['map_change_by'].':'.$val['Skumapping']['map_change'];
				}
				if($val['Skumapping']['change_margin'] != ''){
					$change_margin = $val['Skumapping']['change_margin_by'].':'.$val['Skumapping']['change_margin'];
				}
				
				fputcsv($file,array($purchaser,$val['Skumapping']['sku'],$val['Skumapping']['channel_sku'],$core_n.":".$core_d,$product_info_n.":".$product_info_d,$channel_mapping,$change_margin));  
				  
			}	
		}			
		fclose($file);
		$msg['file'] = "<div class='alert alert-success fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Download <a href='".Router::url('/', true)."logs/".$file_name."'>".$file_name."</a></div>"; 
		
	    echo json_encode($msg);
		
		exit;
	}
	
	public function	getFiltersData(){
 		 
		$this->layout = 'skumapping'; 			 
		$this->loadModel('User'); 
		$this->loadModel('SupplierDetail'); 
		$this->loadModel('SupplierMapping');
		$this->loadModel('SourceComany');						
 		$suppliers = []; 
		if(isset($_REQUEST['filter_by']) && $_REQUEST['filter_by'] == 'channel'){
 			$getStores 	= $this->SourceComany->find('all', array(  'order' => 'channel_title ASC' ));
			$suppliers_str = '<select class="form-control selectpicker" name="ch" id="ch" data-live-search="true"> ';
			
			foreach($getStores  as $get){
 				$selected ='';
				if($_REQUEST['ch'] == $get['SourceComany']['channel_title']){
					$selected = 'selected=selected';
				}
				$suppliers_str .= '<option value="'.$get['SourceComany']['channel_title'].'" '.$selected.'>'.$get['SourceComany']['channel_title'].'</option>';
  			}
			$suppliers_str .= '</select>'; 
 		} 
		else if(isset($_REQUEST['filter_by']) && $_REQUEST['filter_by'] == 'supplier'){
			$all_supplier = $this->SupplierDetail->find('all', array('fields'=>['supplier_code','supplier_name'] ) );
			$suppliers_str = '<select class="form-control selectpicker" name="ch" id="ch" data-live-search="true"> ';
			foreach($all_supplier  as $v){
				$selected ='';
				if($_REQUEST['ch'] == $v['SupplierDetail']['supplier_code']){
					$selected = 'selected=selected';
				}
 				$suppliers_str .= '<option value="'.$v['SupplierDetail']['supplier_code'].'" '.$selected.'>'.$v['SupplierDetail']['supplier_name'].'</option>';
  			}
			$suppliers_str .= '</select>'; 
 		} 	
		else if(isset($_REQUEST['filter_by']) && $_REQUEST['filter_by'] == 'purchaser'){
 			$all_purchaser = $this->User->find('all', array( 'conditions' => array( 'user_type' => 'purchaser' ),'fields'=>['first_name','last_name','username'] ) );
  			$suppliers_str = '<select class="form-control selectpicker" name="ch" id="ch" data-live-search="true">  ';
			foreach($all_purchaser  as $v){
 				$selected ='';
				if($_REQUEST['ch'] == $v['User']['username']){
					$selected = 'selected=selected';
				}
				$suppliers_str .= '<option value="'.$v['User']['username'].'" '.$selected .'>'.$v['User']['first_name'].' '.$v['User']['last_name'].'</option>';
  			}
			$suppliers_str .= '</select>'; 
 		}else{
			$suppliers_str = '<select class="form-control selectpicker" name="ch" id="ch" data-live-search="true"> ';
 				if($_REQUEST['ch'] == 'costbreaker'){
					$suppliers_str .= '<option value="costbreaker" selected=selected>CostBreaker</option>'; 
				}else{
					$suppliers_str .= '<option value="costbreaker">CostBreaker</option>';
				}
				if($_REQUEST['ch'] == 'marec'){
					$suppliers_str .= '<option value="marec" selected=selected>Marec</option>'; 
				}else{
					$suppliers_str .= '<option value="marec">Marec</option>';
				}
				if($_REQUEST['ch'] == 'rainbow'){
					$suppliers_str .= '<option value="rainbow" selected=selected>Rainbow</option>'; 
				}else{
					$suppliers_str .= '<option value="rainbow">Rainbow</option>';
				}
				
  			$suppliers_str .= '</select>'; 
		} 
		$data['supplier'] = $suppliers_str;
		$data['re'] = $_REQUEST;
		echo json_encode($data);    
		      
	
	exit;
	
	}
	
	public function	ProductFilesLog($sku = 0,$temp =''){
		$output_dir = WWW_ROOT. 'files'.DS; 
		$this->loadModel( 'ProductFilesLog' );
  		$data = $this->ProductFilesLog->find('first', array('conditions'=> array('sku LIKE'=>'%'.$sku.'%' ,'template'=>$temp),'order'=>'id DESC'));
  		//$data =  $this->ProductFilesLog->find('first', array('conditions' => array('id' => $id)));

		if(count($data) > 0){
   			$file = $data['ProductFilesLog']['file_name'];
			header('Content-Description: File Transfer');
			header('Content-Disposition: attachment; filename='.basename($data['ProductFilesLog']['uploaded_file']));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($output_dir.$file));
			header("Content-Type: application/csv");
			readfile($output_dir.$file);
		}
		exit;
 	}
	public function getChannelSkus($template = '')  {	
		
		$this->loadModel('Product');	
		$this->loadModel('Skumapping');			
		 
 		$output_dir = WWW_ROOT.'logs'.DS; 
		//$template = trim($this->request->data['template']);
		$msg['msg'] = '';
		 	
		$file_name =  $template."_".date('ymd').".csv";
		
		$file = fopen($output_dir.$file_name,"w") or die("File open error.");
		if($template == 'CoreUploadFields'){
			 $this->Product->unbindModel( array(
			'hasOne' => array(
				'ProductPrice' 
				),
			'hasMany' => array(
				'ProductLocation'
				)		
			) 
		 );
		 fputcsv($file,array('System SKU','Product Title','Brand','UPC/Barcode','Category','Weight(kg)','Shipping Length(mm)','Shipping Width(mm)','Shipping Height(mm)','Dangerous Good? Route(1=road & 2=air)','Fragile Mark','HSN code','Country of origin','Export Invoice Title','Supplier Code','Supplier Product Code')); 
			
			$products	=	$this->Product->find('all',array('fields'=>['Product.product_sku','Product.product_name','ProductDesc.brand','ProductDesc.barcode','ProductDesc.weight','ProductDesc.length','ProductDesc.width','ProductDesc.height','Product.route','Product.category_name','ProductDesc.frezile_mark','Product.country_of_origin','Product.export_invoice_title']));
			
			foreach($products as $val){
				$barcode	=	$this->GlobalBarcode->getLocalBarcode($val['ProductDesc']['barcode']);
 				fputcsv($file,array($val['Product']['product_sku'],$val['Product']['product_name'],$val['ProductDesc']['brand'],$barcode,$val['Product']['category_name'],$val['ProductDesc']['weight'],$val['ProductDesc']['length'],$val['ProductDesc']['width'],$val['ProductDesc']['height'],$val['Product']['route'],$val['ProductDesc']['frezile_mark'],$val['ProductDesc']['height'],$val['Product']['country_of_origin'],$val['Product']['export_invoice_title'],$val['ProductDesc']['height']));  
	
			}
			$msg['msg'] = "File generated.";
		}
		
		/*$map  = $this->Skumapping->find('all', array('conditions' => array( 'channel_name' => "{$channel}")));   
		
 		foreach($map as $val){
  			$msg['msg'] = "File generated."; 
 			fputcsv($file,array($val['Skumapping']['sku'],$val['Skumapping']['channel_sku'],$val['Skumapping']['asin'],$val['Skumapping']['title'],$val['Skumapping']['source'],$val['Skumapping']['channel_name'],$val['Skumapping']['channel_type'],$val['Skumapping']['referral_fee'],$val['Skumapping']['margin'],$val['Skumapping']['status']));  
			  
		}	*/
					
		fclose($file);
		//$msg['file'] = "<div class='alert alert-success fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Download <a href='".$this->webroot."logs/".$template.".csv'>".$template.".csv</a></div>"; 
		
	    header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.basename($file_name));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($output_dir.$file_name));
		header("Content-Type: application/csv");
		readfile($output_dir.$file_name);
		
		exit;
	}
}

?>