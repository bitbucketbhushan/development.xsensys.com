<?php
//error_reporting(0);
class FbaShipmentsController extends AppController
{
    
    var $name = "FbaShipments";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('printTest'));			
    }
 	
   public function index() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Shipments" );	
		$this->loadModel('FbaShipment');
		 
		$this->paginate = array('order'=>'FbaShipment.id DESC','limit' => 20 );
		$all_shipments = $this->paginate('FbaShipment');
		$this->set( 'all_shipments', $all_shipments );				
		
	} 
	
	public function ShipmentItems($shipment_id = null) 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Shipments" );	
		$this->loadModel('FbaShipment');
		 
		if(isset($shipment_id) && $shipment_id > 0 ){
			$shipment_items = $this->FbaShipment->find('first', array('conditions'=>array('FbaShipment.id'=>$shipment_id),'order'=>'FbaShipment.id DESC'));
			$this->set( 'shipment_items', $shipment_items );	
		}
	} 
	 
	public function ExportShipment($shipment_id = null) 
    {	
		$this->layout = "index"; 	
		$this->set( "title","Export Shipments" );	
		$this->loadModel('FbaShipment');		
		$this->loadModel('FbaBundleSku'); 	 
		
		if(isset($shipment_id) && $shipment_id > 0 ){
			$shipment_items = $this->FbaShipment->find('first', array('conditions'=>array('FbaShipment.id'=>$shipment_id),'order'=>'FbaShipment.id DESC'));
			$this->set( 'shipment_items', $shipment_items );
			 
			$bundle = $this->FbaBundleSku->find("all");
			foreach($bundle as $val){
				$bundle_mapping[$val['FbaBundleSku']['bundle_sku']][$val['FbaBundleSku']['master_sku']] =  array('qty'=>$val['FbaBundleSku']['qty'],'title'=>$val['FbaBundleSku']['title'],'bid'=>$val['FbaBundleSku']['bid']);
			} 
			 $this->set( 'bundle_mapping', $bundle_mapping );	
		}
		
		$this->loadModel( 'FbaPackagingList' );						
		$package = $this->FbaPackagingList->find('all');
		
		 foreach($package as $val){
			$envelope[$val['FbaPackagingList']['id']] = $val['FbaPackagingList']['packaging_name'];
		 
		 }
		 $this->set( 'envelope', $envelope );
		
	}
	
	public function getExportShipment($shipment_inc_id = null) 
    {	
		 		
		$this->loadModel('FbaShipmentItemsPo');		
		 
		$shipment_po_items = $this->FbaShipmentItemsPo->find('all', array('conditions'=>array('FbaShipmentItemsPo.shipment_inc_id'=>$shipment_inc_id)));
	 
		return $shipment_po_items ;	 
		 
			
  	}

	
	public function SaveExportShipmentAjax() 
    {	
		//  pr($this->request->data ); exit; 
		if(count($this->request->data) > 0){
		
			$form_data = $this->request->data;
			$this->loadModel('FbaShipment');	 
			//$this->loadModel('FbaShipmentsPo');		
			$this->loadModel('FbaShipmentItemsPo');	  
			 
			/***********UPDATE SHIPMENT STATUS***********/
			$ship['id'] 	= $form_data['shipment_inc_id'];
			$ship['status'] = 'preparing';
			$this->FbaShipment->saveAll( $ship ); 
			 
			/***********ADD/UPDATE SHIPMENT PO ITEMS***********/
			
			$file_path = WWW_ROOT .'fba_shipments/';	
	 		@unlink($file_path.'slips/SL_'.$form_data['fnsku'].'.pdf');
	   		@unlink($file_path.'labels/LA_'.$form_data['fnsku'].'.pdf');
			
			$poItemData['username']			= $this->Session->read('Auth.User.username'); 
			$poItemData['added_date']		= date('Y-m-d H:i:s');				
			$poItemData['merchant_sku'] 	= $form_data['merchant_sku'];
			$poItemData['shipment_id']  	= $form_data['shipment_id'];
			$poItemData['shipment_inc_id']  = $form_data['shipment_inc_id'];
			$poItemData['fnsku']  	 		= $form_data['fnsku'];
			$poItemData['prep_qty']    		= $form_data['shipped_qty'] > 0 ? $form_data['shipped_qty'] : $form_data['prep_qty'];				
			$poItemData['master_sku']  		= $form_data['master_sku'];
			$poItemData['shipped_qty'] 		= $form_data['shipped_qty'];	
						
			/**
			Check PO Iyems are exist.				
			*/
			$po_items = $this->FbaShipmentItemsPo->find("first", array('conditions' => 
				array('FbaShipmentItemsPo.fnsku' => $form_data['fnsku'],'FbaShipmentItemsPo.shipment_inc_id' => $form_data['shipment_inc_id'])));
			
			if(count($po_items) > 0){
				$poItemData['id'] = $po_items['FbaShipmentItemsPo']['id'];
			}
			
			if($form_data['shipped_qty'] !='' ){	
				$this->FbaShipmentItemsPo->saveAll( $poItemData ); 
				$this->generateSlipPdf($form_data['items_inc_id'], $form_data['shipped_qty']);
				$this->generateLabelPdf($form_data['items_inc_id'],$form_data['shipped_qty']);
			}
				
			$msg['msg'] = 'Shipment details of '.$form_data['fnsku'].' saved successfully.' ;
			$msg['error'] = '';			
		
		}else{
		
			$msg['msg'] = 'Please enter Shipment details.' ;
			$msg['error'] = 'yes';
		}		 	
		echo json_encode($msg);
		exit; 
	} 
	
	public function SaveExportShipment() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","Export Shipments" );	
		
		$form_data = $this->request->data;
		// pr($form_data );exit; 
		
		$this->loadModel('FbaShipment');				
		$this->loadModel('FbaShipmentItemsPo');	 
			
		/***********UPDATE SHIPMENT STATUS***********/
		$ship['id'] 	= $form_data['shipment_inc_id'];
		$ship['status'] = 'preparing';
		$this->FbaShipment->saveAll( $ship );
		
		foreach($form_data['poitems'] as $fnsku => $val){	
			$poItemData = array();
			$poItemData['shipment_id']  	= $form_data['shipment_id'];
			$poItemData['shipment_inc_id']  = $form_data['shipment_inc_id'];
			$poItemData['merchant_sku'] 	= $val['merchant_sku'];
			$poItemData['master_sku']   	= $val['master_sku'];
			$poItemData['barcode'] 			= $val['barcode'];			
			$poItemData['fnsku']  	 		= $fnsku;
			$poItemData['prep_qty']    		= $val['qty'] > 0 ? $val['qty'] : $val['prep_qty'];				
			$poItemData['shipped_qty']  	= $val['qty'];	
			$poItemData['username']			= $this->Session->read('Auth.User.username'); 
			$poItemData['added_date']		= date('Y-m-d H:i:s');	
						
			/**
			Check PO Iyems are exist.				
			*/
			$po_items = $this->FbaShipmentItemsPo->find("first", array('conditions' => array('FbaShipmentItemsPo.fnsku' => $fnsku,'FbaShipmentItemsPo.shipment_inc_id' => $form_data['shipment_inc_id'])));
			
			if(count($po_items) > 0){
				$poItemData['id'] = $po_items['FbaShipmentItemsPo']['id'];
			}
			
			if($val['qty'] !='' ){	
				$this->FbaShipmentItemsPo->saveAll( $poItemData ); 
				//$this->generateSlipPdf($val['items_inc_id'],$val['qty']);
				//$this->generateLabelPdf($val['items_inc_id'],$val['qty']);
			}				
			
		}		
		//$this->Session->setFlash('Shipment details saved.', 'flash_success');
		 		
		$this->redirect(Router::url('/', true).'FbaPicklists/createPickList/'.$form_data['shipment_inc_id']);
		 
		exit; 
	}
  
	 
	public function generateSlipPdf( $id = null, $qty = null, $isAjax = 0 )
	{	 	 
		if($id > 0 && $qty > 0){	
				 
			$this->loadModel('FbaShipmentItem');
			$_item = $this->FbaShipmentItem->find('first', array('conditions'=>array('FbaShipmentItem.id'=>$id)));		 
			$fnsku = $_item['FbaShipmentItem']['fnsku'];
			$title = $_item['FbaShipmentItem']['title'];
			if(strlen($title) > 100 ){
				$final_title = substr($title,0,90). '...' . substr($title,-20); 
			}else{
				$final_title = $title ;
			}
			
			$cssPath = WWW_ROOT .'css/';
				
			$html = '<meta charset="utf-8">';
			$html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			$html .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>';
			$html .= '<meta content="" name="description"/>';
			$html .= '<meta content="" name="author"/>';
			
			$html .= '<body>';
			$html .= '<div id="label" style="padding-left: 0px;padding-top: 15px;padding-bottom: 0px;">';
			$html .= '<div class="container">';
			$html .= '<table style="padding:1px 0;">';
			$html .= '<tr><td style="font-size:20px;font-family:Verdana, Arial, Helvetica, sans-serif; text-align:center;"><strong>Total Quantity : '.$qty.'</strong> </td></tr>';
			$html .= '<tr><td style="font-size:20px;font-family:Verdana, Arial, Helvetica, sans-serif; text-align:center; ">'.$fnsku.'</td></tr>';
			$html .= '<tr><td style="font-size:10px;font-family:Verdana, Arial, Helvetica, sans-serif ">'.$final_title.'</td></tr>';
			
			$html .= '</table>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</body>';
			$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
			
			 
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF(); 
			$dompdf->set_paper(array(0, 0, 189, 120), 'portrait'); 
			$dompdf->load_html($html, Configure::read('App.encoding'));
			$dompdf->render();
				
			$file_to_save = WWW_ROOT .'fba_shipments/slips/SL_'.$fnsku.'.pdf';	
			 
			file_put_contents($file_to_save, $dompdf->output()); 
			 
			$msg['msg'] = 'Slip generated.';				
			$msg['error'] = ''; 
		}else{
			$msg['msg'] = 'Qty is not valid.';				
			$msg['error'] = 'yes'; 
		}
		
		if($isAjax > 0){
			echo json_encode($msg);
			exit;
		}
	}
	
	public function printSlip( $id = null )
	{
	 	if($id == 0 ){
			$id  = $this->request->data['id'];
		}		
		$qty  = $this->request->data['qty'];
		$this->loadModel('FbaShipmentItem');
		$_item = $this->FbaShipmentItem->find('first', array('conditions'=>array('FbaShipmentItem.id' => $id)));		 
		$fnsku = $_item['FbaShipmentItem']['fnsku'];
		$file_to_save = WWW_ROOT .'fba_shipments/slips/SL_'.$fnsku.'.pdf';	
		//if(!file_exists($file_to_save)){
			$this->generateSlipPdf($id, $qty);
		//} 
		
		$printerId = $this->getThermalPrinter();
			
		if($printerId != 'Printer_Not_Found'){ 	 
			$serverPath =  Router::url('/', true) .'fba_shipments/slips/SL_'.$fnsku.'.pdf';	
			$sendData = array(
				'printerId' => $printerId,
				'title' => 'Now Print',
				'contentType' => 'pdf_uri',
				'content' => $serverPath,
				'source' => 'Direct'					
			);
			//print_r($sendData);
			App::import( 'Controller' , 'Coreprinters' );
			$Coreprinter = new CoreprintersController();
			//$d = $Coreprinter->toPrint( $sendData );
			//print_r($d);
			 
			$shipdata['slip_print']		 =  'FbaShipmentItem.slip_print + 1'; ;
			$shipdata['slip_print_date'] =  "'". date('Y-m-d H:i:s')."'"; 
			$shipdata['slip_print_user'] =  "'".$this->Session->read('Auth.User.username')."'";	
			$this->FbaShipmentItem->updateAll( $shipdata,array('FbaShipmentItem.id' => $id) );
						
			$msg['msg'] = 'Slip printed.';				
			$msg['error'] = '';
		}else{
			$msg['msg'] = 'Printer not found for user: '. $this->Session->read('Auth.User.username'); 
			$msg['error'] = 'yes';
		}
		echo json_encode($msg);
		exit;
	}
	
	
	public function generateLabelPdf_22022018( $id = null, $qty = null, $isAjax = 0)
	{
		if($id > 0 && $qty > 0){
			
			$this->loadModel('FbaShipmentItem');
			$_item = $this->FbaShipmentItem->find('first', array('conditions'=>array('FbaShipmentItem.id'=>$id)));			
			$fnsku = $_item['FbaShipmentItem']['fnsku'];
			$title = $_item['FbaShipmentItem']['title'];
			$this->getBarcode($_item['FbaShipmentItem']['fnsku']);
			
			if(strlen($title) > 38){
				$final_title = substr($title,0,20). '...' . substr($title,-20); 
			}else{
				$final_title = $title; 
			}
			 
			$cssPath = WWW_ROOT .'css/';	 
			 
			$html  = '';
					
			if($qty < 10){
				 
				for($i = 0; $i < $qty; $i++){
				
					$html .= '<meta charset="utf-8">';
					$html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
					$html .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>';
					$html .= '<meta content="" name="description"/>';
					$html .= '<meta content="" name="author"/>';
					
					$html .= '<body>';
					$html .= '<div id="label" style="padding-left: 0px;padding-top: 15px;padding-bottom: 0px;">';
					$html .= '<div class="container">';
					$html .= '<table style="padding:1px 0;">';
					$html .= '<tr><td style="text-align:center;"><img src="'. Router::url('/', true) .'fba_shipments/barcode/'.$fnsku.'.png"></td></tr>';
					$html .= '<tr><td style="font-size:9px;font-family:Verdana, Arial, Helvetica, sans-serif ">'.$final_title.'</td></tr>';
					$html .= '<tr><td style="font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif">New</td></tr>';
					$html .= '</table>';
					$html .= '</div>';
					$html .= '</div>';
					$html .= '</body>';
					$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				}
				 
				//$printerId = $this->getThermalPrinter();
				$set_paper = array(0, 0, 189, 99);
				
			}else{			
			
				$html = '<!DOCTYPE html>';
				$html .= '<html>'; 
				$html .= '<head>';
				$html .= '<title>Label for PDF Generation</title>';	
				$html .= '</head>';
				$html .= '<body>';
				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				$html .= '<div >';
				$html .= '<div class="container">';
				$html .='<table class="header row" style="border-bottom:0px;margin-bottom:0px; margin-top:0px;padding-top: 0px;">';
				$html .= '<tbody>';
				$html .= '<tr>';
				$r=0;
				 for($i=0; $i<$qty; $i++){
					 $r++;
					 $pb = 9; $pt = 40;
					 if($i >1 && $i % 3 == 0){$html .= '</tr><tr>';} 
					  
					 if($r >1 && $r % 19 == 0){$pb = '1';} if($r >1 && $r % 20 == 0){$pb = '1';}   if($r >1 && $r % 21 == 0){$pb = '1';}
					 if($r < 4  || $r % 22 == 0){$pt = 80;}  
						
						$html .= '<td valign="top" width="33%"  style="text-align:center;padding-left:13px; padding-bottom: '.$pb.'px; padding-top: '.$pt.'px;">';
						$html .= '<table width="90%" align="center">';
						//$html .= '<tr><td width="100%"><img src="'. Router::url('/', true) .'fba_shipments/barcode/'.$fnsku.'.png" width="195" ></td></tr>';
						$html .= '<tr><td width="100%" style="text-align:left;"><img src="'. Router::url('/', true) .'fba_shipments/barcode/'.$fnsku.'.png"></td></tr>';
						$html .= '<tr><td width="100%" style="text-align:left;font-size:10px;font-family:Verdana, Arial, Helvetica, sans-serif ">'.$final_title.'</td></tr>';
						$html .= '<tr><td width="100%" style="text-align:left;font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif; padding:0px; " height="10">New</td></tr>';
						$html .= '</table>';
		
						$html .= '</td>';
				
				 }
				$html .= '</tr>';
			
				
				$html .= '</tbody>';
				$html .= '</table>';
				$html .= '</div> ';
				$html .= '</div>';
				$html .= '</body>';
				$html .= '</html>'; 
				 
				//$printerId	= $this->getLaserPrinter();
				$set_paper  = 'A4';
				 
				} 								  
				//echo $html;//exit;
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF(); 
				$dompdf->set_paper($set_paper, 'portrait');//$dompdf->set_paper($set_paper, 'portrait');
				//$dompdf->set_paper(array(0, 0, 595, 842), 'portrait');
				$dompdf->load_html($html, Configure::read('App.encoding'));
				$dompdf->render();
				//$dompdf->stream('test.pdf');			
							
				$file_to_save = WWW_ROOT .'fba_shipments/labels/LA_'.$fnsku.'.pdf';	
				 
				file_put_contents($file_to_save, $dompdf->output());
				$msg['msg'] = 'Label PDF Generated.';
				$msg['error'] = '';
					 
			}else{			
				$msg['msg'] = 'Please enter valid quantity.';
				$msg['error'] = 'yes';
			}
			
			
			if($isAjax > 0){
				echo json_encode($msg);
				exit;
			}
	}
	
	public function generateLabelPdf( $id = null, $qty = null, $isAjax = 0)
	{
		if($id > 0 && $qty > 0){
			
			$this->loadModel('FbaShipmentItem');
			$_item = $this->FbaShipmentItem->find('first', array('conditions'=>array('FbaShipmentItem.id'=>$id)));			
			$fnsku = $_item['FbaShipmentItem']['fnsku'];
			$title = $_item['FbaShipmentItem']['title'];
			$this->getBarcode($_item['FbaShipmentItem']['fnsku']);
			
			$final_title = substr($title,0,20). '...' . substr($title,-20); 
			 
			$cssPath = WWW_ROOT .'css/';	 
			 
			$html  = '';
					
			if($qty < 10){
				 
				for($i = 0; $i < $qty; $i++){
				
					$html .= '<meta charset="utf-8">';
					$html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
					$html .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>';
					$html .= '<meta content="" name="description"/>';
					$html .= '<meta content="" name="author"/>';
					
					$html .= '<body>';
					$html .= '<div id="label" style="padding-left: 0px;padding-top: 15px;padding-bottom: 0px;">';
					$html .= '<div class="container">';
					$html .= '<table style="padding:1px 0;">';
					$html .= '<tr><td><img src="'. Router::url('/', true) .'fba_shipments/barcode/'.$fnsku.'.png" width="195" ></td></tr>';
					$html .= '<tr><td style="font-size:9px;font-family:Verdana, Arial, Helvetica, sans-serif ">'.$final_title.'</td></tr>';
					$html .= '<tr><td style="font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif">New</td></tr>';
					$html .= '</table>';
					$html .= '</div>';
					$html .= '</div>';
					$html .= '</body>';
					$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				}
				 
				//$printerId = $this->getThermalPrinter();
				$set_paper = array(0, 0, 189, 91);
				
			}else{			
			
				$html = '<!DOCTYPE html>';
				$html .= '<html>'; 
				$html .= '<head>';
				$html .= '<title>Label for PDF Generation</title>';	
				$html .= '</head>';
				$html .= '<body>';
				$html .= '<div >';
				$html .= '<div class="container">';
				$html .='<table class="header row" style="border-bottom:0px;margin-bottom:0px; margin-top:0px;padding-top: 0px;">';
				$html .= '<tbody>';
				$html .= '<tr>';
				$r=0;
				 for($i=0; $i<$qty; $i++){
					 $r++;
					 $pr ='37';  $pb ='17'; $pt= 21;
					 if($i >1 && $i % 3 == 0){$html .= '</tr><tr>';}
					 if($r >1 && $r % 3 == 0){$pr = '0';}
					 if($r >1 && $r % 19 == 0){$pb = '2';} if($r >1 && $r % 20 == 0){$pb = '2';}   if($r >1 && $r % 21 == 0){$pb = '2';}
					 /*if($r >1 && $r % 22 == 0){$pt = '10';} if($r >1 && $r % 23 == 0){$pt = '10';} if($r >1 && $r % 24 == 0){$pt = '10';}
					*/	
						$html .= '<td valign="top" width="100%"  style="padding-right: '.$pr.'px; padding-bottom: '.$pb.'px; padding-top: '.$pt.'px;">';
						$html .= '<table width="100%">';
						$html .= '<tr><td width="100%"><img src="'. Router::url('/', true) .'fba_shipments/barcode/'.$fnsku.'.png" width="195" ></td></tr>';
						$html .= '<tr><td width="100%" style="font-size:9px;font-family:Verdana, Arial, Helvetica, sans-serif ">'.$final_title.'</td></tr>';
						$html .= '<tr><td width="100%" style="font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif; padding:0px; " height="10">New</td></tr>';
						$html .= '</table>';
		
						$html .= '</td>';
				
				 }
				$html .= '</tr>';
			
				
				$html .= '</tbody>';
				$html .= '</table>';
				$html .= '</div> ';
				$html .= '</div>';
				$html .= '</body>';
				$html .= '</html>'; 
				 
				//$printerId	= $this->getLaserPrinter();
				$set_paper  = 'A4';
				 
				} 								  
				//echo $html;//exit;
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF(); 
				$dompdf->set_paper($set_paper, 'portrait');//$dompdf->set_paper($set_paper, 'portrait');
				//$dompdf->set_paper(array(0, 0, 595, 842), 'portrait');
				$dompdf->load_html($html, Configure::read('App.encoding'));
				$dompdf->render();
				//$dompdf->stream('test.pdf');			
							
				$file_to_save = WWW_ROOT .'fba_shipments/labels/LA_'.$fnsku.'.pdf';	
				 
				file_put_contents($file_to_save, $dompdf->output());
				$msg['msg'] = 'Label PDF Generated.';
				$msg['error'] = '';
					 
			}else{			
				$msg['msg'] = 'Please enter valid quantity.';
				$msg['error'] = 'yes';
			}
			
			
			if($isAjax > 0){
				echo json_encode($msg);
				exit;
			}
	}
	
	public function printLabel( $id = null)
	{
	 	if($id == 0 )
		{
			$id = $this->request->data['id'];			
		}
		if($this->request->data['qty'] > 0){
			
			$qty = $this->request->data['qty'];
			
			$this->loadModel('FbaShipmentItem');
			$_item = $this->FbaShipmentItem->find('first', array('conditions'=>array('FbaShipmentItem.id'=>$id)));
			$fnsku = $_item['FbaShipmentItem']['fnsku'];
			$file_to_save = WWW_ROOT .'fba_shipments/labels/LA_'.$fnsku.'.pdf';	
			//if(!file_exists($file_to_save)){
				$this->generateLabelPdf($id, $qty);
			//}
			
			if($qty < 10){			
				$printerId = $this->getThermalPrinter();				
			}else{			
				$printerId	= $this->getLaserPrinter();
			} 
			
			if($printerId !='Printer_Not_Found'){ 
				$serverPath =  Router::url('/', true) .'fba_shipments/labels/LA_'.$fnsku.'.pdf';
				$sendData = array(
					'printerId' => $printerId,
					'title' => 'Now Print',
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'					
				);
				//print_r($sendData);
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				//$d = $Coreprinter->toPrint( $sendData );
				//print_r($d);
				 
				$shipdata['label_print']        = 'FbaShipmentItem.label_print + 1';
				$shipdata['label_print_date'] 	= "'". date('Y-m-d H:i:s')."'"; 
				$shipdata['label_print_user']   = "'".$this->Session->read('Auth.User.username')."'";	
				$this->FbaShipmentItem->updateAll( $shipdata,array('FbaShipmentItem.id' => $id) );
				 
				$msg['msg'] = 'Label printed.';
				$msg['error'] = '';
			}else{
				$msg['msg'] = 'Printer not found for user: '. $this->Session->read('Auth.User.username');
				$msg['error'] = 'yes';
			}			 
		}
		
		echo json_encode($msg);
		exit;
			
	}
	
	public function getLaserPrinter()
	{
		$this->autoRander = false;
		$this->loadModel( 'User' );
		$this->loadModel( 'PcStore' );
		$userId = $this->Session->read('Auth.User.id');

		$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
						
		$getPcText = $getUser['User']['pc_name'];  
		$paramsLaser = array(
			'conditions' => array(
				'PcStore.pc_name' => $getPcText,
				'PcStore.printer_name' => 'Brother HL-6180DW series'
			)
		);
		
		$printer = $this->PcStore->find('first', $paramsLaser)	;
		if(count($printer) > 0){
			$LaserT88 = $printer['PcStore']['printer_id'];
		}else{
			$LaserT88 = 'Printer_Not_Found';
		}
		
		return $LaserT88;	
	}
	
	public function getThermalPrinter()
	{
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );						
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name']; //getenv('COMPUTERNAME');
			$paramsThermal= array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name' => 'FBADymo'
				)
			);
			$printer = $this->PcStore->find('first', $paramsThermal)	;
			if(count($printer) > 0){
				$ThermalPrinter = $printer['PcStore']['printer_id'];
			}else{
				$ThermalPrinter = 'Printer_Not_Found';
			}
		
		return $ThermalPrinter;
	}	
	public function getBarcode( $fnsku ){ 
		
		$imgPath = WWW_ROOT .'fba_shipments'.DS.'barcode'.DS;   
 		
		if(!file_exists($imgPath.$fnsku.'.png')){
		
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
			
			$font = new BCGFontFile(WWW_ROOT.'fonts/Verdana.ttf', 14);
			$colorFront = new BCGColor(0, 0, 0);
			$colorBack = new BCGColor(255, 255, 255); 
			
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(43);
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->setFont($font);
			$code128->setLabel($fnsku);
			$code128->parse($fnsku);
			
			//Drawing Part
			$imgOrder128 = $fnsku.".png";
			$imgOrder128path = $imgPath.$fnsku.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);  
		}
		      
	}
	public function getBarcode_22022018( $fnsku ){ 
		
		$imgPath = WWW_ROOT .'fba_shipments'.DS.'barcode'.DS;   
 		
		if(!file_exists($imgPath.$fnsku.'.png')){
		
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
			
			$font = new BCGFontFile(WWW_ROOT.'fonts/Verdana.ttf', 8);
			$colorFront = new BCGColor(0, 0, 0);
			$colorBack = new BCGColor(255, 255, 255); 
			
			$code128 = new BCGcode128();
			$code128->setScale(1);
			$code128->setThickness(40);
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->setFont($font);
			$code128->setLabel($fnsku);
			$code128->parse($fnsku);
			
			//Drawing Part
			$imgOrder128 = $fnsku.".png";
			$imgOrder128path = $imgPath.$fnsku.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);  
		}
		      
	}
			
	public function getSkuMapping($seller_sku = null)
	{		
		$this->loadModel( 'Skumapping' );	
		$_items = $this->Skumapping->find('first', array('conditions'=>array('Skumapping.channel_sku'=>$seller_sku)));
		if(count($_items) > 0){
			return  array('sku'=>$_items['Skumapping']['sku'],'barcode'=>$_items['Skumapping']['barcode']);
		}else{
			return 0;
		}
	}
	
	public function getSkuBarcode($sku = null)
	{		
		$this->loadModel( 'Product' );	
		$_items = $this->Product->find('first', array('conditions'=>array('Product.product_sku'=>$sku)));
		if(count($_items) > 0){
			return  $this->Components->load('Common')->getLocalBarcode($_items['ProductDesc']['barcode']);  
		}else{
			return 0;
		}
	}
	
	public function updateBarcode()
	{		
		$this->loadModel( 'Product' );	
		$this->loadModel( 'Skumapping' );	
		$_products = $this->Product->find('all', array('fields'=>array('Product.product_sku','ProductDesc.barcode')) );
		
		foreach($_products as $val){
			
			$conditions = array('sku' => $val['Product']['product_sku']);	
			$mapdata['barcode'] = "'".$val['ProductDesc']['barcode']."'";		
			$this->Skumapping->updateAll( $mapdata, $conditions );
			
		}		
	}
	
	public function getFbaPackaging($merchant_sku = null)
	{		
		$this->loadModel( 'FbaPackaging' );			
		$_items = $this->FbaPackaging->find('first', array('conditions'=>array('FbaPackaging.merchant_sku'=>$merchant_sku)));
		if(count($_items) > 0){
			return  $_items['FbaPackaging']['packaging'];
		}else{
			return '';
		}
	}
	public function getFbaBoxing($shipment_id = null)
	{		
		$data = array(); 
		$this->loadModel( 'FbaShipmentBoxItem' );	 		
		$_items = $this->FbaShipmentBoxItem->find('all', array('conditions'=>array('FbaShipmentBoxItem.shipment_id'=>$shipment_id)));
		if(count($_items) > 0){
			foreach($_items as $val){ 
				$data[$val['FbaShipmentBoxItem']['merchant_sku']][] =  $val['FbaShipmentBoxItem']['box_number'];
			}
		} 
		
		return $data;
		 
	}
    public function UploadShipment()
	{
		$this->layout = 'index';
	}
	
	public function Uploadfile()
	{
		$this->loadModel('FbaShipment');
		$this->loadModel('FbaShipmentItem');
		$this->loadModel('FbaPackagingList');	 
		 
		$filetemppath =	$this->data['Shipment']['Uploadfile']['tmp_name'];
		$pathinfo     = pathinfo($this->data['Shipment']['Uploadfile']['name']);
             
		$fileNameWithoutExt = $pathinfo['filename'];
		$fileExtension = strtolower($pathinfo['extension']);
		
		if($fileExtension == 'tsv')
		{
		
			$filename = $fileNameWithoutExt.'_'.date('ymd_his').'_'.$this->Session->read('Auth.User.username').".".$fileExtension;		
			$upload_file = WWW_ROOT. 'fba_shipments'.DS.'shipments'.DS.$filename ; 
			move_uploaded_file($filetemppath,$upload_file); 
			
			$row = 0; $add_count = 0;
			$shipment_inc_id = 0; 
			$handle = fopen($upload_file, "r");
			while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {
			   $num = count($data);
			   $row++;
			   if($row == 1){			 
				   $shipmentdata['shipment_id'] = $data[1];	
			   }else if($row == 2){
				   $shipmentdata['shipment_name'] = $data[1];	
			   }else if($row == 3){
				   $shipmentdata['plan_id'] = $data[1];	
			   }else if($row == 4){
				   $shipmentdata['ship_to'] = $data[1];	
			   }else if($row == 5){
				   $shipmentdata['total_skus'] = $data[1];	
			   }else if($row == 6){
				   $shipmentdata['total_units'] = $data[1];	
			   }else if($row == 7){
				   $shipmentdata['pack_list'] = $data[1];	
			   }
			   
			   if($row == 8){
					$fba_res = $this->FbaShipment->find('first',array('conditions' => array('shipment_id' => $shipmentdata['shipment_id'])));
				
					if(count($fba_res) > 0){
						$shipment_inc_id = $fba_res['FbaShipment']['id'];
					}else{
						$shipmentdata['status']     = 'ready_to_prepare';
						$shipmentdata['added_date'] = date('Y-m-d H:i:s');
						$shipmentdata['username']   = $this->Session->read('Auth.User.username');	
						$this->FbaShipment->saveAll( $shipmentdata );
						$shipment_inc_id = $this->FbaShipment->getLastInsertId();
					}
				}		
					
			   if(($row >= 10) && ($shipment_inc_id > 0)){
					
					$shipment_items = array();	$packaging_list = array();			 
					$fba_res = $this->FbaShipmentItem->find('first',array('conditions' => array('shipment_inc_id' => $shipment_inc_id,'fnsku' => $data[3])));
					if(count($fba_res) == 0){
						$add_count++;
						$shipment_items['shipment_inc_id'] = $shipment_inc_id;
						$shipment_items['merchant_sku'] =  $data[0] ;
						$shipment_items['title'] =  $data[1] ;
						$shipment_items['asin'] =  $data[2] ;
						$shipment_items['fnsku'] =  $data[3] ;
						$shipment_items['external_id'] =  $data[4] ;
						$shipment_items['item_condition'] =  $data[5] ;
						$shipment_items['who_will_prep'] =  $data[6] ;
						$shipment_items['prep_type'] =  $data[7] ;
						$shipment_items['who_will_label'] =  $data[8] ;
						$shipment_items['shipped'] =  $data[9] ;
						$shipment_items['added_date'] = date('Y-m-d H:i:s');
						$shipment_items['username'] = $this->Session->read('Auth.User.username');
						$this->FbaShipmentItem->saveAll( $shipment_items );
						
						
						$pack_res = $this->FbaPackagingList->find('first',array('conditions' => array('packaging_name' => $data[7])));
						if(count($pack_res) == 0){
							$packaging_list['packaging_name'] = $data[7] ;
							$packaging_list['added_date']     = date('Y-m-d H:i:s');
							$packaging_list['username']       = $this->Session->read('Auth.User.username');						
							$this->FbaPackagingList->saveAll( $packaging_list );	
						}
					}				 
				}			
			}
			
			fclose($handle);
			if($add_count > 0){
				$this->Session->setFlash($add_count . " items added.", 'flash_success');
			}else{
				$this->Session->setFlash("All items are already exist.", 'flash_danger');
			}
			
		 }
		 else{
			 $this->Session->setFlash("Invalid file format, Please upload TSV file.", 'flash_danger');
		 }
		 
		$this->redirect($this->referer());
    
	}
	
	public function updateStatus()
	{
		$this->loadModel('FbaShipment');
		  
		$shipmentdata['status'] = $this->request->data['status'];
		$shipmentdata['id'] 	= $this->request->data['id'];
		$shipmentdata['added_date'] = date('Y-m-d H:i:s');
		$shipmentdata['username']   = $this->Session->read('Auth.User.username');	
		$this->FbaShipment->saveAll( $shipmentdata ); 
		echo 'done';
   		exit;
	}
	
	public function updatePackaging()
	{		
		$this->loadModel( 'FbaPackaging' );	 
		
		$conditions = array('merchant_sku' => $this->request->data['merchant_sku']);	
						
		if ($this->FbaPackaging->hasAny($conditions)){
			$DataUpdate['packaging'] = "'".$this->request->data['packaging']."'";		
			$this->FbaPackaging->updateAll( $DataUpdate, $conditions );		
			unset($DataUpdate);		
						
		}else{
			$ResponseData['merchant_sku'] = $this->request->data['merchant_sku'];	
			$ResponseData['packaging'] = $this->request->data['packaging'];	
			$this->FbaPackaging->saveAll( $ResponseData);	
		}	
			  		
		$msg['msg'] = 'Packaging is assigned to : '.$this->request->data['merchant_sku'];
		$msg['error'] = '';
		 
		echo json_encode($msg);
		exit;
	}
	
	public function addPackaging()
	{		
		$this->loadModel( 'FbaPackaging' );	 	
		$this->loadModel( 'FbaPackagingList' );	 
			
		$conditions = array('merchant_sku' => $this->request->data['merchant_sku']);	
						
		if ($this->FbaPackaging->hasAny($conditions)){
			$DataUpdate['packaging'] = "'".$this->request->data['packaging']."'";		
			$this->FbaPackaging->updateAll( $DataUpdate, $conditions );		
			unset($DataUpdate);		
						
		}else{
			$ResponseData['merchant_sku'] = $this->request->data['merchant_sku'];	
			$ResponseData['packaging'] = $this->request->data['packaging'];	
			$this->FbaPackaging->saveAll( $ResponseData);	
		}	
			
				
		$pack_res = $this->FbaPackagingList->find('first',array('conditions' => array('packaging_name' => $this->request->data['packaging'])));
		if(count($pack_res) == 0){
			$packaging_list['packaging_name'] = $this->request->data['packaging'] ;
			$packaging_list['added_date']     = date('Y-m-d H:i:s');
			$packaging_list['username']       = $this->Session->read('Auth.User.username');						
			$this->FbaPackagingList->saveAll( $packaging_list );			  		
			 
			$msg['msg'] = 'Packaging is added and assigned to : '.$this->request->data['merchant_sku'];
			
		}else{
			$msg['msg'] = 'Packaging is already exist.';
		}
		$msg['error'] = '';
		 
		echo json_encode($msg);
		exit;
	}
		
	public function getTitles($shipment_inc_id = null) 
    {	
		$this->loadModel('FbaShipmentItem'); 	
		$this->FbaShipmentItem->unbindModel( array( 'belongsTo' => array( 'FbaShipment' ) ) );
		$shipment_items = $this->FbaShipmentItem->find('all', array('conditions'=>array('FbaShipmentItem.shipment_inc_id'=>$shipment_inc_id)));
		
		foreach( $shipment_items as $val){
			$title[$val['FbaShipmentItem']['fnsku']] = $val['FbaShipmentItem']['title']  ; 
		}	 	
		return $title;		
	}
	
	public function getProcessedQty($shipment_inc_id = null, $fnsku = null ) 
    {
		$this->loadModel('FbaItemLocation'); 
		$items = $this->FbaItemLocation->find('first', array('conditions'=>array('FbaItemLocation.shipment_inc_id'=>$shipment_inc_id,'FbaItemLocation.fnsku'=>$fnsku)));
		$qty = 0;
		if(count($items) > 0){
			 
				$qty = $items['FbaItemLocation']['process_qty'];
			 
		}
		return $qty;
		 
	}
	
	public function ExportInvoice($shipment_inc_id = null) 
    {
		$this->loadModel('FbaShipmentItemsPo');		
		$this->loadModel('PurchaseOrder');		 
		$this->loadModel('FbaBundleSku'); 	
		$this->loadModel('FbaShipment'); 	 
		$this->loadModel('FbaPackaging'); 
		$currency_exchange_rate = $this->getCurrencyRate();
		
		$bundle = $this->FbaBundleSku->find("all");
		foreach($bundle as $val){
			$bundle_mapping[$val['FbaBundleSku']['bundle_sku']][$val['FbaBundleSku']['master_sku']] =  array('qty'=>$val['FbaBundleSku']['qty'],'title'=>$val['FbaBundleSku']['title'],'bid'=>$val['FbaBundleSku']['bid']);
		}
		
	    $shipment  = $this->FbaShipment->find('first', array('conditions'=>array('FbaShipment.id'=>$shipment_inc_id))); 
	 
		$shipment_po_items = $this->FbaShipmentItemsPo->find('all', array('conditions'=>array('FbaShipmentItemsPo.shipment_inc_id'=>$shipment_inc_id))); 
	 
		if(count($shipment_po_items) > 0){
			
			$titleArray = $this->getTitles($shipment_inc_id);
			App::import('Vendor', 'PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
				 
			$row = 1;
			$BorderAll =  array(
			  'borders' => array(
				'allborders' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			  )
			);
			$rightBorder = array(
			  'borders' => array(
					'right' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					),	
					'left' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)																
			  )
			);									
			$style = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				)
			);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':H'.$row);
			
			$row++;			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($BorderAll);				
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'INVOICE');	
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':H'.$row);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($style);			
			$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);	
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(20)->setBold(true);					
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('CCCCCC');
			
			$row++;		
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
			
			$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(80);	
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getAlignment()->applyFromArray(
				array(
						 'vertical'   => PHPExcel_Style_Alignment::VERTICAL_TOP,
						 'rotation'   => 0,
						 'wrap'       => true)
			);
			$address = '';
			$strs = explode(",",$shipment['FbaShipment']['ship_to']);
			for($i = 0; $i< count($strs); $i++){
				if($i < 2) $sep = "\n";
				elseif($i == 4) $sep = "\n";
				else  $sep = ",";
				$address .= trim($strs[$i]).$sep;
			}
			$address = rtrim($address, ",");
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':C'.$row);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Euraco Group Ltd
			36 Harbour Reach
			La Rue De Carteret
			Jersey, JE2 4HR");
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setWrapText(true);
			
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E'.$row.':H'.$row);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $address);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setWrapText(true);
			
			$row++;				
			$BStyle = array(
						  'borders' => array(
							'top' => array(
							  'style' => PHPExcel_Style_Border::BORDER_THIN
							),
							'bottom' => array(
							  'style' => PHPExcel_Style_Border::BORDER_THIN
							)
						  )
						);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($rightBorder);	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(11)->setBold(true);	
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'INVOICE TO/DELIVERY TO:');					
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($BStyle);																						
			unset($BStyle);
			
			$row++;
			$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(80);	
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getAlignment()->applyFromArray(
				array(
						 'vertical'   => PHPExcel_Style_Alignment::VERTICAL_TOP,
						 'rotation'   => 0,
						 'wrap'       => true)
			);
			
			//$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($rightBorder);	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':C'.$row);
			
		
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $address);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setWrapText(true);
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E'.$row.':H'.$row);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$row,"INVOICE REF:ECG". date('dmY').																 																	    "\nINVOICE DATE:". date('m/d/Y').
																	 "\nINVOICE TERMS : 14 Days" );
			$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setWrapText(true); 
				
			$row++;		
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':H'.$row);
			
			$BStyle = array(
			  'borders' => array(
				'allborders' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			  )
			);
			$row++; 
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($BStyle);
			
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$row, 'QTY')
						->setCellValue('B'.$row, 'DESCRIPTION')
						->setCellValue('C'.$row, 'OUR REF')	
						->setCellValue('D'.$row, 'Category')	
						->setCellValue('E'.$row, 'HS Code')	
						->setCellValue('F'.$row, 'Country of Origin')							 
						->setCellValue('G'.$row, 'UNIT PRICE')
						->setCellValue('H'.$row, 'LINE TOTAL(USD)');
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);	
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(37);	
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);	
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(11)->setBold(true);	
			$sub_total = 0;
			 
			$right = array(
			  'borders' => array(
				'right' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			  )
			);
			$left = array(
			  'borders' => array(
				'left' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			  )
			);
			$bottom = array(
			  'borders' => array(
				'bottom' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			  )
			);
			 
			foreach($shipment_po_items as $v){	
				$val = $v['FbaShipmentItemsPo'];
				if($this->getProcessedQty($val['shipment_inc_id'],$val['fnsku']) > 0){	
					$row++;	
					
					$main_price = 0.0;
					if(isset($bundle_mapping[$val['master_sku']])){
					
						foreach($bundle_mapping[$val['master_sku']] as $sku => $bun){
														
						 $po_prices = $this->PurchaseOrder->find('first', array('conditions'=>array('purchase_sku' => $sku),'fields' =>'price','order' => 'id DESC'));
						 if(count($po_prices) > 0) {
							 $main_price += $po_prices['PurchaseOrder']['price'] * $bun['qty'];			 
						  } 
						}
						 
					}else{
					
						$po_prices = $this->PurchaseOrder->find('first', array('conditions'=>array('purchase_sku' => $val['master_sku']),'fields' =>'price','order'=>'id DESC'));
						if(count($po_prices) > 0) {
							$main_price = $po_prices['PurchaseOrder']['price'];
						}
					}
					
					$price = $main_price/2;
					 
					
					$pack = $this->FbaPackaging->find('first', array('conditions'=>array('FbaPackaging.merchant_sku'=>$val['merchant_sku']))); 				
					$hs_code = $country_of_origin = $category = '';
					if(count($pack) > 0){
						$category = $pack['FbaPackaging']['category'];
						$country_of_origin = $pack['FbaPackaging']['country_of_origin'];
						$hs_code = $pack['FbaPackaging']['hs_code'];
					}
					
					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$row, $this->getProcessedQty($val['shipment_inc_id'],$val['fnsku']))														
								->setCellValue('B'.$row, $titleArray[$val['fnsku']])
								->setCellValue('C'.$row, $val['fnsku'])
								->setCellValue('D'.$row, $category)
								->setCellValue('E'.$row, $hs_code)
								->setCellValue('F'.$row, $country_of_origin)
								->setCellValue('G'.$row, $price * $currency_exchange_rate['USD'])
								->setCellValue('H'.$row, ($price * $currency_exchange_rate['USD']) * $val['shipped_qty']);	
							
							
							
					$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setWrapText(true); 		
					$objPHPExcel->getActiveSheet()->getStyle('D'.$row.':H'.$row)->getNumberFormat()->setFormatCode
	(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($left);	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($right);	 
					$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($right);	 
					$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($right);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($right);	 
					$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($right);	
					$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($right);	
					$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($right);	
					$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray($right);	
					
					$sub_total += ($price * $currency_exchange_rate['USD']) * $val['shipped_qty'];
				}
			}		 
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($bottom);	 
			$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($bottom);	 
			$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($bottom);
			$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($bottom);	 
			$objPHPExcel->getActiveSheet()->getStyle('E'.$row.':H'.$row)->applyFromArray($bottom);	
			// $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$mr.':E'.$row+$mr);
			$row++;			
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$row, '')														
						->setCellValue('B'.$row, '')
						->setCellValue('C'.$row, '')
						->setCellValue('D'.$row, '')
						->setCellValue('E'.$row, '')
						->setCellValue('F'.$row, '')
						->setCellValue('G'.$row, 'SUBTOTAL')
						->setCellValue('H'.$row, $sub_total ) ;
			$objPHPExcel->getActiveSheet()->getStyle('G'.$row.':H'.$row)->applyFromArray($BStyle);	
			$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->getNumberFormat()->setFormatCode
(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			
			$row++;			
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$row, '')														
						->setCellValue('B'.$row, '')
						->setCellValue('C'.$row, '')
						->setCellValue('D'.$row, '')
						->setCellValue('E'.$row, '')
						->setCellValue('F'.$row, '')
						->setCellValue('G'.$row, 'VAT')
						->setCellValue('H'.$row, '0.00') ;
			$objPHPExcel->getActiveSheet()->getStyle('G'.$row.':H'.$row)->applyFromArray($BStyle);	
			$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->getNumberFormat()->setFormatCode
(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
 			  			
			$row++;		
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':B'.$row);	
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$row, "PAYMENT INFORMATION(14 DAYS CREDIT)")														
						->setCellValue('B'.$row, '')
						->setCellValue('C'.$row, '')
						->setCellValue('D'.$row, '')
						->setCellValue('E'.$row, '')
						->setCellValue('F'.$row, '')
						->setCellValue('G'.$row, 'TOTAL')
						->setCellValue('H'.$row, $sub_total) ;
						
			$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->getNumberFormat()->setFormatCode
(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($BStyle);	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setWrapText(true);
			
			 
			$file_name = 'Fbainvoice_'.$shipment_inc_id.'_'.date('dmHis').'.xlsx';										
			$file_path = WWW_ROOT.'fba_invoices/'.$file_name;
			
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');		
			$objWriter->save($file_path);	
			file_put_contents(WWW_ROOT.'logs/fba_invoice.log', $file_name."\t".$this->Session->read('Auth.User.username')."\t". date('Y-m-d H:i:s'), LOCK_EX);
			$result =  WWW_ROOT.'fba_invoices/'.$file_name; 
			header('Content-Description: File Transfer');
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename='.basename($file_path));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file_path));   
			readfile($file_path); 
		}		 		
		exit; 
	}
	
	public function resetProcess($shipment_inc_id = null, $fnsku = null)
	{
		$this->loadModel('FbaShipmentItem');
		$this->loadModel('FbaItemLocation');		
		$shipdata['status']		 =  0 ;		 
		$this->FbaShipmentItem->updateAll( $shipdata,array('FbaShipmentItem.shipment_inc_id' => $shipment_inc_id,'FbaShipmentItem.fnsku' => $fnsku) );
	  	$this->FbaItemLocation->updateAll( $shipdata,array('FbaItemLocation.shipment_inc_id' => $shipment_inc_id,'FbaItemLocation.fnsku' => $fnsku) );
		$this->redirect($this->referer());
	}
	
	public function getCurrencyRate()
	{	 			
		$this->loadModel('CurrencyExchangeRate');	
		
		$curencies = array('INR','EUR','USD');
		$url='http://www.floatrates.com/daily/gbp.xml';
		$xml = file_get_contents($url);
		$curArray = json_decode(json_encode(simplexml_load_string($xml)),true);
		if(count($curArray) > 0 ){
			$this->CurrencyExchangeRate->query("TRUNCATE currency_exchange_rates");
			foreach($curArray['item'] as $val) {
				if (in_array($val['targetCurrency'],$curencies)) {			
					$curency['base_currency'] = 'GBP';
					$curency['exchange_rate'] = $val['exchangeRate'];	
					$curency['target_currency'] = $val['targetCurrency'];					
					$this->CurrencyExchangeRate->saveAll($curency);	
				}
			}		
		}
		
		$curency = $this->CurrencyExchangeRate->find("all"); 
		foreach($curency as $val){
			$curencyArr[$val['CurrencyExchangeRate']['target_currency']] = $val['CurrencyExchangeRate']['exchange_rate'];
		}
				
		return $curencyArr; 			
 	}
  
  
  
	public function getAwsBundleSkus(){
 		$this->loadModel( 'FbaBundleSku' ); 
 		$url = 'http://77.81.234.110/api-master-sku-bundle.php'; 
		$curl = curl_init(); 
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FAILONERROR, true); 
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
		$result = curl_exec($curl); 						 
		$error = curl_error($curl); 
		$info = curl_getinfo($curl);
		curl_close($curl);	
		$res = json_decode($result,true);
		if($error == ''){		
			$this->FbaBundleSku->query("TRUNCATE fba_bundle_skus");
			foreach($res['bundle']  as $val){
				if($val['bundle_sku']){
					//$data['bundle_id'] 	= $val['bundle_id'];
					//$data['item_id'] 	= $val['item_id'];
					$data['bundle_sku'] = $val['bundle_sku'];
					$data['master_sku'] = $val['sku'];
					$data['qty'] 		= $val['qty'];
					$data['title'] 		= $val['title'];
					$this->FbaBundleSku->saveAll( $data );
				}
			}
			
			 
			$this->Session->setFlash("Bundle Sku Mapping is updated.", 'flash_success');
			
			//echo 'done.';
		}else{
			 $this->Session->setFlash($error, 'flash_danger');
			//echo $error;
		} 		
		$this->redirect($this->referer());
		exit;
	}   
}