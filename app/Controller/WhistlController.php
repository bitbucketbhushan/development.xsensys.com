<?php
error_reporting(1);
App::uses('Folder', 'Utility');
App::uses('File', 'Utility'); 
class WhistlController extends AppController
{
    
    var $name = "Whistl";    
    var $components = array('Session', 'Common', 'Upload','Auth');    
    var $helpers = array('Html','Form','Common','Session');
    var $licence_code = 'P13422';
   public function beforeFilter()
   {
		parent::beforeFilter();
		$this->layout = false;
		$this->Auth->Allow(array('applyWhistl'));
		$this->common = $this->Components->load('Common'); 
     }
    
	public function index(){} 
	
	public function applyWhistl($order_id = null){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'PostalServiceDesc' );
	  
		$return 		  = array();
		$msg 			  = '';
		 
		$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id,'MergeUpdate.status' => 0 , 'MergeUpdate.delevery_country' => 'United Kingdom')));	
		 
		if(count($orders) >0){
 			foreach($orders  as $orderItems){
			 
				$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $orderItems['MergeUpdate']['product_order_id_identify'],'MergeUpdate.status' => 0 ,'MergeUpdate.delevery_country' => 'United Kingdom')));	
					
				if(count($orderItem) > 0)
				{
					$this->loadModel( 'Product' );
					$this->loadModel( 'ProductDesc' );
					$this->loadModel( 'Country' );	
				
					$quantity 	   = $orderItem['MergeUpdate']['quantity'];
					$order_id 	   = $orderItem['MergeUpdate']['order_id'];
					$sub_source    = $orderItem['MergeUpdate']['source_coming'];
					$ebay_source   = '';
					if(strpos(strtolower($sub_source), 'ebay') !== false){
						$ebay_source = 'ebay';
					}
					 
					$sdata = $this->getSplitOrderDetails($orderItems['MergeUpdate']['product_order_id_identify']);
					//pr($sdata);
						
					 $dim = array($sdata['packet_width'],$sdata['packet_height'],$sdata['packet_length']);
					 asort($dim);
					 $final_dim = array_values($dim) ;							
					
					/* $whPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('max_weight >=' => $sdata['packet_weight'],'length >=' => $final_dim[2],'width >=' => $final_dim[1],'height >=' => $final_dim[0]),'order'=>'per_unit ASC'));*/
					 
					  $whPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $sdata['packet_weight'],'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'Whistl', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
					//  pr( $whPostal);
					  
					$all_whistl = array();  
					$whistlFee = 0; 
					if(count($whPostal) > 0)
					{
						foreach($whPostal as $rv){
							$per_item 			= $rv['PostalServiceDesc']['per_item'];
							$psd_id 			= $rv['PostalServiceDesc']['id'];								 						
							$all_whistl[$psd_id] = $per_item ;	 
						} 
						 
						$whistlFee   = min($all_whistl);	
						$post_id    = array_search($whistlFee, $all_whistl); 
						$post_whistl = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));	
						
  					  
						if($orderItem['MergeUpdate']['packet_weight'] == 0){
							$data['packet_weight']  = $sdata['packet_weight'];
						}
						
						$data['track_id'] 			= '';
						$data['reg_post_number'] 	= '';
						$data['reg_num_img'] 		= '';
						$data['postal_service'] 	= 'Standard';
						$data['service_name'] 		= $post_whistl['PostalServiceDesc']['service_name'];
						$data['provider_ref_code'] 	= $post_whistl['PostalServiceDesc']['provider_ref_code'];
						$data['service_id'] 		= $post_whistl['PostalServiceDesc']['id'];
						$data['service_provider'] 	= 'whistl';								 
						$data['id'] 				= $orderItem['MergeUpdate']['id'];
						 	 
						file_put_contents(WWW_ROOT .'logs/whistl_'.date('ymd').'.log' , $orderItem['MergeUpdate']['product_order_id_identify']."\t".$orderItem['MergeUpdate']['service_id']."\n",  FILE_APPEND|LOCK_EX);	
						
						$this->loadModel( 'MergeUpdate' );				 
					 	$this->MergeUpdate->saveAll( $data );									
						echo $msg .= $orderItem['MergeUpdate']['product_order_id_identify']."\t";
								 
						}
					else{
						 $msg =  'This order may have over weight or any other issue.<br>';
					}
							 
  			    }
				else{
				 $msg =  ' No order found.<br>';
				}		
						
				file_put_contents(WWW_ROOT."logs/whistl_app_by_order_".date('Ymd').".log", date('Y-m-d H:i:s')."\t". $order_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);	
			}
		}
	}
	
	public function customWhistl($order_id = null){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'PostalServiceDesc' );
	  
		$return 		  = array();
		$msg 			  = '';
		 
		 if($order_id == ''){
			 $orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status' => 0 , 'MergeUpdate.delevery_country' => 'United Kingdom')));
		 }else{
			$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id, 'MergeUpdate.delevery_country' => 'United Kingdom')));	
		}
		// pr( $orders); exit;
		if(count($orders) >0){
 			foreach($orders  as $orderItems){
			 
				$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $orderItems['MergeUpdate']['product_order_id_identify'],'MergeUpdate.delevery_country' => 'United Kingdom')));	
					
				if(count($orderItem) > 0)
				{
					$this->loadModel( 'Product' );
					$this->loadModel( 'ProductDesc' );
					$this->loadModel( 'Country' );	
				
					$quantity 	   = $orderItem['MergeUpdate']['quantity'];
					$order_id 	   = $orderItem['MergeUpdate']['order_id'];
					$sub_source    = $orderItem['MergeUpdate']['source_coming'];
					$ebay_source   = '';
					if(strpos(strtolower($sub_source), 'ebay') !== false){
						$ebay_source = 'ebay';
					}
					 
					$sdata = $this->getSplitOrderDetails($orderItems['MergeUpdate']['product_order_id_identify']);
					pr($sdata);
						
					 $dim = array($sdata['packet_width'],$sdata['packet_height'],$sdata['packet_length']);
					 asort($dim);
					 $final_dim = array_values($dim) ;							
					
					/* $whPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('max_weight >=' => $sdata['packet_weight'],'length >=' => $final_dim[2],'width >=' => $final_dim[1],'height >=' => $final_dim[0]),'order'=>'per_unit ASC'));*/
					 
					  $whPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $sdata['packet_weight'],'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'Whistl', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
					//  pr( $whPostal);
					  
					$all_whistl = array();  
					$whistlFee = 0; 
					if(count($whPostal) > 0)
					{
						foreach($whPostal as $rv){
							$per_item 			= $rv['PostalServiceDesc']['per_item'];
							$psd_id 			= $rv['PostalServiceDesc']['id'];								 						
							$all_whistl[$psd_id] = $per_item ;	 
						} 
						 
						$whistlFee   = min($all_whistl);	
						$post_id    = array_search($whistlFee, $all_whistl); 
						$post_whistl = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));	
						
  					  
						if($orderItem['MergeUpdate']['packet_weight'] == 0){
							$data['packet_weight']  = $sdata['packet_weight'];
						}
						
						$data['track_id'] 			= '';
						$data['reg_post_number'] 	= '';
						$data['reg_num_img'] 		= '';
						$data['postal_service'] 	= 'Standard';
						$data['service_name'] 		= $post_whistl['PostalServiceDesc']['service_name'];
						$data['provider_ref_code'] 	= $post_whistl['PostalServiceDesc']['provider_ref_code'];
						$data['service_id'] 		= $post_whistl['PostalServiceDesc']['id'];
						$data['service_provider'] 	= 'whistl';								 
						$data['id'] 				= $orderItem['MergeUpdate']['id'];
						pr($data);
						//file_put_contents(WWW_ROOT."logs/royalmail_".$orderItem['MergeUpdate']['product_order_id_identify'].".log",print_r($_details,true));
						 
						file_put_contents(WWW_ROOT .'logs/whistl_'.date('ymd').'.log' , $orderItem['MergeUpdate']['product_order_id_identify']."\t".$orderItem['MergeUpdate']['service_id']."\n",  FILE_APPEND|LOCK_EX);	
						
						$this->loadModel( 'MergeUpdate' );				 
					 	$this->MergeUpdate->saveAll( $data );									
						echo $msg .= $orderItem['MergeUpdate']['product_order_id_identify']."\t";
								 
						}
					else{
						 $msg =  'This order may have over weight or any other issue.<br>';
					}
							 
  			    }
				else{
				 $msg =  ' No order found.<br>';
				}		
						
				file_put_contents(WWW_ROOT."logs/whistl_app_by_order_".date('Ymd').".log", date('Y-m-d H:i:s')."\t". $order_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);	
			}
		}
	}
	
	public function getLabel($splitOrderId = ''){ 
		
		$this->autoRender = false;
		$this->layout = '';
		//$order_id = '3376332';//'3377768';//'3376332';
		//$splitOrderId = $order_id.'-1';
		$order_id = explode("-",$splitOrderId)[0];
		App::import( 'Controller' , 'Cronjobs' );		
		$objController 	= new CronjobsController();
		$getorderDetail	= $objController->getOpenOrderById($order_id);
		 
		$customer_info = $getorderDetail['customer_info'];
		 
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		
		$addressData = [];
		$_address1 = $customer_info->Address->Address1; 
		$address1 = ''; $address2  = ''; $address3 = '';
		$lines = explode("\n", wordwrap(htmlentities($_address1).' '.htmlentities($customer_info->Address->Address2).' '.htmlentities($customer_info->Address->Address3), '30'));
		
		if(isset($lines[0]) && $lines[0] != ''){
			$address1  =  trim($lines[0]) ;
		}
		if(isset($lines[1]) && $lines[1] != ''){
			$address2 =  trim($lines[1]) ;
		}
		if(isset($lines[2]) && $lines[2] != ''){
			$address3 = trim($lines[2]);
		}
		
		$addressData['address1'] 		= $address1;
		$addressData['address2'] 		= $address2;
		$addressData['address3'] 		= $address3;
		
		$clientAddress = $this->getCountryAddress($addressData);
		$senderAddress =  $this->getSenderInfo(strtolower($getorderDetail['sub_source']));
		//pr($senderAddress);
		 
		if(!file_exists(WWW_ROOT.'img/orders/barcode/'.$splitOrderId)){
			$this->getBarcodeOutside($splitOrderId);
		} 

		$body = '<div class="container" style="width:530px; height:320px;  border:1px solid #000; border-radius:10px; padding:15px;">
			<table  align="center" width="70%">
			<tbody>
			<tr>
			<td valign="top" width="70%" style="padding:5px; font-size:11px;">
				<p style="margin:2px; font-weight:bold;">Whistl Returns:</p>		 
				<p style="margin:2px;">'.$senderAddress['FromCompany'].'</p> 
				<p style="margin:2px;">'.$senderAddress['FromAddress'].'</p>
				<p style="margin:2px;">'.$senderAddress['FromCity'].'</p>
				<p style="margin:2px;">'.$senderAddress['FromPostCode'].'</p> 
			</td>
			<td valign="top"><img src="'.WWW_ROOT.'whistl/whistl.png"></td>
			<td valign="top"><img src="'.WWW_ROOT.'whistl/whistl-rm.png"></td>
			
			</tr>
			<tr>
			<td valign="top" colspan="3" style="padding:5px; border:1px solid #000; font-size:16px;">
			<p style="margin:2px;">'. ucwords($customer_info->Address->FullName).'</p> 
			<p style="margin:2px;">'.$clientAddress.'</p>
			<p style="margin:2px;">'.ucwords($customer_info->Address->Town).'</p>
			<p style="margin:2px;">'.ucwords($customer_info->Address->Region).'</p>
			<p style="margin:2px;">'.ucwords($customer_info->Address->PostCode).'</p>
			<!--<p style="margin:2px;">'.ucwords($customer_info->Address->Country).'</p>-->
			</td>
			</tr>
			<tr><td colspan="3"><center><img src="'.WWW_ROOT.'img/orders/barcode/'.$splitOrderId.'.png" width="224px"><div style="margin-top: -15px;">'.$splitOrderId.'</div></center></td></tr>
			</tbody>
			</table>
 
		</div>';
		
		$html = '<meta charset="utf-8">
				 <meta http-equiv="X-UA-Compatible" content="IE=edge">
				 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				 <meta content="" name="description"/>
				 <meta content="" name="author"/>					 
				 <body>'.$body.'</body>';
		 
	//	 echo $html;//exit;
		//$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
  	 
		/*$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
		$dompdf->load_html(utf8_decode($html));
		$dompdf->render();
		//$dompdf->stream("OrderIdslip-$splitOrderId.pdf");
		
		$imgPath = WWW_ROOT .'whistl/'; 
		$path = Router::url('/', true).'whistl/';		
		$name	=	'Label_'.$splitOrderId.'.pdf';
		
		 file_put_contents($imgPath.$name, $dompdf->output());*/
		return $html;
		exit;
	
	} 
	
 	public function genrateLabel($splitOrderId){ 
		
		$this->layout = '';
	 
		$order_id = explode("-",$splitOrderId)[0];
		App::import( 'Controller' , 'Cronjobs' );		
		$objController 	= new CronjobsController();
		$getorderDetail	= $objController->getOpenOrderById($order_id);
		 
		$customer_info = $getorderDetail['customer_info'];
		 
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		
		$addressData = [];
		$_address1 = $customer_info->Address->Address1; 
		$address1 = ''; $address2  = ''; $address3 = '';
		$lines = explode("\n", wordwrap(htmlentities($_address1).' '.htmlentities($customer_info->Address->Address2).' '.htmlentities($customer_info->Address->Address3), '30'));
		
		if(isset($lines[0]) && $lines[0] != ''){
			$address1  =  trim($lines[0]) ;
		}
		if(isset($lines[1]) && $lines[1] != ''){
			$address2 =  trim($lines[1]) ;
		}
		if(isset($lines[2]) && $lines[2] != ''){
			$address3 = trim($lines[2]);
		}
		
		$addressData['address1'] 		= $address1;
		$addressData['address2'] 		= $address2;
		$addressData['address3'] 		= $address3;
		
		if($splitOrderId == '3411802-1'){
			$address1  = 'Poppy + Ted Unit 7 ';
			$address2  = '(GF)Castle View Business Centre';
		//	$address3  = 'Gas House Road';
			$addressData['address1'] 		= $address1;
			$addressData['address2'] 		= $address2;
			$addressData['address3'] 		= $address3;
		}
		
		$clientAddress = $this->getCountryAddress($addressData);
		$senderAddress =  $this->getSenderInfo(strtolower($getorderDetail['sub_source']));
		//pr($senderAddress);
		 
		if(!file_exists(WWW_ROOT.'img/orders/barcode/'.$splitOrderId.'.png')){
			$this->getBarcodeOutside($splitOrderId);
		} 

		$body = '<div class="container" style="width:550px; height:150px;margin-top: -25px;">
			<table  align="center" width="100%">
			<tbody>
			<tr>
			<td valign="top" width="70%" style="padding:5px; font-size:11px;">
				<p style="margin:1px; font-weight:bold;">Whistl Returns:</p>		 
				<p style="margin:1px;">'.$senderAddress['FromCompany'].'</p> 
				<p style="margin:1px;">'.$senderAddress['FromAddress'].'</p>
				<p style="margin:1px;">'.$senderAddress['FromCity'].'</p>
				<p style="margin:1px;">'.$senderAddress['FromPostCode'].'</p> 
			</td>
			<td valign="top"><img src="'.WWW_ROOT.'whistl/whistl.png"></td>
			<td valign="top"><img src="'.WWW_ROOT.'whistl/whistl-rm.png"></td>
			
			</tr>
			<tr>
			<td valign="top" colspan="3" style="padding:5px; border:1px solid #000; font-size:16px;">
			<p style="margin:1px;">'. ucwords($customer_info->Address->FullName).'</p> 
			<p style="margin:1px;">'.$clientAddress.'</p>
			<p style="margin:1px;">'.ucwords($customer_info->Address->Town).'</p>
			<p style="margin:1px;">'.ucwords($customer_info->Address->Region).'</p>
			<p style="margin:1px;">'.ucwords($customer_info->Address->PostCode).'</p>
			<!--<p style="margin:1px;">'.ucwords($customer_info->Address->Country).'</p>-->
			</td>
			</tr>
			<tr><td valign="top" colspan="3" style="margin-top: -15px;"><center><img src="'.WWW_ROOT.'img/orders/barcode/'.$splitOrderId.'.png" width="224px"><div style="margin-top: -15px;">'.$splitOrderId.'</div></center></td></tr>
			</tbody>
			</table>
 
		</div>';
		
		$html = '<meta charset="utf-8">
				 <meta http-equiv="X-UA-Compatible" content="IE=edge">
				 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				 <meta content="" name="description"/>
				 <meta content="" name="author"/>					 
				 <body>'.$body.'</body>';
	 
	//$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
	require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
	
	$paperHeight    =  '500';
	$paperWidth  	=  '288';
	$paperMode      = 'landscape';;
			
			
	spl_autoload_register('DOMPDF_autoload'); 
	$dompdf = new DOMPDF();
	$dompdf->set_paper(array( 0, 0, $paperWidth, $paperHeight ), $paperMode );
	$dompdf->load_html($html, Configure::read('App.encoding'));
	//$dompdf->load_html(utf8_decode($html));
	$dompdf->render();
	//$dompdf->stream("OrderIdslip-$splitOrderId.pdf");
	
	$imgPath = WWW_ROOT .'whistl/'; 
	$path = Router::url('/', true).'whistl/';		
	$name	=	'Label_'.$splitOrderId.'.pdf';
	file_put_contents($imgPath.$name, $dompdf->output());
	
	copy($imgPath.'Label_'.$splitOrderId.'.pdf',$imgPath.'rm_to_whistel/Label_'.$splitOrderId.'.pdf');
  	
	return 1;
	
	} 
	
	public function genRmLabels(){
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		
		$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.service_provider' => 'Royalmail','MergeUpdate.process_date >' => '2020-12-21 00:00:00'),'fields'=>['service_name','order_id','provider_ref_code','product_order_id_identify','packet_weight']));
		$cc = 0;
		foreach($orders as $v){
 			$this->customWhistl($v['MergeUpdate']['order_id']);
			$this->genrateLabel($v['MergeUpdate']['product_order_id_identify']);
			$cc++;
			//if($cc > 10) exit;
			
			file_put_contents(WWW_ROOT."whistl/rm_to_whistel/oids.log", $v['MergeUpdate']['product_order_id_identify']."\n", FILE_APPEND|LOCK_EX);	
		}
		
			
	exit;
			 
}

 	public function manifest(){
	
  			$this->autoRender = false;
			$this->layout = '';
			$this->loadModel( 'MergeUpdate' );
			
			/*$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status' => 0,'MergeUpdate.service_provider' => 'whistl','MergeUpdate.manifest_date'=>'','MergeUpdate.delevery_country' => 'United Kingdom'),'fields'=>['service_name','provider_ref_code','product_order_id_identify']));	
 			*/
			$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status' => 1,'MergeUpdate.scan_date !=' => '','MergeUpdate.service_provider' => 'whistl','MergeUpdate.manifest_date'=>'','MergeUpdate.delevery_country' => 'United Kingdom'),'fields'=>['service_name','provider_ref_code','product_order_id_identify','packet_weight']));
  		 
			$all_orders = []; $service_name = [];
			if(count($orders) > 0){
				foreach($orders  as $v){
					$all_orders[] = $v['MergeUpdate']['product_order_id_identify'];
					$service_name[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['provider_ref_code'];
				}
				
					//ob_clean();                                                         
				App::import('Vendor', 'PHPExcel/IOFactory');
				App::import('Vendor', 'PHPExcel');                          
				//Set and create Active Sheet for single workbook with singlle sheet
				$objPHPExcel = new PHPExcel();       
				$objPHPExcel->createSheet();
				 
				
				$row = 1;
				$rightBorder = array(
							  'borders' => array(
									'right' => array(
									  'style' => PHPExcel_Style_Border::BORDER_THIN
									),	
									'left' => array(
									  'style' => PHPExcel_Style_Border::BORDER_THIN
									)																
							  )
							);	
				$border = array(
					  'borders' => array(
							'outline' => array(
							  'style' => PHPExcel_Style_Border::BORDER_THIN
							) 
					  )
					);	
				 $align_center = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);								
				
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C'.$row.':G'.$row);			
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':G'.$row)->applyFromArray($border);	
				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Whistl Manifest');
								
				$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30); 			 
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':G'.$row)->applyFromArray($align_center);
				  
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':G'.$row)->getFont()->setSize(18)->setBold(true)->getColor()->setRGB('FFFFFF');			
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':G'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ff6600');
				$row++;$row++;
				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Client:');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);	
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getFont()->setSize(12)->setBold(true)->getColor()->setRGB('FFFFFF');	
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ff6600');			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, 'G3-Euraco Group Limited');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);
				
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'Client ID:');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($border);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getFont()->setSize(12)->setBold(true)->getColor()->setRGB('FFFFFF');	
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ff6600');
				
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $this->licence_code);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($border);
				
				$row++;$row++;
				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Date of Collection:');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);	
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getFont()->setSize(12)->setBold(true)->getColor()->setRGB('FFFFFF');	
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ff6600');			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, date('Y-m-d'));
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);
				
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'Service:');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($border);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getFont()->setSize(12)->setBold(true)->getColor()->setRGB('FFFFFF');	
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ff6600');
				
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$row, 'Allsort');
				$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($border);
				
				
				$row++;$row++;
				
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C'.$row.':G'.$row);	
				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, "This spreadsheet must contain details of all postings to be shipped on the above date and must be\n e-mailed on that day to: justin.coss@whistl.co.uk ");
			 
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':G'.$row)->applyFromArray($align_center);
				$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30); 	
				
				$row++;$row++;
				
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C'.$row.':G'.$row);			
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':G'.$row)->applyFromArray($border);				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Unsorted mail');				 			
				$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(15);  
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':G'.$row)->applyFromArray($align_center);			  
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':G'.$row)->getFont()->setSize(12)->setBold(true)->getColor()->setRGB('FFFFFF');			
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':G'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ff6600');
				$row++;
				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Format');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);	
					
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, 'Weight in grams');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);
				
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, 'Number of items');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
				
			 
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'Job reference');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($align_center);	
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':G'.$row)->getFont()->setSize(12)->setBold(true); 	
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':G'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('CCCCCC');
				
				
				
				$row++;
				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);
				
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '0-100g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);
				
				if(isset($service_name['L100'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['L100']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;$row++;
				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Busienss Mail Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '0-100g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');

				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);  
				$row++;
				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Busienss Mail Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '101-250g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Busienss Mail Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '251-300g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Busienss Mail Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '301-350g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Busienss Mail Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '351-400g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Busienss Mail Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '401-450g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Busienss Mail Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '451-500g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Busienss Mail Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '501-550g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Busienss Mail Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '551-600g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Busienss Mail Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '601-650g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Busienss Mail Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '651-700g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Busienss Mail Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '701-750g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				/*----------------E-Com Large Letter-----------------*/
				$row++;$row++;
				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'E-Com Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '0-100g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 			
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$row.':G'.$row);
					
				if(isset($service_name['LL250'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['LL100']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}  
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);  
				$row++;
				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'E-Com Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '101-250g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);	
				if(isset($service_name['LL250'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['LL250']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}  
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'E-Com Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '251-300g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);
				
				if(isset($service_name['LL300'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['LL300']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'E-Com Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '301-350g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);
							
				if(isset($service_name['LL350'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['LL350']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'E-Com Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '351-400g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);
							
				if(isset($service_name['LL400'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['LL400']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'E-Com Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '401-450g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['LL450'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['LL450']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'E-Com Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '451-500g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['LL500'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['LL500']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'E-Com Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '501-550g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['LL550'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['LL550']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'E-Com Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '551-600g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['LL600'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['LL600']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'E-Com Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '601-650g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['LL650'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['LL650']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'E-Com Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '651-700g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['LL700'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['LL700']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'E-Com Large Letter');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '701-750g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['LL750'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['LL750']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				/*----------------Packet-----------------*/
				$row++;$row++;
				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '0-100g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P100'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P100']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);  
				$row++;
				
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '101-250g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P250'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P250']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '251-300g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P300'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P300']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '301-350g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P350'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P350']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '351-400g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P400'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P400']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{



					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '401-450g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P450'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P450']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '451-500g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P500'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P500']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '501-550g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P550'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P550']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '551-600g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P600'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P600']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '601-650g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P650'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P650']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '651-700g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P700'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P700']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '701-750g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P750'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P750']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '751-800g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P800'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P800']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '801-850g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P850'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P850']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '851-900g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P900'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P900']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '901-950g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P950'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P950']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '951-1000g');
				if(isset($service_name['P1000'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P1000']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '1001-1250g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P1250'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P1250']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '1251-1500g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P1500'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P1500']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '1551-1750g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P1750'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P1750']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$row++;			
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'Packet');
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($border);			
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '1751-2000g');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($border);			
				if(isset($service_name['P2000'])){
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, count($service_name['P2000']));
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'WT'.date('dmY'));
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '');
				}
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($border); 
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':G'.$row)->applyFromArray($border);
				
				$folderName = 'Service Manifest -'. date("d.m.Y");
				
				$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));
				$service = str_replace(' ', '', str_replace(':','_','Whistl-'. date("d.m.Y") .'_'. $time_in_12_hour_format));
				$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
				
				$uploadUrl = WWW_ROOT .'img/cut_off/'. $folderName . '/'.$service.'.xls';                                          
				$uploadUrI = Router::url('/', true) . 'img/cut_off/'. $folderName . '/'.$service.'.xls';                                          
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
				$objWriter->save($uploadUrl);
				
				if(count($all_orders) > 0){
					$user  = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name') ;
					$sql = "UPDATE `merge_updates` SET `manifest_username` = '".$user."', `manifest_date` = '".date('Y-m-d H:i:s')."' WHERE  `product_order_id_identify` IN ('".implode("','",$all_orders)."')";
					$this->MergeUpdate->query($sql);
				}
				
				$this->Session->setflash( 'Whistl manifest is generated!', 'flash_success'); 
 				
				/*$uploadUrl = WWW_ROOT .'whistl/WhistlManifest.xls';                                          
				$uploadUrI = Router::url('/', true) . 'whistl/WhistlManifest.xls';                                          
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
				$objWriter->save($uploadUrl);*/
			
			}else{
				$this->Session->setflash( 'No order found!', 'flash_danger'); 
			}
			
		  $this->redirect( Router::url( $this->referer(), true ) );
		  exit; 
	}
	
	public function generateOrionManifest(){
	
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'WhistlMrnBagNo' );
		$auth = Configure::read('whistl_orion');
		$msg = '';
		$mrn_no  = ''; $bag_no  = 1;	
		
		$date = date('Y-m-d');//"MergeUpdate.manifest_date LIKE '{$date}%'",
		$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status' => 1,'MergeUpdate.scan_date !=' => '','MergeUpdate.service_provider' => 'whistl','MergeUpdate.delevery_country' => 'United Kingdom'),'fields'=>['id','service_id ','service_name','provider_ref_code','product_order_id_identify','packet_weight','manifest_date','postal_service','order_id','source_coming','service_provider','price']));
	
									 
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');                          
		//Set and create Active Sheet for single workbook with singlle sheet
		$objPHPExcel = new PHPExcel();       
		$objPHPExcel->createSheet();
		$row = 1;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'LINENUMBER');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, 'ORIGAIRPORT');		
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'DestAirport');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, 'FlightNo'); 
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, 'ETA');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, 'MAWBNO');		
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$row, 'MAWBWEIGHT');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$row, 'DATEOFEXPORT'); 
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$row, 'VENDOR');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$row, 'TRACKINGNUMBER');		
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$row, 'CUSTOMERSHIPMENTID');
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$row, 'SKU'); 
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$row, 'ORIGINCOUNTRY');
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$row, 'ITEMDESCRIPTION');		
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$row, 'ITEMHARMONIZEDCODE');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$row, 'CPC'); 
		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$row, 'ITEMNETMASS');
		$objPHPExcel->getActiveSheet()->setCellValue('R'.$row, 'QUANTITY');		
		$objPHPExcel->getActiveSheet()->setCellValue('S'.$row, 'UNITPRICE');
		$objPHPExcel->getActiveSheet()->setCellValue('T'.$row, 'TOTALVALUE'); 
		$objPHPExcel->getActiveSheet()->setCellValue('U'.$row, 'CURRENCY');
		$objPHPExcel->getActiveSheet()->setCellValue('V'.$row, 'CONSIGNORID');		
		$objPHPExcel->getActiveSheet()->setCellValue('W'.$row, 'Document Reference');
		$objPHPExcel->getActiveSheet()->setCellValue('X'.$row, 'CONSIGNORNAME'); 
		$objPHPExcel->getActiveSheet()->setCellValue('Y'.$row, 'CONSIGNORADDRESS1');
		$objPHPExcel->getActiveSheet()->setCellValue('Z'.$row, 'CONSIGNORADDRESS2'); 
		$objPHPExcel->getActiveSheet()->setCellValue('AA'.$row, 'CONSIGNORADDRESS3'); 
		$objPHPExcel->getActiveSheet()->setCellValue('AB'.$row, 'CONSIGNEECITY'); 
		$objPHPExcel->getActiveSheet()->setCellValue('AC'.$row, 'CONSIGNEESTATE');
		$objPHPExcel->getActiveSheet()->setCellValue('AD'.$row, 'CONSIGNEEPOSTCODE');		
		$objPHPExcel->getActiveSheet()->setCellValue('AE'.$row, 'CONSIGNEECOUNTRY');
		$objPHPExcel->getActiveSheet()->setCellValue('AF'.$row, 'CONSIGNEENAME'); 
		$objPHPExcel->getActiveSheet()->setCellValue('AG'.$row, 'CONSIGNEEADDRESS1'); 
		$objPHPExcel->getActiveSheet()->setCellValue('AH'.$row, 'CONSIGNEEADDRESS2'); 
		$objPHPExcel->getActiveSheet()->setCellValue('AI'.$row, 'CONSIGNEEADDRESS3'); 
		$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$row, 'CONSIGNEECITY'); 
		$objPHPExcel->getActiveSheet()->setCellValue('AK'.$row, 'CONSIGNEESTATE'); 
		$objPHPExcel->getActiveSheet()->setCellValue('AL'.$row, 'CONSIGNEEPOSTCODE'); 
		$objPHPExcel->getActiveSheet()->setCellValue('AM'.$row, 'CONSIGNEECOUNTRY'); 
		$objPHPExcel->getActiveSheet()->setCellValue('AN'.$row, 'AISTATEMENTTEXT');
		$objPHPExcel->getActiveSheet()->setCellValue('AO'.$row, 'TELEPHONE');
		$objPHPExcel->getActiveSheet()->setCellValue('AP'.$row, 'EMAIL');
		$objPHPExcel->getActiveSheet()->setCellValue('AQ'.$row, 'HAWBNO');
		$objPHPExcel->getActiveSheet()->setCellValue('AR'.$row, 'CONTAINERNO');
		$objPHPExcel->getActiveSheet()->setCellValue('AS'.$row, 'NOOFBOXBAGS');
		$objPHPExcel->getActiveSheet()->setCellValue('AT'.$row, 'BAGBOXCODE');
		$objPHPExcel->getActiveSheet()->setCellValue('AU'.$row, 'NOOFPALLETS');
		$objPHPExcel->getActiveSheet()->setCellValue('AV'.$row, 'PALLETCODE');
		$objPHPExcel->getActiveSheet()->setCellValue('AW'.$row, 'ROUTINGCODE'); 
		$objPHPExcel->getActiveSheet()->setCellValue('AX'.$row, 'FORMATTYPE');
		$objPHPExcel->getActiveSheet()->setCellValue('AY'.$row, 'ISLABELPRINTING');  		
	   
		if(count($orders) > 0){ 
			$file_name = 'xsensys_whistl_orion_manifest.csv';
			$counter = 0;$_weight = 0;
			foreach($orders  as $orderItem){
			
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $orderItem['MergeUpdate']['order_id']) ) );
			if(count($openOrder) > 0){
				
				$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
				$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
				$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']); 
				
				$sdata = $this->getSplitOrderDetails($orderItem['MergeUpdate']['product_order_id_identify'],$customer_info->Address->Country);
				
				$quantity 	   = $sdata['quantity'];
				$order_id 	   = $orderItem['MergeUpdate']['order_id'];
				$sub_source    = $orderItem['MergeUpdate']['source_coming']; 
				$packet_weight = $sdata['packet_weight']/1000;
				$_weight  += $packet_weight;
				$unit_weight   = number_format($packet_weight,2);//number_format(($packet_weight / $sdata['quantity']),2);
				$total_weight  = number_format(($unit_weight * $sdata['quantity']),2);
				$unit_price    = number_format(($orderItem['MergeUpdate']['price'] / $sdata['quantity']),2);
				$total_price   = number_format(($unit_price * $sdata['quantity']),2);
				
				$tracking_id = $orderItem['MergeUpdate']['product_order_id_identify'];
				$hs_code = implode(";",$sdata['hs_code']);
				
				$EORI_Number = 'GB019434034000';
				$UK_VAT_Number = 'GB 307817693'; 
				
				$address1 = ''; $address2  = ''; $address3 = '';
				
				$lines = explode("\n", wordwrap(htmlentities($this->replaceChar($customer_info->Address->Address1)).' '.htmlentities($this->replaceChar($customer_info->Address->Address2)).' '.htmlentities($this->replaceChar($customer_info->Address->Address3)), '30'));
				
				if(isset($lines[0]) && $lines[0] != ''){
					$address1  =  trim($lines[0]) ;
				}
				if(isset($lines[1]) && $lines[1] != ''){
					$address2 =  trim($lines[1]) ;
				}
				if(isset($lines[2]) && $lines[2] != ''){
					$address3 = trim($lines[2]);
				} 
				 
				$bag_code = 'BB'. str_pad($bag_no,4,'0',STR_PAD_LEFT) ;  
				/*fputcsv($file, [$counter,'GBSTH','SEA','GBPME',date("m/d/Y", strtotime("+ 1 day")).'  10:00:00 AM',$mrn_no,$unit_weight,date("m/d/Y"),'Whistl',$tracking_id,$tracking_id,'DDP','JE',$sdata['category_name'],$hs_code,'PM',$total_weight,$sdata['quantity'],'',$total_price,'GBP',$EORI_Number,$UK_VAT_Number,'ESL Limited','Unit 4 Airport Cargo Centre','Le Avenue De La Commune','','St Peter','Jersey','JE3 7BY','JE',$this->replaceChar($customer_info->Address->FullName),$address1,$address2,$address3,$this->replaceChar($customer_info->Address->Town),$this->replaceChar($customer_info->Address->Region),$customer_info->Address->PostCode,$sdata['country_code'],'','','','','','',$bag_code,'','',$auth['routing_code']]);*/
				$counter++;
				$row++;
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $counter);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, 'GBSTH');		
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'GBPME');
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '1T001'); 
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, date("d/m/Y", strtotime("+ 1 day")).'  10:00');
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $mrn_no);		
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $unit_weight);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$row, date("m/d/Y")); 
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$row, 'Whistl');
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $tracking_id);		
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $tracking_id);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$row, 'DDP'); 
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$row, 'JE');
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$row, $sdata['category_name']);		
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$row, $hs_code);
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$row, '4000003'); 
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$row, $total_weight);
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$row, $sdata['quantity']);		
				$objPHPExcel->getActiveSheet()->setCellValue('S'.$row, $unit_price);
				$objPHPExcel->getActiveSheet()->setCellValue('T'.$row, $total_price); 
				$objPHPExcel->getActiveSheet()->setCellValue('U'.$row, 'GBP');
				$objPHPExcel->getActiveSheet()->setCellValue('V'.$row, $EORI_Number);		
				$objPHPExcel->getActiveSheet()->setCellValue('W'.$row, $UK_VAT_Number);
				$objPHPExcel->getActiveSheet()->setCellValue('X'.$row, 'ESL Limited'); 
				$objPHPExcel->getActiveSheet()->setCellValue('Y'.$row, 'Unit 4 Airport Cargo Centre');
				$objPHPExcel->getActiveSheet()->setCellValue('Z'.$row, 'Le Avenue De La Commune');		
				$objPHPExcel->getActiveSheet()->setCellValue('AA'.$row, '');
				$objPHPExcel->getActiveSheet()->setCellValue('AB'.$row, 'St Peter'); 
				$objPHPExcel->getActiveSheet()->setCellValue('AC'.$row, 'Jersey');
				$objPHPExcel->getActiveSheet()->setCellValue('AD'.$row, 'JE3 7BY');		
				$objPHPExcel->getActiveSheet()->setCellValue('AE'.$row, 'JE');
				$objPHPExcel->getActiveSheet()->setCellValue('AF'.$row, $this->replaceChar($customer_info->Address->FullName)); 
				$objPHPExcel->getActiveSheet()->setCellValue('AG'.$row, $address1); 
				$objPHPExcel->getActiveSheet()->setCellValue('AH'.$row, $address2); 
				$objPHPExcel->getActiveSheet()->setCellValue('AI'.$row, $address3); 
				$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$row, $this->replaceChar($customer_info->Address->Town)); 
				$objPHPExcel->getActiveSheet()->setCellValue('AK'.$row, $this->replaceChar($customer_info->Address->Region)); 
				$objPHPExcel->getActiveSheet()->setCellValue('AL'.$row, $customer_info->Address->PostCode); 
				$objPHPExcel->getActiveSheet()->setCellValue('AM'.$row, $sdata['country_code']);  
				$objPHPExcel->getActiveSheet()->setCellValue('AR'.$row, $bag_code);
				$objPHPExcel->getActiveSheet()->setCellValue('AW'.$row, $auth['routing_code']); 
				 
			}
			else{
				$msg =  ' No order found.<br>';
			}		
			 
		}
		
		$total_weight  = number_format($_weight,2);
		for($i=2; $i<= $row;$i++){
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $total_weight);
		}
		
		$file_name = 'xsensys_whistl_orion_manifest_'.date('d-m-Y').'.csv';
		$uploadUrl = WWW_ROOT .'whistl/'.$file_name;                                          
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
		$objWriter->save($uploadUrl);
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$file_name.'"');
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
		exit;
								
		}else{
			$this->Session->setflash( 'No order found!', 'flash_danger'); 
		}
		
	
	  $this->redirect( Router::url( $this->referer(), true ) );
	  exit; 
	}
	public function generateOrionManifest2(){
	
  			$this->autoRender = false;
			$this->layout = '';
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'WhistlMrnBagNo' );
			
 			$msg = '';
			$mrn_no  = ''; $bag_no  = 1;	
			
 			$date = date('Y-m-d');//"MergeUpdate.manifest_date LIKE '{$date}%'",
			$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status' => 1,'MergeUpdate.scan_date !=' => '','MergeUpdate.service_provider' => 'whistl','MergeUpdate.delevery_country' => 'United Kingdom'),'fields'=>['id','service_id ','service_name','provider_ref_code','product_order_id_identify','packet_weight','manifest_date','postal_service','order_id','source_coming','service_provider','price']));
		
			$auth = Configure::read('whistl_orion');
			$all_orders = [];   
			if(count($orders) > 0){ 
				$file_name = 'xsensys_whistl_orion_manifest.csv';
				$file = fopen(WWW_ROOT ."logs/".$file_name,"w");
				fputcsv($file, ['LINENUMBER','ORIGAIRPORT','DestAirport','FlightNo','ETA','MAWBNO','MAWBWEIGHT','DATEOFEXPORT','VENDOR','TRACKINGNUMBER','CUSTOMERSHIPMENTID','SKU','ORIGINCOUNTRY','ITEMDESCRIPTION','ITEMHARMONIZEDCODE','CPC','ITEMNETMASS','QUANTITY','UNITPRICE','TOTALVALUE','CURRENCY','CONSIGNORID','Document Reference','CONSIGNORNAME','CONSIGNORADDRESS1','CONSIGNORADDRESS2','CONSIGNORADDRESS3','CONSIGNORCITY','CONSIGNORSTATE','CONSIGNORPOSTCODE','CONSIGNORCOUNTRY','CONSIGNEENAME','CONSIGNEEADDRESS1','CONSIGNEEADDRESS2','CONSIGNEEADDRESS3','CONSIGNEECITY','CONSIGNEESTATE','CONSIGNEEPOSTCODE','CONSIGNEECOUNTRY','AISTATEMENTTEXT','TELEPHONE','EMAIL','HAWBNO','CONTAINERNO','NOOFBOXBAGS','BAGBOXCODE','NOOFPALLETS','PALLETCODE','ROUTINGCODE','FORMATTYPE','ISLABELPRINTING']);
				$counter = 1;
				foreach($orders  as $orderItem){
				
				$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $orderItem['MergeUpdate']['order_id']) ) );
				if(count($openOrder) > 0){
					
					$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
					$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
					$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']); 
					
					$sdata = $this->getSplitOrderDetails($orderItem['MergeUpdate']['product_order_id_identify'],$customer_info->Address->Country);
					
					$quantity 	   = $sdata['quantity'];
					$order_id 	   = $orderItem['MergeUpdate']['order_id'];
					$sub_source    = $orderItem['MergeUpdate']['source_coming']; 
					
					$unit_weight   = number_format(($sdata['packet_weight'] / $sdata['quantity']),2);
					$total_weight  = number_format(($unit_weight * $sdata['quantity']),2);
					$unit_price    = number_format(($orderItem['MergeUpdate']['price'] / $sdata['quantity']),2);
					$total_price   = number_format(($unit_price * $sdata['quantity']),2);
					
					$tracking_id = $orderItem['MergeUpdate']['product_order_id_identify'];
					$hs_code = implode(";",$sdata['hs_code']);
					
					$EORI_Number = 'GB019434034000';
					$UK_VAT_Number = 'GB 307817693'; 
					
					$address1 = ''; $address2  = ''; $address3 = '';
					
					$lines = explode("\n", wordwrap(htmlentities($this->replaceChar($customer_info->Address->Address1)).' '.htmlentities($this->replaceChar($customer_info->Address->Address2)).' '.htmlentities($this->replaceChar($customer_info->Address->Address3)), '30'));
					
					if(isset($lines[0]) && $lines[0] != ''){
						$address1  =  trim($lines[0]) ;
					}
					if(isset($lines[1]) && $lines[1] != ''){
						$address2 =  trim($lines[1]) ;
					}
					if(isset($lines[2]) && $lines[2] != ''){
						$address3 = trim($lines[2]);
					} 
					 
					$bag_code = 'BB'. str_pad($bag_no,4,'0',STR_PAD_LEFT) ;  
					fputcsv($file, [$counter,'GBSTH','SEA','GBPME',date("m/d/Y", strtotime("+ 1 day")).'  10:00:00 AM',$mrn_no,$unit_weight,date("m/d/Y"),'Whistl',$tracking_id,$tracking_id,'DDP','JE',$sdata['category_name'],$hs_code,'PM',$total_weight,$sdata['quantity'],'',$total_price,'GBP',$EORI_Number,$UK_VAT_Number,'ESL Limited','Unit 4 Airport Cargo Centre','Le Avenue De La Commune','','St Peter','Jersey','JE3 7BY','JE',$this->replaceChar($customer_info->Address->FullName),$address1,$address2,$address3,$this->replaceChar($customer_info->Address->Town),$this->replaceChar($customer_info->Address->Region),$customer_info->Address->PostCode,$sdata['country_code'],'','','','','','',$bag_code,'','',$auth['routing_code']]);
					 $counter++;
					
					//file_put_contents(WWW_ROOT .'logs/whistlapi_'.date('ymd').'.log' , $orderItem['MergeUpdate']['product_order_id_identify']."\t".$orderItem['MergeUpdate']['service_id']."\n",  FILE_APPEND|LOCK_EX);	
					 
				}
				else{
					$msg =  ' No order found.<br>';
				}		
				 
			}
			
			fclose($file);
			
			header("Content-Type: application/csv");
			header("Content-Disposition: attachment; filename={$file_name}");
			header("Pragma: no-cache");
			readfile(WWW_ROOT."logs/{$file_name}");
			exit;
									
			}else{
				$this->Session->setflash( 'No order found!', 'flash_danger'); 
			}
 			
		
		  $this->redirect( Router::url( $this->referer(), true ) );
		  exit; 
	}
	public function getSenderInfo($sub_source = '')
	{ 
 		 
  		$senderData['FromCity'] 	   =  'BEDFORD'; 
		$senderData['FromPostCode']    =  'MK42 5DW';
		$senderData['FromCountry'] 	   =  'United Kingdom'; 
		 
		if(strpos($sub_source,'marec')!== false){
  		  	$senderData['FromPhoneNumber'] = '+443301170238';  
			$senderData['email'] 		   = 'accounts@esljersey.com';  
		  	$senderData['FromCompany']	   =  $this->licence_code.'-EBEX';//ESL LIMITED //(ebuyer express)
			$senderData['FromAddress']	   =  'PO Box 1598' ; 
		 }	
		else if(strpos($sub_source,'rainbow')!== false){
 			$senderData['FromPhoneNumber'] = '0123456789';
			$senderData['email'] 		   = 'accounts@fresherbusiness.co.uk'; 			
		  	$senderData['FromCompany']	   =  $this->licence_code.'-RR-R';//FRESHER BUSINESS LIMITED			 
			$senderData['FromAddress']	   =  'PO Box 1598' ; 
		}else{ 		  	
			$senderData['FromPhoneNumber'] =  '+443301170104';
			$senderData['email'] 		   =  'accounts@euracogroup.co.uk';
			$senderData['FromCompany']	   =   $this->licence_code.'-CostBreaker'; //EURACO GROUP LTD
			$senderData['FromAddress']	   =  'PO Box 1598' ; 
		}
	
  		 
  		return $senderData;	
	}
	
	public function getCountryAddress( $addressArray )
	{
			
			switch ($addressArray['country']) 
			{
			case "United States":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? ucwords(strtolower($addressArray['address1']))."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords(strtolower($addressArray['address2']))."\n" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords(strtolower($addressArray['address3']))."\n" : '' ; 
					return $address;
					break;
			case "Canada":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1'])."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']) : '' ;
					return $address;
					break;
			case "Germany":
			
					if(strtolower($addressArray['sub_source']) == 'costdropper'){
  					
						$address			 =		'';
						if($addressArray['address1'] != '' && $addressArray['address2'] != ''){
							$address			.=		ucwords($addressArray['address2']).' ' . ucwords($addressArray['address1']) ; 
						}
 						if($addressArray['address1'] == '' && $addressArray['address2'] != ''){						
							$address			.=		ucwords($addressArray['address2']) ; 					
						}
						if($addressArray['address1'] != '' && $addressArray['address2'] == ''){						
							$address			.=		ucwords($addressArray['address1']) ; 					
						}
						
					}else{
							$address			 =		'';
							$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1'])."\n" : '' ; 
							
							if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
								$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
								$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;						
							}
							
							if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
								$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ;
							}
							
							if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
								$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;
							}
 						}
					return $address;
					break;
				case "Spain":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? ucwords($addressArray['company'])."\n" : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1'])."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords($addressArray['address3'])."\n" : '' ;
					return $address;
					break;
			
			case "Italy":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? strtoupper($addressArray['address1'])."\n" : '' ; 
					
					 if($addressArray['address2'] != '' && $addressArray['address3'] != ''){
						$address			.=		($addressArray['address2'] != '' )   ? strtoupper($addressArray['address2'])."\n" : '';
						$address			.=		($addressArray['address3'] != '' )   ? strtoupper($addressArray['address3'])."\n" : '';
					}
					else if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
 						$address			.=		($addressArray['address2'] != '' )   ? strtoupper($addressArray['address2'])."\n" : '';
 					}
					else if($addressArray['address2'] == '' && $addressArray['address3'] != ''){
 						$address			.=		($addressArray['address3'] != '' )   ? strtoupper($addressArray['address3'])."\n" : '';
 					}
					return $address;
					break;
			case "France":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? strtoupper($addressArray['address1'])."\n" : '' ; 
					
					if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;						
					}
					
					if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ;
					}
					
					if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;
					}
					
					return $address;
					break;
			case "Denmark":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? $addressArray['company']."\n" : '' ;  
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1']."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3']."\n" : '' ; 
					return $address;
					break;
			case "Austria":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? $addressArray['company']."\n" : '' ; 
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1']."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3']."\n" : '' ; 
					return $address;
					break;
			default:
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1']."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3']."\n" : '' ;
					return $address;
					break;
			}
			exit;
		}
	
	public function getBarcodeOutside($spilt_order_id  = null)
	{ 
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'OpenOrder' ); 
		$this->loadModel( 'MergeUpdate' );
		
		$imgPath = WWW_ROOT .'img/orders/barcode/';    
		
		if($spilt_order_id != ''){
		 $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => $spilt_order_id)));
		}else{
		 $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.status' => '0')));
		}
		
		
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
		//$font = new BCGFontFile(APP .'Vendor/barcodegen/font/Arial.ttf', 13);
		$colorFront = new BCGColor(0, 0, 0);
		$colorBack = new BCGColor(255, 255, 255);
		
		if( count($allSplitOrders) > 0 )	
		{
		  foreach( $allSplitOrders as $allSplitOrder )
		  {					  
			  $id 			= 		$allSplitOrder['MergeUpdate']['id'];
			  $openorderid	=	 	$allSplitOrder['MergeUpdate']['product_order_id_identify'];
			  $barcodeimage	=	    $openorderid.'.png';
			  
				$orderbarcode=$openorderid;
				$code128 = new BCGcode128();
				$code128->setScale(2);
				$code128->setThickness(20);
				$code128->setForegroundColor($colorFront);
				$code128->setBackgroundColor($colorBack);
				$code128->setLabel(false);
				$code128->parse($orderbarcode);
									
				//Drawing Part
				$imgOrder128=$orderbarcode.".png";
				$imgOrder128path=$imgPath.'/'.$orderbarcode.".png";
				$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
				$drawing128->setBarcode($code128);
				$drawing128->draw();
				$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);
			  
			  if( $allSplitOrder['MergeUpdate']['product_order_id_identify'] != "" )
			  {   $data['MergeUpdate']['id'] 	=  $id;
				  $data['MergeUpdate']['order_barcode_image'] 	=  $barcodeimage;
				  $this->MergeUpdate->save($data);
			  }
		  }
		}
		
		
	}
		 
   	private function getSplitOrderDetails($split_order_id = null,$customer_country = null){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'ProductHscode' );
	    $data = [];
		$hs_code = [];
		$country_of_origin = 'CN';
  		$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_order_id)));	
				
		if(count($orderItem) > 0)
		{
			$this->loadModel( 'Product' );
			$this->loadModel( 'ProductDesc' );
			$this->loadModel( 'Country' );	
		
 			$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
			$packet_length = $orderItem['MergeUpdate']['packet_length'];
			$packet_width  = $orderItem['MergeUpdate']['packet_width'];
			$packet_height = $orderItem['MergeUpdate']['packet_height']; 
			
			$_height = array(); $_weight = array(); $length = $width = 0; 
  			
			$country = $this->Country->find('first', array( 'conditions' => array('name' =>trim($customer_country)) ) );
			if(count($country) == 0){
				$country = $this->Country->find('first', array( 'conditions' => array('custom_name' =>trim($customer_country)) ) );
			}
			 
			$pos = strpos($orderItem['MergeUpdate']['sku'],",");
			
			if ($pos === false) {
				$quantity = [];
				$val  = $orderItem['MergeUpdate']['sku'];
				$s    = explode("XS-", $val);
				$_qty = $s[0]; 
				$_sku = "S-".$s[1];	
 						
				$product = $this->Product->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.product_name','Product.country_of_origin','Product.category_name')));
				
				if(count($product) > 0)	{
					$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $_sku,'country_code' => $country['Country']['iso_2'], 'hs_code is NOT NULL' )));
					if(count( $hsresult ) ==  0){				
						$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $_sku,'country_code' => 'GB')));
					}
					if(count( $hsresult ) > 0){
						$hs_code[] = $hsresult['ProductHscode']['hs_code'];	
					}
				
					if($product['Product']['country_of_origin'] != ''){
						$c_code= $this->Country->find('first',array('conditions' => array('Country.name' => $product['Product']['country_of_origin']),'fields'=>array('Country.iso_2'))); 
						if(count($c_code) > 0){
							$country_of_origin = $c_code['Country']['iso_2'];
						}
					}
						
					$length = $product['ProductDesc']['length'];
					$width = $product['ProductDesc']['width'];
					$height[]  = $product['ProductDesc']['height'];
					$weight[]  = ($product['ProductDesc']['weight'] * $_qty );
					$data['category_name'] = $product['Product']['category_name'];	
					$data['product_name'] = $product['Product']['product_name'];										
					$data['country_code'] = $country['Country']['iso_2'];
 					$data['country_of_origin'] = $country_of_origin;						
					$data['hs_code'] = $hs_code;
					$data['sku'] = $_sku;	
					$quantity[] = $_qty;
				}		 
  				
			}else{			
				$sks = explode(",",$orderItem['MergeUpdate']['sku']);
				$_weight = [];	$_height = []; $quantity = [];				 
				foreach($sks as $val){
					$s    = explode("XS-", $val);
					$_qty = $s[0]; 
					$_sku = "S-".$s[1];
					$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.product_name','Product.country_of_origin','Product.category_name')));
					if(count($product) > 0)	{
					
						$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $_sku,'country_code' => $country['Country']['iso_2'], 'hs_code is NOT NULL' )));
						if(count( $hsresult ) ==  0){				
							$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $_sku,'country_code' => 'GB')));
						}
						if(count( $hsresult ) > 0){
							$hs_code[] = $hsresult['ProductHscode']['hs_code'];	
						}
						if($product['Product']['country_of_origin'] != ''){
							$c_code= $this->Country->find('first',array('conditions' => array('Country.name' => $product['Product']['country_of_origin']),'fields'=>array('Country.iso_2'))); 
							if(count($c_code) > 0){
								$country_of_origin = $c_code['Country']['iso_2'];
							}
						}
 						
						$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
						$_height[] = $product['ProductDesc']['height'];
						$length = $product['ProductDesc']['length'];	
						$width  = $product['ProductDesc']['width']; 
						$data['category_name'] = $product['Product']['category_name'];	
						$data['product_name'] = $product['Product']['product_name'];	
						$data['country_code'] = $country['Country']['iso_2'];
						$data['country_of_origin'] = $country_of_origin;	
						$data['hs_code'] = $hs_code;
						$data['sku'] = $_sku;
						$quantity[] = $_qty;
					}				
				}							 			
			}
			 
			if($packet_weight == 0){
				$packet_weight = array_sum($_weight);
			}
			if($packet_height == 0){
				$packet_height = array_sum($_height);
			}
			if($packet_length == 0){
				$packet_length = $length; 
			}
			if($packet_width == 0){
				$packet_width = $width;  
			}
			$data['quantity'] 	   = array_sum($quantity);	
			$data['packet_weight'] = $packet_weight*1000; // KG to Gram
			$data['packet_height'] = $packet_height;
			$data['packet_length'] = $packet_length; 
			$data['packet_width']  = $packet_width;
   		}
		
		return $data;
		
 	}
   	 
	private function sendErrorMail($details = null, $is_manifest = 0){
		$web_store = Configure::read( 'web_store' );
		$subject   = 'RoyalMail issue in '.$web_store;
		$mailBody  = '<p><strong>RoyalMail have some issue please review and solve it.</strong></p>';
		
		if($is_manifest > 0){
			$subject   = 'RoyalMail create manifest issue in '.$web_store;
			$mailBody  = '<p><strong>Create manifest have below issue please review and solve it.</strong></p>';
		}else if(isset($details['split_order_id'])){
			$subject   = $details['split_order_id'].' RoyalMail orders issue in '.$web_store;
			$mailBody  = '<p><strong>'.$details['split_order_id'].' have below issue please review and solve it.</strong></p>';
		}	
		
		$mailBody .= '<p>Error Code : '.$details['error_code'].'</p>';
		$mailBody .= '<p>Error Message : '.$details['error_msg'].'</p>';
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('');
		$email->emailFormat('html');
		$email->from('royal@'.$web_store);
		//$email->to( array('avadhesh.kumar@jijgroup.com','shashi@euracogroup.co.uk','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));	
		if($details['error_code'] == 'E1001'){
			$email->to( array('avadhesh.kumar@jijgroup.com','abhishek@euracogroup.co.uk','deepak@euracogroup.com','vikas.kumar@euracogroup.co.uk','ankit.nagar@euracogroup.co.uk'));
   		}else if(in_array($details['error_code'],array('E0015','E0007','E0005','500'))){
			$email->to( array('avadhesh.kumar@jijgroup.com','shashi@euracogroup.co.uk','abhishek@euracogroup.co.uk','deepak@euracogroup.com','vikas.kumar@euracogroup.co.uk','ankit.nagar@euracogroup.co.uk'));	
		}else{
			$email->to( array('avadhesh.kumar@jijgroup.com'));	
		}	
		
		 			  
		$getBase = Router::url('/', true);
		$email->subject( $subject );
	  	$email->send( $mailBody );
	}
	 
	private function getBarcode( $split_order_id ){ 
		
		$imgPath = WWW_ROOT .'img/orders/barcode/';   
 		
		if(!file_exists($imgPath.$split_order_id.'.png')){
		
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
			
			$colorFront = new BCGColor(0, 0, 0);
			$colorBack = new BCGColor(255, 255, 255); 
			
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->setLabel(false);
			$code128->parse($split_order_id);
			
			//Drawing Part
			$imgOrder128 = $split_order_id.".png";
			$imgOrder128path = $imgPath.$split_order_id.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);  
		}
		      
	}		
 	
	 	
	 private function replaceChar($string = null){
  		return iconv('UTF-8','ASCII//TRANSLIT',$string);	 
	}
}
 
?>