<?php
//error_reporting(0);
class BulksController extends AppController
{
    
		var $name = "Bulks";
		var $components = array('Session','Upload','Common','Auth');
		var $helpers = array('Html','Form','Common','Session');
		public function beforeFilter()
		{
		   parent::beforeFilter();
		   $this->layout = false;
		   $this->Auth->Allow(array('index'));
		   $this->GlobalBarcode = $this->Components->load('Common'); 
		}
	
		public function index()
		{
			echo "Welcome in Bulks Controller.";
			exit;
		}
	
	
		public function bulkDispatch()
		{
			   $this->layout = 'index';
		}
	
		public function getsearchlist()
	 	{
			   $this->autoRender = false;
			   $this->layout = '';
			  
			   $this->loadModel('OpenOrder');
			   $this->loadModel('OrderItem');
			   $this->loadModel('MergeUpdate');
			   $this->loadModel('Product');
			   date_default_timezone_set('Europe/Jersey');
			  
			   $express1	=	array();
			   $express2	=	array();
			   $standerd1	=	array();
			   $standerd2	=	array();
			   $tracked1	=	array();
			   $tracked2	=	array();
			   
			    $gillet = array('S-73624#DCS','S-73512#DCS','S-81471666#DCS','S-78571#DCS','S-81612450#DCS','S-81668920#DCS','S-73510#DCS','S-81609465#DCS','S-73623#DCS','S-707779#DCS','S-GILFUSPROPW10X8#MC','S-GILFUSPROSTYLER#Mc','S-GLT74041#DCS','S-81641948#DCS','S-81307323','S-81641984#DCS','S-81521780#DCS','S-81543872#DCS','S-81469674#DCS','S-81521781#DCS','S-81641939#DCS','S-91269#DCS','S-97179#DCS','S-73506#DCS','S-73508#DCS','S-73357#DCS','S-91268#DCS','S-73354#DCS','S-81631492#DCS','S-81604016#DCS','S-81521763#DCS');
			   
			   $barcode		=	 $this->request->data['barcode'];
			   //$barcode		=	 '4210201382355';//$this->request->data['barcode'];
			    /*************************************Apply code for Global Barcode*********************************************/
			   $barcodeg		=	$this->GlobalBarcode->getGlobalBarcode($barcode);
			   /**************************************Apply code for Global Barcode***********************************************/
			   $usertype = $this->Session->read('Auth.User.service_pkst');
			   
			   $getItems = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcodeg .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1','MergeUpdate.linn_fetch_orders IN' => array(1,4) ) ) );
			  //pr($getItems);
			  //exit;
			   $data = array();
			   
			   foreach($getItems as $getItem){
			  	 	
					$get_skus	=	explode(',',$getItem['MergeUpdate']['sku'] );
					$skusg	=	explode(',', $getItem['MergeUpdate']['sku']); 
					$i = 0;
					foreach($skusg as $k => $v){
						$sku_qty = 'S-'.explode('XS-', $v)[1];
						if(in_array( $sku_qty, $gillet )){
							$i++;
						}
					}
						/*$item_data[$getItem['MergeUpdate']['delevery_country']][ $getItem['MergeUpdate']['postal_service'] ][$getItem['MergeUpdate']['sku']][] = array( 'order_id' => $getItem['MergeUpdate']['product_order_id_identify'], 'sku' => $getItem['MergeUpdate']['sku'], 'barcode' => $getItem['MergeUpdate']['barcode'], 'packaging_type' => $getItem['MergeUpdate']['packaging_type'], 'service_name' => $getItem['MergeUpdate']['service_name']);*/
						$item_data[$getItem['MergeUpdate']['delevery_country']][ $getItem['MergeUpdate']['service_name'] ][$getItem['MergeUpdate']['sku']][] = array( 'order_id' => $getItem['MergeUpdate']['product_order_id_identify'], 'sku' => $getItem['MergeUpdate']['sku'], 'barcode' => $getItem['MergeUpdate']['barcode'], 'packaging_type' => $getItem['MergeUpdate']['packaging_type'], 'service_name' => $getItem['MergeUpdate']['postal_service']);
			   }
			   $cla = "";
			   if($i > 0){
			   	$cla = "gillete";
			   } 
			  $html =   '<table class="bulk table-bordered table-striped dataTable" border = 1 for="'.$cla.'">
						<tr>
							<th>Country</th>
							<th>P. Qty</th>
							<th>Num. Orders</th>
							<th>Total Qty Req.</th>
							<th>Barcode / SKU</th>
							<th>Title</th>
							<th>Service Code</th>
							<th>Service Name</th>
							<th>Packaging Name</th>
							<th>Action</th>
						</tr>';	
			   krsort($item_data); $row = 0;
			   foreach($item_data as $ck => $kv){
			    	foreach($kv as $sk => $sv){
						foreach($sv as $qk => $qv){	 
							$row++;
							$_data = array();$_ddata = array();	
							$html .= '<tr id="r_'.$row.'">';					 
							foreach($qv as $k => $v){
								$_data[]  		=  $v['order_id'] ; 
								$sku 	  		= $v['sku'];	
								$barcode 	  	= $v['barcode'];	
								$packaging_type = $v['packaging_type'];	
								$service_name 	= $v['service_name'];	
								$getProductdesc	= $this->Product->find('first', array( 'conditions' => array('Product.product_sku' => 'S-'.explode('S-',$sku)[1] )));
								$title		='';
								if(isset( $getProductdesc['Product']['product_name'])){
									 $title			= $getProductdesc['Product']['product_name'];
								 }
						 	}
							$number_orders	=	count($_data);			
							$skus			=	 explode( ',', $sku);
					   		$barcode		=	 explode( ',', $barcode);
							
					   		$i = 0;
							$title = array();
							$newSku = array(); $po_barcode = array();
							foreach($skus as $sku)
							   {
									$newSku[]				=	 explode( 'X', $sku)[1];
								    $getProductdesc			=	 $this->Product->find('first', array( 'conditions' => array('Product.product_sku' => explode( 'X', $sku)[1])));
								    if(isset( $getProductdesc['Product']['product_name'])){
									 $title[]			= $getProductdesc['Product']['product_name'];
								 	}
									if(isset( $getProductdesc['ProductDesc']['barcode'])){
									 $po_barcode[]			= $getProductdesc['ProductDesc']['barcode'];
								 	}
									$i++;
							   }
								$f_data[$ck][$sk][$qk]['order_ids'] 	= implode(',', $_data);
								$f_data[$ck][$sk][$qk]['sku'] 			= 'S-'.explode('S-',$sku)[1];
								$f_data[$ck][$sk][$qk]['barcode'] 		= implode(',',$po_barcode);
								$f_data[$ck][$sk][$qk]['packaging']		= $packaging_type;
								$f_data[$ck][$sk][$qk]['title']			= implode(',',$title);
								$f_data[$ck][$sk][$qk]['nub_orders']	= $number_orders;
								$f_data[$ck][$sk][$qk]['tot_qty']		= $number_orders * $qk ;
								$f_data[$ck][$sk][$qk]['service_name']	= $service_name;
								$orde_ids	=	implode(',', $_data);
								$html .= '<td>'.$ck.'</td>
										<td>'.str_replace(',',"<br>",$qk).'</td>
										<td>'.$number_orders.'</td>
										<td>'.$number_orders * $qk.'</td>
										<td>'.implode(',',$newSku).'<br>'.implode(',',$po_barcode).'</td>
										<td>'.implode(',',$title).'</td>
										<td>'.$sk.'</td>
										<td>'.$service_name.'</td>
										<td>'.$packaging_type.'</td>
										<td>
										<a href="javascript:void(0);" for="'.$orde_ids.'" class="printbulk btn btn-info btn-xs" role="button" title="Print Slip & Label" row="'.$row.'"><i class="glyphicon glyphicon-print"></i> Print</a>
										<a href="javascript:void(0);" for="'.$orde_ids.'" class="processbulk btn btn-success btn-xs" row="'.$row.'">Process</a>
										</td>';
										$html .= '</tr>';
						}
					}
			   }
			  	$html .='</table>';
				echo json_encode(array('html'=>$html));
			    exit;
		   }
		
		public function processBulkOrders()
		{
			$this->layout = '';
			$this->autoRander = false;
			$this->loadModel('OpenOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('ScanOrder');
			
			$userid		=	$this->Session->read('Auth.User.id');
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
			
			$username = $firstName.' '.$lastName;
			
			//$this->request->data['ids'] = "1012964-1,1011405-1,1011327-1,1009488-1,1008181-1";
			$ids = explode(",",$this->request->data['ids']);
			$batch_num = 'BPN'.date('YmdHis');
			foreach($ids as $id)
			{
				$openOrderId	=	explode("-",$id)[0];
				$splitOrderId	=	$id;
				
				$result	=	$this->MergeUpdate->updateAll(
								array('MergeUpdate.assign_user' => "'".$username."'", 'MergeUpdate.user_name' => "'".$userid."'", 'MergeUpdate.pc_name' =>"'".$pcName."'",  'MergeUpdate.status' => '1','MergeUpdate.manifest_status' => '2','MergeUpdate.batch_num' => "'".$batch_num."'", 'MergeUpdate.process_date' =>"'".date('Y-m-d H:i:s')."'", 'MergeUpdate.user_id' => $userid), 
								array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId));
			
				App::import('Controller', 'Cronjobs');
				$cronjobtModel = new CronjobsController();
				$cronjobtModel->call_service_counter( $splitOrderId );
				$result = true;
				if($result)
				{
					$getmergeOrders	=	$this->MergeUpdate->find( 'all', array('conditions' => array( 'MergeUpdate.order_id' =>  $openOrderId )) );
					$checkStatus = 0;
					$checkNonProcessStatus = 0;
					
						
					   
						foreach( $getmergeOrders as $getmergeOrder )
						{
							if( $getmergeOrder['MergeUpdate']['status'] == 0 ){
								$checkNonProcessStatus++;
							} else {
								$checkStatus++;
							}
						}
						if($checkNonProcessStatus == 0 && $checkStatus > 0)
						{
							$this->OpenOrder->updateAll(array('OpenOrder.status' => '1','OpenOrder.process_date' => "'".date('Y-m-d H:i:s')."'" ), array('OpenOrder.num_order_id' => $openOrderId));
							$scan_orders	=	$this->ScanOrder->find( 'all', array( 'conditions' => array( 'split_order_id' => $splitOrderId ) ) );
							foreach( $scan_orders as $scan_order)
							{
								$data['update_quantity'] 	= 	$scan_order['ScanOrder']['quantity'];
								$data['id'] 				= 	$scan_order['ScanOrder']['id'];
								$scan_orders				=	$this->ScanOrder->saveAll( $data );
							}
							 $bulk_process_log = "Order id=".$splitOrderId."\tUser=".$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name')."\t".date('Y-m-d H:i:s')."\n";
							file_put_contents(WWW_ROOT."img/bulk_process.log", $bulk_process_log, FILE_APPEND | LOCK_EX);
						}
						else
						{
							echo "2";
						}
				}
				else
				{
					echo "3";
				}
			}
			echo "1";
			exit;
		}

		public function genearateBulkSlipLabel($oids = null)
		{
			$this->autoRender = false;
			$this->layout = '';
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'Product' );
			$this->loadMOdel( 'CategoryContant' );
			$this->loadMOdel( 'Template' );
			$this->loadMOdel( 'BulkLabel' );
			$this->loadModel('MergeUpdate');
			$this->loadModel('PackagingSlip');
			
			//pr($this->request->data['ids']);
			//$this->request->data['ids'] = "1404315-1,1404497-1,1404498-1,1404554-1,1404529-1,1404602-1,1404765-1,1404785-1,1404860-1,1404895-1,1404894-1";
			/*$this->request->data['ids'] = "1356639-1,1358870-1,1359840-1,1361757-1,1362596-1,1362679-1,1362953-1,1363252-1,1363426-1,1363990-1,1364186-1,1365697-1,1365752-1,1366300-1,1366713-1,1366837-1,1367413-1,1367859-1,1367860-1,1368283-1,1369316-1,1370016-1,1370557-1,1371336-1";*/
			

			//$this->request->data['ids'] = "1676409-1,1676363-1";
			
			//$this->request->data['ids'] = "1387236-1,1387220-1,1387282-1,1387646-1,1387625-1,1387900-1,1387939-1,1387941-1,1387902-1,1388011-1,1388011-2,1388230-1,1388565-1,1388804-1,1388856-1,1389189-1,1389279-1,1389279-2,1389402-1,1389600-1,1389644-1,1390009-1,1390570-1,1390757-1,1390774-1,1390860-1,1390904-1,1391125-1,1391421-1,1391473-1,1391622-1,1391689-1,1391747-1,1391798-1,1391798-2,1391798-3,1392173-1";
			
			if(!empty($oids)){
				$ids = $this->request->data['ids'] = $oids;
			} 
			
			$ids = explode(",",$this->request->data['ids']);
			 
			
			App::Import('Controller', 'RoyalMail'); 
			$royal = new RoyalMailController;
			foreach($ids as $id)
			{
				sleep(2);
				$openOrderId	=	explode("-",$id)[0];
				$splitOrderId	=	$id;
				$get_sp_order	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.product_order_id_identify' => $id)));
				$get_sp_order['MergeUpdate']['service_provider'];
				if( $get_sp_order['MergeUpdate']['service_provider'] == 'Royalmail' ){
					$royal->applyRoyalMailByOrder($splitOrderId);
					$label 	= 	$this->royalMailLabel($splitOrderId, $openOrderId);
				} else if( $get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] == 'Spain' ){
					$label 	= 	$this->jerseyPostLabel($splitOrderId, $openOrderId);
				} else if( $get_sp_order['MergeUpdate']['service_provider'] == 'PostNL' && $get_sp_order['MergeUpdate']['delevery_country'] == 'Italy' ){
					$label 	= 	Router::url('/', true).'img/brt/label_'.$splitOrderId.'.pdf';
				}	else {
					$label 	=	$this->getLabel( $splitOrderId, $openOrderId );
				}
				
				
				$slip 	=	$this->getSlip( $splitOrderId, $openOrderId );
				//$label 	=	'';//$this->getLabel( $splitOrderId, $openOrderId );
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				$date = date("m/d/Y H:i:s");	
				$finArray = array();
				if($get_sp_order['MergeUpdate']['service_provider'] == 'Royalmail')
				{
					$html = '';
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="margin-top:12px; height:1090px;  width:740px; border:1px solid #CCC;"> 
							<tr><td height="300" valign="top">'.$slip.'</td></tr> 
							<tr><td valign="top" ><div style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
							transform-origin: 50% 50%;">'.$label.'</div></td></tr>
							</table>';
				}
				else if($get_sp_order['MergeUpdate']['delevery_country'] == 'Italy')
				{
					$html = '';
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
							 
							 
					$html .= '<table style="margin-top:12px; height:1090px;  width:740px; border:1px solid #CCC;"> 
							<tr><td height="300" valign="top">'.$slip.'</td></tr> 
							<tr><td valign="top" ><embed src="'.$label.'" type="application/pdf" width="100%" height="600px" /></td></tr>
							</table>';
				}
				else if($get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] == 'Spain' )
				{
					$html = '';
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="margin-top:12px; height:1070px;  width:740px; border:1px solid #CCC;"> 
							<tr><td height="530" valign="top">'.$slip.'</td></tr> 
							<tr><td valign="top" style="height:367px; width:550px; border:1px solid gray;">'.$label.'</td></tr>
							</table>';
				}
				else
				{
				
				$html = '';
				$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 			 <meta content="" name="description"/>
			 			 <meta content="" name="author"/>';
				$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
						<tr><td style="height:700px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr><td valign="top" style="height:367px; width:520px; padding:0px 0px 5px 12px;border:1px solid gray;">'.$label.'</td><td></td></tr>
						</table>';
				}
				
				$cssPath = WWW_ROOT .'css/';
				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				//echo $html;
				//exit;
				//$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
				$dompdf->load_html($html, Configure::read('App.encoding'));
				$dompdf->render();
				$imgPath = WWW_ROOT .'img/printPDF/Bulk/'; 
				$path = Router::url('/', true).'img/printPDF/Bulk/';
				$name	=	'Label_Slip_'.$splitOrderId.'.pdf';
				file_put_contents($imgPath.$name, $dompdf->output());
				
				//460328
				//306161
				/*$serverPath  	= 	$path.$name ;
				$printerId		=	'306161';
				$sendData = array(
					'printerId' => $printerId,
					'title' => 'Now Print',
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'
				);
				
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();*/
				//$d = $Coreprinter->toPrint( $sendData );
			}	
			echo "1";
			exit;	
			
			
		}
		
		public function jerseyPostLabel( $splitOrderId = null, $openOrderId = null )
		{
				
				$openOrderId	=	$openOrderId ;
				$splitOrderId	=	$splitOrderId ;
				App::import('Controller', 'Linnworksapis');
				$obj = new LinnworksapisController();
				
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				$order				=		$obj->getOpenOrderById( $openOrderId );
				$postalservices		=		$order['shipping_info']->PostalServiceName;
				$destination		=		$order['customer_info']->Address->Country;
				$total				=		$order['totals_info']->TotalCharge;
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$currency			=	 	$order['totals_info']->Currency;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$templateId			=	 	$order['template_id'];
				$subSource			=	 	$order['sub_source'];
				
				$str = ''; 
				
				$totlaWeight	=	0;
				$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
				$ref_code = trim($splitOrderDetail['MergeUpdate']['provider_ref_code']);
				$weight = 0;
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					
					foreach($newSkus as $newSku)
						{
							
							$totalvalue = $total;
							$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
							$getsku = 'S-'.$newSku[1];
							$qty = $newSku[0];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title	=	$getOrderDetail['Product']['product_name'];
							$weight	=	$weight + $getOrderDetail['ProductDesc']['weight'];
							$catName	=	$getOrderDetail['Product']['category_name'];
							if($country == 'United States')
							{
								$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.$catName.'</td>
									<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';
							}
							elseif($country == 'Spain')
								{
									$codearray = array("DP1", "FR7");
									$spaincode = array("mad_air","mad_road","cat_air","cat_road","sp_air","sp_road");
							
									if(in_array($ref_code, $spaincode))
									{
										$str .='<tr>
												<td  width="60%"  style="border:1px solid #000; padding:5px;">'.$catName.'</td>
												<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty * $getOrderDetail['ProductDesc']['weight'].'</td>
												<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty.'</td>';
										$str .= '</tr>';
									} else {
										$str .= '<tr>
											<td valign="top" class="noleftborder rightborder bottomborder">'.$catName.'</td>
											<td valign="top" class="center noleftborder rightborder bottomborder">'.$qty.'</td>
											<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
										$str .= '</tr>';
									}
								}
							else
							{
								$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.substr($title, 0, 25 ).'</td>
										<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';
							}
						}
							unset($newSkus);
				}
				
				$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forl?-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia');
				
				$addData['company'] 	= 	$company;
				$addData['fullname'] 	= 	$fullname;
				$addData['address1'] 	= 	$address1;
				$addData['address2'] 	= 	$address2;
				$addData['address3'] 	= 	$address3;
				$addData['town'] 		= 	$town;
				
				if( $country == 'Italy' )
				{
					if(array_key_exists(strtoupper($resion),$provinces)){
						$addData['resion'] 		= 	$resion;
					}else {
						$addData['resion'] 		= 	array_search(ucfirst($resion), $provinces);
					}
				} else {
						$addData['resion'] 		= 	$resion;
				}
				
				$addData['postcode'] 	= 	$postcode;
				$addData['country'] 	= 	$country;
				
				
				$address 			= 		$obj->getCountryAddress( $addData );
				
				$CARTAS = '';
				if(trim($country) == 'Spain') { 
					$CARTAS = 'CARTAS '.substr($postcode, 0, 1);
					$routingcode = $this->getCorreosBarcode( $postcode, $splitOrderId);
					$corriosImage		=		$routingcode.'.png';
					if(substr($postcode, 0, 1) == 0){
						$CARTAS = 'CARTAS '.substr($postcode, 1, 1);
					}	
				}
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				$currentdate		=	 	date("j, F  Y");
				$BarcodeImage		=		$splitOrderDetail['MergeUpdate']['order_barcode_image'];
				//$corriosImage		=		$postcode.'90019.png';
				
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= str_replace( 'Spain' , 'Spain', $address); 
				$setRepArray[] 					= $country;
				$barcode  						= $order['assign_barcode'];
				$barcodenum						= explode('.', $barcode);
				$barcodePath  					= Router::url('/', true).'img/orders/barcode/';
				$barcodeCorriosPath  			= Router::url('/', true).'img/orders/correos_barcode/';
				
				$barcodenum						= explode('.', $barcode);
			
				/**************** for tempplate *******************/
				$countryArray = Configure::read('customCountry');
				if($country == 'United Kingdom'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				} else if($country != 'United Kingdom'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Rest Of EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				} else {
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Rest Of EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				}
				//pr($labelDetail);
				if($subSource == 'Marec_FR' || $subSource == 'Marec_DE' || $subSource == 'Marec_IT' || $subSource == 'Marec_ES' || $subSource == 'Marec_uk'){
				$company 	=  'Marec';
					} else if($subSource == 'CostBreaker_UK' || $subSource == 'CostBreaker_DE' || $subSource == 'CostBreaker_FR' || $subSource == 'CostBreaker_ES' || $subSource == 'CostBreaker_IT' ){
						$company 	=  'CostBreaker';
					} else if($subSource == 'Tech_Drive_UK' || $subSource == 'Tech_Drive_FR' || $subSource == 'Tech_drive_ES' || $subSource == 'Tech_drive_DE' || $subSource == 'Tech_Drive_IT' ){
						$company 	= 'Tech Drive Supplies';
					} else if($subSource == 'RAINBOW RETAIL DE' || $subSource == 'Rainbow Retail' || $subSource == 'Rainbow_Retail_ES' || $subSource == 'Rainbow_Retail_IT'){
						$company 	= 'Rainbow Retail';
					}
					$this->loadModel( 'ReturnAddre' );
					$ret_add = $this->ReturnAddre->find('first', array( 'conditions' => array( 'company' =>  $company) ) );
					$return_address = $ret_add['ReturnAddre']['return_address'];
				
					$html           =  $labelDetail['BulkLabel']['html'];
				
				
				$barcodeimg = '<img src='.$barcodePath.$BarcodeImage.'>';
			
				$setRepArray[] 	=    $barcodeimg;
				$setRepArray[] 	=    $barcodenum[0];
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totlaWeight;
				$setRepArray[]	=	 $tax;
				$setRepArray[]	=	 $currentdate;
				$logopath		=	 Router::url('/', true).'img/';
				$setRepArray[]	=	 '<img src="'.$logopath.'logo.jpg" >';
				$logo			=	 '<img src="'.$logopath.'logo.jpg" >';
				$signature		=	 '<img src="'.$logopath.'sig.jpg" >';
				
				$setRepArray[]	=	 $logo;
				$setRepArray[]	=	 $signature;
				$setRepArray[]	=	 $splitOrderId;
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				
				$tempId			=	$splitOrderDetail['MergeUpdate']['lable_id'];
				$countryCode	=	$splitOrderDetail['MergeUpdate']['country_code'];
				
				$setRepArray[]	=	 $countryCode;
				$setRepArray[]	=	 $weight;
				$conversionRate	=	Configure::read( 'conversionRate' );
				if($currency == 'GBP')
				{
					$totalvalue = $totalvalue;
				}
				elseif($country == 'Spain' && $currency == 'EUR')
				{
					$totalvalue = $totalvalue;
				}
				elseif($country == 'United States')
				{
					$totalvalue = $totalvalue;
				}
				else
				{
					$totalvalue = number_format($totalvalue/$conversionRate, 2, '.' ,'');
				}
				
				if($totalvalue >= 1000  && $labelDetail['Template']['label_name'] == 'Jersey Post' && $country == 'United States' ){
					$numbers = array("97.11","98.32","98.52","99.72","99.94","150.27","200.45","270.62","370.82","422.11","472.31","530.71");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				else if($totalvalue >= 22  && $labelDetail['Template']['label_name'] == 'Jersey Post' && $country != 'United States'){
					$numbers = array("18.11","18.32","18.52","18.72","18.94","19.27","19.45","19.62","19.82","20.11","20.31","20.71","21.22","21.41", "21.61","21.51");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				
				$setRepArray[]	=	 $totalvalue;
				if($splitOrderDetail['MergeUpdate']['track_id'] != '')
				{
					$trackingnumber = $splitOrderDetail['MergeUpdate']['track_id'];
				}
				else
				{
					$trackingnumber = 'A'.mt_rand(100000, 999999);
				}
				
				$setRepArray[]	=	 $trackingnumber;
				$setRepArray[]	=	 $subSource;
				$setRepArray[]	=	 $CARTAS;
				$setRepArray[]	=	 $splitOrderDetail['MergeUpdate']['reg_num_img'];
				$setRepArray[]	=	 '<img src='.$barcodeCorriosPath.$corriosImage.'>';
				$setRepArray[]	=	($phone != '') ? 'Telefono del cliente:'.$phone : '';
				$setRepArray[]	=	$return_address;
				$setRepArray[]	=	WWW_ROOT;
				
				$cssPath = WWW_ROOT .'css/';
				$imgPath = Router::url('/', true) .'img/';
				//$sinimage	=	 '<img src='.$imgPath.'signature.png>';
				
				$this->loadModel( 'ReturnAddre' );
				$ret_add = $this->ReturnAddre->find('first', array( 'conditions' => array( 'company' =>  $company) ) );
				$return_address = $ret_add['ReturnAddre']['return_address'];
				$html = '<div style="font-family:normal;">
						 <table width=78%>
						 	<tr width=100%>
								<td width=50% style="border: 1px solid #ccc">
									<table>
										<tr>									
											<td>	
												<div style="padding-left:3px" ><strong>Remitente</strong></div>				
												<div style="padding-left:3px" ><strong>ESL LOGISTICS</strong><br>CTL CHAMARTIN<br>AVDA. PIO XII 106-108<br>28070 MADRID</div>
											</td>
											<td>
												<table cellpadding="0" cellspacing="0" width="80%" style="border:1px solid #000;">
													<tr>					 
														<td>
															<div style="text-align: center;font-weight:bold; height:80px;">
																<div style="border-bottom:1px solid #000; padding:2px;"> FRANQUEO <br> PAGADO </div>
																<div style="padding:2px 5px;">Distribuci&#243;n <br> Internacional </div>
															</div>
														</td>				
													</tr>
												</table>
											</td>	
										</tr>
										<tr>
											<td align="left" colspan="2" > 
												<div style="border-bottom:1px solid; width:80%;font-size:12px; margin-top: 10px; "><strong>Destinatario</strong></div>		
												 <div style="font-size:13px; padding-left:3px">_ADDRESS_</div>
											</td>
										</tr>
										<tr>
											<td colspan="2" align="center" style="padding-bottom:1px; padding-top:1px;"><div>ENVÍO ORDINARIO</div> <div style="text-align: center;"><center> _BARCODEIMAGE_</center> <div></td>
										</tr> 
										<tr>
											<td colspan="2" align="center"><div style="margin-top:1px;">_CORRIOSBARCODE_</div></td>
										</tr> 
									</table>
								</td>
								
								<td width=50% style="border: 1px solid #ccc" valign="top">
									<table style="border:1px solid #000;">
										<tr valign="top">
											<td width=48%><strong>CUSTOMS DECLARATION</strong></td>
											<td style="text-align:right;"><strong>CN 22</strong></td>
										</tr>
										<tr valign="top">
											<td width=48% style="font-size:10px;">DECLARATION EN DOUANE</td>
											<td style="text-align:right;">May be opened officially</td>
										</tr>
										<tr>
											<td>Great Britain / Important!</td>
											<td style="text-align:right;"></td>
										</tr>
										<tr>
											<td width="15%"><span style="height:25;width:25; border:2px solid gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Gift</td>
											<td width="25%"><span style="height:25;width:25; border:2px solid gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Documents</td>
										</tr>
										<tr>
											<td width="30%"><span style="height:25;width:25; border:2px solid gray">&nbsp;X&nbsp;</span>Commercial</td>
											<td width="30%"><span style="height:25;width:25; border:2px solid gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Merchandise</td>
										</tr>
									</table>
									<table class="" cellpadding="1px" cellspacing="0" style="border-collapse:collapse" width="100%">
										<tr>
											<th width="60%" style="border:1px solid #000; padding:5px;">Quantity and detailed</th>
											<th valign="top" width="20%"  style="border:1px solid #000; padding:5px;">Weight</th>
											<th valign="top" width="20%"  style="border:1px solid #000; padding:5px;">Qty</th>
										</tr>
										_ORDERDETAIL_
										
										<tr>
											<td  width="60%" style="border:1px solid #000; padding:10px; padding:5px;"> <span class="bold">For commercial items only</td>
											<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">WEIGHT _TOTALWEIGHT_</td>
											<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">VALUE &#8364; _TOTALVALUE_</td>
										</tr>
										</table>
											<div class="fullwidth" style="border:1px solid #000; padding:5px;"><p>I the undersigned, whose name and address are given on the item, certify that the particulars given in this declaration are correct and that this item does not contain any dangerous article or articles prohibited by legislation or by postal or customs regulations.</p>
										<table>
										<tr>
											<td class="date bold">_CURRENTDATE_</td>
											<td><img src="_DYURL_/img/signature.png" height="50px"></td>
										</tr>
										</table>	
										
										</table>
								</td>
							</tr>
						 </table></div>';	
				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				
			echo	$html 	= $obj->setReplaceValueLabel( $setRepArray, $html );
				return $html;
		}
		
		public function getCorreosBarcode( $postcode = null, $splitOrderID = null )
		{
			
			
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 

			$barcodePath 		= 	WWW_ROOT .'img/orders/barcode/';   
			$barcodecorreosPath = 	WWW_ROOT .'img/orders/correos_barcode/';  
			
			$colorFront 		= 	new BCGColor(0, 0, 0);
			$colorBack 			= 	new BCGColor(255, 255, 255);
			
			// Barcode Part
			$orderbarcode=$splitOrderID;
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->clearLabels();
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->parse($orderbarcode);
			// Drawing Part
			$imgOrder128=$orderbarcode;
			$imgOrder128path=$barcodePath.'/'.$imgOrder128.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			############ Routing Barcode #######
			//$destination = $countryCode;
			//$origin ="90019";
			//$routingcode=$destination.$origin;
			
			 
			 
			$routingcode = $this->getCorreosCalculatedBarcode($postcode);
			$routingcode128 = new BCGcode128();
			$routingcode128->setScale(1);
			$routingcode128->setThickness(40);
			$routingcode128->setForegroundColor($colorFront);
			$routingcode128->setBackgroundColor($colorBack);
			$routingcode128->parse(array(CODE128_B, $routingcode));
			//$routingcode128->parse($routingcode);
			// Drawing Part
			$routingimg =$routingcode;
			$routingdrawing128path=$barcodecorreosPath.'/'.$routingimg.".png";
			$routingdrawing128 = new BCGDrawing($routingdrawing128path, $colorBack);
			$routingdrawing128->setBarcode($routingcode128);
			$routingdrawing128->draw();
			$routingdrawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			return $routingcode;
		}
		
		public function getCorreosCalculatedBarcode($postcode = null)
		 {
			$sum_accii = 0;
			
			$code = 'ORDCP'.$postcode; 		
			for($i=0; $i < strlen($code); $i++){
			  $sum_accii += ord($code[$i]);		 
			}
			$remainder = fmod($sum_accii,23);
			$table = array('T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E');
			$control_character = $table[$remainder];
			return	$code.$control_character;			
		} 
		
		
		public function royalMailLabel( $split_id = null, $order_id = null )
		{
			//.$details['split_order_id'].
			$details = $this->getDetail( $split_id, $order_id );
			$service = '48';
			if($details['type'] == 1 ){
				$service = '24';
			}
			
			$box_number = 824; 
			if(strpos($details['sub_source'],'CostBreaker')!== false){
				$box_number = 824;
			}
			else if(strpos($details['sub_source'],'RAINBOW')!== false){
				$box_number = 825;;
			}else if(strpos($details['sub_source'],'Marec')!== false){
				$box_number = 826;
			}	
			elseif(strpos($details['sub_source'],'BBD')!== false){
				$box_number = 827;
			}
			else if(strpos($details['sub_source'],'Tech_Drive')!== false){
				$box_number = 828;
			} 
			$html = '
					<div style=" width:320px; height:auto;margin-top:-250px; margin-left:-210px;">
					<div class="container"  style=" border:1px solid #000;">
					<table  width="100%" cellpadding="0" cellspacing="10">
					<tbody>
					<tr>
					<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/Royal_Mail.png" width="115px"></td>
					<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/'.$service.'.jpg" width="50px"></td>
					<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/royal.png" width="100px" height="70px"></td>
					</tr>
					</tbody>
					</table>
					 
					
					<table width="100%" cellpadding="0" cellspacing="10" style="border-bottom:1px solid #000; border-top:1px solid #000;">
					<tbody>
					<tr>
					<td valign="top" width="50%" align="left" >
					<img src="'.WWW_ROOT.'royalmail/barcode/2d_matrix_'.$details['split_order_id'].'.png" width="83px" height="83px" style="margin-left:10px;">
					</td>
				
					</tr>
					</tbody>
					</table>
					
					<table width="100%"  cellpadding="0" cellspacing="10" >
					<tbody>
					<tr>
					<td valign="top" width="75%">';
					$html .='<strong>'. ucwords(strtolower($details['FullName'])).'</strong><br>';
		
					if(strlen($details['Address1']) > 25 || strlen($details['Address2']) > 25){
					
						$lines = explode("\n", wordwrap(htmlentities($details['Address1']) .' '.htmlentities($details['Address2']), '25'));
						if(isset($lines[0]) && $lines[0] != ''){
							$html .=  $lines[0].'<br>';
						}
						if(isset($lines[1]) && $lines[1] != ''){
							$html .=  $lines[1].'<br>';
						}
						if(isset($lines[2]) && $lines[2] != ''){
							$html .=  $lines[2].'<br>';
						}
					
					}else{
						$html .= ucfirst($details['Address1']).'<br>';
						if($details['Address2']) $html .= ucfirst($details['Address2']).'<br>';
					}
					
					
					$html .= ucfirst($details['Town']).'<br>';
					$html .= $details['PostCode'];
					$html .= '</div></td>';
					
					
					$html .='<td valign="bottom" width="25%" align="left">
					<img src="'.WWW_ROOT.'royalmail/img/'.$box_number.'.png" style="height:100px; margin-left:20px;">
					 </td>
					 </tr>
					</tbody>   
					 </table>
					
					 
					<table width="100%" style="border-top:1px solid #000;" >
					<tbody>
					<tr>
					<td valign="top" align="center" style="padding-top:5px;"><p style="font-family:chevin; font-size:12px;">Customer Reference:'.$details['split_order_id'].'</p>
					
					</td>
					</tr>
					</tbody>
					</table>
				 	 </div>
					 <div  style="text-align:center;width:100%;padding-top:25px;">
						<img src="'.WWW_ROOT.'img/orders/barcode/'.$details['split_order_id'].'.png" style="text-align:center">
					 </div></div>';
					return $html;
		}
		
		
		
		public function getDetail( $split_id = null, $order_id = null )
		{
			
			$this->loadModel( 'Country' );
			App::import( 'Controller' , 'Cronjobs' );		
			$objController 	= new CronjobsController();
			$getorderDetail	= $objController->getOpenOrderById($order_id);
			$cInfo = $getorderDetail['customer_info'];
			$Address1 = $cInfo->Address->Address1;
			$Address2 = $cInfo->Address->Address2;
			$order->TotalsInfo = $getorderDetail['totals_info'];
			$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
			
			$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_id,'MergeUpdate.service_provider' => 'Royalmail')));	
			$type = 2; 
			if($orderItem['MergeUpdate']['service_name'] == 'royalmail_24'){
				$type = 1;
			}
			$service_format = 'P';
			if($orderItem['MergeUpdate']['service_name'] == 'royalmail_LL_48'){
				/*  F	Inland Large Letter, L	Inland Letter, N	Inland format Not Applicable, P	Inland Parcel */
				$service_format = 'F';
			}
			
			$_details = array('merge_id'=> $order_id,
						'split_order_id'=> $split_id,
						'sub_total' => number_format($order->TotalsInfo->Subtotal,2),
						'order_total'=> number_format($order->TotalsInfo->TotalCharge,2),
						'postage_cost' => number_format($order->TotalsInfo->PostageCost,2),
						'order_currency'=>$order->TotalsInfo->Currency,			
						'Company'=>$cInfo->Address->Company,
						'FullName'=> utf8_decode($cInfo->Address->FullName),
						'Address1' =>utf8_decode($Address1),
						'Address2' =>utf8_decode($Address2),
						'Town' =>utf8_decode($cInfo->Address->Town),
						'Region' =>utf8_decode($cInfo->Address->Region),
						'PostCode' =>$cInfo->Address->PostCode,
						'CountryName'=>$cInfo->Address->Country,
						'CountryCode' =>$country_data['Country']['iso_2'],
						'PhoneNumber'=>$cInfo->Address->PhoneNumber	,
						'sub_source' => $getorderDetail['sub_source'],
						'type' 		=> $type,
						'service_format' => $service_format,
						'item_count'=> 1,
						'building_name' => '',
						'building_number' => 0,
					);
					return $_details;
			
		}
		
		public function getSlip($splitOrderId = null, $openOrderId = null)
		{
  				$this->loadModel('OrderLocation');
				App::import('Controller', 'Linnworksapis');
				$obj = new LinnworksapisController();
				$order	=	$obj->getOpenOrderById( $openOrderId );
				
				$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
				$itemPrice			=		$getSplitOrder['MergeUpdate']['price'];
				$itemQuantity		=		$getSplitOrder['MergeUpdate']['quantity'];
				$BarcodeImage		=		$getSplitOrder['MergeUpdate']['order_barcode_image'];
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				$subtotal			=		$order['totals_info']->Subtotal;
				$subsource			=		$order['sub_source'];
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$address			=		'';
				$address			.=		($company != '') ? $company.'<br>' : '' ; 
				$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
				$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
				$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
				$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
				$address			.=		($town != '') ? $town.'<br>' : '';
				$address			.=		($resion != '') ? $resion.'<br>' : '';
				$address			.=		($postcode != '') ? $postcode.'<br>' : '';
				$address			.=		($country != '' ) ? $country.'<br>' : '';
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);//CostBreaker
				if($subsource == 'Marec_FR' || $subsource == 'Marec_DE' || $subsource == 'Marec_IT' || $subsource == 'Marec_ES' || $subsource == 'Marec_uk'){
						$company 	=  'Marec';
					} else if($subsource == 'CostBreaker' || $subsource == 'CostBreaker_UK' || $subsource == 'CostBreaker_DE' || $subsource == 'CostBreaker_FR' || $subsource == 'CostBreaker_ES' || $subsource == 'CostBreaker_IT' ){
						$company 	=  'CostBreaker';
					} else if($subsource == 'Tech_Drive_UK' || $subsource == 'Tech_Drive_FR' || $subsource == 'Tech_drive_ES' || $subsource == 'Tech_Drive_DE' || $subsource == 'Tech_Drive_IT' ){
						$company 	= 'Tech Drive Supplies';
					} else if($subsource == 'RAINBOW RETAIL DE' || $subsource == 'Rainbow Retail' || $subsource == 'Rainbow_Retail_ES' || $subsource == 'Rainbow_Retail_IT' || $subsource == 'Rainbow_Retail_FR'){
						$company 	= 'Rainbow Retail';
					}  else if( $subsource == 'EBAY2' ){
						$company 	= 'EBAY2';
					} else if( $subsource == 'CostBreaker_USABuyer' ){
						$company 	= 'CostBreaker_USA';
					} else if( $subsource == 'CostBreaker_CA' ){
						$company 	= 'CostBreaker_CA';
					} else if( $subsource == 'BBD_EU_DE' ){
						$company 	= 'BBD';
					} else if( $subsource == 'EBAY0' ){
						$company 	= 'EBAY0';
					}
				
				
				$i = 1;
				$str = '';
				$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
				$totalGoods = 0;
				$productInstructions = '';
				$ref_code = trim($getSplitOrder['MergeUpdate']['provider_ref_code']);
				$codearray = array('DP1', 'FR7');
				$this->loadModel( 'BulkSlip' );
				$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => $company ) ) );
				
				if( in_array($ref_code, $codearray)) 
				{
					
					$per_item_pcost 	= $postagecost/count($items);
					$j = 1;
					foreach($items as $item){
					$item_title 		= 	$item->Title;
					$quantity			=	$item->Quantity;
					$price_per_unit		=	$item->PricePerUnit;
					$str .= '<tr>
								<td style="border:1px solid #000;" align="left" valign="top" width="10%">'.$quantity.'</td>
								<td style="border:1px solid #000;" valign="top" width="20%">'.substr($item_title, 0, 15 ).'</td>
								<td style="border:1px solid #000;" valign="top" width="15%">'.$quantity * $price_per_unit.'</td>
								<td style="border:1px solid #000;" valign="top" width="15%">'.$per_item_pcost.'</td>
								<td style="border:1px solid #000;" valign="top" width="20%">'.(($price_per_unit * $quantity) + $per_item_pcost).'</td>
							</tr>';
					$j++;
					}
					$str .=	'<tr><td></td><td></td><td></td><td style="border:1px solid #000;">Total</td>
								<td style="border:1px solid #000;" valign="top" width="20%">'.$totalcharge.'</td>
							</tr>';
					echo $str;
					
				} 
				else 
				{
					//$this->loadMOdel( 'InkSku' );
					//$c_ink = 0;
					foreach( $skus as $sku )
					{
					
						$newSkus[]				=	 explode( 'XS-', $sku);
						$cat_name = array();
						$sub_can_name = array();
						foreach($newSkus as $newSku)
							{
								
								$getsku = 'S-'.$newSku[1];
								$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
								$OrderLocation 	= 	$this->OrderLocation->find( 'first', array( 'conditions' => array('order_id' => $openOrderId, 'sku' => $getsku ) )  );
								if( count($OrderLocation) > 0 ){
									$o_location 	=	$OrderLocation['OrderLocation']['bin_location'];
								} else {
									$o_location 	=	'Check Location' ;
								}
								
								$contentCat 	= 	$getOrderDetail['Product']['category_name'];
								$cat_name[] 	= 	$getOrderDetail['Product']['category_name'];
								$contentSubCat	=	$getOrderDetail['Product']['sub_category'];
								$sub_can_name[] =	$getOrderDetail['Product']['sub_category'];
								$productBarcode	=	$getOrderDetail['ProductDesc']['barcode'];
								
								/*$check_ink		=	$this->InkSku->find( 'first', array( 'conditions' => array( 'sku' => $getsku ) ) );
								if( count($check_ink) > 0 ){
									$c_ink++;
								}*/
								$getContent	= $this->CategoryContant->find( 'first', array( 
																	'conditions'=>array('CategoryContant.sub_category'=>$contentSubCat,'CategoryContant.sub_source'=>$subsource)));
								
								
								if(count($getContent) > 0) {
									$productBarcode			=	$getOrderDetail['ProductDesc']['barcode'];
									$getbarcode				= 	$obj->getBarcode( $productBarcode );
									$productInstructions 	=  '<p style="font-size:14px;">'.$getContent['CategoryContant']['content'].'</p>';
								}
								
								$title	=	$getOrderDetail['Product']['product_name'];
								$totalGoods = $totalGoods + $newSku[0];
								echo $l_barcode		=	$this->GlobalBarcode->getLocalBarcode($productBarcode);
								$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
										<td valign="top" class="rightborder bottomborder">'.substr($title, 0, 15 ).'</td>
										<td valign="top" class="rightborder bottomborder" >'.$l_barcode.'</td>
										<td valign="top" class="rightborder bottomborder">'.$o_location.'</td>
										<td valign="top" class="center norightborder bottomborder">'.$newSku[0].'</td>';
								$str .= '</tr>';
								/*$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
										<td valign="top" class="rightborder bottomborder">'.substr($title, 0, 15 ).'</td>
										<td valign="top" class="center norightborder bottomborder">'.$newSku[0].'</td>';
								$str .= '</tr>';*/
								
								$i++;
								
							}
							unset($newSkus);
					}
					
				}
						
								
				$html 			=	$gethtml['PackagingSlip']['html'];
				$paperHeight    =   $gethtml['PackagingSlip']['paper_height'];
				$paperWidth  	=   $gethtml['PackagingSlip']['paper_width'];
				$barcodeHeight  =   $gethtml['PackagingSlip']['barcode_height'];
				$barcodeWidth   =   $gethtml['PackagingSlip']['barcode_width'];
				$paperMode      =   $gethtml['PackagingSlip']['paper_mode'];
				//pr($gethtml);
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= $postcode;
				$setRepArray[] 					= $country;
				$setRepArray[] 					= $phone;
				$setRepArray[] 					= $ordernumber;
				$setRepArray[] 					= $courier;
				$setRepArray[] 					= $recivedate[0];
				$totalitem = $i - 1;
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totalGoods;
				$setRepArray[]	=	 $subtotal;
				$Path 			= 	'/wms/img/client/';
				$img			=	 '';
				$setRepArray[]	=	 $img;
				$setRepArray[]	=	 $postagecost;
				$setRepArray[]	=	 $tax;
				$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
				//$setRepArray[]	=	 $totalamount;
				$setRepArray[]	=	 $itemPrice;
				$setRepArray[]	=	 $address;
				$barcodePath  	=  Router::url('/', true).'img/orders/barcode/';
				$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.' width='. $barcodeWidth .'>';
				$barcodenum		=	explode('.', $barcode);
				
				$setRepArray[] 	=  $barcodeimg;
				$setRepArray[] 	=  $paymentmethod;
				$setRepArray[] 	=  $barcodenum[0];
				$setRepArray[] 	=  '';
				$img 			=	$gethtml['PackagingSlip']['image_name'];
				$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
				$setRepArray[] 	=  $returnaddress;
				$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
				$setRepArray[] 	= $splitOrderId;
				$setRepArray[] 	= 	utf8_decode( $productInstructions );
			//	$prodBarcodePath  	=  Router::url('/', true).'img/product/barcodes/';			
				$prodBarcodePath  	= WWW_ROOT.'img/product/barcodes/';
				$setRepArray[] 	=  '<img src='.$prodBarcodePath.$productBarcode.'.png width='. $barcodeWidth .'>';
				$setRepArray[] 	=  $totalamount;
				$setRepArray[]	=	WWW_ROOT;
				 /*--------------Ink------------------*/
				$this->loadMOdel( 'InkOrderLocation' );
				$ink_envelope   = 0;
				$order_ink	    = $this->InkOrderLocation->find('all', array('conditions' => array('order_id' => $openOrderId,'status'=>'active')));
				//pr($order_ink);
				if(count($order_ink) > 0 ){
					foreach($order_ink as $_ink){
					 	$ink_envelope = $ink_envelope + $_ink['InkOrderLocation']['quantity'];					 
					}
					$setRepArray[] 	=  '<div style="color:green;font-size:14px;"><center>#'.$ink_envelope.' Envelope Required.</center></div><br><img src="'.Router::url('/', true).'img/ink_xl2.jpg" height="130">';
				}
				 /*--------------End of Ink------------------*/
				 
				$imgPath = WWW_ROOT .'css/';
			 	$html2 			= 	$gethtml['BulkSlip']['html'];
				
				if( $company ==  'Marec' && in_array(  'Mobile Accessories', $cat_name ))
				{
					$this->MobileAsseInstruction( $splitOrderId, $subsource  );
				}
				$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
				$html 		= 	$obj->setReplaceValue( $setRepArray, $html2 );
				return $html;
				
				
				
		}
		
		public function getLabel( $splitOrderId = null, $openOrderId = null )
		{ 
				
				 
			
				$openOrderId	=	$openOrderId ;
				$splitOrderId	=	$splitOrderId ;
				 
				App::import('Controller', 'Linnworksapis');
				$obj = new LinnworksapisController();
				
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId ))); 
				 
				$order				=		$obj->getOpenOrderById( $openOrderId );
				$postalservices		=		$order['shipping_info']->PostalServiceName;
				$destination		=		$order['customer_info']->Address->Country;
				$total				=		$order['totals_info']->TotalCharge;
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$currency			=	 	$order['totals_info']->Currency;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$templateId			=	 	$order['template_id'];
				$subSource			=	 	$order['sub_source'];
				
				$str = ''; 
				
				$totlaWeight	=	0;
				$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
				$ref_code = trim($splitOrderDetail['MergeUpdate']['provider_ref_code']);
				$weight = 0;
				$europe = array('Monaco','Andorra');
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					
					foreach($newSkus as $newSku)
						{
							
							$totalvalue = $total;
							$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
							$getsku = 'S-'.$newSku[1];
							$qty = $newSku[0];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title	=	$getOrderDetail['Product']['product_name'];
							$weight	=	$weight + $getOrderDetail['ProductDesc']['weight'];
							$catName	=	$getOrderDetail['Product']['category_name'];
							if($country == 'United States')
							{
									$str .= '<tr>
										<td valign="top" class="leftborder rightborder bottomborder">'.$catName.'</td>
										<td valign="top" class="leftborder rightborder bottomborder">'.$newSku[0].'</td>
										<td valign="top" class="center rightborder bottomborder">'.$weight.'</td>';
									$str .= '</tr>';
								/*$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.$catName.'</td>
									<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';*/
							}
							elseif($country == 'Canada')
								{
									$str .= '<tr>
										<td valign="top" class="leftborder rightborder bottomborder">'.$catName.'</td>
										<td valign="top" class="leftborder rightborder bottomborder">'.$newSku[0].'</td>
										<td valign="top" class="center rightborder bottomborder">'.$weight.'</td>';
									$str .= '</tr>';
								}
							elseif($country == 'Spain')
								{
									$codearray = array("DP1", "FR7");
									$spaincode = array("mad_air","mad_road","cat_air","cat_road","sp_air","sp_road");
							
									if(in_array($ref_code, $spaincode))
									{
										$str .='<tr>
												<td  width="60%"  style="border:1px solid #000; padding:5px;">'.$catName.'</td>
												<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty * $getOrderDetail['ProductDesc']['weight'].'</td>
												<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty.'</td>';
										$str .= '</tr>';
									} else {
										$str .= '<tr>
											<td valign="top" class="noleftborder rightborder bottomborder">'.$catName.'</td>
											<td valign="top" class="center noleftborder rightborder bottomborder">'.$qty.'</td>
											<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
										$str .= '</tr>';
									}
								}
							elseif( in_array( $country, $europe ) )
								{
									$str .= '<tr>
										<td valign="top" class="leftborder rightborder bottomborder">'.$catName.'</td>
										<td valign="top" class="leftborder rightborder bottomborder">'.$newSku[0].'</td>
										<td valign="top" class="center rightborder bottomborder">'.$weight.'</td>';
									$str .= '</tr>';
								}
							else
							{
								$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.substr($title, 0, 25 ).'</td>
										<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';
							}
						}
							unset($newSkus);
				}
				
				$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forl?-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia');
				
				$cana_provinces = array('AB' =>'Alberta','BC'=>'British Columbia','MB'=>'Manitoba','NB'=>'New Brunswick','NL'=>'Newfoundland and Labrador','NS'=>'Nova Scotia','NT'=>'Northwest Territories','NU'=>'Nunavut','ON'=>'Ontario','PE'=>'Prince Edward Island','QC'=>'Quebec','SK'=>'Saskatchewan','YT'=>'Yukon');
				
				$addData['company'] 	= 	$company;
				$addData['fullname'] 	= 	$fullname;
				$addData['address1'] 	= 	$address1;
				$addData['address2'] 	= 	$address2;
				$addData['address3'] 	= 	$address3;
				$addData['town'] 		= 	$town;
				
				if( $country == 'Italy' )
				{
					if(array_key_exists(strtoupper($resion),$provinces)){
						$addData['resion'] 		= 	$resion;
					}else {
						$addData['resion'] 		= 	array_search(ucfirst($resion), $provinces);
					}
				}
				else if( $country == 'Canada' )
				{
					if(array_key_exists(strtoupper($resion),$cana_provinces)){
						$addData['resion'] 		= 	$resion;
					}else {
						$addData['resion'] 		= 	array_search(ucfirst($resion), $cana_provinces);
					}
				}
				else 
				{
						$addData['resion'] 		= 	$resion;
				}
				
				$addData['postcode'] 	= 	$postcode;
				$addData['country'] 	= 	$country;
				
				
				$address 			= 		$obj->getCountryAddress( $addData );
				
				$CARTAS = '';
				if(trim($country) == 'Spain') { 
					$CARTAS = 'CARTAS '.substr($postcode, 0, 1);
					$routingcode = $obj->getCorreosBarcode( $postcode, $splitOrderId);
					$corriosImage		=		$routingcode.'.png';
					if(substr($postcode, 0, 1) == 0){
						$CARTAS = 'CARTAS '.substr($postcode, 1, 1);
					}	
				}
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				$currentdate		=	 	date("j, F  Y");
				$BarcodeImage		=		$splitOrderDetail['MergeUpdate']['order_barcode_image'];
				$corriosImage		=		$postcode.'90019.png';
				
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= str_replace( 'Spain' , 'ESPA?A', $address); 
				$setRepArray[] 					= $country;
				$barcode  						= $order['assign_barcode'];
				$barcodenum						= explode('.', $barcode);
				$barcodePath  					= Router::url('/', true).'img/orders/barcode/';
				$barcodeCorriosPath  			= Router::url('/', true).'img/orders/correos_barcode/';
				
				$barcodenum						= explode('.', $barcode);
			
				/**************** for tempplate *******************/
				$countryArray = Configure::read('customCountry');
				
				if( in_array( $country, $europe ) )
				{
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				}
				else 
				{
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				}
				
				/*if($country == 'United Kingdom'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				} else if($country != 'United Kingdom'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Rest Of EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				} else {
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Rest Of EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				}*/
				//pr($labelDetail);
				if($subSource == 'Marec_FR' || $subSource == 'Marec_DE' || $subSource == 'Marec_IT' || $subSource == 'Marec_ES' || $subSource == 'Marec_uk'){
				$company 	=  'Marec';
					} else if($subSource == 'CostBreaker_CA' || $subSource == 'CostBreaker_UK' || $subSource == 'CostBreaker_DE' || $subSource == 'CostBreaker_FR' || $subSource == 'CostBreaker_ES' || $subSource == 'CostBreaker_IT' ){
						$company 	=  'CostBreaker';
					} else if($subSource == 'Tech_Drive_UK' || $subSource == 'Tech_Drive_FR' || $subSource == 'Tech_drive_ES' || $subSource == 'Tech_Drive_DE' || $subSource == 'Tech_Drive_IT' ){
						$company 	= 'Tech Drive Supplies';
					} else if($subSource == 'RAINBOW RETAIL DE' || $subSource == 'Rainbow Retail' || $subSource == 'Rainbow_Retail_ES' || $subSource == 'Rainbow_Retail_IT' || $subSource == 'Rainbow_Retail_FR'){
						$company 	= 'Rainbow Retail';
					} else if( $subSource == 'BBD_EU_DE' ){
						$company 	= 'BBD';
					}  else if( $subSource == 'EBAY0' ){
						$company 	= 'EBAY0';
					} else if( $subSource == 'EBAY2' ){
						$company 	= 'EBAY2';
					}
					$this->loadModel( 'ReturnAddre' );
					$ret_add = $this->ReturnAddre->find('first', array( 'conditions' => array( 'company' =>  $company) ) );
					$return_address = $ret_add['ReturnAddre']['return_address'];
				
			 	$html           =  $labelDetail['BulkLabel']['html'];
				//$paperHeight    =  $labelDetail['Template']['paper_height'];
				//$paperWidth  	=  $labelDetail['Template']['paper_width'];
				//$barcodeHeight  =  $labelDetail['Template']['barcode_height'];
				//$barcodeWidth   =  $labelDetail['Template']['barcode_width'];
				//$paperMode      =  $labelDetail['Template']['paper_mode'];
				
				$barcodeimg = '<img src='.$barcodePath.$BarcodeImage.'>';
			
				$setRepArray[] 	=    $barcodeimg;
				$setRepArray[] 	=    $barcodenum[0];
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totlaWeight;
				$setRepArray[]	=	 $tax;
				$setRepArray[]	=	 $currentdate;
				$logopath		=	 Router::url('/', true).'img/';
				$setRepArray[]	=	 '<img src="'.$logopath.'logo.jpg" >';
				$logo			=	 '<img src="'.$logopath.'logo.jpg" >';
				$signature		=	 '<img src="'.$logopath.'sig.jpg" >';
				
				$setRepArray[]	=	 $logo;
				$setRepArray[]	=	 $signature;
				$setRepArray[]	=	 $splitOrderId;
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				
				$tempId			=	$splitOrderDetail['MergeUpdate']['lable_id'];
				$countryCode	=	$splitOrderDetail['MergeUpdate']['country_code'];
				
				$setRepArray[]	=	 $countryCode;
				$setRepArray[]	=	 $weight;
				$conversionRate	=	Configure::read( 'conversionRate' );
				if($currency == 'GBP')
				{
					$totalvalue = $totalvalue;
				}
				elseif($country == 'Spain' && $currency == 'EUR')
				{
					$totalvalue = $totalvalue;
				}
				elseif($country == 'United States')
				{
					$totalvalue = $totalvalue;
				}
				else
				{
					$totalvalue = number_format($totalvalue/$conversionRate, 2, '.' ,'');
				}
				
				if($totalvalue >= 1000  && $labelDetail['Template']['label_name'] == 'Jersey Post' && $country == 'United States' ){
					$numbers = array("97.11","98.32","98.52","99.72","99.94","150.27","200.45","270.62","370.82","422.11","472.31","530.71");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				else if($totalvalue >= 22  && $labelDetail['Template']['label_name'] == 'Jersey Post' && $country != 'United States'){
					$numbers = array("18.11","18.32","18.52","18.72","18.94","19.27","19.45","19.62","19.82","20.11","20.31","20.71","21.22","21.41", "21.61","21.51");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				
				$setRepArray[]	=	 $totalvalue;
				if($splitOrderDetail['MergeUpdate']['track_id'] != '')
				{
					$trackingnumber = $splitOrderDetail['MergeUpdate']['track_id'];
				}
				else
				{
					$trackingnumber = 'A'.mt_rand(100000, 999999);
				}
				
				$setRepArray[]	=	 $trackingnumber;
				$setRepArray[]	=	 $subSource;
				$setRepArray[]	=	 $CARTAS;
				$setRepArray[]	=	 $splitOrderDetail['MergeUpdate']['reg_num_img'];
				$setRepArray[]	=	 '<img src='.$barcodeCorriosPath.$corriosImage.'>';
				$setRepArray[]	=	($phone != '') ? 'Telefono del cliente:'.$phone : '';
				$setRepArray[]	=	$return_address;
				$setRepArray[]	=	WWW_ROOT;
				$cssPath = WWW_ROOT .'css/';
				$imgPath = Router::url('/', true) .'img/';

				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				
			//pr($setRepArray);
				$html 	= $obj->setReplaceValueLabel( $setRepArray, $html ); 	
				return $html;
		}
		
		public function MobileAsseInstruction( $splitOrderId  = null, $subsource = null)
		{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'Product' );
			$get_country = array('Marec_FR'=>'FR','Marec_DE'=>'DE','Marec_IT'=>'IT','Marec_ES'=>'ES','Marec_uk'=>'UK');
			$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.product_order_id_identify' => $splitOrderId)));
			$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
			foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					foreach($newSkus as $newSku)
						{
							$getsku = 'S-'.$newSku[1];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$contentSubCat[]	=	$getOrderDetail['Product']['sub_category'];
						}
							unset($newSkus);
				}
			
			$get_source = $get_country[$subsource];
			$Ins_html	=	$this->getMobileAssHtml( $get_source,$contentSubCat );
			
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();
			$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');
			$cssPath = WWW_ROOT .'css/';
			$dompdf->load_html(utf8_decode($Ins_html), Configure::read('App.encoding'));
			$dompdf->render();
			$imgPath = WWW_ROOT .'img/printPDF/Instruction/'; 
			$path = Router::url('/', true).'img/printPDF/Instruction/';
			$name	=	'instruction_Slip_'.$splitOrderId.'.pdf';
			file_put_contents($imgPath.$name, $dompdf->output());
			$serverPath  	= 	$path.$name ;
			$printerId		=	$this->getEpson();
			
			
			$sendData = array(
				'printerId' => $printerId,
				'title' => 'Now Print',
				'contentType' => 'pdf_uri',
				'content' => $serverPath,
				'source' => 'Direct'					
			);
			App::import( 'Controller' , 'Coreprinters' );
			$Coreprinter = new CoreprintersController();
			$d = $Coreprinter->toPrint( $sendData );
		}
		
		public function getMobileAssHtml($get_source = null, $contentSubCat = null)
		{
			$get_source		=	strtoupper($get_source);
			if($get_source == 'UK')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>BATTERY - </strong>This must be used in combination with a Genuine Cable, and Genuine Charger</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>CABLE - </strong>This must be used in combination with  a Genuine Charger, and Genuine Cable</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>CHARGER - </strong>This must be used in combination with a Genuine Battery, and Genuine Cable</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>EARPHONE/OTHER ACCESSORY - </strong>This is optimised for use on the original manufacturer equipment</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Bulk Packaged item</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >WHAT IS BULK PACKAGING ?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Your item is a Bulk Packaged item and is 100% authentic and brand new without a wasteful retail packaging.</li>
				<li>Bulk-packaged items are simply provided without retail packaging to offer additional costs savings to you.</li>
				<li>All items dispatched are 100% authentic, brand new items sourced from major mobile accessory distributors across the EU.</li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>IMPORTANT INFORMATION REGARDING YOUR ITEM</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">When using this item; it must be used in combination with other Genuine items.<br></td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">If you have any query, question, or concern please contact us immediately on below given details </td></tr>
				<tr><td style="text-align:center;" >Customer support:- +44-3301240338</td></tr>
				<tr><td style="text-align:center;" >Email:- support@ebuyer-express.com</td></tr>
				</table>';
			}
			if($get_source == 'FR')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Batterie - </strong>ceci doit être employé en combinaison avec un câble véritable, et le chargeur véritable</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Câble - </strong>ceci doit être employé en combinaison avec un vrai chargeur, et le câble véritable</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Chargeur - </strong>ceci doit être employé en combinaison avec une batterie véritable, et le câble véritable</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Écouteurs/autre accessoire - </strong>ceci est optimisé pour l\'utilisation sur l\'équipement original de fabricant</td></tr>';
					}
				}
				
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Article emballé en vrac</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >Qu\'est-ce que l\'emballage en vrac?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Votre article est un article emballé en vrac et est 100% authentique et flambant neuf sans un emballage de détail de gaspillage.</li>
				<li>Les articles emballés en vrac sont simplement fournis sans emballage de détail pour vous offrir des économies supplémentaires.</li>
				<li>Tous les articles expédiés sont des articles authentiques et neufs de 100% provenant de grands distributeurs d\'accessoires mobiles à travers l\'UE.</li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>Informations importantes concernant votre article</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Lors de l\'utilisation de cet élément; il doit être utilisé en combinaison avec d\'autres objets authentiques.</td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Si vous avez n\'importe quelle requête, question, ou préoccupation s\'il vous plaît contactez-nous immédiatement sur les détails ci-dessous donnés-</td></tr>
				<tr><td style="text-align:center;" >Support à la clientèle - +44-3301240338</td></tr>
				<tr><td style="text-align:center;" >email - support@ebuyer-express.com</td></tr>
				</table>';
			}
			if($get_source == 'DE')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Batterie - </strong>dies muss in Kombination mit einem echten Kabel und echtem Ladegerät verwendet werden</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Kabel - </strong>dies muss in Kombination mit einem echten Ladegerät und echtem Kabel verwendet werden</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Ladegerät - </strong>dies muss in Kombination mit einer echten Batterie und echtem Kabel verwendet werden</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Ohrhörer/anderes Zubehör - </strong>das ist für den Einsatz auf der Original-Hersteller Ausstattung optimiert</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Schüttgut verpackt</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >Was ist schüttgutverpackung ?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Ihr Artikel ist ein schüttgutverpacktes Produkt und ist 100% authentisch und nagelneu ohne verschwenderische Einzelhandelsverpackungen.</li>
				<li>Schüttgutverpackungen werden einfach ohne Einzelhandelsverpackung zur Verfügung gestellt, um Ihnen zusätzliche Kosteneinsparungen zu bieten.</li>
				<li>Alle versandten Artikel sind 100% authentische, brandneue Artikel, die von großen mobilen Zubehör Händlern in der EU stammen.</li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>Wichtige Informationen zu Ihrem Artikel</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Bei der Verwendung dieses Artikels; Es muss in Kombination mit anderen echten Gegenständen verwendet werden.</td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Wenn Sie Fragen, Fragen oder Bedenken haben, kontaktieren Sie uns bitte sofort unter den angegebenen Details – </td></tr>
				<tr><td style="text-align:center;" >Kundendienst - 44-3301240338</td></tr>
				<tr><td style="text-align:center;" >e-Mail - support@ebuyer-express.com</td></tr>
				</table>';
			}
			if($get_source == 'IT')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Batteria - </strong>questo deve essere usato in combinazione con un cavo genuino, e caricabatterie genuino</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Cavo- </strong>questo deve essere usato in combinazione con un caricabatterie genuino, e cavo genuino</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Caricabatterie - </strong>questo deve essere utilizzato in combinazione con una batteria genuina, e cavo genuino</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Auricolare/altro accessorio - </strong>questo è ottimizzato per l\'uso sull\'attrezzatura originale del produttore</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Articolo imballato sfuso</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >Che cosa è imballaggio alla rinfusa ?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Il vostro articolo è un articolo impaccato alla rinfusa ed è 100% autentico e brandnew senza uno spreco che impacca al minuto.</li>
				<li>Gli articoli impacchettati in massa sono forniti semplicemente senza imballaggi al dettaglio per offrire risparmi aggiuntivi ai costi.</li>
				<li>Tutti gli articoli spediti sono 100% autentici, nuovissimi prodotti provenienti dai principali distributori di accessori mobili in tutta l\'UE. </li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>Informazioni importanti sul tuo articolo</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Quando si utilizza questo elemento; deve essere usato in combinazione con altri oggetti genuini.</td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Se avete qualunque richiesta, domanda, o preoccupazione prego se li mette in contatto con immediatamente sui particolari dati sotto-</td></tr>
				<tr><td style="text-align:center;" >Assistenza clienti - +44-3301240338</td></tr>
				<tr><td style="text-align:center;" >email - support@ebuyer-express.com</td></tr>
				</table>';
			}
			if($get_source == 'ES')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Batería - </strong>esto se debe utilizar conjuntamente con un cable genuino, y el cargador genuino</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Cable - </strong>esto se debe utilizar conjuntamente con un cargador genuino, y el cable genuino</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Cargador - </strong>esto se debe utilizar conjuntamente con una batería genuina, y el cable genuino</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Auricular/otro accesorio - </strong>esto se optimiza para el uso en el equipo original del fabricante</td></tr>';
					}
				}		
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Artículo empaquetado bulto</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >¿ Qué es el empaquetado a granel ?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Su artículo es un artículo empaquetado bulto y es el 100% auténtico y a estrenar sin un empaquetado al por menor derrochador.</li>
				<li>Los artículos empaquetados a granel se proporcionan simplemente sin el empaquetado al por menor para ofrecer ahorros adicionales de los costes para usted.</li>
				<li>Todos los artículos enviados son 100% auténticos, nuevos artículos de origen de los principales distribuidores de accesorios móviles en toda la UE.</li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>Información importante sobre su artículo</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Al usar este artículo; debe ser utilizado en combinación con otros artículos genuinos.</td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Si usted tiene cualquier consulta, pregunta, o preocupación por favor póngase en contacto con nosotros inmediatamente a continuación detalles dados - </td></tr>
				<tr><td style="text-align:center;" >Soporte al cliente - +44-3301240338</td></tr>
				<tr><td style="text-align:center;" >email - support@ebuyer-express.com</td></tr>
				</table>';
			}
			return $cont;
		}
		
		public function getEpson()
		{
			$this->layout = '';
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name']; //getenv('COMPUTERNAME');
			$paramsEpson = array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name' => 'EPSONTM-T88V'
				)
			);
			
			//EPSONTM-T88V			
			$pcDetailEpson = json_decode(json_encode($this->PcStore->find('all', $paramsEpson)),0);			
			$epsonT88 = $pcDetailEpson[0]->PcStore->printer_id;
			
		return $epsonT88;	
		}	
        	
 
}



?>

