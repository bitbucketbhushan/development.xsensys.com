<?php
ini_set( 'max_execution_time' , 3500 );
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class ReservesController extends AppController
{
    
    var $name = "Reserves";
    
    var $components = array('Session','Upload','Common','Auth');
    
    var $helpers = array('Html','Form','Common','Session');
    
    public function reserveInevntoryByOrderId( $orderId = null )
	{
		
		$this->layout = "";
		$this->autoRander = false;
		$this->loadModel('OpenOrder');
		$this->loadModel( 'MergeUpdate' );
		
		//$orderId	=	$this->request->data['orderid'];
		
		$orderId = $orderId;
		$param = array(
			'conditions' => array(
				//'MergeUpdate.status' => 0,
				'MergeUpdate.product_order_id_identify' => $orderId
			),
			'fields' => array(
			
				'MergeUpdate.out_sell_marker',
				'MergeUpdate.sku',
				'MergeUpdate.sku_decode'
			
			)
		);
		
		//data coming
		$getLists = json_decode(json_encode($this->MergeUpdate->find('first', $param)),0);
		pr($getLists);
	
		
		//oversell history
		$skuDecode = unserialize( $getLists->MergeUpdate->sku_decode );
		pr($skuDecode);
		
		//get all skus
		$skuArray = array();
		foreach( $skuDecode as $skuEncode )
		{
							
			$jsonDecoeSku = json_decode($skuEncode);
			$ssku = explode( "XS-", strtoupper( $jsonDecoeSku->sku_decode ) );
			
			$sskInner = $ssku[1];
			$skuArray[] = 'S-'.$sskInner; 
			$ssku = '';
			unset($ssku);
			$sskInner = '';
			
		}
		
		$skuArrayCount = array_count_values( $skuArray );
		pr($skuArrayCount);
		
		$outerSku = array();
		$combinedSku = array();
		$outerVariable = '';
		$k = 0;foreach( $skuArrayCount as $keyData => $value )
		{
			
			foreach( $skuDecode as $value )
			{
				
				$value = json_decode( $value );					
				$oversellValue = explode( 'XS-' , strtoupper($value->sku_decode) );					
				$sku = 'S-'.$oversellValue[1];
				
				if( $keyData === $sku )
				{					
					$outerSku[] = abs($oversellValue[0]);						
					$outerVariable = 'S-'.$oversellValue[1];
				}
				
			}
			
			$combinedSku[$k][0] = '-'.array_sum( $outerSku );
			$combinedSku[$k][1] = $outerVariable;
			
			unset( $outerSku );
			$outerSku = '';
			$outerVariable = '';
			
		$k++;	
		}			
		
		//pr( $combinedSku );
		
		//now up and left inventory accordign to out_sell_marker and other source
		$allSku = explode( "," , strtoupper( $getLists->MergeUpdate->sku ) );
		
		//combine existing skus
		$collectSkuQty = array();
		$k = 0;foreach( $allSku as $skuNew )
		{
			
			$getSku = explode( "XS-" , strtoupper( $skuNew ) );
			$setSku = 'S-'.$getSku[1];
			$qty = $getSku[0];
			
			$collectSkuQty[$k][0] = $setSku;
			$collectSkuQty[$k][1] = $qty;
		$k++;	
		}
		
		pr($collectSkuQty);
		exit;
		//deduct start
		$skt = 0;foreach( $collectSkuQty as $collect )
		{
			
			//pr($collect);			
			foreach( $combinedSku as $combSku )
			{
				
				$combSku[0] = $collect[1] + $combSku[0];
				//pr($combSku);
				//if match the deduct and set negative value it must
				if( $combSku[1] === $collect[0] )
				{		
					//$combSku[0] = $collect[1] + $combSku[0];
				}
				
			}	
			
		$skt++;	
		}
		
		exit;
		
	}
	
	
	public function getAllQuantity( $orderId = null, $splitOrderId = null)
	   {
		   $this->loadMOdel( 'ScanOrder' );
		   $itemdetails	=	$this->ScanOrder->find('all', array('conditions' => array('ScanOrder.order_id' => $orderId, 'ScanOrder.split_order_id' => $splitOrderId)));
		   $quantity = 0;
		   foreach( $itemdetails as $itemdetail )
		   {
			   $quantity	=	$quantity + $itemdetail['ScanOrder']['quantity'];
		   }
		   return $quantity;
	   }
 
}
?>
