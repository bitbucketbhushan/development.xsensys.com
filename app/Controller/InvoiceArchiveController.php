<?php
error_reporting(0);
class InvoiceArchiveController extends AppController
{
    var $name = "InvoiceArchive";
    
    var $helpers = array('Html','Form','Session');
	
	var $COST_STORE_NAME	  = 'EURACO GROUP LIMITED';//CostDroper
	var $COST_STORE_ADDRESS	  = '49 Oxford Road St Helier <br>Jersey JE2 4LJ';
	var $COST_REG_NUMBER 	  = '109464';
	
	var $RAIN_STORE_NAME	  = 'FRESHER BUSINESS LIMITED';//Rainbow
	var $RAIN_STORE_ADDRESS   = 'Beachside Business Centre<br>Rue Du Hocq St Clement<br>Jersey JE2 6LF';
	var $RAIN_REG_NUMBER 	  = '108507';
	
	var $MARE_STORE_NAME	  = 'ESL LIMITED'; //Marec
	var $MARE_STORE_ADDRESS   = 'Unit 4 Airport Cargo Centre<br>L\'avenue De La Commune St Peter<br>Jersey JE3 7BY';
	var $MARE_REG_NUMBER 	  = '118438';
    
	public function beforeFilter()
    {
	 	parent::beforeFilter();
		$this->layout = false; 
		$this->Auth->Allow(array('Generate','GenerateB2b','cronEuFBAInvoice','getEuFBAInvoice')); 		
				
	} 
	
	public function index()
    {
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
		$html .= '<body>'.$htmlTemplate.'</body>';
				
		$name	= 'Invoice.pdf';							
		$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
     
		echo $r = 'Юлия Свеженцева';
		echo "<br>=====";
		echo utf8_encode($r);
		echo "<br>=====";
		echo mb_convert_encoding($r, 'HTML-ENTITIES','UTF-8');
		exit;
		
	}
	
    public function Generate($OrderId = null)
    {
 		$this->layout = '';
		$this->autoRender = false;	
		
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();	
		$result 		=	'';
		$htmlTemplate	=	'';				
				
		//$OrderId	= 	$this->request->data['order_id'];
	 	
		if(isset($_GET["myarray"]) && $_GET["myarray"] != ''){
			$get = json_decode($_GET["myarray"]); 
 			$data['NumOrderId']    = $OrderId;
			$data['GeneralInfo']   = ($get->GeneralInfo);
			$data['ShippingInfo']  = ($get->ShippingInfo);
			$data['CustomerInfo']  = ($get->CustomerInfo);
			$data['TotalsInfo']    = ($get->TotalsInfo);
			$data['Items'] 		   = ($get->Items);
			$data['CustomerInfo']->BillingAddress  = $data['CustomerInfo']->Address;
  			$order				= json_decode(json_encode( $data ),0);	
 			//$order			=	$this->getOrderByNumId( $OrderId );	
		}else{
			$order			=	$this->getOrderByNumId_openorder( $OrderId );	
		}
		//	pr($order);exit;
		if(is_object($order)){			
			$htmlTemplate	=	$this->getTemplate($order);
			$SubSource		=	$order->GeneralInfo->SubSource;
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4)));
			
			/**************** for tempplate *******************/
			$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				 <meta content="" name="description"/>
				 <meta content="" name="author"/>
				 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
			$html .= '<body>'.$htmlTemplate.'</body>';
					
			$name	= 'Invoice.pdf';							
			$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
			//$html_t = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");
			//$dompdf->load_html($html_t);
			
			//$dompdf->load_html(iconv("UTF-8", "CP1252", $html));
			 
			$dompdf->render();			
			//$dompdf->stream($name);
							
			$file_to_save = WWW_ROOT .'img/invoice_template/'.$name;
			//save the pdf file on the server
			file_put_contents($file_to_save, $dompdf->output()); 
			//print the pdf file to the screen for saving
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="'.$name.'"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . filesize($file_to_save));
			header('Accept-Ranges: bytes');
			readfile($file_to_save);
		
		}else{
			echo 'Invalid Order.';
		}
		//echo json_encode($msg);	
		exit;
		
    }
	
	public function GenerateB2b($amazon_order_id = null)
    { 
				 
 		$order = $this->getOrderByNumId_openorder( $amazon_order_id );	
		
		if(is_object($order)){
					 
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();	
			
			$result 		=	'';
			$htmlTemplate	=	'';	
	
			if(strtotime($order->GeneralInfo->ReceivedDate) > strtotime('2019-09-30 23:59:59')){
				 $htmlTemplate	=	$this->getB2Bhtml($order);	
			}else{
				 $htmlTemplate	=	$this->getB2Bhtml30($order);	
			}	
			$SubSource		=	$order->GeneralInfo->SubSource;
			$ReferenceNum   =   $order->GeneralInfo->ReferenceNum;
		
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4))); 
			/**************** for tempplate *******************/
			$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				 <meta content="" name="description"/>
				 <meta content="" name="author"/>
				 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
			echo $html .= '<body>'.$htmlTemplate.'</body>';
			exit;
			//$html =	$htmlTemplate;
			$name	= $amazon_order_id.'.pdf';							
			$dompdf->load_html(($html), Configure::read('App.encoding'));
			$dompdf->render();			
							
			$file_to_save = WWW_ROOT .'b2b_invoices/'.$name;
			 
			//save the pdf file on the server
			file_put_contents($file_to_save, $dompdf->output()); 
			  
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="'.$name.'"'); 
			header('Content-Length: ' . filesize($file_to_save));
			header("Cache-control: private"); 
			readfile($file_to_save);
			exit;
		
		}else{
			echo 'Invalid Order.';
		}		 
			
	}
	public function GenerateB2c($amazon_order_id = null)
    { 
				 
 		$order = $this->getOrderByNumId_openorder( $amazon_order_id );	
		
		if(is_object($order)){
					 
			if($order->GeneralInfo->Source != 'AMAZON'){
				echo 'You can not generate VAT Invoice for <strong>'.$order->GeneralInfo->Source.'</strong>.';
				exit;
			}
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();	
			
			$result 		=	'';
			$htmlTemplate	=	'';	
	
			$htmlTemplate	=	$this->getB2Chtml($order);		
			$SubSource		=	$order->GeneralInfo->SubSource;
			$ReferenceNum   =   $order->GeneralInfo->ReferenceNum;
		
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4))); 
			/**************** for tempplate *******************/
			$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				 <meta content="" name="description"/>
				 <meta content="" name="author"/>
				 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
			$html .= '<body>'.$htmlTemplate.'</body>';
			//$html =	$htmlTemplate;
			$name	= $amazon_order_id.'.pdf';							
			$dompdf->load_html(($html), Configure::read('App.encoding'));
			$dompdf->render();			
							
			$file_to_save = WWW_ROOT .'b2c_invoices/'.$name;
			 
			//save the pdf file on the server
			file_put_contents($file_to_save, $dompdf->output()); 
			  
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="'.$name.'"'); 
			header('Content-Length: ' . filesize($file_to_save));
			header("Cache-control: private"); 
			readfile($file_to_save);
			exit;
		
		}else{
			echo 'Invalid Order.';
		}		 
			
	}
	
	public function getB2BInvoice($amazon_order_id = null){ 
				 
 		$order = $this->getOrderByNumId_openorder( $amazon_order_id );	
		   		 
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();	
 		
		$result 		=	'';
		$htmlTemplate	=	'';	

		if(strtotime($order->GeneralInfo->ReceivedDate) > strtotime('2019-09-30 23:59:59')){
			 $htmlTemplate	=	$this->getB2Bhtml($order);	
		}else{
			 $htmlTemplate	=	$this->getB2Bhtml30($order);	
		}
				
		$SubSource		=	$order->GeneralInfo->SubSource;
		$ReferenceNum   =   $order->GeneralInfo->ReferenceNum;
	
		$result			=	ucfirst(strtolower(substr($SubSource, 0, 4)));
		ob_clean();
		/**************** for tempplate *******************/
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
	 	$html .= '<body>'.$htmlTemplate.'</body>';
	 	$html =	$htmlTemplate;
		$name	= $amazon_order_id.'.pdf';							
		$dompdf->load_html(($html), Configure::read('App.encoding'));
		$dompdf->render();			
						
		$file_to_save = WWW_ROOT .'b2b_invoices/'.$name;
		 
		//save the pdf file on the server
		file_put_contents($file_to_save, $dompdf->output()); 
		//copy($file_to_save,WWW_ROOT .'b2b_invoices/26092019/'.$amazon_order_id.'.pdf');
		
  		//exit;		 
			
	}
	
	public function getEuropeOrderByNumId($amazon_order_id = null)
	{
		$this->loadModel('FbaEuOrder');	 
		$this->loadModel('FbaEuOrderItem');	
		$data 	= [];
		$order 	= $this->FbaEuOrder->find('first', array('conditions'=>array('amazon_order_id' => $amazon_order_id)));
		if(count($order ) > 0)
		{
 			$data['NumOrderId']    = substr($amazon_order_id,-7);
			$general_info['ReferenceNum'] = $order['FbaEuOrder']['amazon_order_id'];
			$general_info['Source'] = $order['FbaEuOrder']['sales_channel'];
			$general_info['SubSource'] = 'not_found';
			$general_info['ReceivedDate'] = $order['FbaEuOrder']['purchase_date']; 
			$general_info['DespatchByDate'] = $order['FbaEuOrder']['earliest_ship_date'];
			
			$customer_info['ChannelBuyerName'] = $order['FbaEuOrder']['buyer_name'];
			$customer_info['Address']['EmailAddress'] = $order['FbaEuOrder']['buyer_email'];
			$customer_info['Address']['Address1'] = $order['FbaEuOrder']['address_line_1'];
			$customer_info['Address']['Address2'] = $order['FbaEuOrder']['address_line_2'];
			$customer_info['Address']['Address3'] ='';
			$customer_info['Address']['Town'] = $order['FbaEuOrder']['city'];			
			$customer_info['Address']['Region'] = $order['FbaEuOrder']['region'];
			$customer_info['Address']['PostCode'] = $order['FbaEuOrder']['post_code'];
			$customer_info['Address']['Country'] = $order['FbaEuOrder']['country_code'];
			$customer_info['Address']['FullName'] = $order['FbaEuOrder']['name'];
			$customer_info['Address']['PhoneNumber'] = '';
			
			$customer_info['BillingAddress']['EmailAddress'] = $order['FbaEuOrder']['buyer_email'];
			$customer_info['BillingAddress']['Address1'] = $order['FbaEuOrder']['bill_address_1'];
			$customer_info['BillingAddress']['Address2'] = $order['FbaEuOrder']['bill_address_2'];
			$customer_info['BillingAddress']['Address3'] = $order['FbaEuOrder']['bill_address_3'];
			$customer_info['BillingAddress']['Town'] = $order['FbaEuOrder']['bill_city'];			
			$customer_info['BillingAddress']['Region'] = $order['FbaEuOrder']['bill_state'];
			$customer_info['BillingAddress']['PostCode'] = $order['FbaEuOrder']['bill_postal_code'];
			$customer_info['BillingAddress']['Country'] = $order['FbaEuOrder']['bill_country'];
			$customer_info['BillingAddress']['FullName'] = $order['FbaEuOrder']['name'];
			$customer_info['BillingAddress']['PhoneNumber'] = $order['FbaEuOrder']['buyer_phone_number'];
		 
			//pr($order['FbaEuOrderItem']);
			
			$sub_total = 0; $shipping_price = 0; $item_tax = 0; $shipping_promotion = 0;
			$gift_wrap = 0; $item_promotion = 0; $currency = '';
			foreach($order['FbaEuOrderItem'] as $i => $v){ 
				
				$items[$i]['ItemId'] = $v['order_item_id'];
				$items[$i]['ItemNumber'] =  $v['order_item_id'];
				$items[$i]['SKU'] = $v['master_sku'];
				$items[$i]['ItemSource'] = 'AMAZON';
				$items[$i]['Title'] = $v['title'];
				$items[$i]['Quantity'] = $v['quantity_shipped'];
				$items[$i]['CategoryName'] = 'Default';
				$items[$i]['PricePerUnit'] = $v['item_price'];
				$items[$i]['item_tax'] = $v['item_tax'];
				$items[$i]['shipping_price'] = $v['shipping_price'];
				$items[$i]['shipping_discount'] = $v['shipping_discount'];
				$items[$i]['item_promotion_discount'] = $v['item_promotion_discount'];
				$items[$i]['gift_wrap_price'] = $v['gift_wrap_price'];
				$items[$i]['gift_wrap_tax'] = $v['gift_wrap_tax'];		
 				$items[$i]['UnitCost'] = 0;
				$items[$i]['Discount'] = '0';
				$items[$i]['Tax'] = '0';
				$items[$i]['Cost'] = $v['item_price'];
				$items[$i]['CostIncTax'] = $v['item_price']  ;	
				$items[$i]['ChannelSKU'] = $v['seller_sku'];
				$items[$i]['ChannelTitle'] = $v['title'];
				if(strpos($v['seller_sku'],"FREURAFBA") !== false){
					$general_info['SubSource'] = 'CostBreaker_FRFBA';
				}else if(strpos($v['seller_sku'],"ESEURAFBA") !== false){
					$general_info['SubSource'] = 'CostBreaker_ESFBA';
				}else if(strpos($v['seller_sku'],"DEEURAFBA") !== false){
					$general_info['SubSource'] = 'CostBreaker_DEFBA';
				}else if(strpos($v['seller_sku'],"ITEURAFBA") !== false){
					$general_info['SubSource'] = 'CostBreaker_ITFBA';
				}else if(strpos($v['seller_sku'],"UKEURAFBA") !== false){
					$general_info['SubSource'] = 'CostBreaker_UKFBA';
				}
				
				else if(strpos($v['seller_sku'],"MRDEFBA") !== false){
					$general_info['SubSource'] = 'Marec_DEFBA';
				}else if(strpos($v['seller_sku'],"MRUKFBA") !== false){
					$general_info['SubSource'] = 'Marec_UKFBA';
				}else if(strpos($v['seller_sku'],"MRITFBA") !== false){
					$general_info['SubSource'] = 'Marec_ITFBA';
				}else if(strpos($v['seller_sku'],"MRFRFBA") !== false){
					$general_info['SubSource'] = 'Marec_FRFBA';
				}
				
				else if(strpos($v['seller_sku'],"RAINUKFBA") !== false){
					$general_info['SubSource'] = 'Rainbow_UK_FBA';
				} 
 				
				$currency = $v['currency_code']; 
				$sub_total += $v['item_price'];
				$shipping_price += $v['shipping_price'];
				$item_tax += $v['item_tax'];
				$gift_wrap += ($v['gift_wrap_price'] + $v['gift_wrap_tax']);
				$item_promotion += $v['item_promotion_discount'];
				$shipping_promotion += $v['shipping_discount'];
			}
	
  		
		$totals_info['Subtotal'] = $sub_total;
		$totals_info['PostageCost'] = $shipping_price;
		$totals_info['Tax'] = $item_tax;		 
		$totals_info['TotalCharge'] = $sub_total + $shipping_price + $item_tax + $gift_wrap + $shipping_promotion + $item_promotion;
		$totals_info['TotalDiscount'] = '0';
		$totals_info['Currency'] = $currency;
		$totals_info['CountryTaxRate'] = '0';
		$totals_info['gift_wrap'] = $gift_wrap;
		$totals_info['shipping_promotion'] = $shipping_promotion;
		$totals_info['item_promotion'] = $item_promotion;
		
		
		$data['GeneralInfo']   = json_decode(json_encode($general_info));
		$data['CustomerInfo']   = json_decode(json_encode($customer_info));
		$data['TotalsInfo']   = json_decode(json_encode($totals_info));
		$data['Items']   = json_decode(json_encode($items));
		
 		} 
		/*pr($data);
		exit;*/
		return json_decode(json_encode( $data ),0);	
		
	}
	
	public function cronEuFBAInvoice($option = null){
		$this->loadModel('FbaEuOrder');	
 		$day = 3;
		if( $option == 'manual'){
			$day = 1;
		} 
 		/*$orders = $this->FbaEuOrder->find('all', array('fields'=>['amazon_order_id'],'order'=>'added_date DESC','conditions'=>array('added_date >' => date('Y-m-d',strtotime("-{$day} Days")) )));*/
		
		$orders = $this->FbaEuOrder->query("SELECT `amazon_order_id` FROM `fba_eu_orders` as  FbaEuOrder WHERE `added_date` > '". date('Y-m-d',strtotime("-{$day} Days"))."' ORDER BY `added_date` DESC ");
		 
		foreach($orders as $v){
			 if(!file_exists(WWW_ROOT.'fba_eu_invoice/'.$v['FbaEuOrder']['amazon_order_id'].'.pdf')){
				 $this->getEuFBAInvoice($v['FbaEuOrder']['amazon_order_id'],'cron');				 
			 } 
		}
		
		if( $option == 'manual'){
			$this->redirect($this->referer()); 			 
		}
		exit;
	}
	
	public function getEuFBAInvoice($order_id = null, $option = null){
	
   		$order = $this->getEuropeOrderByNumId( $order_id );	
  		 
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();	
 		
		$result 		=	'';
		$htmlTemplate	=	''; 

		$htmlTemplate	=	$this->getFbaEuhtml($order);		
		$SubSource		=	$order->GeneralInfo->SubSource;
	
		$result			=	ucfirst(strtolower(substr($SubSource, 0, 4)));
		ob_clean();
		/**************** for tempplate *******************/
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
	 	$html .= '<body>'.$htmlTemplate.'</body>';
	 	$html =	$htmlTemplate;
		$name	= $order_id.'.pdf';							
		$dompdf->load_html(($html), Configure::read('App.encoding'));
		$dompdf->render();			
						
		$file_to_save = WWW_ROOT .'fba_eu_invoice/'.$name;
		 
		//save the pdf file on the server
		file_put_contents($file_to_save, $dompdf->output()); 
		
		if( $option != 'cron'){
			header("Location:". Router::url('/', true).'fba_eu_invoice/'.$name);
 			exit;	
		}	 
			
	}
	
	public function getOrderByNumId_openorder($pkOrderId = null)
	{
		$this->loadModel('OpenOrdersArchive');	
		$data 	= [];
		$order 	= $this->OpenOrdersArchive->find('first', array('conditions'=>array('OpenOrdersArchive.num_order_id' => $pkOrderId)));
		
		if(count($order ) == 0)
		{
			$order 	= $this->OpenOrdersArchive->find('first', array('conditions'=>array('OpenOrdersArchive.amazon_order_id' => $pkOrderId)));
		} 
		
		if(count($order ) > 0)
		{
			$data['NumOrderId']    = $order['OpenOrdersArchive']['num_order_id'];
			$data['GeneralInfo']   = unserialize($order['OpenOrdersArchive']['general_info']);
			$data['ShippingInfo']  = unserialize($order['OpenOrdersArchive']['shipping_info']);
			$data['CustomerInfo']  = unserialize($order['OpenOrdersArchive']['customer_info']);
			$data['TotalsInfo']    = unserialize($order['OpenOrdersArchive']['totals_info']);
			$data['Items'] 		   = unserialize($order['OpenOrdersArchive']['items']);
			$data['vat_number']    = $order['OpenOrdersArchive']['vat_number'];
			 
			if(isset($data['CustomerInfo']->BillingAddress) && count($data['CustomerInfo']->BillingAddress) > 0){
				
			}else{
				$data['CustomerInfo']->BillingAddress  = $data['CustomerInfo']->Address;
			}		 
			
		} 
		return json_decode(json_encode( $data ),0);	
		
	}
		
	public function getOrderByNumId($pkOrderId = null)
	{
		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
	
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
		//$multi = AuthMethods::Multilogin($username, $password);		
		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

		$token = $auth->Token;	
		$server = $auth->Server;		
		$order	= OrdersMethods::GetOrderDetailsByNumOrderId($pkOrderId,$token, $server);
		//pr($order	);
		//exit;
		return $order;	
		
	}
	
	public function getOrderByNumIdDhl($pkOrderId = null)
	{
		 
		//App::import('Vendor', 'Linnworks/src/php/Orders');
		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
		
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
		//$multi = AuthMethods::Multilogin($username, $password);		
		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

		$token = $auth->Token;	
		$server = $auth->Server;		
		$order	= OrdersMethods::GetOrderDetailsByNumOrderId($pkOrderId,$token, $server);
		//print_r($order);
		return $order;	
		
	}
	public function getTemplate($order = null )
	{
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;
		
		
		$this->loadModel('InvoiceAddress');		
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
			$BILLING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName)).'<br>';
			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$order->CustomerInfo->BillingAddress->Country ? $order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
			
			$SHIPPING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName)).'<br>';
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $order->CustomerInfo->Address->Country ? $order->CustomerInfo->Address->Country : '';
			}			
		}		
		
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal.'&nbsp;'.$Currency;
		$POSTAGE		  = $order->TotalsInfo->PostageCost.'&nbsp;'.$Currency;
		$TAX			  = $order->TotalsInfo->Tax.'&nbsp;'.$Currency;
		$TOTAL			  = $order->TotalsInfo->TotalCharge.'&nbsp;'.$Currency;
		$INVOICE_DATE	  = date('Y-M-d', strtotime($order->GeneralInfo->ReceivedDate));
		$templateHtml 	  = NULL;
					
		if($result == 'cost'){
			$templateHtml ='<!-- CostBreaker Invoice -->';			
			$templateHtml .='<div id="label">';
			$templateHtml .='<div class="container">';	
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top" style="font-size:15px;">'.$this->COST_STORE_NAME.'<br>'.$this->COST_STORE_ADDRESS.'<br>Registration Number : '.$this->COST_REG_NUMBER.' </td>';
			$templateHtml .='<td  width="25%" valign="top" >';
			$templateHtml .='<span style="font-weight:bold; font-size:30px">INVOICE</span><br>';
			$templateHtml .='<b>Invoice Number:</b>'.$OrderId.'<br><b>Date:</b> '.$INVOICE_DATE;
			$templateHtml .='</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>'; 
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';	
			$templateHtml .='<td width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';	
			$templateHtml .='</tr>';	
			$templateHtml .='</table>';
	
			$templateHtml  .='<div class="tablesection row" style="margin:5px 0 0 0; border-bottom:0px;height:700px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
				foreach($items as $item){
					
				  $templateHtml .='<tr>
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$item->Title.'</td>
					   <td style="" valign="top" >'.$item->Quantity.'</td>
					   <td style="" valign="top" >'.$item->PricePerUnit.'</td>
					   <td style="" valign="top" >'.$item->TaxRate.'</td>
					   <td style="" valign="top" >'.$item->Tax.'</td>
					   <td style="" valign="top" >'.$item->Cost.'</td>
					   <td style="" valign="top" >'.$item->CostIncTax.'</td>
					</tr>';            
       
				}
			$templateHtml .= '</table>';
	
			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" rowspan="4" width="60%"></th>';
			$templateHtml .= '<th style="" align="left" width="25%">SUB TOTAL:</th>';	
			$templateHtml .= '<td style="" align="right" width="15%">'.$SUB_TOTAL.'</td>';
			$templateHtml .= '</tr>';
	
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" >POSTAGE(Ex TAX):</th>';
			$templateHtml .='<td style="" align="right">'.$POSTAGE.'</td>';	
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" >TAX:</th>';	
			$templateHtml .='<td style="" align="right">'.$TAX.'</td>';	
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" >TOTAL:</th>';	
			$templateHtml .='<td style="" align="right">'.$TOTAL.'</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';			
			$templateHtml .='</div>';
			
			$templateHtml .='<div class="footer row" style="text-align:center; border-bottom:0px;">
			<b>Thank you for your purchase. Happy Shopping!</div>';
			$templateHtml .='</div>';
		
		}
		elseif($result == 'mare'){
			$templateHtml ='<!-- eBuyer Express(Marec) Invoice -->';			
			$templateHtml .='<div id="label" style="margin-top:20px;">';
			$templateHtml .='<div class="container">';			
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top"><img src='.WWW_ROOT.'/img/ebuyerexpress.jpg width="380px"></td>';
			$templateHtml .='<td  width="25%" valign="top">
			'.$this->MARE_STORE_NAME.'<br>
			'.$this->MARE_STORE_ADDRESS.'<br>
			Registration Number : '.$this->MARE_REG_NUMBER.'		
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="37%"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml .='<td valign="top" width="38%"><h4><u>Billing Address:</u></h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';

			$templateHtml .='<td  width="25%" valign="top">
			<table>
			<tr><td><b>Invoice ID:</b></td>	<td>'.$OrderId.'</td></tr>
			<tr><td><b>Invoice Date:</b></td><td>'.$INVOICE_DATE.'</td></tr>
			</table>
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<div style="font-weight:bold; font-size:30px; text-align:center">INVOICE</div>';
			$templateHtml .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:600px">';
			
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
			foreach($items as $count => $item){
		    
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				}else{
					$b = '';
				}					
				
			  $templateHtml .='<tr>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->SKU.'</td>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->Title.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Quantity.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->PricePerUnit.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->TaxRate.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Tax.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Cost.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->CostIncTax.'</td>
				</tr>';            
       
			}
			$templateHtml .= '</table>';
			
			$templateHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$templateHtml .='<tr>
			<th style="border-top:2px solid #000;" align="left" rowspan="4" width="60%"></th>
			<th style="border-top:2px solid #000;" align="left" width="25%">SUB TOTAL:</th>
			<td style="border-top:2px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>
			</tr>';
			
			$templateHtml .='<tr>
			<th style="" align="left">POSTAGE(Ex TAX):</th>
			<td style="" align="right">'.$POSTAGE.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TAX:</th>
			<td style="" align="right">'.$TAX.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TOTAL:</th>
			<td style="" align="right">'.$TOTAL.'</td>
			</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='</div>';
			
			$templateHtml .='<div class="footer row" style="font-size:15px; text-align:center; border-bottom:0px;" >
			<h2>THANK YOU FOR YOUR PURCHASE</h2> 
			<b>Please take a moment to leave us positive feedback.</b><br>
			If however, for any reason you are not entirely happy with your
			order, please contact us<br>to allow us to rectify any issues before
			leaving feedback.</div>';			
			$templateHtml .='</div>';
					
		}
		elseif($result == 'rain'){
			
			$templateHtml ='<!-- Rainbow Invoice -->';			
			$templateHtml .='<div id="label">';
			$templateHtml .='<div class="container">';
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="60%" style="text-align:center; font-weight:bold; font-size:28px">INVOICE</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="50%" valign="top">';
			$templateHtml .='<img src='.WWW_ROOT.'/img/rainbow_logo.png width="250px">';
			$templateHtml .='</td>';			
			$templateHtml .='<td  width="50%" valign="top" style="text-align:right">
				<b>'.$this->RAIN_STORE_NAME.'</b><br>
				'.$this->RAIN_STORE_ADDRESS.'
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<table cellpadding="3" style="background-color:#333; color:#fff; margin-bottom:20px;" class="topborder bottomborder"><tr>';
			$templateHtml .='<td><span class="bold">Date:</span> '.$INVOICE_DATE.'</td>';
			$templateHtml .='<td align="right"><span class="bold">Invoice ID:</span> '.$OrderId.'</td>';
			$templateHtml .='</tr></table>';
			
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  valign="top" width="50%"><h4 class="topborder bottomborder" style="background-color:#333; color:#fff; padding:3px">
			Billing Address:</h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml .='<td  width="50%" valign="top"><h4 class="topborder bottomborder" style="background-color:#333; color:#fff; padding:3px">
			Delivery Address:</h4>'.$SHIPPING_ADDRESS.'</td>';
			
			$templateHtml .='</tr>';		
			$templateHtml .='</table>';
	
			$templateHtml .='<div class="tablesection row" style="margin:5px 0 0 0; border-bottom:0px;height:680px">';
			$templateHtml .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse: collapse; margin:0px;">';
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" class="topborder bottomborder" width="13%">SKU</th>';
			$templateHtml .='<th style="" align="left" valign="top" width="19%" class="topborder bottomborder">Item</th>';
			$templateHtml .='<th style="" valign="top" class="topborder bottomborder" width="10%">Quantity</th>';
			$templateHtml .='<th style="" align="left" class="topborder bottomborder" width="8%">Unit</th>';
			$templateHtml .='<th style="" align="left" valign="top" width="10%" class="topborder bottomborder">Tax Rate</th>';
			$templateHtml .='<th style="" valign="top" class="topborder bottomborder" width="8%">Tax</th>';
			$templateHtml .='<th style="" align="left" class="topborder bottomborder" width="12%">Cost (ex, Tax)</th>';
			$templateHtml .='<th style="" align="left" valign="top" width="10%" class="topborder bottomborder">Line Cost</th>';
			$templateHtml .='</tr>';
				foreach($items as $item){
				  $templateHtml .='<tr>
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$item->Title.'</td>
					   <td style="" valign="top" >'.$item->Quantity.'</td>
					   <td style="" valign="top" >'.$item->PricePerUnit.'</td>
					   <td style="" valign="top" >'.$item->TaxRate.'</td>
					   <td style="" valign="top" >'.$item->Tax.'</td>
					   <td style="" valign="top" >'.$item->Cost.'</td>
					   <td style="" valign="top" >'.$item->CostIncTax.'</td>
					</tr>';            
				
				}
			$templateHtml .='</table>';
		
			$templateHtml .='<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" width="60%" class="topborder"></th>';
			$templateHtml .='<th style="" align="left" width="25%" class="topborder">SUB TOTAL:</th>';
			$templateHtml .='<td style="" align="right" width="15%" class="topborder">'.$SUB_TOTAL.'</td>';
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td></td>';
			$templateHtml .='<th style="" align="left">POSTAGE(Ex TAX):</th>';
			$templateHtml .='<td style="" align="right">'.$POSTAGE.'</td>';
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td></td>';
			$templateHtml .='<th style="" align="left" >TAX:</th>';
			$templateHtml .='<td style="" align="right">'.$TAX.'</td>';
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td></td>';
			$templateHtml .='<th style="" align="left" class="topborder bottomborder">TOTAL:</th>';
			$templateHtml .='<td style="" align="right" class="topborder bottomborder">'.$TOTAL.'</td>';
			$templateHtml .='</tr>';			
			$templateHtml .='</table>';			
			$templateHtml .='</div>';
	
			$templateHtml  .= '<div class="footer row" style="text-align:center; border-bottom:0px;" >
				<b>Thank you for your purchase. Happy Shopping!<br>
				Rainbow Retail</b>';	
			 $templateHtml  .= '</div>';
		}
		elseif($result == 'bbd_'){		
			$templateHtml ='<!-- BBD_EU Invoice -->';			
			$templateHtml .='<div id="label" style="margin-top:20px;">';
			$templateHtml .='<div class="container">';			
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top"><img src=http://xsensys.com/img/bbdEu.png></td>';
			$templateHtml .='<td  width="25%" valign="top">			
								Unit A1 21/F, Officeplus Among Kok<br>
								998 Canton Road<br>
								Hong Kong<br>
								KL<br>
								0000			
							</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="37%"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml .='<td valign="top" width="38%"><h4><u>Billing Address:</u></h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';

			$templateHtml .='<td  width="25%" valign="top">
			<table>
			<tr><td><b>Invoice ID:</b></td>	<td>'.$OrderId.'</td></tr>
			<tr><td><b>Invoice Date:</b></td><td>'.$INVOICE_DATE.'</td></tr>
			</table>
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<div style="font-weight:bold; font-size:30px; text-align:center">INVOICE</div>';
			$templateHtml .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:600px">';
			
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
			foreach($items as $count => $item){
		    
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				}else{
					$b = '';
				}					
				
			  $templateHtml .='<tr>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->SKU.'</td>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->Title.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Quantity.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->PricePerUnit.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->TaxRate.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Tax.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Cost.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->CostIncTax.'</td>
				</tr>';            
       
			}
			$templateHtml .= '</table>';
			
			$templateHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$templateHtml .='<tr>
			<th style="border-top:2px solid #000;" align="left" rowspan="4" width="60%"></th>
			<th style="border-top:2px solid #000;" align="left" width="25%">SUB TOTAL:</th>
			<td style="border-top:2px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>
			</tr>';
			
			$templateHtml .='<tr>
			<th style="" align="left">POSTAGE(Ex TAX):</th>
			<td style="" align="right">'.$POSTAGE.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TAX:</th>
			<td style="" align="right">'.$TAX.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TOTAL:</th>
			<td style="" align="right">'.$TOTAL.'</td>
			</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='</div>';
			
			$templateHtml .='<div class="footer row" style="font-size:15px; text-align:center; border-bottom:0px;" >
			<h2>THANK YOU FOR YOUR PURCHASE</h2> 
			<b>Please take a moment to leave us positive feedback.</b><br>
			If however, for any reason you are not entirely happy with your
			order, please contact us<br>to allow us to rectify any issues before
			leaving feedback.</div>';			
			$templateHtml .='</div>';
					
		}
		elseif($result == 'tech'){
			$templateHtml  ='<!-- TechDrive Invoice -->';
			$templateHtml  .='<div id="label">';
			$templateHtml  .='<div class="container">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px"></table>';
			$templateHtml  .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  width="60%" valign="top">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml  .='<td  width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			$templateHtml  .='</td>';
			$templateHtml  .='<td  width="40%" valign="top" align="center">
			<img src=http://xsensys.com/img/techDriveSupplies2.jpg width="280px"></td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			
			$templateHtml  .='<table>';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td width="80%"><span style="font-weight:bold; font-size:32px">INVOICE #'.$OrderId.'</span><br></td>';
			$templateHtml  .='<td width="20%" align="right"><b>Date:</b> '.$INVOICE_DATE.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';

			$templateHtml  .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:650px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:3px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
				foreach($items as $item){
					
				  $templateHtml .='<tr style="background-color:#f2f2f2">
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$item->Title.'</td>
					   <td style="" valign="top" >'.$item->Quantity.'</td>
					   <td style="" valign="top" >'.$item->PricePerUnit.'</td>
					   <td style="" valign="top" >'.$item->TaxRate.'</td>
					   <td style="" valign="top" >'.$item->Tax.'</td>
					   <td style="" valign="top" >'.$item->Cost.'</td>
					   <td style="" valign="top" >'.$item->CostIncTax.'</td>
					</tr>';            
       
				}
			$templateHtml .= '</table>';		 
		
			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-top:1px solid #000;border-bottom:3px solid #000;" align="left" rowspan="4" width="60%">';		
			$templateHtml .= '<div style="border-bottom:0px;" ><b>Thank you for your purchase. Happy Shopping!<br>TechDrive Supplies</b></div>';
			$templateHtml .= '</th>';
			$templateHtml .= '<th style="border-top:1px solid #000;" align="left" width="25%">SUB TOTAL:</th>';
			$templateHtml .= '<td style="border-top:1px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>';
			$templateHtml .= '</tr>';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >POSTAGE(Ex TAX):</th>';
			$templateHtml .= '<td style="" align="right">'.$POSTAGE.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >TAX:</th>';
			$templateHtml .= '<td style="" align="right">'.$TAX.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-bottom:3px solid #000;" align="left" >TOTAL:</th>';
			$templateHtml .= '<td style="border-bottom:3px solid #000;" align="right">'.$TOTAL.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '</table>';
			$templateHtml .= '</div>';
				
			$templateHtml .= '<div class="row" style="text-align:center; border-bottom:0px;margin-left:20%; padding:0px" >';
			$templateHtml .= '<div style="width:70%; text-align:center; border:2px solid #000000; padding:10px;" >';
	
			$templateHtml .= '<table style="padding:0; text-align:center"><tr>
			<td width="15%"><img src="http://xsensys.com/img/happy_g.png" width="80px"></td>
			<td width="85%" align="center"><span style="font-size:24px">Happy with your order?</span> <br>
			<span style="font-size:10px;">Please take a moment to leave us positive feedback.</span><br>
			<img src="http://xsensys.com/img/5star_g.png" width="120px"><br></td></tr>
			</table>';
			$templateHtml .= '<div style="text-align:left; font-size:10px;">
			If however, for any reason you are not entirely happy with your order, please contact us to allow us to rectify any issues before leaving feedback.
			</div>';
			
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
		
		}		
		elseif($result == 'ebay'){			
			$templateHtml .='<!-- eBuyersDirect Invoice -->';			
			$templateHtml .='<div id="label" style="margin-top:20px;">';
			$templateHtml .='<div class="container">';			
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="50%" valign="top">
			4 Norwood Court<br>
La Rue Militaire<br>
St John, Jersey Channel Islands JE3 4DP<br>
United Kingdom
			</td>';
			$templateHtml .='<td  width="50%" valign="top" align="right">
			<img src=http://xsensys.com/img/ebuyerDirect.png width="300px">
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<hr style="color:#6bc15f">';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>'; 
			
			//mb_convert_encoding($SHIPPING_ADDRESS,'HTML-ENTITIES','UTF-8')			
			$t = mb_convert_encoding($SHIPPING_ADDRESS, "windows-1251", "utf-8");		
			$templateHtml .='<td  valign="top" width="37%"><h4>Delivery Address:</h4>'.$t.'</td>';
			$templateHtml .='<td  valign="top" width="38%"><h4>Billing Address:</h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';
			
			$templateHtml .='<td  width="25%" valign="top">
			<table>
			<tr>
			<td><b>Invoice ID:</b></td>
			<td align="right">'.$OrderId.'</td>
			</tr>
			<tr>
			<td><b>Invoice Date:</b></td>
			<td align="right">'.$INVOICE_DATE.'</td>
			</tr>
			</table>
			<div style="font-size:44px; font-weight:bold; text-align:right">INVOICE</div>
			</td>';
			
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			
			$templateHtml .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:625px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
			foreach($items as $count => $item){
		    
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				}else{
					$b = '';
				}					
				
			  $templateHtml .='<tr>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->SKU.'</td>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->Title.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Quantity.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->PricePerUnit.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->TaxRate.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Tax.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Cost.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->CostIncTax.'</td>
				</tr>';            
       
			}
			$templateHtml .= '</table>';

			$templateHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$templateHtml .='<tr>
			<th style="border-top:2px solid #000;" align="left" rowspan="4" width="60%"></th>
			<th style="border-top:2px solid #000;" align="left" width="25%">SUB TOTAL:</th>
			<td style="border-top:2px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>
			</tr>';

			$templateHtml .='<tr>
			<th style="" align="left" >POSTAGE(Ex TAX):</th>
			<td style="" align="right">'.$POSTAGE.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TAX:</th>
			<td style="" align="right">'.$TAX.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TOTAL:</th>
			<td style="" align="right">'.$TOTAL.'</td>
			</tr>';
			$templateHtml .='</table>';
			$templateHtml .='</div>';
			
			$templateHtml .='<hr style="color:#6bc15f">';
			$templateHtml .='<div class="footer row" style="font-size:15px; text-align:left; border-bottom:0px;" >
			<span style="font-size:28px; font-weight:bold">THANK YOU</span> <i>for using ebuyersDirect-2u</i><br><br>
			<b>Please take a moment to leave us positive feedback.</b><br>
			If however, for any reason you are not entirely happy with your
			order, please contact us to allow us to rectify<br>any issues before
			leaving feedback.</div>';
			$templateHtml .='</div>';
		}
		elseif($result == 'http'){		
			$templateHtml  ='<!-- storeforlife Invoice -->';			
			$templateHtml  .='<div id="label">';
			$templateHtml  .='<div class="container">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px"></table>';
			$templateHtml  .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  width="60%" valign="top">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml  .='<td  width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			$templateHtml  .='</td>';
			$templateHtml  .='<td  width="40%" valign="top" align="center">
			<img src="http://xsensys.com/img/sfl_logo.png"><br>			
			36 HARBOUR REACH, LA RUE DE CARTERET<br> ST HELIER, JERSEY, JE2 4HR</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			
			$templateHtml  .='<table>';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td width="80%"><span style="font-weight:bold; font-size:32px">INVOICE #'.$OrderId.'</span><br></td>';
			$templateHtml  .='<td width="20%" align="right"><b>Date:</b> '.$INVOICE_DATE.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';

			$templateHtml  .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:650px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:3px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
				foreach($items as $item){
					
				  $templateHtml .='<tr style="background-color:#f2f2f2">
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$item->Title.'</td>
					   <td style="" valign="top">'.$item->Quantity.'</td>
					   <td style="" valign="top">'.$item->PricePerUnit.'</td>
					   <td style="" valign="top">'.$item->TaxRate.'</td>
					   <td style="" valign="top">'.$item->Tax.'</td>
					   <td style="" valign="top">'.$item->Cost.'</td>
					   <td style="" valign="top">'.$item->CostIncTax.'</td>
					</tr>';            
       
				}
			$templateHtml .= '</table>';
		 
		
			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-top:1px solid #000;border-bottom:3px solid #000;" align="left" rowspan="4" width="60%">';		
			$templateHtml .= '<div style="border-bottom:0px;" ><b>Thank you for your purchase. Happy Shopping!<br>StoreForLife</b></div>';
			$templateHtml .= '</th>';
			$templateHtml .= '<th style="border-top:1px solid #000;" align="left" width="25%">SUB TOTAL:</th>';
			$templateHtml .= '<td style="border-top:1px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>';
			$templateHtml .= '</tr>';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >POSTAGE(Ex TAX):</th>';
			$templateHtml .= '<td style="" align="right">'.$POSTAGE.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >TAX:</th>';
			$templateHtml .= '<td style="" align="right">'.$TAX.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-bottom:3px solid #000;" align="left" >TOTAL:</th>';
			$templateHtml .= '<td style="border-bottom:3px solid #000;" align="right">'.$TOTAL.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '</table>';
			$templateHtml .= '</div>';
				
			$templateHtml .= '<div class="row" style="text-align:center; border-bottom:0px;margin-left:20%; padding:0px" >';
			$templateHtml .= '<div style="width:70%; text-align:center; border:2px solid #000000; padding:10px;" >';
	
			$templateHtml .= '<table style="padding:0; text-align:center"><tr>
			<td width="15%"><img src="http://xsensys.com/img/happy_g.png" width="80px"></td>
			<td width="85%" align="center"><span style="font-size:24px">Happy with your order?</span> <br>
			<span style="font-size:10px;">Please take a moment to leave us positive feedback.</span><br>
			<img src="http://xsensys.com/img/5star_g.png" width="120px"><br></td>
			</tr>
			</table>';
			$templateHtml .= '<div style="text-align:left; font-size:10px;">
			If however, for any reason you are not entirely happy with your order, please contact us to allow us to rectify any issues before leaving feedback.
			</div>';

			
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
			
		}
				
 		return $templateHtml;	
	}
	
	public function getTemplateDHL($order = null,$waybill_number = null )
	{
		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdatesArchive');
		$this->loadModel('Product');	
		
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  		 		
			
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
			$BILLING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName)).'<br>';
			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';

			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$order->CustomerInfo->BillingAddress->Country ? $order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
			
			$SHIPPING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName)).'<br>';
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $order->CustomerInfo->Address->Country ? $order->CustomerInfo->Address->Country : '';
			}			
		}		
		
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 
		 
			
		$INVOICE_DATE	  = date('Y-M-d', strtotime($order->GeneralInfo->ReceivedDate));
		$templateHtml 	  = NULL;
		
		/*
			Date 11 MAR 2019 on recommendations of Lalit.
			For Over LVCR Limit in Invoice Format need to remove Tax Rate and Tax column and need to modify heading
			
		*/
		$lvcr_limit = 17.99; //gbp
		
		$final_total = $TOTAL ;
		if($Currency == 'GBP'){
			$final_total = $TOTAL * (1.122);
		}
		if($final_total < $lvcr_limit){
			$POSTAGE = 4;
		}
		$ShipmentWeight  = 0.0;
		$swresult = $this->MergeUpdatesArchive->find('first',['conditions' => ['order_id' => $OrderId],'fields'=>['packet_weight','envelope_weight']]);
		if(count( $swresult ) > 0){				
			$ShipmentWeight = $swresult['MergeUpdatesArchive']['packet_weight'] + $swresult['MergeUpdatesArchive']['envelope_weight'];
		}
		$country_code = $this->getCountryCode($Country);
		$tax_rate = ''; $vat_amount = 0;
		$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
		if(count( $thsresult ) > 0){				
			$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
		}
		
		$currency_code = $this->getCurrencyCode($Currency);
								
		if($result == 'cost'){
			
			$EORI_Number = 'GB019020854000';
			$UK_VAT_Number = 'GB 304984295'; 
			$DE_VAT_Number = 'DE 321777974';
			$FR_VAT_Number = ''; 
				
			//$templateHtml ='<!-- CostBreaker Invoice -->';			
			$templateHtml  ='<div id="label">';
			$templateHtml .='<div class="container">';	
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top" style="font-size:15px;">'.$this->COST_STORE_NAME.'<br>'.$this->COST_STORE_ADDRESS.'</td>';
			$templateHtml .='<td  width="25%" valign="top" >';
			$templateHtml .='<span style="font-weight:bold; font-size:20px">INVOICE(CIF)</span>';
			$templateHtml .='<br><b>Invoice Number:</b>'.$OrderId;
			$templateHtml .='<br><b>EORI Number:</b>'.$EORI_Number;
			if($Country == 'United Kingdom' && $UK_VAT_Number != ''){
				$templateHtml .='<br><b>VAT Number:</b>'.$UK_VAT_Number;
			}else if($Country == 'Germany' && $DE_VAT_Number != '' ){
				$templateHtml .='<br><b>VAT Number:</b>'.$DE_VAT_Number;
			}
			if($waybill_number != ''){
				$templateHtml .='<br><b>WayBill Number:</b>'.$waybill_number;
			}
			
			$templateHtml .='<br><b>Date:</b>'.$INVOICE_DATE;
			$templateHtml .='</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';	
			$templateHtml .='<td width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';	
			$templateHtml .='</tr>';	
			$templateHtml .='</table>';
	
			$templateHtml  .='<div class="tablesection row" style="margin:5px 0 0 0; border-bottom:0px;height:700px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">';
  			$country_of_origin = '';
			foreach($items as $count => $item){ 
					
				$Title = $item->Title;
				if(strlen($item->Title) < 10){
					$Title = $item->ChannelTitle;
				}
				$pro_weight = 0;  
				$pro_res = $this->Product->find('first', array('conditions' => ['Product.product_sku' => $item->SKU],'fields' => ['Product.country_of_origin','ProductDesc.weight']));
 				if(count( $pro_res ) > 0){				
					$pro_weight = $pro_res['ProductDesc']['weight'];
					$country_of_origin = $pro_res['Product']['country_of_origin'];
					 
				}
				$hs_code = '';
				/*$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $item->SKU,'country_code' => $country_code)));
				if(count( $hsresult ) > 0){				
					$hs_code = $hsresult['ProductHscode']['hs_code'];
				}*/
				if($count == 0){	
					  $templateHtml  .='<tr>';
						    $templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Item</th>';
							
							$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Qty</th>';
							$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Weight</th>';
						    if($hs_code != ''){
						   		 $templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">HSCode</th>';
							}
							 if($country_of_origin != ''){
						   		 $templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Country of origin</th>';
							}
							$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Price</th>';
							$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Tax Rate</th>';
  						   $templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Tax</th>
						   <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Line Total</th>
						</tr>';
				}
				
				  $item_vat_amount = $item->Cost - ($item->Cost/(1 + ($tax_rate/100)));
				  $item_cost = $item->Cost - $item_vat_amount;
				  
				  $templateHtml .='<tr>';
  				  $templateHtml .='   <td align="left" valign="top" class="">'. $Title  .'</td>'; 			  
				  $templateHtml .='   <td valign="top">'.$item->Quantity.'</td>'; 
				  $templateHtml .='   <td valign="top">'.($pro_weight * $item->Quantity).'</td>'; 	
  				  if($hs_code != ''){
					$templateHtml .='   <td valign="top">'.$hs_code.'</td>'; 
				  }	
				  if($country_of_origin != ''){
					$templateHtml .='   <td valign="top">'.$country_of_origin.'</td>'; 
				  }		  
				  $templateHtml .='   <td valign="top">'.number_format($item_cost,2).'&nbsp;'.$currency_code.'</td>'; 
				  $templateHtml .='   <td valign="top">'.$tax_rate.'%</td>'; 				  
				  $templateHtml .='   <td valign="top">'.number_format($item_vat_amount,2).'&nbsp;'.$currency_code.'</td>'; 
				  $templateHtml .='   <td valign="top">'.number_format($item->Cost,2).'&nbsp;'.$currency_code.'</td>'; 
				$templateHtml .='</tr>'; 
        
			}
			
			 $postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
			 $postage_cost = $POSTAGE - $postage_vat_amount;
			  
			$templateHtml .='<tr>';
			$templateHtml .='<td align="left" valign="top" class="">Shipping & Handling Fee</td>'; 
  			$templateHtml .='<td valign="top">&nbsp;</td>'; 
			$templateHtml .='<td valign="top">&nbsp;</td>'; 
			if($hs_code != ''){
				$templateHtml .='<td valign="top">&nbsp;</td>'; 
			}
			if($country_of_origin != ''){
				$templateHtml .='<td valign="top">&nbsp;</td>'; 
			}
			$templateHtml .='<td valign="top">'.number_format($postage_cost,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='<td valign="top">'.$tax_rate.'%</td>'; 				  
			$templateHtml .='<td valign="top">'.number_format($postage_vat_amount,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='<td valign="top">'.number_format($POSTAGE,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='</tr>'; 
			
			$final_total =  $item_cost + $postage_cost + $item_vat_amount + $postage_vat_amount;
			
			$templateHtml .='<tr>';
				$templateHtml .='<td align="left" valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000"><strong>Total</strong></td>'; 
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>'; 
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>'; 
				
				if($hs_code != ''){
					$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>'; 
				}
				if($country_of_origin != ''){
					$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>'; 
				}
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">'.number_format(($item_cost + $postage_cost),2).'&nbsp;'.$currency_code.'</td>'; 
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">'.$tax_rate.'%</td>'; 				  
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">'.number_format(($item_vat_amount + $postage_vat_amount),2).'&nbsp;'.$currency_code.'</td>'; 
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">'.number_format($final_total,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='</tr>'; 
 					
			$templateHtml .= '</table>';
			
			 
 			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
 			$templateHtml .='<tr>';			 
			$templateHtml .='<td align="left"><strong>Shipment Weight</strong>: '. number_format($ShipmentWeight,2) .'Kg</td>';	
			$templateHtml .='</tr>';

			
			$templateHtml .='<tr>';	
			$templateHtml .='<td align="left" width="33%">'; 
			$templateHtml .='<div style="font-size:14px;"><strong>Declaration:</strong></div>
			<div class="row" style="font-size:12px;">
We hereby certify that the information contained <br>in this invoice is true and correct and that
the <br>contents of this shipment are as started above.</div>';
			$templateHtml .='</td>';
			$templateHtml .='</tr>';
 			$templateHtml .='</table>';	
 			$templateHtml .='</div>';
 
			$templateHtml .='<div class="footer row" style="text-align:center; border-bottom:0px;">
			<b>Thank you for your purchase. Happy Shopping!</div>';
		 	$templateHtml .='</div>'; 
		
		}
		elseif($result == 'mare'){
			
			$EORI_Number = 'GB019434034000';
			$UK_VAT_Number = 'GB 307817693'; 
			$DE_VAT_Number = '';
			$FR_VAT_Number = 'FR 07879900934';
			
			$templateHtml ='<!-- eBuyer Express(Marec) Invoice -->';			
			$templateHtml .='<div id="label" style="margin-top:20px;">';
			$templateHtml .='<div class="container">';			
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top"><img src="'.WWW_ROOT.'img/ebuyerexpress.jpg" width="380px"></td>';
			$templateHtml .='<td  width="25%" valign="top">
			'.$this->MARE_STORE_NAME.'<br>
			'.$this->MARE_STORE_ADDRESS.'			
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="35%"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml .='<td valign="top" width="35%"><h4><u>Billing Address:</u></h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';
			 
 			$templateHtml .='<td  width="29%" valign="top">';
			$templateHtml .='<table>';
			$templateHtml .='<tr><td><b>EORI No:</b></td>	<td>'.$EORI_Number.'</td></tr>';
			$templateHtml .='<tr><td><b>Invoice(CIF):</b></td>	<td>'.$OrderId.'</td></tr>';
			if($Country == 'United Kingdom' && $UK_VAT_Number != '' ){
				$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$UK_VAT_Number.'</td></tr>';
			}else if($Country == 'Germany' && $DE_VAT_Number != ''){
				$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$DE_VAT_Number.'</td></tr>';
			}
			if($waybill_number != ''){
				$templateHtml .='<tr><td><b>WayBill No:</b></td>	<td>'.$waybill_number.'</td></tr>';
			}
			
			$templateHtml .='<tr><td><b>Invoice Date:</b></td><td>'.$INVOICE_DATE.'</td></tr>';
			$templateHtml .='</table>';
			$templateHtml .='</td>'; 			
 			
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
 			 
			$templateHtml .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:500px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">';
 			$country_of_origin = '';
			foreach($items as $count => $item){
		    	$b = '';
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				} 
									
				$pro_weight = 0; 
				$pro_res = $this->Product->find('first', array('conditions' => ['Product.product_sku' => $item->SKU],'fields' => ['Product.country_of_origin','ProductDesc.weight']));
 				if(count( $pro_res ) > 0){				
					$pro_weight = $pro_res['ProductDesc']['weight'];
					$country_of_origin = $pro_res['Product']['country_of_origin'];
 				} 
				
				$hs_code = '';
			   /* $hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $item->SKU,'country_code' => $country_code)));
				if(count( $hsresult ) > 0){				
					$hs_code = $hsresult['ProductHscode']['hs_code'];
				}*/
				
				if( $count == 0){
					
					$templateHtml  .='<tr>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">SKU</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Item</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Qty</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Weight</th>';
					if( $hs_code != ''){
						$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">HSCode</th>';
					}
					if($country_of_origin != ''){						 
						$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Country of origin</th>';
					}
 					
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Price</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Tax Rate</th>';				
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Tax</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Line Total</th>';
					$templateHtml  .='</tr>';
				}
				
				$Title = $item->Title;
				if(strlen($item->Title) < 10){
					$Title = $item->ChannelTitle;
				}
					  
				$templateHtml .='<tr>';
					$templateHtml .=' <td style="'.$b.'" align="left" valign="top">'.$item->SKU.'</td>';
					$templateHtml .=' <td style="'.$b.'" align="left" valign="top">'.$Title.'</td>';
					$templateHtml .=' <td style="'.$b.'" valign="top">'.$item->Quantity.'</td>';
					$templateHtml .=' <td style="'.$b.'" valign="top">'.($pro_weight * $item->Quantity).'</td>';
					if($hs_code != ''){
						$templateHtml .=' <td style="'.$b.'" align="left" valign="top" >'.$hs_code.'</td>';
					}
					if($country_of_origin != ''){
						$templateHtml .='<td style="'.$b.'" align="left" valign="top" >'.$country_of_origin.'</td>'; 
					}
					
					$item_vat_amount = $item->Cost - ($item->Cost/(1 + ($tax_rate/100)));
				    $item_cost = $item->Cost - $item_vat_amount;
				  
 					$templateHtml .=' <td style="'.$b.'" valign="top">'.number_format($item_cost,2).'&nbsp;'.$currency_code.'</td>';	
					$templateHtml .=' <td style="'.$b.'" valign="top">'.$tax_rate.'%</td>';				 
					$templateHtml .=' <td style="'.$b.'" valign="top" >'.number_format($item_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
					$templateHtml .=' <td style="'.$b.'" valign="top" >'.number_format($item->Cost,2).'&nbsp;'.$currency_code.'</td>';
				$templateHtml .='</tr>';            
       
			}
 			
			$postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
			$postage_cost = $POSTAGE - $postage_vat_amount;
			
			$b_bottom = 'border-bottom:2px solid #000;';  
			$templateHtml .='<tr>';
				$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top" colspan="2">SHIPPING HANDLING FEE</td>'; 
   				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				if($hs_code != ''){
					$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
				if($country_of_origin != ''){
					$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format($postage_cost,2).'&nbsp;'.$currency_code.'</td>';	
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.$tax_rate.'%</td>';				 
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top" >'.number_format($postage_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top" >'.number_format($POSTAGE,2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='</tr>';  
			
 			$final_total =  $item_cost + $postage_cost + $item_vat_amount + $postage_vat_amount;
			  
			$templateHtml .='<tr>';
				$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top" colspan="2"><strong>TOTAL</strong></td>'; 
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				if($hs_code != ''){
					$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
				if($country_of_origin != ''){
					$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}  				 
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format(($item_cost+$postage_cost),2).'&nbsp;'.$currency_code.'</td>';	
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.$tax_rate.'%</td>';					 
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format(($item_vat_amount+$postage_vat_amount),2).'&nbsp;'.$currency_code.'</td>';
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format($final_total,2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='</tr>';  
			    
			  
			$templateHtml .= '</table>';
			
			$templateHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$templateHtml .='<tr>';
			$templateHtml .='<td align="left"><strong>Shipment Weight</strong>: '. number_format($ShipmentWeight,2) .'Kg</td>';	
 			$templateHtml .='</tr>';	
			 $templateHtml .='<tr>';	
			$templateHtml .='<td align="left" width="33%">'; 
			$templateHtml .='<div style="font-size:14px;"><strong>Declaration:</strong></div>
			<div class="row" style="font-size:12px;">
We hereby certify that the information contained <br>in this invoice is true and correct and that
the <br>contents of this shipment are as started above.</div>';
			$templateHtml .='</td>';
			$templateHtml .='</tr>';
			
			$templateHtml .='</table>';
			
			$templateHtml .='</div>';
			
 			$templateHtml .='<div class="footer row" style="font-size:15px; text-align:center; border-bottom:0px;" >
			<h2>THANK YOU FOR YOUR PURCHASE</h2> 
			<b>Please take a moment to leave us positive feedback.</b><br>
			If however, for any reason you are not entirely happy with your
			order, please contact us<br>to allow us to rectify any issues before
			leaving feedback.</div>';			
		 	$templateHtml .='</div>';
					
		}
		elseif($result == 'rain'){
		
			$EORI_Number = 'GB318649182000';
			$UK_VAT_Number = 'GB 318649182'; 
			$IT_VAT_Number = 'IT 11061000961';
			$DE_VAT_Number = 'DE 327173988';
			$FR_VAT_Number = '';     
			
			//$templateHtml ='<!-- Rainbow Invoice -->';			
			$templateHtml ='<div id="label">';
			$templateHtml .='<div class="container">';
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="60%" style="text-align:center; font-weight:bold; font-size:28px">INVOICE(CIF)</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="50%" valign="top">';
			$templateHtml .='<img src="'.WWW_ROOT.'img/rainbow_logo.png" width="250px">';
			$templateHtml .='</td>';			
			$templateHtml .='<td  width="50%" valign="top" style="text-align:right">
				<b>'.$this->RAIN_STORE_NAME.'</b><br>
				'.$this->RAIN_STORE_ADDRESS.'
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<table cellpadding="3" style="background-color:#333; color:#fff; margin-bottom:20px;" class="topborder bottomborder"><tr>';
			
			$templateHtml .='<td><span class="bold">Invoice ID:</span> '.$OrderId.'</td>';		
			$templateHtml .='<td><span class="bold">EORI No:'.$EORI_Number.'</td>';
			if($Country == 'United Kingdom' && $UK_VAT_Number != ''){
				$templateHtml .='<td><b>VAT No:</b>'.$UK_VAT_Number.'</td>';
			}else if($Country == 'Germany' && $DE_VAT_Number != '' ){
				$templateHtml .='<td><b>VAT No:</b>'.$DE_VAT_Number.'</td>';
			}
			if($waybill_number != ''){
				$templateHtml .='<td><b>WayBill No:</b>'.$waybill_number.'</td>';
			}
			$templateHtml .='<td><span class="bold">Date:</span> '.$INVOICE_DATE.'</td>';
			
			$templateHtml .='</tr></table>';
			
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  valign="top" width="50%"><h4 class="topborder bottomborder" style="background-color:#333; color:#fff; padding:3px">
			Billing Address:</h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml .='<td  width="50%" valign="top"><h4 class="topborder bottomborder" style="background-color:#333; color:#fff; padding:3px">
			Delivery Address:</h4>'.$SHIPPING_ADDRESS.'</td>';
			
			$templateHtml .='</tr>';		
			$templateHtml .='</table>';
	
			$templateHtml .='<div class="tablesection row" style="margin:5px 0 0 0; border-bottom:0px;height:600px">';
			$templateHtml .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse: collapse; margin:0px;">';
			$country_of_origin = '';
			
			foreach($items as $count => $item){
				
					$Title = $item->Title;
					if(strlen($item->Title) < 10){
						$Title = $item->ChannelTitle;
					}
					
					$pro_weight = 0; 
					$pro_res = $this->Product->find('first', array('conditions' => ['Product.product_sku' => $item->SKU],'fields' => ['Product.country_of_origin','ProductDesc.weight']));
					if(count( $pro_res ) > 0){				
						$pro_weight = $pro_res['ProductDesc']['weight'];
						$country_of_origin = $pro_res['Product']['country_of_origin'];
					} 
					$hs_code = '';
					/*$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $item->SKU,'country_code' => $country_code)));
					if(count( $hsresult ) > 0){				
						$hs_code = $hsresult['ProductHscode']['hs_code'];
					}*/
				
					if($count == 0){
						$templateHtml .='<tr>';
						$templateHtml .='<th align="left" class="topborder bottomborder">SKU</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Item</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Qty</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Weight</th>';
						if($hs_code !='' ){				
							$templateHtml .='<th align="left" class="topborder bottomborder">HSCode</th>';
						}
						if($country_of_origin !='' ){				
							$templateHtml .='<th align="left" class="topborder bottomborder">Country of origin</th>';
						}						
						$templateHtml .='<th align="left" class="topborder bottomborder">Price</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Tax Rate</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Tax</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Line Cost</th>';
						$templateHtml .='</tr>';
					}
				
					$templateHtml .='<tr>';
					$templateHtml .='   <td align="left">'.$item->SKU.'</td>';
					$templateHtml .='   <td align="left">'.$Title.'</td>';
					$templateHtml .='   <td align="left">'.$item->Quantity.'</td>';
					$templateHtml .='   <td align="left">'.($pro_weight * $item->Quantity).'</td>';
					if($hs_code !='' ){	
						$templateHtml .='   <td align="left">'.$hs_code.'</td>';					
					}
					if($country_of_origin !='' ){
						$templateHtml .='   <td align="left">'.$country_of_origin.'</td>';	
					}  
 					$item_vat_amount = $item->Cost - ($item->Cost/(1 + ($tax_rate/100)));
					$item_cost = $item->Cost - $item_vat_amount;
  
  					$templateHtml .='<td style="" valign="top">'.number_format($item_cost,2).'&nbsp;'.$currency_code.'</td>';
  					$templateHtml .='<td style="" valign="top">'.$tax_rate.'%</td>';
					$templateHtml .='<td style="" valign="top">'.number_format($item_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
 					$templateHtml .='<td style="" valign="top">'.number_format($item->Cost,2).'&nbsp;'.$currency_code.'</td>'; 
					$templateHtml .='</tr>';            
				
			}
			
			$postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
			$postage_cost = $POSTAGE - $postage_vat_amount;
			
			$templateHtml .='<tr>';
			$templateHtml .='<td align="left" colspan="2">SHIPPING & HANDLING FEE</td>'; 
 			$templateHtml .='<td align="left">&nbsp;</td>';
			$templateHtml .='<td align="left">&nbsp;</td>';
			if($hs_code !='' ){	
				$templateHtml .='<td align="left">&nbsp;</td>';					
			}
			if($country_of_origin !='' ){
				$templateHtml .='<td align="left">&nbsp;</td>';		
			}
  			$templateHtml .='<td align="left">'.number_format($postage_cost,2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='<td align="left">'.$tax_rate.'%</td>';
			$templateHtml .='<td align="left">'.number_format($postage_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='<td align="left">'.number_format($POSTAGE,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='</tr>'; 
			
			$final_total =  $item_cost + $postage_cost + $item_vat_amount + $postage_vat_amount;
			
			$templateHtml .='<tr>';
			$templateHtml .='   <td align="left" colspan="2" style="border-top:1px solid #000;border-bottom:1px solid"><strong>TOTAL</strong></td>'; 
			
			$templateHtml .='   <td style="border-top:1px solid #000;border-bottom:1px solid">&nbsp;</td>';
			$templateHtml .='   <td style="border-top:1px solid #000;border-bottom:1px solid">&nbsp;</td>';
			if($hs_code !='' ){	
				$templateHtml .='   <td style="border-top:1px solid #000;border-bottom:1px solid">&nbsp;</td>';					
			}
			if($country_of_origin !='' ){
				$templateHtml .='   <td style="border-top:1px solid #000;border-bottom:1px solid">&nbsp;</td>';	
			}
			$templateHtml .='<td style="border-top:1px solid #000;border-bottom:1px solid">'.number_format(($item_cost + $postage_cost),2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='<td style="border-top:1px solid #000;border-bottom:1px solid">'.$tax_rate.'%</td>';
			$templateHtml .='<td style="border-top:1px solid #000;border-bottom:1px solid">'.number_format(($item_vat_amount + $postage_vat_amount),2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='<td style="border-top:1px solid #000;border-bottom:1px solid">'.number_format($final_total,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='</tr>';  
 					
			$templateHtml .='</table>';
		
			$templateHtml .='<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;">';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td align="left"><strong>Shipment Weight</strong>: '. number_format($ShipmentWeight,2) .'Kg</td>';	
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td>';
			$templateHtml .='<div style="font-size:14px;"><strong>Declaration:</strong></div>
			<div class="row" style="font-size:12px;">We hereby certify that the information contained <br>in this invoice is true and correct and that
the <br>contents of this shipment are as started above.</div>';
			 $templateHtml .='<tr>';
			 $templateHtml .='</tr>';
			 			
			$templateHtml .='</table>';			
			$templateHtml .='</div>';
 
			$templateHtml  .= '<div class="footer row" style="text-align:center; border-bottom:0px;" >
				<b>Thank you for your purchase. Happy Shopping!<br>
				Rainbow Retail</b>';	
			 $templateHtml  .= '</div>';
		}
		else{
 		
 			$itmsHtml  ='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:500px">';
			$itmsHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">';
 			$country_of_origin = '';
			foreach($items as $count => $item){
		    	$b = '';
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				} 					
				$pro_weight = 0; 
				$pro_res = $this->Product->find('first', array('conditions' => ['Product.product_sku' => $item->SKU],'fields' => ['Product.country_of_origin','ProductDesc.weight']));
				if(count( $pro_res ) > 0){				
					$pro_weight = $pro_res['ProductDesc']['weight'];
					$country_of_origin = $pro_res['Product']['country_of_origin'];
				} 
				$hs_code = '';
			   /* $hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $item->SKU,'country_code' => $country_code)));
				if(count( $hsresult ) > 0){				
					$hs_code = $hsresult['ProductHscode']['hs_code'];
				}*/
				
				if( $count == 0){
					
					$itmsHtml  .='<tr>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">SKU</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Item</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Qty</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Weight</th>';
					if( $hs_code != ''){
						$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">HSCode</th>';
					}
					if( $country_of_origin != ''){
						$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Country of origin</th>';
					}
 					
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Price</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Tax Rate</th>';				
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Tax</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Line Total</th>';
					$itmsHtml  .='</tr>';
				}
				
				$Title = $item->Title;
				if(strlen($item->Title) < 10){
					$Title = $item->ChannelTitle;
				}
					  
				$itmsHtml .='<tr>';
					$itmsHtml .=' <td style="'.$b.'" align="left" valign="top">'.$item->SKU.'</td>';
					$itmsHtml .=' <td style="'.$b.'" align="left" valign="top">'.$Title.'</td>';
					$itmsHtml .=' <td style="'.$b.'" valign="top">'.$item->Quantity.'</td>';
					$itmsHtml .=' <td style="'.$b.'" valign="top">'.($pro_weight * $item->Quantity).'</td>';
					if($hs_code != ''){
						$itmsHtml .=' <td style="'.$b.'" align="left" valign="top" >'.$hs_code.'</td>';
					}
					if($country_of_origin != ''){
						$itmsHtml .=' <td style="'.$b.'" align="left" valign="top" >'.$country_of_origin.'</td>';
					}
					
					$item_vat_amount = $item->Cost - ($item->Cost/(1 + ($tax_rate/100)));
				    $item_cost = $item->Cost - $item_vat_amount;
				  
 					$itmsHtml .=' <td style="'.$b.'" valign="top">'.number_format($item_cost,2).'&nbsp;'.$currency_code.'</td>';	
					$itmsHtml .=' <td style="'.$b.'" valign="top">'.$tax_rate.'%</td>';				 
					$itmsHtml .=' <td style="'.$b.'" valign="top" >'.number_format($item_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
					$itmsHtml .=' <td style="'.$b.'" valign="top" >'.number_format($item->Cost,2).'&nbsp;'.$currency_code.'</td>';
				$itmsHtml .='</tr>';            
       
			}
 			
			$postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
			$postage_cost = $POSTAGE - $postage_vat_amount;
			
			$b_bottom = 'border-bottom:2px solid #000;';  
			$itmsHtml .='<tr>';
				$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top" colspan="2">SHIPPING HANDLING FEE</td>'; 
   				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				if($hs_code != ''){
					$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}if($country_of_origin != ''){
					$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format($postage_cost,2).'&nbsp;'.$currency_code.'</td>';	
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.$tax_rate.'%</td>';				 
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top" >'.number_format($postage_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top" >'.number_format($POSTAGE,2).'&nbsp;'.$currency_code.'</td>';
			$itmsHtml .='</tr>';  
			
 			$final_total =  $item_cost + $postage_cost + $item_vat_amount + $postage_vat_amount;
			  
			$itmsHtml .='<tr>';
				$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top" colspan="2"><strong>TOTAL</strong></td>'; 
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				if($hs_code != ''){
					$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
				if($country_of_origin != ''){
					$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
  				 
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format(($item_cost+$postage_cost),2).'&nbsp;'.$currency_code.'</td>';	
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.$tax_rate.'%</td>';					 
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format(($item_vat_amount+$postage_vat_amount),2).'&nbsp;'.$currency_code.'</td>';
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format($final_total,2).'&nbsp;'.$currency_code.'</td>';
			$itmsHtml .='</tr>';  
			    
			  
			$itmsHtml .= '</table>';
			
			$itmsHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$itmsHtml .='<tr>';
			$itmsHtml .='<td align="left"><strong>Shipment Weight</strong>: '. number_format($ShipmentWeight,2) .'Kg</td>';	
 			$itmsHtml .='</tr>';	
			 $itmsHtml .='<tr>';	
			$itmsHtml .='<td align="left" width="33%">'; 
			$itmsHtml .='<div style="font-size:14px;"><strong>Declaration:</strong></div>
			<div class="row" style="font-size:12px;">
We hereby certify that the information contained <br>in this invoice is true and correct and that
the <br>contents of this shipment are as started above.</div>';
			$itmsHtml .='</td>';
			$itmsHtml .='</tr>';
			
			$itmsHtml .='</table>';
			
			$itmsHtml .='</div>';
			
 			 
 		
			 if($result == 'tech'){
  			
				$EORI_Number = '';
				$UK_VAT_Number = ''; 
				$DE_VAT_Number = '';
				$FR_VAT_Number = '';
			
				//$templateHtml  ='<!-- TechDrive Invoice -->';
				$templateHtml   ='<div id="label">';
				$templateHtml  .='<div class="container">';
				$templateHtml  .='<table class="header row" style="border-bottom:0px"></table>';
				$templateHtml  .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td  width="60%" valign="top">';
				$templateHtml  .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';
				$templateHtml  .='<td  width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
				$templateHtml  .='</td>';
				$templateHtml  .='<td width="40%" valign="top" align="center"><img src="'.WWW_ROOT.'img/techDriveSupplies2.jpg" width="280px"></td>';
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
				
				$templateHtml  .='<table>';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td><span style="font-weight:bold; font-size:18px">INVOICE(CIF) #'.$OrderId.'</span><br></td>';
  				$templateHtml .='<td><b>EORI No:</b></td>	<td>'.$EORI_Number.'</td>';
				$templateHtml .='<td><b>Invoice(CIF):</b></td>	<td>'.$OrderId.'</td>';
				if($Country == 'United Kingdom' && $UK_VAT_Number != '' ){
					$templateHtml .='<td><b>VAT No:</b></td>	<td>'.$UK_VAT_Number.'</td>';
				}else if($Country == 'Germany' && $DE_VAT_Number != ''){
					$templateHtml .='<td><b>VAT No:</b></td>	<td>'.$DE_VAT_Number.'</td>';
				}
				if($waybill_number != ''){
					$templateHtml .='<td><b>WayBill No:</b></td>	<td>'.$waybill_number.'</td>';
				}
				$templateHtml  .='<td><b>Date:</b> '.$INVOICE_DATE.'</td>';
			
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
	
				$templateHtml  .= $itmsHtml;
  					
				$templateHtml .= '<div class="row" style="text-align:center; border-bottom:0px;margin-left:20%; padding:0px" >';
				$templateHtml .= '<div style="width:70%; text-align:center; border:2px solid #000000; padding:10px;" >';
 				$templateHtml .= '<div style="text-align:left; font-size:10px;">
				If however, for any reason you are not entirely happy with your order, please contact us to allow us to rectify any issues before leaving feedback.
				</div>';
				
				$templateHtml .= '</div>';
				$templateHtml .= '</div>';
				$templateHtml .= '</div>';
			
			}		
			elseif($result == 'bbd_'){	
			
				$EORI_Number = '';
				$UK_VAT_Number = ''; 
				$DE_VAT_Number = '';
				$FR_VAT_Number = '';
					
				//$templateHtml ='<!-- BBD_EU Invoice -->';			
				$templateHtml  ='<div id="label" style="margin-top:20px;">';
				$templateHtml .='<div class="container">';			
				$templateHtml .='<table class="header row" style="border-bottom:0px">';
				$templateHtml .='</table>';
				$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
				$templateHtml .='<tr>';
				$templateHtml .='<td  width="75%" valign="top"><img src="'.WWW_ROOT.'img/bbdEu.png"></td>';
				$templateHtml .='<td  width="25%" valign="top">			
									Unit A1 21/F, Officeplus Among Kok<br>
									998 Canton Road<br>
									Hong Kong<br>
									KL<br>
									0000			
								</td>';
				$templateHtml .='</tr>';
				$templateHtml .='</table>';
				
				$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
				$templateHtml .='<tr>';
				$templateHtml .='<td valign="top" width="37%"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
				$templateHtml .='<td valign="top" width="38%"><h4><u>Billing Address:</u></h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';
	
				$templateHtml .='<td  width="25%" valign="top">';
				$templateHtml .='<table>';
				$templateHtml .='<tr><td><b>Invoice(CIF):</b></td>	<td>'.$OrderId.'</td></tr>';
				
 				if($Country == 'United Kingdom' && $UK_VAT_Number != '' ){
					$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$UK_VAT_Number.'</td></tr>';
				}else if($Country == 'Germany' && $DE_VAT_Number != ''){
					$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$DE_VAT_Number.'</td></tr>';
				}
				if($waybill_number != ''){
					$templateHtml .='<tr><td><b>WayBill No:</b></td>	<td>'.$waybill_number.'</td></tr>';
				}
				$templateHtml .='<tr><td><b>Invoice Date:</b></td><td>'.$INVOICE_DATE.'</td></tr>';
				
				$templateHtml .='</table>';
				$templateHtml .='</td>';
				$templateHtml .='</tr>';
				$templateHtml .='</table>';
				
				$templateHtml .= $itmsHtml;
 				
 				$templateHtml .='<div class="footer row" style="font-size:15px; text-align:center; border-bottom:0px;" >
				<h2>THANK YOU FOR YOUR PURCHASE</h2> 
				<b>Please take a moment to leave us positive feedback.</b><br>
				If however, for any reason you are not entirely happy with your
				order, please contact us<br>to allow us to rectify any issues before
				leaving feedback.</div>';			
				$templateHtml .='</div>';
						
			}
			elseif($result == 'ebay'){			
				//$templateHtml  ='<!-- eBuyersDirect Invoice -->';			
				$templateHtml  ='<div id="label" style="margin-top:20px;">';
				$templateHtml .='<div class="container">';			
				$templateHtml .='<table class="header row" style="border-bottom:0px">';
				$templateHtml .='</table>';
				$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
				$templateHtml .='<tr>';
				$templateHtml .='<td  width="50%" valign="top">
				4 Norwood Court<br>
	La Rue Militaire<br>
	St John, Jersey Channel Islands JE3 4DP<br>
	United Kingdom
				</td>';
				$templateHtml .='<td  width="50%" valign="top" align="right">
				<img src="'.WWW_ROOT.'img/ebuyerDirect.png" width="300px">
				</td>';
				$templateHtml .='</tr>';
				$templateHtml .='</table>';
				$templateHtml .='<hr style="color:#6bc15f">';
				
				$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
				$templateHtml .='<tr>'; 
				
				//mb_convert_encoding($SHIPPING_ADDRESS,'HTML-ENTITIES','UTF-8')			
				$t = mb_convert_encoding($SHIPPING_ADDRESS, "windows-1251", "utf-8");		
				$templateHtml .='<td  valign="top" width="37%"><h4>Delivery Address:</h4>'.$t.'</td>';
				$templateHtml .='<td  valign="top" width="38%"><h4>Billing Address:</h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';
				
				$templateHtml .='<td  width="25%" valign="top">';
				$templateHtml .='<table>';
				$templateHtml .='<tr><td><b>Invoice ID:</b></td><td align="right">'.$OrderId.'</td></tr>';
				if($Country == 'United Kingdom' && $UK_VAT_Number != '' ){
					$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$UK_VAT_Number.'</td></tr>';
				}else if($Country == 'Germany' && $DE_VAT_Number != ''){
					$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$DE_VAT_Number.'</td></tr>';
				}
				if($waybill_number != ''){
					$templateHtml .='<tr><td><b>WayBill No:</b></td>	<td>'.$waybill_number.'</td></tr>';
				}
				
				$templateHtml .='<tr><td><b>Invoice Date:</b></td><td align="right">'.$INVOICE_DATE.'</td></tr>';
				
				
				$templateHtml .='</table>				 
				</td>';
				
 				$templateHtml .='</tr>';
				$templateHtml .='</table>';
				
 				$templateHtml .= $itmsHtml; 
				 
				
				$templateHtml .='<hr style="color:#6bc15f">';
				$templateHtml .='<div class="footer row" style="font-size:15px; text-align:left; border-bottom:0px;" >
				<span style="font-size:28px; font-weight:bold">THANK YOU</span> <i>for using ebuyersDirect-2u</i><br><br>
				<b>Please take a moment to leave us positive feedback.</b><br>
				If however, for any reason you are not entirely happy with your
				order, please contact us to allow us to rectify<br>any issues before
				leaving feedback.</div>';
				$templateHtml .='</div>';
			}
			elseif($result == 'http'){		
			
				//$templateHtml  ='<!-- storeforlife Invoice -->';			
				$templateHtml 	='<div id="label">';
				$templateHtml  .='<div class="container">';
				$templateHtml  .='<table class="header row" style="border-bottom:0px"></table>';
				$templateHtml  .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td  width="60%" valign="top">';
				$templateHtml  .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';
				$templateHtml  .='<td  width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
				$templateHtml  .='</td>';
				$templateHtml  .='<td  width="40%" valign="top" align="center">
				<img src="'.WWW_ROOT.'img/sfl_logo.png"><br>			
				36 HARBOUR REACH, LA RUE DE CARTERET<br> ST HELIER, JERSEY, JE2 4HR</td>';
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
				
				$templateHtml  .='<table>';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td width="80%"><span style="font-weight:bold; font-size:32px">INVOICE #'.$OrderId.'</span><br></td>';
				$templateHtml  .='<td width="20%" align="right"><b>Date:</b> '.$INVOICE_DATE.'</td>';
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
	
				$templateHtml  .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:650px">';
				$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
				<tr>
				   <th style="border-top:3px solid #000;" align="left" class=" " width="14%">SKU</th>
				   <th style="border-top:3px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
				   <th style="border-top:3px solid #000;" valign="top" class=" " width="8%">Quantity</th>
				   <th style="border-top:3px solid #000;" align="left" class=" " width="6%">Unit</th>';
				   
				   if($final_total < $lvcr_limit){
					 $templateHtml  .='<th style="border-top:3px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>';
					 $templateHtml  .='<th style="border-top:3px solid #000;" valign="top" class=" " width="5%">Tax</th>';
				   }
				   
				   $templateHtml  .='<th style="border-top:3px solid #000;" align="left" class=" " width="12%">Cost</th>
				   <th style="border-top:3px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
				</tr>';
					foreach($items as $item){
					
					$Title = $item->Title;
					if(strlen($item->Title) < 10){
						$Title = $item->ChannelTitle;
					}
						
					  $templateHtml .='<tr style="background-color:#f2f2f2">
						   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
						   <td style="" align="left" valign="top" class="">'.$Title.'</td>
						   <td style="" valign="top">'.$item->Quantity.'</td>
						   <td style="" valign="top">'.$item->PricePerUnit.'</td>';
						   if($final_total < $lvcr_limit){
								$templateHtml .=' <td style="" valign="top">'.$item->TaxRate.'</td>';
								$templateHtml .=' <td style="" valign="top">'.$item->Tax.'</td>';
						   }
							$templateHtml .=' <td style="" valign="top">'.$item->Cost.'</td>
						   <td style="" valign="top">'.$item->CostIncTax.'</td>
						</tr>';            
		   
					}
				$templateHtml .= '</table>';
			 
			
				$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
		
				$templateHtml .= '<tr>';
				$templateHtml .= '<th style="border-top:1px solid #000;border-bottom:3px solid #000;" align="left" rowspan="4" width="60%">';		
				$templateHtml .= '<div style="border-bottom:0px;" ><b>Thank you for your purchase. Happy Shopping!<br>StoreForLife</b></div>';
				$templateHtml .= '</th>';
				$templateHtml .= '<th style="border-top:1px solid #000;" align="left" width="25%">SUB TOTAL:</th>';
				$templateHtml .= '<td style="border-top:1px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>';
				$templateHtml .= '</tr>';
		
				$templateHtml .= '<tr>';
				$templateHtml .= '<th style="" align="left" >POSTAGE(Inc VAT):</th>';
				$templateHtml .= '<td style="" align="right">'.$POSTAGE.'</td>';
				$templateHtml .= '</tr>';
				
				/*$templateHtml .= '<tr>';
				$templateHtml .= '<th style="" align="left" >TAX:</th>';
				$templateHtml .= '<td style="" align="right">'.$TAX.'</td>';
				$templateHtml .= '</tr>';*/
			 
				$templateHtml .= '<tr>';
				$templateHtml .= '<th style="border-bottom:3px solid #000;" align="left" >TOTAL INCLUDING VAT:</th>';
				$templateHtml .= '<td style="border-bottom:3px solid #000;" align="right">'.$TOTAL.'</td>';
				$templateHtml .= '</tr>';
				
				$templateHtml .= '</table>';
				$templateHtml .= '</div>';
					
				$templateHtml .= '<div class="row" style="text-align:center; border-bottom:0px;margin-left:20%; padding:0px" >';
				$templateHtml .= '<div style="width:70%; text-align:center; border:2px solid #000000; padding:10px;" >';
		
				$templateHtml .= '<table style="padding:0; text-align:center"><tr>
				<td width="15%"><img src="'.WWW_ROOT.'img/happy_g.png" width="80px"></td>
				<td width="85%" align="center"><span style="font-size:24px">Happy with your order?</span> <br>
				<span style="font-size:10px;">Please take a moment to leave us positive feedback.</span><br>
				<img src="'.WWW_ROOT.'img/5star_g.png" width="120px"><br></td>
				</tr>
				</table>';
				$templateHtml .= '<div style="text-align:left; font-size:10px;">
				If however, for any reason you are not entirely happy with your order, please contact us to allow us to rectify any issues before leaving feedback.
				</div>';
				
				$templateHtml .= '</div>';
				$templateHtml .= '</div>';
				$templateHtml .= '</div>';
				
			}
		}
		
				
		return $templateHtml;	
	}
	 
	public function getDHLSlip2($order_id = null){
	
		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdatesArchive');
		$this->loadModel('Product');	
		$this->loadModel('Skumapping');
		$this->loadModel('OpenOrdersArchive');
		
		
		$open_r = $this->OpenOrdersArchive->find('first',['conditions' => ['num_order_id' => $order_id]]);
		//$ord  = $open_r['OpenOrdersArchive'];
		$ord['NumOrderId'] = $open_r['OpenOrdersArchive']['num_order_id'];
		$ord['GeneralInfo'] = unserialize($open_r['OpenOrdersArchive']['general_info']);
		$ord['ShippingInfo'] = unserialize($open_r['OpenOrdersArchive']['shipping_info']);
		$ord['CustomerInfo'] = unserialize($open_r['OpenOrdersArchive']['customer_info']);
		$ord['TotalsInfo'] = unserialize($open_r['OpenOrdersArchive']['totals_info']);
		$ord['Items'] = unserialize($open_r['OpenOrdersArchive']['items']);
		$order =	json_decode(json_encode($ord),0);
		//$order = unserialize($open_ord->OpenOrdersArchive->general_info);
		
		
		pr($open_r);pr($order);
		echo "<br><br>";
		echo $order->GeneralInfo->SubSource;
		//print_r($order);//$order->GeneralInfo->SubSource
		exit;
	}
	public function getDHLSlip($order_id = null){
	
		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdatesArchive');
		$this->loadModel('Product');	
		$this->loadModel('Skumapping');
		$this->loadModel('OpenOrdersArchive');
 	 
		$open_r = $this->OpenOrdersArchive->find('first',['conditions' => ['num_order_id' => $order_id]]);
 		
		//$ord  = $open_r['OpenOrdersArchive'];
		$ord['NumOrderId'] 		= $open_r['OpenOrdersArchive']['num_order_id'];
		$ord['GeneralInfo']		= unserialize($open_r['OpenOrdersArchive']['general_info']);
		$ord['ShippingInfo'] 	= unserialize($open_r['OpenOrdersArchive']['shipping_info']);
		$ord['CustomerInfo'] 	= unserialize($open_r['OpenOrdersArchive']['customer_info']);
		$ord['TotalsInfo'] 		= unserialize($open_r['OpenOrdersArchive']['totals_info']);
		$ord['Items']			= unserialize($open_r['OpenOrdersArchive']['items']);
		
		$order					= json_decode(json_encode($ord),0);
		//pr($order->Items);
		//pr($order);
		//exit;
		
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  		 		
			
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		
		$EmailAddress	  = $order->CustomerInfo->Address->EmailAddress;
		//qgwn9gw0ybrk78b@marketplace.amazon.com
		//dbjggbqmqzd8tw7@marketplace.amazon.co.uk
		//zyp9sfsnj55pyqz@marketplace.amazon.ca
		$exp = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = $exp[$index];
		$storeName = $this->getStoreData($SubSource, $store_country);
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){}	
		if($SHIPPING_ADDRESS == ''){
			
			$SHIPPING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName)).'<br>';
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
						$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 		
			
 			
 			if(in_array($store_country,['es','it'])){ 
 				if($order->CustomerInfo->Address->Region !=''){
					$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).', ';
				}	
				
				if($order->CustomerInfo->Address->PostCode !=''){
					$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).' ';
				} 
				
				if($order->CustomerInfo->Address->Town !=''){
					$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode )).' ';
				} 
				if($order->CustomerInfo->Address->Country != 'UNKNOWN'){				
					$SHIPPING_ADDRESS .= '<br>'.$order->CustomerInfo->Address->Country;
				}
			}else if(in_array($store_country,['de','fr']) ){
				
				if($order->CustomerInfo->Address->Town !=''){
					$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode )).' ';
				} 
				if($order->CustomerInfo->Address->PostCode !=''){
					$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).' ';
				} 
				if($order->CustomerInfo->Address->Region !=''){
					$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).' ';
				}	
				if($order->CustomerInfo->Address->Country != 'UNKNOWN'){					
					$SHIPPING_ADDRESS .= '<br>'.$order->CustomerInfo->Address->Country;
				}
			}else{
 				 
				if($order->CustomerInfo->Address->Town !=''){
					$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).', ';
				} 
				if($order->CustomerInfo->Address->PostCode !=''){
					$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
				} 
				if($order->CustomerInfo->Address->Region !=''){
					$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
				}	
				if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
					$SHIPPING_ADDRESS .= $order->CustomerInfo->Address->Country ? $order->CustomerInfo->Address->Country : '';
				}
			}			
		}		
		$trans = array();
		if($store_country == 'it'){ 
			$trans = array( "Germany"=>"Germania","Andorra"=>"Andorra","Austria"=>"Austria","Belgium"=>"Belgio","Cyprus"=>"Cipro","Estonia"=>"Estonia","Finland"=>"Finlandia","France"=>"Francia","Greece"=>"Grecia","Ireland"=>"Irlanda","Italy"=>"Italia","Kosovo"=>"Kosovo","Latvia"=>"Lettonia","Lithuania"=>"Lituania","Luxembourg"=>"Lussemburgo","Malta"=>"Malta","Monaco"=>"Monaco","Montenegro"=>"Montenegro","Netherlands"=>"Paesi Bassi","Portugal"=>"Portogallo","San Marino"=>"San Marino","Slovakia"=>"Slovacchia","Slovenia"=>"Slovenia","Spain"=>"Spagna","Vatican City"=>"Città del Vaticano","United Kingdom"=>"Regno Unito","Switzerland"=>"Svizzera","Liechtenstein"=>"Liechtenstein","Sweden"=>"Svezia","Iceland"=>"Islanda","Denmark"=>"Danimarca","Norway"=>"Norvegia","Croatia"=>"Croazia","Czech Republic"=>"Repubblica Ceca","Romania"=>"Romania ","Moldova"=>"Moldavia","Albania"=>"Albania","Bulgaria"=>"Bulgaria","Serbia"=>"Serbia","Macedonia"=>"Macedonia","Russia"=>"Russia ","Belarus"=>"Bielorussia","Turkey"=>"Turchia","Georgia"=>"Georgia","Hungary"=>"Ungheria","Poland"=>"Polonia","Ukraine"=>"Ucraina","Bosnia and Herzegovina"=>"Bosnia ed Erzegovina");			 
		}
		else if($store_country == 'es'){ 
			$trans = array("Germany"=>"Alemania","Andorra"=>"Andorra","Austria"=>"Austria ","Belgium"=>"Bélgica","Cyprus"=>"Chipre","Estonia"=>"Estonia","Finland"=>"Finlandia","France"=>"Francia","Greece"=>"Grecia","Ireland"=>"Irlanda","Italy"=>"Italia","Kosovo"=>"Kosovo","Latvia"=>"Letonia","Lithuania"=>"Lituania","Luxembourg"=>"Luxemburgo","Malta"=>"Malta","Monaco"=>"Mónaco","Montenegro"=>"Montenegro","Netherlands"=>"Países Bajos","Portugal"=>"Portugal","San Marino"=>"San Marino","Slovakia"=>"Eslovaquia","Slovenia"=>"Eslovenia","Spain"=>"España","Vatican City"=>"Ciudad del Vaticano","United Kingdom"=>"Reino Unido","Switzerland"=>"Suiza","Liechtenstein"=>"Liechtenstein","Sweden"=>"Suecia","Iceland"=>"Islandia","Denmark"=>"Dinamarca","Norway"=>"Noruega","Croatia"=>"Croacia","Czech Republic"=>"República Checa","Romania"=>"Rumania","Moldova"=>"Moldavia","Albania"=>"Albania","Bulgaria"=>"Bulgaria","Serbia"=>"Serbia","Macedonia"=>"Macedonia","Russia"=>"Rusia","Belarus"=>"Bielorrusia","Turkey"=>"Turquía","Georgia"=>"Georgia","Hungary"=>"Hungría","Poland"=>"Polonia","Ukraine"=>"Ucrania","Bosnia and Herzegovina"=>"Bosnia-Herzegovina");			
		}
		else if($store_country == 'de'){ 
			$trans = array("Germany"=>"Deutschland","Andorra"=>"Andorra","Austria"=>"Österreich","Belgium"=>"Belgien","Cyprus"=>"Zyperm","Estonia"=>"Estland","Finland"=>"Finnland","France"=>"Frankreich","Greece"=>"Griechenland","Ireland"=>"Irland","Italy"=>"Italien","Kosovo"=>"kosovo","Latvia"=>"Lettland","Lithuania"=>"Litauen","Luxembourg"=>"Luxemburg","Malta"=>"Malta","Monaco"=>"Monaco","Montenegro"=>"Montenegro","Netherlands"=>"Niederlande","Portugal"=>"Portugal","San Marino"=>"San Marino","Slovakia"=>"Slowakei","Slovenia"=>"Slowenien","Spain"=>"Spanien","Vatican City"=>"Vatikanstadt","United Kingdom"=>"Vereinigtes Königreich","Switzerland"=>"Schweiz","Liechtenstein"=>"Liechtenstein","Sweden"=>"Schweden","Iceland"=>"Island","Denmark"=>"Dänemark","Norway"=>"Norwegen","Croatia"=>"Kroatien","Czech Republic"=>"Tschechien","Romania"=>"Rumänien","Moldova"=>"Moldawien","Albania"=>"Albanien","Bulgaria"=>"Bulgarien","Serbia"=>"Serbien","Macedonia"=>"Mazedonien","Russia"=>"Russland","Belarus"=>"Weißrussland","Turkey"=>"truthahn","Georgia"=>"Georgia","Hungary"=>"Ungarn","Poland"=>"Polen","Ukraine"=>"Ukraine","Bosnia and Herzegovina"=>"Bosnien und Herzegowina");
		}else if($store_country == 'fr'){ 
			$trans = array("Germany"=>"Allemagne","Andorra"=>"Andorre","Austria"=>"Autriche","Belgium"=>"Belgique","Cyprus"=>"Chypre","Estonia"=>"Estonie","Finland"=>"Finlande","France"=>"France","Germany"=>"","Greece"=>"Grèce","Ireland"=>"Irlande","Italy"=>"Italie","Kosovo"=>"Kosovo","Latvia"=>"Lettonie","Lithuania"=>"Lituanie","Luxembourg"=>"Luxembourg","Malta"=>"Malte","Monaco"=>"Monaco","Montenegro"=>"Monténégro","Netherlands"=>"Pays-Bas","Portugal"=>"Portugal","San Marino"=>"Saint-Marin","Slovakia"=>"Slovaquie","Slovenia"=>"Slovénie","Spain"=>"Espagne","Vatican City"=>"Vatican","United Kingdom"=>"Royaume-uni","Switzerland"=>"Suisse","Liechtenstein"=>"Liechtenstein","Sweden"=>"Suède","Iceland"=>"Islande","Denmark"=>"Danemark","Norway"=>"Norvège","Croatia"=>"Croatie","Czech Republic"=>"République tchèque","Romania"=>"Roumanie","Moldova"=>"Moldavie","Albania"=>"Albanie","Bulgaria"=>"Bulgarie","Serbia"=>"Serbie","Macedonia"=>"Macédoine","Russia"=>"Russie","Belarus"=>"Biélorussie","Turkey"=>"Turquie","Georgia"=>"Géorgie","Hungary"=>"Hongrie","Poland"=>"Pologne","Ukraine"=>"Ukraine","Bosnia and Herzegovina"=>"Bosnie-Herzégovine");			
		}
			
		if(count($trans) > 0){
			$SHIPPING_ADDRESS  = utf8_decode(strtr($SHIPPING_ADDRESS, $trans));		
		}
 		 
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 		
		$ReferenceNum     = $order->GeneralInfo->ReferenceNum;
		$PostalServiceName= $order->ShippingInfo->PostalServiceName ;
		$BuyerName		  = $order->CustomerInfo->ChannelBuyerName;
	
		
		//pr($exp);
		//pr( $order); 	
		$INVOICE_DATE	  = date('Y-M-d', strtotime($order->GeneralInfo->ReceivedDate));
		//$order_date	  	  = date('D, M d, Y', strtotime($order->GeneralInfo->ReceivedDate));
		$templateHtml 	  = NULL;
		
	  	$lvcr_limit = 17.99; //gbp
		
		$final_total = $TOTAL ;
		if($Currency == 'GBP'){
			$final_total = $TOTAL * (1.122);
		}
		if($final_total < $lvcr_limit){
			$POSTAGE = 4;
		}
		$ShipmentWeight  = 0.0;
		$swresult = $this->MergeUpdatesArchive->find('first',['conditions' => ['order_id' => $OrderId],'fields'=>['packet_weight','envelope_weight']]);
		if(count( $swresult ) > 0){				
			$ShipmentWeight = $swresult['MergeUpdatesArchive']['packet_weight'] + $swresult['MergeUpdatesArchive']['envelope_weight'];
		}
		$country_code = $this->getCountryCode($Country);
		$tax_rate = ''; $vat_amount = 0;
		$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
		if(count( $thsresult ) > 0){				
			$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
		}
		
		$currency_code = $this->getCurrencyCode($Currency);
		
		$TXT_SHIP_TO = 'Ship To';
		$TXT_ORDER_ID = 'Order ID';
		$TXT_THANKS = 'Thank you for buying from '.$storeName.' on Amazon Marketplace.';
		$TXT_SHIP_ADDRESS = 'Shipping Address';		
		$TXT_ORDER_DATE = 'Order Date';
		$TXT_SHIP_SERVICE = 'Shipping Service';
		$TXT_BUYER_NAME = 'Buyer Name';
		$TXT_SELLER_NAME = 'Seller Name'; 
		
		$TXT_QUANTITY = 'Quantity';
		$TXT_PRODUCT_DETAILS = 'Product Details';
		$TXT_UNIT_PRICE = 'Unit price';
		$TXT_ORDER_TOTALS = 'Order Totals';
		
		$TXT_SKU = 'SKU';
		$TXT_ASIN = 'ASIN';
		$TXT_CONDITION = 'Condition';
		$TXT_LISTING_ID = 'Listing ID';
		$TXT_ORDER_ITEM_ID = 'Order Item ID';
 		$condition = 'New';
		
		$TXT_ITEM_SUB_TOTAL = 'Item subtotal';
		$TXT_ITEM_TOTAL = 'Item total';
		$TXT_GRAND_TOTAL = 'Grand total';
		$TXT_ITEM_SHIPPING ='Shipping total';
		
		$RET_MESSAGE  = ' <tr>
			  <td style="padding:5px;">
				 <strong>Returning your item:</strong>
				 <p>Go to "Your Account" on Amazon.com, click "Your Orders" and then click the "seller profile" link for this order to get
					information about the return and refund policies that apply.</br>
					Visit <a href="https://www.amazon.com/returns" style="text-decoration:none;">https://www.amazon.com/returns</a> to print a return shipping label. Please have your order ID ready.
				 </p>
			  </td>
		   </tr>';
		$RET_MESSAGE .= ' <tr>
			  <td style="padding:5px;">
				 <p><strong>Thanks for buying on Amazon Marketplace.</strong> To provide feedback for the seller please visit <a href="http://www.amazon.com/feedback"  style="text-decoration:none;">www.amazon.com/feedback</a>. To
					contact the seller, go to Your Orders in Your Account. Click the seller\'s name under the appropriate product. Then, in the
					"Further Information" section, click "Contact the Seller."
				 </p>
			  </td>
		   </tr>';
		    
		if($store_country == 'uk'){
 
			$RET_MESSAGE = ' <tr>
			  <td style="padding:5px;">
				 <p><strong>Thanks for buying on Amazon Marketplace.</strong> To provide feedback for the seller please visit <a href="http://www.amazon.co.uk/feedback"  style="text-decoration:none;">www.amazon.co.uk/feedback</a>. To
					contact the seller, go to Your Orders in Your Account. Click the seller\'s name under the appropriate product. Then, in the
					"Further Information" section, click "Contact the Seller."
				 </p>
			  </td>
		   </tr>';
		}else if($store_country == 'ca'){

			$RET_MESSAGE = ' <tr>
			  <td style="padding:5px;">
				 <p><strong>Thanks for buying on Amazon Marketplace.</strong> To provide feedback for the seller please visit <a href="http://www.amazon.ca/feedback"  style="text-decoration:none;">www.amazon.ca/feedback</a>. To
					contact the seller, go to Your Orders in Your Account. Click the seller\'s name under the appropriate product. Then, in the
					"Further Information" section, click "Contact the Seller."
				 </p>
			  </td>
		   </tr>';
		}
		 
		$order_date 	= date('D, M d, Y', strtotime($order->GeneralInfo->ReceivedDate));
		$english_days   = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');			 
		$english_months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
  		 						
		if($store_country == 'fr'){
			$TXT_SHIP_TO = utf8_decode('Adresse d\'expédition');
			$TXT_ORDER_ID = utf8_decode('Numéro de la commande');
			$TXT_THANKS = 'Merci pour votre achat avec le vendeur '.$storeName.' sur Amazon.fr';
			$TXT_SHIP_ADDRESS = utf8_decode('Adresse d\'expédition');
			$TXT_ORDER_DATE = 'Date de commande';
			$TXT_SHIP_SERVICE = 'Service de livraison';
			$TXT_BUYER_NAME = 'Nom de l\'acheteur';
			$TXT_SELLER_NAME = 'Nom du vendeur';
			
			$TXT_QUANTITY =  utf8_decode('Quantité');
			$TXT_PRODUCT_DETAILS = utf8_decode('Détails de l\'article');
			$TXT_UNIT_PRICE = utf8_decode('Prix de l\'unité');
			$TXT_ORDER_TOTALS = 'Total des commandes';
			
			$TXT_SKU = 'SKU';
			$TXT_ASIN = 'ASIN';
			$TXT_CONDITION = utf8_decode('État');
			$TXT_LISTING_ID = utf8_decode('ID de l\'offre');
			$TXT_ORDER_ITEM_ID = utf8_decode('ID de l\'article commandé');
			$condition = 'Neuf';
			
			$TXT_ITEM_SUB_TOTAL = 'Sous-total de l\'article';
			$TXT_ITEM_SHIPPING = utf8_decode('Total de l\'expédition');
			$TXT_ITEM_TOTAL = 'Total de l\'article';
			$TXT_GRAND_TOTAL = 'Total';
			 
 			$_order_date = date('D. j M Y', strtotime($order->GeneralInfo->ReceivedDate));		
			$french_days = array('lun', 'mar', 'mer', 'jeu', 'ven', 'sam', 'dim');			 
			$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
			$order_date = str_replace($english_months, $french_months, str_replace($english_days, $french_days, $_order_date ) );
			
 	  
			$RET_MESSAGE  = utf8_decode(' <tr>
						  <td style="padding:5px;">							 
							 <p><strong>Merci de votre achat sur Amazon Marketplace.</strong>Pour évaluer votre vendeur, veuillez vous rendre sur <a href="https://www.amazon.fr/feedback" style="text-decoration:none;">www.amazon.fr/feedback</a>.	Pour contacter votre vendeur, veuillez cliquez sur « Votre compte » puis « Vos commandes ».
Localisez la commande puis cliquez sur « Contactez le vendeur » à droite du produit.</p>
						  </td>
					   </tr>');
					   
 		}else if($store_country == 'es'){
			$TXT_SHIP_TO = 'Enviar a';
			$TXT_ORDER_ID =  ('Nº de pedido');
			$TXT_THANKS = 'Gracias por comprar en '.$storeName.' en Marketplace de Amazon.';
			$TXT_SHIP_ADDRESS = utf8_decode('Dirección de envío');
			$TXT_ORDER_DATE = 'Fecha del pedido';
			$TXT_SHIP_SERVICE = 'Servicio de envío';
			$TXT_BUYER_NAME = 'Nombre del comprador';
			$TXT_SELLER_NAME = 'Nombre del vendedor';
			
			$TXT_QUANTITY =  utf8_decode('Cantidad');
			$TXT_PRODUCT_DETAILS = utf8_decode('Detalles del producto');
			$TXT_UNIT_PRICE = utf8_decode('Detalles del producto');
			$TXT_ORDER_TOTALS = 'Totales del pedido';
			
			$TXT_SKU = 'SKU';
			$TXT_ASIN = 'ASIN';
			$TXT_CONDITION = utf8_decode('Estado');
			$TXT_LISTING_ID = utf8_decode('Identificador del listing');
			$TXT_ORDER_ITEM_ID = utf8_decode('N.º de pedido');
			$condition = 'Nuevo';
			
			$TXT_ITEM_SUB_TOTAL = 'Subtotal del artículo';
			$TXT_ITEM_SHIPPING = utf8_decode('Total del envío');
			$TXT_ITEM_TOTAL = 'Total de l\'article';
			$TXT_GRAND_TOTAL = 'Suma total';
			
			$_order_date = date('D, j M Y', strtotime($order->GeneralInfo->ReceivedDate));				 
			$french_days = array('lun.', 'mar.', 'mié.', 'jue.', 'vie.', 'sáb.', 'dom.');	
			$french_months = array('enero', 'feb.', 'marzo', 'abr.', 'may.', 'jun.', 'jul.', 'agosto', 'sept.', 'oct.', 'nov.', 'dic.');
			$order_date = str_replace($english_months, $french_months, str_replace($english_days, $french_days, $_order_date ) );
		
 			$RET_MESSAGE  = ' <tr>
						  <td style="padding:5px;">
							<p> <strong>Gracias por comprar en Amazon.</strong> Para enviar tus comentarios al vendedor, visita <a href="https://www.amazon.es/feedback" style="text-decoration:none;">www.amazon.es/feedback</a></p>
							<p>Si quieres ponerte en contacto con el vendedor, accede a "Mi cuenta" (en la parte superior derecha de cualquier página de
Amazon) y haz clic en "Mis pedidos". Localiza el pedido y haz clic en "Contactar con el vendedor".</p>
						  </td>
					   </tr>';
 		}else if($store_country == 'it'){
			
			$TXT_SHIP_TO = 'Spedire a';
			$TXT_ORDER_ID = 'Numero dell\'ordine';
			$TXT_THANKS = 'Ti ringraziamo per aver acquistato da '.$storeName.' su Amazon.';
			$TXT_SHIP_ADDRESS = 'Indirizzo di spedizione';		
			$TXT_ORDER_DATE = 'Data ordine';
			$TXT_SHIP_SERVICE = 'Tipo di spedizione';
			$TXT_BUYER_NAME = 'Nome acquirente';
			$TXT_SELLER_NAME = 'Nome venditore'; 
			
			$TXT_QUANTITY = 'Quantità';
			$TXT_PRODUCT_DETAILS = 'Dettagli prodotto';
			$TXT_UNIT_PRICE = 'Prezzo unitario';
			$TXT_ORDER_TOTALS = 'Totale ordine';
			
			$TXT_SKU = 'SKU';
			$TXT_ASIN = 'ASIN';
			$TXT_CONDITION = 'Condizione';
			$TXT_LISTING_ID = 'Numero offerta';
			$TXT_ORDER_ITEM_ID = 'N. prodotto';
			$condition = 'Nuovo';
			
			$TXT_ITEM_SUB_TOTAL = 'Subtotale articoli';
			$TXT_ITEM_TOTAL = 'Tot. articolo';
			$TXT_GRAND_TOTAL = 'Tot.';
			
			$_order_date = date('D. M d Y', strtotime($order->GeneralInfo->ReceivedDate));		
			$it_days = array('lun', 'mar', 'mer', 'jeu', 'ven', 'sam', 'dim');  
			$it_months = array('genn.', 'febbr.', 'mar.', 'apr.', 'giugno', 'luglio', 'ag.', 'sett.', 'ott.', 'octobre', 'nov.', 'dic');
			$order_date = str_replace($english_months, $it_months, str_replace($english_days, $it_days, $_order_date ) );
 			
			$RET_MESSAGE  = ' <tr>
						  <td style="padding:5px;">
							 <strong>Grazie per aver comprato nel Marketplace di Amazon.</strong>
							 <p>Per fornire il tuo feedback sul venditore, visita la pagina seguente:
<a href="https://www.amazon.it/feedback" style="text-decoration:none;">www.amazon.it/feedback</a>. Se desideri contattare il venditore, seleziona il link "Il mio account", che trovi in alto a destra su
ogni pagina di Amazon.it, e accedi alla sezione "I miei ordini". Individua l\'ordine in questione e clicca su "Contatta il
venditore"</p>
						  </td>
					   </tr>';
		
 		}else if($store_country == 'de'){
		//302-0756271-6349905 1737290 
			$TXT_SHIP_TO = 'Liefern an';
			$TXT_ORDER_ID =  ('Bestellnummer');
			$TXT_THANKS =  utf8_decode('Vielen Dank für Ihren Einkauf bei '.$storeName.' auf Amazon.de Marketplace.');
			$TXT_SHIP_ADDRESS = ('Lieferanschrift');
			$TXT_ORDER_DATE = 'Bestellt am';
			$TXT_SHIP_SERVICE = 'Versandart';
			$TXT_BUYER_NAME = utf8_decode('Name des Käufers');
			$TXT_SELLER_NAME = utf8_decode('Name des Verkäufers');
			
			$TXT_QUANTITY =  ('Menge');
			$TXT_PRODUCT_DETAILS = ('Produktdetails');
			$TXT_UNIT_PRICE =  ('Preis pro Einheit');
			$TXT_ORDER_TOTALS = 'Gesamtbestellsumme';
			
			$TXT_SKU = 'SKU';
			$TXT_ASIN = 'ASIN';
			$TXT_CONDITION = ('Zustand');
			$TXT_LISTING_ID = ('Angebotsnummer');
			$TXT_ORDER_ITEM_ID = ('Bestellposten-ID');
			$condition = 'Neu';
			
			$TXT_ITEM_SUB_TOTAL = 'Zwischensumme Artikel';
			$TXT_ITEM_SHIPPING = utf8_decode('Gesamtbetrag Versand');
			$TXT_ITEM_TOTAL = 'Gesamtbetrag Artikel';
			$TXT_GRAND_TOTAL = 'Gesamtbetrag';
			
			$_order_date = date('D. M d Y', strtotime($order->GeneralInfo->ReceivedDate));		
			$de_days = array('Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So');				 
			$de_months = array('Jan.', 'Feb.', 'März', 'Apr.', 'Mai', 'Juni', 'Juli', 'Aug.', 'Sept.', 'Okt.', 'Nov.', 'Dez.');
			$order_date = str_replace($english_months, $de_months, str_replace($english_days, $de_days, $_order_date ) );
		 
 	 		$RET_MESSAGE  = utf8_decode(' <tr>
						  <td style="padding:5px;">
							 <p> <strong>Vielen Dank für Ihren Einkauf bei Amazon Marketplace!</strong> Unter <a href="https://www.amazon.de/feedback" style="text-decoration:none;">www.amazon.de/feedback</a> können Sie Ihre Bewertung für diesen Verkäufer abgeben. Wenn Sie den Verkäufer kontaktieren möchten, gehen Sie über "Mein Konto" auf "Meine
Bestellungen". Suchen Sie die Bestellung und klicken Sie dort bitte auf die Schaltfläche "Verkäufer kontaktieren" um zum
Kontaktformular zu gelangen.
							 </p>
						  </td>
					   </tr>');
					 
 		}
		
		$html = '<!DOCTYPE HTML>';
		$html .= '<html lang="en-US">';
		   $html .= '<head>';
			  $html .= '<meta charset="UTF-8">';
			  $html .= '<title>Pay Slip</title>';
		  $html .= ' </head>';
		   $html .= '<body>
			  <div style="width:660px; height:auto; margin:20px auto;">
				 <table width="100%" style="border-bottom:2px dashed #000;font-family:arial;">
					<tbody>
					   <tr>';
						   $html .= '<td width="100%" value="top" align="left" style="padding:5px;">
							 <div style="font-size:11px;">'.$TXT_SHIP_TO.':</div>
							 <strong>'.$SHIPPING_ADDRESS.'</strong>
						  </td>';
					    $html .= '</tr>
					</tbody>
				 </table>';
				 $html .= '<table width="100%">
					<tbody>
					   <tr>';
						  $html .= '<td width="100%" valign="top" align="left" style="padding:5px;">
							 <strong>'.$TXT_ORDER_ID.': '.$ReferenceNum.'.</strong><br>
							 <div style="font-family:arial;font-size: 12px;">'.$TXT_THANKS.'</div>';
						  $html .= '</td>
					   </tr>
					</tbody>
				 </table>';
				 $html .= '<table width="100%" style="border:1px solid #000;font-family:arial;font-size: 12px;">
					<tbody>
					   <tr>';
						  $html .= '<td width="35%" valign="top" align="left" style="padding:5px;">
 							  <strong>'.$TXT_SHIP_ADDRESS.':</strong> <br>
							  	'.$SHIPPING_ADDRESS.'							  
						  </td>';
						 $html .= ' <td width="25%" valign="top" align="left" style="padding:5px;">							  
								'.$TXT_ORDER_DATE.':<br>
								'.$TXT_SHIP_SERVICE.':<br>
								'.$TXT_BUYER_NAME.':<br>
								'.$TXT_SELLER_NAME.':
 						  </td>';
						 
						 $html .= '<td width="25%" valign="top" align="left" style="padding:5px;">
							    '.$order_date.'<br>
								'.$PostalServiceName.'<br>
								'.$BuyerName.'<br>
								'.$storeName.'
							  
						  </td>';
						 $html .= ' <td width="25%" valign="top" align="left">   
						  </td>';
					   $html .= '</tr>
					</tbody>
				 </table>';
				 $html .= '<table width="100%" border="1" style="margin-top:10px; margin-bottom:2px; border-collapse: collapse;font-family:arial;font-size: 12px;">
					<tbody>';
				 $html .= '<tr>
						  <th style="padding:2px;" align="left">'.$TXT_QUANTITY.'</th>
						  <th style="padding:2px;" align="left">'.$TXT_PRODUCT_DETAILS.'</th>
						  <th style="padding:2px;" align="left">'.$TXT_UNIT_PRICE.'</th>
						  <th style="padding:2px;">'.$TXT_ORDER_TOTALS.'</th>
					   </tr>';					  		 
				$total = 0;
				
				foreach($items as $count => $item){ 
					
					$Title = $item->Title;
					if(strlen($item->Title) < 10){
						$Title = $item->ChannelTitle;
					}
					$asin = 'B00BPPPV8U';
					$asin_data = $this->Skumapping->find('first', array('conditions' => array('Skumapping.channel_sku' => $item->ChannelSKU),'fields'=>['asin']));
					if(count($asin_data) > 0){
						$asin = $asin_data['Skumapping']['asin'];
					}
					$ListingId = $this->getListingId($SubSource,$item->ChannelSKU);
					
					
 					$html .= '<tr>';
					$html .= '<td width="10%" valign="top" align="center" style="padding:2px;">  
							 <strong>'.$item->Quantity.'</strong>
						  </td>';
						  
					$html .= '<td width="40%" valign="top" align="left" style="padding:2px;">
						 <strong>'. $Title.'</strong>
						 <ul style="list-style:none; margin-left:0px; padding-left:0px;">
							<li><strong>'.$TXT_SKU.':</strong> '.$item->ChannelSKU.'</li>
							<li><strong>'.$TXT_ASIN.':</strong> '.$asin.'</li>
							<li><strong>'.$TXT_CONDITION.':</strong> '.$condition.'</li>
							<li><strong>'.$TXT_LISTING_ID.':</strong> '.$ListingId.'</li>
							<li><strong>'.$TXT_ORDER_ITEM_ID.':</strong>'.$item->ItemNumber.'</li>
						 </ul>
					  </td>'; 					  
					    
 		 
					$html .= '<td width="15%" valign="top" align="left" align="center" style="padding:5px;"> '; 
					if( $Currency == 'EUR'){
						$html .= '<strong> '. number_format($item->PricePerUnit,2,',','').' '.$currency_code.' </strong>';
					}else{
						$html .= '<strong> '.$currency_code.$item->PricePerUnit.' </strong>';
					}
							 
					$html .= ' </td>';
					$html .= '<td width="35%" style="padding-left:5px;padding-right:5px;" valign="top">
					<table width="100%" cellpadding="0" cellspacing="0" style="font-weight:bold;padding-top:10px;">';
					
					$bor = 'border-bottom:1px solid #ccc;';
					if($order->TotalsInfo->PostageCost > 0){
						$bor = '';
					}
					
					if( $Currency == 'EUR'){
						$html .= '<tr><td style="'.$bor.'">'.$TXT_ITEM_SUB_TOTAL.'</td><td align="right" style="'.$bor.'">'.number_format($order->TotalsInfo->Subtotal,2,',','').' '.$currency_code.'</td></tr>';
					}else{
						$html .= '<tr><td style="'.$bor.'">'.$TXT_ITEM_SUB_TOTAL.'</td><td align="right" style="'.$bor.'">'.$currency_code.$order->TotalsInfo->Subtotal.'</td></tr>';
					}
					
 					if($order->TotalsInfo->PostageCost > 0){
						if( $Currency == 'EUR'){
							$html .= '<tr><td style="border-bottom:1px solid #ccc;padding-top:5px;">'.$TXT_ITEM_SHIPPING.'</td><td align="right" style="border-bottom:1px solid #ccc;padding-top:5px;">'.number_format($order->TotalsInfo->PostageCost,2,',','').' '.$currency_code.'</td></tr>';
 						}else{
							$html .= '<tr><td style="border-bottom:1px solid #ccc;padding-top:5px;">'.$TXT_ITEM_SHIPPING.'</td><td align="right" style="border-bottom:1px solid #ccc;padding-top:5px;">'.$currency_code.$order->TotalsInfo->PostageCost.'</td></tr>';
						}
					
					}
					
					if( $Currency == 'EUR'){
						$html .= '<tr><td style="border-bottom:1px solid #ccc;padding-top:5px;">'.$TXT_ITEM_TOTAL.'</td><td align="right" style="border-bottom:1px solid #ccc;padding-top:5px;">'.number_format($order->TotalsInfo->TotalCharge,2,',','').' '.$currency_code.'</td></tr>';
					}else{
						$html .= '<tr><td style="border-bottom:1px solid #ccc;padding-top:5px;">'.$TXT_ITEM_TOTAL.'</td><td align="right" style="border-bottom:1px solid #ccc;padding-top:5px;">'.$currency_code.$order->TotalsInfo->TotalCharge.'</td></tr>';
					}
					
					$html .= '</table>'; 
							
					$html .= '</td>';
					$html .= '</tr>';
						  
				 $total  = $order->TotalsInfo->TotalCharge;
        
			}
  		
				$html .= '	</tbody>
				 </table>';
				$html .= ' <table width="100%">';
				$html .= '	<tbody>';
				$html .= '	   <tr>';
				$html .= '		  <td width="60%"> </td>';
				if( $Currency == 'EUR'){
					$html .= '<td align="right" valign="top" width="40%" style="font-family:arial;font-size:12px;"> <strong>'.$TXT_GRAND_TOTAL.': '.number_format($total,2,',','').' '.$currency_code.'</strong> </td>';
				}else{
					$html .= '<td align="right" valign="top" width="40%" style="font-family:arial;font-size:12px;"> <strong>'.$TXT_GRAND_TOTAL.': '.$currency_code.$total.'</strong> </td>';
				}
				$html .= '	   </tr>';
				$html .= '	</tbody>';
				$html .= '</table>';
				$html .= ' <table style="border-bottom:2px dashed #000;font-family:arial;font-size: 12px;">';
				$html .= '	<tbody>';
				
				$html .= $RET_MESSAGE;
				
				$html .= ' 
					</tbody>
				 </table>
			  </div>
		   </body>
		</html>';
		//echo $html; 
		//// <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		
		$_html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			';
	 	$_html .= '<body>'.$html.'</body>';
	 		
		$name	= $OrderId.'.pdf';							
		$dompdf->load_html(utf8_encode($_html), Configure::read('App.encoding'));
		$dompdf->render();			
						
		$file_to_save = WWW_ROOT .'img/dhl/invoice_amz_'.$name;		 
		//save the pdf file on the server
		file_put_contents($file_to_save, $dompdf->output()); 
		copy($file_to_save,WWW_ROOT .'img/dhl/invoice_amz_'.$OrderId.'-1.pdf'); 
		@unlink($file_to_save);
		//exit;
	}
	public function sendEmail( $data = array() ){ 
 			 
			//$to = $data['to'];	
			$from_email = $data['from_email'];
			$to = 'avadhesh.kumar@jijgroup.com'; 
			 
			$_path =  WWW_ROOT .'b2b_invoices/'. $data['file'];   
	   
			App::uses('CakeEmail', 'Network/Email');
			$email = new CakeEmail('');
			$email->from($from_email);	
			$email->to( array('avadhesh.kumar@jijgroup.com'));
			//$email->to( array('avadhesh.kumar@jijgroup.com','pappu.k@euracogroup.co.uk'));
			$email->subject($data['subject']);
			$email->attachments(array(
				    $data['file'] => array(
					'file' => $_path,
					'mimetype' => 'application/pdf',
					'contentId' => 'Euraco'
				)
			));
			
			$email->send( (utf8_decode($data['message'])));
		
			return $from_email;
				
			 
	}
	
	public function sendB2bEmail() {
		
		$this->layout = '';
		$this->autoRender = false;		
		$this->loadModel('OpenOrdersArchive');	
		$this->loadModel('B2bCustomer'); 
		$amazon_order_id = $this->data['amz_order_id'];
 		 	
		$data = $this->OpenOrdersArchive->find('first', array('conditions' => array('amazon_order_id' => $amazon_order_id ),'fields' =>['sub_source','destination','num_order_id','customer_info','amazon_order_id','open_order_date','status','process_date'] ) );

		 
 		$num_order_id 	= $data['OpenOrdersArchive']['num_order_id'];				
		$sub_source 	= strtolower($data['OpenOrdersArchive']['sub_source']);
		$customer_info  = unserialize($data['OpenOrdersArchive']['customer_info']);
		$name		 	= ucwords( strtolower($customer_info->ChannelBuyerName)); 
		$order			= $amazon_order_id	;
		
		$this->getB2BInvoice($amazon_order_id);		
		
		$exp = explode(".",$customer_info->Address->EmailAddress);
		$index = count($exp) - 1;
		$store_country = strtoupper($exp[$index]);
			
		$msgArray	 = $this->getB2bShipmentTemplate($sub_source, $name,$store_country);
		$result		 = strtolower(substr($sub_source, 0, 4));
		
		if($result == 'cost'){
			$from_email = array('sales@cost-dropper.com' => 'Cost-Dropper');
		}else if($result == 'rain'){
			$from_email =  array('crownstationery@gmail.com' => 'RRRetail');
		}else if($result == 'mare'){
			$from_email = array('salesteamexpress@gmail.com' => 'EbuyerExpress');
		}
		
		$data = array();
		$data['from_email'] = $from_email;
		$data['sub_source'] = $sub_source;
		$data['subject']  	= $msgArray['subject'];
		$data['message']  	= $msgArray['msg'];
		$data['sub_source'] = $sub_source;
		$data['file'] 	    = $amazon_order_id.'.pdf';
		$data['to'] 		= $customer_info->Address->EmailAddress;
		 
	//	$from_email = $this->sendEmail($data);
		
		/*----------------Update Email Status---------------*/
		$conditions = array('B2bCustomer.amazon_order_id' => $amazon_order_id);	 
		$send_date  = date('Y-m-d H:i:s');
		$GeneralData['email_send_date']	 = "'".$send_date."'";		
		//$this->B2bCustomer->updateAll( $GeneralData, $conditions );
		/*--------------------End------------------------*/
		$msg['email_send_date'] = $send_date;
 		echo json_encode($msg); 
		exit;
	}
	
	public function getB2bShipmentTemplate($source = null, $name = null,$store_country = null){ 
  	
		$message 		= array();
		$result			= strtolower(substr($source, 0, 4));
	 
		if($result == 'cost'){
		
			if($store_country == 'DE'){
				$message = array('subject'=> "Einkaufsrechnung!",
								 'msg'=>utf8_encode("Lieber ".$name.",\n\nVielen Dank für den Kauf bei uns!\nBitte finden Sie eine beigefügte Kopie Ihrer Rechnung, wenn Sie weitere Hilfe benötigen, teilen Sie uns dies bitte mit.\n\ngrüße\nJake"));	
								 						 
			}elseif($store_country == 'ES'){
				$message = array('subject'=> "Factura de compra!",
								 'msg'=>utf8_encode("Estimado ".$name.",\n\nGracias por comprar de nosotros!\nPor favor, encuentre una copia adjunta de su factura, si necesita más ayuda por favor háganoslo saber.\n\nsaludos\nJake"));	
			}elseif($store_country == 'FR'){
				$message = array('subject'=> "Facture d'achat!",
								 'msg'=>utf8_encode("Cher ".$name.",\n\nMerci d'avoir acheté de nous!\nS'il vous plaît trouver une copie ci-jointe de votre facture, si vous avez besoin d'aide supplémentaire s'il vous plaît laissez-nous savoir.\n\namitiés\nJake"));	
			}elseif($store_country == 'IT'){
				$message = array('subject'=> "Fattura di acquisto!",
								 'msg'=>utf8_encode("Caro ".$name.",\n\nGrazie per l'acquisto da noi!\nSi prega di trovare la copia allegata della fattura, se avete bisogno di ulteriore assistenza si prega di farcelo sapere.\n\nSaluti\nJake"));	
			}else{
				$message = array('subject'=> "Purchase invoice!",
								 'msg'=>utf8_encode("Dear ".$name.",\n\nThank you for purchasing from us!\nPlease find attached copy of your invoice, if you need any further assistance please let us know.\n\nRegards\nJake"));	
			}
  		}
		
		if($result == 'mare'){
		
			if($store_country == 'DE'){
				$message = array('subject'=> "Verkaufsrechnung!",
								 'msg'=>utf8_encode("Hallo ".$name.",\n\nVielen Dank für Ihre Bestellung bei ebuyer express!\nIch habe eine Kopie der Rechnung als Referenz beigefügt.\nBenötigen Sie weitere Hilfe? Bitte zögern Sie nicht, uns wieder zu kontaktieren.\n\nHerzliche Grüße\nMarek"));	
								 						 
			}elseif($store_country == 'ES'){
				$message = array('subject'=> "Factura de venta!",
								 'msg'=>utf8_encode("Hola ".$name.",\n\nGracias por realizar un pedido con ebuyer Express!\nHe adjuntado una copia de la factura para su referencia.\nNecesita más ayuda? Por favor, no dude en ponerse en contacto con nosotros.\n\nAtentamente\nMarek"));	
			}elseif($store_country == 'FR'){
				$message = array('subject'=> "Facture de vente!",
								 'msg'=>utf8_encode("Bonjour ".$name.",\n\nMerci de passer une commande avec ebuyer express!\nJ'ai joint une copie de la facture de votre référence.\nBesoin d'aide supplémentaire? N'hésitez pas à nous contacter.\n\nCordialement\nMarek"));	
			}elseif($store_country == 'IT'){
				$message = array('subject'=> "Fattura di vendita!",
								 'msg'=>utf8_encode("Ciao ".$name.",\n\nGrazie per aver effettuato un ordine con ebuyer express!\nHo allegato una copia della fattura come riferimento.\nHai bisogno di ulteriore aiuto? Si prega di non esitare a contattarci indietro.\n\nCordiali saluti\nMarek"));	
			}else{
				$message = array('subject'=> "Sales invoice!",
								 'msg'=>utf8_encode("Hello ".$name.",\n\nThank you for placing an order with ebuyer express!\nI have attached a copy of the invoice for your reference.\nNeed further help? Please do not hesitate to contact us back..\n\nKind Regards\nMarek"));	
			}
  		}
		
		if($result == 'rain'){
		
			if($store_country == 'DE'){
				$message = array('subject'=> "Auftragsrechnung",
								 'msg'=>utf8_encode("Hallo von Rainbow Retail,\n\nWir danken Ihnen für Ihren Kauf in unserem Shop. Bitte finden Sie Ihre Einkaufsrechnung als Referenz beigefügt.\nBitte teilen Sie uns mit, wenn Sie weitere Fragen haben.\n\nDanke\nJohn"));	
								 						 
			}elseif($store_country == 'ES'){
				$message = array('subject'=> "Factura de pedido",
								 'msg'=>utf8_encode("Hola de Rainbow Retail,\n\nLe agradecemos su compra en nuestra tienda. Por favor, encuentre su factura de compra adjunta para su referencia.\nPor favor, háganos saber si tiene más preguntas.\n\nGracias\nJohn"));	
			}elseif($store_country == 'FR'){
				$message = array('subject'=> "Commande de facture",
								 'msg'=>utf8_encode("Bonjour de Rainbow Retail,\n\nNous vous remercions de votre achat dans notre magasin. Veuillez trouver ci-joint votre facture d'achat pour votre référence.\nS'il vous plaît laissez-nous savoir si vous avez d'autres requêtes.\n\nmerci\nJohn"));	
			}elseif($store_country == 'IT'){
				$message = array('subject'=> "Fattura ordine",
								 'msg'=>utf8_encode("Ciao da Rainbow Retail,\n\nVi ringraziamo per il vostro acquisto sul nostro negozio. Si prega di trovare allegato la fattura di acquisto per il vostro riferimento.\nFateci sapere se avete ulteriori domande.\n\ngrazie\nJohn"));	
			}else{
				$message = array('subject'=> "Order invoice",
								 'msg'=>utf8_encode("Hello from Rainbow Retail,\n\nWe thank you for your purchase on our store. Please find attached your purchase invoice for your reference.\nPlease let us know if you have any further queries.\n\nThanks\nJohn"));	
			}
  		}
		 
 		return $message;
 	
	}
	
	public function b2bUploadFile(){ 
		
		$this->layout = 'index';
		$this->loadModel('OpenOrdersArchive');	
		$this->loadModel('B2bCustomer');
		$orderdata  = [];
		$perPage	= 50;
		
		if(isset($_GET['key']) && $_GET['key']){
			$this->paginate = array('conditions'=>['amazon_order_id'=>trim($_GET['key'])],'order'=>'id DESC','limit' => $perPage); 
			$bbdata = $this->paginate('B2bCustomer');
			if(count($bbdata) == 0){
				$result = $this->OpenOrdersArchive->find('first', array('conditions' => array('num_order_id' => trim($_GET['key']) ),'fields' =>['amazon_order_id'] ) );
				if(count($result) > 0){ 
					$this->paginate = array('conditions'=>['amazon_order_id'=>$result['OpenOrdersArchive']['amazon_order_id']],'order'=>'id DESC','limit' => $perPage); 
				}
			}
		}else{
			$this->paginate = array('order'=>'id DESC','limit' => $perPage);
		}
  		$b2bdata = $this->paginate('B2bCustomer');
		
		if(count($b2bdata) > 0){
		
			foreach($b2bdata as $v){
				$amz_orders[] = $v['B2bCustomer']['amazon_order_id'];
			}
			
			$res = $this->OpenOrdersArchive->find('all', array('conditions' => array('amazon_order_id' => $amz_orders ),'fields' =>['sub_source','destination','num_order_id','customer_info','amazon_order_id','open_order_date','status','process_date'] ) );
			
			foreach($res as $c){
				$orderdata[$c['OpenOrdersArchive']['amazon_order_id']] = [  'num_order_id'=>$c['OpenOrdersArchive']['num_order_id'],
																	'sub_source'=>$c['OpenOrdersArchive']['sub_source'],
																	'destination'=>$c['OpenOrdersArchive']['destination'],
																	'open_order_date'=>$c['OpenOrdersArchive']['open_order_date'],
																	'status'=>$c['OpenOrdersArchive']['status'],
																	'process_date'=>$c['OpenOrdersArchive']['process_date'],
																	'customer_info'=> unserialize($c['OpenOrdersArchive']['customer_info'])
																	];
			} 
		}
		$this->set(compact( 'b2bdata','orderdata'));
		
	}
	
	public function uploadB2bSave(){
		
		$this->layout = 'index';		 
		$this->loadModel('B2bCustomer');
		$this->loadModel('OpenOrdersArchive');	 
		 
		$filetemppath =	$this->data['b2b_customers']['uploadfile']['tmp_name'];
		$pathinfo     = pathinfo($this->data['b2b_customers']['uploadfile']['name']);
             
		$fileNameWithoutExt = $pathinfo['filename'];
		$fileExtension = strtolower($pathinfo['extension']);
		
		if($fileExtension == 'csv')
		{
 			$filename = $fileNameWithoutExt.'_'.date('ymd_his').'_'.$this->Session->read('Auth.User.username').".".$fileExtension;		
			$upload_file = WWW_ROOT. 'b2b_customers'.DS.$filename ; 
			move_uploaded_file($filetemppath,$upload_file); 
			
			$row = 0; $c = 0; $i = 0;
			$handle = fopen($upload_file, "r");
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			  
			   $row++;			
			   if($row > 1){
			   	   //Update openorder
					$this->OpenOrdersArchive->updateAll( array('notes' => 1), array('amazon_order_id' => $data[0]) ); 
 					 
					$res = $this->B2bCustomer->find('count', array('conditions' => array('amazon_order_id' => $data[0] ) ) );
 					if($res == 0){ 
					    $i++;	
					   	$savedata['amazon_order_id']   =  $data[0] ;	
						$savedata['uploaded_date']     =  date('Y-m-d H:i:s');	 
						$savedata['upload_username']   =  $this->Session->read('Auth.User.username') ;	
						$this->B2bCustomer->saveAll( $savedata ); 
 						file_put_contents(WWW_ROOT .'logs/b2b_customers'.date('ymd').'.log' , $data[0]."\n",  FILE_APPEND|LOCK_EX);		
					}else{
						$c++;
					}			
				}					
 			}
			
			fclose($handle);
			
			if($i > 0){
				$this->Session->setFlash($i . " b2b customers added.", 'flash_success');
			}else	if($c > 0){
				$this->Session->setFlash($c++ . " b2b customers order id already exist.", 'flash_success');
			} 
 		 }
		 else{
			 $this->Session->setFlash("Invalid file format, Please upload CSV file.", 'flash_danger');
		 }
		 
		$this->redirect($this->referer());
 		 
	} 
	
	public function MarkB2b(){
		
		$this->layout = 'index';		 
		$this->loadModel('B2bCustomer');
		$this->loadModel('OpenOrdersArchive');	 
	   
 		$this->OpenOrdersArchive->updateAll( array('notes' => 1), array('amazon_order_id' => $this->request->data['amazon_order_id']) ); 
		 
		$res = $this->B2bCustomer->find('count', array('conditions' => array('amazon_order_id' => $this->request->data['amazon_order_id'] ) ) );
		if($res == 0){ 
 			$savedata['amazon_order_id']   =  $this->request->data['amazon_order_id'] ;	
			$savedata['uploaded_date']     =  date('Y-m-d H:i:s');	 
			$savedata['upload_username']   =  $this->Session->read('Auth.User.username') ;	
			$this->B2bCustomer->saveAll( $savedata ); 
			file_put_contents(WWW_ROOT .'logs/b2b_customers'.date('ymd').'.log' , $this->request->data['amazon_order_id']."\n",  FILE_APPEND|LOCK_EX);
			$msg = 'b2b order added.';	
		}else{
			 $msg = 'b2b customers order id already exist.';
		}			
 		
 		 echo json_encode(array('msg'=>$msg));
		 exit;
	} 
	public function getFbaEuhtml($order = null){ 
	
 		$this->loadModel('ProductHscode'); 
		$this->loadModel('MergeUpdatesArchive');
		$this->loadModel('SourceList');	
		$this->loadModel('Country');	
		$this->loadModel('Product');	
		
		 
		$SubSource		  = $order->GeneralInfo->SubSource;
 		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  		 		
			 	
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		$BILLING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName));
		$SHIPPING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName));
		
	 	$Country  = ''; $is_european_country = 0;
		$BillinCountryCode  =  strtolower($order->CustomerInfo->BillingAddress->Country);
		$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $order->CustomerInfo->BillingAddress->Country],'fields'=>['name','country_group']]);
		if(count( $cresult ) > 0){	
			$BillinCountry  =  $cresult['Country']['name'];
			
			if(in_array($cresult['Country']['name'],['Switzerland','Bosnia and Herzegovina','Kosovo','North Macedonia'])){		
				$is_european_country++;
			}else if(in_array($cresult['Country']['country_group'],['rest_of_europe','rest_of_eu','united kingdom','france','italy','germany','spain'])){
				$is_european_country++;
			} 
		}
		
		$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $order->CustomerInfo->Address->Country],'fields'=>['name']]);
		if(count( $cresult ) > 0){	
			$Country  =  $cresult['Country']['name'];
		}
		
		if($BILLING_ADDRESS == ''){
			
 			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS  = ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$BillinCountry;
			}
		}	
		if($SHIPPING_ADDRESS == ''){
 			
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS = ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $Country;
			}			
		}		
		
		$EmailAddress	  = $order->CustomerInfo->Address->EmailAddress;
		$exp = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = $exp[$index];
		  		
		$country_code	  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 
		$ReferenceNum	  = $order->GeneralInfo->ReferenceNum; 
 		$GIFT_WRAP		  = $order->TotalsInfo->gift_wrap;
 		$ITEM_PROMOTION	  = $order->TotalsInfo->item_promotion;
		$SHIP_PROMOTION	  = $order->TotalsInfo->shipping_promotion;
		
 		$ORDER_DATE	  	 = date('d-m-Y', strtotime($order->GeneralInfo->ReceivedDate));
		$INVOICE_NO	 	 = 'INV-'.$OrderId ;
		$INVOICE_DATE 	 = $ORDER_DATE;//date('d-m-Y');
		
 		$templateHtml 	  = NULL;
  		$tax_rate = 0; 	  $vat_amount = 0;
  		$currency_code = $this->getCurrencyCode($Currency);
		
		$VAT_Number = '';
		if($result == 'cost'){
			$EORI_Number = 'GB019020854000';
			$UK_VAT_Number = 'GB 304984295'; 
			$DE_VAT_Number = 'DE 321777974'; 
			$FR_VAT_Number = 'FR 59850070509'; 
			$IT_VAT_Number = 'IT 10905930961'; 
			$ES_VAT_Number = 'ES N6061518D'; 
			$CZ_VAT_Number = 'CZ 684972917';
			
			$STORE_NAME	   = $this->COST_STORE_NAME;
		    $STORE_ADDRESS = $this->COST_STORE_ADDRESS; 			 
		}
		
		if($result == 'mare'){
			$EORI_Number = 'GB019434034000';
			$UK_VAT_Number = 'GB 307817693'; 
			$DE_VAT_Number = 'DE 323086773';
			$IT_VAT_Number = 'IT 11062310963'; // 16 Dec 2019
			$FR_VAT_Number = 'FR 07879900934';  
		
			$STORE_NAME	   = $this->MARE_STORE_NAME;
		    $STORE_ADDRESS = $this->MARE_STORE_ADDRESS;
  		}	
		
		if($result == 'rain'){
 			$EORI_Number = 'GB318649182000';
			$UK_VAT_Number = 'GB 318649182'; 
			$IT_VAT_Number = 'IT 11061000961';
			$DE_VAT_Number = 'DE 327173988';
			$FR_VAT_Number = '';			
		  
			$STORE_NAME	   = $this->RAIN_STORE_NAME;
		    $STORE_ADDRESS = $this->RAIN_STORE_ADDRESS;
 		}	 
		 
 		if($store_country == 'gb' || $store_country == 'uk'){  
			$VAT_Number   = $UK_VAT_Number;
		}else if($store_country == 'de'){
			$VAT_Number   = $DE_VAT_Number; 
		}else if($store_country == 'fr'){
			$VAT_Number   = $FR_VAT_Number;  
		}else if($store_country == 'it'){
			$VAT_Number   = $IT_VAT_Number; 			 
		}else if($store_country == 'es'){
			$VAT_Number   = $ES_VAT_Number; 			 
		}
		 
		$txt = '';
		if($is_european_country > 0){
			$txt = 'Vatable Transaction'; 
		}else{
			$txt = 'Non Taxable Supply (Intra Community Transaction)'; 
		} 
 
  		$templateHtml  ='<head><meta charset="UTF-8"><title>Invoice</title></head>';
		$templateHtml  .='<style>*{font-family:arial; font-size:12px;}</style>';
		$templateHtml  .='<div style="height:950px;margin-bottom:10px;">';						   
		$templateHtml  .='<table border="0" width="700px" align="center" cellpadding="0" cellspacing="0" style="margin:0px auto;">';
		$templateHtml  .='<tbody>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> Invoice </td>';
		$templateHtml  .='  </tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td valign="middle" align="left" width="50%">';
				$templateHtml  .='<p style="font-size:18px ;font-weight:bold;">'.$BILLING_ADDRESS_NAME.'</p>';
				$templateHtml  .='<p>'.$BILLING_ADDRESS.'</p>'; 
 			$templateHtml  .='</td>';
			$templateHtml  .='<td valign="top" align="center" style="background:#dfeced; padding:10px;">';
			  $templateHtml  .='<div  style="border:1px solid #bdc9c9; padding:5px;">';
				$templateHtml  .='<table>';
				  
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td style="font-size:30px ;font-weight:bold;"> Paid</td>';
					 $templateHtml  .='</tr>';
					$templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2">';
						$templateHtml  .='<p>Sold by <strong> '.$STORE_NAME.'</strong></p>';
						if($VAT_Number){
							$templateHtml  .='<p>VAT #<strong>&nbsp;&nbsp;&nbsp;&nbsp;'.$VAT_Number.'</strong></p>'; 
						}
 						 
						$templateHtml  .='</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2" width="100%" height="1px" style="width:100%; height:1px; background:#bdc9c9;"> </td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong>Invoice date / Delivery date</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_DATE.'</td>';
					 $templateHtml  .='</tr>';
					  $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong><strong>Invoice #</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_NO.'</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong><strong>Total payable</strong></td>';
						$templateHtml  .='<td> '.$currency_code.number_format($TOTAL,2).'</td>';
					 $templateHtml  .='</tr>'; 
				  
			   $templateHtml  .='</table>';
			   $templateHtml  .='</div>';
			   
			$templateHtml  .='</td>';
		 $templateHtml  .='</tr>';
		 
		 if($exp[$index] == 'fr'){
			 $amazon_url = 'www.amazon.fr';
		 }else if($exp[$index] == 'es'){
			 $amazon_url = 'www.amazon.es';
		 }else if($exp[$index] == 'it'){
			 $amazon_url = 'www.amazon.it';
		 }else if($exp[$index] == 'de'){
			 $amazon_url = 'www.amazon.de';
		 }else{
			 $amazon_url = 'www.amazon.co.uk';
		 }
		 
		 $templateHtml  .='<tr>';
		/*	$templateHtml  .='<td colspan="2" style="padding:10px 0px;"> For questions about your order, visit <a href="https://'.$amazon_url.'/contact-us">'.$amazon_url.'/contact-us</a></td>';*/
			$templateHtml  .='<td colspan="2" style="padding:10px 0px;"> For questions about your order, visit '.$amazon_url.'/contact-us</td>';
		 $templateHtml  .='</tr>';
		
		
		  $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="padding:5px 0px;"><strong>Tranction Type:</strong>'.$txt.'</td>';
		 $templateHtml  .='</tr>';
		 
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="background:#ddd; height:2px;"> </td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="height:10px;"> </td>';
		 $templateHtml  .='</tr>'; 
		 
		 $templateHtml  .='<tr><td colspan="2">';
		 $templateHtml  .='<table border="0" width="700px" align="center" cellpadding="0" cellspacing="0" >';
			
				$templateHtml  .='<tr>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">BILLING ADDRESS </th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">DELIVERY ADDRESS</th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">SOLD BY </th>';
			   $templateHtml  .='</tr>'; 
			   
			  $templateHtml  .='<tr>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$BILLING_ADDRESS_NAME.'</strong><br>'.$BILLING_ADDRESS.'</p>'; 
				  $templateHtml  .='</td>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$SHIPPING_ADDRESS_NAME.'</strong><br>'.$SHIPPING_ADDRESS.'</p>';
				  $templateHtml  .='</td>';
				 $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$STORE_NAME.'</strong><br>'.$STORE_ADDRESS;
					 if($VAT_Number){
						 $templateHtml  .='<br><strong>'.$VAT_Number.'</strong>';
					 }
					 $templateHtml  .='</p>';
					  
				  $templateHtml  .='</td>'; 
			  $templateHtml  .='</tr>';
				$templateHtml  .='<tr><td colspan="3" style="background:#ddd; height:1px;"> </td></tr>';
				$templateHtml  .='<tr>';
				   $templateHtml  .='<td colspan="3">';
					 $templateHtml  .='<p><strong>Order information</strong></p>';
					 $templateHtml  .='<p>Order date '.$ORDER_DATE.'';
					 $templateHtml  .='<br>Order # '.$ReferenceNum.'</p>';
				   $templateHtml  .='</td>';
				$templateHtml  .='</tr>';
		  
		  
		 $templateHtml  .='</table>';
		 $templateHtml  .='</td></tr>'; 
		$templateHtml  .='</table>';
		$templateHtml  .='</div>';
		
		$templateHtml  .='<div style="height:950px;margin-bottom:-1px;padding-top:100px;">';
		$templateHtml  .='<table width="700px" cellpadding="0" cellspacing="0" align="center">';
		$templateHtml  .='<tbody>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="5"></td>';
			$templateHtml  .='<td  style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> &nbsp; </td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="5"></td>';
			$templateHtml  .='<td  style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> Invoice</td>';
		 $templateHtml  .='</tr>';
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="4"></td>';
			$templateHtml  .='<td  colspan="2" style="font-weight:bold; padding:7px 5px; text-align:right"> Invoice # '.$INVOICE_NO.'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="6" style="font-size:24px; font-weight:bold; border-top:2px solid #ddd; border-bottom:2px solid #ddd; padding:7px 5px;"> Invoice Details</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<th valign="top" style="text-align:left; padding:10px 5px;" width="50%"> Description </th>';
			$templateHtml  .='<th valign="top" style="text-align:left; padding:10px 5px;" width="5%"> Qty </th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="10%"> Unit price<br>(excl. VAT)</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="11%"> VAT rate</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="12%"> Unit price<br> (incl. VAT)</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="12%"> Item subtotal<br> (incl. VAT)</th>';
		 $templateHtml  .='</tr>';
		 
		$total_item_vat_amount = 0;
		$total_item_cost 	   = 0; 
 		$tax_rate = 0;
		
		if($store_country == 'uk'){
			$store_country = 'gb';
		} 
		
		if($is_european_country > 0 ){
			$shipping_per_item = $POSTAGE / count($items); 
			$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $store_country)));
			if(count( $thsresult ) > 0){				
				$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
			}
		} 
		
		foreach($items as $count => $item){   
	
  			 $PricePerUnit = ($item->PricePerUnit + $item->item_tax) ;
			 
			 $item_vat_amount = $PricePerUnit - ($PricePerUnit/(1 + ($tax_rate/100)));
			 $item_cost = $PricePerUnit - $item_vat_amount;
			
			 $total_item_vat_amount  +=$item_vat_amount;
			 $total_item_cost  += $item_cost ;
			 
			 $per_unit_cost  = $item_cost / $item->Quantity;
			 $per_unit_price  = $PricePerUnit / $item->Quantity;
		
 			$templateHtml  .='<tr style="background:#ddd;">';
				$templateHtml  .='<td style="padding:10px 5px;">'.$item->Title.'</td>';
				$templateHtml  .='<td style="padding:10px 5px;">'.$item->Quantity.'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($per_unit_cost,2).'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($per_unit_price,2).'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($item->CostIncTax,2).'</td>';
			 $templateHtml  .='</tr>';
			 
		 } 
  	 
 		 $postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
 		 $postage_cost		 = $POSTAGE - $postage_vat_amount;
		 
		 $templateHtml  .='<tr style="background:#ddd;">';

			$templateHtml  .='<td style="padding:10px 5px;">Shipping Charge</td>';
			$templateHtml  .='<td style="padding:10px 5px;"></td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($postage_cost,2).' </td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
		 $templateHtml  .='</tr>';
 		 
 		  $gift_vat_amount  = $GIFT_WRAP - ($GIFT_WRAP/(1 + ($tax_rate/100)));
 		  $gift_cost		= $GIFT_WRAP - $gift_vat_amount; 
		 
		  $templateHtml  .='<tr style="background:#ddd;">';

			$templateHtml  .='<td style="padding:10px 5px;">Gift Wrap</td>';
			$templateHtml  .='<td style="padding:10px 5px;"></td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($gift_cost,2).' </td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($GIFT_WRAP,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($GIFT_WRAP,2).'</td>';
		 $templateHtml  .='</tr>';
		 
		  $ip_vat_amount  = $ITEM_PROMOTION - ($ITEM_PROMOTION/(1 + ($tax_rate/100)));
 		  $ip_cost		= $ITEM_PROMOTION - $ip_vat_amount; 
		 
		  $templateHtml  .='<tr style="background:#ddd;">';
 			$templateHtml  .='<td style="padding:10px 5px;">Item Promotion</td>';
			$templateHtml  .='<td style="padding:10px 5px;"></td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($ip_cost,2).' </td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($ITEM_PROMOTION,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($ITEM_PROMOTION,2).'</td>';
		 $templateHtml  .='</tr>';
		 
		 $sp_vat_amount  = $SHIP_PROMOTION - ($SHIP_PROMOTION/(1 + ($tax_rate/100)));
 		 $sp_cost		 = $SHIP_PROMOTION - $sp_vat_amount; 
		 
		  $templateHtml  .='<tr style="background:#ddd;">';
 			$templateHtml  .='<td style="padding:10px 5px;">Shipping Promotion</td>';
			$templateHtml  .='<td style="padding:10px 5px;"></td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($sp_cost,2).' </td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($SHIP_PROMOTION,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($SHIP_PROMOTION,2).'</td>';
		 $templateHtml  .='</tr>';
		
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			//$templateHtml  .='<td></td>';
			$templateHtml  .='<td colspan="4"><table width="100%"><tr><td style="font-size:24px; font-weight:bold;" align="left">
			Invoice total</td><td style="font-size:24px; font-weight:bold;"align="right"> '.$currency_code.'&nbsp;'. number_format($TOTAL,2) .'</td></tr></table></td>';
			
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';		 
			$templateHtml  .='<td colspan="4" style="height:2px; background:#ddd;"></td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="6" height="10px;"></td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th style="text-align:left; font-weight:bold; padding:10px 5px;">VAT rate </th>';
			$templateHtml  .='<th style="font-weight:bold; padding:10px 5px;" align="right">Item subtotal<br>(excl. VAT)</th>';
			$templateHtml  .='<th style="font-weight:bold; padding:10px 5px;" align="right">VAT subtotal</th>';
		 $templateHtml  .='</tr>';
		 
		 $total_item_vat_amount = $total_item_vat_amount + $postage_vat_amount  + $gift_vat_amount + $ip_vat_amount + $sp_vat_amount;
		 $total_item_cost  = $total_item_cost + $postage_cost + $gift_cost + $ip_cost + $sp_cost ;
			 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px"> '.$tax_rate.'%</td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($total_item_cost,2).'</td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($total_item_vat_amount,2).'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td style="padding:10px 5px; font-weight:bold;">Total</td>';
			$templateHtml  .='<td style="padding:10px 5px; font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format($total_item_cost,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;  font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format($total_item_vat_amount ,2).'</td>';
		 $templateHtml  .='</tr>';
		$templateHtml  .='</tbody>';
		$templateHtml  .='</table>';
		$templateHtml  .='</div>';
		$templateHtml  .='</body>';
		 
 		return $templateHtml;	
	
 	}
	
	public function getB2Bhtml($order = null){ 
	
 		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdatesArchive');
		$this->loadModel('SourceList');	
		$this->loadModel('Country');	
		$this->loadModel('Product');	
		 
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  		 		
			
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		$BILLING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName));
		$SHIPPING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName));
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
 			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS  = ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$order->CustomerInfo->BillingAddress->Country ? $order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
 			
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS = ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $order->CustomerInfo->Address->Country ? $order->CustomerInfo->Address->Country : '';
			}			
		}		
		
		$EmailAddress	  = $order->CustomerInfo->Address->EmailAddress;
		$exp 	= explode(".",$EmailAddress);
		$index  = count($exp) - 1;
		$store_country = $exp[$index];
		  		
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$_POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 
		$ReferenceNum	  = $order->GeneralInfo->ReferenceNum; 
 			
		$ORDER_DATE	  	 = date('d-m-Y', strtotime($order->GeneralInfo->ReceivedDate));
		$INVOICE_NO	 	 = 'INV-'.$OrderId ;
		$INVOICE_DATE 	 = $ORDER_DATE;//date('d-m-Y');
		
		$templateHtml 	  = NULL;
   
		//echo "</pre>";
		$item_lvcr_limit  = '21.9'; //EUR
		$lvcr_limit_shipping  = '26.28';		
		$final_total =  $TOTAL ;
		if($Currency == 'GBP'){
			$final_total = $TOTAL * (1.122);
			$POSTAGE = '3.81';//GBP 
		}
		
		if($final_total > 26.27){
			$postArr = array('4.29');
		}else if($final_total > 26 && $final_total < 26.28){
			$postArr = array('4.27','4.28','4.29');
		}else{
			$postArr = array('3.78','3.80','3.83','3.85','3.87','3.89','4.03','4.05','4.03','4.15','4.18','4.19','4.21','4.24','4.26','4.27','4.29');
		}
		$key  		= array_rand($postArr);
		$POSTAGE    = $postArr[$key];//EUR
		
		if($_POSTAGE > 0 || $final_total > $lvcr_limit_shipping){
			$POSTAGE = $_POSTAGE;
 		} 
		
		$country_code = $store_country ;
		if($store_country == 'uk'){
			$country_code = 'gb';
		}
		//echo "==TT===";
		// echo "==TT===".($lvcr_limit_shipping  - $final_total);  exit;
		 
		$store_country = [];
		$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $country_code],'fields'=>['name','custom_name']]);
		if(count( $cresult ) > 0){	
			$store_country  = [$cresult['Country']['name'], $cresult['Country']['custom_name']];
		}
		
 		//$country_code = $this->getCountryCode($Country);
  		  		
		$tax_rate = 0; $vat_amount = 0;
		 
  		if((in_array($Country,$store_country)) && (($final_total + $_POSTAGE) > $lvcr_limit_shipping)){
			$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
			if(count( $thsresult ) > 0){				
			 	$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
			}
			 
		} 
		 
		$currency_code = $this->getCurrencyCode($Currency);
		
		$VAT_Number = '';  $txt = '';
		if($result == 'cost'){
			$EORI_Number = 'GB019020854000';
			$UK_VAT_Number = 'GB 304984295'; 
			$DE_VAT_Number = 'DE 321777974'; 
			$FR_VAT_Number = 'FR 59850070509'; 
			$IT_VAT_Number = 'IT 10905930961'; 
			$CZ_VAT_Number = 'CZ 684972917';  
			
			$STORE_NAME	   = $this->COST_STORE_NAME;
		    $STORE_ADDRESS = $this->COST_STORE_ADDRESS; 
			$input = array();
			$txt = 'Non Taxable Supply( Export/Intra Community Transactions)'; 
			if($Country == 'United Kingdom'){ //EURACO GROUP LIMITED
				 $VAT_Number   = $UK_VAT_Number;
				 if($final_total < $lvcr_limit_shipping){
					$txt = 'Non Taxable Supply(Supplied from outside EU below deminimus)'; 
				}
   			}else if($Country == 'Germany'){
 				$VAT_Number   = $DE_VAT_Number; 
				if($final_total < $lvcr_limit_shipping){
					$VAT_Number = $UK_VAT_Number;
				}
			}else if($Country == 'France'){
				$VAT_Number   = $FR_VAT_Number; 
				if($final_total < $lvcr_limit_shipping){
					$VAT_Number = $UK_VAT_Number;
				}
			}else if($Country == 'Italy'){
				$VAT_Number   = $IT_VAT_Number; 
				if($final_total < $lvcr_limit_shipping){
					$VAT_Number = $UK_VAT_Number;
				}
			}else{				
				$tax_rate   = 0;
				if($final_total > $lvcr_limit_shipping){
					$input = array($UK_VAT_Number,$DE_VAT_Number,$FR_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
			}
			
 			if(in_array($Country,['Germany','France'])){
				if($final_total > $lvcr_limit_shipping){
					$txt = 'Vatable Transaction'; 
				}else{
					$txt = 'Non Taxable Supply(Supplied from outside EU below deminimus)'; 
					$tax_rate   = 0;
				}
			}
			
			if(count($input) > 0){
 				  $rand_keys 	  = array_rand($input);
 				  $VAT_Number     = $input[$rand_keys]; 
			}
		
		}
		 
		if($result == 'mare'){ // ESL LIMITED
			$EORI_Number = 'GB019434034000';
			$UK_VAT_Number = 'GB 307817693'; 
			$DE_VAT_Number = 'DE 323086773';
			$IT_VAT_Number = 'IT 11062310963'; // 16 Dec 2019
			$FR_VAT_Number = 'FR 07879900934'; 
		
			$STORE_NAME	   = $this->MARE_STORE_NAME;
		    $STORE_ADDRESS = $this->MARE_STORE_ADDRESS;
			$input = array();
			$txt = 'Non Taxable Supply( Export/Intra Community Transactions)'; 
			if($Country == 'United Kingdom'){
 				$VAT_Number = $UK_VAT_Number;
				 if($final_total < $lvcr_limit_shipping){
					$txt = 'Non Taxable Supply(Supplied from outside EU below deminimus)'; 
					$tax_rate   = 0;
				}
  			}else if($Country == 'Germany'){
 				$VAT_Number = $DE_VAT_Number;
				$txt = 'Vatable Transaction'; 
				if($final_total < $lvcr_limit_shipping){
					$VAT_Number = $UK_VAT_Number;
					$tax_rate   = 0;
					$txt = 'Non Taxable Supply(Supplied from outside EU below deminimus)'; 		
				}
 			}else if($Country == 'Italy'){
 				$VAT_Number = $IT_VAT_Number;
				$txt = 'Vatable Transaction'; 
				if($final_total < $lvcr_limit_shipping){
					$VAT_Number = $UK_VAT_Number;
					$tax_rate   = 0;
					$txt = 'Non Taxable Supply(Supplied from outside EU below deminimus)'; 		
				}
 			}else{		
				$tax_rate   = 0;		
				if($final_total > $lvcr_limit_shipping){
					$input = array($UK_VAT_Number,$DE_VAT_Number);					
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
			}
 			 
  			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}
		}	
		
		if($result == 'rain'){ //FRESHER BUSINESS LIMITED
 			$EORI_Number = 'GB318649182000';
			$UK_VAT_Number = 'GB 318649182'; 
			$IT_VAT_Number = 'IT 11061000961';
			$DE_VAT_Number = 'DE 327173988';
			$FR_VAT_Number = '';
		  
			$STORE_NAME	   = $this->RAIN_STORE_NAME;
		    $STORE_ADDRESS = $this->RAIN_STORE_ADDRESS;
			
			$VAT_Number = $UK_VAT_Number;
			
			$txt = 'Non Taxable Supply( Export/Intra Community Transactions)'; 
			
			if($Country == 'United Kingdom'){
				if($final_total > $lvcr_limit_shipping){
					$txt = 'Vatable Transaction'; 
				}else{
					$txt = 'Non Taxable Supply(Supplied from outside EU below deminimus)'; 
					$tax_rate   = 0;
				}
			}else{
				$tax_rate   = 0;
			}
 		}	
  			
		if($final_total < $lvcr_limit_shipping){
			$txt = 'Non Taxable Supply(Supplied from outside EU below deminimus)'; 
			$tax_rate   = 0;
		}
 		/*----------Changes for new Invoice 26-09-2019--------*/		
		 
 		/*if($Country == 'United Kingdom' && $UK_VAT_Number != ''){
			$VAT_Number   = $UK_VAT_Number ;
		}else if($Country == 'Germany' && $DE_VAT_Number != '' ){
			$VAT_Number   = $DE_VAT_Number ;
		}*/
		
		 
		/* if($final_total > 21.9 && $tax_rate > 0){
			 $txt = 'VATable Transaction'; 
		 }else if($final_total < 22 && $tax_rate > 0){
			 $txt = 'VAT exempt( LVCR Limit)'; 
		 }else if($final_total < 22 && $tax_rate == 0){
			 $txt = 'VAT exempt( Export/Intra Community Transactions)'; 
		 }*/
								
		$templateHtml  ='<head><meta charset="UTF-8"><title>Invoice</title></head>';
		$templateHtml  .='<style>*{font-family:arial; font-size:12px;}</style>';
		$templateHtml  .='<div style="height:950px;margin-bottom:10px;">';						   
		$templateHtml  .='<table border="0" width="700px" align="center" cellpadding="0" cellspacing="0" style="margin:0px auto;">';
		$templateHtml  .='<tbody>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> Invoice </td>';
		$templateHtml  .='  </tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td valign="middle" align="left" width="50%">';
				$templateHtml  .='<p style="font-size:18px ;font-weight:bold;">'.$BILLING_ADDRESS_NAME.'</p>';
				$templateHtml  .='<p>'.$BILLING_ADDRESS.'</p>';
				/*if($VAT_Number){
					$templateHtml  .='<p><strong>'.$VAT_Number.'</strong></p>';
				}*/
 			$templateHtml  .='</td>';
			$templateHtml  .='<td valign="top" align="center" style="background:#dfeced; padding:10px;">';
			  $templateHtml  .='<div  style="border:1px solid #bdc9c9; padding:5px;">';
				$templateHtml  .='<table>';
				  
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td style="font-size:30px ;font-weight:bold;"> Paid</td>';
					 $templateHtml  .='</tr>';
					$templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2">';
						$templateHtml  .='<p>Sold by <strong> '.$STORE_NAME.'</strong></p>';
						if($VAT_Number){
							$templateHtml  .='<p>VAT #<strong>'.$VAT_Number.'</strong></p>'; 
						}
 						 
						$templateHtml  .='</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2" width="100%" height="1px" style="width:100%; height:1px; background:#bdc9c9;"> </td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong>Invoice date / Delivery date</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_DATE.'</td>';
					 $templateHtml  .='</tr>';
					  $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong><strong>Invoice #</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_NO.'</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong><strong>Total payable</strong></td>';
						$templateHtml  .='<td> '.$currency_code.$TOTAL.'</td>';
					 $templateHtml  .='</tr>'; 
				  
			   $templateHtml  .='</table>';
			   $templateHtml  .='</div>';
			   
			$templateHtml  .='</td>';
		 $templateHtml  .='</tr>';
		 
		 if($exp[$index] == 'fr'){
			 $amazon_url = 'www.amazon.fr';
		 }else if($exp[$index] == 'es'){
			 $amazon_url = 'www.amazon.es';
		 }else if($exp[$index] == 'it'){
			 $amazon_url = 'www.amazon.it';
		 }else if($exp[$index] == 'de'){
			 $amazon_url = 'www.amazon.de';
		 }else{
			 $amazon_url = 'www.amazon.co.uk';
		 }
		 
		 $templateHtml  .='<tr>';
		/*	$templateHtml  .='<td colspan="2" style="padding:10px 0px;"> For questions about your order, visit <a href="https://'.$amazon_url.'/contact-us">'.$amazon_url.'/contact-us</a></td>';*/
			$templateHtml  .='<td colspan="2" style="padding:10px 0px;"> For questions about your order, visit '.$amazon_url.'/contact-us</td>';
		 $templateHtml  .='</tr>';
		
		
		  $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="padding:5px 0px;"><strong>Tranction Type: </strong>'.$txt.'</td>';
		 $templateHtml  .='</tr>';
		 
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="background:#ddd; height:2px;"> </td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="height:10px;"> </td>';
		 $templateHtml  .='</tr>'; 
		 
		 $templateHtml  .='<tr><td colspan="2">';
		 $templateHtml  .='<table border="0" width="700px" align="center" cellpadding="0" cellspacing="0" >';
			
				$templateHtml  .='<tr>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">BILLING ADDRESS </th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">DELIVERY ADDRESS</th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">SOLD BY </th>';
			   $templateHtml  .='</tr>'; 
			   
			  $templateHtml  .='<tr>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$BILLING_ADDRESS_NAME.'</strong><br>'.$BILLING_ADDRESS.'</p>'; 
				  $templateHtml  .='</td>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$SHIPPING_ADDRESS_NAME.'</strong><br>'.$SHIPPING_ADDRESS.'</p>';
				  $templateHtml  .='</td>';
				 $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$STORE_NAME.'</strong><br>'.$STORE_ADDRESS;
					 if($VAT_Number){
						 $templateHtml  .='<br><strong>'.$VAT_Number.'</strong>';
					 }
					 $templateHtml  .='</p>';
					  
				  $templateHtml  .='</td>'; 
			  $templateHtml  .='</tr>';
				$templateHtml  .='<tr><td colspan="3" style="background:#ddd; height:1px;"> </td></tr>';
				$templateHtml  .='<tr>';
				   $templateHtml  .='<td colspan="3">';
					 $templateHtml  .='<p><strong>Order information</strong></p>';
					 $templateHtml  .='<p>Order date '.$ORDER_DATE.'';
					 $templateHtml  .='<br>Order # '.$ReferenceNum.'</p>';
				   $templateHtml  .='</td>';
				$templateHtml  .='</tr>';
		  
		  
		 $templateHtml  .='</table>';
		 $templateHtml  .='</td></tr>'; 
		$templateHtml  .='</table>';
		$templateHtml  .='</div>';
		
		$templateHtml  .='<div style="height:950px;margin-bottom:-1px;padding-top:100px;">';
		$templateHtml  .='<table width="700px" cellpadding="0" cellspacing="0" align="center">';
		$templateHtml  .='<tbody>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="5"></td>';
			$templateHtml  .='<td  style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> &nbsp; </td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="5"></td>';
			$templateHtml  .='<td  style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> Invoice</td>';
		 $templateHtml  .='</tr>';
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="4"></td>';
			$templateHtml  .='<td  colspan="2" style="font-weight:bold; padding:7px 5px; text-align:right"> Invoice # '.$INVOICE_NO.'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="6" style="font-size:24px; font-weight:bold; border-top:2px solid #ddd; border-bottom:2px solid #ddd; padding:7px 5px;"> Invoice Details</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<th valign="top" style="text-align:left; padding:10px 5px;" width="50%"> Description </th>';
			$templateHtml  .='<th valign="top" style="text-align:left; padding:10px 5px;" width="5%"> Qty </th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="10%"> Unit price<br>(excl. VAT)</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="11%"> VAT rate</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="12%"> Unit price<br> (incl. VAT)</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="12%"> Item subtotal<br> (incl. VAT)</th>';
		 $templateHtml  .='</tr>';
		 
		$total_item_vat_amount = 0;
		$total_item_cost 	   = 0;
 		$shipping_per_item 	   = 0;
		
		if($_POSTAGE == 0 && $lvcr_limit_shipping >= $final_total){
			$shipping_per_item = $POSTAGE / count($items);
		}
		 
		 foreach($items as $count => $item){ 
			 
			if(strlen($item->Title) > 10){
				$Title = $item->Title; 
			}else{
				$Title = $item->ChannelTitle;
			} 
			 
			if($shipping_per_item > 0){
				$shipping_per_item = ($shipping_per_item / $item->Quantity);	
			}
			$PricePerUnit = $item->PricePerUnit - $shipping_per_item;
			 
			 $item_vat_amount = $PricePerUnit - ($PricePerUnit/(1 + ($tax_rate/100)));
			 $item_cost = $PricePerUnit - $item_vat_amount;
			
			 $total_item_vat_amount  += ($item_vat_amount* $item->Quantity);
			 $total_item_cost  += ($item_cost * $item->Quantity);
			  
			$templateHtml  .='<tr style="background:#ddd;">';
				$templateHtml  .='<td style="padding:10px 5px;">'.$Title.'</td>';
				$templateHtml  .='<td style="padding:10px 5px;">'.$item->Quantity.'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($item_cost,2).'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($PricePerUnit,2).'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format(($item->CostIncTax - 
				($shipping_per_item *$item->Quantity)),2).'</td>';
			 $templateHtml  .='</tr>';
			 
		 } 
		
		 $postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
		 $postage_cost = $POSTAGE - $postage_vat_amount;
		 
		 $templateHtml  .='<tr style="background:#ddd;">';

			$templateHtml  .='<td style="padding:10px 5px;">Shipping Charge</td>';
			$templateHtml  .='<td style="padding:10px 5px;"></td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($postage_cost,2).' </td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
		 $templateHtml  .='</tr>';
		
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td colspan="2" style="font-size:24px; font-weight:bold; padding:20px 0px;">Invoice total</td>';
			$templateHtml  .='<td style="font-size:24px; font-weight:bold; padding:20px 0px;" align="right">'.$currency_code.'&nbsp;'. $TOTAL.'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td colspan="4" style="height:2px; background:#ddd;"></td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="6" height="10px;"></td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th style="text-align:left; font-weight:bold; padding:10px 5px;">VAT rate </th>';
			$templateHtml  .='<th style="font-weight:bold; padding:10px 5px;" align="right">Item subtotal<br>(excl. VAT)</th>';
			$templateHtml  .='<th style="font-weight:bold; padding:10px 5px;" align="right">VAT subtotal</th>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px"> '.$tax_rate.'%</td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_cost + $postage_cost),2).'</td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_vat_amount + $postage_vat_amount),2).'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td style="padding:10px 5px; font-weight:bold;">Total</td>';
			$templateHtml  .='<td style="padding:10px 5px; font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_cost + $postage_cost),2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;  font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_vat_amount + $postage_vat_amount),2).'</td>';
		 $templateHtml  .='</tr>';
		$templateHtml  .='</tbody>';
		$templateHtml  .='</table>';
		$templateHtml  .='</div>';
		$templateHtml  .='</body>';
		 
		if($total_item_vat_amount > 0){
			$file_save = WWW_ROOT .'/logs/b2b_invoices/vat_'.date('MY').'.log';;
			file_put_contents($file_save, $OrderId."\t".$ReferenceNum."\t".$VAT_Number."\t".$total_item_vat_amount."\n",  FILE_APPEND|LOCK_EX);		
		}
		
		return $templateHtml;	
	
 	}
	
	public function getB2Bhtml30($order = null){ 
	
 		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdatesArchive');
		$this->loadModel('SourceList');	
		$this->loadModel('Country');	
		$this->loadModel('Product');	
		
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  		 		
			
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		$BILLING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName));
		$SHIPPING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName));
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
 			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS  = ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$order->CustomerInfo->BillingAddress->Country ? $order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
 			
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS = ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $order->CustomerInfo->Address->Country ? $order->CustomerInfo->Address->Country : '';
			}			
		}		
		
		$EmailAddress	  = $order->CustomerInfo->Address->EmailAddress;
		$exp = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = $exp[$index];
		  		
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$_POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 
		$ReferenceNum	  = $order->GeneralInfo->ReferenceNum; 
 			
		$ORDER_DATE	  	 = date('d-m-Y', strtotime($order->GeneralInfo->ReceivedDate));
		$INVOICE_NO	 	 = 'INV-'.$OrderId ;
		$INVOICE_DATE 	 = $ORDER_DATE;//date('d-m-Y');
		
		$templateHtml 	  = NULL;
   
		//echo "</pre>";
		$item_lvcr_limit  = '21.9'; //EUR
		$lvcr_limit_shipping  = '26.29';		
		$final_total =  $TOTAL ;
		if($Currency == 'GBP'){
			$final_total = $TOTAL * (1.122);
			$POSTAGE = '3.81';//GBP 
		}
		
		if($final_total > 26.27){
			$postArr = array('4.29');
		}else if($final_total > 26 && $final_total < 26.28){
			$postArr = array('4.27','4.28','4.29');
		}else{
			$postArr = array('3.76','3.78','3.79','3.80','3.85','3.87','3.89','4.03','4.05','4.03','4.15','4.18','4.19','4.21','4.24','4.26','4.27','4.29');
		}
		$key  		= array_rand($postArr);
		$POSTAGE    =  $postArr[$key];//EUR
		
 		if($final_total > $lvcr_limit_shipping){
			$POSTAGE = $_POSTAGE;
 		} 
		
		
		//echo "==TT===";
		// echo "==TT===".($lvcr_limit_shipping  - $final_total);  exit;
		
		$sresult = $this->SourceList->find('first',['conditions' => ['LocationName' => $SubSource],'fields'=>['country_code']]);
		if(count( $sresult ) > 0){	
			$country_code   = $sresult['SourceList']['country_code'];
		}
		
		$store_country = [];
		$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $country_code],'fields'=>['name','custom_name']]);
		if(count( $cresult ) > 0){	
			$store_country  = [$cresult['Country']['name'], $cresult['Country']['custom_name']];
		}
		
		//	$country_code = $this->getCountryCode($Country);
	
		$tax_rate = 0; $vat_amount = 0;
		 
  		/*if(in_array($Country,$store_country) && $final_total > 21.9){
			$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
			if(count( $thsresult ) > 0){				
				$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
			}
		}*/
		 
		$currency_code = $this->getCurrencyCode($Currency);
		$VAT_Number = '';
		if($result == 'cost'){
			$EORI_Number = 'GB019020854000';
			$UK_VAT_Number = 'GB 304984295'; 
			$DE_VAT_Number = 'DE 321777974'; 
			$FR_VAT_Number = 'FR 59850070509'; 
			$IT_VAT_Number = 'IT 10905930961'; 
			$CZ_VAT_Number = 'CZ 684972917';
			
			$STORE_NAME	   = $this->COST_STORE_NAME;
		    $STORE_ADDRESS = $this->COST_STORE_ADDRESS; 
			$input = array();
			if($Country == 'United Kingdom'){
				if($final_total > $lvcr_limit_shipping){
 					$input = array($DE_VAT_Number, $FR_VAT_Number,$IT_VAT_Number,$CZ_VAT_Number);	
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
  			}else if($Country == 'Germany'){
 				if($final_total > $lvcr_limit_shipping){
					$input = array($UK_VAT_Number, $FR_VAT_Number,$IT_VAT_Number,$CZ_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
			}else if($Country == 'France'){
				if($final_total > $lvcr_limit_shipping){
					$input = array($UK_VAT_Number, $DE_VAT_Number,$IT_VAT_Number,$CZ_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
			}else if($Country == 'Italy'){
				if($final_total > $lvcr_limit_shipping){
					$input = array($UK_VAT_Number,$FR_VAT_Number,$DE_VAT_Number,$CZ_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
 			}else if($Country == 'Spain'){
				if($final_total > $lvcr_limit_shipping){
					$input = array($DE_VAT_Number,$FR_VAT_Number,$IT_VAT_Number,$CZ_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
 			}else{
				if($final_total > $lvcr_limit_shipping){
					$input = array($UK_VAT_Number,$FR_VAT_Number,$IT_VAT_Number,$CZ_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
			}
			if(count($input) > 0){
 				  $rand_keys 	  = array_rand($input);
 				  $VAT_Number     = $input[$rand_keys]; 
			}
		
		}
		if($result == 'mare'){
			$EORI_Number = 'GB019434034000';
			$UK_VAT_Number = 'GB 307817693'; 
			$DE_VAT_Number = 'DE 323086773';
			$IT_VAT_Number = 'IT 11062310963'; // 16 Dec 2019
			$FR_VAT_Number = 'FR 07879900934'; 
		
			$STORE_NAME	   = $this->MARE_STORE_NAME;
		    $STORE_ADDRESS = $this->MARE_STORE_ADDRESS;
			$input = array();
			if($Country == 'United Kingdom'){
				if($final_total > $lvcr_limit_shipping){
					$input = array($DE_VAT_Number);	
				}else{
					$VAT_Number = $UK_VAT_Number;	
				}			
			}else if($Country == 'Germany'){
 				if($final_total > $lvcr_limit_shipping ){
					$input = array($UK_VAT_Number);
				}else{
					$VAT_Number = $UK_VAT_Number;	
				}
			}else if($Country == 'France'){
				if($final_total > $lvcr_limit_shipping ){
					$input = array($UK_VAT_Number, $DE_VAT_Number, $IT_VAT_Number);
				}else{
					$VAT_Number = $UK_VAT_Number;	
				} 
			}else if($Country == 'Italy'){
				if($final_total > $lvcr_limit_shipping ){
					$input = array($UK_VAT_Number,$DE_VAT_Number,$FR_VAT_Number);
				}else{
					$VAT_Number = $UK_VAT_Number;	
				}
 			}else if($Country == 'Spain'){
				if($final_total > $lvcr_limit_shipping){
					$input = array($DE_VAT_Number,$FR_VAT_Number, $IT_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
 			}else{
				$input = array($UK_VAT_Number,$DE_VAT_Number);
			}
			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}
			
			 
		}	
		if($result == 'rain'){
 			$EORI_Number = 'GB318649182000';
			$UK_VAT_Number = 'GB 318649182'; 
			$IT_VAT_Number = 'IT 11061000961';
			$DE_VAT_Number = 'DE 327173988';
			$FR_VAT_Number = '';
		  
			$STORE_NAME	   = $this->RAIN_STORE_NAME;
		    $STORE_ADDRESS = $this->RAIN_STORE_ADDRESS;
			
			$input = array();
			if($Country == 'United Kingdom'){
				if($final_total > $lvcr_limit_shipping){
					$input = array($DE_VAT_Number);	
				}else{
					$VAT_Number = $UK_VAT_Number;	
				}			
			}else if($Country == 'Germany'){
 				if($final_total > $lvcr_limit_shipping ){
					$input = array($UK_VAT_Number);
				}else{
					$VAT_Number = $UK_VAT_Number;	
				}
			}else if($Country == 'France'){
				if($final_total > $lvcr_limit_shipping ){
					$input = array($UK_VAT_Number);
				}else{
					$VAT_Number = $UK_VAT_Number;	
				} 
			}else if($Country == 'Italy' ){
				if($final_total > $lvcr_limit_shipping ){
					$input = array($UK_VAT_Number);
				}else{
					$VAT_Number = $UK_VAT_Number;	
				}
 			}else{
				$input = array($UK_VAT_Number);
			}
			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}
		}	
		/*----------Changes for new Invoice 26-09-2019--------*/		
		 
 		/*if($Country == 'United Kingdom' && $UK_VAT_Number != ''){
			$VAT_Number   = $UK_VAT_Number ;
		}else if($Country == 'Germany' && $DE_VAT_Number != '' ){
			$VAT_Number   = $DE_VAT_Number ;
		}*/
		
		 $txt = '';
		 if($final_total < $lvcr_limit_shipping){
			 $txt = 'Non Taxable Supply(Supplied from outside EU below deminimus)'; 
		 }else if($final_total > $lvcr_limit_shipping){
			 $txt = 'Non Taxable Supply( Export/Intra Community Transactions)'; 
		 }
		/* if($final_total > 21.9 && $tax_rate > 0){
			 $txt = 'VATable Transaction'; 
		 }else if($final_total < 22 && $tax_rate > 0){
			 $txt = 'VAT exempt( LVCR Limit)'; 
		 }else if($final_total < 22 && $tax_rate == 0){
			 $txt = 'VAT exempt( Export/Intra Community Transactions)'; 
		 }*/
								
		$templateHtml  ='<head><meta charset="UTF-8"><title>Invoice</title></head>';
		$templateHtml  .='<style>*{font-family:arial; font-size:12px;}</style>';
		$templateHtml  .='<div style="height:950px;margin-bottom:10px;">';						   
		$templateHtml  .='<table border="0" width="700px" align="center" cellpadding="0" cellspacing="0" style="margin:0px auto;">';
		$templateHtml  .='<tbody>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> Invoice </td>';
		$templateHtml  .='  </tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td valign="middle" align="left" width="50%">';
				$templateHtml  .='<p style="font-size:18px ;font-weight:bold;">'.$BILLING_ADDRESS_NAME.'</p>';
				$templateHtml  .='<p>'.$BILLING_ADDRESS.'</p>';
				/*if($VAT_Number){
					$templateHtml  .='<p><strong>'.$VAT_Number.'</strong></p>';
				}*/
 			$templateHtml  .='</td>';
			$templateHtml  .='<td valign="top" align="center" style="background:#dfeced; padding:10px;">';
			  $templateHtml  .='<div  style="border:1px solid #bdc9c9; padding:5px;">';
				$templateHtml  .='<table>';
				  
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td style="font-size:30px ;font-weight:bold;"> Paid</td>';
					 $templateHtml  .='</tr>';
					$templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2">';
						$templateHtml  .='<p>Sold by <strong> '.$STORE_NAME.'</strong></p>';
						if($VAT_Number){
							$templateHtml  .='<p>VAT #<strong>'.$VAT_Number.'</strong></p>'; 
						}
 						 
						$templateHtml  .='</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2" width="100%" height="1px" style="width:100%; height:1px; background:#bdc9c9;"> </td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong>Invoice date / Delivery date</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_DATE.'</td>';
					 $templateHtml  .='</tr>';
					  $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong><strong>Invoice #</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_NO.'</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong><strong>Total payable</strong></td>';
						$templateHtml  .='<td> '.$currency_code.$TOTAL.'</td>';
					 $templateHtml  .='</tr>'; 
				  
			   $templateHtml  .='</table>';
			   $templateHtml  .='</div>';
			   
			$templateHtml  .='</td>';
		 $templateHtml  .='</tr>';
		 
		 if($exp[$index] == 'fr'){
			 $amazon_url = 'www.amazon.fr';
		 }else if($exp[$index] == 'es'){
			 $amazon_url = 'www.amazon.es';
		 }else if($exp[$index] == 'it'){
			 $amazon_url = 'www.amazon.it';
		 }else if($exp[$index] == 'de'){
			 $amazon_url = 'www.amazon.de';
		 }else{
			 $amazon_url = 'www.amazon.co.uk';
		 }
		 
		 $templateHtml  .='<tr>';
		/*	$templateHtml  .='<td colspan="2" style="padding:10px 0px;"> For questions about your order, visit <a href="https://'.$amazon_url.'/contact-us">'.$amazon_url.'/contact-us</a></td>';*/
			$templateHtml  .='<td colspan="2" style="padding:10px 0px;"> For questions about your order, visit '.$amazon_url.'/contact-us</td>';
		 $templateHtml  .='</tr>';
		
		
		  $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="padding:5px 0px;"><strong>Tranction Type:</strong>'.$txt.'</td>';
		 $templateHtml  .='</tr>';
		 
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="background:#ddd; height:2px;"> </td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="height:10px;"> </td>';
		 $templateHtml  .='</tr>'; 
		 
		 $templateHtml  .='<tr><td colspan="2">';
		 $templateHtml  .='<table border="0" width="700px" align="center" cellpadding="0" cellspacing="0" >';
			
				$templateHtml  .='<tr>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">BILLING ADDRESS </th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">DELIVERY ADDRESS</th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">SOLD BY </th>';
			   $templateHtml  .='</tr>'; 
			   
			  $templateHtml  .='<tr>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$BILLING_ADDRESS_NAME.'</strong><br>'.$BILLING_ADDRESS.'</p>'; 
				  $templateHtml  .='</td>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$SHIPPING_ADDRESS_NAME.'</strong><br>'.$SHIPPING_ADDRESS.'</p>';
				  $templateHtml  .='</td>';
				 $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$STORE_NAME.'</strong><br>'.$STORE_ADDRESS;
					 if($VAT_Number){
						 $templateHtml  .='<br><strong>'.$VAT_Number.'</strong>';
					 }
					 $templateHtml  .='</p>';
					  
				  $templateHtml  .='</td>'; 
			  $templateHtml  .='</tr>';
				$templateHtml  .='<tr><td colspan="3" style="background:#ddd; height:1px;"> </td></tr>';
				$templateHtml  .='<tr>';
				   $templateHtml  .='<td colspan="3">';
					 $templateHtml  .='<p><strong>Order information</strong></p>';
					 $templateHtml  .='<p>Order date '.$ORDER_DATE.'';
					 $templateHtml  .='<br>Order # '.$ReferenceNum.'</p>';
				   $templateHtml  .='</td>';
				$templateHtml  .='</tr>';
		  
		  
		 $templateHtml  .='</table>';
		 $templateHtml  .='</td></tr>'; 
		$templateHtml  .='</table>';
		$templateHtml  .='</div>';
		
		$templateHtml  .='<div style="height:950px;margin-bottom:-1px;padding-top:100px;">';
		$templateHtml  .='<table width="700px" cellpadding="0" cellspacing="0" align="center">';
		$templateHtml  .='<tbody>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="5"></td>';
			$templateHtml  .='<td  style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> &nbsp; </td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="5"></td>';
			$templateHtml  .='<td  style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> Invoice</td>';
		 $templateHtml  .='</tr>';
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="4"></td>';
			$templateHtml  .='<td  colspan="2" style="font-weight:bold; padding:7px 5px; text-align:right"> Invoice # '.$INVOICE_NO.'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="6" style="font-size:24px; font-weight:bold; border-top:2px solid #ddd; border-bottom:2px solid #ddd; padding:7px 5px;"> Invoice Details</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<th valign="top" style="text-align:left; padding:10px 5px;" width="50%"> Description </th>';
			$templateHtml  .='<th valign="top" style="text-align:left; padding:10px 5px;" width="5%"> Qty </th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="10%"> Unit price<br>(excl. VAT)</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="11%"> VAT rate</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="12%"> Unit price<br> (incl. VAT)</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="12%"> Item subtotal<br> (incl. VAT)</th>';
		 $templateHtml  .='</tr>';
		 
		$total_item_vat_amount = 0;
		$total_item_cost 	   = 0;
 		$shipping_per_item 	   = 0;
		
		if($lvcr_limit_shipping >= $final_total){
			$shipping_per_item = $POSTAGE / count($items);
		}
		
		 foreach($items as $count => $item){ 
			 
			if(strlen($item->Title) > 10){
				$Title = $item->Title; 
			}else{
				$Title = $item->ChannelTitle;
			} 
			 
			if($shipping_per_item > 0){
				$shipping_per_item = ($shipping_per_item / $item->Quantity);	
			}
			$PricePerUnit = $item->PricePerUnit - $shipping_per_item;
			 
			 $item_vat_amount = $PricePerUnit - ($PricePerUnit/(1 + ($tax_rate/100)));
			 $item_cost = $PricePerUnit - $item_vat_amount;
			
			 $total_item_vat_amount  +=$item_vat_amount;
			 $total_item_cost  += $item_cost * $item->Quantity;
			  
			$templateHtml  .='<tr style="background:#ddd;">';
				$templateHtml  .='<td style="padding:10px 5px;">'.$Title.'</td>';
				$templateHtml  .='<td style="padding:10px 5px;">'.$item->Quantity.'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($item_cost,2).'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($PricePerUnit,2).'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format(($item->CostIncTax - 
				($shipping_per_item *$item->Quantity)),2).'</td>';
			 $templateHtml  .='</tr>';
			 
		 } 
		
		 $postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
		 $postage_cost = $POSTAGE - $postage_vat_amount;
		 
		 $templateHtml  .='<tr style="background:#ddd;">';

			$templateHtml  .='<td style="padding:10px 5px;">Shipping Charge</td>';
			$templateHtml  .='<td style="padding:10px 5px;"></td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($postage_cost,2).' </td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
		 $templateHtml  .='</tr>';
		
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td colspan="2" style="font-size:24px; font-weight:bold; padding:20px 0px;">Invoice total</td>';
			$templateHtml  .='<td style="font-size:24px; font-weight:bold; padding:20px 0px;" align="right">'.$currency_code.'&nbsp;'. $TOTAL.'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td colspan="4" style="height:2px; background:#ddd;"></td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="6" height="10px;"></td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th style="text-align:left; font-weight:bold; padding:10px 5px;">VAT rate </th>';
			$templateHtml  .='<th style="font-weight:bold; padding:10px 5px;" align="right">Item subtotal<br>(excl. VAT)</th>';
			$templateHtml  .='<th style="font-weight:bold; padding:10px 5px;" align="right">VAT subtotal</th>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px"> '.$tax_rate.'%</td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_cost + $postage_cost),2).'</td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_vat_amount + $postage_vat_amount),2).'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td style="padding:10px 5px; font-weight:bold;">Total</td>';
			$templateHtml  .='<td style="padding:10px 5px; font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_cost + $postage_cost),2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;  font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_vat_amount + $postage_vat_amount),2).'</td>';
		 $templateHtml  .='</tr>';
		$templateHtml  .='</tbody>';
		$templateHtml  .='</table>';
		$templateHtml  .='</div>';
		$templateHtml  .='</body>';
		 
	 
		return $templateHtml;	
	
	
	}
	
	public function getB2Chtml($order = null){ 
	
 		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdatesArchive');
		$this->loadModel('OpenOrdersArchive'); 
		$this->loadModel('SourceList');	
		$this->loadModel('Country');	
		$this->loadModel('Product');	 
		
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  	
		$saved_vat_number = $order->vat_number;  		 		
			
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		$BILLING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName));
		$SHIPPING_ADDRESS_NAME  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName));
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
 			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS  = ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$order->CustomerInfo->BillingAddress->Country ? $order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
 			
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS = ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $order->CustomerInfo->Address->Country ? $order->CustomerInfo->Address->Country : '';
			}			
		}		
		
		$EmailAddress	  = $order->CustomerInfo->Address->EmailAddress;
		$exp = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = $exp[$index];
		  		
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$_POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 
		$ReferenceNum	  = $order->GeneralInfo->ReferenceNum; 
 			
		$ORDER_DATE	  	 = date('d-m-Y', strtotime($order->GeneralInfo->ReceivedDate));
		$INVOICE_NO	 	 = 'INV-'.$OrderId ;
		$INVOICE_DATE 	 = $ORDER_DATE;//date('d-m-Y');
		
		$templateHtml 	  = NULL;
   
		//echo "</pre>";
		$item_lvcr_limit  = '21.9'; //EUR
		$lvcr_limit_shipping  = '26.29';		
		$final_total =  $TOTAL ;
		if($Currency == 'GBP'){
			$final_total = $TOTAL * (1.122);
			$POSTAGE = '3.81';//GBP 
		}
		
		if($final_total > 26.27){
			$postArr = array('4.29');
		}else if($final_total > 26 && $final_total < 26.28){
			$postArr = array('4.27','4.28','4.29');
		}else{
			$postArr = array('3.76','3.78','3.79','3.80','3.85','3.87','3.89','4.03','4.05','4.03','4.15','4.18','4.19','4.21','4.24','4.26','4.27','4.29');
		}
		$key  		= array_rand($postArr);
		$POSTAGE    =  $postArr[$key];//EUR
		
 		if($final_total > $lvcr_limit_shipping){
			$POSTAGE = $_POSTAGE;
 		} 
		
		
		//echo "==TT===";
		// echo "==TT===".($lvcr_limit_shipping  - $final_total);  exit;
		
		$sresult = $this->SourceList->find('first',['conditions' => ['LocationName' => $SubSource],'fields'=>['country_code']]);
		if(count( $sresult ) > 0){	
			$country_code   = $sresult['SourceList']['country_code'];
		}
		
		$store_country = [];
		$cresult = $this->Country->find('first',['conditions' => ['iso_2' => $country_code],'fields'=>['name','custom_name']]);
		if(count( $cresult ) > 0){	
			$store_country  = [$cresult['Country']['name'], $cresult['Country']['custom_name']];
		}
		
		//	$country_code = $this->getCountryCode($Country);
	
		$tax_rate = 0; $vat_amount = 0;
		 
  		/*if(in_array($Country,$store_country) && $final_total > 21.9){
			$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
			if(count( $thsresult ) > 0){				
				$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
			}
		}*/
		 
		$currency_code = $this->getCurrencyCode($Currency);
		$VAT_Number = '';
		if($result == 'cost'){
			$EORI_Number = 'GB019020854000';
			$UK_VAT_Number = 'GB 304984295'; 
			$DE_VAT_Number = 'DE 321777974'; 
			$FR_VAT_Number = 'FR 59850070509'; 
			$IT_VAT_Number = 'IT 10905930961'; 
			$CZ_VAT_Number = 'CZ 684972917';
			
			$STORE_NAME	   = $this->COST_STORE_NAME;
		    $STORE_ADDRESS = $this->COST_STORE_ADDRESS; 
			$input = array();
			if($Country == 'United Kingdom'){
				if($final_total > $lvcr_limit_shipping){
 					$input = array($DE_VAT_Number, $FR_VAT_Number,$IT_VAT_Number,$CZ_VAT_Number);	
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
  			}else if($Country == 'Germany'){
 				if($final_total > $lvcr_limit_shipping){
					$input = array($UK_VAT_Number, $FR_VAT_Number,$IT_VAT_Number,$CZ_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
			}else if($Country == 'France'){
				if($final_total > $lvcr_limit_shipping){
					$input = array($UK_VAT_Number, $DE_VAT_Number,$IT_VAT_Number,$CZ_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
			}else if($Country == 'Italy'){
				if($final_total > $lvcr_limit_shipping){
					$input = array($UK_VAT_Number,$FR_VAT_Number,$DE_VAT_Number,$CZ_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
 			}else if($Country == 'Spain'){
				if($final_total > $lvcr_limit_shipping){
					$input = array($DE_VAT_Number,$FR_VAT_Number,$IT_VAT_Number,$CZ_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
 			}else{
				if($final_total > $lvcr_limit_shipping){
					$input = array($UK_VAT_Number,$FR_VAT_Number,$CZ_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
			}
			if(count($input) > 0){
 				  $rand_keys 	  = array_rand($input);
 				  $VAT_Number     = $input[$rand_keys]; 
			}
		
		}
		if($result == 'mare'){
			$EORI_Number = 'GB019434034000';
			$UK_VAT_Number = 'GB 307817693'; 
			$DE_VAT_Number = 'DE 323086773';
			$IT_VAT_Number = 'IT 11062310963'; // 16 Dec 2019
			$FR_VAT_Number = 'FR 07879900934'; 
		
			$STORE_NAME	   = $this->MARE_STORE_NAME;
		    $STORE_ADDRESS = $this->MARE_STORE_ADDRESS;
			$input = array();
			if($Country == 'United Kingdom'){
				if($final_total > $lvcr_limit_shipping){
					$input = array($DE_VAT_Number);	
				}else{
					$VAT_Number = $UK_VAT_Number;	
				}			
			}else if($Country == 'Germany'){
 				if($final_total > $lvcr_limit_shipping ){
					$input = array($UK_VAT_Number);
				}else{
					$VAT_Number = $UK_VAT_Number;	
				}
			}else if($Country == 'France'){
				if($final_total > $lvcr_limit_shipping ){
					$input = array($UK_VAT_Number, $DE_VAT_Number);
				}else{
					$VAT_Number = $UK_VAT_Number;	
				} 
			}else if($Country == 'Italy'){
				if($final_total > $lvcr_limit_shipping ){
					$input = array($UK_VAT_Number,$DE_VAT_Number);
				}else{
					$VAT_Number = $UK_VAT_Number;	
				}
 			}else if($Country == 'Spain'){
				if($final_total > $lvcr_limit_shipping){
					$input = array($DE_VAT_Number,$FR_VAT_Number);
				}else{
					$VAT_Number   = $UK_VAT_Number;
				}
 			}else{
				$input = array($UK_VAT_Number,$DE_VAT_Number);
			}
			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}			 
		}	
		
		if($result == 'rain'){
 			$EORI_Number = 'GB318649182000';
			$UK_VAT_Number = 'GB 318649182'; 
			$IT_VAT_Number = 'IT 11061000961';
			$DE_VAT_Number = 'DE 327173988';
			$FR_VAT_Number = '';
		  
			$STORE_NAME	   = $this->RAIN_STORE_NAME;
		    $STORE_ADDRESS = $this->RAIN_STORE_ADDRESS;
			
			$input = array();
			if($Country == 'United Kingdom'){
				if($final_total > $lvcr_limit_shipping){
					$input = array($DE_VAT_Number);	
				}else{
					$VAT_Number = $UK_VAT_Number;	
				}			
			}else if($Country == 'Germany'){
 				if($final_total > $lvcr_limit_shipping ){
					$input = array($UK_VAT_Number);
				}else{
					$VAT_Number = $UK_VAT_Number;	
				}
			}else if($Country == 'France'){
				if($final_total > $lvcr_limit_shipping ){
					$input = array($UK_VAT_Number);
				}else{
					$VAT_Number = $UK_VAT_Number;	
				} 
			}else if($Country == 'Italy' ){
				if($final_total > $lvcr_limit_shipping ){
					$input = array($UK_VAT_Number);
				}else{
					$VAT_Number = $UK_VAT_Number;	
				}
 			}else{
				$input = array($UK_VAT_Number);
			}
			if(count($input) > 0){
 				$rand_keys 	  = array_rand($input);
				$VAT_Number   = $input[$rand_keys];
			}
		}	
 	 
		if(empty($saved_vat_number)){
			$this->OpenOrdersArchive->updateAll(array('vat_number' => "'".$VAT_Number."'"), array('num_order_id' => $OrderId));
	 	}else{
			$VAT_Number = $saved_vat_number;
		}
		
		 $txt = '';
		 if($final_total < $lvcr_limit_shipping){
			 $txt = 'Non Taxable Supply(Supplied from outside EU below deminimus)'; 
		 }else if($final_total > $lvcr_limit_shipping){
			 $txt = 'Non Taxable Supply( Export/Intra Community Transactions)'; 
		 }
		 
								
		$templateHtml  ='<head><meta charset="UTF-8"><title>Invoice</title></head>';
		$templateHtml  .='<style>*{font-family:Verdana; font-size:12px;}</style>';
		$templateHtml  .='<div style="height:950px;margin-bottom:10px;">';						   
		$templateHtml  .='<table border="0" width="700px" align="center" cellpadding="0" cellspacing="0" style="margin:0px auto;">';
		$templateHtml  .='<tbody>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> Invoice </td>';
		$templateHtml  .='  </tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td valign="middle" align="left" width="50%">';
				$templateHtml  .='<p style="font-size:17px ;font-weight:bold;">'.$BILLING_ADDRESS_NAME.'</p>';
				$templateHtml  .='<p>'.$BILLING_ADDRESS.'</p>';
				/*if($VAT_Number){
					$templateHtml  .='<p><strong>'.$VAT_Number.'</strong></p>';
				}*/
 			$templateHtml  .='</td>';
			$templateHtml  .='<td valign="top" align="center" style="padding:10px;">';//background:#dfeced; 
			  $templateHtml  .='<div  style="padding:5px;">';//border:1px solid #bdc9c9; 
				$templateHtml  .='<table>';
				  
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td style="font-size:32px ;font-weight:bold;"> Paid</td>';
					 $templateHtml  .='</tr>';
					$templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2">';
						$templateHtml  .='<p>Sold by <strong> '.$STORE_NAME.'</strong></p>';
						if($VAT_Number){
							$templateHtml  .='<p>VAT #<strong>'.$VAT_Number.'</strong></p>'; 
						}
 						 
						$templateHtml  .='</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td colspan="2" width="100%" height="1px" style="width:100%; height:1px; background:#bdc9c9;"> </td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong>Invoice date / Delivery date</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_DATE.'</td>';
					 $templateHtml  .='</tr>';
					  $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong><strong>Invoice #</strong></td>';
						$templateHtml  .='<td> '.$INVOICE_NO.'</td>';
					 $templateHtml  .='</tr>';
					 $templateHtml  .='<tr>';
						$templateHtml  .='<td> <strong><strong>Total payable</strong></td>';
						$templateHtml  .='<td> '.$currency_code.$TOTAL.'</td>';
					 $templateHtml  .='</tr>'; 
				  
			   $templateHtml  .='</table>';
			   $templateHtml  .='</div>';
			   
			$templateHtml  .='</td>';
		 $templateHtml  .='</tr>';
		 
		 if($exp[$index] == 'fr'){
			 $amazon_url = 'www.amazon.fr';
		 }else if($exp[$index] == 'es'){
			 $amazon_url = 'www.amazon.es';
		 }else if($exp[$index] == 'it'){
			 $amazon_url = 'www.amazon.it';
		 }else if($exp[$index] == 'de'){
			 $amazon_url = 'www.amazon.de';
		 }else{
			 $amazon_url = 'www.amazon.co.uk';
		 }
		
		 if($order->GeneralInfo->Source == 'AMAZON'){
			 $templateHtml  .='<tr>';		 
				$templateHtml  .='<td colspan="2" style="padding:10px 0px;"> For questions about your order, visit '.$amazon_url.'/contact-us</td>';
			 $templateHtml  .='</tr>';
		 }
		 
		
		
		  $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="padding:5px 0px;"><strong>Tranction Type : </strong>'.$txt.'</td>';
		 $templateHtml  .='</tr>';
		 
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="background:#ddd; height:2px;"> </td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="2" style="height:10px;"> </td>';
		 $templateHtml  .='</tr>'; 
		 
		 $templateHtml  .='<tr><td colspan="2">';
		 $templateHtml  .='<table border="0" width="700px" align="center" cellpadding="0" cellspacing="0" >';

			
				$templateHtml  .='<tr>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">BILLING ADDRESS </th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">DELIVERY ADDRESS</th>';
				  $templateHtml  .='<th style="text-align:left; font-weight:bold;">SOLD BY </th>';
			   $templateHtml  .='</tr>'; 
			   
			  $templateHtml  .='<tr>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$BILLING_ADDRESS_NAME.'</strong><br>'.$BILLING_ADDRESS.'</p>'; 
				  $templateHtml  .='</td>';
				  $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$SHIPPING_ADDRESS_NAME.'</strong><br>'.$SHIPPING_ADDRESS.'</p>';
				  $templateHtml  .='</td>';
				 $templateHtml  .='<td valign="top">';
					 $templateHtml  .='<p><strong>'.$STORE_NAME.'</strong><br>'.$STORE_ADDRESS;
					 if($VAT_Number){
						 $templateHtml  .='<br><strong>'.$VAT_Number.'</strong>';
					 }
					 $templateHtml  .='</p>';
					  
				  $templateHtml  .='</td>'; 
			  $templateHtml  .='</tr>';
				$templateHtml  .='<tr><td colspan="3" style="background:#ddd; height:1px;"> </td></tr>';
				$templateHtml  .='<tr>';
				   $templateHtml  .='<td colspan="3">';
					 $templateHtml  .='<p><strong>Order information</strong></p>';
					 $templateHtml  .='<p>Order date '.$ORDER_DATE.'';
					 $templateHtml  .='<br>Order # '.$ReferenceNum.'</p>';
				   $templateHtml  .='</td>';
				$templateHtml  .='</tr>';
		  
		  
		 $templateHtml  .='</table>';
		 $templateHtml  .='</td></tr>'; 
		$templateHtml  .='</table>';
		$templateHtml  .='</div>';
		
		$templateHtml  .='<div style="height:950px;margin-bottom:-1px;padding-top:100px;">';
		$templateHtml  .='<table width="700px" cellpadding="0" cellspacing="0" align="center">';
		$templateHtml  .='<tbody>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="5"></td>';
			$templateHtml  .='<td  style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> &nbsp; </td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="5"></td>';
			$templateHtml  .='<td  style="font-size:24px; font-weight:bold; padding:7px 5px; text-align:right"> Invoice</td>';
		 $templateHtml  .='</tr>';
		 
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="4"></td>';
			$templateHtml  .='<td  colspan="2" style="font-weight:bold; padding:7px 5px; text-align:right"> Invoice # '.$INVOICE_NO.'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="6" style="font-size:24px; font-weight:bold; border-top:2px solid #ddd; border-bottom:2px solid #ddd; padding:7px 5px;"> Invoice Details</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<th valign="top" style="text-align:left; padding:10px 5px;" width="50%"> Description </th>';
			$templateHtml  .='<th valign="top" style="text-align:left; padding:10px 5px;" width="5%"> Qty </th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="10%"> Unit price<br>(excl. VAT)</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="11%"> VAT rate</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="12%"> Unit price<br> (incl. VAT)</th>';
			$templateHtml  .='<th valign="top" style="padding:10px 5px;" align="right" width="12%"> Item subtotal<br> (incl. VAT)</th>';
		 $templateHtml  .='</tr>';
		 
		$total_item_vat_amount = 0;
		$total_item_cost 	   = 0;
 		$shipping_per_item 	   = 0;
		
		if($lvcr_limit_shipping >= $final_total){
			$shipping_per_item = $POSTAGE / count($items);
		}
		
		 foreach($items as $count => $item){ 
			 
			if(strlen($item->Title) > 10){
				$Title = $item->Title; 
			}else{
				$Title = $item->ChannelTitle;
			} 
			 
			if($shipping_per_item > 0){
				$shipping_per_item = ($shipping_per_item / $item->Quantity);	
			}
			$PricePerUnit = $item->PricePerUnit - $shipping_per_item;
			 
			 $item_vat_amount = $PricePerUnit - ($PricePerUnit/(1 + ($tax_rate/100)));
			 $item_cost = $PricePerUnit - $item_vat_amount;
			
			 $total_item_vat_amount  +=$item_vat_amount;
			 $total_item_cost  += $item_cost * $item->Quantity;
			  
			$templateHtml  .='<tr style="background:#ddd;">';
				$templateHtml  .='<td style="padding:10px 5px;">'.$Title.'</td>';
				$templateHtml  .='<td style="padding:10px 5px;">'.$item->Quantity.'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($item_cost,2).'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($PricePerUnit,2).'</td>';
				$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format(($item->CostIncTax - 
				($shipping_per_item *$item->Quantity)),2).'</td>';
			 $templateHtml  .='</tr>';
			 
		 } 
		
		 $postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
		 $postage_cost = $POSTAGE - $postage_vat_amount;
		 
		 $templateHtml  .='<tr style="background:#ddd;">';

			$templateHtml  .='<td style="padding:10px 5px;">Shipping Charge</td>';
			$templateHtml  .='<td style="padding:10px 5px;"></td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($postage_cost,2).' </td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$tax_rate.'%</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format($POSTAGE,2).'</td>';
		 $templateHtml  .='</tr>';
		
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td colspan="2" style="font-size:24px; font-weight:bold; padding:20px 0px;">Invoice total</td>';
			$templateHtml  .='<td style="font-size:24px; font-weight:bold; padding:20px 0px;" align="right">'.$currency_code.'&nbsp;'. $TOTAL.'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td colspan="4" style="height:2px; background:#ddd;"></td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td colspan="6" height="10px;"></td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th> </th>';
			$templateHtml  .='<th style="text-align:left; font-weight:bold; padding:10px 5px;">VAT rate </th>';
			$templateHtml  .='<th style="font-weight:bold; padding:10px 5px;" align="right">Item subtotal<br>(excl. VAT)</th>';
			$templateHtml  .='<th style="font-weight:bold; padding:10px 5px;" align="right">VAT subtotal</th>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px"> '.$tax_rate.'%</td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_cost + $postage_cost),2).'</td>';
			$templateHtml  .='<td style="background:#ddd;padding:10px 5px;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_vat_amount + $postage_vat_amount),2).'</td>';
		 $templateHtml  .='</tr>';
		 $templateHtml  .='<tr>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td></td>';
			$templateHtml  .='<td style="padding:10px 5px; font-weight:bold;">Total</td>';
			$templateHtml  .='<td style="padding:10px 5px; font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_cost + $postage_cost),2).'</td>';
			$templateHtml  .='<td style="padding:10px 5px;  font-weight:bold;" align="right">'.$currency_code.'&nbsp;'.number_format(($total_item_vat_amount + $postage_vat_amount),2).'</td>';
		 $templateHtml  .='</tr>';
		$templateHtml  .='</tbody>';
		$templateHtml  .='</table>';
		$templateHtml  .='</div>';
		$templateHtml  .='</body>';
		 
	  
		return $templateHtml;	
	
	
	}
  	
	public function dateFormat($date = null, $country = null) 
	{
		$date = '2019-03-05';
		$country = 'fr';
		if($country == 'fr'){
		
 				$order_date = date('D. M d Y', strtotime($date));
				$english_days = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
				$french_days = array('lun', 'mar', 'mer', 'jeu', 'ven', 'sam', 'dim');
				//$french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
				$english_months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
				//$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
				$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
				
			
		echo	str_replace($english_months, $french_months, str_replace($english_days, $french_days, $order_date )  );
			
 		}
		
	
		//return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date) ) ) );
	}
	
	public function getStoreData($SubSource = null, $country = null){
		$store_name = '';
		$store = strtolower(substr($SubSource, 0, 4));
		$europe = array('uk','es','it','fr','de');
		if($store == 'cost' && in_array($country,$europe)){
			$store_name = 'Cost-Dropper';
		}else if($store == 'mare' && in_array($country,$europe)){
			$store_name = 'EbuyerExpress';
		}else if($store == 'rain' && in_array($country,$europe)){
			$store_name = 'RRRetail';
		}else if($store == 'bbd_' && in_array($country,$europe)){
			$store_name = 'BBDEU';
		}else if($store == 'cost' && $country == 'ca'){
			$store_name = 'CanadaEmporium';
		}else if($store == 'cost' && $country == 'com'){
			$store_name = 'USABuyer';
		}
		return $store_name;
	}
	
	public function getListingId($SubSource = null, $seller_sku = null){
	
 		$store = strtolower(substr($SubSource, 0, 4));
		
		if($store == 'cost'){
			$url = 'http://cost-dropper.com/Webservice/getListingId'; 
		}else if($store == 'mare'){
			$url = 'http://ebuyer-express.com/Webservice/getListingId'; 
		}
		
		$channel_sku['seller_sku'] = $seller_sku;
		
		$curl = curl_init(); 
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FAILONERROR, true); 
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
		curl_setopt($curl, CURLOPT_POST, count($channel_sku));
		curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($channel_sku));  
		$result = curl_exec($curl); 						 
	 	$error = curl_error($curl); 
		$info = curl_getinfo($curl);
		curl_close($curl);	
		$data = json_decode($result);
		 
		if(count($data) < 1){
			$data = '0612UINKSC5';
		}
		 
 		return $data ;
	}
	
	public function getDHLInvoice($order_id = null){
				 
		$this->loadModel('MergeUpdatesArchive');
		$waybill_number = '';
		$result = $this->MergeUpdatesArchive->find('first', array('conditions' => array('order_id' => $order_id),'fields'=>['track_id']));
		if(count($result) > 0){
			$waybill_number = $result['MergeUpdatesArchive']['track_id'];
		}
		//$order = $this->getOrderByNumId_openorder( $order_id );	
		$order = $this->getOrderByNumIdDhl( $order_id );	
		 
 
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();	
		$result 		=	'';
		$htmlTemplate	=	'';	

		$htmlTemplate	=	$this->getTemplateDHL($order,$waybill_number);		
		$SubSource		=	$order->GeneralInfo->SubSource;
	
		 $result			=	ucfirst(strtolower(substr($SubSource, 0, 4)));
		
		/**************** for tempplate *******************/
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
	 	$html .= '<body>'.$htmlTemplate.'</body>';
	 		
		$name	= $order_id.'.pdf';							
		$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
		$dompdf->render();			
						
		$file_to_save = WWW_ROOT .'img/dhl/invoice_'.$name;
		 
		//save the pdf file on the server
		file_put_contents($file_to_save, $dompdf->output()); 
		
		/*-----------------DHL AMAZON SLIP----------------------*/	
	 	$this->getDHLSlip($order_id);
		
 		exit;		 
			
	}	
	public function Address(){
	
		$this->loadModel('InvoiceAddress');

		$data['order_id'] 	= $this->request->data['order_id'];
		$field 				= $this->request->data['field'];
		$data[$field] 		= $this->request->data['val'];
				
		if($this->Auth->user('username')){
			$data['username']  = $this->Auth->user('username');
		}
		
		$conditions = array('InvoiceAddress.order_id' => $this->request->data['order_id']);
														
		if ($this->InvoiceAddress->hasAny($conditions)){
			foreach($data as $field => $val){
				$DataUpdate[$field] = "'".$val."'";								
			}
			$this->InvoiceAddress->updateAll( $DataUpdate, $conditions );
			$msg['msg'] = 'updated';
		}else{
			$this->InvoiceAddress->saveAll($data);
			$msg['msg'] = 'saved';			
		}
	echo json_encode($msg);
	exit;
	
	}
	public function getCountryCode($Country = null){
		$code = '';
		if($Country == 'United Kingdom'){
			$code = 'gb';
		}else if($Country == 'Germany'){
			$code = 'de';
		}else if($Country == 'France'){
			$code = 'fr';
		}else if($Country == 'Italy'){
			$code = 'it';
		}else if($Country == 'Spain'){
			$code = 'es';
		}else if($Country == 'Australia'){
			$code = 'au';
		}else if($Country == 'Canada'){
			$code = 'ca';
		}else if($Country == 'United States'){
			$code = 'us';
		}
		return $code;
	}
	public function getCurrencyCode($currency  = null){
		$code = $currency;
		if($currency  == 'GBP'){
			$code = '&pound;';
		}else if($currency  == 'EUR'){
			$code = '&euro;';
		}else if($currency  == 'USD'){
			$code = '$';
		}else if($currency  == 'CAD'){
			$code = 'CAD$';
		} 
		return $code;
	}
	public function replaceFrenchChar($string = null){
			
		$unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E','Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Nº'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U','Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c','è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n','nº'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o','ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y','ü'=>'u','º'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}	
	
  	
}

?>

