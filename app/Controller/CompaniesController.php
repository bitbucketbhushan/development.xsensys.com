<?php

class CompaniesController extends AppController
{
    
    var $name = "Companies";
    
    var $components = array('Session', 'Common', 'Upload','Auth');
    
    var $helpers = array('Html','Form','Common','Session');
    
    public function addCompany()
    {
		$this->layout = 'index';
		$flag = false;
        $setNewFlag = 0;
        
		if( !empty( $this->request->data ) )
				{
					$this->Company->set( $this->request->data );
						if( $this->Company->validates( $this->request->data ) )
						{
							   $flag = true;                                      
						}
						else
						{
						   $flag = false;
						   $setNewFlag = 1;
						   $error = $this->Company->validationErrors;
						}
						if( $setNewFlag == 0 )
							 {
								 $this->request->data['updated_by'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
								 $this->Company->saveAll( $this->request->data );
								 $this->Session->setflash( 'Company Added Successfully.', 'flash_success' );
								 $this->redirect( array( 'controller' => '/Manage/Company/showAllCompany' ) );
							 }
				}
				
		$this->set( 'title', 'Add Company' );
	}
	
	public function editCompany( $id = null)
	{
		
		$this->layout = 'index';
		$this->loadModel( 'Company' );
		$companyDetail	=	$this->Company->find('first', array( 'conditions' => array( 'Company.id' =>  $id ) ) );
		$this->set( 'title', 'Edit Company' );
		$this->request->data['updated_by'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
		$this->request->data = $companyDetail;
		
	}
	
	public function showAllCompany()
	{
		$this->layout = 'index';
		/*$params = array( 'conditions' => array( 
				'status' => 1 
					)
				);*/
		$allCompanyDetails 	=	$this->Company->find( 'all' );
		$this->set( 'allCompanyDetails', $allCompanyDetails );
		$this->set( 'title', 'Company List' );
		
	}
	
	public function Location($company_id = 0)
	{
		$this->layout = 'index';
		$this->loadModel( 'Company' );
		$this->loadModel( 'CompanyLocation' );
		
		$title =  'Company Locations';
		$companyDetail	=	$this->Company->find('first', array( 'conditions' => array( 'Company.id' =>  $company_id ) ) );
		if(count($companyDetail) > 0){
			$title = $companyDetail['Company']['company_name'];
		}
		
		$allCompanyDetails = $this->CompanyLocation->find( 'all',array('conditions' => array('company_id' =>$company_id)) );
		$this->set( 'allCompanyDetails', $allCompanyDetails );
		$this->set( 'title', $title);
		
	}
	
	public function addLocation($company_id =0)
    {
		$this->layout = 'index';
		$this->loadModel( 'Country' );
		$this->loadModel( 'CompanyLocation' );
		$flag = false;
        $setNewFlag = 0;
        
		$country = $this->Country->find('all', array( 'fields' => array('name','iso_2' ) ) );
		
		if( !empty( $this->request->data ) )
		{ 
					$this->CompanyLocation->set( $this->request->data );
						if( $this->CompanyLocation->validates( $this->request->data ) )
						{
							   $flag = true;                                      
						}
						else
						{
						   $flag = false;
						   $setNewFlag = 1;
						   $error = $this->CompanyLocation->validationErrors;
						}
						if( $setNewFlag == 0 )
							 {
								 $this->request->data['CompanyLocation']['updated_by'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
								 
 								if($this->data['CompanyLocation']['certificate']['name'] != '')
								{
									$name = $this->Session->read('Auth.User.username').'_'.date('Ymdhis').'_'.$this->request->data['CompanyLocation']['certificate']['name'];									
									$filename = WWW_ROOT. 'files'.DS.$name;
									move_uploaded_file($this->request->data['CompanyLocation']['certificate']['tmp_name'],$filename);  
									$this->request->data['CompanyLocation']['vat_certificate'] = $name;
									unset($this->request->data['CompanyLocation']['certificate']);
								}else{
 									unset($this->request->data['CompanyLocation']['certificate']);
 								}
			
 								 $this->CompanyLocation->saveAll( $this->request->data );
								 $this->Session->setflash( 'Company Location Added Successfully.', 'flash_success' );
								 $this->redirect( array( 'controller' => '/Companies/Location/'.$company_id ) );
							 }
				}
				
		$this->set( 'title', 'Add Company Location' );
		$this->set( 'country', $country  );
	}
	
	public function editLocation( $id = null)
	{
		
		$this->layout = 'index';
		$this->loadModel( 'CompanyLocation' );
		$this->loadModel( 'Country' );
		$country = $this->Country->find('all', array( 'fields' => array('name','iso_2' ) ) );
		$this->set( 'country', $country  );
		
		$companyDetail	=	$this->CompanyLocation->find('first', array( 'conditions' => array( 'CompanyLocation.id' =>  $id ) ) );
		$this->set( 'title', 'Edit Location' );
		$this->request->data['updated_by'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
		$this->request->data = $companyDetail;
		
	}
	
	public function addStore()
	{
		$this->layout = 'index';
		$flag = false;
	    $setNewFlag = 0;
	    
	    $this->loadModel( 'Store' );
        
		if( !empty( $this->request->data ) )
				{
					$this->Store->set( $this->request->data );
						if( $this->Store->validates( $this->request->data ) )
						{
							   $flag = true;                                      
						}
						else
						{
						   $flag = false;
						   $setNewFlag = 1;
						   $error = $this->Store->validationErrors;
						}
						if( $setNewFlag == 0 )
							 {
								 //pr( $this->request->data );
								 //exit;
								 $this->Store->saveAll( $this->request->data );
								 $this->Session->setflash( 'Store Added Successfully.', 'flash_success' );
								 $this->redirect( array( 'controller' => '/Manage/Company/showAllStore' ) );
							 }
				}
		
		
		$params = array( 'conditions' => array( 
										'status' => 1 
						       ),
						 'fields' => array( 'company_name' 
						 )
				);
		$companyList	=	$this->Company->find( 'list',  $params );
		
		$this->set( 'companyList', $companyList );
		$this->set( 'title', 'Add Store' );
        $setNewFlag = 0;
	}
	
	public function showAllStore() 
	{
		$this->layout = 'index';
		 $this->loadModel( 'Store' );
		
		/*$params = array( 'conditions' => array( 
										'status' => 1 
						       )
				);*/
				
		$showStoreDetails	=	$this->Company->find( 'all');
		$this->set( 'showStoreDetails', $showStoreDetails );
		$this->set( 'title', 'All Store' );

	}
	
	public function editStore( $id = null)
	{
		$this->layout = 'index';
		$this->loadModel( 'Store' );
		
		$storeDetail	=	$this->Store->find('first', array( 'conditions' => array('Store.id' =>  $id ) ) );
		$params = array( 'conditions' => array( 
										'status' => 1 
						       ),
						 'fields' => array( 'company_name' 
						 )
				);
		$companyList	=	$this->Company->find( 'list',  $params );
		
		$this->set( 'companyList', $companyList );
		$this->set( 'title', 'Edit Store' );
		$this->request->data = $storeDetail;
	}
	
	/*public function deleteCompany()
	{
		$this->layout = 'index';
		$this->autoRender = false;
		$this->loadModel('Company');
		$this->loadModel('Store');
		$this->loadModel('Template');
		$this->loadModel('PackagingSlip');
		
		$id		=	$this->request->data['id'];
		
		$this->Company->updateAll( array('Company.status' => '0'),array('Company.id' => $id));*/
		//$paramStore				=	array( 'conditions' => array( 'Store.company_id' => $id ) );
		//$paramTempalte			=	array( 'conditions' => array( 'Template.company_id' => $id ) );
		//$paramPackageSlip		=	array( 'conditions' => array( 'PackagingSlip.company_id' => $id ) );
		
		//$this->Company->updateAll( $id );
		//$this->Store->updateAll( $paramStore );
		//$this->Template->updateAll( $paramTempalte );
		//$this->PackagingSlip->updateAll( $paramPackageSlip );
		/*echo "1";
		exit;
		
		
	}*/
	
	public function activeDeactiveCompany()
	{
		$this->layout = 'index';
		$this->autoRender = false;
		$this->loadModel('Company');
		$this->loadModel('Store');
		
		$id 	= $this->request->data['id'];
		$status = $this->request->data['status'];
		
		if( $status == 'active' )
		{
			$this->Session->setflash( 'Deactive Successfully.', 'flash_success' );
			$this->Company->updateAll( array('Company.status' => '0'),array('Company.id' => $id));
			$this->Store->updateAll( array('Store.status' => '0'),array('Store.company_id' => $id));
			echo "1";
			exit;
		}
		else
		{
			$this->Session->setflash( 'Active Successfully.', 'flash_success' );
			$this->Company->updateAll( array('Company.status' => '1'),array('Company.id' => $id));
			$this->Store->updateAll( array('Store.status' => '1'),array('Store.company_id' => $id));
			echo "2";
			exit;
		}
	}
	
	public function activeDeactiveStore()
	{
		
		$this->layout = 'index';
		$this->autoRender = false;
		$this->loadModel('Store');
		$this->loadModel('Company');
		
		$id 	= $this->request->data['id'];
		$status = $this->request->data['status'];
		
		if( $status == 'active' )
		{
			$this->Session->setflash( 'Deactive Successfully.', 'flash_success' );
			$this->Store->updateAll( array('Store.status' => '0'),array('Store.id' => $id));
			echo "1";
			exit;
		}
		else
		{
			$this->Session->setflash( 'Active Successfully.', 'flash_success' );
			$this->Store->updateAll( array('Store.status' => '1'),array('Store.id' => $id));
			echo "2";
			exit;
		}
	}
    
}

?>
