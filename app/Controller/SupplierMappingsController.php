<?php
error_reporting(0);
class SupplierMappingsController extends AppController
{
    
    var $name = "SupplierMappings";
    
    var $components = array('Session','Upload','Common');
    
    var $helpers = array('Html','Form','Session','Common');
    
  	public function index($sup_inc_id = 0) 
    {
		$this->layout = "index";
		
 		$this->loadModel( 'SupplierMapping' );  
		$this->loadModel( 'SupplierDetail' );  
		
		$supplier_code = '';
		$data = $this->SupplierDetail->find('first',array('conditions' => array('id' => $sup_inc_id),'fields'=>['supplier_code']));	
		if(count($data) > 0){
			  $supplier_code = $data['SupplierDetail']['supplier_code'];
		}	
		 
		if(isset($_REQUEST['search_key'])){
			
		 	$searchString =	addslashes(trim($_REQUEST['search_key']));
			$filter = ['','all','added','not_added'];
			if(!empty($_REQUEST['filter'])){
				$filter = trim($_REQUEST['filter']);
				
			}
			
			$this->paginate = array('conditions' =>array('OR'=>
					array(
						"SupplierMapping.master_sku" => $searchString ,
 						"SupplierMapping.supplier_sku LIKE '%{$searchString}%'"
					),
					"SupplierMapping.sup_inc_id" => $sup_inc_id ,
					//"SupplierMapping.status" => $filter 
					),
					'limit' => 50,'order'=>'id DESC');
	
		}else{
    		$this->paginate = array('conditions' => array('sup_inc_id' => $sup_inc_id),'group' => 'master_sku','limit' => 50,'order'=>'id DESC'); 
		}
		$supplier_mapping = $this->paginate('SupplierMapping');
		
		$this->set('supplier_mapping',$supplier_mapping); 
		$this->set('supplier_code',$supplier_code); 
  		//$this->render( 'index' ); 
	}
 	public function DownloadSku($id = 0) 
    {
		$this->layout = "index";  
 		$this->loadModel( 'Product' );
		$this->loadModel( 'SupplierMapping' ); 
 			 
		$data = $this->SupplierMapping->find('all',array('conditions' => array('sup_inc_id' => $id)));
		$file_name = 'logs/supplier_skus.csv';
		file_put_contents(WWW_ROOT .$file_name, "SKU,VIRTUAL STOCK,ADDED IN RANGE,STATUS,ADDED DATE\n",LOCK_EX);
		
		if(count($data) > 0) {
			foreach($data  as $v){
			 	$istatus = 'InActive';
				if($v['SupplierMapping']['istatus'] == 0){
					$istatus = 'Active';
				}
				$product = $this->Product->find('first',array('conditions' => array('product_sku' => $v['SupplierMapping']['master_sku']),'fields'=>['current_stock_level']));
				$stock = 0;
				if(count($product) > 0){
					$stock = $product['Product']['current_stock_level'];
				}
				
 				file_put_contents(WWW_ROOT .$file_name, $v['SupplierMapping']['master_sku'].",".$stock.",".$v['SupplierMapping']['status'].",".$istatus.",".date('Y-m-d',strtotime($v['SupplierMapping']['added_date']))."\n",FILE_APPEND | LOCK_EX);
			}
		}
		
		header('Content-Description: File Transfer');
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename='.basename($file_name));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize(file_name));
		ob_clean();
		flush();
		readfile($file_name);
		exit;
	
	exit;
	}
 	public function AddSupplierSku($supid = 0,$id = 0) 
    {
		$this->layout = "index";  
 		$this->loadModel( 'SupplierDetail' ); 
		$this->loadModel( 'SupplierMapping' ); 
		
			 
		$data = $this->SupplierMapping->find('first',array('conditions' => array('id' => $id)));	
		if(count($data) > 0){
			$this->request->data = $data;
		}
 		
 		$data = $this->SupplierDetail->find('first',array('conditions' => array('id' => $supid)));	
		if(count($data) > 0){
			$this->request->data['SupplierMapping']['sup_code'] = $data['SupplierDetail']['supplier_code'];
		}
		 
	}
	
	public function SupplierSkuDelete($supid = 0,$id = 0) 
    {
		$this->layout = "index";  
 		$this->loadModel( 'SupplierMapping' ); 
		try{	 
			$data = $this->SupplierMapping->find('first',array('conditions' => array('id' => $id)));	
			if(count($data) > 0){
 				$sql = "DELETE FROM `supplier_mappings` WHERE `id` = {$id}";
				$this->SupplierMapping->query($sql);	 
 				$this->Session->setflash( 'Supplier SKU deleted successfully.', 'flash_success' ); 
			}
		}catch(Exception $e) {
 			$this->Session->setflash( 'Message: ' .$e->getMessage(), 'flash_danger' );   
		}
		
		$this->redirect($this->referer());
		exit;
		
	}
	
	public function ChangeStatus($id = 0,$status = 0) 
    {
		$this->layout = "index";  
 		$this->loadModel( 'SupplierMapping' ); 
		try{	 
			$data = $this->SupplierMapping->find('first',array('conditions' => array('id' => $id)));	
			if(count($data) > 0){
 				$sql = "UPDATE `supplier_mappings` SET `istatus` = '".$status."' WHERE `id` = {$id}";
				$this->SupplierMapping->query($sql);	 
 				$this->Session->setflash( 'Supplier SKU status updated successfully.', 'flash_success' ); 
			}
		}catch(Exception $e) {
 			$this->Session->setflash( 'Message: ' .$e->getMessage(), 'flash_danger' );   
		}
		
		$this->redirect($this->referer());
		exit;
		
	}
	public function MoveToProduct($id = 0) 
    {
		$this->layout = "index";
		$this->loadModel( 'SupplierMapping' ); 
		$this->loadModel( 'Product' ); 
		
		$ch_sp = $this->SupplierMapping->find('first',array('conditions' => array('id' => $id)));
		
 		try{
 			if(count($ch_sp) > 0){
				$data = [];
				$data['product_name'] = $ch_sp['SupplierMapping']['sup_title'];
				$data['product_sku']  = $ch_sp['SupplierMapping']['master_sku'];
				$data['barcode'] 	  = $ch_sp['SupplierMapping']['barcode'];
				$data['description']  = $ch_sp['SupplierMapping']['sup_title'];
  				$data['weight']       = $ch_sp['SupplierMapping']['weight'];
 				$data['pack_size']    = $ch_sp['SupplierMapping']['pack_size'];
  				$data['product_status'] = 1;
 				$data['life_of_item'] = $ch_sp['SupplierMapping']['life_of_item'];
				
				$pro = $this->Product->find('first',array('conditions' => array('product_sku' => $ch_sp['SupplierMapping']['master_sku']),'fields'=>['id']));	
 				if(count($ch_sp) > 0){
					$data['id'] = $pro['Product']['id'];  
  					$this->Session->setflash( 'Product updated successfully.', 'flash_success' );   
				}else{
					$data['added_date']   = date('Y-m-d H:i:s');
					$data['added_by']     = $this->Session->read('Auth.User.username');
					$this->Session->setflash( 'Product added successfully.', 'flash_success' );  
				}
				$this->Product->saveAll($data);
				$this->SupplierMapping->query( "UPDATE `supplier_mappings` SET `status` = 'added' WHERE `id` = {$id};");
 			} 
		}catch(Exception $e) {
 			$this->Session->setflash( 'Message: ' .$e->getMessage(), 'flash_danger' );   
		}
 		$this->redirect($this->referer());
		exit;
	}
	public function SaveSupplierSku() 
    {
		$this->layout = "index";
		$this->loadModel( 'SupplierMapping' ); 
		$this->loadModel( 'Product' ); 
		
 		try{
		
			$pro = $this->Product->find('first',array('conditions' => array('product_sku' => $this->request->data['SupplierMapping']['master_sku'])));	 
			if(count($pro) == 0){
				/*$this->Session->setflash( 'Our SKU is not found in products!', 'flash_danger' );  
				$this->redirect($this->referer()); 
				exit;*/
				 $this->request->data['SupplierMapping']['status'] = 'not_added';
			}else{
				 $this->request->data['SupplierMapping']['status'] = 'added';
			}
			$username = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
			
			if(isset($this->request->data['SupplierMapping']['id'])){
 				$this->request->data['SupplierMapping']['updated_date'] = date('Y-m-d H:i:s');
				$this->request->data['SupplierMapping']['updated_by']   = $username;
				$this->SupplierMapping->saveAll($this->request->data);
				$this->Session->setflash( 'Supplier SKU updated successfully.', 'flash_success' );  
			}
			else 
			{
 			 
 				$this->request->data['SupplierMapping']['username']  = $username;
										
				$ch_sp = $this->SupplierMapping->find('first',array('conditions' => array('master_sku' => $this->request->data['SupplierMapping']['master_sku'],'supplier_sku' => $this->request->data['SupplierMapping']['supplier_sku'])));	 
				  
				if(count($ch_sp) == 0){
					$this->request->data['SupplierMapping']['added_date'] = date('Y-m-d H:i:s');
					$this->SupplierMapping->saveAll($this->request->data);	
					$this->Session->setflash( 'Supplier SKU added successfully.', 'flash_success' );   
				}else{
					$this->SupplierMapping->saveAll($this->request->data);	
					$this->Session->setflash( 'Our SKU & Supplier SKU Updated!', 'flash_success' );   
				}
			}
		}catch(Exception $e) {
 			$this->Session->setflash( 'Message: ' .$e->getMessage(), 'flash_danger' );   
		}
 		$this->redirect($this->referer());
		exit;
	}
 
 	public function UploadFile() {
	    
		$this->layout = "ajax";
		$this->loadModel( 'Product' );
		$this->loadModel( 'SupplierDetail' ); 
		$this->loadModel( 'SupplierMapping' ); 
		 
		$extensions  = array('csv');
 		$a = 0;  $u = 0;  $msg['msg'] = '';
		if(isset($_FILES["supplierfile"]))
		{ 
			$errors = array();
			$success = array();
			$warnings = array();
			
			if ($_FILES["supplierfile"]["error"] > 0)
			{
			  $errors[] = 'Error: ' . $_FILES["supplierfile"]["error"];
			}
			else
			{
					$pathinfo = pathinfo($_FILES["supplierfile"]["name"]);
		
					if (array_key_exists('filename', $pathinfo))
						$fileNameWithoutExt = $pathinfo['filename'];
					if (array_key_exists('extension', $pathinfo))
						$fileExtension = strtolower($pathinfo['extension']);
					
					
					if(in_array($fileExtension,$extensions)){
					
							$csvFile = $_FILES["supplierfile"]["tmp_name"];						
							$handle = fopen($csvFile, 'r');
							$cols   = array_flip(fgetcsv($handle));
							$dateTime = date('Y-m-d H:i:s');
							$r=1;
						 
							$ch_sd = $this->SupplierDetail->find('first',
									array('conditions' => 
										array(
											'id' => $this->request->data['sup_inc_id']
											)
										)
									);
							$sup_code = $ch_sd['SupplierDetail']['supplier_code'];
							 
  							while($data = fgetcsv($handle))
							{	
								$r++;	
											
 								if(trim($data[$cols['System SKU']]) == '') {						
									$errors[] =  $data[$cols['System SKU']]." Invalid Master SKU in the Row $r!" ;
								}
								if(trim($data[$cols['Supplier SKU']])== '') {
									$errors[] = $data[$cols['Supplier SKU']]." # $r Row have Invalid Supplier SKU!" ; 
								} 
								$username = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
								 
								$rowdata[] = array(	'master_sku' =>trim(utf8_encode($data[$cols['System SKU']])),
													'supplier_sku' =>trim(utf8_encode($data[$cols['Supplier SKU']])),
													'barcode' =>trim($data[$cols['Barcode']]),
													'title' =>trim($data[$cols['Our Title']]),
													'sup_title' =>trim($data[$cols['Supplier Title']]),
													'case_barcode' =>trim($data[$cols['Case Barcode']]),
													'case_quantity' =>trim($data[$cols['Case Quantity']]),
													'unit_of_sale' =>trim($data[$cols['Unit of sale']]),
													'weight' =>trim($data[$cols['Weight']]),
													'dimension' =>trim($data[$cols['dimension']]),
													'min_order_qty' =>trim($data[$cols['Moq']]),
													'end_sale_life' =>trim($data[$cols['End Of sale Life']]),
													'type_of_sku' =>trim($data[$cols['Type Of SKU']]),
													'product_type' =>trim($data[$cols['Product type']]),
													'price' =>trim($data[$cols['Price']]),
													'case_price' =>trim($data[$cols['Case Price']]),
													'pack_size' =>trim($data[$cols['Pack Size']]),
													'case_size' =>trim($data[$cols['Case size']]),
													'alternative_sku' =>trim($data[$cols['Alternative Sku']]),
													'status' =>trim( strtolower($data[$cols['Status']])) 
												);
							 }
							
							if(empty($errors)) 
							{
								$a = 0;  $u = 0; 
								foreach($rowdata as $k => $dataLine) {					
									$saveArray = [];
									
 									$saveArray['sup_inc_id'] = $this->request->data['sup_inc_id'];
									$saveArray['sup_code']	 = $sup_code ;
									$saveArray['title']	 	 = addslashes($dataLine['title']);
									$saveArray['sup_title']  = addslashes(utf8_decode($dataLine['sup_title']));
									$saveArray['barcode'] 	 = $dataLine['barcode'];
 									$saveArray['case_barcode'] = $dataLine['case_barcode'];
									$saveArray['case_quantity'] = $dataLine['case_quantity'];
									$saveArray['unit_of_sale'] = $dataLine['unit_of_sale'];
									$saveArray['weight'] 	= $dataLine['weight'];
									$saveArray['dimension'] = $dataLine['dimension'];
									$saveArray['min_order_qty'] = $dataLine['min_order_qty'];
									$saveArray['end_sale_life'] = $dataLine['end_sale_life'];
									$saveArray['type_of_sku'] = $dataLine['type_of_sku'];
									$saveArray['product_type'] = $dataLine['product_type'];
									$saveArray['price'] = $dataLine['price'];
									$saveArray['case_price'] = $dataLine['case_price'];								
 									$saveArray['pack_size'] = $dataLine['pack_size'];
									$saveArray['case_size'] = $dataLine['case_size'];
									$saveArray['alternative_sku'] = $dataLine['alternative_sku'];	
									$saveArray['status'] = $dataLine['status'];	
 
 									$ch_sp = $this->SupplierMapping->find('first',
											array('conditions' => 
												array(
													'master_sku' => $dataLine['master_sku'],
													'supplier_sku' => $dataLine['supplier_sku']
													)
												)
											);	 
											
									if(count($ch_sp) > 0){	
										$u++;		
										$saveArray['id'] = $ch_sp['SupplierMapping']['id'];
										$saveArray['updated_date'] = date('Y-m-d H:i:s');
										$saveArray['updated_by']   = $username;
									}
									else{		
 										$a++;	
										$saveArray['master_sku'] = $dataLine['master_sku'];
										$saveArray['supplier_sku'] = $dataLine['supplier_sku'];
										$saveArray['added_date'] = date('Y-m-d H:i:s');
										$saveArray['username'] = $username;
									}	
									
 									try{
									
										$pro = $this->Product->find('first',array('conditions' => array('product_sku' => $dataLine['master_sku'])));	 
										if(count($pro) == 0){
											 $saveArray['status'] = 'not_added';
										}else{
											 $saveArray['status'] = 'added';
										}
										$result =  $this->SupplierMapping->saveAll($saveArray) ;
									
										/*$pro = $this->Product->find('first',array('conditions' => array('product_sku' => $dataLine['master_sku']))); 
										if(count($pro) > 0){
											if(isset($saveArray['updated_by'])){
												$u++;
											}else{
												$a++;	
											}
 											$result =  $this->SupplierMapping->saveAll($saveArray) ;
										}
										else{
											$warnings[] = $dataLine['master_sku']." Invalid Master SKU!" ;
										}*/
									}
									catch(Exception $e) {
											$this->Session->setflash( 'Message: ' .$e->getMessage(), 'flash_danger' );   
									}
																	
								}						
								$date  = date('Ymd_His');					
								move_uploaded_file($_FILES["supplierfile"]["tmp_name"],'supplier_files/'. $fileNameWithoutExt.'~'.$this->Session->read('Auth.User.username').'~'.$date.'.'.$fileExtension); 
								//$success[] = $_FILES["supplierfile"]["name"].' File Uploaded.';
							}
								 
					 }else{		 	
						$errors[] = 'Invalid file type. We accept only CSV file!';
					}
			}
			
			
			if($a){
				$success[] =  '#'.$a.' Record added.';
			}
			if($u){ 
				$success[] =  '#'.$u.' Record Updated.';
			}
			
			if(count($success)>0){
				$success_msg = '';
				foreach($success as $sLine) {					
					$success_msg .= $sLine."\n";
				} 
				$msg['error'] = '';			
				$msg['msg'] .= '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$success_msg.'</div>';
			}	
			if(count($warnings)>0){
				$warnings_msg = '';
				foreach($warnings as $wLine) {					
					$warnings_msg .= $wLine."\n";
				} 
				$msg['error'] = 'yes';
				$msg['msg'] .= '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$warnings_msg.'</div>';
			}	
			if(count($errors)>0){
				$errors_msg = '';
				foreach($errors as $eLine) {					
					$errors_msg .= $eLine."<br>";
				} 
				$msg['error'] = 'yes';			
				$msg['msg'] .= '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$errors_msg.'</div>';
			}	
  		
		}
		 $this->Session->setflash( $msg['msg'], 'flash_success' );                      
         $this->redirect($this->referer());
		exit;
	}
	 
	 
	public function paginate_function($item_per_page, $current_page, $total_records, $total_pages,$data_name=false,$data_value=false,$sort_by=false)
	{
		$pagination = '';
		if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
			$pagination .= '<ul class="pagination">';
			
			$right_links    = $current_page + 3; 
			$previous       = $current_page - 3; //previous link 
			$next           = $current_page + 1; //next link
			$first_link     = true; //boolean var to decide our first link
			
			if($current_page > 1){
				$previous_link = ($previous > 0)? $previous : 1;
				$pagination .= '<li class="first"><a href="#" data-page="1" data-name="'.$data_name.'" order-by="'.$sort_by.'" data-value="'.$data_value.'" title="First">&laquo;</a></li>'; //first link
				$pagination .= '<li><a href="#" data-page="'.$previous_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Previous">&lt;</a></li>'; //previous link
					for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
						if($i > 0){
							$pagination .= '<li><a href="#" data-page="'.$i.'"  data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Page'.$i.'">'.$i.'</a></li>';
						}
					}     
				$first_link = false; //set first link to false
			} 
			
			if($first_link){ //if current active page is first link
				$pagination .= '<li class="first active"><span>'.$current_page.'</span></li>';
			}elseif($current_page == $total_pages){ //if it's the last active link
				$pagination .= '<li class="last active"><span>'.$current_page.'</span></li>';
			}else{ //regular current link
				$pagination .= '<li class="active"><span>'.$current_page.'</span></li>';
			}
					
			for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
				if($i<=$total_pages){
					$pagination .= '<li><a href="#" data-page="'.$i.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Page '.$i.'">'.$i.'</a></li>';
				}
			}
			if($current_page < $total_pages){ 
					$next_link = ($i > $total_pages) ? $total_pages : $i;
					$pagination .= '<li><a href="#" data-page="'.$next_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'" data-value="'.$data_value.'" title="Next">&gt;</a></li>'; //next link
					$pagination .= '<li class="last"><a href="#" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
			}
			
			$pagination .= '</ul>'; 
		}
		return $pagination; //return pagination links
	}
    
}

?>
