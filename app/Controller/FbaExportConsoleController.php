<?php
error_reporting(0); 
class FbaExportConsoleController extends AppController
{  
     
    var $name = "FbaExportConsole";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('fetchOrders' )); 
    }
 	
  	public function index( $id = null ) 
    {	
 		$this->layout = "index"; 	
		$this->loadModel('FbaShipment');		  
  		$this->FbaShipment->unbindModel(
					array(
						'hasMany' => array(
							'FbaShipmentItem'
						)
					)
			    );
				
 		$shipment =  $this->FbaShipment->find('first', array('conditions' => array('FbaShipment.id' => $id)));
		$this->set( "shipment", $shipment );	  
	} 
	
 	public function getSearchList($bar = null) 
    {	
		 $this->autoRender = false;
		 $this->layout = '';	
		
		$this->loadModel('FbaShipment');
		$this->loadModel('FbaItemLocation'); 
		$bar_code = trim($this->request->data['barcode']); 
 	 	$bar = $this->Components->load('Common')->getGlobalBarcode($bar_code);   
  		$this->FbaShipment->unbindModel(
					array(
						'hasMany' => array(
							'FbaShipmentItem'
						)
					)
			    );
 		$all_orders =  $this->FbaShipment->find('all', array(
			'joins' => array(
				array(
					'table' => 'fba_item_locations',
					'alias' => 'FbaItemLocation',
					'type' => 'INNER',		
					'conditions' => array(						
						  'FbaShipment.id = FbaItemLocation.shipment_inc_id',
						  'FbaItemLocation.shipment_inc_id' =>$this->request->data['inc_id'], 
						  'FbaItemLocation.barcode' => $bar,
						  'FbaItemLocation.status < ' => 3, 		    
					)
				)			
			), 
			'order'  => 'sku_type ASC',
			'fields' => array('FbaItemLocation.*','FbaShipment.*')
		 ) 
		);
		 
		if(count($all_orders) > 0){
			 
			/*$this->loadModel('FbaBundleSku'); 
			$bundle = $this->FbaBundleSku->find("all");
			foreach($bundle as $val){
				$bundle_mapping[$val['FbaBundleSku']['bundle_sku']][$val['FbaBundleSku']['master_sku']] =  array('qty'=>$val['FbaBundleSku']['qty'],'title'=>$val['FbaBundleSku']['title'],'bid'=>$val['FbaBundleSku']['bid']);
			}*/
			 
			  
			$html ='<div class="row head" style="clear:both;">';
				  $html .='<div class="col-sm-12">';
					$html .='<div class="col-12">SellerSku</div>';		
					$html .='<div class="col-15">Barcode / MasterSku</div>';	
					//$html .='<div class="col-10">Barcode</div>';			
					$html .='<div class="col-10">ASIN / FNSKU</div>';																				
					$html .='<div class="col-36">Title</div>';	
					$html .='<div class="col-8"><center>P. Qty<br><small style="font-size:10px;">(Preparation)</small></center></div>';	
					$html .='<div class="col-8"><center>Qty to Label</center></div>';
					$html .='<div class="col-10">Action</div>';	
				$html .='</div>';								
			$html .='</div>';	
			
			$html .='<div class="body" id="results">';
			
			foreach($all_orders as $count => $items){
				$cls = '';
				if($count == 0) { $cls = ' selected'; }
				
				$html .='<div class="row sku" id="r_'. $items['FbaItemLocation']['id'].'">';
				$barcode = $this->Components->load('Common')->getLocalBarcode($items['FbaItemLocation']['barcode']); 
				$master_sku = $items['FbaItemLocation']['sku'];
				$quantity = $items['FbaItemLocation']['quantity'];
				if($items['FbaItemLocation']['sku_type'] > 1){
					$map = $this->getSkuMapping($items['FbaItemLocation']['merchant_sku']);
					$master_sku = $map['sku'];
					$barcode = '-';
					$quantity = '-' ;
				}
				$data = $this->getItemData($items['FbaItemLocation']['shipment_inc_id'],$items['FbaItemLocation']['merchant_sku']);
				
				$html .='<div class="col-lg-12 '.$cls.'">	';
						$html .='<div class="col-12">'. $items['FbaItemLocation']['merchant_sku'].'</div>';	
						$html .='<div class="col-15">'. $master_sku."<br>".$barcode.'</div>';	
						$html .='<div class="col-10"><a href="https://www.amazon.com/dp/'.$data['asin'].'" target="_blank" style="color:#333399;">'.$data['asin']."</a><br>".$items['FbaItemLocation']['fnsku'].'</div>';	
						$html .='<div class="col-36"><small>'. $this->getProductTitle($master_sku).'</small></div>';	
						$html .='<div class="col-8"><center>1</center></small></div>';
						$html .='<div class="col-8"><center>'.$data['qty'] .'</center></small></div>';						
						$html .='<div class="col-10"><a href="'.Router::url('/', true).'FbaExportConsole/confirmBarcode/'.$bar_code.'/'.$items['FbaItemLocation']['shipment_inc_id'].'/'.$items['FbaItemLocation']['fnsku'].'" class="btn btn-warning btn-xs select" id="'. $items['FbaItemLocation']['id'].'">Select</a></div>';		
 					
				$html .='</div>';	
					
					
				 
				if($items['FbaItemLocation']['sku_type'] > 1){
				
					  $bundle_items =  $this->getLocationMapping($items['FbaItemLocation']['merchant_sku'],$items['FbaItemLocation']['shipment_inc_id']);
					  foreach($bundle_items as $bval){
							$html .='<div class="col-lg-12" style=" background-color:#BAEEF9;">'; 
							$html .='<div class="col-12">&nbsp;&nbsp;&nbsp;&nbsp;</div>';						
						
							$id = $bval['FbaItemLocation']['id'];//.'_'.$val['bid'];
							$name = $bval['FbaItemLocation']['fnsku'];//.'[qty]['.$val['bid'].']';
							
							$html .= '<div class="col-15" style="font-size:11px;">'.$bval['FbaItemLocation']['sku'].'</div>'; 	
							$html .= '<div class="col-10" style="font-size:11px;">'.$bval['FbaItemLocation']['barcode'].'</div>'; 												
							$html .= '<div class="col-35" style="font-size:11px;">'.$this->getFnskuTitle($items['FbaItemLocation']['fnsku']).'</div>';
							$html .= '<div class="col-3" for="'.$id .'">&nbsp;</div>';
							
 							$eexp  = explode('-',$master_sku);
							$ccount  = count($eexp);
							$qindex = $ccount  - 1; 
							$bunq   = $eexp[$qindex];
							$qstr = ''; $qstr2 ='';
							if($ccount == 3 ){
								$qt = $bval['FbaItemLocation']['quantity'] / $bunq;
								$qstr =	$qt .'x'.$bunq;
								$qstr2 = $bval['FbaItemLocation']['quantity'];								
							}else if($ccount > 3 ){
								$qstr  = $bval['FbaItemLocation']['quantity'] .'x 1';
								$qstr2 =($bval['FbaItemLocation']['quantity'] * 1);
							}	
							
							$html .= '<div class="col-10" style="font-size:11px;" id="l_'.$id.'">'. $qstr.'</div>';
 							
							$html .='<div class="col-7">'.$qstr2.'</div>';	
								
							//$html .= '<div class="col-10" style="text-align:right;padding-right:68px;"><span name="'.$name.'" id="span_'.$id.'">-</span></div>';
							$html .=  '</div>';
							} 
						
						/*foreach($bundle_mapping[$master_sku] as $sku => $val){
							$html .='<div class="col-lg-12" style=" background-color:#BAEEF9;">'; 
							$html .='<div class="col-12">&nbsp;&nbsp;&nbsp;&nbsp;</div>';
						
							$barcode = $this->getSkuBarcode($sku);												
						
							$id = $items['FbaItemLocation']['id'].'_'.$val['bid'];
							$name = $items['FbaItemLocation']['fnsku'].'[qty]['.$val['bid'].']';
							//$html .= '<div class="col-12" style="background-color:#BAEEF9; margin-left:0px !important;margin-right:0px !important; font-size:11px;border-top:1px dashed #bbb;">'; 
							$html .= '<div class="col-15" style="font-size:11px;">'.$sku.'</div>'; 	
							$html .= '<div class="col-10" style="font-size:11px;">'.$barcode.'</div>'; 												
							$html .= '<div class="col-39" style="font-size:11px;">'.$this->getFnskuTitle($items['FbaItemLocation']['fnsku']).'</div>';
							$html .= '<div class="col-8 bundle_'.$items['FbaItemLocation']['id'].'" for="'.$id .'" style="font-size:11px;">'.$val['qty'].'</div>';
							$html .= '<div class="col-5" style="font-size:11px;" id="l_'.$id.'">' 
							
							. $val['qty'] .'x'. $data['qty'] .'='. $val['qty'] * $data['qty']
							
							 .'</div>';
							
							$html .='<div class="col-10"><a href="'.Router::url('/', true).'FbaExportConsole/confirmBarcode/'.$items['FbaItemLocation']['id'].'" class="btn btn-warning btn-xs select" id="'. $items['FbaItemLocation']['id'].'">Select</a></div>';	
								
							//$html .= '<div class="col-10" style="text-align:right;padding-right:68px;"><span name="'.$name.'" id="span_'.$id.'">-</span></div>';
							$html .=  '</div>';
							} */
						 
					} 
						 
				$html .='</div>';	
			}
			
			$html .='</div>';
		}else{
			 $html = '<div class="alert alert-info">There are no SKU to process or picklist is not generated. </div>';
		}
		 
		$msg['data'] = $html;	
							
		echo json_encode($msg);
		exit;
		 
	} 
	
	public function getLocationMapping($seller_sku = null, $shipment_inc_id = null )
	{		
		$this->loadModel('FbaItemLocation');  	
		$_items = $this->FbaItemLocation->find('all', array('conditions'=>array('FbaItemLocation.merchant_sku'=>$seller_sku,'FbaItemLocation.shipment_inc_id'=>$shipment_inc_id)));
		if(count($_items) > 0){
			return  $_items;
		}else{
			return 0;
		}
	}
	
	public function getSkuMapping($seller_sku = null)
	{		
		$this->loadModel( 'Skumapping' );	
		$_items = $this->Skumapping->find('first', array('conditions'=>array('Skumapping.channel_sku'=>$seller_sku)));
		if(count($_items) > 0){
			return  array('sku'=>$_items['Skumapping']['sku'],'barcode'=>$_items['Skumapping']['barcode']);
		}else{
			return 0;
		}
	}
	
	public function updateBarcode()
	{		
		$this->loadModel( 'Product' );	
		$this->loadModel( 'Skumapping' );	
		$_products = $this->Product->find('all', array('fields'=>array('Product.product_sku','ProductDesc.barcode')) );
		
		foreach($_products as $val){
			
			$conditions = array('sku' => $val['Product']['product_sku']);	
			$mapdata['barcode'] = "'".$val['ProductDesc']['barcode']."'";	
			pr($mapdata);	
			$this->Skumapping->updateAll( $mapdata, $conditions );
			
		}		
	}
	public function getSkuBarcode($sku = null)
	{		
		$this->loadModel( 'Product' );	
		$_items = $this->Product->find('first', array('conditions'=>array('Product.product_sku' => $sku),'fields'=>array('ProductDesc.barcode')));
	 	if(count($_items) > 0){ 
			return  $this->Components->load('Common')->getLocalBarcode($_items['ProductDesc']['barcode']);
		}else{
			return 0;
		}
	}
	
	
	public function getHSCode()
	{		
		$this->loadModel( 'FbaPackaging' );			
		$_items = $this->FbaPackaging->find('all', array('group'=>'FbaPackaging.hs_code'));
		$d = array();
		if(count($_items) > 0){
			foreach($_items as $v){
				if($v['FbaPackaging']['hs_code'] !='') $d[] = $v['FbaPackaging']['hs_code'];
			}		
		} 
		return $d;	
		 
	}
	
	public function getCountryOrigin()
	{		
		$this->loadModel( 'FbaPackaging' );			
		$_items = $this->FbaPackaging->find('all', array('group'=>'FbaPackaging.country_of_origin'));
		$d = array();
		if(count($_items) > 0){
			foreach($_items as $v){
				if($v['FbaPackaging']['country_of_origin'] !='') $d[] = $v['FbaPackaging']['country_of_origin'];
			} 
		}
		return $d;
	}
	
	public function getCategory()
	{		
		$this->loadModel( 'FbaPackaging' );			
		$_items = $this->FbaPackaging->find('all', array('group'=>'FbaPackaging.category'));
		$d = array();
		if(count($_items) > 0){
			foreach($_items as $v){
				if($v['FbaPackaging']['category'] !='') $d[] = $v['FbaPackaging']['category'];
			}			
		} 
		return $d;
	}
	public function getPackaging()
	{		
		$this->loadModel( 'FbaPackaging' );			
		$_items = $this->FbaPackaging->find('all', array('group'=>'FbaPackaging.packaging'));
		$d = array();
		if(count($_items) > 0){
			foreach($_items as $v){
				if($v['FbaPackaging']['packaging'] !='') $d[] = $v['FbaPackaging']['packaging'];
			}			
		}
		return $d; 
	}
	
	public function getFbaPackaging($sku = null)
	{		
		$this->loadModel( 'FbaPackaging' );			
		$_items = $this->FbaPackaging->find('first', array('conditions'=>array('FbaPackaging.master_sku'=>$sku)));
		if(count($_items) > 0){
			return array('packaging'=>$_items['FbaPackaging']['packaging'],'hs_code'=>$_items['FbaPackaging']['hs_code'],'country_of_origin'=>$_items['FbaPackaging']['country_of_origin'],'category'=>$_items['FbaPackaging']['category']);
		}else{
			return array();
		}
	}
	
	public function getItemData($shipment_inc_id = null, $merchant_sku = null)
	{		
		$this->loadModel('FbaShipmentItem');
		 
  		$this->FbaShipment->unbindModel(
					array(
						'hasMany' => array(
							'FbaShipmentItem'
						)
					)
			    );
						
		$_items = $this->FbaShipmentItem->find('first', array('conditions'=>array('FbaShipmentItem.shipment_inc_id'=>$shipment_inc_id,'FbaShipmentItem.merchant_sku'=>$merchant_sku)));
		if(count($_items) > 0){
			return array('asin'=>$_items['FbaShipmentItem']['asin'],'qty'=>$_items['FbaShipmentItem']['shipped']);
		}else{
			return array();
		}
	}
	
	public function confirmBarcode($id = null){
	
		$this->layout = "index"; 	
		  
	}
 	
	public function confirmBarcodeAjax($barcode = null, $shipment_inc_id = null, $fnsku = null){
		$this->autoRender = false;
		$this->layout = '';	
 		$this->loadModel('FbaShipment');
		$this->loadModel('FbaItemLocation');  
	 	$bar = $this->Components->load('Common')->getGlobalBarcode($barcode);   
		$this->FbaShipment->unbindModel(
					array(
						'hasMany' => array(
							'FbaShipmentItem'
						)
					)
				);
		$all_orders =  $this->FbaShipment->find('all', array(
			'joins' => array(
				array(
					'table' => 'fba_item_locations',
					'alias' => 'FbaItemLocation',
					'type' => 'INNER',		
					'conditions' => array(						
						  'FbaShipment.id = FbaItemLocation.shipment_inc_id',
						  'FbaItemLocation.shipment_inc_id' =>$shipment_inc_id, 
						  'FbaItemLocation.barcode' => $bar,
						  'FbaItemLocation.fnsku' => $fnsku,
						  'FbaItemLocation.status < ' => 3, 		    
					)
				)			
			), 
			'order'  => 'sku_type ASC',
			 
			'fields' => array('FbaItemLocation.*','FbaShipment.*')
		 ) 
		);
		 $shipment_id = NULL; $fnsku = NULL;  
		if(count($all_orders) > 0){
		
  		$html ='<div class="row head" style="clear:both;">';
			  $html .='<div class="col-sm-12">';
				$html .='<div class="col-12">MerchantSku</div>';		
				$html .='<div class="col-15">MasterSku/Barcode</div>'; 
				$html .='<div class="col-12">ASIN/FNSKU</div>';																				
				$html .='<div class="col-28">Title</div>';	
				$html .='<div class="col-6"><center>P Qty</center></div>';	
				$html .='<div class="col-7"><center>Qty to Label</center></div>';	
				$html .='<div class="col-19"><center>Qty to Send</center></div>'; 
			$html .='</div>';								
		$html .='</div>';		
			
			$html .='<div class="body" id="results">';
			
			foreach($all_orders as $count => $items){
				$shipment_id = $items['FbaShipment']['shipment_id'];
				$fnsku = $items['FbaItemLocation']['fnsku'];
				$shipment='<div class="row">	
						<div class="col-sm-12" style="padding:5px; background-color:#F2F2F2;">
								<div class="col-sm-3"><small><strong>Shipment Id:</strong>'. $items['FbaShipment']['shipment_id'].'</small><input type="hidden" name="shipment_id" id="shipment_id" value="'. $items['FbaShipment']['shipment_id'].'"/></div>
								<div class="col-sm-3"><small><strong>Total Merchant SKU:</strong> '. $items['FbaShipment']['total_skus'].'</small></div>
								<div class="col-sm-2"><small><strong>Total Units:</strong> '. $items['FbaShipment']['total_units'].' </small></div>	
								<div class="col-sm-2"><small><strong>Pack List:</strong> '. $items['FbaShipment']['pack_list'].'</small></div>	
								<div class="col-sm-2"><small><strong>Date:</strong> '. $items['FbaShipment']['added_date'].'</small></div>									
								<div class="col-sm-3"><small><strong>Shipment:</strong> '. $items['FbaShipment']['shipment_name'].'</small></div>		
								<div class="col-sm-7"><small><strong>Ship To:</strong> '. $items['FbaShipment']['ship_to'].'</small></div>
								<div class="col-sm-2"><small><strong>Status:</strong> '. ucfirst($items['FbaShipment']['status']).'</small></div>
						</div> 
						</div>';
			 
 				$html .='<div class="row sku" id="r_'. $items['FbaItemLocation']['id'].'">';
				$barcode = $this->Components->load('Common')->getLocalBarcode($items['FbaItemLocation']['barcode']); 
				$master_sku = $items['FbaItemLocation']['sku'];
				$quantity = $items['FbaItemLocation']['quantity'];
				if($items['FbaItemLocation']['sku_type'] > 1){
					$map = $this->getSkuMapping($items['FbaItemLocation']['merchant_sku']);
					$master_sku = $map['sku'];
					$barcode = '-';
					$quantity = '-' ;
				}
				$data = $this->getItemData($items['FbaItemLocation']['shipment_inc_id'],$items['FbaItemLocation']['merchant_sku']);
				
				$html .='<div class="col-lg-12">	';
						$html .='<div class="col-12"><small>'. $items['FbaItemLocation']['merchant_sku'].'</small>';
							
						$html .='<select name="packaging" for="p" style="font-size:11px; padding:5px; border:1px solid #c6c6c6; width:100%;" onChange="updatePackHsOrigin(this,\''.$items['FbaItemLocation']['merchant_sku'].'\')">';
						$html .='<option value="">--No Packaging--</option>';
						  foreach($all_packaging as  $pack){
							if(isset($packdata['packaging']) && $packdata['packaging'] == $pack){
								$html .= '<option value="'.$pack.'" selected="selected">'.$pack.'</option>';
							}else{
								$html .= '<option value="'.$pack.'">'.$pack.'</option>';
							}
						 }
						$html .='<option value="add_new" style="font-weight:bold; color:#FF0000;">Add New Packaging</option>'	;
						$html .='</select>';
						$html .='<div id="p_'. $items['FbaItemLocation']['merchant_sku'] .'" style="display:none;padding-left: 0px;padding-right: 0px;"><input type="text" id="pin_'. $items['FbaItemLocation']['merchant_sku'] .'" style="width: 100px;float: left;"/><button type="button" class="btn btn-success btn-xs" title="Save" onClick="addPackHsOrigin(\'p\',\''. $items['FbaItemLocation']['merchant_sku'] .'\')" style="float: right;margin-top: 2px;"><i class="fa  fa-check-square"></i></button></div>';
						
						
						$html .='<input type="hidden" id="merchant_sku" value="'.$items['FbaItemLocation']['merchant_sku'] .'" />';
						$html .='<input type="hidden" id="fnsku" value="'.$items['FbaItemLocation']['fnsku'] .'" />';
				
					$html .='</div>';	
						
						
						
						$html .='<div class="col-15">';
						$html .='<div><small>'. $master_sku.'</small></div>';
						$html .='<div style="font-size:11px; border-top: 1px dashed #ccc;">'. $barcode.'</div>';
						
						/*------HS code-----*/
						 
						$html .='<div id="div_hs_'. $items['FbaItemLocation']['merchant_sku'] .'" style="font-size:11px; border-top: 1px dashed #ccc;">';
						
						
						$html .='<select name="hs_code" for="hs" style="font-size:11px; padding:5px; border:1px solid #c6c6c6; width:100%;" onChange="updatePackHsOrigin(this,\''.$items['FbaItemLocation']['merchant_sku'].'\')">';
					 
						$html .='<option value="">--No HS Code--</option>';
						
						  foreach($all_hs_code as  $pack){
							if(isset($packdata['hs_code']) && $packdata['hs_code'] == $pack){
								$html .= '<option value="'.$pack.'" selected="selected">'.$pack.'</option>';
							}else{
								$html .= '<option value="'.$pack.'">'.$pack.'</option>';
							}
						 }
								
						$html .='<option value="add_new" style="font-weight:bold; color:#FF0000;">Add New HS Code</option>'	;											 
						$html .='</select>';
 					 
																
						$html .='</div>';
						
						$html .='<div id="hs_'. $items['FbaItemLocation']['merchant_sku'] .'" style="display:none;"><input type="text" id="hsin_'. $items['FbaItemLocation']['merchant_sku'] .'" style="width:70%; float:left;"/>&nbsp;<button type="button" class="btn btn-info btn-xs" title="Save" onClick="addPackHsOrigin(\'hs\',\''. $items['FbaItemLocation']['merchant_sku'] .'\')"><i class="fa  fa-check-square"></i></button></div>';
						
						/*------End of HS code-----*/
					$html .='</div>';	
					
					$itemdata = $this->getItemData($items['FbaShipment']['id'],$items['FbaItemLocation']['merchant_sku']);
							
					$html .='<div class="col-12">';				
						$html .='<div><a href="https://www.amazon.com/dp/'. $itemdata['asin'].'" target="_blank" style="color:#333399;">'. $itemdata['asin'].'</a></div>';
						$html .='<div style="border-top:1px dashed #bbb; width:55%;">'. $items['FbaItemLocation']['fnsku'].'</div>';
						
						/*------Category-----*/
						$html .='<div id="div_ca_'. $items['FbaItemLocation']['merchant_sku'].'" style="font-size:11px;">';												
					 
						 $html .='<select name="category" for="ca" style="font-size:11px; padding:5px; border:1px solid #c6c6c6; width:100%;" onChange="updatePackHsOrigin(this,\''.$items['FbaItemLocation']['merchant_sku'].'\')">';
					 
						$html .='<option value="">--No Category--</option>'; 
						
						  foreach($all_category as  $pack){
							if(isset($packdata['category']) && $packdata['category'] == $pack){
								$html .= '<option value="'.$pack.'" selected="selected">'.$pack.'</option>';
							}else{ 
								$html .= '<option value="'.$pack.'">'.$pack.'</option>';
							}
						 }
								 
						$html .='<option value="add_new" style="font-weight:bold; color:#FF0000;">Add New Category</option>';											  
						$html .='</select>'; 
						
						$html .='</div>';
						
						$html .='<div id="ca_'. $items['FbaItemLocation']['merchant_sku'].'" style="display:none;"><input type="text" id="cain_'. $items['FbaItemLocation']['merchant_sku'].'" style="width:70%; float:left;"/>&nbsp;<button type="button" class="btn btn-info btn-xs" title="Save" onClick="addPackHsOrigin(\'ca\',\''. $items['FbaItemLocation']['merchant_sku'].'\')"><i class="fa  fa-check-square"></i></button></div>';
													
						/*------End of Category-----*/
												
					$html .='</div>';	
					
					$html .='<div class="col-28"><small>'. $this->getFnskuTitle($items['FbaItemLocation']['fnsku']).'</small>';
					
					/*------Country origin-----*/						
						$html .='<div id="div_co_'.$items['FbaItemLocation']['merchant_sku'].'" style="font-size:11px;">';												
						  										
 						 $html .='<select name="country_of_origin" for="co" style="font-size:11px; padding:5px; border:1px solid #c6c6c6; width:100%;" onChange="updatePackHsOrigin(this,\''.$items['FbaItemLocation']['merchant_sku'].'\')">';
					 
						$html .='<option value="">--No Country of origin--</option>';
						
						  foreach($all_country_origin as  $pack){
							if(isset($packdata['country_of_origin']) && $packdata['country_of_origin'] == $pack){
								$html .= '<option value="'.$pack.'" selected="selected">'.$pack.'</option>';
							}else{
								$html .= '<option value="'.$pack.'">'.$pack.'</option>';
							}
						 }
								
						$html .='<option value="add_new" style="font-weight:bold; color:#FF0000;">Add New CountryOrigin</option>';											 
						$html .='</select>'; 
						
						
						$html .='</div>';
						
						$html .='<div id="co_'. $items['FbaItemLocation']['merchant_sku'] .'" style="display:none;"><input type="text" id="coin_'. $items['FbaItemLocation']['merchant_sku'].'" style="width:70%; float:left;"/>&nbsp;<button type="button" class="btn btn-info btn-xs" title="Save" onClick="addPackHsOrigin(\'co\',\''. $items['FbaItemLocation']['merchant_sku'].'\')"><i class="fa  fa-check-square"></i></button></div>';
						
						/*------End of Country origin-----*/
					
					$html .='</div>';	
					
					$html .='<div class="col-5"><center>1</center></small></div>';
					$html .='<div class="col-7"><center>'. $data['qty'] .'</center></small></div>';
					
					$html .='<div class="col-20">';
					 
						$html .='<div style="text-align:left; float:left;width:40%">';
							$qq = $data['qty'];
							if($items['FbaItemLocation']['sku_type'] == 1){
								$qq = $items['FbaItemLocation']['quantity'];
							}
							$html .='<div style="text-align:center;"><input type="number" name="poitems['.$items['FbaItemLocation']['fnsku'].'][qty]" id="input_'. $items['FbaItemLocation']['id'].'" value="'.$qq.'" class="qtyinput" style="width:65px;"onchange="enterNumber('. $items['FbaItemLocation']['id'].')"/></div>';
							
							$html .='<div style="text-align:center;"><a href="javascript:void(0);" title="Update Quantity" class="btn btn-info btn-xs" id="upd_'.$items['FbaItemLocation']['id'].'"><small>Update Qty</small></a></div>';
						
						$html .='</div>';
						
						$html .='<div style="text-align:center; float:right;width:60%">';
							
							$html .='<button type="button" class="btn btn-success btn-sm print_label" title="Print Label" onClick="printLabel('. $items['FbaItemLocation']['id'].')" id="label_'.$items['FbaItemLocation']['id'].'"><i class="fa fa-print print_label" for="'.$items['FbaItemLocation']['id'].'"></i> PrintLabels</button><input type="hidden" name="label_status" id="label_status" value="'.$items['FbaItemLocation']['status'].'"/>';
							
							$html .='<button type="button" class="btn btn-warning btn-sm print_slip ub" title="Print Slip" onClick="printSlip('. $items['FbaItemLocation']['id'].')" id="slip_'. $items['FbaItemLocation']['id'].'" ><i class="fa fa-print print_label" for="'.$items['FbaItemLocation']['id'].'"></i> PrintSlip</button><input type="hidden" name="slip_status" id="slip_status" value="'.$items['FbaItemLocation']['status'].'"/>';
						
						$html .='</div>';
						
					$html .='</div>';
					
				$html .='</div>';	
					
					
				 
				if($items['FbaItemLocation']['sku_type'] > 1){
				
					  $bundle_items =  $this->getLocationMapping($items['FbaItemLocation']['merchant_sku'],$items['FbaItemLocation']['shipment_inc_id']);
					  foreach($bundle_items as $bval){
							$html .='<div class="col-lg-12" style=" background-color:#BAEEF9;">'; 
							$html .='<div class="col-12">&nbsp;&nbsp;&nbsp;&nbsp;</div>';			
							$id = $bval['FbaItemLocation']['id'];
							$name = $bval['FbaItemLocation']['fnsku'];
							
							$html .= '<div class="col-15" style="font-size:11px;">'.$bval['FbaItemLocation']['sku'].'</div>'; 	
							$html .= '<div class="col-10" style="font-size:11px;">'.$bval['FbaItemLocation']['barcode'].'</div>'; 												
							$html .= '<div class="col-30" style="font-size:11px;">'.$this->getFnskuTitle($items['FbaItemLocation']['fnsku']).'</div>';
							$html .= '<div class="col-3" for="'.$id .'">&nbsp;</div>';
 							
							$eexp  = explode('-',$master_sku);
							$ccount  = count($eexp);
							$qindex = $ccount  - 1; 
							$bunq   = $eexp[$qindex];
							$qstr = '';
							if($ccount == 3 ){
								$qt = $bval['FbaItemLocation']['quantity'] / $bunq;
								$qstr =	$qt .'x'.$bunq.' ='. $bval['FbaItemLocation']['quantity'];								
							}else if($ccount > 3 ){
								$qstr = $bval['FbaItemLocation']['quantity'] .'x 1 ='. ($bval['FbaItemLocation']['quantity'] * 1);
							}	
							
							$html .= '<div class="col-10" style="font-size:11px;" id="l_'.$id.'">'. $qstr.'</div>';
							
							$html .='<div class="col-7">-</div>';									
							 
							$html .=  '</div>';
							} 
												 
					} 
						 
				$html .='</div>';	
			}
			
			$html .='</div>';
		}else{
			 $html = '<div class="alert alert-info">There are no SKU to process or picklist is not generated. </div>';
		}
		 
		 
		
		$html .='<div class="row box-row-1 scan-box">
					<div class="col-lg-12">
						<div class="col-15"><label for="box_barcode">Scan Box Barcode:</label></div>
						<div class="col-30"><input type="text" name="box_barcode" id="box_barcode" value="" class="col-90 form-control" onKeyPress="return scanBoxBarcode(event);"/></div>
						<div style="float:right;"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">+Add More Boxes</button></div>	
					</div>
				</div>';
		
		 
		$sb_html = $this->getShipmentBoxGrid($shipment_id,$fnsku);
		
	 	$msg['data'] 	 = $html;
		$msg['shipment'] = $shipment; 
		$msg['ship_box'] = $sb_html;
		$msg['shipment_inc_id'] 	 = $shipment_inc_id;	
							
		echo json_encode($msg);
		exit;
		 
}
	
	public function getShipmentBoxGrid( $shipment_id = null, $fnsku = null, $is_ajax = 0) 
    {
		$sb_html = '';
		if($shipment_id){
			$ship_box = $this->getShipmentBoxes($shipment_id);
			if(count($ship_box) > 0){
				foreach($ship_box as $val){
				
						$ship_box_items = $this->getShipmentBoxItems($val['FbaShipmentBox']['id']);
						$sel = NULL;
						$dis = ' disable'; 
						$disabled ='disabled="disabled"';
						if(isset($this->request->data['box_barcode']) && $this->request->data['box_barcode'] == $val['FbaShipmentBox']['barcode']){
							$sel = ' selected';
							$dis = NULL;
							$disabled = NULL;
						}
						
						$sb_html .='<div class="row sku '.$sel.'"><div class="col-lg-12">';
						$sb_html .='<div class="col-15">Box-'.$val['FbaShipmentBox']['box_number'].':-</div>'; 
						
						$sb_html .='<input type="hidden" id="box_inc_id_'.$val['FbaShipmentBox']['box_number'].'" value="'.$val['FbaShipmentBox']['id'].'" />';
						$skuqty = 0; $qty = 0;
						if(count($ship_box_items) > 0){
							foreach($ship_box_items as $v){
								 
									//$sb_html .='<div class="col-25">'.$val['FbaShipmentBoxItem']['fnsku'].'</div>';	
									//$sb_html .='<div class="col-25">'.$val['FbaShipmentBoxItem']['fnsku'].'</div>';
									//$sb_html .='<div class="col-25">'.$val['FbaShipmentBoxItem']['merchant_sku'].'</div>';
								if($fnsku == $v['FbaShipmentBoxItem']['fnsku']){
									$skuqty += $v['FbaShipmentBoxItem']['qty'];
								}
								$qty += $v['FbaShipmentBoxItem']['qty'];
							}
						}
						$sb_html .='<div class="col-3">Qty:</div>';
						$sb_html .='<div class="col-22"><input type="text" id="qty_'.$val['FbaShipmentBox']['box_number'].'" value="'.$skuqty.'" class="form-control qbox '.$dis.'" '.$disabled.'><small>Total Box Qty:'.$qty.'</small></div>';
						$sb_html .='<div class="col-7"><button type="button" class="btn btn-success" onClick="saveShipmentBoxQty('.$val['FbaShipmentBox']['box_number'].');">Save</button></div>';
						$sb_html .='<div class="col-10"><button type="button" class="btn btn-warning" id="print_box_label_'.$val['FbaShipmentBox']['id'].'" onClick="printBoxLabel('.$val['FbaShipmentBox']['id'].');"><i class="fa fa-print"></i>&nbsp;PrintBoxLabel</button></div>';	
						
						
					$sb_html .='</div></div>';
				}
			}else{
				$sb_html ='<div class="row"><div classs="col-lg-12"><div class="alert alert-info"> There are no box. Please add new box. </div></div></div>';
			} 
		}
		if($is_ajax > 0){
			$msg['ship_box'] = $sb_html;
			$msg['msg'] = 'done';	
			echo json_encode($msg);
			exit;
		}else{
			return $sb_html;
		}
	} 
	
	public function getShipmentBoxItems( $box_inc_id = null) 
    {
		$this->loadModel( 'FbaShipmentBox' ); 
		$this->loadModel( 'FbaShipmentBoxItem' ); 
  		return $this->FbaShipmentBoxItem->find('all', array('conditions' => array('FbaShipmentBoxItem.box_inc_id' => $box_inc_id),'order' => 'id DESC'));
   	}
	
	public function getShipmentBoxes( $shipment_id = null) 
    {			
		$this->loadModel( 'FbaShipmentBox' ); 
		return $this->FbaShipmentBox->find('all', array('conditions' => array('FbaShipmentBox.shipment_id' => $shipment_id),'order' => 'id DESC')); 
  	}
	
	public function shipmentBoxes( $id = null) 
    {
			
			$this->loadModel( 'FbaShipmentBox' ); 
			$ship_box =  $this->FbaShipmentBox->find('first', array('conditions' => array('FbaShipmentBox.shipment_id' => $this->request->data['shipment_id']),'order' => 'id DESC'));
			
			$msg = array(); 
			if(count($ship_box) > 0){
				$box_number = $ship_box['FbaShipmentBox']['box_number']+1;
			}else{
				$box_number = 1;
			}
			$barcode 			   = substr($this->request->data['shipment_id'],3).'-'.$box_number;
			$data['shipment_id']   = $this->request->data['shipment_id'];
			$data['barcode']   	   = $barcode;
			$data['box_number']    = $box_number;
			$data['box_height']    = $this->request->data['box_height']; 
			$data['box_length']    = $this->request->data['box_length'];
			$data['box_width']     = $this->request->data['box_width'];
			if(isset($this->request->data['box_weight'])){
				$data['box_weight']    = $this->request->data['box_weight'];
			}
			$data['username']      = $this->Session->read('Auth.User.username');
				  
			$this->FbaShipmentBox->saveAll($data);	
			
			$msg['msg'] = 'done';
		 	
		echo json_encode($msg);
		exit;
	}
	
	public function saveShipmentBoxQty() 
    {
			
		$msg = array();
		$this->loadModel( 'FbaShipmentBoxItem' ); 	
		
		$this->loadModel('FbaItemLocation');  
		
	 	$bar = $this->Components->load('Common')->getGlobalBarcode($barcode);   
		 
		$item =  $this->FbaItemLocation->find('first', array(
					'conditions' => array(						
						  'FbaItemLocation.shipment_id' =>$this->request->data['shipment_id'], 
						  'FbaItemLocation.fnsku' => $this->request->data['fnsku'] 
					)
				) 
		);
		
		if(count($item) > 0){		
			$process_qty = $item['FbaItemLocation']['process_qty'];
			$qtys = array(); 
			$all_qty = $this->FbaShipmentBoxItem->find('all', array('conditions' => array(
				'FbaShipmentBoxItem.shipment_id' => $this->request->data['shipment_id'], 
				'FbaShipmentBoxItem.fnsku' => $this->request->data['fnsku'],
				'FbaShipmentBoxItem.merchant_sku' => $this->request->data['merchant_sku']
			  )));
			  
			  if(count($all_qty) > 0){
			 
				  foreach($all_qty as $val){
				  	if($val['FbaShipmentBoxItem']['box_number'] != $this->request->data['box_number']){
						$qtys[] = $val['FbaShipmentBoxItem']['qty'];
					}
				  }			 
			  } 
			  
			  $total_box_qty = array_sum($qtys);
			  if($process_qty > 0 &&(($total_box_qty + $this->request->data['qty']) > $process_qty)){
				  $msg['msg'] = 'You can not add qty more than processed.';
				  $msg['error'] = 'yes';
			  }else{
			  
				$sh = $this->FbaShipmentBoxItem->find('first', array('conditions' => array(
				'FbaShipmentBoxItem.shipment_id' => $this->request->data['shipment_id'],
				'FbaShipmentBoxItem.box_number' => $this->request->data['box_number'],
				'FbaShipmentBoxItem.fnsku' => $this->request->data['fnsku'],
				'FbaShipmentBoxItem.merchant_sku' => $this->request->data['merchant_sku']
				))); 
				if(count($sh) > 0){
					$data['id']   = $sh['FbaShipmentBoxItem']['id'];
				} 
				
				$data['shipment_id']   = $this->request->data['shipment_id'];
				$data['box_inc_id']    = $this->request->data['box_inc_id'];
				$data['box_number']    = $this->request->data['box_number'];;
				$data['fnsku']   	   = $this->request->data['fnsku']; 
				$data['merchant_sku']  = $this->request->data['merchant_sku'];
				$data['qty']    	   = $this->request->data['qty'];
				$data['username']      = $this->Session->read('Auth.User.username'); 
				  
				$this->FbaShipmentBoxItem->saveAll($data);	
				
				$msg['msg'] = 'done';
				$msg['error'] = '';			  
			  }
		}  	
		echo json_encode($msg);
		exit;
	}
	
	public function completeBarcode($fnsku = null, $shipment_inc_id = null, $qty = 0)
	{
 	 
		$this->autorender = false;
		$this->layout = ''; 
		$this->loadModel('FbaItemLocation');  
		$this->loadModel('FbaShipmentItem');
		 	 
 		$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
		
		$user_detils = $firstName .' '. $lastName .'_'. $pcName;
		$username    = $this->Session->read('Auth.User.username'); 
		 
		$update['status']    		    = 3;
		$update['process_qty']    		= $qty;
		$update['process_user']    		= "'".$username."'";
		$update['process_user_details'] = "'".$user_detils."'";				 
		$update['process_date']   		= "'".date('Y-m-d H:i:s')."'";
	 	$this->FbaItemLocation->updateAll($update, array('FbaItemLocation.shipment_inc_id' => $shipment_inc_id,'FbaItemLocation.fnsku' => $fnsku ) );
		
		$ship['status'] = 3;	
		
		$this->FbaShipmentItem->updateAll($ship, array( 'FbaShipmentItem.shipment_inc_id' => $shipment_inc_id, 'FbaShipmentItem.fnsku' => $fnsku ) );  
		$this->redirect(Router::url('/', true).'FbaExportConsole/index/'.$shipment_inc_id);
		exit;
	} 
	
	public function generateSlipPdf( $id = null, $qty = null, $isAjax = 0 )
	{	 	 
		if($id > 0 && $qty > 0){	
				 
			$this->loadModel('FbaItemLocation');
			$_item = $this->FbaItemLocation->find('first', array('conditions'=>array('FbaItemLocation.id'=>$id)));		 
			$fnsku = $_item['FbaItemLocation']['fnsku'];
			$title = $this->getFnskuTitle($_item['FbaItemLocation']['fnsku']);
			if(strlen($title) > 100 ){
				$final_title = substr($title,0,80). '...' . substr($title,-20); 
			}else{
				$final_title = $title ;
			}
			
			$cssPath = WWW_ROOT .'css/';
				
			$html = '<meta charset="utf-8">';
			$html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			$html .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>';
			$html .= '<meta content="" name="description"/>';
			$html .= '<meta content="" name="author"/>';
			
			$html .= '<body>';
			$html .= '<div id="label" style="padding-left: 0px;padding-top: 15px;padding-bottom: 0px;">';
			$html .= '<div class="container">';
			$html .= '<table style="padding:1px 0;">';
			$html .= '<tr><td style="font-size:20px;font-family:Verdana, Arial, Helvetica, sans-serif; text-align:center;"><strong>Total Quantity : '.$qty.'</strong> </td></tr>';
			$html .= '<tr><td style="font-size:20px;font-family:Verdana, Arial, Helvetica, sans-serif; text-align:center; ">'.$fnsku.'</td></tr>';
			$html .= '<tr><td style="font-size:10px;font-family:Verdana, Arial, Helvetica, sans-serif ">'.$final_title.'</td></tr>';
			
			$html .= '</table>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</body>';
			$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
			
			 
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF(); 
			$dompdf->set_paper(array(0, 0, 189, 120), 'portrait'); 
			$dompdf->load_html($html, Configure::read('App.encoding'));
			$dompdf->render();
				
			$file_to_save = WWW_ROOT .'fba_shipments/slips/SL_'.$fnsku.'.pdf';	
			 
			file_put_contents($file_to_save, $dompdf->output()); 
			 
			$msg['msg'] = $fnsku.' Slip generated.';				
			$msg['error'] = ''; 
		}else{
			$msg['msg'] = 'Qty is not valid.';				
			$msg['error'] = 'yes'; 
		}
		
		if($isAjax > 0){
			echo json_encode($msg);
			exit;
		}
	}
	
	public function printBoxLabel( $id = 0 )
	{
	
	 	if($id == 0 ){
			$id  = $this->request->data['id'];
		}		
		
		$this->loadModel('FbaShipmentBox');
		
		$_item = $this->FbaShipmentBox->find('first', array('conditions'=>array('FbaShipmentBox.id' => $id)));		 
		
		if(count($_item) > 0)
		{
			$shipment_id = $_item['FbaShipmentBox']['shipment_id'];
			$box_number  = $_item['FbaShipmentBox']['box_number']; 
			$barcode 	 = $_item['FbaShipmentBox']['barcode'];
			// substr($shipment_id,3).'-'.$box_number;
			$this->getBoxBarcode($barcode);  
			 
			$box_label = $shipment_id.'_'.$box_number;
			
			$cssPath = WWW_ROOT .'css/'; 
			$html  ='';
			$html .= '<meta charset="utf-8">';
			$html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			$html .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>';
			$html .= '<meta content="" name="description"/>';
			$html .= '<meta content="" name="author"/>';
			
			$html .= '<body>';
			$html .= '<div id="label" style="padding-left: 0px;padding-top: 7px;padding-bottom: 0px;">';
			$html .= '<div class="container">';
			$html .= '<table style="padding:1px 0;">';
			$html .= '<tr><td style="font-size:20px;font-family:Verdana, Arial, Helvetica, sans-serif;text-align:center;"> <strong>Box : '.$box_number.'</strong> </td></tr>';
			$html .= '<tr><td><img style="margin-left:-10px" src="'. Router::url('/', true) .'fba_shipments/box_barcode/'.$barcode.'.png" width="215"></td></tr>';			
			$html .= '</table>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</body>';
			$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
			
			/*$html = '<meta charset="utf-8">';
			$html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			$html .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>';
			$html .= '<meta content="" name="description"/>';
			$html .= '<meta content="" name="author"/>';
			
			$html .= '<body>';
			$html .= '<div id="label" style="padding-left: 0px;padding-top: 15px;padding-bottom: 0px;">';
			$html .= '<div class="container">';
			$html .= '<table style="padding:1px 0;">';
			//$html .= '<tr><td style="padding:2px;font-family:Verdana, Arial, Helvetica, sans-serif; text-align:center;">&nbsp;</td></tr>';
			
			$html .= '<tr><td style="font-size:20px;font-family:Verdana, Arial, Helvetica, sans-serif; text-align:center;"><strong>Box : '.$box_number.'</strong> </td></tr>';
			 
			//$html .= '<tr><td style="padding:10px;font-family:Verdana, Arial, Helvetica, sans-serif; text-align:center;">&nbsp;</td></tr>';
			
			$html .= '<tr><td><center><img src="'. Router::url('/', true) .'fba_shipments/box_barcode/'.$barcode.'.png"  style="text-align:center;" width="205"></center></td></tr>';
			 
			
			$html .= '</table>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</body>';
			$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';*/
			
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF(); 
			//$dompdf->set_paper(array(0, 0, 500, 288), 'portrait'); 
			$dompdf->set_paper(array(0, 0, 189, 91), 'portrait'); 
			 
			
			$dompdf->load_html($html, Configure::read('App.encoding'));
			$dompdf->render();
				
			$file_to_save = WWW_ROOT .'fba_shipments/box_labels/'.$box_label.'.pdf';
			 
			file_put_contents($file_to_save, $dompdf->output()); 
				
			
			$printerId = $this->getThermalPrinter();
				
			if($printerId != 'Printer_Not_Found'){ 	 
				$serverPath =  Router::url('/', true) .'fba_shipments/box_labels/'.$box_label.'.pdf';	
				$sendData = array(
					'printerId' => $printerId,
					'title' => 'Now Print',
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'					
				);
				//print_r($sendData);
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				$d = $Coreprinter->toPrint( $sendData );
				//print_r($d);
				  
				$msg['msg'] = $barcode.' Box printed.';				
				$msg['error'] = '';
			}else{
				$msg['msg'] = 'Printer not found for user: '. $this->Session->read('Auth.User.username'); 
				$msg['error'] = 'yes';
			}
		}else{
				$msg['msg'] = 'Box not found.'; 
				$msg['error'] = 'yes';
		}
		echo json_encode($msg);
		exit;
	
	}
	
	public function printBoxDetails( $id = 0 )
	{
	
	 	if($id == 0 ){
			$id  = $this->request->data['id'];
		}		
		
		$this->loadModel('FbaShipmentBox');
		$this->loadModel('FbaShipmentBoxItem');
		$box = $this->FbaShipmentBox->find('first', array('conditions'=>array('FbaShipmentBox.id' => $id)));		 
		$_items = $this->FbaShipmentBoxItem->find('all', array('conditions'=>array('FbaShipmentBoxItem.box_inc_id' => $id)));	
		if(count($_items) > 0)
		{
			 
			$shipment_id = $box['FbaShipmentBox']['shipment_id'];
			$box_number  = $box['FbaShipmentBox']['box_number']; 
			$box_label = $shipment_id.'_'.$box_number;
				
			$cssPath = WWW_ROOT .'css/'; 
			$html  ='';
			$html .= '<meta charset="utf-8">';
			$html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			$html .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>';
			$html .= '<meta content="" name="description"/>';
			$html .= '<meta content="" name="author"/>';
			
			$html .= '<body>';
			$html .= '<div id="label" style="padding-left: 0px;padding-top: 30px;padding-bottom: 30px;">';
			$html .= '<div class="container">';
			$html .= '<table cellpadding="3" cellspacing="1" border="1">';
		 
			$html .= '<tr><td style="font-size:16px;font-family:Verdana, Arial, Helvetica, sans-serif;text-align:center;" colspan="4"> <strong>Box : '.$box_number.' ('.$shipment_id.')</strong> </td></tr>';
			$html .= '<tr style="font-weight:bold; padding:1px;"><td>FNSKU</td><td>MERCHANT SKU</td><td>TITLE </td><td>QTY</td></tr>';	
			
			foreach($_items as $_item){
				 
				$html .= '<tr><td>'.$_item['FbaShipmentBoxItem']['fnsku'].' </td><td>'.$_item['FbaShipmentBoxItem']['merchant_sku'].' </td><td>'.$this->getFnskuTitle($_item['FbaShipmentBoxItem']['fnsku']).' </td><td>'.$_item['FbaShipmentBoxItem']['qty'].' </td></tr>';
						
			}
  			$html .= '</table>';
			
			$html .= '<table>';
			$html .= '<tr><td colspan="4">&nbsp;</td></tr>';
			$weight = $box['FbaShipmentBox']['box_weight'] ? $box['FbaShipmentBox']['box_weight'] : 0;
			$html .= '<tr><td width="19%">&nbsp;</td><td width="19%">&nbsp;</td><td width="22%" align="right"><strong>Weight</strong>:'. number_format($weight,2).' Pound.</td>
			<td align="right"><strong>Dimentions(IN INCH)</strong>:- H:'.$box['FbaShipmentBox']['box_height'].' X L:'.$box['FbaShipmentBox']['box_length'].' X W:'.$box['FbaShipmentBox']['box_width'].'</td></tr></table>';	
			
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</body>';
			$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
			 
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF(); 
			$dompdf->set_paper('A4', 'portrait'); 
			
			//$dompdf->set_paper(array(0, 0, 189, 91), 'portrait'); 
			 
			
			$dompdf->load_html($html, Configure::read('App.encoding'));
			$dompdf->render();
				
			$file_to_save = WWW_ROOT .'fba_shipments/box_details/'.$box_label.'.pdf';
			 
			file_put_contents($file_to_save, $dompdf->output()); 
				
			
			$printerId = $this->getLaserPrinter();
				
			if($printerId != 'Printer_Not_Found'){ 	 
				$serverPath =  Router::url('/', true) .'fba_shipments/box_details/'.$box_label.'.pdf';	
				$sendData = array(
					'printerId' => $printerId,
					'title' => 'Now Print',
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'					
				);
				//print_r($sendData);
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				$d = $Coreprinter->toPrint( $sendData );
				//print_r($d);
				  
				$msg['msg'] = $barcode.' Box Details printed.';				
				$msg['error'] = '';
			}else{
				$msg['msg'] = 'Printer not found for user: '. $this->Session->read('Auth.User.username'); 
				$msg['error'] = 'yes';
			}
		}else{
				$msg['msg'] = 'Box Details not found.'; 
				$msg['error'] = 'yes';
		}
		echo json_encode($msg);
		exit;
	
	}
	
	public function rePrintSlip(  $item_id = 0, $qty = 0)
	{
		if($item_id == 0 )
		{
			$item_id = $this->request->data['id'];			
		}		
		$qty = $this->request->data['qty'];
		
		$this->loadModel('FbaShipment');
		$this->loadModel('FbaShipmentItem');
		$this->FbaShipmentItem->unbindModel(
					array(
						'belongsTo' => array(
							'FbaShipment'
						)
					)
			    );
		
		
		$_item = $this->FbaShipmentItem->find('first', array('conditions'=>array('FbaShipmentItem.id'=>$item_id)));
		
		if(count($_item) > 0){
			$this->loadModel('FbaItemLocation');		
			$loc = $this->FbaItemLocation->find('first', array('conditions'=>array('FbaItemLocation.shipment_inc_id'=>$_item['FbaShipmentItem']['shipment_inc_id'],'FbaItemLocation.merchant_sku'=>$_item['FbaShipmentItem']['merchant_sku'])));
			
			$this->printSlip($loc['FbaItemLocation']['id'],$qty);
			 
		}
		exit;
 	}
	
	public function printSlip( $id = 0, $qty = 0 )
	{
	 	if($id == 0 ){
			$id  = $this->request->data['id'];
		}		
		if($qty == 0 ){
			$qty   = $this->request->data['qty'];
		} 
		$this->loadModel('FbaItemLocation');
		$_item = $this->FbaItemLocation->find('first', array('conditions'=>array('FbaItemLocation.id' => $id)));		 
		$fnsku = $_item['FbaItemLocation']['fnsku'];
		
		$file_to_save = WWW_ROOT .'fba_shipments/slips/SL_'.$fnsku.'.pdf';	
		//if(!file_exists($file_to_save)){
			$this->generateSlipPdf($id, $qty);
		//} 
		
		$printerId = $this->getThermalPrinter();
			
		if($printerId != 'Printer_Not_Found'){ 	 
			$serverPath =  Router::url('/', true) .'fba_shipments/slips/SL_'.$fnsku.'.pdf';	
			$sendData = array(
				'printerId' => $printerId,
				'title' => 'Now Print',
				'contentType' => 'pdf_uri',
				'content' => $serverPath,
				'source' => 'Direct'					
			);
			//print_r($sendData);
			App::import( 'Controller' , 'Coreprinters' );
			$Coreprinter = new CoreprintersController();
			$d = $Coreprinter->toPrint( $sendData );
			//print_r($d);
			$shipdata['status']		 =  2;			 
			$this->FbaItemLocation->updateAll( $shipdata,array('FbaItemLocation.id' => $id) );
			
			/*-----------------Update Shipmen item------------*/
			$this->loadModel('FbaShipmentItem');
			$shipdata['label_print_user']        = "'".$this->Session->read('Auth.User.username')."'";
			$shipdata['label_print_date']        = "'".date('Y-m-d H:i:s')."'";
			$this->FbaShipmentItem->updateAll( $shipdata,array('FbaShipmentItem.shipment_inc_id' => $_item['FbaItemLocation']['shipment_inc_id'],'FbaShipmentItem.fnsku' => $fnsku) );
			/*----------------End Update Shipmen item------------*/ 
			
			$msg['msg'] = $fnsku.' Slip printed.';				
			$msg['error'] = '';
		}else{
			$msg['msg'] = 'Printer not found for user: '. $this->Session->read('Auth.User.username'); 
			$msg['error'] = 'yes';
		}
		echo json_encode($msg);
		exit;
	}
	
	public function generateThermalHtml($qty = null,  $fnsku = null, $final_title = null)
	{
			 
		$cssPath = WWW_ROOT .'css/';	 
		 
		$html  = '';	 
			 
		for($i = 0; $i < $qty; $i++){
		
			$html .= '<meta charset="utf-8">';
			$html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			$html .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>';
			$html .= '<meta content="" name="description"/>';
			$html .= '<meta content="" name="author"/>';
			
			$html .= '<body>';
			$html .= '<div id="label" style="padding-left: 0px;padding-top: 7px;padding-bottom: 0px;">';
			$html .= '<div class="container">';
			$html .= '<table style="padding:1px 0;">';
			$html .= '<tr><td><img src="'. Router::url('/', true) .'fba_shipments/barcode/'.$fnsku.'.png" width="195" ></td></tr>';
			$html .= '<tr><td style="font-size:9px;font-family:Verdana, Arial, Helvetica, sans-serif ">'.$final_title.'</td></tr>';
			$html .= '<tr><td style="font-size:10px;font-family:Verdana, Arial, Helvetica, sans-serif">New</td></tr>';
			$html .= '</table>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</body>';
			$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
		}				 
			
		$set_paper = array(0, 0, 189, 91);
			 
		return array('html' => $html,'set_paper' => $set_paper);	 
				 
	}
	
	public function generateLaserHtml( $qty = null,  $fnsku = null, $final_title = null)
	{
			$cssPath = WWW_ROOT .'css/';	 
			  
			$html = '<!DOCTYPE html>';
			$html .= '<html>';  
			$html .= '<head>';
			$html .= '<title>Label for PDF Generation</title>';	
			$html .= '</head>';
			$html .= '<body>';
			$html .= '<div >';
			$html .= '<div class="container">';
			$html .='<table class="header row" style="border-bottom:0px;margin-bottom:0px; margin-top:0px;padding-top: 0px;">';
			$html .= '<tbody>';
			$html .= '<tr>';
			$r=0; $col = 0; $row = 0;
			 for($i=0; $i<$qty; $i++){
				 $r++;
				 if($col % 3 == 0){
					$col = 0; $row++;
				 }
				 $col++;
				 $pr ='37';  $pb ='14';  /*$pb ='17'; 17MAY2018*/ $pt= 21;
				 if($i >1 && $i % 3 == 0){$html .= '</tr><tr>';} 
				 if($r >1 && $r % 3 == 0){$pr = '0';}
				 if($row % 7 == 0){$pb = '2';}	
				 
					$html .= '<td valign="top" width="100%"  style="padding-right: '.$pr.'px; padding-bottom: '.$pb.'px; padding-top: '.$pt.'px;">';
					$html .= '<table width="100%">';
					$html .= '<tr><td width="100%"><img src="'. Router::url('/', true) .'fba_shipments/barcode/'.$fnsku.'.png" width="195" ></td></tr>';
					$html .= '<tr><td width="100%" style="font-size:9px;font-family:Verdana, Arial, Helvetica, sans-serif ">'.$final_title.'</td></tr>';
					$html .= '<tr><td width="100%" style="font-size:11px;font-family:Verdana, Arial, Helvetica, sans-serif; padding:0px; " height="10">New</td></tr>';
					$html .= '</table>';
	
					$html .= '</td>';  
			
			 }
			 
			$html .= '</tr>';
		
 			$html .= '</tbody>';
			$html .= '</table>';
			$html .= '</div> ';
			$html .= '</div>';
			$html .= '</body>';
			$html .= '</html>'; 
			 
			$set_paper  = 'A4';
		
			return array('html' => $html,'set_paper' => $set_paper);	 	 
				 
	}
	
 	public function generateLabelPdf( $id = null, $qty = null, $printer = null, $isAjax = 0)
	{
		if($id > 0 && $qty > 0){
			
			$this->loadModel('FbaItemLocation');
			$_item = $this->FbaItemLocation->find('first', array('conditions'=>array('FbaItemLocation.id'=>$id)));			
			$fnsku = $_item['FbaItemLocation']['fnsku'];
			$title = $this->getFnskuTitle($_item['FbaItemLocation']['fnsku']);
			$this->getBarcode($_item['FbaItemLocation']['fnsku']);
			
			if(strlen($title) > 38){
				$final_title = substr($title,0,20). '...' . substr($title,-12); 
			}else{
				$final_title = $title; 
			}			
			if($printer == 'thermal'){ 
				$htmlData = $this->generateThermalHtml($qty, $fnsku, $final_title);
			}else if($printer == 'laser'){ 
			  	$htmlData = $this->generateLaserHtml($qty, $fnsku, $final_title);
			}else {
				if($qty < 10){ 
					$htmlData = $this->generateThermalHtml($qty, $fnsku, $final_title);
				}
				else{ 
					$htmlData = $this->generateLaserHtml($qty, $fnsku, $final_title);
				} 
			}
											  
			$html = $htmlData['html'];
			$set_paper = $htmlData['set_paper'];
		
			//echo $html; exit;
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF(); 
			$dompdf->set_paper($set_paper, 'portrait');//$dompdf->set_paper($set_paper, 'portrait');
			//$dompdf->set_paper(array(0, 0, 595, 842), 'portrait');
			$dompdf->load_html($html, Configure::read('App.encoding'));
			$dompdf->render();
			//$dompdf->stream('test.pdf');			
						
			$file_to_save = WWW_ROOT .'fba_shipments/labels/LA_'.$fnsku.'.pdf';	
			 
			file_put_contents($file_to_save, $dompdf->output());
			$msg['msg'] = 'Label PDF Generated.';
			$msg['error'] = '';
				 
		}else{			
			$msg['msg'] = 'Please enter valid quantity.';
			$msg['error'] = 'yes';
		}
		
		
		if($isAjax > 0){
			echo json_encode($msg);
			exit;
		}
	}
	
	public function rePrintLabel( $item_id = 0, $qty = 0)
	{
	
		if($item_id == 0 )
		{
			$item_id = $this->request->data['id'];			
		}
		
		$qty = $this->request->data['qty'];
		
		$this->loadModel('FbaShipment');
		$this->loadModel('FbaShipmentItem');
		$this->FbaShipmentItem->unbindModel(
					array(
						'belongsTo' => array(
							'FbaShipment'
						)
					)
			    );
		
		
		$_item = $this->FbaShipmentItem->find('first', array('conditions'=>array('FbaShipmentItem.id'=>$item_id)));
		
		if(count($_item) > 0){
			$this->loadModel('FbaItemLocation');		
			$loc = $this->FbaItemLocation->find('first', array('conditions'=>array('FbaItemLocation.shipment_inc_id'=>$_item['FbaShipmentItem']['shipment_inc_id'],'FbaItemLocation.merchant_sku'=>$_item['FbaShipmentItem']['merchant_sku'])));
			
			$this->printLabel($loc['FbaItemLocation']['id'],$qty);
			 
		}
		exit;
		
	}
	
 	public function printLabel( $id = null, $qty = 0)
	{
	 	if($id == 0 ){
			$id = $this->request->data['id'];			
		}
		
		if($qty == 0 ){
			$qty = $this->request->data['qty'];			
		}
		 
			
		$this->loadModel('FbaItemLocation');
		
		$_item = $this->FbaItemLocation->find('first', array('conditions'=>array('FbaItemLocation.id'=>$id)));
		$fnsku = $_item['FbaItemLocation']['fnsku'];
		 
 		
		if(isset($this->request->data['printer']) && $this->request->data['printer'] == 'laser'){
			$this->generateLabelPdf($id, $qty, 'laser');
			$printerId	= $this->getLaserPrinter();			
		}else if(isset($this->request->data['printer']) && $this->request->data['printer'] == 'thermal'){
			$this->generateLabelPdf($id, $qty, 'thermal');
			$printerId = $this->getThermalPrinter();		
		}else{
			if($qty < 10){			
				$this->generateLabelPdf($id, $qty, 'thermal');
				$printerId = $this->getThermalPrinter();				
			}else{			
				$this->generateLabelPdf($id, $qty, 'laser');
				$printerId	= $this->getLaserPrinter();
			}
		} 
		
		if($printerId !='Printer_Not_Found'){ 
			$serverPath =  Router::url('/', true) .'fba_shipments/labels/LA_'.$fnsku.'.pdf';
			$sendData = array(
				'printerId' => $printerId,
				'title' => 'Now Print',
				'contentType' => 'pdf_uri',
				'content' => $serverPath,
				'source' => 'Direct'					
			);
			//print_r($sendData);
			App::import( 'Controller' , 'Coreprinters' );
			$Coreprinter = new CoreprintersController();
			$d = $Coreprinter->toPrint( $sendData );
			//print_r($d);
			 
			$shipdata['status']        = 1;			 
			$this->FbaItemLocation->updateAll( $shipdata,array('FbaItemLocation.id' => $id) );
			/*-----------------Update Shipmen item------------*/
			$this->loadModel('FbaShipmentItem');
			$shipdata['label_print_user']        = "'".$this->Session->read('Auth.User.username')."'";
			$shipdata['label_print_date']        = "'".date('Y-m-d H:i:s')."'";
			$this->FbaShipmentItem->updateAll( $shipdata,array('FbaShipmentItem.shipment_inc_id' => $_item['FbaItemLocation']['shipment_inc_id'],'FbaShipmentItem.fnsku' => $fnsku) );
			/*----------------End Update Shipmen item------------*/ 
			
			$msg['msg'] = $fnsku.' Label printed.';
			$msg['error'] = '';
		}else{
			$msg['msg'] = 'Printer not found for user: '. $this->Session->read('Auth.User.username');
			$msg['error'] = 'yes';
		}			 
	
		
		echo json_encode($msg);
		exit;
			
	}
	
	public function getLaserPrinter()
	{
		$this->autoRander = false;
		$this->loadModel( 'User' );
		$this->loadModel( 'PcStore' );
		$userId = $this->Session->read('Auth.User.id');

		$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
						
		$getPcText = $getUser['User']['pc_name'];  
		$paramsLaser = array(
			'conditions' => array(
				'PcStore.pc_name' => $getPcText,
				'PcStore.printer_name' => 'Brother HL-6180DW series'
			)
		);
		
		$printer = $this->PcStore->find('first', $paramsLaser)	;
		if(count($printer) > 0){
			$LaserT88 = $printer['PcStore']['printer_id'];
		}else{
			$LaserT88 = 'Printer_Not_Found';
		}
		
		return $LaserT88;	
	}
	
	public function getThermalPrinter()
	{
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );						
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name']; //getenv('COMPUTERNAME');
			$paramsThermal= array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name' => 'FBADymo'
				)
			);
			$printer = $this->PcStore->find('first', $paramsThermal)	;
			if(count($printer) > 0){
				$ThermalPrinter = $printer['PcStore']['printer_id'];
			}else{
				$ThermalPrinter = 'Printer_Not_Found';
			}
		
		return $ThermalPrinter;
	}	
	
	public function getBarcode( $fnsku ){ 
		
		$imgPath = WWW_ROOT .'fba_shipments'.DS.'barcode'.DS;   
 		
		if(!file_exists($imgPath.$fnsku.'.png')){
		
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
			
			$font = new BCGFontFile(WWW_ROOT.'fonts/Verdana.ttf', 14);
			$colorFront = new BCGColor(0, 0, 0);
			$colorBack = new BCGColor(255, 255, 255); 
			
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(43);
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->setFont($font);
			$code128->setLabel($fnsku);
			$code128->parse($fnsku);
			
			//Drawing Part
			$imgOrder128 = $fnsku.".png";
			$imgOrder128path = $imgPath.$fnsku.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);  
		}
		      
	}	 
	public function getBoxBarcode( $code ){ 
		
		$imgPath = WWW_ROOT .'fba_shipments'.DS.'box_barcode'.DS;   
 		
		//if(!file_exists($imgPath.$code.'.png')){
		
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode39extended.barcode.php');
			
			$font = new BCGFontFile(WWW_ROOT.'fonts/Verdana.ttf', 12);
			$colorFront = new BCGColor(0, 0, 0);
			$colorBack = new BCGColor(255, 255, 255); 
		
			/*$code128 = new BCGcode39extended();
			$code128->setScale(2);
			$code128->setThickness(50);
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->setFont($font);
			$code128->setChecksum(true);
			$code128->setLabel($code);
			$code128->parse($code);
			
		 
			//Drawing Part
			$imgOrder128 = $code.".png";
			$imgOrder128path = $imgPath.$code.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); */ 
				
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(43);
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->setFont($font);
			$code128->setLabel($code);
			$code128->parse($code);
			
			//Drawing Part
			$imgOrder128 = $code.".png";
			$imgOrder128path = $imgPath.$code.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);  
		//}
		      
	}	 
	
	public function getFnskuTitle( $fnsku = null) 
    {	
		$this->layout = " "; 	
		$this->loadModel('FbaShipmentItem'); 
		$product  = $this->FbaShipmentItem->find('first', array('conditions' => array('fnsku' => $fnsku)));
		return $product['FbaShipmentItem']['title'];
			
	}
	  
	public function getProductTitle( $sku = null) 
    {	
		$this->layout = " "; 	
		$this->loadModel('Product'); 
		$product  = $this->Product->find('first', array('conditions' => array('product_sku' => $sku)));
		return $product['Product']['product_name'];
			
	}
	
	public function updatePackHsOrigin()
	{		
		$this->loadModel( 'FbaPackaging' );	
		$this->loadModel( 'Skumapping' );	 
		$field = $this->request->data['field'];	
		$master_sku = $this->Skumapping->find('first',array('conditions' => array('channel_name LIKE' =>'CostBreaker_US','channel_sku' => $this->request->data['merchant_sku']),'fields' => array('sku')));
		
		$msg_str = $this->funPackHsOrigin($field,$this->request->data['value'],$master_sku['Skumapping']['sku']);
		
		if(substr($master_sku['Skumapping']['sku'], 0, 2) == 'B-'){
			$exp  = explode('-',$master_sku['Skumapping']['sku']);
			$count = count($exp);
			$qindex = $count  - 1; 
			
			for($i = 1; $i < $qindex; $i++){
				$msku = 'S-'.$exp[$i]; 
				$msg_str .= $this->funPackHsOrigin($field,$this->request->data['value'],$msku);
			}
		} 
		
				
		$msg['msg'] =$msg_str;
		$msg['error'] = '';
		 
		echo json_encode($msg);
		exit;
	}
	
	private function funPackHsOrigin($field = null, $value = null, $sku = null)
	{		
		$this->loadModel( 'FbaPackaging' );	
		$this->loadModel( 'Skumapping' );	 
		$msg = ''; 
		$map = $this->Skumapping->find('all',array('conditions' => array('channel_name LIKE' =>'CostBreaker_US','sku' =>  $sku),'fields' => array('channel_sku','sku')));
		if(count($map) > 0){
			foreach($map as $val){
				$conditions = array('master_sku' => $sku,'merchant_sku' => $val['Skumapping']['channel_sku']);	
			
				$field = $this->request->data['field'];		
				if($this->FbaPackaging->hasAny($conditions)){
					$DataUpdate[$field] = "'".$value."'";	
					$DataUpdate['master_sku'] = "'".$val['Skumapping']['sku']."'";		
					$this->FbaPackaging->updateAll( $DataUpdate, $conditions );		
					unset($DataUpdate);	
					$msg .= $field.' is assigned to : '.$val['Skumapping']['channel_sku']."\n";	
								
				}else{				
					$ResponseData['master_sku']   = $val['Skumapping']['sku'];
					$ResponseData['merchant_sku'] = $val['Skumapping']['channel_sku'];	
					$ResponseData[$field] = $value;	
					$this->FbaPackaging->saveAll( $ResponseData);	
					$msg .= $field.' is added to : '.$sku;	
				}
			
			}
		}  		
		return $msg ;
	}
	
	public function addPackHsOrigin()
	{		
		$this->loadModel( 'FbaPackaging' );	 	
		$this->loadModel( 'FbaPackaging' );	
		$this->loadModel( 'Skumapping' );	 
		 
		$master_sku = $this->Skumapping->find('first',array('conditions' => array('channel_name LIKE' =>'CostBreaker_US','channel_sku' => $this->request->data['merchant_sku']),'fields' => array('sku')));
		
		$map = $this->Skumapping->find('all',array('conditions' => array('channel_name LIKE' =>'CostBreaker_US','sku' =>  $master_sku['Skumapping']['sku']),'fields' => array('channel_sku','sku')));
		if(count($map) > 0){
			foreach($map as $val){
				$conditions = array('master_sku' => $val['Skumapping']['sku'],'merchant_sku' => $val['Skumapping']['channel_sku']);	
			
				$field = $this->request->data['field'];		
				if($this->FbaPackaging->hasAny($conditions)){
					$DataUpdate[$field] = "'".$this->request->data['value']."'";		
					$this->FbaPackaging->updateAll( $DataUpdate, $conditions );		
					unset($DataUpdate);		
								
				}else{				
					$ResponseData['master_sku']   = $val['Skumapping']['sku'];
					$ResponseData['merchant_sku'] = $val['Skumapping']['channel_sku'];	
					$ResponseData[$field] = $this->request->data['value'];	
					$this->FbaPackaging->saveAll( $ResponseData);	
				}
			
			}
		}  		 
		$msg['msg'] = $field.' is assigned to : '.$this->request->data['merchant_sku'];			
		$msg['error'] = '';
		 
		echo json_encode($msg);
		exit;
	}
	
	public function addPackaging()
	{		
		$this->loadModel( 'FbaPackaging' );	 	
		$this->loadModel( 'FbaPackagingList' );	 
			
		$conditions = array('merchant_sku' => $this->request->data['merchant_sku']);	
						
		if ($this->FbaPackaging->hasAny($conditions)){
			$DataUpdate['packaging'] = "'".$this->request->data['value']."'";		
			$this->FbaPackaging->updateAll( $DataUpdate, $conditions );		
			unset($DataUpdate);		
						
		}else{
			$ResponseData['merchant_sku'] = $this->request->data['merchant_sku'];	
			$ResponseData['packaging'] = $this->request->data['value'];	
			$this->FbaPackaging->saveAll( $ResponseData);	
		}	
			
				
		$pack_res = $this->FbaPackagingList->find('first',array('conditions' => array('packaging_name' => $this->request->data['value'])));
		if(count($pack_res) == 0){
			$packaging_list['packaging_name'] = $this->request->data['value'] ;
			$packaging_list['added_date']     = date('Y-m-d H:i:s');
			$packaging_list['username']       = $this->Session->read('Auth.User.username');						
			$this->FbaPackagingList->saveAll( $packaging_list );			  		
			 
			$msg['msg'] = 'Packaging is added and assigned to : '.$this->request->data['merchant_sku'];
			
		}else{
			$msg['msg'] = 'Packaging is already exist.';
		}
		$msg['error'] = '';
		 
		echo json_encode($msg);
		exit;
	}
	
	public function getBundleSku($shipment_inc_id = null, $fnsku = null) 
    {
		$this->loadModel('FbaShipmentItem');
		$this->FbaShipment->unbindModel(
					array(
						'hasMany' => array(
							'FbaShipmentItem'
						)
					)
			    );
		$item  = $this->FbaShipmentItem->find('first', array('conditions' => array('shipment_inc_id' => $shipment_inc_id, 'fnsku' => $fnsku)));
		if(count($item) > 0){
			return $item;
		}else{
			return array();
		}
	}
	
	/*public function updateBarcode() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Orders" );	
		$this->loadModel('Product'); 
		$this->loadModel('FbaShipmentItemsPo'); 
		$getItems  = $this->FbaShipmentItemsPo->find('all');
		foreach($getItems as $v){
		
			 
			$product  = $this->Product->find('first', array('conditions' => array('product_sku' => $v['FbaShipmentItemsPo']['master_sku'])));
			echo "<br>".$product['ProductDesc']['barcode'];
			
			//$this->FbaShipmentItemsPo->updateAll(array('FbaShipmentItemsPo.barcode' => "'".$product['ProductDesc']['barcode']."'"), array('FbaShipmentItemsPo.master_sku' => $v['FbaShipmentItemsPo']['master_sku']));
			
			
		}
		
	}*/
	
	

}