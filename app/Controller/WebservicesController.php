<?php
error_reporting(0);
class WebservicesController extends AppController
{
    
    var $name = "Webservices";
        
    public function index()
    {
		echo "Index";
		exit;
	}
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('postalDataAws','skuData','productData','getSortedOrders','getProcessedOrders','getSplitOrders','getProcessedPostal','getCurrentStock','getBrtParcelNumberForBest','updateBrtParcelNumber','whistlDataAws','getProductData'));
			$this->GlobalBarcode = $this->Components->load('Common'); 
    }
	
	public function getProductData($date = '') 
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'ProductCsv' );	
		if($date == ''){
			$date = date('Y-m-d');
		}
		 
  		$result	= $this->ProductCsv->find( 'all',array('conditions' => array('OR'=>['added_date >='=>$date, 'updated_date >='=>$date])));
 		echo json_encode($result);
		exit;
	}
	
	public function getCurrentStock() 
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'Product' );			
		$data = array();
		
		$result	= $this->Product->find( 'all', array('fields' => array( 'current_stock_level' , 'product_sku')));
		if(count($result)>0){
			foreach($result as $val){
				$data[$val['Product']['product_sku']] =  $val['Product']['current_stock_level'];
			}
		}
		echo json_encode($data);
		exit;
	}
	
	public function whistlDataAws() 
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' );	
		$this->loadModel( 'ProductHscode' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'Country' ); 
					
		$data = array();
		
		//$this->request->data['manifest_date'] = '2020-12-29';
		
		if( is_array($this->request->data) && count($this->request->data) > 0){
							
 			$result	=	$this->MergeUpdate->find( 'all', array('conditions' => array('service_provider' => 'whistl', 'manifest_date LIKE ' => date('Y-m-d', strtotime($this->request->data['manifest_date'])).'%' ),'fields' => array( 'price' , 'quantity' ,'product_order_id_identify' , 'service_name', 'provider_ref_code', 'service_provider','sku','delevery_country','order_id'),'order'=>array('id DESC')));
 		 
			if(count($result)>0){
			
				foreach($result as $val){
					$delevery_country = $val['MergeUpdate']['delevery_country'];
					if($delevery_country == 'null'){
 						$opresult	= $this->OpenOrder->find( 'first', array('conditions' => array( 'num_order_id' => $val['MergeUpdate']['order_id'] ),'fields' => array( 'customer_info')));
						if(count($opresult) > 0){
							$customer_info = unserialize($opresult['OpenOrder']['customer_info']);
							$delevery_country = $customer_info->Address->Country;
						}
					}
 					
					$country_code = 'GB';
					$iso_2 = $this->Country->find('first', array('conditions' => array('name' => $delevery_country),'fields' => array( 'iso_2')));
 					if(count($iso_2) > 0){
					 	$country_code = $iso_2['Country']['iso_2'];
					}
					
					$hs_code = [];
					$getSku = explode( ',' , $val['MergeUpdate']['sku']);
					foreach($getSku  as $j => $sk){
 						$_sku = explode( 'XS-' , $getSku[$j] ); 
						$sku = 'S-'.$_sku[1]; 
						
						$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $sku,'country_code' => $country_code)));
						if(count( $hsresult ) ==  0){				
							$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $sku,'country_code' => 'GB')));
 						}
						 
						if(count( $hsresult ) > 0){
							 $hs_code[] = $hsresult['ProductHscode']['hs_code'];	
  							 $data[$hsresult['ProductHscode']['hs_code']][] = ['price' => $val['MergeUpdate']['price'],'quantity'=> $val['MergeUpdate']['quantity']];
						}else{
							$data['9603210000'][] = ['price' => $val['MergeUpdate']['price'],'quantity'=> $val['MergeUpdate']['quantity']];
						}
					}
				
					/*$data[$val['MergeUpdate']['product_order_id_identify']] = array( 'service_name'=>$val['MergeUpdate']['service_name'], 'ref_code'=> $val['MergeUpdate']['provider_ref_code'], 'service_provider'=>$val['MergeUpdate']['service_provider'],'hs_code' => implode(";",$hs_code),'hsdata' => $hsdata );*/
				}
		
			}else{
				$data = array('msg'=>'No data Found');
			}
			
		}else{
			$data = array('msg'=>'Invalid Data.');
		}
		
		echo json_encode($data);
		exit;
		
	}
	
	public function postalDataAws() 
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' );			
		$data = array();
		
		if( is_array($this->request->data) && count($this->request->data) > 0){
							
			if(count($this->request->data) == 1 ){
				foreach($this->request->data as $val){
					$order_id = $val;
				}
				$result	=	$this->MergeUpdate->find( 'all', array('conditions' => array( 'product_order_id_identify' =>  $order_id ),'fields' => array( 'product_order_id_identify' , 'service_name', 'provider_ref_code', 'service_provider'),'order'=>array('id DESC')));
			}else{
				$result	=	$this->MergeUpdate->find( 'all', array('conditions' => array( 'product_order_id_identify IN ' =>  $this->request->data ),'fields' => array( 'product_order_id_identify' , 'service_name', 'provider_ref_code', 'service_provider'),'order'=>array('id DESC')));
			}			
		
			if(count($result)>0){
				foreach($result as $val){
					$data[$val['MergeUpdate']['product_order_id_identify']] = array( 'service_name'=>$val['MergeUpdate']['service_name'], 'ref_code'=> $val['MergeUpdate']['provider_ref_code'], 'service_provider'=>$val['MergeUpdate']['service_provider']  );
				}
		
			}else{
				$data = array('msg'=>'No data Found');
			}
			
		}else{
			$data = array('msg'=>'Invalid Data.');
		}
		
		echo json_encode($data);
		exit;
		
	}
	public function getProcessedPostal() 
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' );			
		$data = array();
		
		/*$start_date = date('Y-m-d H:i:s', strtotime('-5 days',strtotime(date('Y-m-d 0:0:0'))));
		$end_date   = date('Y-m-d H:i:s');
		$result	    = $this->MergeUpdate->find( 'all', array('conditions' => array('manifest_date BETWEEN ? AND ? ' => array($start_date, $end_date)),'fields' => array( 'product_order_id_identify' , 'service_name', 'provider_ref_code', 'service_provider'),'order'=>array('id DESC')));
		
		if(count($result)>0){
			foreach($result as $val){
				$data[$val['MergeUpdate']['product_order_id_identify']] = array( 'service_name'=>$val['MergeUpdate']['service_name'], 'ref_code'=> $val['MergeUpdate']['provider_ref_code'], 'service_provider'=>$val['MergeUpdate']['service_provider']  );
			}			 
		}else{
			$data = array('msg'=>'Invalid Data.');
		}*/
		$data = array('msg'=>'Invalid Data.');
		echo json_encode($data);
		exit;
		
	}
   	public function skuData()
	{
			 $this->loadModel('Product');
			 $this->loadModel('ProductDesc');
			 
			 $this->Product->unbindModel( array(
				'hasOne' => array(
					'ProductDesc' , 'ProductPrice' 
					),
				'hasMany' => array(
					'ProductLocation'
					)		
				) 
			 );		
			
			$params = array('conditions' => array( 'product_sku' =>  $this->request->data['sku']),
				'fields' => array(
					'Product.id as MainProductId',
					'Product.product_name',
					'Product.product_sku',
					'Product.category_name',
					'Product.uploaded_stock',
					'Product.current_stock_level',
					'ProductDesc.id as ProductDescId',
					'ProductDesc.product_id',
					'ProductDesc.barcode',
					'ProductDesc.product_type',
					'ProductDesc.product_defined_skus',
					'ProductDesc.product_identifier',
					'ProductDesc.length',
					'ProductDesc.width',
					'ProductDesc.height',
					'ProductDesc.weight',
					'ProductDesc.brand',
					'Product.product_status'
				),
				'order' => 'ProductDesc.id ASC'
			);
			$stockData = $this->ProductDesc->find( 'first' , $params );
			//pr(	$stockData);
			//foreach($stockData as $val){
				$barcode = $this->GlobalBarcode->getAllBarcode($stockData['ProductDesc']['barcode']);
				$data = array( 
					'product_id'=>			$stockData['Product']['MainProductId'], 
					'product_name'=>		$stockData['Product']['product_name'], 
					'product_sku'=>			$stockData['Product']['product_sku'],
					'category_name'=>		$stockData['Product']['category_name'],
					'current_stock_level'=>	$stockData['Product']['current_stock_level'],
					'product_status'=>		$stockData['Product']['product_status'],
					'barcode'=>				$barcode[0],
					'length'=>				$stockData['ProductDesc']['length'],
					'width'=>				$stockData['ProductDesc']['width'],
					'height'=>				$stockData['ProductDesc']['height'],
					'weight'=>				$stockData['ProductDesc']['weight'],
					'brand'=>				$stockData['ProductDesc']['brand'],
					'product_identifier'=>	$stockData['ProductDesc']['product_identifier']
				 );
			//}
			
			//echo  $this->request->data['sku'];
			echo json_encode($data);
			exit; 
	}
		
	
	public function productData()
	{
		 $this->loadModel('Product');
		 $this->loadModel('ProductDesc');
		 
		 $this->Product->unbindModel( array(
			'hasOne' => array(
				'ProductDesc' , 'ProductPrice' 
				),
			'hasMany' => array(
				'ProductLocation'
				)		
			) 
		 );		
		
		$params = array(
			'fields' => array(
				'Product.id as MainProductId',
				'Product.product_name',
				'Product.product_sku',
				'Product.category_name',
				'Product.uploaded_stock',
				'Product.current_stock_level',
				'ProductDesc.id as ProductDescId',
				'ProductDesc.product_id',
				'ProductDesc.barcode',
				'ProductDesc.product_type',
				'ProductDesc.product_defined_skus',
				'ProductDesc.product_identifier',
				'ProductDesc.brand',
				'ProductDesc.length',
				'ProductDesc.width',
				'ProductDesc.height',
				'ProductDesc.weight',
				'Product.product_status'
			),
			'order' => 'ProductDesc.id ASC'
		);
		$stockData = $this->ProductDesc->find( 'all' , $params );
		
		foreach($stockData as $val){
			$barcode = $this->GlobalBarcode->getAllBarcode($val['ProductDesc']['barcode']);
			$data[] = array( 
				'product_id'=>			$val['Product']['MainProductId'], 
				'product_name'=>		$val['Product']['product_name'], 
				'product_sku'=>			$val['Product']['product_sku'],
				'category_name'=>		$val['Product']['category_name'],
				'current_stock_level'=>	$val['Product']['current_stock_level'],
				'product_status'=>		$val['Product']['product_status'],
				'barcode'=>				$barcode[0],
				'length'=>				$val['ProductDesc']['length'],
				'width'=>				$val['ProductDesc']['width'],
				'height'=>				$val['ProductDesc']['height'],
				'weight'=>				$val['ProductDesc']['weight'],
				'brand'=>				$val['ProductDesc']['brand'],
				'product_identifier'=>	$val['ProductDesc']['product_identifier']
			 );
		}
		
		//pr(	$data);
		echo json_encode($data);
		exit; 
	}	
	
	
	public function getProcessedOrders($t = null)
	{
			$this->layout = '';
			$this->autoRender = false;	
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeUpdate' ); 
			 
			
			$processedOrder =  $this->OpenOrder->find('all', array(
													'joins' => array(
														array(
															'table' => 'merge_updates',
															'alias' => 'MergeUpdate',
															'type' => 'INNER',
															'conditions' => array(
																'OpenOrder.num_order_id = MergeUpdate.order_id',
																 'OpenOrder.date >=' => $this->request->data['last_updated'],
																 'OpenOrder.sub_source LIKE ?' =>  $this->request->data['sub_source'].'%',
																 
															)
														)			
													),											
													'fields' => array(																
																'OpenOrder.num_order_id',	
																'OpenOrder.date',
																'OpenOrder.status',
																'MergeUpdate.order_id',
																'MergeUpdate.product_order_id_identify',										
																'MergeUpdate.service_name',
																'MergeUpdate.provider_ref_code',
																'MergeUpdate.service_provider',
																'MergeUpdate.status as mstatus'
																),
													'order' => array('OpenOrder.num_order_id' => 'ASC') 	
													));
													
		 
		if(count($processedOrder) > 0){
			foreach($processedOrder as $val){
				$data[] = array( 				
					'num_order_id'				=> $val['OpenOrder']['num_order_id'], 
					'date'						=> $val['OpenOrder']['date'],
					'status'					=> $val['OpenOrder']['status'],
					'product_order_id_identify'	=> $val['MergeUpdate']['product_order_id_identify'],
					'ref_code'					=> $val['MergeUpdate']['provider_ref_code'],
					'service_name'				=> $val['MergeUpdate']['service_name'],
					'service_provider'			=> $val['MergeUpdate']['service_provider']
				 );
			}
		}else{
			$data['msg'] = 'order_not_found';
		}
		echo json_encode($data);		
		exit; 
													
	}
	
	public function getCanceledOrders()
	{		
		$this->layout = '';
		$this->autoRender = false;	
		$this->loadModel( 'OpenOrder' );
		$data  = array();
		$_date = date('Y-m-d', strtotime('-7 days',strtotime(date('Y-m-d'))));
		
		$params = array('conditions' => array('OpenOrder.status > ' => 1,'OpenOrder.open_order_date > ' =>  $_date ),							
						'fields' => array(										
										'OpenOrder.num_order_id',
										'OpenOrder.open_order_date',
										'OpenOrder.date',										
										'OpenOrder.status'										
										)
									);
									
		
		
		$processedOrder = $this->OpenOrder->find( 'all', $params );
		
		foreach($processedOrder as $val){
			$data[] = array( 				
				'num_order_id'			=> $val['OpenOrder']['num_order_id'], 
				'open_order_date'		=> $val['OpenOrder']['open_order_date'],
				'date'					=> $val['OpenOrder']['date'],
				'status'				=> $val['OpenOrder']['status']
			 );
		}
		echo json_encode($data);		
		exit; 
	}	
	
	public function getSortedOrders()
	{		
		$this->layout = '';
		$this->autoRender = false;	
		$this->loadModel( 'MergeUpdate' );
		$data  = array();
	/*	$t= array('101337','101335','101336');
		$params = array('conditions' => array('sorted_scanned' => 1, 'order_id IN ' =>  $t ),											
						'fields' => array(										
										'MergeUpdate.order_id',
										'MergeUpdate.product_order_id_identify',										
										'MergeUpdate.service_name',
										'MergeUpdate.provider_ref_code',
										'MergeUpdate.service_provider',
										'MergeUpdate.status'
										),
						'limit' =>99
									);*/
		$params = array('conditions' => array('order_id IN ' =>  $this->request->data ),							
						'fields' => array(										
										'MergeUpdate.order_id',
										'MergeUpdate.product_order_id_identify',										
										'MergeUpdate.service_name',
										'MergeUpdate.provider_ref_code',
										'MergeUpdate.service_provider',
										'MergeUpdate.status'
										),
							'limit' =>9999
									);
									
		
		
		$sortedOrder  = $this->MergeUpdate->find( 'all', $params );
		
		foreach($sortedOrder as $val){
			$data[] = array( 				
				'num_order_id'					=> $val['MergeUpdate']['order_id'], 
				'product_order_id_identify'		=> $val['MergeUpdate']['product_order_id_identify'],
				'service_name'					=> $val['MergeUpdate']['service_name'], 
				'ref_code'						=> $val['MergeUpdate']['provider_ref_code'], 
				'service_provider'				=> $val['MergeUpdate']['service_provider'], 
				'status'						=> $val['MergeUpdate']['status']
			 );
		}
		echo json_encode($data);		
		exit; 
	}	
	
	public function getSplitOrders()
	{		
		$this->layout = '';
		$this->autoRender = false;	
		$this->loadModel( 'MergeUpdate' );
		$data  = array();	 
		
		$params = array('conditions' => array('MergeUpdate.order_id ' => $this->request->data ),											
						'fields' => array(										
										'MergeUpdate.order_id',
										'MergeUpdate.product_order_id_identify',
										'MergeUpdate.service_name',
										'MergeUpdate.provider_ref_code',
										'MergeUpdate.service_provider',
										'MergeUpdate.packet_weight',
										'MergeUpdate.packet_length',
										'MergeUpdate.packet_width',
										'MergeUpdate.packet_height',
										'MergeUpdate.envelope_weight',
										'MergeUpdate.quantity',
										'MergeUpdate.status'
										)
									);
		$sortedOrder  = $this->MergeUpdate->find( 'all', $params );
		if(count($sortedOrder) > 1){
			foreach($sortedOrder as $val){
				$data[] = array( 				
					'num_order_id'					=> $val['MergeUpdate']['order_id'], 
					'product_order_id_identify'		=> $val['MergeUpdate']['product_order_id_identify'],
					'service_name'					=> $val['MergeUpdate']['service_name'], 
					'ref_code'						=> $val['MergeUpdate']['provider_ref_code'], 
					'service_provider'				=> $val['MergeUpdate']['service_provider'], 					
					'packet_weight'					=> $val['MergeUpdate']['packet_weight'], 
					'packet_length'					=> $val['MergeUpdate']['packet_length'], 
					'packet_width'					=> $val['MergeUpdate']['packet_width'], 
					'packet_height'					=> $val['MergeUpdate']['packet_height'], 
					'envelope_weight'				=> $val['MergeUpdate']['envelope_weight'], 
					'quantity'						=> $val['MergeUpdate']['quantity'], 
					'status'						=> $val['MergeUpdate']['status']
				 );
			}
		}else{		
			$data  =  array('no data found',$this->request->data);	 
		}
		
		echo json_encode($data);		
		exit; 
	}	
	
	public function getClientOrder()
	{
				
				$configRate = Configure::read('configuration');
				$this->maxValue = $configRate['hiddenConfigBridge']['max_value'];
				$this->minValue  = $configRate['hiddenConfigBridge']['min_value'];;
				$this->conversionRateValue  = $configRate['hiddenConfigBridge']['conversion_rate'];;
				
				$this->autoLayout = 'ajax';
				$this->autoRender = false;
				
				$this->loadModel('Product');
				$this->loadModel('ProductDesc');
				
					

				App::import('Vendor', 'PHPExcel/IOFactory');
			
				$fName    = 'http://techdrive.biz/LinnworksOrdersExport/order.csv'; 
				$f_Name   =  WWW_ROOT .'orders_csv'.DS.'order.csv'; 
									
				file_put_contents( $f_Name, file_get_contents($fName));	
				 
				$oderIdInitial = array();
				$file = fopen($f_Name,"r");
				$i = 0;
				while(! feof($file))
				{
					
					$innerOrderId = fgetcsv($file);
					if($i > 0 && $innerOrderId[0]!=''){
						$oderIdInitial[$i][0] = $innerOrderId[0];
						$orderItems[$innerOrderId[0]][] = $innerOrderId[0];
					}
					$i++;							
				}
				fclose($file);
					if( count( $oderIdInitial ) > 0 )
				{
					
					$objPHPExcel = new PHPExcel();
					$objReader   = PHPExcel_IOFactory::createReader('CSV');
					$objReader->setReadDataOnly(true);		
							
					$objPHPExcel 	= $objReader->load($f_Name);					
					$objWorksheet	= $objPHPExcel->setActiveSheetIndex(0);
					$lastRow 		= $objPHPExcel->getActiveSheet()->getHighestRow();
					$colString		= $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
					$colNumber 		= PHPExcel_Cell::columnIndexFromString($colString);
					$getClientData  = array();
					//no of different orders
					
					for( $inId = 1; $inId <= count( $oderIdInitial ); $inId++ )
					{							
						//order id
						$clientOrderId = $oderIdInitial[$inId][0];
						//outer flags
						$inc = 1;
						$rand = 1001;
					 
						//find the order related to order id													
						for($i=2,$innerIndex = 0; $i <= $lastRow;$i++) 
						{
							$rand++;
							//now area of storing the order according to 
							$cell = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();								
							
							//seeking
							if( $cell == $clientOrderId )
							{
								
								if( $innerIndex == 0 )
								{
									$file_name = date("dmy_Hi");
									//$orderIdSet = strtotime(date('Y-m-d H:i:s')) ;
									$exchangeRate = $objWorksheet->getCellByColumnAndRow(24,$i)->getValue();
									$orderIdSet = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
									$customOrderId = $orderIdSet;
									$getClientData[0]['file_name'] = $file_name.'.csv';
									$getClientData[0]['OrderId'] = $customOrderId;
									$getClientData[0]['NumOrderId'] = $orderIdSet;
									$ReferenceNum = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();//$orderIdSet;
									$status = 1;
									//if($objWorksheet->getCellByColumnAndRow(32,$i)->getValue() == "Paid"){
									//	$status = 1;
									//}
									$getClientData[0]['GeneralInfo']['Status'] = $objWorksheet->getCellByColumnAndRow(32,$i)->getValue();
									$getClientData[0]['GeneralInfo']['Notes'] = $objWorksheet->getCellByColumnAndRow(63,$i)->getValue();
									$getClientData[0]['GeneralInfo']['Marker'] = $objWorksheet->getCellByColumnAndRow(44,$i)->getValue();
									$getClientData[0]['GeneralInfo']['ReferenceNum'] = $ReferenceNum;
									$getClientData[0]['GeneralInfo']['ExternalReferenceNum'] = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
									
									$purchase_date = str_replace("/","-",$objWorksheet->getCellByColumnAndRow(26,$i)->getValue());				
									
									$getClientData[0]['GeneralInfo']['ReceivedDate'] = date("Y-m-d H:i:s",strtotime($purchase_date));
									$getClientData[0]['GeneralInfo']['Source'] =  $objWorksheet->getCellByColumnAndRow(33,$i)->getValue();
									$getClientData[0]['GeneralInfo']['SubSource'] = $objWorksheet->getCellByColumnAndRow(34,$i)->getValue();
									$getClientData[0]['GeneralInfo']['HoldOrCancel'] = $objWorksheet->getCellByColumnAndRow(46,$i)->getValue();
									$getClientData[0]['GeneralInfo']['DespatchByDate'] = date("Y-m-d H:i:s",strtotime($purchase_date + "+3 days"));										
									$getClientData[0]['GeneralInfo']['Location'] = '11c4663e-06b8-40ea-8af4-b7b675a55555';
									
									
									$getClientData[0]['ShippingInfo']['PostalServiceId'] = 'eba09b4d-cc52-44d6-ad58-cb8df1122222';									
									$getClientData[0]['ShippingInfo']['PostalServiceName'] = $objWorksheet->getCellByColumnAndRow(37,$i)->getValue();									
									$getClientData[0]['ShippingInfo']['TotalWeight'] = $objWorksheet->getCellByColumnAndRow(64,$i)->getValue();
									$getClientData[0]['ShippingInfo']['ItemWeight'] = 0;
									$getClientData[0]['ShippingInfo']['PackageCategoryId'] = '00000000-0000-0000-0000-000000000000';
									$getClientData[0]['ShippingInfo']['PackageCategory'] = $objWorksheet->getCellByColumnAndRow(41,$i)->getValue();
									$getClientData[0]['ShippingInfo']['PackageTypeId'] = '00000000-0000-0000-0000-000000000000';
									$getClientData[0]['ShippingInfo']['PackageType'] = 'Default';
									$getClientData[0]['ShippingInfo']['PostageCost'] = $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
									$getClientData[0]['ShippingInfo']['PostageCostExTax'] = '';//$objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
									$getClientData[0]['ShippingInfo']['TrackingNumber'] = $objWorksheet->getCellByColumnAndRow(42,$i)->getValue();
									$getClientData[0]['ShippingInfo']['ManualAdjust'] = '';
									
									$getClientData[0]['CustomerInfo']['ChannelBuyerName'] = $objWorksheet->getCellByColumnAndRow(43,$i)->getValue();
									$getClientData[0]['CustomerInfo']['Address']['EmailAddress'] = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
									$getClientData[0]['CustomerInfo']['Address']['RecipentName'] = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
									$getClientData[0]['CustomerInfo']['Address']['Address1'] = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
									$getClientData[0]['CustomerInfo']['Address']['Address2'] = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
									$getClientData[0]['CustomerInfo']['Address']['Address3'] = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
									$getClientData[0]['CustomerInfo']['Address']['Town'] = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
									$getClientData[0]['CustomerInfo']['Address']['Region'] = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
									
									$PostCode = $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
									$pcountry = array('FR','DE','ES');
									$country_code = $objWorksheet->getCellByColumnAndRow(15,$i)->getValue();
									if((strlen($PostCode) < 5) && in_array($country_code,$pcountry)){
										 $PostCode =  '0'.$PostCode;	
									}										 
									$getClientData[0]['CustomerInfo']['Address']['PostCode'] = 	$PostCode;								
									
									
									$getClientData[0]['CustomerInfo']['Address']['Country'] =  $objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
									$getClientData[0]['CustomerInfo']['Address']['FullName'] = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
									$getClientData[0]['CustomerInfo']['Address']['Company'] = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
									$getClientData[0]['CustomerInfo']['Address']['PhoneNumber'] = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();;
									$getClientData[0]['CustomerInfo']['Address']['CountryId'] = '445c01b5-e48c-4002-aef5-888888888888';										
									$getClientData[0]['CustomerInfo']['BillingAddress'] = 1;
									
									$getClientData[0]['TotalsInfo']['PaymentMethod'] = $objWorksheet->getCellByColumnAndRow(45,$i)->getValue();
									$getClientData[0]['TotalsInfo']['PaymentMethodId'] = '00000000-0000-0000-0000-000000000000';
									$getClientData[0]['TotalsInfo']['ProfitMargin'] = 0;
									$getClientData[0]['TotalsInfo']['TotalDiscount'] = 0;
									$getClientData[0]['TotalsInfo']['Currency'] = $objWorksheet->getCellByColumnAndRow(30,$i)->getValue();
									$getClientData[0]['TotalsInfo']['CountryTaxRate'] = '';
									
									$getClientData[0]['FolderName'] = '';
									$getClientData[0]['TotalsInfo']['Subtotal'] = $objWorksheet->getCellByColumnAndRow(58,$i)->getValue();
									$getClientData[0]['TotalsInfo']['PostageCost'] =$objWorksheet->getCellByColumnAndRow(27,$i)->getValue();
									$getClientData[0]['TotalsInfo']['Tax'] = $objWorksheet->getCellByColumnAndRow(28,$i)->getValue();
									$getClientData[0]['TotalsInfo']['TotalCharge'] = $objWorksheet->getCellByColumnAndRow(29,$i)->getValue();
								}	
								 				
								//order ref number customized
								$getClientData[0]['Items'][$innerIndex]['external_order_ref'] = $ReferenceNum;
																		
								$getClientData[0]['Items'][$innerIndex]['OrderId'] = $customOrderId;
								$getClientData[0]['Items'][$innerIndex]['ItemId'] = '00000000-0000-0000-0000-000000000000';
								$getClientData[0]['Items'][$innerIndex]['StockItemId'] = '00000000-0000-0000-0000-999999999999';
								$getClientData[0]['Items'][$innerIndex]['ItemNumber'] = $objWorksheet->getCellByColumnAndRow(52,$i)->getValue();
																
								$getClientData[0]['Items'][$innerIndex]['SKU'] = $objWorksheet->getCellByColumnAndRow(48,$i)->getValue();
								$getClientData[0]['Items'][$innerIndex]['OuterSKU'] = $objWorksheet->getCellByColumnAndRow(48,$i)->getValue();
								$getClientData[0]['Items'][$innerIndex]['ItemSource'] = $objWorksheet->getCellByColumnAndRow(34,$i)->getValue();
								$getClientData[0]['Items'][$innerIndex]['Title'] = $objWorksheet->getCellByColumnAndRow(50,$i)->getValue();
								$getClientData[0]['Items'][$innerIndex]['Quantity'] = $objWorksheet->getCellByColumnAndRow(53,$i)->getValue();
								$getClientData[0]['Items'][$innerIndex]['CategoryName'] = 'Default';
								$getClientData[0]['Items'][$innerIndex]['CompositeAvailablity'] = '';
								$getClientData[0]['Items'][$innerIndex]['RowId'] = 'a020dc1d-4ed1-4fe7-8bfe-333333333333';
								$getClientData[0]['Items'][$innerIndex]['StockLevelsSpecified'] = 1;
								$getClientData[0]['Items'][$innerIndex]['AvailableStock'] = 0;
								$getClientData[0]['Items'][$innerIndex]['PricePerUnit'] =  $objWorksheet->getCellByColumnAndRow(54,$i)->getValue();
								
								$getClientData[0]['Items'][$innerIndex]['UnitCost'] = $objWorksheet->getCellByColumnAndRow(54,$i)->getValue();
								$getClientData[0]['Items'][$innerIndex]['Discount'] = 0;
								$getClientData[0]['Items'][$innerIndex]['Tax'] = $objWorksheet->getCellByColumnAndRow(57,$i)->getValue();
								$getClientData[0]['Items'][$innerIndex]['TaxRate'] =  0;
								$getClientData[0]['Items'][$innerIndex]['Cost'] = $objWorksheet->getCellByColumnAndRow(54,$i)->getValue();
								$getClientData[0]['Items'][$innerIndex]['CostIncTax'] = 0;
								$getClientData[0]['Items'][$innerIndex]['CompositeSubItems'] = '';
								$getClientData[0]['Items'][$innerIndex]['IsService'] = $objWorksheet->getCellByColumnAndRow(60,$i)->getValue();
								$getClientData[0]['Items'][$innerIndex]['SalesTax'] = 0;
								$getClientData[0]['Items'][$innerIndex]['TaxCostInclusive'] = 1;
								$getClientData[0]['Items'][$innerIndex]['PartShipped'] = '';
								$getClientData[0]['Items'][$innerIndex]['Weight'] = 0;
								// fetch from inventery 
								$getClientData[0]['Items'][$innerIndex]['BarcodeNumber'] = '';
								$getClientData[0]['Items'][$innerIndex]['Market'] = 0;
								$getClientData[0]['Items'][$innerIndex]['ChannelSKU'] = $objWorksheet->getCellByColumnAndRow(51,$i)->getValue();
								$getClientData[0]['Items'][$innerIndex]['ChannelTitle'] = $objWorksheet->getCellByColumnAndRow(49,$i)->getValue();
								
								$innerIndex++;	
							}
							
						}
						
						$tt[]=($getClientData);
						
				
						//$getClientData	=	json_decode(json_encode( $getClientData ),0);
						//$this->saveOpenOrder( $getClientData, $locationName, $customOrderId );													
						
						/**********Order Log *************/
						//$orData['order_id'] = $orderIdSet;
						//$orData['data'] 	= json_encode($getClientData);
						//$this->loadModel( 'OrderSheetLog');						
						//if (!$this->OrderSheetLog->hasAny(array('OrderSheetLog.order_id' => $orderIdSet))){
						//	$this->OrderSheetLog->saveAll( $orData );
					//	}							
						/****************************/
						//unset($getClientData);
						
					}
					pr($tt);
					exit;
							
				}	
			 
				 
				App::import('Vendor', 'Linnworks/src/php/Auth');
				App::import('Vendor', 'Linnworks/src/php/Factory');
				App::import('Vendor', 'Linnworks/src/php/Orders');
				
				$username = Configure::read('linnwork_api_username');
				$password = Configure::read('linnwork_api_password');
				
				$token = Configure::read('access_new_token');
				$applicationId = Configure::read('application_id');
				$applicationSecret = Configure::read('application_secret');
				
				
				
				$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	
		
				$token = $auth->Token;	
				$server = $auth->Server;
				$location = '8844999f-8e27-4155-949b-5b8bb81def86'; 
				
				/***************************Save AND Get Previous Cron Run Time *******************************/
				$previous_cron_time = "-1 day";
				
				if (file_exists( WWW_ROOT .'orders_csv/'.'cron_time.txt')) {
					$previous_cron_time = file_get_contents( WWW_ROOT .'orders_csv/'.'cron_time.txt');		
				}else{
					$fh = fopen( WWW_ROOT .'orders_csv/'.'cron_time.txt', 'w');
					fwrite($fh,"\n");
					fclose($fh);
				}
				
		
				$DateFrom = date("Y-m-d\TH:i:s",strtotime($previous_cron_time)).'.000Z';		
				$DateTo = date("Y-m-d\TH:i:s").'.000Z';		
				$time_start = microtime(true);
		
				$filters = array("DateFields" => array( 
													array("FieldCode" => "GENERAL_INFO_DATE", 
														  "Type" => "Range", 
														  "DateFrom" => $DateFrom, 
														  "DateTo" =>$DateTo)));
		
		
				//file_put_contents( WWW_ROOT .'orders_csv/'.'cron_time.txt', date ("Y-m-d H:i:s"));							
		 
				$openorder	= OrdersMethods::GetAllOpenOrders($filters,'',$location,'',$token, $server);
				pr($filters);
				pr($openorder);
				exit;
					
					
					
					//pr($orderItems);
					//exit;
					
					 				
				
				
				
			}	
			
	public function getBrtParcelNumberForBest()
	{
		$this->loadModel('BrtSerialNo');
		
		$brtSerialNo = $this->BrtSerialNo->find('first', array('order' => 'id desc') );
		$parcel_no   =  '0000001';
		if(count($brtSerialNo) > 0){
			$parcel_no = $brtSerialNo['BrtSerialNo']['parcel_no'] + 1;
		}else{
			$parcel_no = $parcel_no + 1;
		}
		echo json_encode(array('parcel_no' => $parcel_no));
		exit;
	}
	
	public function updateBrtParcelNumber()
	{
		$this->loadModel('BrtSerialNo');
		$this->BrtSerialNo->saveAll($this->request->data);
		file_put_contents(WWW_ROOT.'logs/bestbuy_responce.log', "\n".date('d-m-y H:i:s')."\t".print_r($this->request->data,true)."\n", FILE_APPEND | LOCK_EX);	
	}
			
			

    
}

?>

