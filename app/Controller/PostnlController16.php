<?php
class PostnlController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Postnl";
    var $components = array('Session','Upload','Common','Auth','Paginator');
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
    public function Barcode()
    {
		  $this->layout = '';
		  $this->autoRender = false;
		  $CustomerCode = 'HLRQ';
		  $CustomerNumber = '10472686';
		  $api_key = 'Z7J23nyK3BOEPkR3pgIVAAcOKheVCTDV';
		  
		  $curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api-sandbox.postnl.nl/shipment/v1_1/barcode?CustomerCode={$CustomerCode}&CustomerNumber={$CustomerNumber}&Type=3S&Serie=0000000-9999999",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(				 
				"apikey: {$api_key}",
				"cache-control: no-cache"
			  ),
			));
			
			echo $response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
			  $r = json_decode($response,1);
			  return $r['Barcode'];
			}
		exit;  
	}
	
	public function Shipment()
	{

$p = '{
    "Customer": {
        "CustomerNumber": "11223344",
        "CustomerCode": "DEVC",
        "CollectionLocation": "1234506",
        "Address": {
            "AddressType": "02",
            "City": "Hoofddorp",
            "Countrycode": "NL",
            "Area": "<string>",
            "Buildingname": "<string>",
            "CompanyName": "PostNL",
            "Department": "<string>",
            "Doorcode": "<string>",
            "FirstName": "Peter",
            "Floor": "<string>",
            "HouseNr": "42",
            "HouseNrExt": "<string>",
            "Name": "de Ruiter",
            "Region": "<string>",
            "Street": "Siriusdreef",
            "StreetHouseNrExt": "<string>",
            "Zipcode": "2132WT"
        },
        "ContactPerson": "Janssen",
        "Email": "email@company.com",
        "Name": "Janssen"
    },
    "Message": {
        "MessageID": "1",
        "MessageTimeStamp": "29-06-2016 12:00:00",
        "Printertype": "GraphicFile|PDF"
    },
    "Shipments": [
        {
            "Addresses": [
                {
                    "AddressType": "02",
                    "Countrycode": "NL",
                    "Area": "<string>",
                    "Buildingname": "<string>",
                    "City": "Hoofddorp",
                    "CompanyName": "PostNL",
                    "Department": "<string>",
                    "Doorcode": "<string>",
                    "FirstName": "Peter",
                    "Floor": "<string>",
                    "HouseNr": "42",
                    "HouseNrExt": "<string>",
                    "Name": "de Ruiter",
                    "Region": "<string>",
                    "Street": "Siriusdreef",
                    "StreetHouseNrExt": "<string>",
                    "Zipcode": "2132WT"
                },
                {
                    "AddressType": "02",
                    "Countrycode": "NL",
                    "Area": "<string>",
                    "Buildingname": "<string>",
                    "City": "Hoofddorp",
                    "CompanyName": "PostNL",
                    "Department": "<string>",
                    "Doorcode": "<string>",
                    "FirstName": "Peter",
                    "Floor": "<string>",
                    "HouseNr": "42",
                    "HouseNrExt": "<string>",
                    "Name": "de Ruiter",
                    "Region": "<string>",
                    "Street": "Siriusdreef",
                    "StreetHouseNrExt": "<string>",
                    "Zipcode": "2132WT"
                }
            ],
            "Barcode": "3SDEVC2016112104",
            "Dimension": {
                "Weight": "2000",
                "Height": "<string>",
                "Length": "<string>",
                "Volume": "<string>",
                "Width": "<string>"
            },
            "ProductCodeDelivery": "3085",
            "Amounts": [
                {
                    "AmountType": "01",
                    "Value": "100.00",
                    "AccountName": "<string>",
                    "BIC": "<string>",
                    "Currency": "EUR",
                    "IBAN": "<string>",
                    "Reference": "<string>",
                    "TransactionNumber": "<string>"
                },
                {
                    "AmountType": "01",
                    "Value": "100.00",
                    "AccountName": "<string>",
                    "BIC": "<string>",
                    "Currency": "EUR",
                    "IBAN": "<string>",
                    "Reference": "<string>",
                    "TransactionNumber": "<string>"
                }
            ],
            "CodingText": "<string>",
            "CollectionTimeStampStart": "<string>",
            "CollectionTimeStampEnd": "<string>",
            "Contacts": [
                {
                    "ContactType": "01",
                    "Email": "receiver@email.com",
                    "SMSNr": "+31612345678",
                    "TelNr": "+31301234567"
                },
                {
                    "ContactType": "01",
                    "Email": "receiver@email.com",
                    "SMSNr": "+31612345678",
                    "TelNr": "+31301234567"
                }
            ],
            "Content": "<string>",
            "CostCenter": "<string>",
            "CustomerOrderNumber": "<string>",
            "Customs": [
                {
                    "Content": [
                        {
                            "Description": "Powdered milk",
                            "Quantity": "2",
                            "Weight": "2600",
                            "Value": "20.00",
                            "EAN": "<string>",
                            "ProductURL": "<string>",
                            "HSTariffNr": "100878",
                            "CountryOfOrigin": "NL"
                        },
                        {
                            "Description": "Powdered milk",
                            "Quantity": "2",
                            "Weight": "2600",
                            "Value": "20.00",
                            "EAN": "<string>",
                            "ProductURL": "<string>",
                            "HSTariffNr": "100878",
                            "CountryOfOrigin": "NL"
                        }
                    ],
                    "Currency": "EUR",
                    "Certificate": "<boolean>",
                    "CertificateNr": "<string>",
                    "License": "<boolean>",
                    "LicenseNr": "<string>",
                    "Invoice": "<boolean>",
                    "InvoiceNr": "INV_0120330",
                    "HandleAsNonDeliverable": "<boolean>",
                    "ShipmentType": "Commercial Goods",
                    "TrustedShipperID": "<string>",
                    "ImporterReferenceCode": "<string>",
                    "TransactionCode": "<string>",
                    "TransactionDescription": "<string>"
                },
                {
                    "Content": [
                        {
                            "Description": "Powdered milk",
                            "Quantity": "2",
                            "Weight": "2600",
                            "Value": "20.00",
                            "EAN": "<string>",
                            "ProductURL": "<string>",
                            "HSTariffNr": "100878",
                            "CountryOfOrigin": "NL"
                        },
                        {
                            "Description": "Powdered milk",
                            "Quantity": "2",
                            "Weight": "2600",
                            "Value": "20.00",
                            "EAN": "<string>",
                            "ProductURL": "<string>",
                            "HSTariffNr": "100878",
                            "CountryOfOrigin": "NL"
                        }
                    ],
                    "Currency": "EUR",
                    "Certificate": "<boolean>",
                    "CertificateNr": "<string>",
                    "License": "<boolean>",
                    "LicenseNr": "<string>",
                    "Invoice": "<boolean>",
                    "InvoiceNr": "INV_0120330",
                    "HandleAsNonDeliverable": "<boolean>",
                    "ShipmentType": "Commercial Goods",
                    "TrustedShipperID": "<string>",
                    "ImporterReferenceCode": "<string>",
                    "TransactionCode": "<string>",
                    "TransactionDescription": "<string>"
                }
            ],
            "DeliveryAddress": "01",
            "DeliveryDate": "<string>",
            "DeliveryTimeStampStart": "<string>",
            "DeliveryTimeStampEnd": "<string>",
            "DownPartnerBarcode": "<string>",
            "DownPartnerID": "<string>",
            "DownPartnerLocation": "<string>",
			                         
            "ReceiverDateOfBirth": "<string>",
            "Reference": "<string>",
            "ReferenceCollect": "<string>",
            "Remark": "<string>",
            "ReturnBarcode": "<string>",
            "ReturnReference": "<string>", 
        } 
    ] 
}';
pr(json_decode($p,1));exit;
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api-sandbox.postnl.nl/v1/shipment?confirm=true",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>"{ }",
		  CURLOPT_HTTPHEADER => array(
			"apikey: <string>",
			"Content-Type: application/json"
		  ),
		));
		
		$response = curl_exec($curl);
		
		curl_close($curl);
		echo $response;
		exit;
	}
	
	public function Confirming()
	{
 		$this->loadModel('PostnlBarcode');
 		//$sender = $this->getSenderInfo($data['sub_source']);
		$CustomerCode = 'HLRQ';
		$CustomerNumber = '10472686';
		$api_key = 'Z7J23nyK3BOEPkR3pgIVAAcOKheVCTDV';
		
		$pnlData = $this->PostnlBarcode->find('all',array('conditions' => array('confirming_date IS NULL')));
		if(count($pnlData ) > 0){
			 foreach($pnlData as $pnl){
			    echo "<br>"; 
				$pdata =	array(
				'Customer' => array(json_decode($pnl['PostnlBarcode']['customer'],1)),		
				'Message' => array(json_decode($pnl['PostnlBarcode']['message'],1)),		
				'Shipments' => array(json_decode($pnl['PostnlBarcode']['shipments'],1))
				);
	
				$curl = curl_init();
				
				curl_setopt_array($curl, array(
				  CURLOPT_URL => "https://api-sandbox.postnl.nl/shipment/v2/confirm",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => json_encode($pdata),
				  CURLOPT_HTTPHEADER => array(
					"Content-Type: application/json",
					"Postman-Token: cf1b0480-c8eb-4a20-ab15-cf86e06e4422",
					"apikey: {$api_key}",
					"cache-control: no-cache"
				  ),
				));
				
				$response = curl_exec($curl);
				$err = curl_error($curl);
				
				curl_close($curl);
				
				if ($err) {
				  echo "cURL Error #:" . $err;
				} else {
  					 // echo $response; 
					  $res = json_decode($response,1);  
					  if(isset($res['ResponseShipments'][0]['Errors']) && count($res['ResponseShipments'][0]['Errors']) > 0){
						 pr($res); 
						 echo  $pnl['PostnlBarcode']['split_order_id'] ;
						 pr($pdata);
						 file_put_contents(WWW_ROOT.'logs/postnl_confirming_error_'.$pnl['PostnlBarcode']['split_order_id'].'.log',$response);
					  }else{
						$d['confirming_date']  = date('Y-m-d H:i:s') ;
						$d['id'] = $pnl['PostnlBarcode']['id']; 
						$this->PostnlBarcode->saveAll($d);
 					 }
 				}
			}
		}
		exit;
	}  
	

	public function Labelling()
 	{
		$this->loadModel('MergeUpdate');
		$this->loadModel('OpenOrder');
		$this->loadModel('Country');
		$this->loadModel('Product');
			//2368559
		$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array('order_id'=>'2888559', 'MergeUpdate.status' => 0,'MergeUpdate.delevery_country !=' => 'Italy'),'fields'=>array('sku','product_order_id_identify','order_id','postnl_barcode'),'limit'=>10 ) );
		 
		foreach($orders	 as $v){
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $v['MergeUpdate']['order_id'] ) ) );
			$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
			
			$country	=	$this->Country->find('first', array( 'conditions' => array('name' =>trim($customer_info->Address->Country)) ) );
			if(count($country) == 0){
				$country	=	$this->Country->find('first', array( 'conditions' => array('custom_name' =>trim($customer_info->Address->Country)) ) );
			}
			
			if(count($country) > 0){
 				 
				if(is_null($v['MergeUpdate']['postnl_barcode']) || $v['MergeUpdate']['postnl_barcode'] == ''){
				 	$postnl_barcode =  $this->Barcode();
					$this->MergeUpdate->updateAll( array( 'MergeUpdate.postnl_barcode' => "'".$postnl_barcode."'") , array( 'MergeUpdate.product_order_id_identify' => $v['MergeUpdate']['product_order_id_identify'] ) );	
				}else{
 					$postnl_barcode =  $v['MergeUpdate']['postnl_barcode'];
 				}
				
				$pos = strpos($v['MergeUpdate']['sku'],",");
	
				if ($pos === false) {
						$val  = $v['MergeUpdate']['sku'];			
						$s    = explode("XS-", $val);
						$_qqty = $s[0];
						$_sku = "S-".$s[1];		
							
						$product = $this->Product->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.category_name')));
						$length = $product['ProductDesc']['length'] / 10;
						$width  = $product['ProductDesc']['width'] / 10;
						$height = $product['ProductDesc']['height'] / 10;
						$weight = ($product['ProductDesc']['weight'] * $_qqty ) * 1000;
								
					}else{			
						$sks = explode(",",$v['MergeUpdate']['sku']);
						foreach($sks as $val){
							$s    = explode("XS-", $val);
							$_qqty = $s[0];
							$_sku = "S-".$s[1];	
							$product = $this->Product->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.category_name')));
							$_length[] = $product['ProductDesc']['length'];
							$_width[]  = $product['ProductDesc']['width'];
							$_height[] = $product['ProductDesc']['height'];
							$_weight[] = $product['ProductDesc']['weight'] * $_qqty;					
						}		
						$length	= array_sum($_length) / 10;
						$width	= array_sum($_width) / 10;
						$height	= array_sum($_height) / 10;
						$weight	= array_sum($_weight) * 1000;	 
				}
 				 	
				$data['postnl_barcode'] = $postnl_barcode ;	
				$data['weight'] 		= str_pad($weight,4,'0',STR_PAD_LEFT) ;	
				$data['country_code'] 	= $country['Country']['iso_2'] ;	
				$data['sub_source'] 	= strtolower($openOrder['OpenOrder']['sub_source']) ;
				$data['split_order_id'] = $v['MergeUpdate']['product_order_id_identify'] ;			
				$this->getLabel($data,$customer_info);
			}else{
				pr($customer_info);
			}
 			
		}
		
		exit;
	}  
	
	
	public function getLabel($data, $customer_info)
	{
		$this->loadModel('PostnlBarcode');		
		
		$sender = $this->getSenderInfo($data['sub_source']);
		$CustomerCode = 'HLRQ';
		$CustomerNumber = '10472686';
		$api_key = 'Z7J23nyK3BOEPkR3pgIVAAcOKheVCTDV';
		$customs =	array();
		$ProductCodeDelivery = '4944';
		if($sender['FromCountryCode'] == $data['country_code']){
			$ProductCodeDelivery = '4945';
			
			$customs = array (
					'Content' => 
					array (
					  0 => 
					  array (
						'CountryOfOrigin' => 'NL',
						'Description' => 'Powdered milk',
						'HSTariffNr' => '19019091',
						'Quantity' => '2',
						'Value' => '20.00',
						'Weight' => '4300',
					  ),
					),
					'Currency' => 'EUR',
					'HandleAsNonDeliverable' => 'false',
					'Invoice' => 'true',
					'InvoiceNr' => '22334455',
					'ShipmentType' => 'Gift',
				  ) ;
		}
			
		echo "<br>"; 
			$pdata = array(
			'Customer' => array(
					'Address' => array(
							'AddressType' => '02',
							'City' => $sender['FromCity'],
							'CompanyName' => $sender['FromCompany'] ,
							'Countrycode' => $sender['FromCountryCode'],							 
							'Street' =>  $sender['FromAddress1'] . $sender['FromAddress2'],
							'Zipcode' => $sender['FromPostCode']
						),
		
					'CollectionLocation' => '123456',
					'ContactPerson' => $sender['FromPersonName'],
					'CustomerCode' => $CustomerCode ,
					'CustomerNumber' => $CustomerNumber,
					'Email' => 'shashi@euracogroup.co.uk',
					'Name' => $sender['FromPersonName']
				),
		
			'Message' => array(
					'MessageID' => $data['split_order_id'],
					'MessageTimeStamp' => date('Y-m-d H:i:s'),
					'Printertype' => 'GraphicFile|PDF'
				),
		
			'Shipments' => array(
					'0' => array(
							'Addresses' => array(
										'0' => array(  
  											'AddressType' => '01',
											'City' => $customer_info->Address->Town,
											'Countrycode' =>$data['country_code'], 
											'Name' => $customer_info->Address->FullName,
											'Street' => $customer_info->Address->Address1 .' ' . $customer_info->Address->Address2 .' ' . $customer_info->Address->Address3,
											'Zipcode' => $customer_info->Address->PostCode
										)
		
								),
		
							'Barcode' => $data['postnl_barcode'] ,
							'Contacts' => array(
									'0' => array(
											'ContactType' => 01,
											'Email' =>$customer_info->Address->EmailAddress,
											'TelNr' => '+31612345678',
										)
		
								),
		
							'Dimension' => array(
									'Weight' => $data['weight']
								),
		
							'ProductCodeDelivery' => $ProductCodeDelivery  
						)		
				)
		
			);
			if(count($customs) > 0){
 			 $pdata['Shipments'][0]['Customs'] = $customs;
			}
			
			 
 			$savedata['split_order_id'] = $data['split_order_id'];
			$savedata['barcode'] 		= $data['postnl_barcode'];
			$savedata['customer'] 		= json_encode($pdata['Customer']);
			$savedata['message'] 		= json_encode($pdata['Message']);
			$savedata['shipments'] 		= json_encode($pdata['Shipments']) ;
			$savedata['timestamp'] 		= date('Y-m-d H:i:s');
 			
			$pnl = $this->PostnlBarcode->find('first',array('conditions' => array('split_order_id' => $data['split_order_id']),'fields'=>array('id','barcode')));
			if(count($pnl) > 0){
				$savedata['id'] =  $pnl['PostnlBarcode']['id'] ;
			} 
			
			$this->PostnlBarcode->saveAll($savedata);
			
			file_put_contents(WWW_ROOT.'logs/'.$data['split_order_id'].'_json.log',json_encode($pdata));
  			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api-sandbox.postnl.nl/shipment/v2_2/label",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>json_encode($pdata),
			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Postman-Token: 0218cb1c-6b0e-48af-bbee-f67fe893b579",
				"apikey: {$api_key}",
				"cache-control: no-cache"
			  ),
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
				 // echo $response; 
				  $res = json_decode($response,1);
				  if(isset($res['Errors']) && count($res['Errors']) > 0){
				 	 pr($res); echo  $data['split_order_id'] ;
					 pr($pdata);
				  }else{
					 $content = base64_decode($res['ResponseShipments']['0']['Labels']['0']['Content']);
					 $file_name = 'label_'.$data['split_order_id'].'.pdf';
					 file_put_contents(WWW_ROOT.'logs/'.$file_name,$content);	
				 }
			} 
			
			   	
	}
	
	
	public function getSenderInfo($sub_source = '')
	{
	
		$FromCompany = 'EURACO GROUP LTD';
		/*-------Updated on 12-02-2020-----*/
		$FromPersonName = 'C/O ProFulfillment and Logistics';
		$FromAddress1 = 'Unit 4 Airport Cargo Centre';
		$FromAddress2 = 'L\'avenue De La Commune';
		$FromCity = 'St Peter';
		$FromDivision = 'JE';
		$FromPostCode = 'JE3 7BY';
		$FromCountryCode = 'GB';
		$FromCountryName = 'JERSEY';
		$FromPhoneNumber = '+44 3301170104';
		
		if(strpos($sub_source,'costbreaker')!== false){
			$FromCompany = 'EURACO GROUP LTD';
			/*-------Updated on 12-02-2020-----*/
			$FromPersonName = 'C/O ProFulfillment and Logistics';
			$FromAddress1 = 'Unit 4 Airport Cargo Centre';
			$FromAddress2 = 'L\'avenue De La Commune';
			$FromCity = 'St Peter';
			$FromDivision = 'JE';
			$FromPostCode = 'JE3 7BY';
			$FromCountryCode = 'GB';
			$FromCountryName = 'JERSEY';
			$FromPhoneNumber = '+44 3301170104';
		}else if(strpos($sub_source,'marec')!== false){
			$FromCompany = 'ESL LIMITED';
			$FromPersonName = 'ESL LIMITED';
			/*-------Updated on 12-02-2020-----*/
			$FromAddress1 = 'Unit 4 Airport Cargo Centre';
			$FromAddress2 = 'L\'avenue De La Commune';
			$FromCity = 'St Peter';
			$FromDivision = 'JE';
			$FromPostCode = 'JE3 7BY';
			$FromCountryCode = 'GB';
			$FromCountryName = 'JERSEY';
			$FromPhoneNumber = '+443301170238';
		 }	
		else if(strpos($sub_source,'rainbow')!== false){
			$FromCompany = 'FRESHER BUSINESS LIMITED';
			$FromPersonName = 'C/O ProFulfillment and Logistics';
			/*-------Updated on 12-02-2020-----*/
			$FromAddress1 = 'Unit 4 Airport Cargo Centre';
			$FromAddress2 = 'L\'avenue De La Commune';
			$FromCity = 'St Peter';
			$FromDivision = 'JE';
			$FromPostCode = 'JE3 7BY';
			$FromCountryCode = 'GB';
			$FromCountryName = 'JERSEY';
			$FromPhoneNumber = '0123456789';
		}
		
		$senderData['FromCompany']	   =    $FromCompany;
		$senderData['FromPersonName']  =	$FromPersonName;
		$senderData['FromAddress1']	   =	$FromAddress1 ;
		$senderData['FromAddress2']	   =	$FromAddress2;
		$senderData['FromCity'] 	   =	$FromCity;
		$senderData['FromDivision']    =	$FromDivision;
		$senderData['FromPostCode']    =	$FromPostCode;
		$senderData['FromCountryCode'] =	$FromCountryCode;
		$senderData['FromCountryName'] =	$FromCountryName;
		$senderData['FromPhoneNumber'] =	$FromPhoneNumber;

 		return $senderData;	
	}
	
	 public function updateManifestDate($merge_id)
	   {
		  $this->loadModel('MergeUpdate'); 
		  date_default_timezone_set('Europe/Jersey');
		  $firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		  $lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		  $manifest_username = $firstName.' '.$lastName;
		  $data['id'] = $merge_id;   
		  $data['manifest_date'] = date('Y-m-d H:i:s');   
		  $data['manifest_username'] 	= $manifest_username;   
		  $this->MergeUpdate->saveAll( $data);	    	
	   }
    
}
?>
