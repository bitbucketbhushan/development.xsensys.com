<?php
error_reporting(1);
App::uses('Folder', 'Utility');
App::uses('File', 'Utility'); 
class WhistlApiController extends AppController
{
    
    var $name = "WhistlApi";    
    var $components = array('Session', 'Common', 'Upload','Auth');    
    var $helpers = array('Html','Form','Common','Session');
   
   public function beforeFilter()
   {
		parent::beforeFilter();
		$this->layout = false;
		$this->Auth->Allow(array('applyWhistl'));
		$this->common = $this->Components->load('Common'); 
		//customApply
    }
    
	public function index(){ } 
	
	public function applyWhistl($order_id = null){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'PostalServiceDesc' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'Country' );
		$this->loadModel( 'OpenOrder' );	 
		$this->loadModel( 'ProductHscode') ;
	 	
		$country_of_origin = 'CN';
		$hs_code[]		   = '9603298000';
		$return 		   = array();
		$msg 			   = '';
		 
		$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id,'MergeUpdate.status' => 0 , 'MergeUpdate.delevery_country' => 'United Kingdom')));	
		 
		if(count($orders) >0){
 			foreach($orders  as $orderItem){
 				
				$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id,'OpenOrder.destination' => 'United Kingdom' ) ) );
				if(count($openOrder) > 0){
					
					$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
					$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
					$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']); 
					
					$sdata = $this->getSplitOrderDetails($orderItem['MergeUpdate']['product_order_id_identify'],$customer_info);
					
					$quantity 	   = $sdata['quantity'];
					$order_id 	   = $orderItem['MergeUpdate']['order_id'];
					$sub_source    = $orderItem['MergeUpdate']['source_coming'];
					$ebay_source   = '';
					if(strpos(strtolower($sub_source), 'ebay') !== false){
						$ebay_source = 'ebay';
					}
					
					$dim = array($sdata['packet_width'],$sdata['packet_height'],$sdata['packet_length']);
					asort($dim);
					$final_dim = array_values($dim) ;							
					$whPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $sdata['packet_weight'],'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'Whistl', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
					//  pr( $whPostal);
					
					$all_whistl = array();  
					$whistlFee = 0; 
					if(count($whPostal) > 0)
					{
						foreach($whPostal as $rv){
							$per_item 			= $rv['PostalServiceDesc']['per_item'];
							$psd_id 			= $rv['PostalServiceDesc']['id'];								 						
							$all_whistl[$psd_id] = $per_item ;	 
						} 
						 
						$whistlFee   = min($all_whistl);	
						$post_id     = array_search($whistlFee, $all_whistl); 
						$post_whistl = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));	
						
						if($orderItem['MergeUpdate']['packet_weight'] == 0){
							$data['packet_weight']  = $sdata['packet_weight'];
						}
						 
						$data['total_charge']    	=  $totals_info->TotalCharge ;
						$data['hs_code']  		 	=  implode(";",$sdata['hs_code']);
						$data['sku']  		 		=  $sdata['sku'] ;
						$data['country_of_origin']  =  $sdata['country_of_origin'] ;
						$data['product_name']   	=  $sdata['product_name'] ;
						$data['quantity']   		=  $sdata['quantity'] ;  
						$data['weight'] 			=  $sdata['packet_weight'] ;
						$data['length'] 			=  $sdata['packet_length'] ;
						$data['width'] 				=  $sdata['packet_width'] ;
						$data['height'] 			=  $sdata['packet_height'] ;
						$data['country_code'] 		=  $sdata['country_code'];
						$data['country'] 			=  $customer_info->Address->Country;
						$data['phone_number']   	=  $customer_info->Address->PhoneNumber ;
						$data['sub_source'] 		=  strtolower($openOrder['OpenOrder']['sub_source']) ;
						$data['split_order_id'] 	=  $orderItem['MergeUpdate']['product_order_id_identify'] ;				
						$data['track_id'] 			= '';
						$data['reg_post_number'] 	= '';
						$data['reg_num_img'] 		= '';
						$data['postal_service'] 	= $orderItem['MergeUpdate']['postal_service'] ;	
						$data['service_name'] 		= $post_whistl['PostalServiceDesc']['service_name'];
						$data['provider_ref_code'] 	= $post_whistl['PostalServiceDesc']['provider_ref_code'];
						$data['service_id'] 		= $post_whistl['PostalServiceDesc']['id'];
						$data['service_provider'] 	= 'whistl';								 
						$data['id'] 				= $orderItem['MergeUpdate']['id'];
						
						file_put_contents(WWW_ROOT .'logs/whistlapi_'.date('ymd').'.log' , $orderItem['MergeUpdate']['product_order_id_identify']."\t".$orderItem['MergeUpdate']['service_id']."\n",  FILE_APPEND|LOCK_EX);	
						
						$res = $this->getLabel($data,$customer_info);
								 
					}
					else{
					 $msg =  'This order may have over weight or any other issue.<br>';
					}			 
  			    }
				else{
				 $msg =  ' No order found.<br>';
				}		
						
				file_put_contents(WWW_ROOT."logs/whistlapi_app_by_order_".date('Ymd').".log", date('Y-m-d H:i:s')."\t". $order_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);	
			}
		}
		return 1;
	}
	
  	public function getLabel($data, $customer_info)
	{
		$this->loadModel('PostnlBarcode');
		$this->loadModel('MergeUpdate'); 
 		$auth = Configure::read( 'whistl_orion' );
	 
   		$address1 = ''; $address2  = ''; $address3 = '';
		$lines = explode("\n", wordwrap(htmlentities($this->replaceChar($customer_info->Address->Address1)).' '.htmlentities($this->replaceChar($customer_info->Address->Address2)).' '.htmlentities($this->replaceChar($customer_info->Address->Address3)), '30'));
		
		if(isset($lines[0]) && $lines[0] != ''){
			$address1  =  trim($lines[0]) ;
		}
		if(isset($lines[1]) && $lines[1] != ''){
			$address2 =  trim($lines[1]) ;
		}
		if(isset($lines[2]) && $lines[2] != ''){
			$address3 = trim($lines[2]);
		}
		
		 $data['address1'] 		= $address1;
		 $data['address2'] 		= $address2;
		 $data['address3'] 		= $address3;
		 
  		$customer_phone_number = $customer_info->Address->PhoneNumber ;
 			 
    	$unitpricew = $data['total_charge']/$data['quantity'];
		$unitprice = (int)$unitpricew;
		$unitweight =  number_format($data['weight']/$data['quantity'],2);
	 
 	
		$request = array (
	  'DatasheetDetails' => 
		  array (
			'ExtensionData' => NULL,
			'AISTATEMENTCODE' => '',
			'AISTATEMENTCODE2' => '',
			'AISTATEMENTTEXT' => '',
			'AirportName' => 'JER',
			'BagBoxCode' => 'BA1',
			'COMMODITYADDCODE' => '',
			'CONSIGNEEADDRESS1' =>  $data['address1'],
			'CONSIGNEEADDRESS2' =>  $data['address2'],
			'CONSIGNEEADDRESS3' =>  $data['address3'],
			'CONSIGNEECITY' => $this->replaceChar($customer_info->Address->Town),
			'CONSIGNEECOUNTRY' => $data['country_code'],
			'CONSIGNEEID' => '',
			'CONSIGNEENAME' => $this->replaceChar($customer_info->Address->FullName),
			'CONSIGNEEPOSTCODE' => $customer_info->Address->PostCode,
			'CONSIGNEESTATE' => $this->replaceChar($customer_info->Address->Region),
			
			'CONSIGNORADDRESS1' => $auth['consignor_address1'],
			'CONSIGNORADDRESS2' => $auth['consignor_address2'],
			'CONSIGNORADDRESS3' => $auth['consignor_address3'],
			'CONSIGNORCITY' => $auth['consignor_city'],
			'CONSIGNORCOUNTRY' => $auth['consignor_country'],
			'CONSIGNORID' => '',
			'CONSIGNORNAME' => $auth['consignor_name'],
			'CONSIGNORPOSTCODE' => $auth['consignor_postcode'],
			'CONSIGNORSTATE' => $auth['consignor_state'],
			'CPC' => '400006',
			'CURRENCY' => 'GBP',
			'CUSTOMERSHIPMENTID' => $data['split_order_id'].'ORD',
			'Client' => '',
			'ContatinerNo' => '',
			'Custdduid' => '00000000-0000-0000-0000-000000000000',
			'CustomerReference' => $data['split_order_id'],
			'DOCUMENTCODE' => '',
			'DOCUMENTREFERENCE' => '',
			'DOCUMENTSTATUS' => '',
			'DUTYRATE' => NULL,
			'DUTYTYPE' => '',
			'DateOfExport' => date('Y-m-d H:i:s').'.0000000',
			'DimensionsBreadth' => 0,
			'DimensionsHeight' => 0,
			'DimensionsLength' => 0,
			'EMAIL' => $customer_info->Address->EmailAddress,
			'ErrorMsg' => NULL,
			'FormatType' => '',
			'HAWBNo' => 'HA12',
			'INVOICENUMBER' => '',
			'ITEMDESCRIPTION' => $data['product_name'],
			'ITEMGROSSMASS' => 1,
			'ITEMHARMONIZEDCODE' => '',
			'ITEMNETMASS' => $unitweight,
			'ITEMSTATVALUE' => NULL,
			'ITEMTHIRDQUANTITY' => 0,
			'IsLabelPrinting' => true,
			'IsProcessed' => NULL,
			'LINENUMBER' => '123',
			'MawbNo' => 'MRN-'.date("YmdHis"),
			'MawbWeight' => $data['weight'],
			'NoOfBoxBags' => 0,
			'NoOfPallets' => 0,
			'ORIGINCOUNTRY' => 'JE',
			'PACKAGESKIND' => '',
			'PACKAGESMARKSANDNUMBERS' => '',
			'PACKAGESNUMBER' => NULL,
			'PREFERENCE' => NULL,
			'PREVDOCCLASS1' =>NULL,
			'PREVDOCTYPE1' => NULL,
			'PRODUCTCODE' => NULL,
			'PURCHASEORDER' => NULL,
			'PalletCode' => NULL,
			'PreAlertId' => NULL,
			'PreAlertNo' => NULL,
			'QUANTITY' => $data['quantity'],
			'QUOTA' => '',
			'RoutingCode' => $auth['routing_code'],
			'RowNumber' => NULL,
			'SERIALNO' => '',
			'SKU' => $data['sku'].'AQW',
			'ShipmentId' => NULL,
			'Status' => NULL,
			'SubshipmentId' => NULL,
			'TELEPHONE' => $customer_phone_number,
			'TOTALVALUE' => $data['total_charge'],
			'TRACKINGNUMBER' => 'XSTR'.$data['split_order_id'],
			'TotalRowCount' => NULL,
			'UNITPRICE' => $unitprice,
			'VALUATIONADJUSTMENTCODE' => '',
			'VALUATIONADJUSTMENTPERCENTAGE' => '',
			'VALUATIONMETHODCODE' => '',
			'VATRATE' => '',
			'createdon' => NULL,
			'customername' => NULL,
			'isDataSheet' => false,
			'isexported' => false,
			'modifiedon' => NULL,
			'vendor' => 'Whistl',
			'CustomerInvoicedate' => date('Y-m-d'),
			'CustomerInvoiceNumber' => 'INV'.$data['split_order_id'],
			'TermsOfTrade' => 'DAP',
			'CustomerInvoiceType' => 'Proforma',
			'HSCode' => $data['hs_code'],
			'ArticleReference' => '',
			'ManufactureCountryCode' => $data['country_of_origin'],
			'CustomerExporttype' => 'P',
			'CustomerProductTypeContent' => 'Sold',
			'pickuplocation' => 'unit12',
			'IsDocumentOnly' => false,
			'FlightNo' => '1T001',
			'DestAirport' => 'LHR',
			'ETA' => date("Y-m-d", strtotime("+ 2 day")).' 12:07',
			'AirlineCode' => '',
			'ItemId' => '',
			'IsOfflineBarcode' => '0',
		  ),
		  'HeaderCredential' => 
		  array (
			'UserName' => $auth['email'],
			'UserPassword' => utf8_encode("{$auth['password']}"),
		  ),
	);
	//pr($request);
	echo json_encode($request);
 		file_put_contents(WWW_ROOT.'logs/whistlapi_request_'.$data['split_order_id'].'.json',json_encode($request));
 		$email = $auth['email'];
		$password = $auth['password'];
 		$url =  $auth['server_url'];
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>json_encode($request),
		  CURLOPT_HTTPHEADER => array(				 
			"UserName: $email",
			"UserPassword: $password",
			"cache-control: no-cache"
		  ),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		file_put_contents(WWW_ROOT.'logs/whistlapi_response_'.$data['split_order_id'].'.json',$response);
		$responseArray = json_decode($response,true);
 		pr($responseArray);
 		pr($err);
	    
			/*pr($pdata);
			$pdata['LabelSignature'] = base64_encode(file_get_contents(Router::url('/', true).'img/signature.gif'));
			
 			$savedata['split_order_id'] = $data['split_order_id'];
			$savedata['barcode'] 		= $postnl_barcode;
			$savedata['customer'] 		= json_encode($pdata['Customer']);
			$savedata['message'] 		= json_encode($pdata['Message']);
			$savedata['shipments'] 		= json_encode($pdata['Shipments']) ;
			$savedata['customs'] 		= count($customs);
			$savedata['timestamp'] 		= date('Y-m-d H:i:s');
			 
 			/*pr($savedata);
			exit;*/
			//pr($data);pr($pdata);
			
			/*$pnl = $this->PostnlBarcode->find('first',array('conditions' => array('split_order_id' => $data['split_order_id']),'fields'=>array('id','barcode')));
			if(count($pnl) > 0){
				$savedata['id'] =  $pnl['PostnlBarcode']['id'] ;
			} */
			 
			
		 
			exit;;
 	}
	
	public function generateManifest(){
	
  			$this->autoRender = false;
			$this->layout = '';
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'WhistlMrnBagNo' );
			
 			if(isset($this->request->data['mrn_no']) && $this->request->data['mrn_no'] != ''){
				$msg = '';
				$mrn_no  = $this->request->data['mrn_no'];
				$get_bag = $this->WhistlMrnBagNo->find('first', array('order' => 'id DESC'));
 				$bag_no  = $get_bag['WhistlMrnBagNo']['bag_no'] + 1;	
 				$check_mrn = $this->WhistlMrnBagNo->find('first', array('conditions' => array('mrn_no' => $this->request->data['mrn_no'])));
 				if(count($check_mrn) > 0){
					$bag_no = $check_mrn['WhistlMrnBagNo']['bag_no'];	
				}else{
					$mrndata['bag_no'] 	 	= $bag_no;
					$mrndata['mrn_no'] 	 	= $this->request->data['mrn_no'];
					$mrndata['username'] 	= $this->Session->read('Auth.User.username');
					$mrndata['added_date']  = date('Y-m-d H:i:s');
					$this->WhistlMrnBagNo->saveAll($mrndata);
 				}
 							
				$date = date('Y-m-d');//"MergeUpdate.manifest_date LIKE '{$date}%'",
				$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status' => 1,'MergeUpdate.scan_date !=' => '','MergeUpdate.service_provider' => 'whistl','MergeUpdate.delevery_country' => 'United Kingdom'),'fields'=>['id','service_id ','service_name','provider_ref_code','product_order_id_identify','packet_weight','manifest_date','postal_service','order_id','source_coming','service_provider','price']));
			
				$auth = Configure::read('whistl_orion');
				$all_orders = [];   
				if(count($orders) > 0){ 
					$file_name = 'whistlapi_manifest.csv';
					$file = fopen(WWW_ROOT ."logs/".$file_name,"w");
					fputcsv($file, ['LINENUMBER','ORIGAIRPORT','DestAirport','FlightNo','ETA','MAWBNO','MAWBWEIGHT','DATEOFEXPORT','VENDOR','TRACKINGNUMBER','CUSTOMERSHIPMENTID','SKU','ORIGINCOUNTRY','ITEMDESCRIPTION','ITEMHARMONIZEDCODE','CPC','ITEMNETMASS','QUANTITY','UNITPRICE','TOTALVALUE','CURRENCY','CONSIGNORID','Document Reference','CONSIGNORNAME','CONSIGNORADDRESS1','CONSIGNORADDRESS2','CONSIGNORADDRESS3','CONSIGNORCITY','CONSIGNORSTATE','CONSIGNORPOSTCODE','CONSIGNORCOUNTRY','CONSIGNEENAME','CONSIGNEEADDRESS1','CONSIGNEEADDRESS2','CONSIGNEEADDRESS3','CONSIGNEECITY','CONSIGNEESTATE','CONSIGNEEPOSTCODE','CONSIGNEECOUNTRY','AISTATEMENTTEXT','TELEPHONE','EMAIL	HAWBNO','CONTAINERNO','NOOFBOXBAGS','BAGBOXCODE','NOOFPALLETS','PALLETCODE','ROUTINGCODE','FORMATTYPE','ISLABELPRINTING']);
					$counter = 1;
					foreach($orders  as $orderItem){
					
					$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $orderItem['MergeUpdate']['order_id']) ) );
					if(count($openOrder) > 0){
						
						$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
						$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
						$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']); 
						
						$sdata = $this->getSplitOrderDetails($orderItem['MergeUpdate']['product_order_id_identify'],$customer_info);
						
						$quantity 	   = $sdata['quantity'];
						$order_id 	   = $orderItem['MergeUpdate']['order_id'];
						$sub_source    = $orderItem['MergeUpdate']['source_coming']; 
						
						$unit_weight   = number_format(($sdata['packet_weight'] / $sdata['quantity']),2);
						$total_weight  = number_format(($unit_weight * $sdata['quantity']),2);
 						$unit_price    = number_format(($orderItem['MergeUpdate']['price'] / $sdata['quantity']),2);
						$total_price   = number_format(($unit_price * $sdata['quantity']),2);
 						
						$tracking_id = $orderItem['MergeUpdate']['product_order_id_identify'];
						$hs_code = implode(";",$sdata['hs_code']);
						
						$EORI_Number = 'GB019434034000';
						$UK_VAT_Number = 'GB 307817693'; 
						
						$address1 = ''; $address2  = ''; $address3 = '';
						
						$lines = explode("\n", wordwrap(htmlentities($this->replaceChar($customer_info->Address->Address1)).' '.htmlentities($this->replaceChar($customer_info->Address->Address2)).' '.htmlentities($this->replaceChar($customer_info->Address->Address3)), '30'));
						
						if(isset($lines[0]) && $lines[0] != ''){
							$address1  =  trim($lines[0]) ;
						}
						if(isset($lines[1]) && $lines[1] != ''){
							$address2 =  trim($lines[1]) ;
						}
						if(isset($lines[2]) && $lines[2] != ''){
							$address3 = trim($lines[2]);
						} 
						 
						$bag_code = 'BB'. str_pad($bag_no,4,'0',STR_PAD_LEFT) ;  
						fputcsv($file, [$counter,'GBSTH','SEA','GBPME',date("Y-m-d", strtotime("+ 1 day")).'  10:00:00 AM',$mrn_no,$unit_weight,date("Y-m-d"),'Whistl',$tracking_id,$tracking_id,'DDP','JE',$sdata['product_name'],$hs_code,'PM',$total_weight,$sdata['quantity'],'',$total_price,'GBP',$EORI_Number,$UK_VAT_Number,'ESL Limited','Unit 4 Airport Cargo Centre','Le Avenue De La Commune','','St Peter','Jersey','JE3 7BY','JE',$this->replaceChar($customer_info->Address->FullName),$address1,$address2,$address3,$this->replaceChar($customer_info->Address->Town),$this->replaceChar($customer_info->Address->Region),$customer_info->Address->PostCode,$sdata['country_code'],'','','','','',$bag_code,'','',$auth['routing_code']]);
						 $counter++;
						
						//file_put_contents(WWW_ROOT .'logs/whistlapi_'.date('ymd').'.log' , $orderItem['MergeUpdate']['product_order_id_identify']."\t".$orderItem['MergeUpdate']['service_id']."\n",  FILE_APPEND|LOCK_EX);	
					  	 
 					}
					else{
						$msg =  ' No order found.<br>';
					}		
 					 
				}
				
				fclose($file);
				
				header("Content-Type: application/csv");
				header("Content-Disposition: attachment; filename={$file_name}");
				header("Pragma: no-cache");
				readfile(WWW_ROOT."logs/{$file_name}");
				exit;
										
				}else{
					$this->Session->setflash( 'No order found!', 'flash_danger'); 
				}
 			}
		
		  $this->redirect( Router::url( $this->referer(), true ) );
		  exit; 
	}
	
 	public function getBarcodeOutside($spilt_order_id  = null)
	{ 
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'OpenOrder' ); 
		$this->loadModel( 'MergeUpdate' );
		
		$imgPath = WWW_ROOT .'img/orders/barcode/';    
		
		if($spilt_order_id != ''){
		 $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => $spilt_order_id)));
		}else{
		 $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.status' => '0')));
		}
		
		
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
		//$font = new BCGFontFile(APP .'Vendor/barcodegen/font/Arial.ttf', 13);
		$colorFront = new BCGColor(0, 0, 0);
		$colorBack = new BCGColor(255, 255, 255);
		
		if( count($allSplitOrders) > 0 )	
		{
		  foreach( $allSplitOrders as $allSplitOrder )
		  {					  
			  $id 			= 		$allSplitOrder['MergeUpdate']['id'];
			  $openorderid	=	 	$allSplitOrder['MergeUpdate']['product_order_id_identify'];
			  $barcodeimage	=	    $openorderid.'.png';
			  
				$orderbarcode=$openorderid;
				$code128 = new BCGcode128();
				$code128->setScale(2);
				$code128->setThickness(20);
				$code128->setForegroundColor($colorFront);
				$code128->setBackgroundColor($colorBack);
				$code128->setLabel(false);
				$code128->parse($orderbarcode);
									
				//Drawing Part
				$imgOrder128=$orderbarcode.".png";
				$imgOrder128path=$imgPath.'/'.$orderbarcode.".png";
				$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
				$drawing128->setBarcode($code128);
				$drawing128->draw();
				$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);
			  
			  if( $allSplitOrder['MergeUpdate']['product_order_id_identify'] != "" )
			  {   $data['MergeUpdate']['id'] 	=  $id;
				  $data['MergeUpdate']['order_barcode_image'] 	=  $barcodeimage;
				  $this->MergeUpdate->save($data);
			  }
		  }
		}
		
		
	}
		 
   	private function getSplitOrderDetails($split_order_id = null,$customer_info){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'ProductHscode' );
	    $data = [];
		$hs_code = [];
		$country_of_origin = 'CN';
  		$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_order_id)));	
				
		if(count($orderItem) > 0)
		{
			$this->loadModel( 'Product' );
			$this->loadModel( 'ProductDesc' );
			$this->loadModel( 'Country' );	
		
 			$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
			$packet_length = $orderItem['MergeUpdate']['packet_length'];
			$packet_width  = $orderItem['MergeUpdate']['packet_width'];
			$packet_height = $orderItem['MergeUpdate']['packet_height']; 
			
			$_height = array(); $_weight = array(); $length = $width = 0; 
  			
			$country = $this->Country->find('first', array( 'conditions' => array('name' =>trim($customer_info->Address->Country)) ) );
			if(count($country) == 0){
				$country = $this->Country->find('first', array( 'conditions' => array('custom_name' =>trim($customer_info->Address->Country)) ) );
			}
			 
			$pos = strpos($orderItem['MergeUpdate']['sku'],",");
			
			if ($pos === false) {
				$quantity = [];
				$val  = $orderItem['MergeUpdate']['sku'];
				$s    = explode("XS-", $val);
				$_qty = $s[0]; 
				$_sku = "S-".$s[1];	
 						
				$product = $this->Product->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.product_name','Product.country_of_origin','Product.category_name')));
				
				if(count($product) > 0)	{
					$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $_sku,'country_code' => $country['Country']['iso_2'], 'hs_code is NOT NULL' )));
					if(count( $hsresult ) ==  0){				
						$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $_sku,'country_code' => 'GB')));
					}
					if(count( $hsresult ) > 0){
						$hs_code[] = $hsresult['ProductHscode']['hs_code'];	
					}
				
					if($product['Product']['country_of_origin'] != ''){
						$c_code= $this->Country->find('first',array('conditions' => array('Country.name' => $product['Product']['country_of_origin']),'fields'=>array('Country.iso_2'))); 
						if(count($c_code) > 0){
							$country_of_origin = $c_code['Country']['iso_2'];
						}
					}
						
					$length = $product['ProductDesc']['length'];
					$width = $product['ProductDesc']['width'];
					$height[]  = $product['ProductDesc']['height'];
					$weight[]  = ($product['ProductDesc']['weight'] * $_qty );
					$data['category_name'] = $product['Product']['category_name'];	
					$data['product_name'] = $product['Product']['product_name'];										
					$data['country_code'] = $country['Country']['iso_2'];
 					$data['country_of_origin'] = $country_of_origin;						
					$data['hs_code'] = $hs_code;
					$data['sku'] = $_sku;	
					$quantity[] = $_qty;
				}		 
  				
			}else{			
				$sks = explode(",",$orderItem['MergeUpdate']['sku']);
				$_weight = [];	$_height = []; $quantity = [];				 
				foreach($sks as $val){
					$s    = explode("XS-", $val);
					$_qty = $s[0]; 
					$_sku = "S-".$s[1];
					$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.product_name','Product.country_of_origin','Product.category_name')));
					if(count($product) > 0)	{
					
						$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $_sku,'country_code' => $country['Country']['iso_2'], 'hs_code is NOT NULL' )));
						if(count( $hsresult ) ==  0){				
							$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $_sku,'country_code' => 'GB')));
						}
						if(count( $hsresult ) > 0){
							$hs_code[] = $hsresult['ProductHscode']['hs_code'];	
						}
						if($product['Product']['country_of_origin'] != ''){
							$c_code= $this->Country->find('first',array('conditions' => array('Country.name' => $product['Product']['country_of_origin']),'fields'=>array('Country.iso_2'))); 
							if(count($c_code) > 0){
								$country_of_origin = $c_code['Country']['iso_2'];
							}
						}
 						
						$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
						$_height[] = $product['ProductDesc']['height'];
						$length = $product['ProductDesc']['length'];	
						$width  = $product['ProductDesc']['width']; 
						$data['category_name'] = $product['Product']['category_name'];	
						$data['product_name'] = $product['Product']['product_name'];	
						$data['country_code'] = $country['Country']['iso_2'];
						$data['country_of_origin'] = $country_of_origin;	
						$data['hs_code'] = $hs_code;
						$data['sku'] = $_sku;
						$quantity[] = $_qty;
					}				
				}							 			
			}
			 
			if($packet_weight == 0){
				$packet_weight = array_sum($_weight);
			}
			if($packet_height == 0){
				$packet_height = array_sum($_height);
			}
			if($packet_length == 0){
				$packet_length = $length; 
			}
			if($packet_width == 0){
				$packet_width = $width;  
			}
			$data['quantity'] 	   = array_sum($quantity);	
			$data['packet_weight'] = $packet_weight*1000; // KG to Gram
			$data['packet_height'] = $packet_height;
			$data['packet_length'] = $packet_length; 
			$data['packet_width']  = $packet_width;
   		}
		
		return $data;
		
 	}
   	 
	private function sendErrorMail($details = null, $is_manifest = 0){
		$web_store = Configure::read( 'web_store' );
		$subject   = 'RoyalMail issue in '.$web_store;
		$mailBody  = '<p><strong>RoyalMail have some issue please review and solve it.</strong></p>';
		
		if($is_manifest > 0){
			$subject   = 'RoyalMail create manifest issue in '.$web_store;
			$mailBody  = '<p><strong>Create manifest have below issue please review and solve it.</strong></p>';
		}else if(isset($details['split_order_id'])){
			$subject   = $details['split_order_id'].' RoyalMail orders issue in '.$web_store;
			$mailBody  = '<p><strong>'.$details['split_order_id'].' have below issue please review and solve it.</strong></p>';
		}	
		
		$mailBody .= '<p>Error Code : '.$details['error_code'].'</p>';
		$mailBody .= '<p>Error Message : '.$details['error_msg'].'</p>';
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('');
		$email->emailFormat('html');
		$email->from('royal@'.$web_store);
		//$email->to( array('avadhesh.kumar@jijgroup.com','shashi@euracogroup.co.uk','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));	
		if($details['error_code'] == 'E1001'){
			$email->to( array('avadhesh.kumar@jijgroup.com','abhishek@euracogroup.co.uk','deepak@euracogroup.com','vikas.kumar@euracogroup.co.uk','ankit.nagar@euracogroup.co.uk'));
   		}else if(in_array($details['error_code'],array('E0015','E0007','E0005','500'))){
			$email->to( array('avadhesh.kumar@jijgroup.com','shashi@euracogroup.co.uk','abhishek@euracogroup.co.uk','deepak@euracogroup.com','vikas.kumar@euracogroup.co.uk','ankit.nagar@euracogroup.co.uk'));	
		}else{
			$email->to( array('avadhesh.kumar@jijgroup.com'));	
		}	
		
		 			  
		$getBase = Router::url('/', true);
		$email->subject( $subject );
	  	$email->send( $mailBody );
	}
	 
	private function getBarcode( $split_order_id ){ 
		
		$imgPath = WWW_ROOT .'img/orders/barcode/';   
 		
		if(!file_exists($imgPath.$split_order_id.'.png')){
		
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
			
			$colorFront = new BCGColor(0, 0, 0);
			$colorBack = new BCGColor(255, 255, 255); 
			
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->setLabel(false);
			$code128->parse($split_order_id);
			
			//Drawing Part
			$imgOrder128 = $split_order_id.".png";
			$imgOrder128path = $imgPath.$split_order_id.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);  
		}
		      
	}		
 	
	private function replaceChar($string = null){
  		return iconv('UTF-8','ASCII//TRANSLIT',$string);	 
	}	
	 
}
 
?>