<?php
error_reporting(0);
ini_set('memory_limit', '2048M');
class FunlinnworksController extends AppController
{
    
    var $name = "Funlinnworks";
    
    var $components = array('Session','Upload','Common','Paginator');
    
    var $helpers = array('Html','Form','Session','Common' , 'Soap' , 'Paginator');
    
	 public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->GlobalBarcode = $this->Components->load('Common'); /*Apply code for Global Barcode*/
	}
   
    public function getremaningqty()
	{
		//$barcode	=	'4103330017369';//$this->request->data['barcode'];
		$barcode	=	$this->request->data['barcode'];
		$this->loadModel('ScanOrder');
		$this->loadModel('OrderLocation');
		$this->loadModel('MergeUpdate');
		/*************************************Apply code for Global Barcode*********************************************/
	    $barcodeg		=	$this->GlobalBarcode->getGlobalBarcode($barcode);
	   /**************************************Apply code for Global Barcode***********************************************/
		 $reparams	=	array(
							'joins' => array(
									array(
										'table' => 'merge_updates',
										'alias' => 'MergeUpdate',
										'type' => 'INNER',
										'conditions' => array(
											'MergeUpdate.product_order_id_identify = ScanOrder.split_order_id',
											'MergeUpdate.status = 0',
											'MergeUpdate.pick_list_status = 1'
										)
									)
								),
							'conditions' => array( 'ScanOrder.update_quantity' => '0', 'ScanOrder.barcode' => $barcode ),
							'fields' => array( 'sum(ScanOrder.quantity) as qty','ScanOrder.sku','ScanOrder.barcode' ),
							'group' => 'ScanOrder.barcode'
						);
		
		$getremaningresult_l	=	$this->ScanOrder->find( 'all', $reparams );
		
		$reparams	=	array(
							'joins' => array(
									array(
										'table' => 'merge_updates',
										'alias' => 'MergeUpdate',
										'type' => 'INNER',
										'conditions' => array(
											'MergeUpdate.product_order_id_identify = ScanOrder.split_order_id',
											'MergeUpdate.status = 0',
											'MergeUpdate.pick_list_status = 1'
										)
									)
								),
							'conditions' => array( 'ScanOrder.update_quantity' => '0', 'ScanOrder.barcode' => $barcodeg ),
							'fields' => array( 'sum(ScanOrder.quantity) as qty','ScanOrder.sku','ScanOrder.barcode' ),
							'group' => 'ScanOrder.barcode'
						);
		
		$getremaningresult_g	=	$this->ScanOrder->find( 'all', $reparams );
		$getremaningresult 	=	$getremaningresult_l[0]['0']['qty'] + $getremaningresult_g[0]['0']['qty'];
		
	    /*$params	=	array(
							'joins' => array(
									array(
										'table' => 'merge_updates',
										'alias' => 'MergeUpdate',
										'type' => 'INNER',
										'conditions' => array(
											'MergeUpdate.order_id = OrderLocation.order_id',
											'OR' => array('MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1')
											
										)
									)
								),
							'conditions' => array('OrderLocation.pick_list' => '1','OrderLocation.barcode' => $barcode ),
							'fields' => array( 'SUM(OrderLocation.quantity) as pickqty' )
						);
		
		$getpickedresult	=	$this->OrderLocation->find('all', $params);*/
		
		$canparams	=	array(
							'joins' => array(
									array(
										'table' => 'merge_updates',
										'alias' => 'MergeUpdate',
										'type' => 'INNER',
										'conditions' => array(
											'MergeUpdate.order_id = OrderLocation.order_id',
											'MergeUpdate.status = 2',
											'MergeUpdate.reverce_picklist_status = 1'
										)
									)
								),
							'conditions' => array('OrderLocation.status' => 'canceled', 'OrderLocation.pick_list' => '1','OrderLocation.barcode' => $barcode ),
							'fields' => array( 'SUM(OrderLocation.quantity) as cancelqty' )
						);
		
		$getcanresult_1	=	$this->OrderLocation->find('all', $canparams);
		
		$canparams	=	array(
							'joins' => array(
									array(
										'table' => 'merge_updates',
										'alias' => 'MergeUpdate',
										'type' => 'INNER',
										'conditions' => array(
											'MergeUpdate.order_id = OrderLocation.order_id',
											'MergeUpdate.status = 2',
											'MergeUpdate.reverce_picklist_status = 1'
										)
									)
								),
							'conditions' => array('OrderLocation.status' => 'canceled', 'OrderLocation.pick_list' => '1','OrderLocation.barcode' => $barcodeg ),
							'fields' => array( 'SUM(OrderLocation.quantity) as cancelqty' )
						);
		
		$getcanresult_g	=	$this->OrderLocation->find('all', $canparams);
		$getcanresult = $getcanresult_l[0][0]['cancelqty'] + $getcanresult_g[0][0]['cancelqty'];
		$lockparams	=	array(
							'joins' => array(
									array(
										'table' => 'merge_updates',
										'alias' => 'MergeUpdate',
										'type' => 'INNER',
										'conditions' => array(
											'MergeUpdate.order_id = OrderLocation.order_id',
											'MergeUpdate.status = 3',
										)
									)
								),
							'conditions' => array( 'OrderLocation.pick_list' => '1','OrderLocation.barcode' => $barcode ),
							'fields' => array( 'SUM(OrderLocation.quantity) as lockqty' )
						);
		
		$getlockresult	=	$this->OrderLocation->find('all', $lockparams);
		//pr($getlockresult);
		//echo "121212";
		
		//pr( $getremaningresult );
		//echo $getcanresult[0][0]['cancelqty'];
		//echo "<br>";
		//echo $getremaningresult[0]['ScanOrder']['sku'];
		//echo "<br>";
		//echo $getremaningresult[0]['0']['qty'];
		//echo "<br>";
		//echo $getpickedresult[0][0]['pickqty'];
		//pr($getresult);
		//pr($getpickedresult);
		//pr($getresult);
		$data['sku'] 		= 	$getremaningresult[0]['ScanOrder']['sku'];
		$data['barcode'] 	= 	$barcode;
		$data['cancel']	=	$getcanresult;
		//$data['cancel'] 	= 	( $getcanresult[0][0]['cancelqty'] != '') ? $getcanresult[0][0]['cancelqty'] : '----';
		//$data['remaning'] 	= 	( $getremaningresult[0]['0']['qty'] != '') ? $getremaningresult[0]['0']['qty'] : '----';
		$data['remaning']	=	$getremaningresult;
		//$data['pickresult']	=  	( $getpickedresult[0][0]['pickqty'] != '') ? $getpickedresult[0][0]['pickqty'] : '----';
		$data['lock']		=	( $getlockresult[0][0]['lockqty'] != '' ) ? $getlockresult[0][0]['lockqty'] : '----';
		echo (json_encode(array('status' => '1', 'data' => $data ) ) );
		exit;
	}    
	
	public function updateproductuser()
	{
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('Product');
			$this->loadModel('ProductDesc');
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel 	= new PHPExcel();
			$objReader		= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel	= $objReader->load('files/Updateuser/Updateuser.csv');
			$objWorksheet	= $objPHPExcel->setActiveSheetIndex(0);
			$lastRow 		= $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString		= $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber 		= PHPExcel_Cell::columnIndexFromString($colString);
			$j = 0; $k = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$sku							=	 	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$purchaseUser 					= 		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$listionUser 					= 		$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
				echo $sku.'>>>>>>>>'.$listionUser.'>>>>>>>>'.$purchaseUser;
				echo "<br>";
				$checkProduct 	= $this->Product->find( 'first' , array( 'conditions' => array( 'Product.product_sku' => $sku ) ) );
				echo count($checkProduct);
				echo "<br>";
				$barcode 		= $checkProduct['ProductDesc']['barcode'];
				if(count($checkProduct) == 4)
				{	
					$this->ProductDesc->updateAll(array('ProductDesc.user_id'=>$listionUser,'ProductDesc.purchase_user_id'=>$purchaseUser),array('Product.product_sku' => $sku));
					$j++;
				} else { 
					$k++;
				}
			}
			echo $j.'Updated'.$k.'Not found';
		}	
}

?>
