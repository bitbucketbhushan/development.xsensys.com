<?php
error_reporting(0);
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class ManifestsController extends AppController
{
    
    var $name = "Manifests";
    
    var $components = array('Session', 'Common', 'Upload','Auth');
    
    var $helpers = array('Html','Form','Common','Session');
    
   public function edocGenerateForJpost() 
   {
	   $this->layout = 'index';
	   
	   $this->loadModel( 'PostalProvider' );
		
		$getPostalNames = $this->PostalProvider->find('all' , array( 'conditions' => array( 'PostalProvider.status' => 1 ) ));				
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$manifestPath = WWW_ROOT .'img/cut_off/'.$folderName;
		
		App::uses('Folder', 'Utility');
	    App::uses('File', 'Utility');
		$dir = new Folder($manifestPath, true, 0755);
	    $files = $dir->find('.*\.csv');
	    $files = Set::sort($files, '{n}', 'DESC');
	    
	    //pr($files);
	    
	    $this->set('files', $files);
	    $this->set( 'folderName' , $folderName );		
		$this->set( 'getPostalNames' , $getPostalNames );

   }
   
   public function edocGenerateForBpost() 
   {
		$this->layout = 'index';
		$this->loadModel( 'PostalProvider' );
		
		$getPostalNames = $this->PostalProvider->find('all' , array( 'conditions' => array( 'PostalProvider.status' => 1 ) ));				
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$manifestPath = WWW_ROOT .'img/cut_off/'.$folderName;
		
		App::uses('Folder', 'Utility');
	    App::uses('File', 'Utility');
		$dir = new Folder($manifestPath, true, 0755);
	    $files = $dir->find('.*\.csv');
	    $files = Set::sort($files, '{n}', 'DESC');
	    
	    //pr($files);
	    
	    $this->set('files', $files);
	    $this->set( 'folderName' , $folderName );		
		$this->set( 'getPostalNames' , $getPostalNames );
   }
   
   
   public function edocGenerateForPostnl() 
   {
		$this->layout = 'index';
		$this->loadModel( 'PostalProvider' );
		$this->loadModel( 'Location' );
		date_default_timezone_set('Europe/Jersey');
		$getPostalNames = $this->PostalProvider->find('all' , array( 'conditions' => array( 'PostalProvider.status' => 1,'PostalProvider.provider_name' => 'PostNL') ));	
		//pr($getPostalNames);			
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$manifestPath = WWW_ROOT .'img/cut_off/'.$folderName;
		
		App::uses('Folder', 'Utility');
	    App::uses('File', 'Utility');
		$dir = new Folder($manifestPath, true, 0755);
	    $files = $dir->find('.*\.csv');
	    $files = Set::sort($files, '{n}', 'DESC');
	    $this->set('files', $files);
	    $this->set( 'folderName' , $folderName );		
		$this->set( 'getPostalNames' , $getPostalNames );
	
		foreach( $getPostalNames as $getPostalName )
		{
			if( $getPostalName['PostalProvider']['location_id'] != '' )
			{
				$locationIDs		=	explode(',', $getPostalName['PostalProvider']['location_id']);
				foreach( $locationIDs as $locationID )
				{
					$this->Location->unbindModel( array( 'hasMany' => array( 'PostaServiceDesc' ) ) );
					$locationParam 	=	array( 'conditions'=>array('Location.id' => $locationID) );
					$locationDetail[]	=	$this->Location->find( 'first', $locationParam );
				}
			}
		}
		//pr($locationDetail);
		$this->set( 'locationDetail' , $locationDetail );
		
   }
    
    
   public function edocGenerateForSpainpost() 
   {
		$this->layout = 'index';
		$this->loadModel( 'PostalProvider' );
		date_default_timezone_set('Europe/Jersey');
		$getPostalNames = $this->PostalProvider->find('all' , array( 'conditions' => array( 'PostalProvider.status' => 1 ) ));				
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$manifestPath = WWW_ROOT .'img/cut_off/'.$folderName;
		
		App::uses('Folder', 'Utility');
	    	App::uses('File', 'Utility');
		$dir = new Folder($manifestPath, true, 0755);
	    	$files = $dir->find('.*\.csv');
	    	$files = Set::sort($files, '{n}', 'DESC');
	    	$this->set('files', $files);
	    	$this->set( 'folderName' , $folderName );		
		$this->set( 'getPostalNames' , $getPostalNames );
		
		$servicePost = "Spain Post";
		$this->set('servicePost',$servicePost);


   }
    
	public function generateExportLabel()
	{
		$this->layout  = '';
		$this->autoRender = false;
		/* call the dompdf for print */
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper(array(0, 0, 280, 500), 'landscape');//landscape//portrait 
	
		/* css and html start */
		$imgPath = WWW_ROOT .'css/';
		$html = '<body>
				 <div id="label">
				 <div class="container" style="font-size:40px; margin-top:40px;">
				 B.P.I <br>
				 E.M.C. - Building 829C<br>
				 1931 Zaventem - Brucargo<br>
				 Belgium
				 </div>
				 </div>
				 </body>';

		$html.= '<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
		$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
		$dompdf->render();
		//$dompdf->stream("test.pdf");
		
		$imgPath 	= 	WWW_ROOT .'img/exportPDF/'; 
		$path 		= 	Router::url('/', true).'img/exportPDF/';
		$name		=   'ExportLabel.pdf';
		
		file_put_contents($imgPath.$name, $dompdf->output());
		$serverPath  	= 	$path.$name ;
		$printerId	=	$this->getZebra();
		$sendData = array(
			'printerId' => $printerId,
			'title' => 'Now Print',
			'contentType' => 'pdf_uri',
			'content' => $serverPath,
			'source' => 'Direct'
		);
		
		App::import( 'Controller' , 'Coreprinters' );
		$Coreprinter = new CoreprintersController();
		$d = $Coreprinter->toPrint( $sendData );
		$this->redirect(array('controller'=>'Cronjobs','action' => 'manifestCreate'));
		pr($d); exit;
		
	}
	
	public function generateExportLabelPostnl()
	{
		
		$this->layout  = '';
		$this->autoRender = false;
		/* call the dompdf for print */
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper(array(0, 0, 280, 500), 'landscape');//landscape//portrait 
	
		/* css and html start */
		$imgPath = WWW_ROOT .'css/';
		$html2 = '<body>
				 <div id="label">
				 <div class="container" style="font-size:40px; margin-top:40px;">
				 Businessbalie IMU <br>
				 T.a.v. Jan Zeldenrijk / Leo Groepenhoff<br>
				 Australiehavenweg 100<br>
				 1046 BP Amsterdam
				 </div>
				 </div>
				 </body>';

		$html = '<body>
					 <div id="label">
						 <div class="container" style="font-size:50px; margin-top:40px;">
						 	 Spring Belgium<br>
							 Blarenberglaan 6 <br>
							 2800 MECHELEN<br>
							 BELGIUM<br>
						 </div>
					 </div>
				 </body>';

		$html.= '<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
		$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
		$dompdf->render();
		//$dompdf->stream("test.pdf");
		
		$imgPath 	= 	WWW_ROOT .'img/exportPDF/'; 
		$path 		= 	Router::url('/', true).'img/exportPDF/';
		$name		=   'ExportLabelPostnl.pdf';
		
		file_put_contents($imgPath.$name, $dompdf->output());
		$serverPath  	= 	$path.$name ;
		$printerId	=	$this->getZebra();
		$sendData = array(
			'printerId' => $printerId,
			'title' => 'Now Print',
			'contentType' => 'pdf_uri',
			'content' => $serverPath,
			'source' => 'Direct'
		);
		
		App::import( 'Controller' , 'Coreprinters' );
		$Coreprinter = new CoreprintersController();
		$d = $Coreprinter->toPrint( $sendData );
		$this->redirect(array('controller'=>'Cronjobs','action' => 'manifestCreate'));
		pr($d); exit;
		
	}
	
	public function getZebra()
		{
			$this->layout = '';
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );						
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name']; //getenv('COMPUTERNAME');
			$paramsZDesigner = array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name' => 'ZDesigner'
				)
			);
			//ZDesigner
			$pcDetailZDesigner = json_decode(json_encode($this->PcStore->find('all', $paramsZDesigner)),0);						
			$zDesignerPrinter = $pcDetailZDesigner[0]->PcStore->printer_id;
		
		return $zDesignerPrinter;	
		}
	
	
	 public function createCutOffList()
	{
		$this->layout = '';
		$this->autoRender = false;          
		date_default_timezone_set('Europe/Jersey');
		
		// Get All manifest related services
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'MergeUpdate' );
		
		/* start european country iso code*/
		$isoCode = Configure::read('customIsoCodes');
		  /* end european country iso code*/
		
		//Global Variable
		$glbalSortingCounter = 0;
		
		$serviceProvider = $this->request->data['serviceProvider'];
		
		// Get Data
		$manifest = json_decode(json_encode($this->ServiceCounter->find( 'all' , 
			array( 
				'conditions' => array( 
						//'ServiceCounter.manifest' => 1 , 
						'ServiceCounter.service_provider' => $serviceProvider , 
						'ServiceCounter.order_ids !=' => '' , 
						'ServiceCounter.counter >' => 0 , 
						'ServiceCounter.original_counter >' => 0, 
						//'ServiceCounter.locking_stage' => 0 
					)                                                                                                           
				)                                                                              
			)),0); 
		
		if( count($manifest) > 0 )
		{
			
			// Get Data
			$manifestAll = json_decode(json_encode($this->ServiceCounter->find( 'all' , 
				array( 
					'conditions' => array( 
							//'ServiceCounter.manifest' => 1 , 							
							'ServiceCounter.order_ids !=' => '' , 
							'ServiceCounter.counter >' => 0 , 
							'ServiceCounter.original_counter >' => 0, 
							//'ServiceCounter.locking_stage' => 0 
						)                                                                                                           
					)                                                                              
				)),0); 
			
			if( count($manifestAll) > 0 )
			{
				
				$this->loadModel( 'ManifestBackup' );
				$manifestRecord['ManifestBackup']['date'] = date( 'Y-m-d' );
				$manifestRecord['ManifestBackup']['service_provider'] = 'All';
				$manifestRecord['ManifestBackup']['manifest_bk'] = serialize($manifestAll);
				
				//userGet
				$userDT = $this->Session->read('Auth.User'); 
				$userId = $userDT['id'];
				 
				$manifestRecord['ManifestBackup']['user_id'] = $userId;			
				$this->ManifestBackup->saveAll( $manifestRecord );
					
			}
						
			//We got number of sorted rows which had been done through operator and creating manifest 1 for same provider
			$inc = 1;$cnt = 2;$e = 0;foreach( $manifest as $manifestIndex => $manifestValue )
			{
				$serviceData = json_decode(json_encode($this->ServiceCounter->find( 'first', array( 'conditions' => array( 'ServiceCounter.id' => $manifestValue->ServiceCounter->id ) ) )),0);							
				$originalCounter = $serviceData->ServiceCounter->original_counter - $serviceData->ServiceCounter->counter;
				
				//Update Now at specific id
				$this->request->data['ServiceCounter']['ServiceCounter']['id'] = $manifestValue->ServiceCounter->id;
				$this->request->data['ServiceCounter']['ServiceCounter']['original_counter'] = $originalCounter;
				$this->request->data['ServiceCounter']['ServiceCounter']['counter'] = 0;
				$this->request->data['ServiceCounter']['ServiceCounter']['order_ids'] = '';
				//$this->request->data['ServiceCounter']['ServiceCounter']['locking_stage'] = 1;
				$this->ServiceCounter->saveAll( $this->request->data['ServiceCounter'] );
			$e++;	 
			}
			
			/*//Set First Row  for Amazon FBa Sheet
			$objPHPExcel->setActiveSheetIndex(0);                                                                              
			$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->getAlignment()->applyFromArray(
			array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));                   
			$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet(0)->getStyle("A1:D1")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet(0)
			->getStyle('A1:D1')
			->applyFromArray(
                            array(
                                'fill' => array(
                                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                    'color' => array('rgb' => 'EBE5DB')
                                )
                            )
			);
			
			date_default_timezone_set('Europe/Jersey');
			//echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]';
			//CutOff time store
			$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));
			
			$folderName = 'Service Manifest -'. date("d.m.Y");
			$service = str_replace(' ', '', str_replace(':','_',$serviceProvider.'-'. date("d.m.Y") .'_'. $time_in_12_hour_format));
							  
			// create new folder with date if exists will remain same or else create new one                                                                                                                                                                                                
			$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
			
			$uploadUrl = WWW_ROOT .'img/cut_off/'. $folderName . '/' .$service.'.csv';                                          
			$uploadUrI = Router::url('/', true) . 'img/cut_off/'. $folderName . '/' .$service.'.csv';                                          
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
			$objWriter->save($uploadUrl);
			
			echo $uploadUrI; exit;*/
			
			echo "done"; exit;
			
		}
		else
		{
			echo "blank"; exit;
		}
	}
	
	
    
    /*
	* 
	 * Params, CutOff creation for specific services which have own manifest value(1)
	* 
	 */ 
   
   public function getrand()
   {
	   (float)$min=20.10;
	   (float)$max=21.99;

   return ($min + ($max - $min) * (mt_rand() / mt_getrandmax()));	    	
   }	 
   
   	 public function createCutOffListForJerseyPost()
	{
		$this->layout = '';
		$this->autoRender = false;          
		
		// Get All manifest related services
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'ManifestRecord' );
		
		/* start european country iso code*/
		$isoCode = Configure::read('customIsoCodes');
		  /* end european country iso code*/
		
		//Global Variable
		$glbalSortingCounter = 0;
		
		//Service Provider
		$serviceProvider = $this->request->data['serviceProvider'];
		
		// Get Data
		$manifest = json_decode(json_encode($this->ServiceCounter->find( 'all' , 
			array( 
				'conditions' => array( 
						//'ServiceCounter.manifest' => 1 , 
						'ServiceCounter.service_provider' => $serviceProvider , 
						'ServiceCounter.order_ids !=' => '' , 
						'ServiceCounter.counter >' => 0 , 
						'ServiceCounter.original_counter >' => 0, 
						//'ServiceCounter.locking_stage' => 0 
					)                                                                                                           
				)                                                                              
			)),0); 
		
		if( count($manifest) > 0 )
		{
			
			//We got number of sorted rows which had been done through operator and creating manifest 1 for same provider
			$totalOrders = 0;
			$totalPecises = 0;
			$totalMassWeightNow = 0;

			$inc = 1;$cnt = 6;$e = 0;foreach( $manifest as $manifestIndex => $manifestValue )
			{
				
				if( $e == 0 )
				{
					// Clean Stream (Input)
					//ob_clean();                                                         
					App::import('Vendor', 'PHPExcel/IOFactory');
					App::import('Vendor', 'PHPExcel');                          
					
					//Set and create Active Sheet for single workbook with singlle sheet
					$objPHPExcel = new PHPExcel();       
					$objPHPExcel->createSheet();
					
					//Column Create                              
					$objPHPExcel->setActiveSheetIndex(0);
					
					$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Item');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Shipper');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Receiver');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Address');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Address');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Postcode');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('G5', 'COO');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Pces');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Weight(g)');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Value');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Currency');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Commodity');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Invoice No.');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Bag-No');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('O5', 'Destination');                                                                  
				}
				
				$orderIds = explode( ',' , $manifestValue->ServiceCounter->order_ids);
				
				//pr($orderIds);
				
				$this->loadModel( 'MergeUpdate' );
				$this->loadModel( 'Product' );
				$this->loadModel( 'ProductDesc' );
				$this->loadModel( 'OpenOrder' );
				App::import( 'Controller' , 'Cronjobs');
				$objCronjobs = new CronjobsController();
				
				$combineSku = '';				
				$k = 0;while( $k <= count($orderIds)-1 )
				{
					
					$orderIdSpecified = $orderIds[$k];
					$objCronjobs->updateManifestDate($orderIdSpecified);
					$params = array(
						'conditions' => array(
							'MergeUpdate.id' => $orderIdSpecified
						),
						'fields' => array(
							'MergeUpdate.id',
							'MergeUpdate.order_id',
							'MergeUpdate.product_order_id_identify',
							'MergeUpdate.quantity',
							'MergeUpdate.sku',
							'MergeUpdate.price',
							'MergeUpdate.packet_weight',
                            'MergeUpdate.envelope_weight'
						)
					);
					
					$mergeOrder = json_decode(json_encode($this->MergeUpdate->find(
						'all', $params
					)),0);
					
					//pr($mergeOrder);
										
					$getSku = explode( ',' , $mergeOrder[0]->MergeUpdate->sku);
					
                    $packageWeight = $mergeOrder[0]->MergeUpdate->envelope_weight;                    
					$totalPriceValue = 0;
					$massWeight = 0;
					$totalUnits = 0;
					$combineTitle = '';
					$combineCategory = '';
                    $calculateWeight = 0;
                    
                    $getSpecifier = explode('-', $mergeOrder[0]->MergeUpdate->product_order_id_identify);
                   
                    $setSpaces = '';
                    $identifier = $getSpecifier[1];
                    $em = 0;while( $em < $identifier )
                    {
						$setSpaces .= $setSpaces.' ';						
					$em++;	
					}
					
					$j = 0;while( $j <= count($getSku)-1 )
					{
						$newSku = explode( 'XS-' , $getSku[$j] );
						
						//Get Title of product
						$setNewSku = 'S-'.$newSku[1];
						
						$this->loadModel( 'Product' );
						$this->loadModel( 'Category' );					
					
						$this->Product->bindModel(
							array(
							 'hasOne' => array(
							  'Category' => array(
							   'foreignKey' => false,
							   'conditions' => array('Category.id = Product.category_id'),
							   'fields' => array('Category.id,Category.category_name')
							  )
							 )
							)
						   );
					
						$productSku = $this->Product->find(
							'first' ,
							array(
								'conditions' => array(
									'Product.product_sku' => $setNewSku
								)
							)
						);
						
						if( $combineTitle == '' )
						{
							$combineTitle = substr($productSku['Product']['product_name'],0,25);	
							$totalUnits = $totalUnits + $newSku[0];     
                            $calculateWeight = ($newSku[0] * $productSku['ProductDesc']['weight']);
							$massWeight = $massWeight + $calculateWeight;		
							$combineCategory = $productSku['Category']['category_name'];				
							
						}
						else
						{
							$combineTitle .= ',' . substr($productSku['Product']['product_name'],0,25);	
							$totalUnits = $totalUnits + $newSku[0];
                            $calculateWeight = ($newSku[0] * $productSku['ProductDesc']['weight']);
							$massWeight = $massWeight + $calculateWeight;                                                        
							$combineCategory .= ',' . $productSku['Category']['category_name'];				
							
						}
						 
						if( $combineSku == '' )
							$combineSku = $setNewSku;	
						else
							$combineSku .= ',' . $setNewSku;	
						
					$j++;	
					}
					
					$totalPecises = $totalPecises + $totalUnits;
					
					//package weight + order item weight					
					$massWeight = $packageWeight + $massWeight; 
					$totalMassWeightNow = $totalMassWeightNow + $massWeight;						
					//LineNo
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$cnt, $inc );
					
					//Option
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$cnt, 'ESL' );
					
					
					/* Condignee */
					$paramsConsignee = array(
						'conditions' => array(
							'OpenOrder.num_order_id' => $mergeOrder[0]->MergeUpdate->order_id
						),
						'fields' => array(
							'OpenOrder.num_order_id',
							'OpenOrder.id',
							'OpenOrder.general_info',
							'OpenOrder.shipping_info',
							'OpenOrder.customer_info',
							'OpenOrder.totals_info'							
						)
					);
					
					$getConsigneeDetailFromLinnworksOrder = json_decode(json_encode($this->OpenOrder->find( 'first', $paramsConsignee )),0);					
					//pr(unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->general_info));
					//pr(unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->shipping_info));
					$congineeInfo = unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->customer_info);					
					
					$cityInfo = $congineeInfo->Address->Town .' , '. $congineeInfo->Address->Region;
					//pr($congineeInfo);
					//pr(unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->totals_info));
					
					$totalInfo = unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->totals_info);
					
					//$congineeInfo->Address->FullName;
					
					//ConsigneeName
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$cnt, str_replace(' ',$setSpaces,$congineeInfo->Address->FullName) );
					
					//ConsigneeAddress1
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$cnt, str_replace(' ',$setSpaces,$congineeInfo->Address->Address1) );
					
					//ConsigneeAddress2
					//$objPHPExcel->getActiveSheet()->setCellValue('E'.$cnt, str_replace(' ',$setSpaces,$congineeInfo->Address->Address2) );
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$cnt, $cityInfo );
					
					//ConsigneeGSTNumber
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$cnt, $congineeInfo->Address->PostCode );
					
					//ConsigneePostCode
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$cnt, 'Jersey' );
					
					$setSpaces = '';
					
					//ConsigneeCountry
					$country = '';					
					foreach( $isoCode as $index => $value )
					{
						if( $index == $congineeInfo->Address->Country )
						{
							$country = $value;
						}
					}					
					//NoOfUnits
					//$totalUnits
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$cnt, '1' );
					
					//MassWeight
					$massWeight = ($massWeight * 1000);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$cnt, $massWeight );
					
					$currencyMatter = $totalInfo->Currency;
					
					$globalCurrencyConversion = 1;
					
					if( $currencyMatter == "EUR" )
					{
						$globalCurrencyConversion = 1;
					}
					else
					{
						$globalCurrencyConversion = 1.38;
					}
					
					//Value
					$totalValue = 0;
					$setPrice = $mergeOrder[0]->MergeUpdate->price;
					/*if( ( $setPrice * $globalCurrencyConversion) > 21.99 )
					{
						$totalValue = sprintf( '%.2f' , $this->getrand() );
					}
					else
					{
						$totalValue = sprintf( '%.2f' , $setPrice * $globalCurrencyConversion );
					}*/
					
					if( ( $setPrice * $globalCurrencyConversion) > 21.99 )
					{
						$totalValue = number_format($this->getrand(), 2, '.', ''); //sprintf( '%.2f' , $this->getrand() );
					}
					else
					{
						$totalValue = number_format(( $setPrice * $globalCurrencyConversion ), 2, '.', ''); //sprintf( '%.2f' , $setPrice * $globalCurrencyConversion );
					}
					
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$cnt, $totalValue );
					
					//Currency
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$cnt, 'EUR' );
					
					//ForwardingAgent
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$cnt, $combineTitle );
					
					//ForwardingAgentAddress1
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$cnt, $mergeOrder[0]->MergeUpdate->product_order_id_identify );
					
					//ForwardingAgentAddress2
					$countPostcode = strlen($congineeInfo->Address->PostCode);
					$countChar = $countPostcode - 3;
					if( $countPostcode == 5 )
					{
						$getBagNo = substr( $congineeInfo->Address->PostCode , 0 , 2 );
					}
					else
					{
						$getBagNo = substr( $congineeInfo->Address->PostCode , 0 , 1 );
					}
					
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$cnt, $getBagNo );
					
					//ForwardingAgentCountry
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$cnt, $congineeInfo->Address->Country  );
					
					$combineSku = '';
					$totalUnits = 0;
					$massWeight = 0;
					$combineCategory = '';
					
					$totalOrders++;

				$inc++;	
				$cnt++;	
				$k++;	
				}
				
				$serviceData = json_decode(json_encode($this->ServiceCounter->find( 'first', array( 'conditions' => array( 'ServiceCounter.id' => $manifestValue->ServiceCounter->id ) ) )),0);							
				$originalCounter = $serviceData->ServiceCounter->original_counter - $serviceData->ServiceCounter->counter;
				
				//Store into manifest record
				$manifestRecord = array(
			
					'destination' => $manifestValue->ServiceCounter->destination,
					'service_name' => $manifestValue->ServiceCounter->service_name,
					'service_provider' => $manifestValue->ServiceCounter->service_provider,
					'service_code' => $manifestValue->ServiceCounter->service_code,
					'order_ids' => $manifestValue->ServiceCounter->order_ids,
					'date' => date('Y-m-d')
					
				); 
			
				$this->ManifestRecord->saveAll( $manifestRecord );
				unset($manifestRecord);
				$manifestRecord = '';
				
				//Now update sheet marker corresponding to order id
				$getOrderId = explode('-',$manifestValue->ServiceCounter->order_ids);
				foreach( $getOrderId as $getOrderIdValue )
				{
					
					$innerOrderSorted = explode( ',' , $getOrderIdValue );					
					foreach( $innerOrderSorted as $innerValue )
					{
						
						$this->request->data['MergeUpdate']['MergeUpdate']['id'] = $innerValue;
						$this->request->data['MergeUpdate']['MergeUpdate']['manifest_sheet_marker'] = 1;
						$this->request->data['MergeUpdate']['MergeUpdate']['date'] = date('Y-m-d');
						$this->MergeUpdate->saveAll( $this->request->data['MergeUpdate'] );
						
					}
					
				}				
								
				//Update Now at specific id
				$this->request->data['ServiceCounter']['ServiceCounter']['id'] = $manifestValue->ServiceCounter->id;
				$this->request->data['ServiceCounter']['ServiceCounter']['original_counter'] = $originalCounter;
				$this->request->data['ServiceCounter']['ServiceCounter']['counter'] = 0;
				$this->request->data['ServiceCounter']['ServiceCounter']['order_ids'] = '';
				
				//$this->request->data['ServiceCounter']['ServiceCounter']['locking_stage'] = 1;
				$this->ServiceCounter->saveAll( $this->request->data['ServiceCounter'] );
				
				
				
			$e++;	
			}
			
			//Top rows
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'LOW VALUE MANIFEST DESTINATION SPAIN');
			$objPHPExcel->getActiveSheet()->setCellValue('A2', 'ORIGIN: Jersey');
			$objPHPExcel->getActiveSheet()->setCellValue('A3', 'TOTAL PIECES: '.$totalOrders);
			
			$objPHPExcel->getActiveSheet()->setCellValue('B2', 'Date: '.date('Y-m-d'));
			$objPHPExcel->getActiveSheet()->setCellValue('B3', 'TOTAL WEIGHT:'.$totalMassWeightNow.'kg');
						
			$objPHPExcel->getActiveSheet()->setCellValue('C2', 'DESTINATION: Spain');
			$objPHPExcel->getActiveSheet()->setCellValue('C3', 'TOTAL ITEMS:'.$totalOrders);
			
			//Set First Row  for Amazon FBa Sheet
			$objPHPExcel->setActiveSheetIndex(0);                                                                              
			$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->getAlignment()->applyFromArray(
			array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));                   
			$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet(0)->getStyle("A1:D1")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet(0)
			->getStyle('A1:D1')
			->applyFromArray(
                            array(
                                'fill' => array(
                                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                    'color' => array('rgb' => 'EBE5DB')
                                )
                            )
			);
			
			date_default_timezone_set('Europe/Jersey');
			//echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]';
			//CutOff time store
			$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));
			
			$folderName = 'Service Manifest -'. date("d.m.Y");
			$service = str_replace(' ', '', str_replace(':','_',$serviceProvider.'-'. date("d.m.Y") .'_'. $time_in_12_hour_format));
							  
			// create new folder with date if exists will remain same or else create new one                                                                                                                                                                                                
			$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
			
			$uploadUrl = WWW_ROOT .'img/cut_off/'. $folderName . '/' .$service.'.csv';                                          
			$uploadUrI = Router::url('/', true) . 'img/cut_off/'. $folderName . '/' .$service.'.csv';                                          
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
			$objWriter->save($uploadUrl);
			
			//Update Service Counter
			//$this->call_service_counter();			
			echo $uploadUrI; exit;
		}
		else
		{
			echo "blank"; exit;
		}
	}
   
	public function createCutOffListForSpain()
	{
		$this->layout = '';
		$this->autoRender = false;          
		
		// Get All manifest related services
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'ManifestRecord' );
		
		/* start european country iso code*/
		$isoCode = Configure::read('customIsoCodes');
		  /* end european country iso code*/
		
		//Global Variable
		$glbalSortingCounter = 0;
		
		//Service Provider
		$serviceProvider = $this->request->data['serviceProvider'];
		date_default_timezone_set('Europe/Jersey');
		$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));
			
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$service = str_replace(' ', '', str_replace(':','_',$serviceProvider.'-'. date("d.m.Y") .'_'. $time_in_12_hour_format));
		
		
		// Get Data
		$manifest = json_decode(json_encode($this->ServiceCounter->find( 'all' , 
			array( 
				'conditions' => array( 
						//'ServiceCounter.manifest' => 1 , 
						'ServiceCounter.service_provider' => $serviceProvider , 
						'ServiceCounter.order_ids !=' => '' , 
						'ServiceCounter.counter >' => 0 , 
						'ServiceCounter.original_counter >' => 0, 
						//'ServiceCounter.locking_stage' => 0 
					)                                                                                                           
				)                                                                              
			)),0); 
		
		if( count($manifest) > 0 )
		{
			
			//We got number of sorted rows which had been done through operator and creating manifest 1 for same provider
			$totalOrders = 0;
			$totalPecises = 0;
			$totalMassWeightNow = 0;

			$inc = 1;$cnt = 6;$e = 0;foreach( $manifest as $manifestIndex => $manifestValue )
			{
				
				if( $e == 0 )
				{
					// Clean Stream (Input)
					//ob_clean();                                                         
					App::import('Vendor', 'PHPExcel/IOFactory');
					App::import('Vendor', 'PHPExcel');                          
					
					//Set and create Active Sheet for single workbook with singlle sheet
					$objPHPExcel = new PHPExcel();       
					$objPHPExcel->createSheet();
					
					//Column Create                              
					$objPHPExcel->setActiveSheetIndex(0);
					
					$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Item');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Shipper');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Receiver');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Address');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Address');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Postcode');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('G5', 'COO');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Pces');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Weight(g)');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Value');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Currency');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Commodity');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Invoice No.');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Bag-No');                                                                  
					$objPHPExcel->getActiveSheet()->setCellValue('O5', 'Destination');                                                                  
				}
				
				$orderIds = explode( ',' , $manifestValue->ServiceCounter->order_ids);
				
				//pr($orderIds);
				
				$this->loadModel( 'MergeUpdate' );
				$this->loadModel( 'Product' );
				$this->loadModel( 'ProductDesc' );
				$this->loadModel( 'OpenOrder' );
				App::import( 'Controller' , 'Cronjobs');
				$objCronjobs = new CronjobsController();
				
				$combineSku = '';				
				$k = 0;while( $k <= count($orderIds)-1 )
				{
					
					$orderIdSpecified = $orderIds[$k];
					$objCronjobs->updateManifestDate($orderIdSpecified);
					$params = array(
						'conditions' => array(
							'MergeUpdate.id' => $orderIdSpecified
						),
						'fields' => array(
							'MergeUpdate.id',
							'MergeUpdate.order_id',
							'MergeUpdate.product_order_id_identify',
							'MergeUpdate.quantity',
							'MergeUpdate.sku',
							'MergeUpdate.price',
							'MergeUpdate.packet_weight',
                            'MergeUpdate.envelope_weight'
						)
					);
					
					$mergeOrder = json_decode(json_encode($this->MergeUpdate->find(
						'all', $params
					)),0);
					
					App::import( 'Controller' , 'Cronjobs');
					$objCronjobs = new CronjobsController();
					$objCronjobs->setManifestRecord( $mergeOrder[0]->MergeUpdate->id, $service );
					
					//pr($mergeOrder);
										
					$getSku = explode( ',' , $mergeOrder[0]->MergeUpdate->sku);
					
                    $packageWeight = $mergeOrder[0]->MergeUpdate->envelope_weight;                    
					$totalPriceValue = 0;
					$massWeight = 0;
					$totalUnits = 0;
					$combineTitle = '';
					$combineCategory = '';
                    $calculateWeight = 0;
                    
                    $getSpecifier = explode('-', $mergeOrder[0]->MergeUpdate->product_order_id_identify);
                   
                    $setSpaces = '';
                    $identifier = $getSpecifier[1];
                    $em = 0;while( $em < $identifier )
                    {
						$setSpaces .= $setSpaces.' ';						
					$em++;	
					}
					
					$j = 0;while( $j <= count($getSku)-1 )
					{
						$newSku = explode( 'XS-' , $getSku[$j] );
						
						//Get Title of product
						$setNewSku = 'S-'.$newSku[1];
						
						$this->loadModel( 'Product' );
						$this->loadModel( 'Category' );					
					
						$this->Product->bindModel(
							array(
							 'hasOne' => array(
							  'Category' => array(
							   'foreignKey' => false,
							   'conditions' => array('Category.id = Product.category_id'),
							   'fields' => array('Category.id,Category.category_name')
							  )
							 )
							)
						   );
					
						$productSku = $this->Product->find(
							'first' ,
							array(
								'conditions' => array(
									'Product.product_sku' => $setNewSku
								)
							)
						);
						
						if( $combineTitle == '' )
						{
							$combineTitle = substr($productSku['Product']['product_name'],0,25);	
							$totalUnits = $totalUnits + $newSku[0];     
                            $calculateWeight = ($newSku[0] * $productSku['ProductDesc']['weight']);
							$massWeight = $massWeight + $calculateWeight;		
							$combineCategory = $productSku['Category']['category_name'];				
							
						}
						else
						{
							$combineTitle .= ',' . substr($productSku['Product']['product_name'],0,25);	
							$totalUnits = $totalUnits + $newSku[0];
                            $calculateWeight = ($newSku[0] * $productSku['ProductDesc']['weight']);
							$massWeight = $massWeight + $calculateWeight;                                                        
							$combineCategory .= ',' . $productSku['Category']['category_name'];				
							
						}
						 
						if( $combineSku == '' )
							$combineSku = $setNewSku;	
						else
							$combineSku .= ',' . $setNewSku;	
						
					$j++;	
					}
					
					$totalPecises = $totalPecises + $totalUnits;
					
					//package weight + order item weight					
					$massWeight = $packageWeight + $massWeight; 
					$totalMassWeightNow = $totalMassWeightNow + $massWeight;						
					//LineNo
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$cnt, $inc );
					
					//Option
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$cnt, 'ESL' );
					
					
					/* Condignee */
					$paramsConsignee = array(
						'conditions' => array(
							'OpenOrder.num_order_id' => $mergeOrder[0]->MergeUpdate->order_id
						),
						'fields' => array(
							'OpenOrder.num_order_id',
							'OpenOrder.id',
							'OpenOrder.general_info',
							'OpenOrder.shipping_info',
							'OpenOrder.customer_info',
							'OpenOrder.totals_info'							
						)
					);
					
					$getConsigneeDetailFromLinnworksOrder = json_decode(json_encode($this->OpenOrder->find( 'first', $paramsConsignee )),0);					
					//pr(unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->general_info));
					//pr(unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->shipping_info));
					$congineeInfo = unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->customer_info);					
					
					$cityInfo = $congineeInfo->Address->Town .' , '. $congineeInfo->Address->Region;
					//pr($congineeInfo);
					//pr(unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->totals_info));
					
					$totalInfo = unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->totals_info);
					
					//$congineeInfo->Address->FullName;
					
					//ConsigneeName
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$cnt, str_replace(' ',$setSpaces,$congineeInfo->Address->FullName) );
					
					//ConsigneeAddress1
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$cnt, str_replace(' ',$setSpaces,$congineeInfo->Address->Address1) );
					
					//ConsigneeAddress2
					//$objPHPExcel->getActiveSheet()->setCellValue('E'.$cnt, str_replace(' ',$setSpaces,$congineeInfo->Address->Address2) );
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$cnt, $cityInfo );
					
					//ConsigneeGSTNumber
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$cnt, $congineeInfo->Address->PostCode );
					
					//ConsigneePostCode
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$cnt, 'Jersey' );
					
					$setSpaces = '';
					
					//ConsigneeCountry
					$country = '';					
					foreach( $isoCode as $index => $value )
					{
						if( $index == $congineeInfo->Address->Country )
						{
							$country = $value;
						}
					}					
					//NoOfUnits
					//$totalUnits
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$cnt, '1' );
					
					//MassWeight
					$massWeight = ($massWeight * 1000);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$cnt, $massWeight );
					
					$currencyMatter = $totalInfo->Currency;
					
					$globalCurrencyConversion = 1;
					
					if( $currencyMatter == "EUR" )
					{
						$globalCurrencyConversion = 1;
					}
					else
					{
						$globalCurrencyConversion = 1.38;
					}
					
					//Value
					$totalValue = 0;
					$setPrice = $mergeOrder[0]->MergeUpdate->price;
					/*if( ( $setPrice * $globalCurrencyConversion) > 21.99 )
					{
						$totalValue = sprintf( '%.2f' , $this->getrand() );
					}
					else
					{
						$totalValue = sprintf( '%.2f' , $setPrice * $globalCurrencyConversion );
					}*/
					
					if( ( $setPrice * $globalCurrencyConversion) > 21.99 )
					{
						$totalValue = number_format($this->getrand(), 2, '.', ''); //sprintf( '%.2f' , $this->getrand() );
					}
					else
					{
						$totalValue = number_format(( $setPrice * $globalCurrencyConversion ), 2, '.', ''); //sprintf( '%.2f' , $setPrice * $globalCurrencyConversion );
					}
					
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$cnt, $totalValue );
					
					//Currency
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$cnt, 'EUR' );
					
					//ForwardingAgent
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$cnt, $combineTitle );
					
					//ForwardingAgentAddress1
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$cnt, $mergeOrder[0]->MergeUpdate->product_order_id_identify );
					
					//ForwardingAgentAddress2
					$countPostcode = strlen($congineeInfo->Address->PostCode);
					$countChar = $countPostcode - 3;
					if( $countPostcode == 5 )
					{
						$getBagNo = substr( $congineeInfo->Address->PostCode , 0 , 2 );
					}
					else
					{
						$getBagNo = substr( $congineeInfo->Address->PostCode , 0 , 1 );
					}
					
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$cnt, $getBagNo );
					
					//ForwardingAgentCountry
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$cnt, "Spain" );
					
					$combineSku = '';
					$totalUnits = 0;
					$massWeight = 0;
					$combineCategory = '';
					
					$totalOrders++;

				$inc++;	
				$cnt++;	
				$k++;	
				}
				
				$serviceData = json_decode(json_encode($this->ServiceCounter->find( 'first', array( 'conditions' => array( 'ServiceCounter.id' => $manifestValue->ServiceCounter->id ) ) )),0);							
				$originalCounter = $serviceData->ServiceCounter->original_counter - $serviceData->ServiceCounter->counter;
				
				//Store into manifest record
				$manifestRecord = array(
			
					'destination' => $manifestValue->ServiceCounter->destination,
					'service_name' => $manifestValue->ServiceCounter->service_name,
					'service_provider' => $manifestValue->ServiceCounter->service_provider,
					'service_code' => $manifestValue->ServiceCounter->service_code,
					'order_ids' => $manifestValue->ServiceCounter->order_ids,
					'date' => date('Y-m-d')
					
				); 
			
				$this->ManifestRecord->saveAll( $manifestRecord );
				unset($manifestRecord);
				$manifestRecord = '';
				
				//Now update sheet marker corresponding to order id
				$getOrderId = explode('-',$manifestValue->ServiceCounter->order_ids);
				foreach( $getOrderId as $getOrderIdValue )
				{
					
					$innerOrderSorted = explode( ',' , $getOrderIdValue );					
					foreach( $innerOrderSorted as $innerValue )
					{
						
						$this->request->data['MergeUpdate']['MergeUpdate']['id'] = $innerValue;
						$this->request->data['MergeUpdate']['MergeUpdate']['manifest_sheet_marker'] = 1;
						$this->request->data['MergeUpdate']['MergeUpdate']['date'] = date('Y-m-d');
						$this->MergeUpdate->saveAll( $this->request->data['MergeUpdate'] );
						
					}
					
				}				
								
				//Update Now at specific id
				$this->request->data['ServiceCounter']['ServiceCounter']['id'] = $manifestValue->ServiceCounter->id;
				$this->request->data['ServiceCounter']['ServiceCounter']['original_counter'] = $originalCounter;
				$this->request->data['ServiceCounter']['ServiceCounter']['counter'] = 0;
				$this->request->data['ServiceCounter']['ServiceCounter']['order_ids'] = '';
				
				//$this->request->data['ServiceCounter']['ServiceCounter']['locking_stage'] = 1;
				$this->ServiceCounter->saveAll( $this->request->data['ServiceCounter'] );
				
				
				
			$e++;	
			}
			
			//Top rows
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'LOW VALUE MANIFEST DESTINATION SPAIN');
			$objPHPExcel->getActiveSheet()->setCellValue('A2', 'ORIGIN: Jersey');
			$objPHPExcel->getActiveSheet()->setCellValue('A3', 'TOTAL PIECES: '.$totalOrders);
			
			$objPHPExcel->getActiveSheet()->setCellValue('B2', 'Date: '.date('Y-m-d'));
			$objPHPExcel->getActiveSheet()->setCellValue('B3', 'TOTAL WEIGHT:'.$totalMassWeightNow.'kg');
						
			$objPHPExcel->getActiveSheet()->setCellValue('C2', 'DESTINATION: Spain');
			$objPHPExcel->getActiveSheet()->setCellValue('C3', 'TOTAL ITEMS:'.$totalOrders);
			
			//Set First Row  for Amazon FBa Sheet
			$objPHPExcel->setActiveSheetIndex(0);                                                                              
			$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->getAlignment()->applyFromArray(
			array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));                   
			$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet(0)->getStyle("A1:D1")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet(0)
			->getStyle('A1:D1')
			->applyFromArray(
                            array(
                                'fill' => array(
                                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                    'color' => array('rgb' => 'EBE5DB')
                                )
                            )
			);
			
			/*date_default_timezone_set('Europe/Jersey');
			$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));
			
			$folderName = 'Service Manifest -'. date("d.m.Y");
			$service = str_replace(' ', '', str_replace(':','_',$serviceProvider.'-'. date("d.m.Y") .'_'. $time_in_12_hour_format));*/
							  
			// create new folder with date if exists will remain same or else create new one                                                                                                                                                                                                
			$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
			
			$uploadUrl = WWW_ROOT .'img/cut_off/'. $folderName . '/' .$service.'.csv';                                          
			$uploadUrI = Router::url('/', true) . 'img/cut_off/'. $folderName . '/' .$service.'.csv';                                          
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
			$objWriter->save($uploadUrl);
			
			//Update Service Counter
			//$this->call_service_counter();			
			echo $uploadUrI; exit;
		}
		else
		{
			echo "blank"; exit;
		}
	}
	
	public function amazonReturnLabel()
	{
		
		$this->layout  = '';
		$this->autoRender = false;
		/* call the dompdf for print */
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper(array(0, 0, 280, 500), 'portrait');//landscape//portrait 
	
		/* css and html start */
		$imgPath = WWW_ROOT .'css/';
		
		$html = '				 
<div id="label">
<div class="container">
<table class="header row">
<tr>
<td width="35%"><img src="http://xsensys.com/img/jersey_logo.jpg" width="130px"></td>
<td width="65%" align="right">
<table class="jpoststamp "><tr>
<td class="stampnumber">1</td>
<td class="jerseyserial">Postage Paid<br>
Jersey<br>
Serial 216</td>
</tr>
<tr>
<td colspan="2" class="vatnumber">UK Import VAT Pre-Paid<br>
Auth No 393</td>
</tr>
</table>
</td>
</tr>
</table>

<div class="cn22 row">
<table>
<tr>
<td class="leftside leftheading"><h1>CUSTOMS DECLARATION</h1>
Great Britain
</td>
<td class="rightside rightheading"><h1>CN 22</h1>
May be opened officially
</td>
</tr>
</table>


<div class="fullwidth"><h2>Important!</h2></div>
<div class="producttype">
<table>
<tr>
<td width="15%"><span>&nbsp;</span>Gift</td>
<td width="25%"><span>&nbsp;</span>Documents</td>
<td width="30%"><span>&nbsp;</span>Commercial</td>
<td width="30%"><span>X</span>Merchandise</td>
</tr>
</table>

</div>

<table class="" cellpadding="1px" cellspacing="0" style="border-collapse:collapse" width="100%">
<tr>
<th class="noleftborder topborder rightborder bottomborder" width="60%">Quantity and detailed description of contents</th>
<th valign="top" class="center topborder norightborder bottomborder " width="20%">Weight (kg)</th>
</tr>
<b>Barcode Scanner</b>


<tr>
<td class="noleftborder topborder rightborder " ><span class="bold">For commercial items only</span><br>If known, HS tariff number and country of origin of goods</td>
<td valign="top" class="center topborder norightborder bold" >Total Weight (kg)</td>
</tr>
<tr>
<td class="center noleftborder bottomborder rightborder " ></td>
<td valign="top" class="center bottomborder norightborder  " ></td>
</tr>
</table>
Total Value: <b>£ </b>
<div class="fullwidth"><p>I the undersigned, whose name and address are given on the item, certify that the particulars given in this declaration are correct and that this item does not contain any dangerous article or articles prohibited by legislation or by postal or customs regulations.</p>
<table>
<tr>
<td class="date bold"></td>
<td class="sign right"><img src="http://xsensys.com/img/signature.png" style="margin-top:-5px"></td>
</tr>
</table>
</div>
</div>
<div class="footer row">

<table>
<tr>
<td class="leftside leftheading address"><h3>SHIP TO:</h3>
Amazon - Customer Returns<br>
Amazon Way<br>
Dunfermline<br>
Fife<br>
KY11 8ST<br> GB
</td>
<td class="rightside rightheading leftborder">
<div class="tracking bottomborder"><h3>TRACKING #:</h3>

</div>
<div class="barcode center">Purchase Order<div></div></div>
</td>
</tr>
</table>
</div>
<div class="footer row"><span class="bold">If undelivered please return to:</span> Unit 4, Cargo Centre, Jersey Airport, L\'Avenue de la Commune, St Peter, JE3 7BY</div>

</div>
</div>';
		/*$html = '<body>
				 <div id="label">
				 <div class="container" style="font-size:40px; margin-top:40px;">
				 Amazon - Customer Returns<br>
				 Amazon Way<br>
				 Dunfermline<br>
				 Fife<br>
				 KY11 8ST<br> GB
				 </div>
				 </div>
				 </body>';*/

		$html.= '<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
		$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
		$dompdf->render();
		//$dompdf->stream("test.pdf");
		
		$imgPath 	= 	WWW_ROOT .'img/exportPDF/'; 
		$path 		= 	Router::url('/', true).'img/exportPDF/';
		$name		=   'AmazonReturnLabel.pdf';
		
		file_put_contents($imgPath.$name, $dompdf->output());
		$serverPath  	= 	$path.$name ;
		$printerId	=	$this->getZebra();
		$sendData = array(
			'printerId' => $printerId,
			'title' => 'Now Print',
			'contentType' => 'pdf_uri',
			'content' => $serverPath,
			'source' => 'Direct'
		);
		
		/*App::import( 'Controller' , 'Coreprinters' );
		$Coreprinter = new CoreprintersController();
		$d = $Coreprinter->toPrint( $sendData );
		pr($d); exit;*/
		
	}
	
	/*public function saveManifest()
	{
		$this->loadModel( 'ManifestBackup' );
		
		$getmanifest	=	$this->ManifestBackup->find('first', array('conditions' => array('ManifestBackup.id' => 166)));
		
		pr( unserialize($getmanifest['ManifestBackup']['manifest_bk']) );
	}*/
    
	public function edocGenerateForCorreos() 
     {
		$this->layout = 'index';
		$this->loadModel( 'PostalProvider' );
		$this->loadModel( 'Location' );
		$zonesName = array('MADRID CAPITAL','MADRID REGION','BARCELONA REGION','REST OF COUNTRY');//madrid_region
		$zones = array('madrid_capital','madrid_region','barcelona_region','rest_of_country');
		$this->set( 'zones' , $zones );
		$this->set( 'zonesName' , $zonesName );
		
  	 }
	  public function edocGenerateForDHL() 
     {
		$this->layout = 'index';
		$this->loadModel( 'PostalProvider' );
		$this->loadModel( 'Location' );
		date_default_timezone_set('Europe/Jersey');
		$getPostalNames = $this->PostalProvider->find('all' , array( 'conditions' => array( 'PostalProvider.status' => 1,'PostalProvider.provider_name' => 'DHL') ));				
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$manifestPath = WWW_ROOT .'img/cut_off/'.$folderName;
		
		App::uses('Folder', 'Utility');
	    App::uses('File', 'Utility');
		$dir = new Folder($manifestPath, true, 0755);
	    $files = $dir->find('.*\.csv');
	    $files = Set::sort($files, '{n}', 'DESC');
	    $this->set('files', $files);
	    $this->set( 'folderName' , $folderName );		
		$this->set( 'getPostalNames' , $getPostalNames );
	
		foreach( $getPostalNames as $getPostalName )
		{
			if( $getPostalName['PostalProvider']['location_id'] != '' )
			{
				$locationIDs		=	explode(',', $getPostalName['PostalProvider']['location_id']);
				foreach( $locationIDs as $locationID )
				{
					$this->Location->unbindModel( array( 'hasMany' => array( 'PostaServiceDesc' ) ) );
					$locationParam 	=	array( 'conditions'=>array('Location.id' => $locationID) );
					$locationDetail[]	=	$this->Location->find( 'first', $locationParam );
				}
			}
		}
		$this->set( 'locationDetail' , $locationDetail );
		
  	 }
    public function createManifestFromEntry()
	{
		$this->loadModel('ManifestEntrie');
		$this->loadModel('MergeUpdate');
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'Customer' );
		$date	=	$this->request->data['date'];
		/* start european country iso code*/
		$isoCode = Configure::read('customIsoCodes');
		  /* end european country iso code*/
		
		
		$getmanifestResults = $this->ManifestEntrie->find( 'all', array( 'conditions' => array('ManifestEntrie.manifest_date >' => $date.' 00:00:00', 'ManifestEntrie.manifest_date <' => $date.' 23:59:59', 'ManifestEntrie.service_provider' => array('Royalmail','PostNL') ) ) );
		
		if(count($getmanifestResults) > 0)
		{
			$shipMentPath = WWW_ROOT .'img/shipment.txt';
			file_put_contents($shipMentPath, "LineNo\t"."Option\t"."LineIdentifier\t"."GroupageManifestNo\t"."Consignor\t"."ConsignorAddress1\t"."ConsignorAddress2\t"."ConsignorPostCode\t"."ConsigneeName\t"."ConsigneeAddress1\t"."ConsigneeAddress2\t"."ConsigneeGSTNumber\t"."ConsigneePostCode\t". "ConsigneeCountry\t"."NoOfUnits\t"."GrossMass\t"."Description\t"."Value\t". "ValueCurr\t"."ForwardingAgent\t"."ForwardingAgentAddress1\t". "ForwardingAgentAddress2\t"."ForwardingAgentCountry\t"."CommunityStatus\n", LOCK_EX);  
			
			$inc = 1;
			$cnt = 2;
			$e = 0;
			$k = 0;
			$combineSku = '';
			$combineCategory = '';
			
			foreach($getmanifestResults as $getmanifestResult)
			{
					
					$orderIdSpecified	=	$getmanifestResult['ManifestEntrie']['split_order_id'];
					$params = array('conditions' => array('MergeUpdate.product_order_id_identify' => $orderIdSpecified),
									'fields' => array(
										'MergeUpdate.id',
										'MergeUpdate.order_id',
										'MergeUpdate.product_order_id_identify',
										'MergeUpdate.quantity',
										'MergeUpdate.sku',
										'MergeUpdate.price',
										'MergeUpdate.packet_weight',
										'MergeUpdate.envelope_weight'
									)
								);
						$mergeOrder = json_decode(json_encode($this->MergeUpdate->find(
							'all', $params
						)),0);
						
						$getSku = explode( ',' , $mergeOrder[0]->MergeUpdate->sku);
						$packageWeight = $mergeOrder[0]->MergeUpdate->envelope_weight;   
						$totalPriceValue = 0;
						$massWeight = 0;
						$totalUnits = 0;
						$combineTitle = '';
						$combineCategory = '';
						$calculateWeight = 0;
						
						$getSpecifier = explode('-', $mergeOrder[0]->MergeUpdate->product_order_id_identify);
						$setSpaces = '';
						$identifier = $getSpecifier[1];
						$em = 0;while( $em < $identifier )
							{
								$setSpaces .= $setSpaces.' ';						
								$em++;	
							}
						$j = 0;while( $j <= count($getSku)-1 )
						{
							$newSku = explode( 'XS-' , $getSku[$j] );
							$setNewSku = 'S-'.$newSku[1];
							
							$this->loadModel( 'Product' );
							$this->loadModel( 'Category' );					
						
							$this->Product->bindModel(
								array(
								 'hasOne' => array('Category' => array('foreignKey' => false,'conditions' => array('Category.id = Product.category_id'),
													'fields' => array('Category.id,Category.category_name')))));
						
							$productSku = $this->Product->find('first' ,array('conditions' => array('Product.product_sku' => $setNewSku)));
							
							if( $combineTitle == '' )
							{
								$combineTitle = $newSku[0] .'X' .substr($productSku['Product']['product_name'],0,25);	
								$totalUnits = $totalUnits + $newSku[0];     
															$calculateWeight = ($newSku[0] * $productSku['ProductDesc']['weight']);
								$massWeight = $massWeight + $calculateWeight;		
								$combineCategory = $productSku['Category']['category_name'];				
							}
							else
							{
								$combineTitle .= ',' .  $newSku[0] . 'X' .substr($productSku['Product']['product_name'],0,25);	
								$totalUnits = $totalUnits + $newSku[0];
															$calculateWeight = ($newSku[0] * $productSku['ProductDesc']['weight']);
								$massWeight = $massWeight + $calculateWeight;                                                        
								$combineCategory .= ',' . $productSku['Category']['category_name'];				
							}
							 
							if( $combineSku == '' )
								$combineSku = $setNewSku;	
							else
								$combineSku .= ',' . $setNewSku;	
							
						$j++;	
						}
						$massWeight = $packageWeight + $massWeight; 
						$paramsConsignee = array(
							'conditions' => array(
								'OpenOrder.num_order_id' => $mergeOrder[0]->MergeUpdate->order_id
							),
							'fields' => array(
								'OpenOrder.num_order_id',
								'OpenOrder.id',
								'OpenOrder.general_info',
								'OpenOrder.shipping_info',
								'OpenOrder.customer_info',
								'OpenOrder.totals_info'							
							)
						);
						
						$getConsigneeDetailFromLinnworksOrder = json_decode(json_encode($this->OpenOrder->find( 'first', $paramsConsignee )),0);					
						$congineeInfo = unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->customer_info);					
						$totalInfo = unserialize($getConsigneeDetailFromLinnworksOrder->OpenOrder->totals_info);
						$postcountry =  $congineeInfo->Address->Country;
						$previousDate	=	date('Y-m-d h:i:s', strtotime('-10 days'));
						
							
						$exteraword = '';
						 if($identifier >= 2)
							{
								$getCustomerDetail =	$this->Customer->find('all', 
										array( 
										'conditions' => array( 
															'Customer.country' => $postcountry,
										'and'		=> array('Customer.date >' => '2016-01-01 00:00:00',
															'Customer.date <' => $previousDate
															)),
										 'order' => 'rand()',
										  'limit' => 1,
										)
									);
									
								$customerName		=	$getCustomerDetail[0]['Customer']['name'].' ';
								$customerAddress1	=	$getCustomerDetail[0]['Customer']['address1'].' ';
								$customerAddress2	=	$getCustomerDetail[0]['Customer']['address2'].' ';
							
							}
							else
							{
								$customerName 		= $congineeInfo->Address->FullName;
								$customerAddress1 	= $congineeInfo->Address->Address1;
								$customerAddress2 	= $congineeInfo->Address->Address2;
							}
							$country = '';					
						foreach( $isoCode as $index => $value )
						{
							if( $index == $congineeInfo->Address->Country )
							{
								$country = $value;
							}
						}
						$currencyMatter = $totalInfo->Currency;
						
						$globalCurrencyConversion = 1;
						
						if( $currencyMatter == "EUR" )
						{
							$globalCurrencyConversion = 1;
						}
						else
						{
							$globalCurrencyConversion = 1.38;
						}
						
						$totalValue = 0;
						$setPrice = $mergeOrder[0]->MergeUpdate->price;
						if( ( $setPrice * $globalCurrencyConversion) > 21.99 )
						{
							$totalValue = number_format($this->getrand(), 2, '.', ''); 
						}
						else
						{
							$totalValue = number_format(( $setPrice * $globalCurrencyConversion ), 2, '.', '');
						}
						$orderIdentfire = $mergeOrder[0]->MergeUpdate->product_order_id_identify;
					
					file_put_contents($shipMentPath, $inc."\t"."N\t".$orderIdentfire."\t"."\t"."ESL Limited\t"."Unit 4 Airport Cargo Centre\t"."L'avenue De La Comune, Jersey\t"."JE3 7BY\t".$customerName."\t".$customerAddress1."\t"."\t"."\t".$congineeInfo->Address->PostCode."\t". $country."\t".$totalUnits."\t".$massWeight."\t".$combineCategory."\t".$totalValue."\t". "EUR\t"."ESL Limited\t"."Unit 4 Airport Cargo Centre\t". "L'avenue De La Comune, JE3 7BY\t"."JE\t"."T2\n", FILE_APPEND | LOCK_EX);
					$combineSku = '';
					$totalUnits = 0;
					$massWeight = 0;
					$combineCategory = '';
						
					$inc++;	
					$cnt++;	
					$k++;	
			}
			echo "1";
			exit;
		}
		else
		{
			echo 'Manifest was not generated on ' .$date;
			exit;
		}
		
	}
	
	
	public function customManifest()
		{
			$this->layout = 'index';
			$this->loadModel('ManifestEntrie');
			$getmanifestLists = $this->ManifestEntrie->find('all', array(
																 'conditions' => array('ManifestEntrie.service_provider' => 'PostNL'),
																 'group'=>array('ManifestEntrie.manifest_name'), 
																 'fields' => array('ManifestEntrie.manifest_name','ManifestEntrie.manifest_date' ),
																 'order' => array('manifest_date DESC'),
																 'limit' => '10'
																 ));
			$this->set( 'getmanifestLists' , $getmanifestLists );
			
		}
		
	public function downloadTextFile()
	{
		$result = WWW_ROOT .'img/shipment.txt';
		header('Content-Description: File Transfer');
		header('Content-Type: application/force-download');
		header('Content-Disposition: attachment; filename='.basename($result));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($result));   
		readfile($result);
	}
		
    public function putDhlFiles( $order_ids = array() )
	{
		
		//$order_ids = array('612030-1','613207-1','612049-1');
		
		$this->layout = '';
		$this->autoRender = false;	
		$logs							= 	WWW_ROOT .'img/logs/putDhl.log';
		$file_location					=   $_SERVER['DOCUMENT_ROOT'].'/dhl/';
		
		$authDSetails 					= 	json_decode(json_encode(Configure::read('dhlAuth')));
		$ftp_server 					= 	$authDSetails->server;
		$strServerPort 					= 	$authDSetails->port;
		$ftp_user_name 					= 	$authDSetails->user;
		$ftp_user_pass 					= 	$authDSetails->pass;
		
		$conn_id = ftp_connect($ftp_server);
		$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
			
		$dir = date('m-d-Y');
		$mk_ok = 1;$msg = '';
		if(!@ftp_chdir($conn_id, $dir)){
			$ftp_mkdir = ftp_mkdir($conn_id, $dir);
			if($ftp_mkdir){
				$msg = 'Remote directory created successfully';				
			}
			else{
				$msg = 'Error creating remote directory';
				$mk_ok = 2;
			}
		} 
		$ids = array();
		if($mk_ok == 1){
			$options = array('ftp' => array('overwrite' => true)); 
			$stream = stream_context_create($options);  
	
			foreach($order_ids as $orderid){
				file_put_contents("ftp://$ftp_user_name:$ftp_user_pass@$ftp_server/".$dir."/label_".$orderid.".pdf",file_get_contents($file_location."label_".$orderid.".pdf"), 0, $stream); 
				file_put_contents("ftp://$ftp_user_name:$ftp_user_pass@$ftp_server/".$dir."/invoice_".$orderid.".pdf",file_get_contents($file_location."invoice_".$orderid.".pdf"), 0, $stream); 			
				$ids[] = $orderid;
				$msg = 'File moved successfully.';
			}
		}
	  file_put_contents($logs ,date('Y-m-d H:i:s')."\n".$msg."\n". print_r($ids,true), FILE_APPEND | LOCK_EX );
	
	}
	
	 public function edocGenerateForDeutschePost() 
     {
		$this->layout = 'index';
		$this->loadModel( 'PostalProvider' );
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'Location' );
		date_default_timezone_set('Europe/Jersey');
		$getPostalNames = $this->PostalProvider->find('all' , array( 'conditions' => array( 'PostalProvider.status' => 1,'PostalProvider.provider_name' => 'France-Direct Entry') ));
		$getserviceCount = $this->ServiceCounter->find( 'all' , array( 'conditions' => array( 'ServiceCounter.service_provider' => 'Jersey Post', 'ServiceCounter.service_code IN'=>array('GYE','FRU') ) ) );			
			
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$manifestPath = WWW_ROOT .'img/cut_off/'.$folderName;
		
		App::uses('Folder', 'Utility');
	    App::uses('File', 'Utility');
		$dir = new Folder($manifestPath, true, 0755);
	    $files = $dir->find('.*\.csv');
	    $files = Set::sort($files, '{n}', 'DESC');
	    $this->set('files', $files);
	    $this->set( 'folderName' , $folderName );		
		$this->set( 'getPostalNames' , $getPostalNames );
	
		foreach( $getPostalNames as $getPostalName )
		{
			if( $getPostalName['PostalProvider']['location_id'] != '' )
			{
				$locationIDs		=	explode(',', $getPostalName['PostalProvider']['location_id']);
				foreach( $locationIDs as $locationID )
				{
					$this->Location->unbindModel( array( 'hasMany' => array( 'PostaServiceDesc' ) ) );
					$locationParam 	=	array( 'conditions'=>array('Location.id' => $locationID) );
					$locationDetail[]	=	$this->Location->find( 'first', $locationParam );
				}
			}
		}
		$this->set( 'locationDetail' , $locationDetail );
		$this->set( 'getserviceCount' , $getserviceCount );
		
  	 }
	 
	public function edocDeutschePost( ) 
     {
		$this->layout = 'index';
		$this->loadModel( 'PostalProvider' );
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Location' );
		
		date_default_timezone_set('Europe/Jersey');		
		$scan_date = date('Y-m-d', strtotime("-10 Days"));
		
		$getDe = $this->MergeUpdate->find('all' , array( 'conditions' => array( 'scan_date >' => $scan_date,'manifest_date' => '','service_name'=>'Germany - Direct Entry'),'fields'=>['order_id','merge_order_date','order_date','provider_ref_code','service_name','scan_date','process_date','scan_date'] ));
 		
		/* foreach($getDe as $de){
		   $data[$de['MergeUpdate']['provider_ref_code']][] = ['provider_ref_code'=>$de['MergeUpdate']['provider_ref_code'],'service_name'=>$de['MergeUpdate']['service_name']];
		 }
		  
		 foreach(array_keys($data) as $k){
		  pr( $data[$k]);
		 }
		 exit;*/
		
		$getFr = $this->MergeUpdate->find('all' , array( 'conditions' => array( 'scan_date >' => $scan_date,'manifest_date' => '','service_name'=>'France-Direct Entry'),'fields'=>['order_id','merge_order_date','order_date','provider_ref_code','service_name','scan_date','process_date','scan_date'] ));
 		
		$this->set( 'de_data' , $getDe );
		$this->set( 'fr_data' , $getFr );
		 
	 	 
   	 }
	 
	  
    public function edocGenerateForWhistl() 
    {
		$this->layout = 'index';
		 
		date_default_timezone_set('Europe/Jersey');
		$this->loadModel( 'MergeUpdate' );
			
		$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status' => 0,'MergeUpdate.service_provider' => 'whistl','MergeUpdate.manifest_date'=>'','MergeUpdate.delevery_country' => 'United Kingdom'),'fields'=>['service_name','provider_ref_code','product_order_id_identify','packet_weight']));
		
		$letter = 0;
		$la_le_100 = 0; $la_le_250 = 0; $la_le_300 = 0; $la_le_350 = 0; $la_le_400 = 0;
		$la_le_450 = 0; $la_le_500 = 0; $la_le_550 = 0; $la_le_600 = 0; $la_le_650 = 0;
		$la_le_700 = 0; $la_le_750 = 0;
		
		$p_100 = 0; $p_250 = 0; $p_300 = 0; $p_350 = 0; $p_400 = 0;
		$p_450 = 0; $p_500 = 0; $p_550 = 0; $p_600 = 0; $p_650 = 0;
		$p_700 = 0; $p_750 = 0; $p_800 = 0; $p_850 = 0; $p_900 = 0; 
		$p_950 = 0; $p_1000 = 0; $p_1250 = 0; $p_1500 = 0; $p_1750 = 0; $p_2000 = 0;
		$packet_weight = [];
		$service_name = [];
		foreach($orders  as $v){
		
				$service_name[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['provider_ref_code'];
				$packet_weight[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['packet_weight'];
				
			/*if($v['MergeUpdate']['service_name'] == 'letter'){
				$service_name[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['provider_ref_code'];
				$packet_weight[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['packet_weight'];
			}else if($v['MergeUpdate']['service_name'] == 'large_letters'){
				if($v['MergeUpdate']['provider_ref_code'] == 'LL100'){
					$service_name[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['provider_ref_code'];
					$packet_weight[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['packet_weight'];
				}else if($v['MergeUpdate']['provider_ref_code'] == 'LL250'){ 
					
					$service_name[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['provider_ref_code'];
					$packet_weight[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['packet_weight'];
				}else if($v['MergeUpdate']['provider_ref_code'] == 'LL300'){ 
					$la_le_300++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'LL350'){ 
					$la_le_350++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'LL400'){ 
					$la_le_400++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'LL450'){ 
					$la_le_450++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'LL500'){ 
					$la_le_500++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'LL550'){ 
					$la_le_550++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'LL600'){
					$la_le_600++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'LL650'){ 
					$la_le_650++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'LL700'){ 
					$la_le_700++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'LL750'){ 
					$la_le_750++;
					$service_name[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['provider_ref_code'];
					$packet_weight[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['packet_weight'];
				}
			
			}else if($v['MergeUpdate']['service_name'] == 'packets'){
				if($v['MergeUpdate']['provider_ref_code'] == 'P100'){
					$p_100++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P250'){ 
					$p_250++;
					$service_name[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['provider_ref_code'];
					$packet_weight[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['packet_weight'];
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P300'){ 
					$p_300++;
					$service_name[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['provider_ref_code'];
					$packet_weight[$v['MergeUpdate']['provider_ref_code']][] = $v['MergeUpdate']['packet_weight'];
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P350'){ 
					$p_350++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P400'){ 
					$p_400++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P450'){ 
					$p_450++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P500'){ 
					$p_500++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P550'){ 
					$p_550++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P600'){
					$p_600++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P650'){ 
					$p_650++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P700'){ 
					$p_700++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P750'){ 
					$p_750++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P800'){ 
					$p_800++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P850'){ 
					$p_850++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P900'){ 
					$p_900++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P950'){ 
					$p_950++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P1000'){ 
					$p_1000++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P1250'){ 
					$p_1250++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P1500'){ 
					$p_1500++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P1750'){ 
					$p_1750++;
				}else if($v['MergeUpdate']['provider_ref_code'] == 'P2000'){ 
					$p_2000++;
				}
			
			}*/
		}
	  
		$this->set( compact( 'service_name' , 'packet_weight' ) ); 
		
		 
   }
   
    public function edocGenerateForRoyalmail() 
    {
		$this->layout = 'index';
		$this->loadModel( 'PostalProvider' );
		$this->loadModel( 'Location' );
		date_default_timezone_set('Europe/Jersey');
		$getPostalNames = $this->PostalProvider->find('all' , array( 'conditions' => array( 'PostalProvider.status' => 1,'PostalProvider.provider_name' => 'Royalmail') ));				
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$manifestPath = WWW_ROOT .'img/cut_off/'.$folderName;
		
		App::uses('Folder', 'Utility');
	    App::uses('File', 'Utility');
		$dir = new Folder($manifestPath, true, 0755);
	    $files = $dir->find('.*\.csv');
	    $files = Set::sort($files, '{n}', 'DESC');
	    $this->set('files', $files);
	    $this->set( 'folderName' , $folderName );		
		$this->set( 'getPostalNames' , $getPostalNames );
	
		foreach( $getPostalNames as $getPostalName )
		{
			if( $getPostalName['PostalProvider']['location_id'] != '' )
			{
				$locationIDs		=	explode(',', $getPostalName['PostalProvider']['location_id']);
				foreach( $locationIDs as $locationID )
				{
					$this->Location->unbindModel( array( 'hasMany' => array( 'PostaServiceDesc' ) ) );
					$locationParam 	=	array( 'conditions'=>array('Location.id' => $locationID) );
					$locationDetail[]	=	$this->Location->find( 'first', $locationParam );
				}
			}
		}
		$this->set( 'locationDetail' , $locationDetail );
		
   }
   
   public function getExactWeightRoyalmail()
	{
		
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'PostalProvider' );
		$this->loadModel( 'Location' );
		$countryArray = Configure::read('customCountry');
		
	    //$orderIds = $this->request->data['orderId'];
	    $mergeUpdaeIDs	=	$this->ServiceCounter->find( 'all', array( 'conditions' => array( 'ServiceCounter.service_provider' => 'Royalmail'), 'fields' => 'ServiceCounter.order_ids, ServiceCounter.destination, ServiceCounter.counter' ) );
	    $getPostalNames = $this->PostalProvider->find('all' , array( 'conditions' => array( 'PostalProvider.status' => 1,'PostalProvider.provider_name' => 'Royalmail') ));				
	    foreach( $getPostalNames as $getPostalName )
		{
				$locationIDs		=	explode(',', $getPostalName['PostalProvider']['location_id']);
				foreach( $locationIDs as $locationID )
				{
					$this->Location->unbindModel( array( 'hasMany' => array( 'PostaServiceDesc' ) ) );
					$locationParam 	=	array( 'conditions'=>array('Location.id' => $locationID), 'fields' => array( 'Location.county_name' ) );
					$locations[]	=	$this->Location->find( 'first', $locationParam );
				}
		}
		/* for create rule country array */
		foreach( $locations as $country )
		{
			
			$locationArray[ $country['Location']['county_name'] ] = $country['Location']['county_name'];
		}
		
		
		$i = 0;
		foreach( $mergeUpdaeIDs as $mergeUpdaeID )
				{
					$mergeUpdatearray		=	explode(',', $mergeUpdaeID['ServiceCounter']['order_ids']);
					$counter		=	explode(',', $mergeUpdaeID['ServiceCounter']['counter']);
			foreach( $locations as $locationValue )
					{
						if( $mergeUpdaeID['ServiceCounter']['destination'] == $locationValue['Location']['county_name'] )
						{
							$params = array(                    
									'conditions' => array(
										'MergeUpdate.id' => $mergeUpdatearray,
										'MergeUpdate.delevery_country' => $mergeUpdaeID['ServiceCounter']['destination']
									),
									'fields' => array(                                                
									   'SUM( MergeUpdate.packet_weight ) as Weight',
										'SUM( MergeUpdate.envelope_weight ) as EnvelopeWeight',
										'MergeUpdate.delevery_country',
										'MergeUpdate.provider_ref_code',
										'MergeUpdate.service_provider',
										'MergeUpdate.service_name'
									),
								);
						
							$sumDataById[$i] = $this->MergeUpdate->find( 'first' , $params );
							$sumDataById[$i]['counter'] = $counter;
						}
						else
						{
							$params = array(                    
											'conditions' => array(
												'MergeUpdate.id' => $mergeUpdatearray,
												'MergeUpdate.delevery_country' => $mergeUpdaeID['ServiceCounter']['destination']
											),
											'fields' => array(                                                
											   'SUM( MergeUpdate.packet_weight ) as Weight',
												'SUM( MergeUpdate.envelope_weight ) as EnvelopeWeight',
												'MergeUpdate.delevery_country',
												'MergeUpdate.provider_ref_code',
												'MergeUpdate.service_provider',
												'MergeUpdate.service_name'
											),
										);
								
							$sumDataById[$i] = $this->MergeUpdate->find( 'first' , $params );
							$sumDataById[$i]['counter'] = $counter;
						}
					}
					$i++;
				}
				$totalWeight = 0;
				
				$outerLocation = $locationArray;
				$locateValue = array_keys($locationArray, "Rest Of EU");
				$locateValue = $locateValue[0];
				unset( $outerLocation[$locateValue] );
				
				$getdata = array();
				
				
				if( count($locationArray) > 0 )
				{
					$j = 0;
					$getCounter = 0;
					foreach( $locationArray as $countryName ) //Uk||DE|FR|REST
					{
						
						$totalWeight = 0;
						$encelopWeight = 0;
						$getCounter = 0;
						$weight =0;
						$gTotal = 0;
						
						foreach( $sumDataById as $value )
						{
							
							if( $countryName == $value['MergeUpdate']['delevery_country'] )
							{
								$getCounter 			= $getCounter + $value['counter'][0];
								$totalWeight 			= $value[0]['Weight'];
								$encelopWeight 			= $value[0]['EnvelopeWeight'];
								$weight 				= $value[0]['Weight'] + $value[0]['EnvelopeWeight'];
								$country 				= $value['MergeUpdate']['delevery_country'];
								$postal_provider_code[]	= $value['MergeUpdate']['provider_ref_code'].'###'.$weight.'###'.$value['counter'][0].'###'.$value['MergeUpdate']['service_name'];
								
							}
							else
							{
								if( stristr( $countryName , "Rest Of EU" ) == true )
								{
									
									if( count( $outerLocation ) > 0 )	
									{
										if( !in_array( $value['MergeUpdate']['delevery_country'] , $outerLocation ) )
										{
											$getCounter 			= $getCounter + $value['counter'][0];
											$totalWeight 			= $value[0]['Weight'];
											$encelopWeight 			= $value[0]['EnvelopeWeight'];
											$weight 				= $value[0]['Weight'] + $value[0]['EnvelopeWeight'];
											$country 				= 'RestOfEU';

											$postal_provider_code[]	=	$value['MergeUpdate']['provider_ref_code'] .'###'.$weight.'###'.$value['counter'][0].'###'.$value['MergeUpdate']['service_name'];
											$weight = '';
										}
										
									}
									else
									{
											$getCounter 			= $getCounter + $value['counter'][0];
											$totalWeight 			= $value[0]['Weight'];
											$encelopWeight 			= $value[0]['EnvelopeWeight'];
											$weight 				= $value[0]['Weight'] + $value[0]['EnvelopeWeight'];
											$country 				= 'RestOfEU';
											$postal_provider_code[]	=	$value['MergeUpdate']['provider_ref_code'].'###'.$weight.'###'.$value['counter'][0].'###'.$value['MergeUpdate']['service_name'];
											$weight = '';
									}
									
								}
									
							}
							
						}
						
						if( isset( $country ) && isset($postal_provider_code) )
						{
							$getdata[$j]['totalWeight'] 	= $totalWeight + $encelopWeight;
							$getdata[$j]['counter'] 		= $getCounter;
							$getdata[$j]['delcountry'] 		= $country;
							$getdata[$j]['postal_provider_code'] = $postal_provider_code;
							unset( $totalWeight );
							unset( $country );
							unset( $getCounter );
							unset( $postal_provider_code );
							$j++;
						}
						
					}
				
				}
				else
				{
					echo "0"; exit;
				}
				//pr($getdata);
				//exit;
				
				echo (json_encode(array('status' => '1', 'data' => $getdata)));
				exit;
	}
	
	public function getRoyalmailDocket()
	{
		
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'PostalProvider' );
		$this->loadModel( 'Location' );
		$countryArray = Configure::read('customCountry');
		
	    //$orderIds = $this->request->data['orderId'];
	    $mergeUpdaeIDs	=	$this->ServiceCounter->find( 'all', array( 'conditions' => array( 'ServiceCounter.service_provider' => 'Royalmail'), 'fields' => 'ServiceCounter.order_ids, ServiceCounter.destination, ServiceCounter.counter' ) );
	    $getPostalNames = $this->PostalProvider->find('all' , array( 'conditions' => array( 'PostalProvider.status' => 1,'PostalProvider.provider_name' => 'Royalmail') ));				
	    foreach( $getPostalNames as $getPostalName )
		{
				$locationIDs		=	explode(',', $getPostalName['PostalProvider']['location_id']);
				foreach( $locationIDs as $locationID )
				{
					$this->Location->unbindModel( array( 'hasMany' => array( 'PostaServiceDesc' ) ) );
					$locationParam 	=	array( 'conditions'=>array('Location.id' => $locationID), 'fields' => array( 'Location.county_name' ) );
					$locations[]	=	$this->Location->find( 'first', $locationParam );
				}
		}
		/* for create rule country array */
		foreach( $locations as $country )
		{
			
			$locationArray[ $country['Location']['county_name'] ] = $country['Location']['county_name'];
		}
		
		
		$i = 0;
		foreach( $mergeUpdaeIDs as $mergeUpdaeID )
				{
					$mergeUpdatearray		=	explode(',', $mergeUpdaeID['ServiceCounter']['order_ids']);
					$counter		=	explode(',', $mergeUpdaeID['ServiceCounter']['counter']);
			foreach( $locations as $locationValue )
					{
						if( $mergeUpdaeID['ServiceCounter']['destination'] == $locationValue['Location']['county_name'] )
						{
							$params = array(                    
									'conditions' => array(
										'MergeUpdate.id' => $mergeUpdatearray,
										'MergeUpdate.delevery_country' => $mergeUpdaeID['ServiceCounter']['destination']
									),
									'fields' => array(                                                
									   'SUM( MergeUpdate.packet_weight ) as Weight',
										'SUM( MergeUpdate.envelope_weight ) as EnvelopeWeight',
										'MergeUpdate.delevery_country',
										'MergeUpdate.provider_ref_code',
										'MergeUpdate.service_provider',
										'MergeUpdate.service_name'
									),
								);
						
							$sumDataById[$i] = $this->MergeUpdate->find( 'first' , $params );
							$sumDataById[$i]['counter'] = $counter;
						}
						else
						{
							$params = array(                    
											'conditions' => array(
												'MergeUpdate.id' => $mergeUpdatearray,
												'MergeUpdate.delevery_country' => $mergeUpdaeID['ServiceCounter']['destination']
											),
											'fields' => array(                                                
											   'SUM( MergeUpdate.packet_weight ) as Weight',
												'SUM( MergeUpdate.envelope_weight ) as EnvelopeWeight',
												'MergeUpdate.delevery_country',
												'MergeUpdate.provider_ref_code',
												'MergeUpdate.service_provider',
												'MergeUpdate.service_name'
											),
										);
								
							$sumDataById[$i] = $this->MergeUpdate->find( 'first' , $params );
							$sumDataById[$i]['counter'] = $counter;
						}
					}
					$i++;
				}
				$totalWeight = 0;
				
				$outerLocation = $locationArray;
				$locateValue = array_keys($locationArray, "Rest Of EU");
				$locateValue = $locateValue[0];
				unset( $outerLocation[$locateValue] );
				
				$getdata = array();
				
				
				if( count($locationArray) > 0 )
				{
					$j = 0;
					$getCounter = 0;
					foreach( $locationArray as $countryName ) //Uk||DE|FR|REST
					{
						
						$totalWeight = 0;
						$encelopWeight = 0;
						$getCounter = 0;
						$weight =0;
						$gTotal = 0;
						
						foreach( $sumDataById as $value )
						{
							
							if( $countryName == $value['MergeUpdate']['delevery_country'] )
							{
								$getCounter 			= $getCounter + $value['counter'][0];
								$totalWeight 			= $value[0]['Weight'];
								$encelopWeight 			= $value[0]['EnvelopeWeight'];
								$weight 				= $value[0]['Weight'] + $value[0]['EnvelopeWeight'];
								$country 				= $value['MergeUpdate']['delevery_country'];
								$postal_provider_code[]	= $value['MergeUpdate']['provider_ref_code'].'###'.$weight.'###'.$value['counter'][0].'###'.$value['MergeUpdate']['service_name'];
								
							}
							else
							{
								if( stristr( $countryName , "Rest Of EU" ) == true )
								{
									
									if( count( $outerLocation ) > 0 )	
									{
										if( !in_array( $value['MergeUpdate']['delevery_country'] , $outerLocation ) )
										{
											$getCounter 			= $getCounter + $value['counter'][0];
											$totalWeight 			= $value[0]['Weight'];
											$encelopWeight 			= $value[0]['EnvelopeWeight'];
											$weight 				= $value[0]['Weight'] + $value[0]['EnvelopeWeight'];
											$country 				= 'RestOfEU';

											$postal_provider_code[]	=	$value['MergeUpdate']['provider_ref_code'] .'###'.$weight.'###'.$value['counter'][0].'###'.$value['MergeUpdate']['service_name'];
											$weight = '';
										}
										
									}
									else
									{
											$getCounter 			= $getCounter + $value['counter'][0];
											$totalWeight 			= $value[0]['Weight'];
											$encelopWeight 			= $value[0]['EnvelopeWeight'];
											$weight 				= $value[0]['Weight'] + $value[0]['EnvelopeWeight'];
											$country 				= 'RestOfEU';
											$postal_provider_code[]	=	$value['MergeUpdate']['provider_ref_code'].'###'.$weight.'###'.$value['counter'][0].'###'.$value['MergeUpdate']['service_name'];
											$weight = '';
									}
									
								}
									
							}
							
						}
						
						if( isset( $country ) && isset($postal_provider_code) )
						{
							$getdata[$j]['totalWeight'] 	= $totalWeight + $encelopWeight;
							$getdata[$j]['counter'] 		= $getCounter;
							$getdata[$j]['delcountry'] 		= $country;
							$getdata[$j]['postal_provider_code'] = $postal_provider_code;
							unset( $totalWeight );
							unset( $country );
							unset( $getCounter );
							unset( $postal_provider_code );
							$j++;
						}
						
					}
				
				}
				else
				{
					echo "0"; exit;
				}
				//pr($getdata);
				//exit;
				
			return	 $getdata;
				 
	}
	
	public function edocGenerateForJerseypost() 
   	{
	    $this->layout = 'index';
	    $this->loadModel( 'PostalProvider' );
	    $getPostalNames = $this->PostalProvider->find('all' , array( 'conditions' => array( 'PostalProvider.status' => 1 ) ));				
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$manifestPath = WWW_ROOT .'img/cut_off/'.$folderName;
		
		App::uses('Folder', 'Utility');
	    App::uses('File', 'Utility');
		$dir = new Folder($manifestPath, true, 0755);
	    $files = $dir->find('.*\.csv');
	    $files = Set::sort($files, '{n}', 'DESC');
	    $this->set('files', $files);
	    $this->set( 'folderName' , $folderName );		
		$this->set( 'getPostalNames' , $getPostalNames );
	}
	
	public function setInfoJerseyEdoc()
	{
		$this->layout = '';
		$this->autoRender = false;          
		// Get All manifest related services
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'MergeUpdate' );
		$serviceProvider = $this->request->data['serviceProvider'];
		date_default_timezone_set('Europe/Jersey');
		$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));
		
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$service = str_replace(' ', '', str_replace(':','_',$serviceProvider.'-'. date("d.m.Y") .'_'. $time_in_12_hour_format));
		$params = array('conditions' => array('ServiceCounter.service_provider' => $serviceProvider,'ServiceCounter.counter >' => 0,'ServiceCounter.original_counter !=' => ''),
						'fields' => array('SUM(ServiceCounter.counter) as counter','ServiceCounter.bags','ServiceCounter.service_code','ServiceCounter.order_ids'),
						'group' => 'ServiceCounter.service_code');
		
		$eShipperData = $this->ServiceCounter->find( 'all', $params );	
		/*foreach( $eShipperData as $value )
		{
			 $ids	=	explode(',' , $value['ServiceCounter']['order_ids']);
			 foreach($ids as $id)
			 {
			 	$this->setManifestRecordJerseyPost( $id,  $service );
			 }
		}*/		
		echo (json_encode(array('status' => '1', 'data' => $eShipperData)));
		exit;
		
	}
	
	
	public function gentateManifest()
	{
		$this->layout = '';
		$this->autoRender = false;          
		
		// Get All manifest related services
		$this->loadModel( 'ServiceCounter' );
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'Customer' );
		
		/* start european country iso code*/
		$isoCode = Configure::read('customIsoCodes');
		  /* end european country iso code*/
		
		//Global Variable
		$glbalSortingCounter = 0;
		
		$serviceProvider = 'PostNL';//$this->request->data['serviceProvider'];
		date_default_timezone_set('Europe/Jersey');
		$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));
			
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$service = str_replace(' ', '', str_replace(':','_',$serviceProvider.'-'. date("d.m.Y") .'_'. $time_in_12_hour_format));
 		
 		$manifest = $this->MergeUpdate->find('all', array('conditions' => array('custom_marked' => 0, 'status' => 1,'scan_status' => 1,'sorted_scanned' => 1,'manifest_date is NULL','service_provider'=>[$serviceProvider,'Royalmail','GLS']),'fields'=>['id','sku','product_order_id_identify','merge_order_date','service_provider','dispatch_console','sorted_scanned','scan_status','scan_date','order_id','quantity','sku','price','packet_weight','envelope_weight']));         
  		pr($manifest);
		
		if( count($manifest) > 0 )
		{
		                                       
			App::import('Vendor', 'PHPExcel/IOFactory');
			App::import('Vendor', 'PHPExcel');                          
			
			//Set and create Active Sheet for single workbook with singlle sheet
			$objPHPExcel = new PHPExcel();       
			$objPHPExcel->createSheet();
			
			//Column Create                              
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'LineNo');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Option');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('C1', 'LineIdentifier');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('D1', 'GroupageManifestNo');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Consignor');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('F1', 'ConsignorAddress1');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('G1', 'ConsignorAddress2');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('H1', 'ConsignorPostCode');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('I1', 'ConsigneeName');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('J1', 'ConsigneeAddress1');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('K1', 'ConsigneeAddress2');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('L1', 'ConsigneeGSTNumber');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('M1', 'ConsigneePostCode');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('N1', 'ConsigneeCountry');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('O1', 'NoOfUnits');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('P1', 'GrossMass');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Description');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('R1', 'Value');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('S1', 'ValueCurr');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('T1', 'ForwardingAgent');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('U1', 'ForwardingAgentAddress1');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('V1', 'ForwardingAgentAddress2');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('W1', 'ForwardingAgentCountry');                                                                  
			$objPHPExcel->getActiveSheet()->setCellValue('X1', 'CommunityStatus');                                                                  
			
			
			$inc = 1;$cnt = 2;$e = 0;
			foreach( $manifest as $manifestIndex => $manifestValue )
			{
 			
			  			
			$e++;	
			}
			
			//Set First Row  for Amazon FBa Sheet
			$objPHPExcel->setActiveSheetIndex(0);                                                                              
			$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->getAlignment()->applyFromArray(
			array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));                   
			$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet(0)->getStyle("A1:D1")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet(0)
			->getStyle('A1:D1')
			->applyFromArray(
				array(
					'fill' => array(
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => 'EBE5DB')
					)
				)
			);
			
		                                                                                           
			$dir = new Folder(WWW_ROOT .'logs/cut_off/'.$folderName, true, 0755);
			
			$uploadUrl = WWW_ROOT .'logs/cut_off/'. $folderName . '/' .$service.'.csv';                                          
			$uploadUrI = Router::url('/', true) . 'img/logs/'. $folderName . '/' .$service.'.csv';                                          
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
			$objWriter->save($uploadUrl);
 			 
			
			//Changes for Royalmail
			App::Import('Controller', 'RoyalMail'); 
			$royal = new RoyalMailController;
			//$royal->createManifest();
			 			
			//echo $uploadUrI;
			 exit;
		}
		else
		{
			echo "blank"; 
			exit;
		}
	}	
    
}

?>
