<?php
error_reporting(0);
class FbaBoxingController extends AppController
{
    
    var $name = "FbaBoxing";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('printTest'));
			//'RequestReport','GetReportRequestList','GetReport'
    }
 
   public function index($shipment_inc_id = null) 
    {	 
	 
		$this->layout = "fba"; 	
		$this->set( "title","FBA Shipments" );	
		$this->loadModel('FbaShipmentItemsPo');
		  
		$shipment_po_items = $this->FbaShipmentItemsPo->find('first', array('conditions'=>array('shipment_inc_id'=>$shipment_inc_id),'order'=>'id ASC'));
		$this->set( 'shipment_po_items', $shipment_po_items );	
		 
	} 
	
	public function RemoveDocument($id = null, $field = null ) 
    { 
		if($id > 0 && $field != ''){
			 $this->loadModel('FbaShipmentPacking');	
			
			$pos = $this->FbaShipmentPacking->find("first", array('conditions' => array('id' => $id)));
			 
			if(count($pos) > 0){
				$podata['update_username'] = $this->Session->read('Auth.User.username'); 
				$podata['id'] 			   = $pos['FbaShipmentPacking']['id'];
				$podata[$field] 		   = '';
				
				$document = WWW_ROOT. 'fba_shipments'.DS.'documents'.DS.$pos['FbaShipmentPacking'][$field] ; 
				if(file_exists($document)){
					@unlink($document);
				}
				$this->FbaShipmentPacking->saveAll( $podata );
				$this->Session->setFlash('Document file is removed.', 'flash_success');
			}else{
				$this->Session->setFlash('Document file is not found.', 'flash_danger');
			}
		}else{
				$this->Session->setFlash('Invalid data.', 'flash_danger');
		}	
		 
		$this->redirect($this->referer());
		 		
	} 
	
	public function getShipmentPacking( $shipment_inc_id = null){
	
		$this->loadModel('FbaShipmentPacking');	  
		$this->loadModel('FbaShipmentBox');	
		
		$ship_pack = array();
		
		if(isset($shipment_inc_id)){		 
 			$ship_pack = $this->FbaShipmentPacking->find("first", array('conditions' => array('shipment_inc_id' => $shipment_inc_id)));
		}
		return $ship_pack;
			
	}
	
	public function SaveShipmentPacking(){
	 
		$this->loadModel('FbaShipmentPacking');	  
		$this->loadModel('FbaShipmentBox');	
		
		if(isset($this->request->data['shipment_inc_id']) && isset($this->request->data['mark_status'])){
			$this->loadModel('FbaShipment');	
			$ship['id'] 	= $this->request->data['shipment_inc_id'];
			$ship['status'] = 'exported';
			$this->FbaShipment->saveAll( $ship );
		 }
		 
		if(isset($this->request->data['shipment_inc_id'])){
		
 			$filename_1 = $filename_2 = '';
			$upload_dir = WWW_ROOT. 'fba_shipments'.DS.'documents'.DS ; 
			if(isset($this->data['Shipment']['Documents1']['tmp_name']) && $this->data['Shipment']['Documents1']['tmp_name']!=''){
				$filetemppath =	$this->data['Shipment']['Documents1']['tmp_name'];
				$pathinfo     = pathinfo($this->data['Shipment']['Documents1']['name']);
				$fileNameWithoutExt = $pathinfo['filename'];
				$fileExtension = strtolower($pathinfo['extension']);
				$filename_1 = $fileNameWithoutExt.'_'.date('ymd_his').'_'.$this->Session->read('Auth.User.username').".".$fileExtension;	
				move_uploaded_file($filetemppath,$upload_dir.$filename_1); 				
			}
			if(isset($this->request->data['Shipment']['Documents2']['tmp_name']) && $this->request->data['Shipment']['Documents2']['tmp_name']!=''){
				$filetemppath =	$this->request->data['Shipment']['Documents2']['tmp_name'];
				$pathinfo     = pathinfo($this->request->data['Shipment']['Documents2']['name']);
				$fileNameWithoutExt = $pathinfo['filename'];
				$fileExtension = strtolower($pathinfo['extension']);				
				$filename_2 = $fileNameWithoutExt.'_'.date('ymd_his').'_'.$this->Session->read('Auth.User.username').".".$fileExtension;		
				move_uploaded_file($filetemppath,$upload_dir.$filename_2); 
				
			}
		  	
			$ship_pack = $this->FbaShipmentPacking->find("first", array('conditions' =>
					array('FbaShipmentPacking.shipment_inc_id' => $this->request->data['shipment_inc_id'])));
				 
			if(count($ship_pack) > 0){
				$data['id']	 = $ship_pack['FbaShipmentPacking']['id'];
				$shipment_packing_inc_id = $ship_pack['FbaShipmentPacking']['id'];
			}else{
				$data['added_date'] 	 = date('Y-m-d H:i:s');
				$shipment_packing_inc_id = 0;
			}
			
			$data['shipment_inc_id'] = $this->request->data['shipment_inc_id'];
			$data['shipping_type']   = $this->request->data['shipping_type'];
			$data['no_boxes'] 		 = $this->request->data['no_boxes'];
			
			$data['no_pallets'] 	 = $this->request->data['no_pallets'];
			if($this->request->data['shipping_type'] == 'spd'){
				$data['no_pallets'] = 0;
			}
			
			$data['box_weight'] 	 = json_encode($this->request->data['box_weight']);
			$data['box_length'] 	 = json_encode($this->request->data['box_length']); 
			$data['box_width'] 		 = json_encode($this->request->data['box_width']); 
			$data['box_height'] 	 = json_encode($this->request->data['box_height']); 
			
			$data['ref']     		 = $this->request->data['ref'];
			$data['courier']    	 = $this->request->data['courier'];
			$data['tracking']   	 = $this->request->data['tracking'];
			$data['contact']   	 	 = $this->request->data['contact']; 
			$data['username'] 		 = $this->Session->read('Auth.User.username'); 	
			if($filename_1 != '')	 { $data['documents1']= $filename_1; }
			if($filename_2 != '')	 { $data['documents2']= $filename_2; } 
				 	
			$this->FbaShipmentPacking->saveAll( $data );
			if($shipment_packing_inc_id == 0){
				$shipment_packing_inc_id = $this->FbaShipmentPacking->getLastInsertId();
			}	
			 
			foreach($this->request->data['box'] as $fnsku => $val){
				$ship_box = $this->FbaShipmentBox->find("first", array('conditions' =>
					array('shipment_packing_inc_id' => $shipment_packing_inc_id,'fnsku' => $fnsku)));
				if(count($ship_box) > 0){
					$sbdata['id']	 = $ship_box['FbaShipmentBox']['id']; 
				}else{
					$sbdata['added_date'] = date('Y-m-d H:i:s'); 
				}
						
				$sbdata['shipment_packing_inc_id']	 = $shipment_packing_inc_id;
			
				$sbdata['fnsku'] = $fnsku;
				$sbdata['boxes'] = json_encode( $val);				
				$this->FbaShipmentBox->saveAll( $sbdata );
			}
			
			$msg['msg'] = 'Shipment Boxes saved.';
			$msg['error']='';
		}else{
			$msg['msg'] = 'Shipment Id is missing.';
			$msg['error']='yes';
		}
		 		
		if(isset($this->request->data['save_all_values'])){
			$this->Session->setFlash($msg['msg'] , 'flash_success');
			$this->redirect($this->referer());
		}else{
			echo json_encode($msg);
			exit;
		}
					
	}
	
	public function shippingBoxes(){
		
		$this->loadModel('FbaShipmentItemsPo');	  
		$this->loadModel('FbaShipmentItem');	  
		$this->loadModel('FbaShipmentPacking');	  
		$this->loadModel('FbaShipmentBox');	
		 
  		 
		$no_boxes =  $this->request->data['no_boxes'];
  		
 		$sp = $this->FbaShipmentPacking->find("first", array('conditions' =>
				array('FbaShipmentPacking.shipment_inc_id' => $this->request->data['shipment_inc_id'])));
			 
		if(count($sp) > 0){
			$data['id']	 = $sp['FbaShipmentPacking']['id'];
		}else{
			$data['added_date'] 	 = date('Y-m-d H:i:s');
		}
		$data['shipment_id']	 = $this->request->data['shipment_id'];
		$data['shipment_inc_id'] = $this->request->data['shipment_inc_id'];
		$data['shipping_type']	 = $this->request->data['shipping_type'];
		$data['no_boxes'] 		 = $this->request->data['no_boxes'];	
		$data['no_pallets'] 	 = $this->request->data['no_pallets'];	
		if($this->request->data['shipping_type'] == 'spd'){
			$data['no_pallets'] = 0;
		}
		
		$data['username'] 		 = $this->Session->read('Auth.User.username'); 			
		$this->FbaShipmentPacking->saveAll( $data );
					
	 	if($this->request->data['shipment_inc_id'] > 0)
		{
			$poItems =  $this->FbaShipmentItemsPo->find('all',array('conditions' =>array('FbaShipmentItemsPo.shipment_inc_id' => $this->request->data['shipment_inc_id'])));
		 
			 $ShipmentItems =  $this->FbaShipmentItem->find('all',array('conditions' =>array('FbaShipmentItem.shipment_inc_id' => $this->request->data['shipment_inc_id'])));
			foreach( $ShipmentItems as $pod){
			
				$titleArray[$pod['FbaShipmentItem']['fnsku']] = array('merchant_sku'=>$pod['FbaShipmentItem']['merchant_sku'], 'title'=>$pod['FbaShipmentItem']['title'] ,'external_id'=>$pod['FbaShipmentItem']['external_id']);
			}
			
			// pr($poItems);
			 
			if(count($poItems) > 0){
			
				$msg['msg'] = '00';
				$msg['error'] = '';
				$div_width = 100 ;
				if($no_boxes > 4){
					$div_width = $no_boxes * 23; 
				}
				//if($this->request->data['shipping_type'] == 'pcs') {
				
				//}else{
				$html = '<div class="row head" style="clear:both;">';
				$html .= '<div class="col-sm-12">';
				$html .= '<div class="col-sm-6">Box Contents</div>';
				$html .= '<div class="col-sm-1"><center>Shipment Qty</center></div>';	
				$html .= '<div class="col-sm-5">';
					$html .= '<div style="width:'.$div_width .'%;">';	
					for($i = 1; $i <= $no_boxes; $i++){
						$html .= '<div class="fl-pad"><small>Box '.$i.' Qty</small></div>';
					}
					$html .= '<div class="fl-pad pl">Boxed Qty</div>';	
					$html .= '</div>';
				$html .= '</div>';	
										
				$html .= '</div>';
				$html .= '</div>';
				
				$packdata = $this->getShipmentPacking($this->request->data['shipment_inc_id']);
				
				foreach($packdata['FbaShipmentBox'] as $val){
				 	$boxes[$val['fnsku']] = json_decode($val['boxes'],true);
				}
				$total_shipped_qty  = 0 ;$total_qty  = 0 ;
				foreach($poItems as $item){
					$total_shipped_qty += $item['FbaShipmentItemsPo']['shipped_qty'];
					$html .= '<div class="row sku">';
					$html .= '<div class="col-sm-12">';
					
					 
					$html .= '<div class="col-sm-6">'.$item['FbaShipmentItemsPo']['merchant_sku'].'<br><small>'.$titleArray[$item['FbaShipmentItemsPo']['fnsku']]['title'].'</small><br>'.$titleArray[$item['FbaShipmentItemsPo']['fnsku']]['external_id'].' fnsku:'.$item['FbaShipmentItemsPo']['fnsku'].'</div>';
					
					
					
					$html .= '<div class="col-sm-1"><center>'.$item['FbaShipmentItemsPo']['shipped_qty'].'</center></div>';	
					 
					$html .= '<div class="col-sm-5">';
						$html .= '<div style="width:'. $div_width .'%;">';	
						$tqty = 0;
						for($i = 1; $i <= $no_boxes; $i++){
							$qty = '';
							if(isset($boxes[$item['FbaShipmentItemsPo']['fnsku']][$i])){
								$qty = $boxes[$item['FbaShipmentItemsPo']['fnsku']][$i];
							}
							$tqty += $qty;
							$html .= '<div class="fl-pad"><input type="text" placeholder="box '.$i.'" name="box['.$item['FbaShipmentItemsPo']['fnsku'].']['.$i.']" class="form-control box_qty_'.$item['FbaShipmentItemsPo']['id'].'" onChange="checkBoxes(this);" for="'.$item['FbaShipmentItemsPo']['id'].'" value="'.$qty.'"/></div>';
						}
						$total_qty += $tqty;
						$html .= '<div class="fl-pad pl total_qty" id="boxed_qty_'.$item['FbaShipmentItemsPo']['id'].'">'.$tqty.'</div>';
						$html .= '</div>';
					$html .= '</div>';
					
					$html .= '</div>';							
					$html .= '</div>';
				
				}
				
				$html .= '<div class="row sku bt">';
				$html .= '<div class="col-sm-12">';
				$html .= '<div class="col-sm-6"><strong>'.count($poItems).' MSKUs</strong></div>';
				$html .= '<div class="col-sm-1"><center><strong>Total Qty:'.$total_shipped_qty.'</strong></center></div>';	
				$html .= '<div class="col-sm-5">';
					$html .= '<div style="width:'.$div_width .'%;">';	
					for($i = 1; $i <= $no_boxes; $i++){
						$html .= '<div class="fl-pad">&nbsp;</div>';
					}
					$html .= '<div class="fl-pad pl" id="total_qty"><strong>'.$total_qty.'</strong></div>';	
					$html .= '</div>';
				$html .= '</div>';											
				$html .= '</div>';
				$html .= '</div>';
				
				$box_weight = array(); $total_weight = 0;
				if(isset($packdata['FbaShipmentPacking']['box_weight'])){
					$box_weight = json_decode($packdata['FbaShipmentPacking']['box_weight'],true);
				}
				 
				$html .= '<div class="row sku">';
				$html .= '<div class="col-sm-12">';
				$html .= '<div class="col-sm-5 ">&nbsp;</div>';
				$html .= '<div class="col-sm-2 text-right">Box weight(lb):</div>';	
				$html .= '<div class="col-sm-5">';
					$html .= '<div style="width:'.$div_width .'%;">';	
					for($i = 1; $i <= $no_boxes; $i++){
						$html .= '<div class="fl-pad"><input type="text" placeholder="w. b. '.$i.'" name="box_weight['.$i.']" onChange="checkBoxWeight(this);" class="form-control box_weight" value="'.$box_weight[$i].'"/></div>';
						$total_weight += $box_weight[$i];
					}
					$html .= '<div class="fl-pad pl" id="total_weight">'.$total_weight.'</div>';	
					$html .= '</div>';
				$html .= '</div>';	
										
				$html .= '</div>';
				$html .= '</div>';
				
				$box_length = array();  $total_length = 0;
				if(isset($packdata['FbaShipmentPacking']['box_length'])){
					$box_length = json_decode($packdata['FbaShipmentPacking']['box_length'],true);
				}
				$html .= '<div class="row sku">';
				$html .= '<div class="col-sm-12">';
				$html .= '<div class="col-sm-3">&nbsp;</div>';
				$html .= '<div class="col-sm-4 text-right">Box Length(IN):</div>';	
				
				$html .= '<div class="col-sm-5">';
					$html .= '<div style="width:'.$div_width .'%;">';	
					for($i = 1; $i <= $no_boxes; $i++){
						$html .= '<div class="fl-pad"><input type="text" placeholder="L" name="box_length['.$i.']" id="box_length_'.$i.'" value="'.$box_length[$i].'" class="dim form-control"/></div>';
					$total_length += $box_length[$i];
					}
					$html .= '<div class="fl-pad pl" id="total_length">'.$total_length.'</div>';	
					$html .= '</div>';
				$html .= '</div>';	
										
				$html .= '</div>';
				$html .= '</div>';
				
				$box_width = array(); $total_width  = 0;
				if(isset($packdata['FbaShipmentPacking']['box_width'])){
					$box_width = json_decode($packdata['FbaShipmentPacking']['box_width'],true);
				}
				$html .= '<div class="row sku">';
				$html .= '<div class="col-sm-12">';
				$html .= '<div class="col-sm-3">&nbsp;</div>';
				$html .= '<div class="col-sm-4 text-right">Box Width(IN):</div>';	
				
				$html .= '<div class="col-sm-5">';
					$html .= '<div style="width:'.$div_width .'%;">';	
					for($i = 1; $i <= $no_boxes; $i++){
						$html .= '<div class="fl-pad"><input type="text" placeholder="W" name="box_width['.$i.']" id="box_width_'.$i.'" value="'.$box_width[$i].'" class="dim form-control"/> </div>';
						$total_width += $box_width[$i];
					}
					$html .= '<div class="fl-pad pl" id="total_width">'.$total_width.'</div>';	
					$html .= '</div>';
				$html .= '</div>';	
										
				$html .= '</div>';
				$html .= '</div>';
				
				$box_height = array(); $total_height = 0;
				if(isset($packdata['FbaShipmentPacking']['box_height'])){
					$box_height = json_decode($packdata['FbaShipmentPacking']['box_height'],true);
				}
				$html .= '<div class="row sku">';
				$html .= '<div class="col-sm-12">';
				$html .= '<div class="col-sm-3">&nbsp;</div>';
				$html .= '<div class="col-sm-4 text-right">Box Height(IN):</div>';	
				
				$html .= '<div class="col-sm-5">';
					$html .= '<div style="width:'.$div_width .'%;">';	
					for($i = 1; $i <= $no_boxes; $i++){
						$html .= '<div class="fl-pad"><input type="text" placeholder="H" name="box_height['.$i.']" id="box_height'.$i.'" value="'.$box_height[$i].'" class="dim form-control"/></div>';
						$total_height += $box_height[$i];
					}
					$html .= '<div class="fl-pad pl" id="total_height">'.$total_height.'</div>';	
					$html .= '</div>';
				$html .= '</div>';	
										
				$html .= '</div>';
				$html .= '</div>';
				//}
				
			}
		}

		$msg['data']	=	$html;
		if(isset($this->request->data['save_all_values'])){
			$this->redirect($this->referer());
		}else{
			echo json_encode($msg);
			exit;
		}
	}
	
	public function checkStatus($shipment_inc_id = null){
		
		$this->loadModel('FbaShipment');	
		$get_status = $this->FbaShipment->find('first', array('conditions' => array('id' => $shipment_inc_id) ) );
		return $get_status['FbaShipment']['status'];
			 
	 }
	
	
}