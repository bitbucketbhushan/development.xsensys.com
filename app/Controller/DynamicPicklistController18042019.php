<?php
error_reporting(1);
class DynamicPicklistController extends AppController
{

    var $name = "DynamicPicklist";    
    var $components = array('Session','Upload','Common','Auth','Paginator');
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
    var $virtualFields;
   
    public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->Auth->Allow(array('cancelOrder'));
	   $this->GlobalBarcode = $this->Components->load('Common'); /*Apply code for Global Barcode*/
	}
 	
	public function index()
    {		 
		$this->layout = 'index';
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'DynamicBatche' );
		$this->loadModel( 'User' );   
		$batch = null;
		if(isset($this->request->pass[0])){ 
			$batch = $this->request->pass[0]; 
 			$b = $this->DynamicBatche->find('count', array('conditions' => array('batch_name' =>$batch)));
 			if($b == 0){
				$this->Session->setflash( $batch .' batch number is not correct!', 'flash_danger'); 
				$this->redirect(array('controller'=>'DynamicPicklist','action' => 'batch'));
			}
  		}else{
			$this->Session->setflash( 'Batch number is missing!', 'flash_danger'); 
			//$this->redirect($this->referer());
			$this->redirect(array('controller'=>'DynamicPicklist','action' => 'batch'));
		}
		
		$perPage	=	 50;
		$this->paginate = array('conditions' => array('batch' =>$batch),'order'=>'id DESC','limit' => $perPage);
		$DynamicPicklist = $this->paginate('DynamicPicklist');
		
		$users =	$this->User->find('all', array('conditions' => array('country' =>1),'fields' => array('id','first_name','last_name','username','email'),'group' => 'email'));	
 				
 		$this->set( compact('sections','users') );
		$this->set( 'DynamicPicklist',$DynamicPicklist); //pr( $data);
		
 	}
	 
	
    public function assignUser()
    { 
		$this->layout = "";	 
		$this->loadModel( 'User' );
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'MergeUpdate' );
		
		$this->DynamicPicklist->updateAll( array('assign_user_email' => "'".$this->request->data['assign_user']."'",
		'assign_by_user' => "'".$this->Session->read('Auth.User.email')."'",'assign_time' => "'".date('Y-m-d H:i:s')."'"),array('id' => $this->request->data['picklist_inc_id']));	
		
  		$user =	$this->User->find('first', array('conditions' => array('email' =>$this->request->data['assign_user']),'fields' => array('id','first_name','last_name'),'group' => 'email'));
		  $d['user'] = $user['User']['first_name'].' '.$user['User']['last_name'];
  		  
		  $this->MergeUpdate->updateAll(array('MergeUpdate.user_name' => $user['User']['id'],'MergeUpdate.assign_user' => "'".$d['user']."'"), array('MergeUpdate.picklist_inc_id' => $this->request->data['picklist_inc_id'],'MergeUpdate.status' => '0'));
			
  		/*----------Maintain  Logs---------------------*/
		$log_file = WWW_ROOT .'logs/dynamic_log/assign_user_'.date('dmy').".log";
		
		if(!file_exists($log_file)){
			file_put_contents($log_file, 'AssignByUserEmail'."\t".'Username'."\t".'PcName'."\t".'AssignToUser'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
		}
 		file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$this->request->data['assign_user']."\t".$this->request->data['picklist_inc_id']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
		/*--------End Of Logs-------------*/
		
		echo json_encode($d);
		exit;
	}
	
    public function getDeleveryCountry()
    { 
		$this->layout = "";	 
		$this->loadModel( 'Country' );	
		$this->loadModel( 'MergeUpdate' );		
		
		$country 	= array();
		$countries	= array();
		$countries_grp= array();
		$services   = array();
			//$order_ids  = $this->getSectionLocations($this->request->data['sections']);
		
 			$Where[] = "  status = 0 AND pick_list_status = 0 ";
			//$Where[] = "  order_id IN (".implode(",", $order_ids).")";
			
			if(isset($this->request->data['batch'])){
				$Where[] = " created_batch = '".$this->request->data['batch']."'";
			}
			if($this->request->data['sku_type'] == 'single'){
				$Where[] = " sku NOT LIKE '%,%' AND service_name != 'Over Weight' AND service_name != '' ";
			}else if($this->request->data['sku_type'] == 'multiple'){						
				$Where[] = " sku LIKE '%,%'  AND service_name != 'Over Weight' AND service_name != '' ";
			} 		
						
			if(count($Where) > 0){				  
			
				$sql_query = ''; $where_col ='';
				if(count($Where)>0){
					$w = 1;
					$count_lenght_of_where = count($Where);
					foreach ($Where as $k) {
						$where_col .= $k;
						if ($w < $count_lenght_of_where) {
							$where_col .=' AND ';
						}
						$w++;
					 }
					$sql_query =' WHERE '.$where_col;			
				}	
			}
				//echo "SELECT order_id FROM merge_updates as MergeUpdate $sql_query";
				//exit;
				
			$country = $this->MergeUpdate->query("SELECT delevery_country FROM merge_updates as MergeUpdate $sql_query GROUP BY delevery_country");
			 
			if(count($country) >0){
				foreach($country  as $v){
					if($v['MergeUpdate']['delevery_country'] != 'null'){
						$countries[] = trim($v['MergeUpdate']['delevery_country']);
					}
				}
			} 
			$country_grp = $this->Country->query("SELECT country_group, custom_name,iso_2 FROM countries as Country WHERE name IN('".implode("','",$countries)."') GROUP BY country_group");
			
			if(count($country_grp) >0){
				foreach($country_grp  as $v){
				
					if($v['Country']['country_group'] == $v['Country']['iso_2']){
						$countries_grp[] = $v['Country']['custom_name'] ;
					}else{
						$countries_grp[] = ucwords($v['Country']['country_group']) ;
					}
				}
			}
		 
		 
  		echo json_encode($countries_grp);
		exit;		  
 	}
	
	public function getPoatalServices()
    { 
		$this->layout = "";	 
		$this->loadModel( 'MergeUpdate' );	
		$this->loadModel( 'Country' );	
		
		$orderItems = array();
		$services   = array();
		
		
		if(isset($this->request->data['postal_country']) && $this->request->data['postal_country'] != ''){
 		 
			$country_group = strtolower($this->request->data['postal_country']);		
		 	//$country_group = str_replace(" ", "_", $country_group) ;
			
			$country_grp = $this->Country->query("SELECT custom_name, iso_2 FROM countries as Country WHERE country_group = '".$country_group."' ");
			 
			if(count($country_grp) >0){
				foreach($country_grp  as $v){
					//$string = preg_replace('/[^A-Za-z0-9\-]/', '', $v['Country']['custom_name']); 
 					$countries_grp[] = $v['Country']['custom_name'];					 
				}
				
				$Where[] = " delevery_country IN ('".implode("','",$countries_grp)."')";
			}
		
		}
		// pr($Where);
			
		//$order_ids  = $this->getSectionLocations($this->request->data['sections']);
  		
		
			
			$Where[] = "  status = 0 AND pick_list_status = 0 AND service_name != 'Over Weight' AND service_name != '' "; 
			//$Where[] = "  order_id IN (".implode(",", $order_ids).") ";
			
			if(isset($this->request->data['batch'])){
				$Where[] = " created_batch = '".$this->request->data['batch']."'";
			}
			if($this->request->data['sku_type'] == 'single'){
				$Where[] = " sku NOT LIKE '%,%'";
			}else if($this->request->data['sku_type'] == 'multiple'){						
				$Where[] = " sku LIKE '%,%'";
			}
			 
			/*if(isset($this->request->data['postal_countries']) && count($this->request->data['postal_countries']) > 0){	
				$Where[] = " delevery_country IN('" . implode("','",$this->request->data['postal_countries']) . "') ";
			}*/	
				
						
			if(count($Where) > 0){				  
			
				$sql_query = ''; $where_col ='';
				if(count($Where)>0){
					$w = 1;
					$count_lenght_of_where = count($Where);
					foreach ($Where as $k) {
						$where_col .= $k;
						if ($w < $count_lenght_of_where) {
							$where_col .=' AND ';
						}
						$w++;
					 }
					$sql_query =' WHERE '.$where_col;			
				}	
			}
			
		 	 $sql = "SELECT order_id, service_name FROM merge_updates as MergeUpdate $sql_query GROUP BY service_name";
			
			$log_file = WWW_ROOT .'logs/dynamic_log/get_poatal_sql_'.date('dmy').".log";	
			file_put_contents($log_file, $sql);	
			
 				//exit;
			$orderItems = $this->MergeUpdate->query($sql);
			 
			if(count($orderItems) >0){
				foreach($orderItems as $v){
					if($v['MergeUpdate']['service_name']){
						$services[] = $v['MergeUpdate']['service_name'];
					}
				}
			}			
		
		echo json_encode($services);
		exit;
		  
 	}
	
	public function getBinLocations()
    { 
		$this->layout = "";	 
		$this->loadModel( 'MergeUpdate' );		
		$this->loadModel( 'OrderLocation' );	
		$this->loadModel( 'Country' );
		$skus = array();
 		
		$Where[] = "  status = 0 AND pick_list_status = 0 AND service_name != 'Over Weight' ";
		
		if(isset($this->request->data['batch'])){
			$Where[] = " created_batch = '".$this->request->data['batch']."'";
		}
			
		if($this->request->data['sku_type'] == 'single'){
			$Where[] = " sku NOT LIKE '%,%'";
		}else if($this->request->data['sku_type'] == 'multiple'){						
			$Where[] = " sku LIKE '%,%'";
		}
		 
		/*if(isset($this->request->data['postal_country']) && $this->request->data['postal_country'] != ''){
			$Where[] = " delevery_country LIKE '".$this->request->data['postal_country']."'";
		}*/
		if(isset($this->request->data['postal_country']) && $this->request->data['postal_country'] != ''){
 		 
			$country_group = strtolower($this->request->data['postal_country']);		
			//$country_group = str_replace(" ", "_", $country_group) ;
			
			$country_grp = $this->Country->query("SELECT custom_name, iso_2 FROM countries as Country WHERE country_group = '".$country_group."' ");
				
			if(count($country_grp) >0){
				foreach($country_grp  as $v){
					$countries_grp[] = $v['Country']['custom_name'];					 
				}
				
				$Where[] = " delevery_country IN ('".implode("','",$countries_grp)."')";
			}
		
		}
 			
		if(isset($this->request->data['postal_service']) && $this->request->data['postal_service'] !=''){
			$Where[] = " service_name LIKE '".$this->request->data['postal_service']."'";
		}
					
		$sql_query = '';
					
		if(count($Where) > 0){				  
		
			 $where_col ='';
			if(count($Where)>0){
				$w = 1;
				$count_lenght_of_where = count($Where);
				foreach ($Where as $k) {
					$where_col .= $k;
					if ($w < $count_lenght_of_where) {
						$where_col .=' AND ';
					}
					$w++;
				 }
				$sql_query =' WHERE '.$where_col;			
			}	
		}
		$sql= "SELECT sku,barcode,order_id,product_order_id_identify FROM merge_updates as MergeUpdate $sql_query";
			//exit;
		 
		$orderItems = $this->MergeUpdate->query($sql);
		$tskus = [];
		$binlocations = [];	
		$oids = [];
		$log_file = WWW_ROOT .'logs/dynamic_log/t_'.$this->request->data['postal_country'].'_'.$this->request->data['postal_service'].'_'.date('dmy').".log";	
		if(count($orderItems) > 0){
			foreach($orderItems  as $v){
 						 
				$items = $this->MergeUpdate->query("SELECT sku,bin_location, quantity,order_id FROM order_locations as OrderLocation WHERE order_id ='".$v['MergeUpdate']['order_id']."' AND split_order_id ='".$v['MergeUpdate']['product_order_id_identify']."' AND status = 'active' AND pick_list = '0'");
											
				if(count($items) > 0){
					foreach($items as $m){ 
						$binlocations[$m['OrderLocation']['bin_location']][] = $m['OrderLocation']['quantity'];
						//$oids[] = array($v['MergeUpdate']['product_order_id_identify'],$m['OrderLocation']['bin_location'],$m['OrderLocation']['quantity']);
						
						// echo 'order id:'.$m['OrderLocation']['order_id'].' sku:'. $m['OrderLocation']['sku'].' Q:'.$m['OrderLocation']['quantity'] .' bin_location ' .$m['OrderLocation']['bin_location'];
				//echo "<br>";
				 
						file_put_contents($log_file, "S"."\t".$v['MergeUpdate']['product_order_id_identify']."\t".$m['OrderLocation']['bin_location']."\t".$m['OrderLocation']['quantity']."\n", FILE_APPEND|LOCK_EX);	
				
					}
				}			 
				 
			}
		}  
		//pr($oids);
		$loc = array();
		//$binlocations = $this->msortnew($binlocations);	
		$keys = array_keys($binlocations);
		natsort($keys); 	
		foreach($keys as $k){
			$loc[] = $k .' ['.array_sum($binlocations[$k]).']';
		}
		
  		echo json_encode($loc);
		exit;
		  
 	}
	
	
	public function getSkuList()
    { 
		$this->layout = "";	 
		$this->loadModel( 'MergeUpdate' );	
		$this->loadModel( 'OrderLocation' );	
		$this->loadModel( 'Country' );
			
		$skus = array();
		$binlocations = array();
		$locs = array();
		
		if(isset($this->request->data['bin_locations']) && count($this->request->data['bin_locations'])){
			foreach($this->request->data['bin_locations'] as $l){
				$bin = trim(explode("[",$l)[0]);
				$f = substr($bin,0,2);
				$floor[$f] = $f ;
				$locs[] = $bin;			
			}		
		}
		 
		
		if(	count($locs) > 0){
  	
			$Where[] = "  status = 0 AND pick_list_status = 0 AND service_name != 'Over Weight' ";
			
			if(isset($this->request->data['batch'])){
				$Where[] = " created_batch = '".$this->request->data['batch']."'";
			}
			 
			if($this->request->data['sku_type'] == 'single'){
				$Where[] = " sku NOT LIKE '%,%'";
			}else if($this->request->data['sku_type'] == 'multiple'){						
				$Where[] = " sku LIKE '%,%'";
			}
			 
			/*if(isset($this->request->data['postal_country']) && $this->request->data['postal_country'] != ''){
				$Where[] = " delevery_country LIKE '".$this->request->data['postal_country']."'";
			}*/
			if(isset($this->request->data['postal_country']) && $this->request->data['postal_country'] != ''){
 		 
				$country_group = strtolower($this->request->data['postal_country']);		
				//$country_group = str_replace(" ", "_", $country_group) ;
				
				$country_grp = $this->Country->query("SELECT custom_name, iso_2 FROM countries as Country WHERE country_group = '".$country_group."' ");
					
				if(count($country_grp) >0){
					foreach($country_grp  as $v){
						$countries_grp[] = $v['Country']['custom_name'];					 
					}
					
					$Where[] = " delevery_country IN ('".implode("','",$countries_grp)."')";
				}
		
			}
			if(isset($this->request->data['postal_service']) && $this->request->data['postal_service'] !=''){
				$Where[] = " service_name LIKE '".$this->request->data['postal_service']."'";
			}			
						
			if(count($Where) > 0){				  
			
				$sql_query = ''; $where_col ='';
				if(count($Where)>0){
					$w = 1;
					$count_lenght_of_where = count($Where);
					foreach ($Where as $k) {
						$where_col .= $k;
						if ($w < $count_lenght_of_where) {
							$where_col .=' AND ';
						}
						$w++;
					 }
					$sql_query =' WHERE '.$where_col;			
				}	
			}
			$sql= "SELECT order_id,product_order_id_identify FROM merge_updates as MergeUpdate $sql_query";
				//exit;
				 
			$orderItems = $this->MergeUpdate->query($sql);
			$tskus = [];	$split_order_id = []; 
			if(count($orderItems) >0){
				foreach($orderItems  as $v){
					$order_id[] = $v['MergeUpdate']['order_id'];
					$split_order_id[] = $v['MergeUpdate']['product_order_id_identify'];
				}
			} 
			
			$ssku['error'] = ''; 
			$items = array();
			//pr($$split_order_id); pr($locs);
			 //echo "SELECT sku, quantity FROM order_locations as OrderLocation WHERE order_id IN(".implode(",",$order_id).") AND  bin_location IN ('".implode("','",$locs)."') AND status = 'active' AND pick_list = '0' ";
	 
			$items = $this->OrderLocation->query("SELECT order_id,sku, quantity FROM order_locations as OrderLocation WHERE split_order_id IN('".implode("','",$split_order_id)."') AND  bin_location IN ('".implode("','",$locs)."') AND status = 'active' AND pick_list = '0' ");
			 
 			if(count($items) > 0){
				foreach($items as $m){ 										
					$binlocations[$m['OrderLocation']['sku']][] = $m['OrderLocation']['quantity'];
					//echo 'order id:'.$m['OrderLocation']['order_id'].' sku:'. $m['OrderLocation']['sku'].' Q:'.$m['OrderLocation']['quantity'];
					//echo "<br>";
				}
 				if(count($binlocations) > 0){
					foreach(array_keys($binlocations) as $k){
						$ssku['data'][] = $k .'('.array_sum($binlocations[$k]).')';
					}
				}
			}else{
				$ssku['error'] =  'Sku not found.';
			} 
		}
		else{			 
			$ssku['error'] =  'yes';
		}
		 
  		echo json_encode($ssku);
		exit;
		  
 	} 
	
	public function getBatchOrderStatus($batch_num = 0)
    {
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );		
	  
		$getItems = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.created_batch' => $batch_num),'fields' => array('status','product_order_id_identify','label_status','scan_date','picklist_inc_id','pick_list_status'),'order'=>'status' ) );
 		return $getItems ;
	}
	
	public function getDynamicPicklistUser($inc_id = 0)
    {
		$this->layout = '';
		$this->loadModel( 'DynamicPicklist' );		
	  
		$getItems = $this->DynamicPicklist->find('first', array('conditions' => array('id' => $inc_id),'fields' => array('assign_user_email','assign_time') ) );
 		return $getItems ;
	}
	
	public function getOrderStatus($picklist_inc_id = 0)
    {
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );		
	 
		$getItems = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.picklist_inc_id' => $picklist_inc_id,'status NOT IN '=>array(0,1) ),'fields' => array('status','product_order_id_identify','label_status'),'order'=>'status' ) );
 		return $getItems ;
	}
	
	public function getAllOrderStatus($picklist_inc_id = 0)
    {
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );		
	 
		$getItems = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.picklist_inc_id' => $picklist_inc_id),'fields' => array('status','product_order_id_identify','label_status','scan_date','service_name','provider_ref_code','service_provider','brt','delevery_country','postal_service'),'order'=>'status' ) );
 		return $getItems ;
	}
	
	public function getProcessedOrders($picklist_inc_id = 0)
    {
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );		
	 
		$getItems = $this->MergeUpdate->find('count', array('conditions' => array('MergeUpdate.picklist_inc_id' => $picklist_inc_id,'process_date !='=>'' ) ) );
 		return $getItems ;
	}
	public function getSortedOrders($picklist_inc_id = 0)
    {
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );		
	 
		$getItems = $this->MergeUpdate->find('count', array('conditions' => array('MergeUpdate.picklist_inc_id' => $picklist_inc_id,'scan_date !='=>'' ) ) );
 		return $getItems ;
	}
	
	public function getPicklistOrders($picklist_inc_id = 0)
    { 
		$this->layout = 'index';
		$this->loadModel( 'MergeUpdate' );		
		$this->loadModel( 'OrderLocation' );
		
		$perPage = 50;
		$conditions = array('picklist_inc_id' =>$picklist_inc_id);
		if(isset($_GET['searchkey'])){
			$conditions = array('picklist_inc_id' =>$picklist_inc_id,
			'OR' => array('product_order_id_identify LIKE' =>trim($_GET['searchkey']).'%','sku LIKE' =>'%'.trim($_GET['searchkey']).'%')			
			);
		}
		
		
		$this->paginate = array('conditions' => $conditions,
		'fields' => array('id','order_date','source_coming',
							'scan_status','sorted_scanned','status','label_status',
							'manifest_status','order_id','product_order_id_identify',
							'sku','quantity','price','service_name','provider_ref_code','service_provider','delevery_country','brt','postal_service',
							'track_id','user_id','picked_date','process_date','scan_date','manifest_date',
							'user_name','pc_name','scanned_user','scanned_pc','cancelled_user','pc_name','over_sell_user'),
		'order' => 'label_status',
		'limit' => $perPage);
		$MergeUpdate = $this->paginate('MergeUpdate');
		
		foreach($MergeUpdate as $v){
			$split_orders[] = $v['MergeUpdate']['product_order_id_identify'];
		}
 		$over_sell_orders = []; 
		$getItems = $this->OrderLocation->find('all', array('conditions' => array('split_order_id' => $split_orders,'available_qty_bin' => 0,'status'=>'active'),'fields'=>['split_order_id'] ) );
		if(count($getItems) > 0){
			foreach($getItems as $p){
				$over_sell_orders[] = $p['OrderLocation']['split_order_id'];
			} 
		}
		  		 
		//$this->set( 'MergeUpdate',$MergeUpdate);
		$this->set( compact('MergeUpdate','over_sell_orders')); 
		//exit;
		  
 	}
	
	public function searchOrder()
    { 
		$this->layout = 'index';
		$this->loadModel( 'MergeUpdate' );	 
		 
 		$get_item = $this->MergeUpdate->find('first', array('conditions' => array('product_order_id_identify LIKE' => trim($_GET['searchkey']).'%'),
		'fields' => ['order_id','product_order_id_identify','created_batch','picklist_inc_id','over_sell_user']) );
			
 		$url =  Router::url('/', true). "DynamicPicklist/getPicklistOrders/" . $get_item['MergeUpdate']['picklist_inc_id']."?searchkey=" . $get_item['MergeUpdate']['order_id']."&batch=". $get_item['MergeUpdate']['created_batch']."&p=b";
		 
 		header("location:$url");
  		exit;		  
 	}
	
	public function getOrderCurrency($order_id = 0)
    {
		$this->layout = '';
		$this->loadModel( 'OpenOrder' );		
		$currency = '';
		$getItems = $this->OpenOrder->find('first', array('conditions' => array('num_order_id' => $order_id),'fields'=>['totals_info'] ) );
		if(count($getItems) > 0){
			$data = unserialize($getItems['OpenOrder']['totals_info']) ;
			 
			$currency = $data->Currency;
		}
		
 		return $currency;
	}
	
	public function markCompleted()
    {
  		$this->layout = "";	 
	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel('OrderLocation');
		$locked = [];
		$canceled = [];
		$open = 0;
		$processed = 0;
		$scan_pending = 0;	
		$over_sell[] = '121278-1';
			
		$location_orders =	$this->OrderLocation->find('all', array('fields'=>['split_order_id'], 'conditions'  => array( 'picklist_inc_id' => $this->request->data['picklist_inc_id'], 'status' => 'active','pick_list' => 1,'available_qty_bin' => 0 ) ) ); 
		
		if(count($location_orders) > 0 ){
			foreach($location_orders as $l){
				$over_sell[] = $l['OrderLocation']['split_order_id'];
			}			
		}
									
		$getItems = $this->MergeUpdate->find('all', array('conditions' => array('picklist_inc_id' => $this->request->data['picklist_inc_id']),'fields'=>['scan_status','sorted_scanned','manifest_status','status','label_status','product_order_id_identify','over_sell_user']) );
		
		foreach($getItems as $s){  
		
			if($s['MergeUpdate']['status'] == 3){
				$locked[] = $s['MergeUpdate']['product_order_id_identify'];
			}else if($s['MergeUpdate']['status'] == 2){
				$canceled[] = $s['MergeUpdate']['product_order_id_identify'];
			}else if($s['MergeUpdate']['status'] == 1){
				$processed++;
			}else if($s['MergeUpdate']['status'] == 0 && $s['MergeUpdate']['over_sell_user'] == '' && !in_array($s['MergeUpdate']['product_order_id_identify'],$over_sell)){
				$open++;
			}else if($s['MergeUpdate']['over_sell_user'] != '' || in_array($s['MergeUpdate']['product_order_id_identify'],$over_sell)){
				$over_sell_count++;
			}
			
			if($s['MergeUpdate']['scan_status'] == 0 && $s['MergeUpdate']['status'] < 2 && $s['MergeUpdate']['over_sell_user'] == '' && !in_array($s['MergeUpdate']['product_order_id_identify'],$over_sell)){
				$scan_pending++;
			}
		}
		
		if($open > 0){
 			echo json_encode(array('status'=>'incomplete','open_orders'=>$open,'processed'=>$processed,'locked'=>$locked,'canceled'=>$canceled,'scan_pending'=>$scan_pending,'over_sell'=>$over_sell_count));
 		}else if($scan_pending > 0){
 			echo json_encode(array('status'=>'scan_pending','open_orders'=>$open,'processed'=>$processed,'locked'=>$locked,'canceled'=>$canceled,'scan_pending'=>$scan_pending,'over_sell'=>$over_sell_count));
 		}else{ 
  		
			 $this->DynamicPicklist->updateAll( array('completed_mark_user' => "'".$this->Session->read('Auth.User.email')."'",'completed_date' => "'".date('Y-m-d H:i:s')."'"),array('id' => $this->request->data['picklist_inc_id']));	
			
			/*----------Maintain  Logs---------------------*/
			$log_file = WWW_ROOT .'logs/dynamic_log/mark_completed_'.date('dmy').".log";
			
			if(!file_exists($log_file)){
				file_put_contents($log_file, 'MarkCompletedByEmail'."\t".'Username'."\t".'PcName'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
			}
			 file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$this->request->data['picklist_inc_id']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
			
			/*--------End Of Logs-------------*/
			 
			echo json_encode(array('status'=>'complete','over_sell'=>$over_sell_count));
			
		}
		exit;
	}
	
	public function getSectionLocations($section = null)
    {
		$this->loadModel( 'SectionsRack' );
		$this->loadModel( 'OrderLocation' );  
		
		$order_ids = array(); 
		$locations = array();
		  
		
		if(isset($section) && count($section) > 0 ){
			
			$location = $this->SectionsRack->query("SELECT location FROM sections_racks as SectionsRack WHERE section_name IN('" . implode("','",$section) . "')");
			
			if(count($location) > 0){ 
			 
				foreach($location as $v){
					$locations[] = $v['SectionsRack']['location'];
				}
				 
				$order_loc = $this->OrderLocation->find('all', array('fields'=>array('order_id'),'conditions' => array('pick_list' => 0,'status'=>'active','bin_location IN' =>$locations)));
				
				foreach($order_loc as $v){
					$order_ids[] = $v['OrderLocation']['order_id'];
				}
			}
		}
			 
		return $order_ids;	
			
	}
	
	public function getInkSkus()
    {
		$this->loadModel( 'InkSku');
 		 
		$ink_data = [];		
		$InkSku = $this->InkSku->find('all');
		foreach( $InkSku as $Ink ){
			$ink_data[]	= $Ink['InkSku']['sku'];
		}
		return $ink_data;
	}
	
	public function picklistPreview()
    {
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'SectionsRack' );
		$this->loadModel( 'OrderLocation' );	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'Product' );		 
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'Country' );
		$this->loadModel( 'InkSku');
		
		$i_sku = 0;
		$ink_data = [];		
		$InkSku = $this->InkSku->find('all');
		foreach( $InkSku as $Ink ){
			$ink_data[]	= $Ink['InkSku']['sku'];
		}
		
  		$order_ids  =  array();
  		$Where      =  array();	
		$orderItems =  array();	 
		$locs =  array();	 
		$floor =  array();	
		$created_batch = ''; 
		$result_data['error'] = '';
		 
		$co = date('s');
	
		//$Where[] = " service_name != 'Over Weight' ";
	
		if(isset($this->request->data['sku_type']) && $this->request->data['sku_type']!=''){
		
		$Where[] = "  status = 0 AND pick_list_status = 0 ";				 
		if(isset($this->request->data['batch'])){
			$Where[] = " created_batch = '".$this->request->data['batch']."'";
			$created_batch = $this->request->data['batch']; 
		}
		/*if(isset($this->request->data['postal_country']) && $this->request->data['postal_country']!= ''){
			$Where[] = " delevery_country LIKE '".$this->request->data['postal_country']."'";	
			$co = strtoupper(substr($this->request->data['postal_country'],0,2));		
		}*/
		
		if(isset($this->request->data['postal_country']) && $this->request->data['postal_country'] != ''){
		
			$country_group = strtolower($this->request->data['postal_country']);		
			//$country_group = str_replace(" ", "_", $country_group) ;
			
			$country_grp = $this->Country->query("SELECT custom_name, iso_2 FROM countries as Country WHERE country_group = '".$country_group."' ");
				
			if(count($country_grp) >0){
				foreach($country_grp  as $v){
					$countries_grp[] = $v['Country']['custom_name'];					 
				}
				
				$Where[] = " delevery_country IN ('".implode("','",$countries_grp)."')";
			}
			$co = strtoupper(substr($this->request->data['postal_country'],0,4));
		}
		
		if($this->request->data['sku_type']=='single'){
			
			$Where[] = " sku NOT LIKE '%,%'";
			
			if(isset($this->request->data['bin_locations']) && count($this->request->data['bin_locations'])>0){
				
 				foreach($this->request->data['bin_locations'] as $l){
					$bin = trim(explode("[",$l)[0]);
					$f = substr($bin,0,2);
					$floor[$f] = $f ;
					$locs[] = $bin;			
				}	
					
		 		//if(count($floor) == 1){
  					if(isset($this->request->data['postal_services']) && $this->request->data['postal_services']!=''){
						$Where[] = " service_name LIKE '".$this->request->data['postal_services']."' AND service_name != 'Over Weight' ";
					}
			  /* }else{
			  	 $result_data['error'] = 'Please select location of same floor(level) G1 or G2 etc.';
			   }*/	
			 
			}
		 }
		 else if($this->request->data['sku_type'] == 'multiple'){
			$Where[] = " sku LIKE '%,%' AND service_name != 'Over Weight' AND service_name != '' ";
		 }
	}		
		if(count($Where) > 0){				  
		 	
			$sql_query = ''; $where_col ='';
			if(count($Where)>0){
				$w = 1;
				$count_lenght_of_where = count($Where);
				foreach ($Where as $k) {
					$where_col .= $k;
					if ($w < $count_lenght_of_where) {
						$where_col .=' AND ';
					}
					$w++;
				 }
				$sql_query =' WHERE '.$where_col;			
			}	
			
			$sku_condition = '';
			if(isset($this->request->data['skus']) && count($this->request->data['skus']) > 0){
				$sku_condition = '';
				foreach($this->request->data['skus'] as $s){
					$esk = explode("(",$s);
					$sku_condition .= "  sku LIKE  '%".$esk[0] ."%' OR ";
				}
				$sku_condition = rtrim($sku_condition,' OR') ;
			}
			
			if($sku_condition == ''){

				$query = "SELECT order_id,sku,quantity,product_order_id_identify,created_batch FROM merge_updates as MergeUpdate $sql_query";
			}else{
				$query = "SELECT order_id,sku,quantity,product_order_id_identify,created_batch FROM merge_updates as MergeUpdate $sql_query AND (". $sku_condition.")";
			}
			//   echo $query ;
			
			$orderItems = $this->MergeUpdate->query($query);
			
			$orders = array();
			$split_order_ids = array();
			
			if(count($orderItems) > 0) {
 			 	foreach($orderItems as $v){
				  	$split_order_ids[] = $v['MergeUpdate']['product_order_id_identify']; 					
				}
				
				if(count($locs) > 0){
				
					$pi_sql =  "SELECT `id`,`order_id`, `split_order_id`,`sku`, `barcode`, `bin_location`, `available_qty_bin`, `quantity`, `po_name`, `po_id` FROM order_locations as OrderLocation WHERE split_order_id IN('".implode("','",$split_order_ids)."') AND  bin_location IN ('".implode("','",$locs)."') AND status = 'active' AND pick_list = '0' " ;
					
				}else{
				
			 		$pi_sql = "SELECT `id`,`order_id`, `split_order_id`, `sku`, `barcode`, `bin_location`, `available_qty_bin`, `quantity`, `po_name`, `po_id` FROM order_locations as OrderLocation WHERE split_order_id IN('".implode("','",$split_order_ids)."') AND status = 'active' AND pick_list = '0' ";
				
 				}
  				
 				$pickListItems = $this->OrderLocation->query($pi_sql);
				
				if( count( $pickListItems ) > 0 )
				{
					 
					//manage userd - id with name
					$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
					$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
					$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
					
					$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
					
					$orderids = array(); $data = array(); $tq = 0; $taq = 0; $i_sku = 0;
					foreach( $pickListItems as $val )
					{
					
						$open_ord = $this->OpenOrder->find('first', ['conditions' => ['num_order_id' => $val['OrderLocation']['order_id']],'fields'=>['destination']] );	
						if($open_ord['OpenOrder']['destination'] == 'United Kingdom' ){
							$inkskus = explode(',', $val['OrderLocation']['sku']);
							if(count($inkskus) > 1){
								foreach($inkskus as $k => $v){
									$x = explode('XS-', $v); 
									if(count($x) > 1){
										$insku = 'S-'.$x[1];
										if(in_array( $insku, $ink_data ) ){
											$i_sku = $i_sku + $x[0]*2;  
										}
									}
								}
							}else {
									$x = explode('XS-', $val['OrderLocation']['sku']); 
									if(count($x) > 1){
										$insku = 'S-'.$x[1];
										if(in_array( $insku, $ink_data ) ){
											$i_sku = $i_sku + 2; 						 
										}
									}						 
							}
						}

						$tq += $val['OrderLocation']['quantity'];	
						$taq += $val['OrderLocation']['available_qty_bin'];	
						$orderids[] = array('order_id'=>$val['OrderLocation']['order_id'],'split_order_id'=>$val['OrderLocation']['split_order_id']);			
						$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
										'sku'=>trim($val['OrderLocation']['sku']),
										'barcode' => $val['OrderLocation']['barcode'],
										'quantity' => $val['OrderLocation']['quantity'],
										'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
					}
								
					/* dome pdf vendor */
					require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
					spl_autoload_register('DOMPDF_autoload'); 
					$dompdf = new DOMPDF();
					$dompdf->set_paper('A4', 'portrait');
					/* Html for print pick list */
					
					date_default_timezone_set('Europe/Jersey');
					$today       = date("d_m_Y_H_i_s");					
					$date     	 = date("m/d/Y H:i:s");	
					$finArray = array();
					
					$picklist_barcode = '';
					for($i=0; $i < 13; $i++){
						$picklist_barcode .= mt_rand(0, 9);
					}
					$this->getPicklistBarcode($picklist_barcode);
			
					$html = '<table border="0" width="100%"><tr><td><h3>PickList :- '.$date.'</h3></td><td style="text-align:right;"><img src="'.WWW_ROOT .'img/printPickList/'.$picklist_barcode.'.png"></td></tr></table><hr>';
					
					if($i_sku > 0){
						$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$created_batch.'</h4></td><td><h4 style="text-align:right;">Total Envelope : '.$i_sku.'</h4></td></tr></table>';
					} else {
						$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$created_batch.'</h4></td><td><h4 style="text-align:right;"></h4></td></tr></table>';
					}
								
 					  
					$html .='<table border="0" width="100%" style="font-size:12px;">';
					$html .='<tr>';
						$html .='<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>';
						$html .='<th style="border:1px solid;" width="10%" align="center">SKU</th>';
						$html .='<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>';
						$html .='<th style="border:1px solid;" width="10%" align="center">Barcode</th>';
					$html .='</tr>';
					
					$data = $this->msortnew($data, array('bin_location'));
					$tdata = array();
					foreach($data as $a){
						$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
												
					}			
					
					foreach($tdata as $sku => $d){
							$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
							foreach($d as $_ar){
								$bn[$_ar['bin_location']][] = $_ar['quantity'];	
								$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
								//$bar[$_ar['barcode']] = $_ar['barcode'];	
								$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
								
								if(count($local_barcode) > 0){
									$bar[$local_barcode[0]] = $local_barcode[0]; 
								}else{
									$bar[$_ar['barcode']] = $_ar['barcode'];
								}
							}
							
							$bins = array_keys($bn);  $oversell = array();
							foreach($bins as $b){
								if($b !=''){
									if(array_sum($bn[$b]) == array_sum($bna[$b])){
										$bin_loc[]= $b."(".array_sum($bna[$b]).")";
									}else{
										$bin_loc[]= $b."(".array_sum($bna[$b]).")";
										$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
									}
								}
								$qty[]= array_sum($bn[$b]);
								$avaqty[]= array_sum($bna[$b]);
								
							}	
							$oversell_str = '';
							if(count($oversell) > 0){
								$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
							}
							$pr_sql = "SELECT `product_name` FROM products as Product WHERE product_sku = '".$sku."'";
							$products = $this->Product->query($pr_sql);
							 
							$sku_title = '';		
							if(count($products) > 0){
								$sku_title = substr($products[0]['Product']['product_name'], 0, 40 );
							}
												
							$html.=	'<tr>
								<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
								<td style="border:1px solid;">'.$sku.'</td>
								<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$sku_title.'</td>
								<td style="border:1px solid;">'.implode(",",$bar).'</td>							
								</tr>';
								
					} 
					$rand = '';
					for($i=0; $i < 3; $i++){
						$rand .= mt_rand(0, 9);
					}
					
					$base_name	 = 'PL_'.$today.'_'.$rand.'_'.$co;
					
  					if(file_exists( WWW_ROOT .'logs/dynamic_picklist/'.$base_name.'.pdf')){
						$base_name	 = 'PL_'.$today.'_R'.$rand.'_'.$co .'T';
					}
					
					$html .= '</table>';	 		 
					$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
					$dompdf->render();
					
					$html .= '<div class="col-md-12 text-right" style="padding-top:10px;"><button type="button" class="btn btn-success" id="'.$base_name.'" onclick=generatePickList("'.$base_name.'")>Generate Picklist</button</div>';	
					
					$name = $base_name.'.pdf';
					file_put_contents( WWW_ROOT .'logs/dynamic_picklist/'.$name, $dompdf->output());
					 
					$pickdata['picklist_name'] = $name;
					$pickdata['pc_name'] = $pcName;
					$pickdata['picklist_type'] = $this->request->data['sku_type'];
					$pickdata['sku_count'] = $tq;
					$pickdata['order_count'] = count($orderids);
					$pickdata['picklist_barcode'] = $picklist_barcode;
					$pickdata['created_username'] = $firstName .' '. $lastName;
					$pickdata['batch'] = $this->request->data['batch'];
					$pickdata['country'] =  $this->request->data['postal_country'];
					$pickdata['service_name'] =  $this->request->data['postal_services'];
					$pickdata['created_date'] = date('Y-m-d H:i:s');
					
					file_put_contents(WWW_ROOT.'logs/dynamic_picklist/'.$base_name.'.txt', json_encode($pickdata));	
					file_put_contents(WWW_ROOT.'logs/dynamic_picklist/'.$base_name.'_ord.txt', json_encode($orderids)); 
					file_put_contents(WWW_ROOT.'logs/dynamic_picklist/'.$base_name.'_batch.txt', $this->request->data['batch']);
					file_put_contents(WWW_ROOT.'logs/dynamic_picklist/'.$base_name.'_split_orders.txt', json_encode($split_order_ids));		
					
					
				}else{
					$html = '<br><center>No orders available for pick list!</center>'; 
					$base_name = '';
					//$this->redirect($this->referer());
				}
				  
				//return array('html' => $html,'base_name' => $base_name); 
		
   				//$pick_data = $this->picklistPreview($orders, $split_order_ids, $this->request->data['sku_type'],$this->request->data['batch'] );
				
				$result_data['preview']   = $html;
				
				$result_data['base_name'] = $base_name;
				//$this->Session->setflash( 'Pick list generated!', 'flash_success'); 
			}else{
				$result_data['preview'] ='No data is available for pick list!'; 
				//$this->redirect($this->referer());				 
			}
		}else{
 			$result_data['preview'] = 'No orders are available for pick list!'; 
 		}
		
 		echo json_encode($result_data);
 		exit;
	} 
	
 	public function generatePicklist(){
		
		$this->layout = '';
		$this->autoRander = false;			
		$this->loadModel( 'OrderLocation' );
		$this->loadModel( 'DynamicPicklist' );		
		$this->loadModel( 'MergeUpdate' );	 	
		$this->loadModel( 'Product' );		 
		$this->loadModel( 'OpenOrder' );
		$data['result'] = '';
		if(isset($this->request->data['picklist_name']) && $this->request->data['picklist_name']!= ''){
			$base_name = WWW_ROOT .'logs/dynamic_picklist/'.$this->request->data['picklist_name'];
			if(file_exists($base_name.'.txt')){
				$orderids = []; $split_order_id = [];
				$pickdata = json_decode(file_get_contents($base_name.'.txt'), true); 
				$orders   = json_decode(file_get_contents($base_name.'_ord.txt'), true);   
				$batch    = json_decode(file_get_contents($base_name.'_batch.txt'), true);    
 				if(count($orders) > 0){
					foreach($orders as $v){
						//$orderids[] = $v['order_id'];
						$split_order_id[] = $v['split_order_id'];
					}
				}
 				
  				if(count($pickdata) > 0){
					@copy($base_name.'.pdf', WWW_ROOT.'img/printPickList/'.$this->request->data['picklist_name'].'.pdf');
					@unlink($base_name.'.pdf');
					
					$this->DynamicPicklist->saveAll($pickdata);
					
					$picklist_inc_id = $this->DynamicPicklist->getLastInsertId();
					
					if(count($split_order_id) > 0){
  					
						 
						$this->MergeUpdate->updateAll(array('pick_list_status' => '1','picklist_inc_id' => $picklist_inc_id,'picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('product_order_id_identify' => $split_order_id,'status' => 0,'pick_list_status' => 0));
						
						$this->OrderLocation->updateAll(array('pick_list' => '1','picklist_inc_id' => $picklist_inc_id,'batch' => $batch,'picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('split_order_id'=> $split_order_id, 'status'=>'active', 'pick_list'=> 0));
					 
						
						$data['result'] = 'Picklist generated!'; 
						$data['status'] = 'ok'; 
						
						/*----------Maintain  Logs---------------------*/
						
						file_put_contents( WWW_ROOT .'logs/dynamic_log/pp_'.date('dmy').".log", print_r($split_order_id,true), FILE_APPEND|LOCK_EX);	
						
						$log_file = WWW_ROOT .'logs/dynamic_log/create_picklist_'.date('dmy').".log";
						
						if(!file_exists($log_file)){
							file_put_contents($log_file, 'Username'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
						}
						file_put_contents($log_file,$this->Session->read('Auth.User.username')."\t".$picklist_inc_id."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
						/*--------End Of Logs-------------*/
		
					}else{
						$data['result'] = 'No orders available for pick list!'; 
						$data['status'] = 'no';
					}
				}
			}
		}else{
			$data['result'] = 'Picklist data not available!';
			$data['status'] = 'no';
		}
 		
 		
 		echo json_encode($data);
		exit;
	}
	
	public function generateCountryPicklist(){
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' );   
   	 
		$single_sku_orders =  array();	 
		$multi_sku_orders  =  array();	 
		$s_data = $m_data  =  array(); 		 
		$result_data =  array(); 		
  		 
		if(isset($this->request->data['orders']) && $this->request->data['orders']!=''){
			
			$order_ids = $this->request->data['orders'];
			
			$split_order_ids = explode(",", $order_ids);
			
			 $query = "SELECT order_id,sku,product_order_id_identify FROM merge_updates as MergeUpdate WHERE service_name != 'Over Weight' AND product_order_id_identify IN('".implode("','",$split_order_ids)."') AND status = '0' AND pick_list_status = '0' ";
			$items = $this->MergeUpdate->query($query);	 
			if(count($items) > 0){		
				foreach($items as $v){
 					$skus = explode( ',', $v['MergeUpdate']['sku']);
					if(count($skus) == 1){
						$single_sku_orders[] = $v['MergeUpdate']['product_order_id_identify'];
					}else{
						$multi_sku_orders[] = $v['MergeUpdate']['product_order_id_identify'];
					}					
				}	
				if(count($single_sku_orders) > 0){
					$s_data = $this->createCountryPicklist($this->request->data,$single_sku_orders,'single');	
				}
				if(count($multi_sku_orders) > 0){
					$m_data = $this->createCountryPicklist($this->request->data,$multi_sku_orders,'multiple');	
				}
			}else{
				$result_data['msg'] = 'No orders is available for pick list!'; 						 
			}		 
			
		}
		if(count($s_data) > 0){
			$result_data['single'] = $s_data;
		}
		if(count($m_data) > 0){
			$result_data['multi'] = $m_data;
		}
  		 
  		echo json_encode($result_data);
 		exit;
	 
		 
	}
	
	public function createCountryPicklist($data = [], $split_order_ids = [], $pl_type = NULL){
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'SectionsRack' );
		$this->loadModel( 'OrderLocation' );	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'Product' );		 
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'InkSku');
		
		$i_sku = 0;
		$ink_data = [];		
		$InkSku = $this->InkSku->find('all');
		foreach( $InkSku as $Ink ){
			$ink_data[]	= $Ink['InkSku']['sku'];
		}
			 
  		$batch      =  '';	
		$country	=  '';
		$service 	=  '';
		$order_ids  =  '';
		$orderItems =  array();	 		 
		$result_data['error'] = '';
		
		
		if(count($data) > 0){		
			$this->request->data = $data;
		}
		
		 if(isset($this->request->data['batch']) && $this->request->data['batch']!=''){
			$batch = $this->request->data['batch'];
		 }
		 if(isset($this->request->data['country']) && $this->request->data['country']!=''){
			$country = $this->request->data['country'];
		 }
		 if(isset($this->request->data['service']) && $this->request->data['service']!=''){
			$service = $this->request->data['service'];
		 }
		 
  		 
		if(count($split_order_ids) > 0){
 			
 		 	$pi_sql = "SELECT `id`,`order_id`, `split_order_id`, `sku`, `barcode`, `bin_location`, `available_qty_bin`, `quantity`, `po_name`, `po_id` FROM order_locations as OrderLocation WHERE split_order_id IN('".implode("','",$split_order_ids)."') AND status = 'active' AND pick_list = '0' ";
				
			$pickListItems = $this->OrderLocation->query($pi_sql);
			
			if( count( $pickListItems ) > 0 )
			{
 				//manage userd - id with name
				$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
				$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
				$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
				
				$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
				
			    $data = array(); $tq = 0; $taq = 0;
				foreach( $pickListItems as $val )
				{
					$open_ord = $this->OpenOrder->find('first', ['conditions' => ['num_order_id' => $val['OrderLocation']['order_id']],'fields'=>['destination']] );	
					if($open_ord['OpenOrder']['destination'] == 'United Kingdom' ){
						$inkskus = explode(',', $val['OrderLocation']['sku']);
						if(count($inkskus) > 1){
							foreach($inkskus as $k => $v){
								$x = explode('XS-', $v); 
								if(count($x) > 1){
									$insku = 'S-'.$x[1];
									if(in_array( $insku, $ink_data ) ){
										$i_sku = $i_sku + $x[0]*2;  
									}
								}
							}
						}else {
							$x = explode('XS-', $val['OrderLocation']['sku']); 
							if(count($x) > 1){
								$insku = 'S-'.$x[1];
								if(in_array( $insku, $ink_data ) ){
									$i_sku = $i_sku + 2; 						 
								}
							}					 
						}
					}
				
					$tq += $val['OrderLocation']['quantity'];	
					$taq += $val['OrderLocation']['available_qty_bin'];	
					 		
					$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
									'sku'=>trim($val['OrderLocation']['sku']),
									'barcode' => $val['OrderLocation']['barcode'],
									'quantity' => $val['OrderLocation']['quantity'],
									'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
				}
							
				/* dome pdf vendor */
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				/* Html for print pick list */
				date_default_timezone_set('Europe/Jersey');
				$today    = date("d_m_Y_H_i_s");
				$date     = date("m/d/Y H:i:s");	
				$finArray = array();
				
				$picklist_barcode = '';
				for($i=0; $i < 13; $i++){
					$picklist_barcode .= mt_rand(0, 9);
				}
				$this->getPicklistBarcode($picklist_barcode);
			
				$html = '<table border="0" width="100%"><tr><td><h3>PickList :- '.$date.'</h3></td><td style="text-align:right;"><img src="'.WWW_ROOT .'img/printPickList/'.$picklist_barcode.'.png"></td></tr></table><hr>';
				
				
				if($i_sku > 0){
						$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch.'</h4></td><td><h4 style="text-align:right;">Total Envelope : '.$i_sku.'</h4></td></tr></table>';
					} else {
						$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch.'</h4></td><td><h4 style="text-align:right;"></h4></td></tr></table>';
					}
					
 				$html .='<table border="0" width="100%" style="font-size:12px;">';
				$html .='<tr>';
					$html .='<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>';
					$html .='<th style="border:1px solid;" width="10%" align="center">SKU</th>';
					$html .='<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>';
					$html .='<th style="border:1px solid;" width="10%" align="center">Barcode</th>';
				$html .='</tr>';
				
				$data = $this->msortnew($data, array('bin_location'));
				$tdata = array();
				foreach($data as $a){
					$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
											
				}			
				
				foreach($tdata as $sku => $d){
					$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
					foreach($d as $_ar){
						$bn[$_ar['bin_location']][] = $_ar['quantity'];	
						$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
						//$bar[$_ar['barcode']] = $_ar['barcode'];	
						$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
						
						if(count($local_barcode) > 0){
							$bar[$local_barcode[0]] = $local_barcode[0]; 
						}else{
							$bar[$_ar['barcode']] = $_ar['barcode'];
						}
					}
					
					$bins = array_keys($bn);  $oversell = array();
					foreach($bins as $b){
						if($b !=''){
							if(array_sum($bn[$b]) == array_sum($bna[$b])){
								$bin_loc[]= $b."(".array_sum($bna[$b]).")";
							}else{
								$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
							}
						}
						$qty[]= array_sum($bn[$b]);
						$avaqty[]= array_sum($bna[$b]);
						
					}	
					$oversell_str = '';
					if(count($oversell) > 0){
						$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
					}
					
					
					$pr_sql = "SELECT `product_name` FROM products as Product WHERE product_sku = '".$sku."'";
 					$products = $this->Product->query($pr_sql);
					 
 					$sku_title = '';		
					if(count($products) > 0){
						$sku_title = substr($products[0]['Product']['product_name'], 0, 40 );
					}
										
					$html.=	'<tr>
						<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
						<td style="border:1px solid;">'.$sku.'</td>
						<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$sku_title.'</td>
						<td style="border:1px solid;">'.implode(",",$bar).'</td>							
						</tr>';
						
				} 
				$co = strtoupper(substr($pl_type,0,4));	
				
				$rand = '';
				for($i=0; $i < 3; $i++){
					$rand .= mt_rand(0, 9);
				}
			 
				$base_name	 = 'PL_'.$today .'_' . $rand.'_'.$co;
				
				if(file_exists( WWW_ROOT .'img/printPickList/'.$base_name.'.pdf')){
					$base_name = 'PL_'.$today.'_R'.$rand.'_'.$co .'T';
				}
				
				$html .= '</table>';	
						 
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				
  				$name = $base_name.'.pdf';
				
				file_put_contents( WWW_ROOT .'img/printPickList/'.$name, $dompdf->output());
				 
				$pickdata['picklist_name'] = $name;
				$pickdata['pc_name'] = $pcName;
				$pickdata['picklist_type'] = $pl_type;
				$pickdata['sku_count'] = $tq;
				$pickdata['order_count'] = count($split_order_ids);
				$pickdata['created_username'] = $firstName .' '. $lastName;
				$pickdata['batch'] = $batch;
				$pickdata['picklist_barcode'] = $picklist_barcode;
				$pickdata['country'] = $country;
				$pickdata['service_name'] = $service;
				$pickdata['created_from'] = 'batch_page';
				$pickdata['created_date'] = date('Y-m-d H:i:s');
				
				$this->DynamicPicklist->saveAll($pickdata);
 				$picklist_inc_id = $this->DynamicPicklist->getLastInsertId();
 					
 				$this->MergeUpdate->updateAll(array('pick_list_status' => '1','picklist_inc_id' => $picklist_inc_id,'picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('product_order_id_identify' => $split_order_ids,'status' => 0,'pick_list_status' => 0));
				
				$this->OrderLocation->updateAll(array('pick_list' => '1','picklist_inc_id' => $picklist_inc_id,'batch' => $batch,'picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('split_order_id'=> $split_order_ids, 'status'=>'active', 'pick_list'=> 0));
			 
					
 				/*----------Maintain  Logs---------------------*/
				
				file_put_contents( WWW_ROOT .'logs/dynamic_log/pp_'.date('dmy').$pl_type.".log", print_r($split_order_ids,true), FILE_APPEND|LOCK_EX);	
				
				$log_file = WWW_ROOT .'logs/dynamic_log/create_picklist_'.date('dmy').".log";
				
				if(!file_exists($log_file)){
					file_put_contents($log_file, 'Username'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
				}
				file_put_contents($log_file,$this->Session->read('Auth.User.username')."\t".$picklist_inc_id."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
				/*--------End Of Logs-------------*/
		
					$result_data['status'] 	= 'ok'; 
					$result_data['pl_name'] = $name; 
					$result_data['msg'] 	= 'Pick list generated for '. $pl_type;  
				}
				else{
					$result_data['msg'] = 'No orders founds for pick list!'; 
 				} 		
  		}else{
			$result_data['msg'] ='No orders is available for pick list!'; 
						 
		}
		 
  		return $result_data;	 
	}
	
	public function createCustomPicklist($option = NULL){
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'OrderLocation' );	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'Product' );		 
		$this->loadModel( 'DynamicBatche' );
		$this->loadModel( 'OpenOrder');
		$this->loadModel( 'InkSku');
		
   		$all_order = array();
		$param = array();  		 
		$orderItems =  array(); 
		$ink_data = [];
		$option = 'express';
		
		$InkSku = $this->InkSku->find('all');
		foreach( $InkSku as $Ink ){
			$ink_data[]	=	$Ink['InkSku']['sku'];
		}
 	 	 
		
		$split_order_ids = array();
		$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id','product_order_id_identify','delevery_country','sku'),'conditions' => array('MergeUpdate.status' => '0','MergeUpdate.pick_list_status' => '0','MergeUpdate.postal_service' => 'Express')));	
		$i_sku = 0;
		if(count($orderItems ) > 0){
			foreach($orderItems as $val){
			
				$split_order_ids[] = $val['MergeUpdate']['product_order_id_identify'];
			
				if( $val['MergeUpdate']['delevery_country'] == 'United Kingdom' ){
					$inkskus = explode(',', $val['OrderLocation']['sku']);
					if(count($inkskus) > 1){
						foreach($inkskus as $k => $v){
							$x = explode('XS-', $v); 
							if(count($x) > 1){
								$insku = 'S-'.$x[1];
								if(in_array( $insku, $ink_data ) ){
									$i_sku = $i_sku + $x[0]*2;  
								}
							}
						}
					}else{
						$x = explode('XS-', $val['OrderLocation']['sku']); 
						if(count($x) > 1){
							$insku = 'S-'.$x[1];
							if(in_array( $insku, $ink_data ) ){
								$i_sku = $i_sku + 2; 						 
							}
						}				 
					}
				}
			}
		}
		 //pr($orderItems);
		 
		if(count($split_order_ids)){
			$param = array('conditions' => array('pick_list' => '0','status' => 'active','split_order_id' => $split_order_ids),'order' => 'bin_location ASC');	
		}else{						 
			$this->Session->setflash( 'No UK Express orders found!', 'flash_danger'); 
		}	  
		 	 
		  		 
  		 //pr($orderItems);
		if(isset($param)){
			$pickListItems = $this->OrderLocation->find('all', $param );	
			
 			if( count( $pickListItems ) > 0 )
			{
			
				$batch_num = date('YmdHis');
				$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
				$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
				$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
				$_username = $firstName .' '. $lastName ;
			
  								 
 				//manage userd - id with name
				$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
				$picklist_username = $firstName .' '. $lastName;
				$split_order_ids = array(); $data = array(); $tq = 0; $taq = 0;
				foreach( $pickListItems as $val )
				{
					$tq += $val['OrderLocation']['quantity'];	
					$taq += $val['OrderLocation']['available_qty_bin'];	
					$split_order_ids[] = $val['OrderLocation']['split_order_id'];			
					$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																		'sku'=>trim($val['OrderLocation']['sku']),
																		'barcode' => $val['OrderLocation']['barcode'],
																		'quantity' => $val['OrderLocation']['quantity'],
																		'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
				}
				
							
				/* dome pdf vendor */
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				/* Html for print pick list */
				date_default_timezone_set('Europe/Jersey');
				$today   = date("d_m_Y_H_i_s");
				$date	 = date("m/d/Y H:i:s");	
				$finArray = array();
				
				$picklist_barcode = '';
				for($i=0; $i < 13; $i++){
					$picklist_barcode .= mt_rand(0, 9);
				}
				$this->getPicklistBarcode($picklist_barcode);
			
				$html = '<table border="0" width="100%"><tr><td><h3>PickList :- '.$date.'</h3></td><td style="text-align:right;"><img src="'.WWW_ROOT .'img/printPickList/'.$picklist_barcode.'.png"></td></tr></table><hr>';
		 
				
				if($i_sku > 0){
				$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;">Total Envelope : '.$i_sku.'</h4></td></tr></table>';
				} else {
				$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;"></h4></td></tr></table>';
				}
				
				$html .= '<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
						<tr>
						<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
						<th style="border:1px solid;" width="10%" align="center">SKU</th>
						<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
						<th style="border:1px solid;" width="10%" align="center">Barcode</th>
						</tr>';
				
				$data = $this->msortnew($data, array('bin_location'));
				$tdata = array();
				foreach($data as $a){
					$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
											
				}			
				
				foreach($tdata as $sku => $d){
						$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
						foreach($d as $_ar){
							$bn[$_ar['bin_location']][] = $_ar['quantity'];	
							$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
							//$bar[$_ar['barcode']] = $_ar['barcode'];	
							$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
							
							if(count($local_barcode) > 0){
								$bar[$local_barcode[0]] = $local_barcode[0]; 
							}else{
								$bar[$_ar['barcode']] = $_ar['barcode'];
							}
						}
						
						$bins = array_keys($bn);  $oversell = array();
						foreach($bins as $b){
							if($b !=''){
								if(array_sum($bn[$b]) == array_sum($bna[$b])){
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								}else{
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
									$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
								}
							}
							$qty[]= array_sum($bn[$b]);
							$avaqty[]= array_sum($bna[$b]);
							
						}	
						$oversell_str = '';
						if(count($oversell) > 0){
							$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
						}
						
						
						$pr_sql = "SELECT `product_name` FROM products as Product WHERE product_sku = '".$sku."'";
						$products = $this->Product->query($pr_sql);
						 
						$sku_title = '';		
						if(count($products) > 0){
							$sku_title = substr($products[0]['Product']['product_name'], 0, 40 );
						}
 											
						$html.=	'<tr>
							<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
							<td style="border:1px solid;">'.$sku.'</td>
							<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$sku_title.'</td>
							<td style="border:1px solid;">'.implode(",",$bar).'</td>							
							</tr>';
							
				}	
			
				$html .= '</table>';			 
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
 				  			
 				 
				//$dompdf->stream("Pick_List_(".$date.").pdf");
				
				$imgPath = WWW_ROOT .'img/printPickList/'; 
				$path = Router::url('/', true).'img/printPickList/';
				$name = 'PL_'.strtoupper($option).'_'.$today.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				 
				$db_data = [] ; $pickdata = []; 
				if(count($split_order_ids) > 0){
  				 						
					$db_data['batch_name'] 		= $batch_num;
					$db_data['pc_name'] 		= $pcName;
					$db_data['order_count'] 	= count( $split_order_ids );
					$db_data['creater_name'] 	= $_username;
					$db_data['batch_type'] 		= $option;
					$db_data['created_date'] 	= date('Y-m-d H:i:s');
					$this->DynamicBatche->saveAll($db_data);
 				
					$pickdata['picklist_name'] = $name;
					$pickdata['pc_name'] = $pcName;
					$pickdata['picklist_type'] = 'mixed';
					$pickdata['sku_count'] = $tq;
					$pickdata['order_count'] = count($split_order_ids);
					$pickdata['created_username'] = $firstName .' '. $lastName;
					$pickdata['batch'] = $batch_num;
					$pickdata['picklist_barcode'] = $picklist_barcode;
					$pickdata['country'] = $option;
					$pickdata['service_name'] = $option;
					$pickdata['created_from'] = 'batch_page';
					$pickdata['created_date'] = date('Y-m-d H:i:s');
					
					$this->DynamicPicklist->saveAll($pickdata);
					$picklist_inc_id = $this->DynamicPicklist->getLastInsertId();
				
				
 					$this->MergeUpdate->updateAll(array('MergeUpdate.picklist_username' => "'".$picklist_username."'", 'MergeUpdate.pick_list_status' => '1','MergeUpdate.created_batch' => $batch_num,'MergeUpdate.picklist_inc_id' => $picklist_inc_id,'MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.product_order_id_identify' => $split_order_ids));
					$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','batch' => $batch_num,'picklist_inc_id' => $picklist_inc_id,'OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.split_order_id' => $split_order_ids));
					 
				}
 				 	
				$folderPath = WWW_ROOT .'logs';				 	
				$_Path = $folderPath .'/'. date('dmy_Hi').$option.'_PicklistPC.log';    				  
				file_put_contents($_Path, $username."\n".implode( "," , $orderids ));
					  
 				//now abort file status down
				 
 				header('Content-Encoding: UTF-8');
				header('Content-type: text/csv; charset=UTF-8');
				header('Content-Disposition: attachment;filename="'.$name.'"');
				header("Content-Type: application/octet-stream;");
				header('Cache-Control: max-age=0');
				readfile($imgPath.$name);
				 
				//exit;
					
			}else{
				//$result_data['msg'] = 'No orders are available for pick list!'; 	
				$this->Session->setflash( 'No orders are available for <strong>'. strtoupper($option).'</strong> pick list!', 'flash_danger'); 
					 
			}
		}else{
			//$result_data['msg'] = 'Please selected skus before to generate pick list or choose other option!'; 	
			$this->Session->setflash( 'Please selected skus before to generate pick list or choose other option!', 'flash_danger'); 
				
		}
		 $this->redirect($this->referer());	
  		//return $result_data;	 
	}
	
	public function inkSkuPicklist($batch_num = NULL){
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'OrderLocation' );	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'Product' );		 
		$this->loadModel( 'DynamicBatche' );
		$this->loadModel( 'OpenOrder');
		$this->loadModel( 'InkSku');
		
   		$split_order_ids = array();
		$param = array();  		 
		$orderItems =  array(); 
		$ink_data = [];
		$option = 'ink';
		
		$InkSku = $this->InkSku->find('all');
		foreach( $InkSku as $Ink ){
			$ink_data[]	=	$Ink['InkSku']['sku'];
		}
 	 	 
 		 
		$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id','product_order_id_identify','delevery_country','sku'),'conditions' => array('MergeUpdate.created_batch' => $batch_num ,'MergeUpdate.status' => '0','MergeUpdate.pick_list_status' => '0','MergeUpdate.delevery_country' => 'United Kingdom')));
			
		$i_sku = 0;
		if(count($orderItems ) > 0){
			foreach($orderItems as $val){				 
				$inkskus = explode(',', $val['MergeUpdate']['sku']);
				if(count($inkskus) > 1){
					foreach($inkskus as $k => $v){
						$x = explode('XS-', $v); 
						if(count($x) > 1){
							$insku = 'S-'.$x[1];
							if(in_array( $insku, $ink_data ) ){
								$i_sku = $i_sku + $x[0]*2;  
								$split_order_ids[$val['MergeUpdate']['product_order_id_identify']] = $val['MergeUpdate']['product_order_id_identify'];
							}
						}
					}
				}else{
					$x = explode('XS-', $val['MergeUpdate']['sku']); 
					if(count($x) > 1){
						$insku = 'S-'.$x[1];
						if(in_array( $insku, $ink_data ) ){
							$i_sku = $i_sku + 2; 	
							$split_order_ids[$val['MergeUpdate']['product_order_id_identify']] = $val['MergeUpdate']['product_order_id_identify'];					 
						}
					}
				}
			}
			 
		}
		 
		 
		if(count($split_order_ids)){
			$param = array('conditions' => array('pick_list' => '0','status' => 'active','split_order_id' => $split_order_ids),'order' => 'bin_location ASC');	
		}else{						 
			$this->Session->setflash( 'No INK SKU orders found!', 'flash_danger'); 
		}	  
		
		if(isset($param)){
		
			$pickListItems = $this->OrderLocation->find('all', $param );	
			  
 			if( count( $pickListItems ) > 0 )
			{
 				 
				$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
				$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
				$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
				$_username = $firstName .' '. $lastName ;
			
  				//manage userd - id with name
				$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
				$picklist_username = $firstName .' '. $lastName;
				$split_order_ids = array(); $data = array(); $tq = 0; $taq = 0;
				foreach( $pickListItems as $val )
				{
					$tq += $val['OrderLocation']['quantity'];	
					$taq += $val['OrderLocation']['available_qty_bin'];	
					$split_order_ids[] = $val['OrderLocation']['split_order_id'];			
					$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																		'sku'=>trim($val['OrderLocation']['sku']),
																		'barcode' => $val['OrderLocation']['barcode'],
																		'quantity' => $val['OrderLocation']['quantity'],
																		'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
				}
				
 				/* dome pdf vendor */
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				/* Html for print pick list */
				date_default_timezone_set('Europe/Jersey');
				$today    = date("d_m_Y_H_i_s");
				$date	  = date("m/d/Y H:i:s");	
				$finArray = array();
				
				$picklist_barcode = '';
				for($i=0; $i < 13; $i++){
					$picklist_barcode .= mt_rand(0, 9);
				}
				
				$this->getPicklistBarcode($picklist_barcode);
			
				$html = '<table border="0" width="100%"><tr><td><h3>PickList :- '.$date.'</h3></td><td style="text-align:right;"><img src="'.WWW_ROOT .'img/printPickList/'.$picklist_barcode.'.png"></td></tr></table><hr>';
		 
				
				if($i_sku > 0){
					$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;">Total Envelope : '.$i_sku.'</h4></td></tr></table>';
				} else {
					$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;"></h4></td></tr></table>';
				}
				
				$html .= '<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
						<tr>
						<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
						<th style="border:1px solid;" width="10%" align="center">SKU</th>
						<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
						<th style="border:1px solid;" width="10%" align="center">Barcode</th>
						</tr>';
				
				$data = $this->msortnew($data, array('bin_location'));
				$tdata = array();
				foreach($data as $a){
					$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
											
				}			
				
				foreach($tdata as $sku => $d){
						$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
						foreach($d as $_ar){
							$bn[$_ar['bin_location']][] = $_ar['quantity'];	
							$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
							//$bar[$_ar['barcode']] = $_ar['barcode'];	
							$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
							
							if(count($local_barcode) > 0){
								$bar[$local_barcode[0]] = $local_barcode[0]; 
							}else{
								$bar[$_ar['barcode']] = $_ar['barcode'];
							}
						}
						
						$bins = array_keys($bn);  $oversell = array();
						foreach($bins as $b){
							if($b !=''){
								if(array_sum($bn[$b]) == array_sum($bna[$b])){
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								}else{
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
									$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
								}
							}
							$qty[]= array_sum($bn[$b]);
							$avaqty[]= array_sum($bna[$b]);
							
						}	
						$oversell_str = '';
						if(count($oversell) > 0){
							$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
						}
						
						
						$pr_sql = "SELECT `product_name` FROM products as Product WHERE product_sku = '".$sku."'";
						$products = $this->Product->query($pr_sql);
						 
						$sku_title = '';		
						if(count($products) > 0){
							$sku_title = substr($products[0]['Product']['product_name'], 0, 40 );
						}
 											
						$html.=	'<tr>
							<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
							<td style="border:1px solid;">'.$sku.'</td>
							<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$sku_title.'</td>
							<td style="border:1px solid;">'.implode(",",$bar).'</td>							
							</tr>';
							
				}	
			
				$html .= '</table>';			 
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
 				  			
 				 
				//$dompdf->stream("Pick_List_(".$date.").pdf");
				
				$imgPath = WWW_ROOT .'img/printPickList/'; 
				$path = Router::url('/', true).'img/printPickList/';
				$name = 'PL_'.strtoupper($option).'_'.$today.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				 
				$pickdata = []; 
				if(count($split_order_ids) > 0){
  				  
 					$pickdata['picklist_name'] = $name;
					$pickdata['pc_name'] = $pcName;
					$pickdata['picklist_type'] = 'mixed';
					$pickdata['sku_count'] = $tq;
					$pickdata['order_count'] = count($split_order_ids);
					$pickdata['created_username'] = $firstName .' '. $lastName;
					$pickdata['batch'] = $batch_num;
					$pickdata['picklist_barcode'] = $picklist_barcode;
					$pickdata['country'] = $option;
					$pickdata['service_name'] = $option;
					$pickdata['created_from'] = 'batch_page';
					$pickdata['created_date'] = date('Y-m-d H:i:s');
					
					$this->DynamicPicklist->saveAll($pickdata);
					$picklist_inc_id = $this->DynamicPicklist->getLastInsertId();
				
				
 					$this->MergeUpdate->updateAll(array('MergeUpdate.picklist_username' => "'".$picklist_username."'", 'MergeUpdate.pick_list_status' => '1','MergeUpdate.created_batch' => $batch_num,'MergeUpdate.picklist_inc_id' => $picklist_inc_id,'MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.product_order_id_identify' => $split_order_ids));
					$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','batch' => $batch_num,'picklist_inc_id' => $picklist_inc_id,'OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.split_order_id' => $split_order_ids));
					 
				}
 				 	
				$folderPath = WWW_ROOT .'logs';				 	
				$_Path = $folderPath .'/'. date('dmy_Hi').$option.'_PicklistPC.log';    				  
				file_put_contents($_Path, $username."\n".implode( "," , $split_order_ids ));
					  
 				//now abort file status down
				 
 				header('Content-Encoding: UTF-8');
				header('Content-type: text/csv; charset=UTF-8');
				header('Content-Disposition: attachment;filename="'.$name.'"');
				header("Content-Type: application/octet-stream;");
				header('Cache-Control: max-age=0');
				readfile($imgPath.$name);
				 
				//exit;
					
			}else{
				//$result_data['msg'] = 'No orders are available for pick list!'; 	
				$this->Session->setflash( 'No orders are available for <strong>'. strtoupper($option).'</strong> pick list!', 'flash_danger'); 
					 
			}
		}else{
			//$result_data['msg'] = 'Please selected skus before to generate pick list or choose other option!'; 	
			$this->Session->setflash( 'No INK SKU orders found!', 'flash_danger'); 
				
		}
		 $this->redirect($this->referer());	
  		//return $result_data;	 
	}
	
	public function createManulPicklist(){
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'OrderLocation' );	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'Product' );		 
		$this->loadModel( 'DynamicBatche' ); 
		$this->loadModel( 'OpenOrder');
		$this->loadModel( 'InkSku');
   	 	
		$ink_data = [];		
		$InkSku = $this->InkSku->find('all');
		foreach( $InkSku as $Ink ){
			$ink_data[]	= $Ink['InkSku']['sku'];
		}
 	 	 
 		// die('die');
		 		 
		$option =  'manual'; 

  	 	$split_orders =[ '1756013-1','1814537-1','1816358-1'];
		 
		$param = array('conditions' => array('status' => 'active','split_order_id' => $split_orders),'order' => 'bin_location ASC');	
				
  		$pickListItems = $this->OrderLocation->find('all', $param );	
		 
		
		if( count( $pickListItems ) > 0 ) 
		{
		
			$batch_num = date('YmdHis');
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
			$_username = $firstName .' '. $lastName ;
		
 			//manage userd - id with name
			$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
			$picklist_username = $firstName .' '. $lastName;
			$split_order_ids = array(); $data = array(); $tq = 0; $taq = 0; $i_sku = 0;
			foreach( $pickListItems as $val )
			{
				
				$open_ord = $this->OpenOrder->find('first', ['conditions' => ['num_order_id' => $val['OrderLocation']['order_id']],'fields'=>['destination']] );	
				if($open_ord['OpenOrder']['destination'] == 'United Kingdom' ){
					$inkskus = explode(',', $val['OrderLocation']['sku']);
					if(count($inkskus) > 1){
						foreach($inkskus as $k => $v){
							$x = explode('XS-', $v); 
							if(count($x) > 1){
								$insku = 'S-'.$x[1];
								if(in_array( $insku, $ink_data ) ){
									$i_sku = $i_sku + $x[0]*2;  
								}
							}
						}
					}else {
						$x = explode('XS-', $val['OrderLocation']['sku']); 
						if(count($x) > 1){
							$insku = 'S-'.$x[1];
							if(in_array( $insku, $ink_data ) ){
								$i_sku = $i_sku + 2; 						 
							}
						}
					}
				}
				 
 				
				$tq += $val['OrderLocation']['quantity'];	
				$taq += $val['OrderLocation']['available_qty_bin'];	
				$split_order_ids[] = $val['OrderLocation']['split_order_id'];			
				$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																	'sku'=>trim($val['OrderLocation']['sku']),
																	'barcode' => $val['OrderLocation']['barcode'],
																	'quantity' => $val['OrderLocation']['quantity'],
																	'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
			}
			
						
			/* dome pdf vendor */
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();
			$dompdf->set_paper('A4', 'portrait');
			/* Html for print pick list */
			date_default_timezone_set('Europe/Jersey');
			$today   = date("d_m_Y_H_i_s");
			$date	 = date("m/d/Y H:i:s");	
			$finArray = array();
			
			$picklist_barcode = '';
			for($i=0; $i < 13; $i++){
				$picklist_barcode .= mt_rand(0, 9);
			}
			$this->getPicklistBarcode($picklist_barcode);
			
  			$html = '<table border="0" width="100%"><tr><td><h3>PickList :- '.$date.'</h3></td><td style="text-align:right;"><img src="'.WWW_ROOT .'img/printPickList/'.$picklist_barcode.'.png"></td></tr></table><hr>';
   		  			
			if($i_sku > 0){
				$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;">Total Envelope : '.$i_sku.'</h4></td></tr></table>';
			} else {
				$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;"></h4></td></tr></table>';
			}
			
			$html .= '<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
					<tr>
					<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
					<th style="border:1px solid;" width="10%" align="center">SKU</th>
					<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
					<th style="border:1px solid;" width="10%" align="center">Barcode</th>
					</tr>';
			
			$data = $this->msortnew($data, array('bin_location'));
			$tdata = array();
			foreach($data as $a){
				$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
										
			}			
			
			foreach($tdata as $sku => $d){
					$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
					foreach($d as $_ar){
						$bn[$_ar['bin_location']][] = $_ar['quantity'];	
						$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
						//$bar[$_ar['barcode']] = $_ar['barcode'];	
						$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
						
						if(count($local_barcode) > 0){
							$bar[$local_barcode[0]] = $local_barcode[0]; 
						}else{
							$bar[$_ar['barcode']] = $_ar['barcode'];
						}
					}
					
					$bins = array_keys($bn);  $oversell = array();
					foreach($bins as $b){
						if($b !=''){
							if(array_sum($bn[$b]) == array_sum($bna[$b])){
								$bin_loc[]= $b."(".array_sum($bna[$b]).")";
							}else{
								$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
							}
						}
						$qty[]= array_sum($bn[$b]);
						$avaqty[]= array_sum($bna[$b]);
						
					}	
					$oversell_str = '';
					if(count($oversell) > 0){
						$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
					}
					
					
					$pr_sql = "SELECT `product_name` FROM products as Product WHERE product_sku = '".$sku."'";
					$products = $this->Product->query($pr_sql);
					 
					$sku_title = '';		
					if(count($products) > 0){
						$sku_title = substr($products[0]['Product']['product_name'], 0, 40 );
					}
										
					$html.=	'<tr>
						<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
						<td style="border:1px solid;">'.$sku.'</td>
						<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$sku_title.'</td>
						<td style="border:1px solid;">'.implode(",",$bar).'</td>							
						</tr>';
						
			}	
		
		 	$html .= '</table>'; 	 
			$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
			$dompdf->render();						 
			
			$imgPath = WWW_ROOT .'img/printPickList/'; 
			$path = Router::url('/', true).'img/printPickList/';
			$name = 'PL_'.strtoupper($option).'_'.$today.'.pdf';
			
			file_put_contents($imgPath.$name, $dompdf->output());
			
			$db_data = [] ; $pickdata = []; 
			if(count($split_order_ids) > 0){
									
				$db_data['batch_name'] 		= $batch_num;
				$db_data['pc_name'] 		= $pcName;
				$db_data['order_count'] 	= count( $orderids );
				$db_data['creater_name'] 	= $_username;
				$db_data['batch_type'] 		= $option;
				$db_data['created_date'] 	= date('Y-m-d H:i:s');
				$this->DynamicBatche->saveAll($db_data);
			
				$pickdata['picklist_name'] = $name;
				$pickdata['pc_name'] = $pcName;
				$pickdata['picklist_type'] = 'mixed';
				$pickdata['sku_count'] = $tq;
				$pickdata['order_count'] = count($split_order_ids);
				$pickdata['created_username'] = $firstName .' '. $lastName;
				$pickdata['batch'] = $batch_num;
				$pickdata['picklist_barcode'] = $picklist_barcode;
				$pickdata['country'] = $option;
				$pickdata['service_name'] = $option;
				$pickdata['created_from'] = 'batch_page';
				$pickdata['created_date'] = date('Y-m-d H:i:s');
				
				$this->DynamicPicklist->saveAll($pickdata);
				$picklist_inc_id = $this->DynamicPicklist->getLastInsertId();
			
			
				$this->MergeUpdate->updateAll(array('MergeUpdate.picklist_username' => "'".$picklist_username."'", 'MergeUpdate.pick_list_status' => '1','MergeUpdate.created_batch' => $batch_num,'MergeUpdate.picklist_inc_id' => $picklist_inc_id,'MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.product_order_id_identify' => $split_order_ids));
				$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','batch' => $batch_num,'picklist_inc_id' => $picklist_inc_id,'OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.split_order_id' => $split_order_ids));
				 
			}
				
			$folderPath = WWW_ROOT .'logs';				 	
			$_Path = $folderPath .'/'. date('dmy_Hi').$option.'_PicklistPC.log';    				  
			file_put_contents($_Path, $username."\n".implode( "," , $orderids ));
				  
			//now abort file status down
			 
			header('Content-Encoding: UTF-8');
			header('Content-type: text/csv; charset=UTF-8');
			header('Content-Disposition: attachment;filename="'.$name.'"');
			header("Content-Type: application/octet-stream;");
			header('Cache-Control: max-age=0');
			readfile($imgPath.$name);
			 
			//exit;
				
		}else{
			//$result_data['msg'] = 'No orders are available for pick list!'; 	
			$this->Session->setflash( 'No orders are available for <strong>'. strtoupper($option).'</strong> pick list!', 'flash_danger'); 
				 
		}
		 
		 $this->redirect($this->referer());	
  		//return $result_data;	 
	}
	public function markPicked(){
   		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'OrderLocation' );	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'Product' ); 
		
		$split_order_id = $_REQUEST['searchkey'];
		$batch_num = $_REQUEST['batch']; 
		
   		$pickLists = $this->DynamicPicklist->find('first', array('conditions' => array('batch' => $batch_num,'picklist_type' =>'mixed','completed_mark_user IS NULL' ),'order' => 'id DESC') );
 		
		 
 		$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
		$_username = $firstName .' '. $lastName ;
	
		//manage userd - id with name
		$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
		$picklist_username = $firstName .' '. $lastName;
		date_default_timezone_set('Europe/Jersey');
		$option =  'custom';		
		$today   = date("d_m_Y_H_i_s");
		$name = 'PL_'.strtoupper($option).'_'.$today.'.pdf';
			
		
		$pickdata['pc_name'] = $pcName;
		$pickdata['picklist_type'] = 'mixed';		
		$pickdata['created_username'] = $firstName .' '. $lastName;
		$pickdata['batch'] = $batch_num;		
		$pickdata['country'] = $option;
		$pickdata['service_name'] = $option;
		$pickdata['created_from'] = 'batch_page';
		$pickdata['created_date'] = date('Y-m-d H:i:s');
			
		if( count( $pickLists ) > 0 )
		{
			$name = $pickLists['DynamicPicklist']['picklist_name'];
			$picklist_barcode = $pickLists['DynamicPicklist']['picklist_barcode'];
			$sku_count = $pickLists['DynamicPicklist']['sku_count'] + 1;
			$order_count = $pickLists['DynamicPicklist']['order_count'] + 1;
 			
			$pickdata['picklist_name'] = $name;
			$pickdata['sku_count'] = $sku_count;			
			$pickdata['order_count'] = $order_count;			
			$pickdata['id'] = $pickLists['DynamicPicklist']['id'];
			$picklist_inc_id = $pickLists['DynamicPicklist']['id'];  
		 	$this->DynamicPicklist->saveAll($pickdata);
		}else{
			
			$picklist_barcode = '';
			for($i=0; $i < 13; $i++){
				$picklist_barcode .= mt_rand(0, 9);
			}
			$pickdata['picklist_name'] = $name;
			$pickdata['sku_count'] = 1;
			$pickdata['order_count'] = 1;
		 	$pickdata['picklist_barcode'] = $picklist_barcode;
			$this->DynamicPicklist->saveAll($pickdata);
			$picklist_inc_id = $this->DynamicPicklist->getLastInsertId();  
 		}
		
		
		$this->MergeUpdate->updateAll(array('MergeUpdate.picklist_username' => "'".$picklist_username."'", 'MergeUpdate.pick_list_status' => '1','MergeUpdate.created_batch' => $batch_num,'MergeUpdate.picklist_inc_id' => $picklist_inc_id,'MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.product_order_id_identify' => $split_order_id));
		
		$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','batch' => $batch_num,'picklist_inc_id' => $picklist_inc_id,'OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.split_order_id' => $split_order_id));
		
 		$param = array('conditions' => array('status' => 'active','batch' => $batch_num,'picklist_inc_id' => $picklist_inc_id,'split_order_id IS NOT NULL'),'order' => 'bin_location ASC');	
				
  		$pickListItems = $this->OrderLocation->find('all', $param );
		 
 		$orderids = array(); $data = array(); $tq = 0; $taq = 0;
		
		foreach( $pickListItems as $val )
		{
			$tq += $val['OrderLocation']['quantity'];	
			$taq += $val['OrderLocation']['available_qty_bin'];	
			$orderids[] = $val['OrderLocation']['order_id'];			
			$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																'sku'=>trim($val['OrderLocation']['sku']),
																'barcode' => $val['OrderLocation']['barcode'],
																'quantity' => $val['OrderLocation']['quantity'],
																'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
		}
			
						
		/* dome pdf vendor */
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper('A4', 'portrait');
		/* Html for print pick list */
		 
		$date	 = date("m/d/Y H:i:s");	
		$finArray = array();
	
		$this->getPicklistBarcode($picklist_barcode);
		
		$html = '<table border="0" width="100%"><tr><td><h3>PickList :- '.$date.'</h3></td><td style="text-align:right;"><img src="'.WWW_ROOT .'img/printPickList/'.$picklist_barcode.'.png"></td></tr></table><hr>';
		
		$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;"></h4></td></tr></table>';
		 
		
		$html .= '<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
				<tr>
				<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
				<th style="border:1px solid;" width="10%" align="center">SKU</th>
				<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
				<th style="border:1px solid;" width="10%" align="center">Barcode</th>
				</tr>';
		
		$data = $this->msortnew($data, array('bin_location'));
		$tdata = array();
		foreach($data as $a){
			$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
									
		}			
		
		foreach($tdata as $sku => $d){
				$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
				foreach($d as $_ar){
					$bn[$_ar['bin_location']][] = $_ar['quantity'];	
					$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
					//$bar[$_ar['barcode']] = $_ar['barcode'];	
					$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
					
					if(count($local_barcode) > 0){
						$bar[$local_barcode[0]] = $local_barcode[0]; 
					}else{
						$bar[$_ar['barcode']] = $_ar['barcode'];
					}
				}
				
				$bins = array_keys($bn);  $oversell = array();
				foreach($bins as $b){
					if($b !=''){
						if(array_sum($bn[$b]) == array_sum($bna[$b])){
							$bin_loc[]= $b."(".array_sum($bna[$b]).")";
						}else{
							$bin_loc[]= $b."(".array_sum($bna[$b]).")";
							$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
						}
					}
					$qty[]= array_sum($bn[$b]);
					$avaqty[]= array_sum($bna[$b]);
					
				}	
				$oversell_str = '';
				if(count($oversell) > 0){
					$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
				}
				
				
				$pr_sql = "SELECT `product_name` FROM products as Product WHERE product_sku = '".$sku."'";
				$products = $this->Product->query($pr_sql);
				 
				$sku_title = '';		
				if(count($products) > 0){
					$sku_title = substr($products[0]['Product']['product_name'], 0, 40 );
				}
									
				$html.=	'<tr>
					<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
					<td style="border:1px solid;">'.$sku.'</td>
					<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$sku_title.'</td>
					<td style="border:1px solid;">'.implode(",",$bar).'</td>							
					</tr>';
					
		}	
	
		$html .= '</table>';			 
		$dompdf->load_html($html, Configure::read('App.encoding'));
		$dompdf->render();						 
		
		$imgPath = WWW_ROOT .'img/printPickList/'; 
		$path = Router::url('/', true).'img/printPickList/'.$name;
		 
 		file_put_contents($imgPath.$name, $dompdf->output());
		$folderPath = WWW_ROOT .'logs';				 	
		$_Path = $folderPath .'/'. date('dmy_Hi').$option.'_PicklistPC.log';    				  
		file_put_contents($_Path, $username."\n".implode( "," , $orderids ));
  		 $this->redirect($this->referer());	
 		exit; 
	}
	
  	public function msortnew($array, $key, $sort_flags = SORT_REGULAR) {
	
	  if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) { //pr($v);
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
			        // @TODO This should be fixed, now it will be sorted as string
                   foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
		    asort($mapping, $sort_flags);
			natsort($mapping);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
	}
	
 	/*----------Bulk Pocessing----------*/
 
		public function MobileAsseInstruction( $splitOrderId  = null, $subsource = null)
		{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'Product' );
			//$splitOrderId 	= '1366785-1';
			$subsource = 'Marec_uk';
			$get_country = array('Marec_FR'=>'FR','Marec_DE'=>'DE','Marec_IT'=>'IT','Marec_ES'=>'ES','Marec_uk'=>'UK');
			$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.product_order_id_identify' => $splitOrderId)));
			$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
			foreach( $skus as $sku )
				{
					$newSkus[] = explode( 'XS-', $sku);
					foreach($newSkus as $newSku)
						{
							$getsku = 'S-'.$newSku[1];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$contentSubCat[]	=	$getOrderDetail['Product']['sub_category'];
						}
							unset($newSkus);
				}
			
			$get_source = $get_country[$subsource];
			$Ins_html	=	$this->getMobileAssHtml( $get_source,$contentSubCat );
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();
			$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');
			$cssPath = WWW_ROOT .'css/';
			//echo $Ins_html;
			//$dompdf->load_html($Ins_html, Configure::read('App.encoding'));
			$dompdf->load_html(utf8_decode($Ins_html), Configure::read('App.encoding'));
			$dompdf->render();
			$imgPath = WWW_ROOT .'img/printPDF/Instruction/'; 
			$path = Router::url('/', true).'img/printPDF/Instruction/';
			$name	=	'instruction_Slip_'.$splitOrderId.'.pdf';
			file_put_contents($imgPath.$name, $dompdf->output());
			$serverPath  	= 	$path.$name ;
			$printerId		=	'219842'; 			
			
			$sendData = array(
				'printerId' => $printerId,
				'title' => 'Now Printing',
				'contentType' => 'pdf_uri',
				'content' => $serverPath,
				'source' => 'Direct'					
			);
			App::import( 'Controller' , 'Coreprinters' );
			$Coreprinter = new CoreprintersController();
			//$d = $Coreprinter->toPrint( $sendData );
			
			//pr($d); exit;
		}
		
		public function getMobileAssHtml($get_source = null, $contentSubCat = null)
		{
			if($get_source == 'UK')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>BATTERY - </strong>This must be used in combination with a Genuine Cable, and Genuine Charger</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>CABLE - </strong>This must be used in combination with  a Genuine Charger, and Genuine Cable</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>CHARGER - </strong>This must be used in combination with a Genuine Battery, and Genuine Cable</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>EARPHONE/OTHER ACCESSORY - </strong>This is optimised for use on the original manufacturer equipment</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Bulk Packaged item</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >WHAT IS BULK PACKAGING ?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Your item is a Bulk Packaged item and is 100% authentic and brand new without a wasteful retail packaging.</li>
				<li>Bulk-packaged items are simply provided without retail packaging to offer additional costs savings to you.</li>
				<li>All items dispatched are 100% authentic, brand new items sourced from major mobile accessory distributors across the EU.</li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>IMPORTANT INFORMATION REGARDING YOUR ITEM</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">When using this item; it must be used in combination with other Genuine items.<br></td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">If you have any query, question, or concern please contact us immediately on below given details </td></tr>
				<tr><td style="text-align:center;" >Customer support:- +44-3301240338</td></tr>
				<tr><td style="text-align:center;" >Email:- support@ebuyer-express.com</td></tr>
				</table>';
			}
			if($get_source == 'FR')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Batterie - </strong>ceci doit �tre employ� en combinaison avec un c�ble v�ritable, et le chargeur v�ritable</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>C�ble - </strong>ceci doit �tre employ� en combinaison avec un vrai chargeur, et le c�ble v�ritable</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Chargeur - </strong>ceci doit �tre employ� en combinaison avec une batterie v�ritable, et le c�ble v�ritable</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>�couteurs/autre accessoire - </strong>ceci est optimis� pour l\'utilisation sur l\'�quipement original de fabricant</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-45px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Article emball� en vrac</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >Qu\'est-ce que l\'emballage en vrac?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Votre article est un article emball� en vrac et est 100% authentique et flambant neuf sans un emballage de d�tail de gaspillage.</li>
				<li>Les articles emball�s en vrac sont simplement fournis sans emballage de d�tail pour vous offrir des �conomies suppl�mentaires.</li>
				<li>Tous les articles exp�di�s sont des articles authentiques et neufs de 100% provenant de grands distributeurs d\'accessoires mobiles � travers l\'UE.</li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>Informations importantes concernant votre article</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Lors de l\'utilisation de cet �l�ment; il doit �tre utilis� en combinaison avec d\'autres objets authentiques.</td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Si vous avez n\'importe quelle requ�te, question, ou pr�occupation s\'il vous pla�t contactez-nous imm�diatement sur les d�tails ci-dessous donn�s-</td></tr>
				<tr><td style="text-align:center;" >Support � la client�le - +44-3301240338</td></tr>
				<tr><td style="text-align:center;" >email - support@ebuyer-express.com</td></tr>
				</table>';
			}
			if($get_source == 'DE')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Batterie - </strong>dies muss in Kombination mit einem echten Kabel und echtem Ladeger�t verwendet werden</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Kabel - </strong>dies muss in Kombination mit einem echten Ladeger�t und echtem Kabel verwendet werden</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Ladeger�t - </strong>dies muss in Kombination mit einer echten Batterie und echtem Kabel verwendet werden</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Ohrh�rer/anderes Zubeh�r - </strong>das ist f�r den Einsatz auf der Original-Hersteller Ausstattung optimiert</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Sch�ttgut verpackt</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >Was ist sch�ttgutverpackung ?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Ihr Artikel ist ein sch�ttgutverpacktes Produkt und ist 100% authentisch und nagelneu ohne verschwenderische Einzelhandelsverpackungen.</li>
				<li>Sch�ttgutverpackungen werden einfach ohne Einzelhandelsverpackung zur Verf�gung gestellt, um Ihnen zus�tzliche Kosteneinsparungen zu bieten.</li>
				<li>Alle versandten Artikel sind 100% authentische, brandneue Artikel, die von gro�en mobilen Zubeh�r H�ndlern in der EU stammen.</li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>Wichtige Informationen zu Ihrem Artikel</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Bei der Verwendung dieses Artikels; Es muss in Kombination mit anderen echten Gegenst�nden verwendet werden.</td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Wenn Sie Fragen, Fragen oder Bedenken haben, kontaktieren Sie uns bitte sofort unter den angegebenen Details � </td></tr>
				<tr><td style="text-align:center;" >Kundendienst - 44-3301240338</td></tr>
				<tr><td style="text-align:center;" >e-Mail - support@ebuyer-express.com</td></tr>
				</table>';
			}
			if($get_source == 'IT')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Batteria - </strong>questo deve essere usato in combinazione con un cavo genuino, e caricabatterie genuino</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Cavo- </strong>questo deve essere usato in combinazione con un caricabatterie genuino, e cavo genuino</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Caricabatterie - </strong>questo deve essere utilizzato in combinazione con una batteria genuina, e cavo genuino</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Auricolare/altro accessorio - </strong>questo � ottimizzato per l\'uso sull\'attrezzatura originale del produttore</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Articolo imballato sfuso</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >Che cosa � imballaggio alla rinfusa ?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Il vostro articolo � un articolo impaccato alla rinfusa ed � 100% autentico e brandnew senza uno spreco che impacca al minuto.</li>
				<li>Gli articoli impacchettati in massa sono forniti semplicemente senza imballaggi al dettaglio per offrire risparmi aggiuntivi ai costi.</li>
				<li>Tutti gli articoli spediti sono 100% autentici, nuovissimi prodotti provenienti dai principali distributori di accessori mobili in tutta l\'UE. </li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>Informazioni importanti sul tuo articolo</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Quando si utilizza questo elemento; deve essere usato in combinazione con altri oggetti genuini.</td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Se avete qualunque richiesta, domanda, o preoccupazione prego se li mette in contatto con immediatamente sui particolari dati sotto-</td></tr>
				<tr><td style="text-align:center;" >Assistenza clienti - +44-3301240338</td></tr>
				<tr><td style="text-align:center;" >email - support@ebuyer-express.com</td></tr>
				</table>';
			}
			if($get_source == 'ES')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Bater�a - </strong>esto se debe utilizar conjuntamente con un cable genuino, y el cargador genuino</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Cable - </strong>esto se debe utilizar conjuntamente con un cargador genuino, y el cable genuino</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Cargador - </strong>esto se debe utilizar conjuntamente con una bater�a genuina, y el cable genuino</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Auricular/otro accesorio - </strong>esto se optimiza para el uso en el equipo original del fabricante</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Art�culo empaquetado bulto</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >� Qu� es el empaquetado a granel ?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Su art�culo es un art�culo empaquetado bulto y es el 100% aut�ntico y a estrenar sin un empaquetado al por menor derrochador.</li>
				<li>Los art�culos empaquetados a granel se proporcionan simplemente sin el empaquetado al por menor para ofrecer ahorros adicionales de los costes para usted.</li>
				<li>Todos los art�culos enviados son 100% aut�nticos, nuevos art�culos de origen de los principales distribuidores de accesorios m�viles en toda la UE.</li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>Informaci�n importante sobre su art�culo</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Al usar este art�culo; debe ser utilizado en combinaci�n con otros art�culos genuinos.</td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Si usted tiene cualquier consulta, pregunta, o preocupaci�n por favor p�ngase en contacto con nosotros inmediatamente a continuaci�n detalles dados - </td></tr>
				<tr><td style="text-align:center;" >Soporte al cliente - +44-3301240338</td></tr>
				<tr><td style="text-align:center;" >email - support@ebuyer-express.com</td></tr>
				</table>';
			}
			return $cont;
		}
   
	
	 public function batchPicklistSlipLabel($picklist_id = 0)
	 {
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'OrderLocation' );
		
  		if(isset($this->request->data['picklist_inc_id'])){ 
			$picklist_id = $this->request->data['picklist_inc_id'];
		}
  		
	   	$query = "SELECT order_id,sku,barcode,quantity,product_order_id_identify FROM merge_updates as MergeUpdate WHERE picklist_inc_id = '".$picklist_id."' AND status = 0 AND over_sell_user IS NULL ";   //AND label_status < 2 
			 
 		$orderItems = $this->MergeUpdate->query($query);
		 
		$ord_msg = [];
		$data = array();
		
		if(count($orderItems) > 0){
		
			foreach($orderItems as $v){
			 
 				$_items = $this->OrderLocation->query("SELECT `bin_location` FROM `order_locations` as OrderLocation WHERE split_order_id = '".$v['MergeUpdate']['product_order_id_identify']."' AND order_id = '".$v['MergeUpdate']['order_id']."' AND status = 'active' AND available_qty_bin > 0");
				
				foreach($_items as $val )
				{					 
					$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],'split_order_id'=>$v['MergeUpdate']['product_order_id_identify']);
				}			 
			}
			 
			$data = $this->msortnew($data, array('bin_location'));
			
			 
			
			foreach( $data as $order )
			{				
				sleep(1);   
				//echo $order['split_order_id'];echo ' ='.$order['bin_location']; echo "<br>";
				$ord_msg[] = $this->printSlipLabel($order['split_order_id'],$picklist_id);					
				$log_file = WWW_ROOT .'logs/dynamic_log/print_slip_label_request_'.$picklist_id.".log";			 
				file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$order['split_order_id']."\t".$order['bin_location']."\t".$order['quantity']."\t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
			} 
	 
  		}
  	
 		$log_file = WWW_ROOT .'logs/dynamic_log/print_slip_label_user_'.date('dmy').".log";
		if(!file_exists($log_file)){
			file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
		}
		
 		file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$picklist_id."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
		
		$msg['msg'] ='';
		if(count($ord_msg) > 0){
			$msg['msg'] = implode(",",$ord_msg);
		}
		echo json_encode($msg);
		//echo "1";
		exit;	
		
	}
	
	public function printSlipLabel($split_order_id = null,$picklist_id = 0 )
	{
		if(isset($this->request->data['split_order_id'])){ 
			$split_order_id = $this->request->data['split_order_id'];
		}
		
		$msg = $this->getSlipLabel($split_order_id, $picklist_id);		
		 
		return $msg;
	}
	
	public function printSingleSlipLabel($split_order_id = null,$picklist_id = 0 )
	{
		if(isset($this->request->data['split_order_id'])){ 
			$split_order_id = $this->request->data['split_order_id'];
		}
		if(isset($this->request->data['picklist_id'])){ 
			$picklist_id = $this->request->data['picklist_id'];
		}
		
		$msg['msg'] = $this->getSlipLabel($split_order_id, $picklist_id);
		echo json_encode($msg);
		exit;
	}
	 
	public function getSlipLabel($split_order_id = null,$picklist_id = 0 )
	{ 			
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		$this->loadMOdel( 'CategoryContant' );
		$this->loadMOdel( 'Template' );
		$this->loadMOdel( 'BulkLabel' );
		$this->loadModel( 'MergeUpdate');
		$this->loadModel( 'PackagingSlip');
				
		$openOrderId		=	explode("-",$split_order_id)[0]; 
		$addData			=	$this->getAddress( $openOrderId );
		$address_checked	=	$this->checkAddress( $addData, $openOrderId );
		$msg = '';
		App::Import('Controller', 'RoyalMail'); 
		$royal = new RoyalMailController;
			
	 	$log_file = WWW_ROOT .'logs/dynamic_log/print_slip_label_'.$picklist_id.".log";
		
		//$address_checked = 1;
		if($address_checked == 'ok')
		{
				$get_sp_order =	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_order_id)));
				
				if($split_order_id == '1680467-1'){
					pr($get_sp_order);			 
					exit;
				}
				
				if( $get_sp_order['MergeUpdate']['service_provider'] == 'Royalmail' ){
					$royal->applyRoyalMailByOrder($split_order_id);
					$label 	= $this->royalMailLabel($split_order_id, $openOrderId);
				}else if($get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] == 'Spain'){
 					$label 	= $this->jerseyPostLabel($split_order_id, $openOrderId);
				}else {
					$label 	= $this->getLabel( $split_order_id, $openOrderId );
				}
 			 
			 	$slip =	$this->getSlip( $split_order_id, $openOrderId );
		
				
 				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				$date = date("m/d/Y H:i:s");
				
				$html = '';
				if($get_sp_order['MergeUpdate']['service_provider'] == 'Royalmail')
				{
					if($label != 'error')
					{						 
						$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
								 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
								 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
								 <meta content="" name="description"/>
								 <meta content="" name="author"/>';
						$html .= '<table style="margin-top:12px; height:1090px;  width:740px; border:1px solid #CCC;"> 
								<tr><td height="265" valign="top">'.$slip.'</td></tr> 
								<tr><td valign="top" ><div style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
								transform-origin: 50% 50%;">'.$label.'</div></td></tr>
								</table>';
					
					}
				}
				else if($get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] != 'Spain')
				{
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
							<tr><td style="height:700px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr><td valign="top" style="height:367px; width:560px; padding:0px 0px 5px 12px;border:1px solid gray;">'.$label.'</td><td></td></tr>
						</table>';
				}
				else if($get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] == 'Spain' )
				{
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="margin-top:12px; height:1070px;  width:740px; border:1px solid #CCC;"> 
							<tr><td height="530" valign="top">'.$slip.'</td></tr> 
							<tr><td valign="top" style="height:367px; width:550px; border:1px solid gray;">'.$label.'</td></tr>
							</table>';
				}
				else
				{ 				
 					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
							<tr><td style="height:700px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr><td valign="top" style="height:367px; width:560px; padding:0px 0px 5px 12px;border:1px solid gray;">'.$label.'</td><td></td></tr>
						</table>';
				}
				  
				if($html != ''){
					$cssPath = WWW_ROOT .'css/';
					$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
					// echo $html;
				//	$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
					$dompdf->load_html($html, Configure::read('App.encoding'));
					$dompdf->render();
					$imgPath = WWW_ROOT .'img/printPDF/Bulk/'; 
					$path = Router::url('/', true).'img/printPDF/Bulk/';
					$name	=	'Label_Slip_'.$split_order_id.'.pdf';
					file_put_contents($imgPath.$name, $dompdf->output());
					//exit;
					if($split_order_id == '1816518-1'){
						echo $html;  
						exit;
					}
					// echo $html; 
					$serverPath  	= 	$path.$name ;
					$printerId		=	$this->getPrinterId();//'306161';
					$sendData = array(
						'printerId' => $printerId,
						'title' => 'Now Print',
						'contentType' => 'pdf_uri',
						'content' => $serverPath,
						'source' => 'Direct'
					);
					
					App::import( 'Controller' , 'Coreprinters' );
					$Coreprinter = new CoreprintersController();
				//	$d = $Coreprinter->toPrint( $sendData );
					
					$msg = $split_order_id . "  Label & Slip Printed.";	
 					file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$split_order_id."\t LabelSlip Printed \t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
					
				}else{
					$msg = $split_order_id . "  Label OR Slip Issue.";	 			 
					file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$split_order_id."\t Label OR Slip Issue \t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
				}
				
		}else{
 				$msg = $split_order_id . " " . $address_checked;	 
				file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$split_order_id."\Address Format Issue\t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
			 
		} 
 		return $msg; 
	 }
	 
	public function getAddress( $openOrderId = null)
	{
			App::import('Controller', 'Linnworksapis');
			$obj = new LinnworksapisController();
			$order				=		$obj->getOpenOrderById( $openOrderId );
			$company			=	 	$order['customer_info']->Address->Company;
			$fullname			=		$order['customer_info']->Address->FullName;
			$address1			=		$order['customer_info']->Address->Address1;
			$address2			=		$order['customer_info']->Address->Address2;
			$address3			=		$order['customer_info']->Address->Address3;
			$town				=	 	$order['customer_info']->Address->Town;
			$resion				=	 	$order['customer_info']->Address->Region;
			$postcode			=	 	$order['customer_info']->Address->PostCode;
			$country			=	 	$order['customer_info']->Address->Country;
			
			$addData['company'] 	= 	$order['customer_info']->Address->Company;
			$addData['fullname'] 	= 	$order['customer_info']->Address->FullName;
			$addData['address1'] 	= 	$order['customer_info']->Address->Address1;
			$addData['address2'] 	= 	$order['customer_info']->Address->Address2;
			$addData['address3'] 	= 	$order['customer_info']->Address->Address3;
			$addData['town'] 		= 	$order['customer_info']->Address->Town;
			
			$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forl?-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia');
			
			if( $country == 'Italy' ){
				if(array_key_exists(strtoupper($resion),$provinces)){
					$addData['resion'] 		= 	$resion;
				}else {
					$addData['resion'] 		= 	array_search(ucfirst($resion), $provinces);
				}
			} else {
					$addData['resion'] 		= 	$resion;
			}
			$addData['postcode'] 	= 	$postcode;
			$addData['country'] 	= 	$country;
			return $addData;
	}
	
	public function checkAddress( $address = null, $orderid = null  )
	{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'OrderNote' );
			
 			$_error = 0; $_error_msg = '';
			
			if(strtolower($address['country']) == 'spain' && !is_numeric($address['postcode'])) {
				$_error_msg = "Post code issue.";
			}else if(strlen($address['country']) > 35 ) {				
				$_error_msg = "Country name length is more than 35 characters.";
			}else if(strlen($address['fullname']) > 55 ) {			
				$_error_msg = "Customer name length is more than 55 characters.";
			}else if(strlen($address['address1']) > 70 ) {		
				$_error_msg = "Address1 length is more than 70 characters.";
			}else if(strlen($address['address2']) > 70 ) {		
				$_error_msg = "Address2 length is more than 70 characters.";
			}else if(strlen($address['address3']) > 60 ) {			
				$_error_msg = "Address3 length is more than 60 characters.";
			} 
			
			if($_error_msg != '')
			{
					$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 3, 'MergeUpdate.label_status' => 1 ) , array( 'MergeUpdate.order_id' => $orderid ) );
					$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 3 ) , array( 'OpenOrder.num_order_id' => $orderid ) );
					
					$firstName 	= ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
					$lastName 	= ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
					
					$noteDate['order_id'] 	= 	 $orderid;
					$noteDate['note'] 		= 	 'Address format issue';
					$noteDate['type'] 		= 	 'Lock';
					$noteDate['user'] 		= 	 $firstName.' '.$lastName;
					$noteDate['date'] 		= 	 date('Y-m-d H:i:s');
					$this->OrderNote->saveAll( $noteDate );
					
 					$log_file = WWW_ROOT .'logs/dynamic_log/address_format_issue_'.date('dmy').".log";
					file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$orderid."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
					
  					$subject   = $orderid. ' Xsensys Dev address format issue.';
					$mailBody  = $orderid. ' Xsensys Dev address format issue.';					 
					$mailBody .= '<p>Error : '.$_error_msg.'</p>'; 
										
					App::uses('CakeEmail', 'Network/Email');
					$email = new CakeEmail('');
					$email->emailFormat('html');
					$email->from('info@euracogroup.co.uk');
					$email->to( array('avadhesh.kumar@jijgroup.com','shashi@euracogroup.co.uk','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));	
					//$email->to( array('avadhesh.kumar@jijgroup.com'));	  			
					$email->subject( $subject );
					$email->send( $mailBody );
					
					return $_error_msg;
			} else {
					$this->MergeUpdate->updateAll( array( 'MergeUpdate.label_status' => 2 ) , array( 'MergeUpdate.order_id' => $orderid ) );
					//$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 0 ) , array( 'OpenOrder.num_order_id' => $orderid ) );
					return "ok";
			}
		}

		
	public function getLabel( $splitOrderId = null, $openOrderId = null )
	{ 
				
				$openOrderId	=	$openOrderId ;
				$splitOrderId	=	$splitOrderId ;
				 
				App::import('Controller', 'Linnworksapis');
				$obj = new LinnworksapisController();
				
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				$order				=		$obj->getOpenOrderById( $openOrderId );
				$postalservices		=		$order['shipping_info']->PostalServiceName;
				$destination		=		$order['customer_info']->Address->Country;
				$total				=		$order['totals_info']->TotalCharge;
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				
				$address1 			= 		wordwrap($order['customer_info']->Address->Address1, 30, "\n<br>", false);	
				$address2 			= 		wordwrap($order['customer_info']->Address->Address2, 30, "\n<br>", false);	
				$address3			=		$order['customer_info']->Address->Address3;
				if(strlen($order['customer_info']->Address->Address3) > 30)	{
					$address3 		= 		wordwrap($order['customer_info']->Address->Address3, 30, "\n<br>", false);
				}
				
				/*$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				*/
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$currency			=	 	$order['totals_info']->Currency;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$templateId			=	 	$order['template_id'];
				$subSource			=	 	$order['sub_source'];
				$envelope_weight	=	 	$splitOrderDetail['MergeUpdate']['envelope_weight'];
				
				$str = ''; 
				
				$totlaWeight	=	0;
				$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
				$ref_code = trim($splitOrderDetail['MergeUpdate']['provider_ref_code']);
				$weight = 0;
				$europe = array('Monaco','Andorra');
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					
					foreach($newSkus as $newSku)
						{
							
							$totalvalue = $total;
							$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
							$getsku = 'S-'.$newSku[1];
							$qty = $newSku[0];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title	=	$getOrderDetail['Product']['product_name'];
							$weight	=	$weight + $getOrderDetail['ProductDesc']['weight'];
							$totlaWeight	= $totlaWeight + ($weight * $qty);
							$catName		= $getOrderDetail['Product']['category_name'];
							
							if($country == 'United States')
							{
									$str .= '<tr>
										<td valign="top" class="leftborder rightborder bottomborder" style=" padding-left:5px;">'.$catName.'</td>
										<td valign="top" class="leftborder rightborder bottomborder" style=" padding-left:5px;">'.$qty.'</td>
										<td valign="top" class="rightborder bottomborder" style=" padding-left:5px;">'.$qty * $getOrderDetail['ProductDesc']['weight'].' Kg</td>';
									$str .= '</tr>';
							}
							elseif($country == 'Canada')
							{
								$str .= '<tr>
									<td valign="top" class="leftborder rightborder bottomborder" style=" padding-left:5px;">'.$catName.'</td>
									<td valign="top" class="leftborder rightborder bottomborder" style=" padding-left:5px;">'.$qty.'</td>
									<td valign="top" class="rightborder bottomborder" style=" padding-left:5px;">'.$qty*$weight.' Kg</td>';
								$str .= '</tr>';
							}
							elseif($country == 'Spain')
								{
									$codearray = array("DP1", "FR7");
									$spaincode = array("mad_air","mad_road","cat_air","cat_road","sp_air","sp_road");
							
									if(in_array($ref_code, $spaincode))
									{
										$str .='<tr>
												<td  width="60%"  style="border:1px solid #000; padding:5px;">'.$catName.'</td>
												<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty * $getOrderDetail['ProductDesc']['weight'].'</td>
												<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty.'</td>';
										$str .= '</tr>';
									} else {
										$str .= '<tr>

											<td valign="top" class="noleftborder rightborder bottomborder">'.$catName.'</td>
											<td valign="top" class="center noleftborder rightborder bottomborder">'.$qty.'</td>
											<td valign="top" class="center norightborder bottomborder">'.$qty * $getOrderDetail['ProductDesc']['weight'].'</td>';
										$str .= '</tr>';
									}
								}
							else
							{
								$str .= '<tr>
									<td valign="top" class="leftborder rightborder bottomborder" style=" padding-left:5px;">'.$catName.'</td>
									<td valign="top" class="leftborder rightborder bottomborder" style=" padding-left:5px;">'.$qty.'</td>
									<td valign="top" class="rightborder bottomborder" style=" padding-left:5px;">'.$qty*$weight.' Kg</td>';
								$str .= '</tr>';
								/*$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.substr($title, 0, 25 ).'</td>
										<td valign="top" class="center norightborder bottomborder">'.$qty * $getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';*/
							}
						}
							unset($newSkus);
				}
				
				$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forl?-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia');
				$cana_provinces = array('AB' =>'Alberta','BC'=>'British Columbia','MB'=>'Manitoba','NB'=>'New Brunswick','NL'=>'Newfoundland and Labrador','NS'=>'Nova Scotia','NT'=>'Northwest Territories','NU'=>'Nunavut','ON'=>'Ontario','PE'=>'Prince Edward Island','QC'=>'Quebec','SK'=>'Saskatchewan','YT'=>'Yukon');
				
				$usa_state =  array('AL'=>'Alabama','AK'=>'Alaska','AS'=>'American Samoa','AZ'=>'Arizona','AR'=>'Arkansas','CA'=>'California','CO'=>'Colorado','CT'=>'Connecticut','DE'=>'Delaware','DC'=>'District of Columbia','FM'=>'Federated States of Micronesia','FL'=>'Florida','GA'=>'Georgia','GU'=>'Guam','HI'=>'Hawaii','ID'=>'Idaho','IL'=>'Illinois','IN'=>'Indiana','IA'=>'Iowa ','KS'=>'Kansas','KY'=>'Kentucky','LA'=>'Louisiana','ME'=>'Maine','MH'=>'Marshall Islands','MD'=>'Maryland','MA'=>'Massachusetts','MI'=>'Michigan','MN'=>'Minnesota','MS'=>'Mississippi','MO'=>'Missouri','NE'=>'Nebraska','NV'=>'Nevada','NH'=>'New Hampshire','NJ'=>'New Jersey','NM'=>'New Mexico','NY'=>'New York','NC'=>'North Carolina','ND'=>'North Dakota','MP'=>'Northern Mariana Islands','OH'=>'Ohio','OK'=>'Oklahoma','OR'=>'Oregon','PW'=>'Palau','PA'=>'Pennsylvania','PR'=>'Puerto Rico','RI'=>'Rhode Island','SC'=>'South Carolina','SD'=>'South Dakota','MT'=>'Montana','TN'=>'Tennessee','TX'=>'Texas','UT'=>'Utah','VT'=>'Vermont','VI'=>'Virgin Islands','VA'=>'Virginia','WA'=>'Washington','WV'=>'West Virginia','WI'=>'Wisconsin','WY'=>'Wyoming');
					
				$addData['company'] 	= 	$company;
				$addData['fullname'] 	= 	$fullname;
				$addData['address1'] 	= 	$address1;
				$addData['address2'] 	= 	$address2;
				$addData['address3'] 	= 	$address3;
				$addData['town'] 		= 	$town;
				
				if( $country == 'Italy' )
				{
					if(array_key_exists(strtoupper($resion),$provinces)){
						$addData['resion'] 		= 	$resion;
					}else {
						$addData['resion'] 		= 	array_search(ucfirst($resion), $provinces);
					}
				} 
				else if( $country == 'Canada' )
				{
					if(array_key_exists(strtoupper($resion),$cana_provinces)){
						$addData['resion'] 		= 	$resion;
					}else {
						$addData['resion'] 		= 	array_search(ucfirst($resion), $cana_provinces);
					}
				}
				else if( $country == 'United States' )
				{
					if(array_key_exists(strtoupper($resion),$usa_state)){
						$addData['resion'] 		= 	$resion;
					}else {
						$addData['resion'] 		= 	array_search(ucfirst($resion), $usa_state);
					}
				}
				else
				{
						$addData['resion'] 		= 	$resion;
				}
				
				$addData['postcode'] 	= 	$postcode;
				$addData['country'] 	= 	$country;
				$address 			= 		$obj->getCountryAddress( $addData );
				
				$CARTAS = '';
				if(trim($country) == 'Spain') { 
					$CARTAS = 'CARTAS '.substr($postcode, 0, 1);
					$routingcode = $obj->getCorreosBarcode( $postcode, $splitOrderId);
					$corriosImage		=		$routingcode.'.png';
					if(substr($postcode, 0, 1) == 0){
						$CARTAS = 'CARTAS '.substr($postcode, 1, 1);
					}	
				}
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				$currentdate		=	 	date("j, F  Y");
				$BarcodeImage		=		$splitOrderDetail['MergeUpdate']['order_barcode_image'];
				$corriosImage		=		$postcode.'90019.png';
				
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= str_replace( 'Spain' , 'ESPA?A', $address); 
				$setRepArray[] 					= $country;
				$barcode  						= $order['assign_barcode'];
				$barcodenum						= explode('.', $barcode);
				$barcodePath  					= Router::url('/', true).'img/orders/barcode/';
				$barcodeCorriosPath  			= Router::url('/', true).'img/orders/correos_barcode/';
				
				$barcodenum						= explode('.', $barcode);
			
				/**************** for tempplate *******************/
				$countryArray = Configure::read('customCountry');
				if($country == 'United Kingdom' && $splitOrderDetail['MergeUpdate']['service_provider'] == 'PostNL'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				} 
				else if($country != 'United Kingdom' && $splitOrderDetail['MergeUpdate']['service_provider'] == 'PostNL' && $splitOrderDetail['MergeUpdate']['postal_service'] != 'Tracked'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Rest Of EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				}
				else if(in_array($country,['United States','Canada']) && $splitOrderDetail['MergeUpdate']['service_provider'] == 'Jersey Post'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				}
				else if(!in_array( $country, $europe )  && $splitOrderDetail['MergeUpdate']['service_provider'] == 'Jersey Post'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Other', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				}
				else if( $splitOrderDetail['MergeUpdate']['service_provider'] == 'PostNL' && $splitOrderDetail['MergeUpdate']['reg_post_number'] != '' &&  $splitOrderDetail['MergeUpdate']['reg_num_img'] != '' &&  $splitOrderDetail['MergeUpdate']['track_id'] != '' && $country != 'Italy'){
				
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Other_Reg', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				 
				} 			
				else 
				{
					if( in_array( $country, $europe ) )
						{
							$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
						}
					else 
						{
							$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
						}
				}
					 
				 
				
					$subsource_lower = strtolower($subSource) ;
					$marecArray = array('marec_de','marec_es','marec_fr','marec_it','marec_uk');
					$costbreakerArray = array('costbreaker_de','costbreaker_es','costbreaker_fr','costbreaker_it','costbreaker_uk','costbreaker','costbreaker_ca','costbreaker_usabuyer');
					$techdriveArray = array('tech_drive_de','tech_drive_es','tech_drive_fr','tech_drive_it','tech_drive_uk');				
					$rainbowArray = array('rainbow retail','rainbow retail de','rainbow_retail_es','rainbow_retail_it','rainbow_retail_fr');
					$bbdArray = array('bbd_eu_de');	
				
					if(in_array($subsource_lower,$marecArray)){
						$company 	=  'Marec';
					}else if(in_array($subsource_lower,$costbreakerArray)){
						$company 	=  'CostBreaker';
					}else if(in_array($subsource_lower,$techdriveArray)){
						$company 	= 'Tech Drive Supplies';
					}else if(in_array($subsource_lower,$rainbowArray)){
						$company 	= 'Rainbow Retail';
					}else if(in_array($subsource_lower,$bbdArray)){
						$company 	= 'BBD';
					} else if( $subSource == 'EBAY0' ){
						$company 	= 'EBAY0';
					} else if( $subSource == 'EBAY2' ){
						$company 	= 'EBAY2';
					}
					$this->loadModel( 'ReturnAddre' );
					$ret_add = $this->ReturnAddre->find('first', array( 'conditions' => array( 'company' =>  $company) ) );
					$return_address = $ret_add['ReturnAddre']['return_address'];
				
				$html           =  $labelDetail['BulkLabel']['html'];
				
				//$paperHeight    =  $labelDetail['Template']['paper_height'];
				//$paperWidth  	=  $labelDetail['Template']['paper_width'];
				//$barcodeHeight  =  $labelDetail['Template']['barcode_height'];
				//$barcodeWidth   =  $labelDetail['Template']['barcode_width'];
				//$paperMode      =  $labelDetail['Template']['paper_mode'];
				
				$barcodeimg = '<img src='.$barcodePath.$BarcodeImage.'>';
			 
				$setRepArray[] 	=    $barcodeimg;
				$setRepArray[] 	=    $barcodenum[0];
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totlaWeight + $envelope_weight;
				$setRepArray[]	=	 $tax;
				$setRepArray[]	=	 $currentdate;
				$logopath		=	 Router::url('/', true).'img/';
				$setRepArray[]	=	 '<img src="'.$logopath.'logo.jpg" >';
				$logo			=	 '<img src="'.$logopath.'logo.jpg" >';
				$signature		=	 '<img src="'.$logopath.'sig.jpg" >';
				
				$setRepArray[]	=	 $logo;
				$setRepArray[]	=	 $signature;
				$setRepArray[]	=	 $splitOrderId;
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				
				$tempId			=	$splitOrderDetail['MergeUpdate']['lable_id'];
				$countryCode	=	$splitOrderDetail['MergeUpdate']['country_code'];
				
				$setRepArray[]	=	 $countryCode;
				$setRepArray[]	=	 $totlaWeight + $envelope_weight;
				
				$conversionRate	=	 Configure::read( 'conversionRate' );
				
				if($currency == 'GBP')
				{
					$totalvalue = $totalvalue;
				}
				elseif($currency == 'EUR')
				{
					$totalvalue = $totalvalue;
				}
				elseif($country == 'Spain' && $currency == 'EUR')
				{
					$totalvalue = $totalvalue;
				}
				elseif($country == 'United States' || $country == 'Canada')
				{
					$totalvalue = $totalvalue;
				}
				else
				{
					$totalvalue = number_format($totalvalue/$conversionRate, 2, '.' ,'');
				}
				
				if($totalvalue >= 800  && $labelDetail['BulkLabel']['service_provider'] == 'Jersey Post' && ($country == 'United States')){
					$numbers = array("97.11","98.32","98.52","99.72","99.94","150.27","200.45","270.62","370.82","422.11","472.31","530.71");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				if($totalvalue >= 25  && $labelDetail['BulkLabel']['service_provider'] == 'Jersey Post' && ($country == 'Canada')){
					$numbers = array("20.82","20.11","20.31","20.71","21.22","21.41", "21.61","21.51","22.23","23.72","23.71","23.22","24.41", "24.61");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				else if($totalvalue >= 22  && $labelDetail['BulkLabel']['service_provider'] == 'Jersey Post' && $country != 'United States'){
					$numbers = array("18.11","18.23","18.32","18.52","18.72","18.94","19.27","19.45","19.62","19.82","20.11","20.31","20.71","21.22","21.41", "21.61","21.51");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				
				$setRepArray[]	= $totalvalue;
				
				if($splitOrderDetail['MergeUpdate']['track_id'] != '')
				{
					$trackingnumber = $splitOrderDetail['MergeUpdate']['track_id'];
				}
				else
				{
					$trackingnumber = 'A'.mt_rand(100000, 999999);
				}
				
				$setRepArray[]	=	 $trackingnumber;
				$setRepArray[]	=	 $subSource;
				$setRepArray[]	=	 $CARTAS;
				$setRepArray[]	=	 $splitOrderDetail['MergeUpdate']['reg_num_img'];
				$setRepArray[]	=	 '<img src='.$barcodeCorriosPath.$corriosImage.'>';
				$setRepArray[]	=	($phone != '') ? 'Telefono del cliente:'.$phone : '';
				$setRepArray[]	=	$return_address;
				$setRepArray[]	=	WWW_ROOT;
				$cssPath = WWW_ROOT .'css/';
				$imgPath = Router::url('/', true) .'img/';

				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				 
				$html 	= $obj->setReplaceValueLabel( $setRepArray, $html );
				
				return $html;
		}
		
		
	public function getSlip($splitOrderId = null, $openOrderId = null)
	{
	
		$this->loadModel('OrderLocation');
		App::import('Controller', 'Linnworksapis');
		$obj = new LinnworksapisController();
		$order	=	$obj->getOpenOrderById( $openOrderId );
		
		$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
		$itemPrice			=		$getSplitOrder['MergeUpdate']['price'];
		$itemQuantity		=		$getSplitOrder['MergeUpdate']['quantity'];
		$BarcodeImage		=		$getSplitOrder['MergeUpdate']['order_barcode_image'];
		$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
		$assignedservice	=	 	$order['assigned_service'];
		$courier			=	 	$order['courier'];
		$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
		$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
		$barcode			=	 	$order['assign_barcode'];
		$subtotal			=		$order['totals_info']->Subtotal;
		$subsource			=		$order['sub_source'];
		$totlacharge		=		$order['totals_info']->TotalCharge;
		$ordernumber		=		$order['num_order_id'];
		$fullname			=		$order['customer_info']->Address->FullName;
		/*$address1			=		$order['customer_info']->Address->Address1;
		$address2			=		$order['customer_info']->Address->Address2;
		$address3			=		$order['customer_info']->Address->Address3;*/
		$address1 			= 		wordwrap($order['customer_info']->Address->Address1, 30, "\n<br>", false);	
		$address2 			= 		wordwrap($order['customer_info']->Address->Address2, 30, "\n<br>", false);	
		$address3			=		$order['customer_info']->Address->Address3;
		if(strlen($order['customer_info']->Address->Address3) > 30)	{
			$address3 		= 		wordwrap($order['customer_info']->Address->Address3, 30, "\n<br>", false);
		}
		$town				=	 	$order['customer_info']->Address->Town;
		$resion				=	 	$order['customer_info']->Address->Region;
		$postcode			=	 	$order['customer_info']->Address->PostCode;
		$country			=	 	$order['customer_info']->Address->Country;
		$phone				=	 	$order['customer_info']->Address->PhoneNumber;
		$company			=	 	$order['customer_info']->Address->Company;
		$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
		$postagecost		=	 	$order['totals_info']->PostageCost;
		$tax				=	 	$order['totals_info']->Tax;
		$barcode  			=   	$order['assign_barcode'];
		$items				=	 	$order['items'];
		$address			=		'';
		$address			.=		($company != '') ? $company.'<br>' : '' ; 
		$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
		$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
		$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
		$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
		$address			.=		($town != '') ? $town.'<br>' : '';
		$address			.=		($resion != '') ? $resion.'<br>' : '';
		$address			.=		($postcode != '') ? $postcode.'<br>' : '';
		$address			.=		($country != '' ) ? $country.'<br>' : '';
		$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);//CostBreaker
		
		$subsource_lower = strtolower($subsource) ;
		$marecArray = array('marec_de','marec_es','marec_fr','marec_it','marec_uk');
		$costbreakerArray = array('costbreaker_de','costbreaker_es','costbreaker_fr','costbreaker_it','costbreaker_uk','costbreaker','costbreaker_ca','costbreaker_usabuyer');
		$techdriveArray = array('tech_drive_de','tech_drive_es','tech_drive_fr','tech_drive_it','tech_drive_uk');				
		$rainbowArray = array('rainbow retail','rainbow retail de','rainbow_retail_es','rainbow_retail_it','rainbow_retail_fr');
		$bbdArray = array('bbd_eu_de');	
		
		if(in_array($subsource_lower,$marecArray)){
			$company 	=  'Marec';
		}else if(in_array($subsource_lower,$costbreakerArray)){
			$company 	=  'CostBreaker';
		}else if(in_array($subsource_lower,$techdriveArray)){
			$company 	= 'Tech Drive Supplies';
		}else if(in_array($subsource_lower,$rainbowArray)){
			$company 	= 'Rainbow Retail';
		}else if(in_array($subsource_lower,$bbdArray)){
			$company 	= 'BBD';
		}else if( $subsource == 'EBAY2' ){
			$company 	= 'EBAY2';
		}else if( $subsource == 'EBAY0' ){
			$company 	= 'EBAY0';
		}else if( $subsource == 'EBAY5' ){
			$company 	= 'Marec';
		}
		
		if($splitOrderId == '1668658-1'){
			//echo $company  ;exit;
		}
		
		$i = 1;
		$str = '';
		$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
		$totalGoods = 0;
		$productInstructions = '';
		$ref_code = trim($getSplitOrder['MergeUpdate']['provider_ref_code']);
		$codearray = array('DP1', 'FR7');
		
		$this->loadModel( 'BulkSlip' );
		if($subsource_lower == 'costbreaker_ca'){
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => 'CostBreaker_CA' ) ) );		
		}elseif($subsource_lower == 'costbreaker_usabuyer'){
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => 'CostBreaker_USA' ) ) );		
		}else{
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => $company ) ) );
		}
		 
		
		$p_barcode	=	'';
		
		if( in_array($ref_code, $codearray)) 
		{
			
			$per_item_pcost 	= $postagecost/count($items);
			$j = 1;
			foreach($items as $item){
			$item_title 		= 	$item->Title;
			$quantity			=	$item->Quantity;
			$price_per_unit		=	$item->PricePerUnit;
			$str .= '<tr>
						<td style="border:1px solid #000;" align="left" valign="top" width="10%">'.$quantity.'</td>
						<td style="border:1px solid #000;" valign="top" width="30%">'.substr($item_title, 0, 30 ).'</td>
						<td style="border:1px solid #000;" valign="top" width="10%">'.$quantity * $price_per_unit.'</td>
						<td style="border:1px solid #000;" valign="top" width="10%">'.$per_item_pcost.'</td>
						<td style="border:1px solid #000;" valign="top" width="20%">'.(($price_per_unit * $quantity) + $per_item_pcost).'</td>
					</tr>';
			$j++;
			}
			$str .=	'<tr><td></td><td></td><td></td><td style="border:1px solid #000;">Total</td>
						<td style="border:1px solid #000;" valign="top" width="20%">'.$totalcharge.'</td>
					</tr>';
			echo $str;
			
		} 
		else 
		{
			
			foreach( $skus as $sku )
			{
			
				$newSkus[]				=	 explode( 'XS-', $sku);
				$cat_name = array();
				$sub_can_name = array();
				foreach($newSkus as $newSku)
					{
						
						$getsku = 'S-'.$newSku[1];
						$getOrderDetail = 	$this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
						$OrderLocation 	= 	$this->OrderLocation->find( 'first', array( 'conditions' => array('order_id' => $openOrderId, 'sku' => $getsku ) )  );
						if( count($OrderLocation) > 0 ){
							$o_location 	=	$OrderLocation['OrderLocation']['bin_location'];
						} else {
							$o_location 	=	'Check Location' ;
						}
						
						$contentCat 	= 	$getOrderDetail['Product']['category_name'];
						$cat_name[] 	= 	$getOrderDetail['Product']['category_name'];
						$contentSubCat	=	$getOrderDetail['Product']['sub_category'];
						$sub_can_name[] =	$getOrderDetail['Product']['sub_category'];
						$productBarcode	=	$getOrderDetail['ProductDesc']['barcode'];
						$getContent	= $this->CategoryContant->find( 'first', array( 
															'conditions'=>array('CategoryContant.sub_category'=>$contentSubCat,'CategoryContant.sub_source'=>$subsource)));
						$productBarcode			=	$getOrderDetail['ProductDesc']['barcode'];
						if(count($getContent) > 0) {
							$getbarcode				= 	$this->getBarcode( $productBarcode );
							$productInstructions 	=  '<p style="font-size:14px;">'.$getContent['CategoryContant']['content'].'</p>';
						}
						
						$product_local_barcode = $this->GlobalBarcode->getLocalBarcode($getOrderDetail['ProductDesc']['barcode']);
						
						$title	=	$getOrderDetail['Product']['product_name'];
						$totalGoods = $totalGoods + $newSku[0];
						$str .= '<tr>
								<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
								<td valign="top" class="rightborder bottomborder">'.substr($title, 0, 35 ).'</td>
								<td valign="top" class="rightborder bottomborder" >'.$product_local_barcode.'</td>
								<td valign="top" class="rightborder bottomborder">'.$o_location.'</td>
								<td valign="top" class="center norightborder bottomborder">'.$newSku[0].'</td>';
						$str .= '</tr>';
						$i++;
						
					}
					unset($newSkus);
			}
			
		}
				
						
		/*$html 			=	$gethtml['PackagingSlip']['html'];
		$paperHeight    =   $gethtml['PackagingSlip']['paper_height'];
		$paperWidth  	=   $gethtml['PackagingSlip']['paper_width'];
		$barcodeHeight  =   $gethtml['PackagingSlip']['barcode_height'];
		$barcodeWidth   =   $gethtml['PackagingSlip']['barcode_width'];
		$paperMode      =   $gethtml['PackagingSlip']['paper_mode'];*/
		//
		if($splitOrderId == '1668658-1'){
			//pr($company );
		}
		$setRepArray = array();
		$setRepArray[] 					= $address1;
		$setRepArray[] 					= $address2;
		$setRepArray[] 					= $address3;
		$setRepArray[] 					= $town;
		$setRepArray[] 					= $resion;
		$setRepArray[] 					= $postcode;
		$setRepArray[] 					= $country;
		$setRepArray[] 					= $phone;
		$setRepArray[] 					= $ordernumber;
		$setRepArray[] 					= $courier;
		$setRepArray[] 					= $recivedate[0];
		$totalitem = $i - 1;
		$setRepArray[]	=	 $str;
		$setRepArray[]	=	 $totalGoods;
		$setRepArray[]	=	 $subtotal;
		$Path 			= 	'/wms/img/client/';
		$img			=	 '';
		$setRepArray[]	=	 $img;
		$setRepArray[]	=	 $postagecost;
		$setRepArray[]	=	 $tax;
		$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
		//$setRepArray[]	=	 $totalamount;
		$setRepArray[]	=	 $itemPrice;
		$setRepArray[]	=	 $address;
		$barcodePath  	=  WWW_ROOT.'img/orders/barcode/';
		$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.' width='. $barcodeWidth .'>';
		$barcodenum		=	explode('.', $barcode);
		
		$setRepArray[] 	=  $barcodeimg;
		$setRepArray[] 	=  $paymentmethod;
		$setRepArray[] 	=  $barcodenum[0];
		$setRepArray[] 	=  '';
		$img 			=	$gethtml['PackagingSlip']['image_name'];
		$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
		$setRepArray[] 	=  $returnaddress;
		$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
		$setRepArray[] 	= $splitOrderId;
		$setRepArray[] 	= 	utf8_decode( $productInstructions );
	//	$prodBarcodePath  	=  Router::url('/', true).'img/product/barcodes/';			
		$prodBarcodePath  	= WWW_ROOT.'img/product/barcodes/';
		$setRepArray[] 	=  '<img src='.$prodBarcodePath.$productBarcode.'.png width='. $barcodeWidth .'>';
		$setRepArray[] 	=  $totalamount;
		$setRepArray[]	=	WWW_ROOT;
		$imgPath = WWW_ROOT .'css/';
		$html2 			= 	$gethtml['BulkSlip']['html'];
		//$html2 			= 	'';
		if($splitOrderId == '1668658-1'){
			 //echo $html2;exit;
		}
		if( $company ==  'Marec' && in_array(  'Mobile Accessories', $cat_name ))
		{
			//$this->MobileAsseInstruction( $splitOrderId, $subsource  );
		}
		$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
		$html 		= 	$obj->setReplaceValue( $setRepArray, $html2 );
		return $html;
		
	}
		
		
	public function getBarcode( $barcode = null )
	{
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 
	
		$barcodePath 		= 	WWW_ROOT .'img/product/barcodes/';   
		$barcodecorreosPath = 	WWW_ROOT .'img/orders/correos_barcode/';  
		
		$colorFront 		= 	new BCGColor(0, 0, 0);
		$colorBack 			= 	new BCGColor(255, 255, 255);
		
		// Barcode Part
		$orderbarcode=$barcode;
		$code128 = new BCGcode128();
		$code128->setScale(2);
		$code128->setThickness(15);
		$code128->clearLabels( false );
		$code128->SetLabel($barcode);
		$code128->setForegroundColor($colorFront);
		$code128->setBackgroundColor($colorBack);
		$code128->parse($orderbarcode);
		// Drawing Part
		$imgOrder128=$orderbarcode;
		$imgOrder128path=$barcodePath.'/'.$imgOrder128.".png";
		$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
		$drawing128->setBarcode($code128);
		$drawing128->draw();
		$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
		############ Routing Barcode #######
		
	}	
	
	public function getPicklistBarcode( $picklist_barcode = null )
	{
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 
	
		$barcodePath 		= 	WWW_ROOT .'img/printPickList/'; 	
		$colorFront 		= 	new BCGColor(0, 0, 0);
		$colorBack 			= 	new BCGColor(255, 255, 255);
		
		// Barcode Part 
		 
		$code128 = new BCGcode128();
		$code128->setScale(2);
		$code128->setThickness(15);
		$code128->clearLabels( false );
		$code128->SetLabel($picklist_barcode);
		$code128->setForegroundColor($colorFront);
		$code128->setBackgroundColor($colorBack);
		$code128->parse($picklist_barcode);
		// Drawing Part
		 
		$imgOrder128path=$barcodePath.'/'.$picklist_barcode.".png";
		$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
		$drawing128->setBarcode($code128);
		$drawing128->draw();
		$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
		############ Routing Barcode #######
		
	}
		
	public function jerseyPostLabel( $splitOrderId = null, $openOrderId = null )
	{
		if($splitOrderId == '1680467-1'){
			echo "NNNNN";		 
			exit;
		}
		$openOrderId	=	$openOrderId ;
		$splitOrderId	=	$splitOrderId ;
		App::import('Controller', 'Linnworksapis');
		$obj = new LinnworksapisController();
		
		$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
	
		$order				=		$obj->getOpenOrderById( $openOrderId );
		 
		$postalservices		=		$order['shipping_info']->PostalServiceName;
		$destination		=		$order['customer_info']->Address->Country;
		$total				=		$order['totals_info']->TotalCharge ? $order['totals_info']->TotalCharge : $order['totals_info']->Subtotal;
		$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
		$assignedservice	=	 	$order['assigned_service'];
		$courier			=	 	$order['courier'];
		$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
		$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
		$barcode			=	 	$order['assign_barcode'];
		
		$subtotal			=		$order['totals_info']->Subtotal;
		$totlacharge		=		$order['totals_info']->TotalCharge;
		$ordernumber		=		$order['num_order_id'];
		$fullname			=		$order['customer_info']->Address->FullName;
		$address1			=		$order['customer_info']->Address->Address1;
		$address2			=		$order['customer_info']->Address->Address2;
		$address3			=		$order['customer_info']->Address->Address3;
		$town				=	 	$order['customer_info']->Address->Town;
		
		$resion				=	 	$order['customer_info']->Address->Region;
		$postcode			=	 	$order['customer_info']->Address->PostCode;
		$country			=	 	$order['customer_info']->Address->Country;
		$phone				=	 	$order['customer_info']->Address->PhoneNumber;
		$company			=	 	$order['customer_info']->Address->Company;
		$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
		$postagecost		=	 	$order['totals_info']->PostageCost;
		$tax				=	 	$order['totals_info']->Tax;
		$currency			=	 	$order['totals_info']->Currency;
		$barcode  			=   	$order['assign_barcode'];
		$items				=	 	$order['items'];
		$templateId			=	 	$order['template_id'];
		$subSource			=	 	$order['sub_source'];
		$envelope_weight	=	 	$splitOrderDetail['MergeUpdate']['envelope_weight'];
		
		$str = ''; 
		
		$totlaWeight	=	0;
		$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
		$ref_code = trim($splitOrderDetail['MergeUpdate']['provider_ref_code']);
		$weight = 0;
		foreach( $skus as $sku )
		{
			$newSkus[] = explode( 'XS-', $sku);

			
			foreach($newSkus as $newSku)
				{
					
					$totalvalue = $total;
					$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
					$getsku = 'S-'.$newSku[1];
					$qty = $newSku[0];
					$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
					$title	=	$getOrderDetail['Product']['product_name'];
					$weight	=	$weight + ($getOrderDetail['ProductDesc']['weight'] * $qty) ;
					$totlaWeight = $weight * $qty;
					$catName	 =	$getOrderDetail['Product']['category_name'];
					if($country == 'United States')
					{
						$str .= '<tr>
							<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.$catName.'</td>
							<td valign="top" class="center norightborder bottomborder">'.$qty * $getOrderDetail['ProductDesc']['weight'].'</td>';
						$str .= '</tr>';
					}
					elseif($country == 'Spain')
						{
							$codearray = array("DP1", "FR7");
							$spaincode = array("mad_air","mad_road","cat_air","cat_road","sp_air","sp_road");
					
							if(in_array($ref_code, $spaincode))
							{
								$str .='<tr>
										<td  width="60%"  style="border:1px solid #000; padding:5px;">'.$catName.'</td>
										<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty * $getOrderDetail['ProductDesc']['weight'].' Kg</td>
										<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty.'</td>';
								$str .= '</tr>';
							} else {
								$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$catName.'</td>
									<td valign="top" class="center noleftborder rightborder bottomborder">'.$qty.'</td>
									<td valign="top" class="center norightborder bottomborder">'.$qty *$getOrderDetail['ProductDesc']['weight'].' Kg</td>';
								$str .= '</tr>';
							}
						}
					else
					{
						$str .= '<tr>
								<td valign="top" class="noleftborder rightborder bottomborder">'.$qty.'X'.substr($title, 0, 25 ).'</td>
								<td valign="top" class="center norightborder bottomborder">'.$qty * $getOrderDetail['ProductDesc']['weight'].' Kg</td>';
						$str .= '</tr>';
					}
				}
					unset($newSkus);
		}
		
		$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forl?-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia');
		
		$addData['company'] 	= 	$company;
		$addData['fullname'] 	= 	$fullname;
		$addData['address1'] 	= 	$address1;
		$addData['address2'] 	= 	$address2;
		$addData['address3'] 	= 	$address3;
		$addData['town'] 		= 	$town;
		
		if( $country == 'Italy' )
		{
			if(array_key_exists(strtoupper($resion),$provinces)){
				$addData['resion'] = $resion;
			}else {
				$addData['resion'] = array_search(ucfirst($resion), $provinces);
			}
		} else {
				$addData['resion'] = $resion;
		}
		
		$addData['postcode'] 	= 	$postcode;
		$addData['country'] 	= 	$country;
		
		
		$address = $obj->getCountryAddress( $addData );
		
		$CARTAS = '';
		if(trim($country) == 'Spain') { 
			$CARTAS = 'CARTAS '.substr($postcode, 0, 1);
			$routingcode = $this->getCorreosBarcode( $postcode, $splitOrderId);
			$corriosImage		=		$routingcode.'.png';
			if(substr($postcode, 0, 1) == 0){
				$CARTAS = 'CARTAS '.substr($postcode, 1, 1);
			}	
		}
									
		$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
		$currentdate		=	 	date("j, F  Y");
		$BarcodeImage		=		$splitOrderDetail['MergeUpdate']['order_barcode_image'];
		//$corriosImage		=		$postcode.'90019.png';
		
		
		$setRepArray = array();
		$setRepArray[] 					= $address1;
		$setRepArray[] 					= $address2;
		$setRepArray[] 					= $address3;
		$setRepArray[] 					= $town;
		$setRepArray[] 					= $resion;
		$setRepArray[] 					= str_replace( 'Spain' , 'Spain', $address); 
		$setRepArray[] 					= $country;
		$barcode  						= $order['assign_barcode'];
		$barcodenum						= explode('.', $barcode);
		$barcodePath  					= Router::url('/', true).'img/orders/barcode/';
		//$barcodeCorriosPath  			= Router::url('/', true).'img/orders/correos_barcode/';	
		$barcodeCorriosPath  			= WWW_ROOT.'img/orders/correos_barcode/';
			
		
		$barcodenum						= explode('.', $barcode);
		
		/**************** for tempplate *******************/
		$countryArray = Configure::read('customCountry');
		if($country == 'United Kingdom'){
			$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
		}elseif($country == 'Spain'){
			$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
		} else if($country != 'United Kingdom'){
			$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Rest Of EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
		} else {
			$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Rest Of EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
		}
		
		
			if($splitOrderId == '1680467-1'){
				 pr($labelDetail);			 
				exit;
			}
		
			if($subSource == 'Marec_FR' || $subSource == 'Marec_DE' || $subSource == 'Marec_IT' || $subSource == 'Marec_ES' || $subSource == 'Marec_uk'){
				$company 	=  'Marec';
			} else if($subSource == 'CostBreaker_CA' || $subSource == 'CostBreaker_UK' || $subSource == 'CostBreaker_DE' || $subSource == 'CostBreaker_FR' || $subSource == 'CostBreaker_ES' || $subSource == 'CostBreaker_IT' || $subSource == 'CostBreaker_USABuyer' ){
				$company 	=  'CostBreaker';
			} else if($subSource == 'Tech_Drive_UK' || $subSource == 'Tech_Drive_FR' || $subSource == 'Tech_drive_ES' || $subSource == 'Tech_drive_DE' || $subSource == 'Tech_Drive_IT' ){
				$company 	= 'Tech Drive Supplies';
			} else if($subSource == 'RAINBOW RETAIL DE' || $subSource == 'Rainbow Retail' || $subSource == 'Rainbow_Retail_ES' || $subSource == 'Rainbow_Retail_IT'){
				$company 	= 'Rainbow Retail';
			}
			
			$this->loadModel( 'ReturnAddre' );
			$ret_add = $this->ReturnAddre->find('first', array( 'conditions' => array( 'company' =>  $company) ) );
			$return_address = $ret_add['ReturnAddre']['return_address'];
		
			$html           =  $labelDetail['BulkLabel']['html'];
			
		if($splitOrderId == '1680467-1'){
			echo '=====';
			echo $html;
			exit;
		}
		
		$barcodeimg = '<img src='.$barcodePath.$BarcodeImage.'>';
		
		$setRepArray[] 	=    $barcodeimg;
		$setRepArray[] 	=    $barcodenum[0];
		$setRepArray[]	=	 $str;
		$setRepArray[]	=	 $totlaWeight + $envelope_weight;
		$setRepArray[]	=	 $tax;
		$setRepArray[]	=	 $currentdate;
		$logopath		=	 Router::url('/', true).'img/';
		$setRepArray[]	=	 '<img src="'.$logopath.'logo.jpg" >';
		$logo			=	 '<img src="'.$logopath.'logo.jpg" >';
		$signature		=	 '<img src="'.$logopath.'sig.jpg" >';
		
		$setRepArray[]	=	 $logo;
		$setRepArray[]	=	 $signature;
		$setRepArray[]	=	 $splitOrderId;
		$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
		
		$tempId			=	$splitOrderDetail['MergeUpdate']['lable_id'];
		$countryCode	=	$splitOrderDetail['MergeUpdate']['country_code'];
		
		$setRepArray[]	=	 $countryCode;
		$setRepArray[]	=	 $weight;
		$conversionRate	=	Configure::read( 'conversionRate' );
		 
		if($currency == 'GBP')
		{
			$totalvalue = $totalvalue;
		}
		elseif($country == 'Spain' && $currency == 'EUR')
		{
			$totalvalue = $totalvalue;
		}
		elseif($country == 'United States')
		{
			$totalvalue = $totalvalue;
		}
		else
		{
			$totalvalue = number_format($totalvalue/$conversionRate, 2, '.' ,'');
		}
		
		if($totalvalue >= 1000  && $labelDetail['BulkLabel']['service_provider'] == 'Jersey Post' && $country == 'United States' ){
			$numbers = array("97.11","98.32","98.52","99.72","99.94","150.27","200.45","270.62","370.82","422.11","472.31","530.71");
			shuffle($numbers);					
			$totalvalue = $numbers[0];
		}
		else if($totalvalue >= 22  && $labelDetail['BulkLabel']['service_provider'] == 'Jersey Post' && $country != 'United States'){
			$numbers = array("18.11","18.22","18.32","18.52","18.72","18.94","19.27","19.45","19.62","19.82","20.11","20.31","20.71","21.22","21.41", "21.61","21.51");
			shuffle($numbers);					
			 	$totalvalue = $numbers[0];
		}
 
		$setRepArray[]	= $totalvalue;
		
		if($splitOrderDetail['MergeUpdate']['track_id'] != '')
		{
			$trackingnumber = $splitOrderDetail['MergeUpdate']['track_id'];
		}
		else
		{
			$trackingnumber = 'A'.mt_rand(100000, 999999);
		}
		 
		$setRepArray[]	=	 $trackingnumber;
		$setRepArray[]	=	 $subSource;
		$setRepArray[]	=	 $CARTAS;
		$setRepArray[]	=	 $splitOrderDetail['MergeUpdate']['reg_num_img'];
		$setRepArray[]	=	 '<img src='.$barcodeCorriosPath.$corriosImage.'>';
		$setRepArray[]	=	($phone != '') ? 'Telefono del cliente:'.$phone : '';
		$setRepArray[]	=	$return_address;
		$setRepArray[]	=	WWW_ROOT;
		$total_weight_envelope_weight = $totlaWeight + $envelope_weight;
		
		$cssPath = WWW_ROOT .'css/';
		$imgPath = Router::url('/', true) .'img/';
		//$sinimage	=	 '<img src='.$imgPath.'signature.png>';
		
		$this->loadModel( 'ReturnAddre' );
		$ret_add = $this->ReturnAddre->find('first', array( 'conditions' => array( 'company' =>  $company) ) );
		$return_address = $ret_add['ReturnAddre']['return_address'];
		$html = '<div style="font-family:normal;">
				 <table width=78%>
					<tr width=100%>
						<td width=50% style="border: 1px solid #ccc;padding-left:15px;">
							<table>
								<tr>									
									<td>	
										<div style="padding-left:3px" ><strong>Remitente</strong></div>				
										<div style="padding-left:3px" ><strong>ESL LOGISTICS</strong><br>CTL CHAMARTIN<br>AVDA. PIO XII 106-108<br>28070 MADRID</div>
									</td>
									<td>
										<table cellpadding="0" cellspacing="0" width="80%" style="border:1px solid #000;">
											<tr>					 
												<td>
													<div style="text-align: center;font-weight:bold; height:80px;">
														<div style="border-bottom:1px solid #000; padding:2px;"> FRANQUEO <br> PAGADO </div>
														<div style="padding:2px 5px;">Distribuci&#243;n <br> Internacional </div>
													</div>
												</td>				
											</tr>
										</table>
									</td>	
								</tr>
								<tr>
									<td align="left" colspan="2" > 
										<div style="border-bottom:1px solid; width:80%;font-size:12px; margin-top: 10px; "><strong>Destinatario</strong></div>		
										 <div style="font-size:13px; padding-left:3px">_ADDRESS_</div>
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center" style="padding-bottom:1px; padding-top:1px;"><div>ENV�O ORDINARIO</div> <div style="text-align: center;"><center> _BARCODEIMAGE_</center> <div></td>
								</tr> 
								<tr>
									<td colspan="2" align="center"><div style="margin-top:1px;">_CORRIOSBARCODE_</div></td>
								</tr> 
							</table>
						</td>
						
						<td width=50% style="border: 1px solid #ccc" valign="top">
							<table style="border:1px solid #000;">
								<tr valign="top">
									<td width=48%><strong>CUSTOMS DECLARATION</strong></td>
									<td style="text-align:right;"><strong>CN 22</strong></td>
								</tr>
								<tr valign="top">
									<td width=48% style="font-size:10px;">DECLARATION EN DOUANE</td>
									<td style="text-align:right;">May be opened officially</td>
								</tr>
								<tr>
									<td>Great Britain / Important!</td>
									<td style="text-align:right;"></td>
								</tr>
								<tr>
									<td width="15%"><span style="height:25;width:25; border:2px solid gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Gift</td>
									<td width="25%"><span style="height:25;width:25; border:2px solid gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Documents</td>
								</tr>
								<tr>
									<td width="30%"><span style="height:25;width:25; border:2px solid gray">&nbsp;X&nbsp;</span>Commercial</td>
									<td width="30%"><span style="height:25;width:25; border:2px solid gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Merchandise</td>
								</tr>
							</table>
							<table class="" cellpadding="1px" cellspacing="0" style="border-collapse:collapse" width="100%">
								<tr>
									<th width="60%" style="border:1px solid #000; padding:5px;">Quantity and detailed</th>
									<th valign="top" width="20%"  style="border:1px solid #000; padding:5px;">Weight</th>
									<th valign="top" width="20%"  style="border:1px solid #000; padding:5px;">Qty</th>
								</tr>
								_ORDERDETAIL_
								
								<tr>
									<td  width="60%" style="border:1px solid #000; padding:10px; padding:5px;"> <span class="bold">For commercial items only</td>
									<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">WEIGHT '.$total_weight_envelope_weight.' Kg</td>
									<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">VALUE '.$currency.' _TOTALVALUE_</td>
								</tr>
								</table>
									<div class="fullwidth" style="border:1px solid #000; padding:5px;"><p>I the undersigned, whose name and address are given on the item, certify that the particulars given in this declaration are correct and that this item does not contain any dangerous article or articles prohibited by legislation or by postal or customs regulations.</p>
								<table>
								<tr>
									<td class="date bold">_CURRENTDATE_</td>
									<td><img src="_DYURL_/img/signature.png" height="50px"></td>
								</tr>
								</table>	
								
								</table>
						</td>
					</tr>
				 </table></div>';	
		$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
		
 		
		$html 	= $obj->setReplaceValueLabel( $setRepArray, $html );
		return $html;
	}
		
		public function getCorreosBarcode( $postcode = null, $splitOrderID = null )
		{
			
			
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 

			$barcodePath 		= 	WWW_ROOT .'img/orders/barcode/';   
			$barcodecorreosPath = 	WWW_ROOT .'img/orders/correos_barcode/';  
			
			$colorFront 		= 	new BCGColor(0, 0, 0);
			$colorBack 			= 	new BCGColor(255, 255, 255);
			
			// Barcode Part
			$orderbarcode=$splitOrderID;
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->clearLabels();
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->parse($orderbarcode);
			// Drawing Part
			$imgOrder128=$orderbarcode;
			$imgOrder128path=$barcodePath.'/'.$imgOrder128.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			############ Routing Barcode #######
			//$destination = $countryCode;
			//$origin ="90019";
			//$routingcode=$destination.$origin;
			
			 
			 
			$routingcode = $this->getCorreosCalculatedBarcode($postcode);
			$routingcode128 = new BCGcode128();
			$routingcode128->setScale(1);
			$routingcode128->setThickness(40);
			$routingcode128->setForegroundColor($colorFront);
			$routingcode128->setBackgroundColor($colorBack);
			$routingcode128->parse(array(CODE128_B, $routingcode));
			//$routingcode128->parse($routingcode);
			// Drawing Part
			$routingimg =$routingcode;
			$routingdrawing128path=$barcodecorreosPath.'/'.$routingimg.".png";
			$routingdrawing128 = new BCGDrawing($routingdrawing128path, $colorBack);
			$routingdrawing128->setBarcode($routingcode128);
			$routingdrawing128->draw();
			$routingdrawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			return $routingcode;
		}
		
		public function getCorreosCalculatedBarcode($postcode = null)
		 {
			$sum_accii = 0;
			
			$code = 'ORDCP'.$postcode; 		
			for($i=0; $i < strlen($code); $i++){
			  $sum_accii += ord($code[$i]);		 
			}
			$remainder = fmod($sum_accii,23);
			$table = array('T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E');
			$control_character = $table[$remainder];
			return	$code.$control_character;			
		} 
		
		public function royalMailLabel( $split_id = null, $order_id = null )
		{
			//.$details['split_order_id'].
			$details = $this->getDetail( $split_id, $order_id );
			
			$imgPath = WWW_ROOT.'royalmail/barcode/2d_matrix_'.$details['split_order_id'].'.png';
			
			if(file_exists($imgPath) &&  filesize($imgPath) > 100 )
			{
 				$service = '48';
				if($details['type'] == 1 ){
					$service = '24';
				}
				
				$box_number = 824; 
				if(strpos($details['sub_source'],'CostBreaker')!== false){
					$box_number = 824;
				}
				else if(strpos($details['sub_source'],'RAINBOW')!== false){
					$box_number = 825;;
				}else if(strpos($details['sub_source'],'Marec')!== false){
					$box_number = 826;
				}	
				elseif(strpos($details['sub_source'],'BBD')!== false){
					$box_number = 827;
				}
				else if(strpos($details['sub_source'],'Tech_Drive')!== false){
					$box_number = 828;
				} 
				$html = '
						<div style=" width:320px; height:auto;margin-top:-270px; margin-left:-200px;">
						<div class="container"  style=" border:1px solid #000;">
						<table  width="100%" cellpadding="0" cellspacing="10">
						<tbody>
						<tr>
						<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/Royal_Mail.png" width="115px"></td>
						<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/'.$service.'.jpg" width="50px"></td>
						<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/royal.png" width="100px" height="70px"></td>
						</tr>
						</tbody>
						</table>
						 
						
						<table width="100%" cellpadding="0" cellspacing="10" style="border-bottom:1px solid #000; border-top:1px solid #000;">
						<tbody>
						<tr>
						<td valign="top" width="50%" align="left" >
						<img src="'.WWW_ROOT.'royalmail/barcode/2d_matrix_'.$details['split_order_id'].'.png" width="83px" height="83px" style="margin-left:10px;">
						</td>
					
						</tr>
						</tbody>
						</table>
						
						<table width="100%"  cellpadding="0" cellspacing="10" >
						<tbody>
						<tr>
						<td valign="top" width="75%">';
						$html .='<strong>'. ucwords(strtolower($details['FullName'])).'</strong><br>';
			
						if(strlen($details['Address1']) > 25 || strlen($details['Address2']) > 25){
						
							$lines = explode("\n", wordwrap(htmlentities($details['Address1']) .' '.htmlentities($details['Address2']), '25'));
							if(isset($lines[0]) && $lines[0] != ''){
								$html .=  $lines[0].'<br>';
							}
							if(isset($lines[1]) && $lines[1] != ''){
								$html .=  $lines[1].'<br>';
							}
							if(isset($lines[2]) && $lines[2] != ''){
								$html .=  $lines[2].'<br>';
							}
						
						}else{
							$html .= ucfirst($details['Address1']).'<br>';
							if($details['Address2']) $html .= ucfirst($details['Address2']).'<br>';
						}
						
						
						$html .= ucfirst($details['Town']).'<br>';
						$html .= $details['PostCode'];
						$html .= '</div></td>';
						
						
						$html .='<td valign="bottom" width="25%" align="left">
						<img src="'.WWW_ROOT.'royalmail/img/'.$box_number.'.png" style="height:100px; margin-left:20px;">
						 </td>
						 </tr>
						</tbody>   
						 </table>
						
						 
						<table width="100%" style="border-top:1px solid #000;" >
						<tbody>
						<tr>
						<td valign="top" align="center" style="padding-top:5px;"><p style="font-family:chevin; font-size:12px;">Customer Reference:'.$details['split_order_id'].'</p>
						
						</td>
						</tr>
						</tbody>
						</table>
						 </div>
						 <div  style="text-align:center;width:100%;padding-top:25px;">
							<img src="'.WWW_ROOT.'img/orders/barcode/'.$details['split_order_id'].'.png" style="text-align:center">
						 </div></div>';
						return $html;
					}else{
					return 'error';
				}
		}
		
		public function getDetail( $split_id = null, $order_id = null )
		{
			
			$this->loadModel( 'Country' );
			App::import( 'Controller' , 'Cronjobs' );		
			$objController 	= new CronjobsController();
			$getorderDetail	= $objController->getOpenOrderById($order_id);
			$cInfo = $getorderDetail['customer_info'];
			$Address1 = $cInfo->Address->Address1;
			$Address2 = $cInfo->Address->Address2;
			$order->TotalsInfo = $getorderDetail['totals_info'];
			$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
			
			$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_id,'MergeUpdate.service_provider' => 'Royalmail')));	
			$type = 2; 
			if($orderItem['MergeUpdate']['service_name'] == 'royalmail_24'){
				$type = 1;
			}
			$service_format = 'P';
			if($orderItem['MergeUpdate']['service_name'] == 'royalmail_LL_48'){
				/*  F	Inland Large Letter, L	Inland Letter, N	Inland format Not Applicable, P	Inland Parcel */
				$service_format = 'F';
			}
			
			$_details = array('merge_id'=> $order_id,
						'split_order_id'=> $split_id,
						'sub_total' => number_format($order->TotalsInfo->Subtotal,2),
						'order_total'=> number_format($order->TotalsInfo->TotalCharge,2),
						'postage_cost' => number_format($order->TotalsInfo->PostageCost,2),
						'order_currency'=>$order->TotalsInfo->Currency,			
						'Company'=>$cInfo->Address->Company,
						'FullName'=> utf8_decode($cInfo->Address->FullName),
						'Address1' =>utf8_decode($Address1),
						'Address2' =>utf8_decode($Address2),
						'Town' =>utf8_decode($cInfo->Address->Town),
						'Region' =>utf8_decode($cInfo->Address->Region),
						'PostCode' =>$cInfo->Address->PostCode,
						'CountryName'=>$cInfo->Address->Country,
						'CountryCode' =>$country_data['Country']['iso_2'],
						'PhoneNumber'=>$cInfo->Address->PhoneNumber	,
						'sub_source' => $getorderDetail['sub_source'],
						'type' 		=> $type,
						'service_format' => $service_format,
						'item_count'=> 1,
						'building_name' => '',
						'building_number' => 0,
					);
					return $_details;
			
		}
		
		
		public function batckPicklistBulkOrderProcess($picklist_id = 0, $page = '')
		{
			$this->layout = '';
			$this->autoRander = false;
			$this->loadModel('OpenOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('ScanOrder');
			$this->loadModel('OrderLocation');
 			$ord_msg    =   array();
			$print_msg  =   '';	
			$ids 	    =   array();			
			$userid		=	$_SESSION['Auth']['User']['id'];
			$firstName 	= 	( $_SESSION['Auth']['User']['first_name'] != '' ) ? $_SESSION['Auth']['User']['first_name'] : '_';
			$lastName 	= 	( $_SESSION['Auth']['User']['last_name'] != '' ) ? $_SESSION['Auth']['User']['last_name'] : '_';
			$pcName 	= 	( $_SESSION['Auth']['User']['pc_name'] != '' ) ? $_SESSION['Auth']['User']['pc_name'] : '_';
			
			$batch_num  =   'BPLN'.date('YmdHis');
			$username   =   $firstName.' '.$lastName;
		 
			if(isset($this->request->data['picklist_inc_id']) && $this->request->data['picklist_inc_id'] > 0){
  				$picklist_id	=	$this->request->data['picklist_inc_id'];
 			}
			
			if($picklist_id > 0){
				$batch_orders	=	$this->MergeUpdate->find('all', array('fields'=>['id','product_order_id_identify'],  'conditions'  => array( 'picklist_inc_id' => $picklist_id, 'status' => 0,'pick_list_status' => 1,'over_sell_user IS NULL'  ) ) );
								
				foreach( $batch_orders as $batch_order )
				{
					$ids[]	=	$batch_order['MergeUpdate']['product_order_id_identify'];
				}
			}
			
			if(isset($this->request->data['split_order_id']) && $this->request->data['split_order_id'] != ''){
				$ids[]	= $this->request->data['split_order_id'];
			}
			$over_sell = [] ;
			
			$location_orders =	$this->OrderLocation->find('all', array('fields'=>['split_order_id'], 'conditions'  => array( 'picklist_inc_id' => $picklist_id, 'status' => 'active','pick_list' => 1,'available_qty_bin' => 0 ) ) ); 
			
			if(count($location_orders) > 0 ){
				foreach($location_orders as $l){
					$over_sell[] = $l['OrderLocation']['split_order_id'];
				}			
			}
			 
 			if(count($ids) > 0){
			
				foreach($ids as $id)
				{
					$openOrderId	=	explode("-",$id)[0];
					$splitOrderId	=	$id;
					$console 		=   'dynamic_picklist_console';
					
					if(count($over_sell) > 0){
						$over_sell[] = '111111-1';
						$_check	= $this->MergeUpdate->find('first', array('fields'=>['id','order_id','product_order_id_identify'], 'conditions' => ['label_status' => '2', 'product_order_id_identify' => $splitOrderId, 'product_order_id_identify NOT IN '=>  $over_sell,'over_sell_user IS NULL'] ) );
					
					}else{
						$_check	= $this->MergeUpdate->find('first', array('fields'=>['id','order_id','product_order_id_identify'], 'conditions' =>[ 'label_status' => '2', 'product_order_id_identify' => $splitOrderId, 'over_sell_user IS NULL'] ) ); 
					
					}
					
					 
					if( count($_check) > 0 )
					{
 						$this->MergeUpdate->updateAll(
								array('MergeUpdate.dispatch_console' => "'".$console."'",'MergeUpdate.assign_user' => "'".$username."'", 'MergeUpdate.user_name' => "'".$userid."'", 'MergeUpdate.pc_name' =>"'".$pcName."'",  'MergeUpdate.status' => '1','MergeUpdate.manifest_status' => '2','MergeUpdate.process_date' =>"'".date('Y-m-d H:i:s')."'", 'MergeUpdate.user_id' => $userid),
								 array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId,'MergeUpdate.status' => 0,'MergeUpdate.pick_list_status' => 1));
						 
  					
						App::import('Controller', 'Cronjobs');
						$cronjobtModel = new CronjobsController();
						$cronjobtModel->call_service_counter( $splitOrderId );
						 
 						$getmergeOrders	=	$this->MergeUpdate->find( 'all', array('conditions' => array( 'MergeUpdate.order_id' =>  $openOrderId )) );
						$checkStatus = 0;
						$checkNonProcessStatus = 0;
						
						foreach( $getmergeOrders as $getmergeOrder )
						{
							if( $getmergeOrder['MergeUpdate']['status'] == 0 ){
								$checkNonProcessStatus++;
							}else{
								$checkStatus++;
							}
						}
						
						if($checkNonProcessStatus == 0 && $checkStatus > 0)
						{
							$this->OpenOrder->updateAll(array('OpenOrder.status' => '1','OpenOrder.process_date' => "'".date('Y-m-d H:i:s')."'" ), array('OpenOrder.num_order_id' => $openOrderId));
							$scan_orders	=	$this->ScanOrder->find( 'all', array( 'conditions' => array( 'split_order_id' => $splitOrderId ) ) );
							
							foreach( $scan_orders as $scan_order)
							{
								$data['update_quantity'] 	= 	$scan_order['ScanOrder']['quantity'];
								$data['id'] 				= 	$scan_order['ScanOrder']['id'];
								$this->ScanOrder->saveAll( $data );
							}
							
							$log_file = WWW_ROOT .'logs/dynamic_log/dynamic_process_'.date('dmy').".log";
 							if(!file_exists($log_file)){
								file_put_contents($log_file, 'Order id'."\t".'PickList'."\t".'User'."\t".'DateTime'."\n");	
							}
 			  
							file_put_contents($log_file, $splitOrderId."\t".$picklist_id."\t".$_SESSION['Auth']['User']['first_name'].' '.$_SESSION['Auth']['User']['last_name']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND | LOCK_EX);
							
							$ord_msg[] = "Order {$splitOrderId} is processed.";
							$msg['status'] = 'ok';
						}
						else
						{
							$msg['con'] = "2";
							$ord_msg[] = "Order {$splitOrderId} is already processed.";
						}
						
 			  	}else{				
					$print_msg = "Please print Label & Slip before processing.";
				}
				
			   }
			}else{				
				$ord_msg[]="No orders found for processing.";
			}
			
 			$log_file = WWW_ROOT .'logs/dynamic_log/process_'.date('dmy').".log";
		
			if(!file_exists($log_file)){
				file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
			}
			file_put_contents($log_file, $_SESSION['Auth']['User']['email']."\t".$_SESSION['Auth']['User']['username']."\t".$_SESSION['Auth']['User']['pc_name']."\t".$picklist_id."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
		
			if($print_msg != ''){
				$msg['msg'] = $print_msg;
			}else{
				$msg['msg'] = implode(",",$ord_msg);
			}
			if($page == 'sorting'){
				return $msg;
			}else{
				echo json_encode($msg);				 
			}
			exit;			 
		}
		
 		public function SortSingleOrder()
		{
			$this->layout = '';
			$this->autoRander = false;
		
  			if(isset($this->request->data['split_order_id']) && $this->request->data['split_order_id'] != ''){
			
				$this->loadModel('MergeUpdate');
				App::import('Controller', 'Sortingstations');
				$sortobj = new SortingstationsController();
 				
				$split_order_id = $this->request->data['split_order_id'];
 				
				$log_file = WWW_ROOT .'logs/dynamic_log/sort_single_order_'.date('dmy').".log";
 				if(!file_exists($log_file)){
					file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'Split Ordrer Id'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
				}
				file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$this->request->data['split_order_id']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
				
				$sortobj->checkBarcodeForSortingOperator( $split_order_id );
				 
				echo "1";
				exit;
			} else {
				echo  "2"; 				 
				exit;
			}
			
		}
		
		public function sortBatchAllOrders($picklist_inc_id = null,$page = '')
		{
			$this->layout = '';
			$this->autoRander = false;
			$this->loadModel('MergeUpdate');
 			 
			if(isset($this->request->data['picklist_inc_id']) && $this->request->data['picklist_inc_id'] > 0){
				$picklist_inc_id = $this->request->data['picklist_inc_id'];
			}
			
			$log_file = WWW_ROOT .'logs/dynamic_log/sort_batch_all_orders_req_'.date('dmy').".log";
		
			if(!file_exists($log_file)){
				file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'PicklistIncId'."\t".'DateTime'."\n");	
			}
 			   			
			file_put_contents($log_file, $_SESSION['Auth']['User']['email']."\t".$_SESSION['Auth']['User']['username']."\t".$_SESSION['Auth']['User']['pc_name']."\t".$picklist_inc_id	."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
			
			$ord_msg = $this->sortbatchorders( $picklist_inc_id );
 			 
			if(count($ord_msg) > 1){
				$msg['status'] = 'ok';
			}
			
			$msg['msg'] = implode(",",$ord_msg);
			if($page == 'sorting'){
				return $msg;
			}else{
				echo json_encode($msg);
			}
			exit;
		}
		
		public function sortbatchorders( $picklist_inc_id = null)
		{
			$this->autoRender = false;
			$this->layout = '';
			$this->loadModel('MergeUpdate');		 			
			$this->loadModel('ServiceCounter' );
			$ord_msg = []; 	
 		
			$results = $this->MergeUpdate->find('all', 
						array('conditions' => array('MergeUpdate.picklist_inc_id'=>$picklist_inc_id, 'MergeUpdate.status' => '1' , 
													'MergeUpdate.sorted_scanned' => '0' , 
													'MergeUpdate.scan_status' => '0')));
			 
			if(count($results) > 0 )
			{
				foreach($results as $result)
				{
					$merge_inc_id 		= $result['MergeUpdate']['id'];
					$serviceName 		= $result['MergeUpdate']['service_name'];
					$serviceProvider 	= $result['MergeUpdate']['service_provider'];
					$serviceCode 		= $result['MergeUpdate']['provider_ref_code'];
					$country 			= $result['MergeUpdate']['delevery_country'];
					$serviceid 			= $result['MergeUpdate']['service_id'];
					
					if($serviceProvider == 'DHL'){
						$getCounterValue = $this->ServiceCounter->find( 'first' , array( 'conditions' => array('ServiceCounter.service_id' => $serviceid ) ) );
					}else{
						$getCounterValue = $this->ServiceCounter->find( 'first' , 
						array( 'conditions' => array( 'ServiceCounter.destination' => $country,'ServiceCounter.service_id' => $serviceid )) );
					}
					
					$counter = 0;
					if( $getCounterValue['ServiceCounter']['counter'] > 0 ){
						$counter = $getCounterValue['ServiceCounter']['counter'];
						$counter = $counter + 1;
					} else {
						$counter = $counter + 1;
					}
					
					$orderCommaSeperated = '';
					if( $getCounterValue['ServiceCounter']['order_ids'] != '' ){
						$orderCommaSeperated = $getCounterValue['ServiceCounter']['order_ids'];
						$orderCommaSeperated = $orderCommaSeperated.','.$merge_inc_id;
					} else {
						$orderCommaSeperated = $merge_inc_id;
					}
					
					$ServiceCounter['id'] 		= $getCounterValue['ServiceCounter']['id'];      
					$ServiceCounter['counter'] = $counter;
					$ServiceCounter['order_ids'] = $orderCommaSeperated; 					     
					$this->ServiceCounter->saveAll( $ServiceCounter );

					$MergeUpdate['id'] = $merge_inc_id;            
					$MergeUpdate['sorted_scanned'] = 1;         
					$MergeUpdate['scan_status'] = 1;  
					$MergeUpdate['scan_date'] = date('Y-m-d H:i:s');  
 					$MergeUpdate['scanned_user'] = $_SESSION['Auth']['User']['first_name'].' '.$_SESSION['Auth']['User']['last_name'];  
					$MergeUpdate['scanned_pc'] = $_SESSION['Auth']['User']['pc_name'];  					 
					$this->MergeUpdate->saveAll( $MergeUpdate );
				   
				  	$log_file = WWW_ROOT .'logs/dynamic_log/sort_batch_all_orders_'.date('dmy').".log";
				  
				    if(!file_exists($log_file)){
  						file_put_contents($log_file, 'Split_Order_Id'."\t".'Service id'."\t".'Service name'."\t".'Service Provider'."\t".'Service code'."\t".'picklist_inc_id'."t".'User'."\t".'PcName'."\t".'DateTime'."\n");	
					}		
								
					file_put_contents($log_file, $result['MergeUpdate']['product_order_id_identify']."\t".$getCounterValue['ServiceCounter']['id']."\t".$result['MergeUpdate']['service_name']."\t".$result['MergeUpdate']['service_provider']."\t".$result['MergeUpdate']['provider_ref_code']."\t".$picklist_inc_id."\t".$_SESSION['Auth']['User']['first_name'].' '.$_SESSION['Auth']['User']['last_name']."\t".$_SESSION['Auth']['User']['pc_name']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND | LOCK_EX);
					
					$ord_msg[] = $result['MergeUpdate']['product_order_id_identify']. " is sored";
					
				}
				 
			}else{
				$ord_msg[] = "No order found for sorting.";
			}
			
			return $ord_msg;
		} 
		
		public function MarkOverSell()
		{
			$this->layout = '';
			$this->autoRander = false;			 
			$this->loadModel('MergeUpdate'); 
		 
			$firstName 	= 	( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName 	= 	( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_'; 
 			$username   =   $firstName.' '.$lastName;  
								
			if(isset($this->request->data['split_order_id']) && $this->request->data['split_order_id'] != ''){
 				$this->MergeUpdate->updateAll(
								array('MergeUpdate.over_sell_user' => "'".$username.'|'.date('Y-m-d H:i:s')."'"), array( 'MergeUpdate.product_order_id_identify' => $this->request->data['split_order_id']));
 			} 
 			
 			$log_file = WWW_ROOT .'logs/dynamic_log/over_sell_user_'.date('dmy').".log";
		
			if(!file_exists($log_file)){
				file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'split_order_id'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
			}
			file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$this->request->data['split_order_id']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
		
		    $msg['msg'] = "Order is marked as oversell.";
			echo json_encode($msg);
			exit;			 
		}
		
		public function batch() 
		{
		
 			$this->layout = 'index';
			
			$this->loadModel( 'User' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'DynamicBatche' );
			
			
			$perPage	=	 50;
			$this->paginate = array('order'=>'id DESC','limit' => $perPage);
			$dynamic_batches = $this->paginate('DynamicBatche');
			
			
			$open_orders 	= $this->OpenOrder->find('count', array( 'conditions' => array( 'status' => 0 )) );
			$open_orders_merge 	= $this->MergeUpdate->find('count', array( 'conditions' => array( 'status' => 0 ) ) );
			$batch_orders 	= $this->MergeUpdate->find('count', array( 'conditions' => array( 'status' => 0, 'created_batch !=' => null ) ) );
			$readyforbatch 	= $this->MergeUpdate->find('count', array( 'conditions' => array( 'status' => 0, 'pick_list_status' => 0, 'created_batch' => null ) ) );
			
			//echo $count_nonbatch_orders;
			$o_data['op_ord_count'] 		= $open_orders;
			$o_data['bat_ord_count'] 		= $batch_orders;
			$o_data['open_orders_merge'] 	= $open_orders_merge;
			$o_data['remain_ord_count'] 	= $open_orders_merge	-	$batch_orders;
			$o_data['readyforbatch'] 		= $readyforbatch;
			
 			/******************Dynamic Batch****************/
			$this->set( compact('o_data'));
			
			$this->set( 'dynamic_batches',$dynamic_batches);
			
			
		}
		 
		
		public function batchTest() 
		{
		
 			$this->layout = 'index';
			
			$this->loadModel( 'User' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'DynamicBatche' );
			
			
			$perPage	=	 50;
			$this->paginate = array('order'=>'id DESC','limit' => $perPage);
			$dynamic_batches = $this->paginate('DynamicBatche');
			
			
			$open_orders 	= $this->OpenOrder->find('count', array( 'conditions' => array( 'status' => 0 )) );
			$open_orders_merge 	= $this->MergeUpdate->find('count', array( 'conditions' => array( 'status' => 0 ) ) );
			$batch_orders 	= $this->MergeUpdate->find('count', array( 'conditions' => array( 'status' => 0, 'created_batch !=' => null ) ) );
			$readyforbatch 	= $this->MergeUpdate->find('count', array( 'conditions' => array( 'status' => 0, 'pick_list_status' => 0, 'created_batch' => null ) ) );
			
			//echo $count_nonbatch_orders;
			$o_data['op_ord_count'] 		= $open_orders;
			$o_data['bat_ord_count'] 		= $batch_orders;
			$o_data['open_orders_merge'] 	= $open_orders_merge;
			$o_data['remain_ord_count'] 	= $open_orders_merge	-	$batch_orders;
			$o_data['readyforbatch'] 		= $readyforbatch;
			
			
			
			/******************Dynamic Batch****************/
			$this->set( compact('o_data'));
			
			$this->set( 'dynamic_batches',$dynamic_batches);
			
			
		}
		
		public function batchOrders( $batch = 0) 
		{
			$this->loadModel( 'MergeUpdate' );
			 
			$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('sku','batch_num','scan_status', 'sorted_scanned', 'manifest_status', 'status', 'batch_num', 'dispatch_console', 'created_batch', 'label_status', 'pick_list_status', 'order_id', 'product_order_id_identify', 'service_name', 'provider_ref_code', 'service_provider', 'delevery_country', 'postal_service', 'country_code', 'user_id', 'assign_user', 'picked_date', 'process_date', 'scan_date', 'manifest_date','brt','picklist_inc_id','pick_list_status' ),'conditions' => array('MergeUpdate.created_batch' => $batch),'order'=>'picklist_inc_id'
			));
			
			return $orderItems;
 			
		}
		
		
		public function batchPicklist( $batch = null, $country = null, $service_name = null) 
		{
			$this->loadModel( 'DynamicPicklist' );
			
			$pickItems = $this->DynamicPicklist->find('all', array('fields'=>array('picklist_name'),'conditions' => array('batch' => $batch,'country' => $country,'service_name' => $service_name,'created_from' => 'batch_page')
			));
			 
			return $pickItems;
			
 		}
		
		public function expressPicklist( $batch = null) 
		{
			$this->loadModel( 'DynamicPicklist' );
			
			$pickItems = $this->DynamicPicklist->find('first', array('fields'=>array('picklist_name'),'conditions' => array('batch' => $batch,'created_from' => 'batch_page')
			));
			 
			return $pickItems;
			
 		}
		 
		public function createBatch()
		{
			$this->loadModel( 'MergeUpdate' ); 
			$this->loadModel( 'DynamicBatche' );
			
			$batch_num	= $this->request->data['o_f_batch'];
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
			
			$username = $firstName.' '.$lastName;
 			
			$log_file = WWW_ROOT .'logs/dynamic_log/create_batch_'.date('dmy').".log";
		
			if(!file_exists($log_file)){
				file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'batch_num'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
			}
			file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$batch_num."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
			
			
			$get_o	=	$this->MergeUpdate->find('all', array( 'conditions' => array('status' => 0, 'pick_list_status' => 0, 'created_batch' => null), 'limit' => $batch_num, 'order' => 'merge_order_date ASC' ) );
			
			$batch_num = date('YmdHis');
			
			foreach($get_o as $o)
			{
				$s_o_ids[]	=	$o['MergeUpdate']['product_order_id_identify'];
			}
			
 			
			if( count( $s_o_ids ) > 0 )
			{
				$result	=	$this->MergeUpdate->updateAll(
								array('MergeUpdate.created_batch' => "'".$batch_num."'"), 
								array('MergeUpdate.product_order_id_identify IN' => $s_o_ids));
				$data['batch_name'] 	= $batch_num;
				$data['pc_name'] 		= $pcName;
				$data['order_count'] 	= count( $s_o_ids );
				$data['creater_name'] 	= $username;
				$data['created_date'] 	= date('Y-m-d H:i:s');
				$this->DynamicBatche->saveAll($data);
				
				App::import('Controller' , 'Cronjobs');		
				$mpObj = new CronjobsController();	
  				$mpObj->order_location_update( $batch_num );	
				$mpObj->order_location_up();

				echo "1";
				exit;
			} 
			else
			{
				echo "2";
				exit;
			}
		}
		
		public function markcompletebatch()
		{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'DynamicBatche' );
			
			$batch_num	= $this->request->data['batnum'];
			
 			$orders	= $this->MergeUpdate->find('all', array( 'conditions' => array('created_batch' => $batch_num) ) );
			$processed = $scan_status = 0;
			foreach($orders as $order){
			
				if($order['MergeUpdate']['status'] == 1){
					$processed++;
				}
				if($order['MergeUpdate']['scan_status'] == 1){
					$scan_status++;
				}
			}
			
			if(count($orders) == $processed && count($orders) == $scan_status){
 			
 				$this->DynamicBatche->updateAll( array('status' => 2,'completed_date' => "'".date('Y-m-d H:i:s')."'"),array('batch_name' => $batch_num));	
				
				$log_file = WWW_ROOT .'logs/dynamic_log/markcomplete_batch_'.date('dmy').".log";
			
				if(!file_exists($log_file)){
					file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'batch_num'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
				}
				file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$batch_num."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
				
				$msg['status'] =  'ok';
			
			}else{
				$msg['status'] =  'You can not mark completed. Total Orders:'.count($orders) .' Processed orders :'. $processed .' Scan orders '. $scan_status;
			}
			echo json_encode($msg);
			exit;
		}
		
		public function getOrderStatusQty( $b_num = null )
		{
				$this->loadModel( 'MergeUpdate' );
				$b_to_ords	=	$this->MergeUpdate->find( 'all', array( 'conditions' => array( 'MergeUpdate.created_batch' => $b_num ) ) );
				$o_count = 0;$p_count = 0;$c_count = 0;$l_count = 0;
				$statusarr = array();
				$o_r_count = 0;
				foreach($b_to_ords as $b_to_ord)
				{	
					if( $b_to_ord['MergeUpdate']['status'] == 0 && $b_to_ord['MergeUpdate']['pick_list_status'] == 0 ){
						$o_r_count = $o_r_count + 1;
					}	
				
					if( $b_to_ord['MergeUpdate']['status'] == 0 ){
						$o_count = $o_count + 1;
					} else if( $b_to_ord['MergeUpdate']['status'] == 1 ){
						$b_to_ord['MergeUpdate']['status']."<br>";
						$p_count = $p_count + 1;
					} else if( $b_to_ord['MergeUpdate']['status'] == 2 ){
						$c_count = $c_count + 1;
						$b_to_ord['MergeUpdate']['status']."<br>";
					} else {
						$l_count = $l_count + 1;
					}
				}
					$statusarr['open_order'] 		= $o_count;
					$statusarr['proc_order'] 		= $p_count;
					$statusarr['canc_order'] 		= $c_count;
					$statusarr['lock_order'] 		= $l_count;
					$statusarr['remaning_order'] 	= $o_r_count;	
					return $statusarr;
		}
		
		public function getbatchQty( $b_num = null )
		{
				$this->loadModel( 'MergeUpdate' );
				$b_ords		=	$this->MergeUpdate->find( 'count', array( 'conditions' => array( 'MergeUpdate.created_batch' => $b_num ) ) );
				$p_b_ords	=	$this->MergeUpdate->find( 'count', array( 'conditions' => array( 'MergeUpdate.status' => 1, 'MergeUpdate.created_batch' => $b_num ) ) );
				$b_data['b_orders'] =	$b_ords;
				$b_data['p_b_orders'] =	$p_b_ords;
				return $b_data;
		}
		
		public function getPrinterId()
		{
			$this->layout = '';
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );						
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name'];  
			$paramsZDesigner = array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name LIKE' => '%Brother HL-6180DW series%'
				)
			);
			//ZDesigner
			$pcDetailZDesigner = json_decode(json_encode($this->PcStore->find('all', $paramsZDesigner)),0);		
			
			$zDesignerPrinter = $pcDetailZDesigner[0]->PcStore->printer_id;
		
			return $zDesignerPrinter;	
		}
		
		public function pro_location(){
			 
			$this->loadModel( 'Product' );
			$products = array();
			$_product	= $this->Product->find('all',array('fields'=>array('product_sku','ProductDesc.barcode'),'order'=>'Product.id DESC'));
			
			foreach( $_product as $pro ){
				$products[$pro['Product']['product_sku']] = $pro['ProductDesc']['barcode'];
			}
			pr($products);exit;
		}
		
		public function getOverWeight($batch_num){
			
			$this->loadModel( 'MergeUpdate' );
 			$items = $this->MergeUpdate->query("SELECT product_order_id_identify, sku,delevery_country,service_name FROM merge_updates as MergeUpdate WHERE (`service_name` = 'Over Weight' OR `service_name` = '' ) AND `created_batch` = '".$batch_num."' AND `status` IN(0,2,3,4)");
 			
			if(count($items) > 0 ){
				$fname = 'batch_over_'. date('dmy_Hi').'.txt';
 				$_path = WWW_ROOT .'logs/'.$fname;    				  
				file_put_contents($_path, "order_id\tsku\tdelevery_country\tservice_name"."\r\n");
 				foreach($items as $v){
 					 file_put_contents($_path, $v['MergeUpdate']['product_order_id_identify']."\t".$v['MergeUpdate']['sku']."\t".$v['MergeUpdate']['delevery_country']."\t".$v['MergeUpdate']['service_name']."\r\n", FILE_APPEND|LOCK_EX);	
				
				} 
  				header('Content-Encoding: UTF-8');
				header('Content-type: text/plain; charset=UTF-8');
				header('Content-Disposition: attachment;filename="'.$fname.'"'); 
				header('Cache-Control: max-age=0');
				readfile($_path);				 
				exit;					 
			}else{
				$this->Session->setflash( 'No orders founds.', 'flash_danger'); 
			}
			 $this->redirect($this->referer());	
  		}
		
		public function getOverSell($batch_num){
			
			$this->loadModel( 'OrderLocation' );
			$this->loadModel( 'MergeUpdate' );
 			$_mitems = $this->MergeUpdate->query("SELECT product_order_id_identify,over_sell_user,sku FROM merge_updates as MergeUpdate WHERE `created_batch` = '".$batch_num."' AND `status` IN(0,2,3)");
			
			$fname = 'batch_over_sell_'. date('dmy_Hi').'.txt';
			$_path = WWW_ROOT .'logs/'.$fname;    				  
			file_put_contents($_path, "order_id\tsku\tbin_location or user"."\n");
					
			$split_order_id = [];
			
			if(count($_mitems) > 0 ){
				foreach($_mitems as $r){					
					if($r['MergeUpdate']['over_sell_user']){
						 file_put_contents($_path, $r['MergeUpdate']['product_order_id_identify']."\t".$r['MergeUpdate']['sku']."\t".$r['MergeUpdate']['over_sell_user']."\n", FILE_APPEND|LOCK_EX);	
					}else{
						$split_order_id[] = $r['MergeUpdate']['product_order_id_identify'];
					}
				}
			}
			
			if(count($split_order_id) > 0){
			
				$items = $this->OrderLocation->query("SELECT split_order_id, sku, bin_location FROM order_locations as OrderLocation WHERE  `available_qty_bin` = 0 AND `status`='active' AND split_order_id IN('". implode("','",$split_order_id)."')");
				
				if(count($items) > 0 ){
					
					foreach($items as $v){
						 file_put_contents($_path, $v['OrderLocation']['split_order_id']."\t".$v['OrderLocation']['sku']."\t".$v['OrderLocation']['bin_location']."\n", FILE_APPEND|LOCK_EX);	
					
					} 
					header('Content-Encoding: UTF-8');
					header('Content-type: text/plain; charset=UTF-8');
					header('Content-Disposition: attachment;filename="'.$fname.'"'); 
					header('Cache-Control: max-age=0');
					readfile($_path);				 
					exit;	
				}				 
			}else{
				$this->Session->setflash( 'No orders founds.', 'flash_danger'); 
			}
			 $this->redirect($this->referer());	
  		}
		
		public function labelTime(){
			 
			$this->loadModel( 'Product' );
			$orders = ['1813431-1','1812722-1','1813400-1','1812744-1','1814363-1','1812115-1','1812131-1','1808660-1','1813170-1','1813131-1','1813095-1','1813679-1','1812952-1','1813761-1','1813976-1','1814086-1','1812698-1','1813877-1','1812194-1','1814389-1','1814304-1','1812226-1','1812581-1','1813831-1','1814284-1','1812585-1','1813317-1','1813492-1','1813830-1','1812584-1','1812278-1','1812196-1','1814198-1','1812224-1','1812382-1','1812380-1','1812588-1','1812381-1','1812580-1','1812478-1','1812531-1','1812811-1','1812132-1','1812243-1','1812135-1','1812116-1','1812195-1','1814418-1','1814548-1','1812842-1','1814513-1','1812892-1','1814524-1','1812479-1','1813787-1','1813154-1','1814085-1','1814015-1','1813677-1','1813249-1','1812163-1','1813176-1','1814263-1','1814022-1','1813875-1','1813786-1','1812891-1','1814348-1','1813669-1','1814414-1','1813817-1','1813729-1','1814415-1','1812443-1','1813569-1','1813667-1','1812745-1','1814463-1','1812816-1','1814419-1','1814119-1','1812816-2','1812225-1','1812512-1','1813171-1','1813455-1','1813153-1','1813312-1','1812083-1','1812889-1','1813490-1','1812792-1','1812051-1','1812481-1','1814461-1','1814484-1','1813097-1','1812817-1','1813598-1','1812188-1','1814404-1','1811463-1','1812193-2','1814344-1','1813674-1','1812193-1'];
			
			foreach($orders as $oid){
				  $filename = WWW_ROOT ."img/printPDF/Bulk/Label_Slip_{$oid}.pdf";
				
				if (file_exists($filename)) {
					echo "Label_Slip_{$oid}.pdf was last modified: " . date ("F d Y H:i:s.", filemtime($filename));
					echo "<br>";
					copy($filename, WWW_ROOT ."logs/ava/Label_Slip_{$oid}.pdf");
				}
				//echo "<br>";
			}
		}
		
		
		
}



?>

