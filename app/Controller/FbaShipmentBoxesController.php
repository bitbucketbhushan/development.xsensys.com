<?php
//error_reporting(0);
class FbaShipmentBoxesController extends AppController
{
    
    var $name = "FbaShipmentBoxes";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('printTest'));
			//'RequestReport','GetReportRequestList','GetReport'
    }
 
   public function index($shipment_id = null) 
    {	 
	 
		$this->layout = "fba"; 	
		$this->set( "title","FBA Shipments" );	
		$this->loadModel('FbaShipmentBox'); 
		  
		$shipment_boxes = $this->FbaShipmentBox->find('all', array('conditions'=>array('FbaShipmentBox.shipment_id'=>$shipment_id),'order'=>'id ASC'));
		$this->set( 'shipment_boxes', $shipment_boxes );	
		 
	} 
	
	public function getShipmentBoxItems( $box_inc_id = null){
	
		$this->loadModel('FbaShipmentBoxItem');	  
		return $this->FbaShipmentBoxItem->find("all", array('conditions' => array('box_inc_id' => $box_inc_id)));//,'status' => 0
 	}
	
	public function UpdateBox(){
		$msg = array();
		
		$this->loadModel( 'FbaShipmentBox' ); 
		$this->loadModel( 'FbaShipmentBoxItem' ); 
		$data = array();
		$data['id']   		   = $this->request->data['box_id']; 
		$data['box_height']    = $this->request->data['height']; 
		$data['box_length']    = $this->request->data['length'];
		$data['box_width']     = $this->request->data['width'];
		$data['box_weight']    = $this->request->data['weight'];
		$this->FbaShipmentBox->saveAll($data);	
		
		$qty = $this->request->data['qty'];
		
		foreach($qty as $id => $v){
			$qdata = array();
			$qdata['id']  = $id; 
			$qdata['qty'] = $v; 
			$this->FbaShipmentBoxItem->saveAll($qdata);	
		}
		$msg['msg'] = 'done';
		
		echo json_encode($msg);
		exit;
	}
	
	public function RemoveFromBox(){
		$msg = array();
 		$this->loadModel( 'FbaShipmentBoxItem' ); 
 		$id = $this->request->data['id'];
 		$this->FbaShipmentBoxItem->deleteAll( array( 'FbaShipmentBoxItem.id' => $id ) );
 		$msg['msg'] = 'done'; 		
		echo json_encode($msg);
		exit;
	}
	
	public function getUnboxItems(){
		$msg = array();
		
 		$this->loadModel('FbaItemLocation');  
		$this->loadModel('FbaShipmentBoxItem'); 
		$shipment_id = $this->request->data['shipment_id'];
		$sql = "SELECT * FROM `fba_item_locations` WHERE fnsku NOT IN(SELECT fnsku FROM `fba_shipment_box_items` WHERE shipment_id = '".$shipment_id."') AND  shipment_id = '".$shipment_id."'";
				
 		$all_items =  $this->FbaShipmentBoxItem->query($sql);
		$html = ''; 
 		if(count($all_items) > 0){
 			$html = '<div class="row">
			<div class="col-lg-12">  
			<div class="col-sm-5">';
			$html .='<select name="item_id" id="item_id" class="form-control">';
			foreach($all_items as $val ){
				$html .='<option value="'.$val['fba_item_locations']['id'].'" for="'.$val['fba_item_locations']['process_qty'].'">FNSKU:'.$val['fba_item_locations']['fnsku'].' - Processed Qty:'.$val['fba_item_locations']['process_qty'].'</option>';
			}
			$html .='</select>';
			$html .= '</div>
			<div class="col-4">Qty:</div>
			<div class="col-sm-2"><input type="text" name="item_qty" id="item_qty" class="form-control col-90" value=""/></div>
			<div class="col-sm-2"><button type="button" class="btn btn-success" onclick="saveBoxItem('.$this->request->data['box_id'].');">Save</button></div>
			</div>
			</div>';
		}else{
			$html = '<div class="row"><div class="col-lg-12"> No Unassigned item found.</div></div>';
		}
 		 
 		$msg['result'] = $html; 
		$msg['msg'] = 'done'; 		
		echo json_encode($msg);
		exit;
	}
	
	public function saveBoxItem(){
		 
 		$msg['error'] = $msg['msg'] = '';
		
		$this->loadModel('FbaItemLocation');  
		$this->loadModel('FbaShipmentBoxItem'); 
		
		$box_id   = $this->request->data['box_id'];
		$item_id  = $this->request->data['item_id'];
 		$item_qty  = $this->request->data['item_qty'];
		
		$loc_data = $this->FbaItemLocation->find("first", array('conditions' => array('id' => $item_id))); 
		
		$qty = 0; $box_number = 0; $box_qty = 0;
		if(count($loc_data) > 0){
			
			$process_qty = $loc_data['FbaItemLocation']['process_qty'];
			
			$item_data  = $this->FbaShipmentBoxItem->find("all", array('conditions' => array('shipment_id' => $loc_data['FbaItemLocation']['shipment_id'],'fnsku' => $loc_data['FbaItemLocation']['fnsku'],'merchant_sku' => $loc_data['FbaItemLocation']['merchant_sku']))); 
			
			if(count($item_data > 0)){
			   foreach($item_data as $val){
				$box_qty += $val['FbaShipmentBoxItem']['qty'];
				$box_number .= $val['FbaShipmentBoxItem']['box_number'];
			   }
				
				$qty = $process_qty - $box_qty;
			}
			
			if($item_qty > $process_qty){
				$msg['error'] = 'yes';
				$msg['msg']   = "Entered quantity(".$item_qty.") can not be greater than processed quantity(".$process_qty.")."; 	
			}elseif(($qty > 0) && ($item_qty > $qty)){
				$msg['error'] = 'yes';
				$msg['msg']   = $box_qty ." are already added in box-". $box_number; 	
			}else{
				$this->loadModel('FbaShipmentBox'); 
				$box = $this->FbaShipmentBox->find("first", array('conditions' => array('id' => $box_id))); 
				if(count($box) > 0){
					$data['shipment_id']   	 = $loc_data['FbaItemLocation']['shipment_id']; 
					$data['fnsku']   		 = $loc_data['FbaItemLocation']['fnsku']; 
					$data['merchant_sku']    = $loc_data['FbaItemLocation']['merchant_sku'];
					$data['box_number']    	 = $box['FbaShipmentBox']['box_number'];   
					$data['qty']   			 = $this->request->data['item_qty'];
					$data['username']     	 = $this->Session->read('Auth.User.username'); 
					$this->FbaShipmentBoxItem->saveAll($data);	
					$msg['msg'] = "Item is added in the box."; 	
				}else{
					$msg['error'] = 'yes';
					$msg['msg'] = "Box is not found."; 	
				}		
			}
		}
		 
		echo json_encode($msg);
		exit;
	}
	
	
}