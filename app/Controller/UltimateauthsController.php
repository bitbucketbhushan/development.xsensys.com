<?php

class UltimateauthsController extends AppController
{    
    var $name = "Ultimateauths";   
    
    var $components = array('Session','Upload','Common','Auth');
    
    var $helpers = array('Html','Form','Common','Session');
    
     public function beforeFilter()
	 {
	    parent::beforeFilter();
	    $this->layout = false;
	    $this->Auth->Allow(array('authApply', 'sendVirtualStockMail','sendSoldStockMail' , 'notifyOrderListForVerificationByUser' ));
	 }
      
    public function startThread( $locationName = null , $ccType = null )
    {		
		App::import( 'Component','Ultimate' );
		$ultimate = new UltimateComponent(new ComponentCollection());	 			
		return $ultimate->setAuthentication( $locationName , $ccType );
	}
	 
	public function authApply( $locationName = null , $ccType = null )
	{
		$this->layout = '';
		$this->autoRender = false;
		return UltimateauthsController::startThread( $locationName , $ccType );
	}   
	
	public function sendVirtualStockMail() 
	{
		App::import( 'Controller','Outers' );
		$ultimateController	=	new OutersController( new View() );
		$ultimateController->sendVirtualStockFile();
		
	}
	
	public function sendSoldStockMail() 
	{
		
		App::import( 'Controller','Reports' );
		$reportController	=	new ReportsController( new View() );
		$reportController->getDailySellReport();
	}
	
	/*
	 * 
	 * Notification center
	 * 
	 */ 
	public function notifyOrderListForVerificationByUser()
	{
		
		$this->loadModel( 'PutbackItem' );	
		$getDeleteCancel	= $this->PutbackItem->find('count');
		
		if( $getDeleteCancel > 0 )
		{
			/* Notification User Login */
			App::import( 'Component' , 'Ultimate' );
			$ultimateCompObj = new UltimateComponent(new ComponentCollection());
			$ultimateCompObj->sendOrderVerifyNotification( $getDeleteCancel );
		
		}	
		
	}
}
?>
