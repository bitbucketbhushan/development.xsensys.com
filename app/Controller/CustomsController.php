<?php
//error_reporting(0);
//ini_set('memory_limit', '-1');
	App::uses('Folder', 'Utility');
	App::uses('File', 'Utility');
class CustomsController extends AppController
{	
	var $name = "Customs";
	var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
	public function index()
	{ 
		exit;
	}
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
    }
	
 

	public function getBrtManifestFilesCustom()
	{ 
		$this->layout = 'index';

		if(isset($_POST['parcel_no'])) {
			
		$this->loadModel('BrtSerialNo');

		$parcelNos = $this->request->data['parcel_no']; 
		$orders = array();

		// $parcelNos = explode(',',$parcelNos);

		$parcelNos = array_map('trim', explode(',', $parcelNos));

		$brtdata = $this->BrtSerialNo->find('all', array( 
			'conditions' => array( 'BrtSerialNo.parcel_no'=>$parcelNos),
			'order' => 'parcel_no DESC')
		);
        if(count($brtdata) >0 ) {
			foreach($brtdata as $brtord) { 
			   $orders[] = $brtord['BrtSerialNo']['split_order_id'];
			}  
	    }
        if(count($orders) > 0 ) {
			$brt ='';
			$result	= $this->getBrtSheetManifest($orders);
			if($result == 'ok'){
				$result	= $this->getPhoneNumbreList($orders);
				if($result == 'ok'){
					$result	= $this->getDataForBrtPalletPrintPdf($orders);
					if($result == 'ok'){
							$this->Session->setFlash('Brt Manifest Files generated.', 'flash_success');	
							$brt = '?brt=1';	
					} else {
						$this->Session->setFlash($result, 'flash_danger');
					}
				}else{
					$this->Session->setFlash($result, 'flash_danger');
				}
			}else{			
				$this->Session->setFlash($result, 'flash_danger');
			}
			$this->Session->setFlash('All Brt files are generated.', 'flash_success');
	    } else {
			$this->Session->setFlash('No order id found.', 'flash_error');
		}
		
	    }  
	}
	
	public function getBrtSheetManifest($orders = array())
	{
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('ItalyCode');
		$this->loadModel('BrtSerialNo');
		$start_date = date('Y-m-d H:i:s');
		$end_date   = date('Y-m-d H:i:s');
		
		$italyOrders = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => $orders ) ) );
																		
		if(count($italyOrders) > 0)
		{																		
		$fp = fopen(WWW_ROOT."img/FNVAB00R.csv","w");
		$sep = ";";
		$header = "VABCCM".$sep."VABLNP".$sep."VABAAS".$sep."VABMGS".$sep."VABNRS".$sep."VABNSP".$sep."VABCBO".$sep."VABLNA".$sep."VABRSD".$sep."VABRD2".$sep."VABIND".$sep."VABCAD".$sep."VABLOD".$sep."VABPRD".$sep."VABNZD".$sep."VABGC1".$sep."VABGC2".$sep."VABCTR".$sep."VABTSP".$sep."VABIAS".$sep."VABVAS".$sep."VABNAS".$sep."VABNCL".$sep."VABPKB".$sep."VABVLB".$sep."VABQFT".$sep."VABCAS".$sep."VABTIC".$sep."VABVCA".$sep."VABGCA".$sep."VABRMN".$sep."VABRMA".$sep."VABNCD".$sep."VABNCA".$sep."VABXCO".$sep."VABNOT".$sep."VABNT2".$sep."VABZNC".$sep."VABCTM".$sep."VABFFD".$sep."VABDCR".$sep."VABTCR".$sep."VABHCR".$sep."VABCTS".$sep."VABFTM".$sep."VABVMD".$sep."VABVAD".$sep."VABGMA".$sep."VABGGA".$sep."VABGVA".$sep."VABTC1".$sep."VABTC2".$sep."VABSCL".$sep."VABANT".$sep."VABRMO".$sep."VABCMO".$sep."VABNMO\r\n";
		unlink(WWW_ROOT."img/FNVAB00R.csv");
		file_put_contents(WWW_ROOT."img/FNVAB00R.csv",$header);
			foreach( $italyOrders as $italyOrder )
			{
					$italyOrder['MergeUpdate']['order_id'];
					$orderdetails = $this->MergeUpdate->find('all', array('conditions'=>array('MergeUpdate.product_order_id_identify'=>$italyOrder['MergeUpdate']['product_order_id_identify'])));
					$italyopen	=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id'=>$italyOrder['MergeUpdate']['order_id'])));
				foreach($orderdetails as $orderdetail)
					{
						$brtdata	=	$this->BrtSerialNo->find('first', array( 'conditions' => array( 'BrtSerialNo.split_order_id'=>$orderdetail['MergeUpdate']['product_order_id_identify']),'order' => 'parcel_no DESC'));
						
						if(!empty($brtdata))
						{
							$split_order_id		= 	$orderdetail['MergeUpdate']['product_order_id_identify'];
							$serial_no			=	$brtdata['BrtSerialNo']['serial_no'];
							$parcel_no			=	$brtdata['BrtSerialNo']['parcel_no'];
							$split_order_id		=	$brtdata['BrtSerialNo']['split_order_id'];
							$iso_country		=	$brtdata['BrtSerialNo']['iso_country'];
							$province_recipient	=	$brtdata['BrtSerialNo']['province_recipient'];
							$po_arrival			=	$brtdata['BrtSerialNo']['po_arrival'];
							$po_arrival_des		=	$brtdata['BrtSerialNo']['po_arrival_des'];
							$po_departure_des	=	$brtdata['BrtSerialNo']['po_departure_des'];
							$arrival_terminal	=	$brtdata['BrtSerialNo']['arrival_terminal'];
							$zone_mark_neck		=	$brtdata['BrtSerialNo']['zone_mark_neck'];
							$barcode			=	$brtdata['BrtSerialNo']['barcode'];
							
							$store =	$italyopen['OpenOrder']['sub_source'];
							 
							if($store == 'CostBreaker_IT' || $store == 'CostBreaker_DE' || $store == 'CostBreaker_FR' || $store == 'CostBreaker_ES'){
							 	$acnumber = '1664097';
							} else if($store == 'Rainbow_Retail_IT' || $store == 'Rainbow Retail' || $store == 'Rainbow_Retail_ES' || $store == 'RAINBOW RETAIL DE'){
								$acnumber = '1664098';
							} else if($store == 'Tech_Drive_ES' || $store == 'Tech_Drive_UK' || $store == 'Tech_Drive_DE' || $store == 'Tech_Drive_FR'){
								$acnumber = '1664099';
							} else {
								$acnumber = '1664070';
							}
							
							
							$general_info		=	unserialize( $italyopen['OpenOrder']['general_info'] );
							$shipping_info		=	unserialize( $italyopen['OpenOrder']['shipping_info'] );
							$customer_info		=	unserialize( $italyopen['OpenOrder']['customer_info'] );
							$totals_info		=	unserialize( $italyopen['OpenOrder']['totals_info'] );
							$items				=	unserialize( $italyopen['OpenOrder']['items'] );
							$orderidwithout		=	str_replace('-','',$orderdetail['MergeUpdate']['product_order_id_identify']);
							$orderidwith		=	$orderdetail['MergeUpdate']['product_order_id_identify'];
							
							$vitrual_delete		=	'';
							$departure_depot					=	'';
							$shipment_year						=	date("Y");
							$shipment_monty_day					=	date("md");
							
							$address1 = rtrim($this->getcommaremovedata($customer_info->Address->Address1),' ');
							$address2 = rtrim($this->getcommaremovedata($customer_info->Address->Address2),' ');
							
							$consignee_address 					= 	rtrim($address1.' '.$address2,' ');
							$consigee_cap_zip_code 				= 	str_pad($customer_info->Address->PostCode,5,"0",STR_PAD_LEFT);
							$consignee_city						=	rtrim($this->getcommaremovedata(htmlspecialchars($customer_info->Address->Town)),' ');//city;
							$consignee_province_abbreviation	=	$this->getcommaremovedata(htmlspecialchars($customer_info->Address->Region));//provience;
							$name								=	rtrim($this->getcommaremovedata($customer_info->Address->FullName),' ');
							$email								=	$this->getcommaremovedata($customer_info->Address->EmailAddress);
							$comany 							= 	$this->getcommaremovedata($customer_info->Address->Company); 
							$weight								=	ceil($orderdetail['MergeUpdate']['packet_weight']);
							$consignee_country					=	'';
							$phonenumber						=	str_replace('+','',filter_var($customer_info->Address->PhoneNumber, FILTER_SANITIZE_NUMBER_INT));
						 
							//str_pad($parcel_no,7,"0",STR_PAD_LEFT)1664070
							//$sno	=	substr($parcel_no, -2);
							
							  $content = $acnumber.$sep.'166'.$sep.$shipment_year.$sep.$shipment_monty_day.$sep.'67'.$sep.$parcel_no.$sep.'1'.$sep.$po_arrival.$sep.$name.$sep.''.$sep.$consignee_address.$sep.$consigee_cap_zip_code.$sep.$consignee_city.$sep.$province_recipient.$sep.''.$sep.''.$sep.''.$sep.'300'.$sep.'c'.$sep.''.$sep.''.$sep.'Goods'.$sep.'1'.$sep.$weight.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.$orderidwithout.$sep.$orderidwith.$sep.str_pad($parcel_no,7,"0",STR_PAD_LEFT).$sep.str_pad($parcel_no,7,"0",STR_PAD_LEFT).$sep.''.$sep.''.$sep.''.$sep.$zone_mark_neck.$sep.'2'.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep.''.$sep."\r\n";
							 
							file_put_contents(WWW_ROOT."img/FNVAB00R.csv",$content, FILE_APPEND | LOCK_EX);
							$this->MergeUpdate->updateAll(array('MergeUpdate.brt' => '2'), array('MergeUpdate.id' => $orderdetail['MergeUpdate']['id']));
						}
				  }
			}
			 
			$uploadUrl 		= 	WWW_ROOT .'img/FNVAB00R.csv';
			$folderName = 'Service Manifest -'. date("d.m.Y");
			$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
			copy( $uploadUrl, WWW_ROOT .'img/cut_off/'.$folderName.'/FNVAB00R_'.date('m-d-Y H:i:s').'.csv');  
			$status = 'ok';
			} else {
				$status = 'There is no data for FNVAB00R.';
			}
			return $status;
	}
		
	public function getPhoneNumbreList($orders = array())
	{
	
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('ItalyCode');
		$this->loadModel('BrtSerialNo');
		
		$italyOrders = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => $orders ) ) );
	 																			
		if(count($italyOrders) > 0)
		{
		$fp = fopen(WWW_ROOT."img/FNVAT00R.csv","w");
		$sep = ";";
		$header = "VATCCM".$sep."VATLNP".$sep."VATNRS".$sep."VATNSP".$sep."VATNOT_B".$sep."VATNOT_S\r\n";
		unlink(WWW_ROOT."img/FNVAT00R.csv");
		file_put_contents(WWW_ROOT."img/FNVAT00R.csv",$header);
		foreach( $italyOrders as $italyOrder )
			{
					$italyOrder['MergeUpdate']['order_id'];
					$orderdetails = $this->MergeUpdate->find('all', array('conditions'=>array('MergeUpdate.product_order_id_identify'=>$italyOrder['MergeUpdate']['product_order_id_identify'])));
					$italyopen	=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id'=>$italyOrder['MergeUpdate']['order_id'])));
				foreach($orderdetails as $orderdetail)
					{
						$brtdata	=	$this->BrtSerialNo->find('first', array( 'conditions' => array( 'BrtSerialNo.split_order_id'=>$orderdetail['MergeUpdate']['product_order_id_identify']),'order' => 'parcel_no DESC'));
						
						if(!empty($brtdata))
						{
							$customer_info				=	unserialize( $italyopen['OpenOrder']['customer_info'] );
							$consignee_country			=	'';
							$number						=	str_replace('-','',str_replace('+','',filter_var($customer_info->Address->PhoneNumber, FILTER_SANITIZE_NUMBER_INT)));
							$concode					=	substr($number, 0, 2);
							
							if(strlen($number) < 13 && $concode != '39'){
								$number =  '+39'.$number;	
							} 
							 
							 $store =	$italyopen['OpenOrder']['sub_source'];
							 
							if($store == 'CostBreaker_IT' || $store == 'CostBreaker_DE' || $store == 'CostBreaker_FR' || $store == 'CostBreaker_ES'){
							 	$acnumber = '1664097';
							} else if($store == 'Rainbow_Retail_IT' || $store == 'Rainbow Retail' || $store == 'Rainbow_Retail_ES' || $store == 'RAINBOW RETAIL DE'){
								$acnumber = '1664098';
							} else if($store == 'Tech_Drive_ES' || $store == 'Tech_Drive_UK' || $store == 'Tech_Drive_DE' || $store == 'Tech_Drive_FR'){
								$acnumber = '1664099';
							} else {
								$acnumber = '1664070';
							}
							
							$split_order_id		= 	$orderdetail['MergeUpdate']['product_order_id_identify'];
							$serial_no			=	$brtdata['BrtSerialNo']['serial_no'];
							$parcel_no			=	$brtdata['BrtSerialNo']['parcel_no'];
							$sno	=	substr($parcel_no, -2);
							$content = $acnumber .$sep.'166'.$sep.'67'.$sep.$parcel_no.$sep.$number.$sep.$number."   SN\r\n";
							file_put_contents(WWW_ROOT."img/FNVAT00R.csv",$content, FILE_APPEND | LOCK_EX);
							$this->MergeUpdate->updateAll(array('MergeUpdate.brt_phone' => '2'), array('MergeUpdate.id' => $orderdetail['MergeUpdate']['id']));
						}
				
					 }
				}
			$folderName = 'Service Manifest -'. date("d.m.Y");
			$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
			$uploadUrl 		= 	WWW_ROOT .'img/FNVAT00R.csv';
			copy($uploadUrl,WWW_ROOT .'img/cut_off/'.$folderName.'/FNVAT00R_'.date('m-d-Y H:i:s').'.csv');  
				$status = 'ok';
			} else {
				$status = 'There is no data for FNVAT00R.';
			}
			return $status;		
			
			
	}
	
	public function getDataForBrtPalletPrintPdf($orders = array())
	{
		
		$this->layout  = '';
		$this->autoRender = false;
		$this->loadModel('OpenOrder');
		$this->loadModel('MergeUpdate');
		$this->loadModel('ItalyCode');
		$this->loadModel('BrtSerialNo');
		
		
		$italyOrders = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => $orders ) ) );									
		
		if(count($italyOrders) > 0)
		{
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper(array(0, 0, '794', '1122' ), 'portrait');
	
		$imgPath = WWW_ROOT .'css/';
		$html = '<style>table, th, td { border: 1px solid black; }</style><body>';
		$html .= '<div id="label">';
		$html .= '<div class="container">';
   	  	$html .= '<div class="header row" style="width:100%; border:1px solid #000000; margin:20px 0 10px 0; padding:10px; text-align: center;"><strong style="font-size: 32px;" >Brt Export Sheet</strong><br><strong>ESL Limited</strong><br><strong>'.date('m-d-Y H:i:s').'</strong></div>';
		$html .= '<table class="header row" style="border:1px solid #000000; margin:0px 0 10px 0;" width=100%>';
      	$html .='<tr style="text-align: left;"><th width="20%" >Our Ref:</th><th width="20%" >Consignee Name</th><th width="30%">Consignee Address</th><th width="15%">Consignee PostCode</th><th width="15%">Parcel No</th></tr>';
	  
	  	foreach($italyOrders as $italyOrder) {
		
			$or_id				=	$italyOrder['MergeUpdate']['order_id'];
			$spo_id				=	$italyOrder['MergeUpdate']['product_order_id_identify'];
			$italyopen			=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id'=>$or_id)));
			$brtdata			=	$this->BrtSerialNo->find('first', array( 'conditions' => array( 'BrtSerialNo.split_order_id'=>$spo_id),'order' => 'parcel_no DESC'));
			$customer_info		=	unserialize( $italyopen['OpenOrder']['customer_info'] );
			$consignee_address 	= 	$customer_info->Address->Address1;
			$zip_code 			= 	str_pad($customer_info->Address->PostCode,5,"0",STR_PAD_LEFT);
			$name				=	$customer_info->Address->FullName;
			$parcel_no			=	'--';
			if( !empty( $brtdata ) )
				$parcel_no			=	$brtdata['BrtSerialNo']['parcel_no'];
				
			$html .='<tr><td width="20%">'.$spo_id.'</td><td width="20%">'.$name.'</td><td width="20%">'.$consignee_address.'</td><td width="20%">'.filter_var($zip_code, FILTER_SANITIZE_NUMBER_INT).'</td><td width="20%">'.$parcel_no.'</td></tr>';
			//$this->MergeUpdate->updateAll(array('brt_exprt' => 2), array('product_order_id_identify' => $spo_id));
	  	}
   	  	$html .='</table></div></body>';
		$html.= '<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
		//exit;
		$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
		$dompdf->render();
		//$dompdf->stream("ExportLabelBrt.pdf");
		
		$imgPath 	= 	WWW_ROOT .'img/'; 
		$name		=   'ExportLabelBrt.pdf';
		file_put_contents($imgPath.$name, $dompdf->output());
		
		$folderName = 'Service Manifest -'. date("d.m.Y");
		$dir = new Folder(WWW_ROOT .'img/cut_off/'.$folderName, true, 0755);
		$uploadUrl 		= 	WWW_ROOT .'img/ExportLabelBrt.pdf';
		copy($uploadUrl,WWW_ROOT .'img/cut_off/'.$folderName.'/ExportLabelBrt_'.date('m-d-Y H:i:s').'.pdf');  
		$uploadUrl 		= 	WWW_ROOT .'img/ExportLabelBrt.pdf';		
		$status = 'ok';
		} else {
		$status = 'There is no data for ExportLabelBrt.';
		}
		return $status;
		
		
	}
	
	 public function setStoreSku()
	 {
		$this->layout = 'index';
		 
		if(isset($_POST['skus'])) {

			$skus = $this->request->data['skus']; 
			// $skusList = explode(',',$skus);
			$skusList = array_map('trim', explode(',', $skus));

			$this->loadModel('Product');
			$this->loadModel('Store');
			$this->loadModel('SkuPercentRecord');
			
			/*$getStockDetails	=	$this->Product->find('all', 
										array( 
											'fields' => array( 
														'Product.current_stock_level, 
														Product.product_sku, 
														Product.store_name,
														ProductDesc.barcode,
														Product.product_name,
														ProductDesc.barcode' 
													) 
											)
										);*/
			$getStockDetails	=	$this->Product->find('all', 
										array( 
											'fields' => array( 
														'Product.current_stock_level, 
														Product.product_sku,
														Product.store_name,
														ProductDesc.barcode,
														Product.product_name,
														ProductDesc.barcode' 
													),
											'conditions' => array('Product.product_sku' => $skusList )
											)
										);
			
			
			foreach( $getStockDetails as $getStockDetailValue)
			{
				$storeIds	=	 explode(',', $getStockDetailValue['Product']['store_name']);
				foreach( $storeIds as $storeIdValue )
				{
					$getStoreName	=	$this->Store->find( 'first', array( 
									'conditions' => array( 'Store.id' => $storeIdValue),
									'fields' => array( 'Store.store_name, Store.id') 
									)
								);
					$storeNameAlies = str_replace('.','_',str_replace(' ','_',$getStoreName['Store']['store_name']));		
					$getResult	=	$this->SkuPercentRecord->find('first', array( 'conditions' => array( 'SkuPercentRecord.store_id' => $getStoreName['Store']['id'], 'SkuPercentRecord.sku' => $getStockDetailValue['Product']['product_sku']) ));
					//echo $getStockDetailValue['Product']['product_sku']."<br>";
						if( !empty( $getResult['SkuPercentRecord']['id'] ) && count($getResult['SkuPercentRecord']['id']) > 0  )
						{
							$data['SkuPercentRecord']['id'] 						= $getResult['SkuPercentRecord']['id'];
							$data['SkuPercentRecord']['store_id'] 					= $getStoreName['Store']['id'];
							$data['SkuPercentRecord']['store_name'] 				= $getStoreName['Store']['store_name'];
							$data['SkuPercentRecord']['store_alies'] 				= $storeNameAlies;
							$data['SkuPercentRecord']['sku'] 						= $getStockDetailValue['Product']['product_sku'];
							$data['SkuPercentRecord']['current_stock'] 				= $getStockDetailValue['Product']['current_stock_level'];
							$data['SkuPercentRecord']['product_title'] 				= $getStockDetailValue['Product']['product_name'];
							$data['SkuPercentRecord']['barcode'] 					= ($getStockDetailValue['ProductDesc']['barcode'] != '') ? $getStockDetailValue['ProductDesc']['barcode'] : '0';
							$this->SkuPercentRecord->saveAll( $data );
						}
						else
						{
							$data['SkuPercentRecord']['id'] 						= 	'';
							$data['SkuPercentRecord']['percentage'] 				= 	'50';
							$data['SkuPercentRecord']['store_id'] 					= $getStoreName['Store']['id'];
							$data['SkuPercentRecord']['store_name'] 				= $getStoreName['Store']['store_name'];
							$data['SkuPercentRecord']['store_alies'] 				= $storeNameAlies;
							$data['SkuPercentRecord']['sku'] 						= $getStockDetailValue['Product']['product_sku'];
							$data['SkuPercentRecord']['current_stock'] 				= $getStockDetailValue['Product']['current_stock_level'];
							$data['SkuPercentRecord']['product_title'] 				= $getStockDetailValue['Product']['product_name'];
							$data['SkuPercentRecord']['barcode'] 					= ($getStockDetailValue['ProductDesc']['barcode'] != '') ? $getStockDetailValue['ProductDesc']['barcode'] : '0';
							$this->SkuPercentRecord->saveAll( $data );
						}  
				}
			}
			$this->Session->setFlash('Store skus set successfully.', 'flash_success');
	 } 
	} 
} 
?>