<?php

class BinlocationsController extends AppController
{
    var $name = "Binlocations";    
    var $components = array('Session','Upload','Common','Auth');    
    var $helpers = array('Html','Form','Common','Session');
   
    /* Get Pc's Name now */
    public function index()
	{
		$this->layout = 'index';
	}
	
	public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->GlobalBarcode = $this->Components->load('Common');
	}
	
	public function getproductdetail()
	{
		$this->layout = '';
		$this->autoRender = false;
	
		//$searchkey	=	'G1_R36';//trim($this->request->data['location']);
		$searchkey	=	trim($this->request->data['location']);
		$global_barcode	=	$this->GlobalBarcode->getGlobalBarcode($searchkey);
		$this->loadModel('BinLocation');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		App::import('Controller', 'Reports');
		$productModel = new ReportsController();
		
		if($global_barcode){
			$conditions = array('ProductDesc.barcode'=>$global_barcode);
		}else{
			$conditions = array('Product.product_sku'=>$searchkey);
		} 
		//'BinLocation.bin_location'=>$searchkey,
		$params	=	array(
							'joins' => array(
									array(
										'table' => 'product_descs',
										'alias' => 'ProductDesc',
										'type' => 'INNER',
										'conditions' => array('ProductDesc.barcode = BinLocation.barcode')
									),
									array(
										'table' => 'products',
										'alias' => 'Product',
										'type' => 'INNER',
										'conditions' => array('Product.id = ProductDesc.product_id')
									)
								),
							'conditions' => $conditions,
							'fields' => array('Product.product_sku','Product.product_name','Product.product_name','ProductDesc.Barcode','Product.current_stock_level','BinLocation.stock_by_location','BinLocation.bin_location','BinLocation.id')
						);
		
		
		$getLocations 	=	$this->BinLocation->find('all', $params );
		
		$i = 0;
		foreach($getLocations as $getLocationval )
		{
			$getResult													=	$this->getupparpereItem( $getLocationval['Product']['product_sku'] );
			$getunpickResult											=	$this->getUnpickItem( $getLocationval['Product']['product_sku'],$getLocationval['BinLocation']['bin_location'] );
			$getLocation[$i]['Orderqty']['unpaid_qty'] 					= 	$getResult;
			$getLocation[$i]['Orderqty']['unpick_qty'] 					= 	$getunpickResult;
			$getLocation[$i]['Product']['product_sku'] 					= 	$getLocationval['Product']['product_sku'];
			$getLocation[$i]['Product']['product_name'] 				= 	$getLocationval['Product']['product_name'];
			$getLocation[$i]['Product']['current_stock_level'] 			= 	$getLocationval['Product']['current_stock_level'];
			$getLocation[$i]['ProductDesc']['Barcode'] 					= 	$getLocationval['ProductDesc']['Barcode'];
			$getLocation[$i]['ProductDesc']['loc_barcode'] 				= 	$this->GlobalBarcode->getAllBarcode($getLocationval['ProductDesc']['Barcode']);
			$getLocation[$i]['BinLocation']['stock_by_location'] 		= 	$getLocationval['BinLocation']['stock_by_location'];
			$getLocation[$i]['BinLocation']['bin_location'] 			= 	$getLocationval['BinLocation']['bin_location'];
			$getLocation[$i]['BinLocation']['id'] 						= 	$getLocationval['BinLocation']['id'];
			$i++;
		}
		
		if( !empty($getLocation) && count( $getLocation ) > 0 ){
			echo json_encode(array('status' => '1', 'data' => $getLocation));		
			exit;
		} else {
			echo json_encode(array('status' => '0'));		
			exit;
		}
	}
	
	public function getupparpereItem( $unpsku = null, $location = null)
	{
		$this->loadModel( 'UnprepareOrder' );
		$this->loadModel( 'OrderLocation' );
		
		$unpOrders	=	$this->UnprepareOrder->find('all',array('fields' => array('items','num_order_id','source_name','destination','date')));
		
		foreach($unpOrders as $unpOrder)
		{
			$items		=	unserialize($unpOrder['UnprepareOrder']['items']);
			$orderids[$unpOrder['UnprepareOrder']['num_order_id']]	= 	$unpOrder['UnprepareOrder']['num_order_id'];
			foreach( $items as $val)
			{
							 
					$_sku = explode( '-' , $val->SKU);
					if( count($_sku ) == 3 ) // For Bundle (Single)
					{
						$sku = 'S-'.$_sku[1];
						$qty = $val->Quantity * ($_sku[2]);
						$unp_data[$sku][] = array('quantity'=>$qty);
					}
					else if( count( $_sku ) > 3 ) // For Bundle (Bundle)
					{						
						$inc = 1;	$ij = 0;
						while( $ij < count($_sku)-2 )
						{						
							$sku = 'S-'.$_sku[$inc];
												
							$inc++;
							$ij++;
							$unp_data[$sku][] = array('quantity'=>$val->Quantity);
						}
					}else{				
						$unp_data[$val->SKU][] = array('quantity'=>$val->Quantity);
					}
				}	
		}
				$count = 0;
				
				if(isset($unp_data[$unpsku])){
					$checkorder	=$this->OrderLocation->find('all', array('conditions'=>array('bin_location'=>$location,'status'=>'active','order_id IN'=>$orderids )));
					if(count($checkorder) > 0){
					foreach($unp_data[$unpsku] as $k => $v)
					{
						 $count	= $count + $v['quantity'];
					}
					return $count;
				} else {
					return '0';
				} } else {
				return '0';
				}
				
	}
	
	public function getUnpickItem( $unpicsku = null, $location = null)
	{
		$this->loadModel( 'OrderLocation' );
		$this->loadModel( 'OpenOrder' );
		$params	=	array(
							'joins' => array(
									array(
										'table' => 'open_orders',
										'alias' => 'OpenOrder',
										'type' => 'INNER',
										'conditions' => array(
											'OpenOrder.num_order_id = OrderLocation.order_id',
											'OpenOrder.status = 0'
										)
									)
								),
							'conditions' => array('OrderLocation.sku' =>$unpicsku, 'OrderLocation.bin_location' => $location, 'OrderLocation.status' => 'active', 'pick_list' => 0),
							'fields' => array( 'SUM(OrderLocation.quantity) as locqty' )
						);
		
		
		
		$getresult	=	$this->OrderLocation->find('all', $params);
		if( $getresult[0][0]['locqty'] != '')
		{
			return $getresult[0][0]['locqty'];
		} else {
			return '0';
		}
		
	}
	
	public function freeLocationdeletion()
	{
		$this->loadModel('BinLocation');
		$id			=	$this->request->data['id'];
		$barcode	=	$this->request->data['barcode'];
		$location	=	$this->request->data['location'];
		$stock	=	$this->request->data['stock'];
		
		$this->BinLocation->query( "delete from bin_locations where id = {$id}" );
		
		$location_log = "Location=".$location."\tBarcode=".$barcode."\tQty=".$stock."\tUser=".$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name')."\t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n";
		file_put_contents(WWW_ROOT."img/delete_location.log", $location_log, FILE_APPEND | LOCK_EX);
		echo json_encode(array('status' => '1', 'data' => $id));		
		exit;
	}

	
	
}

?>
