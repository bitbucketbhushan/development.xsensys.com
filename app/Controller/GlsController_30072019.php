<?php
//error_reporting(0);
class GlsController extends AppController
{
    
	var $name = "Gls";    
	var $components = array('Session', 'Common', 'Upload','Auth');    
	var $helpers = array('Html','Form','Common','Session');
	var $uidcliente = "6BAB7A53-3B6D-4D5A-9450-702D2FAC0B11";
	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->layout = false;
		$this->Auth->Allow(array('applyGls','getSlipLabel'));			
	}
 	
	public function glsTest($order_id = null){ 
		$url = "https://wsclientes.asmred.com/b2b.asmx";

		// FUNCIONA SOLO CON ENVIOS NO ENTREGADOS.
		// XML NO RETORNA NADA.
		
		$Referencia = '01429-1757357-1';
		$Tipo       = 'EPL';
		
		// Ahora podemos obtener la etiqueta codificada en base64
		$XML='<?xml version="1.0" encoding="utf-8"?>
			<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
			<soap12:Body>
			<GetPlazaXCP xmlns="http://www.asmred.com/"> 
				<codPais>34</codPais>
				<cp>46530</cp>
			</GetPlazaXCP>
			</soap12:Body>
			</soap12:Envelope>';
		
		//echo "<br>WS PETICION DE ETIQUETA<br>".$XML."<br>";
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $XML);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=UTF-8"));
		
		$postResult = curl_exec($ch);
		 
		
		if (curl_errno($ch)) {
			echo 'No se pudo llamar al WS de GLS<br>';
		}
		
		$result = strpos($postResult, '<base64Binary>');
		
		if($result == false){
			echo '<font size="5">No se ha retornado ninguna etiqueta.</font>';
		}
		else {
			if	(strtoupper($Tipo) == "PDF") $Extension = ".pdf";
			else if (strtoupper($Tipo) == "JPG") $Extension = ".jpg";
			else if (strtoupper($Tipo) == "PNG") $Extension = ".png";
			else if (strtoupper($Tipo) == "EPL") $Extension = ".epl.txt";
			else if (strtoupper($Tipo) == "DPL") $Extension = ".dpl.txt";
			else if (strtoupper($Tipo) == "XML") $Extension = ".xml";
		
			$xml = simplexml_load_string($postResult, NULL, NULL, "http://http://www.w3.org/2003/05/soap-envelope");
			$xml->registerXPathNamespace("asm","http://www.asmred.com/");
			$arr = $xml->xpath("//asm:EtiquetaEnvioResponse/asm:EtiquetaEnvioResult/asm:base64Binary");
		
			$Tot = sizeof($arr);
		
				for( $Num=0 ; $Num <= sizeof($arr)-1 ; $Num++ ) {
				
				$descodificar = base64_decode($arr[$Num]);
		
				$NumBul = $Num + 1;
		
				if ($Extension == ".jpg" or $Extension == ".png") 
					$Nombre = 'GLS_etiqueta_' . $Referencia . ' (' . $NumBul . ' de ' . $Tot . ')' . $Extension;
				else
					$Nombre = 'GLS_etiqueta_' . $Referencia . $Extension;
				
				if(!$fp2 = fopen($Nombre,"wb+")){
					echo 'IMPOSIBLE ABRIR EL ARCHIVO $Nombre <br>';
				}
		
				if(!fwrite($fp2, trim($descodificar))){
					echo'IMPOSIBLE escribir EL ARCHIVO $Nombre <br>';
				}
				fclose($fp2);
		
				echo 'Etiqueta generada en fichero: ' . $Nombre . '<br>';
			}
}
	
	}
	 
	public function applyGls($order_id = null){
		 
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'PostalServiceDesc' );	 
		$this->loadModel( 'GlsSpain' );
		$this->loadModel( 'Country' );
		 
 		$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id,'MergeUpdate.status' => 0 ,   'MergeUpdate.delevery_country' => 'Spain')));	
		 
		if(count($orders) > 0){
			
			App::import( 'Controller' , 'Cronjobs' );		
			$objController 	= new CronjobsController();
			$getorderDetail	= $objController->getOpenOrderById($order_id);
			$cInfo = $getorderDetail['customer_info']; 			
			$TotalsInfo = $getorderDetail['totals_info'];
			
 			foreach($orders  as $orderItems){
			 
			$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $orderItems['MergeUpdate']['product_order_id_identify'],'MergeUpdate.status' => 0 ,'MergeUpdate.delevery_country' => 'Spain')));	
					
				if(count($orderItem) > 0)
				{
  					$source_coming 	 = $orderItem['MergeUpdate']['source_coming'];
					$quantity 	   	 = $orderItem['MergeUpdate']['quantity'];
					$packet_weight 	 = $orderItem['MergeUpdate']['packet_weight']; 
					$packet_length 	 = $orderItem['MergeUpdate']['packet_length']; 
					$packet_height 	 = $orderItem['MergeUpdate']['packet_height']; 
					$packet_width 	 = $orderItem['MergeUpdate']['packet_width']; 
					$order_id 	  	 = $orderItem['MergeUpdate']['order_id'];
					$inc_order_id 	 = $orderItem['MergeUpdate']['id'];
					$split_order_id  = $orderItem['MergeUpdate']['product_order_id_identify'];
					$country 		 = $cInfo->Address->Country;
					$weight		   	 = $packet_weight;
				 				
					$dim = array($packet_width,$packet_height,$packet_length);
					asort($dim);
					$final_dim = array_values($dim) ;	
					
 					$filterResults = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'GLS' ),'order'=>'PostalServiceDesc.per_item ASC'));
					$all_postal = array();
					 
					foreach($filterResults as $pv){
				 
						$per_item 	= $pv['PostalServiceDesc']['per_item'];
						$per_kg 	= $pv['PostalServiceDesc']['per_kilo'];
						$psd_id 	= $pv['PostalServiceDesc']['id'];
						$all_postal[ $psd_id ] = ( $per_item + ($per_kg * $packet_weight));
									 
					} 
					
					$postalFee  = min($all_postal);	
					$post_id    = array_search($postalFee, $all_postal); 
					$postal_service = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));				  		
					$Delivery_Name = utf8_decode(substr($cInfo->Address->FullName,0,78)); 
					$Delivery_Address = utf8_decode(substr(($cInfo->Address->Address1.' '.$cInfo->Address->Address2),0,78));
					 
					$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
					$randomNum = substr(str_shuffle("0123456789"), 0, 5);
					$ShipmentReference = $randomNum.'-'. $split_order_id;
					
 					$sender = 'FRESHER BUSINESS LIMITED C/O G.L. Systems'; 
 					if(strpos($source_coming,'CostBreaker')!== false){
						$sender = 'EURACO GROUP LTD C/O G.L. Systems'; 
					}else if(strpos($source_coming,'Marec')!== false){					 
						$sender = 'ESL LIMITED C/O G.L. Systems'; 
					}else if(strpos($source_coming,'RAINBOW')!== false){						
						$sender = 'FRESHER BUSINESS LIMITED C/O G.L. Systems'; 
					}else if(strpos($source_coming,'Tech_Drive')!== false){						
						$sender = 'TD Supplies  C/O G.L. Systems'; 
					}elseif(strpos($source_coming,'BBD')!== false){
						$sender = 'JIJ Hongkong Ltd C/O G.L. Systems'; 
					}
 					
					
					$URL = "https://wsclientes.asmred.com/b2b.asmx?wsdl";
				 
 					$XML= '<?xml version="1.0" encoding="utf-8"?>
					<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
					<soap12:Body>
					<GrabaServicios  xmlns="http://www.asmred.com/">
					<docIn>
					<Servicios uidcliente="'.$this->uidcliente.'" xmlns="http://www.asmred.com/">
					<Envio>
					<Fecha>' . date('d/m/Y'). '</Fecha>
					<Servicio>1</Servicio>
					<Horario>3</Horario>
					<Bultos>1</Bultos>
					<Peso>' . ceil($weight) . '</Peso>
					<Portes>P</Portes>
					<Importes>
					  <Reembolso>0</Reembolso>
					</Importes>
					<Remite>
					  <Nombre>'.$sender.'</Nombre>
					  <Direccion>Street / Fontaneros 3</Direccion>
					  <Poblacion>San Fernando de Henares</Poblacion>					 
					  <CP>28330</CP>
					</Remite>
					<Destinatario>
					  <Nombre>' . $Delivery_Name. '</Nombre>
					  <Direccion>' . $Delivery_Address. '</Direccion>
					  <Poblacion>' . utf8_decode($cInfo->Address->Town) . '</Poblacion>
					  <Pais>' .  $country_data['Country']['iso_2']. '</Pais>
					  <CP>' . $cInfo->Address->PostCode . '</CP>
					  <Telefono>' . $cInfo->Address->PhoneNumber . '</Telefono>
					  <Email>' . $cInfo->Address->EmailAddress . '</Email>
					  <NIF></NIF>
					  <Observaciones>' . $split_order_id . '</Observaciones>
					</Destinatario>
					<Referencias>
					  <Referencia tipo="C">' . $ShipmentReference. '</Referencia>
					   <Referencia tipo="0">'.$randomNum.'</Referencia> 
					</Referencias>
					</Envio>
					</Servicios>
					</docIn>
					</GrabaServicios>
					</soap12:Body>
					</soap12:Envelope>';
					
					file_put_contents(WWW_ROOT."logs/gls_apply_req_".date('dmy').".txt", $XML ."\n", FILE_APPEND|LOCK_EX);
					
					$ch = curl_init();
					
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_HEADER, FALSE);
					curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
					curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
					curl_setopt($ch, CURLOPT_URL, $URL );
					curl_setopt($ch, CURLOPT_POSTFIELDS, $XML );
					curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=UTF-8"));
					
					//echo 'xml: ' . $XML . '<br><br>';
					
					$postResult = curl_exec($ch);
					curl_close($ch);
					echo 'postResult: ' . $postResult . '<br><br>';
					
					file_put_contents(WWW_ROOT."logs/gls_apply_res_".date('dmy').".txt", $postResult ."\n", FILE_APPEND|LOCK_EX);
					
					$xml = simplexml_load_string($postResult, NULL, NULL, "http://http://www.w3.org/2003/05/soap-envelope");
					$xml->registerXPathNamespace('asm', 'http://www.asmred.com/');
					$arr = $xml->xpath("//asm:GrabaServiciosResponse/asm:GrabaServiciosResult");
					$ret = $arr[0]->xpath("//Servicios/Envio");
					
					$return = $ret[0]->xpath("//Servicios/Envio/Resultado/@return");
					 sleep(1);
					$gls = array();
					if($return[0] == 0){
						$cb = $ret[0]->xpath("//Servicios/Envio/@codbarras");
						$gls['shipment_barcode'] = $cb[0]["codbarras"];						
						$uid = $ret[0]->xpath("//Servicios/Envio/@uid");						 
						$gls['shipment_uid'] = $uid[0]["uid"];
						$codexp = $ret[0]->xpath("//Servicios/Envio/@codexp");						 
						$gls['shipment_number'] = $codexp[0]["codexp"]; 
						
						$glsRef = array();	
						$glsRef['id'] 					= $orderItem['MergeUpdate']['id'];
   						$glsRef['service_name'] 		= $postal_service['PostalServiceDesc']['service_name'];
						$glsRef['track_id'] 			= '';
						$glsRef['reg_post_number'] 		= '';
						$glsRef['reg_num_img'] 			= '';
						//$glsRef['postal_service'] 	= 'Standard';
						$glsRef['provider_ref_code'] 	= $postal_service['PostalServiceDesc']['provider_ref_code'];
						$glsRef['service_id'] 			= $postal_service['PostalServiceDesc']['id'];
						$glsRef['service_provider'] 	= $postal_service['PostalServiceDesc']['courier'];								 
						$glsRef['template_id'] 			= $postal_service['PostalServiceDesc']['template_id'];  
						$glsRef['gls_shipment_ref'] 	= $ShipmentReference;
						$glsRef['gls_shipment_barcode'] = $gls['shipment_barcode']; 
						
 						$this->MergeUpdate->saveAll($glsRef);
						
						$gls['split_order_id']  	= $split_order_id;
						$gls['shipment_reference'] 	= $randomNum;
						$this->GlsSpain->saveAll( $gls ); 
						$this->glsLabel($ShipmentReference,$split_order_id);
						//$this->getSlipLabel($ShipmentReference,$split_order_id);
						 
					}else{
						$err = $arr[0]->xpath("//Servicios/Envio/Errores/Error") ;
						file_put_contents(WWW_ROOT."logs/".$ShipmentReference."-err.txt", $postResult);
						$getBase = Router::url('/', true);
						$details['split_order_id'] = $split_order_id;	
						$details['error_msg'] = $err[0];						 
						$details['error_file'] ='<a href="'.$getBase.'logs/'.$ShipmentReference.'-err.txt">'.$ShipmentReference.'</a>' ;
						$this->sendErrorMail($details);
						$this->lockOrder($details);
					}					  
					
				}
			}
		}
	}
 	
	public function glsLabel($shipment_reference = '',$split_order_id = ''){
		   
		$url = "https://wsclientes.asmred.com/b2b.asmx"; 
		$XML='<?xml version="1.0" encoding="utf-8"?>
			<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
			<soap12:Body>
			<EtiquetaEnvio xmlns="http://www.asmred.com/">
				<codigo>'.$shipment_reference.'</codigo>
				<tipoEtiqueta>PDF</tipoEtiqueta>
			</EtiquetaEnvio>
			</soap12:Body>
			</soap12:Envelope>';
		
		file_put_contents(WWW_ROOT."logs/glsLabel-req-".date('dmy').".log", $XML ."\n", FILE_APPEND|LOCK_EX);
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $XML);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=UTF-8"));
		
		$postResult = curl_exec($ch);
		file_put_contents(WWW_ROOT."logs/glsLabel-res-".date('dmy').".log", $postResult ."\n", FILE_APPEND|LOCK_EX);
		$msg = '';
		if (curl_errno($ch)) {
			$msg = 'The GLS WS could not be called.<br>';
		}
		
		$result = strpos($postResult, '<base64Binary>');
		
		if($result == false){
			$msg .= 'No tags have been returned.';
		}
		else {
			 
 			$xml = simplexml_load_string($postResult, NULL, NULL, "http://http://www.w3.org/2003/05/soap-envelope");
			$xml->registerXPathNamespace("asm","http://www.asmred.com/");
			$arr = $xml->xpath("//asm:EtiquetaEnvioResponse/asm:EtiquetaEnvioResult/asm:base64Binary");
		
			$Tot = sizeof($arr);
		
				for( $Num=0 ; $Num <= sizeof($arr)-1 ; $Num++ ) {
					$descodificar = base64_decode($arr[$Num]);
					$NumBul = $Num + 1;
					$Nombre =  $split_order_id . ".pdf";
					file_put_contents(WWW_ROOT."gls/labels/label_".$split_order_id.".pdf", $descodificar);	
					$msg = 'ok'; 
			}
		}
		return $msg;   
	}
	
	public function glsCancelShipment($ref_barcode = ''){
		
 		$url = "https://wsclientes.asmred.com/b2b.asmx";				 
  	    $XML= '<?xml version="1.0" encoding="utf-8"?>
			 <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:asm="http://www.asmred.com/">
			 <soap:Header/>	
			  <soap:Body>			
			 <asm:Anula>
				<asm:docIn>
					<Servicios uidcliente="'.$this->uidcliente.'">
						<Envio codbarras="'.$ref_barcode.'" />					
					</Servicios>
			   </asm:docIn>  
			   </asm:Anula>
			</soap:Body>
			</soap:Envelope>';
		
		 file_put_contents(WWW_ROOT."logs/gls-cancel-shipment-req-".date('dmy').".log", $XML ."\n", FILE_APPEND|LOCK_EX);
	 
 		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $XML);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=UTF-8"));
		
		echo $postResult = curl_exec($ch);
		
		file_put_contents(WWW_ROOT."logs/gls-cancel-shipment-res-".date('dmy').".log", $postResult ."\n", FILE_APPEND|LOCK_EX);
		exit;
	}
	
	public function glsTracking(){
	
 		$Busca = "41038-1750260-1";
		$URL= "https://wsclientes.asmred.com/b2b.asmx?wsdl";
	
		$XML= '<?xml version="1.0" encoding="utf-8"?>
			   <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
				 <soap12:Body>
				   <GetExpCli xmlns="http://www.asmred.com/">
					 <codigo>' . $Busca . '</codigo>
					 <uid>' . $this->uidcliente.'</uid>
				   </GetExpCli>
				 </soap12:Body>
			   </soap12:Envelope>';
	
		file_put_contents(WWW_ROOT."logs/glsTracking-req-".date('dmy').".log", $XML ."\n", FILE_APPEND|LOCK_EX);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($ch, CURLOPT_URL, $URL );
		curl_setopt($ch, CURLOPT_POSTFIELDS, $XML );
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=UTF-8"));
	
		$postResult = curl_exec($ch);
		curl_close($ch);
	
		file_put_contents(WWW_ROOT."logs/glsTracking-res-".date('dmy').".log", $postResult ."\n", FILE_APPEND|LOCK_EX);
		//echo 'uidCliente: ' . $uidCliente . '<br><br>';;
		//echo 'xml: ' . $XML . '<br><br>';
		//echo 'postResult: ' . $postResult . '<br><br>';
	
		$xml = simplexml_load_string($postResult, NULL, NULL, "http://http://www.w3.org/2003/05/soap-envelope");
		$xml->registerXPathNamespace('asm', 'http://www.asmred.com/');
		$arr = $xml->xpath("//asm:GetExpCliResponse/asm:GetExpCliResult");
	
		if (sizeof($arr) == 0){
		   echo '<font size="5">Posiblemente el "UID Cliente" est� mal informado.</font>';
	   }
		else {
		   $ret2 = $arr[0]->xpath("//expediciones/exp");
		   
		   if ($ret2 == null){
			  echo '<font size="5">Ning�n env�o encontrado con la referencia: ' . $Busca . '</font>';
			  echo '<font size="5">No shipment found with reference: ' . $Busca . '</font>';
		  } else {
		   
			  
			  echo '<font size="7">' . sizeof($ret2) . '</font> shipments found with reference: ' . $Busca . '<br><br><br>';
			  
			  $Num = 0;
			  foreach ($ret2 as $ret) {
				 
				 $Num2 = $Num + 1;
			  
				 //=================================================================================================================
				 // RECOGER DATOS DE EXPEDICION.
				 // GET SHIPMENT DATA.
				 //=================================================================================================================
				 $expedicion       = $ret[0]->xpath("//expediciones/exp/expedicion");
				 $albaran          = $ret[0]->xpath("//expediciones/exp/albaran");
				 $codexp           = $ret[0]->xpath("//expediciones/exp/codexp");
				 $codbar           = $ret[0]->xpath("//expediciones/exp/codbar");
				 $uidExp           = $ret[0]->xpath("//expediciones/exp/uidExp");
																						
				 $codplaza_cli     = $ret[0]->xpath("//expediciones/exp/codplaza_cli");
				 $codcli           = $ret[0]->xpath("//expediciones/exp/codcli");
				 $nmCliente        = $ret[0]->xpath("//expediciones/exp/nmCliente");
																						
				 $fecha            = $ret[0]->xpath("//expediciones/exp/fecha");
				 $FPEntrega        = $ret[0]->xpath("//expediciones/exp/FPEntrega");
																						
				 $nombre_org       = $ret[0]->xpath("//expediciones/exp/nombre_org");
				 $nif_org          = $ret[0]->xpath("//expediciones/exp/nif_org");
				 $calle_org        = $ret[0]->xpath("//expediciones/exp/calle_org");
				 $localidad_org    = $ret[0]->xpath("//expediciones/exp/localidad_org");
				 $cp_org           = $ret[0]->xpath("//expediciones/exp/cp_org");
				 $tfno_org         = $ret[0]->xpath("//expediciones/exp/tfno_org");
				 $departamento_org = $ret[0]->xpath("//expediciones/exp/departamento_org");
				 $codpais_org      = $ret[0]->xpath("//expediciones/exp/codpais_org");
																						
				 $nombre_dst       = $ret[0]->xpath("//expediciones/exp/nombre_dst");
				 $nif_dst          = $ret[0]->xpath("//expediciones/exp/nif_dst");
				 $calle_dst        = $ret[0]->xpath("//expediciones/exp/calle_dst");
				 $localidad_dst    = $ret[0]->xpath("//expediciones/exp/localidad_dst");
				 $cp_dst           = $ret[0]->xpath("//expediciones/exp/cp_dst");
				 $tfno_dst         = $ret[0]->xpath("//expediciones/exp/tfno_dst");
				 $departamento_dst = $ret[0]->xpath("//expediciones/exp/departamento_dst");
				 $codpais_dst      = $ret[0]->xpath("//expediciones/exp/codpais_dst");
																						
				 $codServicio      = $ret[0]->xpath("//expediciones/exp/codServicio");
				 $codHorario       = $ret[0]->xpath("//expediciones/exp/codHorario");
				 $servicio         = $ret[0]->xpath("//expediciones/exp/servicio");
				 $horario          = $ret[0]->xpath("//expediciones/exp/horario");
																						
				 $tipo_portes      = $ret[0]->xpath("//expediciones/exp/tipo_portes");
				 $bultos           = $ret[0]->xpath("//expediciones/exp/bultos");
				 $kgs              = $ret[0]->xpath("//expediciones/exp/kgs");
				 $vol              = $ret[0]->xpath("//expediciones/exp/vol");
				 $Observacion      = $ret[0]->xpath("//expediciones/exp/Observacion");
				 $dac              = $ret[0]->xpath("//expediciones/exp/dac");
				 $retorno          = $ret[0]->xpath("//expediciones/exp/retorno");
																						
				 $borrado          = $ret[0]->xpath("//expediciones/exp/borrado");
				 $codestado        = $ret[0]->xpath("//expediciones/exp/codestado");
				 $estado           = $ret[0]->xpath("//expediciones/exp/estado");
				 $incidencia       = $ret[0]->xpath("//expediciones/exp/incidencia");
			  
				 //=================================================================================================================
				 // MOSTRAR DATOS DE EXPEDICION.
				 // SHOW EXPEDITION DATA.
				 //=================================================================================================================
				 echo '<strong><font size="5">ENVIO N�MERO (SHIPMENT NUMBER) ' . $Num2 . '</font></strong><br><br>';
				 
				 echo'<BLOCKQUOTE>';
				 echo '<strong>Datos de identificaci�n (Identification information)</strong><br>';
				 echo '<table border="1" bgcolor="0#b0">';
				 echo '<tr>';
				 echo '<td><strong>Env�o</strong></td>';
				 echo '<td><strong>Expedici�n</strong></td>';
				 echo '<td><strong>Albar�n</strong></td>';
				 echo '<td><strong>Cod.Exp</strong></td>';
				 echo '<td><strong>Cod.Barras</strong></td>';
				 echo '<td><strong>Uid.Exp</strong></td>';
				 echo '</tr>';
				 echo '<tr>';
				 echo '<td>' . '#' . $Num2 . '</td>';
				 echo '<td>' . $expedicion[$Num] . '</td>';
				 echo '<td>' . $albaran[$Num] . '</td>';
				 echo '<td>' . $codexp[$Num] . '</td>';
				 echo '<td>' . $codbar[$Num] . '</td>';
				 echo '<td>' . $uidExp[$Num] . '</td>';
				 echo '</tr></table><br>';
			  
				 echo '<strong>Datos de env�o (Shipment information)</strong><br>';
				 echo '<table border="1" bgcolor="0#b0">';
				 echo '<tr>';
				 echo '<td><strong>Plaza Paga</strong></td>';
				 echo '<td><strong>Cod.Cliente</strong></td>';
				 echo '<td><strong>Nom.Cliente</strong></td>';
				 echo '<td><strong>Fec.Env�o</strong></td>';
				 echo '<td><strong>Fec.Prev.Entrega</strong></td>';
				 echo '</tr>';
				 echo '<tr>';
				 echo '<td>' . $codplaza_cli[$Num] . '</td>';
				 echo '<td>' . $codcli[$Num] . '</td>';
				 echo '<td>' . $nmCliente[$Num] . '</td>';
				 echo '<td>' . $fecha[$Num] . '</td>';
				 echo '<td>' . $FPEntrega[$Num] . '</td>';
				 echo '</tr></table><br>';
			  
				 echo '<strong>Datos de Remitente (Sender information)</strong><br>';
				 echo '<table border="1" bgcolor="#f#f0">';
				 echo '<tr>';
				 echo '<td><strong>Nombre</strong></td>';
				 echo '<td><strong>Nif</strong></td>';
				 echo '<td><strong>Direcci�n</strong></td>';
				 echo '<td><strong>Localidad</strong></td>';
				 echo '<td><strong>Cod.Postal</strong></td>';
				 echo '<td><strong>Tel�fono</strong></td>';
				 echo '<td><strong>Departamento</strong></td>';
				 echo '<td><strong>CodPais</strong></td>';
				 echo '</tr>';
				 echo '<tr>';
				 echo '<td>' . $nombre_org[$Num] . '</td>';
				 echo '<td>' . $nif_org[$Num] . '</td>';
				 echo '<td>' . $calle_org[$Num] . '</td>';
				 echo '<td>' . $localidad_org[$Num] . '</td>';
				 echo '<td>' . $cp_org[$Num] . '</td>';
				 echo '<td>' . $tfno_org[$Num] . '</td>';
				 echo '<td>' . $departamento_org[$Num] . '</td>';
				 echo '<td>' . $codpais_org[$Num] . '</td>';
				 echo '</tr></table><br>';
			  
				 echo '<strong>Datos de Destinatario (Consignee information)</strong><br>';
				 echo '<table border="1" bgcolor="#f#f0">';
				 echo '<tr>';
				 echo '<td><strong>Nombre</strong></td>';
				 echo '<td><strong>Nif</strong></td>';
				 echo '<td><strong>Direcci�n</strong></td>';
				 echo '<td><strong>Localidad</strong></td>';
				 echo '<td><strong>Cod.Postal</strong></td>';
				 echo '<td><strong>Tel�fono</strong></td>';
				 echo '<td><strong>Departamento</strong></td>';
				 echo '<td><strong>CodPais</strong></td>';
				 echo '</tr>';
				 echo '<tr>';
				 echo '<td>' . $nombre_dst[$Num] . '</td>';
				 echo '<td>' . $nif_dst[$Num] . '</td>';
				 echo '<td>' . $calle_dst[$Num] . '</td>';
				 echo '<td>' . $localidad_dst[$Num] . '</td>';
				 echo '<td>' . $cp_dst[$Num] . '</td>';
				 echo '<td>' . $tfno_dst[$Num] . '</td>';
				 echo '<td>' . $departamento_dst[$Num] . '</td>';
				 echo '<td>' . $codpais_dst[$Num] . '</td>';
				 echo '</tr></table><br>';
			  
				 echo '<strong>Otros datos (another information)</strong><br>';
				 echo '<table border="1" bgcolor="#c#c#c">';
				 echo '<tr>';
				 echo '<td><strong>Cod.Servicio</strong></td>';
				 echo '<td><strong>Nom.Servicio</strong></td>';
				 echo '<td><strong>Cod.Horario</strong></td>';
				 echo '<td><strong>Nom.Horario</strong></td>';
				 echo '<td><strong>Portes</strong></td>';
				 echo '<td><strong>Bultos</strong></td>';
				 echo '<td><strong>Kgs</strong></td>';
				 echo '<td><strong>Volumen</strong></td>';
				 echo '<td><strong>Observaciones</strong></td>';
				 echo '<td><strong>RCS</strong></td>';
				 echo '<td><strong>Retorno</strong></td>';
				 echo '<td><strong>Borrado</strong></td>';
				 echo '<td><strong>Cod.Estado.Actual</strong></td>';
				 echo '<td><strong>Nom.Estado.Actual</strong></td>';
				 echo '<td><strong>Nom.Incidencia.Actual</strong></td>';
				 echo '</tr>';
				 echo '<tr>';
				 echo '<td>' . $codServicio[$Num] . '</td>';
				 echo '<td>' . $servicio[$Num] . '</td>';
				 echo '<td>' . $codHorario[$Num] . '</td>';
				 echo '<td>' . $horario[$Num] . '</td>';
				 echo '<td>' . $tipo_portes[$Num] . '</td>';
				 echo '<td>' . $bultos[$Num] . '</td>';
				 echo '<td>' . $kgs[$Num] . '</td>';
				 echo '<td>' . $vol[$Num] . '</td>';
				 echo '<td>' . $Observacion[$Num] . '</td>';
				 echo '<td>' . $dac[$Num] . '</td>';
				 echo '<td>' . $retorno[$Num] . '</td>';
				 echo '<td>' . $borrado[$Num] . '</td>';
				 echo '<td>' . $codestado[$Num] . '</td>';
				 echo '<td>' . $estado[$Num] . '</td>';
				 echo '<td>' . $incidencia[$Num] . '</td>';
				 echo '</tr></table><br>';
			  
				 //=================================================================================================================
				 // MOSTRAR TRACKING.
				 // SHOW TRACKING.
				 //=================================================================================================================
				 echo '<strong>Historial (Tracking)</strong><br>';
				 echo '<table border="1" bgcolor="0#f0">';
				 echo '<tr>';
				 echo '<td><strong>Fecha</strong></td>';
				 echo '<td><strong>Tipo</strong></td>';
				 echo '<td><strong>C�digo</strong></td>';
				 echo '<td><strong>Descripci�n</strong></td>';
				 echo '<td><strong>Cod.Agencia</strong></td>';
				 echo '<td><strong>Nom.Agencia</strong></td>';
				 echo '</tr>';
			  
				 $ret3 = $ret2[$Num]->xpath("tracking_list/tracking");
				 $Num3 = 0;
				 foreach ($ret3 as $ret) {
					$TrkFecha   = $ret[0]->xpath("//expediciones/exp/tracking_list/tracking/fecha");
					$TrkTipo    = $ret[0]->xpath("//expediciones/exp/tracking_list/tracking/tipo");
					$TrkCodigo  = $ret[0]->xpath("//expediciones/exp/tracking_list/tracking/codigo");
					$TrkDesc    = $ret[0]->xpath("//expediciones/exp/tracking_list/tracking/evento");
					$TrkCodAge  = $ret[0]->xpath("//expediciones/exp/tracking_list/tracking/plaza");
					$TrkNomAge  = $ret[0]->xpath("//expediciones/exp/tracking_list/tracking/nombreplaza");
					
					echo '<tr>';
					echo '<td>' . $TrkFecha[$Num3]  . '</td>';
					echo '<td>' . $TrkTipo[$Num3]   . '</td>';
					echo '<td>' . $TrkCodigo[$Num3] . '</td>';
					echo '<td>' . $TrkDesc[$Num3]   . '</td>';
					echo '<td>' . $TrkCodAge[$Num3] . '</td>';
					echo '<td>' . $TrkNomAge[$Num3] . '</td>';
					echo '</tr>';
					
					$Num3 = $Num3 + 1;
				 }
				 echo '</table><br>';
				 
				 //=================================================================================================================
				 // MOSTRAR DIGITALIZACIONES.
				 // SHOW DIGITIZATIONS.
				 //=================================================================================================================
				 echo '<strong>Digitalizaciones (images)</strong><br>';
				 echo '<table border="1" bgcolor="#a#f#f">';
				 echo '<tr>';
				 echo '<td><strong>Fecha</strong></td>';
				 echo '<td><strong>C�digo</strong></td>';
				 echo '<td><strong>Tipo</strong></td>';
				 echo '<td><strong>Imagen</strong></td>';
				 echo '<td><strong>Observaciones</strong></td>';
				 echo '</tr>';
				 
				 $ret4 = $ret2[$Num]->xpath("digitalizaciones/digitalizacion");
				 $Num4 = 0;
			  
				 foreach ($ret4 as $ret) {
					$DigFecha   = $ret[0]->xpath("//expediciones/exp/digitalizaciones/digitalizacion/fecha");
					$DigCodigo  = $ret[0]->xpath("//expediciones/exp/digitalizaciones/digitalizacion/codtipo");
					$DigTipo    = $ret[0]->xpath("//expediciones/exp/digitalizaciones/digitalizacion/tipo");
					$DigImag    = $ret[0]->xpath("//expediciones/exp/digitalizaciones/digitalizacion/imagen");
					$DigObserv  = $ret[0]->xpath("//expediciones/exp/digitalizaciones/digitalizacion/observaciones");
			  
					echo '<tr>';
					echo '<td>' . $DigFecha[$Num4]  . '</td>';
					echo '<td>' . $DigCodigo[$Num4] . '</td>';
					echo '<td>' . $DigTipo[$Num4]   . '</td>';
					echo '<td>' . '<a href="' . $DigImag[$Num4] . '">Ver pod</a>'  . '</td>';
					echo '<td>' . $DigObserv[$Num4] . '</td>';
					echo '</tr>';
			  
					$Num4 = $Num4 + 1;
				 }
				 echo '</table><br>';
				 echo'</BLOCKQUOTE>';
			  
				 $Num = $Num + 1;
			  }
			  echo '</table>';
		   }
		}
	 exit;
	}
	
	public function glsManifest(){
 		
		$this->loadModel( 'MergeUpdate' ); 
 		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'GlsSpain' );
  					
		$orders = $this->MergeUpdate->find('all', array('conditions' => array(	'MergeUpdate.status' => 1, 
																				'MergeUpdate.delevery_country'=>'Spain',
																				'MergeUpdate.scan_status' => 1,
																				'MergeUpdate.sorted_scanned' => 1,
																				'MergeUpdate.manifest_status' => 2,
																				'MergeUpdate.gls_shipment_barcode IS NOT NULL',
																				'MergeUpdate.gls_shipment_ref IS NOT NULL'
																				
																				) ) );
																				
																				
																								
		$orders = $this->MergeUpdate->find('all', array( 'conditions' => array('gls_shipment_ref IS NOT NULL','gls_shipment_barcode IS NOT NULL')) );
		
		if(count($orders) > 0){
			
				$filename = date('Ymd_His')."_gls";
			
			//file_put_contents(WWW_ROOT."gls/manifests/".$filename, "Expedicion,Doc,Ref. GLS,Ref. Client,Date,Service,Hour,Parcels, Kgs, Refnd, Portes,DAC, Ret., Addressee, Department, Address, Location, Country\n");	
			 
				//ob_clean();                                                         
				App::import('Vendor', 'PHPExcel/IOFactory');
				App::import('Vendor', 'PHPExcel');                          
 				 
				$objPHPExcel = new PHPExcel();       
				$objPHPExcel->createSheet();
 				$objPHPExcel->setActiveSheetIndex(0);
				$inc = 1;
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Customer Number:');  
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, '1083');  
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->getFont()->setSize(18)->setBold(true)->getColor()->setRGB('0000FF');	
				
				$inc++;	
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Custome Name:');
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, 'EURACO GROUP LTD'); 				
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->getFont()->setSize(16)->setBold(true)->getColor()->setRGB('008000');	
				
				$inc++;	
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Manifiesto cierre:');  
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, date('d/m/Y H:i:s'));  
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->getFont()->setSize(12)->setBold(true)->getColor()->setRGB('808080');		
				
				$inc++;
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Number of Shipment:'.count($orders));   
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->getFont()->setSize(12)->setBold(true)->getColor()->setRGB('808080');		
				
				$inc++;
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Number of bultos:'.count($orders));   
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->getFont()->setSize(12)->setBold(true)->getColor()->setRGB('808080');		
				$bborder = array(
				  'borders' => array(
						'bottom' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)																
				  )
				);
				$tborder = array(
				  'borders' => array(
						'top' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)																
				  )
				);
				
				$inc++;	$inc++;	
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->applyFromArray($tborder);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Expedicion');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, 'Doc');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, 'Ref. GLS');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 'Ref. Client');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, 'Date');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 'Service');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 'Hour');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 'Parcels');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 'Kgs');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 'Refnd');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 'Portes');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 'DAC');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 'Ret.');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 'Addressee');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 'Department');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, 'Address');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 'Location');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, 'Country');                                                                
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->getFont()->setSize(12)->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':R'.$inc)->applyFromArray($bborder);		
				foreach($orders as $val){
				$inc++;
				$glsinfo = $this->GlsSpain->find('first', array( 'conditions' => array('shipment_barcode'=>$val['MergeUpdate']['gls_shipment_barcode'],'split_order_id'=>$val['MergeUpdate']['product_order_id_identify'])) );
				
				$sopen = $this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id'=>$val['MergeUpdate']['order_id'])));
				
				$general_info		=	unserialize( $sopen['OpenOrder']['general_info'] );
				$shipping_info		=	unserialize( $sopen['OpenOrder']['shipping_info'] );
				$customer_info		=	unserialize( $sopen['OpenOrder']['customer_info'] );
				$totals_info		=	unserialize( $sopen['OpenOrder']['totals_info'] );
				$items				=	unserialize( $sopen['OpenOrder']['items'] );
				
				$consignee_name 	=	($customer_info->Address->FullName);
				$address1 			= 	rtrim($this->getcommaremovedata(($customer_info->Address->Address1)),' ');
				$address2 			= 	rtrim($this->getcommaremovedata(($customer_info->Address->Address2)),' ');
 				$consigee_zip_code 	= 	str_pad($customer_info->Address->PostCode,5,"`0",STR_PAD_LEFT);
				$consignee_city 	=	rtrim($this->getcommaremovedata(($customer_info->Address->Town)),' '); 
			 	$consignee_country	=   $customer_info->Address->Country;
							
				$consignee_address  = 	rtrim($address1.' '.$address2,' ')." ".$consigee_zip_code ." ".$consignee_city;
				
				if($customer_info->Address->Region != ''){
					$consignee_address .= 	" ". $this->getcommaremovedata(($customer_info->Address->Region));
				}
				
				$expedicion = '771-'.$glsinfo['GlsSpain']['shipment_number'];
				
				//file_put_contents(WWW_ROOT."gls/manifests/".$filename, "{$expedicion},Doc,".$val['MergeUpdate']['gls_shipment_barcode'].",".$val['MergeUpdate']['gls_shipment_ref'].",".$val['MergeUpdate']['merge_order_date'].", ".				$val['MergeUpdate']['service_name'].",0,1,1,,P,,,{$consignee_name},,{$consignee_address},,{$consignee_country}"."\n", FILE_APPEND|LOCK_EX);
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $expedicion);                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, '');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $val['MergeUpdate']['gls_shipment_barcode']);                                             
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $val['MergeUpdate']['gls_shipment_ref']);                                                 
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, date('d/m/Y',strtotime($val['MergeUpdate']['merge_order_date'])));
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $val['MergeUpdate']['service_name']);                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, '0');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, '0');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, '0');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, '0');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 'P');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, '0');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, '0');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, $consignee_name);                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, '');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, $consignee_address);                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, '');                                                                  
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, $consignee_country); 
			}	
			
 			$uploadUrl = WWW_ROOT .'gls/manifests/'.$filename.'.xlsx';  
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
			$objWriter->save($uploadUrl);		 
		}
		exit;
	}	
	
 	public function getcommaremovedata($string = null){
			
		$special_chars = array(';'=>' ','"'=>' ',"'"=>' ','\n'=>'',','=>' ');
		$str = strtr( $string,$special_chars );
		return  $str;
	}
  	private function lockOrder($details = null){
	
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderNote' );	
		$this->loadModel( 'RoyalmailError' );
		$md = $this->MergeUpdate->find('first', array( 'conditions' => array('product_order_id_identify' => $details['split_order_id']),'fields'=>'order_id' ) );
		if(count($md) > 0){
			$lock_note = $details['error_msg'];
			
			$firstName = $_SESSION['Auth']['User']['first_name'];
			$lastName = $_SESSION['Auth']['User']['last_name'];
			$mdata['royalmail'] = 0;
			$mdata['status']    = 3;
			$mdata['rm_error_msg'] = "'".$lock_note."'";
			 
			$this->MergeUpdate->updateAll($mdata, array( 'product_order_id_identify' => $details['split_order_id'] ) );
			$this->OpenOrder->updateAll(array('OpenOrder.status' => 3), array('OpenOrder.num_order_id' => $md['MergeUpdate']['order_id']));
			
			$noteDate['order_id'] = $md['MergeUpdate']['order_id'];
			$noteDate['note'] = $lock_note;
			$noteDate['type'] = 'Lock';
			$noteDate['user'] = $firstName.' '.$lastName;
			$noteDate['date'] = date('Y-m-d H:i:s');
			$this->OrderNote->saveAll( $noteDate );			 
		}
	}	
	
	public function getSlipLabel($ShipmentReference = null, $split_order_id = null)
	{ 
	 
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		$this->loadMOdel( 'CategoryContant' );
		$this->loadMOdel( 'Template' );
		$this->loadMOdel( 'BulkLabel' );
		$this->loadModel( 'MergeUpdate');
		$this->loadModel( 'PackagingSlip');
				
		$openOrderId = explode("-",$split_order_id)[0]; 
 
 	 	$label = $this->getGlsLabel($split_order_id,$ShipmentReference);
		$slip  =	$this->getSlip( $split_order_id, $openOrderId );
 		 
	 	$log_file = WWW_ROOT .'logs/dynamic_log/gls_slip_label_'.date('dmy').".log";
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper('A4', 'portrait');
		$date = date("m/d/Y H:i:s");
		
		$html = '';
		
		if($label == 'ok'){
									 
			$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
					 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
					 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
					 <meta content="" name="description"/>
					 <meta content="" name="author"/>';
			$html .= '<table style="margin-top:12px; height:1090px;  width:740px; border:1px solid #CCC;"> 
					<tr><td height="265" valign="top">'.$slip.'</td></tr> 
					<tr><td height="265" valign="top"><div style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
					transform-origin: 50% 50%; margin-top:45px"><img src="'.Router::url('/', true) .'logs/'.$split_order_id.'.png" height="565"></div></td></tr> 
					</table>';
					/*<tr><td valign="top" ><div style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
					transform-origin: 50% 50%;"><img src="'.WWW_ROOT .'logs/'.$split_order_id.'.png"></div></td></tr>*/
				
				$cssPath = WWW_ROOT .'css/';
				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
				 // echo $html; 
			//	$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
				$dompdf->load_html($html, Configure::read('App.encoding'));
				$dompdf->render();
				$imgPath = WWW_ROOT .'img/printPDF/Bulk/'; 
				$path 	 = Router::url('/', true).'img/printPDF/Bulk/';
				$name	 = 'Label_Slip_'.$split_order_id.'.pdf';
				file_put_contents($imgPath.$name, $dompdf->output());
		 
				echo $msg = $split_order_id . "  Label & Slip Printed.";	
		}else{
			echo 'Issue in GLS Label.';
		}
   				
	}
	
	public function getSlip($splitOrderId = null, $openOrderId = null)
	{
	 
	 
		$this->loadModel('OrderLocation');
		App::import('Controller', 'Linnworksapis');
		$obj = new LinnworksapisController();
		$order	=	$obj->getOpenOrderById( $openOrderId );
		
		$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId))); 
		$itemPrice			=		$getSplitOrder['MergeUpdate']['price'];
		$itemQuantity		=		$getSplitOrder['MergeUpdate']['quantity'];
		$BarcodeImage		=		$getSplitOrder['MergeUpdate']['order_barcode_image'];
		$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
		$assignedservice	=	 	$order['assigned_service'];
		$courier			=	 	$order['courier'];
		$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
		$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
		$barcode			=	 	$order['assign_barcode'];
		$subtotal			=		$order['totals_info']->Subtotal;
		$subsource			=		$order['sub_source'];
		$totlacharge		=		$order['totals_info']->TotalCharge;
		$ordernumber		=		$order['num_order_id'];
		$fullname			=		$order['customer_info']->Address->FullName;
		/*$address1			=		$order['customer_info']->Address->Address1;
		$address2			=		$order['customer_info']->Address->Address2;
		$address3			=		$order['customer_info']->Address->Address3;*/
		$address1 			= 		wordwrap($order['customer_info']->Address->Address1, 30, "\n<br>", false);	
		$address2 			= 		wordwrap($order['customer_info']->Address->Address2, 30, "\n<br>", false);	
		$address3			=		$order['customer_info']->Address->Address3;
		if(strlen($order['customer_info']->Address->Address3) > 30)	{
			$address3 		= 		wordwrap($order['customer_info']->Address->Address3, 30, "\n<br>", false);
		}
		$town				=	 	$order['customer_info']->Address->Town;
		$resion				=	 	$order['customer_info']->Address->Region;
		$postcode			=	 	$order['customer_info']->Address->PostCode;
		$country			=	 	$order['customer_info']->Address->Country;
		$phone				=	 	$order['customer_info']->Address->PhoneNumber;
		$company			=	 	$order['customer_info']->Address->Company;
		$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
		$postagecost		=	 	$order['totals_info']->PostageCost;
		$tax				=	 	$order['totals_info']->Tax;
		$barcode  			=   	$order['assign_barcode'];
		$items				=	 	$order['items'];
		$address			=		'';
		$address			.=		($company != '') ? $company.'<br>' : '' ; 
		$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
		$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
		$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
		$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
		$address			.=		($town != '') ? $town.'<br>' : '';
		$address			.=		($resion != '') ? $resion.'<br>' : '';
		$address			.=		($postcode != '') ? $postcode.'<br>' : '';
		$address			.=		($country != '' ) ? $country.'<br>' : '';
		$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);//CostBreaker
		
		$subsource_lower = strtolower($subsource) ;
		$marecArray = array('marec_de','marec_es','marec_fr','marec_it','marec_uk');
		$costbreakerArray = array('costbreaker_de','costbreaker_es','costbreaker_fr','costbreaker_it','costbreaker_uk','costbreaker','costbreaker_ca','costbreaker_usabuyer');
		$techdriveArray = array('tech_drive_de','tech_drive_es','tech_drive_fr','tech_drive_it','tech_drive_uk');				
		$rainbowArray = array('rainbow retail','rainbow retail de','rainbow_retail_es','rainbow_retail_it','rainbow_retail_fr');
		$bbdArray = array('bbd_eu_de');	
		
		if(in_array($subsource_lower,$marecArray)){
			$company 	=  'Marec';
		}else if(in_array($subsource_lower,$costbreakerArray)){
			$company 	=  'CostBreaker';
		}else if(in_array($subsource_lower,$techdriveArray)){
			$company 	= 'Tech Drive Supplies';
		}else if(in_array($subsource_lower,$rainbowArray)){
			$company 	= 'Rainbow Retail';
		}else if(in_array($subsource_lower,$bbdArray)){
			$company 	= 'BBD';
		}else if( $subsource == 'EBAY2' ){
			$company 	= 'EBAY2';
		}else if( $subsource == 'EBAY0' ){
			$company 	= 'EBAY0';
		}else if( $subsource == 'EBAY5' ){
			$company 	= 'Marec';
		}
		
		if($splitOrderId == '1668658-1'){
			//echo $company  ;exit;
		}
		
		$i = 1;
		$str = '';
		$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
		$totalGoods = 0;
		$productInstructions = '';
		$ref_code = trim($getSplitOrder['MergeUpdate']['provider_ref_code']);
		$codearray = array('DP1', 'FR7');
		
		$this->loadModel( 'BulkSlip' );
		if($subsource_lower == 'costbreaker_ca'){
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => 'CostBreaker_CA' ) ) );		
		}elseif($subsource_lower == 'costbreaker_usabuyer'){
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => 'CostBreaker_USA' ) ) );		
		}else{
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => $company ) ) );
		}
	 	 $gethtml ;
		
		$p_barcode	=	'';
		
		if( in_array($ref_code, $codearray)) 
		{
			
			$per_item_pcost 	= $postagecost/count($items);
			$j = 1;
			foreach($items as $item){
			$item_title 		= 	$item->Title;
			$quantity			=	$item->Quantity;
			$price_per_unit		=	$item->PricePerUnit;
			$str .= '<tr>
						<td style="border:1px solid #000;" align="left" valign="top" width="10%">'.$quantity.'</td>
						<td style="border:1px solid #000;" valign="top" width="30%">'.substr($item_title, 0, 30 ).'</td>
						<td style="border:1px solid #000;" valign="top" width="10%">'.$quantity * $price_per_unit.'</td>
						<td style="border:1px solid #000;" valign="top" width="10%">'.$per_item_pcost.'</td>
						<td style="border:1px solid #000;" valign="top" width="20%">'.(($price_per_unit * $quantity) + $per_item_pcost).'</td>
					</tr>';
			$j++;
			}
			$str .=	'<tr><td></td><td></td><td></td><td style="border:1px solid #000;">Total</td>
						<td style="border:1px solid #000;" valign="top" width="20%">'.$totalcharge.'</td>
					</tr>';
			echo $str;
			
		} 
		else 
		{
			
			foreach( $skus as $sku )
			{
			
				$newSkus[]				=	 explode( 'XS-', $sku);
				$cat_name = array();
				$sub_can_name = array();
				foreach($newSkus as $newSku)
					{
						
				 		$getsku = 'S-'.$newSku[1];
						$getOrderDetail = 	$this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
						$OrderLocation 	= 	$this->OrderLocation->find( 'first', array( 'conditions' => array('order_id' => $openOrderId, 'sku' => $getsku ) )  );
						if( count($OrderLocation) > 0 ){
							$o_location 	=	$OrderLocation['OrderLocation']['bin_location'];
						} else {
							$o_location 	=	'Check Location' ;
						}
						
						$contentCat 	= 	$getOrderDetail['Product']['category_name'];
						$cat_name[] 	= 	$getOrderDetail['Product']['category_name'];
						$contentSubCat	=	$getOrderDetail['Product']['sub_category'];
						$sub_can_name[] =	$getOrderDetail['Product']['sub_category'];
						$productBarcode	=	$getOrderDetail['ProductDesc']['barcode'];
						$getContent	= $this->CategoryContant->find( 'first', array( 
															'conditions'=>array('CategoryContant.sub_category'=>$contentSubCat,'CategoryContant.sub_source'=>$subsource)));
						$productBarcode			=	$getOrderDetail['ProductDesc']['barcode'];
						if(count($getContent) > 0) {
							$getbarcode				= 	'ava';//;$this->getBarcode( $productBarcode );
							$productInstructions 	=  '<p style="font-size:14px;">'.$getContent['CategoryContant']['content'].'</p>';
						}
						
					 	$product_local_barcode ='yyyyyy';// $this->GlobalBarcode->getLocalBarcode($getOrderDetail['ProductDesc']['barcode']);
						
						$title	=	$getOrderDetail['Product']['product_name'];
						$totalGoods = $totalGoods + $newSku[0];
						$str .= '<tr>
								<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
								<td valign="top" class="center rightborder bottomborder">'.$newSku[1].'</td>
								<td valign="top" class="rightborder bottomborder">'.$title.'</td>
								<td valign="top" class="rightborder bottomborder" >'.$product_local_barcode.'</td>
								<td valign="top" class="rightborder bottomborder">'.$o_location.'</td>
								<td valign="top" class="center norightborder bottomborder">'.$newSku[0].'</td>';
						$str .= '</tr>';
						$i++;
						
					}
					unset($newSkus);
			}
			
		}
	 	$str ;		
						
		 
		$setRepArray = array();
		$setRepArray[] 					= $address1;
		$setRepArray[] 					= $address2;
		$setRepArray[] 					= $address3;
		$setRepArray[] 					= $town;
		$setRepArray[] 					= $resion;
		$setRepArray[] 					= $postcode;
		$setRepArray[] 					= $country;
		$setRepArray[] 					= $phone;
		$setRepArray[] 					= $ordernumber;
		$setRepArray[] 					= $courier;
		$setRepArray[] 					= $recivedate[0];
		$totalitem = $i - 1;
		$setRepArray[]	=	 $str;
		$setRepArray[]	=	 $totalGoods;
		$setRepArray[]	=	 $subtotal;
		$Path 			= 	'/wms/img/client/';
		$img			=	 '';
		$setRepArray[]	=	 $img;
		$setRepArray[]	=	 $postagecost;
		$setRepArray[]	=	 $tax;
		$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
		//$setRepArray[]	=	 $totalamount;
		$setRepArray[]	=	 $itemPrice;
		$setRepArray[]	=	 $address;
		$barcodePath  	=  WWW_ROOT.'img/orders/barcode/';
		$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.' width='. $barcodeWidth .'>';
		$barcodenum		=	explode('.', $barcode);
		
		$setRepArray[] 	=  $barcodeimg;
		$setRepArray[] 	=  $paymentmethod;
		$setRepArray[] 	=  $barcodenum[0];
		$setRepArray[] 	=  '';
		$img 			=	$gethtml['PackagingSlip']['image_name'];
		$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
		$setRepArray[] 	=  $returnaddress;
		$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
		$setRepArray[] 	= $splitOrderId;
		$setRepArray[] 	= 	utf8_decode( $productInstructions );
	//	$prodBarcodePath  	=  Router::url('/', true).'img/product/barcodes/';			
		$prodBarcodePath  	= WWW_ROOT.'img/product/barcodes/';
		$setRepArray[] 	=  '<img src='.$prodBarcodePath.$productBarcode.'.png width='. $barcodeWidth .'>';
		$setRepArray[] 	=  $totalamount;
		$setRepArray[]	=	WWW_ROOT;
		$imgPath = WWW_ROOT .'css/';
		$html2 			= 	$gethtml['BulkSlip']['html'];
		//$html2 			= 	'';
		if($splitOrderId == '1668658-1'){
			 //echo $html2;exit;
		}
		if( $company ==  'Marec' && in_array(  'Mobile Accessories', $cat_name ))
		{
			//$this->MobileAsseInstruction( $splitOrderId, $subsource  );
		}
		$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
		$html 		= 	$obj->setReplaceValue( $setRepArray, $html2 );
		return $html;
		
	}
	
	public function getGlsLabel( $split_id = null, $shipment_reference = null )
	{
 		$url = "https://wsclientes.asmred.com/b2b.asmx"; 
 		// Ahora podemos obtener la etiqueta codificada en base64
	/*	$XML='<?xml version="1.0" encoding="utf-8"?>
			<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
			<soap12:Body>
			<GetPlazaXCP xmlns="http://www.asmred.com/"> 
				<codPais>34</codPais>
				<cp>46530</cp>
			</GetPlazaXCP>
			</soap12:Body>
			</soap12:Envelope>';
			*/
			
			$XML='<?xml version="1.0" encoding="utf-8"?>
			<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
			<soap12:Body>
			<EtiquetaEnvio xmlns="http://www.asmred.com/">
				<codigo>'.$shipment_reference.'</codigo>
				<tipoEtiqueta>PNG</tipoEtiqueta>
			</EtiquetaEnvio>
			</soap12:Body>
			</soap12:Envelope>';
		
		 
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $XML);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=UTF-8"));
		
		$postResult = curl_exec($ch);
 
		$msg = '';
		if (curl_errno($ch)) {
			$msg = 'No se pudo llamar al WS de GLS<br>';
		}
		
		$result = strpos($postResult, '<base64Binary>');
		
		if($result == false){
			$msg = 'No se ha retornado ninguna etiqueta.';
		}
		else {
			  
 			$xml = simplexml_load_string($postResult, NULL, NULL, "http://http://www.w3.org/2003/05/soap-envelope");
			$xml->registerXPathNamespace("asm","http://www.asmred.com/");
			$arr = $xml->xpath("//asm:EtiquetaEnvioResponse/asm:EtiquetaEnvioResult/asm:base64Binary");
		
			$Tot = sizeof($arr);
		
			for( $Num=0 ; $Num <= sizeof($arr)-1 ; $Num++ ) {
				
				$descodificar = base64_decode($arr[$Num]);
				$NumBul = $Num + 1;		
				file_put_contents( WWW_ROOT .'logs/'.$split_id.'.png', trim($descodificar));
				$msg = 'ok';
			}
		}
		
		file_put_contents( WWW_ROOT .'logs/getGlsLabel_'.date('dmy').'.log', $split_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);		
		return $msg; 
	}
		
	private function sendErrorMail($details = null){
	
		 
		$subject   = $details['split_order_id'].' GLS Shipping orders issue in Xsensys';
		$mailBody  = '<p><strong>Error :'.$details['error_msg'].'</strong></p>';
  		$mailBody .= '<p>Error file : '.$details['error_file'].'</p>'; 
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('');
		$email->emailFormat('html');
		$email->from('info@euracogroup.co.uk');
		//$email->to( array('avadhesh.kumar@jijgroup.com','shashi@euracogroup.co.uk','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));	
		$email->to( array('avadhesh.kumar@jijgroup.com'));
		$email->subject( $subject );
	  	$email->send( $mailBody );
	}
	
	public function replaceFrenchChar($string = null){
			
		$unwanted_array = array('�'=>'S', '�'=>'s', '�'=>'Z', '�'=>'z', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E','�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', 'N�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U','�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c','�'=>'e', '�'=>'e', '�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n','n�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o','�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'b', '�'=>'y','�'=>'u','�'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}
	
	public function fileFormat(){
		
		$file = 'Inventoryupload24072019.csv';
		$data =	explode("\n",file_get_contents(WWW_ROOT."logs/".$file));
		
		foreach($data as $val){
			$row ='';
			foreach(explode(",",$val) as $v){
				$row.= utf8_encode($this->replaceFrenchChar($v)).",";
			}
			$row = rtrim($row,",");
			file_put_contents(WWW_ROOT."logs/ii_order.csv", $row ."\n", FILE_APPEND|LOCK_EX);
		}
 
		exit;
	}	
	 
}

 
?>