<?php
error_reporting(0);
ini_set('memory_limit', '2048M');
class ProductsController extends AppController
{
    
    var $name = "Products";
    
    var $components = array('Session','Upload','Common','Paginator');
    
    var $helpers = array('Html','Form','Session','Common' , 'Soap' , 'Paginator');
    
    public function beforeFilter()
	{
	    parent::beforeFilter();
	    $this->layout = false;
	    $this->Auth->Allow(array('productVirtualStockCsvGeneration', 'prepareVirtualStock', 'getStockDataForCsv' ,'batchProcessed' , 'prepareExcel','getOverSellSku'));
		$this->GlobalBarcode = $this->Components->load('Common'); 
	}
    
    public function addattributeset()
    {
		$this->layout = 'index';
		$this->loadModel('AttributeSet');
		$attributesets	=	$this->AttributeSet->find('list', array('fields' => array('id', 'set_name')));
		
		$this->set( 'attributesets', $attributesets );
	}
    
    
    public function addproduct($id = null) 
    {
		$setID	=	(isset($this->request->data['Product']['attributeset_id']) && $this->request->data['Product']['attributeset_id']) ? $this->request->data['Product']['attributeset_id'] : '';
		if(empty($this->request->data['Product']['attributeset_id']))
		{
			$this->redirect(array('controller'=>'addProductattributeset'));
		}
		if( isset( $id ) & $id > 0 )
            $this->set( 'title','Edit Product' );
        else
            $this->set( 'title','Add Product' );
	
		$this->layout = "index";
		$this->loadModel( 'Category' );
		$this->loadModel( 'ProductImage' );
		$this->loadModel( 'ProductAttribute' );
		$uploadUrl	=	WWW_ROOT .'img/product/';
		
		$getCategories	=	$this->Category->find('all', array('conditions' => array('Category.parent_id' => 0)));
		
		$flag = false;
        $setNewFlag = 0;
		
		if(!empty($this->request->data))
		{
						
						
						$this->Product->set( $this->request->data );
						if( $this->Product->validates( $this->request->data ) )
						{
							   $flag = true;                                      
						}
						else
						{
						   $flag = false;
						   $setNewFlag = 1;
						   $error = $this->Product->validationErrors;
						    $this->set('errorproduct', $error);
						}
						
						$this->Product->ProductDesc->set( $this->request->data );
						if( $this->Product->ProductDesc->validates( $this->request->data) )
						{
							   $flag = true;                                      
						}
						else
						{
						   $flag = false;
						   $setNewFlag = 1;
						   $error = $this->Product->ProductDesc->validationErrors;
						    $this->set('errordesc', $error);
						}
						
						$this->Product->ProductPrice->set( $this->request->data );
						if( $this->Product->ProductPrice->validates( $this->request->data) )
						{
							   $flag = true;                                      
						}
						else
						{
						   $flag = false;
						   $setNewFlag = 1;
						   $error = $this->Product->ProductPrice->validationErrors;
						   $this->set('errorPrice', $error);
						}
						
						$this->ProductAttribute->set( $this->request->data );
						if( $this->ProductAttribute->validates( $this->request->data) )
						{
							   $flag = true;                                      
						}
						else
						{
						   $flag = false;
						   $setNewFlag = 1;
						   $error = $this->ProductAttribute->validationErrors;
						   //$this->set('errorPrice', $error);
						}
						
						if( $setNewFlag == 0 )
							 {
						
						$this->Product->saveAll( $this->request->data );
						$lastinsertID 		= 	$this->Product->getLastInsertId();
						$i = 1;
						foreach($this->request->data['ProductImage']['product_image'] as $file)
									{
										$this->request->data['ProductImage']['selected_image']		=	(!isset($this->request->data['ProductImage']['select_image'][$i]) && empty($this->request->data['ProductImage']['select_image'][$i])) ? '1' : '0';
										
										if($file['name'] != '' && count($file['name']) > 0 )
											{
												$getImageName = $this->Upload->upload($file, $uploadUrl);
												
												$this->request->data['ProductImage']['image_name'] 		= 	$getImageName;
												$this->request->data['ProductImage']['product_id'] 			= 	$lastinsertID;
												$this->ProductImage->saveAll( $this->request->data['ProductImage']);
											}
											
											$i++;
									}
										
										$this->request->data['ProductAttribute']['attribute_value'] = serialize($this->request->data['ProductAttribute']['attribute_value']);
										$this->request->data['ProductAttribute']['product_id'] = $lastinsertID;
										
										$this->ProductAttribute->saveAll($this->request->data['ProductAttribute']);	
								}
						
			}
			
			$this->loadModel('AttributeSet');
			$this->loadModel('Attribute');
			$this->loadModel('Warehouse');
		
			$attvalue	=	$this->AttributeSet->find('first', array('conditions' => array('AttributeSet.id' => $setID)));
			
			$ids = $attvalue['AttributeSet']['attribute_set_values'];
			$ids	=	explode(',',$ids);
			$attributevalue	=	$this->Attribute->find('all', array('conditions' => array('Attribute.id' => $ids)));
			$this->set('attributeid', $attvalue['AttributeSet']['id']);
			$this->set('attributevalue', $attributevalue);
			$this->Warehouse->bindModel(array('hasMany' => array('WarehouseBin')));
			$warehouse	=	$this->Warehouse->find('all', array('conditions' => array('Warehouse.status' => 0)));
			
			$this->set('warehouses', $warehouse);
			$this->set('getCategories', $getCategories);
			$this->set( 'CountryList', $this->Common->getCountryList() );
		
		
	}
	
	public function getChild($p_id = null)
	{
		echo $p_id;
	}
	
	public function getBinLocation( $barcode = null )
	{
		$this->loadModel( 'BinLocation' );	
		$binLocation	=	$this->BinLocation->find('all', array('conditions' => array('barcode' => $barcode ),'fields' => array('barcode','bin_location','stock_by_location')));
		$location 		= 	array();
		foreach($binLocation as $data)
		{
			 $location[$data['BinLocation']['barcode']][] = array('bin_location' => $data['BinLocation']['bin_location'],
																	'stock_by_location'=> $data['BinLocation']['stock_by_location']);
			
		}	
		if(count($location) > 0 ){
			return $location;
		} else {
			return '1';
		}
	}
	
	public function showallproduct()
    {
		$this->layout = "index";
		
		$this->loadModel( 'BinLocation' );	
		$this->loadModel( 'UnprepareOrder' );		
		/*$binLocation	=	$this->BinLocation->find('all');
		$location 		= 	array();
		foreach($binLocation as $data)
		{
			 $location[$data['BinLocation']['barcode']][] = array('bin_location' => $data['BinLocation']['bin_location'],
																	'stock_by_location'=> $data['BinLocation']['stock_by_location']);
			
		}		
		$this->set('BinLocation',$location);*/
			
		$unpOrders	=	$this->UnprepareOrder->find('all');
		$unp_data 		= 	array();
		foreach($unpOrders as $data){
			
				foreach( unserialize($data['UnprepareOrder']['items']) as $val){
								
					$_sku = explode( '-' , $val->SKU);
					if( count($_sku ) == 3 ) // For Bundle (Single)
					{
						$sku = 'S-'.$_sku[1];
						$qty = $val->Quantity * ($_sku[2]);
						$unp_data[$sku][] = array('quantity'=>$qty, 'order_id'=>$val->OrderId);
					}
					else if( count( $_sku ) > 3 ) // For Bundle (Bundle)
					{						
						$inc = 1;	$ij = 0;
						while( $ij < count($_sku)-2 )
						{						
							$sku = 'S-'.$_sku[$inc];
												
							$inc++;
							$ij++;
							$unp_data[$sku][] = array('quantity'=>$val->Quantity, 'order_id'=>$val->OrderId);
						 
						}
					}else{				
						$unp_data[$val->SKU][] = array('quantity'=>$val->Quantity, 'order_id'=>$val->OrderId);
					}
				}			
		}		
		$this->set('unp_data',$unp_data);
		
        //$productAllDescs = $this->Product->find('all');
                
		$this->paginate = array(
                        'fields' => array( 
                                'DISTINCT Product.product_sku',
                                'Product.*',
                                'ProductDesc.*'
                            ),
			'limit' => 5			
		);
		 
		//$this->Product->recursive = 7;
		
		// we are using the 'Product' model
		$productAllDescs = $this->paginate('Product');
		
			$this->loadModel('MergeUpdate');
			$openorders = array();
			foreach($productAllDescs as $v){
			
				$sku = $v['Product']['product_sku'];
				$getdetails	=	$this->MergeUpdate->find( 'all' , array( 
																	'conditions' => array( 'MergeUpdate.sku LIKE' =>"%{$sku}%",
																							'MergeUpdate.status' => 0 
																			),
																	'fields' => array( 'MergeUpdate.sku' )
																		)
																	);
				if( !empty( $getdetails ) )
				{
					$countSku = 0;
					foreach( $getdetails as $getdetailValue )
					{	
						$allSku   =	$getdetailValue['MergeUpdate']['sku'];
						$coma_exp = explode(",",$allSku);				
						foreach($coma_exp as $pexp){
							 $texp     = explode("-",$pexp);					
							 $t = "S-".trim($texp[1]);
							if($t == $sku){
								$quantity = (int)filter_var($texp[0], FILTER_SANITIZE_NUMBER_INT);	
								$countSku = $countSku + $quantity;	
							}				
						}			
					}
					$openorders[$sku] = $countSku;
				}
			}
		  $this->set('openorders',$openorders);
		 
		  $this->set('productAllDescs',$productAllDescs);	       
		//$productAllDescs = $this->Product->find('all');				
		
		//$this->set('productAllDescs',$productAllDescs);		
		
		$this->set( 'role','Show All Product' );        
	}	
	
	public function showallproduct060517()
    {
		$this->layout = "index";
		
		$this->loadModel( 'BinLocation' );		
		$binLocation	=	$this->BinLocation->find('all');
		$location 		= 	array();
		foreach($binLocation as $data)
		{
			 $location[$data['BinLocation']['barcode']][] = array('bin_location' => $data['BinLocation']['bin_location'],
																	'stock_by_location'=> $data['BinLocation']['stock_by_location']);
			
		}		
		$this->set('BinLocation',$location);
		
        $productAllDescs = $this->Product->find('all');
                
		$this->paginate = array(
                        'fields' => array( 
                                'DISTINCT Product.product_sku',
                                'Product.*',
                                'ProductDesc.*'
                            ),
			'limit' => 250			
		);
		 
		//$this->Product->recursive = 7;
		
		// we are using the 'Product' model
		$productAllDescs = $this->paginate('Product');
                 
		//$productAllDescs = $this->Product->find('all');				
		
		$this->set('productAllDescs',$productAllDescs);		
		
		$this->set( 'role','Show All Product' );        
	}
	
	public function getNthChild()
	{
		$this->layout = "";
		$this->autoRender = false;
		$this->loadModel( 'Category' );
		$id = $this->request->data['id'];
		$getChildList	=	$this->Category->find('all', array('conditions' => array('Category.parent_id' => $id)));
		$space = "&nbsp;&nbsp;&nbsp;&nbsp;";
		echo "<ul class=child>";
		foreach($getChildList as $getChild)
		{
			$haveChild = (count($getChild['Children']) > 0 ) ? "glyphicon-plus" : "";
			echo '<li class="list-group-item node-treeview-checkable" id="'. $getChild['Category']['id'].'" data-nodeid="0" style="color:undefined;background-color:undefined;"><span class="icon expand-icon glyphicon '.$haveChild. '"></span><input type="checkbox" class="l-tcb" id="ext-gen15">'.$space.$getChild['Category']['category_name'].'</li>';
		}
		echo "<ul>";
		//pr($getChildList);
		exit;
		
	}
	
	/*public function getntchild( $node = null)
	{
		$this->layout = '';
		$this->autoRender = false;
		return "[{id:1,label:'123', load_on_demand: true}]";
		exit;
		
	}*/
	
	public function actionlocunlock( $id = null, $strAction = null )
    {
       
        $this->autorender = false;
        if( $strAction === "active" )
			{            
                $action = 1;
                $msg = "Active Successful";
			}
        else
			{
                $action = 0;
                $msg = "Deactive Successful";
			}   
            
            /* update the product status */
            $this->Product->updateAll( array( "Product.product_status" => $action ), array( "Product.id" => $id ) );

            /* Redirect action after success */
            $this->Session->setflash( $msg, 'flash_success' );                      
            $this->redirect( array( "controller" => "showAllProduct" ) );
    }
    
    public function deleteAction($id = null, $isDeleted =null)
	{
		$action 	=	"1";
		if($isDeleted == 0){
			$isDeleted = 1;
			$msg = "Deletion successful"; }
		else{
			$isDeleted = 0;
			$msg = "Retreival successful"; }
			
			$this->Product->updateAll( array( "Product.is_deleted" => $isDeleted, "Product.product_status" => $action), array( "Product.id" => $id ) );
			$this->Session->setflash( $msg ,  'flash_success');                      
            $this->redirect( array( "controller" => "showAllProduct" ) );
	}
	
	public function editProduct( $id = null )
	{
		  $this->layout = "index";
		  $this->loadModel( 'Store' );
		  
		  if( isset( $id ) & $id > 0 )
            $this->set( 'title','Edit Product' );
		  else
            $this->set( 'title','Add Brand' );
            
         $storeList = $this->Store->find( 'list', array( 
													'conditions' => array( 'Store.status' => 1 ),
													'fields' => array( 'Store.id', 'Store.store_name' ) 
													) 
												);
         $this->set( 'storeList', $storeList );
            
		 $getproductArray = $this->Product->find( 'first' ,array('conditions' => array('Product.id'=> $id))); 
		 $getproductArray['Product']['store_name']	=	explode(',', $getproductArray['Product']['store_name']);
		 $this->request->data = $getproductArray;
		
	}
	
	public function checkcategory()
	{
		$this->layout = 'index';
		$this->loadModel( 'Category' );
		$start = '[';
		$end = ']';
		$getCategories	= $this->cat_all_list();
		
		$getCategories	=	$this->Category->find('all', array('conditions' => array('Category.parent_id' => 0)));
		
		$this->set('getCategories', $start.$getCategories.$end);
	}
	
   public function cat_all_list($p_cid=0 , $innerCounter = 0 ,  $st = 0)
	{
		$this->loadModel('Category');
		$this->recursive = -1;
		$categories	=	$this->Category->find('all', array('conditions' => array('Category.parent_id' => $p_cid)));
		$count=count($categories);		
		if($count > 0)
		{
			$st = 0;
			foreach($categories as $categorie)
			{		
					$haveChild = (count($categorie['Children']) > 0 ) ? ",children :[{" : "";
					$endChild = (count($categorie['Children']) > 0 ) ? "" : "}]";			
					
					if( $p_cid > 0 )
					{	
						
						$innerCounter++;
						$haveChild = (count($categorie['Children']) > 0 ) ? ",children :[{" : "";
						$endChild = (count($categorie['Children']) > 0 ) ? "" : "}]";
						echo ",children :[{".'label : '."'".$categorie['Category']['category_name']."'".", id:".$categorie['Category']['id'].$endChild;					
						$st = 1;
						if( $innerCounter > 0  && $haveChild == '')
						{	
							$strCloseTags = '';
							$ik = 0;while( $ik < $innerCounter-1 )
							{
								$strCloseTags .= '}]';
								$ik++;	
							}							
							echo $strCloseTags;				
						}
						else
						{
							echo $strCloseTags = '';
						}
					}
					else
					{	
						if( $st == 0 )
							echo '{label : '."'".$categorie['Category']['category_name']."'".", id:".$categorie['Category']['id'];						
						else
							echo ',{label : '."'".$categorie['Category']['category_name']."'".", id:".$categorie['Category']['id'];							
						$innerCounter = 0;
						$st = 0;
					}
			
					$this->cat_all_list($categorie['Category']['id'] , $innerCounter , $st);			
			}			
		}
	}
	
	public function UploadProducts()
	{
		$this->layout = 'index';
		
		
			$this->loadModel('AttributeSet');
			$this->loadModel('Attribute');
			$this->loadModel('Warehouse');
		
			$this->loadModel('AttributeSet');
			$attributesets	=	$this->AttributeSet->find('list', array('fields' => array('id', 'set_name')));
			$this->set( 'attributesets', $attributesets );
	}
	
	public function DownloadSample()
	{
		$this->autoLayout = 'ajax';
		$this->autoRender = false;
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');
		
		if($this->request->data['Product']['Attribute_set_id'] != '')
		{
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel->getActiveSheet()->setTitle('Stock Master');
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(0);
		
		
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'System SKU');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Title');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Brand');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Short Description');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Long Description');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Contents/Information');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Ingredients');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Usage Information');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Notes');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Technical Specifications');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', 'UPC/Barcode');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Image filename');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Image filename2');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Image filename3');
		$objPHPExcel->getActiveSheet()->setCellValue('O1', 'Image filename4');
		$objPHPExcel->getActiveSheet()->setCellValue('P1', 'Vegan');
		$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Vegetarian');
		$objPHPExcel->getActiveSheet()->setCellValue('R1', 'Gluten Free');
		$objPHPExcel->getActiveSheet()->setCellValue('S1', 'Corn Free');
		$objPHPExcel->getActiveSheet()->setCellValue('T1', 'Wheat Free');
		$objPHPExcel->getActiveSheet()->setCellValue('U1', 'Dairy Free');
		$objPHPExcel->getActiveSheet()->setCellValue('V1', 'Soya Free');
		$objPHPExcel->getActiveSheet()->setCellValue('W1', 'Non-GM');
		$objPHPExcel->getActiveSheet()->setCellValue('X1', 'Weight');
		$objPHPExcel->getActiveSheet()->setCellValue('Y1', 'Category');
		$objPHPExcel->getActiveSheet()->setCellValue('Z1', 'Price');
		$objPHPExcel->getActiveSheet()->setCellValue('AA1', 'Length');
		$objPHPExcel->getActiveSheet()->setCellValue('AB1', 'Width');
		$objPHPExcel->getActiveSheet()->setCellValue('AC1', 'Height');
		$objPHPExcel->getActiveSheet()->setCellValue('AD1', 'Status');
		
		$this->loadModel('AttributeSet');
		$this->loadModel('Attribute');
		
		$attvalue	=	$this->AttributeSet->find('first', array('conditions' => array('AttributeSet.id' => $this->request->data['Product']['Attribute_set_id'])));
			
		$ids = $attvalue['AttributeSet']['attribute_set_values'];
		$ids	=	explode(',',$ids);
		$attributevalues	=	$this->Attribute->find('all', array('conditions' => array('Attribute.id' => $ids)));
		
		$row = 1;
		$num1 = ord('A');
		$num2 = ord('D');
		$num3 = ord('1');
		$i = 1;
		foreach($attributevalues as $attributevalue)
		{
			$num4	=	chr($num2+$i); 
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$num4.'1', $attributevalue['Attribute']['attribute_code']);
			$i++;
		}
		

		$name = 'Stock Master';
		header('Content-Encoding: UTF-8');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment;filename="'.$name.'.csv"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save('php://output');
	
		}
		else
		{
			 $this->Session->setflash( 'Please select attribute set', 'flash_danger' ); 
			 $this->redirect($this->referer());
			
		}
		
	}
	
	/*public function uploads_csv()
	{
		$this->autoLayout = 'ajax';
		$this->autoRender = false;
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('ProductPrice');
		$this->loadModel('ProductImage');
		
		$this->data['Product']['Import_file']['name'];
		if($this->data['Product']['Import_file']['name'] != '')
			{
				
				$filename = WWW_ROOT. 'files'.DS.$this->request->data['Product']['Import_file']['name']; 
				move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
				$name		=	 $this->request->data['Product']['Import_file']['name'];
		
				App::import('Vendor', 'PHPExcel/IOFactory');
			
				$objPHPExcel = new PHPExcel();
				$objReader= PHPExcel_IOFactory::createReader('CSV');
				$objReader->setReadDataOnly(true);
				$objPHPExcel=$objReader->load('files/'.$name);
				$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
				$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
				$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
				$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
				
				
				$count=0;
				$countalready=0;
				$header	=	array('Product Title','Brand','Short Description','Long Description','Contents/Information','Ingredients','Usage Information', 'Notes', 'Technical Specifications',
				 'UPC/Barcode', 'Image filename', 'Image filename2', 'Image filename3', 'Image filename4', 'Vegan', 'Vegetarian', 'Gluten Free', 'Corn Free', 'Wheat Free', 'Dairy Free', 
				 'Soya Free', 'Non-GM', 'Weight', 'Category', 'Price', 'Length', 'Width', 'Height', 'Status');
			$count	= 0;
			$alreadycount	=	0;
			$countbarcode	=	0;		 
			for($i=2;$i<=$lastRow;$i++) 
			{
				
				$productdata['product_sku']					=	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$productdata['product_name']				=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$productdscdata['brand']					=	$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
				$productdscdata['short_description']		=	addslashes($objWorksheet->getCellByColumnAndRow(3,$i)->getValue());
				$productdscdata['long_description']			=	addslashes($objWorksheet->getCellByColumnAndRow(4,$i)->getValue());
				$productdscdata['barcode'] 					=	$objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
				$productimgdata['Image_filename'] 			=	$objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
				$productimgdata['Image_filename2'] 			=	$objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
				$productimgdata['Image_filename3'] 			=	$objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
				$productimgdata['Image_filename4'] 			=	$objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
				$productdscdata['weight']					=	$objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
				//$data['category_id']						=	$objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
				$productprice['product_price']				=	$objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
				$productdscdata['length']					=	$objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
				$productdscdata['width'] 					=	$objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
				$productdscdata['height'] 					=	$objWorksheet->getCellByColumnAndRow(15,$i)->getValue();
				$productdata['product_status']				=	$objWorksheet->getCellByColumnAndRow(16,$i)->getValue();
				
				$checkproduct		=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $productdata['product_sku'])));
				$checkproductdesc	=	$this->ProductDesc->find('first', array('conditions' => array('ProductDesc.barcode' => $productdscdata['barcode'])));
				
				if(count($checkproduct) == 0)
				{
					if(count($checkproductdesc) == 0)
					{
						$this->Product->saveAll($productdata);
						$productid	=	$this->Product->getLastInsertId();
						
						$productdscdata['product_id'] = $productid;
						$this->ProductDesc->saveAll($productdscdata, array('validate' => false));
						
						$productprice['product_id'] = $productid;
						$this->ProductPrice->saveAll($productprice, array('validate' => false));
						
						$productimgdata['product_id'] = $productid;
						$this->ProductImage->saveAll($productimgdata, array('validate' => false));
						
						$count++;
					}
					else
					{
						$countbarcode++;
					}
				}
				else
				{
					$alreadycount++;
				}
				$this->Session->setFlash($count.' :- SKU Inserted <br>'.$countbarcode.' :- Barcode Already Exist <br>'. $alreadycount.' :- SKU Already Exist <br>', 'flash_danger');
			}
			$this->redirect($this->referer());
		}
		else
		{
			$this->Session->setFlash('Please Insert CSV File.', 'flash_danger');
			$this->redirect($this->referer());
		}
	}*/
	
	public function uploads_csv()
	{
		$this->autoLayout = 'ajax';
		$this->autoRender = false;
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('ProductPrice');
		$this->loadModel('ProductImage');
		$this->loadModel('AttributeOption');
		//pr($this->request->data);
		//exit;
		$this->data['Product']['Import_file']['name'];
		if($this->data['Product']['Import_file']['name'] != '')
			{
				
				$filename = WWW_ROOT. 'files'.DS.$this->request->data['Product']['Import_file']['name']; 
				move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
				$name		=	 $this->request->data['Product']['Import_file']['name'];
				
				App::import('Vendor', 'PHPExcel/IOFactory');
				
				$objPHPExcel = new PHPExcel();
				$objReader= PHPExcel_IOFactory::createReader('CSV');
				$objReader->setReadDataOnly(true);				
				$objPHPExcel=$objReader->load('files/'.$name);
				$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
				$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
				$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
				$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
				
				$count=0;
				$countalready=0;
				$header	=	array('Product Title','Brand','Short Description','Long Description','Contents/Information','Ingredients','Usage Information', 'Notes', 'Technical Specifications',
				 'UPC/Barcode', 'Image filename', 'Image filename2', 'Image filename3', 'Image filename4', 'Vegan', 'Vegetarian', 'Gluten Free', 'Corn Free', 'Wheat Free', 'Dairy Free', 
				 'Soya Free', 'Non-GM', 'Weight', 'Category', 'Price', 'Length', 'Width', 'Height', 'Status');
				 
				$count	= 0;
				$alreadycount	=	0;
				$countbarcode	=	0;
						 
			for($i=2;$i<=$lastRow;$i++) 
			{
				
				$productdata['product_sku']				=	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$productdata['asin_no']					=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$productdata['product_name']			=	$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
				$productdscdata['brand']				=	$objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
				$productdscdata['short_description']	=	addslashes($objWorksheet->getCellByColumnAndRow(4,$i)->getValue());
				$productdscdata['long_description']		=	addslashes($objWorksheet->getCellByColumnAndRow(5,$i)->getValue());
				$productdscdata['barcode'] 				=	$objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
				$productimgdata['image']['0'] 			=	$objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
				$productimgdata['image']['1'] 			=	$objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
				$productimgdata['image']['2'] 			=	$objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
				$productimgdata['image']['3'] 			=	$objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
				$productimgdata['image']['4']			=	$objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
				$productdscdata['weight']				=	$objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
				$productdscdata['category']				=	$objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
				$productdscdata['price']				=	$objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
				$productdscdata['length'] 				=	$objWorksheet->getCellByColumnAndRow(15,$i)->getValue();
				$productdscdata['width'] 				=	$objWorksheet->getCellByColumnAndRow(16,$i)->getValue();
				$productdscdata['height']				=	$objWorksheet->getCellByColumnAndRow(17,$i)->getValue();
				$productdata['product_status']			=	$objWorksheet->getCellByColumnAndRow(18,$i)->getValue();
				$productdscdataAttr['attribute']['1']	=	$objWorksheet->getCellByColumnAndRow(19,$i)->getValue();
				$productdscdataAttr['attribute']['2']	=	$objWorksheet->getCellByColumnAndRow(20,$i)->getValue();
				$productdscdataAttr['attribute']['3']	=	$objWorksheet->getCellByColumnAndRow(21,$i)->getValue();
				$productdscdataAttr['attribute']['4']	=	$objWorksheet->getCellByColumnAndRow(22,$i)->getValue();
				$productdscdataAttr['attribute']['5']	=	$objWorksheet->getCellByColumnAndRow(23,$i)->getValue();
				$productdscdataAttr['attribute']['6']	=	$objWorksheet->getCellByColumnAndRow(24,$i)->getValue();
				//echo "*******************<br>";
				$productdscdata['model_no']				=	$objWorksheet->getCellByColumnAndRow(25,$i)->getValue();
				$productdscdata['manufacturer_part_num']=	$objWorksheet->getCellByColumnAndRow(26,$i)->getValue();
				$productdscdata['yield']				=	$objWorksheet->getCellByColumnAndRow(27,$i)->getValue();
				$productdscdata['type']					=	$objWorksheet->getCellByColumnAndRow(28,$i)->getValue();
				$productdscdata['pack_qty']				=	$objWorksheet->getCellByColumnAndRow(29,$i)->getValue();
				
				/*
				 * 
				 * Params, Inserting Product type and product defined skus list with colon based
				 * 
				 */ 
				$productdscdata['product_type']				=	$objWorksheet->getCellByColumnAndRow(31,$i)->getValue();
				$productdscdata['product_defined_skus']		=	$objWorksheet->getCellByColumnAndRow(32,$i)->getValue();
								
				//echo "******************************************************************************************************<br>";
								
				$arrayMarge = array();
				foreach($productdscdataAttr['attribute'] as $attr)
				{
					if( $attr != '')
					{
						$attributeOptions	=	$this->AttributeOption->find('first', array('conditions' => array('AttributeOption.attribute_option_name' => $attr)));
						$attributeId	=	(isset($attributeOptions['AttributeOption']['id']) ? $attributeOptions['AttributeOption']['id'] : '');
						array_push($arrayMarge, $attributeId);
					}
				}
				$productdscdata['attribute_id']	=	$attrbuteid	=	implode(',', $arrayMarge);
				
				$checkproduct		=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $productdata['product_sku'])));
				
				// Check also brand is exists or not
				$this->loadModel( 'Brand' );
				$getBrand = $this->Brand->find( 'first' , array( 'conditions' => array( 'Brand.brand_name' => $productdscdata['brand'] ) ) );
				
				if( count( $getBrand ) == 0 )
				{
					$brandName = $productdscdata['brand'];
					$brandAlias = strtolower($brandName);					
					$this->Brand->query( "insert into brands ( brand_name, brand_alias ) values ( '{$brandName}' , '{$brandAlias}' )" );					
					$productdscdata['brand'] = $brandName;
				}
				else
				{
					$productdscdata['brand'] = $productdscdata['brand'];
				}
				
				//$checkproductdesc	=	$this->ProductDesc->find('first', array('conditions' => array('ProductDesc.barcode' => $productdscdata['barcode'])));
				if(count($checkproduct) == 0)
				{
						$this->Product->saveAll($productdata);
						$productid	=	$this->Product->getLastInsertId();
						
						$productdscdata['product_id'] = $productid;
						$this->ProductDesc->saveAll($productdscdata, array('validate' => false));
						
						$productprice['product_id'] = $productid;
						$this->ProductPrice->saveAll($productprice, array('validate' => false));
						foreach( $productimgdata['image'] as $image)
						{
								$productimgdata['image_name'] = (isset($image) && $image != '') ? $image : '';
								$productimgdata['product_id'] = $productid;
								$productimgdata['selected_image'] = '0';
								$this->ProductImage->saveAll($productimgdata, array('validate' => false));
						}
						$count++;
				}
				else
				{
					$alreadycount++;
				}
				$this->Session->setFlash($count.' :- SKU Inserted <br>'.$countbarcode.' :- Barcode Already Exist <br>'. $alreadycount.' :- SKU Already Exist <br>', 'flash_danger');
			}
			$this->redirect($this->referer());
		}
		else
		{
			$this->Session->setFlash('Please Insert CSV File.', 'flash_danger');
			$this->redirect($this->referer());
		}
	}
	
	/*
	 * 
	 * Params, Function for product details
	 * 
	 */ 
	public function ProductDetail()
	{
		$this->layout = 'index';
		$this->loadModel( 'Brand' );
		
		$this->loadModel( 'PackageEnvelope' );
		$this->loadModel( 'Packagetype' );
		$packageType = $this->Packagetype->find('list', array('fields' => 'Packagetype.id, Packagetype.package_type_name'));
		
		$brandName	=	$this->Brand->find('list', array('fields' => 'brand_name, brand_name'));
		$this->set( 'brandName', $brandName);
		
		//Rack Section
		$this->loadModel( 'Rack' );
		
		//Ground
		$floorRacks = $this->Rack->find('list' , array( 'fields' => array( 'Rack.floor_name' ) , 'order' => array( 'Rack.id ASC' ) , 'group' => array( 'Rack.floor_name' ) ) );
		
		//Racks
		$rack = $rackRacks = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , array( 'fields' => array( 'Rack.id' , 'Rack.rack_floorName' ) , 'conditions' => array( 'Rack.floor_name' => $floorRacks ) , 'group' => array( 'Rack.rack_floorName' ) , 'order' => array( 'Rack.id ASC' ) ) );		
		
		//Level
		$rackLevel = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , 
				array( 
					'fields' => array( 'Rack.id' , 'Rack.level_association' ) , 
					'conditions' => array( 'Rack.floor_name' => $floorRacks , 'Rack.rack_floorName' => $rack ) , 'group' => array( 'Rack.level_association' ) , 'order' => array( 'Rack.id ASC' ) 
					) 
				);
		
		//Section
		$rackSection = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , 
				array( 
					'fields' => array( 'Rack.id' , 'Rack.rack_level_section' ) , 
					'conditions' => array( 'Rack.floor_name' => $floorRacks , 'Rack.rack_floorName' => $rack , 'Rack.level_association' => $rackLevel ) , 'order' => array( 'Rack.id ASC' ) 
					) 
				);
				
		$this->set( 'setNewGroundArray' , $floorRacks );
		$this->set( 'rack' , $rack );
		$this->set( 'rackLevel' , $rackLevel );
		$this->set( 'rackSection' , $rackSection );
		$this->set( 'packageType', $packageType);
	}
	
	public function getBarcodeSearchCheckIn()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		$this->loadModel('BinLocation');
		$this->loadModel('CheckIn');
		$this->loadModel('Po');
		
		$barcode	=	$this->request->data['barcode'];
		$userId	=	$this->request->data['userId'];
		//$barcode	=	trim($this->request->data['barcode']);
		/*Apply code for Global Barcode*/
		$barcode		=	$this->GlobalBarcode->getGlobalBarcode($barcode);
		
		$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array('ProductDesc.barcode' => $barcode)
								)
							);
							
		/*$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array(
										'OR' => array(
												array('ProductDesc.barcode' => $barcode),
												array('Product.product_sku LIKE' => '%'.$barcode.'%'),
												array('Product.product_name LIKE' => '%'.$barcode.'%')
										)
									)
								)
							);*/
		$getBarcodeFromProduct	=	$productdeatil['ProductDesc']['barcode'];
		
		$this->ProductDesc->unbindModel( array(
			'hasOne' => array(
				'Product'
			)
		) );
		
		$binLocation =	$this->BinLocation->find('all', array(
									'joins' => array(
										array(
											'table' => 'product_descs',
											'alias' => 'ProductDesc',
											'type' => 'INNER',
											'conditions' => array(
												'ProductDesc.barcode = BinLocation.barcode',
												'BinLocation.barcode ='.$barcode
											)
										)
									)
								)
							);
		
		$data['id'] 				= 	$productdeatil['ProductDesc']['id'];
		$data['stock'] 				= 	$productdeatil['Product']['current_stock_level'];
		$data['product_id']			= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 				= 	$productdeatil['Product']['product_name'];
		$data['sku'] 				= 	$productdeatil['Product']['product_sku'];
		$data['comment'] 			= 	$productdeatil['Product']['comment'];
		$data['risk_comment'] 		= 	$productdeatil['Product']['risk_comment'];
		$data['shortdescription'] 	= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 	= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 				= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 			= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 				= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 			= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 			= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 			= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		$data['bin_combined_text'] 			= 	$productdeatil['ProductDesc']['bin_combined_text'];
		
		//Get and set Bin Locations
		$binLocate = 0;
		foreach($binLocation as $binLocationIndex => $binLocationValue)
		{
			$binData[$binLocate]['id']					=	$binLocationValue['BinLocation']['id'];	
			$binData[$binLocate]['barcode']				=	$binLocationValue['BinLocation']['barcode'];	
			$binData[$binLocate]['bin_location']		=	$binLocationValue['BinLocation']['bin_location'];	
			$binData[$binLocate]['stock_by_location']	=	$binLocationValue['BinLocation']['stock_by_location'];	
		$binLocate++;
		}
		$data['binLocationArray'] = $binData;
		
		$imageUrl	=	Router::url('/', true).'img/product/';
		
		$i = 0;
		foreach($productdeatil['ProductImage'] as $image)
		{
			if($image['image_name'] != '')
			{
				$imagedata[$i]['imageid']		=	$image['id'];
				$imagedata[$i]['imagename']		=	$imageUrl.$image['image_name'];
				$imagedata[$i]['selectedimage']	=	$image['selected_image'];
			}
			$i++;
		}
		
		$j = 0;
		foreach( $productdeatil['ProductLocation'] as $productLocation)
		{
			$productLocation['warehouse_id'];
			$locationData[$j]['binrackid']	=	$productLocation['bin_rack_address'];
			$warehouseDetail	=	$this->Warehouse->find('first', array('conditions' => array('Warehouse.id' => $productLocation['warehouse_id'])));
			$locationData[$j]['warehousename']	=	$warehouseDetail['Warehouse']['warehouse_name'];
			$locationData[$j]['city']			=	$warehouseDetail['WarehouseDesc']['city_id'];
			$locationData[$j]['warehousetype']	=	$warehouseDetail['WarehouseDesc']['warehouse_type'];
			$j++;
		}
		//echo (json_encode(array('status' => '1', 'data' => $data, 'image' => $imagedata, 'location' => $locationData)));
		//$userLists	=	$this->CheckIn->find( 'all', array( 'conditions' => array('CheckIn.barcode' => $barcode,  'CheckIn.user_id'=> $userId) ) );
		//$userLists	=	$this->CheckIn->find( 'all', array( 'conditions' => array('CheckIn.barcode' => $barcode) ) );
		/*Apply code for Global Barcode*/
		//$userLists	=	$this->CheckIn->find( 'all', array( 'conditions' => array('CheckIn.barcode IN ' => array($barcode,$this->request->data['barcode'])) ) );
		
		$userLists	=	$this->CheckIn->find( 'all', array( 'conditions' => array('CheckIn.barcode IN ' => array($barcode,$this->request->data['barcode']), 'CheckIn.custam_page_name' => 'Check In') ) );
		
		foreach( $userLists as $userListsValue )
		{
			$userdetail = $this->getUserDeatil( $userListsValue['CheckIn']['user_id'] );
			$useData['sku'] 		= 	$userListsValue['CheckIn']['sku'];
			$useData['user_detail'] = 	$userdetail;
			$useData['pc_name'] 	= 	$userListsValue['CheckIn']['pc_name'];
			$useData['qty'] 		= 	$userListsValue['CheckIn']['qty_checkIn'];
			$useData['date'] 		= 	date("jS F, Y", strtotime($userListsValue['CheckIn']['date']));
			$getData[] = $useData; 
		}
		
		$params	=	array(
					'joins' => array(
							array(
								'table' => 'purchase_orders',
								'alias' => 'PurchaseOrder',
								'type' => 'INNER',
								'conditions' => array(
									'Po.po_name = PurchaseOrder.po_name'
								)
							)),
					'fields' => array( 'Po.id','Po.po_name' ),
					'conditions'=> array( 'Po.status' => 1, 'PurchaseOrder.purchase_barcode IN' => array($barcode,$this->request->data['barcode']) ),
					
				);
				
		$getpoList	=	$this->Po->find( 'list', $params);
		/*if(count($getpoList) < 1)
		{
			$params	=	array(
					'joins' => array(
							array(
								'table' => 'purchase_orders',
								'alias' => 'PurchaseOrder',
								'type' => 'INNER',
								'conditions' => array(
									'Po.po_name = PurchaseOrder.po_name'
								)
							)),
					'fields' => array( 'Po.id','Po.po_name' ),
					'conditions'=> array( 'Po.status' => 1, 'PurchaseOrder.purchase_barcode' => $this->request->data['barcode'] ),
					
				);
				
		$getpoList	=	$this->Po->find( 'list', $params);
		}*/
		
		echo (json_encode(array('status' => '1', 'data' => $data, 'userData' => $getData, 'getpoList' => $getpoList)));
		exit;	
	}
	
	public function getUserDeatil( $userId = null )
	{
		$this->loadModel( 'User' );
		$uerDeatil =	$this->User->find( 'first', array( 'conditions' => array( 'User.id' => $userId ) ) );
		$userFullName	=	$uerDeatil['User']['first_name'].' '.$uerDeatil['User']['last_name'];
		return $userFullName;
	}
	
	public function getBarcodeSearch()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		$this->loadModel('BinLocation');
		
		$barcode	=	$this->request->data['barcode'];
		//$barcode	=	trim($this->request->data['barcode']);
		/*************************************Apply code for Global Barcode*********************************************/
	 	$barcode	=	$this->GlobalBarcode->getGlobalBarcode($barcode);
	    /*************************************Apply code for Global Barcode*********************************************/
		
		$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array('ProductDesc.barcode' => $barcode)
								)
							);
							
		/*$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array(
										'OR' => array(
												array('ProductDesc.barcode' => $barcode),
												array('Product.product_sku LIKE' => '%'.$barcode.'%'),
												array('Product.product_name LIKE' => '%'.$barcode.'%')
										)
									)
								)
							);*/
		
		$getBarcodeFromProduct	=	$barcode;//$productdeatil['ProductDesc']['barcode'];
		
		$this->ProductDesc->unbindModel( array(
			'hasOne' => array(
				'Product'
			)
		) );
		
		/*$binLocation =	$this->BinLocation->find('all', array(
									'joins' => array(
										array(
											'table' => 'product_descs',
											'alias' => 'ProductDesc',
											'type' => 'INNER',
											'conditions' => array(
												'ProductDesc.barcode = BinLocation.barcode',
												'BinLocation.barcode ='.$barcode
											)
										)
									)
								)
							);*/
		
               
                $binLocation =	$this->BinLocation->find('all', array( 'conditions' => array(
                                                                        'BinLocation.barcode' => $barcode
                                                                    )
                                                                    
								)
							);
                
		$data['id'] 					= 	$productdeatil['ProductDesc']['id'];
		$data['stock'] 					= 	$productdeatil['Product']['current_stock_level'];
		$data['product_id']				= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 					= 	$productdeatil['Product']['product_name'];
		$data['sku'] 					= 	$productdeatil['Product']['product_sku'];
		$data['shortdescription'] 		= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 		= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 					= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 				= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 					= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 				= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 				= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 				= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 	= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 	= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		$data['bin_combined_text'] 		= 	$productdeatil['ProductDesc']['bin_combined_text'];
		
		//Get and set Bin Locations
		$binLocate = 0;
                
		foreach($binLocation as $binLocationIndex => $binLocationValue)
		{
			$binData[$binLocate]['id']					=	$binLocationValue['BinLocation']['id'];	
			$binData[$binLocate]['barcode']				=	$binLocationValue['BinLocation']['barcode'];	
			$binData[$binLocate]['bin_location']		=	$binLocationValue['BinLocation']['bin_location'];	
			$binData[$binLocate]['stock_by_location']	=	$binLocationValue['BinLocation']['stock_by_location'];	
		$binLocate++;
		}
		$data['binLocationArray'] = $binData;
		
		$imageUrl	=	Router::url('/', true).'img/product/';
		
		$i = 0;
		foreach($productdeatil['ProductImage'] as $image)
		{
			if($image['image_name'] != '')
			{
				$imagedata[$i]['imageid']		=	$image['id'];
				$imagedata[$i]['imagename']		=	$imageUrl.$image['image_name'];
				$imagedata[$i]['selectedimage']	=	$image['selected_image'];
			}
			$i++;
		}
		
		$j = 0;
		foreach( $productdeatil['ProductLocation'] as $productLocation)
		{
			$productLocation['warehouse_id'];
			$locationData[$j]['binrackid']	=	$productLocation['bin_rack_address'];
			$warehouseDetail	=	$this->Warehouse->find('first', array('conditions' => array('Warehouse.id' => $productLocation['warehouse_id'])));
			$locationData[$j]['warehousename']	=	$warehouseDetail['Warehouse']['warehouse_name'];
			$locationData[$j]['city']			=	$warehouseDetail['WarehouseDesc']['city_id'];
			$locationData[$j]['warehousetype']	=	$warehouseDetail['WarehouseDesc']['warehouse_type'];
			$j++;
		}
                
		//echo (json_encode(array('status' => '1', 'data' => $data, 'image' => $imagedata, 'location' => $locationData)));
		echo (json_encode(array('status' => '1', 'data' => $data)));
		exit;
	}
	
	public function removeLocation()
	{
		
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel( 'BinLocation' );	
		
		$barcode = $this->request->data['barcode'];
		
		//Save only
		$getLocation = explode( ',',$this->request->data['getLocation'] );
		$getStock = explode( ',',$this->request->data['getStockByLocation'] );
		
		$getBinLocation = $this->BinLocation->find( 'first', array( 'conditions' => array( 'BinLocation.barcode' => $barcode , 'BinLocation.bin_location' => $getLocation , 'BinLocation.stock_by_location' => $getStock ) , 'fields' => array( 'BinLocation.id' ) ) );
		$subStock = $this->request->data['getStockByLocation'];
		if( count($getBinLocation) > 0 )
		{			
			if( $subStock >= 0 )
			{	
				$product = $this->Product->find( 'first', array( 'conditions' => array( 'ProductDesc.barcode' => $barcode ) , 'fields' => array( 'Product.id','Product.current_stock_level' ) ) ); 
			
				(int)$subtractStock = (int)$product['Product']['current_stock_level'] - (int)$subStock;
				//$data['Product']['Product']['id'] = $product['Product']['id'];
				//$data['Product']['Product']['current_stock_level'] = $subtractStock;				
				//$this->Product->saveAll( $data['Product'] );
				
				$id = $product['Product']['id'];
				
				$this->Product->query( "UPDATE products set current_stock_level = {$subtractStock} where id = {$id}" );
				
				$binId = $getBinLocation['BinLocation']['id'];
				$this->BinLocation->query( "delete from bin_locations where id = {$binId}" );
								
				echo "1"; exit;	
			}		
		}
	}
	
	public function updatePackagingIdByBarcodeSearch()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel( 'BinLocation' );	
		$this->loadModel( 'CheckIn' );	
		
		/*pr($this->request->data);
		pr($this->BinLocation->find('all')); exit;*/
		
		$barcode = $this->request->data['barcode'];
		/*Apply code for Global Barcode*/
		$barcode	=	$this->GlobalBarcode->getGlobalBarcode($barcode);
		//Delete Specific barcode rows
		$this->BinLocation->query( "delete from bin_locations where barcode = '{$barcode}'" );
		
              
		$product_id = $this->request->data['product_id'];
		$descId = $this->request->data['id'];
		$length = $this->request->data['getLength'];
		$width = $this->request->data['getWidth'];
		$height = $this->request->data['getHeight'];
		$weight = $this->request->data['getWeight'];
		$variant_envelope_name = $this->request->data['variant_envelope_name'];
		$variant_envelope_id = $this->request->data['variant_envelope_id'];
		$brand = $this->request->data['brand'];
		$bin = $this->request->data['bin_sku'];
		$binAllocation = $this->request->data['binAllocation'];
		$pageName = $this->request->data['pageName'];
		$currentStock = $this->request->data['currentStock'];
		$userId = $this->request->data['userId'];
		$pcName = $this->request->data['pcName'];
		
		$getStockSum = array_sum( explode(",",$this->request->data['getStockByLocation']) );
		
		//Unbinding Techniques 
		$this->ProductDesc->unbindModel( array( 'belongsTo' => array( 'Product' ) ) );
				
		$this->ProductDesc->updateAll(
			array(
				'length' => $length,
				'width' => $width,
				'height' => $height,
				'weight' => $weight,
				'variant_envelope_name' => "'".$variant_envelope_name."'",
				'variant_envelope_id' => $variant_envelope_id,
				'brand' => "'".$brand."'",
				'bin' => "'".$bin."'"
			),
			array(
				'ProductDesc.id' => $descId
			)
		);
		
		//Save only
		$getLocation = explode( ',',$this->request->data['getLocation'] );
		$getStock = explode( ',',$this->request->data['getStockByLocation'] );
		
		if( count($getLocation) > 0 )
		{
                    
			$stockCount = 0;$e = 0;while( $e < count( $getLocation ) )
			{
				if(  $getLocation[$e] != '' && $getStock[$e] != '' )
				{
                                    
					$this->request->data['BinLocationPreffered']['BinLocation']['barcode'] = $barcode;//$this->request->data['barcode'];
					$this->request->data['BinLocationPreffered']['BinLocation']['bin_location'] = $getLocation[$e];
					$this->request->data['BinLocationPreffered']['BinLocation']['stock_by_location'] = $getStock[$e];
					$stockCount = $stockCount + $getStock[$e];
                                        
					$this->BinLocation->saveAll( $this->request->data['BinLocationPreffered'] );
				}
			$e++;	
			}
		}
		
	   /*$this->Product->updateAll(
				array(
						'current_stock_level' => "'".$stockCount."'"
				),
				array(
						'Product.id' => $product_id
				)
		);*/
		
		$this->Product->query( "UPDATE products set current_stock_level = '{$stockCount}' where id = {$product_id}" );
                
		if( $stockCount > 0 ) 
		{
			//Update Product current stock
			/*$this->Product->updateAll(
				array(
					'current_stock_level' => "'".$stockCount."'"
				),
				array(
					'Product.id' => $product_id
				)
			);*/
                    
                    //$this->Product->query( "UPDATE products set current_stock_level = '{$stockCount}' where id = {$product_id}" );
		}
		
		/*
		 * 
		 * CheckIn record manipulation
		 * 
		 */ 
		$checkIn['sku']				= $bin;
		$checkIn['barcode']			= $barcode;
		$checkIn['qty_checkIn']		= $getStockSum;
		$checkIn['custam_page_name']= $pageName;
		$checkIn['current_stock']	= $currentStock;
		$checkIn['user_id']			= $userId;
		$checkIn['pc_name']			= $pcName;
		
		$this->CheckIn->saveAll( $checkIn );
		unset($checkIn);
		$checkIn = '';
		
		//Get All related data
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		$this->loadModel('BinLocation');
		
		/*$this->ProductDesc->bindModel( array( 
			'hasMany' => array(        
				'BinLocation' => array(									   
					'className' => 'BinLocation',
					'foreignKey' => 'barcode'									   
					)
				) 
			) 
		);*/
		
		$barcode	=	$this->request->data['barcode'];
		
		$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array('ProductDesc.barcode' => $barcode)
								)
							);
		
		$this->ProductDesc->unbindModel( array(
			'hasOne' => array(
				'Product'
			)
		) );
		
		/*$binLocation =	$this->BinLocation->find('all', array(
									'joins' => array(
										array(
											'table' => 'product_descs',
											'alias' => 'ProductDesc',
											'type' => 'INNER',
											'conditions' => array(
												'ProductDesc.barcode = BinLocation.barcode'
											)
										)
									)
								)
							);*/
		$binLocation =	$this->BinLocation->find('all', array( 'conditions' => array(
                                                                        'BinLocation.barcode' => $barcode
                                                                    )
                                                                    
								)
							);
                
		$data['id'] 					= 	$productdeatil['ProductDesc']['id'];
		$data['stock'] 					= 	$productdeatil['Product']['current_stock_level'];
		$data['product_id'] 			= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 					= 	$productdeatil['Product']['product_name'];
		$data['sku'] 					= 	$productdeatil['Product']['product_sku'];
		$data['shortdescription'] 		= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 		= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 					= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 				= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 					= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 				= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 				= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 				= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 	= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 	= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		$data['bin_combined_text'] 		= 	$productdeatil['ProductDesc']['bin_combined_text'];
		
		//Get and set Bin Locations
		$binLocate = 0;
		foreach($binLocation as $binLocationIndex => $binLocationValue)
		{
			$binData[$binLocate]['id']					=	$binLocationValue['BinLocation']['id'];	
			$binData[$binLocate]['barcode']				=	$barcode;//$binLocationValue['BinLocation']['barcode'];	
			$binData[$binLocate]['bin_location']		=	$binLocationValue['BinLocation']['bin_location'];	
			$binData[$binLocate]['stock_by_location']	=	$binLocationValue['BinLocation']['stock_by_location'];	
		$binLocate++;
		}
		$data['binLocationArray'] = $binData;
		
		$imageUrl	=	Router::url('/', true).'img/product/';
		
		$i = 0;
		foreach($productdeatil['ProductImage'] as $image)
		{
			if($image['image_name'] != '')
			{
				$imagedata[$i]['imageid']		=	$image['id'];
				$imagedata[$i]['imagename']		=	$imageUrl.$image['image_name'];
				$imagedata[$i]['selectedimage']	=	$image['selected_image'];
			}
			$i++;
		}
		
		$j = 0;
		foreach( $productdeatil['ProductLocation'] as $productLocation)
		{
			$productLocation['warehouse_id'];
			$locationData[$j]['binrackid']	=	$productLocation['bin_rack_address'];
			$warehouseDetail	=	$this->Warehouse->find('first', array('conditions' => array('Warehouse.id' => $productLocation['warehouse_id'])));
			$locationData[$j]['warehousename']	=	$warehouseDetail['Warehouse']['warehouse_name'];
			$locationData[$j]['city']			=	$warehouseDetail['WarehouseDesc']['city_id'];
			$locationData[$j]['warehousetype']	=	$warehouseDetail['WarehouseDesc']['warehouse_type'];
			$j++;
		}
		$data['status'] 				= 'success';	
                
		echo (json_encode(array('status' => '1', 'data' => $data)));		
		exit;	 
	}
		
	public function getRackAccordingToFloor()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		//Load Model
		$this->loadModel( 'Rack' );
		
		//Get Floor Id
		$rackId = $this->request->data['floorId'];
		$rackName = explode(',',$this->request->data['selectedText']);
		
		$rack = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , array( 'fields' => array( 'Rack.id' , 'Rack.rack_floorName' ) , 'conditions' => array( 'Rack.floor_name' => $rackName ) , 'group' => array( 'Rack.rack_floorName' ) , 'order' => array( 'Rack.id ASC' ) ) );		
		
		$str = '<select id="rackNumber" class="form-control" name="data[rackNumber]" multiple>';
		$str .= '<option value="">Choose Rack Number</option>';
		foreach( $rack as $rackIndex => $rackValue ):
			$str .= '<option value="'.$rackIndex.'" >' .$rackValue. '</option>';
		endforeach;
		$str .= '</select>';
		
		echo $str; exit;
		/*$this->set( compact($rackNameAccordingToFloorText) );
		$this>render( 'rack_number_file' );*/
	}
		
	/*
	 * 
	 * Params, Get Level accordingly above
	 * 
	 */ 
	public function getLevelAccordingG_R()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		//Load Model
		$this->loadModel( 'Rack' );
		
		//Get Floor Id
		$groundName = explode(',',$this->request->data['groundText']);
		$rackText = explode(',',$this->request->data['rackText']);
		
		$rack = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , 
				array( 
					'fields' => array( 'Rack.id' , 'Rack.level_association' ) , 
					'conditions' => array( 'Rack.floor_name' => $groundName , 'Rack.rack_floorName' => $rackText ) , 'group' => array( 'Rack.level_association' ) , 'order' => array( 'Rack.id ASC' ) 
					) 
				);		
		
		$str = '<select id="levelNumber" class="form-control" name="data[levelNumber]" multiple>';
		$str .= '<option value="">Choose Level Number</option>';
		foreach( $rack as $rackIndex => $rackValue ):
			$str .= '<option value="'.$rackIndex.'" >' .$rackValue. '</option>';
		endforeach;
		$str .= '</select>';
		
		echo $str; exit;
		
		/*$this->set( compact($rackNameAccordingToFloorText) );
		$this>render( 'rack_number_file' );*/
	}
	
	/*
	 * 
	 * Params, Get Level accordingly above
	 * 
	 */ 
	public function getSectionAccordingG_R()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		//Load Model
		$this->loadModel( 'Rack' );
		
		//Get Floor Id
		$groundName = explode(',',$this->request->data['groundText']);
		$rackText = explode(',',$this->request->data['rackText']);
		$levelText = explode(',',$this->request->data['levelSelected']);
		
		$rack = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , 
				array( 
					'fields' => array( 'Rack.id' , 'Rack.rack_level_section' ) , 
					'conditions' => array( 'Rack.floor_name' => $groundName , 'Rack.rack_floorName' => $rackText , 'Rack.level_association' => $levelText ) , 'order' => array( 'Rack.id ASC' ) 
					) 
				);	
		$str = '<select id="sectionNumber" class="form-control" name="data[sectionNumber]" multiple>';
		$str .= '<option value="">Choose Section Number</option>';
		foreach( $rack as $rackIndex => $rackValue ):
			$str .= '<option value="'.$rackIndex.'" >' .$rackValue. '</option>';
		endforeach;
		$str .= '</select>';
		
		echo $str; exit;
		
		/*$this->set( compact($rackNameAccordingToFloorText) );
		$this>render( 'rack_number_file' );*/
	}
	
	/*
	 *
	 * Params, CheckIn Process 
	 * 
	 */
	public function checkIn()
	{
		$this->layout = 'index';
		
		$this->loadModel( 'Po' );
		//$getpoList	=	$this->Po->find( 'list', array( 'fields'=> array( 'Po.id','Po.po_name' ) ) );
		$getpoList	=	$this->Po->find( 'list', array( 'fields'=> array( 'Po.id','Po.po_name' ), 'conditions' => array( 'Po.status' => 1) ) );
		$this->set( 'getpoList', $getpoList );
	}
		
	public function searchProductByBarcode()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		
		$barcode	=	$this->request->data['barcode'];
		
		$productdeatil	=	$this->Product->find('first', array('conditions' => array('ProductDesc.barcode' => $barcode)));
		pr($productdeatil); exit;
		 
		$data['id'] 				= 	$productdeatil['ProductDesc']['id'];
		$data['product_id'] 				= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 				= 	$productdeatil['Product']['product_name'];
		$data['sku'] 				= 	$productdeatil['Product']['product_sku'];
		$data['shortdescription'] 	= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 	= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 				= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 			= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 				= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 			= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 			= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 			= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		
		$data['bin_specific_id'] 			= 	$productdeatil['ProductDesc']['bin_specific_id'];
		$data['bin_combined_text'] 			= 	$productdeatil['ProductDesc']['bin_combined_text'];		
		$allocationArray = explode( '##',$productdeatil['ProductDesc']['bin_combined_text'] );
		
		//echo (json_encode(array('status' => '1', 'data' => $data, 'image' => $imagedata, 'location' => $locationData)));
		echo (json_encode(array('status' => '1', 'data' => $data)));
		exit;
	}
	
	public function productStockExcel()
	{	 
			//chr(046)
		  $this->layout = '';
		  $this->autoRender = false;	
		  		  
		  $getStockDetail = ProductsController::getStockData();		 
		  ob_clean();    
		  App::import('Vendor', 'PHPExcel/IOFactory');
		  App::import('Vendor', 'PHPExcel');  
		  $objPHPExcel = new PHPExcel();       
		  
		  //Column Create  
		  $objPHPExcel->setActiveSheetIndex(0);
		  $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Product Name');     
		  $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Sku');
		  $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Barcode');
		  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Brand');		  
		  $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Price');		  
		  $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Avb. Stock');
		  $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Min. Stock'); 
		  $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Packaging Class'); 
		  $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Weight (kg)'); 
		  $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Length (mm)'); 
		  $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Width (mm)'); 
		  $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Height (mm)');
		  
		  /*$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Status'); */
		  
		  $inc = 2; foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue ):
		  
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getStockDetailValue['Product']['product_name'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $getStockDetailValue['Product']['product_sku'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getStockDetailValue['ProductDesc']['barcode'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getStockDetailValue['ProductDesc']['brand'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $getStockDetailValue['ProductPrice']['product_price'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $getStockDetailValue['Product']['current_stock_level'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $getStockDetailValue['Product']['minimum_stock_level'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $getStockDetailValue['ProductDesc']['variant_envelope_name'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $getStockDetailValue['ProductDesc']['weight'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, $getStockDetailValue['ProductDesc']['length'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, $getStockDetailValue['ProductDesc']['width'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, $getStockDetailValue['ProductDesc']['height'] );
				
				/* Get all bin location */ 
				$this->loadModel( 'BinLocation' );
				$params = array(
					'conditions' => array(
						'BinLocation.barcode' =>  $getStockDetailValue['ProductDesc']['barcode']
					)
				);
				
				$binLocation = json_decode(json_encode($this->BinLocation->find( 'all' , $params )),0);
				
				$totalStock = 0;
				$chrAscii = 77;
				$chrAscii2 = 78;
				$chrStart = 65;
				/*foreach( $binLocation as $binLocationIndex => $binLocationValue )
				{
						//echo $inc;
						$ch = chr( $chrAscii );
						$ch2 = chr( $chrAscii2 );
						$chStock = chr( $chrAscii );
						$putChar = chr( $chrStart );
						
						//Location
						$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Location-'.$putChar);
						$objPHPExcel->getActiveSheet()->setCellValue($ch.$inc, $binLocationValue->BinLocation->bin_location );
						
						//Stock per location
						$objPHPExcel->getActiveSheet()->setCellValue($ch2.'1', 'Stock');
						$objPHPExcel->getActiveSheet()->setCellValue($ch2.$inc, $binLocationValue->BinLocation->stock_by_location );
						
						//Stock add
						$totalStock = $totalStock + $binLocationValue->BinLocation->stock_by_location;
						
						$chrAscii = $chrAscii + 2;
						$chrStart++;
						$chrAscii2 = $chrAscii2 + 2;
						
				}*/
				//exit;
				$totalStock = 0;
				/*
				if( $getStockDetailValue['Product']['product_status'] == 0 ):				
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 'Active'); 
				else:
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 'De-Active' ); 
				endif;*/			
				  
		  $inc++;
		  endforeach;   
		  
		  //Set First Row for Range Analysis Sheet 
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:Q1')->getAlignment()->applyFromArray(
		  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:Q1')->getAlignment()->setWrapText(true);
		  $objPHPExcel->getActiveSheet(0)->setTitle('Current Stock');
		  $objPHPExcel->getActiveSheet(0)->getStyle("A1:Q1")->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet(0)
			 ->getStyle('A1:Q1')
			 ->applyFromArray(
			  array(
			   'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EBE5DB')
			   )
			  )
			 );		   
		  
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/stockUpdate.xls';
		  $uploadRemote = $getBase.'app/webroot/img/stockUpdate/stockUpdate.xls';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		  $objWriter->save($uploadUrl);		 
		   
	return $uploadRemote;	  
	}
	
	public function prepareExcel()
	{
		$this->layout = '';
		$this->autoRender = false;			
		echo ProductsController::productStockExcel(); exit;
	}
		
	public function getStockData()
	{
		 $this->loadModel('Product');
		 $this->loadModel('ProductDesc');
		 
		 $stockData = $this->Product->find( 'all' , 
                                array( 
                                    'fields' => array( 
                                        'DISTINCT Product.product_sku',
                                        'Product.*',
                                        'ProductDesc.*'
                                    ),
                                    'order' => 'Product.id DESC' 
                                )
                 );                 
	return $stockData;	 
	}
	
	public function getStockDataForCsv()
	{
		 $this->loadModel('Product');
		 $this->loadModel('ProductDesc');
		 
		 $this->Product->unbindModel( array(
			'hasOne' => array(
				'ProductDesc' , 'ProductPrice' 
				),
			'hasMany' => array(
				'ProductLocation'
				)		
			) 
		 );
		
		/*$this->ProductDesc->unbindModel(
			array(
				'belongsTo' => array(
					'Product'
				)
			)
		);*/
		
		$params = array(
			'fields' => array(
				'Product.id as MainProductId',
				'Product.product_name as ProductTitle',
				'Product.product_sku as MainSku',
				'Product.uploaded_stock as UploadStock',
				'Product.current_stock_level as AvailableStock',
				'ProductDesc.id as ProductDescId',
				'ProductDesc.product_id',
				'ProductDesc.barcode',
				'ProductDesc.product_type',
				'ProductDesc.product_defined_skus',
				'ProductDesc.id as ProductDescId',
				'ProductDesc.product_identifier',
				'ProductDesc.length',
				'ProductDesc.width',
				'ProductDesc.height',
				'ProductDesc.weight',
				'Product.product_status'
			),
			'order' => 'ProductDesc.id ASC'
		);
		$stockData = $this->ProductDesc->find( 'all' , $params );
	return $stockData;	 
	}
		
	/*
	 * 
	 * Params, Prepare csv file for setup virtual product
	 * 
	 */ 
	public function prepareVirtualStock()
	{
		$this->layout = '';
		$this->autoRender = false;	 		
		echo ProductsController::productVirtualStockCsvGeneration();exit;
	}


	
	public function productVirtualStockCsvGeneration()
	{	 
		  $this->layout = '';
		  $this->autoRender = false;	
		  
		  $getBase = Router::url('/', true);
		  		  
		  $getStockDetail = json_decode(json_encode(ProductsController::getStockDataForCsv()),0);		 		  
		  //pr($getStockDetail); exit;
		  
		  /*
		   * 
		   * Params, To setup virtual stock for upload over linnworks where it will update on each platform, 
		   * that we are using ( Amazon, Ebay , Magento etc etc )
		   * 
		   */ 
		  
		   ob_clean();    
		   App::import('Vendor', 'PHPExcel/IOFactory');
		   App::import('Vendor', 'PHPExcel');  
		   $objPHPExcel = new PHPExcel();       
		  
		   //Column Create  
		   $objPHPExcel->setActiveSheetIndex(0);
		   $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Product Title');     
		   $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Sku');
		   $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Barcode');
		   $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Original Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Rainbow Retail(Amazon)');
		   $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Storeforlife (Magento)');
		   $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Rainbow Retail DE');
		   $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Rainbow Retail ES');
		   $objPHPExcel->getActiveSheet()->setCellValue('J1', 'CostBreaker_UK');
		   $objPHPExcel->getActiveSheet()->setCellValue('K1', 'CostBreaker_DE');
		   $objPHPExcel->getActiveSheet()->setCellValue('L1', 'CostBreaker_FR');
		   $objPHPExcel->getActiveSheet()->setCellValue('M1', 'CostBreaker_ES');
		   $objPHPExcel->getActiveSheet()->setCellValue('N1', 'CostBreaker_IT');
		   $objPHPExcel->getActiveSheet()->setCellValue('O1', 'Tech_Drive_UK');
		   $objPHPExcel->getActiveSheet()->setCellValue('P1', 'Tech_Drive_IT');
		   $objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Tech_Drive_FR');
		   $objPHPExcel->getActiveSheet()->setCellValue('R1', 'Tech_Drive_ES');
		   $objPHPExcel->getActiveSheet()->setCellValue('S1', 'Tech_Drive_DE');
		   $objPHPExcel->getActiveSheet()->setCellValue('T1', 'EBAY0');
		   $objPHPExcel->getActiveSheet()->setCellValue('U1', 'Status');
		   $objPHPExcel->getActiveSheet()->setCellValue('V1', 'Length');
		   $objPHPExcel->getActiveSheet()->setCellValue('W1', 'Width');
		   $objPHPExcel->getActiveSheet()->setCellValue('X1', 'Height');
		   $objPHPExcel->getActiveSheet()->setCellValue('Y1', 'Weight');
		  
		   $inc = 2; $e = 0;foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue ):
							$rainbowRetail	 =	0;
							$rainbowRetailDe =	0;
							$rainbowRetailSE =	0;
							$storeForLife	 =	0;
							$costBreakerUk	 =	0;
							$costBreakerDe	 =	0;
							$costBreakerFr	 =	0;
							$costBreakerEs	 =	0;
							$costBreakerIt	 =	0;
							$techDriveUK	 =	0;
							$techDriveIT	 =	0;
							$techDriveFR	 =	0;
							$techDriveES	 =	0;
							$techDriveDE	 =	0;
							$ebay			 =	0;
							
							$sku =	$getStockDetailValue->Product->MainSku;
							$getvertualStockDetails	=	'';//$this->getSkuQuantity( $sku );
							if(!empty($getvertualStockDetails))
							{
								foreach( $getvertualStockDetails as $getvertualStockDetail )
								{
									if( $getvertualStockDetail['subsource']['sub_source'] == 'Rainbow Retail')
									{
										$rainbowRetail = $rainbowRetail + $getvertualStockDetail['subsource']['quantity'];
									}
									else if( $getvertualStockDetail['subsource']['sub_source'] == 'RAINBOW RETAIL DE' )
									{
										$rainbowRetailDe = $rainbowRetailDe + $getvertualStockDetail['subsource']['quantity'];
									}
									else if( $getvertualStockDetail['subsource']['sub_source'] == 'Rainbow_Retail_ES' )
									{
										$rainbowRetailSE = $rainbowRetailSE + $getvertualStockDetail['subsource']['quantity'];
									}
									else if ( $getvertualStockDetail['subsource']['sub_source'] == 'http://www.storeforlife.co.uk' )
									{
										$storeForLife = $storeForLife + $getvertualStockDetail['subsource']['quantity'];
									}
									else if ( $getvertualStockDetail['subsource']['sub_source'] == 'CostBreaker_UK' )
									{
										$costBreakerUk = $costBreakerUk + $getvertualStockDetail['subsource']['quantity'];
									}
									else if ( $getvertualStockDetail['subsource']['sub_source'] == 'CostBreaker_DE' )
									{
										$costBreakerDe = $costBreakerDe + $getvertualStockDetail['subsource']['quantity'];
									}
									else if ( $getvertualStockDetail['subsource']['sub_source'] == 'CostBreaker_FR' )
									{
										$costBreakerFr = $costBreakerFr + $getvertualStockDetail['subsource']['quantity'];
									}
									else if ( $getvertualStockDetail['subsource']['sub_source'] == 'CostBreaker_ES' )
									{
										$costBreakerEs = $costBreakerEs + $getvertualStockDetail['subsource']['quantity'];
									}
									else if ( $getvertualStockDetail['subsource']['sub_source'] == 'CostBreaker_IT' )
									{
										$costBreakerIt = $costBreakerIt + $getvertualStockDetail['subsource']['quantity'];
									}
									else if ( $getvertualStockDetail['subsource']['sub_source'] == 'Tech_Drive_UK' )
									{
										$techDriveUK = $techDriveUK + $getvertualStockDetail['subsource']['quantity'];
									}
									else if ( $getvertualStockDetail['subsource']['sub_source'] == 'Tech_Drive_IT' )
									{
										$techDriveIT = $techDriveIT + $getvertualStockDetail['subsource']['quantity'];
									}
									else if ( $getvertualStockDetail['subsource']['sub_source'] == 'Tech_Drive_FR' )
									{
										$costBreakerIt = $costBreakerIt + $getvertualStockDetail['subsource']['quantity'];
									}
									else if ( $getvertualStockDetail['subsource']['sub_source'] == 'Tech_Drive_ES' )
									{
										$techDriveFR = $techDriveFR + $getvertualStockDetail['subsource']['quantity'];
									}
									else if ( $getvertualStockDetail['subsource']['sub_source'] == 'Tech_Drive_DE' )
									{
										$techDriveDE = $techDriveDE + $getvertualStockDetail['subsource']['quantity'];
									}
									else if ( $getvertualStockDetail['subsource']['sub_source'] == 'EBAY0' )
									{
										$ebay = $ebay + $getvertualStockDetail['subsource']['quantity'];
									}
								}
							}
				
				if( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "single" ):
					
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getStockDetailValue->Product->ProductTitle );    
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
					if( $getStockDetailValue->Product->AvailableStock <= 0 ):
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );	
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, 0 );
						
						//Set highlight full row
						$objPHPExcel->getActiveSheet(0)
						->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));					
						
						//For other subsource
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('S'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('T'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('U'.$inc, $getStockDetailValue->Product->product_status );
						$objPHPExcel->getActiveSheet()->setCellValue('V'.$inc, $getStockDetailValue->ProductDesc->length );
						$objPHPExcel->getActiveSheet()->setCellValue('W'.$inc, $getStockDetailValue->ProductDesc->width );
						$objPHPExcel->getActiveSheet()->setCellValue('X'.$inc, $getStockDetailValue->ProductDesc->height );
						$objPHPExcel->getActiveSheet()->setCellValue('Y'.$inc, $getStockDetailValue->ProductDesc->weight );
					else:
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getStockDetailValue->Product->AvailableStock );
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $getStockDetailValue->Product->AvailableStock );							
						//For other subsource
						if( $getStockDetailValue->Product->AvailableStock == 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('S'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('T'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('U'.$inc, $getStockDetailValue->Product->product_status );
							$objPHPExcel->getActiveSheet()->setCellValue('V'.$inc, $getStockDetailValue->ProductDesc->length );
							$objPHPExcel->getActiveSheet()->setCellValue('W'.$inc, $getStockDetailValue->ProductDesc->width );
							$objPHPExcel->getActiveSheet()->setCellValue('X'.$inc, $getStockDetailValue->ProductDesc->height );
							$objPHPExcel->getActiveSheet()->setCellValue('Y'.$inc, $getStockDetailValue->ProductDesc->weight );
						}
						else if( $getStockDetailValue->Product->AvailableStock < 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('S'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('T'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('U'.$inc, $getStockDetailValue->Product->product_status );
							$objPHPExcel->getActiveSheet()->setCellValue('V'.$inc, $getStockDetailValue->ProductDesc->length );
							$objPHPExcel->getActiveSheet()->setCellValue('W'.$inc, $getStockDetailValue->ProductDesc->width );
							$objPHPExcel->getActiveSheet()->setCellValue('X'.$inc, $getStockDetailValue->ProductDesc->height );
							$objPHPExcel->getActiveSheet()->setCellValue('Y'.$inc, $getStockDetailValue->ProductDesc->weight );
						}
						else
						{							
							
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getStockDetailValue->Product->AvailableStock / 3) + $rainbowRetail );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $storeForLife );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, floor($getStockDetailValue->Product->AvailableStock / 3) + $rainbowRetailDe);
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, floor($getStockDetailValue->Product->AvailableStock / 3) + $rainbowRetailSE);
							$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, floor($getStockDetailValue->Product->AvailableStock / 3 ) + $costBreakerUk);
							$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $costBreakerDe);
							$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, floor($getStockDetailValue->Product->AvailableStock ) + $costBreakerFr);
							$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $costBreakerEs);
							$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $costBreakerIt);
							$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, floor($getStockDetailValue->Product->AvailableStock ) + $techDriveUK);
							$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $techDriveIT);
							$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $techDriveFR);
							$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, floor($getStockDetailValue->Product->AvailableStock ) + $techDriveES);
							$objPHPExcel->getActiveSheet()->setCellValue('S'.$inc, floor($getStockDetailValue->Product->AvailableStock ) + $techDriveDE);
							$objPHPExcel->getActiveSheet()->setCellValue('T'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $ebay);
							$objPHPExcel->getActiveSheet()->setCellValue('U'.$inc, $getStockDetailValue->Product->product_status );
							$objPHPExcel->getActiveSheet()->setCellValue('V'.$inc, $getStockDetailValue->ProductDesc->length );
							$objPHPExcel->getActiveSheet()->setCellValue('W'.$inc, $getStockDetailValue->ProductDesc->width );
							$objPHPExcel->getActiveSheet()->setCellValue('X'.$inc, $getStockDetailValue->ProductDesc->height );
							$objPHPExcel->getActiveSheet()->setCellValue('Y'.$inc, $getStockDetailValue->ProductDesc->weight );
							
							
							/*$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );*/
						}
					endif;
					
										
				elseif( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "bundle" ):					
					
					if( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "single" ):
										
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
		
						$params = array(
							'conditions' => array(
								'Product.product_sku' => trim($getStockDetailValue->ProductDesc->product_defined_skus)
							),
							'fields' => array(
								//'IF(Product.current_stock_level > 2, Product.current_stock_level , 0) as FindStockThrough__LimitFactor'
								'Product.current_stock_level as FindStockThrough__LimitFactor'
							),
							'order' => 'Product.id ASC'
						);
						
						//echo $getStockDetailValue->ProductDesc->product_defined_skus .'=='. $getStockDetailValue->Product->MainSku;
						//echo "<br>";
						
						$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);
						//pr($stockData);
						
						$mainSku = explode('-',$getStockDetailValue->Product->MainSku);						
						$getDivisionNumberFromBundleSku = $mainSku[2];						
						$index = 0;
						$getAvailableStockFor_Bundle_WithSameSku = $stockData[0][$index]->FindStockThrough__LimitFactor;
						
						//Set data into file 
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );
						
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );
						
						if( $getAvailableStockFor_Bundle_WithSameSku > 2 ): 
							$getAvailableStockFor_Bundle_WithSameSku = floor($getAvailableStockFor_Bundle_WithSameSku / $getDivisionNumberFromBundleSku) - 2;
							
							/*if( $getAvailableStockFor_Bundle_WithSameSku < 2 )
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );
							else
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );*/
							
							//For other subsource
							if( $getAvailableStockFor_Bundle_WithSameSku == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('S'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('T'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('U'.$inc, $getStockDetailValue->Product->product_status );
								$objPHPExcel->getActiveSheet()->setCellValue('V'.$inc, $getStockDetailValue->ProductDesc->length );
								$objPHPExcel->getActiveSheet()->setCellValue('W'.$inc, $getStockDetailValue->ProductDesc->width );
								$objPHPExcel->getActiveSheet()->setCellValue('X'.$inc, $getStockDetailValue->ProductDesc->height );
								$objPHPExcel->getActiveSheet()->setCellValue('Y'.$inc, $getStockDetailValue->ProductDesc->weight );
							}
							else if( $getAvailableStockFor_Bundle_WithSameSku < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('S'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('T'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('U'.$inc, $getStockDetailValue->Product->product_status );
								$objPHPExcel->getActiveSheet()->setCellValue('V'.$inc, $getStockDetailValue->ProductDesc->length );
								$objPHPExcel->getActiveSheet()->setCellValue('W'.$inc, $getStockDetailValue->ProductDesc->width );
								$objPHPExcel->getActiveSheet()->setCellValue('X'.$inc, $getStockDetailValue->ProductDesc->height );
								$objPHPExcel->getActiveSheet()->setCellValue('Y'.$inc, $getStockDetailValue->ProductDesc->weight );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/3) + $rainbowRetail);
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/4) + $storeForLife);
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/3) + $rainbowRetailDe);
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/3) + $rainbowRetailSE);
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/3 ) + $costBreakerUk);
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku / 4) + $costBreakerDe);
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku ) + $costBreakerFr);
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku / 4) + $costBreakerEs);
								$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku / 4) + $costBreakerIt);
								$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku ) + $techDriveUK);
								$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku / 4) + $techDriveIT);
								$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku / 4) + $techDriveFR);
								$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku ) + $techDriveES);
								$objPHPExcel->getActiveSheet()->setCellValue('S'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku ) + $techDriveDE);
								$objPHPExcel->getActiveSheet()->setCellValue('T'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku / 4) + $ebay);
								$objPHPExcel->getActiveSheet()->setCellValue('U'.$inc, $getStockDetailValue->Product->product_status );
								$objPHPExcel->getActiveSheet()->setCellValue('V'.$inc, $getStockDetailValue->ProductDesc->length );
								$objPHPExcel->getActiveSheet()->setCellValue('W'.$inc, $getStockDetailValue->ProductDesc->width );
								$objPHPExcel->getActiveSheet()->setCellValue('X'.$inc, $getStockDetailValue->ProductDesc->height );
								$objPHPExcel->getActiveSheet()->setCellValue('Y'.$inc, $getStockDetailValue->ProductDesc->weight );
							
							}	
						else:
							
							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('S'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('T'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('U'.$inc, $getStockDetailValue->Product->product_status );
							$objPHPExcel->getActiveSheet()->setCellValue('V'.$inc, $getStockDetailValue->ProductDesc->length );
							$objPHPExcel->getActiveSheet()->setCellValue('W'.$inc, $getStockDetailValue->ProductDesc->width );
							$objPHPExcel->getActiveSheet()->setCellValue('X'.$inc, $getStockDetailValue->ProductDesc->height );
							$objPHPExcel->getActiveSheet()->setCellValue('Y'.$inc, $getStockDetailValue->ProductDesc->weight );

						endif;	

						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $stockData[0][$index]->FindStockThrough__LimitFactor );						
						
						
							
					elseif( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "multiple" ):
					
						//Set if different sku will manipulate over limit factor						
						/*$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );*/
						 
						$stockData = '';
						  						
						$params = array(
							'conditions' => array(
								'ProductDesc.product_defined_skus' => $getStockDetailValue->ProductDesc->product_defined_skus
							),
							'fields' => array(
								'ProductDesc.product_defined_skus as Diff_Skus',
								'Product.product_sku'
							),
							'order' => 'ProductDesc.id ASC'
						);
						
						$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);	
						//pr($stockData);
						$diff_SkuList = explode(':',trim($stockData[0]->ProductDesc->Diff_Skus));					
						
						//Get Diff Sku stock value under min section according to limit factor
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
						
						$params = array(
							'conditions' => array(
								'Product.product_sku' => $diff_SkuList
							),
							'fields' => array(
								//'IF(Product.current_stock_level > 2, (Product.current_stock_level -2) , 0) as FindStockThrough__LimitFactor'
								'Product.id',
								'Product.current_stock_level'
							),
							'order' => 'Product.id ASC'
						);
						
						//Get relevant data
						$getListStockValue = $this->Product->find( 'list' , $params );							
						$stockDataValue = min($getListStockValue);
						
						//Here we have found min lowest stock sku value tha means, need to put it into csv but keep safety margin
						if( $stockDataValue > 2 ):
							
							//If greater then will need to keep 2 for safety margin
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockDataValue );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							if( ($stockDataValue-2) == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('S'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('T'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('U'.$inc, $getStockDetailValue->Product->product_status );
								$objPHPExcel->getActiveSheet()->setCellValue('V'.$inc, $getStockDetailValue->ProductDesc->length );
								$objPHPExcel->getActiveSheet()->setCellValue('W'.$inc, $getStockDetailValue->ProductDesc->width );
								$objPHPExcel->getActiveSheet()->setCellValue('X'.$inc, $getStockDetailValue->ProductDesc->height );
								$objPHPExcel->getActiveSheet()->setCellValue('Y'.$inc, $getStockDetailValue->ProductDesc->weight );
							}
							else if( ($stockDataValue-2) < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('S'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('T'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('U'.$inc, $getStockDetailValue->Product->product_status );
								$objPHPExcel->getActiveSheet()->setCellValue('V'.$inc, $getStockDetailValue->ProductDesc->length );
								$objPHPExcel->getActiveSheet()->setCellValue('W'.$inc, $getStockDetailValue->ProductDesc->width );
								$objPHPExcel->getActiveSheet()->setCellValue('X'.$inc, $getStockDetailValue->ProductDesc->height );
								$objPHPExcel->getActiveSheet()->setCellValue('Y'.$inc, $getStockDetailValue->ProductDesc->weight );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor(($stockDataValue-2)/3) + $rainbowRetail );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor(($stockDataValue-2)/4) + $storeForLife );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, floor(($stockDataValue-2)/3) + $rainbowRetailDe);
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, floor(($stockDataValue-2)/3) + $rainbowRetailSE);
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, floor(($stockDataValue-2)/3) + $costBreakerUk);
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, floor(($stockDataValue-2)/4) + $costBreakerDe);
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, floor(($stockDataValue-2)) + $costBreakerFr);
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, floor(($stockDataValue-2)/4) + $costBreakerEs);
								$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, floor(($stockDataValue-2)/4) + $costBreakerIt);
								$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, floor(($stockDataValue-2)) + $techDriveUK);
								$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, floor(($stockDataValue-2)/4) + $techDriveIT);
								$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, floor(($stockDataValue-2)/4) + $techDriveFR);
								$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, floor(($stockDataValue-2)) + $techDriveES);
								$objPHPExcel->getActiveSheet()->setCellValue('S'.$inc, floor(($stockDataValue-2)) + $techDriveDE);
								$objPHPExcel->getActiveSheet()->setCellValue('T'.$inc, floor(($stockDataValue-2)/4) + $ebay);
								$objPHPExcel->getActiveSheet()->setCellValue('U'.$inc, $getStockDetailValue->Product->product_status );
								$objPHPExcel->getActiveSheet()->setCellValue('V'.$inc, $getStockDetailValue->ProductDesc->length );
								$objPHPExcel->getActiveSheet()->setCellValue('W'.$inc, $getStockDetailValue->ProductDesc->width );
								$objPHPExcel->getActiveSheet()->setCellValue('X'.$inc, $getStockDetailValue->ProductDesc->height );
								$objPHPExcel->getActiveSheet()->setCellValue('Y'.$inc, $getStockDetailValue->ProductDesc->weight );
				
							}
							
						elseif( $stockDataValue <= 2 ):
							
							//If less or equal then will need to put space / blank
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockDataValue );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('P'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('R'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('S'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('T'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('U'.$inc, $getStockDetailValue->Product->product_status );
							$objPHPExcel->getActiveSheet()->setCellValue('V'.$inc, $getStockDetailValue->ProductDesc->length );
							$objPHPExcel->getActiveSheet()->setCellValue('W'.$inc, $getStockDetailValue->ProductDesc->width );
							$objPHPExcel->getActiveSheet()->setCellValue('X'.$inc, $getStockDetailValue->ProductDesc->height );
							$objPHPExcel->getActiveSheet()->setCellValue('Y'.$inc, $getStockDetailValue->ProductDesc->weight );

							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));
						endif;
						
					endif;
					
				endif;
				
		  $e++;$inc++;
		  endforeach;
		  
		  //File creation 		  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->applyFromArray(
		  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->setWrapText(true);
		  $objPHPExcel->getActiveSheet(0)->setTitle('Virtual Stock');
		  $objPHPExcel->getActiveSheet(0)->getStyle("A1:E1")->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet(0)
			 ->getStyle('A1:E1')
			 ->applyFromArray(
			  array(
			   'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EBE5DB')
			   )
			  )
			 );		   
		  
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/virtualStock.csv';
		  //$uploadRemote = $getBase.'app/webroot/img/stockUpdate/virtualStock.csv';
		  $uploadRemote = $getBase.'img/stockUpdate/virtualStock.csv';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		  $objWriter->save($uploadUrl);	
		  
    return $uploadRemote;       		  
	}




	public function productVirtualStockCsvGeneration_old140416()
	{	 
		  $this->layout = '';
		  $this->autoRender = false;	
		  
		  $getBase = Router::url('/', true);
		  		  
		  $getStockDetail = json_decode(json_encode(ProductsController::getStockDataForCsv()),0);		 		  
		  //pr($getStockDetail); exit;
		  
		  /*
		   * 
		   * Params, To setup virtual stock for upload over linnworks where it will update on each platform, 
		   * that we are using ( Amazon, Ebay , Magento etc etc )
		   * 
		   */ 
		  
		   ob_clean();    
		   App::import('Vendor', 'PHPExcel/IOFactory');
		   App::import('Vendor', 'PHPExcel');  
		   $objPHPExcel = new PHPExcel();       
		  
		   //Column Create  
		   $objPHPExcel->setActiveSheetIndex(0);
		   $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Product Title');     
		   $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Sku');
		   $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Barcode');
		   $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Original Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Rainbow Retail(Amazon)');
		   $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Storeforlife (Magento)');
		   $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Rainbow Retail DE');
		   $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Rainbow Retail ES');
		  
		   $inc = 2; $e = 0;foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue ):
							$rainbowRetail	 =	0;
							$rainbowRetailDe =	0;
							$rainbowRetailSE =	0;
							$storeForLife	 =	0;
							$sku =	$getStockDetailValue->Product->MainSku;
							$getvertualStockDetails	=	$this->getSkuQuantity( $sku );
							foreach( $getvertualStockDetails as $getvertualStockDetail )
							{
								if( $getvertualStockDetail['subsource']['sub_source'] == 'Rainbow Retail')
								{
									$rainbowRetail = $rainbowRetail + $getvertualStockDetail['subsource']['quantity'];
								}
								else if( $getvertualStockDetail['subsource']['sub_source'] == 'RAINBOW RETAIL DE' )
								{
									$rainbowRetailDe = $rainbowRetailDe + $getvertualStockDetail['subsource']['quantity'];
								}
								else if( $getvertualStockDetail['subsource']['sub_source'] == 'Rainbow_Retail_ES' )
								{
									$rainbowRetailSE = $rainbowRetailSE + $getvertualStockDetail['subsource']['quantity'];
								}
								else if ( $getvertualStockDetail['subsource']['sub_source'] == 'http://www.storeforlife.co.uk' )
								{
									$storeForLife = $storeForLife + $getvertualStockDetail['subsource']['quantity'];
								}
							}
				
				if( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "single" ):
					
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
					if( $getStockDetailValue->Product->AvailableStock <= 0 ):
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );	
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, 0 );
						
						//Set highlight full row
						$objPHPExcel->getActiveSheet(0)
						->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));					
						
						//For other subsource
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
					else:
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getStockDetailValue->Product->AvailableStock );
													
						//For other subsource
						if( $getStockDetailValue->Product->AvailableStock == 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 1 );
						}
						else if( $getStockDetailValue->Product->AvailableStock < 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
						}
						else
						{							
							
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getStockDetailValue->Product->AvailableStock / 3) + $rainbowRetail );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $storeForLife );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $rainbowRetailDe);
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $rainbowRetailSE);
							
							
							/*$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );*/
						}
					endif;
					
										
				elseif( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "bundle" ):					
					
					if( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "single" ):
										
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
		
						$params = array(
							'conditions' => array(
								'Product.product_sku' => trim($getStockDetailValue->ProductDesc->product_defined_skus)
							),
							'fields' => array(
								//'IF(Product.current_stock_level > 2, Product.current_stock_level , 0) as FindStockThrough__LimitFactor'
								'Product.current_stock_level as FindStockThrough__LimitFactor'
							),
							'order' => 'Product.id ASC'
						);
						
						//echo $getStockDetailValue->ProductDesc->product_defined_skus .'=='. $getStockDetailValue->Product->MainSku;
						//echo "<br>";
						
						$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);
						//pr($stockData);
						
						$mainSku = explode('-',$getStockDetailValue->Product->MainSku);						
						$getDivisionNumberFromBundleSku = $mainSku[2];						
						$index = 0;
						$getAvailableStockFor_Bundle_WithSameSku = $stockData[0][$index]->FindStockThrough__LimitFactor;
						
						//Set data into file 
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );
						
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );
						
						if( $getAvailableStockFor_Bundle_WithSameSku > 2 ): 
							$getAvailableStockFor_Bundle_WithSameSku = floor($getAvailableStockFor_Bundle_WithSameSku / $getDivisionNumberFromBundleSku) - 2;
							
							/*if( $getAvailableStockFor_Bundle_WithSameSku < 2 )
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );
							else
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );*/
							
							//For other subsource
							if( $getAvailableStockFor_Bundle_WithSameSku == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 1 );
							}
							else if( $getAvailableStockFor_Bundle_WithSameSku < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) + $rainbowRetail);
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) + $storeForLife);
$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) + $rainbowRetailDe);
$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) + $$rainbowRetailSE);
							}	
						else:
							
							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );

						endif;	

						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $stockData[0][$index]->FindStockThrough__LimitFactor );						
						
						
							
					elseif( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "multiple" ):
					
						//Set if different sku will manipulate over limit factor						
						/*$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );*/
						 
						$stockData = '';
						  						
						$params = array(
							'conditions' => array(
								'ProductDesc.product_defined_skus' => $getStockDetailValue->ProductDesc->product_defined_skus
							),
							'fields' => array(
								'ProductDesc.product_defined_skus as Diff_Skus',
								'Product.product_sku'
							),
							'order' => 'ProductDesc.id ASC'
						);
						
						$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);	
						//pr($stockData);
						$diff_SkuList = explode(':',trim($stockData[0]->ProductDesc->Diff_Skus));					
						
						//Get Diff Sku stock value under min section according to limit factor
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
						
						$params = array(
							'conditions' => array(
								'Product.product_sku' => $diff_SkuList
							),
							'fields' => array(
								//'IF(Product.current_stock_level > 2, (Product.current_stock_level -2) , 0) as FindStockThrough__LimitFactor'
								'Product.id',
								'Product.current_stock_level'
							),
							'order' => 'Product.id ASC'
						);
						
						//Get relevant data
						$getListStockValue = $this->Product->find( 'list' , $params );							
						$stockDataValue = min($getListStockValue);
						
						//Here we have found min lowest stock sku value tha means, need to put it into csv but keep safety margin
						if( $stockDataValue > 2 ):
							
							//If greater then will need to keep 2 for safety margin
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockDataValue );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							if( ($stockDataValue-2) == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 1 );
							}
							else if( ($stockDataValue-2) < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor(($stockDataValue-2)/2) + $rainbowRetail );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor(($stockDataValue-2)/2) + $storeForLife );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, floor(($stockDataValue-2)/2) + $rainbowRetailDe);
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, floor(($stockDataValue-2)/2) + $rainbowRetailSE);

							
							}
							
						elseif( $stockDataValue <= 2 ):
							
							//If less or equal then will need to put space / blank
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockDataValue );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );

							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));
						endif;
						
					endif;
					
				endif;
				
		  $e++;$inc++;
		  endforeach;
		  
		  //File creation 		  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->applyFromArray(
		  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->setWrapText(true);
		  $objPHPExcel->getActiveSheet(0)->setTitle('Virtual Stock');
		  $objPHPExcel->getActiveSheet(0)->getStyle("A1:E1")->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet(0)
			 ->getStyle('A1:E1')
			 ->applyFromArray(
			  array(
			   'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EBE5DB')
			   )
			  )
			 );		   
		  
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/virtualStock.csv';
		  //$uploadRemote = $getBase.'app/webroot/img/stockUpdate/virtualStock.csv';
		  $uploadRemote = $getBase.'img/stockUpdate/virtualStock.csv';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		  $objWriter->save($uploadUrl);	
		  
    return $uploadRemote;       		  
	}

	

	public function productVirtualStockCsvGeneration_old240216()
	{	 
		  $this->layout = '';
		  $this->autoRender = false;	
		  
		  $getBase = Router::url('/', true);
		  		  
		  $getStockDetail = json_decode(json_encode(ProductsController::getStockDataForCsv()),0);		 		  
		  //pr($getStockDetail); exit;
		  
		  /*
		   * 
		   * Params, To setup virtual stock for upload over linnworks where it will update on each platform, 
		   * that we are using ( Amazon, Ebay , Magento etc etc )
		   * 
		   */ 
		  
		   ob_clean();    
		   App::import('Vendor', 'PHPExcel/IOFactory');
		   App::import('Vendor', 'PHPExcel');  
		   $objPHPExcel = new PHPExcel();       
		  
		   //Column Create  
		   $objPHPExcel->setActiveSheetIndex(0);
		   $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Product Title');     
		   $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Sku');
		   $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Barcode');
		   $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Original Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Rainbow Retail(Amazon)');
		   $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Storeforlife (Magento)');
		   $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Rainbow Retail DE');
		   $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Rainbow Retail ES');
		  
		   $inc = 2; $e = 0;foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue ):
				
				if( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "single" ):
					
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
					if( $getStockDetailValue->Product->AvailableStock <= 0 ):
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );	
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, 0 );
						
						//Set highlight full row
						$objPHPExcel->getActiveSheet(0)
						->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));					
						
						//For other subsource
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
					else:
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getStockDetailValue->Product->AvailableStock );
													
						//For other subsource
						if( $getStockDetailValue->Product->AvailableStock == 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 1 );
						}
						else if( $getStockDetailValue->Product->AvailableStock < 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
						}
						else
						{							
							$rainbowRetail	 =	0;
							$rainbowRetailDe =	0;
							$rainbowRetailSE =	0;
							$storeForLife	 =	0;
							$sku =	$getStockDetailValue->Product->MainSku;
							$getvertualStockDetails	=	$this->getSkuQuantity( $sku );
							foreach( $getvertualStockDetails as $getvertualStockDetail )
							{
								if( $getvertualStockDetail['subsource']['sub_source'] == 'Rainbow Retail')
								{
									$rainbowRetail = $rainbowRetail + $getvertualStockDetail['subsource']['quantity'];
								}
								else if( $getvertualStockDetail['subsource']['sub_source'] == 'RAINBOW RETAIL DE' )
								{
									$rainbowRetailDe = $rainbowRetailDe + $getvertualStockDetail['subsource']['quantity'];
								}
								else if( $getvertualStockDetail['subsource']['sub_source'] == 'Rainbow_Retail_ES' )
								{
									$rainbowRetailSE = $rainbowRetailSE + $getvertualStockDetail['subsource']['quantity'];
								}
								else if ( $getvertualStockDetail['subsource']['sub_source'] == 'http://www.storeforlife.co.uk' )
								{
									$storeForLife = $storeForLife + $getvertualStockDetail['subsource']['quantity'];
								}
							}
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) + $rainbowRetail );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) + $storeForLife );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) + $rainbowRetailDe);
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) + $rainbowRetailSE);
							
							
							/*$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );*/
						}
					endif;
					
										
				elseif( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "bundle" ):					
					
					if( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "single" ):
										
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
		
						$params = array(
							'conditions' => array(
								'Product.product_sku' => trim($getStockDetailValue->ProductDesc->product_defined_skus)
							),
							'fields' => array(
								//'IF(Product.current_stock_level > 2, Product.current_stock_level , 0) as FindStockThrough__LimitFactor'
								'Product.current_stock_level as FindStockThrough__LimitFactor'
							),
							'order' => 'Product.id ASC'
						);
						
						//echo $getStockDetailValue->ProductDesc->product_defined_skus .'=='. $getStockDetailValue->Product->MainSku;
						//echo "<br>";
						
						$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);
						//pr($stockData);
						
						$mainSku = explode('-',$getStockDetailValue->Product->MainSku);						
						$getDivisionNumberFromBundleSku = $mainSku[2];						
						$index = 0;
						$getAvailableStockFor_Bundle_WithSameSku = $stockData[0][$index]->FindStockThrough__LimitFactor;
						
						//Set data into file 
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );
						
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );
						
						if( $getAvailableStockFor_Bundle_WithSameSku > 2 ): 
							$getAvailableStockFor_Bundle_WithSameSku = floor($getAvailableStockFor_Bundle_WithSameSku / $getDivisionNumberFromBundleSku) - 2;
							
							/*if( $getAvailableStockFor_Bundle_WithSameSku < 2 )
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );
							else
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );*/
							
							//For other subsource
							if( $getAvailableStockFor_Bundle_WithSameSku == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
							}
							else if( $getAvailableStockFor_Bundle_WithSameSku < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) );
							}	
						else:
							
							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						endif;	
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $stockData[0][$index]->FindStockThrough__LimitFactor );						
						
						
							
					elseif( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "multiple" ):
					
						//Set if different sku will manipulate over limit factor						
						/*$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );*/
						 
						$stockData = '';
						  						
						$params = array(
							'conditions' => array(
								'ProductDesc.product_defined_skus' => $getStockDetailValue->ProductDesc->product_defined_skus
							),
							'fields' => array(
								'ProductDesc.product_defined_skus as Diff_Skus',
								'Product.product_sku'
							),
							'order' => 'ProductDesc.id ASC'
						);
						
						$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);	
						//pr($stockData);
						$diff_SkuList = explode(':',trim($stockData[0]->ProductDesc->Diff_Skus));					
						
						//Get Diff Sku stock value under min section according to limit factor
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
						
						$params = array(
							'conditions' => array(
								'Product.product_sku' => $diff_SkuList
							),
							'fields' => array(
								//'IF(Product.current_stock_level > 2, (Product.current_stock_level -2) , 0) as FindStockThrough__LimitFactor'
								'Product.id',
								'Product.current_stock_level'
							),
							'order' => 'Product.id ASC'
						);
						
						//Get relevant data
						$getListStockValue = $this->Product->find( 'list' , $params );							
						$stockDataValue = min($getListStockValue);
						
						//Here we have found min lowest stock sku value tha means, need to put it into csv but keep safety margin
						if( $stockDataValue > 2 ):
							
							//If greater then will need to keep 2 for safety margin
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockDataValue );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							if( ($stockDataValue-2) == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
							}
							else if( ($stockDataValue-2) < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor(($stockDataValue-2)/2) );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor(($stockDataValue-2)/2) );
							}
							
						elseif( $stockDataValue <= 2 ):
							
							//If less or equal then will need to put space / blank
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockDataValue );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						
							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));
						endif;
						
					endif;
					
				endif;
				
		  $e++;$inc++;
		  endforeach;
		  
		  //File creation 		  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->applyFromArray(
		  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->setWrapText(true);
		  $objPHPExcel->getActiveSheet(0)->setTitle('Virtual Stock');
		  $objPHPExcel->getActiveSheet(0)->getStyle("A1:E1")->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet(0)
			 ->getStyle('A1:E1')
			 ->applyFromArray(
			  array(
			   'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EBE5DB')
			   )
			  )
			 );		   
		  
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/virtualStock.csv';
		  //$uploadRemote = $getBase.'app/webroot/img/stockUpdate/virtualStock.csv';
		  $uploadRemote = $getBase.'img/stockUpdate/virtualStock.csv';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		  $objWriter->save($uploadUrl);	
		  
    return $uploadRemote;       		  
	}
	
	public function productVirtualStockCsvGeneration_old230216()
	{	 
		  $this->layout = '';
		  $this->autoRender = false;	
		  
		  $getBase = Router::url('/', true);
		  		  
		  $getStockDetail = json_decode(json_encode(ProductsController::getStockDataForCsv()),0);		 		  
		  //pr($getStockDetail); exit;
		  
		  /*
		   * 
		   * Params, To setup virtual stock for upload over linnworks where it will update on each platform, 
		   * that we are using ( Amazon, Ebay , Magento etc etc )
		   * 
		   */ 
		  
		   ob_clean();    
		   App::import('Vendor', 'PHPExcel/IOFactory');
		   App::import('Vendor', 'PHPExcel');  
		   $objPHPExcel = new PHPExcel();       
		  
		   //Column Create  
		   $objPHPExcel->setActiveSheetIndex(0);
		   $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Product Title');     
		   $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Sku');
		   $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Barcode');
		   $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Original Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Rainbow Retail(Amazon)');
		   $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Storeforlife (Magento)');
		  
		   $inc = 2; $e = 0;foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue ):
				
				if( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "single" ):
					
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
					if( $getStockDetailValue->Product->AvailableStock <= 0 ):
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );	
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, 0 );
						//Set highlight full row
						$objPHPExcel->getActiveSheet(0)
						->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));					
						
						//For other subsource
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
					else:
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getStockDetailValue->Product->AvailableStock );
													
						//For other subsource
						if( $getStockDetailValue->Product->AvailableStock == 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
						}
						else if( $getStockDetailValue->Product->AvailableStock < 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						}
						else
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );
						}
					endif;
					
										
				elseif( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "bundle" ):					
					
					if( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "single" ):
										
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
		
						$params = array(
							'conditions' => array(
								'Product.product_sku' => trim($getStockDetailValue->ProductDesc->product_defined_skus)
							),
							'fields' => array(
								//'IF(Product.current_stock_level > 2, Product.current_stock_level , 0) as FindStockThrough__LimitFactor'
								'Product.current_stock_level as FindStockThrough__LimitFactor'
							),
							'order' => 'Product.id ASC'
						);
						
						//echo $getStockDetailValue->ProductDesc->product_defined_skus .'=='. $getStockDetailValue->Product->MainSku;
						//echo "<br>";
						
						$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);
						//pr($stockData);
						
						$mainSku = explode('-',$getStockDetailValue->Product->MainSku);						
						$getDivisionNumberFromBundleSku = $mainSku[2];						
						$index = 0;
						$getAvailableStockFor_Bundle_WithSameSku = $stockData[0][$index]->FindStockThrough__LimitFactor;
						
						//Set data into file 
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );
						
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );
						
						if( $getAvailableStockFor_Bundle_WithSameSku > 2 ): 
							$getAvailableStockFor_Bundle_WithSameSku = floor($getAvailableStockFor_Bundle_WithSameSku / $getDivisionNumberFromBundleSku) - 2;
							
							/*if( $getAvailableStockFor_Bundle_WithSameSku < 2 )
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );
							else
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );*/
							
							//For other subsource
							if( $getAvailableStockFor_Bundle_WithSameSku == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
							}
							else if( $getAvailableStockFor_Bundle_WithSameSku < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) );
							}	
						else:
							
							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						endif;	
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $stockData[0][$index]->FindStockThrough__LimitFactor );						
						
						
							
					elseif( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "multiple" ):
					
						//Set if different sku will manipulate over limit factor						
						/*$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );*/
						 
						$stockData = '';
						  						
						$params = array(
							'conditions' => array(
								'ProductDesc.product_defined_skus' => $getStockDetailValue->ProductDesc->product_defined_skus
							),
							'fields' => array(
								'ProductDesc.product_defined_skus as Diff_Skus',
								'Product.product_sku'
							),
							'order' => 'ProductDesc.id ASC'
						);
						
						$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);	
						//pr($stockData);
						$diff_SkuList = explode(':',trim($stockData[0]->ProductDesc->Diff_Skus));					
						
						//Get Diff Sku stock value under min section according to limit factor
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
						
						$params = array(
							'conditions' => array(
								'Product.product_sku' => $diff_SkuList
							),
							'fields' => array(
								//'IF(Product.current_stock_level > 2, (Product.current_stock_level -2) , 0) as FindStockThrough__LimitFactor'
								'Product.id',
								'Product.current_stock_level'
							),
							'order' => 'Product.id ASC'
						);
						
						//Get relevant data
						$getListStockValue = $this->Product->find( 'list' , $params );							
						$stockDataValue = min($getListStockValue);
						
						//Here we have found min lowest stock sku value tha means, need to put it into csv but keep safety margin
						if( $stockDataValue > 2 ):
							
							//If greater then will need to keep 2 for safety margin
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockDataValue );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							if( ($stockDataValue-2) == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
							}
							else if( ($stockDataValue-2) < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor(($stockDataValue-2)/2) );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor(($stockDataValue-2)/2) );
							}
							
						elseif( $stockDataValue <= 2 ):
							
							//If less or equal then will need to put space / blank
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockDataValue );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						
							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));
						endif;
						
					endif;
					
				endif;
				
		  $e++;$inc++;
		  endforeach;
		  
		  //File creation 		  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->applyFromArray(
		  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->setWrapText(true);
		  $objPHPExcel->getActiveSheet(0)->setTitle('Virtual Stock');
		  $objPHPExcel->getActiveSheet(0)->getStyle("A1:E1")->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet(0)
			 ->getStyle('A1:E1')
			 ->applyFromArray(
			  array(
			   'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EBE5DB')
			   )
			  )
			 );		   
		  
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/virtualStock.csv';
		  //$uploadRemote = $getBase.'app/webroot/img/stockUpdate/virtualStock.csv';
		  $uploadRemote = $getBase.'img/stockUpdate/virtualStock.csv';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		  $objWriter->save($uploadUrl);	
		  
    return $uploadRemote;       		  
	}
	
	
	public function productVirtualStockCsvGeneration_old()
	{	 
		  $this->layout = '';
		  $this->autoRender = false;	
		  
		  $getBase = Router::url('/', true);
		  		  
		  $getStockDetail = json_decode(json_encode(ProductsController::getStockDataForCsv()),0);		 		  
		  //pr($getStockDetail); exit;
		  
		  /*
		   * 
		   * Params, To setup virtual stock for upload over linnworks where it will update on each platform, 
		   * that we are using ( Amazon, Ebay , Magento etc etc )
		   * 
		   */ 
		  
		   ob_clean();    
		   App::import('Vendor', 'PHPExcel/IOFactory');
		   App::import('Vendor', 'PHPExcel');  
		   $objPHPExcel = new PHPExcel();       
		  
		   //Column Create  
		   $objPHPExcel->setActiveSheetIndex(0);
		   $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Product Title');     
		   $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Sku');
		   $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Barcode');
		   $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Original Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Rainbow Retail(Amazon)');
		   $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Storeforlife (Magento)');
		  
		   $inc = 2; $e = 0;foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue ):
				
				if( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "single" ):
					
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
					if( $getStockDetailValue->Product->AvailableStock <= 0 ):
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );	
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, 0 );
						//Set highlight full row
						$objPHPExcel->getActiveSheet(0)
						->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));					
						
						//For other subsource
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
					else:
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getStockDetailValue->Product->AvailableStock );
													
						//For other subsource
						if( $getStockDetailValue->Product->AvailableStock == 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
						}
						else if( $getStockDetailValue->Product->AvailableStock < 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						}
						else
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );
						}
					endif;
					
										
				elseif( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "bundle" ):					
					
					if( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "single" ):
										
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
		
						$params = array(
							'conditions' => array(
								'Product.product_sku' => $getStockDetailValue->ProductDesc->product_defined_skus
							),
							'fields' => array(
								'IF(Product.current_stock_level > 2, Product.current_stock_level , 0) as FindStockThrough__LimitFactor'
							),
							'order' => 'Product.id ASC'
						);
						
						$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);
						$mainSku = explode('-',$getStockDetailValue->Product->MainSku);						
						$getDivisionNumberFromBundleSku = $mainSku[2];						
						$index = 0;
						$getAvailableStockFor_Bundle_WithSameSku = $stockData[0][$index]->FindStockThrough__LimitFactor;
						
						//Set data into file 
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );
						if( $getAvailableStockFor_Bundle_WithSameSku > 2 ): 
							$getAvailableStockFor_Bundle_WithSameSku = round($getAvailableStockFor_Bundle_WithSameSku / $getDivisionNumberFromBundleSku) - 2;
							
							if( $getAvailableStockFor_Bundle_WithSameSku < 2 )
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );
							else
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );
							
							//For other subsource
							if( $getAvailableStockFor_Bundle_WithSameSku == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
							}
							else if( $getAvailableStockFor_Bundle_WithSameSku < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) );
							}	
						else:
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );	
							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						endif;	
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $stockData[0][$index]->FindStockThrough__LimitFactor );						
						
						
							
					elseif( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "multiple" ):
					
						//Set if different sku will manipulate over limit factor						
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 						
						$params = array(
							'conditions' => array(
								'ProductDesc.product_defined_skus' => $getStockDetailValue->ProductDesc->product_defined_skus
							),
							'fields' => array(
								'ProductDesc.product_defined_skus as Diff_Skus'
							),
							'order' => 'ProductDesc.id ASC'
						);
						
						$stockData = json_decode(json_encode($this->ProductDesc->find( 'all' , $params )),0);	
						$diff_SkuList = explode(':',$stockData[0]->ProductDesc->Diff_Skus);					
						
						//Get Diff Sku stock value under min section according to limit factor
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
						
						$params = array(
							'conditions' => array(
								'Product.product_sku' => $diff_SkuList
							),
							'fields' => array(
								//'IF(Product.current_stock_level > 2, (Product.current_stock_level -2) , 0) as FindStockThrough__LimitFactor'
								'Product.id',
								'Product.current_stock_level'
							),
							'order' => 'Product.id ASC'
						);
						
						//Get relevant data
						$getListStockValue = $this->Product->find( 'list' , $params );						
						$stockDataValue = min($getListStockValue);
						
						//Here we have found min lowest stock sku value tha means, need to put it into csv but keep safety margin
						if( $stockDataValue > 2 ):
							
							//If greater then will need to keep 2 for safety margin
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, ($stockDataValue-2) );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							if( ($stockDataValue-2) == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
							}
							else if( ($stockDataValue-2) < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor(($stockDataValue-2)/2) );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor(($stockDataValue-2)/2) );
							}
							
						elseif( $stockDataValue <= 2 ):
							
							//If less or equal then will need to put space / blank
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						
							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));
						endif;
						
					endif;
					
				endif;
				
		  $e++;$inc++;
		  endforeach;
		  
		  //File creation 		  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->applyFromArray(
		  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->setWrapText(true);
		  $objPHPExcel->getActiveSheet(0)->setTitle('Virtual Stock');
		  $objPHPExcel->getActiveSheet(0)->getStyle("A1:E1")->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet(0)
			 ->getStyle('A1:E1')
			 ->applyFromArray(
			  array(
			   'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EBE5DB')
			   )
			  )
			 );		   
		  
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/virtualStock.csv';
		  //$uploadRemote = $getBase.'app/webroot/img/stockUpdate/virtualStock.csv';
		  $uploadRemote = $getBase.'img/stockUpdate/virtualStock.csv';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		  $objWriter->save($uploadUrl);	
		  
    return $uploadRemote;       		  
	}
	
	public function updatePackagingIdByBarcodeSearchCheck_InPage()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel( 'BinLocation' );	
		$this->loadModel( 'CheckIn' );	
		
		/*pr($this->BinLocation->find('all')); exit;*/
		
		$barcode = $this->request->data['barcode'];
		/*************************************Code start for get globel barcode*********************************************/
		$barcode	=	$this->GlobalBarcode->getGlobalBarcode($barcode);
		/*************************************Code end for get globel barcode*********************************************/
		
		$checkSkuAndBarcode	=	$this->Product->find( 'first', array( 'conditions' => array(
														'Product.product_sku' => $this->request->data['bin_sku'],
														'ProductDesc.barcode' => $barcode
														) 
													) 
												);
		if(empty($checkSkuAndBarcode) )
		{
			echo (json_encode(array('status' => '2', 'data' => 'Please check barcode and sku.')));
			exit;
		}
		
		//pr($this->request->data); exit;
		
		//Delete Specific barcode rows
		$this->BinLocation->query( "delete from bin_locations where barcode = '{$barcode}'" );
		
		$customNewLocation = strtoupper(trim($this->request->data['customNewLocation']));
		$customStock = $this->request->data['customStock'];
		
		$newStockInput = $customStock;
		
		$product_id = $this->request->data['product_id'];	
		$descId = $this->request->data['id'];		
		$bin = $this->request->data['bin_sku'];
		$binAllocation = $this->request->data['binAllocation'];
		$poName = $this->request->data['poName'];
		$userId = $this->request->data['userId'];
		$pcName = $this->request->data['pcName'];
		$pageName = $this->request->data['pageName'];
		$currentStock = $this->request->data['stock'];
		
		//total available stock
		$totalStockNow = $currentStock + $customStock;
		
		$getStockSum = array_sum( explode(",",$this->request->data['getStockByLocation']) );
		
		//Unbinding Techniques 
		$this->ProductDesc->unbindModel( array( 'belongsTo' => array( 'Product' ) ) );
				
		$this->ProductDesc->updateAll(
			array(				
				'bin' => "'".$bin."'",				
			),
			array(
				'ProductDesc.id' => $descId
			)
		);
		
		//Save only
		$getLocation = explode( ',',$this->request->data['getLocation'] );
		$getStock = explode( ',',$this->request->data['getStockByLocation'] );
		
		if( count($getLocation) > 0 )
		{			
			$checkFlag = false;$customManipulation = 0;$stockCount = 0;$e = 0;while( $e < count( $getLocation ) )
			{
				if(  $getLocation[$e] != '' && $getStock[$e] != '' )
				{
					if( $getLocation[$e] == $customNewLocation )
					{
						$this->request->data['BinLocationPreffered']['BinLocation']['barcode'] = $barcode;//$this->request->data['barcode'];
						$this->request->data['BinLocationPreffered']['BinLocation']['bin_location'] = $getLocation[$e];
						
						if( $getStock[$e] > 0 )
							$customManipulation = $getStock[$e] + $customStock;
						else
							$customManipulation = 0 + $customStock;
						
						//$customManipulation = $getStock[$e] + $customStock;
						$this->request->data['BinLocationPreffered']['BinLocation']['stock_by_location'] = $customManipulation;
						
						$stockCount = $stockCount + $customManipulation;
						$this->BinLocation->saveAll( $this->request->data['BinLocationPreffered'] );
						$checkFlag = true;
					}
					else
					{
						$this->request->data['BinLocationPreffered']['BinLocation']['barcode'] = $barcode;//$this->request->data['barcode'];
						$this->request->data['BinLocationPreffered']['BinLocation']['bin_location'] = $getLocation[$e];						
						$this->request->data['BinLocationPreffered']['BinLocation']['stock_by_location'] = $getStock[$e];
						
						
						if( $getStock[$e] > 0 )
							$stockCount = $stockCount + $getStock[$e];
						else
							$stockCount = $stockCount + 0;
							
						//$stockCount = $stockCount + $getStock[$e];
						$this->BinLocation->saveAll( $this->request->data['BinLocationPreffered'] );
					}					
				}
			$e++;	
			}
		}
		
		if( $checkFlag == false )
		{
			//Extra for custom location storage
			$this->request->data['BinNewCustom']['BinLocation']['barcode'] = $barcode;//$this->request->data['barcode'];
			$this->request->data['BinNewCustom']['BinLocation']['bin_location'] = $customNewLocation;						
			$this->request->data['BinNewCustom']['BinLocation']['stock_by_location'] = $customStock;
			
			if( $customStock != '' || $customStock > 0 )
			{
				$stockCount = $stockCount + $customStock;
			}			
			$this->BinLocation->saveAll( $this->request->data['BinNewCustom'] );
			
			//Update Product current stock
			/*$this->Product->updateAll(
				array(
					'current_stock_level' => "'".$stockCount."'"
				),
				array(
					'Product.id' => $product_id
				)
			);*/
			
			$this->Product->updateAll(
				array(
					'current_stock_level' => "'".$totalStockNow."'"
				),
				array(
					'Product.id' => $product_id
				)
			);
			
		}
		else
		{
			//Update Product current stock
			/*$this->Product->updateAll(
				array(
					'current_stock_level' => "'".$stockCount."'"
				),
				array(
					'Product.id' => $product_id
				)
			);*/
			
			$this->Product->updateAll(
				array(
					'current_stock_level' => "'".$totalStockNow."'"
				),
				array(
					'Product.id' => $product_id
				)
			);
			
			
		}
		
		/*
		 * 
		 * CheckIn record manipulation
		 * 
		 */ 
		$checkIn['sku']					= $bin;
		$checkIn['barcode']				= $barcode;//$this->request->data['barcode'];
		$checkIn['qty_checkIn']			= $this->request->data['customStock'];
		$checkIn['po_name']				= $this->request->data['poName'];
		$checkIn['user_id']				= $this->request->data['userId'];
		$checkIn['pc_name']				= $this->request->data['pcName'];
		$checkIn['custam_page_name']	= $pageName;
		$checkIn['current_stock']		= $currentStock;
		$checkIn['bin_location']		= $customNewLocation;
		$checkIn['date']				= date('Y-m-d H:i:s');
		
		$this->CheckIn->saveAll( $checkIn );
		unset($checkIn);
		$checkIn = '';
		
		
		//Get All related data
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		$this->loadModel('BinLocation');
		
		/*$this->ProductDesc->bindModel( array( 
			'hasMany' => array(        
				'BinLocation' => array(									   
					'className' => 'BinLocation',
					'foreignKey' => 'barcode'									   
					)
				) 
			) 
		);*/
		
		//$barcode	=	$this->request->data['barcode'];
		
		$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array('ProductDesc.barcode' => $barcode)
								)
							);
		
		$productData	=	json_decode(json_encode($productdeatil,0));
		App::import('Controller', 'Cronjobs');
		$productCant = new CronjobsController();
		//$ordertype = 'CheckIn With PO-'.$this->request->data['poName'].' By '.$this->request->data['userId'];
		$ordertype = 'CheckIn';
		$productCant->storeInventoryRecord( $productData, $newStockInput, '-', $ordertype, $barcodeVal );
		
		$this->ProductDesc->unbindModel( array(
			'hasOne' => array(
				'Product'
			)
		) );
		
		$binLocation =	$this->BinLocation->find('all', array(
									'joins' => array(
										array(
											'table' => 'product_descs',
											'alias' => 'ProductDesc',
											'type' => 'INNER',
											'conditions' => array(
												'ProductDesc.barcode = BinLocation.barcode',
												'BinLocation.barcode ='.$barcode
											)
										)
									)
								)
							);
		
		$data['id'] 					= 	$productdeatil['ProductDesc']['id'];
		$data['stock'] 					= 	$productdeatil['Product']['current_stock_level'];
		$data['product_id'] 			= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 					= 	$productdeatil['Product']['product_name'];
		$data['sku'] 					= 	$productdeatil['Product']['product_sku'];
		$data['shortdescription'] 		= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 		= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 					= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 				= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 					= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 				= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 				= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 				= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 	= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 	= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		$data['bin_combined_text'] 		= 	$productdeatil['ProductDesc']['bin_combined_text'];
		
		//Get and set Bin Locations
		$binLocate = 0;
		foreach($binLocation as $binLocationIndex => $binLocationValue)
		{
			$binData[$binLocate]['id']					=	$binLocationValue['BinLocation']['id'];	
			$binData[$binLocate]['barcode']				=	$binLocationValue['BinLocation']['barcode'];	
			$binData[$binLocate]['bin_location']		=	$binLocationValue['BinLocation']['bin_location'];	
			$binData[$binLocate]['stock_by_location']	=	$binLocationValue['BinLocation']['stock_by_location'];	
		$binLocate++;
		}
		$data['binLocationArray'] = $binData;
		
		$imageUrl	=	Router::url('/', true).'img/product/';
		
		$i = 0;
		foreach($productdeatil['ProductImage'] as $image)
		{
			if($image['image_name'] != '')
			{
				$imagedata[$i]['imageid']		=	$image['id'];
				$imagedata[$i]['imagename']		=	$imageUrl.$image['image_name'];
				$imagedata[$i]['selectedimage']	=	$image['selected_image'];
			}
			$i++;
		}
		
		$j = 0;
		foreach( $productdeatil['ProductLocation'] as $productLocation)
		{
			$productLocation['warehouse_id'];
			$locationData[$j]['binrackid']	=	$productLocation['bin_rack_address'];
			$warehouseDetail	=	$this->Warehouse->find('first', array('conditions' => array('Warehouse.id' => $productLocation['warehouse_id'])));
			$locationData[$j]['warehousename']	=	$warehouseDetail['Warehouse']['warehouse_name'];
			$locationData[$j]['city']			=	$warehouseDetail['WarehouseDesc']['city_id'];
			$locationData[$j]['warehousetype']	=	$warehouseDetail['WarehouseDesc']['warehouse_type'];
			$j++;
		}
		$data['status'] 				= 'success';		
		echo (json_encode(array('status' => '1', 'data' => $data)));		
		exit;	 
	}
	
	/*Array
	(
		[barcode] => 9780007233038
		[variant_envelope_id] => 6
		[variant_envelope_name] => Poly Bag Size:165x230mm
		[getLength] => 125
		[getWidth] => 31
		[getHeight] => 121
		[getWeight] => 10
		[product_id] => 11
		[id] => 11
	)*/
	
	public function save_editproduct()
	{
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'PurchaseOrder' );
		
		
		if(!empty($this->request->data))
		{
			$storeIds	=	implode(',', $this->request->data['Product']['store_name']);
			$this->request->data['Product']['store_name'] = $storeIds;
			$checkunique	=	$this->Product->find( 'first', array('conditions' => array('Product.id !=' => $this->request->data['Product']['id'], 'ProductDesc.barcode' => $this->request->data['ProductDesc']['barcode'] ) ) );
			$checkSkuUnique	=	$this->Product->find( 'first', array('conditions' => array('Product.id !=' => $this->request->data['Product']['id'], 'Product.product_sku' => $this->request->data['Product']['product_sku'] ) ) );
			
			$purchaseSkuList	=	$this->PurchaseOrder->find( 'list', array(
											'conditions' => array( 'PurchaseOrder.purchase_sku' => $this->request->data['Product']['product_sku'] ),
											'fields' => array( 'PurchaseOrder.id' )
											) );
			
			if( empty( $checkSkuUnique ) )
			{
				if( !empty($checkunique))
				{
					$this->Session->setFlash('Barcode already Exist.', 'flash_danger');
					$this->redirect($this->referer());
				}
				else
				{
					$result = $this->Product->saveAll( $this->request->data, array('validate' => false)); 
					if( count( $purchaseSkuList ) > 0 )
					{
						// Update purchase sku barcode when update original sku barcode
						$this->PurchaseOrder->updateAll( 
												array( 'PurchaseOrder.purchase_barcode' => "'".$this->request->data['ProductDesc']['barcode']."'" ), 
												array( 'PurchaseOrder.id' => $purchaseSkuList ) 
												);
					}
					if($result)
					{
						$this->Session->setFlash('Update Successfully', 'flash_success');
						$this->redirect( array( 'controller' => 'showAllProduct' ) );
					}
				}
			}
			else
			{
				$this->Session->setFlash('Sku already Exist.', 'flash_danger');
				$this->redirect($this->referer());
			}
		}
	}
	
	//BIN PACKING 
	public function getDim()
	{
		
		$data = array(
            'bins' => array(
                            array(
                                'w' => 150,
                                'h' => 150,
                                'd' => 10, 
                                'max_wg' => 0,
                                'id' => 0
                                )
                            ),
            'items' => array(
                            array(
                                'w' => 125,
                                'h' => 112,
                                'd' => 142,
                                'q' => 1,
                                'vr' => 1,
                                'wg' => 0,
                                'id' => 0
                                ),
                            array(
                                'w' => 118,
                                'h' => 80,
                                'd' => 140,
                                'q' => 1,
                                'vr' => 1,
                                'wg' => 0,
                                'id' => 1
                            )
                        ),
            'username' => 'ag.ashish', 
            'api_key' => 'eef4aec27e3748d73bc9ae55dc1ec27e',
            'params' => array(
                            'images_background_color' => '255,255,255',
                            'images_bin_border_color' => '59,59,59',
                            'images_bin_fill_color' => '230,230,230',
                            'images_item_border_color' => '214,79,79',
                            'images_item_fill_color' => '177,14,14',
                            'images_item_back_border_color' => '215,103,103',
                            'images_sbs_last_item_fill_color' => '99,93,93',
                            'images_sbs_last_item_border_color' => '145,133,133',
                            'images_width' => 100,
                            'images_height' => 100,
                            'images_source' => 'file',
                            'images_sbs' => 1,
                            'stats' => 1,
                            'item_coordinates' => 1,
                            'images_complete' => 1,
                            'images_separated' => 1
                            )
            );
			$query = json_encode($data);
			
			$resp = file_get_contents("http://eu.api.3dbinpacking.com/packer/pack?query={$query}");
		
			$response = json_decode($resp,true);
			// display errors
			if(isset($response['response']['errors'])){
			  foreach($response['response']['errors'] as $error){
				echo $error['message'].'<br>';
			  }
			}
			// display data
			if( $response['response']['status'] > -1 ){
			  $b_packed= $response['response']['bins_packed'];
			  foreach ($b_packed as $bin){
				  echo "<h2>Bin id:{$bin['bin_data']['id']}</h2> 
					<p> {$bin['bin_data']['w']} x {$bin['bin_data']['h']} x {$bin['bin_data']['d']}</p>
					<p> Weight:{$bin['bin_data']['weight']}</p>
					<p> Used weight:{$bin['bin_data']['used_weight']}</p>  
					<img src='{$bin['image_complete']}'>
					<h2>Items packed in this bin:</h2>";
				  $items = $bin['items'];
				  echo '<table style="text-align: center">
					<tr><th>Item id</th>
					<th>Item dimensions</th>
					<th>Item weight</th>
					<th>Separated item</th>
					<th>Step by step</th></tr>';
				  foreach ( $items as $item){
				  echo "<tr><td>{$item['id']}</td>       
						<td> ".$item['w']." x ".$item['h']." x ".$bin['bin_data']['d']."</td>
						<td> ".$item['wg']."</td>
						<td><img src='".$item['image_separated']."'></td>
						<td><img src='".$item['image_sbs']."'></td></tr>";
				  }    
				  echo '</table>';
				  echo '<hr>'; 
				   
			  }
			}
			
			exit;	
		
	}
        
        public function orderAdjust()
        {
            $this->layout = 'index';
        }
	
        public function removeOrderById()
        {
            
            $this->loadModel( 'OpenOrder' );
            $this->loadModel( 'MergeUpdate' );
            $this->loadModel( 'ScanOrder' );
            
            $orderId = $this->request->data['orderid'];
            
            $ord = explode('-',$orderId);
            
            $ord1 = $ord[0];
            $ord2 = $ord[1];
            
            //From OpenOrder table through num_order_id
            $this->OpenOrder->query( "Delete from open_orders where num_order_id = '{$ord1}'" );
           
            //From MergeUpdate table through num_order_id
            $this->OpenOrder->query( "Delete from merge_updates where product_order_id_identify = '{$orderId}'" );
            
            //From scanorder table through num_order_id
            $this->OpenOrder->query( "Delete from scan_orders where split_order_id = '{$orderId}'" );
            
            echo "1";
            exit;
        }

	public function getSkuQuantity( $sku = null )
		{
			$this->loadModel( 'ScanOrders' );
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$getSkuDetails	=	$this->ScanOrders->find( 'all', array( 'conditions' => array( 'ScanOrders.sku' => $sku, 'ScanOrders.quantity !=' => 'ScanOrders.update_quantity') ) );
			$j = 0;
			foreach( $getSkuDetails  as $getSkuDetail )
			{
				$getmergeOrders	=	$this->MergeUpdate->find( 'all', array( 'conditions' => array( 'MergeUpdate.product_order_id_identify' =>  $getSkuDetail['ScanOrders']['split_order_id'], 'MergeUpdate.status' => '0' ) ) );
				if( count($getmergeOrders) > 0   )
				{ 
					$i = 0;
					foreach( $getmergeOrders as $getmergeOrder )
					{
							$getOpenOrders	=	$this->OpenOrder->find( 'first', array( 'conditions' => array( 'OpenOrder.num_order_id' =>  $getmergeOrder['MergeUpdate']['order_id'], 'OpenOrder.status' => '0'), 'fields' => array( 'OpenOrder.sub_source, OpenOrder.num_order_id' ) ) );
							if( count( $getOpenOrders ) > 0 )
							{
								$data[$j]['subsource']['quantity']		=	 	$getSkuDetail['ScanOrders']['quantity'];
								$data[$j]['subsource']['sub_source']	= 		$getOpenOrders['OpenOrder']['sub_source'];
								$data[$j]['subsource']['sku']			= 		$getSkuDetail['ScanOrders']['sku'];
								$data[$j]['subsource']['order_id']		= 		$getOpenOrders['OpenOrder']['num_order_id'];
								$i++;
							}
					}
					$j++;
				}
			}
			return $data;
		}
		
		
		public function getSearchResult()
		{
			$this->layout = '';
			$this->autoRender = false;
			
			$this->loadModel( 'BinLocation' );	
			$this->loadModel( 'UnprepareOrder' );	
			/*$binLocation	=	$this->BinLocation->find('all');
			$location 		= 	array();
			foreach($binLocation as $data)
			{
				 $location[$data['BinLocation']['barcode']][] = array('bin_location' => $data['BinLocation']['bin_location'],
																		'stock_by_location'=> $data['BinLocation']['stock_by_location']);
				
			}		
			$this->set('BinLocation',$location);*/
			
			$unpOrders	=	$this->UnprepareOrder->find('all');
			$unp_data 	= 	array();
			foreach($unpOrders as $data){
			
				foreach( unserialize($data['UnprepareOrder']['items']) as $val){
								
					$_sku = explode( '-' , $val->SKU);
					if( count($_sku ) == 3 ) // For Bundle (Single)
					{
						$sku = 'S-'.$_sku[1];
						$qty = $val->Quantity * ($_sku[2]);
						$unp_data[$sku][] = array('quantity'=>$qty, 'order_id'=>$val->OrderId);
					}
					else if( count( $_sku ) > 3 ) // For Bundle (Bundle)
					{						
						$inc = 1;	$ij = 0;
						while( $ij < count($_sku)-2 )
						{						
							$sku = 'S-'.$_sku[$inc];
												
							$inc++;
							$ij++;
							$unp_data[$sku][] = array('quantity'=>$val->Quantity, 'order_id'=>$val->OrderId);
						 
						}
					}else{				
						$unp_data[$val->SKU][] = array('quantity'=>$val->Quantity, 'order_id'=>$val->OrderId);
					}
				}			
			}		
			$this->set('unp_data',$unp_data);
			
			$searchString		=	trim($this->request->data['searchKey']);
			/*************************************Apply code for Global Barcode*********************************************/
			$searchbar		=	$this->GlobalBarcode->getGlobalBarcode($searchString);
			/*************************************Apply code for Global Barcode*********************************************/
			if(!empty($searchbar)) { 
				$searchText = $searchbar;
			} else {
				$searchText = $searchString;
			}
			$getBarcode = $this->BinLocation->find('list', array( 'conditions' => array('BinLocation.bin_location' => $searchText ),'fields' => array( 'BinLocation.barcode' ) ) );
			if(count($getBarcode) > 0)
			{
				$productdeatil	=	$this->Product->find('all', array(								
					'conditions' => array( 'ProductDesc.barcode IN' => $getBarcode ),
					'fields' => array( 'DISTINCT Product.id','Product.*','ProductDesc.*' ) ) );
			
			} 
			else 
			{
				$productdeatil	=	$this->Product->find('all', array(								
						'conditions' => array(
								'OR' => array(
									array('ProductDesc.barcode' => $searchText),
									array('Product.product_sku LIKE' => '%'.$searchText.'%'),
									array('Product.product_name LIKE' => '%'.$searchText.'%'),
									array('ProductDesc.qr_code LIKE' => '%'.$searchText.'%')
								)
							),
						'fields' => array( 'DISTINCT Product.id','Product.*','ProductDesc.*' ) ) );
			}
			
			$this->set( 'productdeatil', $productdeatil );
			$this->render( 'productSerchResult' );
		}
		
		
		public function getSearchResult010617()
		{
			$this->layout = '';
			$this->autoRender = false;
			
			
			$this->loadModel( 'BinLocation' );		
			$binLocation	=	$this->BinLocation->find('all');
			$location 		= 	array();
			foreach($binLocation as $data)
			{
				 $location[$data['BinLocation']['barcode']][] = array('bin_location' => $data['BinLocation']['bin_location'],
																		'stock_by_location'=> $data['BinLocation']['stock_by_location']);
				
			}		
			$this->set('BinLocation',$location);
			
			$searchText		=	trim($this->request->data['searchKey']); 
			$getBarcode = $this->BinLocation->find('list', array( 'conditions' => array('BinLocation.bin_location' => $searchText ),'fields' => array( 'BinLocation.barcode' ) ) );
			if(count($getBarcode) > 0)
			{
				$productdeatil	=	$this->Product->find('all', array(								
					'conditions' => array( 'ProductDesc.barcode IN' => $getBarcode ),
					'fields' => array( 'DISTINCT Product.id','Product.*','ProductDesc.*' ) ) );
			
			} 
			else 
			{
				$productdeatil	=	$this->Product->find('all', array(								
						'conditions' => array(
								'OR' => array(
										array('ProductDesc.barcode' => $searchText),
										array('Product.product_sku LIKE' => '%'.$searchText.'%'),
										array('Product.product_name LIKE' => '%'.$searchText.'%'),
										array('ProductDesc.qr_code LIKE' => '%'.$searchText.'%')
								)
							),
						'fields' => array( 'DISTINCT Product.id','Product.*','ProductDesc.*' ) ) );
			}
			
			$this->set( 'productdeatil', $productdeatil );
			$this->render( 'productSerchResult' );
			
			
			
			
		}
		
		
		public function getSearchResult_old_060416()
		{
			$this->layout = '';
			$this->autoRender = false;
			$searchText		=	trim($this->request->data['searchKey']);
			
			$productdeatil	=	$this->Product->find('all', array(								
									'conditions' => array(
											'OR' => array(
													array('ProductDesc.barcode' => $searchText),
													array('Product.product_sku LIKE' => '%'.$searchText.'%'),
													array('Product.product_name LIKE' => '%'.$searchText.'%')
											)
										)
									)
								);
			$this->set( 'productdeatil', $productdeatil );
			$this->render( 'productSerchResult' );
			
		}
		
		public function batchProcessed_try()
		{
			
			$this->layout = '';
			$this->autoRender = false;	
		  			
			$this->loadModel( 'OpenOrder' );
			
			/*$today = date( 'Y-m-d 00:00:00' );
			
			$params = array( 'conditions' => array( 'OpenOrder.date >=' => $today , 'OpenOrder.status' => 1 ) ,'fields' => array('OpenOrder.num_order_id', 'OpenOrder.date'));
			
			$processedOrder = $this->OpenOrder->find( 'all', $params );*/
			
			//$params = array( 'conditions' => array( 'OpenOrder.date >=' => $today , 'OpenOrder.status' => 1 ) ,'fields' => array('OpenOrder.num_order_id', 'OpenOrder.date'));
			
			$getFrom = date('Y-m-d',strtotime('2016-06-03'));
			$getEnd = date('Y-m-d',strtotime('2016-06-05'. "+1 days"));
			
			$params = array( 'conditions' => array( 
											'OpenOrder.date >=' => $getFrom,
											'OpenOrder.date <=' => $getEnd, 
											'OpenOrder.status' => 1 ) ,
							'fields' => array(
											'OpenOrder.num_order_id', 
											'OpenOrder.date'
											)
										);
			
			$processedOrder = $this->OpenOrder->find( 'all', $params );
			
		    $getBase = Router::url('/', true);
		  		  
		    $getStockDetail = json_decode(json_encode($processedOrder),0);		 		  
		    //pr($getStockDetail); exit;
		  
			/*
			 * 
			 * Params, To setup virtual stock for upload over linnworks where it will update on each platform, 
			 * that we are using ( Amazon, Ebay , Magento etc etc )
			 * 
			 */ 
		  
			ob_clean();    
			App::import('Vendor', 'PHPExcel/IOFactory');
			App::import('Vendor', 'PHPExcel');  
			$objPHPExcel = new PHPExcel();       
		  
			//Column Create  
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Order');     
			$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Processed');
			$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ProcessedDate');
			
			$inc = 2; $e = 0;foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue )
			{
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getStockDetailValue->OpenOrder->num_order_id );
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, 'TRUE' );
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getStockDetailValue->OpenOrder->date );
				$inc++;
			}
			
			  //File creation 		  
			  $objPHPExcel->getActiveSheet(0)->getStyle('A1:C1')->getAlignment()->applyFromArray(
			  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
			  $objPHPExcel->getActiveSheet(0)->getStyle('A1:C1')->getAlignment()->setWrapText(true);
			  $objPHPExcel->getActiveSheet(0)->setTitle('Batch Process Orders');
			  $objPHPExcel->getActiveSheet(0)->getStyle("A1:C1")->getFont()->setBold(true);
			  $objPHPExcel->getActiveSheet(0)
				 ->getStyle('A1:C1')
				 ->applyFromArray(
				  array(
				   'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'EBE5DB')
				   )
				  )
				 );		   
			  
			  $uploadUrl = WWW_ROOT .'img/stockUpdate/batchProcessOrderList.csv';
			  $uploadRemote = $getBase.'app/webroot/img/stockUpdate/batchProcessOrderList.csv';
			  echo $uploadRemote = $getBase.'img/stockUpdate/batchProcessOrderList.csv';
			  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
			  $objWriter->save($uploadUrl);	
			  exit;
		}
		
		public function batchProcessed()
		{
			
			$this->layout = '';
			$this->autoRender = false;	
		  			
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeUpdate' );
			
			//$today = date( '2016-06-03 00:00:00' );
			//$today = date( '2017-02-04 00:00:00' );
			$today = date( 'Y-m-d 00:00:00' );
			
			
			$getFrom= date('Y-m-d', strtotime($today .' -7 day'));				
			$getEnd = date('Y-m-d',strtotime($today .'+1 day'));
			
			$params = array( 'conditions' => array( 'OpenOrder.process_date BETWEEN ? AND ?' => array($getFrom, $getEnd), 'OpenOrder.status' => 1 ) ,'fields' => array('OpenOrder.num_order_id', 'OpenOrder.date','OpenOrder.process_date','destination'));
			
			$processedOrder = $this->OpenOrder->find( 'all', $params );
			
			//$params = array( 'conditions' => array( 'OpenOrder.date >=' => $today , 'OpenOrder.status' => 1 ) ,'fields' => array('OpenOrder.num_order_id', 'OpenOrder.date'));
			
			//$getFrom = date('Y-m-d',strtotime($getData['getFrom']));
			//$getEnd = date('Y-m-d',strtotime($getData['getEnd']. "+1 days"));
			
			/*$params = array( 'conditions' => array( 
											'OpenOrder.date >=' => '2016-05-05',
											'OpenOrder.date <=' => $getEnd, 
											'OpenOrder.status' => 1 ) ,
							'fields' => array(
											'OpenOrder.num_order_id', 
											'OpenOrder.date'
											)
										);
			
			$processedOrder = $this->OpenOrder->find( 'all', $params );*/
						
		    $getBase = Router::url('/', true);
		  		  
		    $getStockDetail = json_decode(json_encode($processedOrder),0);		 		  
		    //pr($getStockDetail); exit;
		  
			/*
			 * 
			 * Params, To setup virtual stock for upload over linnworks where it will update on each platform, 
			 * that we are using ( Amazon, Ebay , Magento etc etc )
			 * 
			 */ 
		  
			ob_clean();    
			App::import('Vendor', 'PHPExcel/IOFactory');
			App::import('Vendor', 'PHPExcel');  
			$objPHPExcel = new PHPExcel();       
		  
			//Column Create  
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Order');     
			$objPHPExcel->getActiveSheet()->setCellValue('B1', 'ProcessedDate');
			$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Tracking Number');
			$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Service Provider');
			
			$inc = 2; $e = 0;foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue )
			{
				$getTrackID = $this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.order_id' =>  $getStockDetailValue->OpenOrder->num_order_id)));
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getStockDetailValue->OpenOrder->num_order_id );
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $getStockDetailValue->OpenOrder->process_date );
				$trackingno	=	$getTrackID['MergeUpdate']['reg_post_number'];
				$track_id	=	$getTrackID['MergeUpdate']['track_id'];
				$brt_track_id	=	str_replace('-','',$getTrackID['MergeUpdate']['product_order_id_identify']);
				//pr($getTrackID);
				
				/*if($getStockDetailValue->OpenOrder->destination == 'Italy')
				{
					$trackingno = 'TR-'.$trackingno;
				}*/
				
				if($getTrackID['MergeUpdate']['service_provider'] == 'PostNL' || $getTrackID['MergeUpdate']['service_provider'] == 'DHL')
				{
					if($getTrackID['MergeUpdate']['postal_service'] == 'Tracked' || $getTrackID['MergeUpdate']['postal_service'] == 'Express')
						{
							if($getStockDetailValue->OpenOrder->destination != 'Italy')
							{
								if($getTrackID['MergeUpdate']['service_provider'] == 'DHL')
								{
									$service_provider = 'DHL';
								} else if($getTrackID['MergeUpdate']['service_provider'] == 'PostNL'){
									$service_provider = 'Netherland Post';
								} else {
									$service_provider = '';
								}
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $trackingno );
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $service_provider );
							}
							else if( $getTrackID['MergeUpdate']['delevery_country'] == 'Italy' && $getTrackID['MergeUpdate']['postal_service'] == 'Tracked' && $getTrackID['MergeUpdate']['track_id'] != '' )
							{
								$service_provider = 'BRT';
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $brt_track_id );
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $service_provider );
							}
						}
					
				}
				else if( $getTrackID['MergeUpdate']['delevery_country'] == 'United Kingdom' && $getTrackID['MergeUpdate']['postal_service'] == 'Express' && $getTrackID['MergeUpdate']['track_id'] != '' )
				{
					$service_provider = 'Royal Mail';
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $track_id );
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $service_provider );
				}
				
				$inc++;
			}
			
			  //File creation 		  
			  $objPHPExcel->getActiveSheet(0)->getStyle('A1:C1')->getAlignment()->applyFromArray(
			  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
			  $objPHPExcel->getActiveSheet(0)->getStyle('A1:C1')->getAlignment()->setWrapText(true);
			  $objPHPExcel->getActiveSheet(0)->setTitle('Batch Process Orders');
			  $objPHPExcel->getActiveSheet(0)->getStyle("A1:C1")->getFont()->setBold(true);
			  $objPHPExcel->getActiveSheet(0)
				 ->getStyle('A1:C1')
				 ->applyFromArray(
				  array(
				   'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'EBE5DB')
				   )
				  )
				 );		   
			  
			  $uploadUrl = WWW_ROOT .'img/stockUpdate/batchProcessOrderList.csv';
			  echo $uploadRemote = $getBase.'app/webroot/img/stockUpdate/batchProcessOrderList.csv';
			  $uploadRemote = $getBase.'img/stockUpdate/batchProcessOrderList.csv';
			  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
			  $objWriter->save($uploadUrl);	
			  exit;
		}
		
		
		public function sellingReport()
		{
			$this->layout = '';
			$this->autoRender = false;
			$this->loadModel( 'ScanOrder' );
			$toDayDate	=	date( 'Y-m-d' );
			
			App::import('Vendor', 'PHPExcel/IOFactory');
			App::import('Vendor', 'PHPExcel');  
			$objPHPExcel = new PHPExcel();     
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'SKU');     
			$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Barcode');
			$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Quantity');
			$todaysSkus	=	$this->ScanOrder->find( 'all', array( 
									'fields' => array( 'sum(ScanOrder.quantity)   AS Quantity' , 'ScanOrder.id, ScanOrder.split_order_id, ScanOrder.sku, ScanOrder.barcode'),
									'group' => 'ScanOrder.sku',
									'order' => 'Quantity DESC'
									 )
								);
			$inc = 2;
			foreach( $todaysSkus as $todaysSku )
			{
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $todaysSku['ScanOrder']['sku']);     
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $todaysSku['ScanOrder']['barcode']);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $todaysSku[0]['Quantity']);
				$inc++;
			}
			
			$objPHPExcel->getActiveSheet(0);
			$getBase = Router::url('/', true);
			$uploadUrl = WWW_ROOT .'img/daily_report/TotalSellReport.csv';
			$uploadRemote = $getBase.'img/daily_report/TotalSellReport.csv';
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
			$objWriter->save($uploadUrl);
			$file = 'TotalSellReport.csv';
			
			$contenttype = "application/force-download";
			header("Content-Type: " . $contenttype);
			header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
			readfile($uploadRemote);
			exit;
		}
		
		/*
         * 
         * Custom process
         * 
         * 
         */ 
		public function customProcess()
		{
			$this->layout = 'index';
		}
		
		
		
		public function globalSearch()
		{
			
			$this->layout = '';
			$this->autoRender = false;
			
			$this->render( '/Linnworksapis/global_search' );
			
		}
		
		//For oversells
		public function getOverSellOrderList()
		{
			
			/* for view of only order */
			$this->layout = '';
			$this->autoRender = false;
			
			$this->loadModel('Order');
			$this->loadModel('Item');
		
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeOrder' );
			$this->loadModel( 'MergeUpdate' );
			
			$param = array(
				'conditions' => array(
					'MergeUpdate.status' => '0',
					'MergeUpdate.out_sell_marker' => '3'
				)
			);		
			
			$getdetails	=	$this->MergeUpdate->find('all', $param);
			if( count($getdetails) > 0 )
			{
				
				$result = json_decode(json_encode($getdetails),0);
				$this->set( 'result', $result );
				
				//Get Over sells order list			
				$this->render( '/Linnworksapis/over_sell' );	
				
			}
			else
			{
				$result = 0;
				$this->set( 'result', $result );
				
				//Get Over sells order list			
				$this->render( '/Linnworksapis/over_sell' );
				
			}
			
			
		}
		
		public function getPostalName( $orderId = null )
		{
			$this->loadModel( 'OpenOrder' );
			
			$param = array(
				'conditions' => array(
					'OpenOrder.num_order_id' => $orderId
				),
				'fields' => array(
					'OpenOrder.num_order_id',
					'OpenOrder.general_info',
					'OpenOrder.shipping_info',
					'OpenOrder.customer_info',
					'OpenOrder.totals_info',
					'OpenOrder.sub_source'
				)
			);
			
			$result = json_decode(json_encode($this->OpenOrder->find( 'first', $param )),0);
		return $result;	
		}
		
		public function test()
		{
			echo "Hello IN"; exit;
		}
		
		/*
		 * 
		 * Params, Set batch cancel orders
		 * 
		 */ 
		 
		 public function setCancelOrderStatus()
		{
			
			$this->layout = '';
			$this->autoRender = false;
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'Product' );
			$this->loadModel( 'BinLocation' );
			$id = $this->request->data['id'];
			
			//$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 2 ) , array( 'MergeUpdate.order_id' => $id ) );
			//$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 2 , 'OpenOrder.cancel_status' => 5 ) , array( 'OpenOrder.num_order_id' => $id ) );
			$orderItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.order_id' => $id)));
			//pr( $orderItems );
			$j = 0;
			foreach( $orderItems as $getItem )
			{
				$mergeUpdateOrderID = $getItem['MergeUpdate']['product_order_id_identify'];
				$outerCheckForPickLIst = $getItem['MergeUpdate']['pick_list_status'];
			    $splitID = $getItem['MergeUpdate']['product_order_id_identify'];
			    $skus	=	 explode( ',', $getItem['MergeUpdate']['sku']);
			    $skuLocationData[$j]['split_order_id']	=	$mergeUpdateOrderID;
			    $k = 0;
			    foreach($skus as $sku)
				   {
					   $newSku				=	explode( 'XS-', $sku);
					   $sku 			=	'S-'.$newSku[1];
					   $getProductBarcode	=	$this->Product->find('first', 
																array( 
																	'conditions' => array( 'Product.product_sku' => $sku ),
																	'fields' => array('ProductDesc.barcode') 
																)
															);
															
						$skuBarcode			=		$getProductBarcode['ProductDesc']['barcode'];
						$getBinLocations 	= 		$this->BinLocation->find( 'all', 
																	array( 
																		'conditions' => array('BinLocation.barcode' => $skuBarcode) 
																		 ) 
																	);
						
						$skuLocationData[$j]['product_sku']		=	$sku;
						$skuLocationData[$j]['splitorder_id']	=	$splitID;
						$skuLocationData[$j]['product_barcode']	=	$skuBarcode;
						$skuLocationData[$j]['sku_qty']			=	$newSku[0];
						$i = 0;
						
						foreach( $getBinLocations as $getBinLocation )
						{
							$skuLocationData[$j]['sku_loacation'][$i]	=	$getBinLocation['BinLocation']['bin_location'];
							$skuLocationData[$j]['stock_by_location'][$i]	=	$getBinLocation['BinLocation']['stock_by_location'];
							$i++;
						}
					    $j++;
				   }
				  $k++;
			}
			$this->set( 'skuLocationData', $skuLocationData );
			$this->set( 'orderID', $id );
			
			$this->render( '/Linnworksapis/show_location_result' );
			//exit;
		}
		 
		 	
		/*public function setCancelOrderStatus_old()
		{
			
			$this->layout = '';
			$this->autoRender = false;
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'Product' );
			$this->loadModel( 'BinLocation' );
			$id = $this->request->data['id'];
			
			$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 2 ) , array( 'MergeUpdate.order_id' => $id ) );
			$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 2 , 'OpenOrder.cancel_status' => 5 ) , array( 'OpenOrder.num_order_id' => $id ) );
			$orderItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.order_id' => $id)));
			
			foreach( $orderItems as $getItem )
			{
				$outerCheckForPickLIst = $getItem['MergeUpdate']['pick_list_status'];
			    $splitID = $getItem['MergeUpdate']['product_order_id_identify'];
			    $skus	=	 explode( ',', $getItem['MergeUpdate']['sku']);
				foreach($skus as $sku)
				   {
					   $newSku				=	 explode( 'XS-', $sku);
					   $sku 					=	'S-'.$newSku[1];
					   $getProductBarcode	=	$this->Product->find('first', 
																array( 
																	'conditions' => array( 'Product.product_sku' => $sku ),
																	'fields' => array('ProductDesc.barcode') 
																)
															);
						$skuBarcode	=	$getProductBarcode['ProductDesc']['barcode'];
						$getBinLocation = $this->BinLocation->find( 'all', array( 'conditions' => array('BinLocation.barcode' => $skuBarcode) ) );
						pr( $getBinLocation );
					   //pr( $newSku );
					   $Quantity 			=	$newSku[0];
					   $sku 				=	'S-'.$newSku[1];
					   $product				=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
					   $currentStickLevel	=	$product['Product']['current_stock_level'];
					   $qtyForUpdate		=	(int)$Quantity + (int)$currentStickLevel;
					   
				   }
			    
			}
			
			
			echo "1"; 
			exit;
		}*/
		
		/*
		 * 
		 * Params, Lock orders
		 * 
		 */ 	
		public function lockOrderNotePopup()
		{
			$this->layout = '';
			$this->autoRander = false;
			$order_id = trim($this->request->data['id']);
			$this->set('orderid', $order_id);
			$this->render( 'lockPopup' );
		}	
		public function unlockOrderNotePopup()
		{
			$this->layout = '';
			$this->autoRander = false;
			$order_id = trim($this->request->data['id']);
			$this->set('orderid', $order_id);
			$this->render( 'unlockPopup' );
		}
		
		public function setOrderMarked()
		{
			
			$this->layout = '';
			$this->autoRender = false;
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'OrderNote' );
			$id = $this->request->data['id'];
			$lock_note = $this->request->data['lockNote'];
			
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			
			$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 3 ) , array( 'MergeUpdate.order_id' => $id ) );
			$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 3 ) , array( 'OpenOrder.num_order_id' => $id ) );
			
			$noteDate['order_id'] = $id;
			$noteDate['note'] = $lock_note;
			$noteDate['type'] = 'Lock';
			$noteDate['user'] = $firstName.' '.$lastName;
			$noteDate['date'] = date('Y-m-d H:i:s');
			$this->OrderNote->saveAll( $noteDate );
			echo "1"; exit;
		}
		
		/*public function getallEuorders()
		{
			$this->layout = '';
			$this->autoRender = false;
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'OrderNote' );
			$getall = $this->MergeUpdate->find( 'all', array( 'conditions' => array( 'delevery_country != ' => 'United Kingdom', 'status' => 0 ), 'fields' => array('order_id') ) );
			pr($getall);
		}*/
				
		public function setOrderMarked_061417()
		{
			
			$this->layout = '';
			$this->autoRender = false;
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$id = $this->request->data['id'];
			
			$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 3 ) , array( 'MergeUpdate.order_id' => $id ) );
			$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 3 ) , array( 'OpenOrder.num_order_id' => $id ) );
			
			echo "1"; exit;
		}
		
		public function setHeldCancelOrderMarked()
		{
			
			$this->layout = '';
			$this->autoRender = false;
		
			$this->loadModel( 'BinLocation' );
			$this->loadModel( 'Product' );
			$this->loadModel( 'CancelSku' );
			$this->loadModel( 'OrderLocation' );
			$this->loadModel( 'CheckIn' );
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'OrderNote' );
			$this->loadMOdel('InkOrderLocation' ); 
			
			
			$order_id = trim($this->request->data['id']);
			$cancel_note = trim($this->request->data['cancelHeldNote']);
			
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
			$username = $firstName.' '.$lastName;
			
			$order_details	= $this->OrderLocation->find('all', array('conditions' => array('OrderLocation.order_id' => $order_id)));
			
			$count = 0; $partial_fulfillment = 0;
			if($order_id){
				$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 2 ) , array( 'MergeUpdate.order_id' => $order_id ) );
				$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 2 , 'OpenOrder.cancel_status' => 5 ) , array( 'OpenOrder.num_order_id' => $order_id ) );
				$count++; 
			}
			
			/*--------------Ink Order Cancel---------------*/	
			$order_ink = $this->InkOrderLocation->find( 'first', array( 'conditions' => array( 'order_id' => $order_id,'status' => 'active' ) ) );
			
			if(count($order_ink) > 0){ 
			
				foreach($order_ink as $val)
				{
 					$barcode = $val['InkOrderLocation']['barcode'];
					$sku	 = $val['InkOrderLocation']['sku'];
					$ink_sku = $val['InkOrderLocation']['ink_sku'];
					$location = $val['InkOrderLocation']['bin_location'];
					$quantity = $val['InkOrderLocation']['quantity'];
					$qty_bin  = $val['InkOrderLocation']['available_qty_bin'];	
					$po_id    = '';
					if(isset(explode(",",$val['InkOrderLocation']['po_id'])[0])) {					
					$po_id	 	=  explode(",",$val['InkOrderLocation']['po_id'])[0];
					}
					$getBinLocation	=	$this->BinLocation->find('first', array( 
																'conditions' => array(
																		'BinLocation.barcode' => $barcode,
																		'BinLocation.bin_location' => $location),
																		));
					$locationCurrentQty = 0;
					$totallocationQty   = 0;
					if(count($getBinLocation) > 0){
						$locationCurrentQty = $getBinLocation['BinLocation']['stock_by_location'];
						$totallocationQty = (int)$locationCurrentQty + (int)$qty_bin;
						$this->BinLocation->updateAll(
													array('BinLocation.stock_by_location' => $totallocationQty),
													array('BinLocation.bin_location' => $location,'BinLocation.barcode' => $barcode)
												  );
												  
						$this->CheckIn->updateAll( array('CheckIn.qty_checkIn' => "CheckIn.qty_checkIn + $qty_bin",'CheckIn.selling_qty' => "CheckIn.selling_qty - $qty_bin"),array('CheckIn.id' => $po_id));	
					}							  
					$getCurrentQty = $this->Product->find('first', array( 
																	'conditions' => array( 'Product.product_sku' => $ink_sku ),
																	'fields' => array( 'Product.current_stock_level' )
																	 ) );
					$currentQty		=	$getCurrentQty['Product']['current_stock_level'];					
					$totalcurrentQty =  (int)$currentQty + (int)$qty_bin ;
					
					$this->Product->updateAll(
												array('Product.current_stock_level' => $totalcurrentQty),
												array('Product.product_sku' => $ink_sku)
											  );
											  
					$cancelData['order_id']				= $order_id;
					$cancelData['sku']					= $ink_sku;	
					$cancelData['barcode']				= $barcode;	
					$cancelData['bin_location']			= $location;	
					$cancelData['qty']					= $qty_bin;	
					$cancelData['current_qty']			= $currentQty;
					$cancelData['after_qty']			= $totalcurrentQty;	
					$cancelData['current_location_qty'] = $locationCurrentQty;
					$cancelData['after_location_qty']	= $totallocationQty;
					$cancelData['user_name']			= $firstName.' '.$lastName;
					$cancelData['date_timef']			= date( 'Y-m-d H:i:s' );
					//pr($cancelData);
					$this->CancelSku->saveAll( $cancelData );
					 
					$data['Product']['product_sku']		=	$ink_sku;
					$data['ProductDesc']['barcode']		=	$barcode;
					$data['Product']['CurrentStock']	=	$currentQty;
					$productData	=	json_decode(json_encode($data,0));					
					
					App::import('Controller', 'Cronjobs');
					$productCant = new CronjobsController();
					$productCant->storeInventoryRecord( $productData, $qty_bin, $order_id, 'Cancel With Location After Process', $barcode );
					$count++;
					
					 
					$this->BinLocation->updateAll(	array('BinLocation.stock_by_location' => "BinLocation.stock_by_location + $qty_bin"),
														array('BinLocation.bin_location' => $location, 'BinLocation.barcode' => $barcode));
						 					
						 
					$this->CheckIn->updateAll( array('CheckIn.selling_qty' => "CheckIn.selling_qty - $qty_bin"),array('CheckIn.id' => $po_id));							  
  					
					$this->InkOrderLocation->updateAll( array( 'InkOrderLocation.status' =>"'canceled'") , array( 'order_id' => $order_id ) );
					  
					file_put_contents(WWW_ROOT .'logs/ink_order_cancele_'.date('dmY').'.log', print_r($cancelData,true), FILE_APPEND | LOCK_EX);
				}
			 	
			}
			
			/*--------------End Ink Order Cancel---------------*/
			
			if(count($order_details) > 0){
			$count = 0;
		
			foreach($order_details as $val){
			
				if($val['OrderLocation']['bin_location'] == 'No Location')
				{
					$partial_fulfillment += $val['OrderLocation']['quantity'];
				}
				else
				{
					$barcode 	= $val['OrderLocation']['barcode'];
					$sku		= $val['OrderLocation']['sku'];
					$location 	= $val['OrderLocation']['bin_location'];
					$quantity 	= $val['OrderLocation']['quantity'];
					$qty_bin 	= $val['OrderLocation']['available_qty_bin'];
					//$qty_chin 	= $val['OrderLocation']['available_qty_check_in'];
					$po_id	 	=  explode(",",$val['OrderLocation']['po_id'])[0];					
					
					$getBinLocation	=	$this->BinLocation->find('first', array( 
																'conditions' => array(
																		'BinLocation.barcode' => $barcode,
																		'BinLocation.bin_location' => $location),
																		));
					$locationCurrentQty = 0;
					$totallocationQty   = 0;
					if(count($getBinLocation) > 0){
						$locationCurrentQty = $getBinLocation['BinLocation']['stock_by_location'];
						$totallocationQty = (int)$locationCurrentQty + (int)$qty_bin;
						$this->BinLocation->updateAll(
													array('BinLocation.stock_by_location' => $totallocationQty),
													array('BinLocation.bin_location' => $location,'BinLocation.barcode' => $barcode)
												  );
												  
						$this->CheckIn->updateAll( array('CheckIn.qty_checkIn' => "CheckIn.qty_checkIn + $qty_bin",'CheckIn.selling_qty' => "CheckIn.selling_qty - $qty_bin"),array('CheckIn.id' => $po_id));	
					}							  
					$getCurrentQty = $this->Product->find('first', array( 
																	'conditions' => array( 'Product.product_sku' => $sku ),
																	'fields' => array( 'Product.current_stock_level' )
																	 ) );
					$currentQty		=	$getCurrentQty['Product']['current_stock_level'];					
					$totalcurrentQty =  (int)$currentQty + (int)$qty_bin ;
					
					$this->Product->updateAll(
												array('Product.current_stock_level' => $totalcurrentQty),
												array('Product.product_sku' => $sku)
											  );
											  
					$cancelData['order_id']				= $order_id;
					$cancelData['sku']					= $sku;	
					$cancelData['barcode']				= $barcode;	
					$cancelData['bin_location']			= $location;	
					$cancelData['qty']					= $qty_bin;	
					$cancelData['current_qty']			= $currentQty;
					$cancelData['after_qty']			= $totalcurrentQty;	
					$cancelData['current_location_qty'] = $locationCurrentQty;
					$cancelData['after_location_qty']	= $totallocationQty;
					$cancelData['user_name']			= $firstName.' '.$lastName;
					$cancelData['date_timef']			= date( 'Y-m-d H:i:s' );
					//pr($cancelData);
					$this->CancelSku->saveAll( $cancelData );
					 
					$data['Product']['product_sku']		=	$sku;
					$data['ProductDesc']['barcode']		=	$barcode;
					$data['Product']['CurrentStock']	=	$currentQty;
					$productData	=	json_decode(json_encode($data,0));					
					
					App::import('Controller', 'Cronjobs');
					$productCant = new CronjobsController();
					$productCant->storeInventoryRecord( $productData, $qty_bin, $order_id, 'Cancel With Location After Process', $barcode );
					$count++;
					
					}
			
			}				
			$this->OrderLocation->updateAll( array( 'OrderLocation.status' => "'canceled'") , array( 'OrderLocation.order_id' => $order_id ) );
			
			$noteDate['order_id'] = $order_id;
			$noteDate['note'] = $cancel_note;
			$noteDate['type'] = 'Cancel After Process';
			$noteDate['user'] = $firstName.' '.$lastName;
			$noteDate['date'] = date('Y-m-d H:i:s');
			$this->OrderNote->saveAll( $noteDate );
			$this->MergeUpdate->updateAll(array('MergeUpdate.cancelled_user'=>"'".$username."'",'MergeUpdate.cancelled_pc'=>"'".$pcName."'"),array( 'MergeUpdate.order_id' => $order_id ) );
			
		}
			
			echo "1"; exit;
		}
		
		public function setHeldCancelOrderMarked_130917()
		{
			
			$this->layout = '';
			$this->autoRender = false;
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'OrderNote' );
			$id = $this->request->data['id'];
			$lock_note = $this->request->data['cancelHeldNote'];
			
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			
			$this->MergeUpdate->updateAll(array( 'MergeUpdate.status' => 2,'MergeUpdate.scan_status'=>2,'MergeUpdate.sorted_scanned'=> 2),array( 'MergeUpdate.order_id' => $id ) );
			$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 2 ) , array( 'OpenOrder.num_order_id' => $id ) );
			
			$noteDate['order_id'] = $id;
			$noteDate['note'] = $lock_note;
			$noteDate['type'] = 'Held Cancel';
			$noteDate['user'] = $firstName.' '.$lastName;
			$noteDate['date'] = date('Y-m-d H:i:s');
			$this->OrderNote->saveAll( $noteDate );
			echo "1"; exit;
		}
		
		/*
		 * 
		 * Params, Lock orders
		 * 
		 */
		 
		public function unSetOrderMarked()
		{
			$this->layout = '';
			$this->autoRender = false;
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'OrderNote' );
			$id 			= $this->request->data['id'];
			$unlock_note 	= $this->request->data['unlockNote'];
		
			$this->MergeUpdate->query( "delete from royalmail_errors where split_order_id LIKE '{$id}%'" );
			$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 0,'MergeUpdate.rm_error_msg' => '""' ) , array( 'MergeUpdate.order_id' => $id ) );
			
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			
			$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 0 ) , array( 'OpenOrder.num_order_id' => $id ) );
			$noteDate['order_id'] = $id;
			$noteDate['note'] = $unlock_note;
			$noteDate['type'] = 'Unlock';
			$noteDate['user'] = $firstName.' '.$lastName;
			$noteDate['date'] = date('Y-m-d H:i:s');
			$this->OrderNote->saveAll( $noteDate );
			echo "1"; exit;
		}
		
		public function unSetOrderMarked_140617()
		{
			
			$this->layout = '';
			$this->autoRender = false;
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$id = $this->request->data['id'];
			
			$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 0 ) , array( 'MergeUpdate.order_id' => $id ) );
			$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 0 ) , array( 'OpenOrder.num_order_id' => $id ) );
			
			echo "1"; exit;
		}
		
		
		
		public function skuLocation()
			{
				$this->layout = 'index';
				$this->loadModel( 'Brand' );
				
				$this->loadModel( 'PackageEnvelope' );
				$this->loadModel( 'Packagetype' );
				$packageType = $this->Packagetype->find('list', array('fields' => 'Packagetype.id, Packagetype.package_type_name'));
				
				$brandName	=	$this->Brand->find('list', array('fields' => 'brand_name, brand_name'));
				$this->set( 'brandName', $brandName);
				
				//Rack Section
				$this->loadModel( 'Rack' );
				
				//Ground
				$floorRacks = $this->Rack->find('list' , array( 'fields' => array( 'Rack.floor_name' ) , 'order' => array( 'Rack.id ASC' ) , 'group' => array( 'Rack.floor_name' ) ) );
				
				//Racks
				$rack = $rackRacks = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , array( 'fields' => array( 'Rack.id' , 'Rack.rack_floorName' ) , 'conditions' => array( 'Rack.floor_name' => $floorRacks ) , 'group' => array( 'Rack.rack_floorName' ) , 'order' => array( 'Rack.id ASC' ) ) );		
				
				//Level
				$rackLevel = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , 
						array( 
							'fields' => array( 'Rack.id' , 'Rack.level_association' ) , 
							'conditions' => array( 'Rack.floor_name' => $floorRacks , 'Rack.rack_floorName' => $rack ) , 'group' => array( 'Rack.level_association' ) , 'order' => array( 'Rack.id ASC' ) 
							) 
						);
				
				//Section
				$rackSection = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , 
						array( 
							'fields' => array( 'Rack.id' , 'Rack.rack_level_section' ) , 
							'conditions' => array( 'Rack.floor_name' => $floorRacks , 'Rack.rack_floorName' => $rack , 'Rack.level_association' => $rackLevel ) , 'order' => array( 'Rack.id ASC' ) 
							) 
						);
						
				$this->set( 'setNewGroundArray' , $floorRacks );
				$this->set( 'rack' , $rack );
				$this->set( 'rackLevel' , $rackLevel );
				$this->set( 'rackSection' , $rackSection );
				$this->set( 'packageType', $packageType);
			}
	
	public function getBarcodeSearchLocation()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		$this->loadModel('BinLocation');
		
		$barcode	=	$this->request->data['barcode'];
		//$barcode	=	trim($this->request->data['barcode']);
		
		$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array('ProductDesc.barcode' => $barcode)
								)
							);
							
		/*$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array(
										'OR' => array(
												array('ProductDesc.barcode' => $barcode),
												array('Product.product_sku LIKE' => '%'.$barcode.'%'),
												array('Product.product_name LIKE' => '%'.$barcode.'%')
										)
									)
								)
							);*/
		
		$getBarcodeFromProduct	=	$productdeatil['ProductDesc']['barcode'];
		
		$this->ProductDesc->unbindModel( array(
			'hasOne' => array(
				'Product'
			)
		) );
		
		/*$binLocation =	$this->BinLocation->find('all', array(
									'joins' => array(
										array(
											'table' => 'product_descs',
											'alias' => 'ProductDesc',
											'type' => 'INNER',
											'conditions' => array(
												'ProductDesc.barcode = BinLocation.barcode',
												'BinLocation.barcode ='.$barcode
											)
										)
									)
								)
							);*/
		
               
                $binLocation =	$this->BinLocation->find('all', array( 'conditions' => array(
                                                                        'BinLocation.barcode' => $barcode
                                                                    )
                                                                    
								)
							);
                
		$data['id'] 				= 	$productdeatil['ProductDesc']['id'];
		$data['stock'] 				= 	$productdeatil['Product']['current_stock_level'];
		$data['product_id']			= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 				= 	$productdeatil['Product']['product_name'];
		$data['sku'] 				= 	$productdeatil['Product']['product_sku'];
		$data['shortdescription'] 	= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 	= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 				= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 			= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 				= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 			= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 			= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 			= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		$data['bin_combined_text'] 			= 	$productdeatil['ProductDesc']['bin_combined_text'];
		
		//Get and set Bin Locations
		$binLocate = 0;
                
		foreach($binLocation as $binLocationIndex => $binLocationValue)
		{
			$binData[$binLocate]['id']					=	$binLocationValue['BinLocation']['id'];	
			$binData[$binLocate]['barcode']				=	$binLocationValue['BinLocation']['barcode'];	
			$binData[$binLocate]['bin_location']		=	$binLocationValue['BinLocation']['bin_location'];	
			$binData[$binLocate]['stock_by_location']	=	$binLocationValue['BinLocation']['stock_by_location'];	
		$binLocate++;
		}
		$data['binLocationArray'] = $binData;
		
		$imageUrl	=	Router::url('/', true).'img/product/';
		
		$i = 0;
		foreach($productdeatil['ProductImage'] as $image)
		{
			if($image['image_name'] != '')
			{
				$imagedata[$i]['imageid']		=	$image['id'];
				$imagedata[$i]['imagename']		=	$imageUrl.$image['image_name'];
				$imagedata[$i]['selectedimage']	=	$image['selected_image'];
			}
			$i++;
		}
		
		$j = 0;
		foreach( $productdeatil['ProductLocation'] as $productLocation)
		{
			$productLocation['warehouse_id'];
			$locationData[$j]['binrackid']	=	$productLocation['bin_rack_address'];
			$warehouseDetail	=	$this->Warehouse->find('first', array('conditions' => array('Warehouse.id' => $productLocation['warehouse_id'])));
			$locationData[$j]['warehousename']	=	$warehouseDetail['Warehouse']['warehouse_name'];
			$locationData[$j]['city']			=	$warehouseDetail['WarehouseDesc']['city_id'];
			$locationData[$j]['warehousetype']	=	$warehouseDetail['WarehouseDesc']['warehouse_type'];
			$j++;
		}
                
		//echo (json_encode(array('status' => '1', 'data' => $data, 'image' => $imagedata, 'location' => $locationData)));
		echo (json_encode(array('status' => '1', 'data' => $data)));
		exit;
	}
	
	public function getOpenOrderSkuQty( $getsku = null )
		{
			
			$this->layout = "";
			$this->autoRender = false;
			$this->loadModel('MergeUpdate');
			
			if( $getsku == '' )
			{
				$sku	=	$this->request->data['currentSku'];
			}
			else
			{
				$sku	=	$getsku;
			}
			
			$getdetails	=	$this->MergeUpdate->find( 'all' , array( 
																'conditions' => array( 'MergeUpdate.sku LIKE' =>"%{$sku}%",
																						'MergeUpdate.status' => 0 
																						
																		),
																'fields' => array( 'MergeUpdate.sku' )
																	)
																);
																 
			if( !empty( $getdetails ) )
			{
					$countSku = 0;
					foreach( $getdetails as $getdetailValue )
					{	
						$allSku   =	$getdetailValue['MergeUpdate']['sku'];
						$coma_exp = explode(",",$allSku);				
						foreach($coma_exp as $pexp){
							 $texp     = explode("-",$pexp);					
							 $t = "S-".trim($texp[1]);
							if($t == $sku){
								$quantity = (int)filter_var($texp[0], FILTER_SANITIZE_NUMBER_INT);	
								$countSku = $countSku + $quantity;	
							}				
						}			
					}
					return $countSku;
				
			} 
			else
			{
				return '0';
			}
		}
		
		
		public function verifyUnpreaperOrder()
		{
			$this->layout = "";
			$this->autoRender = false;
			
			$this->loadModel('UnprepareOrder');
			$pkOrderId	=	$this->request->data['pkOrderId'];
			
			App::import('Vendor', 'Linnworks/src/php/Classes/Auth');
			App::import('Vendor', 'Linnworks/src/php/Factory');
			App::import('Vendor', 'Linnworks/src/php/Orders');
		
			$username = Configure::read('linnwork_api_username');
			$password = Configure::read('linnwork_api_password');
			
			$token 				= Configure::read('access_new_token');
			$applicationId 		= Configure::read('application_id');
			$applicationSecret 	= Configure::read('application_secret');
			
			$multi 				= 	AuthMethods::Multilogin($username, $password);
			$auth 				= 	AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

			$token = $auth->Token;	
			$server = $auth->Server;
			$results = OrdersMethods::GetOrderById($pkOrderId,$token, $server);
			//pr( $results );
			//exit;
			if(isset( $results ) && !empty( $results ))
			{
				if( $results->Processed == '' )
				{
				// order in processing 
				$this->UnprepareOrder->updateAll( 
												array( 'UnprepareOrder.unprepare_status' => '0' ), 
												array( 'UnprepareOrder.order_id' => $pkOrderId ) 
												);
				echo "0";
				exit; 
				}
				else if( $results->Processed == 1 )
				{
					
					
				// cancel or processed 
				$this->UnprepareOrder->updateAll( 
												array( 'UnprepareOrder.unprepare_status' => '1' ), 
												array( 'UnprepareOrder.order_id' => $pkOrderId ) 
												);
				echo "1";
				exit;
				}
			}
			else
			{
				//not found
				$this->UnprepareOrder->updateAll( 
												array( 'UnprepareOrder.unprepare_status' => '2' ), 
												array( 'UnprepareOrder.order_id' => $pkOrderId ) 
												);
				echo "2";
				exit;
			}
		}
		
		public function deleteUnpreaperOrder()
		{
			$this->loadModel('UnprepareOrder');
			
			$numOrderId	=	$this->request->data['numOrderId'];
			
			$conditions = array (
									'UnprepareOrder.num_order_id' => $numOrderId
								);
			$result 		= 	$this->UnprepareOrder->deleteAll( $conditions );
			echo "1";
			exit;
			
		}
		
		
		public function getIntransitSku( $getsku = null )
		{
			$this->layout = '';
			$this->autoRender = false;
		
			if( $getsku == '' )
			{
				$currentSku	=	$this->request->data['currentSku'];
			}
			else
			{
				$currentSku	=	$getsku;
			}
			
			//$currentSku = 'S-2970B001';
			$this->loadModel('Po');
			$this->loadModel('PurchaseOrder');
			$this->loadModel('CheckIn');
			
			
			
			$params	=	array(
					'joins' => array(
							array(
								'table' => 'pos',
								'alias' => 'Po',
								'type' => 'INNER',
								'conditions' => array(
									'Po.po_name = PurchaseOrder.po_name'
								)
							)
						),
					'conditions' => array( 
									'Po.status' => '1',
									'PurchaseOrder.purchase_sku' => $currentSku,
									),
					//'fields' => array('SUM(PurchaseOrder.purchase_qty) AS QUANTITY')
				);
				
			$skuCounts	=	$this->PurchaseOrder->find( 'all', $params );
			$total = 0;
		
			foreach( $skuCounts as $skuCount )
			{
				$checkInPoName	=	 $skuCount['PurchaseOrder']['po_name'];
				$purchaseQty	=	 $skuCount['PurchaseOrder']['purchase_qty'];
				$purQtyCount	=	 $skuCount['PurchaseOrder']['purchase_count'];
				
				$checkInSku		=	 $currentSku;
				$getCheckInskuDetails	=	$this->CheckIn->find( 'all', array( 
															'conditions' => array( 
																				'CheckIn.sku' => $checkInSku,
																				'CheckIn.po_name' => $checkInPoName 
																				) ,
															'fields' => array('SUM(CheckIn.qty_checkIn) AS CHECKINQUANTITY') 
																			)
																		);
				if( !empty($getCheckInskuDetails) )
				{
					$checkInQty		=	 $getCheckInskuDetails['0']['0']['CHECKINQUANTITY'];
					if( $checkInQty > 0 && $checkInQty >=$purQtyCount )
					{
						$intransitQty	=	 $purQtyCount - $checkInQty;
						if( $intransitQty > 0 )
						{
							$purchaseQty = $intransitQty;
						}
						else
						{
							$purchaseQty = '0';
						}
					}
				}
				$total = $total + $purchaseQty;
			}
			return $total;
			exit;
			
		}
		
		
		/*public function getIntransitSku_old( $getsku = null )
		{
			$this->layout = '';
			$this->autoRender = false;
			
			if( $getsku == '' )
			{
				$currentSku	=	$this->request->data['currentSku'];
			}
			else
			{
				$currentSku	=	$getsku;
			}
			
			$this->loadModel('Po');
			$this->loadModel('PurchaseOrder');
			$params	=	array(
					'joins' => array(
							array(
								'table' => 'pos',
								'alias' => 'Po',
								'type' => 'INNER',
								'conditions' => array(
									'Po.po_name = PurchaseOrder.po_name'
								)
							)
						),
					'conditions' => array( 
									'Po.status' => '1',
									'PurchaseOrder.purchase_sku' => $currentSku,
									),
					'fields' => array('SUM(PurchaseOrder.purchase_qty) AS QUANTITY')
				);
				
			$skuCount	=	$this->PurchaseOrder->find( 'all', $params );
			
			if($skuCount['0']['0']['QUANTITY'] > 0)
			{
				return $skuCount['0']['0']['QUANTITY'];
				exit;
			}
			else
			{
				return '0';
				exit;
			}
		}*/
		
		
		public function updateLocation()
		{
			$this->layout = '';
			$this->autoRender = false;
			
			$this->loadModel( 'BinLocation' );
			$this->loadModel( 'Product' );
			$this->loadModel( 'CancelSku' );
			$sku		=	trim($this->request->data['sku']);
			$barcode	=	trim($this->request->data['barcode']);
			$quantity	=	trim($this->request->data['quantity']);
			$location	=	trim($this->request->data['location']);
			$orderID	=	trim($this->request->data['orderId']);
			
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			
			$getBinLocation	=	$this->BinLocation->find('first', array( 
																'conditions' => array(
																		'BinLocation.barcode' => $barcode,
																		'BinLocation.bin_location' => $location),
																		));
			
			$locationCurrentQty = $getBinLocation['BinLocation']['stock_by_location'];
			$totallocationQty = (int)$locationCurrentQty + (int)$quantity;
			$this->BinLocation->updateAll(
											array('BinLocation.stock_by_location' => $totallocationQty),
											array('BinLocation.bin_location' => $location,
											 	  'BinLocation.barcode' => $barcode
											 	)
										  );
			$getCurrentQty = $this->Product->find('first', array( 
															'conditions' => array( 'Product.product_sku' => $sku ),
															'fields' => array( 'Product.current_stock_level' )
															 ) );
			$currentQty		=	$getCurrentQty['Product']['current_stock_level'];
			
			$totalcurrentQty = (int)$quantity + (int)$currentQty;
			$this->Product->updateAll(
										array('Product.current_stock_level' => $totalcurrentQty),
										array('Product.product_sku' => $sku)
									  );
									  
			$cancelData['order_id']				= $orderID;
			$cancelData['sku']					= $sku;	
			$cancelData['barcode']				= $barcode;	
			$cancelData['bin_location']			= $location;	
			$cancelData['qty']					= $quantity;	
			$cancelData['current_qty']			= $currentQty;
			$cancelData['after_qty']			= $totalcurrentQty;	
			$cancelData['current_location_qty'] = $locationCurrentQty;
			$cancelData['after_location_qty']	= $totallocationQty;
			$cancelData['user_name']			= $firstName.' '.$lastName;
			$cancelData['date_time']			= date( 'Y-m-d H:i:s' );
			
			$data['Product']['product_sku']		=	$sku;
			$data['ProductDesc']['barcode']		=	$barcode;
			$data['Product']['CurrentStock']	=	$totalcurrentQty;
			$productData	=	json_decode(json_encode($data,0));
			
			$Quantity		=	$quantity;
			$id				=	$orderID;
			$ordertype		=	'Cancel With Location';
			$barcodeVal		=	$barcode;
			
			App::import('Controller', 'Cronjobs');
			$productCant = new CronjobsController();
			$productCant->storeInventoryRecord( $productData, $Quantity, $id, $ordertype, $barcodeVal );
			
			$this->CancelSku->saveAll( $cancelData );
			echo "1";
			exit;
		}
		
		public function setCancelOrderStatusAfterPopup()
		{
			$this->layout = '';
			$this->autoRender = false;
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'CancelSku' );
			
			$id 			= $this->request->data['openOrderId'];
			$optionValue 	= $this->request->data['optionValue'];
			
			$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 2 ) , array( 'MergeUpdate.order_id' => $id ) );
			$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 2 , 'OpenOrder.cancel_status' => 5 ) , array( 'OpenOrder.num_order_id' => $id ) );
			
			if( $optionValue == 'No' )
			{
				$Quantity							=	0;
				$ordertype							=	'Cancel With Location';
				$barcodeVal							=	'---';
				$data['Product']['product_sku']		=	'---';
				$data['ProductDesc']['barcode']		=	'---';
				$data['Product']['CurrentStock']	=	'---';
				$productData	=	json_decode(json_encode($data,0));
				App::import('Controller', 'Cronjobs');
				$productCant = new CronjobsController();
				$productCant->storeInventoryRecord( $productData, $Quantity, $id, $ordertype, $barcodeVal );
				
				$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
				$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
				
				$cancelData['order_id']				= $id;
				$cancelData['sku']					= '---';	
				$cancelData['barcode']				= '---';	
				$cancelData['bin_location']			= '---';	
				$cancelData['qty']					= '---';	
				$cancelData['current_qty']			= '---';
				$cancelData['after_qty']			= '---';	
				$cancelData['current_location_qty'] = '---';
				$cancelData['after_location_qty']	= '---';
				$cancelData['user_name']			= $firstName.' '.$lastName;
				$cancelData['date_time']			= date( 'Y-m-d H:i:s' );
				
				$this->CancelSku->saveAll( $cancelData );
				
			}
			echo "1";
			exit;
		}
		
		
		
		public function updateBarcode(){
		
			$old_barcode = $this->request->data['old_barcode'];
			$new_barcode = $this->request->data['new_barcode'];
			
			$this->loadModel( 'BinLocation' );
			//$this->loadModel( 'CancelOrder' );			
			$this->loadModel( 'CheckIn' );
			$this->loadModel( 'InventoryRecord' );				
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );			
			$this->loadModel( 'ProductDesc' );	
			$this->loadModel( 'PurchaseOrder' );				
			$this->loadModel( 'ScanOrder' );
			$this->loadModel( 'SkuPercentRecord' );
			$this->loadModel( 'UpdateInventorie' );
			
		
			$this->ProductDesc->unbindModel( array( 'belongsTo' => array( 'Product' ) ) );
			$c = 0;
			$barcodeCount = $this->ProductDesc->find('first', array('conditions' => array('barcode' => $old_barcode)));
			if(count($barcodeCount) > 0){				
				$this->BinLocation->updateAll(array('barcode' => "'".$new_barcode."'"),	array('barcode' => $old_barcode)  );
				$c++;
			}
			
			$binCount = $this->BinLocation->find('first', array('conditions' => array('barcode' => $old_barcode)));
			if(count($barcodeCount) > 0){
				$this->BinLocation->updateAll(array('barcode' => "'".$new_barcode."'"),	array('barcode' => $old_barcode)  );
				$c++;
			}
			
			$checCount = $this->CheckIn->find('first', array('conditions' => array('barcode' => $old_barcode)));
			if(count($checCount) > 0){
				$this->CheckIn->updateAll(array('barcode' => "'".$new_barcode."'"),	array('barcode' => $old_barcode)  );
				$c++;
			}
			
			$invCount = $this->InventoryRecord->find('first', array('conditions' => array('barcode' => $old_barcode)));
			if(count($invCount) > 0){
				$this->InventoryRecord->updateAll(array('barcode' => "'".$new_barcode."'"),	array('barcode' => $old_barcode)  );
				$c++;
			}
			
			$mergeCount = $this->MergeUpdate->find('first', array('conditions' => array('barcode' => $old_barcode)));
			if(count($mergeCount) > 0){
				$this->MergeUpdate->updateAll(array('barcode' => "'".$new_barcode."'"),	array('barcode' => $old_barcode)  );
				$c++;
			}
			
			$prodCount = $this->ProductDesc->find('first', array('conditions' => array('barcode' => $old_barcode)));
			if(count($prodCount) > 0){
				$this->ProductDesc->updateAll(array('barcode' => "'".$new_barcode."'"),	array('barcode' => $old_barcode)  );
				$c++;
			}
			
			$poCount = $this->PurchaseOrder->find('first', array('conditions' => array('purchase_barcode' => $old_barcode)));
			if(count($poCount) > 0){
				$this->PurchaseOrder->updateAll(array('purchase_barcode' => "'".$new_barcode."'"),	array('purchase_barcode' => $old_barcode)  );
				$c++;
			}
			
			$scCount = $this->ScanOrder->find('first', array('conditions' => array('barcode' => $old_barcode)));
			if(count($scCount) > 0){
				$this->ScanOrder->updateAll(array('barcode' => "'".$new_barcode."'"),	array('barcode' => $old_barcode)  );
				$c++;
			}
			$skupCount = $this->SkuPercentRecord->find('first', array('conditions' => array('barcode' => $old_barcode)));
			if(count($skupCount) > 0){
				$this->SkuPercentRecord->updateAll(array('barcode' => "'".$new_barcode."'"),	array('barcode' => $old_barcode)  );
				$c++;
			}
			$upinCount = $this->UpdateInventorie->find('first', array('conditions' => array('barcode' => $old_barcode)));
			if(count($upinCount) > 0){
				$this->UpdateInventorie->updateAll(array('barcode' => "'".$new_barcode."'"),	array('barcode' => $old_barcode)  );
				$c++;
			}
			
			if($c > 0){			
				//$this->CancelOrder->updateAll(array('barcode' => "'".$new_barcode."'"),	array('barcode' => $old_barcode)  );			
				$this->OpenOrder->query("UPDATE open_orders set items=replace(items, '".$old_barcode."','".$new_barcode."') WHERE items like '%".$old_barcode."%'");					
				
				$update_log = "New barcode=".$new_barcode."\tOld barcode=".$old_barcode."\tUser=".$this->Session->read('Auth.User.username')."\t".date('Y-m-d H:i:s')."\n";
				file_put_contents(WWW_ROOT."img/barcodeUpdate.log", $update_log, FILE_APPEND | LOCK_EX);				
				$msg['msg']= '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Barcode Updated.</div>';	
		
			}else{
				$msg['msg']= '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>This Barcode Not Found.</div>';
			}
			
			echo json_encode($msg);
			exit;
	}
	
	public function getOverSellSku()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('InventoryRecord');
		$date		=	date('Y-m-d');
		$getRecords	=	$this->InventoryRecord->find('all', array(
																'conditions' => array('InventoryRecord.after_maniplation <=' => '-1', 'InventoryRecord.date' => $date ),
																'fields' => array('sum(InventoryRecord.after_maniplation) AS Quantity', 'InventoryRecord.date', 'InventoryRecord.sku'),
																'group' => 'InventoryRecord.sku',
																)
															);
															
		$mailFormate = '<html><head><style>
								table {
									border-collapse: collapse;
									width: 100%;
								}
								th, td {
									text-align: left;
									padding: 8px;
								}
								tr:hover {background-color: #f5f5f5}
								tr:nth-child(even){background-color: #f2f2f2}
								th {
									background-color: #4CAF50;
									color: white;
								}
								</style>
								</head><h1>Please check the quantity of these product!</h1>
								<br><table border="1" style="border: 1px solid #c2c2c2;">
									<tr><th>SKU</th><th>Quantity</th><th>Date</th></tr>';
		if(!empty($getRecords))
		{
			foreach( $getRecords as $getRecord)
			{
			$mailFormate.= '<tr><td>'.$getRecord['InventoryRecord']['sku'].'</td><td>'.$getRecord[0]['Quantity'].'</td><td>'.$getRecord['InventoryRecord']['date'].'</td></tr>';
			}
			
			$mailFormate.=  '</table></body></html>';
			App::uses('CakeEmail', 'Network/Email');
			$email = new CakeEmail('');
			$email->emailFormat('html');
			$email->from('info@xsensys.com');
			$email->to( array('amit.gaur@jijgroup.com','shashi.b.kumar@jijgroup.com','abhishek.srivastava@jijgroup.com'));					  
			$getBase = Router::url('/', true);
			$email->subject('Xsensys : Over Sell');
			$email->send( $mailFormate );
		}
		exit;
	}
	
	public function getSkuLocation()
	{
		$this->layout = 'index';
	}
	
	public function getproductSku()
	{
		$this->layout = '';
		$this->autoRender = false;
	
		$location	=	trim($this->request->data['location']);
		$this->loadModel('BinLocation');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		App::import('Controller', 'Reports');
		$productModel = new ReportsController();
		
		
		$params	=	array(
							'joins' => array(
									array(
										'table' => 'product_descs',
										'alias' => 'ProductDesc',
										'type' => 'INNER',
										'conditions' => array(
											'ProductDesc.barcode = BinLocation.barcode'
										)
									),
									array(
										'table' => 'products',
										'alias' => 'Product',
										'type' => 'INNER',
										'conditions' => array(
											'Product.id = ProductDesc.product_id'
										)
									)
								),
							'conditions' => array( 
											'BinLocation.bin_location' => $location,
											),
							'fields' => array('Product.product_sku','Product.product_name','Product.product_name','ProductDesc.Barcode','Product.current_stock_level','BinLocation.stock_by_location')
						);
		
		
		$getLocation 	=	$this->BinLocation->find('all', $params );
		$i = 0;
		foreach( $getLocation as $Location )
		{
			//$getResult				=	$this->getupparpereItem( $Location['Product']['product_sku'] );
			$getResult				=	$this->getupparpereItem( $Location['Product']['product_sku'] );
			$getunpickResult		=	$this->getUnpickItem( $Location['Product']['product_sku'] );
			$getLocation[$i]['unpaid']['qty'] 		= 	$getResult;
			$getLocation[$i]['unpick']['qty'] 		= 	$getunpickResult;
			$getLocation[$i]['onself']['qty'] 		= 	$getResult + $getunpickResult + $Location['BinLocation']['stock_by_location'];
			$allLocation 							= 	$this->getAllskuLocation( $Location['ProductDesc']['Barcode'], $Location['Product']['product_sku']);
			$getLocation[$i]['alllocation']['loc']	=	$allLocation;
			$allOrders 								= 	$this->getAllOrderQty( $Location['Product']['product_sku'] );
			$getLocation[$i]['allorders']['opun']	=	$allOrders + $getResult;
			$allpickqty 							= 	$this->getpickqtyresult( $Location['Product']['product_sku'] );
			$getLocation[$i]['pick']['qty']	=	$allpickqty;
			$getLocation[$i]['ProductDesc']['Barcodelo']	=	$this->GlobalBarcode->getAllBarcode( $getLocation[$i]['ProductDesc']['Barcode'] );
			
			$i++;
		}
		
		if( count( $getLocation ) > 0 )
		{
			echo json_encode(array('status' => '1', 'data' => $getLocation));		
			exit;
		}
		else
		{
			echo json_encode(array('status' => '0'));		
			exit;
		}
	}
	
	public function getpickqtyresult( $picksku = null, $location = null)
	{
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderLocation' );
		$params	=	array(
							'joins' => array(
									array(
										'table' => 'open_orders',
										'alias' => 'OpenOrder',
										'type' => 'INNER',
										'conditions' => array(
											'OpenOrder.num_order_id = OrderLocation.order_id',
											'OpenOrder.status = 0'
										)
									)
								),
							'conditions' => array( 'OrderLocation.sku' => $picksku, 'OrderLocation.bin_location' => $location, 'OrderLocation.status' => 'active', 'OrderLocation.pick_list' => '1'),
							'fields' => array( 'SUM(OrderLocation.quantity) as locqty' )
						);
		
		
		$getresult	=	$this->OrderLocation->find('all', $params);
		
		if( $getresult[0][0]['locqty'] != '')
		{
			return $getresult[0][0]['locqty'];
		} else {
			return '0';
		}
	}
	
	public function getAllOrderQty( $openordersku = null )
	{
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderLocation' );
		$params	=	array(
							'joins' => array(
									array(
										'table' => 'open_orders',
										'alias' => 'OpenOrder',
										'type' => 'INNER',
										'conditions' => array(
											'OpenOrder.num_order_id = OrderLocation.order_id',
											'OpenOrder.status = 0'
										)
									)
								),
							'conditions' => array( 'OrderLocation.sku' => $openordersku, 'OrderLocation.status' => 'active'),
							'fields' => array( 'SUM(OrderLocation.quantity) as locqty' )
						);
		
		
		$getresult	=	$this->OrderLocation->find('all', $params);
		
		if( $getresult[0][0]['locqty'] != '')
		{
			return $getresult[0][0]['locqty'];
		} else {
			return '0';
		}
		
		
		/*$openorder	=	$this->OpenOrder->find('all',array('conditions' => array('OpenOrder.status' => '0'), 'fields' => array('items','num_order_id','sub_source','destination','date')));
		
		foreach($openorder as $openorder)
		{
			$items		=	unserialize($openorder['OpenOrder']['items']);
			foreach( $items as $val)
			{
								 
					$_sku = explode( '-' , $val->SKU);
					if( count($_sku ) == 3 ) //For Bundle (Single)
					{
						$sku = 'S-'.$_sku[1];
						$qty = $val->Quantity * ($_sku[2]);
						$open_data[$sku][] = array('quantity'=>$qty);
						
						
					}
					else if( count( $_sku ) > 3 ) //For Bundle (Bundle)
					{						
						$inc = 1;	$ij = 0;
						while( $ij < count($_sku)-2 )
						{						
							$sku = 'S-'.$_sku[$inc];
												
							$inc++;
							$ij++;
							$open_data[$sku][] = array('quantity'=>$val->Quantity);
						}
					}else{				
						$open_data[$val->SKU][] = array('quantity'=>$val->Quantity);
					}
				}	
			}
				$count = 0;
				if(isset($open_data[$openordersku])){
					foreach($open_data[$openordersku] as $k => $v)
					{
						 $count	= $count + $v['quantity'];
					}
					return $count;
				} else {
					return '0';
				}*/
	}
	
	public function getproductSku_041017()
	{
		$this->layout = '';
		$this->autoRender = false;
	
		$location	=	trim($this->request->data['location']);
		$this->loadModel('BinLocation');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		App::import('Controller', 'Reports');
		$productModel = new ReportsController();
		
		
		$params	=	array(
							'joins' => array(
									array(
										'table' => 'product_descs',
										'alias' => 'ProductDesc',
										'type' => 'INNER',
										'conditions' => array(
											'ProductDesc.barcode = BinLocation.barcode'
										)
									),
									array(
										'table' => 'products',
										'alias' => 'Product',
										'type' => 'INNER',
										'conditions' => array(
											'Product.id = ProductDesc.product_id'
										)
									)
								),
							'conditions' => array( 
											'BinLocation.bin_location' => $location,
											),
							'fields' => array('Product.product_sku','Product.product_name','Product.product_name','ProductDesc.Barcode','Product.current_stock_level','BinLocation.stock_by_location')
						);
		
		
		$getLocation 	=	$this->BinLocation->find('all', $params );
		
		$i = 0;
		foreach( $getLocation as $Location )
		{
			$getResult				=	$this->getupparpereItem( $Location['Product']['product_sku'] );
			$getunpickResult		=	$this->getUnpickItem( $Location['Product']['product_sku'] );
			$getLocation[$i]['unpaid']['qty'] 		= $getResult;
			$getLocation[$i]['unpick']['qty'] 		= $getunpickResult;
			$getLocation[$i]['onself']['qty'] 		= $getResult + $getunpickResult + $Location['BinLocation']['stock_by_location'];
			$allLocation 							= $this->getAllskuLocation( $Location['ProductDesc']['Barcode'] );
			$getLocation[$i]['alllocation']['loc']	=	$allLocation;
			$i++;
		}
		//pr($getLocation );
		if( count( $getLocation ) > 0 )
		{
			echo json_encode(array('status' => '1', 'data' => $getLocation));		
			exit;
		}
		else
		{
			echo json_encode(array('status' => '0'));		
			exit;
		}
	}
	public function getAllskuLocation( $barcode = null, $sku = null )
	{
		$this->loadModel( 'BinLocation' );
		//$barcode	=	'734646967112';
		$getallLocations	=	$this->BinLocation->find( 'all', array( 'conditions' => array( 'BinLocation.barcode' => $barcode ) ) );
		$i = 0;
		foreach( $getallLocations as $getallLocation )
		{
			//pr($getallLocation);
			$location													=	$getallLocation['BinLocation']['bin_location'];
			$barcode													=	$getallLocation['BinLocation']['barcode'];
			$getopenordercount 											= 	$this->getordercountbylocation( $location,  $barcode);
			$getallLocationu[$i]['BinLocation']['id']					=	$getallLocation['BinLocation']['id'];
			$getallLocationu[$i]['BinLocation']['barcode']				=	$getallLocation['BinLocation']['barcode'];
			$getallLocationu[$i]['BinLocation']['bin_location']			=	$getallLocation['BinLocation']['bin_location'];
			$getallLocationu[$i]['BinLocation']['stock_by_location']	=	$getallLocation['BinLocation']['stock_by_location'];
			$getallLocationu[$i]['BinLocation']['priority']				=	$getallLocation['BinLocation']['priority'];
			$getallLocationu[$i]['BinLocation']['timestamp']			=	$getallLocation['BinLocation']['timestamp'];
			$getallLocationu[$i]['BinLocation']['locqty']				=	$getopenordercount;
			$unpick 													=	$this->getUnpickItem( $sku, $location);
			$getallLocationu[$i]['BinLocation']['unpickorder']			=	$unpick;
			$pick 														=	$this->getpickqtyresult( $sku, $location );
			$getallLocationu[$i]['BinLocation']['pickorder']			=	$pick;
			$unprapr													=	$this->getupparpereItem( $sku,$location);
			$getallLocationu[$i]['BinLocation']['unpaidorder']			=	$unprapr;
			$getallLocationu[$i]['BinLocation']['onself'] 				= 	$unprapr + $unpick + $getallLocation['BinLocation']['stock_by_location'];
			$unprapr = ''; $unpick = ''; $pick = '';
			$i++;
		}
		return  $getallLocationu;
	}
	
	public function getordercountbylocation( $locaton = null, $barcode = null )
	{
		$this->loadModel( 'OrderLocation' );
		$this->loadModel( 'OpenOrder' );
		//$locaton = 'G1_R14-L4-S2';
		//$barcode = '734646967112';
		
		$params	=	array(
							'joins' => array(
									array(
										'table' => 'open_orders',
										'alias' => 'OpenOrder',
										'type' => 'INNER',
										'conditions' => array(
											'OpenOrder.num_order_id = OrderLocation.order_id',
											'OpenOrder.status = 0'
										)
									)
								),
							'conditions' => array( 'OrderLocation.barcode' => $barcode, 'OrderLocation.bin_location' => $locaton, 'OrderLocation.status' => 'active'),
							'fields' => array( 'SUM(OrderLocation.quantity) as locqty' )
						);
		
		/*$getresult	=	$this->OrderLocation->find('all', array( 
																'conditions' => array( 'barcode' => $barcode, 'bin_location' => $locaton, 'status' => 'active'),
																'fields' => array( 'SUM(quantity) as locqty' )
																) );*/
		
		
		$getresult	=	$this->OrderLocation->find('all', $params);
		//pr($getresult);
		
		
		if( $getresult[0][0]['locqty'] != '')
		{
			return $getresult[0][0]['locqty'];
		} else {
			return '0';
		}
		
	}
	
	public function getUnpickItem( $unpicsku = null, $location = null)
	{
		$this->loadModel( 'OrderLocation' );
		$this->loadModel( 'OpenOrder' );
		//$locaton = 'G1_R14-L4-S2';
		//$barcode = '734646967112';
		
		$params	=	array(
							'joins' => array(
									array(
										'table' => 'open_orders',
										'alias' => 'OpenOrder',
										'type' => 'INNER',
										'conditions' => array(
											'OpenOrder.num_order_id = OrderLocation.order_id',
											'OpenOrder.status = 0'
										)
									)
								),
							'conditions' => array( 'OrderLocation.sku' => $unpicsku, 'OrderLocation.bin_location' => $location, 'OrderLocation.status' => 'active', 'pick_list' => 0),
							'fields' => array( 'SUM(OrderLocation.quantity) as locqty' )
						);
		
		/*$getresult	=	$this->OrderLocation->find('all', array( 
																'conditions' => array( 'barcode' => $barcode, 'bin_location' => $locaton, 'status' => 'active'),
																'fields' => array( 'SUM(quantity) as locqty' )
																) );*/
		
		
		$getresult	=	$this->OrderLocation->find('all', $params);
		//pr($getresult);
		
		
		if( $getresult[0][0]['locqty'] != '')
		{
			return $getresult[0][0]['locqty'];
		} else {
			return '0';
		}
		
	}
	
	
	public function getupparpereItem( $unpsku = null, $location = null)
	{
		
		$this->loadModel( 'UnprepareOrder' );
		$this->loadModel( 'OrderLocation' );
		
		$unpOrders	=	$this->UnprepareOrder->find('all',array('fields' => array('items','num_order_id','source_name','destination','date')));
		
		foreach($unpOrders as $unpOrder)
		{
			$items		=	unserialize($unpOrder['UnprepareOrder']['items']);
			$orderids[$unpOrder['UnprepareOrder']['num_order_id']]	= 	$unpOrder['UnprepareOrder']['num_order_id'];
			foreach( $items as $val)
			{
							 
					$_sku = explode( '-' , $val->SKU);
					if( count($_sku ) == 3 ) // For Bundle (Single)
					{
						$sku = 'S-'.$_sku[1];
						$qty = $val->Quantity * ($_sku[2]);
						$unp_data[$sku][] = array('quantity'=>$qty);
						
						
					}
					else if( count( $_sku ) > 3 ) // For Bundle (Bundle)
					{						
						$inc = 1;	$ij = 0;
						while( $ij < count($_sku)-2 )
						{						
							$sku = 'S-'.$_sku[$inc];
												
							$inc++;
							$ij++;
							$unp_data[$sku][] = array('quantity'=>$val->Quantity);
						}
					}else{				
						$unp_data[$val->SKU][] = array('quantity'=>$val->Quantity);
					}
				}	
		}
		
		
				$count = 0;
				
				if(isset($unp_data[$unpsku])){
					$checkorder	=$this->OrderLocation->find('all', array( 'conditions' => array( 'sku' => $unpsku, 'bin_location' => $location, 'status' => 'active', 'order_id IN' => $orderids ) ) );
					//pr($checkorder);
					
					foreach($checkorder as $k => $v)
						{
							 $count	=  $v['OrderLocation']['quantity'];
						}
						
						
					/*if(count($checkorder) > 0){
						foreach($unp_data[$unpsku] as $k => $v)
						{
							 $count	= $count + $v['quantity'];
						}					
					}*/
				 
				}
			
			return $count;	
	}
	
	public function getproductSku_220917()
	{
		$this->layout = '';
		$this->autoRender = false;
	
		$location	=	trim($this->request->data['location']);
		
		$this->loadModel('BinLocation');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		
		
		$params	=	array(
							'joins' => array(
									array(
										'table' => 'product_descs',
										'alias' => 'ProductDesc',
										'type' => 'INNER',
										'conditions' => array(
											'ProductDesc.barcode = BinLocation.barcode'
										)
									),
									array(
										'table' => 'products',
										'alias' => 'Product',
										'type' => 'INNER',
										'conditions' => array(
											'Product.id = ProductDesc.product_id'
										)
									)
								),
							'conditions' => array( 
											'BinLocation.bin_location' => $location,
											),
							'fields' => array('Product.product_sku','Product.product_name','Product.product_name','ProductDesc.Barcode','Product.current_stock_level')
						);
		
		
		$getLocation 	=	$this->BinLocation->find('all', $params );
		if( count( $getLocation ) > 0 )
		{
			echo json_encode(array('status' => '1', 'data' => $getLocation));		
			exit;
		}
		else
		{
			echo json_encode(array('status' => '0'));		
			exit;
		}
	}
	
	
	public function updateinventerybuylocationsearch()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'Product' );
		$this->loadModel( 'BinLocation' );
		$this->loadModel( 'InventoryRecord' );
		$barcode	=	$this->request->data['barcode'];
		$sku		=	$this->request->data['sku'];
		$qty		=	$this->request->data['qty'];
		$location	=	$this->request->data['location'];
		$type		=	$this->request->data['type'];
		
		$productData	=	$this->Product->find( 'first', array( 'conditions' => array( 'product_sku' => $sku ) ) );
		$stock	=	$productData['Product']['current_stock_level'];
		$getAllLocation		=	$this->BinLocation->find( 'all', array( 'conditions' => array( 'barcode' => $barcode ),'fields' => array( 'SUM(stock_by_location) as Qty' ) ) );
		$totlaLocatonQty	=	$getAllLocation[0][0]['Qty'];
		$status = '';
		if($type == 'Add')
		{
			$updatedStock =	$totlaLocatonQty + $qty;
			$this->Product->updateAll( array( 'Product.current_stock_level' => $updatedStock), array( 'Product.product_sku' => "$sku" ) );
			$binlocationData		=	$this->BinLocation->find( 'first', array( 'conditions' => array( 'barcode' => $barcode, 'bin_location' => $location ) ) );
			$locationStock			=	$binlocationData['BinLocation']['stock_by_location'];
			$updatedLocatioStock	=	$locationStock + $qty;
			$this->BinLocation->updateAll( 
											array('BinLocation.stock_by_location' => $updatedLocatioStock), 
											array( 'BinLocation.barcode' => $barcode, 'BinLocation.bin_location' => $location ) 
										);
			$search_location_log = "SKU=".$sku."\tBarcode=".$barcode."\tlocation=".$location."\tAction=".$type."\tQty=".$qty."\tStock Before Update=".$stock."\tStock After Update=".$updatedStock."\tQty Before Update=".$locationStock."\tQty After Update=".$updatedLocatioStock."\tUser=".$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name')."\t".date('Y-m-d H:i:s')."\n";
			file_put_contents(WWW_ROOT."img/QtyUpdateBySearchLocation.log", $search_location_log, FILE_APPEND | LOCK_EX);
			$status = 'Quantity Increase from get all sku of location.';
		} 
		elseif( $type == 'Subtraction' )
		{
			$updatedStock =	$totlaLocatonQty - $qty;
			$this->Product->updateAll( array( 'Product.current_stock_level' => $updatedStock), array( 'Product.product_sku' => "$sku" ) );
			$binlocationData		=	$this->BinLocation->find( 'first', array( 'conditions' => array( 'barcode' => $barcode, 'bin_location' => $location ) ) );
			$locationStock			=	$binlocationData['BinLocation']['stock_by_location'];
			$updatedLocatioStock	=	$locationStock - $qty;
			$this->BinLocation->updateAll( 
											array('BinLocation.stock_by_location' => $updatedLocatioStock), 
											array( 'BinLocation.barcode' => $barcode, 'BinLocation.bin_location' => $location ) 
										);
			$search_location_log = "SKU=".$sku."\tBarcode=".$barcode."\tlocation=".$location."\tAction=".$type."\tQty=".$qty."\tStock Before Update=".$stock."\tStock After Update=".$updatedStock."\tQty Before Update=".$locationStock."\tQty After Update=".$updatedLocatioStock."\tUser=".$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name')."\t".date('Y-m-d H:i:s')."\n";
			file_put_contents(WWW_ROOT."img/QtyUpdateBySearchLocation.log", $search_location_log, FILE_APPEND | LOCK_EX);
			$status = 'Quantity decrease from get all sku of location.';
		}
		else
		{
		}
				$updateData['action_type']			=	$type;
				$updateData['sku']					=	$sku;
				$updateData['barcode']				=	$barcode;
				$updateData['currentStock']			=	$stock;
				$updateData['quantity']				=	$qty;
				$updateData['after_maniplation'] 	=	$updatedStock;
				$updateData['split_order_id'] 		= 	'-';
				$updateData['status'] 				= 	$status;
				$updateData['date'] 				= 	date('Y-m-d');
				
				$this->InventoryRecord->saveAll( $updateData );
		
		echo json_encode(array('status' => '1', 'stockUpdate' => $updatedStock, 'locationupdate' => $updatedLocatioStock));		
		exit;
		
	}
	
	
	public function updateinventerybuylocationsearch_041017()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'Product' );
		$this->loadModel( 'BinLocation' );
		$barcode	=	$this->request->data['barcode'];
		$sku		=	$this->request->data['sku'];
		$qty		=	$this->request->data['qty'];
		$location	=	$this->request->data['location'];
		$type		=	$this->request->data['type'];
		
		
		$productData	=	$this->Product->find( 'first', array( 'conditions' => array( 'product_sku' => $sku ) ) );
		$stock	=	$productData['Product']['current_stock_level'];
		$getAllLocation		=	$this->BinLocation->find( 'all', array( 'conditions' => array( 'barcode' => $barcode ),'fields' => array( 'SUM(stock_by_location) as Qty' ) ) );
		$totlaLocatonQty	=	$getAllLocation[0][0]['Qty'];
		$status = '';
		if($type == 'Add')
		{
			$updatedStock =	$totlaLocatonQty + $qty;
			$this->Product->updateAll( array( 'Product.current_stock_level' => $updatedStock), array( 'Product.product_sku' => "$sku" ) );
			$binlocationData		=	$this->BinLocation->find( 'first', array( 'conditions' => array( 'barcode' => $barcode, 'bin_location' => $location ) ) );
			$locationStock			=	$binlocationData['BinLocation']['stock_by_location'];
			$updatedLocatioStock	=	$locationStock + $qty;
			$this->BinLocation->updateAll( 
											array('BinLocation.stock_by_location' => $updatedLocatioStock), 
											array( 'BinLocation.barcode' => $barcode, 'BinLocation.bin_location' => $location ) 
										);
			$search_location_log = "SKU=".$sku."\tBarcode=".$barcode."\tlocation=".$location."\tAction=".$type."\tQty=".$qty."\tStock Before Update=".$stock."\tStock After Update=".$updatedStock."\tQty Before Update=".$locationStock."\tQty After Update=".$updatedLocatioStock."\tUser=".$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name')."\t".date('Y-m-d H:i:s')."\n";
			file_put_contents(WWW_ROOT."img/QtyUpdateBySearchLocation.log", $search_location_log, FILE_APPEND | LOCK_EX);
			$status = 'Quantity Increase from get all sku of location.';
		} 
		elseif( $type == 'Subtraction' )
		{
			$updatedStock =	$totlaLocatonQty - $qty;
			if($updatedStock < 0)
			{
				$updatedStock = 0;
			}
			$this->Product->updateAll( array( 'Product.current_stock_level' => $updatedStock), array( 'Product.product_sku' => "$sku" ) );
			$binlocationData		=	$this->BinLocation->find( 'first', array( 'conditions' => array( 'barcode' => $barcode, 'bin_location' => $location ) ) );
			$locationStock			=	$binlocationData['BinLocation']['stock_by_location'];
			$updatedLocatioStock	=	$locationStock - $qty;
			if($updatedLocatioStock < 0)
			{
				$updatedLocatioStock = 0;
			}
			$this->BinLocation->updateAll( 
											array('BinLocation.stock_by_location' => $updatedLocatioStock), 
											array( 'BinLocation.barcode' => $barcode, 'BinLocation.bin_location' => $location ) 
										);
			$search_location_log = "SKU=".$sku."\tBarcode=".$barcode."\tlocation=".$location."\tAction=".$type."\tQty=".$qty."\tStock Before Update=".$stock."\tStock After Update=".$updatedStock."\tQty Before Update=".$locationStock."\tQty After Update=".$updatedLocatioStock."\tUser=".$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name')."\t".date('Y-m-d H:i:s')."\n";
			file_put_contents(WWW_ROOT."img/QtyUpdateBySearchLocation.log", $search_location_log, FILE_APPEND | LOCK_EX);
			$status = 'Quantity decrease from get all sku of location.';
		}
		else
		{
		}
		
				$updateData['action_type']			=	$type;
				$updateData['sku']					=	$sku;
				$updateData['barcode']				=	$barcode;
				$updateData['currentStock']			=	$stock;
				$updateData['quantity']				=	$qty;
				$updateData['after_maniplation'] 	=	$updatedStock;
				$updateData['split_order_id'] 		= 	'-';
				$updateData['status'] 				= 	$status;
				$updateData['date'] 				= 	date('Y-m-d');
				
				$this->InventoryRecord->saveAll( $updateData );
		
		
		echo json_encode(array('status' => '1', 'stockUpdate' => $updatedStock, 'locationupdate' => $updatedLocatioStock));		
		exit;
		
	}
	
	/*Apply code for Global Barcode*/
	public function addBarcode( $sku )
	{
		$this->layout = 'index';
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductBarcode' );
		$sku = str_replace('_h_','#',$sku); 
		$getproductdetail	=	$this->Product->find('first', array( 'conditions' => array( 'Product.product_sku' => $sku ) ) );
		$barcode			=	$getproductdetail['ProductDesc']['barcode'];
		$getAllBarcode		=	$this->ProductBarcode->find( 'all', array( 'conditions' => array( 'global_barcode' => $barcode ) ) );
		$this->set('getproductdetail', $getproductdetail);
		$this->set('getAllBarcodes', $getAllBarcode);
	}
	
	/*Apply code for Global Barcode*/
	public function savenewbarcode()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'ProductBarcode' );
		$barcode		=	$this->request->data['barcode'];
		$master			=	$this->request->data['masterbarcode'];
		$checkbarcode	=	$this->ProductBarcode->find('all',array( 'conditions' => array( 'barcode' => $barcode ) ) );
		
		if(count($checkbarcode) > 0)
		{
			$msg					=	 "Barcode Already exist.";
			echo json_encode(array('status' => '0', 'msg' => $msg));		
			exit;
			
		} else {
			$server = 	date('Y-m-d H:i:s');
			$jersey	=	date("Y-m-d H:i:s", strtotime("+4 hours",strtotime($server)));
			$data['barcode'] 		= 	$barcode;
			$data['global_barcode'] = 	$master;
			$data['username'] 		= 	($this->Session->read('Auth.User.username') != '') ? $this->Session->read('Auth.User.username') : '';
			$data['added_date'] 	= 	$jersey;
			$this->ProductBarcode->saveAll($data);
			$msg					=	 "Barcode save Successfully.";
			echo json_encode(array('status' => '1', 'msg' => $msg));		
			exit;
		}
		exit;
	}
	
	/*Apply code for Global Barcode*/
	public function updBarcode()
	{
		$this->layout = 'index';
		 
		$this->loadModel( 'ProductBarcode' );
		/*$this->loadModel( 'Product' );*/	
		$this->loadModel( 'ProductDesc' );	
		//$getproductdetail	=	$this->Product->find('all');
		
		
		$getproduct  = $this->ProductBarcode->query( "SELECT id,barcode FROM `product_descs` where product_type = 'Single'");	
	 	
		
		echo count($getproduct);echo  "<br>";
		$count = 0;
		foreach($getproduct as $getproductdetail){
		foreach($getproductdetail as $val){
		   echo $count++;
		   echo  "==="; $bar = array();
			$global_barcode = '';
			for($i = 0; $i < 13; $i++) {
				$global_barcode .= mt_rand(1, 9);
			}				
			echo $bar['global_barcode'] = $global_barcode;
			echo  "<br>";
			$barcode		= $val['barcode'];
			$username		= $this->Session->read('Auth.User.username');
			$added_date		= date('Y-m-d H:i:s');
			 
			//$this->ProductBarcode->saveAll($bar);
			
			//$this->ProductBarcode->query( "insert into product_barcodes(global_barcode,barcode, username,added_date)values('{$global_barcode}','{$barcode}' , '{$username}','{$added_date}')");	
			
			$productdscdata['barcode'] = $global_barcode;						
			$productdscdata['id']      = $val['id'];
			//pr($productdscdata);
			//exit;
			//$this->ProductDesc->saveAll($productdscdata, array('validate' => false));
			 
		  
			}
		}
		echo "<br>count=".$count ;exit;
		
		 
	}	
	/*Apply code for Global Barcode*/
	public function replaceBinBarcode()
	{
		$this->loadModel( 'ProductBarcode' );
		$this->loadModel( 'BinLocation' );	
		$allbins	=	$this->BinLocation->find('all');
		
		foreach($allbins as $allbin)
		{
			echo $barcode 	= $allbin['BinLocation']['barcode'];
			$globcode	=	$this->ProductBarcode->find('first', array( 'conditions' => array( 'barcode' =>  $barcode ) ) );
			if(!empty($globcode))
			{
				echo "  global_barcode==". $binlocationcdata['barcode'] = $globcode['ProductBarcode']['global_barcode'];
				//pr($globcode);
				//exit;
				echo "<br>";						
				$binlocationcdata['id']      = $allbin['BinLocation']['id'];
				pr($binlocationcdata);
				//$this->BinLocation->saveAll($binlocationcdata);
				//exit;
			}
		}
		exit;
	}
	
	
	
	
	public function getHSCode()
	{
		$this->loadModel( 'ProductBarcode' );
		$this->loadModel( 'BinLocation' );	
		$allbins	=	$this->BinLocation->find('all');
		
		foreach($allbins as $allbin)
		{
			echo $barcode 	= $allbin['BinLocation']['barcode'];
			$globcode	=	$this->ProductBarcode->find('first', array( 'conditions' => array( 'barcode' =>  $barcode ) ) );
			if(!empty($globcode))
			{
				echo "  global_barcode==". $binlocationcdata['barcode'] = $globcode['ProductBarcode']['global_barcode'];
				//pr($globcode);
				//exit;
				echo "<br>";						
				$binlocationcdata['id']      = $allbin['BinLocation']['id'];
				pr($binlocationcdata);
				//$this->BinLocation->saveAll($binlocationcdata);
				//exit;
			}
		}
		exit;
	}
	
	public function uploadHSCode()
	{
		$this->loadModel( 'ProductBarcode' );
		$this->loadModel( 'BinLocation' );	
		$allbins	=	$this->BinLocation->find('all');
		
		foreach($allbins as $allbin)
		{
			echo $barcode 	= $allbin['BinLocation']['barcode'];
			$globcode	=	$this->ProductBarcode->find('first', array( 'conditions' => array( 'barcode' =>  $barcode ) ) );
			if(!empty($globcode))
			{
				echo "  global_barcode==". $binlocationcdata['barcode'] = $globcode['ProductBarcode']['global_barcode'];
				//pr($globcode);
				//exit;
				echo "<br>";						
				$binlocationcdata['id']      = $allbin['BinLocation']['id'];
				pr($binlocationcdata);
				//$this->BinLocation->saveAll($binlocationcdata);
				//exit;
			}
		}
		exit;
	}
	
	public function addHSCode()
	{
		$this->loadModel( 'ProductBarcode' );
		$this->loadModel( 'BinLocation' );	
		$allbins	=	$this->BinLocation->find('all');
		
		foreach($allbins as $allbin)
		{
			echo $barcode 	= $allbin['BinLocation']['barcode'];
			$globcode	=	$this->ProductBarcode->find('first', array( 'conditions' => array( 'barcode' =>  $barcode ) ) );
			if(!empty($globcode))
			{
				echo "  global_barcode==". $binlocationcdata['barcode'] = $globcode['ProductBarcode']['global_barcode'];
				//pr($globcode);
				//exit;
				echo "<br>";						
				$binlocationcdata['id']      = $allbin['BinLocation']['id'];
				pr($binlocationcdata);
				//$this->BinLocation->saveAll($binlocationcdata);
				//exit;
			}
		}
		exit;
	}
	
	
		
}


?>
