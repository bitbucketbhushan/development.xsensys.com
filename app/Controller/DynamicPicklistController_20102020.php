<?php
error_reporting(0);
class DynamicPicklistController extends AppController
{
    var $name = "DynamicPicklist";    
    var $components = array('Session','Upload','Common','Auth','Paginator');
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
    var $virtualFields; 
   
    public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->Auth->Allow(array('cancelOrder','generatePicklistSlipLabel'));
	   $this->GlobalBarcode = $this->Components->load('Common'); /*Apply code for Global Barcode*/
	  // die('System is under maintenance');
	}
 	 
	public function index()  
    {		 
		$this->layout = 'index'; 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'DynamicBatche' );
		$this->loadModel( 'User' );  
		$batch = null; 
		if(isset($this->request->pass[0])){ 
			$batch = $this->request->pass[0]; 
 			$b = $this->DynamicBatche->find('count', array('conditions' => array('batch_name' =>$batch)));
 			if($b == 0){
				$this->Session->setflash( $batch .' batch number is not correct!', 'flash_danger'); 
				$this->redirect(array('controller'=>'DynamicPicklist','action' => 'batch'));
			}
  		}else{
			$this->Session->setflash( 'Batch number is missing!', 'flash_danger'); 
			//$this->redirect($this->referer());
			$this->redirect(array('controller'=>'DynamicPicklist','action' => 'batch'));
		}
		
		$perPage	=	 50;
		$this->paginate = array('conditions' => array('batch' =>$batch),'order'=>'id DESC','limit' => $perPage);
		$DynamicPicklist = $this->paginate('DynamicPicklist');
		
		$users =	$this->User->find('all', array('conditions' => array('country' =>1,'is_deleted' =>0),'fields' => array('id','first_name','last_name','username','email'),'group' => 'email'));	
 				
 		$this->set( compact('sections','users') );
		$this->set( 'DynamicPicklist',$DynamicPicklist); //pr( $data);
		
 	}
	
    public function pickingUser()
    { 
		$this->layout = "";	 
		$this->loadModel( 'User' );
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'MergeUpdate' );
		
		$this->DynamicPicklist->updateAll( array('picking_user_email' => "'".$this->request->data['picking_user']."'",
		'assign_by_user' => "'".$this->Session->read('Auth.User.email')."'",'picking_assign_time' => "'".date('Y-m-d H:i:s')."'"),array('id' => $this->request->data['picklist_inc_id']));	
		
  		$user =	$this->User->find('first', array('conditions' => array('email' =>$this->request->data['picking_user']),'fields' => array('id','first_name','last_name'),'group' => 'email'));
		  $d['user'] = $user['User']['first_name'].' '.$user['User']['last_name'];
  		  
		  $this->MergeUpdate->updateAll(array('MergeUpdate.user_name' => $user['User']['id'],'MergeUpdate.assign_user' => "'".$d['user']."'"), array('MergeUpdate.picklist_inc_id' => $this->request->data['picklist_inc_id'],'MergeUpdate.status' => '0'));
			
  		/*----------Maintain  Logs---------------------*/
		$log_file = WWW_ROOT .'logs/dynamic_log/picking_assign_user_'.date('dmy').".log";
		
		if(!file_exists($log_file)){
			file_put_contents($log_file, 'AssignByUserEmail'."\t".'Username'."\t".'PcName'."\t".'AssignToUser'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
		}
 		file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$this->request->data['picking_user']."\t".$this->request->data['picklist_inc_id']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
		/*--------End Of Logs-------------*/
		
		echo json_encode($d);
		exit;
	}
	
	public function packingUser()
    { 
		$this->layout = "";	 
		$this->loadModel( 'User' );
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'MergeUpdate' );
		
		$this->DynamicPicklist->updateAll( array('packing_user_email' => "'".$this->request->data['packing_user']."'",
		'assign_by_user' => "'".$this->Session->read('Auth.User.email')."'",'packing_assign_time' => "'".date('Y-m-d H:i:s')."'"),array('id' => $this->request->data['picklist_inc_id']));	
		
  		$user =	$this->User->find('first', array('conditions' => array('email' =>$this->request->data['packing_user']),'fields' => array('id','first_name','last_name'),'group' => 'email'));
		  $d['user'] = $user['User']['first_name'].' '.$user['User']['last_name'];
  		  
		  $this->MergeUpdate->updateAll(array('MergeUpdate.user_name' => $user['User']['id'],'MergeUpdate.assign_user' => "'".$d['user']."'"), array('MergeUpdate.picklist_inc_id' => $this->request->data['picklist_inc_id'],'MergeUpdate.status' => '0'));
			
  		/*----------Maintain  Logs---------------------*/
		$log_file = WWW_ROOT .'logs/dynamic_log/packing_assign_user_'.date('dmy').".log";
		
		if(!file_exists($log_file)){
			file_put_contents($log_file, 'AssignByUserEmail'."\t".'Username'."\t".'PcName'."\t".'AssignToUser'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
		}
 		file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$this->request->data['packing_assign_user']."\t".$this->request->data['picklist_inc_id']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
		/*--------End Of Logs-------------*/
		
		echo json_encode($d);
		exit;
	}
	
	public function calculateTime($start_time = null ,$end_time = null)
    { 
 		$interval = (strtotime($end_time)) - (strtotime($start_time) );
		$days = floor($interval / 86400); // seconds in one day
		$interval = $interval % 86400;
		$hours = floor($interval / 3600);
		$interval = $interval % 3600;
		$minutes = floor($interval / 60);
		$interval = $interval % 60;
		$seconds = $interval;
		$data = array();
		if($days > 0) $data['days'] =  $days;
		if($hours > 0)  $data['hours'] =   $hours;
		if($minutes > 0)  $data['minutes'] =   $minutes;
		if($minutes == 0)  $data['seconds'] =   $seconds;
		return $data;
	}
	
    public function getDeleveryCountry()
    { 
		$this->layout = "";	 
		$this->loadModel( 'Country' );	
		$this->loadModel( 'MergeUpdate' );		
		
		$country 	= array();
		$countries	= array();
		$countries_grp= array();
		$services   = array();
			//$order_ids  = $this->getSectionLocations($this->request->data['sections']);
		
 			$Where[] = "  `status` = 0 AND `pick_list_status` = 0 ";
			//$Where[] = "  order_id IN (".implode(",", $order_ids).")";
			
			if(isset($this->request->data['batch'])){
				$Where[] = " `created_batch` = '".$this->request->data['batch']."'";
			}
			if($this->request->data['sku_type'] == 'single'){
				$Where[] = " `sku` NOT LIKE '%,%' AND `service_name` != 'Over Weight' AND `service_name` != '' ";
			}else if($this->request->data['sku_type'] == 'multiple'){						
				$Where[] = " `sku` LIKE '%,%'  AND `service_name` != 'Over Weight' AND `service_name` != '' ";
			} 		
						
			if(count($Where) > 0){				  
			
				$sql_query = ''; $where_col ='';
				if(count($Where)>0){
					$w = 1;
					$count_lenght_of_where = count($Where);
					foreach ($Where as $k) {
						$where_col .= $k;
						if ($w < $count_lenght_of_where) {
							$where_col .=' AND ';
						}
						$w++;
					 }
					$sql_query =' WHERE '.$where_col;			
				}	
			}
				//echo "SELECT order_id FROM merge_updates as MergeUpdate $sql_query";
				//exit; Guadeloupe Guadaloupe
			$sql = "SELECT delevery_country FROM merge_updates as MergeUpdate $sql_query GROUP BY delevery_country";	
			$country = $this->MergeUpdate->query($sql);
			$log_file = WWW_ROOT .'logs/dynamic_log/country_'.date('dmy').".log";	
			file_put_contents($log_file, $sql);	
			 
			if(count($country) >0){
				foreach($country  as $v){
					if($v['MergeUpdate']['delevery_country'] != 'null'){
						if($v['MergeUpdate']['delevery_country']  == 'Guadaloupe'){
							$countries[] = 'Guadeloupe';
						}
						$countries[] = trim($v['MergeUpdate']['delevery_country']);
					}
				}
			} 
			
			$sql = "SELECT country_group, custom_name,iso_2 FROM countries as Country WHERE name IN('".implode("','",$countries)."') GROUP BY country_group";
			$country_grp = $this->Country->query($sql);
			$log_file = WWW_ROOT .'logs/dynamic_log/country_group_'.date('dmy').".log";	
			file_put_contents($log_file, $sql);	
			
			if(count($country_grp) >0){
				foreach($country_grp  as $v){
				
					if($v['Country']['country_group'] == $v['Country']['iso_2']){
						$countries_grp[] = $v['Country']['custom_name'] ;
					}else{
						$countries_grp[] = ucwords($v['Country']['country_group']) ;
					}
				}
			}
		   
		 
  		echo json_encode($countries_grp);
		exit;		  
 	}
	
	public function getPoatalServices()
    { 
		$this->layout = "";	 
		$this->loadModel( 'MergeUpdate' );	
		$this->loadModel( 'Country' );	
		
		$orderItems = array();
		$services   = array();
		
		
		if(isset($this->request->data['postal_country']) && $this->request->data['postal_country'] != ''){
 		 
			$country_group = strtolower($this->request->data['postal_country']);		
		 	//$country_group = str_replace(" ", "_", $country_group) ;
			
			$country_grp = $this->Country->query("SELECT custom_name, iso_2 FROM countries as Country WHERE country_group = '".$country_group."' ");
			 
			if(count($country_grp) >0){
				foreach($country_grp  as $v){
					//$string = preg_replace('/[^A-Za-z0-9\-]/', '', $v['Country']['custom_name']); 
 					$countries_grp[] = $v['Country']['custom_name'];					 
				}
				
				$Where[] = " delevery_country IN ('".implode("','",$countries_grp)."')";
			}
		
		}
		// pr($Where);
			
		//$order_ids  = $this->getSectionLocations($this->request->data['sections']);
  		
		
			
			$Where[] = "  status = 0 AND pick_list_status = 0 AND service_name != 'Over Weight' AND service_name != '' "; 
			//$Where[] = "  order_id IN (".implode(",", $order_ids).") ";
			
			if(isset($this->request->data['batch'])){
				$Where[] = " created_batch = '".$this->request->data['batch']."'";
			}
			if($this->request->data['sku_type'] == 'single'){
				$Where[] = " sku NOT LIKE '%,%'";
			}else if($this->request->data['sku_type'] == 'multiple'){						
				$Where[] = " sku LIKE '%,%'";
			}
			 
			/*if(isset($this->request->data['postal_countries']) && count($this->request->data['postal_countries']) > 0){	
				$Where[] = " delevery_country IN('" . implode("','",$this->request->data['postal_countries']) . "') ";
			}*/	
				
						
			if(count($Where) > 0){				  
			
				$sql_query = ''; $where_col ='';
				if(count($Where)>0){
					$w = 1;
					$count_lenght_of_where = count($Where);
					foreach ($Where as $k) {
						$where_col .= $k;
						if ($w < $count_lenght_of_where) {
							$where_col .=' AND ';
						}
						$w++;
					 }
					$sql_query =' WHERE '.$where_col;			
				}	
			}
			
		 	 $sql = "SELECT order_id, service_name FROM merge_updates as MergeUpdate $sql_query GROUP BY service_name";
			
			$log_file = WWW_ROOT .'logs/dynamic_log/get_poatal_sql_'.date('dmy').".log";	
			file_put_contents($log_file, $sql);	
			
 				//exit;
			$orderItems = $this->MergeUpdate->query($sql);
			 
			if(count($orderItems) >0){
				foreach($orderItems as $v){
					if($v['MergeUpdate']['service_name']){
						$services[] = $v['MergeUpdate']['service_name'];
					}
				}
			}			
		
		echo json_encode($services);
		exit;
		  
 	}
	
	public function getBinLocations()
    { 
		$this->layout = "";	 
		$this->loadModel( 'MergeUpdate' );		
		$this->loadModel( 'OrderLocation' );	
		$this->loadModel( 'Country' );
		$skus = array();
 		
		$Where[] = "  status = 0 AND pick_list_status = 0 AND service_name != 'Over Weight' ";
		
		if(isset($this->request->data['batch'])){
			$Where[] = " created_batch = '".$this->request->data['batch']."'";
		}
			
		if($this->request->data['sku_type'] == 'single'){
			$Where[] = " sku NOT LIKE '%,%'";
		}else if($this->request->data['sku_type'] == 'multiple'){						
			$Where[] = " sku LIKE '%,%'";
		}
		 
		/*if(isset($this->request->data['postal_country']) && $this->request->data['postal_country'] != ''){

			$Where[] = " delevery_country LIKE '".$this->request->data['postal_country']."'";
		}*/
		if(isset($this->request->data['postal_country']) && $this->request->data['postal_country'] != ''){
 		 
			$country_group = strtolower($this->request->data['postal_country']);		
			//$country_group = str_replace(" ", "_", $country_group) ;
			
			$country_grp = $this->Country->query("SELECT custom_name, iso_2 FROM countries as Country WHERE country_group = '".$country_group."' ");
				
			if(count($country_grp) >0){
				foreach($country_grp  as $v){
					$countries_grp[] = $v['Country']['custom_name'];					 
  					if($v['Country']['custom_name']  == 'Guadeloupe'){
						$countries_grp[] = 'Guadaloupe';
					}
				}
				
				$Where[] = " delevery_country IN ('".implode("','",$countries_grp)."')";
			}
		
		}
 			
		if(isset($this->request->data['postal_service']) && $this->request->data['postal_service'] !=''){
			$Where[] = " service_name LIKE '".$this->request->data['postal_service']."'";
		}
					
		$sql_query = '';
					
		if(count($Where) > 0){				  
		
			 $where_col ='';
			if(count($Where)>0){
				$w = 1;
				$count_lenght_of_where = count($Where);
				foreach ($Where as $k) {
					$where_col .= $k;
					if ($w < $count_lenght_of_where) {
						$where_col .=' AND ';
					}
					$w++;
				 }
				$sql_query =' WHERE '.$where_col;			
			}	
		}
		$sql= "SELECT sku,barcode,order_id,product_order_id_identify FROM merge_updates as MergeUpdate $sql_query";
			//exit;
		 
		$orderItems = $this->MergeUpdate->query($sql);
		$tskus = [];
		$binlocations = [];	
		$oids = [];
		$log_file = WWW_ROOT .'logs/dynamic_log/t_'.$this->request->data['postal_country'].'_'.$this->request->data['postal_service'].'_'.date('dmy').".log";	
		if(count($orderItems) > 0){
			foreach($orderItems  as $v){
 						 
				$items = $this->MergeUpdate->query("SELECT sku,bin_location, quantity,order_id FROM order_locations as OrderLocation WHERE order_id ='".$v['MergeUpdate']['order_id']."' AND split_order_id ='".$v['MergeUpdate']['product_order_id_identify']."' AND status = 'active' AND pick_list = '0'");
											
				if(count($items) > 0){
					foreach($items as $m){ 
						$binlocations[$m['OrderLocation']['bin_location']][] = $m['OrderLocation']['quantity'];
						//$oids[] = array($v['MergeUpdate']['product_order_id_identify'],$m['OrderLocation']['bin_location'],$m['OrderLocation']['quantity']);
						
						// echo 'order id:'.$m['OrderLocation']['order_id'].' sku:'. $m['OrderLocation']['sku'].' Q:'.$m['OrderLocation']['quantity'] .' bin_location ' .$m['OrderLocation']['bin_location'];
				//echo "<br>";
				 
						file_put_contents($log_file, "S"."\t".$v['MergeUpdate']['product_order_id_identify']."\t".$m['OrderLocation']['bin_location']."\t".$m['OrderLocation']['quantity']."\n", FILE_APPEND|LOCK_EX);	
				
					}
				}			 
				 
			}
		}  
		//pr($oids);
		$loc = array();
		//$binlocations = $this->msortnew($binlocations);	
		$keys = array_keys($binlocations);
		natsort($keys); 	
		foreach($keys as $k){
			$loc[] = $k .' ['.array_sum($binlocations[$k]).']';
		}
		
  		echo json_encode($loc);
		exit;
		  
 	}
	
	
	public function getSkuList()
    { 
		$this->layout = "";	 
		$this->loadModel( 'MergeUpdate' );	
		$this->loadModel( 'OrderLocation' );	
		$this->loadModel( 'Country' );
			
		$skus = array();
		$binlocations = array();
		$locs = array();
		
		if(isset($this->request->data['bin_locations']) && count($this->request->data['bin_locations'])){
			foreach($this->request->data['bin_locations'] as $l){
				$bin = trim(explode("[",$l)[0]);
				$f = substr($bin,0,2);
				$floor[$f] = $f ;
				$locs[] = $bin;			
			}		
		}
		 
		
		if(	count($locs) > 0){
  	
			$Where[] = "  status = 0 AND pick_list_status = 0 AND service_name != 'Over Weight' ";
			
			if(isset($this->request->data['batch'])){
				$Where[] = " created_batch = '".$this->request->data['batch']."'";
			}
			 
			if($this->request->data['sku_type'] == 'single'){
				$Where[] = " sku NOT LIKE '%,%'";
			}else if($this->request->data['sku_type'] == 'multiple'){						
				$Where[] = " sku LIKE '%,%'";
			}
			 
			/*if(isset($this->request->data['postal_country']) && $this->request->data['postal_country'] != ''){
				$Where[] = " delevery_country LIKE '".$this->request->data['postal_country']."'";
			}*/
			if(isset($this->request->data['postal_country']) && $this->request->data['postal_country'] != ''){
 		 
				$country_group = strtolower($this->request->data['postal_country']);		
				//$country_group = str_replace(" ", "_", $country_group) ;
				
				$country_grp = $this->Country->query("SELECT custom_name, iso_2 FROM countries as Country WHERE country_group = '".$country_group."' ");
					
				if(count($country_grp) >0){
					foreach($country_grp  as $v){
						$countries_grp[] = $v['Country']['custom_name'];					 
					}
					
					$Where[] = " delevery_country IN ('".implode("','",$countries_grp)."')";
				}
		
			}
			if(isset($this->request->data['postal_service']) && $this->request->data['postal_service'] !=''){
				$Where[] = " service_name LIKE '".$this->request->data['postal_service']."'";
			}			
						
			if(count($Where) > 0){				  
			
				$sql_query = ''; $where_col ='';
				if(count($Where)>0){
					$w = 1;
					$count_lenght_of_where = count($Where);
					foreach ($Where as $k) {
						$where_col .= $k;
						if ($w < $count_lenght_of_where) {
							$where_col .=' AND ';
						}
						$w++;
					 }
					$sql_query =' WHERE '.$where_col;			
				}	
			}
			$sql= "SELECT order_id,product_order_id_identify FROM merge_updates as MergeUpdate $sql_query";
				//exit;
				 
			$orderItems = $this->MergeUpdate->query($sql);
			$tskus = [];	$split_order_id = []; 
			if(count($orderItems) >0){
				foreach($orderItems  as $v){
					$order_id[] = $v['MergeUpdate']['order_id'];
					$split_order_id[] = $v['MergeUpdate']['product_order_id_identify'];
				}
			} 
			
			$ssku['error'] = ''; 
			$items = array();
			//pr($$split_order_id); pr($locs);
			 //echo "SELECT sku, quantity FROM order_locations as OrderLocation WHERE order_id IN(".implode(",",$order_id).") AND  bin_location IN ('".implode("','",$locs)."') AND status = 'active' AND pick_list = '0' ";
	 
			$items = $this->OrderLocation->query("SELECT order_id,sku, quantity FROM order_locations as OrderLocation WHERE split_order_id IN('".implode("','",$split_order_id)."') AND  bin_location IN ('".implode("','",$locs)."') AND status = 'active' AND pick_list = '0' ");
			 
 			if(count($items) > 0){
				foreach($items as $m){ 										
					$binlocations[$m['OrderLocation']['sku']][] = $m['OrderLocation']['quantity'];
					//echo 'order id:'.$m['OrderLocation']['order_id'].' sku:'. $m['OrderLocation']['sku'].' Q:'.$m['OrderLocation']['quantity'];
					//echo "<br>";
				}
 				if(count($binlocations) > 0){
					foreach(array_keys($binlocations) as $k){
						$ssku['data'][] = $k .'('.array_sum($binlocations[$k]).')';
					}
				}
			}else{
				$ssku['error'] =  'Sku not found.';
			} 
		}
		else{			 
			$ssku['error'] =  'yes';
		}
		 
  		echo json_encode($ssku);
		exit;
		  
 	} 
	
	public function getBatchOrderStatus($batch_num = 0)
    {
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );		
	  
		$getItems = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.created_batch' => $batch_num),'fields' => array('status','product_order_id_identify','label_status','custom_marked','scan_date','picklist_inc_id','pick_list_status'),'order'=>'status' ) );
 		return $getItems ;
	}
	
	public function getDynamicPicklistUser($inc_id = 0)
    {
		$this->layout = '';
		$this->loadModel( 'DynamicPicklist' );		
	  
		$getItems = $this->DynamicPicklist->find('first', array('conditions' => array('id' => $inc_id),'fields' => array('picking_assign_time','picking_user_email','picking_completion_time','packing_assign_time','packing_user_email','packing_completion_time') ) );
 		return $getItems ;
	}
	
	public function getOrderStatus($picklist_inc_id = 0)
    {
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );		
	 
		$getItems = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.picklist_inc_id' => $picklist_inc_id,'status NOT IN '=>array(0,1) ),'fields' => array('status','custom_marked','product_order_id_identify','label_status'),'order'=>'status' ) );
 		return $getItems ;
	}
	
	public function getAllOrderStatus($picklist_inc_id = 0)
    {
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );		
	 
		$getItems = $this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.picklist_inc_id' => $picklist_inc_id),'fields' => array('status','product_order_id_identify','label_status','scan_status','scan_date','service_name','provider_ref_code','service_provider','brt','delevery_country','postal_service','over_sell_user','custom_marked'),'order'=>'status' ) );
 		return $getItems ;
	}
	
	public function getProcessedOrders($picklist_inc_id = 0)
    {
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );		
	 
		$getItems = $this->MergeUpdate->find('count', array('conditions' => array('MergeUpdate.picklist_inc_id' => $picklist_inc_id,'process_date !='=>'' ) ) );
 		return $getItems ;
	}
	public function getSortedOrders($picklist_inc_id = 0)
    {
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );		
	 
		$getItems = $this->MergeUpdate->find('count', array('conditions' => array('MergeUpdate.picklist_inc_id' => $picklist_inc_id,'scan_date !='=>'' ) ) );
 		return $getItems ;
	}
	
	
	public function getPicklistOrders($picklist_inc_id = 0)
    { 
		$this->layout = 'index';
		$this->loadModel( 'MergeUpdate' );		
		$this->loadModel( 'OrderLocation' );
		$perPage = 50;
		$conditions = array('picklist_inc_id' =>$picklist_inc_id);
		if(isset($_GET['searchkey'])){
			$conditions = array('picklist_inc_id' =>$picklist_inc_id,
			'OR' => array('product_order_id_identify LIKE' =>trim($_GET['searchkey']).'%','sku LIKE' =>'%'.trim($_GET['searchkey']).'%')			
			);
		}
		
		
		$this->paginate = array('conditions' => $conditions,
		'fields' => array('id','order_date','source_coming','custom_marked','batch_num','created_batch','picklist_inc_id',
							'scan_status','sorted_scanned','status','label_status',
							'manifest_status','order_id','product_order_id_identify',
							'sku','quantity','price','service_name','provider_ref_code','service_provider','delevery_country','brt','postal_service',
							'track_id','user_id','picked_date','process_date','scan_date','manifest_date',
							'user_name','pc_name','scanned_user','scanned_pc','cancelled_user','pc_name','over_sell_user'),
		'order' => 'label_status',
		'limit' => $perPage);
		$MergeUpdate = $this->paginate('MergeUpdate');
 		 
		foreach($MergeUpdate as $v){
			$split_orders[] = $v['MergeUpdate']['product_order_id_identify'];
		}
 		$over_sell_orders = []; 
		$getItems = $this->OrderLocation->find('all', array('conditions' => array('split_order_id' => $split_orders,'available_qty_bin' => 0,'status'=>'active'),'fields'=>['split_order_id'] ) );
		if(count($getItems) > 0){
			foreach($getItems as $p){
				$over_sell_orders[] = $p['OrderLocation']['split_order_id'];
			} 
		}
		  		 
		//$this->set( 'MergeUpdate',$MergeUpdate);
		$this->set( compact('MergeUpdate','over_sell_orders'));
		 
		//exit;
		  
 	}
	
	public function searchOrder()
    { 
		$this->layout = 'index';
		$this->loadModel( 'MergeUpdate' );	 
		 
 		$get_item = $this->MergeUpdate->find('first', array('conditions' => array('product_order_id_identify LIKE' => trim($_GET['searchkey']).'%'),
		'fields' => ['order_id','product_order_id_identify','created_batch','picklist_inc_id','over_sell_user']) );
	 
		$url =  Router::url('/', true). "DynamicPicklist/getPicklistOrders/" . $get_item['MergeUpdate']['picklist_inc_id']."?searchkey=" . $get_item['MergeUpdate']['order_id']."&batch=". $get_item['MergeUpdate']['created_batch']."&p=b";
		 
 		header("location:$url");
  		exit;		  
 	}

	
	public function getOrderCurrency($order_id = 0)
    {
		$this->layout = '';
		$this->loadModel( 'OpenOrder' );		

		$currency = '';
		$getItems = $this->OpenOrder->find('first', array('conditions' => array('num_order_id' => $order_id),'fields'=>['totals_info'] ) );
		if(count($getItems) > 0){
			$data = unserialize($getItems['OpenOrder']['totals_info']) ;
			 
			$currency = $data->Currency;
		}
		
 		return $currency;
	}
	
	public function markPickPackCompleted()
    {
		$this->layout = "";	 
		$this->loadModel( 'DynamicPicklist' ); 
		
		if($this->request->data['action'] == 'picking'){
 			$this->DynamicPicklist->updateAll( array('picking_completion_time' => "'".date('Y-m-d H:i:s')."'",'picking_completion_markby' => "'".$this->Session->read('Auth.User.email')."'"),array('id' => $this->request->data['picklist_inc_id']));	
		}else{
			$this->DynamicPicklist->updateAll( array('packing_completion_time' => "'".date('Y-m-d H:i:s')."'",'packing_completion_markby' => "'".$this->Session->read('Auth.User.email')."'"),array('id' => $this->request->data['picklist_inc_id']));	
		}
		
		/*----------Maintain  Logs---------------------*/
		$log_file = WWW_ROOT .'logs/dynamic_log/mark_completed_'.$this->request->data['action'].date('dmy').".log";
		
		if(!file_exists($log_file)){
			file_put_contents($log_file, 'MarkCompletedByEmail'."\t".'Username'."\t".'PcName'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
		}
		file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$this->request->data['picklist_inc_id']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
		
		/*--------End Of Logs-------------*/
		
 		$dy = $this->DynamicPicklist->find('first', array('conditions' => array('id' => $this->request->data['picklist_inc_id'])));
		 
		$timedata = array();
		
		if($this->request->data['action'] == 'picking'){ 			 
			$timedata = $this->calculateTime($dy['DynamicPicklist']['picking_assign_time'],$dy['DynamicPicklist']['picking_completion_time']);
			$completion_time =$dy['DynamicPicklist']['picking_completion_time'];
		}else{
			$timedata = $this->calculateTime($dy['DynamicPicklist']['packing_assign_time'],$dy['DynamicPicklist']['packing_completion_time']);
			$completion_time =$dy['DynamicPicklist']['packing_completion_time'];
		}
		
		$time_taken = '';
		
		if($timedata['days'] > 0) $time_taken =  $timedata['days'] .' Days <br>';
		if($timedata['hours'] > 0)  $time_taken .=  $timedata['hours'] .' Hours<br>';
		if($timedata['minutes'] > 0)  $time_taken .=  $timedata['minutes'] .' Minutes';
		if($timedata['minutes'] == 0)  $time_taken .=  $timedata['seconds'] .' Seconds';
		
		
		echo json_encode(array('completion_time' => $completion_time,'time_taken' => $time_taken ));
			
		 
		exit;
	}
	
	public function markCompleted()
    {
  		$this->layout = "";	 
	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel('OrderLocation');
		$locked = [];
		$canceled = [];
		$open = 0;
		$processed = 0;
		$scan_pending = 0;	
		$over_sell_count = 0;		
		
		$over_sell[] = '121278-1';
			
		$location_orders =	$this->OrderLocation->find('all', array('fields'=>['split_order_id'], 'conditions'  => array( 'picklist_inc_id' => $this->request->data['picklist_inc_id'], 'status' => 'active','pick_list' => 1,'available_qty_bin' => 0 ) ) ); 
		
		if(count($location_orders) > 0 ){
			foreach($location_orders as $l){
				$over_sell[] = $l['OrderLocation']['split_order_id'];
			}			
		}
						
		$getItems = $this->MergeUpdate->find('all', array('conditions' => array('picklist_inc_id' => $this->request->data['picklist_inc_id']),'fields'=>['scan_status','sorted_scanned','manifest_status','status','label_status','product_order_id_identify','over_sell_user','custom_marked']) );
		
		foreach($getItems as $s){
		
			if($s['MergeUpdate']['status'] == 3){
				$locked[] = $s['MergeUpdate']['product_order_id_identify'];
			}else if($s['MergeUpdate']['status'] == 2){
				$canceled[] = $s['MergeUpdate']['product_order_id_identify'];
			}else if($s['MergeUpdate']['status'] == 1){
				$processed++;
			}else if($s['MergeUpdate']['status'] == 0 && $s['MergeUpdate']['over_sell_user'] == '' && !in_array($s['MergeUpdate']['product_order_id_identify'],$over_sell)){
				$open++;
			}else if($s['MergeUpdate']['over_sell_user'] != '' || in_array($s['MergeUpdate']['product_order_id_identify'],$over_sell)){
				$over_sell_count++;
			}
			
			if($s['MergeUpdate']['scan_status'] == 0 && $s['MergeUpdate']['custom_marked'] == 0 && $s['MergeUpdate']['status'] < 2 && $s['MergeUpdate']['over_sell_user'] == '' && !in_array($s['MergeUpdate']['product_order_id_identify'],$over_sell)){
 				$scan_pending++;
 			}
		}
		
		//if($this->request->data['picklist_inc_id'] == '799'){pr($getItems);}
		
		if($open > 0){
 			echo json_encode(array('status'=>'incomplete','open_orders'=>$open,'processed'=>$processed,'locked'=>$locked,'canceled'=>$canceled,'scan_pending'=>$scan_pending,'over_sell'=>$over_sell_count));
 		}else if($scan_pending > 0){
 			echo json_encode(array('status'=>'scan_pending','open_orders'=>$open,'processed'=>$processed,'locked'=>$locked,'canceled'=>$canceled,'scan_pending'=>$scan_pending,'over_sell'=>$over_sell_count));
 		}else{ 
  		
			 $this->DynamicPicklist->updateAll( array('completed_mark_user' => "'".$this->Session->read('Auth.User.email')."'",'completed_date' => "'".date('Y-m-d H:i:s')."'"),array('id' => $this->request->data['picklist_inc_id']));	
			
			/*----------Maintain  Logs---------------------*/
			$log_file = WWW_ROOT .'logs/dynamic_log/mark_completed_'.date('dmy').".log";
			
			if(!file_exists($log_file)){
				file_put_contents($log_file, 'MarkCompletedByEmail'."\t".'Username'."\t".'PcName'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
			}
			 file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$this->request->data['picklist_inc_id']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
			
			/*--------End Of Logs-------------*/
			 
			echo json_encode(array('status'=>'complete','over_sell'=>$over_sell_count));
			
		}
		exit;
	}
	
	public function getSectionLocations($section = null)
    {
		$this->loadModel( 'SectionsRack' );
		$this->loadModel( 'OrderLocation' );  
		
		$order_ids = array(); 
		$locations = array();
		  
		
		if(isset($section) && count($section) > 0 ){
			
			$location = $this->SectionsRack->query("SELECT location FROM sections_racks as SectionsRack WHERE section_name IN('" . implode("','",$section) . "')");
			
			if(count($location) > 0){ 
			 
				foreach($location as $v){
					$locations[] = $v['SectionsRack']['location'];
				}
				 
				$order_loc = $this->OrderLocation->find('all', array('fields'=>array('order_id'),'conditions' => array('pick_list' => 0,'status'=>'active','bin_location IN' =>$locations)));
				
				foreach($order_loc as $v){
					$order_ids[] = $v['OrderLocation']['order_id'];
				}
			}
		}
			 
		return $order_ids;	
			
	}
 	
	public function getInkSkus()
    {
		$this->loadModel( 'InkSku');
 		 
		$ink_data = [];		
		$InkSku = $this->InkSku->find('all');
		foreach( $InkSku as $Ink ){
			$ink_data[]	= $Ink['InkSku']['sku'];
		}
		return $ink_data;
	}
	
	public function picklistPreview()
    {
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'SectionsRack' );
		$this->loadModel( 'OrderLocation' );	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'Product' );		 
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'Country' );
		$this->loadModel( 'InkSku');
		
		$i_sku = 0;
		$ink_data = [];		
		$InkSku = $this->InkSku->find('all');
		foreach( $InkSku as $Ink ){
			$ink_data[]	= $Ink['InkSku']['sku'];
		}
		
  		$order_ids  =  array();
  		$Where      =  array();	
		$orderItems =  array();	 
		$locs =  array();	 
		$floor =  array();	
		$created_batch = ''; 
		$result_data['error'] = '';
		 
		$co = date('s');
		
	
		//$Where[] = " service_name != 'Over Weight' ";
	
		if(isset($this->request->data['sku_type']) && $this->request->data['sku_type']!=''){
		
		$Where[] = "  status = 0 AND pick_list_status = 0 ";				 
		if(isset($this->request->data['batch'])){
			$Where[] = " created_batch = '".$this->request->data['batch']."'";
			$created_batch = $this->request->data['batch']; 
		}
		/*if(isset($this->request->data['postal_country']) && $this->request->data['postal_country']!= ''){
			$Where[] = " delevery_country LIKE '".$this->request->data['postal_country']."'";	
			$co = strtoupper(substr($this->request->data['postal_country'],0,2));		
		}*/
		
		if(isset($this->request->data['postal_country']) && $this->request->data['postal_country'] != ''){
		
			$country_group = strtolower($this->request->data['postal_country']);		
			//$country_group = str_replace(" ", "_", $country_group) ;
			
			$country_grp = $this->Country->query("SELECT custom_name, iso_2 FROM countries as Country WHERE country_group = '".$country_group."' ");
				
			if(count($country_grp) >0){
				foreach($country_grp  as $v){
					$countries_grp[] = $v['Country']['custom_name'];	
					if($v['Country']['custom_name']  == 'Guadeloupe'){
						$countries_grp[] = 'Guadaloupe';
					}				 
				}
				
				$Where[] = " delevery_country IN ('".implode("','",$countries_grp)."')";
			}
			$co = strtoupper(substr($this->request->data['postal_country'],0,3));
			if($this->getCountryCode($this->request->data['postal_country'])!= ''){
				$co = $this->getCountryCode($this->request->data['postal_country']);
			}
		}
		
		if($this->request->data['sku_type']=='single'){
			
			$Where[] = " sku NOT LIKE '%,%'";
			
			if(isset($this->request->data['bin_locations']) && count($this->request->data['bin_locations'])>0){
				
 				foreach($this->request->data['bin_locations'] as $l){
					$bin = trim(explode("[",$l)[0]);
					$f = substr($bin,0,2);
					$floor[$f] = $f ;
					$locs[] = $bin;			
				}	
					
		 		//if(count($floor) == 1){
  					if(isset($this->request->data['postal_services']) && $this->request->data['postal_services']!=''){
						$Where[] = " service_name LIKE '".$this->request->data['postal_services']."' AND service_name != 'Over Weight' ";
					}
			  /* }else{
			  	 $result_data['error'] = 'Please select location of same floor(level) G1 or G2 etc.';
			   }*/	
			 
			}
		 }
		 else if($this->request->data['sku_type'] == 'multiple'){
			$Where[] = " sku LIKE '%,%' AND service_name != 'Over Weight' AND service_name != '' ";
		 }
	}		
		if(count($Where) > 0){				  
		 	
			$sql_query = ''; $where_col ='';
			if(count($Where)>0){
				$w = 1;
				$count_lenght_of_where = count($Where);
				foreach ($Where as $k) {
					$where_col .= $k;
					if ($w < $count_lenght_of_where) {
						$where_col .=' AND ';
					}
					$w++;
				 }
				$sql_query =' WHERE '.$where_col;			
			}	
			
			$sku_condition = '';
			if(isset($this->request->data['skus']) && count($this->request->data['skus']) > 0){
				$sku_condition = '';
				foreach($this->request->data['skus'] as $s){
					$esk = explode("(",$s);
					$sku_condition .= "  sku LIKE  '%".$esk[0] ."%' OR ";
				}
				$sku_condition = rtrim($sku_condition,' OR') ;
			}
			
			if($sku_condition == ''){

				$query = "SELECT order_id,sku,quantity,product_order_id_identify,created_batch FROM merge_updates as MergeUpdate $sql_query";
			}else{
				$query = "SELECT order_id,sku,quantity,product_order_id_identify,created_batch FROM merge_updates as MergeUpdate $sql_query AND (". $sku_condition.")";
			}
			 //  echo $query ;
			
			$orderItems = $this->MergeUpdate->query($query);
			
			$orders = array();
			$split_order_ids = array();
			
			if(count($orderItems) > 0) {
 			 	foreach($orderItems as $v){
				  	$split_order_ids[] = $v['MergeUpdate']['product_order_id_identify']; 					
				}
				
				if(count($locs) > 0){
				
					$pi_sql =  "SELECT `id`,`order_id`, `split_order_id`,`sku`, `barcode`, `bin_location`, `available_qty_bin`, `quantity`, `po_name`, `po_id` FROM order_locations as OrderLocation WHERE split_order_id IN('".implode("','",$split_order_ids)."') AND  bin_location IN ('".implode("','",$locs)."') AND status = 'active' AND pick_list = '0' " ;
					
				}else{
				
			 		$pi_sql = "SELECT `id`,`order_id`, `split_order_id`, `sku`, `barcode`, `bin_location`, `available_qty_bin`, `quantity`, `po_name`, `po_id` FROM order_locations as OrderLocation WHERE split_order_id IN('".implode("','",$split_order_ids)."') AND status = 'active' AND pick_list = '0' ";
				
 				}
  				
 				$pickListItems = $this->OrderLocation->query($pi_sql);
				
				if( count( $pickListItems ) > 0 )
				{
					 
					//manage userd - id with name
					$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
					$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
					$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
					
					$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
					
					$orderids = array(); $data = array(); $tq = 0; $taq = 0;
					foreach( $pickListItems as $val )
					{
						$open_ord = $this->OpenOrder->find('first', ['conditions' => ['num_order_id' => $val['OrderLocation']['order_id']],'fields'=>['destination']] );	
						if($open_ord['OpenOrder']['destination'] == 'United Kingdom' ){
							$inkskus = explode(',', $val['OrderLocation']['sku']);
							if(count($inkskus) > 1){
								foreach($inkskus as $k => $v){
									$x = explode('XS-', $v); 
									if(count($x) > 1){
										$insku = 'S-'.$x[1];
										if(in_array( $insku, $ink_data ) ){
											$i_sku = $i_sku + $x[0]*2;  
										}
									}
								}
							}else {
									$x = explode('XS-', $val['OrderLocation']['sku']); 
									if(count($x) > 1){
										$insku = 'S-'.$x[1];
										if(in_array( $insku, $ink_data ) ){
											$i_sku = $i_sku + 2; 						 
										}
									}						 
							}
						}
						$tq += $val['OrderLocation']['quantity'];	
						$taq += $val['OrderLocation']['available_qty_bin'];	
						$orderids[] = array('order_id'=>$val['OrderLocation']['order_id'],'split_order_id'=>$val['OrderLocation']['split_order_id']);			
						$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
										'sku'=>trim($val['OrderLocation']['sku']),
										'barcode' => $val['OrderLocation']['barcode'],
										'quantity' => $val['OrderLocation']['quantity'],
										'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
					}
								
					/* dome pdf vendor */
					require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
					spl_autoload_register('DOMPDF_autoload'); 
					$dompdf = new DOMPDF();
					$dompdf->set_paper('A4', 'portrait');
					/* Html for print pick list */
					
					date_default_timezone_set('Europe/Jersey');
					$today       = date("d_m_Y_His");					
					$date     	 = date("m/d/Y H:i:s");	
					$finArray = array();
					
					$picklist_barcode = '';
					for($i=0; $i < 13; $i++){
						$picklist_barcode .= mt_rand(0, 9);
					}
					$this->getPicklistBarcode($picklist_barcode);
					
 					$html = '<table border="0" width="100%"><tr><td><h3>PickList :- '.$date.'</h3></td><td style="text-align:right;"><img src="'.WWW_ROOT .'img/printPickList/'.$picklist_barcode.'.png"></td></tr></table><hr>';				
					if($i_sku > 0){
						$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$created_batch.'</h4></td><td><h4 style="text-align:right;">Total Envelope : '.$i_sku.'</h4></td></tr></table>';
					} else {
						$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$created_batch.'</h4></td><td><h4 style="text-align:right;"></h4></td></tr></table>';
					}  
					  
					$html .='<table border="0" width="100%" style="font-size:12px;">';
					$html .='<tr>';
						$html .='<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>';
						$html .='<th style="border:1px solid;" width="10%" align="center">SKU</th>';
						$html .='<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>';
						$html .='<th style="border:1px solid;" width="10%" align="center">Barcode</th>';
					$html .='</tr>';
					
					$data = $this->msortnew($data, array('bin_location'));
					$tdata = array();
					foreach($data as $a){
						$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
												
					}			
					
					foreach($tdata as $sku => $d){
							$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
							foreach($d as $_ar){
								$bn[$_ar['bin_location']][] = $_ar['quantity'];	
								$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
								//$bar[$_ar['barcode']] = $_ar['barcode'];	
								$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
								
								if(count($local_barcode) > 0){
									$bar[$local_barcode[0]] = $local_barcode[0]; 
								}else{
									$bar[$_ar['barcode']] = $_ar['barcode'];
								}
							}
							
							$bins = array_keys($bn);  $oversell = array();
							foreach($bins as $b){
								if($b !=''){
									if(array_sum($bn[$b]) == array_sum($bna[$b])){
										$bin_loc[]= $b."(".array_sum($bna[$b]).")";
									}else{
										$bin_loc[]= $b."(".array_sum($bna[$b]).")";
										$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
									}
								}
								$qty[]= array_sum($bn[$b]);
								$avaqty[]= array_sum($bna[$b]);
								
							}	
							$oversell_str = '';
							if(count($oversell) > 0){
								$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
							}
							$pr_sql = "SELECT `product_name` FROM products as Product WHERE product_sku = '".$sku."'";
							$products = $this->Product->query($pr_sql);
							 
							$sku_title = '';		
							if(count($products) > 0){
								$sku_title = substr($products[0]['Product']['product_name'], 0, 38 );
							}
												
							$html.=	'<tr>
								<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
								<td style="border:1px solid;">'.$sku.'</td>
								<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$sku_title.'</td>
								<td style="border:1px solid;">'.implode(",",$bar).'</td>							
								</tr>';
								
					} 
					$rand = '';
					for($i=0; $i < 3; $i++){
						$rand .= mt_rand(0, 9);
					}
					
					$base_name	 = $today.'_'.$rand.'_'.$co;
					
  					if(file_exists( WWW_ROOT .'logs/dynamic_picklist/'.$base_name.'.pdf')){
						$base_name	 = $today.'_R'.$rand.'_'.$co .'T';
					}
					
					$html .= '</table>';	 		 
					$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
					$dompdf->render();
					
					$html .= '<div class="col-md-12 text-right" style="padding-top:10px;"><button type="button" class="btn btn-success" id="'.$base_name.'" onclick=generatePickList("'.$base_name.'")>Generate Picklist</button</div>';	
					
					$name = $base_name.'.pdf';
					file_put_contents( WWW_ROOT .'logs/dynamic_picklist/'.$name, $dompdf->output());
					 
					$pickdata['picklist_name'] = $name;
					$pickdata['pc_name'] = $pcName;
					$pickdata['picklist_type'] = $this->request->data['sku_type'];
					$pickdata['sku_count'] = $tq;
					$pickdata['order_count'] = count($orderids);
					$pickdata['created_username'] = $firstName .' '. $lastName;
					$pickdata['batch'] = $this->request->data['batch'];
					$pickdata['picklist_barcode'] = $picklist_barcode;
					$pickdata['country'] =  $this->request->data['postal_country'];
					$pickdata['service_name'] =  $this->request->data['postal_services'];
					$pickdata['created_date'] = date('Y-m-d H:i:s');
					
					file_put_contents(WWW_ROOT.'logs/dynamic_picklist/'.$base_name.'.txt', json_encode($pickdata));	
					file_put_contents(WWW_ROOT.'logs/dynamic_picklist/'.$base_name.'_ord.txt', json_encode($orderids)); 
					file_put_contents(WWW_ROOT.'logs/dynamic_picklist/'.$base_name.'_batch.txt', $this->request->data['batch']);
					file_put_contents(WWW_ROOT.'logs/dynamic_picklist/'.$base_name.'_split_orders.txt', json_encode($split_order_ids));		
					 
					
				}else{
					$html = '<br><center>No orders available for pick list!</center>'; 
					$base_name = '';
					//$this->redirect($this->referer());
				}
				  
				//return array('html' => $html,'base_name' => $base_name); 
		
   				//$pick_data = $this->picklistPreview($orders, $split_order_ids, $this->request->data['sku_type'],$this->request->data['batch'] );
				
				$result_data['preview']   =  utf8_encode($html);
				
				$result_data['base_name'] = $base_name;
				//$this->Session->setflash( 'Pick list generated!', 'flash_success'); 
			}else{
				$result_data['preview'] ='No data is available for pick list!'; 
				//$this->redirect($this->referer());				 
			}
		}else{
 			$result_data['preview'] = 'No orders are available for pick list!'; 
 		}
		
 		echo json_encode($result_data);
 		exit;
	} 
	
 	public function generatePicklist(){
		
		$this->layout = '';
		$this->autoRander = false;			
		$this->loadModel( 'OrderLocation' );
		$this->loadModel( 'DynamicPicklist' );		
		$this->loadModel( 'MergeUpdate' );	 	
		$this->loadModel( 'Product' );		 
		$this->loadModel( 'OpenOrder' );
		$data['result'] = '';
		if(isset($this->request->data['picklist_name']) && $this->request->data['picklist_name']!= ''){
			$base_name = WWW_ROOT .'logs/dynamic_picklist/'.$this->request->data['picklist_name'];
			if(file_exists($base_name.'.txt')){
				$orderids = []; $split_order_id = [];
				$pickdata = json_decode(file_get_contents($base_name.'.txt'), true); 
				$orders   = json_decode(file_get_contents($base_name.'_ord.txt'), true);   
				$batch    = json_decode(file_get_contents($base_name.'_batch.txt'), true);    
 				if(count($orders) > 0){
					foreach($orders as $v){
						//$orderids[] = $v['order_id'];
						$split_order_id[] = $v['split_order_id'];
					}
				}
 				
  				if(count($pickdata) > 0){
					@copy($base_name.'.pdf', WWW_ROOT.'img/printPickList/'.$this->request->data['picklist_name'].'.pdf');
					@unlink($base_name.'.pdf');
					
					$this->DynamicPicklist->saveAll($pickdata);
					
					$picklist_inc_id = $this->DynamicPicklist->getLastInsertId();
					
					if(count($split_order_id) > 0){
  					
 						$this->MergeUpdate->updateAll(array('pick_list_status' => '1','picklist_inc_id' => $picklist_inc_id,'picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('product_order_id_identify' => $split_order_id,'status' => 0,'pick_list_status' => 0));
						
						$this->OrderLocation->updateAll(array('pick_list' => '1','picklist_inc_id' => $picklist_inc_id,'batch' => $batch,'picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('split_order_id'=> $split_order_id, 'status'=>'active', 'pick_list'=> 0));
						 
						
						$data['result'] = 'Picklist generated!'; 
						$data['status'] = 'ok'; 
						
						/*----------Maintain  Logs---------------------*/
						
						file_put_contents( WWW_ROOT .'logs/dynamic_log/pp_'.date('dmy').".log", print_r($split_order_id,true), FILE_APPEND|LOCK_EX);	
						
						$log_file = WWW_ROOT .'logs/dynamic_log/create_picklist_'.date('dmy').".log";
						
						if(!file_exists($log_file)){
							file_put_contents($log_file, 'Username'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
						}
						file_put_contents($log_file,$this->Session->read('Auth.User.username')."\t".$picklist_inc_id."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
						/*--------End Of Logs-------------*/
		
					}else{
						$data['result'] = 'No orders available for pick list!'; 
						$data['status'] = 'no';
					}
				}
			}
		}else{
			$data['result'] = 'Picklist data not available!';
			$data['status'] = 'no';
		}
 		
 		
 		echo json_encode($data);
		exit;
	}
	
	public function generateCountryPicklist(){
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' );   
   	 
		$single_sku_orders =  array();	 
		$multi_sku_orders  =  array();	 
		$s_data = $m_data  =  array(); 		 
		$result_data =  array(); 		
  		 
		if(isset($this->request->data['orders']) && $this->request->data['orders']!=''){
			
			$order_ids = $this->request->data['orders'];
			
			$split_order_ids = explode(",", $order_ids);
			
			 $query = "SELECT order_id,sku,product_order_id_identify FROM merge_updates as MergeUpdate WHERE service_name != 'Over Weight' AND product_order_id_identify IN('".implode("','",$split_order_ids)."') AND status = '0' AND pick_list_status = '0' ";
			$items = $this->MergeUpdate->query($query);	 
			if(count($items) > 0){		
				foreach($items as $v){
 					$skus = explode( ',', $v['MergeUpdate']['sku']);
					if(count($skus) == 1){
						$single_sku_orders[] = $v['MergeUpdate']['product_order_id_identify'];
					}else{
						$multi_sku_orders[] = $v['MergeUpdate']['product_order_id_identify'];
					}					
				}	
				if(count($single_sku_orders) > 0){
					$s_data = $this->createCountryPicklist($this->request->data,$single_sku_orders,'single');	
				}
				if(count($multi_sku_orders) > 0){
					$m_data = $this->createCountryPicklist($this->request->data,$multi_sku_orders,'multiple');	
				}
			}else{
				$result_data['msg'] = 'No orders is available for pick list!'; 						 
			}		 
			
		}
		if(count($s_data) > 0){
			$result_data['single'] = $s_data;
		}
		if(count($m_data) > 0){
			$result_data['multi'] = $m_data;
		}
  		 
  		echo json_encode($result_data);
 		exit;
	 
		 
	}
	
	public function createCountryPicklist($data = [], $split_order_ids = [], $pl_type = NULL){
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'SectionsRack' );
		$this->loadModel( 'OrderLocation' );	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'Product' );		 
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'InkSku');
		
		$i_sku = 0;
		$ink_data = [];		
		$InkSku = $this->InkSku->find('all');
		foreach( $InkSku as $Ink ){
			$ink_data[]	= $Ink['InkSku']['sku'];
		}
   	 
  		$batch      =  '';	
		$country	=  '';
		$service 	=  '';
		$order_ids  =  '';
		$orderItems =  array();	 		 
		$result_data['error'] = '';
		 
		
		if(count($data) > 0){		
			$this->request->data = $data;
		}
		
		 if(isset($this->request->data['batch']) && $this->request->data['batch']!=''){
			$batch = $this->request->data['batch'];
		 }
		 if(isset($this->request->data['country']) && $this->request->data['country']!=''){
			$country = $this->request->data['country'];
		 }
		 if(isset($this->request->data['service']) && $this->request->data['service']!=''){
			$service = $this->request->data['service'];
		 }
		 
  		 
		if(count($split_order_ids) > 0){
 			
 		 	$pi_sql = "SELECT `id`,`order_id`, `split_order_id`, `sku`, `barcode`, `bin_location`, `available_qty_bin`, `quantity`, `po_name`, `po_id` FROM order_locations as OrderLocation WHERE split_order_id IN('".implode("','",$split_order_ids)."') AND status = 'active' AND pick_list = '0' ";
				
			$pickListItems = $this->OrderLocation->query($pi_sql);
			
			if( count( $pickListItems ) > 0 )
			{
 				//manage userd - id with name
				$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
				$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
				$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
				
				$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
				
			    $data = array(); $tq = 0; $taq = 0;
				foreach( $pickListItems as $val )
				{
					$open_ord = $this->OpenOrder->find('first', ['conditions' => ['num_order_id' => $val['OrderLocation']['order_id']],'fields'=>['destination']] );	
					if($open_ord['OpenOrder']['destination'] == 'United Kingdom' ){
						$inkskus = explode(',', $val['OrderLocation']['sku']);
						if(count($inkskus) > 1){
							foreach($inkskus as $k => $v){
								$x = explode('XS-', $v); 
								if(count($x) > 1){
									$insku = 'S-'.$x[1];
									if(in_array( $insku, $ink_data ) ){
										$i_sku = $i_sku + $x[0]*2;  
									}
								}
							}
						}else {
							$x = explode('XS-', $val['OrderLocation']['sku']); 
							if(count($x) > 1){
								$insku = 'S-'.$x[1];
								if(in_array( $insku, $ink_data ) ){
									$i_sku = $i_sku + 2; 						 
								}
							}
						}
					}
					
					$tq += $val['OrderLocation']['quantity'];	
					$taq += $val['OrderLocation']['available_qty_bin'];	
					 		
					$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
									'sku'=>trim($val['OrderLocation']['sku']),
									'barcode' => $val['OrderLocation']['barcode'],
									'quantity' => $val['OrderLocation']['quantity'],
									'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
				}
							
				/* dome pdf vendor */
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				/* Html for print pick list */
				date_default_timezone_set('Europe/Jersey');
				
				$today    = date("d_m_Y_His");
				$date     = date("m/d/Y H:i:s");	
				$finArray = array();
				
 				$picklist_barcode = '';
				for($i=0; $i < 13; $i++){
					$picklist_barcode .= mt_rand(0, 9);
				}
				$this->getPicklistBarcode($picklist_barcode);
			
				$html = '<table border="0" width="100%"><tr><td><h3>PickList :- '.$date.'</h3></td><td style="text-align:right;"><img src="'.WWW_ROOT .'img/printPickList/'.$picklist_barcode.'.png"></td></tr></table><hr>';
								
				if($i_sku > 0){
					$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch.'</h4></td><td><h4 style="text-align:right;">Total Envelope : '.$i_sku.'</h4></td></tr></table>';
				} else {
					$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch.'</h4></td><td><h4 style="text-align:right;"></h4></td></tr></table>';
				} 
				  
				$html .='<table border="0" width="100%" style="font-size:12px;">';
				$html .='<tr>';
					$html .='<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>';
					$html .='<th style="border:1px solid;" width="10%" align="center">SKU</th>';
					$html .='<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>';
					$html .='<th style="border:1px solid;" width="10%" align="center">Barcode</th>';
				$html .='</tr>';
				
				$data = $this->msortnew($data, array('bin_location'));
				$tdata = array();
				foreach($data as $a){
					$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
											
				}			
				
				foreach($tdata as $sku => $d){
					$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
					foreach($d as $_ar){
						$bn[$_ar['bin_location']][] = $_ar['quantity'];	
						$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
						//$bar[$_ar['barcode']] = $_ar['barcode'];	
						$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
						
						if(count($local_barcode) > 0){
							$bar[$local_barcode[0]] = $local_barcode[0]; 
						}else{
							$bar[$_ar['barcode']] = $_ar['barcode'];
						}
					}
					
					$bins = array_keys($bn);  $oversell = array();
					foreach($bins as $b){
						if($b !=''){
							if(array_sum($bn[$b]) == array_sum($bna[$b])){
								$bin_loc[]= $b."(".array_sum($bna[$b]).")";
							}else{
								$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
							}
						}
						$qty[]= array_sum($bn[$b]);
						$avaqty[]= array_sum($bna[$b]);
						
					}	
					$oversell_str = '';
					if(count($oversell) > 0){
						$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
					}
					
					
					$pr_sql = "SELECT `product_name` FROM products as Product WHERE product_sku = '".$sku."'";
 					$products = $this->Product->query($pr_sql);
					 
 					$sku_title = '';		
					if(count($products) > 0){
						$sku_title = substr($products[0]['Product']['product_name'], 0, 40 );
					}
										
					$html.=	'<tr>
						<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
						<td style="border:1px solid;">'.$sku.'</td>
						<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$sku_title.'</td>
						<td style="border:1px solid;">'.implode(",",$bar).'</td>							
						</tr>';
						
				} 
				$co = strtoupper(substr($pl_type,0,3));	
				
				$rand = '';
				for($i=0; $i < 3; $i++){
					$rand .= mt_rand(0, 9);
				}
			 
				$base_name	 = $today .'_' . $rand.'_'.$co;
				
				if(file_exists( WWW_ROOT .'img/printPickList/'.$base_name.'.pdf')){
					$base_name = $today.'_R'.$rand.'_'.$co .'T';
				}
				
				$html .= '</table>';	
						 
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				
  				$name = $base_name.'.pdf';
				
				file_put_contents( WWW_ROOT .'img/printPickList/'.$name, $dompdf->output());
				 
				$pickdata['picklist_name'] = $name;
				$pickdata['pc_name'] = $pcName;
				$pickdata['picklist_type'] = $pl_type;
				$pickdata['sku_count'] = $tq;
				$pickdata['order_count'] = count($split_order_ids);
				$pickdata['created_username'] = $firstName .' '. $lastName;
				$pickdata['batch'] = $batch;
				$pickdata['picklist_barcode'] = $picklist_barcode;
				$pickdata['country'] = $country;
				$pickdata['service_name'] = $service;
				$pickdata['created_from'] = 'batch_page';
				$pickdata['created_date'] = date('Y-m-d H:i:s');
				
				$this->DynamicPicklist->saveAll($pickdata);
 				$picklist_inc_id = $this->DynamicPicklist->getLastInsertId();
   					
 				$this->MergeUpdate->updateAll(array('pick_list_status' => '1','picklist_inc_id' => $picklist_inc_id,'picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('product_order_id_identify' => $split_order_ids,'status' => 0,'pick_list_status' => 0));
				
				$this->OrderLocation->updateAll(array('pick_list' => '1','picklist_inc_id' => $picklist_inc_id,'batch' => $batch,'picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('split_order_id' => $split_order_ids, 'status'=>'active', 'pick_list'=> 0));
				
 					
 				/*----------Maintain  Logs---------------------*/
				
				file_put_contents( WWW_ROOT .'logs/dynamic_log/pp_'.date('dmy').$pl_type.".log", print_r($split_order_ids,true), FILE_APPEND|LOCK_EX);	
				
				$log_file = WWW_ROOT .'logs/dynamic_log/create_picklist_'.date('dmy').".log";
				
				if(!file_exists($log_file)){
					file_put_contents($log_file, 'Username'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
				}
				file_put_contents($log_file,$this->Session->read('Auth.User.username')."\t".$picklist_inc_id."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
				/*--------End Of Logs-------------*/
		
					$result_data['status'] 	= 'ok'; 
					$result_data['pl_name'] = $name; 
					$result_data['msg'] 	= 'Pick list generated for '. $pl_type;  
				}
				else{
					$result_data['msg'] = 'No orders founds for pick list!'; 
 				} 		
  		}else{
			$result_data['msg'] ='No orders is available for pick list!'; 
						 
		}
		 
  		return $result_data;	 
	}
	
	public function createCustomPicklist($option = NULL){
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'OrderLocation' );	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'Product' );		 
		$this->loadModel( 'DynamicBatche' );
		$this->loadModel( 'OpenOrder');
		$this->loadModel( 'InkSku');
		date_default_timezone_set('Europe/Jersey');
		 
   		$all_order = array();
		$param = array();  		 
		$orderItems =  array(); 
		$ink_data = [];
		$option = 'express';
		$InkSku = $this->InkSku->find('all');
		
		foreach( $InkSku as $Ink ){
			$ink_data[]	=	$Ink['InkSku']['sku'];
		}
 	 	 
 		$split_order_ids = array();
		$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id','product_order_id_identify','delevery_country','sku'),'conditions' => array('MergeUpdate.status' => '0','MergeUpdate.pick_list_status' => '0','MergeUpdate.postal_service' => 'Express')));	
		$i_sku = 0;
		if(count($orderItems ) > 0){
			foreach($orderItems as $val){
			
				$split_order_ids[] = $val['MergeUpdate']['product_order_id_identify'];
				
				if( $val['MergeUpdate']['delevery_country'] == 'United Kingdom' ){
					$inkskus = explode(',', $val['OrderLocation']['sku']);
					if(count($inkskus) > 1){
						foreach($inkskus as $k => $v){
						foreach($inkskus as $k => $v){
							$x = explode('XS-', $v); 
							if(count($x) > 1){
								$insku = 'S-'.$x[1];
								if(in_array( $insku, $ink_data ) ){
									$i_sku = $i_sku + $x[0]*2;  
								}
							}
						}
					}
					}else  {
						$x = explode('XS-', $val['OrderLocation']['sku']); 
						if(count($x) > 1){
							$insku = 'S-'.$x[1];
							if(in_array( $insku, $ink_data ) ){
								$i_sku = $i_sku + 2; 						 
							}
						}				 
					}
				}
			}
		}
		 //pr($orderItems);
		 
		if(count($split_order_ids)){
			$param = array('conditions' => array('pick_list' => '0','status' => 'active','split_order_id' => $split_order_ids),'order' => 'bin_location ASC');	
		}else{						 
			$this->Session->setflash( 'No UK Express orders found!', 'flash_danger'); 
		}	  
	 
 		 
  		 //pr($orderItems);
		if(isset($param)){
			$pickListItems = $this->OrderLocation->find('all', $param );	
			
 			if( count( $pickListItems ) > 0 )
			{
			
				$batch_num = date('YmdHis');
				$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
				$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
				$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
				$_username = $firstName .' '. $lastName ;
			
  				//manage userd - id with name
				$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
				$picklist_username = $firstName .' '. $lastName;
				$orderids = array(); $split_order_ids =[]; $data = array(); $tq = 0; $taq = 0;
				foreach( $pickListItems as $val )
				{
					$tq += $val['OrderLocation']['quantity'];	
					$taq += $val['OrderLocation']['available_qty_bin'];	
					$orderids[] = $val['OrderLocation']['order_id'];	
					$split_order_ids[] = $val['OrderLocation']['split_order_id'];		
					$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																		'sku'=>trim($val['OrderLocation']['sku']),
																		'barcode' => $val['OrderLocation']['barcode'],
																		'quantity' => $val['OrderLocation']['quantity'],
																		'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
				}
				
							
				/* dome pdf vendor */
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				/* Html for print pick list */
				
				$today   = date("d_m_Y_His");
				$date	 = date("m/d/Y H:i:s");	
				$finArray = array();
				
				$picklist_barcode = '';
				for($i=0; $i < 13; $i++){
					$picklist_barcode .= mt_rand(0, 9);
				}
				$this->getPicklistBarcode($picklist_barcode);
			
				$html = '<table border="0" width="100%"><tr><td><h3>PickList :- '.$date.'</h3></td><td style="text-align:right;"><img src="'.WWW_ROOT .'img/printPickList/'.$picklist_barcode.'.png"></td></tr></table><hr>';
		 
				
				if($i_sku > 0){
					$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;">Total Envelope : '.$i_sku.'</h4></td></tr></table>';
				} else {
					$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;"></h4></td></tr></table>';
				}
				
				$html .= '<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
						<tr>
						<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
						<th style="border:1px solid;" width="10%" align="center">SKU</th>
						<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
						<th style="border:1px solid;" width="10%" align="center">Barcode</th>
						</tr>';
				
				$data = $this->msortnew($data, array('bin_location'));
				$tdata = array();
				foreach($data as $a){
					$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
											
				}			
				
				foreach($tdata as $sku => $d){
						$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
						foreach($d as $_ar){
							$bn[$_ar['bin_location']][] = $_ar['quantity'];	
							$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
							//$bar[$_ar['barcode']] = $_ar['barcode'];	
							$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
							
							if(count($local_barcode) > 0){
								$bar[$local_barcode[0]] = $local_barcode[0]; 
							}else{
								$bar[$_ar['barcode']] = $_ar['barcode'];
							}
						}
						
						$bins = array_keys($bn);  $oversell = array();
						foreach($bins as $b){
							if($b !=''){
								if(array_sum($bn[$b]) == array_sum($bna[$b])){
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								}else{
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
									$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
								}
							}
							$qty[]= array_sum($bn[$b]);
							$avaqty[]= array_sum($bna[$b]);
							
						}	
						$oversell_str = '';
						if(count($oversell) > 0){
							$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
						}
						
						
						$pr_sql = "SELECT `product_name` FROM products as Product WHERE product_sku = '".$sku."'";
						$products = $this->Product->query($pr_sql);
						 
						$sku_title = '';		
						if(count($products) > 0){
							$sku_title = substr($products[0]['Product']['product_name'], 0, 40 );
						}
 											
						$html.=	'<tr>
							<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
							<td style="border:1px solid;">'.$sku.'</td>
							<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$sku_title.'</td>
							<td style="border:1px solid;">'.implode(",",$bar).'</td>							
							</tr>';
							
				}	
			
				$html .= '</table>';			 
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
 				  			
 				//$dompdf->stream("Pick_List_(".$date.").pdf");
				
				$imgPath = WWW_ROOT .'img/printPickList/'; 
				$path = Router::url('/', true).'img/printPickList/';
				$name = strtoupper($option).'_'.$today.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				 
				$db_data = [] ; $pickdata = []; 
				if(count($orderids) > 0){
  				 						
					$db_data['batch_name'] 		= $batch_num;
					$db_data['pc_name'] 		= $pcName;
					$db_data['order_count'] 	= count( $orderids );
					$db_data['creater_name'] 	= $_username;
					$db_data['batch_type'] 		= $option;
					$db_data['created_date'] 	= date('Y-m-d H:i:s');
					$this->DynamicBatche->saveAll($db_data);
 				
					$pickdata['picklist_name'] = $name;
					$pickdata['pc_name'] = $pcName;
					$pickdata['picklist_type'] = 'mixed';
					$pickdata['sku_count'] = $tq;
					$pickdata['order_count'] = count($orderids);
					$pickdata['created_username'] = $firstName .' '. $lastName;
					$pickdata['batch'] = $batch_num;
					$pickdata['picklist_barcode'] = $picklist_barcode;
					$pickdata['country'] = $option;
					$pickdata['service_name'] = $option;
					$pickdata['created_from'] = 'batch_page';
					$pickdata['created_date'] = date('Y-m-d H:i:s');
					
					$this->DynamicPicklist->saveAll($pickdata);
					$picklist_inc_id = $this->DynamicPicklist->getLastInsertId();
				
				
 					$this->MergeUpdate->updateAll(array('MergeUpdate.picklist_username' => "'".$picklist_username."'", 'MergeUpdate.pick_list_status' => '1','MergeUpdate.created_batch' => $batch_num,'MergeUpdate.picklist_inc_id' => $picklist_inc_id,'MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.order_id' => $orderids,'MergeUpdate.product_order_id_identify' => $split_order_ids,'MergeUpdate.status' => 0));
					
					$this->OrderLocation->updateAll(array('pick_list' => '1','batch' => $batch_num,'picklist_inc_id' => $picklist_inc_id,'picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('order_id' => $orderids,'split_order_id' => $split_order_ids, 'status'=>'active'));
					 
 				}
 				 	
				$folderPath = WWW_ROOT .'logs';				 	
				$_Path = $folderPath .'/'. $option.'_'.date('dmy_Hi').'_PicklistPC.log';    				  
				file_put_contents($_Path, $username."\n".implode( "," , $orderids ));
					  
 				//now abort file status down
				 
 				header('Content-Encoding: UTF-8');
				header('Content-type: text/csv; charset=UTF-8');
				header('Content-Disposition: attachment;filename="'.$name.'"');
				header("Content-Type: application/octet-stream;");
				header('Cache-Control: max-age=0');
				readfile($imgPath.$name);
				 
				//exit;
					
			}else{
				//$result_data['msg'] = 'No orders are available for pick list!'; 	
				$this->Session->setflash( 'No orders are available for <strong>'. strtoupper($option).'</strong> pick list!', 'flash_danger'); 
					 
			}
		}else{
			//$result_data['msg'] = 'Please selected skus before to generate pick list or choose other option!'; 	
			$this->Session->setflash( 'Please selected skus before to generate pick list or choose other option!', 'flash_danger'); 
				
		}
		 $this->redirect($this->referer());	
  		//return $result_data;	 
	}
	
	public function inkSkuPicklist($batch_num = NULL){
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'OrderLocation' );	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'Product' );		 
		$this->loadModel( 'DynamicBatche' );
		$this->loadModel( 'OpenOrder');
		$this->loadModel( 'InkSku');
		
   		$split_order_ids = array();
		$param = array();  		 
		$orderItems =  array(); 
		$ink_data = [];
		$option = 'ink';
		
		$InkSku = $this->InkSku->find('all');
		foreach( $InkSku as $Ink ){
			$ink_data[]	=	$Ink['InkSku']['sku'];
		}
 	 	 
 		$all_order = array(); $oids = [];
		$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('order_id','product_order_id_identify','delevery_country','sku'),'conditions' => array('MergeUpdate.created_batch' => $batch_num ,'MergeUpdate.status' => '0','MergeUpdate.pick_list_status' => '0','MergeUpdate.delevery_country' => 'United Kingdom')));	
		$i_sku = 0; $oids = [];
		if(count($orderItems ) > 0){
			foreach($orderItems as $val){
			 
				$inkskus = explode(',', $val['MergeUpdate']['sku']);
				if(count($inkskus) > 1){
					foreach($inkskus as $k => $v){
						$x = explode('XS-', $v); 

						if(count($x) > 1){
							$insku = 'S-'.$x[1];
							if(in_array( $insku, $ink_data ) ){
								$i_sku = $i_sku + $x[0]*2;  
								$split_order_ids[$val['MergeUpdate']['product_order_id_identify']] = $val['MergeUpdate']['product_order_id_identify'];
								$oids[$val['MergeUpdate']['order_id']] = $val['MergeUpdate']['order_id'];
							}
						}
					}
				}else{
					$x = explode('XS-', $val['MergeUpdate']['sku']); 
					if(count($x) > 1){
						$insku = 'S-'.$x[1];
						if(in_array( $insku, $ink_data ) ){
							$i_sku = $i_sku + 2; 	
							$split_order_ids[$val['MergeUpdate']['product_order_id_identify']] = $val['MergeUpdate']['product_order_id_identify'];	
							$oids[$val['MergeUpdate']['order_id']] = $val['MergeUpdate']['order_id'];	 
						}
					}
				}			 
			}
		}
		 
		 
		if(count($split_order_ids)){
			$param = array('conditions' => array('pick_list' => '0','status' => 'active','split_order_id' => $split_order_ids),'order' => 'bin_location ASC');	
		}else{						 
			$this->Session->setflash( 'No INK SKU orders found!', 'flash_danger'); 
		}	  
		
		if(isset($param)){
		
			$pickListItems = $this->OrderLocation->find('all', $param );	
			 
 			if( count( $pickListItems ) > 0 )
			{
 				 
				$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
				$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
				$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
				$_username = $firstName .' '. $lastName ;
			
  				//manage userd - id with name
				$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
				$picklist_username = $firstName .' '. $lastName;
				$split_order_ids = array(); $data = array(); $tq = 0; $taq = 0;
				foreach( $pickListItems as $val )
				{
					$tq += $val['OrderLocation']['quantity'];	
					$taq += $val['OrderLocation']['available_qty_bin'];	
					$split_order_ids[] = $val['OrderLocation']['split_order_id'];			
					$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																		'sku'=>trim($val['OrderLocation']['sku']),
																		'barcode' => $val['OrderLocation']['barcode'],
																		'quantity' => $val['OrderLocation']['quantity'],
																		'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
				}
				
 				/* dome pdf vendor */
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				/* Html for print pick list */
				date_default_timezone_set('Europe/Jersey');
				$today    = date("d_m_Y_His");
				$date	  = date("m/d/Y H:i:s");	
				$finArray = array();
				
				$picklist_barcode = '';
				for($i=0; $i < 13; $i++){
					$picklist_barcode .= mt_rand(0, 9);
				}
				
				$this->getPicklistBarcode($picklist_barcode);
			
				$html = '<table border="0" width="100%"><tr><td><h3>PickList :- '.$date.'</h3></td><td style="text-align:right;"><img src="'.WWW_ROOT .'img/printPickList/'.$picklist_barcode.'.png"></td></tr></table><hr>';
		 
				
				if(count($oids) > 0){
					$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;">Total Envelope : '.( count($oids) * 2).'</h4></td></tr></table>';
				} else {
					$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;"></h4></td></tr></table>';
				}
				
				$html .= '<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
						<tr>
						<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
						<th style="border:1px solid;" width="10%" align="center">SKU</th>
						<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
						<th style="border:1px solid;" width="10%" align="center">Barcode</th>
						</tr>';
				
				$data = $this->msortnew($data, array('bin_location'));
				$tdata = array();
				foreach($data as $a){
					$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
											
				}			
				
				foreach($tdata as $sku => $d){
						$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
						foreach($d as $_ar){
							$bn[$_ar['bin_location']][] = $_ar['quantity'];	
							$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
							//$bar[$_ar['barcode']] = $_ar['barcode'];	
							$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
							
							if(count($local_barcode) > 0){
								$bar[$local_barcode[0]] = $local_barcode[0]; 
							}else{
								$bar[$_ar['barcode']] = $_ar['barcode'];
							}
						}
						
						$bins = array_keys($bn);  $oversell = array();
						foreach($bins as $b){
							if($b !=''){
								if(array_sum($bn[$b]) == array_sum($bna[$b])){
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								}else{
									$bin_loc[]= $b."(".array_sum($bna[$b]).")";
									$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
								}
							}
							$qty[]= array_sum($bn[$b]);
							$avaqty[]= array_sum($bna[$b]);
							
						}	
						$oversell_str = '';
						if(count($oversell) > 0){
							$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
						}
						
						
						$pr_sql = "SELECT `product_name` FROM products as Product WHERE product_sku = '".$sku."'";
						$products = $this->Product->query($pr_sql);
						 
						$sku_title = '';		
						if(count($products) > 0){
							$sku_title = substr($products[0]['Product']['product_name'], 0, 40 );
						}
 											
						$html.=	'<tr>
							<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
							<td style="border:1px solid;">'.$sku.'</td>
							<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$sku_title.'</td>
							<td style="border:1px solid;">'.implode(",",$bar).'</td>							
							</tr>';
							
				}	
			
				$html .= '</table>';			 
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
 				  			
 				 
				//$dompdf->stream("Pick_List_(".$date.").pdf");
				
				$imgPath = WWW_ROOT .'img/printPickList/'; 
				$path = Router::url('/', true).'img/printPickList/';
				$name = strtoupper($option).'_'.$today.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				 
				$pickdata = []; 
				if(count($split_order_ids) > 0){
  				  
 					$pickdata['picklist_name'] = $name;
					$pickdata['pc_name'] = $pcName;
					$pickdata['picklist_type'] = 'mixed';
					$pickdata['sku_count'] = $tq;
					$pickdata['order_count'] = count($split_order_ids);
					$pickdata['created_username'] = $firstName .' '. $lastName;
					$pickdata['batch'] = $batch_num;
					$pickdata['picklist_barcode'] = $picklist_barcode;
					$pickdata['country'] = $option;
					$pickdata['service_name'] = $option;
					$pickdata['created_from'] = 'batch_page';
					$pickdata['created_date'] = date('Y-m-d H:i:s');
					
					$this->DynamicPicklist->saveAll($pickdata);
					$picklist_inc_id = $this->DynamicPicklist->getLastInsertId();
				
				
 					$this->MergeUpdate->updateAll(array('MergeUpdate.picklist_username' => "'".$picklist_username."'", 'MergeUpdate.pick_list_status' => '1','MergeUpdate.created_batch' => $batch_num,'MergeUpdate.picklist_inc_id' => $picklist_inc_id,'MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.product_order_id_identify' => $split_order_ids));
					$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','batch' => $batch_num,'picklist_inc_id' => $picklist_inc_id,'OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.split_order_id' => $split_order_ids));
					 
				}
 				 	
				$folderPath = WWW_ROOT .'logs';					    
				$_Path = $folderPath .'/'. $option.'_'.date('dmy_Hi').'_PicklistPC.log';  				  
				file_put_contents($_Path, $username."\n".implode( "," , $split_order_ids ));
					  
 				//now abort file status down
				 
 				header('Content-Encoding: UTF-8');
				header('Content-type: text/csv; charset=UTF-8');
				header('Content-Disposition: attachment;filename="'.$name.'"');
				header("Content-Type: application/octet-stream;");
				header('Cache-Control: max-age=0');
				readfile($imgPath.$name);
				 
				//exit;
					
			}else{
				//$result_data['msg'] = 'No orders are available for pick list!'; 	
				$this->Session->setflash( 'No orders are available for <strong>'. strtoupper($option).'</strong> pick list!', 'flash_danger'); 
					 
			}
		}else{
			//$result_data['msg'] = 'Please selected skus before to generate pick list or choose other option!'; 	
			$this->Session->setflash( 'No INK SKU orders found!', 'flash_danger'); 
				
		}
		 $this->redirect($this->referer());	
  		//return $result_data;	 
	}
	
	public function createManulPicklist(){
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'OrderLocation' );	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'Product' );		 
		$this->loadModel( 'DynamicBatche' ); 
		$this->loadModel( 'OpenOrder');
		$this->loadModel( 'InkSku');
   	 	
		$ink_data = [];		
		$InkSku = $this->InkSku->find('all');
		foreach( $InkSku as $Ink ){
			$ink_data[]	= $Ink['InkSku']['sku'];
		}
		
		 // die('die');
		 		 
		$option =  'manual'; 

  	 	$split_orders =['2754809-1','2754810-1','2754811-1','2754814-1','2754818-1','2754822-1','2754823-1','2754824-1','2754825-1','2754826-1','2754832-1','2754833-1','2754836-1','2754837-1','2754839-1','2754841-1','2754885-1','2754887-1','2754888-1','2754894-1','2754895-1','2754896-1','2754899-1','2754900-1','2754902-1','2754903-1','2754910-1','2754911-1','2754913-1','2754914-1','2754915-1','2754917-1','2754918-1','2754920-1','2754921-1','2754922-1','2754925-1','2754926-1','2754983-1','2754984-1','2754986-1','2754988-1','2754992-1','2754993-1','2755026-1','2755027-1','2755028-1','2755029-1','2755031-1','2755033-1','2755034-1','2755036-1','2755045-1','2755049-1','2755051-1','2755052-1','2755053-1','2755055-1','2755056-1','2755058-1','2755059-1','2755061-1','2755062-1','2755063-1','2755069-1','2755074-1','2755075-1','2755121-1','2755122-1','2755123-1','2755124-1','2755127-1','2755128-1','2755129-1','2755132-1','2755134-1','2755135-1','2755136-1','2755137-1','2755139-1','2755141-1','2755142-1','2755143-1','2755144-1','2755145-1','2755146-1','2755149-1','2755211-1','2755212-1','2755215-1','2755217-1','2755218-1','2755219-1','2755220-1','2755223-1','2755224-1','2755225-1','2755226-1','2755227-1','2755259-1','2755261-1','2755263-1','2755271-1','2755312-1','2755313-1','2755317-1','2755318-1','2755319-1','2755321-1','2755323-1','2755324-1','2755329-1','2755330-1','2755331-1','2755332-1','2755335-1','2755336-1','2755337-1','2755338-1','2755370-1','2755371-1','2755374-1','2755375-1','2755380-1','2755386-1','2755414-2','2755415-1','2755417-1','2755418-1','2755422-1','2755423-1','2755424-1','2755426-1','2755449-1','2755450-1','2755454-1','2755461-1','2755462-1','2755464-1','2755466-1','2755467-1','2755510-1','2755511-1','2755512-1','2755513-1','2755514-1','2755516-1','2755518-1','2755521-1','2755547-1','2755549-1','2755550-1','2755570-1','2755573-1','2755575-1','2755576-1','2755578-1','2755579-1','2755604-1','2755620-1','2755621-1','2755623-1','2755700-1','2755704-1','2755706-1','2755707-1','2755711-1','2755712-1','2755713-1','2755736-1','2755737-1','2755742-1','2755751-1','2755755-1','2755774-1','2755778-1','2755785-1','2755787-1','2755792-1','2755796-1','2755801-1'
		]; 
 
		 
		$param = array('conditions' => array('status' => 'active','split_order_id' => $split_orders),'order' => 'bin_location ASC');	
				
  		$pickListItems = $this->OrderLocation->find('all', $param );	
		 
		
		if( count( $pickListItems ) > 0 )
		{
		
			$batch_num = date('YmdHis');
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
			$_username = $firstName .' '. $lastName ;
		
							 
			//manage userd - id with name
			$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
			$picklist_username = $firstName .' '. $lastName;
			$orderids = array(); $split_order_ids = []; $data = array(); $tq = 0; $taq = 0;
			foreach( $pickListItems as $val )
			{
				$open_ord = $this->OpenOrder->find('first', ['conditions' => ['num_order_id' => $val['OrderLocation']['order_id']],'fields'=>['destination']] );	
				if($open_ord['OpenOrder']['destination'] == 'United Kingdom' ){
					$inkskus = explode(',', $val['OrderLocation']['sku']);
					if(count($inkskus) > 1){
						foreach($inkskus as $k => $v){
							$x = explode('XS-', $v); 
							if(count($x) > 1){
								$insku = 'S-'.$x[1];
								if(in_array( $insku, $ink_data ) ){
									$i_sku = $i_sku + $x[0]*2;  
								}
							}
						}
					}else  {
						$x = explode('XS-', $val['OrderLocation']['sku']); 
						if(count($x) > 1){
							$insku = 'S-'.$x[1];
							if(in_array( $insku, $ink_data ) ){
								$i_sku = $i_sku + 2; 						 
							}
						}
					}
				}
				
				$tq += $val['OrderLocation']['quantity'];	
				$taq += $val['OrderLocation']['available_qty_bin'];	
				$orderids[] = $val['OrderLocation']['order_id'];
				$split_order_ids[] = $val['OrderLocation']['split_order_id'];			
				$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																	'sku'=>trim($val['OrderLocation']['sku']),
																	'barcode' => $val['OrderLocation']['barcode'],
																	'quantity' => $val['OrderLocation']['quantity'],
																	'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
			}
			
						
			/* dome pdf vendor */
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();
			$dompdf->set_paper('A4', 'portrait');
			/* Html for print pick list */
			date_default_timezone_set('Europe/Jersey');
			$today   = date("d_m_Y_H_i_s");
			$date	 = date("m/d/Y H:i:s");	
			$finArray = array();
			$picklist_barcode = '';
				for($i=0; $i < 13; $i++){
					$picklist_barcode .= mt_rand(0, 9);
				}
				$this->getPicklistBarcode($picklist_barcode);
			
				$html = '<table border="0" width="100%"><tr><td><h3>PickList :- '.$date.'</h3></td><td style="text-align:right;"><img src="'.WWW_ROOT .'img/printPickList/'.$picklist_barcode.'.png"></td></tr></table><hr>';
 		 
				if($i_sku > 0){
					$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;">Total Envelope : '.$i_sku.'</h4></td></tr></table>';
				} else {
					$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;"></h4></td></tr></table>';
				}
			 
			
			$html .= '<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
					<tr>
					<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
					<th style="border:1px solid;" width="10%" align="center">SKU</th>
					<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
					<th style="border:1px solid;" width="10%" align="center">Barcode</th>
					</tr>';
			
			$data = $this->msortnew($data, array('bin_location'));
			$tdata = array();
			foreach($data as $a){
				$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
										
			}			
			
			foreach($tdata as $sku => $d){
					$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
					foreach($d as $_ar){
						$bn[$_ar['bin_location']][] = $_ar['quantity'];	
						$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
						//$bar[$_ar['barcode']] = $_ar['barcode'];	
						$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
						
						if(count($local_barcode) > 0){
							$bar[$local_barcode[0]] = $local_barcode[0]; 
						}else{
							$bar[$_ar['barcode']] = $_ar['barcode'];
						}
					}
					
					$bins = array_keys($bn);  $oversell = array();
					foreach($bins as $b){
						if($b !=''){
							if(array_sum($bn[$b]) == array_sum($bna[$b])){
								$bin_loc[]= $b."(".array_sum($bna[$b]).")";
							}else{
								$bin_loc[]= $b."(".array_sum($bna[$b]).")";
								$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
							}
						}
						$qty[]= array_sum($bn[$b]);
						$avaqty[]= array_sum($bna[$b]);
						
					}	
					$oversell_str = '';
					if(count($oversell) > 0){
						$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
					}
					
					
					$pr_sql = "SELECT `product_name` FROM products as Product WHERE product_sku = '".$sku."'";
					$products = $this->Product->query($pr_sql);
					 
					$sku_title = '';		
					if(count($products) > 0){
						$sku_title = substr($products[0]['Product']['product_name'], 0, 40 );
					}
										
					$html.=	'<tr>
						<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
						<td style="border:1px solid;">'.$sku.'</td>
						<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$sku_title.'</td>
						<td style="border:1px solid;">'.implode(",",$bar).'</td>							
						</tr>';
						
			}	
		
			$html .= '</table>';			 
			$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
			$dompdf->render();						 
			
			$imgPath = WWW_ROOT .'img/printPickList/'; 
			$path = Router::url('/', true).'img/printPickList/';
			$name = strtoupper($option).'_'.$today.'.pdf';
			
			file_put_contents($imgPath.$name, $dompdf->output());
			
			$db_data = [] ; $pickdata = []; 
			if(count($orderids) > 0){
									
				$db_data['batch_name'] 		= $batch_num;
				$db_data['pc_name'] 		= $pcName;
				$db_data['order_count'] 	= count( $orderids );
				$db_data['creater_name'] 	= $_username;
				$db_data['batch_type'] 		= $option;
				$db_data['created_date'] 	= date('Y-m-d H:i:s');
			 	$this->DynamicBatche->saveAll($db_data);
				//$batch_num = '20191007050731'; 
				
				$pickdata['picklist_name'] = $name;
				$pickdata['pc_name'] = $pcName;
				$pickdata['picklist_type'] = 'mixed';
				$pickdata['sku_count'] = $tq;
				$pickdata['order_count'] = count($orderids);
				$pickdata['created_username'] = $firstName .' '. $lastName;
				$pickdata['batch'] = $batch_num;
				$pickdata['picklist_barcode'] = $picklist_barcode;
				$pickdata['country'] = $option;
				$pickdata['service_name'] = $option;
				$pickdata['created_from'] = 'batch_page';
				$pickdata['created_date'] = date('Y-m-d H:i:s'); 
				
				$this->DynamicPicklist->saveAll($pickdata);
				$picklist_inc_id = $this->DynamicPicklist->getLastInsertId();
			
			
				$this->MergeUpdate->updateAll(array('MergeUpdate.picklist_username' => "'".$picklist_username."'", 'MergeUpdate.pick_list_status' => '1','MergeUpdate.created_batch' => $batch_num,'MergeUpdate.picklist_inc_id' => $picklist_inc_id,'MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.order_id' => $orderids,'MergeUpdate.product_order_id_identify' => $split_order_ids,'MergeUpdate.status' => 0));
				 //
				$this->OrderLocation->updateAll(array('pick_list' => '1','batch' => $batch_num,'picklist_inc_id' => $picklist_inc_id,'picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('order_id' => $orderids,'split_order_id' => $split_order_ids, 'status'=>'active'));
			 
 			}
				
			$folderPath = WWW_ROOT .'logs';				  
			$_Path = $folderPath .'/'. $option.'_'.date('dmy_Hi').'_PicklistPC.log';    				  
			file_put_contents($_Path, $username."\n".implode( "," , $orderids ));
				  
			//now abort file status down
			 
			header('Content-Encoding: UTF-8');
			header('Content-type: text/csv; charset=UTF-8');
			header('Content-Disposition: attachment;filename="'.$name.'"');
			header("Content-Type: application/octet-stream;");
			header('Cache-Control: max-age=0');
			readfile($imgPath.$name);
			 
			//exit;
				
		}else{
			//$result_data['msg'] = 'No orders are available for pick list!'; 	
			$this->Session->setflash( 'No orders are available for <strong>'. strtoupper($option).'</strong> pick list!', 'flash_danger'); 
				 
		}
		 
		// $this->redirect($this->referer());	
  		//return $result_data;	 
	}
	public function markPicked(){
   		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'OrderLocation' );	 
		$this->loadModel( 'DynamicPicklist' ); 
		$this->loadModel( 'Product' ); 
		
		$split_order_id = $_REQUEST['searchkey'];
		$batch_num = $_REQUEST['batch']; 
		
   		$pickLists = $this->DynamicPicklist->find('first', array('conditions' => array('batch' => $batch_num,'picklist_type' =>'mixed','completed_mark_user IS NULL' ),'order' => 'id DESC') );
 		
		 
 		$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
		$_username = $firstName .' '. $lastName ;
	
		//manage userd - id with name
		$username = $firstName .' '. $lastName .'( ' . $pcName . ' )';
		$picklist_username = $firstName .' '. $lastName;
		date_default_timezone_set('Europe/Jersey');
		$option  = 'custom';		
		$today   = date("d_m_Y_H_i_s");
		$name 	 = strtoupper($option).'_'.$today.'.pdf';
			
		
		$pickdata['pc_name'] = $pcName;
		$pickdata['picklist_type'] = 'mixed';		
		$pickdata['created_username'] = $firstName .' '. $lastName;
		$pickdata['batch'] = $batch_num;		
		$pickdata['country'] = $option;
		$pickdata['service_name'] = $option;
		$pickdata['created_from'] = 'batch_page';
		$pickdata['created_date'] = date('Y-m-d H:i:s');
			
		if( count( $pickLists ) > 0 )
		{
			$name = $pickLists['DynamicPicklist']['picklist_name'];
			$picklist_barcode = $pickLists['DynamicPicklist']['picklist_barcode'];
			$sku_count = $pickLists['DynamicPicklist']['sku_count'] + 1;
			$order_count = $pickLists['DynamicPicklist']['order_count'] + 1;
 			
			$pickdata['picklist_name'] = $name;
			$pickdata['sku_count'] = $sku_count;			
			$pickdata['order_count'] = $order_count;			
			$pickdata['id'] = $pickLists['DynamicPicklist']['id'];
			$picklist_inc_id = $pickLists['DynamicPicklist']['id'];  
		 	$this->DynamicPicklist->saveAll($pickdata);
		}else{
			
			$picklist_barcode = '';
			for($i=0; $i < 13; $i++){
				$picklist_barcode .= mt_rand(0, 9);
			}
			$pickdata['picklist_name'] = $name;
			$pickdata['sku_count'] = 1;
			$pickdata['order_count'] = 1;
		 	$pickdata['picklist_barcode'] = $picklist_barcode;
			$this->DynamicPicklist->saveAll($pickdata);
			$picklist_inc_id = $this->DynamicPicklist->getLastInsertId();  
 		}
		
		
		$this->MergeUpdate->updateAll(array('MergeUpdate.picklist_username' => "'".$picklist_username."'", 'MergeUpdate.pick_list_status' => '1','MergeUpdate.created_batch' => $batch_num,'MergeUpdate.picklist_inc_id' => $picklist_inc_id,'MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.product_order_id_identify' => $split_order_id));
		
		$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','batch' => $batch_num,'picklist_inc_id' => $picklist_inc_id,'OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.split_order_id' => $split_order_id));
		
 		$param = array('conditions' => array('status' => 'active','batch' => $batch_num,'picklist_inc_id' => $picklist_inc_id,'split_order_id IS NOT NULL'),'order' => 'bin_location ASC');	
				
  		$pickListItems = $this->OrderLocation->find('all', $param );
		 
 		$orderids = array(); $data = array(); $tq = 0; $taq = 0;
		
		foreach( $pickListItems as $val )
		{
			$tq += $val['OrderLocation']['quantity'];	
			$taq += $val['OrderLocation']['available_qty_bin'];	
			$orderids[] = $val['OrderLocation']['order_id'];			
			$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],
																'sku'=>trim($val['OrderLocation']['sku']),
																'barcode' => $val['OrderLocation']['barcode'],
																'quantity' => $val['OrderLocation']['quantity'],
																'available_qty_bin' => $val['OrderLocation']['available_qty_bin']);
		}
			
						
		/* dome pdf vendor */
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper('A4', 'portrait');
		/* Html for print pick list */
		 
		$date	 = date("m/d/Y H:i:s");	
		$finArray = array();
	
		$this->getPicklistBarcode($picklist_barcode);
		
		$html = '<table border="0" width="100%"><tr><td><h3>PickList :- '.$date.'</h3></td><td style="text-align:right;"><img src="'.WWW_ROOT .'img/printPickList/'.$picklist_barcode.'.png"></td></tr></table><hr>';
		
		$html .= '<table border="0" width="100%"><tr><td><h4>Total Item  : '.$tq.'</h4></td><td><h4 style="text-align:right;">Batch : '.$batch_num.'</h4></td><td><h4 style="text-align:right;"></h4></td></tr></table>';
		 
		
		$html .= '<table border="0" width="100%" style="font-size:12px; margin-top:10px;">
				<tr>
				<th style="border:1px solid;" width="12%" align="center">Bin/Rack</th>
				<th style="border:1px solid;" width="10%" align="center">SKU</th>
				<th style="border:1px solid;" width="20%" align="center">Qty / Item Title</th>
				<th style="border:1px solid;" width="10%" align="center">Barcode</th>
				</tr>';
		
		$data = $this->msortnew($data, array('bin_location'));
		$tdata = array();
		foreach($data as $a){
			$tdata[$a['sku']][] = array('bin_location'=>$a['bin_location'],'barcode'=>$a['barcode'],'quantity'=>$a['quantity'],'available_qty_bin'=>$a['available_qty_bin']);	
									
		}			
		
		foreach($tdata as $sku => $d){
				$bn = array(); $bna = array();  $bar = array(); $bin_loc = array();  $qty = array();  $avaqty = array(); 
				foreach($d as $_ar){
					$bn[$_ar['bin_location']][] = $_ar['quantity'];	
					$bna[$_ar['bin_location']][] = $_ar['available_qty_bin'];	
					//$bar[$_ar['barcode']] = $_ar['barcode'];	
					$local_barcode = $this->GlobalBarcode->getAllBarcode($_ar['barcode']);
					
					if(count($local_barcode) > 0){
						$bar[$local_barcode[0]] = $local_barcode[0]; 
					}else{
						$bar[$_ar['barcode']] = $_ar['barcode'];
					}
				}
				
				$bins = array_keys($bn);  $oversell = array();
				foreach($bins as $b){
					if($b !=''){
						if(array_sum($bn[$b]) == array_sum($bna[$b])){
							$bin_loc[]= $b."(".array_sum($bna[$b]).")";
						}else{
							$bin_loc[]= $b."(".array_sum($bna[$b]).")";
							$oversell[] = (array_sum($bn[$b]) - array_sum($bna[$b]));
						}
					}
					$qty[]= array_sum($bn[$b]);
					$avaqty[]= array_sum($bna[$b]);
					
				}	
				$oversell_str = '';
				if(count($oversell) > 0){
					$oversell_str =	'<br>Over Sell('.array_sum($oversell).')';
				}
				
				
				$pr_sql = "SELECT `product_name` FROM products as Product WHERE product_sku = '".$sku."'";
				$products = $this->Product->query($pr_sql);
				 
				$sku_title = '';		
				if(count($products) > 0){
					$sku_title = substr($products[0]['Product']['product_name'], 0, 40 );
				}
									
				$html.=	'<tr>
					<td style="border:1px solid;">'.implode("<br>",$bin_loc). $oversell_str .'</td>
					<td style="border:1px solid;">'.$sku.'</td>
					<td style="border:1px solid;" align="left"><b>'.array_sum($qty).'</b> X '.$sku_title.'</td>
					<td style="border:1px solid;">'.implode(",",$bar).'</td>							
					</tr>';
					
		}	
	
		$html .= '</table>';			 
		$dompdf->load_html($html, Configure::read('App.encoding'));
		$dompdf->render();						 
		
		$imgPath = WWW_ROOT .'img/printPickList/'; 
		$path = Router::url('/', true).'img/printPickList/'.$name;
		 
 		file_put_contents($imgPath.$name, $dompdf->output());
		$folderPath = WWW_ROOT .'logs';				 
		$_Path = $folderPath .'/'. $option.'_'.date('dmy_Hi').'_PicklistPC.log';     				  
		file_put_contents($_Path, $username."\n".implode( "," , $orderids ));
  		 $this->redirect($this->referer());	
 		exit; 
	}
  	public function msortnew($array, $key, $sort_flags = SORT_REGULAR) {
	
	  if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) { //pr($v);
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
			        // @TODO This should be fixed, now it will be sorted as string
                   foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
		    asort($mapping, $sort_flags);
			natsort($mapping);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
	}
	
 	/*----------Bulk Pocessing----------*/
 
		public function MobileAsseInstruction( $splitOrderId  = null, $subsource = null)
		{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'Product' );
			//$splitOrderId 	= '1366785-1';
			$subsource = 'Marec_uk';
			$get_country = array('Marec_FR'=>'FR','Marec_DE'=>'DE','Marec_IT'=>'IT','Marec_ES'=>'ES','Marec_uk'=>'UK');
			$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.product_order_id_identify' => $splitOrderId)));
			$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
			foreach( $skus as $sku )
				{
					$newSkus[] = explode( 'XS-', $sku);
					foreach($newSkus as $newSku)
						{
							$getsku = 'S-'.$newSku[1];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$contentSubCat[]	=	$getOrderDetail['Product']['sub_category'];
						}
							unset($newSkus);
				}
			
			$get_source = $get_country[$subsource];
			$Ins_html	=	$this->getMobileAssHtml( $get_source,$contentSubCat );
			require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
			spl_autoload_register('DOMPDF_autoload'); 
			$dompdf = new DOMPDF();
			$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');
			$cssPath = WWW_ROOT .'css/';
			//echo $Ins_html;
			//$dompdf->load_html($Ins_html, Configure::read('App.encoding'));
			$dompdf->load_html(utf8_decode($Ins_html), Configure::read('App.encoding'));
			$dompdf->render();
			$imgPath = WWW_ROOT .'img/printPDF/Instruction/'; 
			$path = Router::url('/', true).'img/printPDF/Instruction/';
			$name	=	'instruction_Slip_'.$splitOrderId.'.pdf';
			file_put_contents($imgPath.$name, $dompdf->output());
			$serverPath  	= 	$path.$name ;
			$printerId		=	'219842';
			
			
			$sendData = array(
				'printerId' => $printerId,
				'title' => 'Now Printing',
				'contentType' => 'pdf_uri',
				'content' => $serverPath,
				'source' => 'Direct'					
			);
			App::import( 'Controller' , 'Coreprinters' );
			$Coreprinter = new CoreprintersController();
			$d = $Coreprinter->toPrint( $sendData );
			
			//pr($d); exit;
		}
		
		public function getMobileAssHtml($get_source = null, $contentSubCat = null)
		{
			if($get_source == 'UK')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>BATTERY - </strong>This must be used in combination with a Genuine Cable, and Genuine Charger</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>CABLE - </strong>This must be used in combination with  a Genuine Charger, and Genuine Cable</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>CHARGER - </strong>This must be used in combination with a Genuine Battery, and Genuine Cable</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>EARPHONE/OTHER ACCESSORY - </strong>This is optimised for use on the original manufacturer equipment</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Bulk Packaged item</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >WHAT IS BULK PACKAGING ?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Your item is a Bulk Packaged item and is 100% authentic and brand new without a wasteful retail packaging.</li>
				<li>Bulk-packaged items are simply provided without retail packaging to offer additional costs savings to you.</li>
				<li>All items dispatched are 100% authentic, brand new items sourced from major mobile accessory distributors across the EU.</li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>IMPORTANT INFORMATION REGARDING YOUR ITEM</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">When using this item; it must be used in combination with other Genuine items.<br></td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">If you have any query, question, or concern please contact us immediately on below given details </td></tr>
				<tr><td style="text-align:center;" >Customer support:- +44-3301240338</td></tr>
				<tr><td style="text-align:center;" >Email:- support@ebuyer-express.com</td></tr>
				</table>';
			}
			if($get_source == 'FR')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Batterie - </strong>ceci doit �tre employ� en combinaison avec un c�ble v�ritable, et le chargeur v�ritable</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>C�ble - </strong>ceci doit �tre employ� en combinaison avec un vrai chargeur, et le c�ble v�ritable</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Chargeur - </strong>ceci doit �tre employ� en combinaison avec une batterie v�ritable, et le c�ble v�ritable</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>�couteurs/autre accessoire - </strong>ceci est optimis� pour l\'utilisation sur l\'�quipement original de fabricant</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-45px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Article emball� en vrac</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >Qu\'est-ce que l\'emballage en vrac?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Votre article est un article emball� en vrac et est 100% authentique et flambant neuf sans un emballage de d�tail de gaspillage.</li>
				<li>Les articles emball�s en vrac sont simplement fournis sans emballage de d�tail pour vous offrir des �conomies suppl�mentaires.</li>
				<li>Tous les articles exp�di�s sont des articles authentiques et neufs de 100% provenant de grands distributeurs d\'accessoires mobiles � travers l\'UE.</li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>Informations importantes concernant votre article</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Lors de l\'utilisation de cet �l�ment; il doit �tre utilis� en combinaison avec d\'autres objets authentiques.</td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Si vous avez n\'importe quelle requ�te, question, ou pr�occupation s\'il vous pla�t contactez-nous imm�diatement sur les d�tails ci-dessous donn�s-</td></tr>
				<tr><td style="text-align:center;" >Support � la client�le - +44-3301240338</td></tr>
				<tr><td style="text-align:center;" >email - support@ebuyer-express.com</td></tr>
				</table>';
			}
			if($get_source == 'DE')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Batterie - </strong>dies muss in Kombination mit einem echten Kabel und echtem Ladeger�t verwendet werden</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Kabel - </strong>dies muss in Kombination mit einem echten Ladeger�t und echtem Kabel verwendet werden</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Ladeger�t - </strong>dies muss in Kombination mit einer echten Batterie und echtem Kabel verwendet werden</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Ohrh�rer/anderes Zubeh�r - </strong>das ist f�r den Einsatz auf der Original-Hersteller Ausstattung optimiert</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Sch�ttgut verpackt</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >Was ist sch�ttgutverpackung ?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Ihr Artikel ist ein sch�ttgutverpacktes Produkt und ist 100% authentisch und nagelneu ohne verschwenderische Einzelhandelsverpackungen.</li>
				<li>Sch�ttgutverpackungen werden einfach ohne Einzelhandelsverpackung zur Verf�gung gestellt, um Ihnen zus�tzliche Kosteneinsparungen zu bieten.</li>
				<li>Alle versandten Artikel sind 100% authentische, brandneue Artikel, die von gro�en mobilen Zubeh�r H�ndlern in der EU stammen.</li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>Wichtige Informationen zu Ihrem Artikel</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Bei der Verwendung dieses Artikels; Es muss in Kombination mit anderen echten Gegenst�nden verwendet werden.</td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Wenn Sie Fragen, Fragen oder Bedenken haben, kontaktieren Sie uns bitte sofort unter den angegebenen Details � </td></tr>
				<tr><td style="text-align:center;" >Kundendienst - 44-3301240338</td></tr>
				<tr><td style="text-align:center;" >e-Mail - support@ebuyer-express.com</td></tr>
				</table>';
			}
			if($get_source == 'IT')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Batteria - </strong>questo deve essere usato in combinazione con un cavo genuino, e caricabatterie genuino</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Cavo- </strong>questo deve essere usato in combinazione con un caricabatterie genuino, e cavo genuino</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Caricabatterie - </strong>questo deve essere utilizzato in combinazione con una batteria genuina, e cavo genuino</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Auricolare/altro accessorio - </strong>questo � ottimizzato per l\'uso sull\'attrezzatura originale del produttore</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Articolo imballato sfuso</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >Che cosa � imballaggio alla rinfusa ?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Il vostro articolo � un articolo impaccato alla rinfusa ed � 100% autentico e brandnew senza uno spreco che impacca al minuto.</li>
				<li>Gli articoli impacchettati in massa sono forniti semplicemente senza imballaggi al dettaglio per offrire risparmi aggiuntivi ai costi.</li>
				<li>Tutti gli articoli spediti sono 100% autentici, nuovissimi prodotti provenienti dai principali distributori di accessori mobili in tutta l\'UE. </li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>Informazioni importanti sul tuo articolo</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Quando si utilizza questo elemento; deve essere usato in combinazione con altri oggetti genuini.</td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Se avete qualunque richiesta, domanda, o preoccupazione prego se li mette in contatto con immediatamente sui particolari dati sotto-</td></tr>
				<tr><td style="text-align:center;" >Assistenza clienti - +44-3301240338</td></tr>
				<tr><td style="text-align:center;" >email - support@ebuyer-express.com</td></tr>
				</table>';
			}
			if($get_source == 'ES')
			{
				$cont = '';
				foreach($contentSubCat as $contentCat)
				{
					if($contentCat == 'Battery'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Bater�a - </strong>esto se debe utilizar conjuntamente con un cable genuino, y el cargador genuino</td></tr>';
					}
					if($contentCat == 'Cable' || $contentCat == 'USB Cables'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Cable - </strong>esto se debe utilizar conjuntamente con un cargador genuino, y el cable genuino</td></tr>';
					}
					if($contentCat =='Charger'){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Cargador - </strong>esto se debe utilizar conjuntamente con una bater�a genuina, y el cable genuino</td></tr>';
					}
					if($cont ==''){
						$cont .= '<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;" ><strong>Auricular/otro accesorio - </strong>esto se optimiza para el uso en el equipo original del fabricante</td></tr>';
					}
				}
				$cont = '<table style="width: 300px; margin-left:-35px; margin-top:-40px; padding:10px; border:1px solid gray;" >
				<tr><td><h2 style="text-align:center; font-size:17px;">Art�culo empaquetado bulto</h2></td></tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" >� Qu� es el empaquetado a granel ?</td></tr>
				<tr>
				<td>
				<ul style="font-size: 12px; text-align: justify;" >
				<li>Su art�culo es un art�culo empaquetado bulto y es el 100% aut�ntico y a estrenar sin un empaquetado al por menor derrochador.</li>
				<li>Los art�culos empaquetados a granel se proporcionan simplemente sin el empaquetado al por menor para ofrecer ahorros adicionales de los costes para usted.</li>
				<li>Todos los art�culos enviados son 100% aut�nticos, nuevos art�culos de origen de los principales distribuidores de accesorios m�viles en toda la UE.</li>
				</ul>
				</td>
				</tr>
				<tr><td style="font-size: 13px; font-weight: bold; padding-left: 10px;" ><center>Informaci�n importante sobre su art�culo</center></td></tr>
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Al usar este art�culo; debe ser utilizado en combinaci�n con otros art�culos genuinos.</td></tr>'.$cont.'
				<tr><td style="font-size: 12px; text-align: justify; padding-left: 25px;">Si usted tiene cualquier consulta, pregunta, o preocupaci�n por favor p�ngase en contacto con nosotros inmediatamente a continuaci�n detalles dados - </td></tr>
				<tr><td style="text-align:center;" >Soporte al cliente - +44-3301240338</td></tr>
				<tr><td style="text-align:center;" >email - support@ebuyer-express.com</td></tr>
				</table>';
			}
			return $cont;
		}
   
	
	 public function generatePicklistSlipLabel($picklist_id = 0)
	 {
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'OrderLocation' );
		
  		if(isset($this->request->data['picklist_inc_id'])){ 
			$picklist_id = $this->request->data['picklist_inc_id'];
		}
  		
	  // 	$query = "SELECT order_id,sku,barcode,quantity,product_order_id_identify FROM merge_updates as MergeUpdate WHERE picklist_inc_id = '".$picklist_id."' AND status = 0 AND over_sell_user IS NULL ";   //AND label_status < 2 
		
		 
		$query = "SELECT order_id,sku,barcode,quantity,product_order_id_identify FROM merge_updates as MergeUpdate WHERE picklist_inc_id = '".$picklist_id."' AND over_sell_user IS NULL ";  
		 
  		$orderItems = $this->MergeUpdate->query($query);
		$ord_msg = [];
		$data = array();
		
		if(count($orderItems) > 0){
		
			foreach($orderItems as $v){
			 
 				$_items = $this->OrderLocation->query("SELECT `bin_location`,`sku`,`available_qty_bin` FROM `order_locations` as OrderLocation WHERE split_order_id = '".$v['MergeUpdate']['product_order_id_identify']."' AND order_id = '".$v['MergeUpdate']['order_id']."' AND status = 'active'");
				
				foreach($_items as $val )
				{		
					if($val['OrderLocation']['available_qty_bin'] > 0){
						$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],'split_order_id'=>$v['MergeUpdate']['product_order_id_identify'],'quantity'=>$v['MergeUpdate']['quantity']);
					}else{
						$ord_msg[] = $val['OrderLocation']['sku'] .' is over sell.';
					}
				}			 
			}
			
			if(count($data) > 0){
				$data = $this->msortnew($data, array('bin_location'));
				 
				foreach( $data as $order )
				{				
					//sleep(2);   
					//echo $order['split_order_id'];echo ' ='.$order['bin_location']; echo "<br>";
					$ord_msg[] = $this->genrateSlipLabel($order['split_order_id'],$picklist_id);	 				
					$log_file = WWW_ROOT .'logs/dynamic_log/gen_slip_label_request_'.$picklist_id.".log";			 
					file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$order['split_order_id']."\t".$order['bin_location']."\t".$order['quantity']."\t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
				} 
			}
	 
  		}
  	
 		$log_file = WWW_ROOT .'logs/dynamic_log/gen_slip_label_user_'.date('dmy').".log";
		if(!file_exists($log_file)){
			file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
		}
		
 		file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$picklist_id."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
		
		$msg['msg'] = 'sss';
		if(count($ord_msg) > 0){
			$msg['msg'] = implode(",",$ord_msg);
		}
		echo json_encode($msg);
		//echo "1";
		exit;	
		
	} 
	public function batchPicklistSlipLabel($picklist_id = 0)
	 {
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'OrderLocation' );
		
  		if(isset($this->request->data['picklist_inc_id'])){ 
			$picklist_id = $this->request->data['picklist_inc_id'];
		}
  		
	   	$query = "SELECT order_id,sku,barcode,quantity,product_order_id_identify FROM merge_updates as MergeUpdate WHERE picklist_inc_id = '".$picklist_id."' AND status = 0 AND over_sell_user IS NULL ";   //AND label_status < 2 
		
		if(in_array($picklist_id,array(7762,7761,7682,7624,7623,7587,7582,7544,7539,7509,7501,7475,7474,7452))){ 
			$query = "SELECT order_id,sku,barcode,quantity,product_order_id_identify FROM merge_updates as MergeUpdate WHERE picklist_inc_id = '".$picklist_id."' AND over_sell_user IS NULL ";  
		}
		
		if($picklist_id == '7582'){ 
			$query = "SELECT order_id,sku,barcode,quantity,product_order_id_identify FROM merge_updates as MergeUpdate WHERE picklist_inc_id = '".$picklist_id."' AND over_sell_user IS NULL ";   
		}
			 
 		$orderItems = $this->MergeUpdate->query($query);
		$ord_msg = [];
		$data = array();
		
		if(count($orderItems) > 0){
		
			foreach($orderItems as $v){
			 
 				$_items = $this->OrderLocation->query("SELECT `bin_location` FROM `order_locations` as OrderLocation WHERE split_order_id = '".$v['MergeUpdate']['product_order_id_identify']."' AND order_id = '".$v['MergeUpdate']['order_id']."' AND status = 'active' AND available_qty_bin > 0");
				
				foreach($_items as $val )
				{					 
					$data[] = array('bin_location' => $val['OrderLocation']['bin_location'],'split_order_id'=>$v['MergeUpdate']['product_order_id_identify']);
				}			 
			}
			 
			$data = $this->msortnew($data, array('bin_location'));
			
 			foreach( $data as $order )
			{				
				sleep(2);   
				//echo $order['split_order_id'];echo ' ='.$order['bin_location']; echo "<br>";
				$ord_msg[] = $this->printSlipLabel($order['split_order_id'],$picklist_id);					
				$log_file = WWW_ROOT .'logs/dynamic_log/print_slip_label_request_'.$picklist_id.".log";			 
				file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$order['split_order_id']."\t".$order['bin_location']."\t".$order['quantity']."\t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
			} 
	 
  		}
  	
 		$log_file = WWW_ROOT .'logs/dynamic_log/print_slip_label_user_'.date('dmy').".log";
		if(!file_exists($log_file)){
			file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
		}
		
 		file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$picklist_id."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
		
		$msg['msg'] ='';
		if(count($ord_msg) > 0){
			$msg['msg'] = implode(",",$ord_msg);
		}
		echo json_encode($msg);
		//echo "1";
		exit;	
		
	}
	
	public function printSlipLabel($split_order_id = null,$picklist_id = 0 )
	{
		if(isset($this->request->data['split_order_id'])){ 
			$split_order_id = $this->request->data['split_order_id'];
		}
		
		$msg = $this->getSlipLabel($split_order_id, $picklist_id);		
		 
		return $msg;
	}
	
	public function genSlipLabel($split_order_id = null,$picklist_id = 0 )
	{
		if(isset($this->request->data['split_order_id'])){ 
			$split_order_id = $this->request->data['split_order_id'];
		}
		
		$msg = $this->genrateSlipLabel($split_order_id, $picklist_id);		
		 
		return $msg;
	}
	
	public function printSingleSlipLabel($split_order_id = null,$picklist_id = 0 )
	{
		if(isset($this->request->data['split_order_id'])){ 
			$split_order_id = $this->request->data['split_order_id'];
		}
		if(isset($this->request->data['picklist_id'])){ 
			$picklist_id = $this->request->data['picklist_id'];
		}
		
		$msg['msg'] = $this->getSlipLabel($split_order_id, $picklist_id);
		echo json_encode($msg);
		exit;
	}
	public function getSlipLabel($split_order_id = null,$picklist_id = 0 )
	{ 			
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		$this->loadMOdel( 'CategoryContant' );
		$this->loadMOdel( 'Template' );
		$this->loadMOdel( 'BulkLabel' );
		$this->loadModel( 'MergeUpdate');
		$this->loadModel( 'PackagingSlip');
		$this->loadModel( 'DynamicPicklist' );
			
  		$openOrderId		=	explode("-",$split_order_id)[0]; 
		$addData			=	$this->getAddress( $openOrderId );
		$address_checked	=	$this->checkAddress( $addData, $openOrderId,$split_order_id );
		$msg = '';
		App::Import('Controller', 'RoyalMail'); 
		$royal = new RoyalMailController;
			
	  	$log_file = WWW_ROOT .'logs/dynamic_log/print_slip_label_'.$picklist_id.".log";
		 
		//$address_checked = 1;
		if($address_checked == 'ok')
		{
				$get_sp_order =	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_order_id)));
			 
 				if( $get_sp_order['MergeUpdate']['service_provider'] == 'Royalmail' ){
					$royal->applyRoyalMailByOrder($split_order_id);
				 	$label 	= $this->royalMailLabel($split_order_id, $openOrderId); 
				}else if($get_sp_order['MergeUpdate']['service_provider'] == 'PostNL' && !in_array($addData['country'],['Italy','United Kingdom','Netherlands','Brazil'])){
  					$label 	= $this->getPostnlLabel($split_order_id, $get_sp_order['MergeUpdate']['postnl_barcode']); 
 				}else if($get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] == 'Spain'){
 					$label 	= $this->jerseyPostLabel($split_order_id, $openOrderId);
				}else {
					$label 	= $this->getLabel( $split_order_id, $openOrderId );
				}
 			 
			 	$slip =	$this->getSlip( $split_order_id, $openOrderId );
		
				if($split_order_id == '2210974-1'){
				//	pr($label);			 
					//exit;
				}
 				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				$date = date("m/d/Y H:i:s");
				
				$html = '';
				if($get_sp_order['MergeUpdate']['service_provider'] == 'Royalmail')
				{
					if($label != 'error')
					{						 
						$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
								 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
								 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
								 <meta content="" name="description"/>
								 <meta content="" name="author"/>';
						$html .= '<table style="margin-top:12px; height:1090px;  width:740px; border:1px solid #CCC;"> 
								<tr><td height="205" valign="top">'.$slip.'</td></tr> 
								<tr><td valign="top" ><div style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
								transform-origin: 50% 50%;margin-left: -200px; margin-top: -20px;">'.$label.'</div></td></tr>
								</table>';
					
					} 
				}else if($get_sp_order['MergeUpdate']['service_provider'] == 'PostNL' && !in_array($addData['country'],['Italy','United Kingdom','Netherlands','Brazil'])){
  					 
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
								 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
								 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
								 <meta content="" name="description"/>
								 <meta content="" name="author"/>';
					$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
						<tr><td style="height:600px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr>'.$label.'</tr>
						</table>';
 				}
				else if($get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] == 'Germany')
				{
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
							<tr><td valign="top" style="height:560px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
							<tr><td valign="bottom" style="width:560px; padding:100px 0px 10px 0px;"><div style="border:1px solid gray;">'.$label.'</div></td><td></td></tr>
						</table>';
				}else if($get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] != 'Spain')
				{
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
							<tr><td style="height:700px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr><td valign="top" style="height:367px; width:560px; padding:0px 0px 5px 12px;border:1px solid gray;">'.$label.'</td><td></td></tr>
						</table>';
				}
				else if($get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] == 'Spain' )
				{
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="margin-top:12px; height:1070px;  width:740px; border:1px solid #CCC;"> 
							<tr><td height="530" valign="top">'.$slip.'</td></tr> 
							<tr><td valign="top" style="height:367px; width:550px; border:1px solid gray;">'.$label.'</td></tr>
							</table>';
				}
				else
				{ 				
 					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
							<tr><td style="height:700px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr><td valign="top" style="height:367px; width:560px; padding:0px 0px 5px 12px;border:1px solid gray;">'.$label.'</td><td></td></tr>
						</table>';
				}
				  
				if($html != ''){
					$cssPath = WWW_ROOT .'css/';
					$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
					//  echo $html;
				//	$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
					$dompdf->load_html($html, Configure::read('App.encoding'));
					$dompdf->render();
					$imgPath = WWW_ROOT .'img/printPDF/Bulk/'; 
					$path = Router::url('/', true).'img/printPDF/Bulk/';
					$name	=	'Label_Slip_'.$split_order_id.'.pdf';
					file_put_contents($imgPath.$name, $dompdf->output());
					//exit;
					if($split_order_id == '2046608-1'){
						echo $slip;
						unlink($imgPath.$name); 
						file_put_contents($imgPath.$name, $dompdf->output());
						exit;
					}
					// echo $html; 
					$serverPath  	= 	'http://xsensys.com/img/printPDF/Bulk/'.$name ;
					$printerId		=	$this->getPrinterId();//'306161';
					/*$sendData = array(
						'printerId' => $printerId,
						'title' => 'Printing DP:'.$split_order_id,
						'contentType' => 'pdf_uri',
						'content' => $serverPath,
						'source' => 'Direct'
					); */
					if(in_array($picklist_id,array(6662,6663,6664,6665,6674,6675,6676,6677,6678,6700,6705,6706,6707,6709,6710,6711,6775,6853,6959,6711,6934,6933))){ 
						$pickItems = $this->DynamicPicklist->find('first', array('conditions' => array('id' => $picklist_id)));
						$picklist_barcode = $pickItems['DynamicPicklist']['picklist_barcode'];
						if (!is_dir(WWW_ROOT.$picklist_barcode)) {
							mkdir(WWW_ROOT.$picklist_barcode, 0777, true);
						}
						copy($imgPath.$name, WWW_ROOT.$picklist_barcode."/".$name);
					}else{
					
						App::import( 'Controller' , 'Coreprinters' );
						$Coreprinter = new CoreprintersController();
						//$out = $Coreprinter->toPrint( $sendData ); 
						//$out = $this->printNode( $sendData ); 
						
						file_put_contents(WWW_ROOT .'logs/out.log', $out."\t".$split_order_id."\t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
					}
					
					$msg = $split_order_id . "  Label & Slip Printed.";	
 					file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$split_order_id."\t LabelSlip Printed \t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
					
				}else{
					$msg = $split_order_id . "  Label OR Slip Issue.";	 			 
					file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$split_order_id."\t Label OR Slip Issue \t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
				}
				
		}else{
 				$msg = $split_order_id . " " . $address_checked;	 
				file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$split_order_id."\Address Format Issue\t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
			 
		} 
 		return $msg; 
	 }
	 
	public function printNode($data = array() )  {
			$key = base64_encode('72b154e71a740ba5ca548ca045c38c9f789e0284');
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.printnode.com/printjobs",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => json_encode($data),
			  CURLOPT_HTTPHEADER => array(
				"Accept: */*",
				"Accept-Encoding: gzip, deflate",
				"Authorization: Basic {$key}",
				"Cache-Control: no-cache",
				"Connection: keep-alive",
				"Content-Length: 129",
				"Content-Type: application/json",
				"Host: api.printnode.com",
				"Postman-Token: c4f586dc-97df-41e1-ae5d-51610c72f08f,e684e626-ddf7-43ae-8080-d7b32e482863",
				"User-Agent: PostmanRuntime/7.17.1",
				"cache-control: no-cache"
			  ),
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  return "cURL Error #:" . $err;
			} else {
			  return $response;
			}
	} 
	 public function GenerateSingleSlipLabel($split_order_id = null,$picklist_id = 0 )
	{
		if(isset($this->request->data['split_order_id'])){ 
			$split_order_id = $this->request->data['split_order_id'];
		}
		if(isset($this->request->data['picklist_id'])){ 
			$picklist_id = $this->request->data['picklist_id'];
		}
		
		$msg['msg'] = $this->genrateSlipLabel($split_order_id, $picklist_id);
		echo json_encode($msg);
		exit;
	}
	
	public function genrateSlipLabel($split_order_id = null,$picklist_id = 0 )
	{ 			
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		$this->loadMOdel( 'CategoryContant' );
		$this->loadMOdel( 'Template' );
		$this->loadMOdel( 'BulkLabel' );
		$this->loadModel( 'MergeUpdate');
		$this->loadModel( 'PackagingSlip');
		$this->loadModel( 'DynamicPicklist' );
			
  		$openOrderId		=	explode("-",$split_order_id)[0]; 
		$addData			=	$this->getAddress( $openOrderId );
		$address_checked	=	$this->checkAddress( $addData, $openOrderId,$split_order_id );
		$msg = '';
		App::Import('Controller', 'RoyalMail'); 
		$royal = new RoyalMailController;
 	  	$log_file = WWW_ROOT .'logs/dynamic_log/print_slip_label_'.$picklist_id.".log";
		 
		//$address_checked = 1;
		if($address_checked == 'ok')
		{
				$get_sp_order =	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_order_id)));
				 
 				if( $get_sp_order['MergeUpdate']['service_provider'] == 'Royalmail' ){
					$royal->applyRoyalMailByOrder($split_order_id);
				 	$label 	= $this->royalMailLabel($split_order_id, $openOrderId); 
				}else if($get_sp_order['MergeUpdate']['service_provider'] == 'PostNL' && !in_array($addData['country'],['Italy','United Kingdom','Netherlands','Brazil'])){
  					$label 	= $this->getPostnlLabel($split_order_id, $get_sp_order['MergeUpdate']['postnl_barcode']); 
 				}else if($get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] == 'Spain'){
 					$label 	= $this->jerseyPostLabel($split_order_id, $openOrderId);
				}else {
					$label 	= $this->getLabel( $split_order_id, $openOrderId );
				}
 			 
			 	$slip =	$this->getSlip( $split_order_id, $openOrderId );
		
				if($split_order_id == '2210974-1'){
				//	pr($label);			 
					//exit;
				}
 				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				$date = date("m/d/Y H:i:s");
				
				$html = '';
				if($get_sp_order['MergeUpdate']['service_provider'] == 'Royalmail')
				{
					if($label != 'error')
					{						 
						$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
								 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
								 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
								 <meta content="" name="description"/>
								 <meta content="" name="author"/>';
						$html .= '<table style="margin-top:12px; height:1090px;  width:740px; border:1px solid #CCC;"> 
								<tr><td height="205" valign="top">'.$slip.'</td></tr> 
								<tr><td valign="top" ><div style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
								transform-origin: 50% 50%;margin-left: -200px; margin-top: -20px;">'.$label.'</div></td></tr>
								</table>';
					
					} 
				}else if($get_sp_order['MergeUpdate']['service_provider'] == 'PostNL' && !in_array($addData['country'],['Italy','United Kingdom','Netherlands','Brazil'])){
  					 
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
								 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
								 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
								 <meta content="" name="description"/>
								 <meta content="" name="author"/>';
					$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
						<tr><td style="height:600px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr>'.$label.'</tr>
						</table>';
 				}
				else if($get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] == 'Germany')
				{
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
							<tr><td valign="top" style="height:560px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
							<tr><td valign="bottom" style="width:560px; padding:100px 0px 10px 0px;"><div style="border:1px solid gray;">'.$label.'</div></td><td></td></tr>
						</table>';
				}else if($get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] != 'Spain')
				{
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
							<tr><td style="height:700px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr><td valign="top" style="height:367px; width:560px; padding:0px 0px 5px 12px;border:1px solid gray;">'.$label.'</td><td></td></tr>
						</table>';
				}
				else if($get_sp_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $get_sp_order['MergeUpdate']['delevery_country'] == 'Spain' )
				{
					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="margin-top:12px; height:1070px;  width:740px; border:1px solid #CCC;"> 
							<tr><td height="530" valign="top">'.$slip.'</td></tr> 
							<tr><td valign="top" style="height:367px; width:550px; border:1px solid gray;">'.$label.'</td></tr>
							</table>';
				}
				else
				{ 				
 					$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/>';
					$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
							<tr><td style="height:700px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr><td valign="top" style="height:367px; width:560px; padding:0px 0px 5px 12px;border:1px solid gray;">'.$label.'</td><td></td></tr>
						</table>';
				}
				  
				if($html != ''){
					$cssPath = WWW_ROOT .'css/';
					$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
					//  echo $html;
				//	$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
					$dompdf->load_html($html, Configure::read('App.encoding'));
					$dompdf->render();
					$imgPath = WWW_ROOT .'img/printPDF/Bulk/'; 
					$path = Router::url('/', true).'img/printPDF/Bulk/';
					$name	=	'Label_Slip_'.$split_order_id.'.pdf';
					file_put_contents($imgPath.$name, $dompdf->output());
					//exit;
					if($split_order_id == '2046608-1'){
						echo $slip;
						unlink($imgPath.$name); 
						file_put_contents($imgPath.$name, $dompdf->output());
						exit;
					}
					// echo $html; 
					$serverPath  	= 	'http://xsensys.com/img/printPDF/Bulk/'.$name ;
					$printerId		=	$this->getPrinterId();//'306161';
					/*$sendData = array(
						'printerId' => $printerId,
						'title' => 'Printing DP:'.$split_order_id,
						'contentType' => 'pdf_uri',
						'content' => $serverPath,
						'source' => 'Direct'
					); */
					if(in_array($picklist_id,array(6662,6663,6664,6665,6674,6675,6676,6677,6678,6700,6705,6706,6707,6709,6710,6711,6775,6853,6959,6711,6934,6933))){ 
						$pickItems = $this->DynamicPicklist->find('first', array('conditions' => array('id' => $picklist_id)));
						$picklist_barcode = $pickItems['DynamicPicklist']['picklist_barcode'];
						if (!is_dir(WWW_ROOT.$picklist_barcode)) {
							mkdir(WWW_ROOT.$picklist_barcode, 0777, true);
						}
						copy($imgPath.$name, WWW_ROOT.$picklist_barcode."/".$name);
					}else{
					
						App::import( 'Controller' , 'Coreprinters' );
						$Coreprinter = new CoreprintersController();
						//$out = $Coreprinter->toPrint( $sendData ); 
						//$out = $this->printNode( $sendData ); 
						
						file_put_contents(WWW_ROOT .'logs/out.log', $out."\t".$split_order_id."\t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
					}
					
					$msg = $split_order_id . "  Label & Slip Printed.";	
 					file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$split_order_id."\t LabelSlip Printed \t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
					
				}else{
					$msg = $split_order_id . "  Label OR Slip Issue.";	 			 
					file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$split_order_id."\t Label OR Slip Issue \t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
				}
				
		}else{
 				$msg = $split_order_id . " " . $address_checked;	 
				file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$split_order_id."\Address Format Issue\t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
			 
		} 
 		return $msg; 
	 }
	 
	public function getAddress( $openOrderId = null)
	{
			App::import('Controller', 'Linnworksapis');
			$obj = new LinnworksapisController();
			$order				=		$obj->getOpenOrderById( $openOrderId );
			$company			=	 	$order['customer_info']->Address->Company;
			$fullname			=		$order['customer_info']->Address->FullName;
			$address1			=		$order['customer_info']->Address->Address1;
			$address2			=		$order['customer_info']->Address->Address2;
			$address3			=		$order['customer_info']->Address->Address3;
			$town				=	 	$order['customer_info']->Address->Town;
			$resion				=	 	$order['customer_info']->Address->Region;
			$postcode			=	 	$order['customer_info']->Address->PostCode;
			$country			=	 	$order['customer_info']->Address->Country;
			$addData['sub_source'] 	= 	$order['sub_source'];
			$addData['company'] 	= 	$order['customer_info']->Address->Company;
			$addData['fullname'] 	= 	$order['customer_info']->Address->FullName;
			$addData['address1'] 	= 	$order['customer_info']->Address->Address1;
			$addData['address2'] 	= 	$order['customer_info']->Address->Address2;
			$addData['address3'] 	= 	$order['customer_info']->Address->Address3;
			$addData['town'] 		= 	$order['customer_info']->Address->Town;
			
			$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forl?-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia');
			
			if( $country == 'Italy' ){
				if(array_key_exists(strtoupper($resion),$provinces)){
					$addData['resion'] 		= 	$resion;
				}else {
					$addData['resion'] 		= 	array_search(ucfirst($resion), $provinces);
				}
			} else {
					$addData['resion'] 		= 	$resion;
			}
			$addData['postcode'] 	= 	$postcode;
			$addData['country'] 	= 	$country;
			return $addData;
	}
	
	public function checkAddress( $address = null, $orderid = null, $split_order_id  = null )
	{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'OrderNote' );
			
 			$_error = 0; $_error_msg = '';
			
			if(strtolower($address['country']) == 'spain' && !is_numeric($address['postcode'])) {
				$_error_msg = "Post code issue.";
			}else if(strlen($address['country']) > 35 ) {				
				$_error_msg = "Country name length is more than 35 characters.";
			}else if(strlen($address['fullname']) > 55 ) {			
				$_error_msg = "Customer name length is more than 55 characters.";
			}else if(strlen($address['address1']) > 70 ) {		
				$_error_msg = "Address1 length is more than 70 characters.";
			}else if(strlen($address['address2']) > 70 ) {		
				$_error_msg = "Address2 length is more than 70 characters.";
			}else if(strlen($address['address3']) > 60 ) {			
				$_error_msg = "Address3 length is more than 60 characters.";
			} 
			
			if($_error_msg != '')
			{
					$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 3, 'MergeUpdate.label_status' => 1 ) , array( 'MergeUpdate.order_id' => $orderid ) );
					$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 3 ) , array( 'OpenOrder.num_order_id' => $orderid ) );
					
					$firstName 	= ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
					$lastName 	= ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
					
					$noteDate['order_id'] 	= 	 $orderid;
					$noteDate['note'] 		= 	 'Address format issue';
					$noteDate['type'] 		= 	 'Lock';
					$noteDate['user'] 		= 	 $firstName.' '.$lastName;
					$noteDate['date'] 		= 	 date('Y-m-d H:i:s');
					$this->OrderNote->saveAll( $noteDate );
					
 					$log_file = WWW_ROOT .'logs/dynamic_log/address_format_issue_'.date('dmy').".log";
					file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$orderid."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);	
					
					$subject   = $orderid. ' Xsensys address format issue.';
					$mailBody  = $orderid. ' Xsensys address format issue.';					 
					$mailBody .= '<p>Error : '.$_error_msg.'</p>'; 
										
					App::uses('CakeEmail', 'Network/Email');
					$email = new CakeEmail('');
					$email->emailFormat('html');
					$email->from('info@euracogroup.co.uk');
					$email->to( array('avadhesh.kumar@jijgroup.com','shashi@euracogroup.co.uk','deepak@euracogroup.com','abhishek@euracogroup.co.uk','vikas.kumar@euracogroup.co.uk','ankit.nagar@euracogroup.co.uk'));	
					//$email->to( array('avadhesh.kumar@jijgroup.com'));	  			
					$email->subject( $subject );
					$email->send( $mailBody );
					
					return $_error_msg;
			} else {
					$this->MergeUpdate->updateAll( array( 'MergeUpdate.label_status' => 2 ) , array( 'MergeUpdate.order_id' => $orderid,'MergeUpdate.product_order_id_identify' => $split_order_id ) );
					//$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 0 ) , array( 'OpenOrder.num_order_id' => $orderid ) );
					return "ok";
			}
		}

		
	public function getLabel( $splitOrderId = null, $openOrderId = null )
	{ 
				
				$openOrderId	=	$openOrderId ;
				$splitOrderId	=	$splitOrderId ;
				 
				App::import('Controller', 'Linnworksapis');
				$obj = new LinnworksapisController();
				
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				$order				=		$obj->getOpenOrderById( $openOrderId );
				$postalservices		=		$order['shipping_info']->PostalServiceName;
				$destination		=		$order['customer_info']->Address->Country;
				$total				=		$order['totals_info']->TotalCharge;
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				
				$address1 			= 		wordwrap($order['customer_info']->Address->Address1, 30, "\n<br>", false);	
				$address2 			= 		wordwrap($order['customer_info']->Address->Address2, 30, "\n<br>", false);	
				$address3			=		$order['customer_info']->Address->Address3;
				if(strlen($order['customer_info']->Address->Address3) > 30)	{
					$address3 		= 		wordwrap($order['customer_info']->Address->Address3, 30, "\n<br>", false);
				}
				
				/*$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				*/
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$currency			=	 	$order['totals_info']->Currency;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$templateId			=	 	$order['template_id'];
				$subSource			=	 	$order['sub_source'];
				$envelope_weight	=	 	$splitOrderDetail['MergeUpdate']['envelope_weight'];
				
				$str = ''; 
				
				$totlaWeight	=	0;
				$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
				$ref_code = trim($splitOrderDetail['MergeUpdate']['provider_ref_code']);
				$weight = 0;
				$europe = array('Monaco','Andorra');
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					
					foreach($newSkus as $newSku)
						{
							
							$totalvalue = $total;
							$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
							$getsku = 'S-'.$newSku[1];
							$qty = $newSku[0];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title	=	$getOrderDetail['Product']['product_name'];
							$weight	=	$weight + $getOrderDetail['ProductDesc']['weight'];
							$totlaWeight	= $totlaWeight + ($weight * $qty);
							$catName		= $getOrderDetail['Product']['category_name'];
							
							if($country == 'United States')
							{
									$str .= '<tr>
										<td valign="top" class="leftborder rightborder bottomborder" style=" padding-left:5px;">'.$catName.'</td>
										<td valign="top" class="leftborder rightborder bottomborder" style=" padding-left:5px;">'.$qty.'</td>
										<td valign="top" class="rightborder bottomborder" style=" padding-left:5px;">'.$qty * $getOrderDetail['ProductDesc']['weight'].' Kg</td>';
									$str .= '</tr>';
							}
							elseif($country == 'Canada')
							{
								$str .= '<tr>
									<td valign="top" class="leftborder rightborder bottomborder" style=" padding-left:5px;">'.$catName.'</td>
									<td valign="top" class="leftborder rightborder bottomborder" style=" padding-left:5px;">'.$qty.'</td>
									<td valign="top" class="rightborder bottomborder" style=" padding-left:5px;">'.$qty*$weight.' Kg</td>';
								$str .= '</tr>';
							}
							/*elseif($country == 'Spain')
								{
									$codearray = array("DP1", "FR7");
									$spaincode = array("mad_air","mad_road","cat_air","cat_road","sp_air","sp_road");
							
									if(in_array($ref_code, $spaincode))
									{
										$str .='<tr>
												<td  width="60%"  style="border:1px solid #000; padding:5px;">'.$catName.'</td>
												<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty * $getOrderDetail['ProductDesc']['weight'].'</td>
												<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty.'</td>';
										$str .= '</tr>';
									} else {
										$str .= '<tr>

											<td valign="top" class="noleftborder rightborder bottomborder">'.$catName.'</td>
											<td valign="top" class="center noleftborder rightborder bottomborder">'.$qty.'</td>
											<td valign="top" class="center norightborder bottomborder">'.$qty * $getOrderDetail['ProductDesc']['weight'].'</td>';
										$str .= '</tr>';
									}
								}*/
							else
							{
								$str .= '<tr>
									<td valign="top" class="leftborder rightborder bottomborder" style=" padding-left:5px;">'.$catName.'</td>
									<td valign="top" class="leftborder rightborder bottomborder" style=" padding-left:5px;">'.$qty.'</td>
									<td valign="top" class="rightborder bottomborder" style=" padding-left:5px;">'.$qty*$weight.' Kg</td>';
								$str .= '</tr>';
								/*$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.substr($title, 0, 25 ).'</td>
										<td valign="top" class="center norightborder bottomborder">'.$qty * $getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';*/
							}
						}
							unset($newSkus);
				}
				
				$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forl?-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia');
				$cana_provinces = array('AB' =>'Alberta','BC'=>'British Columbia','MB'=>'Manitoba','NB'=>'New Brunswick','NL'=>'Newfoundland and Labrador','NS'=>'Nova Scotia','NT'=>'Northwest Territories','NU'=>'Nunavut','ON'=>'Ontario','PE'=>'Prince Edward Island','QC'=>'Quebec','SK'=>'Saskatchewan','YT'=>'Yukon');
				
				$usa_state =  array('AL'=>'Alabama','AK'=>'Alaska','AS'=>'American Samoa','AZ'=>'Arizona','AR'=>'Arkansas','CA'=>'California','CO'=>'Colorado','CT'=>'Connecticut','DE'=>'Delaware','DC'=>'District of Columbia','FM'=>'Federated States of Micronesia','FL'=>'Florida','GA'=>'Georgia','GU'=>'Guam','HI'=>'Hawaii','ID'=>'Idaho','IL'=>'Illinois','IN'=>'Indiana','IA'=>'Iowa ','KS'=>'Kansas','KY'=>'Kentucky','LA'=>'Louisiana','ME'=>'Maine','MH'=>'Marshall Islands','MD'=>'Maryland','MA'=>'Massachusetts','MI'=>'Michigan','MN'=>'Minnesota','MS'=>'Mississippi','MO'=>'Missouri','NE'=>'Nebraska','NV'=>'Nevada','NH'=>'New Hampshire','NJ'=>'New Jersey','NM'=>'New Mexico','NY'=>'New York','NC'=>'North Carolina','ND'=>'North Dakota','MP'=>'Northern Mariana Islands','OH'=>'Ohio','OK'=>'Oklahoma','OR'=>'Oregon','PW'=>'Palau','PA'=>'Pennsylvania','PR'=>'Puerto Rico','RI'=>'Rhode Island','SC'=>'South Carolina','SD'=>'South Dakota','MT'=>'Montana','TN'=>'Tennessee','TX'=>'Texas','UT'=>'Utah','VT'=>'Vermont','VI'=>'Virgin Islands','VA'=>'Virginia','WA'=>'Washington','WV'=>'West Virginia','WI'=>'Wisconsin','WY'=>'Wyoming');
					
				
				$addData['sub_source'] 	= 	$order['sub_source'];
				$addData['company'] 	= 	$company;
				$addData['fullname'] 	= 	$fullname;
				$addData['address1'] 	= 	$address1;
				$addData['address2'] 	= 	$address2;
				$addData['address3'] 	= 	$address3;
				$addData['town'] 		= 	$town;
				
				if( $country == 'Italy' )
				{
					if(array_key_exists(strtoupper($resion),$provinces)){
						$addData['resion'] 		= 	$resion;
					}else {
						$addData['resion'] 		= 	array_search(ucfirst($resion), $provinces);
					}
				} 
				else if( $country == 'Canada' )
				{
					if(array_key_exists(strtoupper($resion),$cana_provinces)){
						$addData['resion'] 		= 	$resion;
					}else {
						$addData['resion'] 		= 	array_search(ucfirst($resion), $cana_provinces);
					}
				}
				else if( $country == 'United States' )
				{
					if(array_key_exists(strtoupper($resion),$usa_state)){
						$addData['resion'] 		= 	$resion;
					}else {
						$addData['resion'] 		= 	array_search(ucfirst($resion), $usa_state);
					}
				}
				else
				{
						$addData['resion'] 		= 	$resion;
				}
				
				$addData['postcode'] 	= 	$postcode;
				$addData['country'] 	= 	$country;
				
				$address 			= 		$obj->getCountryAddress( $addData );
				 
				
				$CARTAS = '';
				if(trim($country) == 'Spain') { 
					$CARTAS = 'CARTAS '.substr($postcode, 0, 1);
					$routingcode = $obj->getCorreosBarcode( $postcode, $splitOrderId);
					$corriosImage = $routingcode.'.png';
					if(substr($postcode, 0, 1) == 0){
						$CARTAS = 'CARTAS '.substr($postcode, 1, 1);
					}	
				}
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				$currentdate		=	 	date("j, F  Y");
				$BarcodeImage		=		$splitOrderDetail['MergeUpdate']['order_barcode_image'];
				$corriosImage		=		$postcode.'90019.png';
				
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= str_replace( 'Spain' , 'ESPAÑA', $address); 
				$setRepArray[] 					= $country;
				$barcode  						= $order['assign_barcode'];
				$barcodenum						= explode('.', $barcode);
				
				if($splitOrderId  == '2304049-1'){
					pr($order['customer_info']->Address);
					pr($setRepArray);
				}
				
				//$barcodePath  					= Router::url('/', true).'img/orders/barcode/';
				//$barcodeCorriosPath  			= Router::url('/', true).'img/orders/correos_barcode/';
				$barcodePath  					= WWW_ROOT.'img/orders/barcode/';
				$barcodeCorriosPath  			= WWW_ROOT.'img/orders/correos_barcode/';
				
				$barcodenum						= explode('.', $barcode);
			
				/**************** for tempplate *******************/
				$countryArray = Configure::read('customCountry');
				if($country == 'United Kingdom' && $splitOrderDetail['MergeUpdate']['service_provider'] == 'PostNL'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				} 
				else if($country != 'United Kingdom' && $splitOrderDetail['MergeUpdate']['service_provider'] == 'PostNL' && $splitOrderDetail['MergeUpdate']['postal_service'] != 'Tracked'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Rest Of EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				}
				else if(in_array($country,['United States','Canada','France']) && $splitOrderDetail['MergeUpdate']['service_provider'] == 'Jersey Post'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				}
				else if($country == 'Germany'  && $splitOrderDetail['MergeUpdate']['service_provider'] == 'Jersey Post'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );  
				}else if(!in_array( $country, $europe )  && $splitOrderDetail['MergeUpdate']['service_provider'] == 'Jersey Post'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Other', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );  
				}
				else if( $splitOrderDetail['MergeUpdate']['service_provider'] == 'PostNL' && $splitOrderDetail['MergeUpdate']['reg_post_number'] != '' &&  $splitOrderDetail['MergeUpdate']['reg_num_img'] != '' &&  $splitOrderDetail['MergeUpdate']['track_id'] != '' && $country != 'Italy'){
				
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Other_Reg', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				 
				} 			
				else 
				{
					if( in_array( $country, $europe ) )
						{
							$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
						}
					else 
						{
							$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
						}
				}
					 
				 
				
					$subsource_lower = strtolower($subSource) ;
					$marecArray = array('marec_de','marec_es','marec_fr','marec_it','marec_uk');
					$costbreakerArray = array('costbreaker_de','costbreaker_es','costbreaker_fr','costbreaker_it','costbreaker_uk','costbreaker','costbreaker_ca','costbreaker_usabuyer');
					$techdriveArray = array('tech_drive_de','tech_drive_es','tech_drive_fr','tech_drive_it','tech_drive_uk');				
					$rainbowArray = array('rainbow retail','rainbow retail de','rainbow_retail_es','rainbow_retail_it','rainbow_retail_fr');
					$bbdArray = array('bbd_eu_de');	
				
					if(in_array($subsource_lower,$marecArray)){
						$company 	=  'Marec'; $store_logo = 'ebuyerexpress_logo.jpg';
						$store_logo = 'rainbow.png';
					}else if(in_array($subsource_lower,$costbreakerArray)){
						$company 	=  'CostBreaker'; $store_logo = 'cost_stopper2.png';						
					}else if(in_array($subsource_lower,$techdriveArray)){
						$company 	= 'Tech Drive Supplies'; $store_logo = 'techDriveSupplies_logo.jpg';
					}else if(in_array($subsource_lower,$rainbowArray)){
						$company 	= 'Rainbow Retail';	$store_logo = 'rainbow.png';
					}else if(in_array($subsource_lower,$bbdArray)){
						$company 	= 'BBD'; $store_logo = 'bbdEu.jpg';
					} else if( $subSource == 'EBAY0' ){
						$company 	= 'EBAY0'; $store_logo = 'ebay0.jpg';
					} else if( $subSource == 'EBAY2' ){  
						$company 	= 'EBAY2'; $store_logo = 'ebay0.jpg';
					}
					$this->loadModel( 'ReturnAddre' );
					$ret_add = $this->ReturnAddre->find('first', array( 'conditions' => array( 'company' =>  $company) ) );
					$return_address = $ret_add['ReturnAddre']['return_address'];
				
				$html           =  $labelDetail['BulkLabel']['html'];
				if($splitOrderId == '2311053-1'){
					//pr($labelDetail);
				}
				//$paperHeight    =  $labelDetail['Template']['paper_height'];
				//$paperWidth  	=  $labelDetail['Template']['paper_width'];
				//$barcodeHeight  =  $labelDetail['Template']['barcode_height'];
				//$barcodeWidth   =  $labelDetail['Template']['barcode_width'];
				//$paperMode      =  $labelDetail['Template']['paper_mode'];
				
				$barcodeimg = '<img src='.$barcodePath.$BarcodeImage.'>';
			 
				$setRepArray[] 	=    $barcodeimg;
				$setRepArray[] 	=    $barcodenum[0];
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totlaWeight + $envelope_weight;
				$setRepArray[]	=	 $tax;
				$setRepArray[]	=	 $currentdate;
				$logopath		=	 Router::url('/', true).'img/';
				$setRepArray[]	=	 '<img src="'.$logopath.'logo.jpg" >';
				$logo			=	 '<img src="'.$logopath.'logo.jpg" >';
				$signature		=	 '<img src="'.$logopath.'sig.jpg" >';
				
				$setRepArray[]	=	 $logo;
				$setRepArray[]	=	 $signature;
				$setRepArray[]	=	 $splitOrderId;
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				
				$tempId			=	$splitOrderDetail['MergeUpdate']['lable_id'];
				$countryCode	=	$splitOrderDetail['MergeUpdate']['country_code'];
				
				$setRepArray[]	=	 $countryCode;
				$setRepArray[]	=	 $totlaWeight + $envelope_weight;
				
				$conversionRate	=	 Configure::read( 'conversionRate' );
				
				if($currency == 'GBP')
				{
					$totalvalue = $totalvalue;
				}
				elseif($currency == 'EUR')
				{
					$totalvalue = $totalvalue;
				}
				elseif($country == 'Spain' && $currency == 'EUR')
				{
					$totalvalue = $totalvalue;
				}
				elseif($country == 'United States' || $country == 'Canada')
				{
					$totalvalue = $totalvalue;
				}
				else
				{
					$totalvalue = number_format($totalvalue/$conversionRate, 2, '.' ,'');
				}
				
				if($totalvalue >= 800  && $labelDetail['BulkLabel']['service_provider'] == 'Jersey Post' && ($country == 'United States')){
					$numbers = array("97.11","98.32","98.52","99.72","99.94","150.27","200.45","270.62","370.82","422.11","472.31","530.71");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				if($totalvalue >= 25  && $labelDetail['BulkLabel']['service_provider'] == 'Jersey Post' && ($country == 'Canada')){
					$numbers = array("20.82","20.11","20.31","20.71","21.22","21.41", "21.61","21.51","22.23","23.72","23.71","23.22","24.41", "24.61");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				else if($totalvalue >= 22  && $labelDetail['BulkLabel']['service_provider'] == 'Jersey Post' && $country != 'United States'){
					$numbers = array("18.11","18.23","18.32","18.52","18.72","18.94","19.27","19.45","19.62","19.82","20.11","20.31","20.71","21.22","21.41", "21.61","21.51");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				
				$setRepArray[]	= $totalvalue;
				
				if($splitOrderDetail['MergeUpdate']['track_id'] != '')
				{
					$trackingnumber = $splitOrderDetail['MergeUpdate']['track_id'];
				}
				else
				{
					$trackingnumber = 'A'.mt_rand(100000, 999999);
				}
				
				$setRepArray[]	=	 $trackingnumber;
				$setRepArray[]	=	 $subSource;
				$setRepArray[]	=	 $CARTAS;
				$setRepArray[]	=	 $splitOrderDetail['MergeUpdate']['reg_num_img'];
				$setRepArray[]	=	 '<img src='.$barcodeCorriosPath.$corriosImage.'>';
				$setRepArray[]	=	($phone != '') ? 'Telefono del cliente:'.$phone : '';
				$setRepArray[]	=	 $return_address;
				$setRepArray[]	=	 WWW_ROOT;
				$setRepArray[]	=	 $store_logo;
				$cssPath = WWW_ROOT .'css/';
				$imgPath = Router::url('/', true) .'img/';

				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				 
				$html 	= $obj->setReplaceValueLabel( $setRepArray, $html );
				
				
				return $html;
		}
		
		
	public function getSlip($splitOrderId = null, $openOrderId = null)
	{
	
		//echo 'xxxxxxxxxxxxxxx';exit;
		$this->loadModel('OrderLocation');
		App::import('Controller', 'Linnworksapis');
		$obj = new LinnworksapisController();
		$order	=	$obj->getOpenOrderById( $openOrderId );
		
		$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
		$itemPrice			=		$getSplitOrder['MergeUpdate']['price'];
		$itemQuantity		=		$getSplitOrder['MergeUpdate']['quantity'];
		$BarcodeImage		=		$getSplitOrder['MergeUpdate']['order_barcode_image'];
		$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
		$assignedservice	=	 	$order['assigned_service'];
		$courier			=	 	$order['courier'];
		$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
		$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
		$barcode			=	 	$order['assign_barcode'];
		$subtotal			=		$order['totals_info']->Subtotal;
		$subsource			=		$order['sub_source'];
		$totlacharge		=		$order['totals_info']->TotalCharge;
		$ordernumber		=		$order['num_order_id'];
		$fullname			=		$order['customer_info']->Address->FullName;
		$address1			=		$order['customer_info']->Address->Address1;
		$address2			=		$order['customer_info']->Address->Address2;
		$address3			=		$order['customer_info']->Address->Address3;
		/*$address1 			= 		wordwrap($order['customer_info']->Address->Address1, 30, "\n<br>", false);	
		$address2 			= 		wordwrap($order['customer_info']->Address->Address2, 30, "\n<br>", false);	
		$address3			=		$order['customer_info']->Address->Address3;
		if(strlen($order['customer_info']->Address->Address3) > 30)	{
			$address3 		= 		wordwrap($order['customer_info']->Address->Address3, 30, "\n<br>", false);
		}*/
		$town				=	 	$order['customer_info']->Address->Town;
		$resion				=	 	$order['customer_info']->Address->Region;
		$postcode			=	 	$order['customer_info']->Address->PostCode;
		$country			=	 	$order['customer_info']->Address->Country;
		$phone				=	 	$order['customer_info']->Address->PhoneNumber;
		$company			=	 	$order['customer_info']->Address->Company;
		$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
		$postagecost		=	 	$order['totals_info']->PostageCost;
		$tax				=	 	$order['totals_info']->Tax;
		$barcode  			=   	$order['assign_barcode'];
		$items				=	 	$order['items'];
		$address			=		'';
		$address			.=		($company != '') ? $company.'<br>' : '' ; 
		$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
		$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
		$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
		$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
		$address			.=		($town != '') ? $town.'<br>' : '';
		$address			.=		($resion != '') ? $resion.'<br>' : '';
		$address			.=		($postcode != '') ? $postcode.'<br>' : '';
		$address			.=		($country != '' ) ? $country.'<br>' : '';
		
		$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forlì-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia');
				
		$cana_provinces = array('AB' =>'Alberta','BC'=>'British Columbia','MB'=>'Manitoba','NB'=>'New Brunswick','NL'=>'Newfoundland and Labrador','NS'=>'Nova Scotia','NT'=>'Northwest Territories','NU'=>'Nunavut','ON'=>'Ontario','PE'=>'Prince Edward Island','QC'=>'Quebec','SK'=>'Saskatchewan','YT'=>'Yukon');
		
		$addData['sub_source'] 	= 	$order['sub_source'];
		$addData['company'] 	= 	$company;
		$addData['fullname'] 	= 	$fullname;
		$addData['address1'] 	= 	$address1;
		$addData['address2'] 	= 	$address2;
		$addData['address3'] 	= 	$address3;
		$addData['town'] 		= 	$town;
		if( $country == 'Italy' )
		{
			if(array_key_exists(strtoupper($resion),$provinces)){
				$addData['resion'] 		= 	$resion;
			}else {
				$addData['resion'] 		= 	array_search(ucfirst($resion), $provinces);
			}
		}
		else if( $country == 'Canada' )
		{
			if(array_key_exists(strtoupper($resion),$cana_provinces)){
				$addData['resion'] 		= 	$resion;
			}else {
				$addData['resion'] 		= 	array_search(ucfirst($resion), $cana_provinces);
			}
		}
		else {
			$addData['resion'] 		= 	$resion;
		}
		
		$addData['postcode'] 	= 	$postcode;
		$addData['country'] 	= 	$country;
				
		$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);//CostBreaker
		
		$subsource_lower = strtolower($subsource) ;
		$marecArray = array('marec_de','marec_es','marec_fr','marec_it','marec_uk');
		$costbreakerArray = array('costbreaker_de','costbreaker_es','costbreaker_fr','costbreaker_it','costbreaker_uk','costbreaker','costbreaker_ca','costbreaker_usabuyer','onbuy','flubit','euraco.fyndiq','costdropper');
		$techdriveArray = array('tech_drive_de','tech_drive_es','tech_drive_fr','tech_drive_it','tech_drive_uk');				
		$rainbowArray = array('rainbow retail','rainbow retail de','rainbow_retail_es','rainbow_retail_it','rainbow_retail_fr');
		$bbdArray = array('bbd_eu_de');	
		
		if(in_array($subsource_lower,$marecArray)){
			$company 	=  'Marec';
		}else if(in_array($subsource_lower,$costbreakerArray)){
			$company 	=  'CostBreaker';
		}else if(in_array($subsource_lower,$techdriveArray)){
			$company 	= 'Tech Drive Supplies';
		}else if(in_array($subsource_lower,$rainbowArray)){
			$company 	= 'Rainbow Retail';
		}else if(in_array($subsource_lower,$bbdArray)){
			$company 	= 'BBD';
		}else if( $subsource == 'EBAY2' ){
			$company 	= 'EBAY2';
		}else if( $subsource == 'EBAY0' ){
			$company 	= 'EBAY0';
		}else if( $subsource == 'EBAY5' ){
			$company 	= 'Marec';
		}
		  
		
		if($splitOrderId == '2311053-1'){
			//echo $company  ;exit;
		}
		
		$i = 1;
		$str = '';
		$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
		$totalGoods = 0;
		$productInstructions = '';
		$ref_code = trim($getSplitOrder['MergeUpdate']['provider_ref_code']);
		$codearray = array('DP1', 'FR7');
		
		$this->loadModel( 'BulkSlip' );
		if($subsource_lower == 'costbreaker_ca'){
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => 'CostBreaker_CA' ) ) );		
		}elseif($subsource_lower == 'costbreaker_usabuyer'){
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => 'CostBreaker_USA' ) ) );		
		}else{
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => $company ) ) );
		}
		 
		
		$p_barcode	 = '';
		$is_bulk_sku = 0;
		if( in_array($ref_code, $codearray)) 
		{
			
			$per_item_pcost 	= $postagecost/count($items);
			$j = 1;
			foreach($items as $item){
			$item_title 		= 	$item->Title;
			$quantity			=	$item->Quantity;
			$price_per_unit		=	$item->PricePerUnit;
			$str .= '<tr>
						<td style="border:1px solid #000;" align="left" valign="top" width="10%">'.$quantity.'</td>
						<td style="border:1px solid #000;" valign="top" width="20%">'.substr($item_title, 0, 30 ).'</td>
						<td style="border:1px solid #000;" valign="top" width="15%">'.$quantity * $price_per_unit.'</td>
						<td style="border:1px solid #000;" valign="top" width="15%">'.$per_item_pcost.'</td>
						<td style="border:1px solid #000;" valign="top" width="20%">'.(($price_per_unit * $quantity) + $per_item_pcost).'</td>
					</tr>';
			$j++;
			}
			$str .=	'<tr><td></td><td></td><td></td><td style="border:1px solid #000;">Total</td>
						<td style="border:1px solid #000;" valign="top" width="20%">'.$totalcharge.'</td>
					</tr>';
			//echo $str;
			
		} 
		else 
		{
			
			foreach( $skus as $sku )
			{
			
				$newSkus[]				=	 explode( 'XS-', $sku);
				$cat_name = array();
				$sub_can_name = array();
				foreach($newSkus as $newSku)
					{
						
						$getsku = 'S-'.$newSku[1];
						$getOrderDetail = 	$this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
						$OrderLocation 	= 	$this->OrderLocation->find( 'first', array( 'conditions' => array('order_id' => $openOrderId, 'sku' => $getsku ) )  );
						if( count($OrderLocation) > 0 ){
							$o_location 	=	$OrderLocation['OrderLocation']['bin_location'];
						} else {
							$o_location 	=	'Check Location' ;
						}
						
						$contentCat 	= 	$getOrderDetail['Product']['category_name'];
						$cat_name[] 	= 	$getOrderDetail['Product']['category_name'];
						$contentSubCat	=	$getOrderDetail['Product']['sub_category'];
						$sub_can_name[] =	$getOrderDetail['Product']['sub_category'];
						$productBarcode	=	$getOrderDetail['ProductDesc']['barcode'];
						$is_bulk_sku	=	$getOrderDetail['Product']['is_bulk_sku'];
						
						$getContent	= $this->CategoryContant->find( 'first', array( 
															'conditions'=>array('CategoryContant.sub_category'=>$contentSubCat,'CategoryContant.sub_source'=>$subsource)));
						$productBarcode			=	$getOrderDetail['ProductDesc']['barcode'];
						if(count($getContent) > 0) {
							$getbarcode				= 	$this->getBarcode( $productBarcode );
							$productInstructions 	=  '<p style="font-size:14px;">'.$getContent['CategoryContant']['content'].'</p>';
						}
						
						$product_local_barcode = $this->GlobalBarcode->getLocalBarcode($getOrderDetail['ProductDesc']['barcode']);
						
						$title	=	$getOrderDetail['Product']['product_name'];
						$totalGoods = $totalGoods + $newSku[0];
						$str .= '<tr>
								<td valign="top" class="rightborder bottomborder">'.$i.'</td>
								<td valign="top" class="center rightborder bottomborder">'.$newSku[1].'</td>
								<td valign="top" class="rightborder bottomborder">'.$title.'</td>
								<td valign="top" class="rightborder bottomborder" >'.$product_local_barcode.'</td>
								<td valign="top" class="rightborder bottomborder">'.$o_location.'</td>
								<td valign="top" class="center norightborder bottomborder">'.$newSku[0].'</td>';
						$str .= '</tr>';
						$i++;
						
					}
					unset($newSkus);
			}
			
		}
				
						
		/*$html 			=	$gethtml['PackagingSlip']['html'];
		$paperHeight    =   $gethtml['PackagingSlip']['paper_height'];
		$paperWidth  	=   $gethtml['PackagingSlip']['paper_width'];
		$barcodeHeight  =   $gethtml['PackagingSlip']['barcode_height'];
		$barcodeWidth   =   $gethtml['PackagingSlip']['barcode_width'];
		$paperMode      =   $gethtml['PackagingSlip']['paper_mode'];*/
		//
		if($splitOrderId == '1668658-1'){
			//pr($company );
		}
		$setRepArray = array();
		$setRepArray[] 					= $address1;
		$setRepArray[] 					= $address2;
		$setRepArray[] 					= $address3;
		$setRepArray[] 					= $town;
		$setRepArray[] 					= $resion;
		$setRepArray[] 					= $postcode;
		$setRepArray[] 					= $country;
		$setRepArray[] 					= $phone;
		$setRepArray[] 					= $ordernumber;
		$setRepArray[] 					= $courier;
		$setRepArray[] 					= $recivedate[0];
		$totalitem = $i - 1;
		$setRepArray[]	=	 $str;
		$setRepArray[]	=	 $totalGoods;
		$setRepArray[]	=	 $subtotal;
		$Path 			= 	'/wms/img/client/';
		$img			=	 '';
		$setRepArray[]	=	 $img;
		$setRepArray[]	=	 $postagecost;
		$setRepArray[]	=	 $tax;
		$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
		//$setRepArray[]	=	 $totalamount;
		$setRepArray[]	=	 $itemPrice;
		
		$address		= $obj->getCountryAddress( $addData  );
		
		$setRepArray[]	=	 $address;
		$barcodePath  	=  WWW_ROOT.'img/orders/barcode/';
		$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.' width='. $barcodeWidth .'>';
		$barcodenum		=	explode('.', $barcode);
		
		$setRepArray[] 	=  $barcodeimg;
		$setRepArray[] 	=  $paymentmethod;
		$setRepArray[] 	=  $barcodenum[0];
		$setRepArray[] 	=  '';
		$img 			=	$gethtml['PackagingSlip']['image_name'];
		$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
		$setRepArray[] 	=  $returnaddress;
		$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
		$setRepArray[] 	=  $splitOrderId;
		$setRepArray[] 	= 	utf8_decode( $productInstructions );
		//$prodBarcodePath  	=  Router::url('/', true).'img/product/barcodes/';			
		$prodBarcodePath  	= WWW_ROOT.'img/product/barcodes/';
		$setRepArray[] 	=  '<img src='.$prodBarcodePath.$productBarcode.'.png width='. $barcodeWidth .'>';
		$setRepArray[] 	=  $totalamount;
		$setRepArray[]	=	WWW_ROOT;
		/*--------------Ink------------------*/
		$this->loadMOdel( 'InkOrderLocation' );	 
		$order_ink	    = $this->InkOrderLocation->find('first', array('conditions' => array('order_id' => $openOrderId,'split_order_id'=>$openOrderId.'-1','status'=>'active')));		
		if(count($order_ink) > 0 ){			 
			$setRepArray[] 	=  '<div style="color:green;font-size:14px;"><center>#2 Envelope Required.</center></div><br><img src="'.WWW_ROOT.'img/ink_xl2.jpg" height="130">';
		}
		/*--------------End of Ink------------------*/
		$imgPath = WWW_ROOT .'css/';
		$html2 			= 	$gethtml['BulkSlip']['html'];
		//$html2 			= 	'';
		if($splitOrderId == '1668658-1'){
			 //echo $html2;exit;
		}
		if( $company ==  'Marec' && in_array(  'Mobile Accessories', $cat_name ))
		{
			//$this->MobileAsseInstruction( $splitOrderId, $subsource  );
		}
		
		
		$get_country = array('marec_fr'=>'FR','marec_de'=>'DE','marec_it'=>'IT','marec_es'=>'ES','marec_uk'=>'UK');
		$source_country  = $get_country[$subsource_lower];
  			
		if( $company ==  'Marec' && $is_bulk_sku == 1)
		{	
			$html2 	.= $this->BulkSkuNote($source_country);			 
		}
		
		$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
		$html 		= 	$obj->setReplaceValue( $setRepArray, $html2 );
		return $html;
		
	}
	
	public function BulkSkuNote($source_country = null)
	{
		if($source_country == 'DE'){
		
			$note = '<br> <br><div style="border:1px solid; padding:5px; width:93%">';
			$note .= '<table>';
			$note .= '<tr><td colspan="2"><strong>Hinweis:</strong> Der Artikel, den Sie gekauft haben, ist ein Sammelgut verpackt. Bei diesen Zubehörteilen handelt es sich um echte Produkte, die entweder aus Mobilteilboxen über unsere Reverse-Logistik-Kanäle entnommen oder über offizielle Vertriebskanäle geliefert werden. Im Falle einer Anfrage im Zusammenhang mit Produkt können Sie uns unter folgenden Details kontaktieren.<br></td></tr>';			
			$note .= '<tr><td align="left">Telefon-Nr. :  <strong>+44-3301240338</strong> </td> <td align="right">E-Mail - <strong>info@esljersey.com</strong></td></tr>'; 
			$note .= '</table>';
			$note .= '</div>';
			
		}else if($source_country == 'FR'){
 			$note = '<br><div style="border:1px solid; padding:5px; width:93%">';
			$note .= '<table>';
			$note .= '<tr><td colspan="2"><strong>Note-</strong> L\'article que vous avez acheté est un emballé en vrac. Ces accessoires sont de véritables produits soit pris à partir de boîtes de combinés par nos canaux de logistique inversée ou fournis par les canaux de distribution officiels. En cas de requête relative au produit, vous pouvez nous contacter sur les détails suivants.<br></td></tr>';			
			$note .= '<tr><td align="left">Téléphone non : <strong>+44-3301240338</strong> </td> <td align="right">email - <strong>info@esljersey.com</strong></td></tr>'; 
			$note .= '</table>';
			$note .= '</div>';
		
		}else if($source_country == 'ES'){
		
			$note = '<br> <br><div style="border:1px solid; padding:5px; width:93%">';
			$note .= '<table>';
			$note .= '<tr><td colspan="2"><strong>Nota:</strong> el artículo que has comprado es un embalado a granel. Estos accesorios son productos genuinos tomados de cajas de teléfonos a través de nuestros canales de logística inversa o suministrados a través de canales de distribución oficiales. En caso de cualquier consulta relacionada con el producto, puede ponerse en contacto con nosotros en los siguientes detalles.<br></td></tr>';			
			$note .= '<tr><td align="left">Telefono no : <strong>+44-3301240338</strong> </td> <td align="right">Correo electronico - <strong>info@esljersey.com</strong></td></tr>'; 
			$note .= '</table>';
			$note .= '</div>';	
				
		}else if($source_country == 'IT'){
		
			$note = '<br><div style="border:1px solid; padding:5px; width:93%">';
			$note .= '<table>';
			$note .= '<tr><td colspan="2"><strong>Nota:</strong> l\'articolo acquistato è un imballato. Questi accessori sono prodotti genuini presi da scatole portatili attraverso i nostri canali di logistica inversa o forniti attraverso canali di distribuzione ufficiali. In caso di qualsiasi query relativa al prodotto potete contattarci in seguito ai seguenti dettagli.<br></td></tr>';			
			$note .= '<tr><td align="left">Numero di telefono: <strong>+44-3301240338</strong> </td> <td align="right">Posta elettronica - <strong>info@esljersey.com</strong></td></tr>'; 
			$note .= '</table>';
			$note .= '</div>';
		
		}else{
			$note = '<br><div style="border:1px solid; padding:5px; width:93%">';
			$note .= '<table>';
			$note .= '<tr><td colspan="2"><strong>Note-</strong> The item you purchased is a bulked packed. These accessories are genuine products either taken from handset boxes through our reverse logistics channels or supplied through official distribution channels. In case of any query related to product you may contact us on following details.<br></td></tr>';
			$note .= '<tr><td align="left">Phone no :  <strong>+44-3301240338</strong> </td> <td align="right">Email - <strong>info@esljersey.com</strong></td></tr>';
			$note .= '</table>';
			$note .= '</div>';
		}
		return $note;
	}	 	
		
	public function getBarcode( $barcode = null )
	{
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 
	
		$barcodePath 		= 	WWW_ROOT .'img/product/barcodes/';   
		$barcodecorreosPath = 	WWW_ROOT .'img/orders/correos_barcode/';  
		
		$colorFront 		= 	new BCGColor(0, 0, 0);
		$colorBack 			= 	new BCGColor(255, 255, 255);
		
		// Barcode Part
		$orderbarcode=$barcode;
		$code128 = new BCGcode128();
		$code128->setScale(2);
		$code128->setThickness(15);
		$code128->clearLabels( false );
		$code128->SetLabel($barcode);
		$code128->setForegroundColor($colorFront);
		$code128->setBackgroundColor($colorBack);
		$code128->parse($orderbarcode);
		// Drawing Part
		$imgOrder128=$orderbarcode;
		$imgOrder128path=$barcodePath.'/'.$imgOrder128.".png";
		$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
		$drawing128->setBarcode($code128);
		$drawing128->draw();
		$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
		############ Routing Barcode #######
		
	}	
	
	public function getPicklistBarcode( $picklist_barcode = null )
	{
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 
	
		$barcodePath 		= 	WWW_ROOT .'img/printPickList/'; 	
		$colorFront 		= 	new BCGColor(0, 0, 0);
		$colorBack 			= 	new BCGColor(255, 255, 255);
		
		// Barcode Part 
		 
		$code128 = new BCGcode128();
		$code128->setScale(2);
		$code128->setThickness(15);
		$code128->clearLabels( false );
		$code128->SetLabel($picklist_barcode);
		$code128->setForegroundColor($colorFront);
		$code128->setBackgroundColor($colorBack);
		$code128->parse($picklist_barcode);
		// Drawing Part
		 
		$imgOrder128path=$barcodePath.'/'.$picklist_barcode.".png";
		$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
		$drawing128->setBarcode($code128);
		$drawing128->draw();
		$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
		############ Routing Barcode #######
		
	}
		
	public function jerseyPostLabel( $splitOrderId = null, $openOrderId = null )
	{
		//$this->loadModel('MergeUpdate'); 
		//$this->loadModel('Product'); 
		if($splitOrderId == '1680467-1'){
			echo "NNNNN";		 
			exit;
		}
		 
		$openOrderId	=	$openOrderId ;
		$splitOrderId	=	$splitOrderId ;
		App::import('Controller', 'Linnworksapis');
		$obj = new LinnworksapisController();
		
		$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
	
		$order				=		$obj->getOpenOrderById( $openOrderId );
		 
		$postalservices		=		$order['shipping_info']->PostalServiceName;
		$destination		=		$order['customer_info']->Address->Country;
		$total				=		$order['totals_info']->TotalCharge ? $order['totals_info']->TotalCharge : $order['totals_info']->Subtotal;
		$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
		$assignedservice	=	 	$order['assigned_service'];
		$courier			=	 	$order['courier'];
		$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
		$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
		$barcode			=	 	$order['assign_barcode'];
		
		$subtotal			=		$order['totals_info']->Subtotal;
		$totlacharge		=		$order['totals_info']->TotalCharge;
		$ordernumber		=		$order['num_order_id'];
		$fullname			=		$order['customer_info']->Address->FullName;
		$address1			=		$order['customer_info']->Address->Address1;
		$address2			=		$order['customer_info']->Address->Address2;
		$address3			=		$order['customer_info']->Address->Address3;
		$town				=	 	$order['customer_info']->Address->Town;
		
		$resion				=	 	$order['customer_info']->Address->Region;
		$postcode			=	 	$order['customer_info']->Address->PostCode;
		$country			=	 	$order['customer_info']->Address->Country;
		$phone				=	 	$order['customer_info']->Address->PhoneNumber;
		$company			=	 	$order['customer_info']->Address->Company;
		$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
		$postagecost		=	 	$order['totals_info']->PostageCost;
		$tax				=	 	$order['totals_info']->Tax;
		$currency			=	 	$order['totals_info']->Currency;
		$barcode  			=   	$order['assign_barcode'];
		$items				=	 	$order['items'];
		$templateId			=	 	$order['template_id'];
		$subSource			=	 	$order['sub_source'];
		$envelope_weight	=	 	$splitOrderDetail['MergeUpdate']['envelope_weight'];
		
		$str = ''; 
		
		$totlaWeight	=	0;
		$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
		$ref_code = trim($splitOrderDetail['MergeUpdate']['provider_ref_code']);
		$weight = 0;
		foreach( $skus as $sku )
		{
			$newSkus[] = explode( 'XS-', $sku);

			
			foreach($newSkus as $newSku)
				{
					
					$totalvalue = $total;
					$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
					$getsku = 'S-'.$newSku[1];
					$qty = $newSku[0];
					$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
					$title	=	$getOrderDetail['Product']['product_name'];
					$weight	=	$weight + ($getOrderDetail['ProductDesc']['weight'] * $qty) ;
					$totlaWeight = $weight * $qty;
					$catName	 =	$getOrderDetail['Product']['category_name'];
					if($country == 'United States')
					{
						$str .= '<tr>
							<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.$catName.'</td>
							<td valign="top" class="center norightborder bottomborder">'.$qty * $getOrderDetail['ProductDesc']['weight'].'</td>';
						$str .= '</tr>';
					}
					elseif($country == 'Spain')
						{
							$codearray = array("DP1", "FR7");
							$spaincode = array("mad_air","mad_road","cat_air","cat_road","sp_air","sp_road");
					
							if(in_array($ref_code, $spaincode))
							{
								$str .='<tr>
										<td  width="60%"  style="border:1px solid #000; padding:5px;">'.$catName.'</td>
										<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty * $getOrderDetail['ProductDesc']['weight'].' Kg</td>
										<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty.'</td>';
								$str .= '</tr>';
							} else {
								$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$catName.'</td>
									<td valign="top" class="center noleftborder rightborder bottomborder">'.$qty.'</td>
									<td valign="top" class="center norightborder bottomborder">'.$qty *$getOrderDetail['ProductDesc']['weight'].' Kg</td>';
								$str .= '</tr>';
							}
						}
					else
					{
						$str .= '<tr>
								<td valign="top" class="noleftborder rightborder bottomborder">'.$qty.'X'.substr($title, 0, 25 ).'</td>
								<td valign="top" class="center norightborder bottomborder">'.$qty * $getOrderDetail['ProductDesc']['weight'].' Kg</td>';
						$str .= '</tr>';
					}
				}
					unset($newSkus);
		}
		
		$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forl?-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia');
		
		$addData['sub_source'] 	= 	$order['sub_source'];
		$addData['company'] 	= 	$company;
		$addData['fullname'] 	= 	$fullname;
		$addData['address1'] 	= 	$address1;
		$addData['address2'] 	= 	$address2;
		$addData['address3'] 	= 	$address3;
		$addData['town'] 		= 	$town;
		
		if( $country == 'Italy' )
		{
			if(array_key_exists(strtoupper($resion),$provinces)){
				$addData['resion'] = $resion;
			}else {
				$addData['resion'] = array_search(ucfirst($resion), $provinces);
			}
		} else {
				$addData['resion'] = $resion;
		}
		
		$addData['postcode'] 	= 	$postcode;
		$addData['country'] 	= 	$country;
		
		
		$address = $obj->getCountryAddress( $addData );
		
		$CARTAS = '';
		if(trim($country) == 'Spain') { 
			$CARTAS = 'CARTAS '.substr($postcode, 0, 1);
			$routingcode = $this->getCorreosBarcode( $postcode, $splitOrderId);
			$corriosImage		=		$routingcode.'.png';
			if(substr($postcode, 0, 1) == 0){
				$CARTAS = 'CARTAS '.substr($postcode, 1, 1);
			}	
		}
									
		$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
		$currentdate		=	 	date("j, F  Y");
		$BarcodeImage		=		$splitOrderDetail['MergeUpdate']['order_barcode_image'];
		//$corriosImage		=		$postcode.'90019.png';
		
		
		$setRepArray = array();
		$setRepArray[] 					= $address1;
		$setRepArray[] 					= $address2;
		$setRepArray[] 					= $address3;
		$setRepArray[] 					= $town;
		$setRepArray[] 					= $resion;
		$setRepArray[] 					= str_replace( 'Spain' , 'Spain', $address); 
		$setRepArray[] 					= $country;
		$barcode  						= $order['assign_barcode'];
		$barcodenum						= explode('.', $barcode);
		//$barcodePath  					= Router::url('/', true).'img/orders/barcode/';
		$barcodePath  					= WWW_ROOT.'img/orders/barcode/';
		//$barcodeCorriosPath  			= Router::url('/', true).'img/orders/correos_barcode/';	
		$barcodeCorriosPath  			= WWW_ROOT.'img/orders/correos_barcode/';
			
		
		$barcodenum						= explode('.', $barcode);
		$this->loadModel('BulkLabel'); 
		/**************** for tempplate *******************/
		$countryArray = Configure::read('customCountry');
		if($country == 'United Kingdom'){
			$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
		}elseif($country == 'Spain'){
			$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
		} else if($country != 'United Kingdom'){
			$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Rest Of EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
		} else {
			$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Rest Of EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
		}
		
		
			if($splitOrderId == '1680467-1'){
				 pr($labelDetail);			 
				exit;
			}
		
			if($subSource == 'Marec_FR' || $subSource == 'Marec_DE' || $subSource == 'Marec_IT' || $subSource == 'Marec_ES' || $subSource == 'Marec_uk'){
				$company 	=  'Marec';
			} else if($subSource == 'CostBreaker_CA' || $subSource == 'CostBreaker_UK' || $subSource == 'CostBreaker_DE' || $subSource == 'CostBreaker_FR' || $subSource == 'CostBreaker_ES' || $subSource == 'CostBreaker_IT' || $subSource == 'CostBreaker_USABuyer' ){
				$company 	=  'CostBreaker';
			} else if($subSource == 'Tech_Drive_UK' || $subSource == 'Tech_Drive_FR' || $subSource == 'Tech_drive_ES' || $subSource == 'Tech_drive_DE' || $subSource == 'Tech_Drive_IT' ){
				$company 	= 'Tech Drive Supplies';
			} else if($subSource == 'RAINBOW RETAIL DE' || $subSource == 'Rainbow Retail' || $subSource == 'Rainbow_Retail_ES' || $subSource == 'Rainbow_Retail_IT'){
				$company 	= 'Rainbow Retail';
			}
			
			$this->loadModel( 'ReturnAddre' );
			$ret_add = $this->ReturnAddre->find('first', array( 'conditions' => array( 'company' =>  $company) ) );
			$return_address = $ret_add['ReturnAddre']['return_address'];
		
			$html           =  $labelDetail['BulkLabel']['html'];
			
		if($splitOrderId == '18766d61-1'){
			echo '=====';
			echo $html;
			exit;
		}
		
		$barcodeimg = '<img src='.$barcodePath.$BarcodeImage.'>';
		
		$setRepArray[] 	=    $barcodeimg;
		$setRepArray[] 	=    $barcodenum[0];
		$setRepArray[]	=	 $str;
		$setRepArray[]	=	 $totlaWeight + $envelope_weight;
		$setRepArray[]	=	 $tax;
		$setRepArray[]	=	 $currentdate;
		$logopath		=	 Router::url('/', true).'img/';
		$setRepArray[]	=	 '<img src="'.$logopath.'logo.jpg" >';
		$logo			=	 '<img src="'.$logopath.'logo.jpg" >';
		$signature		=	 '<img src="'.$logopath.'sig.jpg" >';
		
		$setRepArray[]	=	 $logo;
		$setRepArray[]	=	 $signature;
		$setRepArray[]	=	 $splitOrderId;
		$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
		
		$tempId			=	$splitOrderDetail['MergeUpdate']['lable_id'];
		$countryCode	=	$splitOrderDetail['MergeUpdate']['country_code'];
		
		$setRepArray[]	=	 $countryCode;
		$setRepArray[]	=	 $weight;
		$conversionRate	=	Configure::read( 'conversionRate' );
		 
		if($currency == 'GBP')
		{
			$totalvalue = $totalvalue;
		}
		elseif($country == 'Spain' && $currency == 'EUR')
		{
			$totalvalue = $totalvalue;
		}
		elseif($country == 'United States')
		{
			$totalvalue = $totalvalue;
		}
		else
		{
			$totalvalue = number_format($totalvalue/$conversionRate, 2, '.' ,'');
		}
		
		if($totalvalue >= 1000  && $labelDetail['BulkLabel']['service_provider'] == 'Jersey Post' && $country == 'United States' ){
			$numbers = array("97.11","98.32","98.52","99.72","99.94","150.27","200.45","270.62","370.82","422.11","472.31","530.71");
			shuffle($numbers);					
			$totalvalue = $numbers[0];
		}
		else if($totalvalue >= 22  && $labelDetail['BulkLabel']['service_provider'] == 'Jersey Post' && $country != 'United States'){
			$numbers = array("18.11","18.22","18.32","18.52","18.72","18.94","19.27","19.45","19.62","19.82","20.11","20.31","20.71","21.22","21.41", "21.61","21.51");
			shuffle($numbers);					
			 	$totalvalue = $numbers[0];
		}
 
		$setRepArray[]	= $totalvalue;
		
		if($splitOrderDetail['MergeUpdate']['track_id'] != '')
		{
			$trackingnumber = $splitOrderDetail['MergeUpdate']['track_id'];
		}
		else
		{
			$trackingnumber = 'A'.mt_rand(100000, 999999);
		}
		 
		$setRepArray[]	=	 $trackingnumber;
		$setRepArray[]	=	 $subSource;
		$setRepArray[]	=	 $CARTAS;
		$setRepArray[]	=	 $splitOrderDetail['MergeUpdate']['reg_num_img'];
		$setRepArray[]	=	 '<img src='.$barcodeCorriosPath.$corriosImage.'>';
		$setRepArray[]	=	($phone != '') ? 'Telefono del cliente:'.$phone : '';
		$setRepArray[]	=	$return_address;
		$setRepArray[]	=	WWW_ROOT;
		$total_weight_envelope_weight = $totlaWeight + $envelope_weight;
		
		$cssPath = WWW_ROOT .'css/';
		$imgPath = Router::url('/', true) .'img/';
		//$sinimage	=	 '<img src='.$imgPath.'signature.png>';
		
		$this->loadModel( 'ReturnAddre' );
		$ret_add = $this->ReturnAddre->find('first', array( 'conditions' => array( 'company' =>  $company) ) );
		$return_address = $ret_add['ReturnAddre']['return_address'];
		$html = '<div style="font-family:normal;">
				 <table width=78%>
					<tr width=100%>
						<td width=50% style="border: 1px solid #ccc;padding-left:15px;">
							<table>
								<tr>									
									<td>	
										<div style="padding-left:3px" ><strong>Remitente</strong></div>				
										<div style="padding-left:3px" ><strong>ESL LOGISTICS</strong><br>CTL CHAMARTIN<br>AVDA. PIO XII 106-108<br>28070 MADRID</div>
									</td>
									<td>
										<table cellpadding="0" cellspacing="0" width="80%" style="border:1px solid #000;">
											<tr>					 
												<td>
													<div style="text-align: center;font-weight:bold; height:80px;">
														<div style="border-bottom:1px solid #000; padding:2px;"> FRANQUEO <br> PAGADO </div>
														<div style="padding:2px 5px;">Distribuci&#243;n <br> Internacional </div>
													</div>
												</td>				
											</tr>
										</table>
									</td>	
								</tr>
								<tr>
									<td align="left" colspan="2" > 
										<div style="border-bottom:1px solid; width:80%;font-size:12px; margin-top: 10px; "><strong>Destinatario</strong></div>		
										 <div style="font-size:13px; padding-left:3px">_ADDRESS_</div>
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center" style="padding-bottom:1px; padding-top:1px;"><div>ENV�O ORDINARIO</div> <div style="text-align: center;"><center> _BARCODEIMAGE_</center> <div></td>
								</tr> 
								<tr>
									<td colspan="2" align="center"><div style="margin-top:1px;">_CORRIOSBARCODE_</div></td>
								</tr> 
							</table>
						</td>
						
						<td width=50% style="border: 1px solid #ccc" valign="top">
							<table style="border:1px solid #000;">
								<tr valign="top">
									<td width=48%><strong>CUSTOMS DECLARATION</strong></td>
									<td style="text-align:right;"><strong>CN 22</strong></td>
								</tr>
								<tr valign="top">
									<td width=48% style="font-size:10px;">DECLARATION EN DOUANE</td>
									<td style="text-align:right;">May be opened officially</td>
								</tr>
								<tr>
									<td>Great Britain / Important!</td>
									<td style="text-align:right;"></td>
								</tr>
								<tr>
									<td width="15%"><span style="height:25;width:25; border:2px solid gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Gift</td>
									<td width="25%"><span style="height:25;width:25; border:2px solid gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Documents</td>
								</tr>
								<tr>
									<td width="30%"><span style="height:25;width:25; border:2px solid gray">&nbsp;X&nbsp;</span>Commercial</td>
									<td width="30%"><span style="height:25;width:25; border:2px solid gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Merchandise</td>
								</tr>
							</table>
							<table class="" cellpadding="1px" cellspacing="0" style="border-collapse:collapse" width="100%">
								<tr>
									<th width="60%" style="border:1px solid #000; padding:5px;">Quantity and detailed</th>
									<th valign="top" width="20%"  style="border:1px solid #000; padding:5px;">Weight</th>
									<th valign="top" width="20%"  style="border:1px solid #000; padding:5px;">Qty</th>
								</tr>
								_ORDERDETAIL_
								
								<tr>
									<td  width="60%" style="border:1px solid #000; padding:10px; padding:5px;"> <span class="bold">For commercial items only</td>
									<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">WEIGHT '.$total_weight_envelope_weight.' Kg</td>
									<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">VALUE '.$currency.' _TOTALVALUE_</td>
								</tr>
								</table>
									<div class="fullwidth" style="border:1px solid #000; padding:5px;"><p>I the undersigned, whose name and address are given on the item, certify that the particulars given in this declaration are correct and that this item does not contain any dangerous article or articles prohibited by legislation or by postal or customs regulations.</p>
								<table>
								<tr>
									<td class="date bold">_CURRENTDATE_</td>
									<td><img src="_DYURL_/img/signature.png" height="50px"></td>
								</tr>
								</table>	
								
								</table>
						</td>
					</tr>
				 </table></div>';	
		$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
		
 		
		$html 	= $obj->setReplaceValueLabel( $setRepArray, $html );
		return $html;
	}
		
		public function getCorreosBarcode( $postcode = null, $splitOrderID = null )
		{
			
			
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 

			$barcodePath 		= 	WWW_ROOT .'img/orders/barcode/';   
			$barcodecorreosPath = 	WWW_ROOT .'img/orders/correos_barcode/';  
			
			$colorFront 		= 	new BCGColor(0, 0, 0);
			$colorBack 			= 	new BCGColor(255, 255, 255);
			
			// Barcode Part
			$orderbarcode=$splitOrderID;
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->clearLabels();
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->parse($orderbarcode);
			// Drawing Part
			$imgOrder128=$orderbarcode;
			$imgOrder128path=$barcodePath.'/'.$imgOrder128.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			############ Routing Barcode #######
			//$destination = $countryCode;
			//$origin ="90019";
			//$routingcode=$destination.$origin;
			
			 
			 
			$routingcode = $this->getCorreosCalculatedBarcode($postcode);
			$routingcode128 = new BCGcode128();
			$routingcode128->setScale(1);
			$routingcode128->setThickness(40);
			$routingcode128->setForegroundColor($colorFront);
			$routingcode128->setBackgroundColor($colorBack);
			$routingcode128->parse(array(CODE128_B, $routingcode));
			//$routingcode128->parse($routingcode);
			// Drawing Part
			$routingimg =$routingcode;
			$routingdrawing128path=$barcodecorreosPath.'/'.$routingimg.".png";
			$routingdrawing128 = new BCGDrawing($routingdrawing128path, $colorBack);
			$routingdrawing128->setBarcode($routingcode128);
			$routingdrawing128->draw();
			$routingdrawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
			return $routingcode;
		}
		
		public function getCorreosCalculatedBarcode($postcode = null)
		 {
			$sum_accii = 0;
			
			$code = 'ORDCP'.$postcode; 		
			for($i=0; $i < strlen($code); $i++){
			  $sum_accii += ord($code[$i]);		 
			}
			$remainder = fmod($sum_accii,23);
			$table = array('T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E');
			$control_character = $table[$remainder];
			return	$code.$control_character;			
		} 
		
		public function royalMailLabel( $split_id = null, $order_id = null )
		{
			return '<img src="'.WWW_ROOT.'royalmail/labels/'.$split_id.'.png" width="45%">'; 
			exit;
 		}
		
		public function getPostnlLabel( $split_id = null,$postnl_barcode = null )
		{
 			$postnl_label = WWW_ROOT .'postnl/label_'.$split_id.'.jpg';
			if(!file_exists($postnl_label)){
				$ex = explode("-",$split_id);
				App::Import('Controller', 'Postnl'); 
				$postnl = new PostnlController;
				$postnl->Labelling($ex[0]);
			}
			
			if(substr($postnl_barcode,0,2) == 'RI'){	 
				$label  = '<td valign="top" style="padding:0px 0px 5px 12px;">						
				<div style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
				transform-origin: 50% 50%;height:270px; width:450px;"><img src="'.WWW_ROOT.'postnl/labels/'.$split_id.'.jpg" width="350px" style="margin-left: -110px; margin-right: -110px;margin-top: -70px;"></div></td>';
				
				$label  .='<td><img src="'.WWW_ROOT.'img/orders/barcode/'.$split_id.'.png" style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;height:60px; margin-left: -28px;" width="224px"></td>';
				}else{
					$label  = '<td valign="top" style="padding:0px 0px 5px 12px;">						
				<div style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
				transform-origin: 50% 50%;height:270px; width:450px;">
				<img src="'.WWW_ROOT .'postnl/labels/label_'.$split_id.'.jpg" width="350px" style="margin-left: -110px; margin-right: -110px;margin-top: -70px;"></div></td><td></td>';
			}
						
 			return $label ; 
			exit;
 		}
		
		public function royalMailLabel123( $split_id = null, $order_id = null )
		{
			//.$details['split_order_id'].
			$details = $this->getDetail( $split_id, $order_id );
			
			$imgPath = WWW_ROOT.'royalmail/barcode/2d_matrix_'.$details['split_order_id'].'.png';
			
			if(file_exists($imgPath) &&  filesize($imgPath) > 100 )
			{
 				$service = '48';
				if($details['type'] == 1 ){ 
					$service = '24';
				}
				
				$box_number = 824; 
				if(strpos($details['sub_source'],'CostBreaker')!== false){
					$box_number = 824;
				}
				else if(strpos($details['sub_source'],'RAINBOW')!== false){
					$box_number = 825;;
				}else if(strpos($details['sub_source'],'Marec')!== false){
					$box_number = 826;
				}	
				elseif(strpos($details['sub_source'],'BBD')!== false){
					$box_number = 827;
				}
				else if(strpos($details['sub_source'],'Tech_Drive')!== false){
					$box_number = 828;
				} 
				
 				
				$h = '<div style=" width:320px; height:auto;margin-top:-270px; margin-left:-200px;">';
				if(strpos($details['sub_source'],'EBAY')!== false){  
					$h = '<div style=" width:320px; height:auto;margin-top:-270px; margin-left:-150px;">';
				}
				if(strpos($details['sub_source'],'Marec')!== false){  
					$h = '<div style=" width:320px; height:auto;margin-top:-250px; margin-left:-250px;">';
					unlink(WWW_ROOT.'img/printPDF/Bulk/Label_Slip_2078223-1.pdf');
				}
				 
				$html = $h.'<div class="container"  style=" border:1px solid #000;">
						<table  width="100%" cellpadding="0" cellspacing="10">
						<tbody>
						<tr>
						<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/Royal_Mail.png" width="115px"></td>
						<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/'.$service.'.jpg" width="50px"></td>
						<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/royal.png" width="100px" height="70px"></td>
						</tr>
						</tbody>
						</table>
						 
						
						<table width="100%" cellpadding="0" cellspacing="10" style="border-bottom:1px solid #000; border-top:1px solid #000;">
						<tbody>
						<tr>
						<td valign="top" width="50%" align="left" >
						<img src="'.WWW_ROOT.'royalmail/barcode/2d_matrix_'.$details['split_order_id'].'.png" width="83px" height="83px" style="margin-left:10px;">
						</td>
					
						</tr>
						</tbody>
						</table>
						
						<table width="100%"  cellpadding="0" cellspacing="10" >
						<tbody>
						<tr>
						<td valign="top" width="75%">';
						$html .='<strong>'. ucwords(strtolower($details['FullName'])).'</strong><br>';
			
						if(strlen($details['Address1']) > 25 || strlen($details['Address2']) > 25){
						
							$lines = explode("\n", wordwrap(htmlentities($details['Address1']) .' '.htmlentities($details['Address2']), '25'));
							if(isset($lines[0]) && $lines[0] != ''){
								$html .=  $lines[0].'<br>';
							}
							if(isset($lines[1]) && $lines[1] != ''){
								$html .=  $lines[1].'<br>';
							}
							if(isset($lines[2]) && $lines[2] != ''){
								$html .=  $lines[2].'<br>';
							}
						
						}else{
							$html .= ucfirst($details['Address1']).'<br>';
							if($details['Address2']) $html .= ucfirst($details['Address2']).'<br>';
						}
						
						
						$html .= ucfirst($details['Town']).'<br>';
						$html .= $details['PostCode'];
						$html .= '</div></td>';
						
						
						$html .='<td valign="bottom" width="25%" align="left">
						<img src="'.WWW_ROOT.'royalmail/img/'.$box_number.'.png" style="height:100px; margin-left:20px;">
						 </td>
						 </tr>
						</tbody>   
						 </table>
						
						 
						<table width="100%" style="border-top:1px solid #000;" >
						<tbody>
						<tr>
						<td valign="top" align="center" style="padding-top:5px;"><p style="font-family:chevin; font-size:12px;">Customer Reference:'.$details['split_order_id'].'</p>
						
						</td>
						</tr>
						</tbody>
						</table>
						 </div>
						 <div  style="text-align:center;width:100%;padding-top:25px;">
							<img src="'.WWW_ROOT.'img/orders/barcode/'.$details['split_order_id'].'.png" style="text-align:center">
						 </div></div>';
						 if( $details['split_order_id']== '2080973-1'){
						 	//echo $html;
						 }
						return $html;
					}else{
					return 'error';
				}
		}
		
		public function getDetail( $split_id = null, $order_id = null )
		{
			
			$this->loadModel( 'Country' );
			App::import( 'Controller' , 'Cronjobs' );		
			$objController 	= new CronjobsController();
			$getorderDetail	= $objController->getOpenOrderById($order_id);
			 
			$cInfo = $getorderDetail['customer_info'];
			$Address1 = $cInfo->Address->Address1;
			$Address2 = $cInfo->Address->Address2;
			$order->TotalsInfo = $getorderDetail['totals_info'];
			$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
			
			$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_id,'MergeUpdate.service_provider' => 'Royalmail')));	
			$type = 2; 
			if($orderItem['MergeUpdate']['service_name'] == 'royalmail_24'){
				$type = 1;
			}
			$service_format = 'P';
			if($orderItem['MergeUpdate']['service_name'] == 'royalmail_LL_48'){
				/*  F	Inland Large Letter, L	Inland Letter, N	Inland format Not Applicable, P	Inland Parcel */
				$service_format = 'F';
			}
			
			$_details = array('merge_id'=> $order_id,
						'split_order_id'=> $split_id,
						'sub_total' => number_format($order->TotalsInfo->Subtotal,2),
						'order_total'=> number_format($order->TotalsInfo->TotalCharge,2),
						'postage_cost' => number_format($order->TotalsInfo->PostageCost,2),
						'order_currency'=>$order->TotalsInfo->Currency,			
						'Company'=>$cInfo->Address->Company,
						'FullName'=> utf8_decode($cInfo->Address->FullName),
						'Address1' =>utf8_decode($Address1),
						'Address2' =>utf8_decode($Address2),
						'Town' =>utf8_decode($cInfo->Address->Town),
						'Region' =>utf8_decode($cInfo->Address->Region),
						'PostCode' =>$cInfo->Address->PostCode,
						'CountryName'=>$cInfo->Address->Country,
						'CountryCode' =>$country_data['Country']['iso_2'],
						'PhoneNumber'=>$cInfo->Address->PhoneNumber	,
						'sub_source' => $getorderDetail['sub_source'],
						'type' 		=> $type,
						'service_format' => $service_format,
						'item_count'=> 1,
						'building_name' => '',
						'building_number' => 0,
					);
					return $_details;
			
		}
		
		public function batckPicklistBulkOrderProcess($picklist_id = 0, $page = '')
		{
			$this->layout = '';
			$this->autoRander = false;
			$this->loadModel('OpenOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('ScanOrder');
			$this->loadModel('OrderLocation');
 			$ord_msg    =   array();
			$print_msg  =   '';	
			$ids 	    =   array();			
			$userid		=	$_SESSION['Auth']['User']['id'];
			$firstName 	= 	( $_SESSION['Auth']['User']['first_name'] != '' ) ? $_SESSION['Auth']['User']['first_name'] : '_';
			$lastName 	= 	( $_SESSION['Auth']['User']['last_name'] != '' ) ? $_SESSION['Auth']['User']['last_name'] : '_';
			$pcName 	= 	( $_SESSION['Auth']['User']['pc_name'] != '' ) ? $_SESSION['Auth']['User']['pc_name'] : '_';
			
			$batch_num  =   'BPLN'.date('YmdHis');
			$username   =   $firstName.' '.$lastName;
		 
			if(isset($this->request->data['picklist_inc_id']) && $this->request->data['picklist_inc_id'] > 0){
  				$picklist_id	=	$this->request->data['picklist_inc_id'];
 			}
	 					
			$skip_count = 0 ;							
			$brt_orders = $dhl_orders = $uk_priority_orders = $jp_tracked_orders = [];
										
			if($picklist_id > 0){
				$batch_orders	=	$this->MergeUpdate->find('all', array('fields'=>['id','product_order_id_identify','service_provider','provider_ref_code','postal_service'], 'conditions'  => array( 'picklist_inc_id' => $picklist_id, 'status' => 0,'pick_list_status' => 1,'over_sell_user IS NULL'  ) ) );
								
				foreach( $batch_orders as $batch_order ) 
				{
				
					if($batch_order['MergeUpdate']['service_provider'] == 'DHL' ){
						$dhl_orders[] = $batch_order['MergeUpdate']['product_order_id_identify']; 
						$skip_count++;
					}else if($batch_order['MergeUpdate']['brt'] == '1' ){
						$skip_count++;
						$brt_orders[] = $batch_order['MergeUpdate']['product_order_id_identify'];
					}else if($batch_order['MergeUpdate']['service_provider'] == 'Jersey Post'  && $batch_order['MergeUpdate']['provider_ref_code'] == 'UKP'){
						$skip_count++;
						$uk_priority_orders[] = $batch_order['MergeUpdate']['product_order_id_identify'];
					}else if($batch_order['MergeUpdate']['service_provider'] == 'Jersey Post' && $batch_order['MergeUpdate']['postal_service'] == 'Tracked'){
						$skip_count++;
						$jp_tracked_orders[] = $batch_order['MergeUpdate']['product_order_id_identify'];
					}
					else{
						$ids[]	=	$batch_order['MergeUpdate']['product_order_id_identify'];
					}
					
				}
			}
			 
			if(isset($this->request->data['split_order_id']) && $this->request->data['split_order_id'] != ''){
				$ids[]	= $this->request->data['split_order_id'];
			}
			$over_sell = [] ;
			
			$location_orders =	$this->OrderLocation->find('all', array('fields'=>['split_order_id'], 'conditions'  => array( 'picklist_inc_id' => $picklist_id, 'status' => 'active','pick_list' => 1,'available_qty_bin' => 0 ) ) ); 
			
			if(count($location_orders) > 0 ){
				foreach($location_orders as $l){
					if( $l['OrderLocation']['split_order_id'] != ''){
						$over_sell[] = $l['OrderLocation']['split_order_id'];
					}
				}			
			}
			 
 			if(count($ids) > 0){
			
				foreach($ids as $id)
				{
					$openOrderId	=	explode("-",$id)[0];
				  	$splitOrderId	=	$id;
					$console 		=   'dynamic_picklist_console';
					//pr($over_sell);
					if(count($over_sell) > 0){
						$over_sell[] = '111111-1';
						$_check	= $this->MergeUpdate->find('first', array('fields'=>['id','order_id','product_order_id_identify'], 'conditions' => ['label_status' => '2', 'product_order_id_identify' => $splitOrderId, 'product_order_id_identify NOT IN '=>  $over_sell,'over_sell_user IS NULL'] ) );
					 
					}else{
						$_check	= $this->MergeUpdate->find('first', array('fields'=>['id','order_id','product_order_id_identify'], 'conditions' =>[ 'label_status' => '2', 'product_order_id_identify' => $splitOrderId, 'over_sell_user IS NULL'] ) ); 
					
					}
					//pr($_check);
					 
					if( count($_check) > 0 )
					{
 						$this->MergeUpdate->updateAll(
								array('MergeUpdate.dispatch_console' => "'".$console."'",'MergeUpdate.assign_user' => "'".$username."'", 'MergeUpdate.user_name' => "'".$userid."'", 'MergeUpdate.pc_name' =>"'".$pcName."'",  'MergeUpdate.status' => '1','MergeUpdate.manifest_status' => '2','MergeUpdate.process_date' =>"'".date('Y-m-d H:i:s')."'", 'MergeUpdate.user_id' => $userid),
								 array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId,'MergeUpdate.status' => 0,'MergeUpdate.pick_list_status' => 1));
						 
  					
						App::import('Controller', 'Cronjobs');
						$cronjobtModel = new CronjobsController();
						$cronjobtModel->call_service_counter( $splitOrderId );
						 
 						$getmergeOrders	=	$this->MergeUpdate->find( 'all', array('conditions' => array( 'MergeUpdate.order_id' =>  $openOrderId )) );
						$checkStatus = 0;
						$checkNonProcessStatus = 0;
						
						foreach( $getmergeOrders as $getmergeOrder )
						{
							if( $getmergeOrder['MergeUpdate']['status'] == 0 ){
								$checkNonProcessStatus++;
							}else{
								$checkStatus++;
							}
						}
						
						if($checkNonProcessStatus == 0 && $checkStatus > 0)
						{
							$this->OpenOrder->updateAll(array('OpenOrder.status' => '1','OpenOrder.process_date' => "'".date('Y-m-d H:i:s')."'" ), array('OpenOrder.num_order_id' => $openOrderId));
							$scan_orders	=	$this->ScanOrder->find( 'all', array( 'conditions' => array( 'split_order_id' => $splitOrderId ) ) );
							
							foreach( $scan_orders as $scan_order)
							{
								$data['update_quantity'] 	= 	$scan_order['ScanOrder']['quantity'];
								$data['id'] 				= 	$scan_order['ScanOrder']['id'];
								$this->ScanOrder->saveAll( $data );
							}
							
							$log_file = WWW_ROOT .'logs/dynamic_log/dynamic_process_'.date('dmy').".log";
 							if(!file_exists($log_file)){
								file_put_contents($log_file, 'Order id'."\t".'PickList'."\t".'User'."\t".'DateTime'."\n");	
							}
 			  
							file_put_contents($log_file, $splitOrderId."\t".$picklist_id."\t".$_SESSION['Auth']['User']['first_name'].' '.$_SESSION['Auth']['User']['last_name']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND | LOCK_EX);
							
							$ord_msg[] = "Order {$splitOrderId} is processed.";
							$msg['status'] = 'ok';
						}
						else
						{
							$msg['con'] = "2";
							$ord_msg[] = "Order {$splitOrderId} is processed.";
						}
						
 			  	}else{				
					$print_msg = "Please print Label & Slip before processing.";
				}
				
			   }
			}else{				
				$ord_msg[]="No orders found for processing.";
			}
			
			if($skip_count > 0){
				$ord_msg = array();
			}
			
			if(count($brt_orders) > 0){
				$ord_msg[]= count($brt_orders) . ' BRT orders. So please go through normal console.';
			}
			if(count($dhl_orders) > 0){
 				$ord_msg[]= count($dhl_orders). ' DHL orders. So please go through normal console.';
			}
			if(count($uk_priority_orders) > 0){
				$ord_msg[]= count($uk_priority_orders). ' UK Priority orders. So please go through normal console.';
			}
			if(count($jp_tracked_orders) > 0){
				$ord_msg[]= count($jp_tracked_orders). ' JP Tracked orders. So please go for through console.';
			}
			 
			
			
 			$log_file = WWW_ROOT .'logs/dynamic_log/process_'.date('dmy').".log";
		
			if(!file_exists($log_file)){
				file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
			}
			file_put_contents($log_file, $_SESSION['Auth']['User']['email']."\t".$_SESSION['Auth']['User']['username']."\t".$_SESSION['Auth']['User']['pc_name']."\t".$picklist_id."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
		
			if($print_msg != ''){
				$msg['msg'] = $print_msg;
			}else{
				$msg['msg'] = implode(",",$ord_msg);
			}
			if($page == 'sorting'){
				return $msg;
			}else{
				echo json_encode($msg);				 
			}
			exit;			 
		}
		
		public function batckPicklistBulkOrderProcess_10June()
		{
			$this->layout = '';
			$this->autoRander = false;
			$this->loadModel('OpenOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('ScanOrder');
			$this->loadModel('OrderLocation');
 			$ord_msg    =   array();
			$print_msg  =   '';	
			$ids 	    =   array();			
			$userid		=	$this->Session->read('Auth.User.id');
			$firstName 	= 	( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName 	= 	( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$pcName 	= 	( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
			$batch_num  =   'BPLN'.date('YmdHis');
			$username   =   $firstName.' '.$lastName;
			$picklist_id	= 0;
			if(isset($this->request->data['picklist_inc_id']) && $this->request->data['picklist_inc_id'] != ''){
				
				$picklist_id	=	$this->request->data['picklist_inc_id'];
				$batch_orders	=	$this->MergeUpdate->find('all', array( 'fields'=>['id','product_order_id_identify'], 'conditions'  => array( 'picklist_inc_id' => $picklist_id, 'status' => 0,'pick_list_status' => 1 ,'over_sell_user IS NULL'))  );	
							
 				foreach( $batch_orders as $batch_order )
				{
					$ids[]	=	$batch_order['MergeUpdate']['product_order_id_identify'];
				}
				//$this->request->data['ids'] = "1012964-1,1011405-1,1011327-1,1009488-1,1008181-1";
				
			}
			
			if(isset($this->request->data['split_order_id']) && $this->request->data['split_order_id'] != ''){
				$ids[]	= $this->request->data['split_order_id'];
			}
			 
			$over_sell = [];
			
			$location_orders =	$this->OrderLocation->find('all', array('fields'=>['split_order_id'], 'conditions'  => array( 'picklist_inc_id' => $picklist_id, 'status' => 'active','pick_list' => 1,'available_qty_bin' => 0 ,'split_order_id IS NOT NULL' ) ) ); 
			
			if(count($location_orders) > 0 ){
				foreach($location_orders as $l){
					$over_sell[] = $l['OrderLocation']['split_order_id'];
				}			
			}
			
			if(count($ids) > 0){
			
				foreach($ids as $id)
				{
					$openOrderId	=	explode("-",$id)[0];
					$splitOrderId	=	$id;
					$console 		=   'dynamic_picklist_console';
					
					if(count($over_sell) > 0){
						$over_sell[] = '111111-1';
						$_check	= $this->MergeUpdate->find('first', array('fields'=>['id','order_id','product_order_id_identify'], 'conditions' => ['label_status' => '2', 'product_order_id_identify' => $splitOrderId, 'product_order_id_identify NOT IN '=>  $over_sell,'over_sell_user IS NULL'] ) );
					
					}else{
						$_check	= $this->MergeUpdate->find('first', array('fields'=>['id','order_id','product_order_id_identify'], 'conditions' =>[ 'label_status' => '2', 'product_order_id_identify' => $splitOrderId, 'over_sell_user IS NULL'] ) ); 
					
					}
					 
					
					if( count($_check) > 0 )
					{  						 
						$this->MergeUpdate->updateAll(
								array('MergeUpdate.dispatch_console' => "'".$console."'",'MergeUpdate.assign_user' => "'".$username."'", 'MergeUpdate.user_name' => "'".$userid."'", 'MergeUpdate.pc_name' =>"'".$pcName."'",  'MergeUpdate.status' => '1','MergeUpdate.manifest_status' => '2','MergeUpdate.process_date' =>"'".date('Y-m-d H:i:s')."'", 'MergeUpdate.user_id' => $userid),
								 array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId,'MergeUpdate.status' => 0,'MergeUpdate.pick_list_status' => 1));
								 
						App::import('Controller', 'Cronjobs');
						$cronjobtModel = new CronjobsController();
						$cronjobtModel->call_service_counter( $splitOrderId );
						 
 						$getmergeOrders	=	$this->MergeUpdate->find( 'all', array('conditions' => array( 'MergeUpdate.order_id' =>  $openOrderId )) );
						$checkStatus = 0;
						$checkNonProcessStatus = 0;
						
						foreach( $getmergeOrders as $getmergeOrder )
						{
							if( $getmergeOrder['MergeUpdate']['status'] == 0 ){
								$checkNonProcessStatus++;
							}else{
								$checkStatus++;
							}
						}
						
						if($checkNonProcessStatus == 0 && $checkStatus > 0)
						{
							$this->OpenOrder->updateAll(array('OpenOrder.status' => '1','OpenOrder.process_date' => "'".date('Y-m-d H:i:s')."'" ), array('OpenOrder.num_order_id' => $openOrderId));
							$scan_orders	=	$this->ScanOrder->find( 'all', array( 'conditions' => array( 'split_order_id' => $splitOrderId ) ) );
							
							foreach( $scan_orders as $scan_order)
							{
								$data['update_quantity'] 	= 	$scan_order['ScanOrder']['quantity'];
								$data['id'] 				= 	$scan_order['ScanOrder']['id'];
								$this->ScanOrder->saveAll( $data );
							}
							
							$log_file = WWW_ROOT .'logs/dynamic_log/dynamic_process_'.date('dmy').".log";
 							if(!file_exists($log_file)){
								file_put_contents($log_file, 'Order id'."\t".'PickList'."\t".'User'."\t".'DateTime'."\n");	
							}
 			  
							file_put_contents($log_file, $splitOrderId."\t".$picklist_id."\t".$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND | LOCK_EX);
							
							$ord_msg[] = "Order {$splitOrderId} is processed.";
							$msg['status'] = 'ok';
						}
						else
						{
							$msg['con'] = "2";
							$ord_msg[] = "Order {$splitOrderId} is already processed.";
						}
						
 			  	}else{				
					$print_msg = "Please print Label & Slip before processing.";
				}
				
			   }
			}else{				
				$ord_msg[]="No orders found for processing.";
			}
			
 			$log_file = WWW_ROOT .'logs/dynamic_log/process_'.date('dmy').".log";
		
			if(!file_exists($log_file)){
				file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'PicklistIncId'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
			}
			file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$picklist_id."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
		
			if($print_msg != ''){
				$msg['msg'] = $print_msg;
			}else{
				$msg['msg'] = implode(",",$ord_msg);
			}
			echo json_encode($msg);
			exit;			 
		}
		
		
		public function SortSingleOrder()
		{
			$this->layout = '';
			$this->autoRander = false;
			$this->loadModel('MergeUpdate');
			$this->loadModel('ServiceCounter'); 
			$ord_msg = '';
		
			if(isset($this->request->data['split_order_id']) && $this->request->data['split_order_id'] != ''){
			
				$result = $this->MergeUpdate->find('first', 
					array('conditions' => array('MergeUpdate.product_order_id_identify'=>$this->request->data['split_order_id'], 'MergeUpdate.status' => '1' , 
												'MergeUpdate.sorted_scanned' => '0' , 
												'MergeUpdate.scan_status' => '0')));
			
				if(count($result) > 0){
				
					$merge_inc_id 		= $result['MergeUpdate']['id'];
					$serviceName 		= $result['MergeUpdate']['service_name'];
					$serviceProvider 	= $result['MergeUpdate']['service_provider'];
					$serviceCode 		= $result['MergeUpdate']['provider_ref_code'];
					$country 			= $result['MergeUpdate']['delevery_country'];
					$serviceid 			= $result['MergeUpdate']['service_id'];
					
					if($serviceProvider == 'DHL'){
						$getCounterValue = $this->ServiceCounter->find( 'first' , array( 'conditions' => array('ServiceCounter.service_id' => $serviceid ) ) );
					}else{
						$getCounterValue = $this->ServiceCounter->find( 'first' , 
						array( 'conditions' => array( 'ServiceCounter.destination' => $country,'ServiceCounter.service_id' => $serviceid )) );
					}
					 
					
					$counter = 0;
					if( count($getCounterValue) > 0 ){  						
						$counter = $getCounterValue['ServiceCounter']['counter'];  
						$counter = $counter + 1;
						
						$orderCommaSeperated = '';
						if( $getCounterValue['ServiceCounter']['order_ids'] != '' ){
							$orderCommaSeperated = $getCounterValue['ServiceCounter']['order_ids'];
							$orderCommaSeperated = $orderCommaSeperated.','.$merge_inc_id;
						} else {
							$orderCommaSeperated = $merge_inc_id;
						}
						
						$ServiceCounter['id'] = $getCounterValue['ServiceCounter']['id'];    
						$ServiceCounter['counter'] = $counter;
						$ServiceCounter['order_ids'] = $orderCommaSeperated; 								     
						$this->ServiceCounter->saveAll( $ServiceCounter );
						
 						$MergeUpdate['id'] = $merge_inc_id;            
						$MergeUpdate['sorted_scanned'] = 1;         
						$MergeUpdate['scan_status'] = 1;  
						$MergeUpdate['scan_date'] = date('Y-m-d H:i:s');  
						$MergeUpdate['scanned_user'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');  
						$MergeUpdate['scanned_pc'] = $this->Session->read('Auth.User.pc_name');  					 
						$this->MergeUpdate->saveAll( $MergeUpdate );
					   
						   
						$log_file = WWW_ROOT .'logs/dynamic_log/sort_single_order_'.date('dmy').".log";
						if(!file_exists($log_file)){
							file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'Split Ordrer Id'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
						}
						file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$this->request->data['split_order_id']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
						
						$ord_msg = $result['MergeUpdate']['product_order_id_identify']. " is sored.";
					
					} else {
						$ord_msg = "Destination country and service is not found in service_counters table.";		
					}
					 
 				} else {
					$ord_msg = $this->request->data['split_order_id']. " is alreday sored.";				 
				}
			}else{
				$ord_msg = $this->request->data['split_order_id']. " is not found.";	
			}
			$msg['msg'] = $ord_msg;
			echo json_encode($msg);
			exit;
		}
		
		public function sortBatchAllOrders()
		{
			$this->layout = '';
			$this->autoRander = false;
			$this->loadModel('MergeUpdate');
 			 
			if(isset($this->request->data['picklist_inc_id']) && $this->request->data['picklist_inc_id'] > 0){
				$picklist_inc_id = $this->request->data['picklist_inc_id'];
			}
			
			$log_file = WWW_ROOT .'logs/dynamic_log/sort_batch_all_orders_req_'.date('dmy').".log";
		
			if(!file_exists($log_file)){
				file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'PicklistIncId'."\t".'DateTime'."\n");	
			}
 			   			
			file_put_contents($log_file, $_SESSION['Auth']['User']['email']."\t".$_SESSION['Auth']['User']['username']."\t".$_SESSION['Auth']['User']['pc_name']."\t".$picklist_inc_id	."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
			
			$all_msg = $this->sortbatchorders( $picklist_inc_id );
 			 
			$msg['cancel_ord_msg'] = ''; 
			if(count($all_msg['cancel_orders']) > 0){
				$msg['cancel_ord_msg'] = implode(",",$all_msg['cancel_orders']) ." orders are canceled. Please put back these orders items.";
			} 
 			if(count($all_msg['ord_msg']) > 1){
				$msg['status'] = 'ok';
			}
			$msg['msg'] = implode(",",$all_msg['ord_msg']);
				
			if($page == 'sorting'){
				return $msg;
			}else{
				echo json_encode($msg);
			}
			exit;
		}
		public function sortbatchorders( $picklist_inc_id = null)
		{
			$this->autoRender = false;
			$this->layout = '';
			$this->loadModel('MergeUpdate');		 			
			$this->loadModel('ServiceCounter' );
			$sort_count = 0;
			$all_msg = []; 	
			$ord_msg = []; 	
 			$cancel_orders = []; 
			$open_orders = [];
			$results = $this->MergeUpdate->find('all', 
						array('conditions' => array('MergeUpdate.picklist_inc_id'=>$picklist_inc_id, 
													'MergeUpdate.sorted_scanned' => '0' , 
													'MergeUpdate.scan_status' => '0')));
			 
			 
			if(count($results) > 0 )
			{
				foreach($results as $result)
				{
					if($result['MergeUpdate']['status'] == 1){
							
						$merge_inc_id 		= $result['MergeUpdate']['id'];
						$serviceName 		= $result['MergeUpdate']['service_name'];
						$serviceProvider 	= $result['MergeUpdate']['service_provider'];
						$serviceCode 		= $result['MergeUpdate']['provider_ref_code'];
						$country 			= $result['MergeUpdate']['delevery_country'];
						$serviceid 			= $result['MergeUpdate']['service_id'];
						
						if($serviceProvider == 'DHL'){
							$getCounterValue = $this->ServiceCounter->find( 'first' , array( 'conditions' => array('ServiceCounter.service_id' => $serviceid ) ) );
						}else{
							$getCounterValue = $this->ServiceCounter->find( 'first' , 
							array( 'conditions' => array( 'ServiceCounter.destination' => $country,'ServiceCounter.service_id' => $serviceid )) );
						}
						/*echo $result['MergeUpdate']['order_id'];
						echo "<br>";
						echo $country;
						echo $serviceid ;
						echo "<br>";pr($getCounterValue );
						if($result['MergeUpdate']['order_id'] == '2120659'){
							pr($getCounterValue );
							exit;
						}*/
						
						$counter = 0;
						if( $getCounterValue['ServiceCounter']['counter'] > 0 ){
							$counter = $getCounterValue['ServiceCounter']['counter'];
							$counter = $counter + 1;
						} else {
							$counter = $counter + 1;
						}
						
						$orderCommaSeperated = '';
						if( $getCounterValue['ServiceCounter']['order_ids'] != '' ){
							$orderCommaSeperated = $getCounterValue['ServiceCounter']['order_ids'];
							$orderCommaSeperated = $orderCommaSeperated.','.$merge_inc_id;
						} else {
							$orderCommaSeperated = $merge_inc_id;
						}
						
						if(count($getCounterValue) > 0){
							$ServiceCounter['id'] 		= $getCounterValue['ServiceCounter']['id'];     
						} 
						$ServiceCounter['counter'] = $counter;
						$ServiceCounter['order_ids'] = $orderCommaSeperated; 					     
						$this->ServiceCounter->saveAll( $ServiceCounter );
	
						$MergeUpdate['id'] = $merge_inc_id;            
						$MergeUpdate['sorted_scanned'] = 1;         
						$MergeUpdate['scan_status']  = 1;  
						$MergeUpdate['scan_date']    = date('Y-m-d H:i:s');  
						$MergeUpdate['scanned_user'] = $_SESSION['Auth']['User']['first_name'].' '.$_SESSION['Auth']['User']['last_name'];  
						$MergeUpdate['scanned_pc']   = $_SESSION['Auth']['User']['pc_name'];  					 
						$this->MergeUpdate->saveAll( $MergeUpdate );
					   
						$log_file = WWW_ROOT .'logs/dynamic_log/sort_batch_all_orders_'.date('dmy').".log";
					  
						if(!file_exists($log_file)){
							file_put_contents($log_file, 'Split_Order_Id'."\t".'Service id'."\t".'Service name'."\t".'Service Provider'."\t".'Service code'."\t".'picklist_inc_id'."t".'User'."\t".'PcName'."\t".'DateTime'."\n");	
						}		
									
						file_put_contents($log_file, $result['MergeUpdate']['product_order_id_identify']."\t".$getCounterValue['ServiceCounter']['id']."\t".$result['MergeUpdate']['service_name']."\t".$result['MergeUpdate']['service_provider']."\t".$result['MergeUpdate']['provider_ref_code']."\t".$picklist_inc_id."\t".$_SESSION['Auth']['User']['first_name'].' '.$_SESSION['Auth']['User']['last_name']."\t".$_SESSION['Auth']['User']['pc_name']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND | LOCK_EX);
						
						$ord_msg[] = $result['MergeUpdate']['product_order_id_identify']. " is sorted.";
						$sort_count++;
					
					}else if($result['MergeUpdate']['status'] == 0){					
						$open_orders[] = $result['MergeUpdate']['product_order_id_identify'] ;
					}else if($result['MergeUpdate']['status'] == 2){					
						$cancel_orders[] = $result['MergeUpdate']['product_order_id_identify'] ;
					}
				
				}
				 
			}else{
				$ord_msg[] = "No order found for sorting.";
			}
			
			$all_msg['ord_msg'] = $ord_msg;
			$all_msg['open_orders'] = $open_orders;
			$all_msg['cancel_orders'] = $cancel_orders;
			$all_msg['sort_count'] = $sort_count; 
			$all_msg['open_count'] = count($open_orders); 
			$all_msg['cancel_count'] = count($cancel_orders); 
			return $all_msg;
		} 
		public function sortbatchorders_10June( $picklist_inc_id = null)
		{
			$this->autoRender = false;
			$this->layout = '';
			$this->loadModel('MergeUpdate');		 			
			$this->loadModel('ServiceCounter' );
			$ord_msg = []; 	
 		
			$results = $this->MergeUpdate->find('all', 
						array('conditions' => array('MergeUpdate.picklist_inc_id'=>$picklist_inc_id, 'MergeUpdate.status' => '1' , 
													'MergeUpdate.sorted_scanned' => '0' , 
													'MergeUpdate.scan_status' => '0')));
			 
			if(count($results) > 0 )
			{
				foreach($results as $result)
				{
					$merge_inc_id 		= $result['MergeUpdate']['id'];
					$serviceName 		= $result['MergeUpdate']['service_name'];
					$serviceProvider 	= $result['MergeUpdate']['service_provider'];
					$serviceCode 		= $result['MergeUpdate']['provider_ref_code'];
					$country 			= $result['MergeUpdate']['delevery_country'];
					$serviceid 			= $result['MergeUpdate']['service_id'];
					
					if($serviceProvider == 'DHL'){
						$getCounterValue = $this->ServiceCounter->find( 'first' , array( 'conditions' => array('ServiceCounter.service_id' => $serviceid ) ) );
					}else{
						$getCounterValue = $this->ServiceCounter->find( 'first' , 
						array( 'conditions' => array( 'ServiceCounter.destination' => $country,'ServiceCounter.service_id' => $serviceid )) );
					}
					
					$counter = 0;
					if( $getCounterValue['ServiceCounter']['counter'] > 0 ){
						$counter = $getCounterValue['ServiceCounter']['counter'];
						$counter = $counter + 1;
					} else {
						$counter = $counter + 1;
					}
					
					$orderCommaSeperated = '';
					if( $getCounterValue['ServiceCounter']['order_ids'] != '' ){
						$orderCommaSeperated = $getCounterValue['ServiceCounter']['order_ids'];
						$orderCommaSeperated = $orderCommaSeperated.','.$merge_inc_id;
					} else {
						$orderCommaSeperated = $merge_inc_id;
					}
					
					$ServiceCounter['id'] 		= $getCounterValue['ServiceCounter']['id'];      
					$ServiceCounter['counter'] = $counter;
					$ServiceCounter['order_ids'] = $orderCommaSeperated; 					     
					$this->ServiceCounter->saveAll( $ServiceCounter );

					$MergeUpdate['id'] = $merge_inc_id;            
					$MergeUpdate['sorted_scanned'] = 1;         
					$MergeUpdate['scan_status'] = 1;  
					$MergeUpdate['scan_date'] = date('Y-m-d H:i:s');  
 					$MergeUpdate['scanned_user'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');  
					$MergeUpdate['scanned_pc'] = $this->Session->read('Auth.User.pc_name');  					 
					$this->MergeUpdate->saveAll( $MergeUpdate );
				   
				  	$log_file = WWW_ROOT .'logs/dynamic_log/sort_batch_all_orders_'.date('dmy').".log";
				  
				    if(!file_exists($log_file)){
  						file_put_contents($log_file, 'Split_Order_Id'."\t".'Service id'."\t".'Service name'."\t".'Service Provider'."\t".'Service code'."\t".'picklist_inc_id'."t".'User'."\t".'PcName'."\t".'DateTime'."\n");	
					}		
								
					file_put_contents($log_file, $result['MergeUpdate']['product_order_id_identify']."\t".$getCounterValue['ServiceCounter']['id']."\t".$result['MergeUpdate']['service_name']."\t".$result['MergeUpdate']['service_provider']."\t".$result['MergeUpdate']['provider_ref_code']."\t".$picklist_inc_id."\t".$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name')."\t".$this->Session->read('Auth.User.pc_name')."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND | LOCK_EX);
					
					$ord_msg[] = $result['MergeUpdate']['product_order_id_identify']. " is sored";
					
				}
				 
			}else{
				$ord_msg[] = "No order found for sorting.";
			}
			
			return $ord_msg;
		} 
		
		public function MarkOverSell()
		{
			$this->layout = '';
			$this->autoRander = false;			 
			$this->loadModel('MergeUpdate'); 
		 
			$firstName 	= 	( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName 	= 	( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_'; 
 			$username   =   $firstName.' '.$lastName;  
								
			if(isset($this->request->data['split_order_id']) && $this->request->data['split_order_id'] != ''){
 				$this->MergeUpdate->updateAll(
								array('MergeUpdate.over_sell_user' => "'".$username.'|'.date('Y-m-d H:i:s')."'"), array( 'MergeUpdate.product_order_id_identify' => $this->request->data['split_order_id']));
 			} 
 			
 			$log_file = WWW_ROOT .'logs/dynamic_log/over_sell_user_'.date('dmy').".log";
		
			if(!file_exists($log_file)){
				file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'split_order_id'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
			}
			file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$this->request->data['split_order_id']."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
		
		    $msg['msg'] = "Order is marked as oversell.";
			echo json_encode($msg);
			exit;			 
		}
		public function batch() 
		{
		
 			$this->layout = 'index';
			
			$this->loadModel( 'User' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'DynamicBatche' );
			
			
			$perPage	=	 30;
			$this->paginate = array('order'=>'id DESC','limit' => $perPage);
			$dynamic_batches = $this->paginate('DynamicBatche');
			
			
			$open_orders 	= $this->OpenOrder->find('count', array( 'conditions' => array( 'status' => 0 )) );
			$open_orders_merge 	= $this->MergeUpdate->find('count', array( 'conditions' => array( 'status' => 0 ) ) );
			$batch_orders 	= $this->MergeUpdate->find('count', array( 'conditions' => array( 'status' => 0, 'created_batch !=' => null ) ) );
			$readyforbatch 	= $this->MergeUpdate->find('count', array( 'conditions' => array( 'status' => 0, 'pick_list_status' => 0, 'created_batch' => null ) ) );
			
			//echo $count_nonbatch_orders;
			$o_data['op_ord_count'] 		= $open_orders;
			$o_data['bat_ord_count'] 		= $batch_orders;
			$o_data['open_orders_merge'] 	= $open_orders_merge;
			$o_data['remain_ord_count'] 	= $open_orders_merge	-	$batch_orders;
			$o_data['readyforbatch'] 		= $readyforbatch;
			
			
			
			/******************Dynamic Batch****************/
			$this->set( compact('o_data'));
			
			$this->set( 'dynamic_batches',$dynamic_batches);
			
			
		}
		 
		
		public function batchTest() 
		{
		
 			$this->layout = 'index';
			
			$this->loadModel( 'User' );
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'DynamicBatche' );
			
			
			$perPage	=	 50;
			$this->paginate = array('order'=>'id DESC','limit' => $perPage);
			$dynamic_batches = $this->paginate('DynamicBatche');
			
			
			$open_orders 	= $this->OpenOrder->find('count', array( 'conditions' => array( 'status' => 0 )) );
			$open_orders_merge 	= $this->MergeUpdate->find('count', array( 'conditions' => array( 'status' => 0 ) ) );
			$batch_orders 	= $this->MergeUpdate->find('count', array( 'conditions' => array( 'status' => 0, 'created_batch !=' => null ) ) );
			$readyforbatch 	= $this->MergeUpdate->find('count', array( 'conditions' => array( 'status' => 0, 'pick_list_status' => 0, 'created_batch' => null ) ) );
			
			//echo $count_nonbatch_orders;
			$o_data['op_ord_count'] 		= $open_orders;
			$o_data['bat_ord_count'] 		= $batch_orders;
			$o_data['open_orders_merge'] 	= $open_orders_merge;
			$o_data['remain_ord_count'] 	= $open_orders_merge	-	$batch_orders;
			$o_data['readyforbatch'] 		= $readyforbatch;
			
			
			
			/******************Dynamic Batch****************/
			$this->set( compact('o_data'));
			
			$this->set( 'dynamic_batches',$dynamic_batches);
			
			
		}
		
		public function batchOrders( $batch = 0) 
		{
			$this->loadModel( 'MergeUpdate' );
			 
			$orderItems = $this->MergeUpdate->find('all', array('fields'=>array('sku','batch_num','scan_status', 'sorted_scanned', 'manifest_status', 'status', 'batch_num', 'dispatch_console', 'created_batch', 'label_status', 'pick_list_status', 'order_id', 'product_order_id_identify', 'service_name', 'provider_ref_code', 'service_provider', 'delevery_country', 'postal_service', 'country_code', 'user_id', 'assign_user', 'picked_date', 'process_date', 'scan_date', 'manifest_date','brt','picklist_inc_id' ),'conditions' => array('MergeUpdate.created_batch' => $batch),'order'=>'picklist_inc_id'
			));
			
			return $orderItems;
 			
		}
		
		
		public function batchPicklist( $batch = null, $country = null, $service_name = null) 
		{
			$this->loadModel( 'DynamicPicklist' );
			
			$pickItems = $this->DynamicPicklist->find('all', array('fields'=>array('picklist_name'),'conditions' => array('batch' => $batch,'country' => $country,'service_name' => $service_name,'created_from' => 'batch_page')
			));
			 
			return $pickItems;
			
 		}
		
		public function expressPicklist( $batch = null) 
		{
			$this->loadModel( 'DynamicPicklist' );
			
			$pickItems = $this->DynamicPicklist->find('first', array('fields'=>array('picklist_name'),'conditions' => array('batch' => $batch,'created_from' => 'batch_page')
			));
			 
			return $pickItems;
			
 		}
		 
		public function createBatch()
		{
			$this->loadModel( 'MergeUpdate' ); 
			$this->loadModel( 'DynamicBatche' );
			
			$batch_num	= $this->request->data['o_f_batch'];
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
			
			$username = $firstName.' '.$lastName;
 			
			$log_file = WWW_ROOT .'logs/dynamic_log/create_batch_'.date('dmy').".log";
		
			if(!file_exists($log_file)){
				file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'batch_num'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
			}
			file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$batch_num."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
			
			
			$get_o	=	$this->MergeUpdate->find('all', array( 'conditions' => array('status' => 0, 'pick_list_status' => 0, 'created_batch' => null), 'limit' => $batch_num, 'order' => 'purchase_date ASC' ) );
			
			$batch_num = date('YmdHis');
			
			foreach($get_o as $o)
			{
				$s_o_ids[]	=	$o['MergeUpdate']['product_order_id_identify'];
			}
			
 			
			if( count( $s_o_ids ) > 0 )
			{
				$result	=	$this->MergeUpdate->updateAll(
								array('MergeUpdate.created_batch' => "'".$batch_num."'"), 
								array('MergeUpdate.product_order_id_identify IN' => $s_o_ids));
				$data['batch_name'] 	= $batch_num;
				$data['pc_name'] 		= $pcName;
				$data['order_count'] 	= count( $s_o_ids );
				$data['creater_name'] 	= $username;
				$data['created_date'] 	= date('Y-m-d H:i:s');
				$this->DynamicBatche->saveAll($data);
				
				App::import('Controller' , 'Cronjobs');		
				$mpObj = new CronjobsController();	
  				$mpObj->order_location_update( $batch_num );	
				$mpObj->order_location_up();

				echo "1";
				exit;
			} 
			else
			{
				echo "2";
				exit;
			}
		}
		
		public function markcompletebatch()
		{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'DynamicBatche' );
			
			$batch_num	= $this->request->data['batnum'];
			
 			$orders	= $this->MergeUpdate->find('all', array( 'conditions' => array('created_batch' => $batch_num),'fields' => ['status','scan_status'] ) );
			$processed = $scan_status = 0; $other = 0;
			foreach($orders as $order){
			
				if($order['MergeUpdate']['status'] == 1){
					$processed++;
				}else if($order['MergeUpdate']['status'] > 1){
					$other++;
				}if($order['MergeUpdate']['scan_status'] == 1){
					$scan_status++;
				}
			}
			$torders = count($orders) - $other;
			$msg['torders'] = $torders;
			$msg['other'] = $other;
			
			if(($torders == $processed) && ($torders = $scan_status)){  
 			
 				$this->DynamicBatche->updateAll( array('status' => 2,'completed_date' => "'".date('Y-m-d H:i:s')."'"),array('batch_name' => $batch_num));	
				
				$log_file = WWW_ROOT .'logs/dynamic_log/markcomplete_batch_'.date('dmy').".log";
			
				if(!file_exists($log_file)){
					file_put_contents($log_file, 'Email'."\t".'Username'."\t".'PcName'."\t".'batch_num'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
				}
				file_put_contents($log_file, $this->Session->read('Auth.User.email')."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$batch_num."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
				
				$msg['status'] =  'ok';
			
			}else{
				$msg['status'] =  'You can not mark completed. Total Orders:'.count($orders) .' Processed orders :'. $processed .' Scan orders '. $scan_status;
			}
			echo json_encode($msg);
			exit;
		}
		
		public function getOrderStatusQty( $b_num = null )
		{
				$this->loadModel( 'MergeUpdate' );
				$b_to_ords	=	$this->MergeUpdate->find( 'all', array( 'conditions' => array( 'MergeUpdate.created_batch' => $b_num ) ) );
				$o_count = 0;$p_count = 0;$c_count = 0;$l_count = 0;
				$statusarr = array();
				$o_r_count = 0;
				foreach($b_to_ords as $b_to_ord)
				{	
					if( $b_to_ord['MergeUpdate']['status'] == 0 && $b_to_ord['MergeUpdate']['pick_list_status'] == 0 ){
						$o_r_count = $o_r_count + 1;
					}	
				
					if( $b_to_ord['MergeUpdate']['status'] == 0 ){
						$o_count = $o_count + 1;
					} else if( $b_to_ord['MergeUpdate']['status'] == 1 ){
						$b_to_ord['MergeUpdate']['status']."<br>";
						$p_count = $p_count + 1;
					} else if( $b_to_ord['MergeUpdate']['status'] == 2 ){
						$c_count = $c_count + 1;
						$b_to_ord['MergeUpdate']['status']."<br>";
					} else {
						$l_count = $l_count + 1;
					}
				}
					$statusarr['open_order'] 		= $o_count;
					$statusarr['proc_order'] 		= $p_count;
					$statusarr['canc_order'] 		= $c_count;
					$statusarr['lock_order'] 		= $l_count;
					$statusarr['remaning_order'] 	= $o_r_count;	
					return $statusarr;
		}
		
		public function getbatchQty( $b_num = null )
		{
				$this->loadModel( 'MergeUpdate' );
				$b_ords		=	$this->MergeUpdate->find( 'count', array( 'conditions' => array( 'MergeUpdate.created_batch' => $b_num ) ) );
				$p_b_ords	=	$this->MergeUpdate->find( 'count', array( 'conditions' => array( 'MergeUpdate.status' => 1, 'MergeUpdate.created_batch' => $b_num ) ) );
				$b_data['b_orders'] =	$b_ords;
				$b_data['p_b_orders'] =	$p_b_ords;
				return $b_data;
		}
		
		public function getPrinterId()
		{
			$this->layout = '';
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );						
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name'];  
			$paramsZDesigner = array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name LIKE' => '%Brother HL-6180DW series%'
				)
			);
			//ZDesigner
			$pcDetailZDesigner = json_decode(json_encode($this->PcStore->find('all', $paramsZDesigner)),0);		
			
			$zDesignerPrinter = $pcDetailZDesigner[0]->PcStore->printer_id;
		
			return $zDesignerPrinter;	
		}
		
			public function getOverWeight($batch_num){
			
			$this->loadModel( 'MergeUpdate' );
 			$items = $this->MergeUpdate->query("SELECT product_order_id_identify, sku,delevery_country,service_name FROM merge_updates as MergeUpdate WHERE (`service_name` = 'Over Weight' OR `service_name` = '' ) AND `created_batch` = '".$batch_num."' AND `status` IN(0,2,3,4)");
 			
			if(count($items) > 0 ){
				$fname = 'batch_over_weight_'. $batch_num.'_'.date('dmy').'.txt';
 				$_path = WWW_ROOT .'logs/'.$fname;    				  
				file_put_contents($_path, "order_id\tsku\tdelevery_country\tservice_name"."\r\n");
 				foreach($items as $v){
 					 file_put_contents($_path, $v['MergeUpdate']['product_order_id_identify']."\t".$v['MergeUpdate']['sku']."\t".$v['MergeUpdate']['delevery_country']."\t".$v['MergeUpdate']['service_name']."\r\n", FILE_APPEND|LOCK_EX);	
				
				} 
  				header('Content-Encoding: UTF-8');
				header('Content-type: text/plain; charset=UTF-8');
				header('Content-Disposition: attachment;filename="'.$fname.'"'); 
				header('Cache-Control: max-age=0');
				readfile($_path);				 
				exit;					 
			}else{
				$this->Session->setflash( 'No orders founds.', 'flash_danger'); 
			}
			 $this->redirect($this->referer());	
  		}
  		
		public function getOverSell($batch_num){
			
			$this->loadModel( 'OrderLocation' );
			$this->loadModel( 'MergeUpdate' );
 			$_mitems = $this->MergeUpdate->query("SELECT product_order_id_identify,over_sell_user,sku FROM merge_updates as MergeUpdate WHERE `created_batch` = '".$batch_num."' AND `status` IN(0,2,3)");
			
			$fname = 'batch_over_sell_'. date('dmy_Hi').'.txt';
			$_path = WWW_ROOT .'logs/'.$fname;    				  
			file_put_contents($_path, "order_id\tsku\tbin_location or user"."\n");
					
			$split_order_id = [];
			if(count($_mitems) > 0 ){
				foreach($_mitems as $r){
					
					if($r['MergeUpdate']['over_sell_user']){					
						 file_put_contents($_path, $r['MergeUpdate']['product_order_id_identify']."\t".$r['MergeUpdate']['sku']."\t".$r['MergeUpdate']['over_sell_user']."\n", FILE_APPEND|LOCK_EX);	
					}else{
						$split_order_id[] = $r['MergeUpdate']['product_order_id_identify'];
					}
				}
			}
			
			if(count($split_order_id) > 0){
			
				$items = $this->OrderLocation->query("SELECT split_order_id, sku, bin_location FROM order_locations as OrderLocation WHERE  `available_qty_bin` = 0 AND `status`='active' AND split_order_id IN('". implode("','",$split_order_id)."')");
				
				if(count($items) > 0 ){
					$fname = 'batch_over_sell_'. $batch_num.'_'.date('dmy').'.txt';
					$_path = WWW_ROOT .'logs/'.$fname;    				  
					file_put_contents($_path, "order_id\tsku\tbin_location"."\n");
					foreach($items as $v){
						 file_put_contents($_path, $v['OrderLocation']['split_order_id']."\t".$v['OrderLocation']['sku']."\t".$v['OrderLocation']['bin_location']."\n", FILE_APPEND|LOCK_EX);	
					
					} 
					header('Content-Encoding: UTF-8');
					header('Content-type: text/plain; charset=UTF-8');
					header('Content-Disposition: attachment;filename="'.$fname.'"'); 
					header('Cache-Control: max-age=0');
					readfile($_path);				 
					exit;	
				}else{
					$this->Session->setflash( 'No orders founds.', 'flash_danger'); 
				}				 
			}else{
				$this->Session->setflash( 'No orders founds.', 'flash_danger'); 
			}
			 $this->redirect($this->referer());	
  		}
		
		
		public function pro_location(){
			 
			$this->loadModel( 'Product' );
			$products = array();
			$_product	= $this->Product->find('all',array('fields'=>array('product_sku','ProductDesc.barcode'),'order'=>'Product.id DESC'));
			
			foreach( $_product as $pro ){
				$products[$pro['Product']['product_sku']] = $pro['ProductDesc']['barcode'];
			}
			pr($products);exit;
		}
		
		public function getCountryCode($country = null){
			$p = '';
			$this->loadModel( 'Country' );
 			$d = $this->Country->find('first',array('fields'=>array('iso_2'),'conditions'=>array('custom_name' => $country)));
			if(count($d) > 0){
				$p = $d['Country']['iso_2'];
			}
			return $p ;
 		}
		
		public function assignBatch()
		{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'DynamicBatche' );
			
			$created_batch	= $this->request->data['created_batch'];
			$split_order_id	= $this->request->data['split_order_id'];
			$msg = [];
			
 			$batch	= $this->DynamicBatche->find('first', array( 'conditions' => array('batch_name' => $created_batch),'fields' => ['id','order_count','status'] ) );
			
			if(count($batch) > 0 ){
				if($batch['DynamicBatche']['status'] == 1){
			
					$order 	= $this->MergeUpdate->find('first', array( 'conditions' => array('product_order_id_identify' => $split_order_id),'fields' => ['status','id'] ) );
					 
					 if(count($order) > 0 ){
						 
							$order_count = $batch['DynamicBatche']['order_count'] + 1;
							$this->DynamicBatche->updateAll( array('order_count' => $order_count),array('id' => $batch['DynamicBatche']['id']));
							$this->MergeUpdate->updateAll( array('created_batch' => $created_batch),array('id' => $order['MergeUpdate']['id']));
						 
							$log_file = WWW_ROOT .'logs/dynamic_log/assignBatch_'.date('dmy').".log";
						
							if(!file_exists($log_file)){
								file_put_contents($log_file, 'Split_order_id'."\t".'Username'."\t".'PcName'."\t".'batch_num'."\t".'DateTime'."\n", FILE_APPEND|LOCK_EX);	
							}
							file_put_contents($log_file, $split_order_id."\t".$this->Session->read('Auth.User.username')."\t".$this->Session->read('Auth.User.pc_name')."\t".$created_batch."\t".date('Y-m-d H:i:s')."\n", FILE_APPEND|LOCK_EX);
							$msg['msg'] =  'Batch assigned successfully.';
							$msg['status'] = 1;
						
					}else{
						$msg['msg'] =  'Order not found.';
						$msg['status'] = 2;
					}
				}else{
					$msg['msg'] =  'This batch is already completed. Please assign other batch.';
					$msg['status'] = 2;
				}
			}else{
					$msg['msg'] =  'Batch number not found.';
					$msg['status'] = 2;
			}
			
			 
			echo json_encode($msg);
			exit;
		}
		
		
		public function labelTime(){
			 
			$this->loadModel( 'Product' );
			$orders = ['1813431-1','1812722-1','1813400-1','1812744-1','1814363-1','1812115-1','1812131-1','1808660-1','1813170-1','1813131-1','1813095-1','1813679-1','1812952-1','1813761-1','1813976-1','1814086-1','1812698-1','1813877-1','1812194-1','1814389-1','1814304-1','1812226-1','1812581-1','1813831-1','1814284-1','1812585-1','1813317-1','1813492-1','1813830-1','1812584-1','1812278-1','1812196-1','1814198-1','1812224-1','1812382-1','1812380-1','1812588-1','1812381-1','1812580-1','1812478-1','1812531-1','1812811-1','1812132-1','1812243-1','1812135-1','1812116-1','1812195-1','1814418-1','1814548-1','1812842-1','1814513-1','1812892-1','1814524-1','1812479-1','1813787-1','1813154-1','1814085-1','1814015-1','1813677-1','1813249-1','1812163-1','1813176-1','1814263-1','1814022-1','1813875-1','1813786-1','1812891-1','1814348-1','1813669-1','1814414-1','1813817-1','1813729-1','1814415-1','1812443-1','1813569-1','1813667-1','1812745-1','1814463-1','1812816-1','1814419-1','1814119-1','1812816-2','1812225-1','1812512-1','1813171-1','1813455-1','1813153-1','1813312-1','1812083-1','1812889-1','1813490-1','1812792-1','1812051-1','1812481-1','1814461-1','1814484-1','1813097-1','1812817-1','1813598-1','1812188-1','1814404-1','1811463-1','1812193-2','1814344-1','1813674-1','1812193-1'];
			
			foreach($orders as $oid){
				  $filename = WWW_ROOT ."img/printPDF/Bulk/Label_Slip_{$oid}.pdf";
				 $this->MergeUpdate->updateAll(array('MergeUpdate.picklist_username' => "'".$picklist_username."'", 'MergeUpdate.pick_list_status' => '1','MergeUpdate.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('MergeUpdate.order_id = ' => $orderids[0]));
					 	$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '1','OrderLocation.picked_date' => "'" . date('Y-m-d H:i:s') . "'"), array('OrderLocation.order_id = ' => $orderids[0]));
				//echo "<br>";
			}
		}
		
		
		public function picklistupdate(){
			 
			$this->loadModel( 'MergeUpdate' );
			$orders = [ '2318986','2319851','2319950','2319936','2319215','2320166','2319924','2319921','2320555','2320438','2319591','2320197','2319326','2318939','2321662','2318840','2320331','2319060','2321353','2321550','2321770','2321409','2318895','2321528','2319495','2321921','2319975','2319485','2321780','2320348','2319499','2321928','2321937','2319501','2320824','2319512','2319245','2320984','2319283','2321687','2319005','2318997','2321222','2321957','2320114','2321238','2319526','2320420','2321569','2320401','2320533','2319020','2320544','2320549','2319549','2318813','2319408','2320563','2319298','2320138','2321991','2320567','2321003','2319162','2322003','2318914','2320423','2320432','2320576','2320579','2321161','2320442','2318858','2322013','2319443','2321623','2322019','2322027','2319379','2318837','2320278','2320428','2321510','2319198','2319963','2319219','2320982','2321109','2321233','2322047','2318898','2320763','2321001','2319896','2319666','2320547','2320429','2319297','2319561','2321455','2320603','2321537','2319842','2319842','2319956','2320075','2321506','2319334','2321639','2321899','2320829','2321778','2320213','2320203','2320816','2321403','2319328','2320325','2321406','2319716','2319333','2319719','2320217','2320211','2319607','2320962','2320843','2320858','2320855','2320859','2320222','2321940','2320748','2321087','2321247','2320110','2321094','2319990','2321106','2321338','2321096','2319992','2321105','2321324','2321120','2321560','2321577','2321675','2321701','2319631','2320012','2319780','2321010','2319760','2319801','2319901','2320896','2319778','2320669','2321140','2319553','2320900','2319800','2319417','2321877','2319288','2319166','2321040','2319677','2320076','2320804','2320686','2321492','2320920','2321188','2321391','2319955','2319318','2319319','2319221','2321180','2319858','2319966','2320334','2320206','2319136','2320133','2319051','2320554','2319309','2319913','2320421','2320581','2320722','2320454','2320079','2319073','2319039','2321518','2319350','2321076','2321076','2321552','2319733','2319733','2319745','2320999','2321720','2319674','2320681','2319953','2319953','2321694','2321405','2321246','2321661','2320204','2321568','2321612','2321113','2321251','2319694','2319717','2320086','2319976','2321442','2320967','2319618','2321683','2320743','2320269','2321846','2321072','2319684','2319832','2321466','2320441','2319432','2320298','2320182','2320440','2321768','2320521','2319385','2321357','2320452','2319761','2320723','2320760','2320196','2320679','2321065','2321902','2320360','2321907','2321567','2320657','2320552','2321735','2320463','2321100','2321384','2319951','2321892','2319454','2319330','2319964','2320068','2321396','2320833','2320695','2318946','2319069','2319874','2320712','2318969','2320659','2319881','2320727','2320974','2318775','2319107','2319762','2319269','2319393','2320741','2321976','2320003','2319407','2321016','2321005','2320781','2320159','2318912','2321034','2319572','2320430','2320174','2319441','2319052','2320041','2320077','2319578','2320916','2320798','2320943','2320888','2319412','2319721','2321716','2321719','2320445','2319588','2319595','2320825','2320968','2320959','2319829','2321546','2319873','2321468','2320141','2320424','2319291','2320462','2320379','2319035','2319200','2320302','2319325','2319615','2318918','2320422','2319203','2319523','2321027','2318798','2320124','2319818','2321499','2319204','2319455','2321284','2321745','2321074','2321292','2320481','2319827','2321908','2321064','2320215','2321551','2320354','2320851','2321690','2321690','2321231','2321547','2321425','2321423','2318991','2319248','2321954','2320517','2321820','2321684','2321830','2320986','2321581','2321597','2319027','2321475','2321621','2321629','2319322','2320297','2321881','2320599','2318144','2320778','2321713','2319152','2321763','2320416','2319072','2321985','2320673','2320450','2320040','2320983','2320541','2320017','2319425','2319958','2321029','2321004','2320516','2319378','2319068','2319500','2319903','2321185','2322018','2320469','2321177','2321066','2321411','2320214','2321410','2321295','2320835','2320969','2320854','2320716','2320507','2321951','2320382','2320767','2320765','2321124','2320032','2321134','2321835','2319274','2320132','2321358','2320656','2321733','2320158','2321865','2321483','2321484','2321262','2321894','2320799','2321891','2321620','2318926','2319724','2321584','2320645','2319664','2320675','2321630','2319626','2320608','2320724','2318790','2320170','2320324','2320198','2319897','2319585','2319308','2320719','2320620','2321507','2321638','2318976','2319092','2321563','2321436','2321709','2319006','2318864','2319253','2321832','2321361','2319150','2321608','2321869','2321419','2320098','2321327','2320940','2320323','2321337','2320730','2320136','2321463','2319142','2320169','2319774','2319453','2319076','2320735','2319045','2319753','2321717','2319711','2318897','2319128','2319542','2320553','2319028','2320287','2321895','2320231','2319254','2321420','2320789','2320400','2320353','2322030','2320470','2319828','2322015','2320465','2320708','2319147','2321905','2319899','2320317','2319699','2320078','2321325','2319718','2321562','2320856','2321219','2320759','2320065','2321759','2319647','2321294','2319695','2319866','2320277','2320574','2321897','2318862','2321156','2319969','2319262','2321949','2321969','2321133','2321721','2319625','2320884','2319820','2321283','2321635','2320622','2321924','2320754','2320642','2320004','2320644','2320909','2321316','2320694','2319558','2321013','2321352','2319654','2319693','2320238','2318855','2320720','2320850','2321349','2321241','2321416','2319744','2319031','2320300','2319185','2321227','2320023','2319570','2319798','2319610','2318970','2319367','2320726','2321478','2321254','2320275','2319671','2319804','2319667','2319548','2321995','2319589','2321107','2321526','2318956','2321870','2322038','2320862','2319244','2319938','2322038','2319729','2320995','2321092','2321553','2321473','2321845','2321605','2320283','2318906','2320779','2319785','2320418','2319843','2320056','2320199','2320200','2319872','2320239','2320257','2319776','2320046','2320777','2321160','2320666','2320052','2321266','2321054','2321061','2318985','2319725','2318960','2320486','2321572','2319121','2319084','2318844','2320979','2320099','2319871','2319023','2319023','2320683','2321785','2321398','2319343','2320846','2321426','2320367','2321235','2319249','2320255','2319885','2320892','2320019','2320021','2321146','2319566','2319984','2321669','2321791','2318950','2318794','2320055','2319949','2319444','2321170','2318925','2320189','2320398','2319838','2321444','2319994','2321360','2319665','2320911','2320782','2320053','2319292','2319932','2319959','2320201','2319736','2320577','2320320','2319840','2320605','2319840','2320605','2321522','2321504','2321779','2318941','2319720','2320626','2320108','2320097','2321438','2321932','2319735','2319228','2321207','2320632','2319740','2320117','2321351','2321351','2320873','2320873','2319748','2320395','2320147','2319024','2319911','2319911','2319270','2320787','2319660','2319802','2319802','2320901','2321619','2318923','2319945','2318828','2319941','2321049','2319942','2320809','2321184','2319315','2321682','2321718','2319159','2321956','2320377','2318998','2321964','2319032','2321751','2319183','2319236','2321018','2319948','2319055','2319074','2321230','2321099','2321631','2319865','2319864','2319105','2318931','2320092','2318938','2321929','2320369','2319528','2321836','2319544','2320560','2322020','2321916','2321062','2319184','2319546','2321753','2321488','2321418','2321962','2320212','2321313','2319845','2320235','2321819','2321017','2320797','2321193','2321935','2321583','2321993','2321198','2318977','2319359','2319035','2320803','2320094','2320728','2318872','227463209222019','2320443','2319300','2320455','2321524','2320993','2320993','2320993','2319947','2320700','2320060','2319082','2320412','2321164','2320960','2320697','2320785','2320045','2319679','2321055','2319085','2320817','2319459','2320497','2320641','2320641','2321858','2319025','2319160','2321368','2321379','2321491','2320812','2321171','2318942','2321916','2321062','2321926','2319410','2321108','2319770','2320415','2320678','2321138','2321834','2319638','2321811','2321131','2320266','2319181','2320466','2320184','2319442','2322023','2318834','2321668','2320125','2321445','2319556','228049009222019','2319480','2322002','2319704','2320857','2321232','2320370','2319630','2321833','2319644','2320180','2319510','2320784','2321434','2320627','2321479','2319833','2321195','2320089','2319974','2320522','2320534','2320530','2320543','2319399','2319925','2321593','2322010','2320805','2321052','2322001','2321743','2320830','2319993','2318924','2321407','2319063','2320838','2320118','2320010','2319688','2320194','2319320','2319064','2318954','2318786','2320919','2319374','2320874','2319879','2320815','2319386','2320326','2318863','2318972','228933609222019','2320734','2321470','2321226','2320671','2321649','2319403','2319302','2318817','2321200','2320822','2320713','2320359','2319614','2320279','2320677','2319540','2319158','2320800','2318853','2319460','2320688','2319347','2319995','2320559','2320472','2321933','2321408','2319855','2319125','2320883','2320994','2318848','2318847','2319173','2321168','2321653','2319007','2318824','2319034','2319678','2320491','227663209222019','2319577','228248409212019','2321058','2319726','2319752','2320225','2320273','2319706','2321312','2319744','2321341','2320584','2321788','2320227','2321117','2320008','2319628','227781109202019','2321725','2320301','2319180','2319633','2320468','2322033','2319363','2319621','2319766','2319757','2319268','2319809','2322022','2321634','2321666','2320532','2321856','2320802','2319429','2318829','2320593','2320366','2319013','2318885','2319490','2321654','2319580','2320128','2320546','2319423','2320845','2320328','2319206','2319070','2319216','2320221','2319505','2319632','2320512','2320167','2319541','2319539','2321144','2319401','2318809','2319795','2319569','2321874','2321875','2319451','2318768','2321950','2318807','2318806','2320663','2319287','2318660','2319439','2318952','2321314','2320256','2321837','2319438','2319489','226543609222019','2321667','2320489','2321656','2320621','2321459','2319151','2319742','2319225','2320253','2320014','2320399','2321021','2321704','2318947','2321554','2320362','2321538','2319904','2320931','2319193','2321539','2319080','2321915','2319731','2320866','2319651','2319641','2319675','2321166','2319687','2321450','2318779','2319749','2320531','2321126','2321449','2321364','2320774','2321456','2321697','2320031','2319813','2320175','2319852','2321068','2320936','2321912','2321906','2321782','2319473','2321520','2320818','2320095','2320616','2321413','2320347','2319613','2321218','2319608','2321803','2321806','2320236','2321680','2318982','2321938','2319372','2319878','2320882','2321589','2319532','2319396','2321469','2319783','2320153','2318886','2318887','226929209222019','2321362','2319670','2319560','2321150','2319791','213099909212019','2321471','2319295','2321642','2321876','2318928','2320192','2319939','2319458','2321400','2320922','2321542','2320490','2319835','2322040','2318973','2321575','2321540','2321485','2321486','2321643','2321773','2321301','2320672','2319446','2321525','2320664','2321766','2320768','2319238','2319229','2319384','2319392','2321854','2320149','2318937','2319209','2320193','2318842','2321541','2319472','2319474','2321559','2321772','2320345','2320631','2319346','2320980','2319979','2320229','2319115','2319103','2320717','2320374','2320988','2320145','2320427','2320165','2320151','2319547','2321374','2318908','2321024','2321502','2321037','2321031','2321503','2321286','2319175','2321737','2321389','2319816','2321627','2320218','2319000','2318796','2319104','2317924','2319430','2319431','227682609212019','2320927','2318940','2321303','2320085','2321429','2320001','2318778','2318795','2320650','2321974','2318799','2319050','2318883','2321157','2318959','221674209222019','2319402','2319648','2319416','2320556','2319148','2320583','2318962','2320232','2321556','2320106','2319563','2321339','2320630','2321454','2320015','2319509','2319263','2320792','2320417','2320860','2319581','2321056','2320946','2320954','2322039','2320002','2321609','2319123','2321102','2320251','2321574','2321977','2321225','2318815','2321500','2320459','2320692','2319132','2319019','2320497','2319160','2319062','2321048','2320119','2319863','2320073','2317477','2321480','2321922','2320123','2321558','2319987','2321557','2318781','2321828','2321127','2321592','2319920','2321447','2320274','2319909','2320181','2319420','2319418','2321135','2319293','2320738','2320821','2320635','2320790','2319391','2320823','2320336','2318802','2321878','2319682','2321063','2319329','2320080','2319545','2321757','2321155','224248409222019','2319890','2321628','2320282','2320044','2321944','2319382','2320319','215312309222019','2319923','2321509','2321512','2321296','2321750','2319849','2318975','2318891','2321026','2321274','2321616','2321749','2319977','2321424','2320742','2320131','2319278','2321263','2320042','2320188','2320828','2319066','2321535','2319130','2320868','2321008','2318881','2321861','2319168','2319792','2319165','2319220','2320607','2321931','2320732','2321678','2319260','2321344','2319908','2321136','2320690','2321402','2321046','2321329','2319836','2319348','2318990','229601809212019','2319237','2320244','2320878','2319698','2319307','2320434','2319099','2319587','2319476','2320309','2320349','2318980','2320376','2320372','2319530','2319538','2320545','2320260','2319781','2321992','2319042','2321756','2320171','2322026','2321280','2319468','2321265','2319078','2321181','2321805','2320365','2321103','2319767','2319758','2315493','2320134','2321137','2319421','2320152','2319790','2319294','2321501','2318785','2321696','2320059','2319837','2321545','2322032','2319462','2321771','2321671','2321457','2319502','2319497','2320505','2319619','2321945','2320524','2321110','2319381','2321970','2321595','2320648','2321700','2320562','2320571','2318913','2321998','2319567','2322011','2321041','2321515','2321514','2321516','2320461','2319059','2321781','2321549','2319088','2319478','2321975','2320658','2320561','2319564','2319424','2319044','2321529','2320566','2321039','2320335','2319611','2320115','2318995','2319520','2321959','2320523','2321239','2319535','2321963','2320542','2320018','2320305','2321722','2319149','2319285','2320148','2319161','2321879','2320589','2321259','2321767','2319371','2320247','2319763','2320387','2320281','2320464','2319741','2320633','2320637','2320535','2321347','2321613','2320908','2320696','2320698','2321517','2320718','2321430','2320270','2321032','2320667','2321089','2321071','2319826','2317814','2321248','2319590','2319475','2321958','2320168','2321840','2321984','2319557','2320439','2320437','2322031','2319481','2319138','2320913','2319912','2319356','2320446','2321887','2321252','2319568','2320006','2320548','2320891','2319189','2319457','2320195','2319712','2319327','2319821','2321393','2321395','2320313','2319466','2320332','2319332','2319594','2319170','2319477','2320483','2320344','2318957','2319487','2319337','2319231','2321672','2321084','2321930','2319098','2321936','2319498','2321817','2319233','2318759','2319110','2319508','2320368','2319097','2318769','2318979','2319616','2319982','2319511','2319983','2320504','2319620','2319513','2321570','2320510','2319515','2321821','2321440','2319118','2319043','2321810','2319524','2319521','2319527','2321965','2320526','2320005','229242309202019','2321839','2319380','2321596','2321972','2320529','2319533','2321968','2320388','2321971','2321582','2320381','2319021','2318873','2318372','2321967','2318874','2319139','2320751','2321699','2321123','2321978','2321132','2319552','2320029','2321143','2318814','2320410','2319550','2319551','2321732','2321731','2321990','2319415','2319554','2319146','2319555','2319787','2318819','2320568','2319303','2319422','2319562','2320569','2318915','2321866','2319672','2322004','2320289','2319304','2320580','2319036','2319096','2318920','2319435','2320582','2319171','2321880','2320585','2319573','2319683','2320591','2321382','2319437','2321497','2320294','2320595','2320594','2319317','2321652','2320163','2319668','2319207','2319227','2319655','2321370','2320791','2319696','2321988','2322006','2320598','2319765','2319892','2319649','2318907','2321257','2321774','2320311','2320527','2319794','228785609232019','2319178','2320601','2321896','2321394','2321527','2321328','2320511','2318996','2320537','2319140','2319127','2319141','2321983','2320411','2321996','2322005','2322009','2320172','2319434','2320586','2320588','2321496','2320306','2321871','2320307','2319172','2319182','2321083','2320484','2321439','2320508','2319117','2321112','2321006','2321611','2319797','2319789','2319807','2318830','2320453','2320375','2321012','2320473','2319482','2321431','2319108','2320254','2319017','2318812','2321015','2321385','2320295','2319796','2319341','2321919','2321918','2320386','2320384','2320373','2320761','2321464','2321598','2319413','2320404','2319414','2320904','2319799','2321272','2321271','2320185','2319450','2320494','2320329','2320431','2320093','2321934','2322037','2318932','2320500','2319529','2321980','2319825','2321427','2321566','2321859','2321249','2321838','2321191','2319179','2321044','2321910','2321909','2320081','2319340','2320957','2319361','2319240','2321960','2320985','2320397','2319900','2319406','2319773','2319915','2321159','2319428','2321495','2320296','2319697','2319710','2321297','2320709','2319844','2318981','2321209','2319788','2320906','2321278','2320602','2319700','2319962','2319167','2319707','2318965','2321523','2320844','2321206','2319747','2320991','2321462','2320640','2320011','2320156','2319935','2319196','2319583','2320304','2321220','2321270','2320842','2320872','2321576','2321693','2320127','2319658','2321760','2319067','2320625','2321573','2319239','2318773','2321095','2321477','230467409222019','2320879','2321884','2320233','2320233','2319037','223861809222019','2320813','2320314','2321914','2319067','2319981','2320100','2319250','2320869','2319998','2319891','2320867','2319769','2321000','2319394','2319779','2319156','2321153','2320066','2320322','2321659','2319334','2320826','2319205','2319600','2321650','2319967','2320224','2320963','2321532','2320711','2319368','2321216','2319623','2320864','2319599','2319884','2319755','2321822','2321953','2321461','2320887','2319889','2320258','2321707','2320660','2321240','2320265','2318875','2320414','2320676','2320786','2319022','2321868','2320161','2320668','2320435','2320160','2319047','2320935','2319575','2320811','2321293','2320070','2319213','2320840','2319883','2320975','2321228','2320250','2321852','2321600','2321614','2320769','2320071','2319680','2319054','2319700','2319962','2319167','2319707','2318965','2320112','2320844','2321206','2319747','2320640','2320011','2321841','2320156','2319935','2319196','2319583','2319191','2320606','2321070','2321417','2321088','2320880','2321571','2321689','2318876','2319907','2318899','2318899','2321073','2319839','2320852','2320102','2320615','2319867','2319112','2320744','2320636','2320998','2321359','2320143','2320408','2321019','2319046','2320955','2321035','2321813','2320871','2321080','2318230','2319305','2320801','2322029','2321651','2321319','2321318','2321688','2321432','2320990','2321213','2321215','2321460','2321645','2321601','2321602','2319782','2321375','2321375','2320460','2321636','2320016','2320476','2319083','2320478','2321777','2321777','2319715','2321290','2318846','2321323','2321658','2318843','2318843','2321060','2320613','2321304','2322041','2320617','2320618','2319850','2319841','2320715','2320964','2321801','2320965','2320111','2321561','2319483','2319241','2320241','2320223','2320223','2318988','2319985','2319369','2319612','2319723','2320731','2319722','2318776','2319617','2319989','2318770','2320729','2321350','2320870','2321121','2319991','2321104','2321585','2321578','2320126','2319251','2320116','2319252','2319645','2320747','2319129','2319397','2320654','2321255','2321708','2321009','2319652','2318888','2320249','2320651','2320651','2321141','2320770','2319669','2320902','2319258','2320684','2320661','2320039','2321862','2320771','2321493','2318889','2321476','2321476','2319041','2319916','2320652','2319916','2321276','2320910','2321377','2321277','2321376','2319681','2321158','2318922','2319676','2318917','2321154','2320049','2320186','2321154','2319815','2319812','2318930','2321490','2320685','2319946','2318832','2320191','2320054','2319176','2319177','2321752','2319691','2320810','2321886','2321618','2321886','2319057','2319944','2321169','2319584','2321498','2321632','2319195','2321307','2319190','2319255','2320699','2320941','2319960','2321665','2321404','2320026','2319877','2321322','2320109','2321710','2321674','2320394','2320886','2320027','2321843','2320773','2320924','2321741','2320912','2321380','2321617','2321378','2320285','2319943','2321508','2320814','2320841','2319819','2321273','2321414','2321714','2319296','2320064','2320861','2320939','2320925','2320701','2321793','2321794','2319601','2320216','2321197','2320971','2320363','2321758','2319734','2319122','2321321','2319624','2318999','2320762','2320390','2321842','2319784','2321139','2320146','2320142','2319910','2321275','2319929','2319311','2319931','2320179','2319377','2319256','2319713','2319961','2321776','2319602','2321326','2321081','2321800','2320101','2321298','2320749','2321221','2319235','2321214','2319746','2320135','2320992','2319643','2321369','2319768','2319661','2320406','2320772','2321152','2319917','2319038','2319280','2321253','2320048','2319436','2319576','2319934','2319058','2321187','2320190','2321045','2318933','2320949','2321381','2321746','2321943','2321331','2319314','2320035','2319830','2321101','2321826','2320057','2320139','2320069','2320000','2319199','2320973','2319224','2320876','2321011','2319447','2320113','2318825','2320609','2320389','2319604','2320090','2320499','2321679','2319124','2320997','2318884','2321242','2320419','2321372','2319445','2318801','2319536','2319411','2320477','2320788','2320312','2319937','2321189','2320590','2320956','2320338','2319011','2319596','2321695','2320385','2319208','2319709','2320346','2321670','2321309','2321330','2321797','2319750','2319247','2320122','2321474','2321827','2320130','2321724','2320263','2320875','2321860','2320558','2320680','2321036','2321755','2321042','2319685','2320303','2321167','2321511','2319456','2319846','2322036','2321903','2321764','2318948','2320710','2321647','2318839','2319186','2319331','2321925','2319470','2320942','2320611','2320340','2320333','2320966','2321210','2319218','2318974','2319211','2321530','2321194','2321201','2320493','2320849','2321175','2321544','2319479','2320832','2319973','2321715','2319353','2319738','2319087','2319355','2319488','2319354','2320848','2319226','2319737','2321927','2319496','2319870','2320725','2320977','2320629','2319507','2321807','2318784','2320226','2319875','2320863','2321097','2319876','2320361','2321799','2321798','2320514','2320242','2321686','2319001','2319517','2319888','2319743','2319109','2320380','2321816','2318859','2320739','2318860','2320740','2321590','2321111','2320407','2320987','2321237','2319771','2321730','2321250','2321130','2321002','2318879','2320540','2321594','2321851','2321580','2321754','2320764','2320402','2320259','2319137','2320752','2318804','2319405','2320396','2320154','2320753','2319898','2320551','2321481','2321850','2321494','2321982','2318831','2318820','2321849','2318821','2320897','2318811','2321014','2320557','2319030','2319153','2320670','2321355','2320665','2321383','2319290','2320780','2319169','2320572','2320288','2321043','2321795','2319427','2320280','2322007','2318936','2321872','2320795','2320707','2319574','2320934','2320923','2320183','2320409','2319313','2321765','2321882','2322017','2321098','2320299','2319301','2321883','2319452','2321633','2321085','2319448','2319823','2321889','2319806','2319187','2320457','2321648','2320806','2319316','2320456','2320310','2320528','2320067','2320330','2320205','2319859','2321190','2319965','2320496','2319636','2321223','2318978','2319014','2320515','2319880','2320519','2320121','2321955','2320674','2320173','2321260','2320316','2318934','2321059','2321587','2319640','2321458','2319811','2318836','2319629','2320120','2320448','2321792','2321291','2321305','2321435','2319120','2321128','2319143','2319659','2319164','2320503','2321824','2320474','2319026','2321172','2320831','2319471','2319388','2321979','2318818','2320083','2318958','2319970','2320978','2319111','2321685','2319637','2318880','2319272','2320028','2320921','2319786','2319772','2319646','2321308','2321796','2318987','2319847','2319016','2318852','2319335','2319754','2320449','2320721','2321028','2319957','2319271','2319289','2318944','2319426','2319188','2319848','2320628','2319071','2320495','2320082','2319345','2320276','2320525','2321354','2319400','2318808','2319543','2321999','2320706','2319053','2320597','2321174','2319868','2321555','2319357','2319351','2319692','2319212','2320647','2321236','2321236','2319086','2321952','2320703','2321660','2318968','2319861','2318827','2319491','2321453','2321966','2320750','2320513','2319653','2319805','2319703','2319312','2321421','2321742','2321901','2321997','2319999','2318994','2319210','2321285','2321543','2320953','2321917','2321946','2320371','2320243','2318772','2321989','2318927','2321790','2321261','2321340','2319919','2319299','2319370','2319793','2318963','2319009','2319387','2321173','2319492','2319243','2319860','2319986','2319003','2321622','2320950'];
			 
 			$this->loadModel( 'OrderLocation' );
			
			foreach($orders as $oid){
				 $it	= $this->MergeUpdate->find('count',array('conditions'=>array('status'=>0,'order_id'=>$oid)));
				
				if($it > 0) {
					echo "{$oid}  was last modified";
					echo "<br>";
					 
					
 					//$this->MergeUpdate->updateAll(array('MergeUpdate.picklist_username' => "''", 'MergeUpdate.pick_list_status' => '0','MergeUpdate.picked_date' => "''"), array('MergeUpdate.order_id' => $oid));
					//$this->OrderLocation->updateAll(array('OrderLocation.pick_list' => '0','OrderLocation.picked_date' => "''"), array('OrderLocation.order_id' => $oid));
						
 				}
				//echo "<br>";
			}
		}
		
		
		
}



?>

