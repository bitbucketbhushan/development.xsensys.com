<?php

class AbortsController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Aborts";
    
    var $components = array('Session','Upload','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
    var $virtualFields;
    
    //abort all synchings
    public function utilizeAbortSync()
    {
		
		$this->layout = '';
		$this->autoRender = false;
		
		//Authorization
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		$AbortFilePath = WWW_ROOT .'img/Abort/Ajax/Route/'; 
		$dir = new Folder($AbortFilePath, true, 0755);
		$fileName = "abortPicklist.txt";
		
		//Authentication and autorization		
		if( file_get_contents( $AbortFilePath . $fileName ) == 1 ) 
		{
			//abort synch and all invocations
			$this->abortNow( file_get_contents( $AbortFilePath . $fileName ) );
		}
		else
		{
			//process now
			$this->abortNow( file_get_contents( $AbortFilePath . $fileName ) );
		}
		
	}
    
    private function abortNow( $fileData = null )
    {	
		$this->layout = '';
		$this->autoRender = false;
		
		echo $fileData;
		exit;
	}
    
}
?>
