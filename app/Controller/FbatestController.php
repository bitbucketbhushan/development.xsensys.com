<?php
error_reporting(1);
class FbatestController extends AppController
{
    var $name = "Fbatest"; 
    var $components = array('Session','Upload','Common','Auth','Paginator');
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
  		
	public function beforeFilter()
    {
		parent::beforeFilter();
		$this->layout = false;
		$this->Auth->Allow(array('getSourceList','pullOrder','getEods'));
		$this->Common = $this->Components->load('Common');  
     }
 
 	public function getSourceList()
	{
 		$this->layout = 'index';
		$this->autoRender = false;
		$this->loadModel( 'FbaLinnSource' );
		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
		App::import('Vendor', 'Linnworks/src/php/Inventory');
		
		
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
		//$multi = AuthMethods::Multilogin($username, $password);
 		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	
	
		$token = $auth->Token;	
		$server = $auth->Server;
		
		$getChannel = InventoryMethods::GetStockLocations( $token, $server );
		
		 if(count($getChannel) > 0){
		 	$this->FbaLinnSource->query("TRUNCATE `fba_linn_sources`");
 		   	foreach($getChannel as $v){
				if($v->LocationTag != ''){
					$data = [];
					$data['stock_location_id'] = $v->StockLocationId;
					$data['location_name'] = $v->LocationName;
					$data['location_tag'] = $v->LocationTag;
					$this->FbaLinnSource->saveAll($data);
 				}
			}
   		 }
		 exit;
	}
	
	public function pullOrder()
	{
  		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('Skumapping');
		$this->loadModel('FbaLinnSource');
		$this->loadModel('FbaLinnOrder'); 
		$this->loadModel('FbaLinnOrderItem');
 		 
 		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
	
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
 		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	
	
		$token = $auth->Token;	
		$server = $auth->Server;
	 		 	
		$sources = $this->FbaLinnSource->find('all');
		foreach($sources as $loc){
	 
			$location = $loc['FbaLinnSource']['stock_location_id'];
			
			$openorder	=	OrdersMethods::GetAllOpenOrders("","",$location,"",$token, $server);  
			
			if(count( $openorder) > 0){
			
				foreach($openorder as $orderid)
				{
					$checkOpenOrder = $this->FbaLinnOrder->find('first', array('conditions'=>array('order_id' => $orderid)));
					if(count($checkOpenOrder) == 0 )
					{
						$orders[]	=	$orderid;
					}
				}
				
				if(!empty( $orders ) && count( $orders ) > 0 )
				{				
					$results = OrdersMethods::GetOrders($orders,$location,true,true,$token, $server);
				 
					foreach($results as $val){
					//	pr($val);
						$orderdata = [];
						if($val->GeneralInfo->Status > 0){
						
							$orderdata['num_order_id'] = $val->NumOrderId;
							$orderdata['status'] = $val->GeneralInfo->Status;
							$orderdata['marker'] = $val->GeneralInfo->Marker;
							$orderdata['reference_num'] = $val->GeneralInfo->ReferenceNum;
							$orderdata['received_date'] = $val->GeneralInfo->ReceivedDate;
							$orderdata['source'] = $val->GeneralInfo->Source;
							$orderdata['sub_source'] = $val->GeneralInfo->SubSource;
							$orderdata['despatch_by_date'] = $val->GeneralInfo->DespatchByDate;
							
							$orderdata['postal_service_name'] = $val->ShippingInfo->PostalServiceName;
							$orderdata['postage_cost'] = $val->ShippingInfo->PostageCost;
							$orderdata['tracking_number'] = $val->ShippingInfo->TrackingNumber;
							
							$orderdata['channel_buyer_name'] = $val->CustomerInfo->ChannelBuyerName;
							$orderdata['email_address'] = $val->CustomerInfo->Address->EmailAddress;
							$orderdata['address1'] = $val->CustomerInfo->Address->Address1;
							$orderdata['address2'] = $val->CustomerInfo->Address->Address2;
							$orderdata['address3'] = $val->CustomerInfo->Address->Address3;					
							$orderdata['town'] = $val->CustomerInfo->Address->Town;
							$orderdata['region'] = $val->CustomerInfo->Address->Region;
							$orderdata['post_code'] = $val->CustomerInfo->Address->PostCode;
							$orderdata['country'] = $val->CustomerInfo->Address->Country;
							$orderdata['phone_number'] = $val->CustomerInfo->Address->PhoneNumber;
							$orderdata['company'] = $val->CustomerInfo->Address->Company;
							
							$orderdata['subtotal'] = $val->TotalsInfo->Subtotal;
							$orderdata['postage_cost_total'] = $val->TotalsInfo->PostageCost;
							$orderdata['tax_total'] = $val->TotalsInfo->Tax;
							$orderdata['total_charge'] = $val->TotalsInfo->TotalCharge;
							$orderdata['currency'] = $val->TotalsInfo->Currency;
							$orderdata['order_id'] = $val->OrderId;
							$orderdata['added_date'] = date('Y-m-d');
							
							$check = $this->FbaLinnOrder->find('first', array('conditions'=>array('num_order_id' => $val->NumOrderId)));
							if(count($check) == 0){
								$this->FbaLinnOrder->saveAll($orderdata);
								$order_inc_id	=	$this->FbaLinnOrder->getLastInsertId();
								
								foreach($val->Items as $item){
									$itemdata = [];
 									$master_sku = '';
									$map = $this->Skumapping->find('first', array('conditions'=>array('channel_sku' => $item->ChannelSKU),'fields'=>['sku']));
									if(count($map) > 0){
										$master_sku = $map['Skumapping']['sku'];
									}
									$itemdata['order_inc_id'] = $order_inc_id;
									$itemdata['num_order_id'] = $val->NumOrderId;
									$itemdata['item_number'] = $item->ItemNumber;
									$itemdata['item_source'] = $item->ItemSource;
									$itemdata['title'] = addslashes($item->Title);
									$itemdata['quantity'] = $item->Quantity;
									$itemdata['channel_sku'] = $item->ChannelSKU;
									$itemdata['master_sku'] = $master_sku;
									$itemdata['channel_title'] = addslashes($item->ChannelTitle);
									$itemdata['price_per_unit'] = $item->PricePerUnit;
									$itemdata['tax'] = $item->Tax;
									$itemdata['tax_rate'] = $item->TaxRate;
									$itemdata['cost'] = $item->Cost;
									$itemdata['cost_inc_tax'] = $item->CostIncTax;
									$itemdata['sales_tax'] = $item->SalesTax;
									$itemdata['added_date'] = date('Y-m-d');
									$this->FbaLinnOrderItem->saveAll($itemdata);
 								
								}
							}
						}
						
					}
					
				}
			}else{
				echo 'no open orders';				
			}
		}
		 exit;
	}
	public function getEods()
    {
 		$this->layout = 'index';
 		$this->loadModel('Product' );  
 		$this->loadModel('SalesEod' );  
		$this->loadModel('StockEod' );  
		$this->loadModel('OpenOrder'); 	 
		$this->loadModel('FbaLinnOrder'); 
		$this->loadModel('FbaLinnOrderItem');
		
		$_stock = []; 		
		$stockDetail = $this->Product->find( 'all' , array('fields'=>['id','product_sku','current_stock_level'],'order' => 'Product.id ASC'));
		foreach( $stockDetail as $product )
		{
			$mainSku = $product['Product']['product_sku'];		
			$_stock[$mainSku] = $product['Product']['current_stock_level'];
 		}	
  		
		$eod = []; 
		$eod['stock']   	= json_encode($_stock);
		$eod['stock_date']  = date('Y-m-d'); 
		$this->StockEod->save($eod);
		mail('avadhesh.kumar@jijgroup.com','Live stock eod','xsensys Stock Eod Mail '.date('Y-m-d H:i:s'));
		
		
		$fbaSale = $this->FbaLinnOrderItem->find('all', array('conditions'=>array('added_date' => date('Y-m-d')),'fields'=>['id','master_sku','quantity']));
 	 	$fbasales = []; $sales_skus = []; 
		foreach($fbaSale  as $fba )
		{
			$sku = $fba['FbaLinnOrderItem']['master_sku'];		
			$sales_skus[$sku][] = $fba['FbaLinnOrderItem']['quantity'];
 		}	
		
  		foreach(array_keys($sales_skus) as $sku){
			$fbasales[$sku]  = array_sum($sales_skus[$sku]); 
		}
		unset($sales_skus);
		
   		$sales = $this->OpenOrder->find('all', array('conditions'=>array('open_order_date LIKE' => date('Y-m-d').'%'),'fields'=>['id','items']));
 		$marsales = []; $sales_skus = []; 
		foreach($sales  as $mar )
		{
			$items = unserialize($mar['OpenOrder']['items']);		
			foreach($items  as $item )
			{
				$sku = $item->SKU;
				$sales_skus[$sku][] = $item->Quantity;
			}
   		} 		 
		
  		foreach(array_keys($sales_skus) as $sku){
			$marsales[$sku]  = array_sum($sales_skus[$sku]); 
		}
		unset($sales_skus);
		
			
		$eod = []; 
		$eod['fba_sale']   		= json_encode($fbasales);
		$eod['merchant_sale']	= json_encode($marsales);
		$eod['sale_date']  		= date('Y-m-d'); 
		$this->SalesEod->save($eod);
		mail('avadhesh.kumar@jijgroup.com','Live stock eod','Xsensys Sales Eod Mail '.date('Y-m-d H:i:s'));
  		 
		exit;
 	}
	
 	public function getFbaStockEods($days = 30)
    {
 		$this->layout = 'index';
		$this->loadModel('FbaStockEod'); 
		$this->loadModel('FbaInventoryDetail'); 
		
 		$this->loadModel('Skumapping');
 		$this->loadModel('SupplierMapping');
		$this->loadModel('FbaCurrentStock');
		$this->loadModel('ProChannel'); 
		$this->loadModel('FbaStockEod'); 
		
 		
 		$channel_type = []; $channels = [];
		$pchannel = $this->ProChannel->find('all', array('conditions' => array('channel_type' => 'fba'),'fields' => array('channel_title','channel_type')));
		if(count($pchannel)> 0){
			foreach($pchannel as $sk){
				$channel_type[$sk['ProChannel']['channel_title']] = $sk['ProChannel']['channel_type'];
				$channels[$sk['ProChannel']['channel_title']] = $sk['ProChannel']['channel_title'];
			}
		}
		
 	 	$cb_fba_skus = []; $ma_fba_skus = []; $ra_fba_skus = [];
		$m_skus = $this->Skumapping->find('all', array('conditions' => array('channel_name' => $channels),'fields' => array('sku','channel_sku','stock_status','listing_status','channel_name')));
		foreach($m_skus as $sk){
 			if(substr($sk['Skumapping']['channel_name'],0,11) == 'CostBreaker'){
				$cb_fba_skus[$sk['Skumapping']['channel_sku']] = $sk['Skumapping']['channel_sku'] ;
			}else if(substr($sk['Skumapping']['channel_name'],0,5) == 'Marec'){
				$ma_fba_skus[$sk['Skumapping']['channel_sku']] = $sk['Skumapping']['channel_sku'] ;
			}else if(substr($sk['Skumapping']['channel_name'],0,4) == 'Rain'){
				$ra_fba_skus[$sk['Skumapping']['channel_sku']] = $sk['Skumapping']['channel_sku'] ;
			}
  		}
		 
 		
 	 	$date = date('Y-m-d',strtotime("-{$days} days"));
		$fbas = $this->FbaInventoryDetail->find('all', array('conditions'=>array('snapshotdate >' => $date),'fields'=>['id','snapshotdate','channel_sku','store','quantity']));
 	 	$costbreaker = []; $rainbow = [];  $marec = []; $dates = [];
		foreach($fbas  as $fba )
		{
			$date = $fba['FbaInventoryDetail']['snapshotdate'];
			$dates[$date] = $date;
			if($fba['FbaInventoryDetail']['store'] == 'costbreaker'){
  				 $costbreaker[$date][$fba['FbaInventoryDetail']['channel_sku']][] = $fba['FbaInventoryDetail']['quantity'];
 			}else if($fba['FbaInventoryDetail']['store'] == 'rainbow'){
				$rainbow[$date][$fba['FbaInventoryDetail']['channel_sku']][] = $fba['FbaInventoryDetail']['quantity'];
			}else{
				$marec[$date][$fba['FbaInventoryDetail']['channel_sku']][] = $fba['FbaInventoryDetail']['quantity'];
			}
  		}	
 		foreach($dates as $date){
			$_costbreaker = [];$_rainbow = [];$_marec = [];
			if(isset($costbreaker[$date])){
				foreach($costbreaker[$date] as $sku => $qtArr ){
 					$_costbreaker[$sku] = array_sum($qtArr); 
				}
			}
			foreach($cb_fba_skus as $sku ){
				if(!isset($_costbreaker[$sku])){
					$_costbreaker[$sku] = 0; echo $sku;exit;
				} 
			}
			
  			if(isset($rainbow[$date])){
				foreach($rainbow[$date] as $sku => $qtArr ){
					$_rainbow[$sku] = array_sum($qtArr); 
				}
			}
			foreach($ra_fba_skus as $sku ){
				if(!isset($_rainbow[$sku])){
					$_rainbow[$sku] = 0;
				} 
			}
			
 			if(isset($marec[$date])){
				foreach($marec[$date] as $sku => $qtArr ){
					$_marec[$sku] = array_sum($qtArr); 
				}
			}
			foreach($ma_fba_skus as $sku ){
				if(!isset($_marec[$sku])){
					$_marec[$sku] = 0;
				} 
			}
			$eod = []; 
			$stock = $this->FbaStockEod->find('first', array('conditions'=>array('stock_date' => $date),'fields'=>['id']));	
			if(count($stock) > 0){
				$eod['id'] = $stock['FbaStockEod']['id']; 
			} 
 			$eod['costbreaker']   	= json_encode($_costbreaker);
			$eod['marec']   		= json_encode($_marec);
			$eod['rainbow']			= json_encode($_rainbow);
			$eod['stock_date']  	= $date; 
			$eod['modified']  		= date('Y-m-d H:i:s'); 
			$this->FbaStockEod->saveAll($eod);
		}
		
		 $this->Session->setFlash("Fba Update Stock Eod updated", 'flash_success');
 		 $this->redirect($this->referer());//mail('avadhesh.kumar@jijgroup.com','Live stock eod','Xsensys Sales Eod Mail '.date('Y-m-d H:i:s'));
  		 
		exit;
 	}
	
	 

}