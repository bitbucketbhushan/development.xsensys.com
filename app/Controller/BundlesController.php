<?php

class BundlesController extends AppController
{
    
    var $name = "Bundles";
    
    var $components = array('Session','Upload','Common','Paginator');
    
    var $helpers = array('Html','Form','Session','Common' , 'Soap' , 'Paginator');
    
    public function beforeFilter()
	{
	    parent::beforeFilter();
	    $this->layout = false;
	    $this->Auth->Allow(array('productVirtualStockCsvGeneration', 'prepareVirtualStock', 'getStockDataForCsv'));
	}
    
    public function bundle()
    {
		
		$this->layout = "index";
		$this->set( 'role' , 'Create Bundle' );
		
	}
    	
	public function showallproduct()
    {
		$this->layout = "index";
		
		$this->loadModel( 'BinLocation' );
		
        $productAllDescs = $this->Product->find('all');
                
		$this->paginate = array(
                        'fields' => array( 
                                'DISTINCT Product.product_sku',
                                'Product.*',
                                'ProductDesc.*'
                            ),
			'limit' => 10			
		);
		 
		//$this->Product->recursive = 7;
		
		// we are using the 'Product' model
		$productAllDescs = $this->paginate('Product');
                 
		//$productAllDescs = $this->Product->find('all');				
		
		$this->set('productAllDescs',$productAllDescs);
		$this->set( 'role','Show All Product' );        
	}
	
	public function getBarcodeSearchCheckIn()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		$this->loadModel('BinLocation');
		$this->loadModel('CheckIn');
		$this->loadModel('Po');
		
		$barcode	=	trim($this->request->data['barcode']);
		
		/*$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array('ProductDesc.barcode' => $barcode)
								)
							);*/
							
		$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array(
										'OR' => array(
												array('ProductDesc.barcode' => $barcode),
												array('Product.product_sku LIKE' => '%'.$barcode.'%'),
												array('Product.product_name LIKE' => '%'.$barcode.'%')
										)
									)
								)
							);
		
		$getBarcodeFromProduct	=	$productdeatil['ProductDesc']['barcode'];
		
		$this->ProductDesc->unbindModel( array(
			'hasOne' => array(
				'Product'
			)
		) );
		
		$binLocation =	$this->BinLocation->find('all', array(
									'joins' => array(
										array(
											'table' => 'product_descs',
											'alias' => 'ProductDesc',
											'type' => 'INNER',
											'conditions' => array(
												'ProductDesc.barcode = BinLocation.barcode',
												'BinLocation.barcode ='.$getBarcodeFromProduct
											)
										)
									)
								)
							);
		
		$data['id'] 				= 	$productdeatil['ProductDesc']['id'];
		$data['stock'] 				= 	$productdeatil['Product']['current_stock_level'];
		$data['product_id']			= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 				= 	$productdeatil['Product']['product_name'];
		$data['sku'] 				= 	$productdeatil['Product']['product_sku'];
		$data['shortdescription'] 	= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 	= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 				= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 			= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 				= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 			= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 			= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 			= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		$data['bin_combined_text'] 			= 	$productdeatil['ProductDesc']['bin_combined_text'];
		
		//Get and set Bin Locations
		$binLocate = 0;
		foreach($binLocation as $binLocationIndex => $binLocationValue)
		{
			$binData[$binLocate]['id']					=	$binLocationValue['BinLocation']['id'];	
			$binData[$binLocate]['barcode']				=	$binLocationValue['BinLocation']['barcode'];	
			$binData[$binLocate]['bin_location']		=	$binLocationValue['BinLocation']['bin_location'];	
			$binData[$binLocate]['stock_by_location']	=	$binLocationValue['BinLocation']['stock_by_location'];	
		$binLocate++;
		}
		$data['binLocationArray'] = $binData;
		
		$imageUrl	=	Router::url('/', true).'img/product/';
		
		$i = 0;
		foreach($productdeatil['ProductImage'] as $image)
		{
			if($image['image_name'] != '')
			{
				$imagedata[$i]['imageid']		=	$image['id'];
				$imagedata[$i]['imagename']		=	$imageUrl.$image['image_name'];
				$imagedata[$i]['selectedimage']	=	$image['selected_image'];
			}
			$i++;
		}
		
		$j = 0;
		foreach( $productdeatil['ProductLocation'] as $productLocation)
		{
			$productLocation['warehouse_id'];
			$locationData[$j]['binrackid']	=	$productLocation['bin_rack_address'];
			$warehouseDetail	=	$this->Warehouse->find('first', array('conditions' => array('Warehouse.id' => $productLocation['warehouse_id'])));
			$locationData[$j]['warehousename']	=	$warehouseDetail['Warehouse']['warehouse_name'];
			$locationData[$j]['city']			=	$warehouseDetail['WarehouseDesc']['city_id'];
			$locationData[$j]['warehousetype']	=	$warehouseDetail['WarehouseDesc']['warehouse_type'];
			$j++;
		}
		//echo (json_encode(array('status' => '1', 'data' => $data, 'image' => $imagedata, 'location' => $locationData)));
		// get user detail related to sku
		
		$userLists	=	$this->CheckIn->find( 'all', array( 'conditions' => array('CheckIn.barcode' => $barcode,  'CheckIn.user_id'=> $userId) ) );
		
		
		
		foreach( $userLists as $userListsValue )
		{
			$userdetail = $this->getUserDeatil( $userListsValue['CheckIn']['user_id'] );
			$useData['sku'] 		= 	$userListsValue['CheckIn']['sku'];
			$useData['user_detail'] = 	$userdetail;
			$useData['pc_name'] 	= 	$userListsValue['CheckIn']['pc_name'];
			$useData['qty'] 		= 	$userListsValue['CheckIn']['qty_checkIn'];
			$useData['date'] 		= 	date("jS F, Y", strtotime($userListsValue['CheckIn']['date']));
			$getData[] = $useData; 
		}
		
		//$this->Po->bindModel(array('hasMany' => 'CheckIn'));
		
		$params	=	array(
					'joins' => array(
							array(
								'table' => 'purchase_orders',
								'alias' => 'PurchaseOrder',
								'type' => 'INNER',
								'conditions' => array(
									'Po.po_name = PurchaseOrder.po_name'
								)
							)),
					'fields' => array( 'Po.id','Po.po_name' ),
					'conditions'=> array( 'Po.status' => 1, 'PurchaseOrder.purchase_barcode' => $barcode ),
					
				);
				
		$getpoList	=	$this->Po->find( 'list', $params);
		//$this->set( 'getpoList', $getpoList );
	
		echo (json_encode(array('status' => '1', 'data' => $data, 'userData' => $getData, 'getpoList' => $getpoList)));
		exit;	
	}
	
	public function getUserDeatil( $userId = null )
	{
		$this->loadModel( 'User' );
		$uerDeatil =	$this->User->find( 'first', array( 'conditions' => array( 'User.id' => $userId ) ) );
		$userFullName	=	$uerDeatil['User']['first_name'].' '.$uerDeatil['User']['last_name'];
		return $userFullName;
	}
	
	public function getBarcodeSearch()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		$this->loadModel('BinLocation');
		
		$barcode	=	trim($this->request->data['barcode']);
		
		/*$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array('ProductDesc.barcode' => $barcode)
								)
							);*/
		
		$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array(
										'OR' => array(
												array('ProductDesc.barcode' => $barcode),
												array('Product.product_sku LIKE' => '%'.$barcode.'%'),
												array('Product.product_name LIKE' => '%'.$barcode.'%')
										)
									)
								)
							);
		
		$getBarcodeFromProduct	=	$productdeatil['ProductDesc']['barcode'];
		
		$this->ProductDesc->unbindModel( array(
			'hasOne' => array(
				'Product'
			)
		) );
		
		/*$binLocation =	$this->BinLocation->find('all', array(
									'joins' => array(
										array(
											'table' => 'product_descs',
											'alias' => 'ProductDesc',
											'type' => 'INNER',
											'conditions' => array(
												'ProductDesc.barcode = BinLocation.barcode',
												'BinLocation.barcode ='.$barcode
											)
										)
									)
								)
							);*/
		
               
                $binLocation =	$this->BinLocation->find('all', array( 'conditions' => array(
                                                                        'BinLocation.barcode' => $getBarcodeFromProduct
                                                                    )
                                                                    
								)
							);
                
		$data['id'] 				= 	$productdeatil['ProductDesc']['id'];
		$data['stock'] 				= 	$productdeatil['Product']['current_stock_level'];
		$data['product_id']			= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 				= 	$productdeatil['Product']['product_name'];
		$data['sku'] 				= 	$productdeatil['Product']['product_sku'];
		$data['shortdescription'] 	= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 	= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 				= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 			= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 				= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 			= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 			= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 			= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		$data['bin_combined_text'] 			= 	$productdeatil['ProductDesc']['bin_combined_text'];
		
		//Get and set Bin Locations
		$binLocate = 0;
                
		foreach($binLocation as $binLocationIndex => $binLocationValue)
		{
			$binData[$binLocate]['id']					=	$binLocationValue['BinLocation']['id'];	
			$binData[$binLocate]['barcode']				=	$binLocationValue['BinLocation']['barcode'];	
			$binData[$binLocate]['bin_location']		=	$binLocationValue['BinLocation']['bin_location'];	
			$binData[$binLocate]['stock_by_location']	=	$binLocationValue['BinLocation']['stock_by_location'];	
		$binLocate++;
		}
		$data['binLocationArray'] = $binData;
		
		$imageUrl	=	Router::url('/', true).'img/product/';
		
		$i = 0;
		foreach($productdeatil['ProductImage'] as $image)
		{
			if($image['image_name'] != '')
			{
				$imagedata[$i]['imageid']		=	$image['id'];
				$imagedata[$i]['imagename']		=	$imageUrl.$image['image_name'];
				$imagedata[$i]['selectedimage']	=	$image['selected_image'];
			}
			$i++;
		}
		
		$j = 0;
		foreach( $productdeatil['ProductLocation'] as $productLocation)
		{
			$productLocation['warehouse_id'];
			$locationData[$j]['binrackid']	=	$productLocation['bin_rack_address'];
			$warehouseDetail	=	$this->Warehouse->find('first', array('conditions' => array('Warehouse.id' => $productLocation['warehouse_id'])));
			$locationData[$j]['warehousename']	=	$warehouseDetail['Warehouse']['warehouse_name'];
			$locationData[$j]['city']			=	$warehouseDetail['WarehouseDesc']['city_id'];
			$locationData[$j]['warehousetype']	=	$warehouseDetail['WarehouseDesc']['warehouse_type'];
			$j++;
		}
                
		//echo (json_encode(array('status' => '1', 'data' => $data, 'image' => $imagedata, 'location' => $locationData)));
		echo (json_encode(array('status' => '1', 'data' => $data)));
		exit;
	}
	
	public function removeLocation()
	{
		
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel( 'BinLocation' );	
		
		$barcode = $this->request->data['barcode'];
		
		//Save only
		$getLocation = explode( ',',$this->request->data['getLocation'] );
		$getStock = explode( ',',$this->request->data['getStockByLocation'] );
		
		$getBinLocation = $this->BinLocation->find( 'first', array( 'conditions' => array( 'BinLocation.barcode' => $barcode , 'BinLocation.bin_location' => $getLocation , 'BinLocation.stock_by_location' => $getStock ) , 'fields' => array( 'BinLocation.id' ) ) );
		$subStock = $this->request->data['getStockByLocation'];
		if( count($getBinLocation) > 0 )
		{			
			if( $subStock >= 0 )
			{	
				$product = $this->Product->find( 'first', array( 'conditions' => array( 'ProductDesc.barcode' => $barcode ) , 'fields' => array( 'Product.id','Product.current_stock_level' ) ) ); 
			
				(int)$subtractStock = (int)$product['Product']['current_stock_level'] - (int)$subStock;
				//$data['Product']['Product']['id'] = $product['Product']['id'];
				//$data['Product']['Product']['current_stock_level'] = $subtractStock;				
				//$this->Product->saveAll( $data['Product'] );
				
				$id = $product['Product']['id'];
				
				$this->Product->query( "UPDATE products set current_stock_level = {$subtractStock} where id = {$id}" );
				
				$binId = $getBinLocation['BinLocation']['id'];
				$this->BinLocation->query( "delete from bin_locations where id = {$binId}" );
								
				echo "1"; exit;	
			}		
		}
	}
	
	public function updatePackagingIdByBarcodeSearch()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel( 'BinLocation' );	
		
		/*pr($this->request->data);
		pr($this->BinLocation->find('all')); exit;*/
		
		$barcode = $this->request->data['barcode'];
		//Delete Specific barcode rows
		$this->BinLocation->query( "delete from bin_locations where barcode = '{$barcode}'" );
		      
		$product_id = $this->request->data['product_id'];
		$descId = $this->request->data['id'];
		$length = $this->request->data['getLength'];
		$width = $this->request->data['getWidth'];
		$height = $this->request->data['getHeight'];
		$weight = $this->request->data['getWeight'];
		$variant_envelope_name = $this->request->data['variant_envelope_name'];
		$variant_envelope_id = $this->request->data['variant_envelope_id'];
		$brand = $this->request->data['brand'];
		$bin = $this->request->data['bin_sku'];
		$binAllocation = $this->request->data['binAllocation'];
		
		//Unbinding Techniques 
		$this->ProductDesc->unbindModel( array( 'belongsTo' => array( 'Product' ) ) );
				
		$this->ProductDesc->updateAll(
			array(
				'length' => $length,
				'width' => $width,
				'height' => $height,
				'weight' => $weight,
				'variant_envelope_name' => "'".$variant_envelope_name."'",
				'variant_envelope_id' => $variant_envelope_id,
				'brand' => "'".$brand."'",
				'bin' => "'".$bin."'"
			),
			array(
				'ProductDesc.id' => $descId
			)
		);
		
		//Save only
		$getLocation = explode( ',',$this->request->data['getLocation'] );
		$getStock = explode( ',',$this->request->data['getStockByLocation'] );
		
		if( count($getLocation) > 0 )
		{
                    
			$stockCount = 0;$e = 0;while( $e < count( $getLocation ) )
			{
				
				$productdeatilInner	=	$this->Product->find('first', array(								
								'conditions' => array(
										'OR' => array(
												array('ProductDesc.barcode' => $barcode),
												array('Product.product_sku LIKE' => '%'.$barcode.'%'),
												array('Product.product_name LIKE' => '%'.$barcode.'%')
										)
									)
									,
									'fields' => array(
										'ProductDesc.barcode',
										'Product.product_sku'
									)
								)
							);	
							
				//pr($productdeatilInner); exit;
				
				$productBarcode = $productdeatilInner['ProductDesc']['barcode'];			
				if(  $getLocation[$e] != '' && $getStock[$e] != '' )
				{
					
					$this->request->data['BinLocationPreffered']['BinLocation']['barcode'] = $productBarcode;
					$this->request->data['BinLocationPreffered']['BinLocation']['bin_location'] = $getLocation[$e];
					$this->request->data['BinLocationPreffered']['BinLocation']['stock_by_location'] = $getStock[$e];
					$stockCount = $stockCount + $getStock[$e];
                    
					$this->BinLocation->saveAll( $this->request->data['BinLocationPreffered'] );
				}
			$e++;	
			}
		}
		
	   /*$this->Product->updateAll(
				array(
						'current_stock_level' => "'".$stockCount."'"
				),
				array(
						'Product.id' => $product_id
				)
		);*/
		
		$this->Product->query( "UPDATE products set current_stock_level = '{$stockCount}' where id = {$product_id}" );
                
		if( $stockCount > 0 ) 
		{
			//Update Product current stock
			/*$this->Product->updateAll(
				array(
					'current_stock_level' => "'".$stockCount."'"
				),
				array(
					'Product.id' => $product_id
				)
			);*/
                    
                    //$this->Product->query( "UPDATE products set current_stock_level = '{$stockCount}' where id = {$product_id}" );
		}
		
		//Get All related data
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		$this->loadModel('BinLocation');
		
		/*$this->ProductDesc->bindModel( array( 
			'hasMany' => array(        
				'BinLocation' => array(									   
					'className' => 'BinLocation',
					'foreignKey' => 'barcode'									   
					)
				) 
			) 
		);*/
		
		$barcode	=	$this->request->data['barcode'];
		
		/*$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array('ProductDesc.barcode' => $barcode, )
								)
							);*/
							
		$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array(
										'OR' => array(
												array('ProductDesc.barcode' => $barcode),
												array('Product.product_sku LIKE' => '%'.$barcode.'%'),
												array('Product.product_name LIKE' => '%'.$barcode.'%')
										)
									)
								)
							);					
		
		
		$this->ProductDesc->unbindModel( array(
			'hasOne' => array(
				'Product'
			)
		) );
		
		/*$binLocation =	$this->BinLocation->find('all', array(
									'joins' => array(
										array(
											'table' => 'product_descs',
											'alias' => 'ProductDesc',
											'type' => 'INNER',
											'conditions' => array(
												'ProductDesc.barcode = BinLocation.barcode'
											)
										)
									)
								)
							);*/
		$binLocation =	$this->BinLocation->find('all', array( 'conditions' => array(
                                                                        'BinLocation.barcode' => $productdeatil['ProductDesc']['barcode']
                                                                    )
                                                                    
								)
							);
        
            
		$data['id'] 				= 	$productdeatil['ProductDesc']['id'];
		$data['stock'] 				= 	$productdeatil['Product']['current_stock_level'];
		$data['product_id'] 				= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 				= 	$productdeatil['Product']['product_name'];
		$data['sku'] 				= 	$productdeatil['Product']['product_sku'];
		$data['shortdescription'] 	= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 	= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 				= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 			= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 				= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 			= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 			= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 			= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		$data['bin_combined_text'] 			= 	$productdeatil['ProductDesc']['bin_combined_text'];
		
		//Get and set Bin Locations
		$binLocate = 0;
		foreach($binLocation as $binLocationIndex => $binLocationValue)
		{
			$binData[$binLocate]['id']					=	$binLocationValue['BinLocation']['id'];	
			$binData[$binLocate]['barcode']				=	$binLocationValue['BinLocation']['barcode'];	
			$binData[$binLocate]['bin_location']		=	$binLocationValue['BinLocation']['bin_location'];	
			$binData[$binLocate]['stock_by_location']	=	$binLocationValue['BinLocation']['stock_by_location'];	
		$binLocate++;
		}
		$data['binLocationArray'] = $binData;
		
		$imageUrl	=	Router::url('/', true).'img/product/';
		
		$i = 0;
		foreach($productdeatil['ProductImage'] as $image)
		{
			if($image['image_name'] != '')
			{
				$imagedata[$i]['imageid']		=	$image['id'];
				$imagedata[$i]['imagename']		=	$imageUrl.$image['image_name'];
				$imagedata[$i]['selectedimage']	=	$image['selected_image'];
			}
			$i++;
		}
		
		$j = 0;
		foreach( $productdeatil['ProductLocation'] as $productLocation)
		{
			$productLocation['warehouse_id'];
			$locationData[$j]['binrackid']	=	$productLocation['bin_rack_address'];
			$warehouseDetail	=	$this->Warehouse->find('first', array('conditions' => array('Warehouse.id' => $productLocation['warehouse_id'])));
			$locationData[$j]['warehousename']	=	$warehouseDetail['Warehouse']['warehouse_name'];
			$locationData[$j]['city']			=	$warehouseDetail['WarehouseDesc']['city_id'];
			$locationData[$j]['warehousetype']	=	$warehouseDetail['WarehouseDesc']['warehouse_type'];
			$j++;
		}
		$data['status'] 				= 'success';	
                
		echo (json_encode(array('status' => '1', 'data' => $data)));		
		exit;	 
	}
		
	public function getRackAccordingToFloor()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		//Load Model
		$this->loadModel( 'Rack' );
		
		//Get Floor Id
		$rackId = $this->request->data['floorId'];
		$rackName = explode(',',$this->request->data['selectedText']);
		
		$rack = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , array( 'fields' => array( 'Rack.id' , 'Rack.rack_floorName' ) , 'conditions' => array( 'Rack.floor_name' => $rackName ) , 'group' => array( 'Rack.rack_floorName' ) , 'order' => array( 'Rack.id ASC' ) ) );		
		
		$str = '<select id="rackNumber" class="form-control" name="data[rackNumber]" multiple>';
		$str .= '<option value="">Choose Rack Number</option>';
		foreach( $rack as $rackIndex => $rackValue ):
			$str .= '<option value="'.$rackIndex.'" >' .$rackValue. '</option>';
		endforeach;
		$str .= '</select>';
		
		echo $str; exit;
		/*$this->set( compact($rackNameAccordingToFloorText) );
		$this>render( 'rack_number_file' );*/
	}
		
	/*
	 * 
	 * Params, Get Level accordingly above
	 * 
	 */ 
	public function getLevelAccordingG_R()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		//Load Model
		$this->loadModel( 'Rack' );
		
		//Get Floor Id
		$groundName = explode(',',$this->request->data['groundText']);
		$rackText = explode(',',$this->request->data['rackText']);
		
		$rack = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , 
				array( 
					'fields' => array( 'Rack.id' , 'Rack.level_association' ) , 
					'conditions' => array( 'Rack.floor_name' => $groundName , 'Rack.rack_floorName' => $rackText ) , 'group' => array( 'Rack.level_association' ) , 'order' => array( 'Rack.id ASC' ) 
					) 
				);		
		
		$str = '<select id="levelNumber" class="form-control" name="data[levelNumber]" multiple>';
		$str .= '<option value="">Choose Level Number</option>';
		foreach( $rack as $rackIndex => $rackValue ):
			$str .= '<option value="'.$rackIndex.'" >' .$rackValue. '</option>';
		endforeach;
		$str .= '</select>';
		
		echo $str; exit;
		
		/*$this->set( compact($rackNameAccordingToFloorText) );
		$this>render( 'rack_number_file' );*/
	}
	
	/*
	 * 
	 * Params, Get Level accordingly above
	 * 
	 */ 
	public function getSectionAccordingG_R()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		//Load Model
		$this->loadModel( 'Rack' );
		
		//Get Floor Id
		$groundName = explode(',',$this->request->data['groundText']);
		$rackText = explode(',',$this->request->data['rackText']);
		$levelText = explode(',',$this->request->data['levelSelected']);
		
		$rack = $rackNameAccordingToFloorText = $this->Rack->find( 'list' , 
				array( 
					'fields' => array( 'Rack.id' , 'Rack.rack_level_section' ) , 
					'conditions' => array( 'Rack.floor_name' => $groundName , 'Rack.rack_floorName' => $rackText , 'Rack.level_association' => $levelText ) , 'order' => array( 'Rack.id ASC' ) 
					) 
				);	
		$str = '<select id="sectionNumber" class="form-control" name="data[sectionNumber]" multiple>';
		$str .= '<option value="">Choose Section Number</option>';
		foreach( $rack as $rackIndex => $rackValue ):
			$str .= '<option value="'.$rackIndex.'" >' .$rackValue. '</option>';
		endforeach;
		$str .= '</select>';
		
		echo $str; exit;
		
		/*$this->set( compact($rackNameAccordingToFloorText) );
		$this>render( 'rack_number_file' );*/
	}
	
	/*
	 *
	 * Params, CheckIn Process 
	 * 
	 */
	public function checkIn()
	{
		$this->layout = 'index';
		
		$this->loadModel( 'Po' );
		$this->loadModel( 'CheckIn' );	
		
		$getpoList	=	$this->Po->find( 'list', array( 'fields'=> array( 'Po.id','Po.po_name' ), 'conditions' => array( 'Po.status' => 1) ) );
		$this->set( 'getpoList', $getpoList );
	}
		
	public function searchProductByBarcode()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		
		$barcode	=	$this->request->data['barcode'];
		
		$productdeatil	=	$this->Product->find('first', array('conditions' => array('ProductDesc.barcode' => $barcode)));
		//pr($productdeatil); exit;
		 
		$data['id'] 				= 	$productdeatil['ProductDesc']['id'];
		$data['product_id'] 				= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 				= 	$productdeatil['Product']['product_name'];
		$data['sku'] 				= 	$productdeatil['Product']['product_sku'];
		$data['shortdescription'] 	= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 	= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 				= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 			= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 				= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 			= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 			= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 			= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		
		$data['bin_specific_id'] 			= 	$productdeatil['ProductDesc']['bin_specific_id'];
		$data['bin_combined_text'] 			= 	$productdeatil['ProductDesc']['bin_combined_text'];		
		$allocationArray = explode( '##',$productdeatil['ProductDesc']['bin_combined_text'] );
		
		//echo (json_encode(array('status' => '1', 'data' => $data, 'image' => $imagedata, 'location' => $locationData)));
		echo (json_encode(array('status' => '1', 'data' => $data)));
		exit;
	}
	
	public function productStockExcel()
	{	 
			//chr(046)
		  $this->layout = '';
		  $this->autoRender = false;	
		  		  
		  $getStockDetail = ProductsController::getStockData();		 
		  ob_clean();    
		  App::import('Vendor', 'PHPExcel/IOFactory');
		  App::import('Vendor', 'PHPExcel');  
		  $objPHPExcel = new PHPExcel();       
		  
		  //Column Create  
		  $objPHPExcel->setActiveSheetIndex(0);
		  $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Product Name');     
		  $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Sku');
		  $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Barcode');
		  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Brand');		  
		  $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Price');		  
		  $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Avb. Stock');
		  $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Min. Stock'); 
		  $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Packaging Class'); 
		  $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Weight (kg)'); 
		  $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Length (mm)'); 
		  $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Width (mm)'); 
		  $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Height (mm)');
		  
		  /*$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Status'); */
		  
		  $inc = 2; foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue ):
		  
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getStockDetailValue['Product']['product_name'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $getStockDetailValue['Product']['product_sku'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getStockDetailValue['ProductDesc']['barcode'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getStockDetailValue['ProductDesc']['brand'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $getStockDetailValue['ProductPrice']['product_price'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $getStockDetailValue['Product']['current_stock_level'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $getStockDetailValue['Product']['minimum_stock_level'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $getStockDetailValue['ProductDesc']['variant_envelope_name'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $getStockDetailValue['ProductDesc']['weight'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, $getStockDetailValue['ProductDesc']['length'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, $getStockDetailValue['ProductDesc']['width'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, $getStockDetailValue['ProductDesc']['height'] );
				
				/* Get all bin location */ 
				$this->loadModel( 'BinLocation' );
				$params = array(
					'conditions' => array(
						'BinLocation.barcode' =>  $getStockDetailValue['ProductDesc']['barcode']
					)
				);
				
				$binLocation = json_decode(json_encode($this->BinLocation->find( 'all' , $params )),0);
				
				$totalStock = 0;
				$chrAscii = 77;
				$chrAscii2 = 78;
				$chrStart = 65;
				foreach( $binLocation as $binLocationIndex => $binLocationValue )
				{
						//echo $inc;
						$ch = chr( $chrAscii );
						$ch2 = chr( $chrAscii2 );
						$chStock = chr( $chrAscii );
						$putChar = chr( $chrStart );
						
						//Location
						$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Location-'.$putChar);
						$objPHPExcel->getActiveSheet()->setCellValue($ch.$inc, $binLocationValue->BinLocation->bin_location );
						
						//Stock per location
						$objPHPExcel->getActiveSheet()->setCellValue($ch2.'1', 'Stock');
						$objPHPExcel->getActiveSheet()->setCellValue($ch2.$inc, $binLocationValue->BinLocation->stock_by_location );
						
						//Stock add
						$totalStock = $totalStock + $binLocationValue->BinLocation->stock_by_location;
						
						$chrAscii = $chrAscii + 2;
						$chrStart++;
						$chrAscii2 = $chrAscii2 + 2;
						
				}
				//exit;
				$totalStock = 0;
				/*
				if( $getStockDetailValue['Product']['product_status'] == 0 ):				
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 'Active'); 
				else:
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$inc, 'De-Active' ); 
				endif;*/			
				  
		  $inc++;
		  endforeach;   
		  
		  //Set First Row for Range Analysis Sheet 
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:Q1')->getAlignment()->applyFromArray(
		  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:Q1')->getAlignment()->setWrapText(true);
		  $objPHPExcel->getActiveSheet(0)->setTitle('Current Stock');
		  $objPHPExcel->getActiveSheet(0)->getStyle("A1:Q1")->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet(0)
			 ->getStyle('A1:Q1')
			 ->applyFromArray(
			  array(
			   'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EBE5DB')
			   )
			  )
			 );		   
		  
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/stockUpdate.xls';
		  $uploadRemote = $getBase.'app/webroot/img/stockUpdate/stockUpdate.xls';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		  $objWriter->save($uploadUrl);		 
		   
	return $uploadRemote;	  
	}
	
	public function prepareExcel()
	{
		$this->layout = '';
		$this->autoRender = false;			
		echo ProductsController::productStockExcel(); exit;
	}
		
	public function getStockData()
	{
		 $this->loadModel('Product');
		 $this->loadModel('ProductDesc');
		 
		 $stockData = $this->Product->find( 'all' , 
                                array( 
                                    'fields' => array( 
                                        'DISTINCT Product.product_sku',
                                        'Product.*',
                                        'ProductDesc.*'
                                    ),
                                    'order' => 'Product.id DESC' 
                                )
                 );                 
	return $stockData;	 
	}
	
	public function getStockDataForCsv()
	{
		 $this->loadModel('Product');
		 $this->loadModel('ProductDesc');
		 
		 $this->Product->unbindModel( array(
			'hasOne' => array(
				'ProductDesc' , 'ProductPrice' 
				),
			'hasMany' => array(
				'ProductLocation'
				)		
			) 
		 );
		
		/*$this->ProductDesc->unbindModel(
			array(
				'belongsTo' => array(
					'Product'
				)
			)
		);*/
		
		$params = array(
			'fields' => array(
				'Product.id as MainProductId',
				'Product.product_name as ProductTitle',
				'Product.product_sku as MainSku',
				'Product.uploaded_stock as UploadStock',
				'Product.current_stock_level as AvailableStock',
				'ProductDesc.id as ProductDescId',
				'ProductDesc.product_id',
				'ProductDesc.barcode',
				'ProductDesc.product_type',
				'ProductDesc.product_defined_skus',
				'ProductDesc.id as ProductDescId',
				'ProductDesc.product_identifier'
			),
			'order' => 'ProductDesc.id ASC'
		);
		$stockData = $this->ProductDesc->find( 'all' , $params );
	return $stockData;	 
	}
		
	/*
	 * 
	 * Params, Prepare csv file for setup virtual product
	 * 
	 */ 
	public function prepareVirtualStock()
	{
		$this->layout = '';
		$this->autoRender = false;	 		
		echo ProductsController::productVirtualStockCsvGeneration();exit;
	}
	
	public function productVirtualStockCsvGeneration()
	{	 
		  $this->layout = '';
		  $this->autoRender = false;	
		  
		  $getBase = Router::url('/', true);
		  		  
		  $getStockDetail = json_decode(json_encode(ProductsController::getStockDataForCsv()),0);		 		  
		  //pr($getStockDetail); exit;
		  
		  /*
		   * 
		   * Params, To setup virtual stock for upload over linnworks where it will update on each platform, 
		   * that we are using ( Amazon, Ebay , Magento etc etc )
		   * 
		   */ 
		  
		   ob_clean();    
		   App::import('Vendor', 'PHPExcel/IOFactory');
		   App::import('Vendor', 'PHPExcel');  
		   $objPHPExcel = new PHPExcel();       
		  
		   //Column Create  
		   $objPHPExcel->setActiveSheetIndex(0);
		   $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Product Title');     
		   $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Sku');
		   $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Barcode');
		   $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Original Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Rainbow Retail(Amazon)');
		   $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Storeforlife (Magento)');
		   $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Rainbow Retail DE');
		   $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Rainbow Retail ES');
		   $objPHPExcel->getActiveSheet()->setCellValue('J1', 'CostBreaker_UK');
		   $objPHPExcel->getActiveSheet()->setCellValue('K1', 'CostBreaker_DE');
		   $objPHPExcel->getActiveSheet()->setCellValue('L1', 'CostBreaker_FR');
		   $objPHPExcel->getActiveSheet()->setCellValue('M1', 'CostBreaker_ES');
		   $objPHPExcel->getActiveSheet()->setCellValue('N1', 'CostBreaker_IT');
		   $objPHPExcel->getActiveSheet()->setCellValue('O1', 'ebuyersdirect-2u-uk');
		  
		   /*
		    * 
		    * 
		    * Outer Looping for each sku ( Single | Bundle-Single | Bundle-Multiple )
		    * 
		    * 
		    */ 
		   $inc = 2; $e = 0;
		   foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue ):
				$rainbowRetail	 =	0;
				$rainbowRetailDe =	0;
				$rainbowRetailSE =	0;
				$storeForLife	 =	0;
				$costBreakerUk	 =	0;
				$costBreakerDe	 =	0;
				$costBreakerFr	 =	0;
				$costBreakerEs	 =	0;
				$costBreakerIt	 =	0;
				$ebuyersdirect	 =	0;
				$sku =	$getStockDetailValue->Product->MainSku;
				$getvertualStockDetails	=	$this->getSkuQuantity( $sku );
				
				//Most Inner loop for 			
				foreach( $getvertualStockDetails as $getvertualStockDetail )
				{
					
					if( $getvertualStockDetail['subsource']['sub_source'] == 'Rainbow Retail')
					{
						$rainbowRetail = $rainbowRetail + $getvertualStockDetail['subsource']['quantity'];
					}
					else if( $getvertualStockDetail['subsource']['sub_source'] == 'RAINBOW RETAIL DE' )
					{
						$rainbowRetailDe = $rainbowRetailDe + $getvertualStockDetail['subsource']['quantity'];
					}
					else if( $getvertualStockDetail['subsource']['sub_source'] == 'Rainbow_Retail_ES' )
					{
						$rainbowRetailSE = $rainbowRetailSE + $getvertualStockDetail['subsource']['quantity'];
					}
					else if ( $getvertualStockDetail['subsource']['sub_source'] == 'http://www.storeforlife.co.uk' )
					{
						$storeForLife = $storeForLife + $getvertualStockDetail['subsource']['quantity'];
					}
					else if ( $getvertualStockDetail['subsource']['sub_source'] == 'CostBreaker_UK' )
					{
						$costBreakerUk = $costBreakerUk + $getvertualStockDetail['subsource']['quantity'];
					}
					else if ( $getvertualStockDetail['subsource']['sub_source'] == 'CostBreaker_DE' )
					{
						$costBreakerDe = $costBreakerDe + $getvertualStockDetail['subsource']['quantity'];
					}
					else if ( $getvertualStockDetail['subsource']['sub_source'] == 'CostBreaker_FR' )
					{
						$costBreakerFr = $costBreakerFr + $getvertualStockDetail['subsource']['quantity'];
					}
					else if ( $getvertualStockDetail['subsource']['sub_source'] == 'CostBreaker_ES' )
					{
						$costBreakerEs = $costBreakerEs + $getvertualStockDetail['subsource']['quantity'];
					}
					else if ( $getvertualStockDetail['subsource']['sub_source'] == 'CostBreaker_IT' )
					{
						$costBreakerIt = $costBreakerIt + $getvertualStockDetail['subsource']['quantity'];
					}
					else if ( $getvertualStockDetail['subsource']['sub_source'] == 'EBAY0' )
					{
						$ebuyersdirect = $ebuyersdirect + $getvertualStockDetail['subsource']['quantity'];
					}
				}
				
				if( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "single" ):
					
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
					if( $getStockDetailValue->Product->AvailableStock <= 0 ):
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );	
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, 0 );
						
						//Set highlight full row
						$objPHPExcel->getActiveSheet(0)
						->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));					
						
						//For other subsource
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );
					else:
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getStockDetailValue->Product->AvailableStock );
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $getStockDetailValue->Product->AvailableStock );							
						//For other subsource
						if( $getStockDetailValue->Product->AvailableStock == 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 1 );
						}
						else if( $getStockDetailValue->Product->AvailableStock < 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );
						}
						else
						{							
							
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getStockDetailValue->Product->AvailableStock ) + $rainbowRetail );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) + $storeForLife );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) + $rainbowRetailDe);
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) + $rainbowRetailSE);
							$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, floor($getStockDetailValue->Product->AvailableStock ) + $costBreakerUk);
							$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $costBreakerDe);
							$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $costBreakerFr);
							$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $costBreakerEs);
							$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, floor($getStockDetailValue->Product->AvailableStock / 4) + $costBreakerIt);
							$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) + $ebuyersdirect);
							
							
							/*$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );*/
						}
					endif;
										
				elseif( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "bundle" ):					
					
					if( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "single" ):
										
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
		
						$params = array(
							'conditions' => array(
								'Product.product_sku' => trim($getStockDetailValue->ProductDesc->product_defined_skus)
							),
							'fields' => array(
								//'IF(Product.current_stock_level > 2, Product.current_stock_level , 0) as FindStockThrough__LimitFactor'
								'Product.current_stock_level as FindStockThrough__LimitFactor'
							),
							'order' => 'Product.id ASC'
						);
						
						//echo $getStockDetailValue->ProductDesc->product_defined_skus .'=='. $getStockDetailValue->Product->MainSku;
						//echo "<br>";
						
						$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);
						//pr($stockData);
						
						$mainSku = explode('-',$getStockDetailValue->Product->MainSku);						
						$getDivisionNumberFromBundleSku = $mainSku[2];						
						$index = 0;
						$getAvailableStockFor_Bundle_WithSameSku = $stockData[0][$index]->FindStockThrough__LimitFactor;
						
						//Set data into file 
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );
						
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );
						
						if( $getAvailableStockFor_Bundle_WithSameSku > 2 ): 
							$getAvailableStockFor_Bundle_WithSameSku = floor($getAvailableStockFor_Bundle_WithSameSku / $getDivisionNumberFromBundleSku) - 2;
							
							/*if( $getAvailableStockFor_Bundle_WithSameSku < 2 )
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );
							else
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );*/
							
							//For other subsource
							if( $getAvailableStockFor_Bundle_WithSameSku == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );
							}
							else if( $getAvailableStockFor_Bundle_WithSameSku < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku) + $rainbowRetail);
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) + $storeForLife);
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) + $rainbowRetailDe);
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) + $$rainbowRetailSE);
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku ) + $costBreakerUk);
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku / 4) + $costBreakerDe);
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku / 4) + $costBreakerFr);
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku / 4) + $costBreakerEs);
								$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku / 4) + $costBreakerIt);
								$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku / 2) + $ebuyersdirect);
							
							}	
						else:
							
							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );

						endif;	

						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $stockData[0][$index]->FindStockThrough__LimitFactor );						
						
						
							
					elseif( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "multiple" ):
					
						//Set if different sku will manipulate over limit factor						
						/*$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );*/
						 
						$stockData = '';
						  						
						$params = array(
							'conditions' => array(
								'ProductDesc.product_defined_skus' => $getStockDetailValue->ProductDesc->product_defined_skus
							),
							'fields' => array(
								'ProductDesc.product_defined_skus as Diff_Skus',
								'Product.product_sku'
							),
							'order' => 'ProductDesc.id ASC'
						);
						
						$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);	
						//pr($stockData);
						$diff_SkuList = explode(':',trim($stockData[0]->ProductDesc->Diff_Skus));					
						
						//Get Diff Sku stock value under min section according to limit factor
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
						
						$params = array(
							'conditions' => array(
								'Product.product_sku' => $diff_SkuList
							),
							'fields' => array(
								//'IF(Product.current_stock_level > 2, (Product.current_stock_level -2) , 0) as FindStockThrough__LimitFactor'
								'Product.id',
								'Product.current_stock_level'
							),
							'order' => 'Product.id ASC'
						);
						
						//Get relevant data
						$getListStockValue = $this->Product->find( 'list' , $params );							
						$stockDataValue = min($getListStockValue);
						
						//Here we have found min lowest stock sku value tha means, need to put it into csv but keep safety margin
						if( $stockDataValue > 2 ):
							
							//If greater then will need to keep 2 for safety margin
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockDataValue );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							if( ($stockDataValue-2) == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 1 );
							}
							else if( ($stockDataValue-2) < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor(($stockDataValue-2)) + $rainbowRetail );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor(($stockDataValue-2)/2) + $storeForLife );
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, floor(($stockDataValue-2)/2) + $rainbowRetailDe);
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, floor(($stockDataValue-2)/2) + $rainbowRetailSE);
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, floor(($stockDataValue-2)) + $costBreakerUk);
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, floor(($stockDataValue-2)/4) + $costBreakerDe);
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, floor(($stockDataValue-2)/4) + $costBreakerFr);
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, floor(($stockDataValue-2)/4) + $costBreakerEs);
								$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, floor(($stockDataValue-2)/4) + $costBreakerIt);
								$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, floor(($stockDataValue-2)/2) + $ebuyersdirect);
				
							}
							
						elseif( $stockDataValue <= 2 ):
							
							//If less or equal then will need to put space / blank
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockDataValue );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, 0 );

							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));
						endif;
						
					endif;
					
				endif;
				
		  $e++;$inc++;
		  endforeach;
		  
		  //File creation 		  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->applyFromArray(
		  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->setWrapText(true);
		  $objPHPExcel->getActiveSheet(0)->setTitle('Virtual Stock');
		  $objPHPExcel->getActiveSheet(0)->getStyle("A1:E1")->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet(0)
			 ->getStyle('A1:E1')
			 ->applyFromArray(
			  array(
			   'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EBE5DB')
			   )
			  )
			 );		   
		  
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/virtualStock.csv';
		  //$uploadRemote = $getBase.'app/webroot/img/stockUpdate/virtualStock.csv';
		  $uploadRemote = $getBase.'img/stockUpdate/virtualStock.csv';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		  $objWriter->save($uploadUrl);	
		  
    return $uploadRemote;       		  
	}
	
	
	public function productVirtualStockCsvGeneration_old()
	{	 
		  $this->layout = '';
		  $this->autoRender = false;	
		  
		  $getBase = Router::url('/', true);
		  		  
		  $getStockDetail = json_decode(json_encode(ProductsController::getStockDataForCsv()),0);		 		  
		  //pr($getStockDetail); exit;
		  
		  /*
		   * 
		   * Params, To setup virtual stock for upload over linnworks where it will update on each platform, 
		   * that we are using ( Amazon, Ebay , Magento etc etc )
		   * 
		   */ 
		  
		   ob_clean();    
		   App::import('Vendor', 'PHPExcel/IOFactory');
		   App::import('Vendor', 'PHPExcel');  
		   $objPHPExcel = new PHPExcel();       
		  
		   //Column Create  
		   $objPHPExcel->setActiveSheetIndex(0);
		   $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Product Title');     
		   $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Sku');
		   $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Barcode');
		   $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Original Stock');
		   $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Rainbow Retail(Amazon)');
		   $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Storeforlife (Magento)');
		  
		   $inc = 2; $e = 0;foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue ):
				
				if( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "single" ):
					
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
					if( $getStockDetailValue->Product->AvailableStock <= 0 ):
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );	
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, 0 );
						//Set highlight full row
						$objPHPExcel->getActiveSheet(0)
						->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));					
						
						//For other subsource
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
					else:
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getStockDetailValue->Product->AvailableStock );
													
						//For other subsource
						if( $getStockDetailValue->Product->AvailableStock == 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
						}
						else if( $getStockDetailValue->Product->AvailableStock < 1 )	
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						}
						else
						{
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getStockDetailValue->Product->AvailableStock / 2) );
						}
					endif;
					
										
				elseif( strtolower( $getStockDetailValue->ProductDesc->product_type ) === "bundle" ):					
					
					if( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "single" ):
										
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
		
						$params = array(
							'conditions' => array(
								'Product.product_sku' => $getStockDetailValue->ProductDesc->product_defined_skus
							),
							'fields' => array(
								'IF(Product.current_stock_level > 2, Product.current_stock_level , 0) as FindStockThrough__LimitFactor'
							),
							'order' => 'Product.id ASC'
						);
						
						$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);
						$mainSku = explode('-',$getStockDetailValue->Product->MainSku);						
						$getDivisionNumberFromBundleSku = $mainSku[2];						
						$index = 0;
						$getAvailableStockFor_Bundle_WithSameSku = $stockData[0][$index]->FindStockThrough__LimitFactor;
						
						//Set data into file 
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );
						if( $getAvailableStockFor_Bundle_WithSameSku > 2 ): 
							$getAvailableStockFor_Bundle_WithSameSku = round($getAvailableStockFor_Bundle_WithSameSku / $getDivisionNumberFromBundleSku) - 2;
							
							if( $getAvailableStockFor_Bundle_WithSameSku < 2 )
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );
							else
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getAvailableStockFor_Bundle_WithSameSku );
							
							//For other subsource
							if( $getAvailableStockFor_Bundle_WithSameSku == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
							}
							else if( $getAvailableStockFor_Bundle_WithSameSku < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor($getAvailableStockFor_Bundle_WithSameSku/2) );
							}	
						else:
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );	
							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						endif;	
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $stockData[0][$index]->FindStockThrough__LimitFactor );						
						
						
							
					elseif( strtolower( $getStockDetailValue->ProductDesc->product_identifier ) === "multiple" ):
					
						//Set if different sku will manipulate over limit factor						
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 						
						$params = array(
							'conditions' => array(
								'ProductDesc.product_defined_skus' => $getStockDetailValue->ProductDesc->product_defined_skus
							),
							'fields' => array(
								'ProductDesc.product_defined_skus as Diff_Skus'
							),
							'order' => 'ProductDesc.id ASC'
						);
						
						$stockData = json_decode(json_encode($this->ProductDesc->find( 'all' , $params )),0);	
						$diff_SkuList = explode(':',$stockData[0]->ProductDesc->Diff_Skus);					
						
						//Get Diff Sku stock value under min section according to limit factor
						//Set if same sku is there with single-bundle type
						$this->Product->unbindModel( array(
							'hasOne' => array(
								'ProductDesc' , 'ProductPrice' 
								),
							'hasMany' => array(
								'ProductLocation'
								)		
							) 
						 );
						 
						 $this->ProductDesc->unbindModel(
							array(
								'belongsTo' => array(
									'Product'
								)
							)
						);
						
						$params = array(
							'conditions' => array(
								'Product.product_sku' => $diff_SkuList
							),
							'fields' => array(
								//'IF(Product.current_stock_level > 2, (Product.current_stock_level -2) , 0) as FindStockThrough__LimitFactor'
								'Product.id',
								'Product.current_stock_level'
							),
							'order' => 'Product.id ASC'
						);
						
						//Get relevant data
						$getListStockValue = $this->Product->find( 'list' , $params );						
						$stockDataValue = min($getListStockValue);
						
						//Here we have found min lowest stock sku value tha means, need to put it into csv but keep safety margin
						if( $stockDataValue > 2 ):
							
							//If greater then will need to keep 2 for safety margin
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, ($stockDataValue-2) );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							if( ($stockDataValue-2) == 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 1 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 1 );
							}
							else if( ($stockDataValue-2) < 1 )
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
							}
							else
							{
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, floor(($stockDataValue-2)/2) );
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, floor(($stockDataValue-2)/2) );
							}
							
						elseif( $stockDataValue <= 2 ):
							
							//If less or equal then will need to put space / blank
							//Set data into file 
							$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, addslashes($getStockDetailValue->Product->ProductTitle) );    
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, addslashes($getStockDetailValue->Product->MainSku) );    
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, addslashes($getStockDetailValue->ProductDesc->barcode) );    
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, implode( $getListStockValue , ',' ) );						
							
							//For other subsource
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0 );
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 0 );
						
							//Set highlight full row
							$objPHPExcel->getActiveSheet(0)
							->getStyle('A'.$inc.':E'.$inc)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'D83C3C'))));
						endif;
						
					endif;
					
				endif;
				
		  $e++;$inc++;
		  endforeach;
		  
		  //File creation 		  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->applyFromArray(
		  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:E1')->getAlignment()->setWrapText(true);
		  $objPHPExcel->getActiveSheet(0)->setTitle('Virtual Stock');
		  $objPHPExcel->getActiveSheet(0)->getStyle("A1:E1")->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet(0)
			 ->getStyle('A1:E1')
			 ->applyFromArray(
			  array(
			   'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EBE5DB')
			   )
			  )
			 );		   
		  
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/virtualStock.csv';
		  //$uploadRemote = $getBase.'app/webroot/img/stockUpdate/virtualStock.csv';
		  $uploadRemote = $getBase.'img/stockUpdate/virtualStock.csv';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		  $objWriter->save($uploadUrl);	
		  
    return $uploadRemote;       		  
	}
	
	
	public function updatePackagingIdByBarcodeSearchCheck_InPage()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel( 'BinLocation' );	
		$this->loadModel( 'CheckIn' );	
		
		/*pr($this->BinLocation->find('all')); exit;*/
		
		$barcode = $this->request->data['barcode'];
		//Delete Specific barcode rows
		$this->BinLocation->query( "delete from bin_locations where barcode = '{$barcode}'" );
		
		$customNewLocation = $this->request->data['customNewLocation'];
		$customStock = $this->request->data['customStock'];
		$product_id = $this->request->data['product_id'];	
		$descId = $this->request->data['id'];		
		$bin = $this->request->data['bin_sku'];
		$binAllocation = $this->request->data['binAllocation'];
		$poName = $this->request->data['poName'];
		$userId = $this->request->data['userId'];
		$pcName = $this->request->data['pcName'];
		
		$getStockSum = array_sum( explode(",",$this->request->data['getStockByLocation']) );
		
		//Unbinding Techniques 
		$this->ProductDesc->unbindModel( array( 'belongsTo' => array( 'Product' ) ) );
				
		$this->ProductDesc->updateAll(
			array(				
				'bin' => "'".$bin."'",				
			),
			array(
				'ProductDesc.id' => $descId
			)
		);
		
		//Save only
		$getLocation = explode( ',',$this->request->data['getLocation'] );
		$getStock = explode( ',',$this->request->data['getStockByLocation'] );
		
		if( count($getLocation) > 0 )
		{			
			$checkFlag = false;$customManipulation = 0;$stockCount = 0;$e = 0;while( $e < count( $getLocation ) )
			{
				if(  $getLocation[$e] != '' && $getStock[$e] != '' )
				{
					if( $getLocation[$e] == $customNewLocation )
					{
						$this->request->data['BinLocationPreffered']['BinLocation']['barcode'] = $this->request->data['barcode'];
						$this->request->data['BinLocationPreffered']['BinLocation']['bin_location'] = $getLocation[$e];
						
						if( $getStock[$e] > 0 )
							$customManipulation = $getStock[$e] + $customStock;
						else
							$customManipulation = 0 + $customStock;
						
						//$customManipulation = $getStock[$e] + $customStock;
						$this->request->data['BinLocationPreffered']['BinLocation']['stock_by_location'] = $customManipulation;
						
						$stockCount = $stockCount + $customManipulation;
						$this->BinLocation->saveAll( $this->request->data['BinLocationPreffered'] );
						$checkFlag = true;
					}
					else
					{
						$this->request->data['BinLocationPreffered']['BinLocation']['barcode'] = $this->request->data['barcode'];
						$this->request->data['BinLocationPreffered']['BinLocation']['bin_location'] = $getLocation[$e];						
						$this->request->data['BinLocationPreffered']['BinLocation']['stock_by_location'] = $getStock[$e];
						
						
						if( $getStock[$e] > 0 )
							$stockCount = $stockCount + $getStock[$e];
						else
							$stockCount = $stockCount + 0;
							
						//$stockCount = $stockCount + $getStock[$e];
						$this->BinLocation->saveAll( $this->request->data['BinLocationPreffered'] );
					}					
				}
			$e++;	
			}
		}
		
		if( $checkFlag == false )
		{
			//Extra for custom location storage
			$this->request->data['BinNewCustom']['BinLocation']['barcode'] = $this->request->data['barcode'];
			$this->request->data['BinNewCustom']['BinLocation']['bin_location'] = $customNewLocation;						
			$this->request->data['BinNewCustom']['BinLocation']['stock_by_location'] = $customStock;
			
			if( $customStock != '' || $customStock > 0 )
			{
				$stockCount = $stockCount + $customStock;
			}			
			$this->BinLocation->saveAll( $this->request->data['BinNewCustom'] );
			
			//Update Product current stock
			$this->Product->updateAll(
				array(
					'current_stock_level' => "'".$stockCount."'"
				),
				array(
					'Product.id' => $product_id
				)
			);
		}
		else
		{
			//Update Product current stock
			$this->Product->updateAll(
				array(
					'current_stock_level' => "'".$stockCount."'"
				),
				array(
					'Product.id' => $product_id
				)
			);
		}
		
		/*
		 * 
		 * CheckIn record manipulation
		 * 
		 */ 
		$checkIn['sku']				= $bin;
		$checkIn['barcode']			= $this->request->data['barcode'];
		$checkIn['qty_checkIn']		= $this->request->data['customStock'];
		$checkIn['po_name']			= $this->request->data['poName'];
		$checkIn['user_id']			= $this->request->data['userId'];
		$checkIn['pc_name']			= $this->request->data['pcName'];
		
		$this->CheckIn->saveAll( $checkIn );
		unset($checkIn);
		$checkIn = '';
		
		
		//Get All related data
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		$this->loadModel('BinLocation');
		
		/*$this->ProductDesc->bindModel( array( 
			'hasMany' => array(        
				'BinLocation' => array(									   
					'className' => 'BinLocation',
					'foreignKey' => 'barcode'									   
					)
				) 
			) 
		);*/
		
		$barcode	=	$this->request->data['barcode'];
		
		$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array('ProductDesc.barcode' => $barcode)
								)
							);
		
		$this->ProductDesc->unbindModel( array(
			'hasOne' => array(
				'Product'
			)
		) );
		
		$binLocation =	$this->BinLocation->find('all', array(
									'joins' => array(
										array(
											'table' => 'product_descs',
											'alias' => 'ProductDesc',
											'type' => 'INNER',
											'conditions' => array(
												'ProductDesc.barcode = BinLocation.barcode',
												'BinLocation.barcode ='.$barcode
											)
										)
									)
								)
							);
		
		$data['id'] 				= 	$productdeatil['ProductDesc']['id'];
		$data['stock'] 				= 	$productdeatil['Product']['current_stock_level'];
		$data['product_id'] 				= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 				= 	$productdeatil['Product']['product_name'];
		$data['sku'] 				= 	$productdeatil['Product']['product_sku'];
		$data['shortdescription'] 	= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 	= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 				= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 			= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 				= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 			= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 			= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 			= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		$data['bin_combined_text'] 			= 	$productdeatil['ProductDesc']['bin_combined_text'];
		
		//Get and set Bin Locations
		$binLocate = 0;
		foreach($binLocation as $binLocationIndex => $binLocationValue)
		{
			$binData[$binLocate]['id']					=	$binLocationValue['BinLocation']['id'];	
			$binData[$binLocate]['barcode']				=	$binLocationValue['BinLocation']['barcode'];	
			$binData[$binLocate]['bin_location']		=	$binLocationValue['BinLocation']['bin_location'];	
			$binData[$binLocate]['stock_by_location']	=	$binLocationValue['BinLocation']['stock_by_location'];	
		$binLocate++;
		}
		$data['binLocationArray'] = $binData;
		
		$imageUrl	=	Router::url('/', true).'img/product/';
		
		$i = 0;
		foreach($productdeatil['ProductImage'] as $image)
		{
			if($image['image_name'] != '')
			{
				$imagedata[$i]['imageid']		=	$image['id'];
				$imagedata[$i]['imagename']		=	$imageUrl.$image['image_name'];
				$imagedata[$i]['selectedimage']	=	$image['selected_image'];
			}
			$i++;
		}
		
		$j = 0;
		foreach( $productdeatil['ProductLocation'] as $productLocation)
		{
			$productLocation['warehouse_id'];
			$locationData[$j]['binrackid']	=	$productLocation['bin_rack_address'];
			$warehouseDetail	=	$this->Warehouse->find('first', array('conditions' => array('Warehouse.id' => $productLocation['warehouse_id'])));
			$locationData[$j]['warehousename']	=	$warehouseDetail['Warehouse']['warehouse_name'];
			$locationData[$j]['city']			=	$warehouseDetail['WarehouseDesc']['city_id'];
			$locationData[$j]['warehousetype']	=	$warehouseDetail['WarehouseDesc']['warehouse_type'];
			$j++;
		}
		$data['status'] 				= 'success';		
		echo (json_encode(array('status' => '1', 'data' => $data)));		
		exit;	 
	}
	
	
	
	
	public function updatePackagingIdByBarcodeSearchCheck_InPage_old230516()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel( 'BinLocation' );	
		
		/*pr($this->BinLocation->find('all')); exit;*/
		
		$barcode = $this->request->data['barcode'];
		//Delete Specific barcode rows
		$this->BinLocation->query( "delete from bin_locations where barcode = '{$barcode}'" );
		
		$customNewLocation = $this->request->data['customNewLocation'];
		$customStock = $this->request->data['customStock'];
		$product_id = $this->request->data['product_id'];	
		$descId = $this->request->data['id'];		
		$bin = $this->request->data['bin_sku'];
		$binAllocation = $this->request->data['binAllocation'];
		$poName = $this->request->data['poName'];
		
		//Unbinding Techniques 
		$this->ProductDesc->unbindModel( array( 'belongsTo' => array( 'Product' ) ) );
				
		$this->ProductDesc->updateAll(
			array(				
				'bin' => "'".$bin."'",				
			),
			array(
				'ProductDesc.id' => $descId
			)
		);
		
		//Save only
		$getLocation = explode( ',',$this->request->data['getLocation'] );
		$getStock = explode( ',',$this->request->data['getStockByLocation'] );
		
		if( count($getLocation) > 0 )
		{			
			$checkFlag = false;$customManipulation = 0;$stockCount = 0;$e = 0;while( $e < count( $getLocation ) )
			{
				if(  $getLocation[$e] != '' && $getStock[$e] != '' )
				{
					if( $getLocation[$e] == $customNewLocation )
					{
						$this->request->data['BinLocationPreffered']['BinLocation']['barcode'] = $this->request->data['barcode'];
						$this->request->data['BinLocationPreffered']['BinLocation']['bin_location'] = $getLocation[$e];
						
						if( $getStock[$e] > 0 )
							$customManipulation = $getStock[$e] + $customStock;
						else
							$customManipulation = 0 + $customStock;
							
						$this->request->data['BinLocationPreffered']['BinLocation']['stock_by_location'] = $customManipulation;
						
						$stockCount = $stockCount + $customManipulation;
						$this->BinLocation->saveAll( $this->request->data['BinLocationPreffered'] );
						$checkFlag = true;
					}
					else
					{
						$this->request->data['BinLocationPreffered']['BinLocation']['barcode'] = $this->request->data['barcode'];
						$this->request->data['BinLocationPreffered']['BinLocation']['bin_location'] = $getLocation[$e];						
						$this->request->data['BinLocationPreffered']['BinLocation']['stock_by_location'] = $getStock[$e];
						
						if( $getStock[$e] > 0 )
							$stockCount = $stockCount + $getStock[$e];
						else
							$stockCount = $stockCount + 0;
							
						$this->BinLocation->saveAll( $this->request->data['BinLocationPreffered'] );
					}					
				}
			$e++;	
			}
		}
		
		if( $checkFlag == false )
		{
			//Extra for custom location storage
			$this->request->data['BinNewCustom']['BinLocation']['barcode'] = $this->request->data['barcode'];
			$this->request->data['BinNewCustom']['BinLocation']['bin_location'] = $customNewLocation;						
			$this->request->data['BinNewCustom']['BinLocation']['stock_by_location'] = $customStock;
			
			if( $customStock != '' || $customStock > 0 )
			{
				$stockCount = $stockCount + $customStock;
			}			
			$this->BinLocation->saveAll( $this->request->data['BinNewCustom'] );
			
			//Update Product current stock
			$this->Product->updateAll(
				array(
					'current_stock_level' => "'".$stockCount."'"
				),
				array(
					'Product.id' => $product_id
				)
			);
		}
		else
		{
			//Update Product current stock
			$this->Product->updateAll(
				array(
					'current_stock_level' => "'".$stockCount."'"
				),
				array(
					'Product.id' => $product_id
				)
			);
		}
		
		//Get All related data
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('Warehouse');
		$this->loadModel('BinLocation');
		
		/*$this->ProductDesc->bindModel( array( 
			'hasMany' => array(        
				'BinLocation' => array(									   
					'className' => 'BinLocation',
					'foreignKey' => 'barcode'									   
					)
				) 
			) 
		);*/
		
		$barcode	=	$this->request->data['barcode'];
		
		$productdeatil	=	$this->Product->find('first', array(								
								'conditions' => array('ProductDesc.barcode' => $barcode)
								)
							);
		
		$this->ProductDesc->unbindModel( array(
			'hasOne' => array(
				'Product'
			)
		) );
		
		$binLocation =	$this->BinLocation->find('all', array(
									'joins' => array(
										array(
											'table' => 'product_descs',
											'alias' => 'ProductDesc',
											'type' => 'INNER',
											'conditions' => array(
												'ProductDesc.barcode = BinLocation.barcode',
												'BinLocation.barcode ='.$barcode
											)
										)
									)
								)
							);
		
		$data['id'] 				= 	$productdeatil['ProductDesc']['id'];
		$data['stock'] 				= 	$productdeatil['Product']['current_stock_level'];
		$data['product_id'] 				= 	$productdeatil['ProductDesc']['product_id'];
		$data['name'] 				= 	$productdeatil['Product']['product_name'];
		$data['sku'] 				= 	$productdeatil['Product']['product_sku'];
		$data['shortdescription'] 	= 	$productdeatil['ProductDesc']['short_description'];
		$data['longdescription'] 	= 	$productdeatil['ProductDesc']['long_description'];
		$data['brand'] 				= 	$productdeatil['ProductDesc']['brand'];
		$data['length'] 			= 	$productdeatil['ProductDesc']['length'];
		$data['width'] 				= 	$productdeatil['ProductDesc']['width'];
		$data['height'] 			= 	$productdeatil['ProductDesc']['height'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['weight'] 			= 	$productdeatil['ProductDesc']['weight'];
		$data['variant_envelope_name'] 			= 	$productdeatil['ProductDesc']['variant_envelope_name'];
		$data['variant_envelope_id'] 			= 	$productdeatil['ProductDesc']['variant_envelope_id'];
		$data['bin_combined_text'] 			= 	$productdeatil['ProductDesc']['bin_combined_text'];
		
		//Get and set Bin Locations
		$binLocate = 0;
		foreach($binLocation as $binLocationIndex => $binLocationValue)
		{
			$binData[$binLocate]['id']					=	$binLocationValue['BinLocation']['id'];	
			$binData[$binLocate]['barcode']				=	$binLocationValue['BinLocation']['barcode'];	
			$binData[$binLocate]['bin_location']		=	$binLocationValue['BinLocation']['bin_location'];	
			$binData[$binLocate]['stock_by_location']	=	$binLocationValue['BinLocation']['stock_by_location'];	
		$binLocate++;
		}
		$data['binLocationArray'] = $binData;
		
		$imageUrl	=	Router::url('/', true).'img/product/';
		
		$i = 0;
		foreach($productdeatil['ProductImage'] as $image)
		{
			if($image['image_name'] != '')
			{
				$imagedata[$i]['imageid']		=	$image['id'];
				$imagedata[$i]['imagename']		=	$imageUrl.$image['image_name'];
				$imagedata[$i]['selectedimage']	=	$image['selected_image'];
			}
			$i++;
		}
		
		$j = 0;
		foreach( $productdeatil['ProductLocation'] as $productLocation)
		{
			$productLocation['warehouse_id'];
			$locationData[$j]['binrackid']	=	$productLocation['bin_rack_address'];
			$warehouseDetail	=	$this->Warehouse->find('first', array('conditions' => array('Warehouse.id' => $productLocation['warehouse_id'])));
			$locationData[$j]['warehousename']	=	$warehouseDetail['Warehouse']['warehouse_name'];
			$locationData[$j]['city']			=	$warehouseDetail['WarehouseDesc']['city_id'];
			$locationData[$j]['warehousetype']	=	$warehouseDetail['WarehouseDesc']['warehouse_type'];
			$j++;
		}
		$data['status'] 				= 'success';		
		echo (json_encode(array('status' => '1', 'data' => $data)));		
		exit;	 
	}
	
        public function getSkuQuantity( $sku = null )
		{
			$this->loadModel( 'ScanOrders' );
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );
			$getSkuDetails	=	$this->ScanOrders->find( 'all', array( 'conditions' => array( 'ScanOrders.sku' => $sku, 'ScanOrders.quantity !=' => 'ScanOrders.update_quantity') ) );
			pr($getSkuDetails);
			$j = 0;
			foreach( $getSkuDetails  as $getSkuDetail )
			{
				$getmergeOrders	=	$this->MergeUpdate->find( 'all', array( 'conditions' => array( 'MergeUpdate.product_order_id_identify' =>  $getSkuDetail['ScanOrders']['split_order_id'], 'MergeUpdate.status' => '0' ) ) );
				if( count($getmergeOrders) > 0   )
				{
					$i = 0;
					foreach( $getmergeOrders as $getmergeOrder )
					{
							$getOpenOrders	=	$this->OpenOrder->find( 'first', array( 'conditions' => array( 'OpenOrder.num_order_id' =>  $getmergeOrder['MergeUpdate']['order_id'], 'OpenOrder.status' => '0'), 'fields' => array( 'OpenOrder.sub_source, OpenOrder.num_order_id' ) ) );
							if( count( $getOpenOrders ) > 0 )
							{
								$data[$j]['subsource']['quantity']		=	 	$getSkuDetail['ScanOrders']['quantity'];
								$data[$j]['subsource']['sub_source']	= 		$getOpenOrders['OpenOrder']['sub_source'];
								$data[$j]['subsource']['sku']			= 		$getSkuDetail['ScanOrders']['sku'];
								$data[$j]['subsource']['order_id']		= 		$getOpenOrders['OpenOrder']['num_order_id'];
								$i++;
							}
					}
					$j++;
				}
			}
			return $data;
		}
		
		public function getSearchResult()
		{
			
			$this->layout = '';
			$this->autoRender = false;
			$searchText		=	$this->request->data['searchKey'];			
			$this->loadModel( 'Product' );
			
			$productdeatil	=	$this->Product->find('all', array(								
									'conditions' => array(
											'OR' => array(
													array('ProductDesc.barcode' => $searchText),
													array('Product.product_sku LIKE' => '%'.$searchText.'%'),
													array('Product.product_name LIKE' => '%'.$searchText.'%'),
													array('ProductDesc.qr_code LIKE' => '%'.$searchText.'%')
											),
											'ProductDesc.product_type' => 'Single',
											'ProductDesc.product_identifier' => 'Single'
										),
									'fields' => array(
									
										'Product.id, Product.product_name, Product.product_sku, ProductDesc.barcode'
									
									)	
								)
							);
								
			//rand(10,100);
			if( count($productdeatil) == 1)
			{
				$checkarray = array( '1111111111111', '10000000000000', 'blank' );
				$barcode	=	($productdeatil[0]['ProductDesc']['barcode'] != '' ) ? $productdeatil[0]['ProductDesc']['barcode'] : 'blank';
				if( in_array($barcode, $checkarray))
				{
					$searchText = $randnum = '11'.time();
					$productData['ProductDesc']['id']			=	$productdeatil[0]['ProductDesc']['id'];
					$productData['ProductDesc']['barcode']		=	$randnum;
					$this->Product->ProductDesc->saveAll( $productData, array('validate' => false));
				}
			}
			else
			{
				
			}
			$productdeatil	=	$this->Product->find('all', array(								
									'conditions' => array(
											'OR' => array(
													array('ProductDesc.barcode' => $searchText),
													array('Product.product_sku LIKE' => '%'.$searchText.'%'),
													array('Product.product_name LIKE' => '%'.$searchText.'%'),
													array('ProductDesc.qr_code LIKE' => '%'.$searchText.'%')
											),
											'ProductDesc.product_type' => 'Single',
											'ProductDesc.product_identifier' => 'Single'
										),
									'fields' => array(
									
										'Product.id, Product.product_name, Product.product_sku, ProductDesc.barcode'
									
									)
								)
							);
			
			//check single or multiple here to render the view accordingly
			if( count( $productdeatil ) == 0 ) :

				$this->set( 'productdeatil', $productdeatil );
				$this->render( 'productSerchResult' );
			
			elseif( count( $productdeatil ) == 1 ) :
				
				$this->set( 'productdeatil', $productdeatil );
				$this->render( 'productSerchResult' );
			
			elseif( count( $productdeatil ) > 1 ) :

				$this->set( 'productdeatil', $productdeatil );
				$this->render( 'bundleList' );
			
			else :
			
				$this->set( 'productdeatil', $productdeatil );
				$this->render( 'productSerchResult' );
			
			endif;
			
		}
		
		public function mergeListRow()
		{
			
			$this->layout = "";
			$this->autoRender = false;
			
			$data['Product']['id']				= $this->request->data['id'];
			$data['Product']['product_sku']		= $this->request->data['sku'];
			$data['Product']['product_name']	= $this->request->data['title'];
			$data['Product']['barcode']			= $this->request->data['barcode'];
			
			$this->set('productdeatil' , $data );
			$this->render( 'rowMerge' );
		}
		
		public function getSearchResultOnce()
		{
			
			$this->layout = '';
			$this->autoRender = false;
			$searchText		=	$this->request->data['searchKey'];			
			$this->loadModel( 'Product' );
			
			$productdeatil	=	$this->Product->find('all', array(								
									'conditions' => array(
											'OR' => array(
													array('ProductDesc.barcode' => $searchText),
													array('Product.product_sku LIKE' => '%'.$searchText.'%'),
													array('Product.product_name LIKE' => '%'.$searchText.'%'),
													array('ProductDesc.qr_code LIKE' => '%'.$searchText.'%')
											),
											'ProductDesc.product_type' => 'Single',
											'ProductDesc.product_identifier' => 'Single'
										),
									'fields' => array(
									
										'Product.id, Product.product_name, Product.product_sku, ProductDesc.barcode'
									
									)
								)
							);
								
			//rand(10,100);
			if( count($productdeatil) == 1)
			{
				$checkarray = array( '1111111111111', '10000000000000', 'blank' );
				$barcode	=	($productdeatil[0]['ProductDesc']['barcode'] != '' ) ? $productdeatil[0]['ProductDesc']['barcode'] : 'blank';
				if( in_array($barcode, $checkarray))
				{
					$searchText = $randnum = '11'.time();
					$productData['ProductDesc']['id']			=	$productdeatil[0]['ProductDesc']['id'];
					$productData['ProductDesc']['barcode']		=	$randnum;
					$this->Product->ProductDesc->saveAll( $productData, array('validate' => false));
				}
			}
			else
			{
				
			}
			
			$productdeatil	=	$this->Product->find('all', array(								
									'conditions' => array(
											'OR' => array(
													array('ProductDesc.barcode' => $searchText),
													array('Product.product_sku LIKE' => '%'.$searchText.'%'),
													array('Product.product_name LIKE' => '%'.$searchText.'%'),
													array('ProductDesc.qr_code LIKE' => '%'.$searchText.'%')
											),
											'ProductDesc.product_type' => 'Single',
											'ProductDesc.product_identifier' => 'Single'
										),
									'fields' => array(
									
										'Product.id, Product.product_name, Product.product_sku, ProductDesc.barcode'
									
									)
								)
							);
			
			if( count( $productdeatil ) == 0 )
			{
				$data['status'] = 'error';
			}
			else if( count( $productdeatil ) == 1 )
			{
				$data['status'] = 'single';
			}
			else if( count( $productdeatil ) > 1 )
			{
				$data['status'] = 'multiple';
			}
			
		echo json_encode($data); exit;			
		}
		
		public function sellingReport()
		{
			$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'ScanOrder' );
		$toDayDate	=	date( 'Y-m-d' );
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'SKU');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Barcode');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Quantity');
		$todaysSkus	=	$this->ScanOrder->find( 'all', array( 
								'conditions' => array( 'ScanOrder.scan_date >=' =>  '2016-04-01', 'ScanOrder.scan_date <=' =>  '2016-04-15'),
								'fields' => array( 'sum(ScanOrder.quantity)   AS Quantity' , 'ScanOrder.id, ScanOrder.split_order_id, ScanOrder.sku, ScanOrder.barcode'),
								'group' => 'ScanOrder.sku',
								'order' => 'ScanOrder.quantity ASC'
								 )
							);
		$inc = 2;
		foreach( $todaysSkus as $todaysSku )
		{
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $todaysSku['ScanOrder']['sku']);     
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $todaysSku['ScanOrder']['barcode']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $todaysSku[0]['Quantity']);
			$inc++;
		}
		
		
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		$uploadUrl = WWW_ROOT .'img/daily_report/DailySellReport.csv';
		$uploadRemote = $getBase.'img/daily_report/DailySellReport.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'DailySellReport.csv';
		
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
		}
		
		
		/*
         * 
         * Custom process
         * 
         *  
			 */ 
		public function customProcess()
		{
			$this->layout = 'index';
		}
		
		/*
		 * 
		 * Params, finally bundle create
		 * 
		 * 
		 */ 
		public function createBundle()
		{
			
			/*Array
			(
				[createBundleSku] => S-018CX032E,S-0617B001
				[qtyBundle] => 1,1
				[barcodeBundle] => 734646964425,4960999273433
				[bundleCreateTitle] => Ashish
				[bundleCreatePrice] => 0.77
				[status] => true
			)*/
			
			$skuList 		= explode(',',$this->request->data['createBundleSku']);
			$barcodeList 	= explode(',',$this->request->data['barcodeBundle']);
			$qtyList 	= explode(',',$this->request->data['qtyBundle']);
			
			$createBundleSku = '';
			$createBundleBarcode = '';
			$createBundleQty = '';
			$originalSku = '';
			
			//manage sku
			for( $i = 0; $i <= count($skuList)-1 ; $i++ )
			{
				
				//bundle sku detection
				if( $i == 0 )
				{
					$cBundle = explode('-',$skuList[$i]);
					$createBundleSku = $cBundle[1];
					$originalSku = $skuList[$i];
				}
				else
				{
					$cBundle = explode('-',$skuList[$i]);
					$createBundleSku .= '-'.$cBundle[1];
					$originalSku .= ':' . $skuList[$i];
				}
				
				//bundle sku detection
				if( $i == 0 )
				{
					$createBundleBarcode = $barcodeList[$i];
				}
				else
				{
					$createBundleBarcode .= ',' . $barcodeList[$i];
				}
				
				//bundle quantity detection
				if( $i == 0 )
				{
					$createBundleQty = $qtyList[$i];
				}
				else
				{
					$createBundleQty .= ',' . $qtyList[$i];
				}
				
			}
			
			//ready bundle sku
			if( count( $skuList ) == 1 )
				$createBundleSku = 'B-' . $createBundleSku . '-2';
			else
				$createBundleSku = 'B-' . $createBundleSku . '-' . count($skuList);
			
			//other information setup
			$title = $this->request->data['bundleCreateTitle'];
			$price = $this->request->data['bundleCreatePrice'];
			
			//set identifier
			$status = $this->request->data['status'];
			
			//fields setup
			/*product_name
			product_sku
			category_id
			product_status
			all_barcodes
			date
			
			//product desc
			short_description
			long_description
			product_type
			product_defined_skus
			product_identifier*/
			
			
			$data['Product']['id'] = '';
			$data['Product']['product_name'] = $title;
			$data['Product']['product_sku_alias'] = $this->request->data['bundleCreateAlias'];
			$data['Product']['product_sku'] = $createBundleSku;
			$data['Product']['all_barcodes'] = $createBundleBarcode;
			$data['Product']['al_quantitues'] = $createBundleQty;
			$data['Product']['category_id']	= 1;
			$data['Product']['product_status'] = 1;
			$data['Product']['date'] = date( 'Y-m-d J:i:s' );
			
			$data['ProductDesc']['short_description'] = $title;
			$data['ProductDesc']['long_description'] = $title;
			$data['ProductDesc']['product_type'] = "Bundle";
			$data['ProductDesc']['product_defined_skus'] = $originalSku;
			$data['ProductDesc']['product_identifier'] = $status;
			
			$this->loadModel( 'Product' );
			if( $this->Product->saveAll( $data , array('validate'=>false) ) )
			{
				unset($data);
				$newData['status'] 	= 'sucess';
				$newData['id'] 		= $this->Product->id;
				$newData['bundle'] 		= $createBundleSku;
				echo json_encode($newData); exit; 
			}
			else
			{
				unset($data);
				$newData['status'] 	= 'sucess';
				$newData['id'] 		= '';
				echo json_encode($newData); exit; 
			}
			
		} 
		
		
		
	
}

?>
