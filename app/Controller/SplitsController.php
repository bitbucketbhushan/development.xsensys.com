<?php
error_reporting(1);
class SplitsController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Splits";
    
    var $components = array('Session','Upload','Common','Auth','Paginator');
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
    var $virtualFields;
   
   	public function index()
	{
			$this->layout = 'index';
			$this->loadModel( 'MergeUpdate' );
			
			$order_detail = array();
			if(isset($_REQUEST['searchkey'])){ 
				$order_detail	=	$this->MergeUpdate->find( 'all', array( 'conditions' => array( 'MergeUpdate.order_id' => $_REQUEST['searchkey'] ) ) );
			}
			
			$this->set('order_detail', $order_detail);
	} 
	 
	public function VerifyOrder()
	{
			$this->layout = 'index';
			$this->loadModel( 'Product' );
			$this->loadModel( 'MergeUpdate' );
			$html = '';
			$order_detail = array();   
			if($this->request->data['sku'][0] !='' && $this->request->data['sku'][1] !=''){
			
 				if(isset($_REQUEST['searchkey']))
				{ 
					$order_detail = $this->MergeUpdate->find( 'first', array( 'conditions' => array( 'MergeUpdate.order_id' => $_REQUEST['searchkey'] ) ) );
				}
				
				if(count($order_detail) > 0){
				
				$unit_price = $order_detail['MergeUpdate']['price'] / $order_detail['MergeUpdate']['quantity'];
				
				$html =	'<div class="row">
					<div class="col-lg-12 head-row">	 
						<div class="col-sm-1">ORDER ID</div>
						<div class="col-sm-1">SOURCE</div>
						<div class="col-sm-1">COUNTRY</div>
						<div class="col-sm-2">SKU</div>
						<div class="col-sm-1">PRICE</div>
						<div class="col-12">POSTAL PROVIDER</div>
						<div class="col-10">SERVICE</div>
						<div class="col-sm-1">WEIGHT</div>
						<div class="col-14">DIMENTIONS(LxWxH)</div>
					</div>
				  </div>';
				
 				foreach($this->request->data['sku'] as $count => $data)
				{ 
					$height = [];
					$weight = [];
					$price  = []; 
					$final_skus = [];
					$skus = explode(',', $data);
  					
					if(count($skus) > 1){
						foreach($skus as $k => $v){
							$x = explode('XS-', $v); 
							if(count($x) > 1){
								$qty = $x[0];
								$sku = 'S-'.$x[1];	
								$detail	= $this->Product->find( 'first', array( 'conditions' => ['product_sku' => $sku],'fields'=>['ProductDesc.barcode','ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','ProductPrice.product_price'] ) );
								if(count($detail) > 0){			
									$length = $detail['ProductDesc']['length'] ;
									$width = $detail['ProductDesc']['width'] ;
									$height[] = $detail['ProductDesc']['height'] * $qty;
									$weight[] = $detail['ProductDesc']['weight'] * $qty ;
									$final_skus[] = $qty . 'X' . $sku;
									$price[] = $unit_price * $qty;	
								}else{
									$msg['html'] =	'<div class="alert alert-danger"> <strong>'.$sku.'</strong> SKU not found.</div> ';
								}
							}
						}
					}else {
							$x = explode('XS-', $data); 
							if(count($x) > 1){
								$qty = $x[0];
								$sku = 'S-'.$x[1];	
								$detail	= $this->Product->find( 'first', array( 'conditions' => ['product_sku' => $sku],'fields'=>['ProductDesc.barcode','ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','ProductPrice.product_price'] ) );	
								if(count($detail) > 0){							 
									$length = $detail['ProductDesc']['length'] ;
									$width = $detail['ProductDesc']['width'] ;
									$height[] = $detail['ProductDesc']['height'] * $qty;
									$weight[] = $detail['ProductDesc']['weight'] * $qty;
								
									$final_skus[] = $qty . 'X' . $sku;	
									$price[] = $unit_price * $qty;	
								}else{
									$msg['html'] =	'<div class="alert alert-danger"> <strong>'.$sku.'</strong> SKU not found.</div> ';
								}						
							}						 
					} 
							 
					$_height = array_sum($height);
					$_weight = array_sum($weight);
					
					$product_order_id_identify = $this->request->data['searchkey'] .'-'.($count+1);
					
					$html .='<div class="row"><div class="col-lg-12 normal-row">';
						 
					$html .='<div class="col-sm-1">'.$product_order_id_identify.'</div>';
					$html .='<div class="col-sm-1">'.$order_detail['MergeUpdate']['source_coming'].'</div>';
 					$html .='<div class="col-sm-1">'.$order_detail['MergeUpdate']['delevery_country'].'</div>';
					$html .='<div class="col-sm-2">'.implode(",",$final_skus).'</div>';
					$html .='<div class="col-sm-1">'.number_format(array_sum($price),2).'</div>';
					$html .='<div class="col-12">'.$order_detail['MergeUpdate']['service_provider'].'('.$order_detail['MergeUpdate']['postal_service'].')</div>';
 					$html .='<div class="col-10">'.($order_detail['MergeUpdate']['service_name'] ? $order_detail['MergeUpdate']['service_name'] : '-') .'</div>';
					$html .='<div class="col-sm-1">'.number_format($_weight,2).'</div>';
					$html .='<div class="col-14">L:'.number_format($length,2).' x W:'.number_format($width,2).' x H:'.number_format($_height,2).'</div>';
 					$html .= '</div></div>';
 					 
				}
				$html .='<div class="row"><div class="col-lg-12 normal-row">';
				$html .='<div class="col-sm-2"><button type="button" class="btn btn-warning"  onClick="SaveSubOrder();">Save Order</button></div><div class="col-sm-4" id="msg"></div>';
				$html .= '</div></div>';
				$msg['html'] = $html;
			}
			}else{
 				$msg['html'] =	'<div class="alert alert-danger"> <strong>Alert!</strong> Please enter SKUs.</div> ';
			}
			
			echo json_encode($msg);
			exit;
	} 
	
	public function SaveSubOrder() 
	{
			$this->layout = 'index';
			$this->loadModel( 'Product' );
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OrderLocation' );
			
			$_msg = $error_msg = '';
			$order_detail = array();  
			$log_file = WWW_ROOT .'logs/order_split_'.date('Ymd').".log"; 
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$_username = $firstName.' '.$lastName;
									
			if($this->request->data['sku'][0] !='' && $this->request->data['sku'][1] !=''){
			
 				if(isset($_REQUEST['searchkey']))
				{ 
					$order_detail = $this->MergeUpdate->find( 'first', array( 'conditions' => array( 'MergeUpdate.order_id' => $_REQUEST['searchkey'] ) ) );
				}
				
				if(count($order_detail) > 0){
 					$unit_price = $order_detail['MergeUpdate']['price'] / $order_detail['MergeUpdate']['quantity'];
			 
					foreach($this->request->data['sku'] as $count => $data)
					{ 
							$height = [];
							$weight = [];
							$price  = []; 
							$final_qty = [];
							$final_skus = [];
							$final_barcode = [];
							$sku_barcodes = [];
							$skus = explode(',', $data);
							
							if(count($skus) > 1){
								foreach($skus as $k => $v){
									$x = explode('XS-', $v); 
									if(count($x) > 1){
										$qty = $x[0];
										$sku = 'S-'.$x[1];	
										$detail	= $this->Product->find( 'first', array( 'conditions' => ['product_sku' => $sku],'fields'=>['ProductDesc.barcode','ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','ProductPrice.product_price'] ) );
										if(count($detail) > 0){	
											$length = $detail['ProductDesc']['length'] ;
											$width = $detail['ProductDesc']['width'] ;
											$height[] = $detail['ProductDesc']['height'] * $qty;
											$weight[] = $detail['ProductDesc']['weight'] * $qty ;
											$final_skus[] = $qty . 'X' . $sku;
											$price[] = $unit_price * $qty;	
											$final_qty[] = $qty;
											$final_barcode[] = $detail['ProductDesc']['barcode'];
											$sku_barcodes[$sku] = $detail['ProductDesc']['barcode'];
										}else{
											 $error_msg = $sku.' SKU not found.';
										}
									}
								}
							}else {
									$x = explode('XS-', $data); 
									if(count($x) > 1){
										$qty = $x[0];
										$sku = 'S-'.$x[1];	
										$detail	= $this->Product->find( 'first', array( 'conditions' => ['product_sku' => $sku],'fields'=>['ProductDesc.barcode','ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','ProductPrice.product_price'] ) );								 
										if(count($detail) > 0){	
											$length = $detail['ProductDesc']['length'] ;
											$width = $detail['ProductDesc']['width'] ;
											$height[] = $detail['ProductDesc']['height'] * $qty;
											$weight[] = $detail['ProductDesc']['weight'] * $qty;
											$final_skus[] = $qty . 'X' . $sku;	
											$price[] = $unit_price * $qty;	
											$final_qty[] = $qty;	
											$final_barcode[] = $detail['ProductDesc']['barcode'];
											$sku_barcodes[$sku] = $detail['ProductDesc']['barcode'];	
										}else{
											 $error_msg = $sku.' SKU not found.';
										}				
									}						 
							} 
							if($error_msg == ''){		 
								$_height = array_sum($height);
								$_weight = array_sum($weight);
								
								$product_order_id_identify = $this->request->data['searchkey'] .'-'.($count+1);
								$dataUpdate = [];
								unset($order_detail['MergeUpdate']['id']);
								$dataUpdate = $order_detail;
								
								$_detail = $this->MergeUpdate->find( 'first', array( 'conditions' => array( 'MergeUpdate.product_order_id_identify' => $product_order_id_identify) ) );
								
								if(count($_detail) > 0){
									$dataUpdate['MergeUpdate']['id'] = $_detail['MergeUpdate']['id'];
								}
								
								$dataUpdate['MergeUpdate']['product_order_id_identify'] = $product_order_id_identify;				 
								$dataUpdate['MergeUpdate']['order_barcode_image'] 		= $product_order_id_identify.".png";
								$dataUpdate['MergeUpdate']['sku'] 						= implode(",",$final_skus);				 
								$dataUpdate['MergeUpdate']['price']						= number_format(array_sum($price),2);
								$dataUpdate['MergeUpdate']['quantity'] 					= array_sum($final_qty);				
								$dataUpdate['MergeUpdate']['barcode'] 					= implode(",",$final_barcode);
								$dataUpdate['MergeUpdate']['packet_weight']    			= number_format($_weight,2);
								$dataUpdate['MergeUpdate']['packet_length']    			= number_format($length,2);
								$dataUpdate['MergeUpdate']['packet_width']    			= number_format($width,2);
								$dataUpdate['MergeUpdate']['packet_height']    			= number_format($_height,2);
								if($count > 0) {
									$dataUpdate['MergeUpdate']['label_status'] = 0;
								}
															 
								$this->MergeUpdate->saveAll( $dataUpdate ); 
								//pr($final_skus);
								foreach($final_skus as $sk){
								$t = explode("XS-",$sk);
								$s_sku = "S-".$t[1]; 
								$s_qty = $t[0];
								$_location = $this->OrderLocation->find('first', array('conditions' => array('order_id' => $dataUpdate['MergeUpdate']['order_id'],'sku' => $s_sku,'status' => 'active') ) );
								
								if(count($_location) > 0){
									 
									$olUpdate = [];
									
									$ord_location = $this->OrderLocation->find('first', array('conditions' => array('order_id' => $dataUpdate['MergeUpdate']['order_id'],'split_order_id'=>$product_order_id_identify,'sku' => $s_sku,'status' => 'active') ) );
									
									if(count($ord_location) > 0){										
										if($product_order_id_identify == $ord_location['OrderLocation']['split_order_id'] ){
											$olUpdate['OrderLocation']['id'] = $ord_location['OrderLocation']['id'];
										}
									}else{
										$ord_location = $_location;
									}
									
									$olUpdate['OrderLocation']['order_id'] 				= $dataUpdate['MergeUpdate']['order_id'];				 
									$olUpdate['OrderLocation']['split_order_id'] 		= $dataUpdate['MergeUpdate']['product_order_id_identify'];
									$olUpdate['OrderLocation']['sku'] 					= $s_sku;				 
									$olUpdate['OrderLocation']['barcode'] 				= $sku_barcodes[$s_sku];
									$olUpdate['OrderLocation']['bin_location']    		= $ord_location['OrderLocation']['bin_location'];
									$olUpdate['OrderLocation']['available_qty_check_in']= $s_qty;
									$olUpdate['OrderLocation']['available_qty_bin']    	= $s_qty;
									$olUpdate['OrderLocation']['quantity'] 				= $s_qty;	
									$olUpdate['OrderLocation']['po_name']    			= $ord_location['OrderLocation']['po_name'];
									$olUpdate['OrderLocation']['po_id']    				= $ord_location['OrderLocation']['po_id'];
									$olUpdate['OrderLocation']['pick_list']    			= $ord_location['OrderLocation']['pick_list'];
									$olUpdate['OrderLocation']['batch']    				= $ord_location['OrderLocation']['batch'];							
									$olUpdate['OrderLocation']['picklist_inc_id']    	= $ord_location['OrderLocation']['picklist_inc_id'];
									$olUpdate['OrderLocation']['picked_date']    		= $ord_location['OrderLocation']['picked_date']; 
									$olUpdate['OrderLocation']['data_con']    			= $ord_location['OrderLocation']['data_con'];
		
									$this->OrderLocation->saveAll( $olUpdate ); 
 									
 									file_put_contents($log_file, $this->request->data['searchkey']."\t".$dataUpdate['MergeUpdate']['product_order_id_identify']."\t".$_username."\t".date('Y-m-d H:i:s')."\n",  FILE_APPEND | LOCK_EX);	
									$_msg = 'Order Splitted Successfully.';
								 
							}
							 
							}
						}
						
					 }
			   }else{
			  	 $error_msg = 'Order not found.';
			   }
			}else{
 				 $error_msg = 'Please enter SKUs.';
 			}
			
			if($error_msg != ''){
			 	$msg['msg'] = '<div class="alert alert-danger"><strong>Alert!</strong> '.$error_msg.'</div> ';
				$msg['status'] = 2;
				file_put_contents($log_file, $this->request->data['searchkey']."\t".$error_msg."\t".$_username."\t".date('Y-m-d H:i:s')."\n",  FILE_APPEND | LOCK_EX);	
			}else if($_msg != ''){
			 	$msg['msg'] = '<div class="alert alert-success"><strong>Success!</strong> '.$_msg.'</div> ';
				$msg['status'] = 1;
				file_put_contents($log_file, $this->request->data['searchkey']."\t".$_msg."\t".$_username."\t".date('Y-m-d H:i:s')."\n",  FILE_APPEND | LOCK_EX);	
			} 
  				 
 			echo json_encode($msg);
			exit;
	}
   
    
    public function order_location($data = array(),$split_order_id = null){
		
		$this->loadModel( 'OrderLocation' );
		$this->loadModel( 'BinLocation' );		
		$this->loadModel( 'CheckIn' );	
		 
		if(count($data) > 0){
				
			$quantity = $data['quantity'];					
			$sku      = $data['sku'];
			$barcode  = $data['barcode'];
			$order_id = $data['split_order_id']; 
			$data_con = 1;
			if(isset($data['data_con'])){
				$data_con = $data['data_con']; 
			}
					
			$po_name = array(); $posIds = array(); $pos = array(); $binname = array(); 
			$available_qty = 0; $available_qty_bin = 0;	
			
			$local_barcode = $this->Components->load('Common')->getLocalBarcode($barcode);
			
			$poDetail = $this->CheckIn->find( 'all', 
				array( 'conditions' => array( 'CheckIn.barcode IN' => array($barcode,$local_barcode), 'CheckIn.selling_qty != CheckIn.qty_checkIn','po_name !=""'),
						'order' => 'CheckIn.date  ASC ' ) );
			
			/*$poDetail = $this->CheckIn->find( 'all', 
				array( 'conditions' => array( 'CheckIn.barcode' => $barcode, 'CheckIn.selling_qty != CheckIn.qty_checkIn','po_name !=""'),
						'order' => 'CheckIn.date  ASC ' ) );*/
			
			if(count($poDetail) > 0){
				$qt_count = 0; 				
				foreach($poDetail as $po){
					$poQty = $po['CheckIn']['qty_checkIn'] - $po['CheckIn']['selling_qty'];
					if($poQty > 0){						
						 for($i=0; $i <= $quantity ;$i++){						 
							 if($qt_count >= $quantity ){
								 break;
							 }else if(($poQty - $i)  > 0){							
								$po_name[$po['CheckIn']['id']][] = $po['CheckIn']['po_name'];	
								$available_qty++;							
								$qt_count++;
							}												
						 }						
					}
				}
			}
			 
			
			$getStock = $this->BinLocation->find( 'all', array( 'conditions' => array( 'BinLocation.barcode' => $barcode),'order' => 'priority ASC' ) );
			$bin_name = array(); $finalData['bin_location'] = "";
			if(count($getStock) > 0){
				
				$qt_count = 0 ;
				foreach($getStock as $val){
					//if($val['BinLocation']['stock_by_location'] >= $quantity ) {
						$binData['id'] = $val['BinLocation']['id'];					
						 for($i=0; $i <= $quantity ;$i++){						 
							 if($qt_count >= $quantity ){
								 break;
							 }else if(($val['BinLocation']['stock_by_location'] - $i)  > 0){							
								$bin_name[$val['BinLocation']['id']][] = $val['BinLocation']['bin_location'];	
								$available_qty_bin++;						
								$qt_count++;
							}												
						 }						 
						//break;
					//}
				}				
				
			}
			
			//$ol = $this->OrderLocation->find( 'first', array( 'conditions' => array( 'order_id' => $order_id,'sku' => $sku)));							
						
			//if(count($ol) == 0){
				if(count($po_name) > 0){
					foreach(array_keys($po_name) as $k){
						$qts   = count($po_name[$k]);	
						$pos[] = $po_name[$k][0];
						$posIds[] = $k;
						$this->CheckIn->updateAll( array('CheckIn.selling_qty' =>  "CheckIn.selling_qty + $qts"),array('CheckIn.id' => $k));
					}
				}
				
				if(count($bin_name) > 0)
				{
					foreach(array_keys($bin_name) as $k){					
						$qts = count($bin_name[$k]);	
						$binname[] = $bin_name[$k][0];	
						
						$finalData['available_qty_check_in'] = $available_qty;
						$finalData['available_qty_bin'] = $qts;
						$finalData['quantity']      	= $quantity;
						$finalData['order_id']			= $order_id;	
						$finalData['split_order_id']	= $split_order_id;					
						$finalData['sku']	   			= $sku;								
						$finalData['barcode']  			= $barcode;	
						$finalData['po_name']  			= implode(",",$pos);	
						$finalData['po_id']    			= implode(",",$posIds);	
						$finalData['bin_location']  	= $bin_name[$k][0];	
						$finalData['data_con']  		= $data_con;					
						$this->OrderLocation->saveAll( $finalData );
						
						$this->BinLocation->updateAll( array('BinLocation.stock_by_location' =>  "BinLocation.stock_by_location - $qts"),array('BinLocation.id' => $k));
					}
				} else {
					
						$finalData['available_qty_check_in'] = $available_qty;
						$finalData['available_qty_bin'] = '0';
						$finalData['quantity']      	= $quantity;
						$finalData['order_id']			= $order_id;
						$finalData['split_order_id']	= $split_order_id;						
						$finalData['sku']	   			= $sku;								
						$finalData['barcode']  			= $barcode;	
						$finalData['po_name']  			= implode(",",$pos);	
						$finalData['po_id']    			= implode(",",$posIds);	
						$finalData['bin_location']  	= 'No Location';
						$finalData['data_con']  		= $data_con;					
						$this->OrderLocation->saveAll( $finalData );
				}	
			//}
			
		}						
	}
    
}
?>
