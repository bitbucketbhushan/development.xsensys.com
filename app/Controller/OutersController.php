<?php

class OutersController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Outers";
    
    var $components = array('Session','Upload','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
    var $virtualFields;
    
    public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('sendVirtualStockFile'));
    }
    
    public function getReconcilationReport()
    {
		
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'Product' );
		$this->loadModel( 'BinLocation' );
		date_default_timezone_set('Europe/Jersey');
		
		/*************************************************************************************************/
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'SKU');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Barcode');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Location');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Stock');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Location Quantity');
		
		$skulocate = $this->Product->find('all');
		
		$inc = 2;
		foreach( $skulocate as $skulocateIndex => $skulocateValue )
		{
			
			$barcode	= $skulocateValue['ProductDesc']['barcode'];
			
			$getBinLocations		=	$this->BinLocation->find( 'all', array( 'conditions' => array('BinLocation.barcode' =>  $barcode )) );
			$totalLocationQty = 0;
			foreach( $getBinLocations as $getBinLocation )
			{
				
				$totalLocationQty	=	$totalLocationQty + $getBinLocation['BinLocation']['stock_by_location'];
			}
			
			//$getProductDescs	=	$this->Product->find( 'first', array( 'conditions' => array( 'ProductDesc.barcode' => $barcode ) ) );
			$stockLevel			=	$skulocateValue['Product']['current_stock_level'];
			
			//if( $stockLevel  $totalLocationQty )
			
			/*if(1)
			{
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $skulocateValue['Product']['product_sku'] );
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $barcode );
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getBinLocation['BinLocation']['bin_location']);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockLevel );
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $totalLocationQty );
				$inc++;
			}*/
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $skulocateValue['Product']['product_sku'] );
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $barcode );
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getBinLocation['BinLocation']['bin_location']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $stockLevel );
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $totalLocationQty );
			$inc++;
			
		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/checkinventery/checkinventery.csv';
		$uploadRemote = $getBase.'img/checkinventery/checkinventery.csv';
		$path = $getBase.'img/checkinventery/';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		//$recipient_email = 'amit.gaur@jijgroup.com , ashish.gupta@jijgroup.com amit.gaur@jijgroup.com , ashish.gupta@jijgroup.com , lalit.prasad@jijgroup.com , jake.shaw@euracogroup.co.uk , shashi.b.kumar@jijgroup.com'; //recipient email
		$subject = 'Recancilation Report'; //subject of email     
		$user_email = '';
		
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('');
		$email->from('webmaster@xsensys.com');
		//$email->to( array('lalit.prasad@jijgroup.com','jake.shaw@euracogroup.co.uk' , 'shashi.b.kumar@jijgroup.com' ));
		$email->to();
		$email->subject('Recancilation Report');
		$getBase = Router::url('/', true);
		$email->attachments(array(
			'checkinventery.csv' => array(
				'file' => $uploadUrl,
				'mimetype' => 'text/csv',
				'contentId' => 'Euraco'
			)
		));
		$email->send( 'Please get attachment file.' );
		$this->redirect( Router::url( $this->referer(), true ) );
		exit;
		
		
		/**************************************************************************************************/		
		
	}
    
    public function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message)
	{
		$file = $path.$filename;
		$file_size = filesize($file);
		$handle = fopen($file, "r");
		$content = fread($handle, $file_size);
		fclose($handle);
		$content = chunk_split(base64_encode($content));
		$uid = md5(uniqid(time()));
		$header = "From: ".$from_name." <".$from_mail.">\r\n";
		$header .= "Reply-To: ".$replyto."\r\n";
		$header .= "MIME-Version: 1.0\r\n";
		$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
		$header .= "This is a multi-part message in MIME format.\r\n";
		$header .= "--".$uid."\r\n";
		$header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
		$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		$header .= $message."\r\n\r\n";
		$header .= "--".$uid."\r\n";
		$header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
		$header .= "Content-Transfer-Encoding: base64\r\n";
		$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
		$header .= $content."\r\n\r\n";
		$header .= "--".$uid."--";
		if (mail($mailto, $subject, "", $header))
		{
			echo "mail send ... OK"; // or use booleans here
		}
		else
		{
			echo "mail send ... ERROR!";
		}
	}
    
	/*
	 * 
	 * Params, to send the mail to someone who will check the status of stock and manipulate accordignly
	 * cheers!
	 * 
	 */ 	
	 public function sendVirtualStockFile()
    {
		
			$this->layout = '';
			$this->autoRender = false;
			date_default_timezone_set('Europe/Jersey');
			// send email
			App::uses('CakeEmail', 'Network/Email');
			$email = new CakeEmail('');
			$email->from('info@xsensys.com');
			$email->to( array('lalitjij014@gmail.com'));
			$email->subject('Virtual Stock Status');
			$getBase = Router::url('/', true);
			$uploadRemote = WWW_ROOT.'img/stockUpdate/virtualStock.csv';

			$email->attachments(array(
				'virtualStock.csv' => array(
					'file' => $uploadRemote,
					'mimetype' => 'text/csv',
					'contentId' => 'Euraco'
				)
			));
			$email->send( 'Please get attachment file.' );
			exit;
	}
	
	
	/*
	 * 
	 * Params, Get and set for delete and cancel order where need to specify, actual what they did after perform delete / cancel
	 * 
	 * 
	 */ 
	 
	public function getAll_Delete_Cancel()
	{
		
		$this->layout = 'index';
		$this->autoRender = false;
		
		$this->loadModel( 'PutbackItem' );	
		$getDeleteCancel	= $this->PutbackItem->find('all');
		$this->set('getDeleteCancel', $getDeleteCancel);
		$this->render( '/PutbackItems/showall' );
		
	}	
	
	
	//verify items
	public function verifyItem( $id )
	{
		
		
		$this->layout = 'index';
		$this->autoRender = false;
		
		$this->loadModel( 'PutbackItem' );	
		$this->PutbackItem->delete( $id );
		$this->Session->setFlash('Item Verified successfully.', 'flash_success');
		$this->redirect( Router::url( $this->referer(), true ) );
		
	}
	
	public function getStockData()
	{
		 $this->loadModel('Product');
		 $this->loadModel('ProductDesc');
		 
		 $stockData = $this->Product->find( 'all' , 
                                array( 
                                    'fields' => array( 
                                        'DISTINCT Product.product_sku',
                                        'Product.*',
                                        'ProductDesc.*'
                                    ),
                                    'order' => 'Product.current_stock_level DESC' 
                                )
                 );                 
	return $stockData;	 
	}
	
	public function getStock_ActualReport_old()
	{	 
			//chr(046)
		  $this->layout = '';
		  $this->autoRender = false;	
		  		  
		  $getStockDetail = OutersController::getStockData();		 
		  ob_clean();    
		  App::import('Vendor', 'PHPExcel/IOFactory');
		  App::import('Vendor', 'PHPExcel');  
		  $objPHPExcel = new PHPExcel();       
		  
		  //Column Create  
		  $objPHPExcel->setActiveSheetIndex(0);
		  $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Product Name');     
		  $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Sku');
		  $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Barcode');
		  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Avb. Stock');
		  
		  /*$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Status'); */
		  //pr( $getStockDetail );
		  //exit;
		  $inc = 2; foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue ):
		  
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getStockDetailValue['Product']['product_name'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $getStockDetailValue['Product']['product_sku'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getStockDetailValue['ProductDesc']['barcode'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getStockDetailValue['Product']['current_stock_level'] );     
				
				/* Get all bin location */ 
				$this->loadModel( 'BinLocation' );
				$params = array(
					'conditions' => array(
						'BinLocation.barcode' =>  $getStockDetailValue['ProductDesc']['barcode']
					)
				);
				
				$binLocation = json_decode(json_encode($this->BinLocation->find( 'all' , $params )),0);
				
				$ch_label = 69; // 78 // 80
				$ch_value = 70; // 79 // 81
				
				$incVal = 1;
				$alphabet = array( 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
									'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN');
				//pr( $alphabet );
				//exit;
				
				
				if( count($binLocation) > 0 )
				{
					//pr($binLocation);
					$i = 0;
					foreach( $binLocation as $binLocationIndex => $binLocationValue )
					{
						$objPHPExcel->getActiveSheet()->setCellValue($alphabet[$i].'1', 'Location'.$incVal);
						$objPHPExcel->getActiveSheet()->setCellValue( $alphabet[$i].$inc , $binLocationValue->BinLocation->bin_location );
						$i++;
						//Stock per location
						$objPHPExcel->getActiveSheet()->setCellValue($alphabet[$i].'1', 'Stock');
						$objPHPExcel->getActiveSheet()->setCellValue( $alphabet[$i].$inc, $binLocationValue->BinLocation->stock_by_location );
							
						$ch_label += 2;
						$ch_value += 2;
						$incVal++;
						
						$i++;
					}
				}
				/*else
				{
					foreach( $binLocation as $binLocationIndex => $binLocationValue )
					{
						
						$newCh = sprintf("%c",$ch_label); 						
						$newCh1 = sprintf("%c",$ch_value); 
						
						//Location
						$objPHPExcel->getActiveSheet()->setCellValue($newCh.'1', 'Location'.$incVal);
						$objPHPExcel->getActiveSheet()->setCellValue($newCh.$inc, $binLocationValue->BinLocation->bin_location );
						
						//Stock per location
						$objPHPExcel->getActiveSheet()->setCellValue($newCh1.'1', 'Stock');
						$objPHPExcel->getActiveSheet()->setCellValue($newCh1.$inc, $binLocationValue->BinLocation->stock_by_location );
							
						$ch_label += 2;
						$ch_value += 2;
						$incVal++;
						
						
					}
				}*/
				
				$totalStock = 0;
						
				  
		  $inc++;
		  endforeach;   
		  
		  //Set First Row for Range Analysis Sheet 
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:Q1')->getAlignment()->applyFromArray(
		  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:Q1')->getAlignment()->setWrapText(true);
		  $objPHPExcel->getActiveSheet(0)->setTitle('Current Stock');
		  $objPHPExcel->getActiveSheet(0)->getStyle("A1:Q1")->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet(0)
			 ->getStyle('A1:Q1')
			 ->applyFromArray(
			  array(
			   'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EBE5DB')
			   )
			  )
			 );		   
		  $getBase = Router::url('/', true);
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/stockUpdate1.csv';
		  $uploadRemote = $getBase.'app/webroot/img/stockUpdate/stockUpdate1.csv';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		  $objWriter->save($uploadUrl);		 
		   
	//return $uploadRemote;	  
	}
	
	
	
	public function getStock_ActualReport()
	{	 
			//chr(046)
		  $this->layout = '';
		  $this->autoRender = false;	
		  		  
		  $getStockDetail = OutersController::getStockData();		 
		  //ob_clean();    
		  App::import('Vendor', 'PHPExcel/IOFactory');
		  App::import('Vendor', 'PHPExcel');  
		  $objPHPExcel = new PHPExcel();       
		  
		  //Column Create  
		  $objPHPExcel->setActiveSheetIndex(0);
		  $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Product Name');     
		  $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Product Sku');
		  $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Barcode');
		  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Avb. Stock');
		  
		  /*$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Status'); */
		  //pr( $getStockDetail );
		  //exit;
		  $inc = 2; foreach( $getStockDetail as $getStockDetailIndex => $getStockDetailValue ):
		  
				/*$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getStockDetailValue['Product']['product_name'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $getStockDetailValue['Product']['product_sku'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getStockDetailValue['ProductDesc']['barcode'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $getStockDetailValue['Product']['current_stock_level'] );     
				*/
				
				
				
				/* Get all bin location */ 
				$this->loadModel( 'BinLocation' );
				$params = array(
					'conditions' => array(
						'BinLocation.barcode' =>  $getStockDetailValue['ProductDesc']['barcode']
					)
				);
				
				$binLocation = json_decode(json_encode($this->BinLocation->find( 'all' , $params )),0);
				
				$ch_label = 69; // 78 // 80
				$ch_value = 70; // 79 // 81
				
				$incVal = 1;
				$alphabet = array( 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
									'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN');
				//pr( $alphabet );
				//exit;
				
				
				if( count($binLocation) > 0 )
				{
					$data['product_sku'] = $getStockDetailValue['Product']['product_sku'];
					$data['barcode'] 	 = $getStockDetailValue['ProductDesc']['barcode'];
					$data['product_name'] 	 = $getStockDetailValue['Product']['product_name'];
					$data['current_stock_level'] 	 = $getStockDetailValue['Product']['current_stock_level'];
					//pr($binLocation);
					$i = 0;
					foreach( $binLocation as $binLocationIndex => $binLocationValue )
					{
						
						$data['location'][$i]['bin_location']		=	$binLocationValue->BinLocation->bin_location;
						$data['location'][$i]['stock_by_location']	=	$binLocationValue->BinLocation->stock_by_location;
						$i++;
					}
					
					
					$jk = 0;while( $jk <= count($data['location'])-1 )
						{
							$binLocate = $data['location'][$jk]['bin_location'];
							$binExplode = explode( ',', $binLocate );
							$binText = $binExplode[0];			
							$skulocate['loc'][$jk] = $binText;
							$skulocate['sku'] 		= $data['product_sku'];
							$skulocate['barcode'] 	= $data['barcode'];
							$skulocate['product_name'] 	= $data['product_name'];
							$skulocate['current_stock_level'] 	= $data['current_stock_level'];
							$jk++;	
						}
						
					natsort( $skulocate['loc'] );
					$location[] = $skulocate;
					unset( $skulocate );
					unset($data);
					$data = '';
					
				}
						
				  
		  $inc++;
		  endforeach;
		  $alphabet = array( 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
									'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN');
	      $incVal = 1;
	      $inc = 2;
		 foreach( $location as $locationValue )
		 {
			// pr( $locationValue );
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $locationValue['sku'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $locationValue['barcode'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $locationValue['product_name'] );     
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $locationValue['current_stock_level'] );     
				
			 $i = 0;
			 foreach( $locationValue['loc'] as $value )
			 {
						$stockByLocation 	=	$this->getStock( $locationValue['barcode'], $value ); 
						$objPHPExcel->getActiveSheet()->setCellValue($alphabet[$i].'1', 'Location'.$incVal);
						$objPHPExcel->getActiveSheet()->setCellValue( $alphabet[$i].$inc , $value );
						$i++;
						//Stock per location
						$objPHPExcel->getActiveSheet()->setCellValue($alphabet[$i].'1', 'Stock');
						$objPHPExcel->getActiveSheet()->setCellValue($alphabet[$i].$inc, $stockByLocation['BinLocation']['stock_by_location'] );
							
						$ch_label += 2;
						$ch_value += 2;
						$incVal++;
						$i++;
				 
			 }
			  $inc++;
		 }
		 
		  
		  
		  
		  //Set First Row for Range Analysis Sheet 
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:Q1')->getAlignment()->applyFromArray(
		  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));  
		  $objPHPExcel->getActiveSheet(0)->getStyle('A1:Q1')->getAlignment()->setWrapText(true);
		  $objPHPExcel->getActiveSheet(0)->setTitle('Current Stock');
		  $objPHPExcel->getActiveSheet(0)->getStyle("A1:Q1")->getFont()->setBold(true);
		  $objPHPExcel->getActiveSheet(0)
			 ->getStyle('A1:Q1')
			 ->applyFromArray(
			  array(
			   'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EBE5DB')
			   )
			  )
			 );		   
		  $getBase = Router::url('/', true);
		  $uploadUrl = WWW_ROOT .'img/stockUpdate/stockUpdate1.csv';
		  $uploadRemote = $getBase.'app/webroot/img/stockUpdate/stockUpdate1.csv';
		  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		  $objWriter->save($uploadUrl);	
		  echo $uploadRemote;
		  exit;	 
		   
	//return $uploadRemote;	  
	}
	
	public function getStock( $barcode = null, $location = null )
	{
		$this->loadModel( 'BinLocation' );
		$binLocation =	$this->BinLocation->find( 'first', array( 
									'conditions' => array( 'BinLocation.barcode' => $barcode, 'BinLocation.bin_location' => $location),
									'fields' => array( 'BinLocation.stock_by_location' )
									)
								);
								//pr( $binLocation );
								//exit;
		if( $binLocation )
		{
			return $binLocation;
		}
								
	}
	
	public function cancelUnpraperOrder()
	{
		$this->loadModel( 'UnprepareOrder' );
		$this->loadModel( 'OrderLocation' );
		$this->loadModel( 'BinLocation' );
		$this->loadModel( 'CheckIn' );
		$this->loadModel( 'Product' );
		$orderid 		= 	$this->request->data['orderID'];
		
		$getOrder		=	$this->UnprepareOrder->find( 'first', array( 'conditions' => array( 'UnprepareOrder.num_order_id' => $orderid ) ) );
		$orderLocations	=	$this->OrderLocation->find( 'all', array( 'conditions' => array( 'order_id' => $orderid ) ) );
		
		foreach( $orderLocations as $orderLocation)
		{
			
			$barcode		=	$orderLocation['OrderLocation']['barcode'];
			$qty_bin		=	$orderLocation['OrderLocation']['quantity'];
			$po_id			=	$orderLocation['OrderLocation']['po_id'];
			$bin_location	=	$orderLocation['OrderLocation']['bin_location'];
			$sku			=	$orderLocation['OrderLocation']['sku'];
			
			$orderLocation['OrderLocation']['status'];
			$orderLocation['OrderLocation']['order_id'];
			$product			=	$this->Product->find( 'first', array( 'conditions' => array( 'Product.product_sku' => $sku ), 'fields' => array( 'current_stock_level' ) ) );
			$currentStickLevel	=	$product['Product']['current_stock_level'];
			App::import('Controller', 'Cronjobs');
			$productCant = new CronjobsController();
			
			if( $bin_location != 'No Location' )
			{
				$this->BinLocation->updateAll(	array('BinLocation.stock_by_location' => "BinLocation.stock_by_location + $qty_bin"),
													array('BinLocation.bin_location' => $bin_location, 'BinLocation.barcode' => $barcode));
				$this->CheckIn->updateAll( array('CheckIn.selling_qty' => "CheckIn.selling_qty + $qty_bin"),array('CheckIn.id' => $po_id));
				$this->Product->updateAll(array('Product.current_stock_level' => "Product.current_stock_level + $qty_bin"), array('Product.product_sku' => $sku));
				$status = 1;
			} else {
				$this->Product->updateAll(array('Product.current_stock_level' => "Product.current_stock_level + $qty_bin"), array('Product.product_sku' => $sku));
				$status = 1;
			}
			
			if($status == 1)
				{
					$ordertype = 'Cancel Unprapered';
					$data['Product']['product_sku']		=	$sku;
					$data['ProductDesc']['barcode']		=	$barcode;
					$data['Product']['CurrentStock']	=	$currentStickLevel;
					$productData	=	json_decode(json_encode($data,0));
					$this->UnprepareOrder->updateAll(array('UnprepareOrder.status' => '2'), array('UnprepareOrder.num_order_id' => $orderid));
					$productCant->storeInventoryRecord( $productData, $qty_bin, $orderid, $ordertype, $barcode);
				}
		}
			
		echo 'Unprapered order cancelled';
		exit;
		
	}
	 
	 
}
?>
