<?php

class StaffOrdersController extends AppController
{
    var $name = "StaffOrders";
	var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
	public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->Auth->Allow(array('update_amazon_order_id'));
	   $this->GlobalBarcode = $this->Components->load('Common'); 
	}
	
	 public function index( $sku = null )
    {
		$this->layout = 'index';
		$this->loadModel('User');
		$this->loadModel('StaffOrder');
		 
		$this->paginate = array('limit' => 50);
 		$getStaffOrders = $this->paginate('StaffOrder');
		
		$users = $this->User->find('all', array('conditions' => array('country' =>1),'fields' => array('id','first_name','last_name','username','email'),'group' => 'email'));	
		//$this->set( 'getStaffOrders', $getStaffOrders );
		$this->set( compact('getStaffOrders','users') );
	}
	 
	
	public function checkProduct()
	{
		$this->layout = '';
		$this->loadModel('Product');		
		$this->loadModel('PurchaseOrder');
		$this->loadModel('StaffTempOrder');
 		 
		$data = [];
		$product_barcode = trim($this->request->data['product_barcode']);
 		$product_global_barcode = $this->GlobalBarcode->getGlobalBarcode($product_barcode);
		$checkSku =	$this->Product->find('first', array( 'conditions' => array( 'ProductDesc.barcode LIKE' => $product_global_barcode ),'fields'=>['Product.product_name','Product.current_stock_level','Product.product_sku'] ) );
		
		if(count($checkSku) > 0){
 			
			$getPurchaseDetail	= $this->PurchaseOrder->find('first', array('conditions'=> array('PurchaseOrder.purchase_sku' => $checkSku['Product']['product_sku'],'PurchaseOrder.price >' => 0 ), 'order' => array('PurchaseOrder.date DESC') ) );
			
			if(count($getPurchaseDetail) > 0)
			{
 				$data['product_name'] = $checkSku['Product']['product_name'];
				$data['product_sku']  = $checkSku['Product']['product_sku'];	
				$data['current_stock']= $checkSku['Product']['current_stock_level'];
				$data['price'] 		  = $getPurchaseDetail['PurchaseOrder']['price']; 
				$data['po_name']      = $getPurchaseDetail['PurchaseOrder']['po_name'] ; 
				
				if(isset($_SESSION['staff_order'][$product_barcode])){
					if(isset($_SESSION['staff_order'][$product_barcode]['qty'])){
						 if( $_SESSION['staff_order'][$product_barcode]['qty'] < 3){
							$data['qty'] = $_SESSION['staff_order'][$product_barcode]['qty'] + 1; 
						}
					}  
				}else{
					$data['qty'] = 1;
				}
				$_SESSION['staff_order'][$product_barcode] = $data;
 			}
			 
 		} 
		$html = '<tr>				
				<th>Poduct Barcode</th>
				<th>Poduct SKU</th>
				<th width="40%">Poduct Title</th>
				<th width="8%">Unit Price</th> 
				<th width="8%">Quantity</th> 
				<th width="8%">Current Stock</th> 
				<th width="8%">Total</th> 
				<th width="8%">Action</th>
			</tr>';
		foreach($_SESSION['staff_order'] as $_barcode => $v){ 
		
			$selected_1 = 'selected="selected"';
			$selected_2 = $selected_3 ='';
			if($v['qty'] == 2){
				$selected_2 = 'selected="selected"';
			}else if($v['qty'] == 3){
				$selected_3 = 'selected="selected"';
			}
		
			 $html .= '<tr>				
				<td>'.$_barcode.'</td>
				<td>'.$v['product_sku'].'</th>
				<td>'.$v['product_name'].'</td> 
				<td id="price_'.$_barcode.'">'.$v['price'].'</td> 
				<td id="qty_'.$_barcode.'"><select name="quantity" id="quantity_'.$_barcode.'" class="form-control" onchange="updateQty('.$_barcode.')"><option value="1" '.$selected_1.'>1</option><option value="2" '.$selected_2.'>2</option><option value="3" '.$selected_3.'>3</option></select></td> 
				<td id="current_stock_'.$_barcode.'">'. $v['current_stock'].'</td>
				<td id="total_'.$_barcode.'">'. number_format( $v['qty']*$v['price'] ,2).'</td>
				<td><button type="button" class="btn btn-danger btn-sm" onclick="removeItem('.$_barcode.');">Remove Item</button></td>
			</tr>';
		}
			
		$data['html'] = $html;    
		echo json_encode($data);
		exit;
  	}
	
	public function removeItem()
	{
		$this->layout = '';
  		unset($_SESSION['staff_order'][$this->request->data['barcode']]);
		$data = []; 
			  
		$html = '<tr>				
				<th>Poduct Barcode</th>
				<th>Poduct SKU</th>
				<th width="40%">Poduct Title</th>
				<th width="8%">Unit Price</th> 
				<th width="8%">Quantity</th> 
				<th width="8%">Current Stock</th> 
				<th width="8%">Total</th> 
				<th width="8%">Action</th>
			</tr>';
		foreach($_SESSION['staff_order'] as $_barcode => $v){ 
			 $html .= '<tr>				
				<td>'.$_barcode.'</td>
				<td>'.$v['product_sku'].'</th>
				<td>'.$v['product_name'].'</td> 
				<td id="price_'.$_barcode.'">'.$v['price'].'</td> 
				<td id="qty_'.$_barcode.'"><select name="quantity" id="quantity_'.$_barcode.'" class="form-control" onchange="updateQty('.$_barcode.')"><option value="1">1</option><option value="2">2</option><option value="3">3</option></select></td> 
				<td id="current_stock_'.$_barcode.'">'. $v['current_stock'].'</td>
				<td id="total_'.$_barcode.'">'. $v['price'].'</td>
				<td><button type="button" class="btn btn-danger btn-sm" onclick="removeItem('.$_barcode.');">Remove Item</button></td>
			</tr>';
		}
			
		$data['html'] = $html;    
		echo json_encode($data);
		exit;
  	}
	
	public function createOrder()
	{
		$this->layout = '';
		$this->loadModel('Product');		
		$this->loadModel('PurchaseOrder');
		
		$data = [];
		
 		$product_global_barcode = $this->GlobalBarcode->getGlobalBarcode($this->request->data['product_barcode']);
		$checkSku =	$this->Product->find('first', array( 'conditions' => array( 'ProductDesc.barcode LIKE' => $product_global_barcode ),'fields'=>['Product.product_name','Product.current_stock_level','Product.product_sku'] ) );
		
		if(count($checkSku) > 0){
 			
			$getPurchaseDetail	= $this->PurchaseOrder->find('first', array('conditions'=> array('PurchaseOrder.purchase_sku' => $checkSku['Product']['product_sku'],'PurchaseOrder.price >' => 0 ), 'order' => array('PurchaseOrder.date DESC') ) );
			
			if(count($getPurchaseDetail) > 0)
			{
 				$data['product_name'] = $checkSku['Product']['product_name'];
				$data['product_sku']  = $checkSku['Product']['product_sku'];
				$data['current_stock'] = $checkSku['Product']['current_stock_level'];	
				$data['price'] 		  = $getPurchaseDetail['PurchaseOrder']['price']; 
				$data['po_name']      = $getPurchaseDetail['PurchaseOrder']['po_name'] ; 
 			}
			 
 		} 
		echo json_encode($data);
		exit;
		
		
	}
	
 	public function manageQuantity($data = array()){
		
		$this->loadModel('FbaItemLocation');
		$this->loadModel('BinLocation');		
		$this->loadModel('CheckIn');
		$this->loadModel('InventoryRecord');	
		
		if(count($data) > 0){
			 
			$quantity 	 = $data['quantity'];					
			$sku     	 = $data['sku'];
			$fnsku    	 = $data['fnsku'];
			$barcode  	 = $data['barcode'];
			$shipment_id = $data['shipment_id']; 
			$shipment_inc_id = $data['shipment_inc_id']; 
			
			
			$getItem = $this->FbaItemLocation->find('first', array('conditions' => array('shipment_id' => $shipment_id, 'fnsku' => $fnsku, 'sku' => $sku) ) );
 			if(count($getItem) == 0){
						
				$po_name = array(); $posIds = array(); $pos = array(); $binname = array(); 
				$available_qty = 0; $available_qty_bin = 0;	
				
				$poDetail = $this->CheckIn->find( 'all', 
					array( 'conditions' => array( 'CheckIn.barcode' => $barcode, 'CheckIn.selling_qty != CheckIn.qty_checkIn','po_name !=""'),
							'order' => 'CheckIn.date  ASC ' ) );
				
				if(count($poDetail) > 0){
					$qt_count = 0; 				
					foreach($poDetail as $po){
						$poQty = $po['CheckIn']['qty_checkIn'] - $po['CheckIn']['selling_qty'];
						if($poQty > 0){						
							 for($i=0; $i <= $quantity ;$i++){						 
								 if($qt_count >= $quantity ){
									 break;
								 }else if(($poQty - $i)  > 0){							
									$po_name[$po['CheckIn']['id']][] = $po['CheckIn']['po_name'];	
									$available_qty++;							
									$qt_count++;
								}												
							 }						
						}
					}
				}
				 
				
				$getStock = $this->BinLocation->find( 'all', array( 'conditions' => array( 'BinLocation.barcode' => $barcode),'order' => 'priority ASC' ) );
				//pr($getStock );
				$bin_name = array();
				$finalData['bin_location'] = '';
				if(count($getStock) > 0){
					$qt_count = 0 ;
					foreach($getStock as $val){
						 $binData['id'] = $val['BinLocation']['id'];					
						 for($i=0; $i <= $quantity ;$i++){						 
							 if($qt_count >= $quantity ){
								 break;
							 }else if(($val['BinLocation']['stock_by_location'] - $i)  > 0){							
								$bin_name[$val['BinLocation']['id']][] = $val['BinLocation']['bin_location'];	
								$available_qty_bin++;						
								$qt_count++;
							}												
						 }							
					}				
				}							
							
				
				if(count($po_name) > 0){
					foreach(array_keys($po_name) as $k){
						$qts   = count($po_name[$k]);	
						$pos[] = $po_name[$k][0];
						$posIds[] = $k;
						if($shipment_id != 'TEST_SHIPMENT'){
					 		$this->CheckIn->updateAll( array('CheckIn.selling_qty' =>  "CheckIn.selling_qty + $qts"),array('CheckIn.id' => $k));
						}
					}
				}
								
				if(count($bin_name) > 0)
				{
					foreach(array_keys($bin_name) as $k){					
						$qts = count($bin_name[$k]);	
						$binname[] = $bin_name[$k][0];	
						$finalData = array();						 
						$finalData['available_qty_check_in'] = $available_qty;
						$finalData['available_qty_bin'] = $qts;
						$finalData['quantity']      	= $quantity;
						$finalData['shipment_id']		= $shipment_id;	
						$finalData['shipment_inc_id']	= $shipment_inc_id;	
						$finalData['merchant_sku']  	= $data['merchant_sku'];
						$finalData['sku_type']			= $data['sku_type']; 				
						$finalData['sku']	   			= $sku;	
						$finalData['fnsku']	   			= $fnsku;								
						$finalData['barcode']  			= $barcode;	
						$finalData['po_name']  			= implode(",",$pos);	
						$finalData['po_id']    			= implode(",",$posIds);	
						$finalData['bin_location']  	= $bin_name[$k][0];		
						$finalData['username'] 			= $this->Session->read('Auth.User.username').'_'.$this->Session->read('Auth.User.pc_name') ;
						$finalData['added_date'] 		= date('Y-m-d H:i:s');		
						$this->FbaItemLocation->saveAll( $finalData );
					
						if($shipment_id != 'TEST_SHIPMENT'){
							$this->BinLocation->updateAll( array('BinLocation.stock_by_location' =>  "BinLocation.stock_by_location - $qts"),array('BinLocation.id' => $k));
						 }
						/**
						Updating current stock of product.
						*/
						$current_qty = 0;
						$_product = $this->Product->find('first',array('conditions'=>array('Product.product_sku' => $sku), 'fields' => array('current_stock_level'),'order'=>'Product.id DESC'));
						
						if(count($_product) > 0){
						
							$current_stock = $_product['Product']['current_stock_level'];														
							$current_qty   = $_product['Product']['current_stock_level'] - $qts;
							if($current_qty < 0){
								$current_qty = 0;
							}
							
							if($shipment_id != 'TEST_SHIPMENT'){
								$this->Product->updateAll( array('Product.current_stock_level' => $current_qty),array('Product.product_sku' => $sku));
							}
							
							$inv_data = array();
							$inv_data['action_type']  	 	= 'FBA_Inventory_Deduction';
							$inv_data['sku'] 		  	 	= $sku;					
							$inv_data['barcode']	  	 	= $barcode;
							$inv_data['currentStock'] 		= $current_stock;
							$inv_data['quantity'] 	   		= $qts;
							$inv_data['status'] 	   		= 'fba';
							$inv_data['after_maniplation'] 	= $current_qty;
							$inv_data['location'] 			= $bin_name[$k][0];
							$inv_data['split_order_id']		= $shipment_id.'_'.$fnsku;
							$inv_data['date'] 		   		= date('Y-m-d' );
				 			if($shipment_id != 'TEST_SHIPMENT'){
								$this->InventoryRecord->saveAll( $inv_data );
							}
						
						}
						// end of stock updation.						
			
					}
				} 
				else {
						$finalData = array();
						$finalData['available_qty_check_in'] = $available_qty;
						$finalData['available_qty_bin'] = '0';
						$finalData['quantity']      	= $quantity;
						$finalData['shipment_id']		= $shipment_id;	
						$finalData['shipment_inc_id']	= $shipment_inc_id;	
						$finalData['merchant_sku']  	= $data['merchant_sku'];
						$finalData['sku_type']			= $data['sku_type']; 						
						$finalData['fnsku']	   			= $fnsku;	
						$finalData['sku']	   			= $sku;								
						$finalData['barcode']  			= $barcode;	
						$finalData['po_name']  			= implode(",",$pos);	
						$finalData['po_id']    			= implode(",",$posIds);	
						$finalData['bin_location']  	= 'No_Location';			
						$finalData['username'] 			= $this->Session->read('Auth.User.username').'_'.$this->Session->read('Auth.User.pc_name') ;
						$finalData['added_date'] 		= date('Y-m-d H:i:s');			
						$this->FbaItemLocation->saveAll( $finalData );
				}
				
			}	
		}					
	}
	
	
	    
}

?>
