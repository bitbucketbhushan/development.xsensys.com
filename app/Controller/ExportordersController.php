<?php 

class ExportordersController extends AppController {

	
	var $name = "Exportorders";
    
    var $components = array('Session','Upload','Common','Auth');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter() {
	    parent::beforeFilter();
		$this->Auth->Allow(array('getOrders')); 
		 date_default_timezone_set('Europe/Jersey');
	   
	}

	public function getOrders()
		{
			$this->loadModel('MergeUpdate');
			$getorder	=	$this->MergeUpdate->find('all', array( 
													 'conditions' => array( 'order_date >=' => date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-7 days" ) ))
													 ,'fields' => array( 'order_id','order_date','process_date','manifest_date','assign_user','scan_date','status' ) ) );
			echo json_encode($getorder);
			exit;
		}
	public function checktime()
	{
		
	}
	
	public function getSkuSelectAgeReport()
	  {
	  		$this->layout = 'index';
			$this->loadModel('Product');
			$this->loadModel('ProductDesc');
			$this->loadModel('PurchaseOrder');
			$this->loadModel('ScanOrder');
			$this->loadModel('CheckIn');
			$this->loadModel('User');
			$this->loadModel('Po');
			
			$allusers = $this->User->find('all');
			foreach($allusers as $alluser){
				$user_data[$alluser['User']['id']]	=	$alluser['User']['first_name'].' '.$alluser['User']['last_name'];
			}
		
			$this->paginate = array('conditions' => array('ProductDesc.product_type' => 'Single', 'Product.current_stock_level >' => '0'), 'fields'=> array('Product.product_sku','Product.current_stock_level','ProductDesc.user_id','ProductDesc.purchase_user_id'),'order' => 'current_stock_level DESC', 'limit' => 10 );
		 
		//$this->Product->recursive = 7;
		
		// we are using the 'Product' model
			$getAllProducts = $this->paginate('Product');
		
			//$getAllProducts	=	$this->Product->find('all',  );
			$i = 0; 
			$pos =	array();
			foreach($getAllProducts as $getAllProduct)
			{	
					//$usesrname 	= 	$this->getUsername( $getAllProduct['ProductDesc']['purchase_user_id'] );
					$purchase	=	$this->CheckIn->find('all', array('conditions' => array('sku' => $getAllProduct['Product']['product_sku'], 'custam_page_name' => 'Check In' ), 'fields' => array('date','qty_checkIn','po_name','selling_qty','sku'),'order' => 'date DESC','limit' => '2' ) );
						$pos[$getAllProduct['Product']['product_sku']]['stock'] = $getAllProduct['Product']['current_stock_level'];
						$pro = array();
					foreach($purchase as $pur ){
						$pos[$getAllProduct['Product']['product_sku']]['po'][] = $pur['CheckIn']['po_name'];
						$pro[]	=	$this->getpurprice( $pur['CheckIn']['po_name'], $getAllProduct['Product']['product_sku'] );
						$pos[$getAllProduct['Product']['product_sku']]['check_in_date']  = $purchase[0]['CheckIn']['date'];
						$pos[$getAllProduct['Product']['product_sku']]['age']  		= floor($this->pleasecheckdatediff(date('Y-m-d'), date("Y-m-d", strtotime($purchase[0]['CheckIn']['date']))));
					}
						if(count($pro) > 0 ) { $pric = array_sum($pro)/count($pro); } else { $pric = 0; }
						$pos[$getAllProduct['Product']['product_sku']]['price']  	= $pric;
						$pos[$getAllProduct['Product']['product_sku']]['name']  	= $user_data[ $getAllProduct['ProductDesc']['purchase_user_id'] ];
						$pos[$getAllProduct['Product']['product_sku']]['status']  	= $this->getskustatus( $getAllProduct['Product']['product_sku'] );
			}
			$this->set( 'skuage', $pos);
	}
	
	public function pleasecheckdatediff( $date1 = null, $date2 = null )
	{
		$diff 	= 	abs(strtotime($date2) - strtotime($date1));
		$days	=	$diff  / (60*60*24);
		return $days; 
	} 
	
	public function getskustatus( $sku = null )
	{
		$skuQty = $this->ScanOrder->find('all', array( 'conditions' => array( 'sku' =>  $sku, 'scan_date >' => date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-15 days" ) )), 'fields' => array( 'SUM(quantity) as Qty' ) ) );
		foreach( $skuQty as $skuQt )
		{
			$avg_qty	=	ceil($skuQt[0]['Qty']/15);
		}
		if($avg_qty >= 2 ){
			$status = 'Active';
		} else {
			$status = 'Sleep';
		}
		return $status;
		
	}
	
	public function getpurprice( $po = null, $sku = null )
	{
		$purchaseprice = $this->PurchaseOrder->find('first', array( 'conditions' => array( 'purchase_sku' =>  $sku, 'po_name' =>  $po  ) ) );
		if( count($purchaseprice) > 0 ){
			$price = $purchaseprice['PurchaseOrder']['price'];
		} else {
			$price = '0';
		}
		return $price;
	}
	
	public function getUsername( $id = null )
	{
		$getusername = $this->User->find('first', array( 'conditions' => array( 'id' =>  $id ) ) );
		if( count($getusername) > 0 ){
			$fullname = $getusername['User']['first_name'].' '.$getusername['User']['last_name'];
		} else {
			$fullname = 'No User';
		}
		return $fullname;
		
	}
	
	
	public function generateSkuAgeFile()
	{
			$this->layout = '';
			$this->loadModel('Product');
			$this->loadModel('ProductDesc');
			$this->loadModel('PurchaseOrder');
			$this->loadModel('ScanOrder');
			$this->loadModel('CheckIn');
			$this->loadModel('User');
			$this->loadModel('Po');
			
			$allusers = $this->User->find('all');
			foreach($allusers as $alluser){
				$user_data[$alluser['User']['id']]	=	$alluser['User']['first_name'].' '.$alluser['User']['last_name'];
			}
			
			$this->Product->unbindModel(array('hasMany' => array('ProductLocation')));
			
			$getAllProducts	=	$this->Product->find('all', array('conditions' => array('ProductDesc.product_type' => 'Single', 'Product.current_stock_level >' => '0'), 'fields'=> array('Product.product_sku','Product.current_stock_level','ProductDesc.user_id','ProductDesc.purchase_user_id') ) );
			
			foreach($getAllProducts as $product ){
				$pro_data[ $product['Product']['product_sku'] ] = array('stock' => $product['Product']['current_stock_level'], 'user' => $product['ProductDesc']['purchase_user_id']);
			}
			
			$i = 0;
			$path = WWW_ROOT.'img/';
		 	$header = "SKU,Stock,Price,Po,ChackIn Date,< 30 days,> 30 days,Purchase User,Status\r\n";
			file_put_contents($path."/getSkuAge.csv",$header, LOCK_EX);
			foreach( $pro_data as $prok => $prov){ 
				$purchase	=	$this->CheckIn->find('all', array('conditions' => array('sku' => $prok, 'custam_page_name' => 'Check In' ), 'fields' => array('date','po_name','sku'),'order' => 'date DESC','limit' => '2' ) );
				if(count($purchase) > 0){	
						$pro = array(); $po = array();
						foreach($purchase as $pur ){
							$po[] 	= 	$pur['CheckIn']['po_name'];
							$pro[]	=	$this->getpurprice( $pur['CheckIn']['po_name'], $prok );
						}
						if(!empty($purchase[0]['CheckIn']['date']) ){ 
							$date		= 	$purchase[0]['CheckIn']['date']; 
							$date1		=	date('Y-m-d');
							$diff 		= 	abs(strtotime($date) - strtotime($date1));
							$days		=	$diff  / (60*60*24);
							$datediff	=	$days;//floor($this->pleasecheckdatediff(date('Y-m-d'), date("Y-m-d", strtotime($purchase[0]['CheckIn']['date']))));
						} else {
							$date = 'Not Checkin'; 
							$datediff = 'Not Checkin';
						}
						
						if(ceil($datediff) > 30){
							$below = '0';
							$above = ceil($datediff);
						} else {
							$below = ceil($datediff);
							$above = '0';
						}
						$pos[ $prok ]['check_in_date']  = $date;
						$pos[ $prok ]['age']  		= ceil($datediff);
						$price  					= array_sum($pro)/count($pro);
						$name  		= (!empty($user_data[$prov['user']]) && $user_data[$prov['user']]  != '') ? $user_data[$prov['user']] : '--';
						$status  	= $this->getskustatus( $prok );
						$content = $prok.",".$prov['stock'].','.$price.','.implode(';',$po).','.date('d-m-Y',strtotime($date)).','.$below.','.$above.','.$name .','.$status."\r\n";
						file_put_contents($path."/getSkuAge.csv",$content, FILE_APPEND | LOCK_EX);
					}
			}
			/*pr($pos);
			exit;
			$pos =	array();
			$i = 0;
			foreach($getAllProducts as $getAllProduct)
			{	
					
					$purchase	=	$this->CheckIn->find('all', array('conditions' => array('sku' => $getAllProduct['Product']['product_sku'], 'custam_page_name' => 'Check In' ), 'fields' => array('date','qty_checkIn','po_name','selling_qty','sku'),'order' => 'date DESC','limit' => '2' ) );
					echo $getAllProduct['Product']['Product'];
						$pos[$getAllProduct['Product']['product_sku']]['stock'] = $getAllProduct['Product']['current_stock_level'];
						$pro = array(); $po = array();
					if(count($purchase) > 0){	
						foreach($purchase as $pur ){
							$po[] 	= 	$pur['CheckIn']['po_name'];
							//$pro[]	=	$this->getpurprice( $pur['CheckIn']['po_name'], $getAllProduct['Product']['product_sku'] );
						}
						
						echo $i++;
						pr($po);
						pr($pro);
					}
						/*$pos[$getAllProduct['Product']['product_sku']]['po'] = implode(';',$po);
						if(count($pro) > 0 ) { $pric = array_sum($pro)/count($pro); } else { $pric = 0; }
						if(!empty($purchase[0]['CheckIn']['date']) ){ 
							$date		= 	$purchase[0]['CheckIn']['date']; 
							$datediff	=	floor($this->pleasecheckdatediff(date('Y-m-d'), date("Y-m-d", strtotime($purchase[0]['CheckIn']['date']))));
						} else {
							
							$date = 'Not Checkin'; 
							$datediff = 'Not Checkin';
						}
						$pos[$getAllProduct['Product']['product_sku']]['check_in_date']  = $date;
						$pos[$getAllProduct['Product']['product_sku']]['age']  		= $datediff;
						$pos[$getAllProduct['Product']['product_sku']]['price']  	= $pric;
						$pos[$getAllProduct['Product']['product_sku']]['name']  	= ($user_data[ $getAllProduct['ProductDesc']['purchase_user_id'] ] != '') ? $user_data[ $getAllProduct['ProductDesc']['purchase_user_id'] ] : '--';
						$pos[$getAllProduct['Product']['product_sku']]['status']  	= $this->getskustatus( $getAllProduct['Product']['product_sku'] );
						*/
						
			//}
			
			//pr($pos);
			/*exit;
			$path = WWW_ROOT.'img/';
		 	$header = "SKU,Stock,Price,Po,ChackIn Date,< 30 days,> 30 days,Purchase User,Status\r\n";
			file_put_contents($path."/getSkuAge.csv",$header, LOCK_EX);
			
			foreach($pos as $posk => $posv){
				$po_name		=	(isset($posv['po']) && $posv['po'] != '') ? implode(';',$posv['po']) : '--';
				$check_indate	=	date('d-m-Y',strtotime($posv['check_in_date']));
				$below			=	(isset($posv['age']) && $posv['age'] < '30') ? $posv['age'] : '-';
				$above			=	(isset($posv['age']) && $posv['age'] > '30') ? $posv['age'] : '-';
				
				echo $content = $posk.",".$posv['stock'].','.$posv['price'].','.$po_name.','.$check_indate.','.$below.','.$above.','.$posv['name'].','.$posv['status']."\r\n";
				file_put_contents($path."/getSkuAge.csv",$content, FILE_APPEND | LOCK_EX);
			}*/
		   $result	=	Router::url('/', true)."img/getSkuAge.csv";
	
		   header('Content-Description: File Transfer');
		   header('Content-Type: application/force-download');
		   header('Content-Disposition: attachment; filename='.basename($result));
		   header('Content-Transfer-Encoding: binary');
		   header('Expires: 0');
		   header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		   header('Pragma: public'); 
		   readfile($result);
		   exit;
	}
	
	
	
	
	
	
	public function generateSkuAgeFileOpt()
	{
			$this->layout = '';
			$this->loadModel('Product');
			$this->loadModel('ProductDesc');
			$this->loadModel('PurchaseOrder');
			$this->loadModel('ScanOrder');
			$this->loadModel('CheckIn');
			$this->loadModel('User');
			$this->loadModel('Po');
			
			echo "111";
			$allusers = $this->User->find('all');
			foreach($allusers as $alluser){
				$user_data[$alluser['User']['id']]	=	$alluser['User']['first_name'].' '.$alluser['User']['last_name'];
			}
			pr($user_data);
			exit;
			$this->Product->unbindModel(array('hasMany' => array('ProductLocation')));
			
			$getAllProducts	=	$this->Product->find('all', array('conditions' => array('ProductDesc.product_type' => 'Single', 'Product.current_stock_level >' => '0'), 'fields'=> array('Product.product_sku','Product.current_stock_level','ProductDesc.user_id','ProductDesc.purchase_user_id') ) );
			
			foreach($getAllProducts as $product ){
				$pro_data[ $product['Product']['product_sku'] ] = array('stock' => $product['Product']['current_stock_level'], 'user' => $product['ProductDesc']['purchase_user_id']);
			}
			
			
		   
	}
	
	
	
	
}



?>