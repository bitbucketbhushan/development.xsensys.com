<?php
error_reporting(1);
set_time_limit(30);

class SupplierDetailsController extends AppController
{
    
    var $name = "SupplierDetails";
    
    var $components = array('Session','Upload','Common');
    
    var $helpers = array('Html','Form','Session','Common');
    
  	public function index() 
    {
		$this->layout = "index";
		$this->loadModel('User'); 
		$this->loadModel( 'SupplierDetail' ); 
		$users = $this->User->find('all', array('conditions' => array('role_type' =>'3','is_deleted' =>0),'fields' => array('id','first_name','last_name','username'),'order'=>'first_name'));
		
		if(isset($this->request->query['key']) && $this->request->query['key'] != ''){
			$key = trim($this->request->query['key']);
			$this->paginate = array('conditions' => array('OR'=>['SupplierDetail.supplier_code LIKE' => $key.'%','SupplierDetail.supplier_name LIKE' => '%'.$key.'%'] ),'limit' => 25);
		}else if(isset($this->request->query['purchaser']) && $this->request->query['purchaser'] != ''){
			$key = trim($this->request->query['purchaser']);
			$this->paginate = array('conditions' => array('SupplierDetail.purchaser LIKE' => $key.'%'),'limit' => 25);
		}else{
			$this->paginate = array('limit' => 25);
		}
		$SupplierDetail = $this->paginate('SupplierDetail');
		$this->set('SupplierDetail',$SupplierDetail);
		$this->set( 'users', $users );	   
	}
	
	public function getSupplierItems($sup_code = 0) 
    {
		$this->loadModel( 'SupplierMapping' ); 
		$added = $not_added = $active = $inactive = 0;
		$supdata = $this->SupplierMapping->find('all', array('conditions' => array('sup_code' =>$sup_code),'fields' => array('status','istatus')));
		foreach($supdata as $v){
			if($v['SupplierMapping']['status'] == 'added'){
				$added++;
			}else{
				$not_added++;
			}
			
			if($v['SupplierMapping']['istatus'] == 1){
				$inactive++;
			}else{
				$active++;
			}
			
		}
		$sup_data['added'] = $added; 
		$sup_data['not_added']= $not_added; 
		$sup_data['inactive'] = $inactive; 
		$sup_data['active']= $active;
		return $sup_data;
		exit;
	}
	
 	public function SupplierPos($code = 0) 
    {
		$this->layout = "index";
		
		$this->loadModel('Po');
		$this->loadModel('PurchaseOrder');
		$this->loadModel('CheckIn');
 		
		$this->paginate = array( 'conditions' => array('supplier_code' => $code),
                        	'fields' => array( 
                                'DISTINCT Po.po_name',
                                'Po.*'
                            ),
							'order' => 'id DESC',
							'limit' => 25			
					);
					
		$getAllPurchaseOrders = $this->paginate('Po');

		$this->set('getAllPurchaseOrders', $getAllPurchaseOrders);
	
	}
	
	public function showPoDetail( $id = null,$type = null )
	{
		
		$this->layout = 'index';  
		$this->loadModel('Po');
		$this->loadModel('PurchaseOrder');
		$this->loadModel('CheckIn');
		$this->loadModel('BinLocation');

		$getPoName	=	$this->Po->find('first', array('conditions' => array( 'Po.id' => $id )));
		
		
		$poName		=	$getPoName['Po']['po_name'];

		$getPoItems	=	$this->PurchaseOrder->find('all', array('conditions' => array('PurchaseOrder.po_id' => $id)));
		if(!empty($getPoItems))
		{
			$getPoItems	=	$this->PurchaseOrder->find('all', array('conditions' => array('PurchaseOrder.po_name' => $poName)));
		}

		foreach ($getPoItems as  $value) {
			
			$checkin = $this->CheckIn->find( 'all' ,array('conditions' => array('CheckIn.sku'=> $value['PurchaseOrder']['purchase_sku'],'po_name'=>$value['PurchaseOrder']['po_name'])));
			$BinLocation = $this->BinLocation->find( 'all' ,array('conditions' => array('barcode'=> $value['PurchaseOrder']['purchase_barcode']))); 
			
			$getPoItem['PurchaseOrder']['bin_location']= $BinLocation;
			$getPoItem['PurchaseOrder']['id']=$value['PurchaseOrder']['id'];
			$getPoItem['PurchaseOrder']['po_name']=$value['PurchaseOrder']['po_name'];
			$getPoItem['PurchaseOrder']['purchase_sku']=$value['PurchaseOrder']['purchase_sku'];
			$getPoItem['PurchaseOrder']['purchase_code']=$value['PurchaseOrder']['purchase_code'];
			$getPoItem['PurchaseOrder']['purchase_barcode']=$value['PurchaseOrder']['purchase_barcode'];
			$getPoItem['PurchaseOrder']['ean_barcode']=$value['PurchaseOrder']['ean_barcode'];
			$getPoItem['PurchaseOrder']['pack_size']=$value['PurchaseOrder']['pack_size'];
			$getPoItem['PurchaseOrder']['purchase_description']=$value['PurchaseOrder']['purchase_description'];
			$getPoItem['PurchaseOrder']['purchase_qty']=$value['PurchaseOrder']['purchase_qty'];
			$getPoItem['PurchaseOrder']['order_case']=$value['PurchaseOrder']['order_case'];
			$getPoItem['PurchaseOrder']['total_qty']=$value['PurchaseOrder']['total_qty'];
			$getPoItem['PurchaseOrder']['case_price']=$value['PurchaseOrder']['case_price'];
			$getPoItem['PurchaseOrder']['price']=$value['PurchaseOrder']['price'];
			$getPoItem['PurchaseOrder']['total_cost']=$value['PurchaseOrder']['total_cost'];
			$getPoItem['PurchaseOrder']['purchase_count']=$value['PurchaseOrder']['purchase_count'];
			$getPoItem['PurchaseOrder']['warehouse_fill_in']=$value['PurchaseOrder']['warehouse_fill_in'];
			$getPoItem['PurchaseOrder']['comment_id']=$value['PurchaseOrder']['comment_id'];
			$getPoItem['PurchaseOrder']['comment']=$value['PurchaseOrder']['comment'];
			$getPoItem['PurchaseOrder']['seller_comment']=$value['PurchaseOrder']['seller_comment'];
			$getPoItem['PurchaseOrder']['supplier_code']=$value['PurchaseOrder']['supplier_code'];
			$getPoItem['PurchaseOrder']['po_date']=$value['PurchaseOrder']['po_date'];
			$getPoItem['PurchaseOrder']['date']=$value['PurchaseOrder']['date'];
			$getPoItem['PurchaseOrder']['updated_date']=$value['PurchaseOrder']['updated_date'];
			$getPoItem['PurchaseOrder']['po_id']=$value['PurchaseOrder']['po_id'];
			$getPoItem['PurchaseOrder']['checkin']=$checkin;
			$data[]=$getPoItem;
			}
		
		
		// foreach ($getPoItems as $value) {
		// 	print_r($value);
		// }

		$this->set('type', $type);
		$this->set('getPoItems', $data);
		$this->set('getPoName', $getPoName);
	}
	
	public function SupplierInvoices($id = 0) 
    {
		$this->layout = "index";
		$this->loadModel('ScanInvoice');
		$this->paginate = array( 'conditions' => array('po_id' => $id),
 							'order' => 'id DESC',
							'limit' => 25			
					);
					
		$ScanInvoice = $this->paginate('ScanInvoice');

		$this->set('ScanInvoice', $ScanInvoice);
	}
	
	public function SupplierGeneratePo($id = 0) 
    {
		$this->layout = "index";
		$this->render( 'index' );
	}
	
	public function AddSupplier($id = 0) 
    {
		$this->layout = "index";
 		$this->loadModel('User'); 
		$this->loadModel( 'SupplierDetail' ); 
		$users = $this->User->find('all', array('conditions' => array('role_type' =>'3','is_deleted' =>0),'fields' => array('id','first_name','last_name','username'),'order'=>'first_name'));
		
		$data = $this->SupplierDetail->find('first',array('conditions' => array('id' => $id)));	
		if(count($data) > 0){
			$this->request->data = $data;
		}
		$this->set('users', $users);
	}
	
	public function SaveSupplier() 
    {
		$this->layout = "index";
		$this->loadModel( 'SupplierDetail' ); 
 		try{
		
			if(isset($this->request->data['SupplierDetail']['id'])){
				$this->SupplierDetail->saveAll($this->request->data);
				$this->Session->setflash( 'Supplier updated successfully.', 'flash_success' );  
			}
			else
			{
 		
				$ch_sp = $this->SupplierDetail->find('first',array('conditions' => array('supplier_name' => $this->request->data['SupplierDetail']['supplier_name'])));	 
				if(count($ch_sp) > 0){
					$ch_sp = $this->SupplierDetail->find('first',array('conditions' => array('supplier_code' => $this->request->data['SupplierDetail']['supplier_code'])));	 
				}
				
 				if(count($ch_sp) == 0){
					$this->SupplierDetail->saveAll($this->request->data);	
					$this->Session->setflash( 'Supplier added successfully.', 'flash_success' );   
				}else{
					$this->Session->setflash( 'Supplier name & Supplier code already exists!', 'flash_danger' );   
				}
			}
		}catch(Exception $e) {
 			$this->Session->setflash( 'Message: ' .$e->getMessage(), 'flash_danger' );   
		}
 		$this->redirect($this->referer());
		exit;
	}
	
	 
	public function AssignPurchaser() 
    {
		$this->layout = "ajax";
		$this->loadModel( 'SupplierDetail' ); 
 		$this->loadModel( 'SupplierPurchaser' ); 
		try{
			$ch_sp = $this->SupplierDetail->find('first',array('conditions' => array('id' => $this->request->data['id'])));	 
			if(count($ch_sp) > 0){
				$data['supplier_id']   = $ch_sp['SupplierDetail']['id'];
				$data['supplier_code'] = $ch_sp['SupplierDetail']['supplier_code'];
				$data['purchaser']     = $this->request->data['purchaser'];
				$data['added_by']      = $this->Session->read('Auth.User.username');
				$data['added_date']    = date('Y-m-d H:i:s');
				$this->SupplierPurchaser->saveAll($data);
				
				$this->SupplierDetail->updateAll(['purchaser'=>"'".$this->request->data['purchaser']."'"],['id'=>$this->request->data['id']]);	
				$msg['msg'] = 'Purchaser Assigned successfully!';
			}else{
				$msg['msg'] = 'Supplier name & Supplier code already exists!';
				$msg['error'] = 'yes';
			}
		}catch(Exception $e) {
  			$msg['msg'] = 'Message: ' .$e->getMessage();
		}
		echo json_encode($msg);
 		exit;
	}
	
	public function ajaxAddSupplier() 
    {
		$this->layout = "ajax";
		$this->loadModel( 'SupplierDetail' ); 
		
		try{
			$ch_sp = $this->SupplierDetail->find('first',array('conditions' => array('supplier_name' => $this->request->data['supplier_name'])));	 
			if(count($ch_sp) > 0){
				$ch_sp = $this->SupplierDetail->find('first',array('conditions' => array('supplier_code' => $this->request->data['supplier_code'])));	 
			}
			
			if(count($ch_sp) == 0){
				$this->SupplierDetail->saveAll($this->request->data);	
				$msg['msg'] = 'Supplier registered successfully!';
			}else{
				$msg['msg'] = 'Supplier name & Supplier code already exists!';
				$msg['error'] = 'yes';
			}
		}catch(Exception $e) {
  			$msg['msg'] = 'Message: ' .$e->getMessage();
		}
		echo json_encode($msg);
 		exit;
	}
	
	public function ajaxUpdateSupplier() 
    {
		$this->layout = "ajax";
		$this->loadModel( 'SupplierDetail' ); 
		
		try{
			$ch_sp = $this->SupplierDetail->find('first',array('conditions' => array('id' => $this->request->data['id'])));	 
			if(count($ch_sp) > 0){
 				$this->SupplierDetail->saveAll($this->request->data);	
				$msg['msg'] = 'Supplier updated successfully!';
			}else{
				$msg['msg'] = 'Supplier name & Supplier code already exists!';
				$msg['error'] = 'yes';
			}
		}catch(Exception $e) {
  			$msg['msg'] = 'Message: ' .$e->getMessage();
		}
		echo json_encode($msg);
 		exit;
	}
	public function ajaxGetSuppliers() 
    {
		$this->layout = "ajax";
		$this->loadModel( 'SupplierDetail' ); 
        $get_suppliers = $this->SupplierDetail->find( 'all');
		$html = '';
		if(count($get_suppliers) > 0){
			foreach($get_suppliers as $v){
				$html .='<tr>';
					$html .='<td>'.$v['SupplierDetail']['supplier_name'].'</td>';
					$html .='<td>'.$v['SupplierDetail']['supplier_code'].'</td>';
					$html .='<td>'.$v['SupplierDetail']['lead_time'].'</td>';
					$html .='<td>'.$v['SupplierDetail']['credit_limit'].'</td>';
					$html .='<td>'.$v['SupplierDetail']['credit_limit_available'].'</td>';
					$html .='<td>'.$v['SupplierDetail']['credit_period'].'</td>';
					$html .='<td>'.$v['SupplierDetail']['currency'].'</td>';
					$html .='<td>'.$v['SupplierDetail']['address'].'</td>	';							
					$html .='<td><button type="button" class="btn btn-primary btn-sm" onclick="editSupplier('.$v['SupplierDetail']['id'].')" style="margin-bottom:4px;">Edit</button>  <button type="button" class="btn btn-danger btn-sm" onclick="delSupplier('.$v['SupplierDetail']['id'].')">Delete</button>
					</td>';
				$html .='</tr>';
 			}
		}
		$data['html'] = $html;
		echo $html;//echo json_encode( $data );
		exit;
	}
	public function ajaxGetSupplier() 
    {
		$this->layout = "ajax";
		$this->loadModel( 'SupplierDetail' ); 
        $get_supplier  =  $this->SupplierDetail->find('first',array('conditions' => array('id' => $this->request->data['id'])));	 
		 $data =[];
		if(count($get_supplier) > 0){
			$data['id'] = $get_supplier['SupplierDetail']['id'];
			$data['supplier_name'] = $get_supplier['SupplierDetail']['supplier_name'];
			$data['supplier_code'] = $get_supplier['SupplierDetail']['supplier_code'];
			$data['lead_time'] = $get_supplier['SupplierDetail']['lead_time'];
			$data['credit_limit'] = $get_supplier['SupplierDetail']['credit_limit'];
			$data['credit_limit_available'] = $get_supplier['SupplierDetail']['credit_limit_available'];
			$data['credit_period'] = $get_supplier['SupplierDetail']['credit_period'];
			$data['currency'] = $get_supplier['SupplierDetail']['currency'];
			$data['address'] = $get_supplier['SupplierDetail']['address'];
			
 		}
		
		echo json_encode( $data );
		exit;
	}
	public function ajaxDelSuppliers() 
    {
		$this->layout = "ajax";
		$this->loadModel( 'SupplierDetail' ); 
		try{
			$ch_sp = $this->SupplierDetail->find('first',array('conditions' => array('id' => $this->request->data['id'])));	 
			if(count($ch_sp) > 0){
				$id  = $this->request->data['id'];
				$sql = "DELETE FROM `supplier_details` WHERE `supplier_details`.`id` = {$id}";
				$this->SupplierDetail->query($sql);	 
				$msg['msg'] = 'Supplier deleted!';
			}else{
				$msg['msg'] = 'Supplier not nound!';
				$msg['error'] = 'yes';
			}
		}catch(Exception $e) {
  			$msg['msg'] = 'Message: ' .$e->getMessage();
		}
		echo json_encode($msg);
 		exit;
	}
	
   public function Supplier_Mapping()
    {
		$this->layout = 'editable';
		$this->loadModel( 'SupplierDetail' );
		$this->loadModel( 'SupplierMapping' ); 
 		$suppliers = $this->SupplierDetail->find( 'all' ); 
		$this->set('suppliers',$suppliers);	
		
		$this->paginate = array('group' => 'master_sku','limit' => 50,'order'=>'id DESC'); 
		$supplier_mapping = $this->paginate('SupplierMapping');
		
		$this->set('supplier_mapping',$supplier_mapping); 
					
 	}
	
	public function SupplierPricing()
    {
		$this->layout = 'editable';
		$this->loadModel( 'SupplierDetail' );
		$this->loadModel( 'SupplierMapping' ); 
		$this->loadModel( 'SupplierPricing' ); 
 		$suppliers = $this->SupplierDetail->find( 'all' ); 
		$this->set('suppliers',$suppliers);	
		
		$this->paginate = array('group' => 'master_sku','limit' => 50,'order'=>'id DESC'); 
		$supplier_mapping = $this->paginate('SupplierMapping');
		
		$this->set('supplier_mapping',$supplier_mapping); 
					
 	}
	public function ajaxGetSupplierMapping() 
    {
	 	$this->loadModel( 'SupplierMapping' ); 
		
		if(isset($this->request->data["page"])){
			$page_number = filter_var($this->request->data["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
			if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
		}else{
			$page_number = 1; //if there's no page number, set it to 1
		}
		
		$sql_query = '';
		if($this->request->data["master_sku"] || $this->request->data["supplier_sku"]){
			$Where = array();	
								  
			if($this->request->data["master_sku"]!=''){
				$Where[] = "  SupplierMapping.master_sku LIKE '%".trim($this->request->data['master_sku'])."%'";
			}
			if($this->request->data["supplier_sku"]!=''){
				$Where[] = "  SupplierMapping.supplier_sku LIKE '%".trim($this->request->data['supplier_sku'])."%'";
			}
			if(isset($this->request->data["supplier_code"]) && $this->request->data["supplier_code"]!=''){
				$Where[] = "  SupplierMapping.supplier_code LIKE '%".$this->request->data['supplier_code']."%'";
			}
			
			if(count($Where)>0){
				$w = 1; $where_col = '';
				$count_lenght_of_where = count($Where);
				foreach ($Where as $k) {
					$where_col .= $k;
					if ($w < $count_lenght_of_where) {
						$where_col .='AND ';
					}
					$w++;
				 }
				$sql_query =' WHERE '.$where_col;			
			}						 	
		}
	 										
		
		
	 	$sql_count = "SELECT SupplierMapping.id, Product.product_sku,SupplierMapping.case_size  FROM products Product LEFT JOIN supplier_mappings SupplierMapping  ON Product.product_sku = SupplierMapping.master_sku $sql_query GROUP BY Product.product_sku";
		
		$result = $this->SupplierMapping->query($sql_count);	
								
		$get_total_rows = $result; //hold total records in variable
		$item_per_page = 50;
		$total_rows =  count($result);
		$total_pages = ceil($total_rows/$item_per_page);
			
		//get starting position to fetch the records
		$page_position = (($page_number-1) * $item_per_page);
	
		$sql = $sql_count." ORDER BY `master_sku` DESC LIMIT $page_position, $item_per_page " ;		
								
		$results = $this->SupplierMapping->query($sql); 
		$sort_by = 'Product.product_sku'; $order_by = 'Desc';
		if(count($results) > 0){
			$AllSupplier = $this->getAllSupplier();
			$count = 0; $msg['data'] = '';
			foreach($results as $v){	
			$msg['data'] .=	'<tr>';
			$msg['data'] .=	'<td>'.$v['Product']['product_sku'].'</td>';	
			
					 
					 
						
						foreach($AllSupplier as $k => $val){ 						
						$supData = $this->getSupplierSku($val['supplier_code'], $v['Product']['product_sku']);					
							if($supData['supplier_sku'] != '-'){
								$msg['data'] .= '<td><section><span class="xedit" data-name="supplier_sku" supplier-code="'.$val['supplier_code'].'" pk="'.$v['Product']['product_sku'].'" action="update" data-value="'.$supData['supplier_sku'].'">'.$supData['supplier_sku'].'</span></section><section></section></td>';
							}else{					
								$msg['data'] .= '<td><section><span class="xedit" data-name="supplier_sku" supplier-code="'.$val['supplier_code'].'" pk="'.$v['Product']['product_sku'].'" action="update" data-value=""></span></section></td>';
							}					
						}
			
			$msg['data'] .='</tr>';	
			$count++;	
		
			}
			$msg['data'] .='<script>jQuery(\'.xedit\').editable();</script>';
			$msg['paginate'] = '<div class="total_pages">Total Page: '.$total_pages.' Items#'.$count.'</div>
				<div class="paginate">'.$this->paginate_function($item_per_page, $page_number, $total_rows, $total_pages,$sort_by,$order_by).	'</div>
				<div class="total_rows">Total Row: '.$total_rows.'</div>';	
		}else{
			$msg['paginate'] ='';		
			$msg['data'] = '<tr><td colspan="18"><div align="center"><div class="alert alert-danger fade in">No data found!</div></div></td></tr>';
		}
		echo json_encode($msg);
		exit;
	
	}
	public function getAllSupplier() 
    {
		$this->layout = "ajax";
		$this->loadModel( 'SupplierDetail' ); 
	 
		$data = array();	
 		$ch_sp = $this->SupplierDetail->find('all');	
 		if(count($ch_sp) > 0){			
			foreach($ch_sp as $v) {
				$data[] = array('supplier_code' => $v['SupplierDetail']['supplier_code'],'currency' => $v['SupplierDetail']['currency']);	
			}
		}
 		return $data;
	}
	
	public function ajaxGridEitable() 
    {
		$this->layout = "ajax";
		$this->loadModel( 'SupplierMapping' ); 
	 	if($this->request->data['value'] !=''){
			try{
				$ch_sp = $this->SupplierMapping->find('first',array('conditions' => array('supplier_code' => $this->request->data['supplier_code'],'master_sku' => $this->request->data['pk'])));	 
				
				if(count($ch_sp) > 0){
					$fields = "`".$this->request->data['name']."` =  '".$this->request->data['value']."' ";
					$sql_query = "UPDATE `supplier_mappings` SET $fields  WHERE `master_sku` = '".$this->request->data['pk']."' AND `supplier_code` = '".$this->request->data['supplier_code']."' ";	
					$this->SupplierMapping->query($sql_query);
					$msg['msg'] = 'Supplier sku mapped successfully!';
					$msg['status'] = 'ok';
				}else{
 					$sql = "INSERT INTO `supplier_mappings` (`supplier_code`, `master_sku`, `supplier_sku`) 
					VALUES ('".$this->request->data['supplier_code']."','".$this->request->data['pk']."','".$this->request->data['value']."');";
					$this->SupplierMapping->query($sql);		
					//echo ' Item Added.';	
					$msg['msg'] = 'Supplier sku already exists!';
					$msg['status'] = 'ok';
				}
			}catch(Exception $e) {
				$msg['msg'] = 'Message: ' .$e->getMessage();
				$msg['status'] = 'error';
			}
		}else{
			$msg['msg'] = 'Please enter supplier code!';
			$msg['status'] = 'error';
		}
		echo json_encode($msg);
 		exit;
	}
	public function DownloadSuppliers(){
		$this->loadModel( 'SupplierDetail' ); 
		$this->loadModel( 'Company' );
  		$this->loadModel( 'User' );
		
		$users = $this->User->find('all',array('conditions'=>['role_type'=>[2,3,4,5],'is_deleted'=>0],'fields'=>['role_type','first_name','last_name','username']));
		foreach($users as $us){
			$allusers[$us['User']['username']] = $us['User']['first_name'].' '.$us['User']['last_name'];
		}
   		
		if(isset($this->request->query['purchaser']) && $this->request->query['purchaser'] != ''){
 			$get = $this->SupplierDetail->find('all',array('conditions' => array('SupplierDetail.purchaser LIKE' => $this->request->query['purchaser'])));
		}else{
			$get = $this->SupplierDetail->find('all'); 	
		}
		 
		$ch_file = 'Suppliers.csv'; 	
		file_put_contents(WWW_ROOT. 'logs'.DS.$ch_file, "Supplier name,Supplier code,Email, Phone number,Credit limit,Lead time,Payment mode,Min Order Amount,Credit period,Currency,Address,Purchaser,Username,Added date,Updated date\n", LOCK_EX); 	
		foreach($get as $m){
			$purchaser = '';  
			 
			if(isset($allusers[$m['SupplierDetail']['purchaser']])){
				$purchaser = $allusers[$m['SupplierDetail']['purchaser']];
			}
			 
			file_put_contents(WWW_ROOT. 'logs'.DS.$ch_file, 
 			'"'.$m['SupplierDetail']['supplier_name'].'"'.",".
 			$m['SupplierDetail']['supplier_code'].",".
			$m['SupplierDetail']['email'].",".
			$m['SupplierDetail']['phone_number'].",".
			$m['SupplierDetail']['credit_limit'].",".
			$m['SupplierDetail']['lead_time'].",".
 			$m['SupplierDetail']['payment_mode'].",".
			$m['SupplierDetail']['min_order_amount'].",".
			$m['SupplierDetail']['credit_period'].",".
			$m['SupplierDetail']['currency'].",".
			'"'.$m['SupplierDetail']['address'].'"'.",".
			$purchaser.",".
			$m['SupplierDetail']['added_by'].",".
			$m['SupplierDetail']['added_date'].",".
			$m['SupplierDetail']['updated_date'].","."\n", FILE_APPEND | LOCK_EX); 		
		}
 	 	
 		 
  		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.basename($ch_file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT."logs/".$ch_file));
		header("Content-Type: application/force-download");
		readfile(WWW_ROOT."logs/".$ch_file);
		exit;
	}
	public function UploadFile() {
	    $this->layout = "ajax";
		$this->loadModel( 'SupplierDetail' ); 
		$this->loadModel( 'SupplierMapping' ); 
		$extensions  = array('csv');
 		$a = 0;  $u = 0;  $msg['msg'] = '';
		if(isset($_FILES["supplierfile"]))
		{ 
			$errors = array();
			$success = array();
			$warnings = array();
			
			if ($_FILES["supplierfile"]["error"] > 0)
			{
			  $errors[] = 'Error: ' . $_FILES["supplierfile"]["error"];
			}
			else
			{
					$pathinfo = pathinfo($_FILES["supplierfile"]["name"]);
		
					if (array_key_exists('filename', $pathinfo))
						$fileNameWithoutExt = $pathinfo['filename'];
					if (array_key_exists('extension', $pathinfo))
						$fileExtension = strtolower($pathinfo['extension']);
					
					
					if(in_array($fileExtension,$extensions)){
					
							$csvFile = $_FILES["supplierfile"]["tmp_name"];						
							$handle = fopen($csvFile, 'r');
							$cols   = array_flip(fgetcsv($handle));
							$dateTime = date('Y-m-d H:i:s');
							$r=1;
							$ch_sp = $this->SupplierDetail->find('all');	
							if(count($ch_sp) > 0){			
								foreach($ch_sp as $v) {
									$suppliers[] = $v['SupplierDetail']['supplier_code'];	
								}
							}
		
 							while($data = fgetcsv($handle))
							{	
								$r ++;				
								
								if(trim($data[$cols['Our Code']]) == '') {						
									$errors[] =  $data[$cols['Our Code']]." Invalid Master SKU in the Row $r!" ;
								}
 								if(trim($data[$cols['Supplier Code']])!= '') {
 														
									if(!in_array(trim($data[$cols['Supplier Code']]),$suppliers)) {						
										$errors[] = $data[$cols['Supplier Code']]." # $r Row have Invalid Supplier Code,Please check in Supplier list!" ;
									}														
									$rowdata[] = array(	'master_sku' =>trim($data[$cols['Our Code']]),
														'supplier_sku' =>trim($data[$cols['Supplier Sku']]),
														'supplier_code' =>trim($data[$cols['Supplier Code']]),
														'pack_size' =>trim($data[$cols['Pack Size']]),
														'case_size' =>trim($data[$cols['Case size']]),
														'individual_item_price' =>trim($data[$cols['Per unit price']]),
														'case_price' =>trim($data[$cols['Case price']])
													);
													
								 } 
							 }
							
							if(empty($errors)) 
							{
								$a = 0;  $u = 0; 
								foreach($rowdata as $k => $dataLine) {					
									 
									$ch_sp = $this->SupplierMapping->find('first',array('conditions' => array('supplier_code' => $dataLine['supplier_code'],'master_sku' => $dataLine['master_sku'])));	 
				
								if(count($ch_sp) > 0){	
 									 									
										if( $dataLine['supplier_sku'] !=''){
										
										//		if(array_key_exists('update',$cols) && $cols['update'] != ''){ 
													$u++;					 			
													$sql = "UPDATE `supplier_mappings` SET `supplier_sku` = '".$dataLine['supplier_sku']."',
													`supplier_sku` = '".$dataLine['supplier_sku']."',
													`pack_size` = '".$dataLine['pack_size']."',
													`case_size` = '".$dataLine['case_size']."',
													`case_price` = '".$dataLine['case_price']."',
													`individual_item_price` = '".$dataLine['individual_item_price']."'
													WHERE `master_sku` = '".$dataLine['master_sku']."' AND `supplier_code` = '".$dataLine['supplier_code']."' ";
													$this->SupplierMapping->query($sql) ;	
											 
												/*}else{
													$sql = "SELECT master_sku FROM `supplier_mappings` WHERE `supplier_sku` = '".$dataLine['supplier_sku']."' "	;
													$result =  $this->SupplierMapping->query($sql) ;
													if(count($result) > 0){	
 														$errors[] = "This  Supplier SKU is already associated with:".$result['SupplierMapping']['master_sku'];					
													} 
												}*/
											 
										}								
									}else{							
										$a++;					 			
										$sql = "INSERT INTO `supplier_mappings` (`supplier_code`, `master_sku`, `supplier_sku`,`pack_size`,`case_size`,`case_price`,`individual_item_price`,`added_date`,`username`) 
													VALUES ('".$dataLine['supplier_code']."','".$dataLine['master_sku']."','".$dataLine['supplier_sku']."','".$dataLine['pack_size']."','".$dataLine['case_size']."','".$dataLine['case_price']."','".$dataLine['individual_item_price']."','".date('Y-m-d H:i:s')."','". $this->Session->read('Auth.User.username')."');";
										$result =  $this->SupplierMapping->query($sql) ;
									}								
								}						
								$date  = date('Ymd_His');					
								move_uploaded_file($_FILES["supplierfile"]["tmp_name"],'files/sup_'. $fileNameWithoutExt.$date.'.'.$fileExtension); 
								//$success[] = $_FILES["supplierfile"]["name"].' File Uploaded.';
							}
								 
					 }else{		 	
						$errors[] = 'Invalid file type. We accept only CSV file!';
					}
			}
			
			
			if($a){
				$success[] =  '#'.$a.' Record added.';
			}
			if($u){ 
				$success[] =  '#'.$u.' Record Updated.';
			}
			
			if(count($success)>0){
				$success_msg = '';
				foreach($success as $sLine) {					
					$success_msg .= $sLine."\n";
				} 
				$msg['error'] = '';			
				$msg['msg'] .= '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$success_msg.'</div>';
			}	
			if(count($warnings)>0){
				$warnings_msg = '';
				foreach($warnings as $wLine) {					
					$warnings_msg .= $wLine."\n";
				} 
				$msg['error'] = 'yes';
				$msg['msg'] .= '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$warnings_msg.'</div>';
			}	
			if(count($errors)>0){
				$errors_msg = '';
				foreach($errors as $eLine) {					
					$errors_msg .= $eLine."<br>";
				} 
				$msg['error'] = 'yes';			
				$msg['msg'] .= '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$errors_msg.'</div>';
			}	
				
					
			//echo json_encode($msg);
		
		}
		 $this->Session->setflash( $msg['msg'], 'flash_success' );                      
         $this->redirect($this->referer());
		exit;
	}
	
	public function ajaxMapSupplier() 
    {
		$this->layout = "ajax";
		$this->loadModel( 'SupplierMapping' ); 
	  	/*$this->request->data['supplier_sku']
		$this->request->data['master_sku']
		$this->request->data['supplier_code']*/
 		 
		try{
			$ch_sp = $this->SupplierMapping->find('first',array('conditions' => array('supplier_code' => $this->request->data['supplier_code'],'supplier_sku' => $this->request->data['supplier_sku'],'master_sku' => $this->request->data['master_sku'])));	 
			
  			if(count($ch_sp) == 0){
				$this->SupplierMapping->saveAll($this->request->data);	
				$msg['msg'] = 'Supplier sku mapped successfully!';
			}else{
				$msg['msg'] = 'Supplier sku already exists!';
				$msg['error'] = 'yes';
			}
		}catch(Exception $e) {
  			$msg['msg'] = 'Message: ' .$e->getMessage();
		}
		echo json_encode($msg);
 		exit;
	}
	
	public function getSupplierSku($code,$msku){
		
		$this->loadModel( 'SupplierMapping' ); 
			
 		$result =  $this->SupplierMapping->find('first',array('conditions' => array('supplier_code' => $code,'master_sku' => $msku)));	
		if(count($result)> 0){						
			 
			if($result['SupplierMapping']['supplier_sku']){	
				return	array('supplier_sku'=>$result['SupplierMapping']['supplier_sku'], 'id'=>$result['SupplierMapping']['id']);
			}else{
				return array('supplier_sku'=>'NoSKU', 'sm_id' => $result['SupplierMapping']['id']);
			}
			
		}else{
			return array('supplier_sku'=>'-', 'id'=>0);
		}
		exit;
	}
	
	public function SaveSupplierContact() 
    {
		$this->layout = "ajax";
 		$this->loadModel('SupplierContact'); 
		
 		try{
		
		$dataArray = array();
		foreach($this->request->data['values'] as $val){
			$data = explode("=",$val);
			$dataArray[$data[0]] = $data[1];			
		} 
		if($dataArray['designation'] == ''){
			$msg['msg'] = 'Designation is Empty!';
		}elseif($dataArray['contact_person_name'] == ''){
			$msg['msg'] = 'Contact Person Name is Empty!';
		}else{
 		 
				$dataArray['username']      = $this->Session->read('Auth.User.username');
 				$dataArray['supplier_id']   = $this->request->data['id']; 
				if(isset($this->request->data['inc_id'])){
					$ch_sp = $this->SupplierContact->find('first',array('conditions' => array('id' => $this->request->data['inc_id'])));	
				}else{
					$ch_sp = $this->SupplierContact->find('first',array('conditions' => array('supplier_id' => $this->request->data['id'],'contact_person_name' => $dataArray['contact_person_name'],'phone' => $dataArray['phone'],'department' => $dataArray['department'])));	
				}
				 
				if(count($ch_sp) > 0){
					$dataArray['id']   = $ch_sp['SupplierContact']['id'];
 					if($this->SupplierContact->saveAll($dataArray)){
						$msg['msg'] = 'Supplier Contact is updated successfully!';
					}
 				}else{
 					$dataArray['added_date']      = date('Y-m-d H:i:s');
					if($this->SupplierContact->saveAll($dataArray)){
						$msg['msg'] = 'Supplier Contact added successfully!';
					}
 				} 
 			}
		}catch(Exception $e) {
  			$msg['msg'] = 'Message: ' .$e->getMessage();
		}
		 
		echo json_encode($msg);
 		exit;
	}
	
	public function getSupplierContact() 
    {
		$this->layout = "ajax";
 		$this->loadModel('SupplierContact'); 
		$html = ''; $msg =[];
 		try{
  			
			$ch_sp = $this->SupplierContact->find('all',array('conditions' => array('supplier_id' => $this->request->data['id'])));	 
			if(count($ch_sp) > 0){
				$html  = '<table class="table table-striped" style="border: 1px #000 solid;">
						  <thead>
							<tr>
								<th width="15%">Designation</th>
								<th>Department</th>
								<th>Contact Person</th>
								<th>Email</th>
								<th>Phone</th>
								<th>Action</th> 
							</tr>
 						  </thead>';
				foreach($ch_sp as $v){
					$html  .='<tr>
								<td>'.$v['SupplierContact']['designation'].'</td>
								<td>'.$v['SupplierContact']['department'].'</td>
								<td>'.$v['SupplierContact']['contact_person_name'].'</td>
								<td>'.$v['SupplierContact']['email'].'</td>
								<td>'.$v['SupplierContact']['phone'].'</td> 
								<td><span onclick="edit('.$v['SupplierContact']['id'].','.$this->request->data['id'].')" style="cursor: pointer;color: blue;">Edit</span> | <a href="#"  style="color: blue;" onclick="getDelete('.$v['SupplierContact']['id'].','.$this->request->data['id'].')">Delete</a></td> 
							</tr>';
							
					 
				}
				 $html  .= '</tbody>
						</table>';
				$msg['html'] = $html;
 			}
 			 
		}catch(Exception $e) {
  			$msg['msg'] = 'Message: ' .$e->getMessage();
		}
		 
		echo json_encode($msg);
 		exit;
	}
	
	public function getDelete() 
    {
		$this->layout = "ajax";
		$this->loadModel( 'SupplierContact' ); 
		try{
			$ch_sp = $this->SupplierContact->find('first',array('conditions' => array('id' => $this->request->data['id'])));	 
			if(count($ch_sp) > 0){
				file_put_contents(WWW_ROOT .'logs/supplier_contacts_del_'.date('ymd').'.log',$this->Session->read('Auth.User.username')."\t".date('y-m-d H:i:s').print_r($ch_sp,true)."\r\n", FILE_APPEND | LOCK_EX);	
				$id  = $this->request->data['id'];
				$sql = "DELETE FROM `supplier_contacts` WHERE `id` = {$id}";
				$this->SupplierContact->query($sql);	 
				$msg['msg'] = 'Supplier contact  deleted!';
			}else{
				$msg['msg'] = 'Supplier contact not nound!';
				$msg['error'] = 'yes';
			}
		}catch(Exception $e) {
  			$msg['msg'] = 'Message: ' .$e->getMessage();
		}
		echo json_encode($msg);
 		exit;
	}
	public function getContact() 
    {
		$this->layout = "ajax";
 		$this->loadModel('SupplierContact'); 
		$data = [];
 		try{
  			
			$v = $this->SupplierContact->find('first',array('conditions' => array('id' => $this->request->data['id'])));	 
			if(count($v) > 0){
  				$data['designation'] = $v['SupplierContact']['designation'];
				$data['department'] = $v['SupplierContact']['department'];
				$data['contact_person_name'] = $v['SupplierContact']['contact_person_name'];
				$data['email'] = $v['SupplierContact']['email'];
				$data['phone'] = $v['SupplierContact']['phone']; 
				$data['button'] = '<button type="button" class="btn btn-warning" 
				onclick="UpdateContact('.$v['SupplierContact']['id'].','.$v['SupplierContact']['supplier_id'].');"><i class="glyphicon glyphicon-check"></i>&nbsp;Update</button>';
   			}
 			 
		}catch(Exception $e) {
  			$data['msg'] = 'Message: ' .$e->getMessage();
		}
		 
		echo json_encode($data);
 		exit;
	}
	
	public function paginate_function($item_per_page, $current_page, $total_records, $total_pages,$data_name=false,$data_value=false,$sort_by=false)
	{
		$pagination = '';
		if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
			$pagination .= '<ul class="pagination">';
			
			$right_links    = $current_page + 3; 
			$previous       = $current_page - 3; //previous link 
			$next           = $current_page + 1; //next link
			$first_link     = true; //boolean var to decide our first link
			
			if($current_page > 1){
				$previous_link = ($previous > 0)? $previous : 1;
				$pagination .= '<li class="first"><a href="#" data-page="1" data-name="'.$data_name.'" order-by="'.$sort_by.'" data-value="'.$data_value.'" title="First">&laquo;</a></li>'; //first link
				$pagination .= '<li><a href="#" data-page="'.$previous_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Previous">&lt;</a></li>'; //previous link
					for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
						if($i > 0){
							$pagination .= '<li><a href="#" data-page="'.$i.'"  data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Page'.$i.'">'.$i.'</a></li>';
						}
					}     
				$first_link = false; //set first link to false
			} 
			
			if($first_link){ //if current active page is first link
				$pagination .= '<li class="first active"><span>'.$current_page.'</span></li>';
			}elseif($current_page == $total_pages){ //if it's the last active link
				$pagination .= '<li class="last active"><span>'.$current_page.'</span></li>';
			}else{ //regular current link
				$pagination .= '<li class="active"><span>'.$current_page.'</span></li>';
			}
					
			for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
				if($i<=$total_pages){
					$pagination .= '<li><a href="#" data-page="'.$i.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Page '.$i.'">'.$i.'</a></li>';
				}
			}
			if($current_page < $total_pages){ 
					$next_link = ($i > $total_pages) ? $total_pages : $i;
					$pagination .= '<li><a href="#" data-page="'.$next_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'" data-value="'.$data_value.'" title="Next">&gt;</a></li>'; //next link
					$pagination .= '<li class="last"><a href="#" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
			}
			
			$pagination .= '</ul>'; 
		}
		return $pagination; //return pagination links
	}
    
}

?>
