<?php

class StaffOrdersController extends AppController
{
    var $name = "StaffOrders";
	var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
	public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->Auth->Allow(array('update_amazon_order_id'));
	   $this->GlobalBarcode = $this->Components->load('Common'); 
	}
	
	public function index( $sku = null )
    {
		$this->layout = 'index';
		$this->loadModel('User');
		$this->loadModel('StaffOrder');
		 
		 
		$this->paginate = array('group' => 'order_id','order'=>'id DESC','limit' => 50);
		$getStaffOrders = $this->paginate('StaffOrder');
		$order_ids = [];
		foreach($getStaffOrders as $v){
			$order_ids[] = $v['StaffOrder']['order_id'];
		}
		
		$staffOrders = [];
		$staffOrder = $this->StaffOrder->find('all', array( 'conditions' => array( 'order_id' => $order_ids ) ) );
		foreach($staffOrder as $v){
			$staffOrders[$v['StaffOrder']['order_id']][] = $v;
		}	
		
		$users = $this->User->find('all', array('conditions' => array('country' =>[1,3]),'fields' => array('id','first_name','last_name','username','email','country'),'group' => 'email'));	
		//$this->set( 'getStaffOrders', $getStaffOrders );
		$this->set( compact('getStaffOrders','users','staffOrders') );
	}
	 
	
	public function checkProduct()
	{
		$this->layout = '';
		$this->loadModel('Product');		
		$this->loadModel('PurchaseOrder');
		$this->loadModel('StaffTempOrder');
 		 
		$data = [];
		$product_barcode = trim($this->request->data['product_barcode']);
 	 	$product_global_barcode = $this->GlobalBarcode->getGlobalBarcode($product_barcode);
		$checkSku =	$this->Product->find('first', array( 'conditions' => array( 'ProductDesc.barcode LIKE' => $product_global_barcode ),'fields'=>['Product.product_name','Product.current_stock_level','Product.product_sku','ProductDesc.barcode'] ) );
	 
		if(count($checkSku) > 0){
 		 
 			if(substr($checkSku['Product']['product_sku'],0,2) == 'B-'){
				$data['msg'] = 'You can not purchase Bundle Items.'; 
			}else{
				$getPurchaseDetail	= $this->PurchaseOrder->find('first', array('conditions'=> array('PurchaseOrder.purchase_sku' => $checkSku['Product']['product_sku'],'PurchaseOrder.price >' => 0 ), 'order' => array('PurchaseOrder.date DESC') ) );
				
				if(count($getPurchaseDetail) > 0)
				{
					$data['product_name'] = $checkSku['Product']['product_name'];
					$data['product_sku']  = $checkSku['Product']['product_sku'];	
					$data['current_stock']= $checkSku['Product']['current_stock_level'];
					$data['price'] 		  = $getPurchaseDetail['PurchaseOrder']['price']; 
					$data['po_name']      = $getPurchaseDetail['PurchaseOrder']['po_name'] ; 
					
					if(isset($_SESSION['staff_order'][$product_barcode])){
						if(isset($_SESSION['staff_order'][$product_barcode]['qty'])){
							$qty = $_SESSION['staff_order'][$product_barcode]['qty'] + 1;
							if($qty > $checkSku['Product']['current_stock_level']){
								$data['msg'] = 'You can not purchase more than current stock.'; 
							}else{ 
								if( $_SESSION['staff_order'][$product_barcode]['qty'] < 3){
									$data['qty'] = $_SESSION['staff_order'][$product_barcode]['qty'] + 1; 								
								}else{
									$data['qty'] = 3;
									$data['msg'] = 'You can not purchase more than 3 qty.';
								} 
							}
						}else{
							$data['qty'] = 1;
						} 
					} 
 					$_SESSION['staff_order'][$product_barcode] = $data;
				}else{
					$data['msg'] = 'Purchase price not found of this item.'; 
				}
			} 
 		} 
		$html = '<tr>				
				<th>Poduct Barcode</th>
				<th>Poduct SKU</th>
				<th width="40%">Poduct Title</th>
				<th width="8%">Unit Price</th> 
				<th width="8%">Quantity</th> 
				<th width="8%">Current Stock</th> 
				<th width="8%">Total</th> 
				<th width="8%">Action</th>
			</tr>';
			
			if(isset($_SESSION['staff_order'])){
		foreach($_SESSION['staff_order'] as $_barcode => $v){ 
		
			$selected_1 = 'selected="selected"';
			$selected_2 = $selected_3 =''; $qty =1;
			if(isset($v['qty']) && $v['qty'] == 1){
				$selected_1 = 'selected="selected"';
				$qty = 1;
			}else if(isset($v['qty']) && $v['qty'] == 2){
				$selected_2 = 'selected="selected"';
				$qty = 2;
			}else if(isset($v['qty']) && $v['qty'] == 3){
				$selected_3 = 'selected="selected"';
				$qty = 3;
			}
		
			 $html .= '<tr>				
				<td>'.$_barcode.'</td>
				<td>'.$v['product_sku'].'</th>
				<td>'.$v['product_name'].'</td> 
				<td id="price_'.$_barcode.'">'.$v['price'].'</td> 
				<td id="qty_'.$_barcode.'"><select name="quantity" id="quantity_'.$_barcode.'" class="form-control" onchange="updateQty('.$_barcode.')"><option value="1" '.$selected_1.'>1</option><option value="2" '.$selected_2.'>2</option><option value="3" '.$selected_3.'>3</option></select></td> 
				<td id="current_stock_'.$_barcode.'">'. $v['current_stock'].'</td>
				<td id="total_'.$_barcode.'">'. number_format( $qty*$v['price'] ,2).'</td>
				<td><button type="button" class="btn btn-danger btn-sm" onclick="removeItem('.$_barcode.');">Remove Item</button></td>
			</tr>';
		}
		} 
		$html .= '<tr>
				<td colspan="8" style="text-align: right;"><button type="button" class="btn btn-success btn-sm" onclick="createOrder();">Create Order</button></td>
			</tr>';	
		$data['html'] = $html;    
		echo json_encode($data);
		exit;
		
  	}
	
	public function updateQty()
	{
		$this->layout = '';
		$product_barcode = $this->request->data['barcode'] ;
		$data = [];
		if($this->request->data['quantity'] > $this->request->data['current_stock']){
			$data['msg'] = 'You can not purchase more than current stock.';
		}else{ 
 			$_SESSION['staff_order'][$product_barcode]['qty'] = $this->request->data['quantity'];
			$total = number_format($this->request->data['quantity'] * $this->request->data['price'],2);
			$data['total'] = $total;
			$data['msg'] = '';
		}
 		echo json_encode($data); 
		exit;
  	}
	
	public function removeItem()
	{
		$this->layout = '';
		if(isset($this->request->data['barcode'])){
  			unset($_SESSION['staff_order'][$this->request->data['barcode']]);
		}
		$data = []; 
		if(isset($_SESSION['staff_order'])){	  
		$html = '<tr>				
				<th>Poduct Barcode</th>
				<th>Poduct SKU</th>
				<th width="40%">Poduct Title</th>
				<th width="8%">Unit Price</th> 
				<th width="8%">Quantity</th> 
				<th width="8%">Current Stock</th> 
				<th width="8%">Total</th> 
				<th width="8%">Action</th>
			</tr>';
		foreach($_SESSION['staff_order'] as $_barcode => $v){ 
			$selected_1 = 'selected="selected"';
			$selected_2 = $selected_3 =''; $qty =1;
			if(isset($v['qty']) && $v['qty'] == 1){
				$selected_1 = 'selected="selected"';
				$qty = 1;
			}else if(isset($v['qty']) && $v['qty'] == 2){
				$selected_2 = 'selected="selected"';
				$qty = 2;
			}else if(isset($v['qty']) && $v['qty'] == 3){
				$selected_3 = 'selected="selected"';
				$qty = 3;
			}
			 $html .= '<tr>				
				<td>'.$_barcode.'</td>
				<td>'.$v['product_sku'].'</th>
				<td>'.$v['product_name'].'</td> 
				<td id="price_'.$_barcode.'">'.$v['price'].'</td> 
					<td id="qty_'.$_barcode.'"><select name="quantity" id="quantity_'.$_barcode.'" class="form-control" onchange="updateQty('.$_barcode.')"><option value="1" '.$selected_1.'>1</option><option value="2" '.$selected_2.'>2</option><option value="3" '.$selected_3.'>3</option></select></td> 
				<td id="current_stock_'.$_barcode.'">'. $v['current_stock'].'</td>
				<td id="total_'.$_barcode.'">'. number_format( $qty*$v['price'] ,2).'</td> 
				<td><button type="button" class="btn btn-danger btn-sm" onclick="removeItem('.$_barcode.');">Remove Item</button></td>
			</tr>';
		}
		$html .= '<tr>
				<td colspan="8" style="text-align: right;"><button type="button" class="btn btn-success btn-sm" onclick="createOrder();">Create Order</button></td>
			</tr>';	
		$data['html'] = $html;    
		}
		echo json_encode($data);
		exit;
  	}
	
	public function createOrder()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Product');		
		$this->loadModel('PurchaseOrder');
		
		$today = date("YmdHis");
		$rand ='';
		for($i=0; $i < 2; $i++){
			$rand .= mt_rand(0, 9);
		}
 		$order_id	 = $today.$rand;
		
		foreach($_SESSION['staff_order'] as $_barcode => $v){ 
 			$data = [];
			$data['order_id']    = $order_id;	
			$data['quantity']	 = isset($v['qty']) ? $v['qty'] : 1;					
			$data['sku']     	 = $v['product_sku'];		 
			$data['barcode']  	 = $_barcode;
			$data['unit_price']	 = $v['price'];	
			$data['product_title']	 = $v['product_name'];	
			$data['staff_person_email']	 = trim($this->request->data['staff_person_email']);
			$this->manageQuantity($data);
 		}
		unset($_SESSION['staff_order']);
		
		$data['msg'] = 'Order id is # '.$order_id;
 		echo json_encode($data);
		exit;
		
		
	}
	
	public function ReturnOrder($order_id = 0)
	{
			$this->layout = '';
			$this->autoRender = false;
			
			if( isset($this->request->data['order_id']) && $this->request->data['order_id'] > 0 )
			{
				$order_id	= $this->request->data['order_id'];
			}
			 
 			$this->loadModel('Product'); 
			$this->loadModel('BinLocation');		 
			$this->loadModel('CheckIn');
			$this->loadModel('StaffOrder');
			
 			$orders = $this->StaffOrder->find('all', array('conditions' => array('StaffOrder.order_id' => $order_id,'StaffOrder.status' => 'active')));
		 
  			$mailFormate = '';
 			if(count($orders) > 0){
			 		
 					foreach($orders as $val)
					{
 						$barcode 	= $val['StaffOrder']['barcode'];
						$sku		= $val['StaffOrder']['sku'];
						$location 	= $val['StaffOrder']['bin_location'];
						$quantity 	= $val['StaffOrder']['quantity'];
						$qty_bin 	= $val['StaffOrder']['available_qty_bin']; 
						$po_id	 	= explode(",",$val['StaffOrder']['po_id'])[0];	
								
 						if($location != 'No Location')
						{
							$this->BinLocation->updateAll(	array('BinLocation.stock_by_location' => "BinLocation.stock_by_location + $qty_bin"),
														array('BinLocation.bin_location' => $location, 'BinLocation.barcode' => $barcode));
						}					
						 
						$this->CheckIn->updateAll( array('CheckIn.selling_qty' => "CheckIn.selling_qty - $qty_bin"),array('CheckIn.id' => $po_id));							  
						$product =	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
						// look for sku if exist  
						if( count( $product ) > 0 )
						{
							$ordertype = 'Delete';
							
							$currentStickLevel	=	$product['Product']['current_stock_level'];
							$qtyForUpdate		=	(int)$currentStickLevel + (int)$qty_bin;								
							
							$data['Product']['product_sku']		=	$product['Product']['product_sku'];
							$data['ProductDesc']['barcode']		=	$product['ProductDesc']['barcode'];
							$data['Product']['CurrentStock']	=	$currentStickLevel;
							
							$productData	=	json_decode(json_encode($data,0));
							App::import('Controller', 'Cronjobs');
							$productCant = new CronjobsController();
							$productCant->storeInventoryRecord( $productData, $qty_bin, $order_id, $ordertype, $barcode);
							
							$this->Product->updateAll(array('Product.current_stock_level' => $qtyForUpdate), array('Product.product_sku' => $sku));
 								
						}
						$this->StaffOrder->updateAll( array( 'StaffOrder.status' =>"'return'") , array( 'StaffOrder.order_id' => $order_id,'barcode'=>$val['StaffOrder']['barcode'] ) );
						
						$mailFormate.='<tr>											 
						<td>'.$order_id.'</td>
						<td>'.$sku.'</td>
						<td>'.$product['Product']['product_name'].'</td>
						<td>'.$product['ProductDesc']['barcode'].'</td>
						<td>'.$location.'</td>
						<td>'.$qty_bin.'</td>
						</tr>';		
					}
				
			   }
			  if($mailFormate != ''){ 
			 	 $mailBoday = '<table border="1" style="border: 1px solid #c2c2c2;">
													<tr>
														<th>OrderID</th>
														<th>SKU</th>
														<th>Title</th>
														<th>Barcode</th>
														<th>Location</th>
														<th>Quantity</th>
													</tr>'.$mailFormate;
													
					App::uses('CakeEmail', 'Network/Email');
					$email = new CakeEmail('');
					$email->emailFormat('html');
					$email->from('info@euracogroup.co.uk');
					$email->to( array('avadhesh.kumar@jijgroup.com'));					
					$email->subject('Xsensys : Staff Order returned');
					$email->send( $mailBoday );
			}
				 							
   	  }
	
 	public function manageQuantity($data = array()){
		
	 
		$this->loadModel('InventoryRecord');
		$this->loadModel('BinLocation');	
		$this->loadModel('StaffOrder');	
		$this->loadModel('CheckIn');
		
		if(count($data) > 0){
			 
			$order_id    = $data['order_id'];	
			$quantity 	 = $data['quantity'];					
			$sku     	 = $data['sku'];			
			$barcode  	 = $data['barcode'];
			$unit_price  = $data['unit_price'];
			$product_title	 = $data['product_title'];	
			$staff_person_email  =$data['staff_person_email']	;
			$po_name = array(); $posIds = array(); $pos = array(); $binname = array(); 
			$available_qty = 0; $available_qty_bin = 0;	
			
			$poDetail = $this->CheckIn->find( 'all', 
				array( 'conditions' => array( 'CheckIn.barcode' => $barcode, 'CheckIn.selling_qty != CheckIn.qty_checkIn','po_name !=""'),
						'order' => 'CheckIn.date  ASC ' ) );
			
			if(count($poDetail) > 0){
				$qt_count = 0; 				
				foreach($poDetail as $po){
					$poQty = $po['CheckIn']['qty_checkIn'] - $po['CheckIn']['selling_qty'];
					if($poQty > 0){						
						 for($i=0; $i <= $quantity ;$i++){						 
							 if($qt_count >= $quantity ){
								 break;
							 }else if(($poQty - $i)  > 0){							
								$po_name[$po['CheckIn']['id']][] = $po['CheckIn']['po_name'];	
								$available_qty++;							
								$qt_count++;
							}												
						 }						
					}
				}
			}
			 
			
			$getStock = $this->BinLocation->find( 'all', array( 'conditions' => array( 'BinLocation.barcode' => $barcode),'order' => 'priority ASC' ) );
			//pr($getStock );
			$bin_name = array();
			$finalData['bin_location'] = '';
			if(count($getStock) > 0){
				$qt_count = 0 ;
				foreach($getStock as $val){
					 $binData['id'] = $val['BinLocation']['id'];					
					 for($i=0; $i <= $quantity ;$i++){						 
						 if($qt_count >= $quantity ){
							 break;
						 }else if(($val['BinLocation']['stock_by_location'] - $i)  > 0){							
							$bin_name[$val['BinLocation']['id']][] = $val['BinLocation']['bin_location'];	
							$available_qty_bin++;						
							$qt_count++;
						}												
					 }							
				}				
			}							
						
			
			if(count($po_name) > 0){
				foreach(array_keys($po_name) as $k){
					$qts   = count($po_name[$k]);	
					$pos[] = $po_name[$k][0];
					$posIds[] = $k;
					$this->CheckIn->updateAll( array('CheckIn.selling_qty' =>  "CheckIn.selling_qty + $qts"),array('CheckIn.id' => $k));
				}
			}
							
			if(count($bin_name) > 0)
			{
				foreach(array_keys($bin_name) as $k){					
					$qts = count($bin_name[$k]);	
					$binname[] = $bin_name[$k][0];	
					$finalData = array();						 
					$finalData['available_qty_check_in'] = $available_qty;
					$finalData['available_qty_bin'] = $qts;
					$finalData['order_id']    	    = $order_id;	
					$finalData['quantity']      	= $quantity;			
					$finalData['sku']	   			= $sku;	
					$finalData['unit_price']	   	= $unit_price;	 							
					$finalData['barcode']  			= $barcode;	
					$finalData['product_title']  	= $product_title;	
					$finalData['po_name']  			= implode(",",$pos);	
					$finalData['po_id']    			= implode(",",$posIds);	
					$finalData['bin_location']  	= $bin_name[$k][0];	
					$finalData['staff_person_email']=$staff_person_email;
					$finalData['created_by_name'] 	= $this->Session->read('Auth.User.username').'_'.$this->Session->read('Auth.User.pc_name') ;
					$finalData['created_by_userid'] = $this->Session->read('Auth.User.id') ;					
					$finalData['created_date'] 		= date('Y-m-d H:i:s');		
					$this->StaffOrder->saveAll( $finalData );
				
					$this->BinLocation->updateAll( array('BinLocation.stock_by_location' =>  "BinLocation.stock_by_location - $qts"),array('BinLocation.id' => $k));
					
					/**
					Updating current stock of product.
					*/
					$current_qty = 0;
					$_product = $this->Product->find('first',array('conditions'=>array('Product.product_sku' => $sku), 'fields' => array('current_stock_level'),'order'=>'Product.id DESC'));
					
					if(count($_product) > 0){
					
						$current_stock = $_product['Product']['current_stock_level'];														
						$current_qty   = $_product['Product']['current_stock_level'] - $qts;
						if($current_qty < 0){
							$current_qty = 0;
						}
						
						$this->Product->updateAll( array('Product.current_stock_level' => $current_qty),array('Product.product_sku' => $sku));
						 
						
						$inv_data = array();
						$inv_data['action_type']  	 	= 'Staff order';
						$inv_data['sku'] 		  	 	= $sku;					
						$inv_data['barcode']	  	 	= $barcode;
						$inv_data['currentStock'] 		= $current_stock;
						$inv_data['quantity'] 	   		= $qts;
						$inv_data['status'] 	   		= 'staff_order';
						$inv_data['after_maniplation'] 	= $current_qty;
						$inv_data['location'] 			= $bin_name[$k][0];
						$inv_data['split_order_id']		= $order_id;
						$inv_data['date'] 		   		= date('Y-m-d' );
						$this->InventoryRecord->saveAll( $inv_data );
						 
 					}
					// end of stock updation.						
 				}
			} 
			else {
					$finalData = array();
					$finalData['available_qty_check_in'] = $available_qty;
					$finalData['available_qty_bin'] = '0';
					$finalData['order_id']    	    = $order_id;	
					$finalData['quantity']      	= $quantity;	
					$finalData['sku']	   			= $sku;		
					$finalData['unit_price']	   	= $unit_price;							
					$finalData['barcode']  			= $barcode;	
					$finalData['product_title']  	= $product_title;
					$finalData['po_name']  			= implode(",",$pos);	
					$finalData['po_id']    			= implode(",",$posIds);	
					$finalData['bin_location']  	= 'No_Location';		
					$finalData['staff_person_email']=$staff_person_email;	
					$finalData['created_by_name'] 	= $this->Session->read('Auth.User.username').'_'.$this->Session->read('Auth.User.pc_name') ;
					$finalData['created_by_userid'] = $this->Session->read('Auth.User.id') ;	
					$finalData['created_date'] 		= date('Y-m-d H:i:s');			
					$this->StaffOrder->saveAll( $finalData );
			}
			
			
		}					
	}
	
	
	    
}

?>
