<?php
//error_reporting(0);
class TestdpController extends AppController
{
    
    var $name = "Testdp";
    
    var $components = array('Session','Upload','Common','Auth');
    var $helpers = array('Html','Form','Common','Session');
	public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->Auth->Allow(array('index'));
	   $this->GlobalBarcode = $this->Components->load('Common'); 
	}
	
    public function index()
    {/*
		//echo " Welcome in test Controller.";
		
		//require_once(APP . 'Vendor' . DS.'pdf_merge' . DS .'vendor' . DS .'autoload.php');
		//App::import('Vendor', array('file' => 'autoload'));
		require APP . 'Vendor' . DS.'pdf_merge' . DS .'vendor' . DS .'autoload.php';
		
		use iio\libmergepdf\Merger; 
		
		$merger = new Merger;
		$merger->addIterator(['A.pdf', 'B.pdf']);
		$createdPdf = $merger->merge();
		
		file_put_contents( 'maa.pdf', $createdPdf);
		
		exit;
	*/}
	
	  
	public function genrateSlipLabel($split_order_id = '2660473-1',$picklist_id = 0 )
	{ 			
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		$this->loadMOdel( 'CategoryContant' );
		$this->loadMOdel( 'Template' );
		$this->loadMOdel( 'BulkLabel' );
		$this->loadModel( 'MergeUpdate');
		$this->loadModel( 'PackagingSlip');
		$this->loadModel( 'DynamicPicklist' ); 
		
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper('A4', 'portrait');
		
		
		$filename = WWW_ROOT.'jerseypost/labels/'.$split_order_id.'.jpg';
		$degrees = 270;  		
		$source = imagecreatefromjpeg($filename);		
		$rotate = imagerotate($source, $degrees, 0);		
		$r_label = WWW_ROOT .'jerseypost/labels/rotate_'.$split_order_id.'.jpg';
 		imagejpeg($rotate,$r_label);
		// Free the memory
		imagedestroy($source);
		imagedestroy($rotate); 
 		
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
							 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
							 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
							 <meta content="" name="description"/>
							 <meta content="" name="author"/><table width="100%" style="height:1095px;"> 
						<tr><td style="padding-top:20px; padding:5px 0px 5px 5px; height:695px;" colspan="2" valign="top" ><head><meta charset="UTF-8"><title>Invoice</title></head><style>*{font-family:Verdana; font-size:12px;}</style><table border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0px auto;"><tr><td style="font-size:14px; font-weight:bold; padding:3px 2px; text-align:left">Slip/Invoice </td><td style="font-size:14px; font-weight:bold; padding:3px 2px; text-align:right">IOSS:123456789</td>  </tr><tr><td valign="middle" align="left" width="50%"><p style="font-size:11px ;font-weight:bold;">Test</p><p>53 AVENUE<br>DE L<br>EUROPE<br>37100, Tours<br>France</p></td><td valign="top" align="center" style="padding:4px;background:#dfeced;"><div  style="padding:1px;"><table><tr><td style="font-size:11px ;font-weight:bold;"> Paid</td></tr><tr><td colspan="2">Sold by <strong> EURACO GROUP LIMITED</strong><br>VAT #<strong>GB 304984295</strong></td></tr><tr><td colspan="2" width="100%" height="1px" style="width:100%; height:1px; background:#bdc9c9;"> </td></tr><tr><td> <strong>Invoice date / Delivery date</strong></td><td> 16-03-2020</td></tr><tr><td> <strong><strong>Invoice #</strong></td><td> INV-2660473</td></tr><tr><td> <strong><strong>Total payable</strong></td><td> 22</td></tr></table></div></td></tr><tr><td colspan="2" style="padding:2px 0px;"><strong>Tranction Type : </strong>Taxable Supply</td></tr><tr><td colspan="2" style="background:#ddd; height:1px;"> </td></tr><tr><td colspan="2"><table border="0" width="100%" align="center" cellpadding="0" cellspacing="0" ><tr><th style="text-align:left; font-weight:bold;">BILLING ADDRESS </th><th style="text-align:left; font-weight:bold;">DELIVERY ADDRESS</th><th style="text-align:left; font-weight:bold;">SOLD BY </th></tr><tr><td valign="top"><strong>Test</strong><br>53 AVENUE<br>DE L<br>EUROPE<br>37100, Tours<br>France</td><td valign="top"><strong>Test</strong><br>53 AVENUE<br>DE L<br>EUROPE<br>37100, Tours<br>France</td><td valign="top"><strong>EURACO GROUP LIMITED</strong><br>49 Oxford Road St Helier <br>Jersey JE2 4LJ<br><strong>GB 304984295</strong></td></tr><tr><td colspan="3" style="background:#ddd; height:1px;"> </td></tr><tr><td colspan="3"><strong>Order information</strong></td></tr><tr><td colspan="2">Order date 16-03-2020</td><td>Order # DRAFT</td></tr></table></td></tr></table><table cellpadding="0" cellspacing="0" align="center"><tr><td colspan="6" style="font-size:16px; font-weight:bold; border-top:1px solid #ddd; border-bottom:1px solid #ddd; padding:5px 3px;"> Invoice Details</td></tr><tr><th valign="top" style="text-align:left; padding:5px 2px;" width="50%"> Description </th><th valign="top" style="text-align:left; padding:5px 2px;" width="5%"> Qty </th><th valign="top" style="padding:5px 2px;" align="right" width="10%"> Unit price<br>(excl. VAT)</th><th valign="top" style="padding:5px 2px;" align="right" width="11%"> VAT rate</th><th valign="top" style="padding:5px 2px;" align="right" width="12%"> Unit price<br> (incl. VAT)</th><th valign="top" style="padding:5px 2px;" align="right" width="12%"> Item subtotal<br> (incl. VAT)</th></tr><tr style="background:#ddd;"><td style="padding:5px 2px;">De Longhi Water Filter DLSC002 (Pack of 1)</td><td style="padding:5px 2px;">1</td><td style="padding:5px 2px;" align="right">&nbsp;18.33</td><td style="padding:5px 2px;" align="right">20.00%</td><td style="padding:5px 2px;" align="right">&nbsp;22.00</td><td style="padding:5px 2px;" align="right">&nbsp;22.00</td></tr><tr style="background:#ddd;"><td style="padding:5px 2px;">Shipping Charge</td><td style="padding:5px 2px;"></td><td style="padding:5px 2px;" align="right">&nbsp;0.00 </td><td style="padding:5px 2px;" align="right">20.00%</td><td style="padding:5px 2px;" align="right">&nbsp;0.00</td><td style="padding:5px 2px;" align="right">&nbsp;0.00</td></tr><tr><td></td><td></td><td></td><td colspan="2" style="font-size:12px; font-weight:bold; padding:10px 0px;">Invoice total</td><td style="font-size:14px; font-weight:bold; padding:10px 0px;" align="right">&nbsp;22.00</td></tr><tr><td></td><td></td><td></td><td colspan="4" style="height:1px; background:#ddd;"></td></tr><tr><th> </th><th> </th><th> </th><th style="text-align:left; font-weight:bold; padding:5px 2px;">VAT rate </th><th style="font-weight:bold; padding:5px 2px;" align="right">Item subtotal<br>(excl. VAT)</th><th style="font-weight:bold; padding:5px 2px;" align="right">VAT subtotal</th></tr><tr><td></td><td></td><td></td><td style="background:#ddd;padding:5px 2px"> 20.00%</td><td style="background:#ddd;padding:5px 2px;" align="right">&nbsp;18.33</td><td style="background:#ddd;padding:5px 2px;" align="right">&nbsp;3.67</td></tr><tr><td></td><td></td><td></td><td style="padding:5px 2px; font-weight:bold;">Total</td><td style="padding:5px 2px; font-weight:bold;" align="right">&nbsp;18.33</td><td style="padding:5px 2px;  font-weight:bold;" align="right">&nbsp;3.67</td></tr><tr><td colspan="6"><div style="font-size:11px; border:1px solid; text-align:left;padding:5px;">This product is CE marked products in accordance with European health, safety, and environmental protection standards for products sold within the European Economic Area (EEA).Authorised representative European Union:Brother Internationale Industriemaschinen Gesellschaft MBH Duesseldorfer Str. 7-9, D-46446 Emmerich am Rhein, Germany TEL: +49-(0)2822-6090 FAX: +49-(0)2822-60950</div></td></tr></table></td></tr> 
						<tr><td style="padding:2px;valign:bottom;" valign="bottom"><img src="'.$r_label.'" width="540px"></td><td></td></tr>
						</table><style>
@font-face {
  font-family: \'arialuni\';
  src:  url(\'arialuni.ttf\')  format(\'truetype\'); 
}

@page { margin: 0px 28px 0px 28px; }
body{margin:0px; padding:0; font-family:\'arialuni\'}
#label{padding:5px; font-size:11px; color:#000000; font-family:Helvetica,Arial,sans-serif;}
#label .container{width:100%}
.header,  .cn22,  .tablesection,  .footer{border-bottom:1px solid #000000; clear:both; }
 .footer{font-size:10px;}
 .header1{width:auto;}
 .leftside,  .rightside,  .date,  .sign{ width:50%}
 .jpoststamp{border:1px solid #000000; padding:0px;}
 .stampnumber{width: 20%; border:1px solid #000000; padding:4px; background:#000000;  color:#ffffff; text-align:center; font-size:22px;}
 .jerseyserial{     padding: 0 5px;    width: 80%;text-transform:uppercase; font-size:12px;font-weight:bold}
  .vatnumber{     padding: 2px 0 0 0; font-size:11px;font-weight:bold; width:100%}
 h1{ font-size:12px; margin:0px; }
 h3{font-size:11px;font-weight:bold; margin:0}
 h4{font-size:14px;font-weight:bold; margin:0}
 .rightheading{text-align:center;}
 .fullwidth{clear:both}
 .fullwidth h2{text-align:center; font-size:14px; font-weight:bold; margin:0; padding:5px 0;}
 .fullwidth p{ margin-top:5px; margin-bottom:0px;}
 .producttype{margin-bottom:5px; }
.producttype td{font-size:11px}
 .producttype div{width:25%; display:inline-block;}
 .producttype span{border: 1px solid #000000;
    display: inline-block;
    font-size: 9px;
    height: 12px;
    margin-right: 2px;
    text-align: center;
    width: 12px;}
	
table{border:none; width:100%; }
	 
 th{font-weight:bold;font-size:10px; }
 th,td{padding:0px;font-size:12px; text-align:left}
 td table td{padding:0px;}
 
 .norightborder{border-right:0px;}
 .noleftborder{border-left:0px;}
 .rightborder{border-right:1px solid #000000;}
 .leftborder{border-left:1px solid #000000;}
 .topborder{border-top:1px solid #000000;}
 .bottomborder{border-bottom:1px solid #000000;}
 .center{text-align:center}
 .right{text-align:right}
 .bold{font-weight:bold;font-size:12px; }
 .sign{ margin-top: -20px;}
 .barcode{padding:5px 0;}
 .barcodeleft{width:150px;}
 .tracking{padding:5px 0;font-size:10px}
 .address{font-size:12px;}
 .barcode div{}
 .otherinfo{width:40%;  margin-top:5px}
 .totalprice{width:60%;  margin-top:5px}
 .tablesection{padding:0px 0px 8px ; margin-top:-1px}
 
 .countrycode h3 {
    font-family: Helvetica,Arial,sans-serif;
    font-size: 40px;
    font-weight: 800;
	color:#999999;
}
.aprior div, .bpost div {
    font-size: 9px;
    line-height: 1;
	margin-top:5px;
}

.aprior .po, .bpost .po {
    font-size: 18px;
    line-height: 1;
	margin-top:5px;
}
.sflBarcode img{width:250px;}
.rainbowBarcode img{width:250px;}
</style> ';
 		 
		$cssPath = WWW_ROOT .'css/';
		$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
		  echo $html;
		//	$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
		$dompdf->load_html($html, Configure::read('App.encoding'));
		$dompdf->render();
		$imgPath = WWW_ROOT .'img/printPDF/Bulk/'; 
		$path = Router::url('/', true).'img/printPDF/Bulk/';
		$name	=	'Label_Slip_'.$split_order_id.'.pdf';
		unlink($imgPath.$name);   
		file_put_contents($imgPath.$name, $dompdf->output());
		exit;
				  
	 } 
        	
 }
?>