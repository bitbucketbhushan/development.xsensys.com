<?php
error_reporting(0);
set_time_limit(180);

class ReorderController extends AppController
{
    var $name = "Reorder";
    var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
  	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('virtualInTransitStock','lifeOfItem','chk_supplier_mappings','minStockLevel'));
			$this->GlobalBarcode = $this->Components->load('Common'); 
    } 
	
	public function index()
    {
		$this->layout = 'index';
		$this->loadModel( 'SupplierDetail' );
		$this->loadModel( 'Company' );
		$company = $this->Company->find( 'all' ); 
 		$suppliers = $this->SupplierDetail->find( 'all' ); 
		$this->set('suppliers',$suppliers);		
		$this->set('company',$company);				
 	}
	
 	public function SupplierPricing($quotation_id = null)
    {
		$this->layout = 'index';
		$this->loadModel( 'SupplierDetail' );
		$this->loadModel('Quotation');
		$quote_supplier = [];
		$qt = $this->Quotation->find('all', array('conditions' => array('quotation_id' => $quotation_id),'fields'=>array('id','supplier_code','uploaded_file')));
		if(count($qt) > 0){
			foreach($qt  as $q){
				$quote_supplier[] = $q['Quotation']['supplier_code'];
			}
		}


 		$suppliers = $this->SupplierDetail->find( 'all' ); 
		$fee = $this->getAllFee();
		$this->set('suppliers',$suppliers);	
		$this->set('fee',$fee);	
		$this->set('quote_supplier',$quote_supplier);				
		 
 	}
	
	public function Update()
	{	
		$msg = array();   	
		$this->loadModel('SupplierMapping');
		if($this->request->data['name'] !=''){	
			$fields = "`".$this->request->data['name']."` =  '".$this->request->data['value']."' ";
			$sql_query = "UPDATE `supplier_mappings` SET $fields  WHERE `id` = '".$this->request->data['pk']."'";	
			$this->SupplierMapping->query($sql_query);	
			$msg['msg'] = $this->request->data['pk']." UPDATED".$sql_query;	
		}else{
			$msg['msg'] = "Invalid Data!";
			$msg['status'] = 'error';
		}				
		echo json_encode($msg);
		exit;
	}
	
	 
	public function getSupplierPricing()
	{
 		$this->layout = 'ajax';
		$this->loadModel('Product');
		$this->loadModel('SupplierDetail');
		$this->loadModel('SupplierMapping');
		$this->loadModel('QuotationOurExpectedPrice');

		$sql_query = '';								  
		if(isset($this->request->data["master_sku"]) && $this->request->data["master_sku"]!=''){
			$sql_query = "  AND `SupplierMapping`.`master_sku` LIKE '%".trim($this->request->data['master_sku'])."%'";
		}	
		$order_by = 'DESC'; $sort_by ="qty";
		if(isset($this->request->data["name"]) && isset($this->request->data["value"]) && $this->request->data["name"]!='Supplier_Count'){	
			$sort_by = $this->request->data["name"];	
			$order_by = $this->request->data["value"];	
		}
 	
		$suppliers = array();
		foreach(explode(",", $this->request->data['supplier_code']) as $supp){
			$suppliers[] = "'".$supp."'";
		} 
		
		$results = $this->SupplierDetail->find('all', array(
															'conditions'=>array('supplier_code'=> explode(",", $this->request->data['supplier_code'])),
															'fields'=>array('id','supplier_code','supplier_name','credit_period','credit_limit','credit_limit_available'),
															'order'=>'`supplier_name` ASC'
															)
												); 
		 
		$msg['header'] = '';
		if(count($results) > 0){	 
			foreach($results as $v){	
			$d = $v['SupplierDetail']['credit_limit'] - $v['SupplierDetail']['credit_limit_available'];
			$msg['header'] .=  '<th width="10%" class="suppliers '.$v['SupplierDetail']['supplier_code'].'"><span onClick="SortBy(this);" class="sort-by" data-name="price" order-by="ASC"><i class="glyphicon glyphicon-sort-by-attributes"></i> '. ucfirst($v['SupplierDetail']['supplier_code']).'</span><br>('.$v['SupplierDetail']['credit_limit'].'/'.$d.')<div class="dtq"><span class="tq" id="qty_'.$v['SupplierDetail']['supplier_code'].'"></span></div></th>';	
			}
		}
		
  	 	$sql = "SELECT `SupplierMapping`.`id`,`SupplierMapping`.`master_sku` AS `sku`,`SupplierMapping`.`sup_code`,`SupplierMapping`.`supplier_sku`,`QuotationOurExpectedPrice`.`price`,`QuotationOurExpectedPrice`.`qty` from `supplier_mappings` `SupplierMapping` join `quotation_our_expected_prices` `QuotationOurExpectedPrice` ON (`SupplierMapping`.`master_sku` = `QuotationOurExpectedPrice`.`master_sku`) WHERE ((`SupplierMapping`.`price` > 0) AND (`SupplierMapping`.`supplier_qty` > 0) AND SupplierMapping.`sup_code` IN (".implode(",",$suppliers).")) $sql_query GROUP BY `SupplierMapping`.`master_sku`  ORDER BY $sort_by $order_by";
	
  		$results = $this->SupplierMapping->query($sql);
 			
		if(count($results) > 0){
			$currencySym = array('&euro;' => 'EUR', '&pound;' => 'GBP');
			$Currency 	 = $this->getCurrencyrate();		
			$fee 		 = $this->getAllFee();
			$poMargin	 = $fee['Fee']['po_margin']  > 0 ? $fee['Fee']['po_margin'] : 5;	
			$sappData    = $this->getAvailableSupp($this->request->data['qt']);	
			$avaData 	 =  array(); 
			foreach($results as $varr){			
						
				/*------------------Availability Calulation-------------------------*/	
				$expected = array('price' => $varr['QuotationOurExpectedPrice']['price'], 'qty' => $varr['QuotationOurExpectedPrice']['qty']);	
				if(count($sappData)>0){
					$Result = $this->search($sappData, 'master_sku',$varr['SupplierMapping']['sku']) ;	 
 					$Result = $this->sksort($Result, 'supplier_qty');			
					$Available_Qty = $this->getAvailableQty($expected, $Result,$Currency,$fee);
 					$available = 'pna'; $sc = 0;					
					if(count($Available_Qty) > 0 ){		
						$available = '';		
						foreach($Available_Qty as $val){										 
							//$available .= $val['supplier_code'].' &pound;'.$val['supplier_price'] .' Q'.$val['supplier_qty'].'<br> ';
							$available .= '<select><option>'.$val['supplier_code'].'</option></select> &pound;'.$val['supplier_price'] .' Q'.$val['supplier_qty'].'<br> ';
							$sc++;
						}
					}				
				}else{
					$available  = '<span class="pna">Price or Qty not available!</span>';
				}
				$max_eprice = number_format(($varr['QuotationOurExpectedPrice']['price'] + ($varr['QuotationOurExpectedPrice']['price'] * $poMargin / 100)),2);				
				$avaData[$varr['SupplierMapping']['sku']] = array('ExpPrice'=>$varr['QuotationOurExpectedPrice']['price'], 'MaxExpPrice' => $max_eprice,'ExpQty' => $varr['QuotationOurExpectedPrice']['qty'],'Available_Qty' => $available,'Supplier_Count'=> $sc,'sm_id'=>$varr['SupplierMapping']['id']);
							
			}		
			
			$sort_by = 'Supplier_Count'; $orderby = 'DESC'; 
			if(isset($this->request->data["name"]) && $this->request->data["name"] == 'price'){
				$sort_by = 'ExpPrice';			
			}else if(isset($this->request->data["name"]) && $this->request->data["name"] == 'qty'){
				$sort_by = 'ExpQty';
			}
			else if(isset($this->request->data["name"]) && $this->request->data["name"] == 'Supplier_Count'){
				$sort_by = 'Supplier_Count';
			}		
			
			if(isset($this->request->data["orderby"]) && $this->request->data["orderby"] == 'ASC'){ 			
				$sorted = $this->sksort($avaData, $sort_by, true);
				$order_by = $orderby = $this->request->data["orderby"];	
			}else{
				$sorted = $this->sksort($avaData, $sort_by);
				$order_by = $orderby = 'ASC';	
			}
 			
			if(isset($this->request->data["page"])){
				$page_number = filter_var($this->request->data["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); 
				if(!is_numeric($page_number)){die('Invalid page number!');} 
			}else{
				$page_number = 1; 
			}
			
			$item_per_page = 5;
			$total_rows = count($sorted); 		
			$total_pages = ceil($total_rows/$item_per_page);
			$page_position = (($page_number-1) * $item_per_page);
		
			$sorted = array_slice( $sorted, $page_position,  $item_per_page );
			 
		//	$Mapping = getAllMappingData();
			
			$msg['data']=''; $count = 0;
			//$AllSupplier = $this->getAllSupplier();
			
			
			$AllSupplier = explode(",", $this->request->data['supplier_code']);
			//print_r($AllSupplier);
			foreach($sorted as $sku => $v){		 
			
			//	$masterSkuMap = search($Mapping, 'master_sku',$sku) ;
				
				$msg['data'] .='<tr>';
				$msg['data'] .='<td>'.$sku.'</td>';			
				$sm_id = $v['sm_id'];
				
				foreach($AllSupplier as $k => $supplier_code){ 
							
					$supData = $this->getSupplierData($sku, $supplier_code);
					 
					$price = ''; $s_currency = ''; $s_qty = ''; $qty =''; $supplier_eur = '';	$exchangeRate = 1;	
					if(count($supData) > 0)
					{					
						if($supData['supplier_price'] > 0){
							
							if($val['currency'] == 'EUR'){
								$exchangeRate = $Currency['EUR'];						
								$supplier_eur = '(&euro;'.($supData['supplier_price'] / $exchangeRate).')';
							 }						
							$s_currency = array_search($val['currency'],$currencySym);
							$qty  	 = $supData['supplier_qty'];
							$s_qty 	 = 'Q:';
							$price   = $supData['supplier_price'];
						}			
 					
				  	$msg['data'] .='<td width="10%" class="'.$supplier_code.'"><section>'.$s_currency.'<span id="sp_'.$supData['sm_id'].'" class="xedit price" data-name="price" pk="'.$supData['sm_id'].'" action="update" data-value="'.$price.'">'.$price.'</span></section>'.$supplier_eur.'&nbsp;<section>'.$s_qty.'<span id="sq_'.$supData['sm_id'].'" class="xedit qty" data-name="supplier_qty" pk="'.$supData['sm_id'].'" action="update" data-value="'.$qty.'">'.$qty.'</span></section></td>';	
					}else{
						$msg['data'] .='<td width="10%" class="'.$supplier_code.'"> </td>';	
					}
									
				}					
				$msg['data'] .='<td><section><span class="xedit" id="exp_'.$sm_id.'" pk="'.$sku.'" action="expectedPQ" data-name="price" data-value="'.$v['ExpPrice'].'">'.$v['ExpPrice'].'</span></section></td>';
				$msg['data'] .='<td>'.$v['MaxExpPrice'].'</td>';
				 
				$msg['data'] .='<td><section><span class="xedit" id="eq_'.$sm_id.'" pk="'.$sku.'" action="expectedPQ" data-name="qty" data-value="'.$v['ExpQty'].'">'.$v['ExpQty'].'</span></section></td>';		
				
				if($v['Available_Qty'] == 'pna'){									
					$msg['data'] .='<td><div class="pna">Purchase Not Allowed<div></td>';
				}else{							
					$msg['data'] .='<td>'.$v['Available_Qty'].'R</td>';					
				}		
								
				$msg['data'] .='</tr>';		
				$count++;	
			}
			
			$msg['data'] .='<script>jQuery(\'.xedit\').editable();</script>';			
					
			$sql_qty    = "SELECT SUM(qty) as qty FROM quotation_our_expected_prices";
			$qty_result = $this->QuotationOurExpectedPrice->query($sql_qty);
 			$msg['total_qty'] = 'Qty:'.$qty_result[0][0]['qty'];				 
 			
			
			$sup_results = $this->SupplierDetail->find('all', array(
 															'fields'=>array('supplier_code'),
															'order'=>'`supplier_name` ASC'
															)
												);
												
			 
			if(count($sup_results) > 0){				 
					foreach($sup_results as $vs){	
 					$s_qty    = "SELECT SUM(supplier_qty) as qty FROM supplier_mappings WHERE sup_code = '".$vs['SupplierDetail']['supplier_code']."'";
					$s_result = $this->SupplierDetail->query($s_qty);
 						if($s_result[0][0]['qty'] > 0){
							$supp .=  $vs['SupplierDetail']['supplier_code'] .'=Qty:'.$s_result[0][0]['qty'].'|';
						}else{
							$supp .=  $vs['SupplierDetail']['supplier_code'] .'=Qty:0|';
						}
 					}
			}
			
			$msg['suppliers'] = $supp;		
									 
			$msg['paginate'] = '<div class="total_pages">Total Page: '.$total_pages.' Items #'.$count.'</div>
				<div class="paginate">'.$this->paginate_function($item_per_page, $page_number, $total_rows, $total_pages,$sort_by,$order_by,$orderby).	'</div>
				<div class="total_rows">Total Row: '.$total_rows.'</div>';	
		}else{
			$msg['paginate'] = '';
			$msg['suppliers']= '';
			$msg['data']='<tr><td colspan="11"><div align="center"><div class="alert alert-danger fade in">No data found!</div></div></td></tr>';
		}
		
 		echo json_encode($msg);
		exit;
				
	}
	
	public function search($array, $key, $value)
	{
		$results = array();
	
		if (is_array($array)) {
			if (isset($array[$key]) && $array[$key] == $value) {
				$results[] = $array;
			}
	
			foreach ($array as $subarray) {
				$results = array_merge($results, $this->search($subarray, $key, $value));
			}
		}
	
		return $results;
	}
	
	public function getAllFee()
	 {	
		$this->loadModel('Fee');
		$result = $this->Fee->find('first');
 		return	$result;
 	}
	
	public function UpdateMargin(){
		$this->loadModel('Fee');
		$sql = "UPDATE `fees` SET po_margin = '".$this->request->data['value']."' WHERE `id` = '".$this->request->data['id']."'";	
		$this->Fee->query($sql);	
		$msg['msg'] = "UPDATED!";	 
	
		echo json_encode($msg);
		exit;
	}
	
	public function getSupplierData($msku,$scode){
 		$this->loadModel('SupplierMapping');

		$result = $this->SupplierMapping->find('first', array(
															'conditions'=>array('sup_code'=>$scode,'master_sku' => $msku),
															'fields'=>array('id','sup_code','price','supplier_qty'),
															'order'=>'`title` ASC'
															)
												);
		
		$data = array();	
	 
 		if(count($result) > 0){			
 			$data = array('supplier_price' => $result['SupplierMapping']['price'], 'supplier_qty' => $result['SupplierMapping']['supplier_qty'], 'sm_id' => $result['SupplierMapping']['id']);	
		}
		 				
		return $data;
	}
	
	public function getLowestPrice($msku){ }
	public function getPreviousPrice($msku,$scode){
		$data = array();
		$this->loadModel('PO');
		$this->loadModel('PurchaseOrder');
		
 		$params	=	array(
					'joins' => array(
							array(
								'table' => 'pos',
								'alias' => 'Po',
								'type' => 'INNER',
								'conditions' => array(
									'Po.is = PurchaseOrder.po_id'
								)
							)
						),
					'conditions' => array( 
									'Po.supplier_code' => $scode,
									'PurchaseOrder.purchase_sku' => $msku,
									),
					'fields' => array( 
									'PurchaseOrder.purchase_qty',
									'PurchaseOrder.price',
									),
					'order' => array(
									'PurchaseOrder.`date` DESC'  
 									)
					
				);
				
		$_result	=	$this->PurchaseOrder->find( 'first', $params );
		 
 		if(count($_result) > 0){			
 				$data[] = array('price'=>$_result['PurchaseOrder']['price'], 'qty'=>$_result['PurchaseOrder']['purchase_qty']);		
 		}else{		
			$data = array('price'=>0, 'qty'=>0);	
		}
		 	
		return $data;
	}
	public function getExpectedPrice($msku){
		$data = array();
		$this->loadModel('QuotationOurExpectedPrice');
 		$result = $this->QuotationOurExpectedPrice->find('first', array('conditions'=>['master_sku'=>$msku], 'fields'=>array('price','qty')));
  		if(count($result) > 0){						
 			$data = array('price'=>$result['QuotationOurExpectedPrice']['price'], 'qty'=>$result['QuotationOurExpectedPrice']['qty']);			
		}
		return $data;
	}

	public function getAllSupplier(){
		$this->loadModel('SupplierDetail');
		$data = array();	
 		
 		$result = $this->SupplierDetail->find('all', array('fields'=>array('supplier_code','currency'),'order'=>'`supplier_name` ASC'));
  		if(count($result) == 0){
			$data[] = array('supplier_price'=> [], 'supplier_qty'=>0);
		}else{
 			foreach($result as $v){
				$data[] = array('supplier_code' => $v['SupplierDetail']['supplier_code'],'currency' => $v['SupplierDetail']['currency']);	
			}
 		}	
		return $data;
	}
	
	public function getAvailableSupp($quotation_id = []){
		
		$this->loadModel('SupplierDetail');
		$this->loadModel('SupplierMapping');
		$this->loadModel('Quotation');
		$this->loadModel('QuotationItem');
		
		$data = array();
		$suppliers = array();
		 
 				
 		$Currency 	 = $this->getCurrencyrate();
		 
		$quotations=  $this->Quotation->find('all', array('conditions'=>['quotation_id'=>$quotation_id]));
		foreach($quotations as $q){
			$quotation_inc_id[] = $q['Quotation']['id'];
			$suppliers[$q['Quotation']['id']] = $q['Quotation']['supplier_code'];
		}
		$results =  $this->QuotationItem->find('all', array('conditions'=>['quotation_inc_id'=>$quotation_inc_id]));
 		if(count($results) > 0){	
			$data = array();	
			foreach($results as $val){ 
				$exchangeRate = 1;
				/*if($val['SupplierDetail']['currency'] == 'EUR'){
					$exchangeRate = $Currency['EUR'];
				}*/
				$supplier_price = number_format($val['QuotationItem']['supplier_quote_price'] / $exchangeRate, 2);	
				
				$data[] = array(
							'master_sku' => $val['QuotationItem']['master_sku'], 
							'supplier_code' => $suppliers[$val['QuotationItem']['quotation_inc_id']], 
							'supplier_sku'=> $val['QuotationItem']['supplier_itemcode'], 
 							'supplier_name' => $suppliers[$val['QuotationItem']['quotation_inc_id']], 
							'supplier_price'=> $supplier_price,						 
							'currency'=> $val['QuotationItem']['quotation_number'], 
							'supplier_qty' => $val['QuotationItem']['supplier_available_qty'],
							'credit_limit_available'=> $val['QuotationItem']['quotation_number']
							);	
			}
		} 
		  
		return $data;
	}
	public function getAvailableSupp_20122021($suppliers){
		
		$this->loadModel('SupplierDetail');
		$this->loadModel('SupplierMapping');
		$data = array();
		$av_suppliers = array();
		foreach(explode(",", $suppliers) as $supp){
			$av_suppliers[] = "'".$supp."'";
		} 
 				
 		$Currency 	 = $this->getCurrencyrate();
		$sql = "SELECT  SupplierMapping.id, 
						SupplierMapping.master_sku, 
						SupplierMapping.sup_code,
						SupplierMapping.price, 
						SupplierMapping.supplier_qty,
						SupplierMapping.supplier_sku, 
						
						SupplierDetail.supplier_name, 
						SupplierDetail.credit_limit, 
						SupplierDetail.credit_limit_available, 
						SupplierDetail.currency
					FROM supplier_mappings SupplierMapping 			
						  JOIN supplier_details SupplierDetail
				ON (SupplierMapping.sup_code = SupplierDetail.supplier_code)   
					WHERE SupplierMapping.price > 0 AND SupplierMapping.supplier_qty > 0 AND SupplierDetail.`supplier_code` IN (".implode(",",$av_suppliers).") ORDER BY SupplierMapping.`supplier_qty` DESC" ;
		
		$results =  $this->SupplierDetail->query($sql);	
		if(count($results) > 0){	
			$data = array();	
			foreach($results as $val){ 
				$exchangeRate = 1;
				if($val['SupplierDetail']['currency'] == 'EUR'){
					$exchangeRate = $Currency['EUR'];
				}
				$supplier_price = number_format($val['SupplierMapping']['price'] / $exchangeRate, 2);	
				
				$data[] = array(
							'master_sku' => $val['SupplierMapping']['master_sku'], 
							'supplier_code' => $val['SupplierMapping']['sup_code'], 
							'supplier_sku'=> $val['SupplierMapping']['supplier_sku'], 
							
							'supplier_name' => $val['SupplierDetail']['supplier_name'], 
							'supplier_price'=> $supplier_price,						 
							'currency'=> $val['SupplierDetail']['currency'], 
							'supplier_qty' => $val['SupplierMapping']['supplier_qty'],
							'credit_limit_available'=> $val['SupplierDetail']['credit_limit_available']
							);	
			}
		} 
		return $data;
	}

	public function getAvailableQty($expected, $dataArray, $Currency, $fee){ 
		$data_sup = array(); $data = array();	 
		$this->loadModel('SupplierDetail');
		$this->loadModel('SupplierMapping');
		$Currency 	 = $this->getCurrencyrate();		
		$Expected_PQ =  $expected; //getExpectedPrice($msku);
		//print_r($Expected_PQ); die;
		$fee 		 = $this->getAllFee();
		$poMargin	 = $fee['Fee']['po_margin'] > 0 ? $fee['Fee']['po_margin'] : 5;
		$exp_price   = $Expected_PQ['price'];
		$exp_qty     = $Expected_PQ['qty'];
		$min_qty	 = ceil($exp_qty/2);
		$n_price	 = $exp_price + (($exp_price * $poMargin) / 100);	
		
		//$sorted = array_orderby($dataArray, 'supplier_price', SORT_ASC);	
		$sorted = $this->sksort($dataArray, 'supplier_price',true);	
		//$sorted = array_orderby($sorted, 'supplier_price', SORT_DESC);
		
		$_qty = 0; $suppliers = array();
		foreach($sorted as $val){		
			$req_qty = 0;
			if($val['supplier_price'] <= $exp_price && $val['supplier_qty'] > 0){			
				for($c = 0; $c < $val['supplier_qty']; $c++){	
					$_qty++;		
					if($exp_qty >= $_qty ){
						$req_qty++;										
					}else{
					   break; 
					}								
				}	
					
				if($req_qty > 0){	
					$suppliers[] = $val['supplier_code'];			
					$data_sup[]  = array(
									'supplier_name' => $val['supplier_name'],
									'supplier_code' => $val['supplier_code'],
									'supplier_sku'  => $val['supplier_sku'], 
									'currency' 		=> $val['currency'], 
									'supplier_price'=> $val['supplier_price'], 
									'credit_limit_available'=> $val['credit_limit_available'], 
									'supplier_qty' => $req_qty
									);	
				}
			}	
		}
	
		$r_qty = ceil($min_qty - $_qty);		
		if($r_qty > 0){	
			$_qty = 0; 
			foreach($sorted as $val){		
				$req_qty = 0;	//echo	$val['supplier_price'].'--'. $n_price.'<br>';
				if((!in_array($val['supplier_code'],$suppliers)) && ($val['supplier_price'] <= $n_price) && ($val['supplier_qty'] >0)){	
				
					for($c = 0; $c < $val['supplier_qty']; $c++){
						$_qty++;				
						if($r_qty >= $_qty ){
							$req_qty++;										
						}else{
							break; 
						}										
					}	
					
					if($req_qty > 0){								
						$data_sup[] = array(
										'supplier_name' => $val['supplier_name'],
										'supplier_code' => $val['supplier_code'], 
										'supplier_sku'	=> $val['supplier_sku'],
										'currency' 		=> $val['currency'],  
										'supplier_price'=> $val['supplier_price'], 
										'credit_limit_available'=> $val['credit_limit_available'], 
										'supplier_qty' => $req_qty
										);	
					}
				 }
			}				
		}	
		
		
		return $data_sup;
	}
 	
	public function generatePo(){
			
 		$this->loadModel('PoQty');
		$this->loadModel('SupplierDetail');
		$this->loadModel('SupplierMapping');
		$this->loadModel('QuotePurchaseOrder');
		$this->loadModel('QuotePurchaseOrderSupplier');
		$this->loadModel('QuotePurchaseOrderSupplierItem');
		$this->loadModel('QuotationOurExpectedPrice');
		/** Include PHPExcel */
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');
			
			
		//$sql = "SELECT ms.sku, ms.title, ms.barcode FROM rng_master_stock ms";
		//$sql = "SELECT * FROM `po_qty` order by qty DESC";
		
		$sql = "SELECT Product.product_name, ProductDesc.barcode, PoQty.sm_id, PoQty.sku, PoQty.supplier_code, PoQty.supplier_sku, PoQty.price, PoQty.qty
			FROM products Product	
		 JOIN po_qty PoQty ON Product.product_sku = PoQty.sku JOIN product_descs ProductDesc ON Product.id = ProductDesc.product_id order by PoQty.price ASC";
 		
		$_mresult = $this->QuotePurchaseOrder->query($sql);	
		
		if(count($_mresult) > 0){
			$purchase_amount = 0;
			$total_qty = 0;	
			
			$Currency 	 = $this->getCurrencyrate();		
			$fee 		 = $this->getAllFee();
			$poMargin	 = $fee['Fee']['po_margin'] > 0 ? $fee['Fee']['po_margin'] : 5;			
			$sappData    = $this->getAvailableSupp($this->request->data['supplier_code']);
			
			$priceArray  = array();
			foreach($_mresult as $rs){	
								
 				$expected = array('price' => $rs['PoQty']['price'], 'qty' => $rs['PoQty']['qty']);					
				$Result   = $this->search($sappData, 'master_sku',$rs['PoQty']['sku']) ;	
				$Result   = $this->sksort($Result, 'supplier_qty');		
				
				$Available_Qty = $this->getAvailableQty($expected, $Result,$Currency,$fee);
				
				if(count($Available_Qty)>0){
											
					foreach($Available_Qty as $val){	
										
							$suppliers[$val['supplier_code']][] = array('sku'		=>	$rs['PoQty']['sku'],
																		'title'		=>	$rs['Product']['product_name'],
																		'barcode'	=>	$rs['ProductDesc']['barcode'],
																		'supplier_sku'=>$val['supplier_sku'],																																
																		'qty'		=>	$val['supplier_qty'],
																		'price'		=>	$val['supplier_price']
																		);	
							$purchase_amount +=$val['supplier_price'];
							$total_qty +=$val['supplier_qty'];
							
							$priceArray[$rs['PoQty']['sku']][] = $val['supplier_price'];
					}
				}				
			}		
		}			
		if(count($priceArray)>0){
			$insert_str = '';
			foreach($priceArray as $sku => $val){
				/*$sql = "SELECT `id` FROM `quotation_our_expected_prices` WHERE `master_sku` = '".$sku."' ";	
				$qresult =  $this->QuotePurchaseOrder->query($sql);*/
				$qresult = $this->QuotationOurExpectedPrice->find('first', 	array(
						'conditions' =>array('master_sku' => $sku),
						'fields'=>array('id')
						)
					);
					
					
				
				$minprice = min($val);
				if(count($qresult) > 0){						 
					$sql = "UPDATE `quotation_our_expected_prices` SET `price` =  '".$minprice."' WHERE `master_sku` = '".$sku."' ";	
					$this->QuotationOurExpectedPrice->query($sql) ;
				 }else{						
					$insert_str .= "('".$sku."','".$minprice."'),";
				 }	
				  $maxprice = max($val);
				  $sql = "UPDATE `products` SET `price` =  '".$maxprice."' WHERE `product_sku` = '".$sku."' ";	
				 // $this->SupplierDetail->query($sql) ;						  
			}
			if($insert_str){
				$insert_str = rtrim($insert_str,',');	
				$sql = "INSERT INTO `quotation_our_expected_prices` (`master_sku`,`price`) VALUES $insert_str";	
				$this->QuotationOurExpectedPrice->query($sql);
			}
		}
		$f_name = '';
		if(count($suppliers)>0){
			 
			$qpo = [];
			$qpo['purchase_amount'] = $purchase_amount;
			$qpo['total_qty'] 		= $total_qty; 
			$qpo['username']  		= $this->Session->read('Auth.User.username');
 			$this->QuotePurchaseOrder->saveAll($qpo);	
 			$po_id = $this->QuotePurchaseOrder->getLastInsertId();	
			 
			if($po_id){						
				foreach($suppliers as $supplier_code => $_data){
					$objPHPExcel = new PHPExcel();
					$row = 1;
					$rightBorder = array(
								  'borders' => array(
										'right' => array(
										  'style' => PHPExcel_Style_Border::BORDER_THIN
										),	
										'left' => array(
										  'style' => PHPExcel_Style_Border::BORDER_THIN
										)																
								  )
								);
					//$sql = "SELECT `supplier_name`, `credit_limit`, `credit_limit_available`, `credit_period`, `currency`, `address` FROM `suppliers` WHERE supplier_code = '".$supplier_code."' ";
					
 					$result = $this->SupplierDetail->find('first', 	array(
						'conditions' =>array('supplier_code' => $supplier_code),
						'fields'=>array('supplier_name','credit_limit','credit_limit_available','credit_period','currency','address')
						)
					);

					//$result = $mysqli->query($sql) or die($mysqli->error);
					//$obj = $result->fetch_object();				
					
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':G'.$row);
					
					$row++;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Euraco Group Ltd')
							->setCellValue('D'.$row, 'Purchase Order');				
					$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);	
					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(70);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->getFont()->setSize(20)->setBold(true);					
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('3498DB');
					
					$row++;		
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':G'.$row);		
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'REGISTERED_ADDRESS');
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
					
					$row++;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':G'.$row);
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Tel :'.'REGISTERED_PHONE' .', Fax :'. 'REGISTERED_FAX' .', Email :'. 'REGISTERED_EMAIL');
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
					
					$row++;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':G'.$row);
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'GST No: '.'REGISTERED_GST');
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
									
					$row++;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':G'.$row);
					$row++;				
					$BStyle = array(
								  'borders' => array(
									'top' => array(
									  'style' => PHPExcel_Style_Border::BORDER_THIN
									),
									'bottom' => array(
									  'style' => PHPExcel_Style_Border::BORDER_THIN
									)
								  )
								);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->getFont()->setSize(12)->setBold(true);	
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Purchase From')
							->setCellValue('B'.$row, 'Deliver To')	
							->setCellValue('E'.$row, 'Tender Details');					
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($BStyle);																						
					unset($BStyle);
					
					$row++;
					$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(80);	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->getAlignment()->applyFromArray(
						array(
								 'vertical'   => PHPExcel_Style_Alignment::VERTICAL_TOP,
								 'rotation'   => 0,
								 'wrap'       => true)
					);
	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $result['SupplierDetail']['address']);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setWrapText(true);
					
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.$row.':D'.$row);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, 'DELIVERY_ADDRESS');
					$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setWrapText(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B'.$row)->setWidth(45);
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E'.$row.':G'.$row);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, "P. O. No# ". date('Ymd'). strtoupper($supplier_code).																 																	    "\nDate:". date('m/d/Y'));
					$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setWrapText(true);
	
						
					$row++;		
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($rightBorder);					
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':G'.$row);
					$row++;
					
					$BStyle = array(
					  'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
					);
				
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($BStyle);
					
					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$row, 'DESCRIPTION')
								->setCellValue('B'.$row, 'CODE')
								->setCellValue('C'.$row, 'OUR REF')
								->setCellValue('D'.$row, 'BARCODE')
								->setCellValue('E'.$row, 'QTY')	
								->setCellValue('F'.$row, 'UNIT PRICE')
								->setCellValue('G'.$row, 'UNIT TOTAL');	
			
					$file_name = $supplier_code.date('ymd_Hi').'.xlsx';			
								
					$qpos['po_id'] = $po_id;
					$qpos['po_label'] = $supplier_code.'_'.$po_id;
					$qpos['supplier_code'] = $supplier_code;
					$qpos['po_sheet'] = $file_name;
					$qpos['purchase_date'] = date("Y-m-d H:i:s");
					
					$this->QuotePurchaseOrderSupplier->saveAll($qpos);	
 					$pos_id = $this->QuotePurchaseOrderSupplier->getLastInsertId();	 
 					
					/*$pos_sql = "INSERT INTO `rng_purchase_order_supplier` 
									(`po_id`, `po_label`, `supplier_code`, `po_sheet`,`purchase_date`) 
								VALUES 
									('".$po_id."', '".$supplier_code.'_'.$po_id."','".$supplier_code."', '".$file_name."', '".date("Y-m-d H:i:s")."');";
					
					$mysqli->query($pos_sql) or die($mysqli->error);
					$pos_id = $mysqli->insert_id;*/
					
					$sup_total_purchase = 0; $sup_qty = 0; $credit_limit = 0;
					//$formatter 	= new NumberFormatter('en_GB',  NumberFormatter::CURRENCY);	
					 
					foreach($_data as $val){
						$row++;	
						$currency 	= $result['SupplierDetail']['currency'];
						
						$exchangeRate = 1;
						if($currency == 'EUR'){
							$exchangeRate = $Currency['EUR'];
						}
						$price 		= number_format(($val['price']*$exchangeRate),2);
						$unit_total = number_format(($val['qty']*$val['price']*$exchangeRate),2);
						//$price 		= $formatter->formatCurrency($val['price']*$exchangeRate,$obj->currency);
						//$unit_total = $formatter->formatCurrency(($val['qty']*$val['price']*$exchangeRate),$obj->currency);							
										
						$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue('A'.$row, $val['title'])														
									->setCellValue('B'.$row, $val['supplier_sku'])
									->setCellValue('C'.$row, $val['sku'])
									->setCellValue('D'.$row, $val['barcode'])
									->setCellValue('E'.$row, $val['qty'])							
									->setCellValue('F'.$row, $price)
									->setCellValue('G'.$row, $unit_total);
						$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($BStyle);			
									
						$qposi = [];										
						$qposi['pos_id'] = $pos_id;
						$qposi['master_sku'] = $val['sku'];
						$qposi['supplier_sku'] = $val['supplier_sku'];
						$qposi['price'] = $val['price'];
						$qposi['qty'] = $val['qty'];
						$qposi['purchase_date'] = date('Y-m-d H:i:s');
 		 
						$this->QuotePurchaseOrderSupplierItem->saveAll($qposi);	
 						//$pos_id = $this->QuotePurchaseOrderSupplierItem->getLastInsertId();	 
					
				/*$sql = "INSERT INTO `rng_purchase_order_supplier_items` 
									(`pos_id`, `master_sku`, `supplier_sku`, `price`, `qty`,`purchase_date`)
								VALUES 
									('".$pos_id."', '".$val['sku']."','".$val['supplier_sku']."','".$val['price']."','".$val['qty']."','".date('Y-m-d H:i:s')."');";	
						$mysqli->query($sql) or die($mysqli->error);*/
						
						$sup_total_purchase += $val['price']*$val['qty'];
						$sup_qty += $val['qty'];									
					}
					$sql_query = "UPDATE `quote_purchase_order_suppliers` SET 
										`total_purchase_cost` = '".$sup_total_purchase."', 
										`total_qty` = '".$sup_qty."' 
								  WHERE `id` = '".$pos_id."'";	
					$this->QuotePurchaseOrderSupplier->query($sql_query);
					
					/*-------------------UPDATE credit limit Database--------------*/
					
					$credit_limit	 =  $result['SupplierDetail']['credit_limit'];					
					$limit_available =  $result['SupplierDetail']['credit_limit_available'] + $sup_total_purchase;		
								
					$sql_limit = "UPDATE `supplier_details` SET `credit_limit_available` = '".$limit_available."' WHERE `supplier_code` = '".$supplier_code."'";
					$this->SupplierDetail->query($sql_limit);
						
					$objPHPExcel->getActiveSheet()->setTitle($supplier_code);			
					$objPHPExcel->setActiveSheetIndex(0);	
					/*-------------------Total Qty & Cost Calculation--------------*/	
					$row++;
					//$price_sup_total	= $formatter->formatCurrency($sup_total_purchase,$currency);
					$price_sup_total	= number_format($sup_total_purchase,2);
					
					
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$row, $sup_qty)
														->setCellValue('G'.$row, $price_sup_total);
									
					$objPHPExcel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->getColor()->setRGB('ffffff');								
					$objPHPExcel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('808000');  
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray($BStyle);
					
					/*-------------------Over limit Calculation--------------*/	
					if($credit_limit < $limit_available)
					{	
						$row++;
						$over_limit  = $credit_limit - $limit_available;
						//$over_limit	 = $formatter->formatCurrency($over_limit, $currency);
						$over_limit	 = number_format($over_limit,2);
						
						$objPHPExcel->getActiveSheet()
									->setCellValue('E'.$row, "Over limit:")
									->setCellValue('G'.$row, $over_limit);	
													
						$objPHPExcel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->getColor()->setRGB('ffffff');									
						$objPHPExcel->getActiveSheet()
							->getStyle("A".$row.":G".$row)
							->getFill()
							->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
							->getStartColor()
							->setARGB('FF0000');  
					}				
					$f_name	.= '<li><a href="'.WWW_ROOT.'quotation'.DS.'purchase_orders'.DS.$file_name.'" target="_blank">'.ucfirst($supplier_code).' File</a></li>'; 
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');		
					$objWriter->save(WWW_ROOT.'quotation'.DS.'purchase_orders'.DS.$file_name);			
					}
			}
					
		}		
		//print_r($suppliers);	
		$msg['msg'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Report Generated.</div>';	
		$msg['file_link'] = $f_name;	
		echo json_encode($msg); 
		exit;
	}

  	public function getCurrencyrate()
	{		
		$file_name = 'currency_'.date('Y-m-d').'.xml';
		$currencyFile = WWW_ROOT.'logs'.DS.$file_name;	
		
		if(!file_exists($currencyFile)){			 
		 
			$url='http://www.floatrates.com/daily/gbp.xml';
			$curs = array('GBP','EUR','USD','CAD');
			$xml  = file_get_contents($url);
			$obj  = json_decode(json_encode(simplexml_load_string($xml)),true);
			$xml  = "<?xml version='1.0' encoding='UTF-8'?>\n";
			$xml .= "<currency>\n";		 
			foreach($obj['item'] as $a) {
				 $t = $a['targetCurrency'];
				 if(in_array($t,$curs)) {								
					$xml .= "<".$t.">".$a['exchangeRate']."</".$t.">\n";			  	 
				 }
			}		
			$xml .= "</currency>";
			file_put_contents($currencyFile, $xml);		
 		}
		 		 				
		$data['EUR'] = 1.14;			
		$data['USD'] = 1.40;		
		if(file_exists($currencyFile)){	   
			$xml = file_get_contents($currencyFile);
			$_Currency = json_decode(json_encode(simplexml_load_string($xml)),true);	
			if($_Currency['EUR'] < 1 ){
				return $data;
			}else{
				return json_decode(json_encode(simplexml_load_string($xml)),true);	
			}
 		}else{	
			 return $data;
		}	
		exit;		
	 }
 
	public function getTimeInterval($ts1, $ts2)
	{
		$interval = $t = (int)$ts2 - (int)$ts1;
		if ( $interval < 0 ) {
			return false;
		} else {
			$days = floor($interval / 86400); // seconds in one day
			$interval = $interval % 86400;
			$hours = floor($interval / 3600);
			$interval = $interval % 3600;
			$minutes = floor($interval / 60);
			$interval = $interval % 60;
			$seconds = $interval;
		}
		return array(
			'days' => $days,
			'hours' => $hours,
			'minutes' => $minutes,
			'seconds' => $seconds,
			'totalhours' =>floor($t/3600)
		);
	}

	 
	public function Dashboard()
    {
		$this->layout = 'index';
 	} 
	
	public function UploadFile()
    {
		$this->layout = 'index';
		$this->loadModel('Product');
		$this->loadModel('PurchaseOrder');
		$this->loadModel('SalesAverage');
		
		$name		= $this->request->data['upload']['import_file']['name'];
		$nameExplode= explode('.', $name);
		$nameNew 	= end( $nameExplode );
		$filename	= WWW_ROOT. 'files'.DS.'reorder_lavel'.DS.$nameExplode[0].'__'.time().".".$nameNew; 
		if( $nameNew == 'csv')
		{
			move_uploaded_file($this->request->data['upload']['import_file']['tmp_name'],$filename); 
			$fileData = file_get_contents($filename);
			foreach(explode("\n",$fileData) as $k => $val){
				if($k > 0){
					$data = explode(",",$val);
					$sku = trim($data[0]);
					$qty = trim($data[1]); 
					$product = $this->Product->find('first', array('conditions' => array('product_sku' => $sku),'fields'=>array('product_sku','min_stock_level')));
					if(count($product) > 0){
						$this->Product->query("UPDATE `products` SET `min_stock_level` = '".$qty."' WHERE `product_sku` = '".$sku."';");
					}
				}
			}
 			$this->Session->setflash(  "File uploaded successfully.",  'flash_success' );
			$this->redirect($this->referer());	
		}
		else
		{
			$this->Session->setFlash('Please upload valid file( CSV ).', 'flash_danger');
			$this->redirect($this->referer());	
		}	
		exit;
		
 	}   
	
	public function getSellingStores() 
	{
 		$this->layout = 'index';
		$this->loadModel( 'ProChannel' );
		$company =  $this->request->data['buying_company'];
		/*if($this->request->data['buying_company'] == 'euraco'){
			$company = 'costbreaker';
		}else if($this->request->data['buying_company'] == 'fresher'){
			$company = 'rainbow';
		}else if($this->request->data['buying_company'] == 'esl'){
			$company = 'marec';
		} */
		if($company != ''){
			$buying_company = $this->ProChannel->find('all', array('conditions' => array('company_id' => $company,'status'=>'Active'),'fields'=>array('channel_title','country','channel_type'),'order'=>'channel_title'));
		}else{
			$buying_company = $this->ProChannel->find('all', array('conditions' => array('status'=>'Active'),'fields'=>array('channel_title','country','channel_type'),'order'=>'channel_title'));
		}
		
		$select = ''; $options = [];
		if(count($buying_company) > 0){
			/*$select = '<select name="stores" id="stores" class="form-control" multiple="multiple" onchange="loadajax()">
									<option value="all">ALL</option>';
			foreach($buying_company as $v){
				$select .='<option value="'.$v['SourceComany']['channel_title'].'">'.$v['SourceComany']['channel_title'].'</option>';
			}
			$select .= '</select>'; */
			foreach($buying_company as $v){
				$options[] = ['channel_title'=>$v['ProChannel']['channel_title'],'country'=>$v['ProChannel']['country'],'channel_type'=>$v['ProChannel']['channel_type']];
			}
		}
		
 		//$data['html'] = $select;
		$data['options'] = $options;
		echo json_encode($data);
		exit;
	}
	public function getSuppliers() 
	{
 		$this->layout = 'index';
		$this->loadModel( 'SupplierDetail' );
 		$suppliers = $this->SupplierDetail->find( 'all' ); 
		
		$options = [];
		if(count($suppliers) > 0){
			 
			foreach($suppliers as $v){
				$options[] = $v['SupplierDetail']['supplier_code'];
			}
		}
  		$data['options'] = $options;
		echo json_encode($data);
		exit;
	}
	
	public function Quotations() 
	{
 		$this->layout = 'index';
		$this->loadModel( 'Quotation' );
		//$quotations = $this->Quotation->find( 'all',array('order' => 'id DESC') ); 
 		$this->paginate = array('order'=>'id DESC','limit' => 50 );
 		$quotations = $this->paginate('Quotation');
		
		$this->set('quotations',$quotations);	
	}
	
	public function deleteQuotation($id = 0) 
	{
		$this->layout = 'index';
		$this->loadModel( 'Quotation' );
		$this->loadModel( 'QuotationItem' );
		
		$this->Quotation->query( "DELETE FROM `quotations` WHERE `quotations`.`id` = {$id} AND `po_status` = 0"); 
		$this->QuotationItem->query( "DELETE FROM `quotation_items` WHERE `quotation_items`.`quotation_inc_id` = {$id}"); 
		$this->Session->setflash(  "Quotation deleted successfully.",  'flash_success' );
	 	$this->redirect( Router::url( $this->referer(), true ) );
	}
	
	public function QuotationItems($qid = null) 
	{
		$this->layout = 'index';
		$this->loadModel( 'QuotationItem' );
		//$quotation_items = $this->QuotationItem->find( 'all',array('conditions'=>['quotation_inc_id'=>$qid],'order' => 'id DESC') ); 
		
		$this->paginate = array('conditions'=>['quotation_inc_id'=>$qid],
 				  'order'=>'id DESC','limit' => 50 );
 		$quotation_items = $this->paginate('QuotationItem');
 		
		$this->set('quotation_items',$quotation_items);	
	}
	
	public function deleteQuotationItem($id = 0) 
	{
		$this->layout = 'index';
		$this->loadModel( 'QuotationItem' );
		$this->QuotationItem->query( "DELETE FROM `quotation_items` WHERE `quotation_items`.`id` = {$id}"); 
		$this->Session->setflash(  "Item deleted successfully.",  'flash_success' );
	 	$this->redirect( Router::url( $this->referer(), true ) );
	}
	
 	public function getQuotation() 
	{
		$this->layout = 'ajax';
		$this->autoRender = false;
		$this->loadModel( 'Product' ); 
 		$this->loadModel( 'SalesAverage' ); 
		$this->loadModel( 'PurchaseOrder' );
		$this->loadModel( 'SupplierDetail' );
		$this->loadModel( 'CheckIn' ); 
		$this->loadModel( 'Skumapping' );  
  		$stores = explode(",",$this->request->data['stores']);
		$all_skus = $this->Skumapping->find('all', array('conditions' => array('channel_name' => $stores),'fields' => array('sku','channel_sku','stock_status','listing_status')));
		foreach($all_skus as $sk){
 			if($sk['Skumapping']['listing_status'] ==  'inactive' || $sk['Skumapping']['stock_status'] ==  'npa'){
				$inactive_skus[$sk['Skumapping']['channel_sku']] = $sk['Skumapping']['channel_sku'];
			}
 		 	$sku_maps[$sk['Skumapping']['channel_sku']] = $sk['Skumapping']['sku'];
			 
  		}
		 	
		$sup = $this->SupplierDetail->find('all', array('conditions' => array('supplier_code' => explode(",",$this->request->data['suppliers'])),'fields'=>array('lead_time','fba_lead_time','fba_uk_lead_time')));
 		 
		$lead_time = 0;
		$fba_lead_time = 0;
		$fba_uk_lead_time = 0;
		if(count($sup) > 0){
			//$lead_time = $sup['SupplierDetail']['lead_time'];
			//$fba_lead_time = $sup['SupplierDetail']['fba_lead_time'];
			//$fba_uk_lead_time = $sup['SupplierDetail']['fba_uk_lead_time'];
		}
		
		
		//$Where[] = " SalesAverage.`master_sku` IN( '". implode("','",$active_skus)."' )";	
		
 		$Where[] = "SupplierMapping.`sup_code` IN('". implode("','", explode(",",$this->request->data['suppliers']))."')";

 		//$Where[] = "SupplierMapping.`sup_code` IN('". $this->request->data['suppliers']."')";
		
		if(isset($this->request->data['stock_type']) && $this->request->data['stock_type'] != 'all'){
			//$Where[] = " `nature_of_sku` = '".$this->request->data['stock_type']."' ";	
		}
		if(isset($this->request->data['master_sku']) && $this->request->data['master_sku'] != ''){
			$Where[] = " SalesAverage.`master_sku` = '".$this->request->data['master_sku']."' ";	
		}
 		if(isset($this->request->data['zero_vs']) && $this->request->data['zero_vs'] != ''){
			$Where[] = " `virtual_stock` = 0 ";	
		}
		if(isset($this->request->data['warehouse']) && $this->request->data['warehouse'] != ''){
			$Where[] = " SalesAverage.`warehouse` = '".$this->request->data['warehouse']."' ";	
		} 
 		if(count($Where) > 0){				  
		 	
			$sql_query = ''; $where_col ='';
			if(count($Where)>0){
				$w = 1;
				$count_lenght_of_where = count($Where);
				foreach ($Where as $k) {
					$where_col .= $k;
					if ($w < $count_lenght_of_where) {
						$where_col .=' AND ';
					}
					$w++;
				 }
				 $sql_query =' WHERE '.$where_col;			
			}	
		}
	 $sql = "SELECT SalesAverage.master_sku, SalesAverage.product_name as title, SalesAverage.barcode, SalesAverage.`fresh_required_qty`,SalesAverage.`safety_stock`, SalesAverage.`virtual_stock`, SalesAverage.`stock_in_transit`, SalesAverage.`required_qty`, SalesAverage.`fba_req_qty`, SalesAverage.`fba_virtual_stock`, SalesAverage.`fba_stock_in_preparation`, SalesAverage.`fba_stock_in_tansit`			 							
									FROM sales_averages SalesAverage
 										JOIN  supplier_mappings SupplierMapping 
									ON SalesAverage.`master_sku` = SupplierMapping.`master_sku` $sql_query";
 	
		$product = $this->SalesAverage->query($sql); 
		 
		$dataArray = []; $case_size = 'case_size';
		if(isset($this->request->data['case_size'])){
			$case_size = $this->request->data['case_size'];
		}
		$all_skus = []; $all_packs = [];
		if(count($product)>0){
 			foreach($product as $val){
				$master_sku = $val['SalesAverage']['master_sku'];	
				$all_skus[$master_sku] = $master_sku;
 			}
			$pos = $this->PurchaseOrder->find('all', array('conditions' => array('purchase_sku' => $all_skus),'fields'=>array('purchase_qty','purchase_sku')));
			if(count($pos)>0){
				foreach($pos as $po){
					$all_packs[$po['PurchaseOrder']['purchase_sku']] = 1;//$po['PurchaseOrder']['purchase_qty'];
				}
			}
		}
		
		$min_stock_level = []; $fba_stock_level = []; 
		$pros = $this->Product->find('all', array('conditions' => array('product_sku' => $all_skus),'fields'=>array('min_stock_level','product_sku','fba_stock')));
 		foreach($pros as $pr){
			$min_stock_level[$pr['Product']['product_sku']] = $pr['Product']['min_stock_level'];
			$fba_stock_level[$pr['Product']['product_sku']] = 1;//$pr['Product']['fba_stock'];
 		}
		
		$stock_req   = $this->request->data['stock_req'];
		$expiry_date = date("Y-m-d", strtotime("+{$stock_req} days"));
		  
	 	$_sals =  $this->getAllSkuSale($this->request->data['avr_sale_w'],$stores,$this->request->data['suppliers'],$this->request->data['master_sku']);
		$sales_ch_array = $_sals['mfn'];
		$fba_ch_array   = $_sals['fba'];
		//$fba_ch_array = $this->getAllSkuFbaSale($this->request->data['avr_sale_w'],$stores);
		//pr($sales_array );pr($fba_array );
		$_sales_sku_array = []; $sales_array = [];$sales_detail_array = [];
		foreach($sales_ch_array as $slk => $sl){
			$ssku = $sku_maps[$slk];
			$_sales_sku_array[$ssku][]  =  $sl;
			$sales_detail_array[$ssku][$slk]  =  $sl;
		}
		foreach( array_keys($_sales_sku_array) as $slk ){
			$sales_array[$slk] = array_sum($_sales_sku_array[$slk]);
		}
		$_fba_sku_array = []; $fba_array = []; $fba_detail_array = [];
		foreach($fba_ch_array as $slk => $sl){
			$ssku = $sku_maps[$slk];
			$_fba_sku_array[$ssku][]  =  $sl;
			$fba_detail_array[$ssku][$slk]  =  $sl;
		}
		foreach( array_keys($_fba_sku_array) as $slk ){
			$fba_array[$slk] = array_sum($_fba_sku_array[$slk]);
		}
		
		file_put_contents( WWW_ROOT .'logs/sales_sku_array__.log',print_r($_sales_sku_array,1) );
		file_put_contents( WWW_ROOT .'logs/fba_array.log',print_r($fba_array,1) );
		file_put_contents( WWW_ROOT .'logs/sales_detail_array.log',print_r($sales_detail_array,1) );
		file_put_contents( WWW_ROOT .'logs/fba_detail_array.log',print_r($fba_detail_array,1) );
		@unlink( WWW_ROOT .'logs/sales_detail_array.csv');
		foreach($sales_detail_array as $ss => $yy){
			foreach($yy as $xx => $sq){
				file_put_contents( WWW_ROOT .'logs/sales_detail_array.csv',$ss.",".$xx.",".$sq."\n", FILE_APPEND | LOCK_EX);
			}
		}
		
		@unlink( WWW_ROOT .'logs/fba_detail_array.csv');
		foreach($fba_detail_array as $ss => $yy){
			foreach($yy as $xx => $sq){
				file_put_contents( WWW_ROOT .'logs/fba_detail_array.csv',$ss.",".$xx.",".$sq."\n", FILE_APPEND | LOCK_EX);
			}
		}

		$weekly_sales = []; 
		$round_up 		= 1; 
		if( count($product) > 0 ){
		
			foreach($product as $val){
 				$total_av_sale    = 0; 
				$master_sku = $val['SalesAverage']['master_sku'];
  				$total_days = $this->request->data['avr_sale_w'];
				if(isset($sales_array[$master_sku])){
				 $total_days_av_sale    = number_format($sales_array[$master_sku],2);
				}else{
 			  	  $total_days_av_sale    = 0; 
				}
				if(isset($fba_array[$master_sku])){
				  $fba_days_av_sale  = number_format($fba_array[$master_sku],2);
				}else{
 			  	  $fba_days_av_sale  = 0; 
				}
				$total_av_sale = $total_days_av_sale +  $fba_days_av_sale;
				//$total_days_av_sale = ceil($total_days_sale/$total_days);	
				$weekly_av_sale 	= 1;//ceil($total_weekly_sale/$this->request->data['weekly_sale']);			
				
				$stock_required_m = $total_days_av_sale * $this->request->data['stock_req'];
				$stock_required_f = $fba_days_av_sale * $this->request->data['fba_stock_req'];
				$stock_required = $stock_required_m + $stock_required_f;
				/*if($this->request->data['stock_type'] == 'merchant'){
					$stock_required = $total_days_av_sale * $this->request->data['stock_req'];
				}else if($this->request->data['stock_type'] == 'fba'){
					$stock_required = $fba_days_av_sale * $this->request->data['stock_req'];
				}else{
					$stock_required = ($total_days_av_sale * $this->request->data['stock_req']) + ($fba_days_av_sale * $this->request->data['stock_req']);
				}*/
				$safety_stock = 0;
				if($stock_required < $val['SalesAverage']['safety_stock']){
					$safety_stock   = $val['SalesAverage']['safety_stock'];
 				} 				 
				$total_stock_req  	= $stock_required ;//+ $val['SalesAverage']['safety_stock'];

				
				if(isset($this->request->data['in_transit_excl']) && $this->request->data['in_transit_excl'] != ''){
					$po_qty = $total_stock_req - $val['SalesAverage']['virtual_stock'];	
				}else{
 					$po_qty = $total_stock_req - $val['SalesAverage']['virtual_stock'] - $val['SalesAverage']['stock_in_transit'];	
				}	
 				
				///////////
				$unit = 1; $order_case = '-';
				if(isset($all_packs[$master_sku])){
					$order_case = strtoupper($all_packs[$master_sku]);
  				}
				
				$final_qty = ($po_qty > 0) ?  $this->roundUpToAny($po_qty,$round_up) : $po_qty;
 				if($val['SalesAverage']['fresh_required_qty'] > 0){ $final_qty = $val['SalesAverage']['fresh_required_qty'];}
 				//if($stock_required) $final_qty = $stock_required; 
				
 				$min_sl = 0 ;$min_fba_sl = 0 ;
				if(isset($min_stock_level[$master_sku])){
					$min_sl = max($min_stock_level[$master_sku],0);
  				}
				if(isset($fba_stock_level[$master_sku])){
					$min_fba_sl = max($fba_stock_level[$master_sku],0);
  				}
  		 
 				if($this->request->data['stock_type'] == 'fba'){
					$min_sl = $min_fba_sl;
				}else{
					$min_sl = $min_sl + $min_fba_sl;
				}
				
				
				$stock_expire_with_in_lead_time = 0 ;
				
				if(isset($recom[$master_sku])){
					$stock_expire_with_in_lead_time = max($recom[$master_sku],0);
  				}
			//	$reconsider_stock = $val['SalesAverage']['virtual_stock'] - $stock_to_be_expired;
				//$reconsider_stock = $val['SalesAverage']['virtual_stock'];
				
				 
				$mfn_qty = $stock_required_m - ($val['SalesAverage']['virtual_stock'] + $val['SalesAverage']['stock_in_transit']) ;//+$min_sl
				$fba_qty = $stock_required_f - ($val['SalesAverage']['fba_virtual_stock'] + $val['SalesAverage']['fba_stock_in_tansit'] + $val['SalesAverage']['fba_stock_in_preparation']);
				
				if(($val['SalesAverage']['virtual_stock'] + $val['SalesAverage']['stock_in_transit'] + $val['SalesAverage']['fba_virtual_stock'] + $val['SalesAverage']['fba_stock_in_preparation']  + $val['SalesAverage']['fba_stock_in_tansit']) == 0){
				 $final_qty = max(($mfn_qty+$fba_qty),0) + $safety_stock;
				}else{
				 $final_qty = max(($mfn_qty+$fba_qty),0); 
				} 
				
					
 				$dataArray[$master_sku] = array(
				'sku' => $master_sku,
				'title' => $val['SalesAverage']['title'],
				'barcode' => $val['SalesAverage']['barcode'],
				'stock_to_be_expired' => $stock_to_be_expired,
				'stock_expire_with_in_lead_time' => $stock_expire_with_in_lead_time,
				'min_stock_level' => $min_sl,
				'order_case' => $order_case,
				'final_qty' => $final_qty,
 				'sales_qty' => $total_av_sale,
				'm_sales_qty' => $total_days_av_sale,
				'f_sales_qty' => $fba_days_av_sale,
  				'weekly_av_sale' => $weekly_av_sale,
				'stock_required' => $stock_required,
				'stock_required_m' => $stock_required_m,
				'stock_required_f' => $stock_required_f,
 				'mfn_qty' => $mfn_qty,
				'fba_qty' => $fba_qty,
 				'safety_stock' =>$val['SalesAverage']['safety_stock'],
				'total_stock_req' => $total_stock_req,
				'virtual_stock' => $val['SalesAverage']['virtual_stock'],
				'stock_in_transit' => $val['SalesAverage']['stock_in_transit'],
				'po_qty' => $po_qty,
				'required_qty' => $stock_required,				
				'fresh_required_qty' => $val['SalesAverage']['fresh_required_qty'],
 				'sale_of_days_avr' => $this->request->data['avr_sale_w'],
				'fba_virtual_stock' => $val['SalesAverage']['fba_virtual_stock'],
				'fba_stock_in_preparation' => $val['SalesAverage']['fba_stock_in_preparation'],
				'fba_stock_in_tansits' => $val['SalesAverage']['fba_stock_in_tansit'],
				'fresh' => 0 );
			}
 			$sort_by = isset($this->request->data["name"])? $this->request->data["name"] :'sales_qty';		
			$order_by = $orderby = isset($this->request->data["orderby"]) ? $this->request->data["orderby"] : 'DESC';	
			if(isset($this->request->data["orderby"]) && $this->request->data["orderby"] == 'ASC'){ 			
				$sorted = $this->sksort($dataArray, $sort_by, true);
			}else{
				$sorted = $this->sksort($dataArray, $sort_by);
			}
			
 			if(isset($this->request->data['action']) && ($this->request->data['action']=='generate_po')){ 
		 		$this->generateQuotation($sorted, $this->request->data['stock_type'], $this->request->data['suppliers'],$this->request->data['avr_sale_w'],$this->request->data['buying_company']);
				exit;
			}
					
			if(isset($this->request->data["page"])){
				$page_number = filter_var($this->request->data["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); 
				if(!is_numeric($page_number)){die('Invalid page number!');} 
			}else{
				$page_number = 1; 
			}
			
			$item_per_page  = 50;
			$total_rows 	= count($sorted); 		
			$total_pages	= ceil($total_rows/$item_per_page);
			$page_position  = (($page_number-1) * $item_per_page);
			
			$sortedArry 	= array_slice( $sorted, $page_position,  $item_per_page );
		 
			$count 			= 0;			
			$msg['data']    = '';
			foreach($sortedArry as $sku => $v){	
			
 				$final_qty = $v['final_qty'];
 				if($v['fresh_required_qty'] > 0){ $final_qty = $v['fresh_required_qty'];}
 				//if($v['required_qty']) $final_qty = $v['required_qty']; 
				$vrt = '';//'<div class="col-60">Channel SKU</div><div class="col-10">Type</div><div class="col-10">Qty</div>';
				/*foreach($sales_detail_array[$v['sku']] as $xcs =>$tt){
					$vrt .= '<div class="col-70">'.$xcs.'</div><div class="col-20">MFN</div><div class="col-10">'.$tt .'</div>';
				}
				foreach($fba_detail_array[$v['sku']] as $xcs =>$tt){
					$vrt .= '<div class="col-70">'.$xcs.'</div><div class="col-20">FBA</div><div class="col-10">'.$tt .'</div>';
				}*/
					
   				$count++; 
				$msg['data'].='<tr>';
				$msg['data'].='<td style="min-width: 170px;"><div>'.$v['sku'].'</div><div class="small"><small>'.$v['title'].'</small></div>
				<div><small>'.$vrt .'</small></div>
 				</td>';
				
				if($v['sales_qty']){
					$msg['data'].=' <td>M:'. $v['m_sales_qty'].'<br>F:'.$v['f_sales_qty'].'</td>';
				}else{
					$msg['data'].=' <td>-</td>';
				}
 				 					
				$msg['data'].='<td>'.$v['stock_required_m'] .'</td>';
				$msg['data'].='<td>'.$v['stock_required_f'] .'</td>';
				
				
				//$msg['data'].='<td>'.$v['order_case'].'</td>';
				//$msg['data'].='<td>'.$unit.'</td>';
				//$msg['data'].='<td>'.$v['safety_stock'].'</td>';
				$msg['data'].='	<td>'.$v['stock_required'].'</td>';
				
				
			//	$msg['data'].='<td>'.$v['stock_expire_with_in_lead_time'] .'</td>';
				
				$msg['data'].='<td><center><input type="number" min="1" name="min_stock_level" class="form-control" id="min_stock_level_'.$v['sku'].'" value="'.$v['min_stock_level'].'" onClick="ChangeMinStock(\''.$v['sku'].'\')"></center></td>';  
				
				$msg['data'].='<td>'.$v['virtual_stock'].'</td>';
				$msg['data'].='<td>'.$v['fba_virtual_stock'].'</td>';
				$msg['data'].='<td>'.$v['fba_stock_in_preparation'].'</td>'; 
				/*'fba_virtual_stock' => $val['SalesAverage']['fba_virtual_stock'],
				'fba_stock_in_preparation' => $val['SalesAverage']['fba_stock_in_preparation'],
				 */
				
				
 				$msg['data'].='<td>'.$v['stock_in_transit'].'</td>';
				$msg['data'].='<td>'.$v['fba_stock_in_tansits'].'</td>';
				if($v['fresh']){
					$msg['data'].='<td>'. number_format($final_qty,0) .'</td>';
				}else{
					$msg['data'].='<td><input type="text" value="'.number_format($final_qty,0).'" id="'.$v['sku'].'" size="5" class="cqty" onchange="updateQty(\''.$v['sku'].'\')"></td>';
				}			
				$msg['data'].='</tr>';		
			}	
			
			$msg['paginate'] = '<div class="total_pages">Total Page: '.$total_pages.' Items #'.$count.'</div>
			<div class="paginate">'.$this->paginate_function($item_per_page, $page_number, $total_rows, $total_pages,$sort_by,$order_by,$orderby).	'</div>
			<div class="total_rows">Total Row: '.$total_rows.'</div>';	
		}else{
			$msg['paginate'] ='';		
			$msg['data']='<tr><td colspan="11"><div align="center"><div class="alert alert-danger fade in">No data found!</div></div></td></tr>';
		} 
		$msg['data'] = utf8_encode($msg['data']);
		echo json_encode($msg);
		exit;
	}
	 
	public function  updateRequiredStock(){	
 		
		$this->layout = '';
		$this->autoRender = false; 
		$this->loadModel('SalesAverage'); 
 		if($this->request->data['sku'] !='' && $this->request->data['stock_req'] !=''){		
			 $sql="UPDATE `sales_averages` SET `required_qty` =  '".$this->request->data['stock_req']."' WHERE `master_sku` = '".$this->request->data['sku']."'";
			 $this->SalesAverage->query($sql) ;
			 $msg['msg'] = 'Data updated';		
		}else{
			$msg['msg'] = '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Invalid '.$this->request->data['sku'].'!</div>';
			$msg['error'] = 'yes';
		}		
		echo json_encode($msg);
 		exit;
	}
	
	public function getAllSkuSale($days = 5,$channels = ['costbreaker_de','Marec_uk','costbreaker_frfba'],$suppliers =[],$master_sku = ''){
	
		$this->layout = '';
		$this->autorender = false;
		$this->loadModel('SalesEod'); 
		$this->loadModel('StockEod');
		$this->loadModel('Skumapping');
 		$this->loadModel('SupplierMapping');
		//$this->loadModel('FbaUploadInventory');
		$this->loadModel('ProChannel'); 
		$this->loadModel('FbaStockEod'); 
		
		if(isset($_GET['days'])){
			$days = $_GET['days'];  
			$channels = explode(",",$_GET['stores']);
  		}
		$sup_skus =[];
		if(isset($_GET['supplier'])){
 			$supplier = explode(",",$_GET['supplier']); 
			 
 		}else if(count($suppliers) > 0){
			$supplier = explode(",",$suppliers); 
			
		}
		
		$m_skus = $this->SupplierMapping->find('all', array('conditions' => array('sup_code' => $supplier),'fields' => array('master_sku')));
		if($master_sku != ''){
			$m_skus = $this->SupplierMapping->find('all', array('conditions' => array('sup_code' => $supplier,
			'master_sku'=>$master_sku),'fields' => array('master_sku')));
		}
		foreach($m_skus as $sk){
			$sup_skus[] = $sk['SupplierMapping']['master_sku'];
		}
			
 		$channel_type = [];
		$pchannel = $this->ProChannel->find('all', array('conditions' => array('channel_title' => $channels),'fields' => array('channel_title','channel_type')));
		if(count($pchannel)> 0){
			foreach($pchannel as $sk){
				$channel_type[$sk['ProChannel']['channel_title']] = $sk['ProChannel']['channel_type'];
			}
		}
		
		//$days = $days + 1;
		
		$inactive_skus = [];$sku_maps = []; $all_skus = [];$all_channel_sku = []; $mfn_sku_maps = $fba_sku_maps = []; $all_fba_skus = [];
		$m_skus = $this->Skumapping->find('all', array('conditions' => array('channel_name' => $channels,'sku' => $sup_skus),'fields' => array('sku','channel_sku','stock_status','listing_status','channel_name')));
		foreach($m_skus as $sk){
 			if($sk['Skumapping']['listing_status'] ==  'inactive' || $sk['Skumapping']['stock_status'] ==  'npa'){
				$inactive_skus[$sk['Skumapping']['channel_sku']] = $sk['Skumapping']['channel_sku'];
			}else{
		 		$all_channel_sku[$sk['Skumapping']['channel_sku']] = $sk['Skumapping']['channel_sku'];
				if(isset($channel_type[$sk['Skumapping']['channel_name']])){
					if($channel_type[$sk['Skumapping']['channel_name']] == 'merchant'){
						$mfn_sku_maps[$sk['Skumapping']['sku']][] = $sk['Skumapping']['channel_sku'] ;
					}else if($channel_type[$sk['Skumapping']['channel_name']] == 'fba'){
						$fba_sku_maps[$sk['Skumapping']['sku']][] = $sk['Skumapping']['channel_sku'] ;
						$all_fba_skus[$sk['Skumapping']['channel_sku']] = $sk['Skumapping']['sku'] ;
					}
 				}
			}
   			
			if(substr($sk['Skumapping']['sku'],0,2) == 'S-') {
				$all_skus[$sk['Skumapping']['sku']] = $sk['Skumapping']['sku'];  
			}
  		} 
	 
		file_put_contents( WWW_ROOT .'logs/mfn_sku_maps.log',print_r($mfn_sku_maps,1));

       	$getFrom 	= date('Y-m-d', strtotime("- {$days} days"));
		$getEnd 	= date('Y-m-d', strtotime("- 1 day"));
		//echo  	$getFrom  .'='.$getEnd.'<br>';
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			),
			'fields' => array('total_sale','fba_total_sales','sale_date')
 		);
		  
 		$sales = [];  $skusales = []; $fba_sales = []; $fbaskusales = []; 
		$ssales = []; $fssales = []; $datesArr = []; $ffba_sales = []; $qssales =[];
		 
		$all_data =  $this->SalesEod->find( 'all', $paramHostory);  
		 
		$file_name = 'Sales_'.$getFrom.'-T-'.$getEnd.'_'.$this->Session->read('Auth.User.username').'.csv';
		//if file already exist then direct download
 		if(isset($_GET['days'])){
			/*if(file_exists(WWW_ROOT .'logs/'.$file_name)){
 				if(strtotime(date('Y-m-d H:i:s')) - filemtime(WWW_ROOT.'logs'.DS.$file_name) < 300){ 
 					header('Content-Description: File Transfer');
					header('Content-Type: application/csv');
					header('Content-Disposition: attachment; filename='.basename($file_name));
					header('Content-Transfer-Encoding: binary');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
					header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$file_name)); 
					readfile( WWW_ROOT .'logs/'.$file_name);
					exit;
 				}
 			} */
		}
		 //end
	 	$sep = ",";
		$content = 'SKU,CHANNEL SKU,TYPE'; 
		if(count($all_data) > 0){
			foreach($all_data as $items){
  				$content .= $sep .  date('d M Y', strtotime($items['SalesEod']['sale_date'])) ; 
				file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");
				$date = date('Y-m-d', strtotime($items['SalesEod']['sale_date']));
				$datesArr[$date] = $date;
				$merchant = json_decode($items['SalesEod']['total_sale'],1);
 				
 				// pr($stockEod);exit;
 				foreach($merchant as $sku => $stck){ 
  					/*$ssales[$date][$sku]['sku_sale']  = $stck['sku_sale']	;
					$ssales[$date][$sku]['channel_sku']  = $stck['channel_sku'];*/
					$qw = 0;
					foreach($stck['channel_sku'] as $ch => $q){ 
						if(in_array($ch, $all_channel_sku)){
 							$ssales[$date][$sku]['channel_sku'][$ch] = $q ;
							//$ssales[$date][$sku]['asins']['MFN']     = $q;
							$qw += $q;
						}
 					}
					if($qw > 0){
						$ssales[$date][$sku]['sku_sale'] = $qw;
						
					}
        		}	
					
				$ffba_sales = json_decode($items['SalesEod']['fba_total_sales'],1);
 				
				foreach($ffba_sales as $sku => $stck){ 
 					$qw = 0;
					foreach($stck['channel_sku'] as $ch => $q){ 
						if(in_array($ch, $all_channel_sku)){
							$fba_sales[$ch][] = $q;
							$qw += $q;
							//$ssales[$date][$sku]['sku_sale']     = $q;
							$fssales[$date][$sku]['channel_sku'][$ch] =  $q;
							//$ssales[$date][$sku]['asins']['FBA'] = $q;
						}
					}
					if($qw > 0){
						$fssales[$date][$sku]['sku_sale'] = $qw;
					}
     			}	 
			}		
		 
			//unset($sku_maps);
			unset($all_channel_sku);   
			foreach($all_skus as $sku => $tt){ 
				$content =	$sku ;
				$content .= ' , ,';
				$asin_sku_d = []; $ch_sku_d = []; $type = []; $ch_sku =[]; $cch_sku=[];
				foreach($all_data as $items){								
					$date = date('Y-m-d', strtotime($items['SalesEod']['sale_date']));
					$sale = '-';$fsale=[];
					if(isset($ssales[$date][$sku])){
						$ss = $ssales[$date][$sku];
						$fsale[] = $ss['sku_sale'];
  						foreach($ss['channel_sku'] as $chs => $aqty){
							$type[$sku][$chs] = 'MFN';
							$ch_sku[$sku][$chs] = $aqty;
							$ch_sku_d[$sku][$date][$chs] = $aqty;
							$sales[$chs][]  = $aqty;
						}
						//$stockEod[$date][$sk][$chk] =  $qt;
						
					} 
					$stockEod = [];
					$_stocks = $this->StockEod->find( 'first', array('conditions' => array('StockEod.stock_date' => $date))); 
					if(count($_stocks) > 0){
						$stocks  = json_decode($_stocks['StockEod']['stock'],1);
						 foreach($stocks as $sk => $qt){
							if(isset($mfn_sku_maps[$sk])){
								foreach($mfn_sku_maps[$sk] as $chk){
									$stockEod[$sk][$date][$chk] =  $qt;
								}
							}
 						}
 					}
				
					if(isset($mfn_sku_maps[$sku])){ 
						foreach($mfn_sku_maps[$sku] as $chsi){  
							if(!isset($ch_sku_d[$sku][$date][$chsi])){	 
								if(isset($stockEod[$sku][$date][$chsi])){ 
									if($stockEod[$sku][$date][$chsi] > 0){
										$ch_sku_d[$sku][$date][$chsi] = 0;
										$type[$sku][$chsi] = 'MFN';
										$ch_sku[$sku][$chsi] = 0;
										$sales[$chsi][] = 0;
									}else{
										$ch_sku_d[$sku][$date][$chsi] = '-';
										$type[$sku][$chsi] = 'MFN';
										$ch_sku[$sku][$chsi] =  '-';
									}
								
								}
							}
						}
					}
					//file_put_contents(WWW_ROOT.'logs/stockEod.log',print_r($stockEod,1), FILE_APPEND | LOCK_EX);	 
					if(isset($fssales[$date][$sku])){
						$ss = $fssales[$date][$sku];
						$fsale[] = $ss['sku_sale'];
  						foreach($ss['channel_sku'] as $chs => $aqty){
							$type[$sku][$chs] = 'FBA';  
							$ch_sku[$sku][$chs] = $aqty;
							$ch_sku_d[$sku][$date][$chs] = $aqty;
   						}
						
					}
					
					$stockEod = [];
					$_stocks = $this->FbaStockEod->find( 'first', array('conditions' => array('stock_date' => $date))); 
					if(count($_stocks) > 0){
						$stocks  = json_decode($_stocks['FbaStockEod']['costbreaker'],1);
						foreach($stocks as $sk => $qt){
							 if(isset($all_fba_skus[$sk])){
							    $skk = $all_fba_skus[$sk];
 							 	if(isset($fba_sku_maps[$skk])){
									foreach($fba_sku_maps[$skk] as $chk){
										$stockEod[$skk][$date][$chk] =  $qt;
									}
								}
							}
						}
						$stocks  = json_decode($_stocks['FbaStockEod']['rainbow'],1);
						foreach($stocks as $sk => $qt){
							 if(isset($all_fba_skus[$sk])){
							    $skk = $all_fba_skus[$sk];
								if(isset($fba_sku_maps[$skk])){
								foreach($fba_sku_maps[$skk] as $chk){
									$stockEod[$skk][$date][$chk] =  $qt;
								}
							}
							}
						}
						$stocks  = json_decode($_stocks['FbaStockEod']['marec'],1);
						foreach($stocks as $sk => $qt){
							 if(isset($all_fba_skus[$sk])){
								 $skk = $all_fba_skus[$sk]; 
								 if(isset($fba_sku_maps[$skk])){
								foreach($fba_sku_maps[$skk] as $chk){
									$stockEod[$skk][$date][$chk] =  $qt;
								}
							}
							}
						}
 					}
					
 				 
					if(isset($fba_sku_maps[$sku])){   
						foreach($fba_sku_maps[$sku] as $chsi){ 
							if(!isset($ch_sku_d[$sku][$date][$chsi])){	 
								if(isset($stockEod[$sku][$date][$chsi])){ 
									if($stockEod[$sku][$date][$chsi] > 0){
										$ch_sku_d[$sku][$date][$chsi] = 0;
										$type[$sku][$chsi] = 'FBA';
										$ch_sku[$sku][$chsi] = 0;
										$sales[$chsi][] = 0;
									}else{
										$ch_sku_d[$sku][$date][$chsi] = '-';
										$type[$sku][$chsi] = 'FBA';
										try {
											$ch_sku[$sku][$chsi] =  '-';
										}catch(Exception $e) {
										  echo 'Message: '.$sku.'=='. $chsi.'##'.$e->getMessage();
										}
									}
								
								}
							}
						}
					}
					  
					  if(count($fsale) > 0){
					 	 $sale = array_sum($fsale);
					  }
					 
 					$content .= $sep . $sale; 
				}
				file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n", FILE_APPEND | LOCK_EX);
				//$_ch_sku =  array_merge($ch_sku,$cch_sku);
				
				file_put_contents(WWW_ROOT.'logs/ch_sku.log', print_r($ch_sku,1), FILE_APPEND | LOCK_EX);
				file_put_contents(WWW_ROOT.'logs/cch_sku.log', print_r($cch_sku,1), FILE_APPEND | LOCK_EX);
				
				$str = '';
				if(count($ch_sku) > 0 && isset($ch_sku[$sku])){
					foreach(array_keys($ch_sku[$sku]) as $chs){   
						$str  = ' ,'.$chs . $sep . $type[$sku][$chs];  
						foreach($datesArr as $date){ 
							//if(isset($ch_sku_d[$sku][$date]) && isset($ch_sku_d[$sku][$date][$chs])){
								$str  .= $sep . ($ch_sku_d[$sku][$date][$chs]);
								//file_put_contents(WWW_ROOT.'logs/ch_sku_d.log', print_r($ch_sku_d,1), FILE_APPEND | LOCK_EX);
							//}
						}
						file_put_contents(WWW_ROOT.'logs/'.$file_name, $str."\r\n", FILE_APPEND | LOCK_EX);
					}
				}
				
			} 
			
			if(isset($_GET['days'])){ 
				header('Content-Description: File Transfer');
				header('Content-Type: application/csv');
				header('Content-Disposition: attachment; filename='.basename($file_name));
				header('Content-Transfer-Encoding: binary');
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$file_name));
				ob_clean();
				flush();
				readfile( WWW_ROOT .'logs/'.$file_name);
				exit;
			}
			else{
			
				foreach($sales as $sku => $saleArr){  
					//$skusales[$sku] = ceil(array_sum($sales[$sku])/count($sales[$sku]));
					$skusales[$sku] = (array_sum($sales[$sku])/count($sales[$sku]));
				}
				 
				file_put_contents(WWW_ROOT.'logs/salessales.log',print_r($sales,1), LOCK_EX);
				$fbaskusales = [];	
				foreach($fba_sales as $sku => $saleArr){ 
					//$fbaskusales[$sku] = ceil(array_sum($fba_sales[$sku])/count($fba_sales[$sku]));
					$fbaskusales[$sku] = (array_sum($fba_sales[$sku])/count($fba_sales[$sku]));
				}
				file_put_contents(WWW_ROOT.'logs/fba_salesfba_sales.log',print_r($fba_sales,1), LOCK_EX);
				file_put_contents(WWW_ROOT.'logs/fbaskusales.log',print_r($fbaskusales,1), LOCK_EX);
				return ['mfn'=>$skusales, 'fba'=> $fbaskusales];
			}
			
		}		
 	
 	}
	
	public function getAllSkuFbaSale($days = 5,$channels = ['costbreaker_frfba','marec_uk','rainbow_it_fba']){
	
		$this->autorender = false;
		$this->loadModel('SalesEod'); 
		$this->loadModel('FbaUploadInventory');
		$this->loadModel('ProChannel');
		$this->loadModel('Skumapping');
		
		$inactive_skus = [];$sku_maps = [];
		$all_skus = $this->Skumapping->find('all', array('conditions' => array('channel_name' => $channels),'fields' => array('sku','channel_sku','stock_status','listing_status')));
		foreach($all_skus as $sk){
 			if($sk['Skumapping']['listing_status'] ==  'inactive' || $sk['Skumapping']['stock_status'] ==  'npa'){
				$inactive_skus[$sk['Skumapping']['channel_sku']] = $sk['Skumapping']['channel_sku'];
			}
 		 	$sku_maps[$sk['Skumapping']['channel_sku']] = $sk['Skumapping']['sku'];
			 
  		}
		
       	$getFrom 	= date('Y-m-d' ,strtotime("- {$days} days"));
		$getEnd 	= date('Y-m-d');
		
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		 
 		$sales = []; $skusales = [];
		$all_data =  $this->SalesEod->find( 'all', $paramHostory); 
		
		$file_name = 'Sales_'.$getFrom.'TO'.$getEnd.'.csv';
		file_put_contents( WWW_ROOT .'logs/fbasale.log',print_r($channels,1));
 		
		//exit;
 		if(count($all_data) > 0){
			foreach($all_data as $items){
			
				$channel_sale = json_decode($items['SalesEod']['fba_source_sale'],1);
				 
				$sale_date =  $items['SalesEod']['sale_date'];
				//$_stocks = $this->StockEod->find( 'first', array('conditions' => array('StockEod.stock_date' => $items['SalesEod']['sale_date']))); 
				//$stocks  = json_decode($_stocks['StockEod']['stock'],1);
				// pr($channel_sale);exit;
				foreach($channel_sale as $ch_name => $channel){  
 						$invdata = [];
						$chnls = $this->ProChannel->find('all', array('conditions' => array('channel_title LIKE' => $ch_name.'%','channel_type' => 'fba'),'fields' => array('country','channel_type','channel_title'))); 
					 
 						if(count($chnls) > 0){ 
						
							foreach($chnls as $chnl){
								$store  = '';
								if(substr(strtolower($ch_name),0,5) == 'marec'){
									$store = 'marec';
								}
								else if(substr(strtolower($ch_name),0,5) == 'costb'){
									$store = 'costbreaker';
								} 
								else if(substr(strtolower($ch_name),0,7) == 'rainbow'){
									$store = 'rainbow';
								}
								//echo $sale_date.' == ' .$store .' = '.$chnl['ProChannel']['country'];
								//echo '<br>';
								$invdata =  $this->FbaUploadInventory->find( 'all', array(
										'conditions' => array(
											'FbaUploadInventory.added_date LIKE' => $sale_date.'%',
											'FbaUploadInventory.store' => $store, 
											'FbaUploadInventory.total_units >' => 0, 
											'FbaUploadInventory.country' => $chnl['ProChannel']['country'], 
										),
										'fields' => array(
											'seller_sku','sku','total_units'
										)
									)); 
									
								$fba_stocks = [];	 
								if(count($invdata) > 0){
									foreach($invdata as $fbas){
										$fba_stocks[$fbas['FbaUploadInventory']['sku']][] = $fbas['FbaUploadInventory']['total_units'];
 									}
								}
								$stocks = [];
								if(count($fba_stocks) > 0){
									foreach(array_keys($fba_stocks) as $msk){
										$stocks[$msk] = array_sum($fba_stocks[$msk]);
									}
								}
 							 
								$in_stock = 0;
								foreach($channel as $channel_sku => $stock){ 
									if(isset($sku_maps[$channel_sku])){
										$master_sku = $sku_maps[$channel_sku];
										if(substr($master_sku,0,1) == 'B'){
											$_sk = explode("-",$master_sku);
											$ind = count($_sk) - 1;
											$_sku = '';
											for($b = 1; $b < $ind  ; $b++){
												$_sku = 'S-'.$_sk[$b];
												if(isset($stocks[$_sku]) && $stocks[$_sku] > 0){
													$in_stock = 1;
												} 
											}
										}
										else if(isset($stocks[$master_sku]) && $stocks[$master_sku] > 0){
											$in_stock = 1;
										} 
									   //$channel_sku.'='.$in_stock;
									    //$in_stock > 0 &&
 										if(!in_array($channel_sku, $inactive_skus)){
											$sales[$channel_sku][$items['SalesEod']['sale_date']] = $stock;	 
										}
									
									}
								}
							
							}
						} 
				}	
   			}	
		}
		 
		foreach($sales as $sku => $saleArr){ 
 			$skusales[$sku] = ceil(array_sum($sales[$sku])/count($sales[$sku]));
		}
  		return $skusales;
 	}

 	public function downloadAsinChannelSkuEod()
	{  
		$this->layout = 'index';
		$this->autorender = false;
		$this->loadModel('Product');  
 		$this->loadModel('StockEod'); 
		$this->loadModel('SalesEod'); 
		
       	$getFrom 	= date('Y-m-d' ,strtotime($this->request->query['start']));
		$getEnd 	= date('Y-m-d' ,strtotime($this->request->query['end']));
		
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		//Processing now
		$all_data =  $this->SalesEod->find( 'all', $paramHostory); 
		
		ob_clean(); 
		$sep = ",";
		$content = 'SKU,CHANNEL SKU,ASIN'; 
		$salesData = []; $sales = [];$msales = []; $fsales = []; $datesArr = [];
		$file_name = 'sale_fba_fbm_'.$this->Session->read('Auth.User.username').'.csv'; 
		file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");	
		if(count($all_data) > 0){
			foreach($all_data as $items){
				$content .= $sep .  date('d M Y', strtotime($items['SalesEod']['sale_date'])) ; 
				file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");
				
				$date = date('Y-m-d', strtotime($items['SalesEod']['sale_date']));
				$datesArr[$date] = $date;
				
 				$merchant = json_decode($items['SalesEod']['total_sale'],1);
 				foreach($merchant as $sku => $stock){ 
					$sales[$items['SalesEod']['sale_date']][$sku]['sku_sale']  = $stock['sku_sale']	;
					$sales[$items['SalesEod']['sale_date']][$sku]['channel_sku']  = $stock['channel_sku'];
					$sales[$items['SalesEod']['sale_date']][$sku]['asins']  = $stock['asins'];
   				}	
				$fba_sales = json_decode($items['SalesEod']['fba_total_sales'],1);
 				 
				foreach($fba_sales as $sku => $stock){ 
					if(isset($sales[$items['SalesEod']['sale_date']][$sku])){
						$sku_sale = $sales[$items['SalesEod']['sale_date']][$sku]['sku_sale'];
 						$sales[$items['SalesEod']['sale_date']][$sku]['sku_sale'] = ($sku_sale + $stock['sku_sale']);
						
						$mchannel_sku = $sales[$items['SalesEod']['sale_date']][$sku]['channel_sku'];
						$fchannel_sku = $stock['channel_sku'];
						$sums = [];
						foreach (array_keys($mchannel_sku + $fchannel_sku) as $key) {
							$sums[$key] = (isset($mchannel_sku[$key]) ? $mchannel_sku[$key] : 0) + (isset($fchannel_sku[$key]) ? $fchannel_sku[$key] : 0);
						}
  						$sales[$items['SalesEod']['sale_date']][$sku]['channel_sku'] = $sums;
						
						$masins = $sales[$items['SalesEod']['sale_date']][$sku]['asins'];
						$fasins = $stock['asins'];
						$sums = [];
						foreach (array_keys($masins + $fasins) as $key) {
							$sums[$key] = (isset($masins[$key]) ? $masins[$key] : 0) + (isset($fasins[$key]) ? $fasins[$key] : 0);
						}
  						$sales[$items['SalesEod']['sale_date']][$sku]['asins'] = $sums;
						
					}else{
						$sales[$items['SalesEod']['sale_date']][$sku]['sku_sale']  = $stock['sku_sale']	;
						$sales[$items['SalesEod']['sale_date']][$sku]['channel_sku']  = $stock['channel_sku'];
						$sales[$items['SalesEod']['sale_date']][$sku]['asins']  = $stock['asins'];
					}
   				}	
				
 			}	
   		}	 		 
		 		 
		foreach($salesData as $date => $data){ 
 		  foreach(array_keys($data) as $sku){
			 $sales[$date][$sku] =  array_sum($data[$sku])	;
		  }
 		}
 		//pr($sales);
		//exit; 
		$ch_sku =[]; 
  		$products = $this->Product->find('all', array('conditions'=>['is_deleted' => 0,'product_sku LIKE' => 'S-%'],'fields' =>['product_sku'],'order' => 'product_sku desc') ); 	
		foreach($products as $pr){
			$content =	$sku = trim($pr['Product']['product_sku']);
			$content .= ' , ,';
  			$asin_sku_d = []; $ch_sku_d = []; $asin_sku = [];
			foreach($all_data as $items){								
				$date = $items['SalesEod']['sale_date'];  
   				$sale = '-';
  				if(isset($sales[$date][$sku])){
					$ss = $sales[$date][$sku];
					$sale = str_replace(",",";", json_encode($ss['sku_sale']) );
					//$sale .= str_replace(",",";", json_encode($ss['channel_sku']) ). str_replace(",",";",json_encode($ss['asins']));
					
					foreach($ss['asins'] as $asn => $aqty){
						//$asin_sku[$sku][$asn][] = $aqty;
						$asin_sku_d[$sku][$date][$asn][] = $aqty;
						foreach($ss['channel_sku'] as $chs => $q){
							$asin_sku[$sku][$chs] = $asn;
						}
					}
					
					foreach($ss['channel_sku'] as $chs => $aqty){
						$ch_sku[$sku][$chs][] = $aqty;
						$ch_sku_d[$sku][$date][$chs][] = $aqty;
					}
					
 					//$sale = str_replace(",",";", json_encode($ss['channel_sku']) ). str_replace(",",";",json_encode($ss['asins']));
 				} 
				
 				$content .= $sep . $sale; 
			}
 			file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n", FILE_APPEND | LOCK_EX);
			
 			$str = '';
			foreach(array_keys($ch_sku[$sku]) as $chs){   
 				$str  = ' ,'.$chs . $sep . $asin_sku[$sku][$chs];  
				foreach($datesArr as $date){  
				 	$str  .= $sep . array_sum($ch_sku_d[$sku][$date][$chs]);
					//$str  .= $sep .  date('d M Y', strtotime($date)) ; 
					
				}
 				file_put_contents(WWW_ROOT.'logs/'.$file_name, $str."\r\n", FILE_APPEND | LOCK_EX);
			}
			
		}
		//exit;
		  
 		header('Content-Description: File Transfer');
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename='.basename($file_name));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$file_name));
		ob_clean();
		flush();
		readfile( WWW_ROOT .'logs/'.$file_name);
		exit;
 	}
	
 	public function ChangeMinStockLavel()
	{
		$this->layout = 'ajax';
		$this->autoRender = false;
		$this->loadModel( 'Product' ); 
		$savedata = [];
		 
		$product =  $this->Product->find('first', array('conditions' => array('Product.product_sku' => $this->request->data['sku'])));
		if(count($product) > 0){
 			$savedata['sku'] = $product['Product']['product_sku'] ;
			$savedata['msg'] = 'ok';
 			$this->Product->query("UPDATE `products` SET `min_stock_level` = '".$this->request->data['qty']."' WHERE  `product_sku` ='".$product['Product']['product_sku']."'");
			file_put_contents(WWW_ROOT .'logs/Reorder.min_stock_level_'.date('dmY').'.log', $product['Product']['product_sku']."\t".$product['Product']['min_stock_level']."\t".$this->request->data['qty']."\t".$this->Session->read('Auth.User.username')."\n",FILE_APPEND | LOCK_EX);
 		}
		 
		echo json_encode($savedata);
		exit;
	}
	
 	public function chk_supplier_mappings(){
 		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('SupplierMapping');
		$results = $this->SupplierMapping->find('all',array('order'=>'master_sku')); //,'limit'=>3000,'page'=>1
		foreach($results as $v){
		
			if($v['SupplierMapping']['sup_code'] == 'oa-fcg-' || $v['SupplierMapping']['sup_code'] == 'oa-fcg--'){
				$sup_code = 'oa-fcg' ;
			}
		
			$result = $this->SupplierMapping->find('all', array('conditions' => array("sup_code LIKE" => $sup_code.'%','master_sku' => $v['SupplierMapping']['master_sku'],'supplier_sku' => $v['SupplierMapping']['supplier_sku']),'order'=>'id desc')); 
			if(count($result) > 0){
				foreach($result as $c => $v){
					if($c > 0){						
						$this->SupplierMapping->query("DELETE FROM `supplier_mappings` WHERE `id` = '".$v['SupplierMapping']['id']."'");
						echo '<br>delete = '.$v['SupplierMapping']['id'] .'= '.$v['SupplierMapping']['master_sku'];
					}else{
						echo '<br>keep '.$c.' = '.$v['SupplierMapping']['id'] ;
					}
				}
			}
		}
/*WHERE `sup_code` LIKE 'management-all' 
DELETE FROM `a1b20310_wmsdev`.`supplier_mappings` WHERE `sup_code` LIKE 'management-all' 
UPDATE `a1b20310_wmsdev`.`supplier_mappings` SET `sup_code` = 'DCS' WHERE `sup_code` LIKE '%_dcs%';
*/
 	  exit;
 		 
 	}
 	public function generateQuotation($dataArray = [], $nature_of_po = '', $sup_code = '',$sale_of_days_avr = '',$buying_company = ''){

		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Quotation');
		$this->loadModel('QuotationItem');
		$this->loadModel('SupplierDetail'); 
		$this->loadModel( 'Company' );
		
		$company = $this->Company->find('first', array('conditions' => array('company_name' => $buying_company)));

 		$quotation = array();
		 
		foreach($dataArray as $sku => $v){	
			 
			if( $v['fresh'] > 0 ){
				$v['required_qty'] = 1;	
			} 
			
			//if($v['po_qty'] > 0 || $v['required_qty'] > 0 ){	
				
				if($v['required_qty'] ){
					$v['po_qty'] = $v['required_qty'] ;
				}					
  				$quotation[] = array('title' => $v['title'], 'sku' => $v['sku'],'barcode' => $v['barcode'],'po_qty'=>$v['po_qty'],'virtual_stock'=>$v['virtual_stock'],'stock_in_transit'=>$v['stock_in_transit'],'final_qty'=>$v['final_qty'],'sales_qty' => $v['sales_qty']); 
 								
			//}	
		}	
 		 file_put_contents( WWW_ROOT .'logs/quotation_'.date('YmdHis').'.log',print_r($quotation,1), FILE_APPEND | LOCK_EX);
  		/**************************************Generate Quate*****************************************************/ 
		if(count($quotation)> 0){
			 
			// print_r($suppliers); exit;
			   
			App::import('Vendor', 'PHPExcel/IOFactory');
			App::import('Vendor', 'PHPExcel');
			$objPHPExcel = new PHPExcel();
			
			$file_folder = 'quotation/'; $zip_file_name = '';						
			if(extension_loaded('zip'))
			{
				//$zip = new ZipArchive(); // Load zip library 
				//$zip_file_name = "quotation".date('dmYHis').".zip"; // Zip name
				//$zip_name  = $file_folder.$zip_file_name; 
				//$zip->open($zip_name, ZipArchive::CREATE);
			}
			$f_name = '';
								
			$quotation = $this->sksort($quotation, 'sales_qty');	
			$quotation_nid = date('ymdHis')."_".substr(str_shuffle("0123456789"), 0, 3);

 			$results = $this->SupplierDetail->find('all', array('conditions' => array('supplier_code' => explode(",",$sup_code)),'ORDER'=>'supplier_name'));
		 		
			if(count($results) > 0){			
				foreach($results as $sup){		 			
 				
					$row = 1;
					$rightBorder = array(
								  'borders' => array(
										'right' => array(
										  'style' => PHPExcel_Style_Border::BORDER_THIN
										),	
										'left' => array(
										  'style' => PHPExcel_Style_Border::BORDER_THIN
										)																
								  )
								);									
					
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':H'.$row);
					
					$row++;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Euraco Group Ltd')
							->setCellValue('E'.$row, 'Purchase Order');				
					$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);	
					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(70);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(20)->setBold(true);					
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('EB984E');
 					$row++;		
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':H'.$row);		
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$company['Company']['company_name']."\n".$company['Company']['operational_address'] /*"Euraco Group Ltd 49 Oxford Road St. Helier Jersey JE2 4LJ United Kingdom"*/);
					 
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
 					$row++;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':H'.$row);
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Tel :'.$company['Company']['phone_no'].', Email :'.$company['Company']['email']);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
 					$row++;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':H'.$row);
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'GST No:'.$company['Company']['cin']);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
 					$row++;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':H'.$row);
					
					$row++;				
					$BStyle = array(
								  'borders' => array(
									'top' => array(
									  'style' => PHPExcel_Style_Border::BORDER_THIN
									),
									'bottom' => array(
									  'style' => PHPExcel_Style_Border::BORDER_THIN
									)
								  )
								);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(12)->setBold(true);	
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Purchase From')
							->setCellValue('B'.$row, 'Deliver To')	
							->setCellValue('E'.$row, 'Tender Details');					
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($BStyle);																						
					unset($BStyle);
					
					$row++;
					$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(80);	
	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getAlignment()->applyFromArray(
						array(
								 'vertical'   => PHPExcel_Style_Alignment::VERTICAL_TOP,
								 'rotation'   => 0,
								 'wrap'       => true)
					);
	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $sup['SupplierDetail']['address']);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setWrapText(true);
					
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.$row.':D'.$row);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $company['Company']['company_name']."\n".$company['Company']['operational_address']/*"Euraco Group Ltd c/o Pro Fulfilment & Logistics,   
	Warehouse 4 Airport Cargo & Commercial Units,  
	L'avenue De La Commune,
	St Peter,
	Jersey, JE3 7BY"*/);
	
					$quotation_number =  $quotation_nid.'_'.strtoupper($sup['SupplierDetail']['supplier_code']).'_'.strtoupper($nature_of_po);
  					
					$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setWrapText(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B'.$row)->setWidth(45);
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E'.$row.':H'.$row);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, "PO. No# ".$quotation_number.																 																	    "\nDate:". date('m/d/Y'));
					$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setWrapText(true);
						
					$row++;		
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($rightBorder);					
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':H'.$row);
					$row++;
					
					$BStyle = array(
					  'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
					);
					 
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($BStyle);
					
					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$row, 'DESCRIPTION')
								->setCellValue('B'.$row, 'OUR CODE')
								->setCellValue('C'.$row, 'BARCODE')
								->setCellValue('D'.$row, 'SUPPLIER CODE')								
 								->setCellValue('E'.$row, 'REQUIRED QTY')
								/*->setCellValue('F'.$row, 'CASE SIZE')	
								->setCellValue('G'.$row, 'CASE QTY')*/
								->setCellValue('F'.$row, 'TOTAL QTY')	
								->setCellValue('G'.$row, 'UNIT PRICE')  
								//->setCellValue('H'.$row, 'CASE PRICE') 
 								->setCellValue('H'.$row, 'QTY AVAILABLE') ;	
					$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(11);	
					$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(12)->setBold(true);	
					
					/********************Save Quotation************************/
					//$file_name = $quotation_number.'.xlsx';
					$file_name = $sup['SupplierDetail']['supplier_code'].'_'.strtoupper($nature_of_po).'_'.$sale_of_days_avr.'_AVER_'.date('dmY').'.xlsx';
 					$quotation['quotation_id'] 		= $quotation_nid;	
					$quotation['quotation_number']  = $quotation_number;	
 					$quotation['supplier_code'] 	= $sup['SupplierDetail']['supplier_code'];
					$quotation['file_name'] 	    = $file_name;		
					$quotation['username'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');	
					$this->Quotation->saveAll( $quotation );	
					$quotation_id = $this->Quotation->getLastInsertId();
					/********************End Save Quotation**********************/	
 					
					$items = 0;
					foreach($quotation as $val){
							$supp_data = $this->getSupplierSku($sup['SupplierDetail']['supplier_code'],$val['sku']);							
							if(count($supp_data) > 0 && ($val['sku'] != $val['barcode'])){	 // filter supplier products						
								
 								foreach($supp_data as $supp_sku){
									$row++;	
									$items++;
									$case_size = 1; $case_qty = 0; $unit_price = 0; $case_price = 0; $order_case = 1;
									$po_data = $this->PurchaseOrders($supp_sku['supplier_sku'],$val['sku']);	
									if(count($po_data) > 0 )
									{
										$case_size  = 1;//($po_data['total_qty']/$po_data['order_case']); 
										
										$unit_price = $po_data['price']; 
										$case_price = $po_data['price']; 
										$order_case = $po_data['purchase_qty']; 
										$case_qty   = round($val['final_qty']/$order_case); 
										
									}else{
										$case_qty   = $val['final_qty']; 
									} 
  									
									$final_qty = $case_size * $case_qty;
									$objPHPExcel->setActiveSheetIndex(0)
												->setCellValue('A'.$row, $val['title'])														
												->setCellValue('B'.$row, $val['sku'])
												->setCellValue('C'.$row, $val['barcode'])
												->setCellValue('D'.$row, $supp_sku['supplier_sku'])
												->setCellValue('E'.$row, $val['final_qty'])												
												/*->setCellValue('F'.$row, $order_case)
												->setCellValue('G'.$row, $case_qty)*/
												->setCellValue('F'.$row, round($case_qty*$order_case)) 
 												->setCellValue('G'.$row, '')
												->setCellValue('H'.$row, '');
											//	->setCellValue('I'.$row, '');
												
									$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->applyFromArray($BStyle);	
									
									if(count($supp_data) > 1){
										$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('EB984E');
									}
									
									$quote = [];
									$quote['quotation_inc_id']  = $quotation_id;
									$quote['quotation_number']  = $quotation_number;
									$quote['supplier_itemcode'] = $supp_sku['supplier_sku'];
									$quote['master_sku'] 	    = $val['sku'];
									$quote['barcode'] 		    = $val['barcode'];
									$quote['calculated_qty'] 	= $val['po_qty'];								
									$quote['case_size'] 		= $case_size;
									$quote['qty'] 			    = $final_qty;
									$quote['virtual_stock'] 	= $val['virtual_stock'];
									$quote['stock_in_transit']  = $val['stock_in_transit'];
 									$this->QuotationItem->saveAll( $quote ); 
						 		}
							}
					}
					/*$row++;			
					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$row, '')														
								->setCellValue('B'.$row, '')
								->setCellValue('C'.$row, '')
								->setCellValue('D'.$row, '')
								->setCellValue('E'.$row, '')							
								->setCellValue('F'.$row, '') ;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($BStyle);	*/
					if($items > 0){
								
						if($sup['SupplierDetail']['supplier_code']){	
							$objPHPExcel->getActiveSheet()->setTitle($sup['SupplierDetail']['supplier_code']);	
						}									
						$f_name	.= '<li><a href="'.Router::url('/', true).'quotation/'.$file_name.'" target="_blank">'.ucfirst($sup['SupplierDetail']['supplier_code']).' '.strtoupper($nature_of_po).'</a></li>'; 
						$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');		
						$objWriter->save(WWW_ROOT.'quotation/'.$file_name);	
						//$zip->addFile( 'quotation/'.$file_name);
						
					}else{
  						 $this->Quotation->query( "DELETE FROM `quotations` WHERE `quotation_number` = '".$quotation_number."' AND `supplier_code` = '".$sup['SupplierDetail']['supplier_code']."' " ); 
						 $f_name	.= '<li class="nf">Items not found for '.ucfirst($sup['SupplierDetail']['supplier_code']).'</li>'; 
					}
							
				} 
			}			
			$msg['msg'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Report Generated.</div>';	
  			$f_name	.= '<li><a href="'.Router::url('/', true).'quotation/'.$zip_file_name.'" target="_blank">'.$zip_file_name.'</a></li>'; 
			$msg['file_link'] = $f_name;	
		//	$zip->close();		
			echo json_encode($msg);	
		}
		else{
			$msg['file_link'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Items not found.</div>';	
			echo json_encode($msg);	
		}

	}
  
	public function PurchaseOrders($supplier_p_code,$msku){
		$this->loadModel('PurchaseOrder');
 		$result = $this->PurchaseOrder->find('first', array('conditions' => array('supplier_code' => $supplier_p_code,'purchase_sku' => $msku),'fields'=>['price','purchase_qty']),'order DESC');
		if(count($result) > 0){			
			return	array('price'=>$result['PurchaseOrder']['price'],'purchase_qty'=> $result['PurchaseOrder']['purchase_qty']);
		}else{
			return [];
		}
	}
	
	public function getSupplierMapId($supplier_code,$msku){
		$this->loadModel('SupplierMapping');
 		$result = $this->SupplierMapping->find('first', array('conditions' => array('sup_code' => $supplier_code,'master_sku' => $msku),'fields'=>['id','supplier_sku']));
		if(count($result) > 0){			
			return	1;
		}else{
			return 0;
		}
	}
	public function getSupplierDetails($code){
		$this->loadModel('SupplierDetail');
		$sql = "SELECT * FROM supplier_details WHERE supplier_code = '".$code."'";
		$result =  $this->SupplierDetail->query($sql);	
		if(count($result) > 0){							
			return $result;			
		}else{
			return 0;
		}
	}
	public function getSupplierSku($code,$msku){
		$this->loadModel('SupplierMapping');	
		$data = [];
		$result = $this->SupplierMapping->find('all', array('conditions' => array('sup_code' => $code,'master_sku' => $msku),'fields'=>['id','supplier_sku']));
  		if(count($result) > 0){	
 			foreach($result as $val){
 				if($val['SupplierMapping']['supplier_sku']){	
					 $data[] = array('supplier_sku'=>$val['SupplierMapping']['supplier_sku'], 'id'=>$val['SupplierMapping']['id']);
				}else{
					$data[] = array('supplier_sku'=>'NoSKU', 'id' => $val['SupplierMapping']['id']);
				}
 			}	
 		}else{
			$data = [];// array('supplier_sku'=>'-', 'id'=>0);
		}
		return $data;
	}
	
	public function sksort(&$array, $subkey="id", $sort_ascending=false) {
	
 		if (count($array))
			$temp_array[key($array)] = array_shift($array);
	
		foreach($array as $key => $val){
			$offset = 0;
			$found = false;
			foreach($temp_array as $tmp_key => $tmp_val)
			{
				if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey]))
				{
					$temp_array = array_merge(    (array)array_slice($temp_array,0,$offset),
												array($key => $val),
												array_slice($temp_array,$offset)
											  );
					$found = true;
				}
				$offset++;
			}
			if(!$found) $temp_array = array_merge($temp_array, array($key => $val));
		}
	
		if ($sort_ascending) $array = array_reverse($temp_array);
	
		else $array = $temp_array;
		
		return $array;
	}
	
	public function viewRecomendation()
	{
 		$this->layout = 'index';
 		$this->loadModel( 'SalesAverage' ); 
 		$this->loadModel( 'SupplierMapping' ); 
 	 	$this->loadModel( 'SupplierDetail' );  
		$chk_sa = $this->SupplierDetail->find('all', array('fields'=>array('supplier_code','lead_time','min_order_amount','supplier_name')));
		$supplier_data = [];
		if(count($chk_sa) > 0){ 
 			foreach($chk_sa as $val){
				$supplier_data[$val['SupplierDetail']['supplier_code']] = $val['SupplierDetail']['lead_time'];
			}
		}
			
		$path = WWW_ROOT.'logs/';
 		
 		$stockDetail = $this->SalesAverage->find('all');
	   
		$suppliers = [];$supplier_skus = [];
		foreach($stockDetail as $val){
			 
			$sku = $val['SalesAverage']['master_sku'];
			$smap = 'no_supplier';
			$sup = $this->SupplierMapping->find('first', array('conditions' => array( 'master_sku' => $sku),'fields'=>['sup_code','supplier_sku'],'order' => 'id') );
			if(count($sup) > 0 && !empty($sup['SupplierMapping']['sup_code']) ){
				$smap = $sup['SupplierMapping']['sup_code'] ;
				$suppliers[$smap] = $smap;	
				$supplier_skus[$smap][] = $sku;		
			} else{
				$supplier_skus[$smap][] = $sku;	
 				file_put_contents(WWW_ROOT .'logs/Recomendation_nsupp_'.date('ymd').'.log',$sku."\r\n", FILE_APPEND | LOCK_EX);	
			}
 		}
 		file_put_contents(WWW_ROOT .'logs/Recomendation.log',print_r($other,1));	 		
  				
 			//	pr($pp);exit;
	 
		$this->set('suppliers',$suppliers);	
		$this->set('supplier_data',$supplier_data);
		$this->set('supplier_skus',$supplier_skus);
		
	}
	
	public function recomendationStock()
	{
 		/*-----------------Checked in but not sold-----------------*/
		$this->layout = 'index';
 		
 	 
		$path = WWW_ROOT.'logs/';
		 
 		$rec = [];
		$date = date("Y-m-d",strtotime("+1 day"));
		
		$productArr = [];
		$products = $this->Product->find('all', array('fields'=>['product_sku','nature_of_sku']) );
  	 	foreach($products as $pro )
		{
			$productArr[$pro['Product']['product_sku']] = $pro['Product']['nature_of_sku'];
 		}
		
 		$stockDetail = $this->CheckIn->find('all', array('conditions' => array( 'expiry_date >= ' => $date),'fields'=>['id','sku','date','expiry_date','qty_checkIn','selling_qty','damage_qty','expired_selling_qty']) );
	  
		foreach($stockDetail as $inv )
		{
 			$sku = trim($inv['CheckIn']['sku']);
			$qty = $inv['CheckIn']['qty_checkIn'] - ($inv['CheckIn']['selling_qty'] + $inv['CheckIn']['damage_qty'] + $inv['CheckIn']['expired_selling_qty']);
			$rec[$sku][] = $qty;
   		}
		$recom = [];
		foreach($rec as $sk => $arr){
			$recom[$sk] = array_sum($rec[$sk]);
		}
		$ambient = []; $fresh = [];$frozen = []; $other =[]; $suppliers = []; 
		foreach($recom as $sk => $qt){
			if($qt < 1){
				
				$smap = 'no supplier';
				$sup = $this->SupplierMapping->find('first', array('conditions' => array( 'master_sku' => $sk),'fields'=>['sup_code','supplier_sku'],'order' => 'id') );
  				if(count($sup) > 0 && !empty($sup['SupplierMapping']['sup_code']) ){
 					$smap = $sup['SupplierMapping']['sup_code'] ;
  				} 
				$nature_of_sku = 'not found';
				if(isset($productArr[$sk]) && !empty($productArr[$sk])){
					$nature_of_sku = $productArr[$sk];
				}
				if($nature_of_sku == 'ambient'){
					$ambient[$smap][] = $sk ;
				}
				else if($nature_of_sku == 'fresh'){
					$fresh[$smap][] = $sk ;
				}
				else if($nature_of_sku == 'frozen'){
					$frozen[$smap][] = $sk ;
				}else{
					$other[$smap][] = $sk ;
				}
 				$suppliers[$smap][$nature_of_sku] = $smap;	
 				
 			}
		}
 				
		$this->set('ambient',$ambient);
		$this->set('frozen',$frozen);		
		$this->set('fresh',$fresh);	
		$this->set('other',$other);	
		$this->set('suppliers',$suppliers);	
	}
	
	public function minStockLevel()
    {
		//min stock lavel 
   		$this->layout = 'ajax';
		$this->autoRender = false;
 		
  		$this->loadModel('Product');
		$this->loadModel('PurchaseOrder');
  	 
 		$pack_size = []; 
 		 
		$products = $this->Product->find('all', array('fields'=>array('id','product_sku','current_stock_level','min_stock_level','barcode','product_name','nature_of_sku') ));
  		if(count($products) > 0){		
			foreach($products as $val){
				$product_sku =  $val['Product']['product_sku'];
				
				$purchase_ord = $this->PurchaseOrder->find('first', array('conditions' => array('purchase_sku'=>$product_sku),'fields'=>array('id','purchase_sku','pack_size','order_case','purchase_qty'),'order' => 'id DESC'));
 		 		$order_case = 0;
				if(count($purchase_ord) > 0){  
 					$psz = explode("X", strtoupper($purchase_ord['PurchaseOrder']['pack_size']));
					$pack_size[] = trim($psz['0']);
					$order_case = $purchase_ord['PurchaseOrder']['purchase_qty'];
 				}
				$minsl = 1; $cal_minsl = 0; 
				$_pack_size = min($pack_size);
				if($order_case > 0){
					$cal_minsl = $order_case * '0.5';
					echo $product_sku.' =='.$minsl.' == '.$cal_minsl.' oc = '.$order_case;echo '<br>';
					//$this->Product->updateAll(array('pack_size' => "'".$_pack_size."'",'min_stock_level'=>"'".$minsl."'"), array('product_sku' => $product_sku));
					//exit;
				} 
				if(empty($_pack_size)){
					//echo $product_sku.'<br>';
				}
				
				$log_file =  WWW_ROOT .'logs/minStockLevel_'.date('dmY').'.log';			 
				file_put_contents($log_file,$product_sku."\t".$_pack_size."\tm=".$cal_minsl."\r\n", FILE_APPEND | LOCK_EX);	 		
  				$this->Product->updateAll(array('pack_size' => "'". addslashes($_pack_size)."'",'min_stock_level'=>"'".max($minsl,$cal_minsl)."'"), array('product_sku' => $product_sku));
				
   			}
 		}
  		exit;
 	} 

 	public function  virtualInTransitStock(){	
 		$this->layout = 'ajax';
		$this->autoRender = false;
		$this->loadModel('Po'); 
		$this->loadModel('Product');
		$this->loadModel('PurchaseOrder');
		$this->loadModel('SalesAverage');
 		$this->loadModel('CheckIn');
		$this->loadModel('FbaShipment'); 
 		$this->loadModel('FbaShipmentItem');  
		 
  		$this->loadModel('Skumapping');
  		$this->loadModel('FbaStockEod');
		$this->loadModel('ProChannel');
		
		$channels = [];
		$pchannel = $this->ProChannel->find('all', array('conditions' => array('channel_type' => 'fba'),'fields' => array('channel_title','channel_type')));
		if(count($pchannel)> 0){
			foreach($pchannel as $sk){
				$channels[$sk['ProChannel']['channel_title']] = $sk['ProChannel']['channel_title'];
			}
		}
		
 		$all_channel_sku = [];
 		$m_skus = $this->Skumapping->find('all', array('conditions' => array('channel_name' => $channels),'fields' => array('sku','channel_sku')));
		foreach($m_skus as $sk){
	  		$all_channel_sku[$sk['Skumapping']['channel_sku']] = $sk['Skumapping']['sku'];
 		}
	 
 		$sql = "SELECT FbaItemLocation.sku,FbaItemLocation.available_qty_check_in,FbaShipment.status  FROM fba_shipments FbaShipment LEFT JOIN fba_item_locations FbaItemLocation  ON FbaShipment.shipment_id = FbaItemLocation.shipment_id WHERE FbaShipment.status  IN('preparing','exported')";

  		$shipments = $this->FbaShipment->query($sql);
		//pr($shipments);
		//exit;
		$exported = []; $preparing = [];
	 	if(count($shipments) > 0){
			foreach($shipments as $v){
 				if($v['FbaShipment']['status'] == 'preparing'){
					$preparing[$v['FbaItemLocation']['sku']][] = $v['FbaItemLocation']['available_qty_check_in'];//inpreation
				}
 				if($v['FbaShipment']['status'] == 'exported'){
					$exported[$v['FbaItemLocation']['sku']][] = $v['FbaItemLocation']['available_qty_check_in'];//intransit
				}
			}
		} 
		$fbaPreparingStock = [];
		foreach(array_keys($preparing) as $sku){
			$fbaPreparingStock[$sku] = array_sum($preparing[$sku]);
		}
		$fbaExportedStock = [];
		foreach(array_keys($exported) as $sku){
			$fbaExportedStock[$sku] = array_sum($exported[$sku]);
		}
		$fbaVstock = [];
		$fba_inv = $this->FbaStockEod->find('first', array('order'=>'stock_date desc'));  
 		if(count($fba_inv)){
			$stock = json_decode($fba_inv['FbaStockEod']['costbreaker'],1);
 			foreach($stock as $sk => $v){
				if(isset($all_channel_sku[$sk])){
					$msku = $all_channel_sku[$sk];
					$fbaVstock[$msku][] = $v;	
				}		
			}
			$stock = json_decode($fba_inv['FbaStockEod']['rainbow'],1);
 			foreach($stock as $sk => $v){
				if(isset($all_channel_sku[$sk])){
					$msku = $all_channel_sku[$sk];
					$fbaVstock[$msku][] = $v;	
				}		
			}
			$stock = json_decode($fba_inv['FbaStockEod']['marec'],1);
  			foreach($stock as $sk => $v){
				if(isset($all_channel_sku[$sk])){
					$msku = $all_channel_sku[$sk];
					$fbaVstock[$msku][] = $v;	
				}		
			}
		}
		 
		$fbaVirtualStock = [];
		foreach(array_keys($fbaVstock) as $sku){
			$fbaVirtualStock[$sku] = array_sum($fbaVstock[$sku]);
		}
		 
   		$this->Po->query("TRUNCATE sales_averages") ;
		
		$date = date("Y-m-d",strtotime("+1 day"));
		
		$po_ids = [];$purchase_qty = [];$quote_ids = [];$quote_qty = [];
		
 		$pos = $this->Po->find('list', array('conditions' => array('Po.status' => 1),'fields'=>array('id')));
 		
		if(count($pos) > 0){		
 			$purchase_ord = $this->PurchaseOrder->find('all', array('conditions' => array('PurchaseOrder.po_id' => $pos),'fields'=>array('purchase_sku','purchase_qty')));
			if(count($purchase_ord) > 0){  
				foreach($purchase_ord as $pod){
					$purchase_qty[$pod['PurchaseOrder']['purchase_sku']][] = $pod['PurchaseOrder']['purchase_qty'];
				}
			}		 
 		 
			if(count($purchase_qty) > 0){
				foreach(array_keys($purchase_qty) as $sku){
					$po_qty[$sku] = array_sum($purchase_qty[$sku]);
				}
			}
		}
 		
		
 		$avr_sale_w  = 5;//Sale Of Days	
 		$stock_req   = 10;//Stock Required For;
		$sales_array = [];$fba_array = [];
		//$sales_array = $this->getAllSkuSale($avr_sale_w);
		//$fba_array = $this->getAllSkuFbaSale($avr_sale_w);
		 
 		/*$products = $this->Product->find('all', array('conditions' => array( 'OR'=>['`Product.current_stock_level` <= `Product.min_stock_level`','Product.current_stock_level' => 0] ),'fields'=>array('id','Product.product_sku','Product.current_stock_level','Product.min_stock_level','ProductDesc.barcode','Product.product_name')));*/
		
		$products = $this->Product->find('all', array('conditions' => array('is_deleted'=>0,'product_sku LIKE'=>'S-%'),'fields'=>array('id','Product.product_sku','Product.current_stock_level','Product.min_stock_level','ProductDesc.barcode','Product.product_name')));
		 
		foreach($products as $val){
				
			$SalesAvg = [];
			$product_sku = $val['Product']['product_sku'];
			$SalesAvg['virtual_stock'] = 0 ;
			if($val['Product']['current_stock_level'] > 0){
				$SalesAvg['virtual_stock'] = $val['Product']['current_stock_level'];
			}
			$SalesAvg['safety_stock']  = 1;
			if(!empty($val['Product']['min_stock_level'])){
				$SalesAvg['safety_stock']  = $val['Product']['min_stock_level'];
			}
			$SalesAvg['barcode'] 	   = $val['ProductDesc']['barcode'];
			$SalesAvg['product_name']  = $val['Product']['product_name'];
 			$SalesAvg['master_sku']    = $product_sku;
			$SalesAvg['warehouse']     = 'jersey';
 			
			if(isset($fbaVirtualStock[$product_sku])){
				$SalesAvg['fba_virtual_stock'] = $fbaVirtualStock[$product_sku];
			}else{
				$SalesAvg['fba_virtual_stock'] = 0;
			}
			
			if(isset($fbaPreparingStock[$product_sku])){
				$SalesAvg['fba_stock_in_preparation'] = $fbaPreparingStock[$product_sku];
			}else{
				$SalesAvg['fba_stock_in_preparation'] = 0;
			}
			
			if(isset($fbaExportedStock[$product_sku])){
				$SalesAvg['fba_stock_in_tansit'] = $fbaExportedStock[$product_sku];
			}else{
				$SalesAvg['fba_stock_in_tansit'] = 0;
			}
  			 
			$stock_req   = 10; //days
 			
			$total_days 		= $avr_sale_w;
			if(isset($sales_array[$product_sku])){
			 $total_days_av_sale    = $sales_array[$product_sku];
			}else{
			  $total_days_av_sale    = 1; 
			}
			
			if(isset($fba_array[$product_sku])){
			  $fba_days_av_sale  = $fba_array[$product_sku];
			}else{
			  $fba_days_av_sale  = 0; 
			}
			
			//$total_days_av_sale = ceil($total_days_sale/$total_days);	
		//	$weekly_av_sale 	= ceil($total_weekly_sale/$weekly_sale);			
			$stock_required   	= $total_days_av_sale * $stock_req;
			$fba_stock_required = $fba_days_av_sale * $stock_req;
			
			$total_stock_req  	= $stock_required + $fba_stock_required + $SalesAvg['safety_stock'];
			////
			//if($total_stock_req >= $val['Product']['min_stock_level']){
				$SalesAvg['required_qty'] = $total_stock_req ;
				
				if(isset($po_qty[$product_sku])){		
					$qty = $po_qty[$product_sku];
					$SalesAvg['stock_in_transit'] = $qty;
				}else{
					$SalesAvg['stock_in_transit'] = 0;
				}
				
				$chk_sa = $this->SalesAverage->find('first', array('conditions' => array('SalesAverage.master_sku' => $product_sku),'fields'=>array('id')));
				if(count($chk_sa) > 0){ 
					$SalesAvg['id'] = $chk_sa['SalesAverage']['id'];
				}
				
				try {
				 $this->SalesAverage->saveAll($SalesAvg) ;
				 $msg = 'Virtual & InTransit Stock Refreshed.';
				}						
				catch(Exception $e) {
				   $msg .= $e->getMessage();
				}
			//}
				 
 		}
 		 
		$ret['msg'] = $msg;
		echo json_encode($ret);
		exit;
	}
	
	public function lifeOfItem(){	
 		$this->layout = 'ajax';
		$this->autoRender = false;
 		$this->loadModel('Product');
		$this->loadModel('CheckIn'); 
 		 
		$skus = []; 
 		 
		$products = $this->Product->find('all', array('fields'=>array('id','product_sku','current_stock_level','life_of_item','min_stock_level','barcode','product_name','nature_of_sku') ));
  		if(count($products) > 0){		
			foreach($products as $val){
				$product_sku =  $val['Product']['product_sku'];
				$checkins = $this->CheckIn->find('all', array('conditions' => array('sku' => $product_sku),'fields'=>array('id','sku','expiry_date','date')));
				if(count($checkins) > 0){
					foreach($checkins as $v){
						$time_diff = strtotime($v['CheckIn']['expiry_date']) - strtotime($v['CheckIn']['date']);
					/*	echo	$v['CheckIn']['expiry_date'];
						echo  ' = '.$v['CheckIn']['date'];
						echo  ' = ';*/
						 $days = floor($time_diff/86400);  
						/*echo  ' = '.$product_sku;
						echo '<br>';*/
						$skus[$product_sku][] = $days ;
					}
				
				}
   			}
 		}
		
		foreach($skus as $sk => $v){
			//echo $sk .' === ' ;
			 $life_of_item = max($skus[$sk]);
			//echo '<br>';
			$this->Product->updateAll(array('life_of_item' => $life_of_item), array('product_sku' => $sk));
				 
		}
 		exit;
	}
	
 	
	public function roundUpToAny($n,$x=5) {
		return (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x;
	}
	
	public function paginate_function($item_per_page, $current_page, $total_records, $total_pages,$data_name=false,$data_value=false,$sort_by=false)
	{
		$pagination = '';
		if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
			$pagination .= '<ul class="pagination">';
			
			$right_links    = $current_page + 3; 
			$previous       = $current_page - 3; //previous link 
			$next           = $current_page + 1; //next link
			$first_link     = true; //boolean var to decide our first link
			
			if($current_page > 1){
				$previous_link = ($previous > 0)? $previous : 1;
				$pagination .= '<li class="first"><a href="#" data-page="1" data-name="'.$data_name.'" order-by="'.$sort_by.'" data-value="'.$data_value.'" title="First">&laquo;</a></li>'; //first link
				$pagination .= '<li><a href="#" data-page="'.$previous_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Previous">&lt;</a></li>'; //previous link
					for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
						if($i > 0){
							$pagination .= '<li><a href="#" data-page="'.$i.'"  data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Page'.$i.'">'.$i.'</a></li>';
						}
					}     
				$first_link = false; //set first link to false
			} 
			
			if($first_link){ //if current active page is first link
				$pagination .= '<li class="first active"><span>'.$current_page.'</span></li>';
			}elseif($current_page == $total_pages){ //if it's the last active link
				$pagination .= '<li class="last active"><span>'.$current_page.'</span></li>';
			}else{ //regular current link
				$pagination .= '<li class="active"><span>'.$current_page.'</span></li>';
			}
					
			for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
				if($i<=$total_pages){
					$pagination .= '<li><a href="#" data-page="'.$i.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Page '.$i.'">'.$i.'</a></li>';
				}
			}
			if($current_page < $total_pages){ 
					$next_link = ($i > $total_pages) ? $total_pages : $i;
					$pagination .= '<li><a href="#" data-page="'.$next_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'" data-value="'.$data_value.'" title="Next">&gt;</a></li>'; //next link
					$pagination .= '<li class="last"><a href="#" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
			}
			
			$pagination .= '</ul>'; 
		}
		return $pagination; //return pagination links
	}
	
	public function sendEmail($data = [])
	{ 			 
		$mailSubject = $data['subject'];
		$mailBoday = $data['boday'];
		$to ='avadhesh.kumar@jijgroup.com';
		 $bcc = 'pappu.k@euracogroup.co.uk';
		//$to = $data['email'];
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('smtp');
		$email->emailFormat('html');
		$email->from('webmaster@firstchoice.je');
		$email->to($to);	
		$email->bcc($bcc);				
		$email->subject($mailSubject);
		if($email->send( nl2br($mailBoday) ))
		{
			$_file = WWW_ROOT .'logs/Stock.'.$mailSubject.date('dmY').".log";
			$content =  $to."\t".$mailSubject."\t".$mailBoday."\t".date('Y-m-d H:i:s'); 
			file_put_contents($_file, $content."\n", FILE_APPEND|LOCK_EX);	
		}	
			
	}
 
	 
	
}
	
	

?>


 
