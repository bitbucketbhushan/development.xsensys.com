<?php
//error_reporting(0);
class FbaCreateShipmentPlansController extends AppController
{
    
    var $name = "FbaCreateShipmentPlans";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('printTest'));					
    }
 	
   public function index() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Shipments Address" );	 
	} 
	 
	public function ShipmentPlanAjax() 
    {	
		 
		$this->loadModel('FbaInventory');
		$this->loadModel('FbaOrder');
		$this->loadModel('FbaOrderItem');
		$this->loadModel('PurchaseOrder');
		//$this->FbaOrderItem->unbindModel( array( 'belongsTo' => array( 'FbaOrder' ) ) );	
		 $sale_days      =  $this->request->data['sale_days'];
		 $stock_required =  $this->request->data['stock_required'];
		   
		 $date_range = array(date('Y-m-d H:i:s', strtotime("-{$sale_days} days")),date('Y-m-d H:i:s'));
		 
		 $orders = $this->FbaOrder->find('all', array('conditions'=>array('FbaOrder.order_status !=' =>'Canceled', 'FbaOrder.purchase_date BETWEEN ? AND ? '=> $date_range)));
		 
		 foreach($orders as $val){
			  $pdate = date("Ymd", strtotime($val['FbaOrder']['purchase_date']));
			  foreach($val['FbaOrderItem'] as $items){
				if($items['quantity_shipped'] > 0){
					$data[$items['seller_sku']][$pdate][$items['asin']][] =  $items['quantity_shipped'];
				}
			  }
		 } 
		
		 $average_sale = array();
		 foreach($data as $sku => $val){
		    $total_sku_sale = 0;
			foreach($val as $date => $cat){	
				foreach($cat as $asin => $qty){		 
					$total_sku_sale += 	array_sum($qty);   
					$count[$sku][$date]  =  $total_sku_sale;
				}
			}
			$average = ceil($total_sku_sale / count($count[$sku]));
			$average_sale[$sku] =  array('stock_required'=>ceil($average * $stock_required),'asin' =>$asin,'sale'=>$total_sku_sale,'average'=>$average);
		 }
		 
		 // pr(  $average_sale);
		 $finalData = array(); $m_sku = array();$seller_skus= array();
		 foreach( $average_sale as $seller_sku => $val){
		 	$seller_skus[] = $seller_sku;
			$map = $this->getSkuMapping($seller_sku);
			$sku = $map['single_sku'];
			if($map['bundle_sku'] != '-'){				 
				$bundle_qty = $map['bundle_qty'];
				for($i=0; $i<$bundle_qty; $i++){
					$m_sku[$map['single_sku']][] = $map['single_sku'];
				}
			}else{
				$m_sku[$map['single_sku']][] = $map['single_sku'];
			}
			
			$supply_details = $this->getInventorySupplyDetails($seller_sku);			
			
			$stock_required = ($val['stock_required'] - ($supply_details['in_stock'] + $supply_details['in_bound']));
			
			$finalData[] = array('single_sku' =>$map['single_sku'],'bundle_sku'=>$map['bundle_sku'],'bundle_qty'=>$map['bundle_qty'],'stock_required'=>$stock_required,'seller_sku'=>$seller_sku,'asin'=>$val['asin'],'sale'=>$val['sale'], 'average'=> $val['average'],'in_stock'=>$supply_details['in_stock'],'in_bound'=>$supply_details['in_bound']); 
			
		 } 
		 
		 $finalData = $this->sksort($finalData, 'stock_required');
		 $html = '';
		 
		 foreach( $finalData as  $val){
		 
		 	 $orders = $this->FbaOrderItem->find('all', array('conditions'=>array('FbaOrderItem.item_price >' => 0, 'FbaOrderItem.seller_sku IN'=> $seller_skus),'ORDER' =>'FbaOrderItem.timestamp DESC'));
			 
			foreach($orders as $oritem){
				$order_price[$oritem['FbaOrderItem']['seller_sku']]= ($oritem['FbaOrderItem']['item_price'] + $oritem['FbaOrderItem']['shipping_price'] + $oritem['FbaOrderItem']['item_tax'] +$oritem['FbaOrderItem']['shipping_tax']  + $oritem['FbaOrderItem']['shipping_discount']); 
			}
				 
			$master_sku_count = count($m_sku[$val['single_sku']]); 
			 
			$xsensys = $this->getXsensysStock($val['single_sku']);
			$cls = "instock";$xscls = '';
			
			if($val['stock_required'] > -1){$cls = "outstock ";}
			
			
		    $xsensys_stock = floor($xsensys['stock']/$master_sku_count);
			
			$bundle_qty = 1;
			$master_sku = $val['single_sku'];
			if($val['bundle_sku'] != '-'){				 
				$master_sku = $val['bundle_sku'];
				$bundle_qty = $val['bundle_qty'];
				if($master_sku_count > 2){
					$xsensys_stock  = floor($xsensys_stock / $val['bundle_qty']);
				}
			}
		 
		    if($xsensys_stock < $val['stock_required']){ $xscls = "outstock ";}
			
			$po_price = 0;
			$po_prices = $this->PurchaseOrder->find('first',array('conditions'=>array('purchase_sku' => $val['single_sku']),'fields' =>'price','order'=>'id DESC'));
			if(count($po_prices) > 0){$po_price = $po_prices['PurchaseOrder']['price'] * $bundle_qty;}
		 
			$html .='<div class="row sku">';
			$html .='<div class="col-lg-12">';	
				$html .='<div style="float:left; width:11%"><small>'. $val['seller_sku'].'</small></div>';	
				$html .='<div style="float:left; width:16%"><small>'. $master_sku.'</small></div>';	
				$html .='<div style="float:left; width:11%"><small>'. $val['asin'] .'</small></div> ';					
				$html .='<div style="float:left; width:5%"><small>'. $order_price[$val['seller_sku']] .'</small></div> ';
				$html .='<div style="float:left; width:6%"><small>'. $po_price .'</small></div> ';
				
				$html .='<div style="float:left; width:9%"><small><center>'.$val['sale'].'</center></small></div>';
				$html .='<div style="float:left; width:10%"><small><center>'.$val['average'].'</center></small></div>';
				$html .='<div style="float:left; width:6%"><small><center>'.$val['in_stock'].'</center></small></div>';				
				$html .='<div style="float:left; width:7%"><small><center>'.$val['in_bound'].'</center></small></div>';
				$html .='<div style="float:left; width:7%;margin-right:1px;" class="'.$xscls.'"><small><center>'.$xsensys_stock.'</center></small></div>';
				$html .='<div style="float:left; width:10%;" class="'.$cls.'"><center><small><input type="number" id="'.$val['seller_sku'].'" value="'.$val['stock_required'].'" style="width:90%;"/></small></center></div>';
				//$html .='<div style="float:left; width:35%"><small>'.$val['stock_required'].'</small></div>';					
				//$html .='<div style="float:right; width:62%"><input type="number" id="'.$val['seller_sku'].'" value="'.$val['stock_required'].'" style="width:90%"/></div>';					
				//$html .='</div>';	
					 
			$html .='</div>';
			
			/*------Start Bundle----*/	 
			
			$bundle_mapping = $this->getBundleSku();
			
			 
			if(isset($bundle_mapping[$master_sku])){
			
			
				$html .='<div style="float:left; width:11%">&nbsp;&nbsp;&nbsp;&nbsp;</div>';			 
				$html .='<div style="float:left; width:87%"><div style=" margin-bottom:5px; background-color:#BAEEF9; font-size:11px; padding-left:0px !important;padding-right:0px !important;">';
				
				
				foreach($bundle_mapping[$master_sku] as $bm){
					$html .= '<div class="col-sm-12" style="background-color:#BAEEF9; margin-left:0px !important;margin-right:0px !important; font-size:11px;border-top:1px dashed #bbb;">'; 
					$html .= '<div style="float:left; width:18%; font-size:11px;">'.$bm['master_sku'].'</div>'; 												
					$html .= '<div style="float:left; width:64%; font-size:11px; text-align:left; ">'.$bm['title'].'</div>';
					$html .= '<div style="float:left; width:06%; font-size:11px; text-align:center;">'.$bm['qty'].'</div>';
					$html .= '<div style="float:left; width:11%; font-size:11px; text-align:center;">'.$bm['qty'].'x'.$val['stock_required'].'='. ($bm['qty']*$val['stock_required']) .'</div>';
					 
					$html .=  '</div>';
				} 
				$html .='</div> '; 
				$html .='</div>';
			
			}
			 
			/*------End Bundle----*/	 
											 
																						
			$html .='</div>';
			
			$msg = 'ok';					
		 } 
  		echo json_encode(array('htmldata'=>$html,'msg'=>$msg));
 		exit;
		
	}
	
	
	public function getShipmentAllAddress() 
    {
		$this->loadModel('FbaApiShipmentAddress');  
		return $this->FbaApiShipmentAddress->find('all');
	} 
	
	public function getShipmentAddress( $id ) 
    {
		$this->loadModel('FbaApiShipmentAddress');  
		return $this->FbaApiShipmentAddress->find('first', array('conditions'=>array('FbaApiShipmentAddress.id'=>$id)));
	} 
	
	public function getInventorySupplyDetails( $seller_sku ) 
    {
		$this->loadModel('FbaInventorySupplyDetail');  
		$in_stock = 0;
		$in_bound = 0;
		$Supply = $this->FbaInventorySupplyDetail->find('all', array('conditions'=>array('FbaInventorySupplyDetail.seller_sku'=>$seller_sku)));
		foreach($Supply as $val){
			if(in_array($val['FbaInventorySupplyDetail']['supply_type'], array('Transfer','InStock'))){
				$in_stock += $val['FbaInventorySupplyDetail']['quantity'];
			}
			if($val['FbaInventorySupplyDetail']['supply_type'] == 'Inbound'){
				$in_bound += $val['FbaInventorySupplyDetail']['quantity'];
			}		
		}
		return array('in_bound'=>$in_bound,'in_stock' =>$in_stock);
	} 
	
	public function getXsensysStock( $master_sku ) 
    {
		$bundle_mapping = $this->getBundleSku();
		 
		$this->loadModel('Product');  
		 
		 $product = $this->Product->find('first', array('conditions'=>array('Product.product_sku' => $master_sku)));
		 if(count($product) > 0){
		  
				$qty = array();
				if(isset($bundle_mapping[$product['Product']['product_sku']])){
				
					foreach($bundle_mapping[$product['Product']['product_sku']] as $bun){
						$bp = $this->Product->find('first', array('conditions'=>array('Product.product_sku' => $bun['master_sku'])));			 
						$qty[] = $bp['Product']['current_stock_level'];///$bun['qty'];						
					}
					$stock =  min($qty);					 
				}
				else{										
					$stock = $product['Product']['current_stock_level'];
				}
		
			return  array('product_name'=>$product['Product']['product_name'],'stock'=> $stock );
		}else{
			return 0;
		}
	} 
	
	
	private function getSkuMapping($seller_sku = null)
	{		
		$this->loadModel( 'Skumapping' );		
		$_items = $this->Skumapping->find('first', array('conditions'=>array('channel_sku'=>$seller_sku)));
		if(count($_items) > 0){
			$bundle_sku = '-'; $qty = 1;
			$bundle_mapping = $this->getBundleSku();
			if(isset($bundle_mapping[$_items['Skumapping']['sku']])){
					foreach($bundle_mapping[$_items['Skumapping']['sku']] as $bun){
						$single_sku = $bun['master_sku'];	
						$qty = $bun['qty'];		 
					}
					$bundle_sku = $_items['Skumapping']['sku'];										 
				}
				else{										
					$single_sku = $_items['Skumapping']['sku'];
				}
				
			return array('bundle_sku' => $bundle_sku,'single_sku'=> $single_sku,'bundle_qty'=> $qty );
		}else{
			return 0;
		}
	}
	
	private function getBundleSku()
	{		
		$this->loadModel('FbaBundleSku');  
		$bundle = $this->FbaBundleSku->find("all");
		foreach($bundle as $val){
			$bundle_mapping[$val['FbaBundleSku']['bundle_sku']][] =  array('master_sku'=>$val['FbaBundleSku']['master_sku'],'qty'=>$val['FbaBundleSku']['qty'],'title'=>$val['FbaBundleSku']['title'],'bid'=>$val['FbaBundleSku']['bid']);
		}
		return $bundle_mapping;
		
	}
	private function sksort(&$array, $subkey="id", $sort_ascending=false) {

		if (count($array))
			$temp_array[key($array)] = array_shift($array);
	
		foreach($array as $key => $val){
			$offset = 0;
			$found = false;
			foreach($temp_array as $tmp_key => $tmp_val)
			{
				if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey]))
				{
					$temp_array = array_merge(    (array)array_slice($temp_array,0,$offset),
												array($key => $val),
												array_slice($temp_array,$offset)
											  );
					$found = true;
				}
				$offset++;
			}
			if(!$found) $temp_array = array_merge($temp_array, array($key => $val));
		}
	
		if ($sort_ascending) $array = array_reverse($temp_array);
	
		else $array = $temp_array;
		
	return $array;
    }  
}