<?php 
error_reporting(0);
class ReturnOrdersController extends AppController
{ 
    var $components = array('Session','Upload','Common','Auth');
    var $helpers = array('Html','Form','Common','Session');

    public function index() {

        $this->loadModel('MergeUpdate');
        $this->loadModel('OpenOrder'); 
        $this->loadModel('Refund'); 
        
        $this->layout = 'index';
        $orderRelation = array();
        $perPage = Configure::read( 'perpage' );
        $status = array('1','4');
        $childParent = array();
        $emails = array('jake.shaw@euracogroup.co.uk','lalit.prasad@jijgroup.com','shashi.b.kumar@jijgroup.com','avadhesh.kumar@jijgroup.com','anna.kedziora@onet.eu','aakash.kushwah@jijgroup.com','neha@euracogroup.co.uk','pappu.k@euracogroup.co.uk');
		
		if(in_array($this->Session->read('Auth.User.email'),$emails)){ 
		    if(isset($_REQUEST['searchkey']) && $_REQUEST['searchkey']){
                if($_REQUEST['searchtype']=='orderid') {
                   $conditions =  array('MergeUpdate.status' => $status, 'MergeUpdate.product_order_id_identify LIKE ' => trim($_REQUEST['searchkey']).'%') ;
                } else if($_REQUEST['searchtype']=='referenceid') {
                   $conditions =  array('MergeUpdate.status' => $status, 'OpenOrder.amazon_order_id LIKE ' => trim($_REQUEST['searchkey']).'%') ;
                } else {
                   $conditions =  array('MergeUpdate.status' => $status, 'OpenOrder.customer_info LIKE ' => '%'.trim($_REQUEST['searchkey']).'%') ; 
                }
			} else {
				$conditions =  array('MergeUpdate.status' => $status);
			} 
		} else { 
			if(isset($_REQUEST['searchkey']) && $_REQUEST['searchkey']){
                if($_REQUEST['searchtype']=='orderid') {
                    $conditions =  array('MergeUpdate.status' => $status, 'MergeUpdate.user_id' => $this->Session->read('Auth.User.id'), 'MergeUpdate.product_order_id_identify LIKE ' => trim($_REQUEST['searchkey']).'%') ;
                } else if($_REQUEST['searchtype']=='referenceid') {
                    $conditions =  array('MergeUpdate.status' => $status, 'MergeUpdate.user_id' => $this->Session->read('Auth.User.id'), 'OpenOrder.amazon_order_id LIKE ' => trim($_REQUEST['searchkey']).'%') ;
                } else {
                    $conditions =  array('MergeUpdate.status' => $status, 'MergeUpdate.user_id' => $this->Session->read('Auth.User.id'), 'OpenOrder.customer_info LIKE ' => '%'.trim($_REQUEST['searchkey']).'%') ; 
                }
			} else {
				$conditions =  array('MergeUpdate.status' => $status, 'MergeUpdate.user_id' => $this->Session->read('Auth.User.id'));
            }
        } 

        $this->paginate = array(
            'joins' => array(
                array(
                    'table' => 'open_orders',
                    'alias' => 'OpenOrder',
                    'type' => 'INNER',
                    'conditions' => array(
                        'MergeUpdate.order_id = OpenOrder.num_order_id'
                    )
                ),
                // array(
                //     'table' => 'refunds',
                //     'alias' =>'Refund',
                //     'type' => 'LEFT',
                //     'conditions' => array(
                //         'MergeUpdate.product_order_id_identify = Refund.order_id' 
                //     ),
                //     'order' => array('Refund.id' => 'desc')
                // ),
                array(
                    'table' => 'users',
                    'alias' =>'User',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'MergeUpdate.user_id = User.id' 
                    )
                )
            ),
            'conditions' => $conditions,
            'fields' => array( 'OpenOrder.num_order_id','OpenOrder.parent_id','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.items','OpenOrder.totals_info','OpenOrder.amazon_order_id', 'User.first_name', 'User.last_name', 'MergeUpdate.*'),
            'order' => 'MergeUpdate.process_date DESC',
            'limit' => $perPage
        );

        
        $Returnorders = $this->paginate('MergeUpdate');  
        if($Returnorders) {
            foreach($Returnorders as $retOrder) {
                if($retOrder['OpenOrder']['parent_id']!='' && $retOrder['OpenOrder']['parent_id']!='0') {
                   $orderRelation[$retOrder['OpenOrder']['parent_id']]=$retOrder['OpenOrder']['num_order_id']; 
                }
                $childParent[$retOrder['MergeUpdate']['product_order_id_identify']]=$retOrder['MergeUpdate']['order_id'];
            }
        }
        //get latest refund amount
        $Refundamount = $this->Refund->find('all',array(
           'order' => array(
               'Refund.id asc'
           )
        ));   
        $Refundoptions = Configure::read('refundoptions'); 
        $refAmountList = array();
        if(count($Refundamount)>0) {
            foreach($Refundamount as $refamount) {
               if(isset($childParent[$refamount['Refund']['order_id']]))  {
                    $refAmountList[$childParent[$refamount['Refund']['order_id']]]['refund_amount']  = $refamount['Refund']['refund_amount']; 
                    $refAmountList[$childParent[$refamount['Refund']['order_id']]]['refund_option']  = $Refundoptions[$refamount['Refund']['refund_option']];
                    $refAmountList[$childParent[$refamount['Refund']['order_id']]]['refund_reason']  = $refamount['Refund']['reason'];
                    $refAmountList[$childParent[$refamount['Refund']['order_id']]]['refund_by_user'] = $refamount['Refund']['refund_by_user'];
                    $refAmountList[$childParent[$refamount['Refund']['order_id']]]['created_at']     = $refamount['Refund']['created_at'];
               }
            }
        }
         
        $this->set( 'Refundoptions', $Refundoptions );
        $this->set( 'Refamountlist', $refAmountList );
        $this->set( 'Returnorders', $Returnorders );
        $this->set( 'Orderrelation', $orderRelation );
        
    }

    public function confirmReturn() {
        $this->loadModel('MergeUpdate');
        $this->loadModel('OpenOrder');
        $orderId = $this->request->data['orderid'];
        $returnReason = $this->request->data['returnreason'];
        $itemCondition = $this->request->data['itemcondition'];
        $comments = $this->request->data['comments'];
        $userId = $this->Session->read('Auth.User.id');
        $returnByUser=$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
        //update merge update table
        $updateMergeUpdate = $this->MergeUpdate->updateAll( 
            array( 
                'MergeUpdate.status' => 4,
                'MergeUpdate.return_reason'=>"'".$returnReason."'",
                'MergeUpdate.item_condition'=>"'".$itemCondition."'",
                'MergeUpdate.comments'=>"'".$comments."'",
                'MergeUpdate.return_by'=>"'".$userId."'",
                'MergeUpdate.return_date'=>"'".date('Y-m-d H:i:s')."'",
                'MergeUpdate.return_by_user'=>"'".$returnByUser."'" 
            ), 
            array( 'MergeUpdate.product_order_id_identify' => $orderId)
        );

        //update open order table
        if($updateMergeUpdate) { 
            //    $this->OpenOrder->updateAll(
            //        array(
            //         'OpenOrder.status'=>4
            //        ),
            //        array('OpenOrder.num_order_id'=>$orderId)
            //     );
        }
        if($updateMergeUpdate) {
            $this->Session->setFlash('Order status updated successfully!', 'flash_success');
            echo json_encode(array('status'=>'1'));
            exit;
        } else {
            $this->Session->setFlash('Unable to change order status!', 'flash_error');
            echo json_encode(array('status'=>'0','msg'=>'Unable to change order status!'));
            exit; 
        } 
    }

    public function confirmRefund() {
        $this->loadModel('Refund');
        $this->loadModel('MergeUpdate');
        $this->loadModel('CurrencyExchange');

        $dataRefund = array();
        $orderId = $this->request->data['orderid'];
        $productPrice = $this->request->data['productprice'];
        $refundOptions = $this->request->data['refundoptions'];
        $shippingCost = $this->request->data['shippingcost'];
        $totalPrice = $this->request->data['totalprice'];
        $refundReason = $this->request->data['customerefundreason'];
        $userId = $this->Session->read('Auth.User.id');
        $customrefundamount = $this->request->data['customrefundamount'];
        $refundByUser = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
        
        $dataRefund['Refund']['order_id'] = $orderId;

        if($refundOptions == 'productcost' || $refundOptions == 'shippingcost' || $refundOptions == 'discount') {
           $dataRefund['Refund']['refund_amount'] = $customrefundamount; 
        } else if($refundOptions == 'additionalamount') {
           $dataRefund['Refund']['refund_amount'] = $customrefundamount + $totalPrice; 
        } else {
           $dataRefund['Refund']['refund_amount'] = $totalPrice;
        } 

        // $dataRefund['Refund']['custom_refund'] = $customrefundamount; 
        $dataRefund['Refund']['order_id'] = $orderId;
        $dataRefund['Refund']['refund_by'] = $userId;
        $dataRefund['Refund']['refund_by_user'] = $refundByUser;
        $dataRefund['Refund']['refund_option'] = $refundOptions;
        $dataRefund['Refund']['reason'] = $refundReason;
        
        //currency conversion if currency not in GBP
        $currency	    =	$this->CurrencyExchange->find('all', array('order' => 'CurrencyExchange.date DESC'));
        $exchangeRate	=	$currency[0]['CurrencyExchange']['rate'];
        $dataRefund['Refund']['refund_amount_converted'] = $dataRefund['Refund']['refund_amount'];
        if($currency!='GBP') {
           $dataRefund['Refund']['refund_amount_converted'] = $exchangeRate * $dataRefund['Refund']['refund_amount']; 
        }

        $submitFlag = true;
        if(($refundOptions == 'productcost' || $refundOptions == 'discount') && $dataRefund['Refund']['refund_amount']>$totalPrice) {
            $submitFlag = false;
        } 

        //check is custom_refund is less than equal to total price when submit additional cost option
        if($submitFlag == true) {
            //update open order table         
            $dataRefund['Refund']['original_price'] = $totalPrice;
            $refundOrder = $this->Refund->save($dataRefund);
            
            if($refundOrder) {
                //update refund flag in open_order
                $this->MergeUpdate->updateAll(
                    array( 
                     'MergeUpdate.is_refund'=>1, 
                    ),
                    array('MergeUpdate.product_order_id_identify'=>$orderId)
                );
                $this->Session->setFlash('Refunded successfully!', 'flash_success');
                echo json_encode(array('status'=>'1'));
                exit;
            } else {
                $this->Session->setFlash('Unable to refund!', 'flash_success');
                echo json_encode(array('status'=>'0','msg'=>'Unable to refund!'));
                exit; 
            } 
        } else {
            $this->Session->setFlash('Refund amount cannot be greater than total price!', 'flash_error');
            echo json_encode(array('status'=>'0','msg'=>'Refund amount cannot be greater than total price!'));
            exit; 
        }
        
    }

    public function editReturn() {
        $this->loadModel('MergeUpdate'); 
        $orderId = $this->request->data['orderid'];
        $returnReason = $this->request->data['returnreason'];
        $itemCondition = $this->request->data['itemcondition'];
        $comments = $this->request->data['comments'];
 
        $updateReason = $this->MergeUpdate->updateAll(
            array( 
                'MergeUpdate.return_reason'=>"'".$returnReason."'",
                'MergeUpdate.item_condition'=>"'".$itemCondition."'",
                'MergeUpdate.comments'=>"'".$comments."'"
            ),
            array('MergeUpdate.product_order_id_identify'=>$orderId)
        );
        
        if($updateReason) {
            $this->Session->setFlash('Return reason updated successfully!', 'flash_success');
            echo json_encode(array('status'=>'1'));
            exit;
        } else {
            $this->Session->setFlash('Unable to update return reason!', 'flash_success');
            echo json_encode(array('status'=>'0','msg'=>'Unable to update return reason'));
            exit; 
        } 
    }

    public function getDetails() {
        $this->loadModel('Store');
        $this->loadModel('OpenOrder');
        $this->loadModel('MergeUpdate');
        $this->loadModel('TempOpenOrder');
        $this->loadModel('TempMergeUpdate');
        $orderId = $this->request->data['orderid'];
        $mainId = explode('-',$orderId);
        $storeData = ''; 
        $customerInfo = '';
        $address1 = '';
        $address2 = '';
        $address3 = '';
        $town = '';
        $region = '';
        $postcode ='';
        $country ='';
        $fullName='';
        $company='';
        $phoneNumber='';
        $resendReason = '';
        $resendComments = ''; 
        $skuPriceList = array();
        $showConfirmFullOrderButton = 1;
        //get store list
        $Description=$this->Store->find('all',
                            array('conditions' => 
                            array('Store.status' => 1)
                            )
                        ); 

        if($Description) {
            $storeData .= '<option value="">Select an order location</option>';
            foreach($Description as $store) {
                $storeData .= '<option value="'.$store['Store']['id'].'">'.$store['Store']['store_name'].'</option>';               
            }
        }

        $checkOrdersChild = $this->OpenOrder->find('first',array(
            'conditions' => array(
                'OpenOrder.parent_id' => $mainId[0],
                'OpenOrder.status' => 0
            ),
            'fields' => array('OpenOrder.id'),
        ));

        //hide confirm full order button if any order resend already
        if(count($checkOrdersChild) > 0) {
            $showConfirmFullOrderButton = 0;
        }

        //check records in temp table
        $mergeTable = 'TempMergeUpdate';
        $orderDetails = $this->TempMergeUpdate->find('all', array( 
            'conditions' => array(
                'TempMergeUpdate.parent_id' => $orderId
            ),
            'fields' => array('TempMergeUpdate.sku'),
        ));

        //get price and addresss if record saved in temp table
        $tempOpenOrderDetails = $this->TempOpenOrder->find('first',array(
            'conditions' => array(
                'TempOpenOrder.parent_id' => $mainId[0]
            ),
            'fields' => array('TempOpenOrder.items','TempOpenOrder.customer_info'),
        ));
         
        if(count($tempOpenOrderDetails)>0) {
            $priceDetails = unserialize($tempOpenOrderDetails['TempOpenOrder']['items']);
            if($priceDetails) {
                foreach($priceDetails as $pd) { 
                    $skuPriceList[$pd->SKU] = $pd->PricePerUnit;
                }
            }

            //get customer info and resend details if saved in temp table
            $customerInfo = unserialize($tempOpenOrderDetails['TempOpenOrder']['customer_info']);
            $address1 = $customerInfo->Address->Address1;
            $address2 = $customerInfo->Address->Address2;
            $address3 = $customerInfo->Address->Address3;
            $town = $customerInfo->Address->Town;
            $region = $customerInfo->Address->Region;
            $postcode =$customerInfo->Address->PostCode;
            $country =$customerInfo->Address->Country;
            $fullName=$customerInfo->Address->FullName;
            $company=$customerInfo->Address->Company;
            $phoneNumber=$customerInfo->Address->PhoneNumber;
            // $resendReason = $orderDetails['TempMergeUpdate']['resend_reason'];
            // $resendComments = $orderDetails['TempMergeUpdate']['resend_comments'];
            
        } 
        //get customer info from open order main table if not found in temp table
        if(!$customerInfo){
            //get main order id
            $mainOrder = $this->MergeUpdate->find('first', array(
                            'conditions' => array(
                                'MergeUpdate.product_order_id_identify' => $orderId
                            ),
                            'fields' => array('MergeUpdate.order_id'),
                        ));

            $openOrderDetails = $this->OpenOrder->find('first',array(
                'conditions' => array(
                    'OpenOrder.num_order_id' => $mainOrder['MergeUpdate']['order_id']
                ),
                'fields' => array('OpenOrder.customer_info'),
            ));
 
            $customerInfo = unserialize($openOrderDetails['OpenOrder']['customer_info']);
            $address1 = $customerInfo->Address->Address1;
            $address2 = $customerInfo->Address->Address2;
            $address3 = $customerInfo->Address->Address3;
            $town = $customerInfo->Address->Town;
            $region = $customerInfo->Address->Region;
            $postcode =$customerInfo->Address->PostCode;
            $country =$customerInfo->Address->Country;
            $fullName=$customerInfo->Address->FullName;
            $company=$customerInfo->Address->Company;
            $phoneNumber=$customerInfo->Address->PhoneNumber;
            // $resendReason = $mainOrder['MergeUpdate']['resend_reason'];
            // $resendComments = $mainOrder['MergeUpdate']['resend_comments'];
        }


        //else get order details from original tables
        if(count($orderDetails)==0) {
            $mergeTable = 'MergeUpdate';
            $orderDetails = $this->MergeUpdate->find('all', array( 
                'conditions' => array(
                    'MergeUpdate.product_order_id_identify' => $orderId
                ),
                'fields' => array('MergeUpdate.sku'),
            ));
        } 

        if($orderDetails) {
        
            $products = '<table class="table">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Sku</th>
                    <th>Quantity</th>
                    <th>Price Per Unit (including shipping)</th>
                    <th>Cost (Price Per Unit * Quantity)</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody id="skulistbody">'; 
          foreach($orderDetails as $order) {
            //extracting quantity from sku
            $skus = explode(',',$order[$mergeTable]['sku']);
            if($skus) {
                foreach($skus as $key=>$value) {
                    $sku = explode('S-',$value);
                    $quantity = explode('X',$sku[0]);
                    $pricePerUnit = '';
                    if(isset($skuPriceList['S-'.$sku[1]])) {
                        $pricePerUnit = $skuPriceList['S-'.$sku[1]];
                    }
                    $products .='<tr class="productlistcontainer">
                                    <td><input checked class="checkrow" type="checkbox" data-row="'.$key.'" name="checkSave[]"/></td>
                                    <td><input class="requiredfield normalbox form-control skurow-'.$key.'" data-row="'.$key.'" type="text" value="S-'.$sku[1].'" name="sku[]"/></td>
                                    <td><input onkeypress="return isNumber(event)" class="requiredfield normalbox qtybox form-control skurow-'.$key.' qtyrow-'.$key.'" data-row="'.$key.'" type="text" value="'.$quantity[0].'" name="quantity[]"/></td>
                                    <td><input onkeypress="return isNumber(event)" data-row="'.$key.'" class="requiredfield pricebox normalbox form-control skurow-'.$key.' pricerow-'.$key.'" type="text" value="'.$pricePerUnit.'" name="price[]"/></td>
                                    <td id="costskurow-'.$key.'">'.$quantity[0]*$pricePerUnit.'</td>
                                    <td>&nbsp;</td>
                                </tr>';
                }
            }
          }
          $products.='</tbody></table>';
        }

        echo json_encode(array(
                'storelocation'=>$storeData,
                'skuslist'=>$products,
                // 'resendreason'=>$resendReason,
                // 'resendcomments'=>$resendComments,
                'address1'=>$address1,
                'address2'=>$address2,
                'address3'=>$address3,
                'town'=>$town,
                'region'=>$region,
                'postcode'=>$postcode,
                'country'=>$country,
                'fullname'=>$fullName,
                'company'=>$company,
                'phonenumber'=>$phoneNumber,
                'showConfirmFullOrderButton'=>$showConfirmFullOrderButton
            )); 
        exit;                
    }

    public function validateSku($sku=array()) {
        $this->loadModel('Product');
        $skuList = array();
        $productDetails = $this->Product->find('all', array( 
            'conditions' => array(
                'Product.product_sku' => $sku
            ),
            'fields' => array('Product.product_sku'),
        ));
         
        if($productDetails) {
            foreach($productDetails as $prod) {
               $skuList[] = $prod['Product']['product_sku'];
            }
        } 
        if(is_array($sku) && is_array($skuList))
           $skuList = array_diff($sku, $skuList);

        if($skuList) {
            return $skuList; //error true
        } else {
            return false;
        }
    }

    public function saveTemp() {
        $this->loadModel('OpenOrder');
        $this->loadModel('MergeUpdate');
        $this->loadModel('TempOpenOrder');
        $this->loadModel('TempMergeUpdate'); 
        $this->loadModel('ProductDesc');

        $sku = isset($this->request->data['sku'])?$this->request->data['sku']:'';
        $quantity = isset($this->request->data['quantity'])?$this->request->data['quantity']:'';
        $price = isset($this->request->data['price'])?$this->request->data['price']:'';
        $orderId = $this->request->data['resend_order_id'];
        $reason = $this->request->data['resend_reason'];
        $comments = $this->request->data['resend_comments'];
        $address1 = $this->request->data['address1'];
        $address2 = $this->request->data['address2'];
        $address3 = $this->request->data['address3'];
        $town = $this->request->data['town'];
        $region = $this->request->data['region'];
        $postcode = $this->request->data['postcode'];
        $country = $this->request->data['country'];
        $fullName=  $this->request->data['fullname'];
        $company= $this->request->data['company'];
        $phoneNumber = $this->request->data['phonenumber'];
        $parentId = $this->request->data['mainorderId'];
        $newItems = array();
        $dataMergeUpdate = array();
        $dataOpenOrder = array();
        $tempMergeUpdateSave = '';
        $tempOpenOrderSave = '';
        $splitData = explode('-',$orderId);
        $newOrderId = $parentId.date('mdY');
        $newOrderIdSplitId = $newOrderId.'-'.$splitData[1];
        $error = false;
        $skuProductList = array();
        $productBarcodeList = array();

        $weight = array();
        $length = array();
        $width = array();
        $height = array();
        $envelopeDetails = array();

        //validate skus if exists
        if(is_array($sku))
           $sku = array_filter($sku);
    
        $error = $this->validateSku($sku);
        $textError='';
        if($error==false) {
            
            $skuCountError = array();
            //validate duplicate skus passed
            $skuCount='';
            if(is_array($sku))
               $skuCount = array_count_values($sku);

            if($skuCount) {
                foreach($skuCount as $sKey=>$sVal) {
                    if($sVal>1) {
                        $textError .= 'Sku '.$sKey.' cannot be repeated.<br/>';
                    }
                }
            } 
            if($textError=='') { 
                //check if new order id already processed
                $checkProcessStatus = $this->OpenOrder->find('first', array( 
                    'conditions' => array(
                        'OpenOrder.num_order_id' => $newOrderId,
                        'OpenOrder.status' => 1
                    ),
                    'fields' => array('OpenOrder.id'),
                )); 
                if(count($checkProcessStatus) > 0) {
                   $newOrderId = $parentId.$splitData[1].date('mdY');
                   $newOrderIdSplitId = $newOrderId.'-'.$splitData[1];
                }

                //get open order to clone
                $orderDetails = $this->OpenOrder->find('first', array( 
                    'conditions' => array(
                        'OpenOrder.num_order_id' => $orderId
                    ),
                    'fields' => array('OpenOrder.*'),
                ));

                //save open order 
                if($sku && $quantity) {

                    $dataOpenOrder['TempOpenOrder']['sub_source'] = $orderDetails['OpenOrder']['sub_source'];
                    $dataOpenOrder['TempOpenOrder']['source_count'] = $orderDetails['OpenOrder']['source_count'];
                    $dataOpenOrder['TempOpenOrder']['order_parked'] = 0;
                    $dataOpenOrder['TempOpenOrder']['linn_fetch_orders'] = 1;
                    $dataOpenOrder['TempOpenOrder']['open_order_date'] = date('Y-m-d H:i:s');
                    $dataOpenOrder['TempOpenOrder']['sorted_scanned'] = 0;
                    $dataOpenOrder['TempOpenOrder']['destination'] = $country;
                    $dataOpenOrder['TempOpenOrder']['order_id'] = 'RESEND'.$orderId;
                    $dataOpenOrder['TempOpenOrder']['num_order_id'] = $newOrderId;
                    $dataOpenOrder['TempOpenOrder']['amazon_order_id'] = $orderId;
                    $dataOpenOrder['TempOpenOrder']['general_info'] = $orderDetails['OpenOrder']['general_info'];
                    $generalDetails = unserialize($orderDetails['OpenOrder']['general_info']);
                    $generalDetails->ReceivedDate = date('Y-m-d H:i:s');
                    $refnum = $parentId.date('YmdHis'); 
                    $generalDetails->ReferenceNum = $refnum;
                    $generalDetails->SecondaryReference = $refnum;
                    $generalDetails->ExternalReferenceNum = $refnum;
                    $dataOpenOrder['TempOpenOrder']['general_info'] = serialize($generalDetails);  
                    $dataOpenOrder['TempOpenOrder']['shipping_info'] = $orderDetails['OpenOrder']['shipping_info'];
                    
                    $customerDetails = unserialize($orderDetails['OpenOrder']['customer_info']); 
                    $customerDetails->Address->Address1 = $address1;
                    $customerDetails->Address->Address2 = $address2;
                    $customerDetails->Address->Address3 = $address3;
                    $customerDetails->Address->Town = $town;
                    $customerDetails->Address->Region = $region;
                    $customerDetails->Address->PostCode = $postcode;
                    $customerDetails->Address->Country = $country;
                    $customerDetails->Address->FullName=  $fullName;
                    $customerDetails->Address->Company= $company;
                    $customerDetails->Address->PhoneNumber = $phoneNumber;

                    $dataOpenOrder['TempOpenOrder']['customer_info'] = serialize($customerDetails);       
                    $dataOpenOrder['TempOpenOrder']['folder_name'] = $orderDetails['OpenOrder']['folder_name'];
                    $dataOpenOrder['TempOpenOrder']['parent_id'] = $parentId;
                    
                    $orderItems = unserialize($orderDetails['OpenOrder']['items']);  

                    //get product details from product
                    $productDetails = $this->Product->find('all', array(
                        'conditions' => array(
                            'Product.product_sku' => $sku
                        ),
                        'fields' => array('Product.product_sku','Product.product_name','ProductDesc.barcode'),
                    ));

                    if($productDetails) {
                        foreach($productDetails as $prod) {
                            $skuProductList[$prod['Product']['product_sku']] = $prod['Product']['product_name'];
                            $productBarcodeList[$prod['Product']['product_sku']] = $prod['ProductDesc']['barcode'];
                        }
                    }

                    $totalCharge = 0.00;
                    if($sku) {
                        foreach($sku as $key=>$val) {
                            if($val!='') {
                                $cost = (float)$price[$key] * (float)$quantity[$key];
                                $totalCharge = (float)$totalCharge + (float)$cost;
                                $itemObject = new stdClass;   
                                $itemObject->ItemId='';
                                $itemObject->ItemNumber='';
                                $itemObject->SKU= $val;
                                $itemObject->ItemSource = isset($orderItems[0]->ItemSource)?$orderItems[0]->ItemSource:'';
                                $itemObject->Title = $skuProductList[$val];
                                $itemObject->Quantity = $quantity[$key];
                                $itemObject->CategoryName = 'Default';
                                $itemObject->StockLevelsSpecified = true;
                                $itemObject->OnOrder = 0;
                                $itemObject->InOrderBook = 0;
                                $itemObject->Level = 0;
                                $itemObject->MinimumLevel = 0;
                                $itemObject->AvailableStock = 0;
                                $itemObject->PricePerUnit = $price[$key];
                                $itemObject->UnitCost = 0.00;
                                $itemObject->DespatchStockUnitCost = 0;
                                $itemObject->Discount = 0;
                                $itemObject->Tax = 0;
                                $itemObject->TaxRate = 0;
                                $itemObject->Cost = $cost;
                                $itemObject->CostIncTax = $cost;
                                $itemObject->CompositeSubItems = array();
                                $itemObject->IsService = false;
                                $itemObject->SalesTax = 0;
                                $itemObject->TaxCostInclusive = true;
                                $itemObject->PartShipped = false;
                                $itemObject->Weight = 0;
                                $itemObject->BarcodeNumber = $productBarcodeList[$val];
                                $itemObject->Market = 0;
                                $itemObject->ChannelSKU = '';
                                $itemObject->ChannelTitle = '';
                                $itemObject->DiscountValue = 0;
                                $itemObject->HasImage = '';
                                $itemObject->AdditionalInfo = array(); 
                                $itemObject->StockLevelIndicator = 3;
                                $itemObject->BatchNumberScanRequired = false;
                                $itemObject->SerialNumberScanRequired = false;
                                $itemObject->InventoryTrackingType = 0;
                                $itemObject->isBatchedStockItem = false;
                                $itemObject->IsWarehouseManaged = false;
                                $itemObject->HasPurchaseOrders = false;
                                $itemObject->CanPurchaseOrderFulfil = false;
                                $itemObject->IsUnlinked = false;
                                $itemObject->RowId = '';
                                $itemObject->OrderId = '';
                                $itemObject->StockItemId = ''; 

                                $newItems[] = $itemObject; 
                            }
                        }
                    }

                    $totalInfo = unserialize($orderDetails['OpenOrder']['totals_info']); 
                    $totalInfo->Subtotal = $totalCharge;
                    $totalInfo->PostageCost = 0;
                    $totalInfo->Tax = 0;
                    $totalInfo->TotalCharge = $totalCharge;
                    $totalInfo->PaymentMethod = 'Default';
                    $totalInfo->PaymentMethodId = '00000000-0000-0000-0000-000000000000';
                    $totalInfo->ProfitMargin = $totalCharge;
                    $totalInfo->TotalDiscount = 0;
                    $totalInfo->Currency = $totalInfo->Currency;
                    $totalInfo->CountryTaxRate = 0;
                    $totalInfo->ConversionRate = 0;

                    $dataOpenOrder['TempOpenOrder']['items'] = serialize($newItems);
                    $dataOpenOrder['TempOpenOrder']['service_name'] = "";
                    $dataOpenOrder['TempOpenOrder']['service_provider'] = "";
                    $dataOpenOrder['TempOpenOrder']['service_code'] = "";
                    $dataOpenOrder['TempOpenOrder']['scanning_qty'] = 0;
                    $dataOpenOrder['TempOpenOrder']['order_status'] = 0;
                    $dataOpenOrder['TempOpenOrder']['status'] = 0;
                    $dataOpenOrder['TempOpenOrder']['cancel_status'] = 0;
                    $dataOpenOrder['TempOpenOrder']['manifest'] = 0;
                    $dataOpenOrder['TempOpenOrder']['cn22'] = 0;
                    $dataOpenOrder['TempOpenOrder']['track_id'] = '0';
                    $dataOpenOrder['TempOpenOrder']['date'] = date('Y-m-d H:i:s');
                    $dataOpenOrder['TempOpenOrder']['error_code'] = '';
                    $dataOpenOrder['TempOpenOrder']['template_id'] = 0;
                    $dataOpenOrder['TempOpenOrder']['user_id'] = 0;
                    $dataOpenOrder['TempOpenOrder']['product_type'] = '';
                    $dataOpenOrder['TempOpenOrder']['product_defined_skus'] = '';
                    $dataOpenOrder['TempOpenOrder']['process_date'] = ''; 
                    $dataOpenOrder['TempOpenOrder']['order_type'] = 'Resend'; 
                    $dataOpenOrder['TempOpenOrder']['totals_info'] = serialize($totalInfo); 
                    $dataOpenOrder['TempOpenOrder']['order_split_id_identifier'] = $newOrderIdSplitId;    

                if($dataOpenOrder) {
                    //delete existing record
                    $delete = $this->TempOpenOrder->deleteAll(array('order_split_id_identifier' => $newOrderIdSplitId), false); 
                    $tempOpenOrderSave = $this->TempOpenOrder->save($dataOpenOrder);
                }

                if($tempOpenOrderSave) {
                    //get merge update data to clone
                    $orderDetails = $this->MergeUpdate->find('first', array( 
                        'conditions' => array(
                            'MergeUpdate.product_order_id_identify' => $orderId
                        ),
                        'fields' => array('MergeUpdate.*'),
                    ));

                $dataMergeUpdate['TempMergeUpdate']['order_date'] = date('Y-m-d H:i:s');
                $dataMergeUpdate['TempMergeUpdate']['source_coming'] = $orderDetails['MergeUpdate']['source_coming'];
                $dataMergeUpdate['TempMergeUpdate']['store_id'] = $orderDetails['MergeUpdate']['store_id'];
                $dataMergeUpdate['TempMergeUpdate']['custom_marked'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['custom_service_status'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['linn_fetch_orders'] = 1;
                $dataMergeUpdate['TempMergeUpdate']['scan_status'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['sorted_scanned'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['manifest_status'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['status'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['batch_num'] = NULL;
                $dataMergeUpdate['TempMergeUpdate']['created_batch'] = NULL;
                $dataMergeUpdate['TempMergeUpdate']['dispatch_console'] = NULL; 
                $dataMergeUpdate['TempMergeUpdate']['label_status'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['out_sell_marker'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['sku_decode'] = '';
                $dataMergeUpdate['TempMergeUpdate']['pick_list_status'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['reverce_picklist_status'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['custom_service_marked'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['packaging_variant_id'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['order_barcode_image'] = '';
                $dataMergeUpdate['TempMergeUpdate']['row_status'] = 1;
                $dataMergeUpdate['TempMergeUpdate']['order_id'] = $newOrderId;
                $dataMergeUpdate['TempMergeUpdate']['order_split'] = $orderDetails['MergeUpdate']['order_split'];
                $dataMergeUpdate['TempMergeUpdate']['product_order_id_identify'] = $newOrderIdSplitId;
                $dataMergeUpdate['TempMergeUpdate']['order_type'] = 'Resend';
                $dataMergeUpdate['TempMergeUpdate']['parent_id'] = $parentId;    
                
                //prepare sku insert data
                $newSkus = array();
                $product=array();
                $skuDimensions = array();

                if(count($sku)>1) {
                    $product = $this->Product->find('all',array(
                        'conditions' => array('Product.product_sku IN' => $sku),
                        'fields'=>array('Product.product_sku','ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.category_name')));
                } else {
                    $product = $this->Product->find('all',array(
                        'conditions' => array('Product.product_sku' => $sku),
                        'fields'=>array('Product.product_sku','ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.category_name')));
                } 
                
                foreach($product as $prod) {
                  $skuDimensions[$prod['Product']['product_sku']]['weight'] = $prod['ProductDesc']['weight']; 
                  $skuDimensions[$prod['Product']['product_sku']]['height'] = $prod['ProductDesc']['height']; 
                  $skuDimensions[$prod['Product']['product_sku']]['length'] = $prod['ProductDesc']['length'];
                  $skuDimensions[$prod['Product']['product_sku']]['width']  = $prod['ProductDesc']['width'];  
                }

                foreach($sku as $key=>$val) {
                  $newSkus[] = $quantity[$key].'X'.$val;
                  $weight[]  = $skuDimensions[$val]['weight']*$quantity[$key];
                  $length[]  = $skuDimensions[$val]['length'];
                  $width[]   = $skuDimensions[$val]['width'];
                  $height[]  = $skuDimensions[$val]['height']*$quantity[$key];
                }

                $packetWeight = array_sum($weight);
                $packetLength = max($length);
                $packetWidth  = max($width);
                $packetHeight = array_sum($height);

                $dataMergeUpdate['TempMergeUpdate']['sku'] = implode(',',$newSkus);
                $dataMergeUpdate['TempMergeUpdate']['product_type'] = 'single';
                $dataMergeUpdate['TempMergeUpdate']['product_sku_identifier'] = 'single';
                $dataMergeUpdate['TempMergeUpdate']['pack_order_quantity'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['price'] = $totalCharge;
                $dataMergeUpdate['TempMergeUpdate']['quantity'] = array_sum($quantity);
                $dataMergeUpdate['TempMergeUpdate']['barcode'] = implode(',',$productBarcodeList);
                $dataMergeUpdate['TempMergeUpdate']['bin_rack'] = '';
                $dataMergeUpdate['TempMergeUpdate']['update_quantity'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['service_name'] = $orderDetails['MergeUpdate']['service_name'];
                $dataMergeUpdate['TempMergeUpdate']['provider_ref_code'] = $orderDetails['MergeUpdate']['provider_ref_code'];
                $dataMergeUpdate['TempMergeUpdate']['service_provider'] = $orderDetails['MergeUpdate']['service_provider'];

                $dataMergeUpdate['TempMergeUpdate']['packet_weight'] = ($packetWeight=='')?0:$packetWeight;
                $dataMergeUpdate['TempMergeUpdate']['packet_length'] = ($packetLength=='')?0:$packetLength;
                $dataMergeUpdate['TempMergeUpdate']['packet_width']  = ($packetWidth=='')?0:$packetWidth;
                $dataMergeUpdate['TempMergeUpdate']['packet_height'] = ($packetHeight=='')?0:$packetHeight;  

                $dataMergeUpdate['TempMergeUpdate']['envelope_weight'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['envelope_cost']   = 0;
                $dataMergeUpdate['TempMergeUpdate']['envelope_name']   = 0;
                $dataMergeUpdate['TempMergeUpdate']['envelope_id']     = 0;
                $dataMergeUpdate['TempMergeUpdate']['packaging_type']  = 0;

                $dataMergeUpdate['TempMergeUpdate']['warehouse'] = $orderDetails['MergeUpdate']['warehouse'];
                $dataMergeUpdate['TempMergeUpdate']['delevery_country'] = $country;
                $dataMergeUpdate['TempMergeUpdate']['template_id'] = $orderDetails['MergeUpdate']['template_id'];
                $dataMergeUpdate['TempMergeUpdate']['packSlipId'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['lable_id'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['manifest'] = $orderDetails['MergeUpdate']['manifest'];
                $dataMergeUpdate['TempMergeUpdate']['lvcr'] = $orderDetails['MergeUpdate']['lvcr'];
                $dataMergeUpdate['TempMergeUpdate']['cn_required'] = $orderDetails['MergeUpdate']['cn_required'];
                $dataMergeUpdate['TempMergeUpdate']['brt_barcode'] = NULL;
                $dataMergeUpdate['TempMergeUpdate']['postal_service'] = $orderDetails['MergeUpdate']['postal_service'];
                $dataMergeUpdate['TempMergeUpdate']['service_id'] = $orderDetails['MergeUpdate']['service_id'];
                $dataMergeUpdate['TempMergeUpdate']['track_id'] = '0';
                $dataMergeUpdate['TempMergeUpdate']['country_code'] = $orderDetails['MergeUpdate']['country_code'];
                $dataMergeUpdate['TempMergeUpdate']['reg_post_number'] = NULL;
                $dataMergeUpdate['TempMergeUpdate']['reg_num_img'] = NULL;
                $dataMergeUpdate['TempMergeUpdate']['user_id'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['assign_user'] = '';
                $dataMergeUpdate['TempMergeUpdate']['picked_date'] = '';
                $dataMergeUpdate['TempMergeUpdate']['process_date'] = '';
                $dataMergeUpdate['TempMergeUpdate']['scan_date'] = '';
                $dataMergeUpdate['TempMergeUpdate']['manifest_date'] = '';
                $dataMergeUpdate['TempMergeUpdate']['user_name'] = '';
                $dataMergeUpdate['TempMergeUpdate']['pc_name'] = '';
                $dataMergeUpdate['TempMergeUpdate']['picklist_username'] = '';
                $dataMergeUpdate['TempMergeUpdate']['manifest_username'] = '';
                $dataMergeUpdate['TempMergeUpdate']['scanned_pc'] = '';
                $dataMergeUpdate['TempMergeUpdate']['cancelled_user'] = '';
                $dataMergeUpdate['TempMergeUpdate']['cancelled_pc'] = '';
                $dataMergeUpdate['TempMergeUpdate']['brt'] = '0';
                $dataMergeUpdate['TempMergeUpdate']['brt_phone'] = '0';
                $dataMergeUpdate['TempMergeUpdate']['brt_exprt'] = '0';
                $dataMergeUpdate['TempMergeUpdate']['royalmail'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['picklist_inc_id'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['merge_order_date'] = date('Y-m-d H:i:s');
                $dataMergeUpdate['TempMergeUpdate']['rm_error_msg'] = 0;
                $dataMergeUpdate['TempMergeUpdate']['timestamp'] = date('Y-m-d H:i:s');
                
                if($dataMergeUpdate) {
                    //delete existing record
                    $delete = $this->TempMergeUpdate->deleteAll(array('product_order_id_identify' => $newOrderIdSplitId), false);
                    $tempMergeUpdateSave = $this->TempMergeUpdate->save($dataMergeUpdate);  
                }
            }

            } else {
                echo json_encode(array('msg'=>'Please enter at least one sku and quantity!','status'=>0));
                exit;
            }
        } else {
            echo json_encode(array('msg'=>$textError,'status'=>0));  
            exit;
        }

        } else {
            echo json_encode(array('msg'=>'Skus '.implode(', ',$error).' do not exists!','status'=>0));  
            exit;
        }

        if($tempMergeUpdateSave) {
           $this->getPackagingVariant($newOrderIdSplitId);
           echo json_encode(array('msg'=>'Data saved successfully!','status'=>1,'neworderid'=>$newOrderId));
        } else { 
           echo json_encode(array('msg'=>'Unable to save data!','status'=>0)); 
        }
        
        exit;                
    } 
    
    public function confirmResend() {
        $this->loadModel('TempOpenOrder');
        $this->loadModel('TempMergeUpdate'); 
        $this->loadModel('OpenOrder');
        $this->loadModel('MergeUpdate');
      
        $sku = $this->request->data['sku'];
        $quantity = $this->request->data['quantity']; 
        $newOrderId = $this->request->data['new_order_id']; 
        $orderId=$this->request->data['resend_order_id']; 
        $reason   = $this->request->data['resend_reason'];
        $comments = $this->request->data['resend_comments'];
        $splitOrderData = explode('-',$orderId);
        $splitOrderId = $newOrderId.'-'.$splitOrderData[1];
        $saveTempOrder = ''; 
        $saveOpenOrder = '';   
        $userId = $this->Session->read('Auth.User.id');
        $resendByUser = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');

        $checkData = $this->MergeUpdate->find('first', array( 
            'conditions' => array(
                'MergeUpdate.product_order_id_identify' => $splitOrderId, 
            ),
            'fields' => array('MergeUpdate.product_order_id_identify'),
        ));
        //check if split id already created
        if(count($checkData)==0) {

            //check if order id exist already in open order table
            $orderDetails = $this->OpenOrder->find('first', array( 
                'conditions' => array(
                    'OpenOrder.num_order_id' => $newOrderId
                ),
                'fields' => array('OpenOrder.*'),
            )); 
            if(count($orderDetails)==0) {
            //insert order
            //get data from temp table
            
            $data = $this->TempOpenOrder->find('first', array( 
                'conditions' => array(
                    'TempOpenOrder.order_split_id_identifier' => $splitOrderId
                ),
                'fields' => array('TempOpenOrder.*'), 
            )); 

            if(isset($data['TempOpenOrder']['id'])) 
                unset($data['TempOpenOrder']['id']);
        
            $saveOpenOrder = $this->OpenOrder->saveMany($data, array('deep' => false));   

            if($saveOpenOrder) {
                //delete from temp table
                $this->TempOpenOrder->deleteAll(array('order_split_id_identifier' => $splitOrderId), false);  
                //save in merge update
                $data = $this->TempMergeUpdate->find('first', array( 
                    'conditions' => array(
                        'TempMergeUpdate.product_order_id_identify' => $splitOrderId,
                        'TempMergeUpdate.order_type' => 'Resend'
                    ),
                    'fields' => array('TempMergeUpdate.*'),
                )); 
                
                if(isset($data['TempMergeUpdate']['id'])) 
                unset($data['TempMergeUpdate']['id']);

                $saveTempOrder = $this->MergeUpdate->saveMany($data, array('deep' => false));  
            }

            if($saveTempOrder) {
                //delete from temp table
                $this->TempMergeUpdate->deleteAll(array('product_order_id_identify' => $splitOrderId), false);

                $this->MergeUpdate->updateAll(
                    array( 
                        'MergeUpdate.is_resend'=>1,
                        'MergeUpdate.resend_by'=>$userId,
                        'MergeUpdate.resend_date'=>"'".date('Y-m-d H:i:s')."'",
                        'MergeUpdate.resend_by_user'=> "'".$resendByUser."'",
                        'MergeUpdate.resend_reason'=>"'".$reason."'",
                        'MergeUpdate.resend_comments'=>"'".$comments."'",
                    ),
                    array('MergeUpdate.product_order_id_identify'=>$orderId)
                ); 
                $mergeData = $this->getMergeDataBySplitOrderId($splitOrderId);

                $this->saveScanOrders($mergeData,$newOrderId);
                /*---------Manage Ink SKU Qty-----*/
                $this->InkSkuInventory($mergeData);
                /*------------End----------------*/
                $this->saveorderLocations($mergeData,$newOrderId);
                
                $this->saveInventoryRecord($mergeData);
                $this->applyPostal($newOrderId);
                $this->Session->setFlash('Order created successfully!', 'flash_success');
                echo json_encode(array('msg'=>'Order created successfully!','status'=>1,'neworderid'=>$newOrderId));
            } else { 
                echo json_encode(array('msg'=>'Unable to create order!','status'=>0)); 
            } 

            }  else {

                $dataOpenOrder = array();
                //update open order
                //get temp open order details
                $tempOpenOrderDetails = $this->TempOpenOrder->find('first', array( 
                    'conditions' => array(
                        'TempOpenOrder.order_split_id_identifier' => $splitOrderId
                    ),
                    'fields' => array('TempOpenOrder.*'), 
                )); 
    
                $generalDetails = unserialize($tempOpenOrderDetails['TempOpenOrder']['general_info']);
                $generalDetails->ReceivedDate = date('Y-m-d H:i:s');
                $generalInfo  = serialize($generalDetails);    
                $customerInfo = $tempOpenOrderDetails['TempOpenOrder']['customer_info'];         
                
                $totalCharge = 0.00;

                $orderItems = unserialize($orderDetails['OpenOrder']['items']);
                $orderTempItems = unserialize($tempOpenOrderDetails['TempOpenOrder']['items']);   
            
                //prepare items object of already added item. If any.
                if($orderItems) {
                    foreach($orderItems as $itemsOpen) { 
                        $cost = (float)$itemsOpen->CostIncTax;
                        $totalCharge = (float)$totalCharge + (float)$cost;
                        $itemObject = new stdClass;   
                        $itemObject->ItemId=$itemsOpen->ItemId;
                        $itemObject->ItemNumber=$itemsOpen->ItemNumber;
                        $itemObject->SKU=$itemsOpen->SKU;
                        $itemObject->ItemSource =$itemsOpen->ItemSource;
                        $itemObject->Title =$itemsOpen->Title;
                        $itemObject->Quantity =$itemsOpen->Quantity;
                        $itemObject->CategoryName =$itemsOpen->CategoryName;
                        $itemObject->StockLevelsSpecified =$itemsOpen->StockLevelsSpecified;
                        $itemObject->OnOrder =$itemsOpen->OnOrder;
                        $itemObject->InOrderBook =$itemsOpen->InOrderBook;
                        $itemObject->Level =$itemsOpen->Level;
                        $itemObject->MinimumLevel =$itemsOpen->MinimumLevel;
                        $itemObject->AvailableStock = $itemsOpen->AvailableStock;
                        $itemObject->PricePerUnit =$itemsOpen->PricePerUnit;
                        $itemObject->UnitCost =$itemsOpen->UnitCost;
                        $itemObject->DespatchStockUnitCost =$itemsOpen->DespatchStockUnitCost;
                        $itemObject->Discount =$itemsOpen->Discount;
                        $itemObject->Tax = $itemsOpen->Tax;
                        $itemObject->TaxRate = $itemsOpen->TaxRate;
                        $itemObject->Cost = $itemsOpen->Cost;
                        $itemObject->CostIncTax = $itemsOpen->CostIncTax;
                        $itemObject->CompositeSubItems = $itemsOpen->CompositeSubItems;
                        $itemObject->IsService = $itemsOpen->IsService;
                        $itemObject->SalesTax = $itemsOpen->SalesTax;
                        $itemObject->TaxCostInclusive = $itemsOpen->TaxCostInclusive;
                        $itemObject->PartShipped = $itemsOpen->PartShipped;
                        $itemObject->Weight = $itemsOpen->Weight;
                        $itemObject->BarcodeNumber = $itemsOpen->BarcodeNumber;
                        $itemObject->Market = $itemsOpen->Market;
                        $itemObject->ChannelSKU = $itemsOpen->ChannelSKU;
                        $itemObject->ChannelTitle = addslashes($itemsOpen->ChannelTitle);
                        $itemObject->DiscountValue = $itemsOpen->DiscountValue;
                        $itemObject->HasImage = $itemsOpen->HasImage;
                        $itemObject->AdditionalInfo = $itemsOpen->AdditionalInfo; 
                        $itemObject->StockLevelIndicator = $itemsOpen->StockLevelIndicator;
                        $itemObject->BatchNumberScanRequired = $itemsOpen->BatchNumberScanRequired;
                        $itemObject->SerialNumberScanRequired = $itemsOpen->SerialNumberScanRequired;
                        $itemObject->InventoryTrackingType = $itemsOpen->InventoryTrackingType;
                        $itemObject->isBatchedStockItem = $itemsOpen->isBatchedStockItem;
                        $itemObject->IsWarehouseManaged = $itemsOpen->IsWarehouseManaged;
                        $itemObject->HasPurchaseOrders = $itemsOpen->HasPurchaseOrders;
                        $itemObject->CanPurchaseOrderFulfil = $itemsOpen->CanPurchaseOrderFulfil;
                        $itemObject->IsUnlinked = $itemsOpen->IsUnlinked;
                        $itemObject->RowId = $itemsOpen->RowId;
                        $itemObject->OrderId = $itemsOpen->OrderId;
                        $itemObject->StockItemId = $itemsOpen->StockItemId; 

                        $newItems[] = $itemObject;  
                    }
                }

                //prepare items object of already added item. If any.
                if($orderTempItems) {
                    foreach($orderTempItems as $itemsOpen) { 
                        $cost = (float)$itemsOpen->CostIncTax;
                        $totalCharge = (float)$totalCharge + (float)$cost;
                        $itemObject = new stdClass;   
                        $itemObject->ItemId=$itemsOpen->ItemId;
                        $itemObject->ItemNumber=$itemsOpen->ItemNumber;
                        $itemObject->SKU=$itemsOpen->SKU;
                        $itemObject->ItemSource =$itemsOpen->ItemSource;
                        $itemObject->Title =$itemsOpen->Title;
                        $itemObject->Quantity =$itemsOpen->Quantity;
                        $itemObject->CategoryName =$itemsOpen->CategoryName;
                        $itemObject->StockLevelsSpecified =$itemsOpen->StockLevelsSpecified;
                        $itemObject->OnOrder =$itemsOpen->OnOrder;
                        $itemObject->InOrderBook =$itemsOpen->InOrderBook;
                        $itemObject->Level =$itemsOpen->Level;
                        $itemObject->MinimumLevel =$itemsOpen->MinimumLevel;
                        $itemObject->AvailableStock = $itemsOpen->AvailableStock;
                        $itemObject->PricePerUnit =$itemsOpen->PricePerUnit;
                        $itemObject->UnitCost =$itemsOpen->UnitCost;
                        $itemObject->DespatchStockUnitCost =$itemsOpen->DespatchStockUnitCost;
                        $itemObject->Discount =$itemsOpen->Discount;
                        $itemObject->Tax = $itemsOpen->Tax;
                        $itemObject->TaxRate = $itemsOpen->TaxRate;
                        $itemObject->Cost = $itemsOpen->Cost;
                        $itemObject->CostIncTax = $itemsOpen->CostIncTax;
                        $itemObject->CompositeSubItems = $itemsOpen->CompositeSubItems;
                        $itemObject->IsService = $itemsOpen->IsService;
                        $itemObject->SalesTax = $itemsOpen->SalesTax;
                        $itemObject->TaxCostInclusive = $itemsOpen->TaxCostInclusive;
                        $itemObject->PartShipped = $itemsOpen->PartShipped;
                        $itemObject->Weight = $itemsOpen->Weight;
                        $itemObject->BarcodeNumber = $itemsOpen->BarcodeNumber;
                        $itemObject->Market = $itemsOpen->Market;
                        $itemObject->ChannelSKU = $itemsOpen->ChannelSKU;
                        $itemObject->ChannelTitle =addslashes($itemsOpen->ChannelTitle);
                        $itemObject->DiscountValue = $itemsOpen->DiscountValue;
                        $itemObject->HasImage = $itemsOpen->HasImage;
                        $itemObject->AdditionalInfo = $itemsOpen->AdditionalInfo; 
                        $itemObject->StockLevelIndicator = $itemsOpen->StockLevelIndicator;
                        $itemObject->BatchNumberScanRequired = $itemsOpen->BatchNumberScanRequired;
                        $itemObject->SerialNumberScanRequired = $itemsOpen->SerialNumberScanRequired;
                        $itemObject->InventoryTrackingType = $itemsOpen->InventoryTrackingType;
                        $itemObject->isBatchedStockItem = $itemsOpen->isBatchedStockItem;
                        $itemObject->IsWarehouseManaged = $itemsOpen->IsWarehouseManaged;
                        $itemObject->HasPurchaseOrders = $itemsOpen->HasPurchaseOrders;
                        $itemObject->CanPurchaseOrderFulfil = $itemsOpen->CanPurchaseOrderFulfil;
                        $itemObject->IsUnlinked = $itemsOpen->IsUnlinked;
                        $itemObject->RowId = $itemsOpen->RowId;
                        $itemObject->OrderId = $itemsOpen->OrderId;
                        $itemObject->StockItemId = $itemsOpen->StockItemId; 

                        $newItems[] = $itemObject;  
                    }
                }

                $totalInfo = unserialize($tempOpenOrderDetails['TempOpenOrder']['totals_info']); 
                $totalInfo->Subtotal = $totalCharge;
                $totalInfo->PostageCost = 0;
                $totalInfo->Tax = 0;
                $totalInfo->TotalCharge = $totalCharge;
                $totalInfo->PaymentMethod = 'Default';
                $totalInfo->PaymentMethodId = '00000000-0000-0000-0000-000000000000';
                $totalInfo->ProfitMargin = $totalCharge;
                $totalInfo->TotalDiscount = 0;
                $totalInfo->Currency = $totalInfo->Currency;
                $totalInfo->CountryTaxRate = 0;
                $totalInfo->ConversionRate = 0;

                //temporarily unbind association
                // $this->OpenOrder->unbindModel(
                //     array('hasOne' => array('AssignService'))
                // );

                $openOrderUpdate = $this->OpenOrder->updateAll(
                    array( 
                        'OpenOrder.items'=>"'".serialize($newItems)."'",
                        'OpenOrder.general_info'=>"'".$generalInfo."'",
                        'OpenOrder.customer_info'=>"'".$customerInfo."'", 
                        'OpenOrder.totals_info'=>"'".serialize($totalInfo)."'", 
                    ),
                    array('OpenOrder.num_order_id'=>$newOrderId),
                    array('deep' => false)
                ); 
    
                if($openOrderUpdate) {
                    //delete from temp table
                    $this->TempOpenOrder->deleteAll(array('order_split_id_identifier' => $splitOrderId), false);
                    //save in merge update
                    $data = $this->TempMergeUpdate->find('first', array( 
                        'conditions' => array(
                            'TempMergeUpdate.product_order_id_identify' => $splitOrderId,
                            'TempMergeUpdate.order_type' => 'Resend'
                        ),
                        'fields' => array('TempMergeUpdate.*'),
                    )); 
                    
                    if(isset($data['TempMergeUpdate']['id'])) 
                    unset($data['TempMergeUpdate']['id']);
                    
                    $this->MergeUpdate->deleteAll(array('product_order_id_identify' => $splitOrderId), false);
                    $saveTempOrder = $this->MergeUpdate->saveMany($data, array('deep' => false));  
                }

                if($saveTempOrder) {
                    //delete from temp table
                    $this->TempMergeUpdate->deleteAll(array('product_order_id_identify' => $splitOrderId), false);
        
                    $this->MergeUpdate->updateAll(
                        array( 
                            'MergeUpdate.is_resend'=>1,
                            'MergeUpdate.resend_by'=>$userId,
                            'MergeUpdate.resend_date'=>"'".date('Y-m-d H:i:s')."'",
                            'MergeUpdate.resend_by_user'=> "'".$resendByUser."'",
                            'MergeUpdate.resend_reason'=>"'".$reason."'",
                            'MergeUpdate.resend_comments'=>"'".$comments."'",
                        ),
                        array('MergeUpdate.product_order_id_identify'=>$orderId)
                    ); 
                    $mergeData = $this->getMergeDataBySplitOrderId($splitOrderId);
        
                    $this->saveScanOrders($mergeData,$newOrderId);
                    /*---------Manage Ink SKU Qty-----*/
                    $this->InkSkuInventory($mergeData);
                    /*------------End----------------*/
                    $this->saveorderLocations($mergeData,$newOrderId);
                    
                    $this->saveInventoryRecord($mergeData);
					$this->applyPostal($newOrderId);
                    
                    $this->Session->setFlash('Order created successfully!', 'flash_success');
                    echo json_encode(array('msg'=>'Order created successfully!','status'=>1,'neworderid'=>$newOrderId));
                } else { 
                    echo json_encode(array('msg'=>'Unable to create order!','status'=>0)); 
                } 
            }
        } else {
			/*echo '==2==';
			$r =$this->applyPostal($newOrderId);
			pr($r);*/
            echo json_encode(array('msg'=>'Order already resend for today! Please try tommorrow','status'=>-1)); 
        }
        exit;
    }

    public function confirmFullOrderResend() {
        $this->loadModel('OpenOrder');
        $this->loadModel('MergeUpdate');
        $this->loadModel('TempOpenOrder');
        $this->loadModel('TempMergeUpdate');

        $orderId  = $this->request->data['resend_order_id'];
        $reason   = $this->request->data['resend_reason'];
        $comments = $this->request->data['resend_comments'];
        $mainOrderId = $this->request->data['mainorderId'];
        $address1 = $this->request->data['address1'];
        $address2 = $this->request->data['address2'];
        $address3 = $this->request->data['address3'];
        $town = $this->request->data['town'];
        $region = $this->request->data['region'];
        $postcode = $this->request->data['postcode'];
        $country = $this->request->data['country'];
        $fullname = $this->request->data['fullname'];
        $company = $this->request->data['company'];
        $phonenumber = $this->request->data['phonenumber'];
        // $sku = $this->request->data['sku']; 
        $newOrderId = $mainOrderId.date('mdY'); 
        $orderMainId = explode('-',$orderId);
        $saveTempOpenOrder = ''; 
        $saveTempMergeupdate = '';
        $saveOpenOrder = ''; 
        $saveMergeupdate = '';  
        $newSplitOrderId = '';
        $userId = $this->Session->read('Auth.User.id');
        $resendByUser = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
        $data = array(); 

        //check if new order id already processed
        $checkProcessStatus = $this->OpenOrder->find('first', array( 
            'conditions' => array(
                'OpenOrder.num_order_id' => $newOrderId,
                'OpenOrder.status' => 1
            ),
            'fields' => array('OpenOrder.id'),
        )); 
       
        if(count($checkProcessStatus) > 0) {
            $newOrderId = $mainOrderId.$orderMainId[1].date('mdY');
        }

        $getSplitOrders = $this->MergeUpdate->find('all', array( 
            'conditions' => array(
                'MergeUpdate.order_id' => $mainOrderId, 
            ),
            'fields' => array('MergeUpdate.product_order_id_identify'),
        ));

        $getSplitOrdersForNewOrderId = $this->MergeUpdate->find('all', array( 
            'conditions' => array(
                'MergeUpdate.order_id' => $newOrderId,
            ),
            'fields' => array('MergeUpdate.product_order_id_identify'),
        ));

        //check if all new split ids are created
        if((count($getSplitOrdersForNewOrderId) < count($getSplitOrders))) {

            //check if order is fully processed
            $data = $this->OpenOrder->find('first', array( 
                'conditions' => array(
                    'OpenOrder.num_order_id' => $mainOrderId
                ),
                'fields' => array('OpenOrder.*'), 
            ));  
           
            if($data['OpenOrder']['status'] == 1) {
            
             
            //create new open order and new split orders for the order
            if($getSplitOrders) {
                $i=1;
                //delete new order if any created
                foreach($getSplitOrders as $gsplit) {
                    
                    // $newsid = explode('-',$gsplit['MergeUpdate']['product_order_id_identify']);
                    $newSplitOrderId = $newOrderId.'-'.$i;

                    //check if split id already created
                    $checkData = $this->MergeUpdate->find('first', array( 
                        'conditions' => array(
                            'MergeUpdate.product_order_id_identify' => $newSplitOrderId, 
                        ),
                        'fields' => array('MergeUpdate.product_order_id_identify'),
                    ));
                   
                    //check if any split id already resend
                    $oid = explode('-',$gsplit['MergeUpdate']['product_order_id_identify']);
                    $checkIfAlreadyResend = $this->MergeUpdate->find('first', array( 
                        'conditions' => array(
                            'MergeUpdate.product_order_id_identify' => $oid[0].date('mdY').'-'.$oid[1], 
                        ),
                        'fields' => array('MergeUpdate.product_order_id_identify'),
                    ));

                    if(count($checkData)==0 && count($checkIfAlreadyResend)==0) { 

                        if(isset($data['OpenOrder']['id'])) 
                            unset($data['OpenOrder']['id']);
                
                        $data['OpenOrder']['open_order_date'] = date('Y-m-d H:i:s');
                        $data['OpenOrder']['sorted_scanned'] = 0; 
                        $data['OpenOrder']['num_order_id'] = $newOrderId; 
                        $data['OpenOrder']['destination'] = $country; 
                        if(isset($data['OpenOrder']['customer_info'])) {
                            $customerDetails = unserialize($data['OpenOrder']['customer_info']);
                            $customerDetails->Address->Address1 = $address1;
                            $customerDetails->Address->Address2 = $address2;
                            $customerDetails->Address->Address3 = $address3;
                            $customerDetails->Address->Town = $town;
                            $customerDetails->Address->Region = $region;
                            $customerDetails->Address->PostCode = $postcode;
                            $customerDetails->Address->Country = $country;
                            $customerDetails->Address->FullName=  $fullname;
                            $customerDetails->Address->Company= $company;
                            $customerDetails->Address->PhoneNumber = $phonenumber;
                            $data['OpenOrder']['customer_info'] = serialize($customerDetails);
                        } 
                        $refnum = $mainOrderId.date('YmdHis');
                        if(isset($data['OpenOrder']['general_info'])) {
                            $generalDetails = unserialize($data['OpenOrder']['general_info']);
                            $generalDetails->ReceivedDate = date('Y-m-d H:i:s');
                            $generalDetails->ReferenceNum = $refnum;
                            $generalDetails->SecondaryReference = $refnum;
                            $generalDetails->ExternalReferenceNum = $refnum;
                            $data['OpenOrder']['general_info'] = serialize($generalDetails); 
                        }
                                
                        $data['OpenOrder']['parent_id'] = $mainOrderId;
                        $data['OpenOrder']['scanning_qty'] = 0;
                        $data['OpenOrder']['order_status'] = 0;
                        $data['OpenOrder']['status'] = 0;
                        $data['OpenOrder']['cancel_status'] = 0;
                        $data['OpenOrder']['manifest'] = 0;
                        $data['OpenOrder']['cn22'] = 0;
                        $data['OpenOrder']['track_id'] = '0';
                        $data['OpenOrder']['date'] = date('Y-m-d H:i:s');
                        $data['OpenOrder']['error_code'] = '';
                        $data['OpenOrder']['template_id'] = 0;
                        $data['OpenOrder']['user_id'] = 0;
                        $data['OpenOrder']['product_type'] = '';
                        $data['OpenOrder']['product_defined_skus'] = '';
                        $data['OpenOrder']['process_date'] = ''; 
                        $data['OpenOrder']['comments'] = '';
                        $data['OpenOrder']['order_type'] = 'Resend'; 
                        if(isset($data['OpenOrder']['order_id'])) {
                           $data['OpenOrder']['order_id'] = 'RESEND'.$data['OpenOrder']['order_id'];
                        }
                        $data['OpenOrder']['num_order_id'] = $newOrderId;
                        $data['OpenOrder']['amazon_order_id'] = $mainOrderId; 
                        $data['OpenOrder']['order_split_id_identifier']=$newSplitOrderId;

                        $saveTempOpenOrder = $this->TempOpenOrder->saveMany($data, array('deep' => false));   

                        if($saveTempOpenOrder) {
                            //save in merge updates data in temp table
                            $data = $this->MergeUpdate->find('first', array( 
                            'conditions' => array(
                                'MergeUpdate.product_order_id_identify' => $gsplit['MergeUpdate']['product_order_id_identify']
                            ),
                            'fields' => array('MergeUpdate.*'), 
                            )); 

                            if(isset($data['MergeUpdate']['id'])) 
                            unset($data['MergeUpdate']['id']);
                            
                            $data['MergeUpdate']['order_date'] = date('Y-m-d H:i:s'); 
                            $data['MergeUpdate']['custom_marked'] = 0;
                            $data['MergeUpdate']['custom_service_status'] = 0; 
                            $data['MergeUpdate']['scan_status'] = 0;
                            $data['MergeUpdate']['scan_date'] = '';
                            $data['MergeUpdate']['sorted_scanned'] = 0;
                            $data['MergeUpdate']['linn_fetch_orders'] = 1;
                            $data['MergeUpdate']['manifest_status'] = 0;
                            $data['MergeUpdate']['status'] = 0; 
                            $data['MergeUpdate']['assign_user']='';
                            $data['MergeUpdate']['batch_num'] = NULL;
                            $data['MergeUpdate']['created_batch'] = NULL;
                            $data['MergeUpdate']['dispatch_console'] = NULL; 
                            $data['MergeUpdate']['label_status'] = 0;
                            $data['MergeUpdate']['out_sell_marker'] = 0; 
                            $data['MergeUpdate']['pick_list_status'] = 0;
                            $data['MergeUpdate']['reverce_picklist_status'] = 0;
                            $data['MergeUpdate']['custom_service_marked'] = 0;
                            $data['MergeUpdate']['packaging_variant_id'] = 0;
                            $data['MergeUpdate']['order_barcode_image'] = '';
                            $data['MergeUpdate']['row_status'] =1;
                            $data['MergeUpdate']['order_id'] = $newOrderId; 
                            $data['MergeUpdate']['product_order_id_identify'] = $newSplitOrderId;
                            $data['MergeUpdate']['order_type'] = 'Resend';
                            $data['MergeUpdate']['parent_id'] = $mainOrderId;
                            $data['MergeUpdate']['user_id'] = 0;
                            $data['MergeUpdate']['product_type'] = 'single';
                            $data['MergeUpdate']['product_sku_identifier'] = 'single';
                            $data['MergeUpdate']['pack_order_quantity'] = 0;
                            $data['MergeUpdate']['bin_rack'] = 0;
                            $data['MergeUpdate']['update_quantity'] = 0;
                            $data['MergeUpdate']['packSlipId'] = 0;
                            $data['MergeUpdate']['lable_id'] = 0;
                            $data['MergeUpdate']['brt_barcode'] = NULL;
                            $data['MergeUpdate']['track_id'] = '0';
                            $data['MergeUpdate']['assign_user'] = '';
                            $data['MergeUpdate']['picked_date'] = '';
                            $data['MergeUpdate']['process_date'] = ''; 
                            $data['MergeUpdate']['manifest_date'] = '';
                            $data['MergeUpdate']['user_name'] = '';
                            $data['MergeUpdate']['pc_name'] = '';
                            $data['MergeUpdate']['picklist_username'] = '';
                            $data['MergeUpdate']['manifest_username'] = '';
                            $data['MergeUpdate']['scanned_pc'] = '';
                            $data['MergeUpdate']['cancelled_user'] = '';
                            $data['MergeUpdate']['cancelled_pc'] = '';
                            $data['MergeUpdate']['brt'] = '0';
                            $data['MergeUpdate']['brt_phone'] = '0';
                            $data['MergeUpdate']['brt_exprt'] = '0';
                            $data['MergeUpdate']['royalmail'] = 0;
                            $data['MergeUpdate']['picklist_inc_id'] = 0;
                            $data['MergeUpdate']['merge_order_date'] = date('Y-m-d H:i:s');
                            $data['MergeUpdate']['rm_error_msg'] = 0;
                            $data['MergeUpdate']['timestamp'] = date('Y-m-d H:i:s'); 
                            $data['MergeUpdate']['delevery_country'] = $country;

                            $saveTempMergeupdate =  $this->TempMergeUpdate->saveMany($data, array('deep' => false));
                        }

                        //save in main open and merge order table
                        if($saveTempOpenOrder && $saveTempMergeupdate) {
                            
                            $data = $this->TempOpenOrder->find('first', array( 
                                'conditions' => array(
                                    'TempOpenOrder.num_order_id' => $newOrderId
                                ),
                                'fields' => array('TempOpenOrder.*'), 
                            ));

                            if(isset($data['TempOpenOrder']['id'])) 
                               unset($data['TempOpenOrder']['id']);
                            
                            $checkOpenOrder = $this->OpenOrder->find('first', array( 
                                'conditions' => array(
                                    'OpenOrder.num_order_id' => $newOrderId
                                ),
                                'fields' => array('OpenOrder.id'), 
                            ));
                            
                            if(count($checkOpenOrder) == 0) {
                               $saveOpenOrder = $this->OpenOrder->saveMany($data, array('deep' => false));
                            } else {
                               //open order is already in first iteration of loop
                               $saveOpenOrder = true;
                            }

                            if($saveOpenOrder) {  
                                $this->TempOpenOrder->deleteAll(array('order_split_id_identifier' => $newSplitOrderId), false);
                            
                                $data = $this->TempMergeUpdate->find('first', array( 
                                    'conditions' => array(
                                        'TempMergeUpdate.order_id' => $newOrderId
                                    ),
                                    'fields' => array('TempMergeUpdate.*'), 
                                )); 

                                if(isset($data['TempMergeUpdate']['id'])) 
                                unset($data['TempMergeUpdate']['id']);

                                $saveMergeupdate = $this->MergeUpdate->saveMany($data, array('deep' => false));
                            }
                        }

                    if($saveMergeupdate) {
                        //delete from temp table

                        $this->TempMergeUpdate->deleteAll(array('order_id' => $newOrderId), false); 
                        $this->MergeUpdate->updateAll(
                            array( 
                                'MergeUpdate.is_resend'=>1,
                                'MergeUpdate.resend_by'=>$userId,
                                'MergeUpdate.resend_date'=>"'".date('Y-m-d H:i:s')."'",
                                'MergeUpdate.resend_by_user'=> "'".$resendByUser."'",
                                'MergeUpdate.resend_reason' =>"'".$reason."'",
                                'MergeUpdate.resend_comments' =>"'".$comments."'" 
                            ),
                            array('MergeUpdate.order_id'=>$mainOrderId)
                        ); 
                        $mergeData = $this->getMergeDataByOrderId($newOrderId); 
                        $this->saveScanOrders($mergeData,$newOrderId);
                        /*---------Manage Ink SKU Qty-----*/
                        $this->InkSkuInventory($mergeData);
                        /*------------End----------------*/
                        $this->saveorderLocations($mergeData,$newOrderId);
                        
                        $this->saveInventoryRecord($mergeData);  
						
                        
                    }  
            } 
           $i++;
         }
		 $this->applyPostal($newOrderId);
         $this->Session->setFlash('Order created successfully!', 'flash_success');
         echo json_encode(array('msg'=>'Order created successfully!','status'=>1,'neworderid'=>$newOrderId));
        }
        } else {
            $splitStatus = '';
            //get split order status
            $splitOrder = $this->MergeUpdate->find('all', array(
                'conditions' => array(
                    'MergeUpdate.order_id'=>$mainOrderId
                ),
                'fields'=>array('MergeUpdate.product_order_id_identify','MergeUpdate.status')
            ));
            if($splitOrder) {
                $splitStatus = '<table class="table">
                <thead>
                <tr>
                    <th>Order ID</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>';
                foreach($splitOrder as $split) {
                    $sts = ($split['MergeUpdate']['status']==1)?'Processed':'-';
                    $splitStatus .='<tr class="productlistcontainer">
                                    <td><span>'.$split['MergeUpdate']['product_order_id_identify'].'</span></td>
                                    <td><span>'.$sts.'</span></td>
                                </tr>';
                }

                $splitStatus.='</tbody></table>';
            }
            echo json_encode(array('msg'=>'<h5>Cannot full resend order as this order is not completely processed. Below are the order details:</h5>'.$splitStatus,'status'=>2));
            exit;
        }
        } else { 
			/*echo '==1==';
			$r =$this->applyPostal($newOrderId);
			pr($r);*/
            echo json_encode(array('msg'=>'Order already resend for today! Please try tommorrow','status'=>-1));
        }
        exit;
    }
	
	public function applyPostal($order_id = '') {
		
		$this->loadModel('MergeUpdate');
		$this->loadModel('OpenOrder');
		
		$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id) ) );
		
		if(count($openOrder) > 0){
 			
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
 			$shipping_info = unserialize( $openOrder['OpenOrder']['shipping_info']);
			 
 		 	if($customer_info->Address->Country == 'United Kingdom' && strtolower($shipping_info->PostalServiceName) == 'standard'){
 				App::import( 'Controller' , 'Whistl' );		
				$wobj 	= new WhistlController();
				$wobj->applyWhistl($order_id);
			}elseif($customer_info->Address->Country == 'United Kingdom' && strtolower($shipping_info->PostalServiceName) != 'standard'){
 				App::import( 'Controller' , 'Jerseypost' );		
				$jobj 	= new JerseypostController();
				$jobj->createShipment($order_id);				
			}elseif($customer_info->Address->Country == 'Italy'){
 				App::import( 'Controller' , 'Brt' );		
				$bobj 	= new BrtController();
				$bobj->generateBrtLabels($order_id);
			}else{			 
				App::import( 'Controller' , 'Cronjobs' );		
				$jobj 	= new CronjobsController();
				$jobj->deliveryService($order_id);
				 
				App::import( 'Controller' , 'Postnl' );		
				$pobj 	= new PostnlController();
				$pobj->Labelling($order_id);
			}
			
			
			/*elseif(strtolower($shipping_info->PostalServiceName) == 'express'){
 				App::import( 'Controller' , 'Cronjobs' );		
				$jobj 	= new CronjobsController();
				//$jobj->deliveryService($order_id);
				echo '11';
			}*/
		}
		return 1;
	}
 
    public function saveInventoryRecord($mergeData=array()) {
        //get current stock level for the product
        $this->loadModel('InventoryRecord'); 
        $this->loadModel('Product');

        if($mergeData) {

          foreach($mergeData as $merge) { 
            
            $skusMerge = explode(',',$merge['MergeUpdate']['sku']);
            
            foreach($skusMerge as $mkey=>$mvalue) {
                
                $skusingle = explode('S-',$mvalue);  
                $sku='S-'.$skusingle[1];
                $quantity = explode('X',$skusingle[0]);

                $prodDetails = $this->Product->find('first',array(
                    'conditions' => array(
                        'Product.product_sku' => $sku, 
                    ),
                    'fields' => array('Product.current_stock_level','ProductDesc.barcode')
                ));
                $barcode = '';	
                if(count($prodDetails)){
                    $barcode = $prodDetails['ProductDesc']['barcode'];
                } 
                $totalQty = $quantity[0]; 
                $currentStock = $prodDetails['Product']['current_stock_level'];
                $newCurrentStock = (int)$currentStock - (int)$totalQty;
                //prepare inventory record array
                $inventoryRecord=array();
                $inventoryRecord['InventoryRecord']['action_type']  = $merge['MergeUpdate']['order_id'].'-Order Resend -deduct inventory by-'.$totalQty;
                $inventoryRecord['InventoryRecord']['sku']          = $sku;
                $inventoryRecord['InventoryRecord']['barcode']      = $barcode;
                $inventoryRecord['InventoryRecord']['currentStock'] = $currentStock;
                $inventoryRecord['InventoryRecord']['quantity']     = $totalQty;
                $inventoryRecord['InventoryRecord']['after_maniplation'] = $newCurrentStock;
                $inventoryRecord['InventoryRecord']['split_order_id'] = $merge['MergeUpdate']['order_id'];
                $inventoryRecord['InventoryRecord']['status'] = 'Resend Order';
                $inventoryRecord['InventoryRecord']['date'] = date('Y-m-d H:i:s');

                //insert in inventory records table
                $this->InventoryRecord->create();
                $this->InventoryRecord->save($inventoryRecord);
          }
        }
       }
       return true;
    }

    public function getPackagingVariant($identifireID='') {
        $this->loadModel('TempMergeUpdate');
        $envelope = array();
        $params = array(
            'conditions' => array(
                'TempMergeUpdate.product_order_id_identify' => $identifireID
            ),
            'fields' => array(
                'TempMergeUpdate.order_id',
                'TempMergeUpdate.id',
                'TempMergeUpdate.packet_weight as weight',
                'TempMergeUpdate.packet_length as lenght',
                'TempMergeUpdate.packet_width as width',
                'TempMergeUpdate.packet_height as height',
                'TempMergeUpdate.order_split',
                'TempMergeUpdate.sku as SKU',
                'TempMergeUpdate.product_order_id_identify'
            )				
        );
        $getSpecificOrder = $this->TempMergeUpdate->find( 'first', $params ); 
        $this->packagingManipulate( $identifireID , $getSpecificOrder );
        return true;
    }

    public function packagingManipulate( $identifireID = null,$getMregeUpdateOrders = null )	
    {			
        $this->loadModel( 'PackageEnvelope' );
        $this->loadModel( 'Packagetype' );
        $this->loadModel( 'Product' );
        $this->loadModel( 'ProductDesc' );
        $data = array();
        $mergeOrders = array();
        //Manipulate all conditions
        /*
            * 1) Box Only (High Priority) 
            * 2) Envelope / Box (Medium Priority) 
            * 3) Any (Low Priority)
            * 
            */ 
        $mergeOrders = $getMregeUpdateOrders;	
                 
            //Split all sku for getting class type
            $skuSplit = explode( ',' , $mergeOrders['TempMergeUpdate']['SKU']);
            $variantArray = array();
            $k = 0;
            while( $k <= count($skuSplit)-1 ) {
                //Getting proper class from every sku
                $pop = $skuSplit[$k];
                $properSku = explode( 'XS-' , $pop );
                
                $doneSku = 'S-'.$properSku[1]; 
                
                $param = array(
                    'conditions' => array(
                        'Product.product_sku' => $doneSku
                    ),
                    'fields' => array(
                        'ProductDesc.variant_envelope_id',
                        'ProductDesc.variant_envelope_name'
                    )
                );
                $variant = $this->ProductDesc->find( 'first' , $param );			
                if(isset($variant['ProductDesc']['variant_envelope_name'])) {		
                   $variantArray[] = $variant['ProductDesc']['variant_envelope_name'];
                }
                if(isset($variant['ProductDesc']['variant_envelope_id'])) {			
                   $variantArray[] = $variant['ProductDesc']['variant_envelope_id'];	
                }
                
                
            $k++;	
            }
            $boxOnly = 0;
            $envelopeBox = 0;
            $any	 = 0;

            if( in_array( 'Box Only', $variantArray )) {
                $boxOnly++;
            } else if( in_array( 'Envelope / Box', $variantArray)) {
                $envelopeBox++;
            } else if( in_array( 'Any', $variantArray)) {
                $any++;
            } else {}
            
            $type='';
            if($boxOnly > 0 ) {
                $type = 'Box Only'; 
            } else if( $boxOnly == 0 && $envelopeBox > 0 && $any == 0) {
                $type = 'Envelope / Box'; 
            } else if($boxOnly == 0 && $envelopeBox == 0 && $any > 0) {
                $type = 'Any'; 
            } else {}
            $id		= $mergeOrders['TempMergeUpdate']['id'];
            $weight	= $mergeOrders['TempMergeUpdate']['weight'];
            $length	= $mergeOrders['TempMergeUpdate']['lenght'];
            $width  = $mergeOrders['TempMergeUpdate']['width'];
            $height	= $mergeOrders['TempMergeUpdate']['height']; 

            $getEnvelopes =	$this->PackageEnvelope->find( 'all', array(
                'conditions' => array(
                    'PackageEnvelope.envelope_length >= ' => $length,
                    'PackageEnvelope.envelope_width >= ' => $width,
                    'PackageEnvelope.envelope_height >= ' => $height,
                    'Packagetype.package_type_name' => $type
                )
            ));
            
            if( count($getEnvelopes) > 0 ) {
                    $minCost    =  Set::sort($getEnvelopes, '{n}.PackageEnvelope.cost', 'ASC');   
                    
                    $wgth = $minCost[0]['PackageEnvelope']['envelope_weight'];
                    $cost = $minCost[0]['PackageEnvelope']['envelop_cost'];
                    $name = $minCost[0]['PackageEnvelope']['envelope_name'];
                    $pE_id = $minCost[0]['PackageEnvelope']['id'];
                    $type = $type;
                } else {
                    $wgth = '0';
                    $cost = '0';
                    $name = 'Not Matched';
                    $pE_id = '0';
                    $type= $type;
                } 
                
                $strQuery = "Update temp_merge_updates as MU set MU.envelope_weight = {$wgth}, 
                            MU.envelope_cost = {$cost}, 
                            MU.envelope_name = '{$name}', 
                            MU.envelope_id = {$pE_id}, 
                            MU.packaging_type = '{$type}' 
                            where MU.id = {$id}";
            
                $this->TempMergeUpdate->query( $strQuery );  
                return true;
    }
    
    public function getMergeDataBySplitOrderId($newOrderId='') {
        $mergeData = array();
        $mergeData = $this->MergeUpdate->find('all', array( 
            'conditions' => array(
                'MergeUpdate.product_order_id_identify' => $newOrderId
            ),
            'fields' => array('MergeUpdate.barcode','MergeUpdate.id','MergeUpdate.sku','MergeUpdate.product_order_id_identify','MergeUpdate.order_id'), 
        )); 

        return $mergeData;
    }

    public function getMergeDataByOrderId($newOrderId='') {
        $mergeData = array();
        $mergeData = $this->MergeUpdate->find('all', array( 
            'conditions' => array(
                'MergeUpdate.order_id' => $newOrderId
            ),
            'fields' => array('MergeUpdate.id','MergeUpdate.barcode','MergeUpdate.sku','MergeUpdate.product_order_id_identify','MergeUpdate.order_id'), 
        )); 

        return $mergeData;
    }   

    public function saveScanOrders($mergeData=array(),$newOrderId='') {
        $this->loadModel('ScanOrder');
        $this->loadModel('MergeUpdate'); 
        $saveData = array();  
        if(count($mergeData) > 0 ) {
            foreach($mergeData as $mgdata) {

                $id = $mgdata['MergeUpdate']['id'];
                $skus = explode(',',$mgdata['MergeUpdate']['sku']);
                $barcode = explode( ',', $mgdata['MergeUpdate']['barcode']);
                $splitOrderID =	$mgdata['MergeUpdate']['product_order_id_identify']; 
                $i = 0;

                if($skus) {
                    
                    foreach($skus as $key=>$value) {

                        $newSku[$i]			=	 explode( 'XS-', $value);
                        $newSku[$i][2]		=	 $barcode[$i];
                        
                        $searchsku	=	$this->ScanOrder->find( 'first', array('conditions' =>array('ScanOrder.split_order_id' => $splitOrderID, 'ScanOrder.barcode' =>  $newSku[$i][2])));
                       
                        $scanId = '';
                        $quantity = 0; 

                        if( count($searchsku) > 0)
                        {
                            $quantity = $searchsku['ScanOrder']['quantity'];
                            $scanId = $searchsku['ScanOrder']['id'];
                        }

                        $saveData[]['ScanOrder'] = array(
                            'split_order_id' => $splitOrderID, 
                            'sku'=>'S-'.$newSku[$i][1],
                            'barcode'=>$newSku[$i][2],
                            'order_id'=>$id,
                            'quantity'=>$quantity + $newSku[$i][0],
                            'id'=>$scanId,
                            'scan_date'=>date( 'Y-m-d' )
                        );
                        $quantity=0;
                        $i++;
                    }
                }
           }
        }

        $this->ScanOrder->saveMany($saveData);
        return true;
    }

    public function saveorderLocations($mergeData = array(),$splitOrderId=''){
		$this->loadModel('OrderLocation');
        $this->loadModel('BinLocation');		
        $this->loadModel('Product');
		$this->loadModel('CheckIn');	
		 
        if(count($mergeData) > 0 ) {
            foreach($mergeData as $mgdata) {
                $skus = explode(',',$mgdata['MergeUpdate']['sku']);

                if($skus) {
                    foreach($skus as $key=>$value) {
                    $t = explode('XS-',$value);  

                    $sku = "S-".trim($t[1]);  
                    $quantity = trim($t[0]);

                    $product = $this->Product->find('first',array('conditions' => array('Product.product_sku'=>$sku),'fields'=>array('ProductDesc.barcode')));
					$barcode = '';	
                    if(count($product)){
                        $barcode = $product['ProductDesc']['barcode'];
                    } 
                    $order_id = $mgdata['MergeUpdate']['order_id']; 
                    $data_con = 1; 
                            
                    $po_name = array(); $posIds = array(); $pos = array(); $binname = array(); 
                    $available_qty = 0; $available_qty_bin = 0;	
                    
                    $local_barcode = $this->Components->load('Common')->getLocalBarcode($barcode);
			
			        $poDetail = $this->CheckIn->find( 'all', 
				    array( 'conditions' => array( 'CheckIn.barcode IN' => array($barcode,$local_barcode), 'CheckIn.selling_qty != CheckIn.qty_checkIn','po_name !=""'),
                        'order' => 'CheckIn.date  ASC ' ) );
 
                    if(count($poDetail) > 0){
                        $qt_count = 0; 				
                        foreach($poDetail as $po){
                            $poQty = $po['CheckIn']['qty_checkIn'] - $po['CheckIn']['selling_qty'];
                            if($poQty > 0){						
                                for($i=0; $i <= $quantity ;$i++){						 
                                    if($qt_count >= $quantity ){
                                        break;
                                    }else if(($poQty - $i)  > 0){							
                                        $po_name[$po['CheckIn']['id']][] = $po['CheckIn']['po_name'];	
                                        $available_qty++;							
                                        $qt_count++;
                                    }												
                                }						
                            }
                        }
                    }
			 
			
                $getStock = $this->BinLocation->find( 'all', array( 'conditions' => array( 'BinLocation.barcode' => $barcode),'order' => 'priority ASC' ) );
                $bin_name = array(); $finalData['bin_location'] = "";
                if(count($getStock) > 0){
                    
                    $qt_count = 0 ;
                    foreach($getStock as $val){ 
                            $binData['id'] = $val['BinLocation']['id'];					
                            for($i=0; $i <= $quantity ;$i++){						 
                                if($qt_count >= $quantity ){
                                    break;
                                }else if(($val['BinLocation']['stock_by_location'] - $i)  > 0){							
                                    $bin_name[$val['BinLocation']['id']][] = $val['BinLocation']['bin_location'];	
                                    $available_qty_bin++;						
                                    $qt_count++;
                                }												
                            }			 
                    }				
                    
                } 
				if(count($po_name) > 0){
					foreach(array_keys($po_name) as $k){
						$qts   = count($po_name[$k]);	
						$pos[] = $po_name[$k][0];
						$posIds[] = $k;
						$this->CheckIn->updateAll( array('CheckIn.selling_qty' =>  "CheckIn.selling_qty + $qts"),array('CheckIn.id' => $k));
					}
				}
				
				if(count($bin_name) > 0)
				{
					foreach(array_keys($bin_name) as $k){					
						$qts = count($bin_name[$k]);	
						$binname[] = $bin_name[$k][0];	
						
						$finalData['available_qty_check_in'] = $available_qty;
						$finalData['available_qty_bin'] = $qts;
						$finalData['quantity']      	= $quantity;
						$finalData['order_id']			= $order_id;	
						$finalData['split_order_id']	= $mgdata['MergeUpdate']['product_order_id_identify'];					
						$finalData['sku']	   			= $sku;								
						$finalData['barcode']  			= $barcode;	
						$finalData['po_name']  			= implode(",",$pos);	
						$finalData['po_id']    			= implode(",",$posIds);	
						$finalData['bin_location']  	= $bin_name[$k][0];	
						$finalData['data_con']  		= $data_con;					
						$this->OrderLocation->saveAll( $finalData );
						
						$this->BinLocation->updateAll( array('BinLocation.stock_by_location' =>  "BinLocation.stock_by_location - $qts"),array('BinLocation.id' => $k));
					}
				} else {
					
						$finalData['available_qty_check_in'] = $available_qty;
						$finalData['available_qty_bin'] = '0';
						$finalData['quantity']      	= $quantity;
						$finalData['order_id']			= $order_id;
						$finalData['split_order_id']	= $mgdata['MergeUpdate']['product_order_id_identify'];						
						$finalData['sku']	   			= $sku;								
						$finalData['barcode']  			= $barcode;	
						$finalData['po_name']  			= implode(",",$pos);	
						$finalData['po_id']    			= implode(",",$posIds);	
						$finalData['bin_location']  	= 'No Location';
						$finalData['data_con']  		= $data_con;					
						$this->OrderLocation->saveAll( $finalData );
                }
                 
                $currentLevelStock =  $this->Product->find('first',array(
                    'conditions' => array(
                        'Product.product_sku' => $sku
                    ),
                    'fields' => array('Product.current_stock_level'),  
                ));
                
                if(isset($currentLevelStock['Product']['current_stock_level'])) {
                    $cqty=1;
                    while($cqty<=$quantity) {
                        if(($currentLevelStock['Product']['current_stock_level']-1)>=0) {
                          $this->Product->updateAll( array('Product.current_stock_level' =>  "Product.current_stock_level - 1"),array('Product.product_sku' => $sku));
                        }
                        $cqty++;
                   }  
                }
            }				
        }
      }
    }	
    return true;	
    }


    
    public function InkSkuInventory($mergeData = array()){
        
    if(count($mergeData) > 0 ) {
        foreach($mergeData as $mgdata) {
            $skus = explode(',',$mgdata['MergeUpdate']['sku']);

            if($skus) {
              foreach($skus as $key=>$value) {
                $data = array();  
                $t = explode('XS-',$value);   
                $sku = "S-".trim($t[1]);   

                $data['quantity']= trim($t[0]);					
                $data['sku']="S-".trim($t[1]);			 
                $data['split_order_id']=$mgdata['MergeUpdate']['order_id']; 

            if(count($data) > 0){
            
                $this->loadModel( 'InkSku' ); 
                
                $quantity = $data['quantity'];					
                $sku      = $data['sku'];			 
                $order_id = $data['split_order_id']; 
                        
                $po_name = array(); $posIds = array();
                $pos = array();     $binname = array(); 
                $available_qty = 0; $available_qty_bin = 0;	
                    
                $check_ink = $this->InkSku->find( 'first', array( 'conditions' => array( 'sku' => $sku ) ) );
                
                if( count($check_ink) > 0 ){
                    
                    $this->loadModel( 'OpenOrder' );
                    $ord = $this->OpenOrder->find( 'first', array( 'conditions' => array('num_order_id' =>$order_id),'fields' => array('customer_info') ) ); 
                    $customer_info = unserialize( $ord['OpenOrder']['customer_info']);  
                    
                    if($customer_info->Address->Country == 'United Kingdom'){
                            
                        $this->loadModel( 'BinLocation' );		
                        $this->loadModel( 'CheckIn'); 	
                        $this->loadModel( 'Product'); 
                        $this->loadModel( 'InkOrderLocation'); 
                
                        $ink_sku = 'S-INKENVELOPES';
                        $pro_ink = $this->Product->find( 'first', array( 'conditions' => array( 'product_sku' => $ink_sku ) ) );
                        $ink_barcode = $pro_ink['ProductDesc']['barcode'] ;
                        
                        $local_barcode = $this->Components->load('Common')->getLocalBarcode($ink_barcode);
                        
                        $poDetail = $this->CheckIn->find( 'all', 
                            array( 'conditions' => array( 'CheckIn.barcode IN' => array($ink_barcode,$local_barcode), 'CheckIn.selling_qty != CheckIn.qty_checkIn','po_name !=""'),'order' => 'CheckIn.date  ASC ' ) );
                        
                            
                            if(count($poDetail) > 0){
                            $qt_count = 0; 				
                            foreach($poDetail as $po){
                                $poQty = $po['CheckIn']['qty_checkIn'] - $po['CheckIn']['selling_qty'];
                                if($poQty > 0){						
                                        for($i=0; $i <= $quantity ;$i++){						 
                                            if($qt_count >= $quantity ){
                                                break;
                                            }else if(($poQty - $i)  > 0){							
                                            $po_name[$po['CheckIn']['id']][] = $po['CheckIn']['po_name'];	
                                            $available_qty++;							
                                            $qt_count++;
                                        }												
                                        }						
                                }
                            }
                        }
                            
                        
                        $getStock = $this->BinLocation->find( 'all', array( 'conditions' => array( 'BinLocation.barcode' => $ink_barcode),'order' => 'priority ASC' ) );
                        $bin_name = array(); $finalData['bin_location'] = "";
                        if(count($getStock) > 0){
                            
                            $qt_count = 0 ;
                            foreach($getStock as $val){
                                    
                                    $binData['id'] = $val['BinLocation']['id'];					
                                        for($i=0; $i <= $quantity ;$i++){						 
                                            if($qt_count >= $quantity ){
                                                break;
                                            }else if(($val['BinLocation']['stock_by_location'] - $i)  > 0){							
                                            $bin_name[$val['BinLocation']['id']][] = $val['BinLocation']['bin_location'];	
                                            $available_qty_bin++;						
                                            $qt_count++;
                                        }												
                                        }		 
                            }			 
                        } 
                            
                        if(count($po_name) > 0){
                            foreach(array_keys($po_name) as $k){
                                $qts   = count($po_name[$k]);	
                                $pos[] = $po_name[$k][0];
                                $posIds[] = $k;
                                $this->CheckIn->updateAll( array('CheckIn.selling_qty' =>  "CheckIn.selling_qty + $qts"),array('CheckIn.id' => $k));
                            }
                        }
                        
                        if(count($bin_name) > 0)
                        {
                            foreach(array_keys($bin_name) as $k){					
                                $qts = count($bin_name[$k]);	
                                $binname[] = $bin_name[$k][0];	
                                
                                $finalData['available_qty_check_in'] = $available_qty;
                                $finalData['available_qty_bin'] = $qts;
                                $finalData['quantity']      	= $quantity;
                                $finalData['order_id']			= $order_id;					
                                $finalData['sku']	   			= $sku;				
                                $finalData['ink_sku']	   		= $ink_sku;					
                                $finalData['barcode']  			= $ink_barcode;	
                                $finalData['po_name']  			= implode(",",$pos);	
                                $finalData['po_id']    			= implode(",",$posIds);	
                                $finalData['bin_location']  	= $bin_name[$k][0];					
                                $this->InkOrderLocation->saveAll( $finalData );
                                file_put_contents(WWW_ROOT .'logs/ink_sku_'.date('dmY').'.log', print_r($finalData,true), FILE_APPEND | LOCK_EX);
                                
                                $this->BinLocation->updateAll( array('BinLocation.stock_by_location' =>  "BinLocation.stock_by_location - $qts"),array('BinLocation.id' => $k));
                            }
                        } else {
                            
                                $finalData['available_qty_check_in'] = $available_qty;
                                $finalData['available_qty_bin'] = '0';
                                $finalData['quantity']      	= $quantity;
                                $finalData['order_id']			= $order_id;					
                                $finalData['sku']	   			= $sku;	
                                $finalData['ink_sku']	   		= $ink_sku;								
                                $finalData['barcode']  			= $ink_barcode;	
                                $finalData['po_name']  			= implode(",",$pos);	
                                $finalData['po_id']    			= implode(",",$posIds);	
                                $finalData['bin_location']  	= 'No Location';					
                                $this->InkOrderLocation->saveAll( $finalData );
                                file_put_contents(WWW_ROOT .'logs/ink_sku_'.date('dmY').'.log', print_r($finalData,true), FILE_APPEND | LOCK_EX);
                    
                        }	
                        
                        // $this->Product->updateAll( array('Product.current_stock_level' =>  "Product.current_stock_level - $quantity"),array('Product.product_sku' => $ink_sku));

                        // $currentLevelStock =  $this->Product->find('first',array(
                        //     'conditions' => array(
                        //         'Product.product_sku' => $ink_sku
                        //     ),
                        //     'fields' => array('Product.current_stock_level'),  
                        // ));
                        
                        // if(count($currentLevelStock['Product']['current_stock_level'])>0) {
                        //    foreach($cqty=1;$cqty<=$currentLevelStock['Product']['current_stock_level'];$cqty++) {
                        //     $this->Product->updateAll( array('Product.current_stock_level' =>  "Product.current_stock_level - $cqty"),array('Product.product_sku' => $ink_sku));    
                        //    }
                        // }
                    
                    }
            }						
        }
        return true;
       }
      }
     } 
    }
   }

   public function showReturnOrders() {
        $this->loadModel('MergeUpdate');
        $this->loadModel('OpenOrder'); 
        // $this->loadModel('Refund'); 
        ini_set('max_execution_time', 0);
        $this->layout = 'index';
        $orderRelation = array();
        $perPage = Configure::read( 'perpage' );
        $status = 4;
        $emails = array('jake.shaw@euracogroup.co.uk','lalit.prasad@jijgroup.com','shashi.b.kumar@jijgroup.com','pappu.k@euracogroup.co.uk','avadhesh.kumar@jijgroup.com','anna.kedziora@onet.eu','aakash.kushwah@jijgroup.com','neha@euracogroup.co.uk');
        
        if(in_array($this->Session->read('Auth.User.email'),$emails)){ 
            if(isset($_REQUEST['searchkey']) && $_REQUEST['searchkey']){
                if($_REQUEST['searchtype']=='orderid') {
                $conditions =  array('MergeUpdate.status' => $status, 'MergeUpdate.product_order_id_identify LIKE ' => trim($_REQUEST['searchkey']).'%') ;
                } else if($_REQUEST['searchtype']=='referenceid') {
                $conditions =  array('MergeUpdate.status' => $status, 'OpenOrder.amazon_order_id LIKE ' => trim($_REQUEST['searchkey']).'%') ;
                } else {
                $conditions =  array('MergeUpdate.status' => $status, 'OpenOrder.customer_info LIKE ' => '%'.trim($_REQUEST['searchkey']).'%') ; 
                }
            } else { 
                $conditions =  array('MergeUpdate.status' => $status);
            } 
        } else { 
            if(isset($_REQUEST['searchkey']) && $_REQUEST['searchkey']){
                if($_REQUEST['searchtype']=='orderid') {
                    $conditions =  array('MergeUpdate.status' => $status, 'MergeUpdate.user_id' => $this->Session->read('Auth.User.id'), 'MergeUpdate.product_order_id_identify LIKE ' => trim($_REQUEST['searchkey']).'%') ;
                } else if($_REQUEST['searchtype']=='referenceid') {
                    $conditions =  array('MergeUpdate.status' => $status, 'MergeUpdate.user_id' => $this->Session->read('Auth.User.id'), 'OpenOrder.amazon_order_id LIKE ' => trim($_REQUEST['searchkey']).'%') ;
                } else {
                    $conditions =  array('MergeUpdate.status' => $status, 'MergeUpdate.user_id' => $this->Session->read('Auth.User.id'), 'OpenOrder.customer_info LIKE ' => '%'.trim($_REQUEST['searchkey']).'%') ; 
                }
            } else {
                $conditions =  array('MergeUpdate.status' => $status, 'MergeUpdate.user_id' => $this->Session->read('Auth.User.id'));
            }
        } 

        $this->paginate = array(
            'joins' => array(
                array(
                    'table' => 'open_orders',
                    'alias' => 'OpenOrder',
                    'type' => 'INNER',
                    'conditions' => array(
                        'MergeUpdate.order_id = OpenOrder.num_order_id'
                    )
                )
            ),
            'conditions' => $conditions,
            'fields' => array('OpenOrder.num_order_id','OpenOrder.parent_id','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.items','OpenOrder.totals_info','OpenOrder.amazon_order_id','MergeUpdate.*'),
            'order' => 'MergeUpdate.return_date DESC',
            'limit' => $perPage
        );
        
        $Returnorders = $this->paginate('MergeUpdate');
        
        $this->set( 'Returnorders', $Returnorders );
    }

    public function getReturnDetails() { 
        $orderId = $this->request->data['orderid']; 
        $splitid = $this->request->data['splitid'];
        $this->loadModel('OpenOrder');
        $this->loadModel('MergeUpdate'); 
        $this->loadModel('OrderLocation');
        $this->loadModel('Product');
        $this->loadModel('BinLocation');
        $storeData = ''; 
        $customerInfo = '';
        $address1 = '';
        $address2 = '';
        $address3 = '';
        $town = '';
        $region = '';
        $postcode ='';
        $country ='';
        $fullName='';
        $company='';
        $phoneNumber='';
        $resendReason = '';
        $resendComments = ''; 
        $skuPriceList = array();
        $skuTitle = array();
		$source='';
		$subSource='';
		$orderDate = '';
		$serviceName='';
		$totalCharge='';
        $products='';
        $warehouseItemCondition = '';
        $warehouseReturnComments = '';
        $skuLocationlist='';
        $productListItem = array();
        $skuData=array();
        $isMovedToLocation=0;
        $skuItems = array();
        $locations = array();

        $orderMergeDetails = $this->MergeUpdate->find('first', array( 
            'conditions' => array(
                'MergeUpdate.product_order_id_identify' => $splitid
            )
         ));  
        $openOrderDetails = $this->OpenOrder->find('first', array(
           'conditions' => array(
			   'OpenOrder.num_order_id' =>$orderId
		   )
		)); 
        
        $locations = $this->BinLocation->find('all', array(
            'fields' => array(
              'BinLocation.bin_location'
             )
            )
        ); 
        if(count($openOrderDetails)>0) {
 
            $skusMerge = explode(',',$orderMergeDetails['MergeUpdate']['sku']);
            foreach($skusMerge as $mkey=>$mvalue) {
                $skusingle = explode('S-',$mvalue);  
                $skuItems[]='S-'.$skusingle[1]; 
            } 

            if($skuItems) {
                $productListItem = $this->Product->find('all', array(
                    'conditions' => array(
                        'Product.product_sku'=>$skuItems
                    ),
                    'fields'=>array('Product.product_name','Product.product_sku')
                )); 
            }

            if($productListItem) {
                foreach($productListItem as $prod) {
                    $skuTitle[$prod['Product']['product_sku']] = $prod['Product']['product_name'];
                }
            }
            //get customer info and resend details if saved in temp table
			$customerInfo = unserialize($openOrderDetails['OpenOrder']['customer_info']);
			$generalInfo  = unserialize($openOrderDetails['OpenOrder']['general_info']);
			$totalDetails = unserialize($openOrderDetails['OpenOrder']['totals_info']);
            $address1 = $customerInfo->Address->Address1;
            $address2 = $customerInfo->Address->Address2;
            $address3 = $customerInfo->Address->Address3;
            $town = $customerInfo->Address->Town;
            $region = $customerInfo->Address->Region;
            $postcode =$customerInfo->Address->PostCode;
            $country =$customerInfo->Address->Country;
            $fullName=$customerInfo->Address->FullName;
            $company=$customerInfo->Address->Company;
            $phoneNumber=$customerInfo->Address->PhoneNumber;
            $resendReason = $orderMergeDetails['MergeUpdate']['resend_reason'];
			$resendComments = $orderMergeDetails['MergeUpdate']['resend_comments']; 
			$source = $generalInfo->Source;
            $subSource = $generalInfo->SubSource;
            $totalCharge = 0;
            if(isset($totalDetails->TotalCharge)) {
                $totalCharge = $totalDetails->TotalCharge.' '.$totalDetails->Currency;    
            } 
            $warehouseItemCondition = $orderMergeDetails['MergeUpdate']['warehouse_return_item_condition'];
            $warehouseReturnComments = $orderMergeDetails['MergeUpdate']['warehouse_return_comments'];
            $isMovedToLocation = $orderMergeDetails['MergeUpdate']['is_returned_moved'];
		}
		
        if($orderMergeDetails) {
        
            $products = '<table class="table">
                <thead>
                <tr> 
                  <th>Sku</th>
                  <th>Title</th>
                  <th>Quantity</th>
                </tr>
                </thead>
                <tbody id="skulistbody">';
            // foreach($orderMergeDetails as $order) {
			$serviceName = $orderMergeDetails['MergeUpdate']['postal_service'];
			$orderDate = $orderMergeDetails['MergeUpdate']['order_date'];
            //extracting quantity from sku
            $skus = explode(',',$orderMergeDetails['MergeUpdate']['sku']);
				if($skus) {
					foreach($skus as $key=>$value) {
						$sku = explode('S-',$value);
						$quantity = explode('X',$sku[0]);
                        $prodTitle = '';
                        $skuData[]='S-'.$sku[1];
						if(isset($skuTitle['S-'.$sku[1]])) {
							$prodTitle = $skuTitle['S-'.$sku[1]];
						}
						$products .='<tr class="productlistcontainer"> 
                                        <td><span>S-'.$sku[1].'</span></td>
                                        <td><span>'.$prodTitle.'</span></td>
										<td><span>'.$quantity[0].'</span></td>  
									</tr>';
					}
				}
        //   }
          $products.='</tbody></table>';
        }
        
        //get location details for skus
        if($skuData) {
            $skuLocationDetails = $this->OrderLocation->find('all',array(
                'conditions' => array(
                    'OrderLocation.sku' => $skuData,
                    'OrderLocation.split_order_id' => $splitid,
                    'OrderLocation.status'=>'active'
                ),
                'fields' => array('OrderLocation.sku','OrderLocation.barcode','OrderLocation.bin_location','OrderLocation.quantity','OrderLocation.po_name'),
            ));
        }
        
        $locSelect  = '';
        $locSelect  = '<select style="width: 200px;" name="binloc[]" class="requiredfield form-control">';
                        if($locations) {
                            $locSelect .= '<option value="" class="required">Select a location</option>';
                            foreach($locations as $locs) {
                                $locSelect .= '<option value="'.$locs['BinLocation']['bin_location'].'" >'.$locs['BinLocation']['bin_location'].'</option>';
                            } 
                        }
        $locSelect .= '</select>';

        if($skuLocationDetails && $isMovedToLocation==0) {
            foreach($skuLocationDetails as $skud) {
                $nolocation = 0; 
                $skuLocationlist .= '<tr><td><span>'.$skud['OrderLocation']['sku'].'</span><input name="prodsku[]" type="hidden" value='.$skud['OrderLocation']['sku'].' /><input name="prodbarcode[]" type="hidden" value='.$skud['OrderLocation']['barcode'].' /></td>';
                if(strtolower($skud['OrderLocation']['bin_location'])=='no location') {
                    $nolocation = 1;
                    $skuLocationlist .= '<td>'.$locSelect.'<input name="nolocation[]" type="hidden" value="'.$nolocation.'" /></td>';
                } else {
                    $skuLocationlist .= '<td><span>'.$skud['OrderLocation']['bin_location'].'</span><input name="nolocation[]" type="hidden" value="'.$nolocation.'" /><input name="binloc[]" type="hidden" value="'.$skud['OrderLocation']['bin_location'].'" /></td>';    
                }
                $skuLocationlist .= '<td><span>'.$skud['OrderLocation']['quantity'].'</span><input name="poname[]" type="hidden" value="'.$skud['OrderLocation']['po_name'].'" /><input name="quantity[]" type="hidden" value='.$skud['OrderLocation']['quantity'].' /></td></tr>'; 
            } 
        }

        echo json_encode(array( 
			'skuslist'=>$products, 
			'address1'=>$address1,
			'address2'=>$address2,
			'address3'=>$address3,
			'town'=>$town,
			'region'=>$region,
			'postcode'=>$postcode,
			'country'=>$country,
			'fullname'=>$fullName,
			'company'=>$company,
			'phonenumber'=>$phoneNumber,
			'source'=>$source,
			'subSource'=>$subSource,
			'orderdate'=>$orderDate,
			'servicename'=>$serviceName,
            'totalcharge'=>$totalCharge,
            'warehouseitemcondition'=>$warehouseItemCondition,
            'warehousereturncomments'=>$warehouseReturnComments,
            'skuLocationlist'=>$skuLocationlist,
            'ismovedtolocation'=>$isMovedToLocation,
            'movecheckbox' => '<div class="form-group"><label class="col-sm-2" for="Comments Warehouse">Want to move item to inventory for selling? </label><div class="col-sm-8"> <input type="checkbox" name="islocation" id="movetolocation" class="checkbox" value="1"></div></div>'
		));
        exit;
    }
    
    public function confirmGrading() {
        $this->loadModel('MergeUpdate');
        $this->loadModel('OpenOrder');
        $this->loadModel('BinLocation');
        $this->loadModel('Product');
        $this->loadModel('InventoryRecord');
        $this->loadModel('CheckIn');

        $orderId = $this->request->data['order_id']; 
        $splitId = $this->request->data['split_id'];
        $noLocation = $this->request->data['nolocation'];
        $itemCondition = $this->request->data['itemcondition_warehouse'];
        $comments = $this->request->data['comments_warehouse'];
        $isLocation = isset($this->request->data['islocation'])?$this->request->data['islocation']:0;
        $prodSku = isset($this->request->data['prodsku'])?$this->request->data['prodsku']:'';
        $prodBarcode = isset($this->request->data['prodbarcode'])?$this->request->data['prodbarcode']:'';
        $poName = isset($this->request->data['poname'])?$this->request->data['poname']:'';
        $binLoc = isset($this->request->data['binloc'])?$this->request->data['binloc']:''; 
        $quantity = isset($this->request->data['quantity'])?$this->request->data['quantity']:'';
        $userId = $this->Session->read('Auth.User.id');
        $returnByUser=$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
        $prodStock = array();
        $checkInData = array();

        $updateGrading = $this->MergeUpdate->updateAll(
            array(  
            'MergeUpdate.warehouse_return_item_condition'=>"'".$itemCondition."'",
            'MergeUpdate.warehouse_return_comments'=>"'".$comments."'",
            'MergeUpdate.warehouse_return_by_user_id'=>"'".$userId."'",
            'MergeUpdate.warehouse_return_date'=>"'".date('Y-m-d H:i:s')."'",
            'MergeUpdate.warehouse_return_by_user'=>"'".$returnByUser."'",
            'MergeUpdate.is_returned_moved'=>1
            ),
            array('MergeUpdate.product_order_id_identify'=>$splitId)
        );


        if($updateGrading) { 
            
            //update bin_location
            if($isLocation==1 && $prodBarcode) {  
              foreach($prodBarcode as $key=>$val) {

                //check if location stock already exists
                $checkLocation = $this->BinLocation->find('first',array(
                       'conditions' => array(
                         'BinLocation.bin_location'=>$binLoc[$key],
                         'BinLocation.barcode'=>$val,
                       ),
                       'fields' => array('BinLocation.id')
                    )
                );
                

                if(count($checkLocation)>0) {
                   //update location stock
                   $binLocationUpdated = $this->BinLocation->updateAll(
                    array(   
                        "BinLocation.stock_by_location"=>"BinLocation.stock_by_location + $quantity[$key]"
                    ),
                    array(
                        'BinLocation.barcode'=>$val,
                        'BinLocation.bin_location'=>$binLoc[$key]
                    )
                   );
                } else {
                    //insert location stock 
                    $locationRecord = array();
                    $locationRecord['BinLocation']['barcode'] = $val;
                    $locationRecord['BinLocation']['bin_location'] = $binLoc[$key];
                    $locationRecord['BinLocation']['stock_by_location'] = $quantity[$key];
                    $locationRecord['BinLocation']['priority'] = 1;
                    $locationRecord['BinLocation']['timestamp'] = date('Y-m-d H:i:s');
                    $this->BinLocation->create();
                    $this->BinLocation->save($locationRecord); 
                }
                
                //update check in only if there was location for quantity deduction when order was initially generated
                if($noLocation[$key] == 0) {
                    $checkInData = $this->CheckIn->find('first', array(
                        'conditions' => array( 
                            'CheckIn.bin_location'=>$binLoc[$key],
                            'CheckIn.barcode'=>$val,
                            'CheckIn.po_name'=>$poName[$key]
                        ),
                        'fields' => array('CheckIn.selling_qty')
                    ));
                }
                
                if(isset($checkInData['CheckIn']['selling_qty'])) {
                    $cqty=1;
                    while($cqty<=$quantity[$key]) {
                        if(($checkInData['CheckIn']['selling_qty']-1)>=0) {
                            $this->CheckIn->updateAll(
                                array(
                                    "CheckIn.selling_qty" => "CheckIn.selling_qty - 1"
                                ),
                                array( 
                                    'CheckIn.bin_location'=>$binLoc[$key],
                                    'CheckIn.barcode'=>$val,
                                    'CheckIn.po_name'=>$poName[$key]
                                )
                            );
                        }
                        $cqty++;
                    }  
                }

                $prodStock[$prodSku[$key]]['barcode'] = $val;
                $prodStock[$prodSku[$key]]['quantity'][] = $quantity[$key]; 
              } 
            }

            //update inventory_records
            if($prodStock) {   
                foreach($prodStock as $key=>$stock) {
                       
                    //get current stock level for the product
                    $prodDetails = $this->Product->find('first',array(
                        'conditions' => array(
                            'Product.product_sku' => $key, 
                        ),
                        'fields' => array('Product.current_stock_level')
                    ));
                    
                    $totalQty = array_sum($stock['quantity']); 
                    $currentStock = $prodDetails['Product']['current_stock_level'];
                    $newCurrentStock = (int)$currentStock + (int)$totalQty;
                    //prepare inventory record array
                    $inventoryRecord=array();
                    $inventoryRecord['InventoryRecord']['action_type']  = $orderId.'-Order Returned -increase inventory by-'.$totalQty;
                    $inventoryRecord['InventoryRecord']['sku']          = $key;
                    $inventoryRecord['InventoryRecord']['barcode']      = $stock['barcode'];
                    $inventoryRecord['InventoryRecord']['currentStock'] = $currentStock;
                    $inventoryRecord['InventoryRecord']['quantity']     = $totalQty;
                    $inventoryRecord['InventoryRecord']['after_maniplation'] = $newCurrentStock;
                    $inventoryRecord['InventoryRecord']['split_order_id'] = $orderId;
                    $inventoryRecord['InventoryRecord']['status'] = 'Returned Order';
                    $inventoryRecord['InventoryRecord']['date'] = date('Y-m-d H:i:s');
                    
                    //update product table current_stock_level
                    $stockLevelUpdated = $this->Product->updateAll(
                        array(   
                            "Product.current_stock_level"=>$newCurrentStock
                        ),
                        array(
                            'Product.product_sku'=>$key 
                        )
                    );

                    //insert in inventory records table
                    $this->InventoryRecord->create();
                    $this->InventoryRecord->save($inventoryRecord); 
            } 
        }

            $this->Session->setFlash('Grading updated successfully!', 'flash_success');
            echo json_encode(array('status'=>'1','msg'=>'Grading updated successfully!'));
            exit;
        } else {
            $this->Session->setFlash('Unable to grade order!', 'flash_error');
            echo json_encode(array('status'=>'0','msg'=>'Unable to grade order!'));
            exit; 
        }
    }

    public function getRefunds() {
        $refundOptions = Configure::read('refundoptions');
        $orderId = $this->request->data['orderid'];
        $mainOrderId = explode('-',$orderId); 
        $currency = $this->request->data['currency']; 
        $this->loadModel('Refund');
        $refundList = '';

        $orderRefundDetails = $this->Refund->find('all', array(
            'conditions' => array('Refund.order_id LIKE'=> $mainOrderId[0].'%'), 
            'fields' => array('Refund.refund_amount','Refund.refund_option','Refund.refund_by_user','Refund.reason'),
            'order' => 'Refund.id DESC'
        ));
        $showFullAmountOrderFlag = 1;
        if($orderRefundDetails) {
        
            $refundList = '<table class="table">
                <thead>
                <tr> 
                  <th>Refund Title</th>
                  <th>Refund Amount</th>
                  <th>Refund By</th>
                  <th>Refund Reason</th>
                </tr>
                </thead>
                <tbody id="skulistbody">';

                foreach($orderRefundDetails as $refundDetails) {  
                    $refundList .='<tr> 
                                    <td><span>'.$refundOptions[$refundDetails['Refund']['refund_option']].'</span></td>
                                    <td><span>'.$refundDetails['Refund']['refund_amount'].' '.$currency.'</span></td>
                                    <td><span>'.$refundDetails['Refund']['refund_by_user'].'</span></td>  
                                    <td><span>'.$refundDetails['Refund']['reason'].'</span></td>
                                </tr>';
                    if($refundDetails['Refund']['refund_option'] == 'fullamountoforder') {
                       $showFullAmountOrderFlag = 0;
                    }
                }
 
          $refundList.='</tbody></table>';

        }
        echo json_encode(array('status'=>'1','refunds'=>$refundList,'showfullamountorderflag'=>$showFullAmountOrderFlag));
        exit; 
    } 

    public function showResendRefundReport() {
        ini_set('max_execution_time', 0);
        $this->layout = 'index'; 
	 
        if(isset($_POST['rangestart'])) {
    
            $this->loadModel( 'MergeUpdate' ); 
			$this->loadModel( 'MergeUpdatesArchive' ); 
			$this->loadModel( 'DynamicPicklist'); 
			$this->loadModel( 'User');
                  
            $getFrom 	= date('Y-m-d' ,strtotime($this->request->data['rangestart']));
            $getEnd 	= date('Y-m-d' ,strtotime($this->request->data['rangeend']));
            $option     = $this->request->data['option']; 

            $emails = array('jake.shaw@euracogroup.co.uk','lalit.prasad@jijgroup.com','shashi.b.kumar@jijgroup.com','avadhesh.kumar@jijgroup.com','anna.kedziora@onet.eu','aakash.kushwah@jijgroup.com','pappu.k@euracogroup.co.uk','neha@euracogroup.co.uk');
            $typeCondtion = '';        
            $conditions = array();
            if(in_array($this->Session->read('Auth.User.email'),$emails)) { 
               
                if($option == 'refund') {
                    $conditions =  array(  
                        'MergeUpdate.is_refund'=>1, 
                        'date(Refund.created_at) >=' => $getFrom,
                        'date(Refund.created_at) <=' => $getEnd 
                    ); 
					
 					 $getRefundResendReport = $this->MergeUpdate->find('all', array(
						'joins' => array(
							array(
								'table' => 'open_orders',
								'alias' => 'OpenOrder',
								'type' => 'INNER',
								'conditions' => array(
									'MergeUpdate.order_id = OpenOrder.num_order_id'
								)
							),
							array(
								'table' => 'refunds',
								'alias' =>'Refund',
								'type' => 'INNER',
								'conditions' => array(
									'MergeUpdate.product_order_id_identify = Refund.order_id' 
								),
								'order' => array('Refund.id' => 'desc')
							) 
						),
						'conditions' => $conditions,
						'fields' => array('OpenOrder.totals_info','OpenOrder.sub_source', 'MergeUpdate.*','Refund.*'),
						'order' => 'MergeUpdate.process_date DESC',
					));
				   //sheet setup
					App::import('Vendor', 'PHPExcel/IOFactory');
					App::import('Vendor', 'PHPExcel');
					$objPHPExcel = new PHPExcel(); 
					$objPHPExcel->setActiveSheetIndex(0);
					$inc = 2;		 
					if( count( $getRefundResendReport ) > 0 )		
					 {  
  							foreach( $getRefundResendReport as $refundResendRow )
							{ 
								$picked_user = $refundResendRow['MergeUpdate']['picklist_username'] ;
 								
								$dp = $this->DynamicPicklist->find('first', array('conditions' => array('id'=> $refundResendRow['MergeUpdate']['picklist_inc_id']),'fields' => array('assign_user_email')));
								
								if(count($dp) > 0){
									 $user = $this->User->find('first', array('conditions' => array('email'=> $dp['DynamicPicklist']['assign_user_email']),'fields' => array('first_name','last_name')));
									 if(count($user) > 0){
										$picked_user =  $user['User']['first_name'] .' '. $user['User']['last_name'];
									 }
								}
		
			//changes
								$skuList = array();
								$skus = explode(',',$refundResendRow['MergeUpdate']['sku']);
								foreach($skus as $key=>$value) {
									$sku       = explode('S-',$value); 
									$skuList[] = 'S-'.$sku[1];
								}
								$skuList = implode(',',$skuList);
			
								$totalDetails = unserialize($refundResendRow['OpenOrder']['totals_info']); 
								if($refundResendRow['MergeUpdate']['is_refund']==1) { 
								
								
								
									$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $refundResendRow['MergeUpdate']['product_order_id_identify']);
									$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $refundResendRow['MergeUpdate']['order_date']);
									$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $refundResendRow['Refund']['created_at']); 
									$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $totalDetails->Currency);
									$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $refundResendRow['Refund']['refund_amount_converted']);
									$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $Refundoptions[$refundResendRow['Refund']['refund_option']]);
									$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $refundResendRow['Refund']['reason']); 
 									$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $refundResendRow['OpenOrder']['sub_source']); 
									$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $skuList); 
									$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, $refundResendRow['Refund']['refund_by_user']); 
									$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, $refundResendRow['MergeUpdate']['assign_user']); 
									$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, $refundResendRow['MergeUpdate']['scanned_user']); 
									$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, $picked_user); 
									$inc++;
								}
								 
							} 
 					 }
					  
					  
                    $conditions =  array(  
                        'MergeUpdatesArchive.is_refund'=>1, 
                        'date(Refund.created_at) >=' => $getFrom,
                        'date(Refund.created_at) <=' => $getEnd 
                    ); 
					
 					 $getRefundResendReportArchive = $this->MergeUpdatesArchive->find('all', array(
						'joins' => array(
							array(
								'table' => 'open_orders_archives',
								'alias' => 'OpenOrdersArchive',
								'type' => 'INNER',
								'conditions' => array(
									'MergeUpdatesArchive.order_id = OpenOrdersArchive.num_order_id'
								)
							),
							array(
								'table' => 'refunds',
								'alias' =>'Refund',
								'type' => 'INNER',
								'conditions' => array(
									'MergeUpdatesArchive.product_order_id_identify = Refund.order_id' 
								),
								'order' => array('Refund.id' => 'desc')
							) 
						),
						'conditions' => $conditions,
						'fields' => array('OpenOrdersArchive.totals_info','OpenOrdersArchive.sub_source', 'MergeUpdatesArchive.*','Refund.*'),
						'order' => 'MergeUpdatesArchive.process_date DESC',
					));
					 
					if( count( $getRefundResendReportArchive ) > 0 )		
					{ 
					 
						$Refundoptions = Configure::read('refundoptions'); 
					
						 
						foreach( $getRefundResendReportArchive as $refundResendRow )
						{ 
							$picked_user = $refundResendRow['MergeUpdatesArchive']['picklist_username'] ;
							
							$dp = $this->DynamicPicklist->find('first', array('conditions' => array('id'=> $refundResendRow['MergeUpdatesArchive']['picklist_inc_id']),'fields' => array('assign_user_email')));
							
							if(count($dp) > 0){
								 $user = $this->User->find('first', array('conditions' => array('email'=> $dp['DynamicPicklist']['assign_user_email']),'fields' => array('first_name','last_name')));
								 if(count($user) > 0){
									$picked_user =  $user['User']['first_name'] .' '. $user['User']['last_name'];
								 }
							}
					
						   //changes
							$skuList = array();
							$skus = explode(',',$refundResendRow['MergeUpdatesArchive']['sku']);
							foreach($skus as $key=>$value) {
								$sku       = explode('S-',$value); 
								$skuList[] = 'S-'.$sku[1];
							}
							$skuList = implode(',',$skuList);
					
							$totalDetails = unserialize($refundResendRow['OpenOrdersArchive']['totals_info']); 
							if($refundResendRow['MergeUpdatesArchive']['is_refund']==1) { 
							
  								$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $refundResendRow['MergeUpdatesArchive']['product_order_id_identify']);
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $refundResendRow['MergeUpdatesArchive']['order_date']);
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $refundResendRow['Refund']['created_at']); 
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $totalDetails->Currency);
								$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $refundResendRow['Refund']['refund_amount_converted']);
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $Refundoptions[$refundResendRow['Refund']['refund_option']]);
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $refundResendRow['Refund']['reason']); 
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $refundResendRow['OpenOrdersArchive']['sub_source']); 
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $skuList); 
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, $refundResendRow['Refund']['refund_by_user']); 
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, $refundResendRow['MergeUpdatesArchive']['assign_user']); 
								$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, $refundResendRow['MergeUpdatesArchive']['scanned_user']); 
								$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, $picked_user); 
								$inc++;
							}
							 
						} 
						
					}
							
					if( count( $getRefundResendReport ) > 0 || count( $getRefundResendReportArchive ) > 0 )		
					{ 
						$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderItemNumber');
						$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Order Date');
						$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Action Date'); 
						$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Amount');
						$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Currency'); 
						$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Refund Option');
						$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Reason'); 
						$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Sub Source');
						$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Skus');
						$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Refund BY');
						$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Processed BY');
						$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Scaned BY');
						$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Picked BY');
						$Refundoptions = Configure::read('refundoptions'); 
						
						ob_start();
						ob_clean();
						//report setup
						$name = 'Refunds_'.date('d_m_Y_H_i_s');
						header('Content-Encoding: UTF-8');
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
						header('Content-Disposition: attachment;filename="'.$name.'.xlsx"');
						header('Pragma: no-cache');
						header('Cache-Control: no-store, no-cache, must-revalidate'); 
						header('Cache-Control: post-check=0, pre-check=0');
						$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');            
						$objWriter->save('php://output');
						exit;
					}
					else {
						$this->Session->setFlash('No refund record found!', 'flash_danger');
					}
                
  					   
                }else if($option == 'resend') {
                    $conditions =  array(  
                        'MergeUpdate.is_resend'=>1,                         
                        'date(MergeUpdate.resend_date) >=' => $getFrom,
                        'date(MergeUpdate.resend_date) <=' => $getEnd,
                    ); 
					 
					 $getRefundResendReport = $this->MergeUpdate->find('all', array(
						'joins' => array(
							array(
								'table' => 'open_orders',
								'alias' => 'OpenOrder',
								'type' => 'INNER',
								'conditions' => array(
									'MergeUpdate.order_id = OpenOrder.num_order_id'
								)
							)
							
						),
						'conditions' => $conditions,
						'fields' => array('OpenOrder.totals_info','OpenOrder.sub_source', 'MergeUpdate.*'),
						'order' => 'MergeUpdate.process_date DESC',
					));
			 	
					App::import('Vendor', 'PHPExcel/IOFactory');
					App::import('Vendor', 'PHPExcel');
					$objPHPExcel = new PHPExcel(); 
					$objPHPExcel->setActiveSheetIndex(0);
					$inc = 2;
					if( count( $getRefundResendReport ) > 0 )		
					{ 
  						
						foreach( $getRefundResendReport as $refundResendRow )
						{ 
							$picked_user = $refundResendRow['MergeUpdate']['picklist_username'] ;
 								
							$dp = $this->DynamicPicklist->find('first', array('conditions' => array('id'=> $refundResendRow['MergeUpdate']['picklist_inc_id']),'fields' => array('assign_user_email')));
							
							if(count($dp) > 0){
								 $user = $this->User->find('first', array('conditions' => array('email'=> $dp['DynamicPicklist']['assign_user_email']),'fields' => array('first_name','last_name')));
								 if(count($user) > 0){
									$picked_user =  $user['User']['first_name'] .' '. $user['User']['last_name'];
								 }
							}
								
								//changes
							$skuList = array();
							$skus = explode(',',$refundResendRow['MergeUpdate']['sku']);
							foreach($skus as $key=>$value) {
								$sku       = explode('S-',$value); 
								$skuList[] = 'S-'.$sku[1];
							}
							$skuList = implode(',',$skuList);
		
							$totalDetails = unserialize($refundResendRow['OpenOrder']['totals_info']); 
 							if($refundResendRow['MergeUpdate']['is_resend']==1 ) {  
							
								$comments = $refundResendRow['MergeUpdate']['resend_comments'];
								
								if($refundResendRow['MergeUpdate']['resend_comments'] == ''){
									$comments = $refundResendRow['MergeUpdate']['comments'];
								} 
 							
								$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $refundResendRow['MergeUpdate']['product_order_id_identify']);
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $refundResendRow['MergeUpdate']['order_date']);
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $refundResendRow['MergeUpdate']['resend_date']);
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $refundResendRow['MergeUpdate']['resend_reason']);
								$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $comments);
 								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $refundResendRow['OpenOrder']['sub_source']);
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $skuList);
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $refundResendRow['MergeUpdate']['resend_by_user'] ); 
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $refundResendRow['MergeUpdate']['assign_user']); 
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, $refundResendRow['MergeUpdate']['scanned_user']);
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, $picked_user);
 								$inc++;
							} 
						} 
 						
					 }
					 
					 	/*------------Archive--------------*/
						 $conditions =  array(  
							'MergeUpdatesArchive.is_resend'=>1,                         
							'date(MergeUpdatesArchive.resend_date) >=' => $getFrom,
							'date(MergeUpdatesArchive.resend_date) <=' => $getEnd,
						); 
						
						 $getRefundResendReportArchive = $this->MergeUpdatesArchive->find('all', array(
							'joins' => array(
								array(
									'table' => 'open_orders_archives',
									'alias' => 'OpenOrdersArchive',
									'type' => 'INNER',
									'conditions' => array(
										'MergeUpdatesArchive.order_id = OpenOrdersArchive.num_order_id'
									)
								)
								
							),
							'conditions' => $conditions,
							'fields' => array('OpenOrdersArchive.totals_info','OpenOrdersArchive.sub_source', 'MergeUpdatesArchive.*'),
							'order' => 'MergeUpdatesArchive.process_date DESC',
						));
 					  
					 if( count( $getRefundResendReportArchive ) > 0 ){
 						 
						 
						foreach( $getRefundResendReportArchive as $refundResendRow )
						{ 
							$picked_user = $refundResendRow['MergeUpdatesArchive']['picklist_username'] ;
 								
							$dp = $this->DynamicPicklist->find('first', array('conditions' => array('id'=> $refundResendRow['MergeUpdatesArchive']['picklist_inc_id']),'fields' => array('assign_user_email')));
							
							if(count($dp) > 0){
								 $user = $this->User->find('first', array('conditions' => array('email'=> $dp['DynamicPicklist']['assign_user_email']),'fields' => array('first_name','last_name')));
								 if(count($user) > 0){
									$picked_user =  $user['User']['first_name'] .' '. $user['User']['last_name'];
								 }
							}
								
								//changes
							$skuList = array();
							$skus = explode(',',$refundResendRow['MergeUpdatesArchive']['sku']);
							foreach($skus as $key=>$value) {
								$sku       = explode('S-',$value); 
								$skuList[] = 'S-'.$sku[1];
							}
							$skuList = implode(',',$skuList);
		
							$totalDetails = unserialize($refundResendRow['OpenOrdersArchive']['totals_info']); 
 							if($refundResendRow['MergeUpdatesArchive']['is_resend']==1 ) {  
							
								$comments = $refundResendRow['MergeUpdatesArchive']['resend_comments'];
								
								if($refundResendRow['MergeUpdatesArchive']['resend_comments'] == ''){
									$comments = $refundResendRow['MergeUpdatesArchive']['comments'];
								} 
						 
							
								$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $refundResendRow['MergeUpdatesArchive']['product_order_id_identify']);
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $refundResendRow['MergeUpdatesArchive']['order_date']);
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $refundResendRow['MergeUpdatesArchive']['resend_date']);
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $refundResendRow['MergeUpdatesArchive']['resend_reason']);
								$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $comments);
 								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $refundResendRow['OpenOrdersArchive']['sub_source']);
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $skuList);
								$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $refundResendRow['MergeUpdatesArchive']['resend_by_user'] ); 
								$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $refundResendRow['MergeUpdatesArchive']['assign_user']); 
								$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, $refundResendRow['MergeUpdatesArchive']['scanned_user']);
								$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, $picked_user);
 								$inc++;
							} 
						}
						
					}
					/*-------End Archive-----------*/
					 if( count( $getRefundResendReport ) > 0  || count( $getRefundResendReportArchive ) > 0){
					
							//sheet setup
 		
						$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderItemNumber');
						$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Order Date');
						$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Action Date');      
						$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Reason'); 
						$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Comments'); 
						$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Sub Source');
						$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Skus');
						$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Resend BY');
						$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Processed BY');
						$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Scaned BY');
						$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Picked BY');
						
						//report setup
						$name = 'Resend_'.date('d_m_Y_H_i_s');
						header('Content-Encoding: UTF-8');
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						// header('Content-type: text/csv; charset=UTF-8');
						// header("Content-type: application/octet-stream");
						header('Content-Disposition: attachment;filename="'.$name.'.xlsx"');
						header('Pragma: no-cache');
						header('Cache-Control: no-store, no-cache, must-revalidate'); 
						header('Cache-Control: post-check=0, pre-check=0');
						$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');            
						$objWriter->save('php://output');
						exit;
					} else {
						 $this->Session->setFlash('No resend record found!', 'flash_danger');
					}
                } 
             } 
			else{
			  $this->Session->setFlash($this->Session->read('Auth.User.email').' is not authorized to download reports!', 'flash_danger'); 
			 }
   		} 
    }

    public function loadFilters() {
        $checkOptions = '<input type="checkbox"  name="checkfilterresend" id="checkfilterresend" />Resend&nbsp;<input type="checkbox" id="checkfilterrefund" name="checkfilterrefund" />Refund';
        echo json_encode(array('checkOptions'=>$checkOptions));
        exit;
    }  

    //changesnewstarts
    public function checkInventory() {
        $this->loadModel('MergeUpdate');
        $this->loadModel('Product');

        $orderId = ''; 
        $type = '';

        if(isset($this->request->data['orderid'])) {
           $orderId = $this->request->data['orderid'];
        }
        if(isset($this->request->data['type'])) {
           $type = $this->request->data['type'];
        }
        $dataItems = array();
        $splitSkus = array();
        $doneSku   = array();
        $stockList = '';
        $warningFlag = 0;

        if(strtolower($type)=='fullorder') {
            //get items from merge update 
            $dataItems = $this->MergeUpdate->find('all', array(
                'conditions' => array( 
                    'MergeUpdate.order_id'=>$orderId, 
                ),
                'fields' => array('MergeUpdate.sku')
            ));
        } else {
            //incase of split order resend
            $skuSplit = isset($this->request->data['sku'])?$this->request->data['sku']:'';
            if($skuSplit) {
                foreach($skuSplit as $skukey=>$skuval) {
                    $doneSku[] = $skuval; 
                }
            } 
        }
 
        //in case of confirm full order resend
        if(count($dataItems)) {
           foreach($dataItems as $items) {
            $skuSplit  = explode( ',' , $items['MergeUpdate']['sku']);
            foreach($skuSplit as $key=>$value) {
                $properSku = explode( 'XS-' , $value );
                $doneSku[] = 'S-'.$properSku[1];
            }    
           }
        }
 
        //check product available stock
        $stockDetails = $this->Product->find('all', array(
                'conditions' => array( 
                    array('Product.product_sku'=>$doneSku), 
                ),
                'fields' => array('Product.current_stock_level','Product.product_sku')
            )
        );
 
        if(count($stockDetails) > 0 ) {
            $stockList = '<table class="table">
            <thead>
            <tr> 
              <th>Sku</th>
              <th>Current Stock</th> 
            </tr>
            </thead>
            <tbody id="skulistbody">';
           foreach($stockDetails as $stock) { 
                if($stock['Product']['current_stock_level']<=5) {
                   $warningFlag=1;
                }
                $stockLevel = ($stock['Product']['current_stock_level']!='')?$stock['Product']['current_stock_level']:'0';
                $stockList .='<tr> 
                                <td><span>'.$stock['Product']['product_sku'].'</span></td>
                                <td><span>'.$stockLevel.'</span></td> 
                            </tr>';  
           }
           $stockList.='</tbody></table>';
        }

        if($warningFlag==1) { 
            echo json_encode(array('status'=>'1','msg'=>$stockList));
            exit; 
        } else {
            echo json_encode(array('status'=>'0','msg'=>true));
            exit;
        }
    }

    // edit address code starts 
    public function getAddressDetails() {
        
        $this->loadModel('OpenOrder');

        $orderId = $this->request->data['orderid'];
        $openOrderDetails = $this->OpenOrder->find('first',array(
            'conditions' => array(
                'OpenOrder.num_order_id' => $orderId
            ),
            'fields' => array('OpenOrder.customer_info'),
        ));

        $customerInfo = unserialize($openOrderDetails['OpenOrder']['customer_info']);
        $address1 = $customerInfo->Address->Address1;
        $address2 = $customerInfo->Address->Address2;
        $address3 = $customerInfo->Address->Address3;
        $town = $customerInfo->Address->Town;
        $region = $customerInfo->Address->Region;
        $postcode =$customerInfo->Address->PostCode;
        $country =$customerInfo->Address->Country;  


        echo json_encode(array( 
            'address1'=>$address1,
            'address2'=>$address2,
            'address3'=>$address3,
            'town'=>$town,
            'region'=>$region,
            'postcode'=>$postcode,
            'country'=>$country, 
        )); 
        exit;     
    }

    function updateAddress() {
        $this->loadModel('OpenOrder');
        $orderId  = $this->request->data['address_order_id'];
        $address1 = $this->request->data['address1'];
        $address2 = $this->request->data['address2'];
        $address3 = $this->request->data['address3'];
        $town     = $this->request->data['town'];
        $region   = $this->request->data['region'];
        $postcode = $this->request->data['postcode'];
        $country  = $this->request->data['country'];

        $data = array();
        $data = $this->OpenOrder->find('first',array(
            'conditions' => array(
                'OpenOrder.num_order_id' => $orderId
            ),
            'fields' => array('OpenOrder.customer_info'),
        ));

        $customerDetails = unserialize($data['OpenOrder']['customer_info']);
        $customerDetails->Address->Address1 = $address1;
        $customerDetails->Address->Address2 = $address2;
        $customerDetails->Address->Address3 = $address3;
        $customerDetails->Address->Town = $town;
        $customerDetails->Address->Region = $region;
        $customerDetails->Address->PostCode = $postcode;
        $customerDetails->Address->Country = $country;

        $updateAddress = $this->OpenOrder->updateAll( 
            array( 
                'OpenOrder.customer_info' => "'".serialize($customerDetails)."'", 
            ), 
            array( 'OpenOrder.num_order_id' => $orderId)
        );

        //update open order table
        if($updateAddress) { 
            echo json_encode(array('msg'=>'Addrss updated successfully!','status'=>1)); 
        } else {
            echo json_encode(array('msg'=>'Unable to update address!','status'=>0));
        }
        exit;
    }
    // edit address code ends 
}