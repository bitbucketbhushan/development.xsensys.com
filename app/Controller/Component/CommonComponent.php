<?php
	
	class CommonComponent extends Component
	{	
		
		function startup (Controller $controller)
                {
            
		}
        
        /*
         * Function : GetCountry List
         * Version : 1.0
         * Company : JijGroup US - UK - India - Europe
         * Parameters : @Null
         * Result List Array
         * 
         */
        public function getCountryList()
        {
            
            /* Start here set the country list */            
            App::import( "Model","Location" );
            $location = new Location();            
            $options = array(				
				'fields' => array('Location.county_name')
			);
            
        return $getLocationArray = $location->find( 'list', $options );
        }

        public function getStateList()
        {
             /* Start here set the country list */            
            App::import( "Model","State" );
            $state = new State();            
            $options = array(				
				'fields' => array('State.id','State.state_name','State.is_deleted')
			);
            $getStateAllWithStatus = $state->find( 'all', $options );			
            $newStateList = array();
            if( $state->find( 'count', $options ) > 0 )
            {				
                foreach( $getStateAllWithStatus as $index => $value )
                {					
                    if( $value["State"]["is_deleted"] == "1" )
                    {
                            $newStateList[$value["State"]["id"]] = $value["State"]["state_name"]." (Under Deleted)";						
                    }
                    else
                    {
                            $newStateList[$value["State"]["id"]] = $value["State"]["state_name"];						
                    }
                }
            }
        return $getCityList = $newStateList;	
        }
        
        public function getCityList()
        {
            
            /* Start here set the country list */            
            App::import( "Model","City" );
            $city = new City();            
            $options = array('fields' => array('City.id','City.city_name','City.is_deleted'));
            $getCityAllWithStatus = $city->find( 'all', $options );			
            $newCityList = array();
            if( $city->find( 'count', $options ) > 0 )
            {				
                foreach( $getCityAllWithStatus as $index => $value )
                {					
                    if( $value["City"]["is_deleted"] == "1" )
                    {
                            $newCityList[$value["City"]["id"]] = $value["City"]["city_name"]." (Under Deleted)";						
                    }
                    else
                    {
                            $newCityList[$value["City"]["id"]] = $value["City"]["city_name"];						
                    }
                }
            }
        return $getCityList = $newCityList;
        }
        
        public function getWarehouseList()
		{
			
			/* Load Model of warehouses and setup the list */
			App::import( 'Model', 'Warehouse' );
			$warehouse = new Warehouse();			
			$getWarehouseAllWithStatus = $warehouse->find( 'all' );
			
			$newWarehouseList = array();
			if( $warehouse->find( 'count' ) > 0 )
			{				
				foreach( $getWarehouseAllWithStatus as $index => $value )
				{					
					if( $value["WarehouseDesc"]["is_deleted"] == "1" )
					{
						$newWarehouseList[$value["Warehouse"]["id"]] = $value["Warehouse"]["warehouse_name"]." (Under Deleted)";						
					}
					else
					{
						$newWarehouseList[$value["Warehouse"]["id"]] = $value["Warehouse"]["warehouse_name"];						
					}
				}
			}
		return $getWarehouseList = $newWarehouseList;	
		}




public function getCategoryList()
        {
            
            /* Start here set the country list */            
            App::import( "Model","Category" );
            $category = new Category();            
            $options = array('fields' => array('Category.id','Category.category_name','Category.parent_id', 'Category.is_deleted', 'Category.status', 'Category.is_blocked'),
					'conditions' => array('Category.is_deleted' => '0'));
					
            $getCategoryAllWithStatus = $category->find( 'all', $options );			
            $newcategoryList = array();
            if( $category->find( 'count', $options ) > 0 )
            {				
                foreach( $getCategoryAllWithStatus as $index => $value )
                {					
                    if( $value["Category"]["status"] == "1" )
                    {
                            $newCategoryList[$value["Category"]["id"]] = $value["Category"]["category_name"]." (Deactive)";						
                    }
                    else
                    {
                            $newCategoryList[$value["Category"]["id"]] = $value["Category"]["category_name"];						
                    }
                }
                return $getCategoryList = $newCategoryList;
            }
			
        }


	public function getRoleList()
        {
            /* Start here set the role list */            
            App::import( "Model","Role" );
            $role = new Role();            
            return $getRolesList = $role->find( 'list',array('fields' => array('Role.id', 'Role.role_name')));			
        }
        
        public function getProviderList()
        {
            /* Start here set the role list */            
            App::import( "Model","PostalProvider" );
            $PostalProvider = new PostalProvider();            
            return $getproviderList = $PostalProvider->find( 'list',array('fields' => array('PostalProvider.id', 'PostalProvider.provider_name')));			
        }

	
		public function getPaginate($item_per_page = NULL, $current_page = NULL, $total_records = NULL, $total_pages = NULL, $data_name = false, $data_value=false, $sort_by=false,$url = NULL )
		{
			$pagination = '';
			if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
				$pagination .= '<ul class="pagination">';
				
				$right_links    = $current_page + 5; 
				$previous       = $current_page - 5; //previous link 
				$next           = $current_page + 1; //next link
				$first_link     = true; //boolean var to decide our first link
				
				if($current_page > 1){
					$previous_link = ($previous > 0)? $previous : 1;
					$pagination .= '<li class="first"><a href="'.$url.'1" data-page="1" data-name="'.$data_name.'" order-by="'.$sort_by.'" data-value="'.$data_value.'" title="First">&laquo;</a></li>'; //first link
					$pagination .= '<li><a href="'.$url.$previous_link.'" data-page="'.$previous_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Previous">&lt;</a></li>'; //previous link
						for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
							if($i > 0){
								$pagination .= '<li><a href="'.$url.$i.'" data-page="'.$i.'"  data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Page'.$i.'">'.$i.'</a></li>';
							}
						}   
					$first_link = false; //set first link to false
				}
				
				if($first_link){ //if current active page is first link
					$pagination .= '<li class="first active"><span>'.$current_page.'</span></li>';
				}elseif($current_page == $total_pages){ //if it's the last active link
					$pagination .= '<li class="last active"><span>'.$current_page.'</span></li>';
				}else{ //regular current link
					$pagination .= '<li class="active"><span>'.$current_page.'</span></li>';
				}
						
				for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
					if($i<=$total_pages){
						$pagination .= '<li><a href="'.$url.$i.'" data-page="'.$i.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Page '.$i.'">'.$i.'</a></li>';
					}
				}
				if($current_page < $total_pages){ 
						$next_link = ($i > $total_pages) ? $total_pages : $i;
						$pagination .= '<li><a href="'.$url.$next_link.'" data-page="'.$next_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'" data-value="'.$data_value.'" title="Next">&gt;</a></li>'; //next link
						$pagination .= '<li class="last"><a href="'.$url.$next_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
				}
				
				$pagination .= '</ul>'; 
			}
			return $pagination; //return pagination links
		}
		/*Apply code for Global Barcode*/
		public function getGlobalBarcode($barcode)
        {
			 App::import( "Model","ProductBarcode" );
             $ProductBarcode = new ProductBarcode(); 
			 $global_barcode = '';     
			 $all_barcode = $this->checkZero($barcode);      
             $list = $ProductBarcode->find('first', array('conditions' => array('barcode'=> $all_barcode),  'fields' => array( 'global_barcode') ) );
			 if(count($list) > 0){
				 $global_barcode = $list['ProductBarcode']['global_barcode'];
			 } 
			 return  $global_barcode ;
         }
		 
		 public function checkZero($barcode = null){ 
		
		//$barcode = '0000254523687';
 		
		if(strlen($barcode) < 13){
		  	$zeros = '';
			$short_len = 13 - strlen($barcode);
			for($i = 0; $i < $short_len; $i++){
				  $zeros .= '0';
 			}
			$barcode = $zeros.$barcode;
		 }
		 $z = [];
		 for($i = 0; $i < strlen($barcode); $i++){
			 if($barcode[$i] == 0){
				 $z[] = $i;
			 }else{			 	
				 break;
			 }
 		 }
		 
		 $str = substr($barcode,$i,strlen($barcode));
		 if(strlen($str) > 0){
 			 $sku_combinations[] = $str;
			 for($i = 0; $i < count($z); $i++){
				$t = '';
				for($j = 0; $j <= $i; $j++){
					$t .='0';
				}
				$t .= $str;			
				$sku_combinations[] = $t;
			  }
		  }else{
		 	 $sku_combinations[] = $barcode;
		  }
		 return $sku_combinations;
	}
		 /*Apply code for Global Barcode*/
		public function getLocalBarcode($global_barcode)
        {
			 App::import( "Model","ProductBarcode" );
             $ProductBarcode = new ProductBarcode(); 
			 $barcode = '';           
             $list = $ProductBarcode->find('first', array('conditions' => array('global_barcode'=> $global_barcode),  'fields' => array( 'barcode'), 'order' => 'id DESC' ) );
			 if(count($list) > 0){
				 $barcode = $list['ProductBarcode']['barcode'];
			 } 
			 return  $barcode ;
         }
		 
		 
		 /*Apply code for Global Barcode*/
		public function getAllBarcode($global_barcode)
        {
           App::import( "Model","ProductBarcode" );
           $ProductBarcode = new ProductBarcode();  
		   $barcodeArray = array();      
           $list = $ProductBarcode->find('all', array('conditions' => array('global_barcode'=> $global_barcode), 'fields' => array('barcode'), 'order' => 'id DESC' ) );
		   foreach($list as $val){
		 	  $barcodeArray[] = $val['ProductBarcode']['barcode'];
		   }
		   return $barcodeArray;
         }
		 
		/*----------25 AUG 2021-----*/
		public function getInvoiceId($company = '')
        {
			App::import( "Model","InvoiceCreditnoteNo" );
			$invModel = new InvoiceCreditnoteNo();  
			$new_invoice_no = '';      
			$list = $invModel->find('first', array('conditions' => array('company'=>strtoupper($company)), 'fields' => array('id','invoice_start_id','invoice_id') ) );
			if(count($list) > 0){
				$id = $list['InvoiceCreditnoteNo']['id'];
 				if(empty($list['InvoiceCreditnoteNo']['invoice_id'])){
					$new_invoice_no = $list['InvoiceCreditnoteNo']['invoice_start_id'] + 1;
				}else{
					$new_invoice_no = $list['InvoiceCreditnoteNo']['invoice_id'] + 1;
 				}
				$invModel->query("UPDATE `invoice_creditnote_nos` SET `invoice_id` = '".$new_invoice_no."' WHERE `id` = {$id}"); 
			}
			
			return $new_invoice_no;
         }
		 /*----------25 AUG 2021-----*/
		public function getCreditNoteId($company = '')
        {
			App::import( "Model","InvoiceCreditnoteNo" );
			$invModel = new InvoiceCreditnoteNo();  
			$credit_note_id = '';      
			$list = $invModel->find('first', array('conditions' => array('company'=> strtoupper($company)), 'fields' => array('id','credit_note_start_id','credit_note_id') ) );
			if(count($list) > 0){
				$id = $list['InvoiceCreditnoteNo']['id'];
 				if(empty($list['InvoiceCreditnoteNo']['credit_note_id'])){
					$credit_note_id = $list['InvoiceCreditnoteNo']['credit_note_start_id'];
				}else{
					$credit_note_id = $list['InvoiceCreditnoteNo']['credit_note_id'];
 				}
				$credit_note_id = $credit_note_id + 1;
 				$invModel->query("UPDATE `invoice_creditnote_nos` SET `credit_note_id` = '".$credit_note_id."' WHERE `id` = {$id}"); 
 			}
			
			return $credit_note_id;
         }
  		 
		 /*get code for SubSource*/
		public function getSubSource($seller_sku)
        {
			$SubSource = '';
			if(strpos($seller_sku,"FREURAFBA") !== false){
				$SubSource= 'CostBreaker_FRFBA';
			}else if(strpos($seller_sku,"ESEURAFBA") !== false){
				$SubSource= 'CostBreaker_ESFBA';
			}else if(strpos($seller_sku,"DEEURAFBA") !== false){
				$SubSource= 'CostBreaker_DEFBA';
			}else if(strpos($seller_sku,"ITEURAFBA") !== false){
				$SubSource= 'CostBreaker_ITFBA';
			}else if(strpos($seller_sku,"UKEURAFBA") !== false){
				$SubSource= 'CostBreaker_UKFBA';
			}
			else if(strpos($seller_sku,"MRDEFBA") !== false){
				$SubSource= 'Marec_DEFBA';
			}else if(strpos($seller_sku,"MRUKFBA") !== false){
				$SubSource= 'Marec_UKFBA';
			}else if(strpos($seller_sku,"MRITFBA") !== false){
				$SubSource= 'Marec_ITFBA';
			}else if(strpos($seller_sku,"MRFRFBA") !== false){
				$SubSource= 'Marec_FRFBA';
			}
			else if(strpos($seller_sku,"RAINUKFBA") !== false){
				$SubSource= 'Rainbow_UK_FBA';
			} 	
		    return $SubSource;
         }
		  
		public function pushCostBreakerOrders()
		{
		 
			$url = 'http://xsensysc.nextmp.net/Cborders/fetchOrders'; 
			$curl = curl_init(); 
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_FAILONERROR, true); 
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
			$result = curl_exec($curl); 						 
			$error = curl_error($curl); 
			$info = curl_getinfo($curl);
			curl_close($curl); 
		}
		
		public function checkCostBreakerOrders( $orderid = null)
		{
		 
			$url = "http://xsensysc.nextmp.net/Cborders/getOrders/{$orderid}"; 
			$curl = curl_init(); 
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_FAILONERROR, true); 
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
			$result = curl_exec($curl); 						 
			$error = curl_error($curl); 
			$info = curl_getinfo($curl);
			curl_close($curl); 
			return $result;
		}
		
		
		
		
		
		
  }
?>
