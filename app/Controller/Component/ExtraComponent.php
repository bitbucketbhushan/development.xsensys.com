<?php
	
	class ExtraComponent extends Component
	{	
		
		function startup (Controller $controller)
        {
            
		}
        
        /*
         * Function : Timer calculation for response
         * Version : 1.0
         * Company : JijGroup US - UK - India - Europe
         * Parameters : @Null
         * Result List Array
         * 
         */
        public function getResponseTime( $pickedDate = null )
        {
            
            $etime = time() - strtotime($pickedDate);
			$getTime = '';
			if ($etime < 1)
			{
				return '0 seconds';
			}

			$a = array( 365 * 24 * 60 * 60  =>  'year',
						 30 * 24 * 60 * 60  =>  'month',
							  24 * 60 * 60  =>  'day',
								   60 * 60  =>  'hour',
										60  =>  'minute',
										 1  =>  'second'
						);
			$a_plural = array( 'year'   => 'years',
							   'month'  => 'months',
							   'day'    => 'days',
							   'hour'   => 'hours',
							   'minute' => 'minutes',
							   'second' => 'seconds'
						);

			foreach ($a as $secs => $str)
			{
				$d = $etime / $secs;
				if ($d >= 1)
				{
					$r = round($d);
					return $getTime = $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
				}
			}
            
            
        }

        

	}
	
?>
