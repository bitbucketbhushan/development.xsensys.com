<?php
	
	class DhlServiceComponent extends Component
	{	
		
		function startup (Controller $controller)
        {
            
		}
        
        /*
         * Function : Dhl service 
         * Version : 1.0
         * Company : JijGroup US - UK - India - Europe
         * Parameters : @Null
         * Result List Array
         * 
         */
        /*
		 * 
		 * Protect dhl production / sandbox url
		 * 
		 * 
		 */ 	
		 public function getUrlByType( $typeOffset = null )
		 {
			 if( isset( $typeOffset ) && $typeOffset != '' )
			 {
				 //sandbox
				 if( $typeOffset == 1 )
				 {
					return "sandbox" ;
				 }
				 
				 //production
				 if( $typeOffset == 2 )
				 {
					return "production" ;
				 }
			 }
		 }
		 
	}
	
?>
