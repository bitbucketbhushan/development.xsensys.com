<?php
	
	class StoreComponent extends Component
	{	
		
		function startup (Controller $controller)
        {
            
		}
        
        public function getSoldByStore( $sku, $store )
        {
			App::import( "Model","SkuPercentRecord" );
            $percentage = new SkuPercentRecord();            
            $options = array(
				'conditions' => array( 'SkuPercentRecord.sku' => $sku, 'SkuPercentRecord.store_name' => $store ),				
				'fields' => array('SkuPercentRecord.percentage','SkuPercentRecord.margin_value')
			);
			$getdata = $percentage->find( 'first', $options );
			if( count( $getdata ) ==1 )
			{
				return $getPercentage = $getdata['SkuPercentRecord']['percentage'].'____'.$getdata['SkuPercentRecord']['margin_value'];
			}
			else
			{
				return $getPercentage = 'Not Assign';
			}
			
		}
        
	}
	
?>
