<?php
class UserComponent extends Component
{	
	 public $components = array('Session');
	 
	 public function loginTime()
	{
		 
		$this->UserTiming = ClassRegistry::init('UserTiming'); 			
		$user =	$this->UserTiming->find('first', array('conditions' => array('user_inc_id' => $this->Session->read('Auth.User.id'),'login_time >' => date("Y-m-d"))) );
		if(count($user) == 0){
 			$data['client']		 = $this->getDomain();	
			$data['user_inc_id'] = CakeSession::read('Auth.User.id');			
			$data['email'] 		 = CakeSession::read('Auth.User.email');
			$data['user_name'] 	 = $this->Session->read('Auth.User.username');
			$data['login_ip'] 	 = $_SERVER['REMOTE_ADDR'];
			$data['login_time']  = date("Y-m-d H:i:s");
			$this->UserTiming->saveAll( $data );
		}
	}
 
	public function logoutTime() {
	
		$this->UserTiming = ClassRegistry::init('UserTiming'); 		
		$user =	$this->UserTiming->find('first', array('conditions' => array('user_inc_id' => $this->Session->read('Auth.User.id'),'login_time BETWEEN ? AND ? ' => array(date("Y-m-d"),date("Y-m-d")." 23:59:59")) )); 
		
		if(count($user) > 0){
			$data['id'] 		  = $user['UserTiming']['id'];		
			$data['client']		  = $this->getDomain();
			$data['logout_time']  = date("Y-m-d H:i:s");
			$this->UserTiming->saveAll( $data );
		}
	}
	
	public function getDomain() {
		if (substr($_SERVER['HTTP_HOST'], 0, 4) == 'www.') {
			$domain = substr($_SERVER['HTTP_HOST'], 4);
		} else {
			$domain = $_SERVER['HTTP_HOST'];
		}
		return $domain ;
	}
}

?>
