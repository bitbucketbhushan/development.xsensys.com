<?php
	
	class UltimateComponent extends Component
	{	
		/*
		 * 
		 * Params, To call utlimate functions 
		 * 
		 */ 
		public function setAuthentication( $locationName = null , $ccType = null )
        {
						 
			/* Start here set the Auth list */            
            App::import('Helper','Ultimatetext');
			$utltimateHelper = new UltimatetextHelper(new View());                        			
            $utltimateHelper->callAuth( $locationName , $ccType = null );
            		
        }
        
        /*
         * 
         * Notification
         * 
         */ 
        public function setAuthorizeFornotification( $authData = null )
        {
			
			/* Start here set the Auth list */            
            App::import('Controller','MyExceptions');
			$myExceptionObj = new MyExceptionsController(new View());                        
            $myExceptionObj->allowNotification( $authData );
			
		}
		
		/*
         * 
         * Notification for verify orders items afetr cancel and delete
         * 
         */ 
        public function sendOrderVerifyNotification( $getDeleteCancel = null )
        {
			
			/* Start here set the Auth list */            
            App::import('Controller','MyExceptions');
			$myExceptionObj = new MyExceptionsController(new View());                        
            $myExceptionObj->send_order_notification( $getDeleteCancel );
			
		}
		
		/*
         * 
         * Notification for verify orders items afetr cancel and delete
         * 
         */ 
        public function sendCancelOrderVerifyNotification( $getDeleteCancel = null )
        {
			
			/* Start here set the Auth list */            
            App::import('Controller','MyExceptions');
			$myExceptionObj = new MyExceptionsController(new View());                        
            $myExceptionObj->send_order_notification( $getDeleteCancel );
			
		}
				
	}
	
	
	
?>
