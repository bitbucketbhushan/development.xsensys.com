<?php

class ApiactionsController extends AppController
{
    
    var $name = "Apiactions";
    
    var $components = array('Session', 'Common', 'Upload','Auth');
    
    var $helpers = array('Html','Form','Common','Session');
    
	public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->Auth->Allow(array('cancalOrdersByApi','getUnprapereOrders'));
	}
	
    
    public function index()
	{
	}
	
	public function cancalOrdersByApi()
	{
			$this->loadModel('SourceList');
			$this->loadModel( 'OpenOrder' );
			
			App::import('Vendor', 'Linnworks/src/php/Auth');
			App::import('Vendor', 'Linnworks/src/php/Factory');
			App::import('Vendor', 'Linnworks/src/php/Orders');
		
			$username 			= 	Configure::read('linnwork_api_username');
			$password 			= 	Configure::read('linnwork_api_password');
			
			$token 				= 	Configure::read('access_new_token');
			$applicationId 		= 	Configure::read('application_id');
			$applicationSecret 	= 	Configure::read('application_secret');
		
			$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	
			
			$token = $auth->Token;	
			$server = $auth->Server;
			
			$getSourceLists = $this->SourceList->find('all', array('fields' => array( 'StockLocationId' ) ) );
			foreach( $getSourceLists as $getSourceList )
			{
				$locationID 	= 	$getSourceList['SourceList']['StockLocationId'];
				$openorderids	=	OrdersMethods::GetAllOpenOrders("3000","",$locationID,"",$token, $server);	
				foreach( $openorderids as $openorderid )
				{
					$getOpenOrderStatus = $this->OpenOrder->find('first', array( 'conditions' => array( 'status' => '2', 'order_id' =>  $openorderid ), 
																				 'fields' => array( 'num_order_id','order_id', 'status' ) ) );
					if( count($getOpenOrderStatus) == '1' && $getOpenOrderStatus['OpenOrder']['status'] == '2')
					{
						$numOrderID = 	$getOpenOrderStatus['OpenOrder']['num_order_id'];
						$OrderID 	= 	$getOpenOrderStatus['OpenOrder']['order_id'];
						$status		= 	$getOpenOrderStatus['OpenOrder']['status'];
						$orderId	=	$OrderID;
						$fulFilment	=	$locationID;
						$refund		=	'0.00';
						$note 		= 	'refund';
						$result 	= 	OrdersMethods::CancelOrder($orderId,$fulFilment,$refund,$note,$token, $server);
						$this->customLogGlobal( 'Log','Order Cancel [ By Cron__' . date( 'Y-m-d j:i:s' ) . ' ]>>>>>'. $OrderID.'>>>>>'. $numOrderID );
					}
				}
			}
			exit;
	}
	
	
	public function customLogGlobal( $logType = null , $msg = null )
			 {
				  App::uses('Folder', 'Utility');
				  App::uses('File', 'Utility');
				  
				  $folderPath = WWW_ROOT .'img/cancel_log';
				  $dir = new Folder($folderPath, true, 0755);
				  
				  $imgPath = $folderPath .'/'. date('Y-m-d') .'__'. time() .'cancel_log.txt';    				  
				  file_put_contents($imgPath, $msg);
				  
			 }
	
	public function getUnprapereOrders()
	{
			$this->loadModel('SourceList');
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'UnprepareOrder' );
			
			App::import('Vendor', 'Linnworks/src/php/Auth');
			App::import('Vendor', 'Linnworks/src/php/Factory');
			App::import('Vendor', 'Linnworks/src/php/Orders');
			App::import('Vendor', 'Linnworks/src/php/ProcessOrders');
		
			$username 			= 	Configure::read('linnwork_api_username');
			$password 			= 	Configure::read('linnwork_api_password');
			
			$token 				= 	Configure::read('access_new_token');
			$applicationId 		= 	Configure::read('application_id');
			$applicationSecret 	= 	Configure::read('application_secret');
		
			$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	
			App::import('Controller', 'Linnworksapis');
			$linnCant = new LinnworksapisController();
			$status = '';
			$token = $auth->Token;	
			$server = $auth->Server;
			$getSourceLists = $this->SourceList->find('all', array('fields' => array( 'StockLocationId' ) ) );
			$mailFormate = '<html><head><style>table { border-collapse: collapse;width: 100%;}
								th, td {text-align: left;padding: 8px;}								
								tr:hover {background-color: #f5f5f5}
								tr:nth-child(even){background-color: #f2f2f2}
								th {background-color: #4CAF50;color: white;}
								</style>
								</head>
								Please check all unprepared order
								<br><br>
								<table border="1" style="border: 1px solid #c2c2c2;">
										<tr>
											<th>OrderID</th>
											<th>Source</th>
											<th>Ch. SKU</th>
											<th>SKU</th>
											<th>Quantity</th>
											<th>Days</th>
										</tr>';
			foreach( $getSourceLists as $getSourceList )
			{
				$locationID 	= 	$getSourceList['SourceList']['StockLocationId'];
				$openorderids	=	OrdersMethods::GetAllOpenOrders("3000","",$locationID,"",$token, $server);	
				$k = 0;
				foreach( $openorderids as $openorderid )
				{
					$checkOpenOrder 	=	$this->UnprepareOrder->find('first', array('conditions'=>array('UnprepareOrder.order_id' => $openorderid)));
					if( count($checkOpenOrder) == 1 )
					{
						$general_info 		= 	unserialize( $checkOpenOrder['UnprepareOrder']['general_info'] );
						$items 				= 	unserialize( $checkOpenOrder['UnprepareOrder']['items'] );
						$source_name		=	$checkOpenOrder['UnprepareOrder']['source_name'];
						
						$order_date_time	=	explode('T', $general_info->ReceivedDate);
						$order_date			=	$order_date_time[0];
						$datetime1 			= 	new DateTime($order_date);
						$datetime2 			= 	new DateTime(date('Y-m-d'));
						$interval 			= 	$datetime1->diff($datetime2);
						$days				= 	$interval->format('%a');
						
						if( $days > 3 )
						{
							$sku_arr = array(); $qty_arr = array(); $ch_sku = array();
							foreach( $items as $item )
							{
								$sku_arr[] = $item->SKU;
								$qty_arr[] = $item->Quantity;
								$ch_sku[]  = $item->ChannelSKU;
							}
								$unprapereId = $checkOpenOrder['UnprepareOrder']['num_order_id'];
								$mailFormate.=	 '<tr>
													<td>'.$unprapereId.'</td>
													<td>'.$source_name.'</td>
													<td>'.implode(',',$ch_sku).'</td>
													<td>'.implode(',',$sku_arr).'</td>
													<td>'.implode(',',$qty_arr).'</td>
													<td>'.$days.' Days</td>
											  </tr>';
								$status = 'getOrder';
							} else {
							
						}	
					}
				}
			}
			$mailFormate.=  '</table></body></html>';
			if( $status ==  'getOrder') 
			{
				App::uses('CakeEmail', 'Network/Email');
				$email = new CakeEmail('');
				$email->emailFormat('html');
				$email->from('info@euracogroup.co.uk');
				$email->to( array('amit.gaur@jijgroup.com','aakash@euracogroup.co.uk','abhishek@euracogroup.co.uk','vikas.kumar@euracogroup.co.uk'));
				//$email->to( array('amit.gaur@jijgroup.com'));								  
				$getBase = Router::url('/', true);
				$email->subject('Unprepared Orders ');
				$email->send( $mailFormate );
			}
			exit;
	}
	
	
	public function assignRegisteredBarcodeCustom()
	{
				$this->layout = '';
				$this->autoRender = false;
				$this->loadModel( 'MergeUpdate' );
				$this->loadModel( 'RegisteredNumber' );
				
				require_once(APP . 'Vendor' . DS . 'code39' . DS . 'Barcode39.php'); 			
				$checkRegBarcode	=	$this->MergeUpdate->find('all', array('conditions' => array(
																 'OR' => array(array( 'delevery_country' => 'Italy', 'reg_post_number' => '', 'pick_list_status' => 0),
																			   array( 'product_order_id_identify' => '734855-1', 'postal_service' => 'Tracked','reg_post_number' => '')
																				))));
				
				//pr($checkRegBarcode);exit;
				
				if( count($checkRegBarcode) > 0 )
				{
					foreach( $checkRegBarcode as $checkRegBarcod )
					{
						$getRegNum	=	$this->RegisteredNumber->find('first', array( 'conditions' => array( 'split_order_id' => '' ),'orders' => 'reg_number DESC' ) );
	
						$id						=	$getRegNum['RegisteredNumber']['id'];
						$regNumber				=	$getRegNum['RegisteredNumber']['reg_number'];
						$regBarcodeNumber		=	$getRegNum['RegisteredNumber']['reg_baecode_number'];
						
						$data['id'] 			= 	$id;
						$data['split_order_id'] = 	$checkRegBarcod['MergeUpdate']['product_order_id_identify'];
						
					echo	$text = $regNumber;
						$barimg = WWW_ROOT."code39barcode/".$text.".png";
						$bc = new barcode() ;
						$result = $bc->code39($text, $height = "100", $widthScale = "2",$barimg); 
						if($result == '1')
						{
							$this->RegisteredNumber->saveAll( $data );
							$mergeUpdateData['MergeUpdate']['id'] 				= 	$checkRegBarcod['MergeUpdate']['id'];
							$mergeUpdateData['MergeUpdate']['reg_post_number']	=	$text;
							$mergeUpdateData['MergeUpdate']['track_id']			=	$text;
							$mergeUpdateData['MergeUpdate']['reg_num_img']		=	$text.".png";
							$this->MergeUpdate->saveAll( $mergeUpdateData );
						}
				 	}
				}
			}
			
	public function assignTracking($split_order_id)
	{
		$this->layout = '';
		$this->autoRender = false;
		
		if($split_order_id !='' )
		{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'RegisteredNumber' );
			
			require_once(APP . 'Vendor' . DS . 'code39' . DS . 'Barcode39.php'); 			
			$checkRegBarcod	=	$this->MergeUpdate->find('first', array('conditions' => array( 'product_order_id_identify' => $split_order_id, 'postal_service' => 'Tracked', 'service_provider' => 'PostNL','reg_post_number' => '')));
			  
			//  pr($checkRegBarcod);exit;
			if( count($checkRegBarcod) > 0 )
			{
 				 
 					$chkRegNum = $this->RegisteredNumber->find('first', array( 'conditions' => array('split_order_id' => $split_order_id )) );
					
					if( count($chkRegNum) == 0 )
					{
						$getRegNum = $this->RegisteredNumber->find('first', array( 'conditions' => array( 'split_order_id' => '' ),'orders' => 'reg_number DESC' ) );
						if( count($getRegNum) > 0 )
						{					 	
						$regNumber = $getRegNum['RegisteredNumber']['reg_number'];
						 
						$barimg = WWW_ROOT."code39barcode/".$regNumber.".png";
						$bc = new barcode() ;
						$result = $bc->code39($regNumber , $height = "100", $widthScale = "2", $barimg); 
						if($result == '1')
						{
							$data['id'] 			= $getRegNum['RegisteredNumber']['id'];
							$data['split_order_id'] = $checkRegBarcod['MergeUpdate']['product_order_id_identify'];
							$data['date'] 			= date('Y-m-d H:i:s');
							$this->RegisteredNumber->saveAll( $data );
							
							$mergeUpdateData['MergeUpdate']['id'] 				= $checkRegBarcod['MergeUpdate']['id'];
							$mergeUpdateData['MergeUpdate']['reg_post_number']	= $regNumber;
							$mergeUpdateData['MergeUpdate']['track_id']			= $regNumber;
							$mergeUpdateData['MergeUpdate']['reg_num_img']		= $regNumber.".png";
							$this->MergeUpdate->saveAll( $mergeUpdateData );
							
							$data = array('status' => 1, 'msg' =>$regNumber);
						}
					 }
				 }else{
					$msg = 'Tracking Id is Already assigned.';
					$data = array('status' => 2, 'msg' =>$msg);
				}
				 
			}else{
				$msg = 'This split order id is not found.';
				$data = array('status' => 2, 'msg' =>$msg);
			}
		}else{
			$msg = 'Please use valid split order id.';
			$data = array('status' => 2, 'msg' =>$msg);
		}
		
		echo json_encode($data);
		exit;
	}	
	
	public function getBarcodeOutside()
	{ 
	  
	  $this->layout = '';
	  $this->autoRender = false;
	  $this->loadModel( 'OpenOrder' );
	  $this->loadModel( 'AssignService' );
	  $this->loadModel( 'MergeUpdate' );
	
	  //$uploadUrl = $this->getUrlBase();
	  $imgPath = WWW_ROOT .'img/orders/barcode/';   
	  //$allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.status' => '0')));
	  
	  $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => '570112-2')));
	  
	  
	require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
	require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
	require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
	//$font = new BCGFontFile(APP .'Vendor/barcodegen/font/Arial.ttf', 13);
	$colorFront = new BCGColor(0, 0, 0);
	$colorBack = new BCGColor(255, 255, 255);
		
	  if( count($allSplitOrders) > 0 )	
	  {
		  foreach( $allSplitOrders as $allSplitOrder )
		  {					  
			  $id 			= 		$allSplitOrder['MergeUpdate']['id'];
			  $openorderid	=	 	$allSplitOrder['MergeUpdate']['product_order_id_identify'];
			  $barcodeimage	=	$openorderid.'.png';
			  
				$orderbarcode=$openorderid;
				$code128 = new BCGcode128();
				$code128->setScale(2);
				$code128->setThickness(20);
				$code128->setForegroundColor($colorFront);
				$code128->setBackgroundColor($colorBack);
				$code128->setLabel(false);
				$code128->parse($orderbarcode);
									
				//Drawing Part
				$imgOrder128=$orderbarcode.".png";
				$imgOrder128path=$imgPath.'/'.$orderbarcode.".png";
				$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
				$drawing128->setBarcode($code128);
				$drawing128->draw();
				$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);
			  
			  if( $allSplitOrder['MergeUpdate']['product_order_id_identify'] != "" )
			  {
				  //$content = file_get_contents($uploadUrl.$openorderid);
				  //file_put_contents($imgPath.$barcodeimage, $content);
				  
				  $data['MergeUpdate']['id'] 	=  $id;
				  $data['MergeUpdate']['order_barcode_image'] 	=  $barcodeimage;
				  $this->MergeUpdate->save($data);
			  }
		  }
	  }
	  
	  $this->redirect( Router::url( $this->referer(), true ) );
  
 }
    
    
}

?>
