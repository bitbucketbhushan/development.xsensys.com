<?php
class InvoiceController extends AppController
{
    
    var $name = "Invoice";
    
    var $helpers = array('Html','Form','Session');
    
	public function beforeFilter()
    {
		parent::beforeFilter();
		$this->layout = false; 
		$this->Auth->Allow(array('Generate'));			
				
	}
	
	public function index()
    {
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
		$html .= '<body>'.$htmlTemplate.'</body>';
				
		$name	= 'Invoice.pdf';							
		$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
     
		echo $r = 'Юлия Свеженцева';
		echo "<br>=====";
		echo utf8_encode($r);
		echo "<br>=====";
		echo mb_convert_encoding($r, 'HTML-ENTITIES','UTF-8');
		exit;
		
	}
	
    public function Generate($OrderId = null)
    {
        
		$this->layout = '';
		$this->autoRender = false;	
		
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();	
		$result 		=	'';
		$htmlTemplate	=	'';				
				
		//$OrderId	= 	$this->request->data['order_id'];
		$order			=	$this->getOrderByNumId( $OrderId );	
		//	pr($order);exit;
		if(is_object($order)){			
			$htmlTemplate	=	$this->getTemplate($order);
			$SubSource		=	$order->GeneralInfo->SubSource;
			$result			=	ucfirst(strtolower(substr($SubSource, 0, 4)));
			
			/**************** for tempplate *******************/
			$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				 <meta content="" name="description"/>
				 <meta content="" name="author"/>
				 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
			$html .= '<body>'.$htmlTemplate.'</body>';
					
			$name	= 'Invoice.pdf';							
			$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
			//$html_t = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");
			//$dompdf->load_html($html_t);
			
			//$dompdf->load_html(iconv("UTF-8", "CP1252", $html));
			 
			$dompdf->render();			
			//$dompdf->stream($name);
							
			$file_to_save = WWW_ROOT .'img/invoice_template/'.$name;
			//save the pdf file on the server
			file_put_contents($file_to_save, $dompdf->output()); 
			//print the pdf file to the screen for saving
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="'.$name.'"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . filesize($file_to_save));
			header('Accept-Ranges: bytes');
			readfile($file_to_save);
		
		}else{
			echo 'Invalid Order.';
		}
		//echo json_encode($msg);	
		exit;
		
    }
		
	public function getOrderByNumId_openorder($pkOrderId = null)
	{
		$this->loadModel('OpenOrder');	
		$order 	=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id' => $pkOrderId)));
		$data['NumOrderId']    = $pkOrderId;
		$data['GeneralInfo']   = unserialize($order['OpenOrder']['general_info']);
		$data['ShippingInfo']  = unserialize($order['OpenOrder']['shipping_info']);
		$data['CustomerInfo']  = unserialize($order['OpenOrder']['customer_info']);
		$data['TotalsInfo']    = unserialize($order['OpenOrder']['totals_info']);
		$data['Items'] 		   = unserialize($order['OpenOrder']['items']);
		
		if( count($data['CustomerInfo']->BillingAddress) == 0){
			$data['CustomerInfo']->BillingAddress  = $data['CustomerInfo']->Address;
		}		 
		return json_decode(json_encode( $data ),0);	
		
	}
		
	public function getOrderByNumId($pkOrderId = null)
	{
		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
	
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
		//$multi = AuthMethods::Multilogin($username, $password);		
		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

		$token = $auth->Token;	
		$server = $auth->Server;		
		$order	= OrdersMethods::GetOrderDetailsByNumOrderId($pkOrderId,$token, $server);
		//pr($order	);
		//exit;
		return $order;	
		
	}
	
	public function getOrderByNumIdDhl($pkOrderId = null)
	{
		 
		//App::import('Vendor', 'Linnworks/src/php/Orders');
		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
		
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
		//$multi = AuthMethods::Multilogin($username, $password);		
		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

		$token = $auth->Token;	
		$server = $auth->Server;		
		$order	= OrdersMethods::GetOrderDetailsByNumOrderId($pkOrderId,$token, $server);
		
		return $order;	
		
	}
	public function getTemplate($order = null )
	{
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;
		
		
		$this->loadModel('InvoiceAddress');		
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
			$BILLING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName)).'<br>';
			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$order->CustomerInfo->BillingAddress->Country ? $order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
			
			$SHIPPING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName)).'<br>';
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $order->CustomerInfo->Address->Country ? $order->CustomerInfo->Address->Country : '';
			}			
		}		
		
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal.'&nbsp;'.$Currency;
		$POSTAGE		  = $order->TotalsInfo->PostageCost.'&nbsp;'.$Currency;
		$TAX			  = $order->TotalsInfo->Tax.'&nbsp;'.$Currency;
		$TOTAL			  = $order->TotalsInfo->TotalCharge.'&nbsp;'.$Currency;
		$INVOICE_DATE	  = date('Y-M-d', strtotime($order->GeneralInfo->ReceivedDate));
		$templateHtml 	  = NULL;
					
		if($result == 'cost'){
			$templateHtml ='<!-- CostBreaker Invoice -->';			
			$templateHtml .='<div id="label">';
			$templateHtml .='<div class="container">';	
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top" style="font-size:15px;">
				EURACO GROUP LIMITED, 36 HARBOUR REACH<br>LA RUE DE CARTERET, ST HELIER<br>JERSEY, JE2 4HR</td>';
			$templateHtml .='<td  width="25%" valign="top" >';
			$templateHtml .='<span style="font-weight:bold; font-size:30px">INVOICE</span><br>';
			$templateHtml .='<b>Invoice Number:</b>'.$OrderId.'<br><b>Date:</b> '.$INVOICE_DATE;
			$templateHtml .='</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';	
			$templateHtml .='<td width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';	
			$templateHtml .='</tr>';	
			$templateHtml .='</table>';
	
			$templateHtml  .='<div class="tablesection row" style="margin:5px 0 0 0; border-bottom:0px;height:700px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
				foreach($items as $item){
					
				  $templateHtml .='<tr>
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$item->Title.'</td>
					   <td style="" valign="top" >'.$item->Quantity.'</td>
					   <td style="" valign="top" >'.$item->PricePerUnit.'</td>
					   <td style="" valign="top" >'.$item->TaxRate.'</td>
					   <td style="" valign="top" >'.$item->Tax.'</td>
					   <td style="" valign="top" >'.$item->Cost.'</td>
					   <td style="" valign="top" >'.$item->CostIncTax.'</td>
					</tr>';            
       
				}
			$templateHtml .= '</table>';
	
			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" rowspan="4" width="60%"></th>';
			$templateHtml .= '<th style="" align="left" width="25%">SUB TOTAL:</th>';	
			$templateHtml .= '<td style="" align="right" width="15%">'.$SUB_TOTAL.'</td>';
			$templateHtml .= '</tr>';
	
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" >POSTAGE(Ex TAX):</th>';
			$templateHtml .='<td style="" align="right">'.$POSTAGE.'</td>';	
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" >TAX:</th>';	
			$templateHtml .='<td style="" align="right">'.$TAX.'</td>';	
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" >TOTAL:</th>';	
			$templateHtml .='<td style="" align="right">'.$TOTAL.'</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';			
			$templateHtml .='</div>';
			
			$templateHtml .='<div class="footer row" style="text-align:center; border-bottom:0px;">
			<b>Thank you for your purchase. Happy Shopping!</div>';
			$templateHtml .='</div>';
		
		}
		elseif($result == 'mare'){
			$templateHtml ='<!-- eBuyer Express(Marec) Invoice -->';			
			$templateHtml .='<div id="label" style="margin-top:20px;">';
			$templateHtml .='<div class="container">';			
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top"><img src=http://xsensys.com/img/ebuyerexpress.jpg width="380px"></td>';
			$templateHtml .='<td  width="25%" valign="top">
			THIRD FLOOR<br>
			40 ESPLANADE<br>
			ST HELIER UNITED KINGDOM<br>
			JE4 9RJ			
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="37%"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml .='<td valign="top" width="38%"><h4><u>Billing Address:</u></h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';

			$templateHtml .='<td  width="25%" valign="top">
			<table>
			<tr><td><b>Invoice ID:</b></td>	<td>'.$OrderId.'</td></tr>
			<tr><td><b>Invoice Date:</b></td><td>'.$INVOICE_DATE.'</td></tr>
			</table>
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<div style="font-weight:bold; font-size:30px; text-align:center">INVOICE</div>';
			$templateHtml .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:600px">';
			
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
			foreach($items as $count => $item){
		    
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				}else{
					$b = '';
				}					
				
			  $templateHtml .='<tr>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->SKU.'</td>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->Title.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Quantity.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->PricePerUnit.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->TaxRate.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Tax.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Cost.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->CostIncTax.'</td>
				</tr>';            
       
			}
			$templateHtml .= '</table>';
			
			$templateHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$templateHtml .='<tr>
			<th style="border-top:2px solid #000;" align="left" rowspan="4" width="60%"></th>
			<th style="border-top:2px solid #000;" align="left" width="25%">SUB TOTAL:</th>
			<td style="border-top:2px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>
			</tr>';
			
			$templateHtml .='<tr>
			<th style="" align="left">POSTAGE(Ex TAX):</th>
			<td style="" align="right">'.$POSTAGE.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TAX:</th>
			<td style="" align="right">'.$TAX.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TOTAL:</th>
			<td style="" align="right">'.$TOTAL.'</td>
			</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='</div>';
			
			$templateHtml .='<div class="footer row" style="font-size:15px; text-align:center; border-bottom:0px;" >
			<h2>THANK YOU FOR YOUR PURCHASE</h2> 
			<b>Please take a moment to leave us positive feedback.</b><br>
			If however, for any reason you are not entirely happy with your
			order, please contact us<br>to allow us to rectify any issues before
			leaving feedback.</div>';			
			$templateHtml .='</div>';
					
		}
		elseif($result == 'rain'){
			
			$templateHtml ='<!-- Rainbow Invoice -->';			
			$templateHtml .='<div id="label">';
			$templateHtml .='<div class="container">';
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="60%" style="text-align:center; font-weight:bold; font-size:28px">INVOICE</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="50%" valign="top">';
			$templateHtml .='<img src=http://xsensys.com/img/rainbow_logo.png width="250px">';
			$templateHtml .='</td>';			
			$templateHtml .='<td  width="50%" valign="top" style="text-align:right">
				<b>FRESHER BUSINESS LIMITED</b><br>
				BEACHSIDE BUSINESS CENTRE<br>
				RUE DU HOCQ<br>
				ST CLEMENT<br>
				JERSEY<br>
				JE2 6LF
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<table cellpadding="3" style="background-color:#333; color:#fff; margin-bottom:20px;" class="topborder bottomborder"><tr>';
			$templateHtml .='<td><span class="bold">Date:</span> '.$INVOICE_DATE.'</td>';
			$templateHtml .='<td align="right"><span class="bold">Invoice ID:</span> '.$OrderId.'</td>';
			$templateHtml .='</tr></table>';
			
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  valign="top" width="50%"><h4 class="topborder bottomborder" style="background-color:#333; color:#fff; padding:3px">
			Billing Address:</h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml .='<td  width="50%" valign="top"><h4 class="topborder bottomborder" style="background-color:#333; color:#fff; padding:3px">
			Delivery Address:</h4>'.$SHIPPING_ADDRESS.'</td>';
			
			$templateHtml .='</tr>';		
			$templateHtml .='</table>';
	
			$templateHtml .='<div class="tablesection row" style="margin:5px 0 0 0; border-bottom:0px;height:680px">';
			$templateHtml .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse: collapse; margin:0px;">';
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" class="topborder bottomborder" width="13%">SKU</th>';
			$templateHtml .='<th style="" align="left" valign="top" width="19%" class="topborder bottomborder">Item</th>';
			$templateHtml .='<th style="" valign="top" class="topborder bottomborder" width="10%">Quantity</th>';
			$templateHtml .='<th style="" align="left" class="topborder bottomborder" width="8%">Unit</th>';
			$templateHtml .='<th style="" align="left" valign="top" width="10%" class="topborder bottomborder">Tax Rate</th>';
			$templateHtml .='<th style="" valign="top" class="topborder bottomborder" width="8%">Tax</th>';
			$templateHtml .='<th style="" align="left" class="topborder bottomborder" width="12%">Cost (ex, Tax)</th>';
			$templateHtml .='<th style="" align="left" valign="top" width="10%" class="topborder bottomborder">Line Cost</th>';
			$templateHtml .='</tr>';
				foreach($items as $item){
				  $templateHtml .='<tr>
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$item->Title.'</td>
					   <td style="" valign="top" >'.$item->Quantity.'</td>
					   <td style="" valign="top" >'.$item->PricePerUnit.'</td>
					   <td style="" valign="top" >'.$item->TaxRate.'</td>
					   <td style="" valign="top" >'.$item->Tax.'</td>
					   <td style="" valign="top" >'.$item->Cost.'</td>
					   <td style="" valign="top" >'.$item->CostIncTax.'</td>
					</tr>';            
				
				}
			$templateHtml .='</table>';
		
			$templateHtml .='<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			
			$templateHtml .='<tr>';
			$templateHtml .='<th style="" align="left" width="60%" class="topborder"></th>';
			$templateHtml .='<th style="" align="left" width="25%" class="topborder">SUB TOTAL:</th>';
			$templateHtml .='<td style="" align="right" width="15%" class="topborder">'.$SUB_TOTAL.'</td>';
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td></td>';
			$templateHtml .='<th style="" align="left">POSTAGE(Ex TAX):</th>';
			$templateHtml .='<td style="" align="right">'.$POSTAGE.'</td>';
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td></td>';
			$templateHtml .='<th style="" align="left" >TAX:</th>';
			$templateHtml .='<td style="" align="right">'.$TAX.'</td>';
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td></td>';
			$templateHtml .='<th style="" align="left" class="topborder bottomborder">TOTAL:</th>';
			$templateHtml .='<td style="" align="right" class="topborder bottomborder">'.$TOTAL.'</td>';
			$templateHtml .='</tr>';			
			$templateHtml .='</table>';			
			$templateHtml .='</div>';
	
			$templateHtml  .= '<div class="footer row" style="text-align:center; border-bottom:0px;" >
				<b>Thank you for your purchase. Happy Shopping!<br>
				Rainbow Retail</b>';	
			 $templateHtml  .= '</div>';
		}
		elseif($result == 'bbd_'){		
			$templateHtml ='<!-- BBD_EU Invoice -->';			
			$templateHtml .='<div id="label" style="margin-top:20px;">';
			$templateHtml .='<div class="container">';			
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top"><img src=http://xsensys.com/img/bbdEu.png></td>';
			$templateHtml .='<td  width="25%" valign="top">			
								Unit A1 21/F, Officeplus Among Kok<br>
								998 Canton Road<br>
								Hong Kong<br>
								KL<br>
								0000			
							</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="37%"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml .='<td valign="top" width="38%"><h4><u>Billing Address:</u></h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';

			$templateHtml .='<td  width="25%" valign="top">
			<table>
			<tr><td><b>Invoice ID:</b></td>	<td>'.$OrderId.'</td></tr>
			<tr><td><b>Invoice Date:</b></td><td>'.$INVOICE_DATE.'</td></tr>
			</table>
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<div style="font-weight:bold; font-size:30px; text-align:center">INVOICE</div>';
			$templateHtml .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:600px">';
			
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
			foreach($items as $count => $item){
		    
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				}else{
					$b = '';
				}					
				
			  $templateHtml .='<tr>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->SKU.'</td>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->Title.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Quantity.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->PricePerUnit.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->TaxRate.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Tax.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Cost.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->CostIncTax.'</td>
				</tr>';            
       
			}
			$templateHtml .= '</table>';
			
			$templateHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$templateHtml .='<tr>
			<th style="border-top:2px solid #000;" align="left" rowspan="4" width="60%"></th>
			<th style="border-top:2px solid #000;" align="left" width="25%">SUB TOTAL:</th>
			<td style="border-top:2px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>
			</tr>';
			
			$templateHtml .='<tr>
			<th style="" align="left">POSTAGE(Ex TAX):</th>
			<td style="" align="right">'.$POSTAGE.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TAX:</th>
			<td style="" align="right">'.$TAX.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TOTAL:</th>
			<td style="" align="right">'.$TOTAL.'</td>
			</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='</div>';
			
			$templateHtml .='<div class="footer row" style="font-size:15px; text-align:center; border-bottom:0px;" >
			<h2>THANK YOU FOR YOUR PURCHASE</h2> 
			<b>Please take a moment to leave us positive feedback.</b><br>
			If however, for any reason you are not entirely happy with your
			order, please contact us<br>to allow us to rectify any issues before
			leaving feedback.</div>';			
			$templateHtml .='</div>';
					
		}
		elseif($result == 'tech'){
			$templateHtml  ='<!-- TechDrive Invoice -->';
			$templateHtml  .='<div id="label">';
			$templateHtml  .='<div class="container">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px"></table>';
			$templateHtml  .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  width="60%" valign="top">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml  .='<td  width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			$templateHtml  .='</td>';
			$templateHtml  .='<td  width="40%" valign="top" align="center">
			<img src=http://xsensys.com/img/techDriveSupplies2.jpg width="280px"></td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			
			$templateHtml  .='<table>';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td width="80%"><span style="font-weight:bold; font-size:32px">INVOICE #'.$OrderId.'</span><br></td>';
			$templateHtml  .='<td width="20%" align="right"><b>Date:</b> '.$INVOICE_DATE.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';

			$templateHtml  .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:650px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:3px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
				foreach($items as $item){
					
				  $templateHtml .='<tr style="background-color:#f2f2f2">
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$item->Title.'</td>
					   <td style="" valign="top" >'.$item->Quantity.'</td>
					   <td style="" valign="top" >'.$item->PricePerUnit.'</td>
					   <td style="" valign="top" >'.$item->TaxRate.'</td>
					   <td style="" valign="top" >'.$item->Tax.'</td>
					   <td style="" valign="top" >'.$item->Cost.'</td>
					   <td style="" valign="top" >'.$item->CostIncTax.'</td>
					</tr>';            
       
				}
			$templateHtml .= '</table>';		 
		
			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-top:1px solid #000;border-bottom:3px solid #000;" align="left" rowspan="4" width="60%">';		
			$templateHtml .= '<div style="border-bottom:0px;" ><b>Thank you for your purchase. Happy Shopping!<br>TechDrive Supplies</b></div>';
			$templateHtml .= '</th>';
			$templateHtml .= '<th style="border-top:1px solid #000;" align="left" width="25%">SUB TOTAL:</th>';
			$templateHtml .= '<td style="border-top:1px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>';
			$templateHtml .= '</tr>';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >POSTAGE(Ex TAX):</th>';
			$templateHtml .= '<td style="" align="right">'.$POSTAGE.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >TAX:</th>';
			$templateHtml .= '<td style="" align="right">'.$TAX.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-bottom:3px solid #000;" align="left" >TOTAL:</th>';
			$templateHtml .= '<td style="border-bottom:3px solid #000;" align="right">'.$TOTAL.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '</table>';
			$templateHtml .= '</div>';
				
			$templateHtml .= '<div class="row" style="text-align:center; border-bottom:0px;margin-left:20%; padding:0px" >';
			$templateHtml .= '<div style="width:70%; text-align:center; border:2px solid #000000; padding:10px;" >';
	
			$templateHtml .= '<table style="padding:0; text-align:center"><tr>
			<td width="15%"><img src="http://xsensys.com/img/happy_g.png" width="80px"></td>
			<td width="85%" align="center"><span style="font-size:24px">Happy with your order?</span> <br>
			<span style="font-size:10px;">Please take a moment to leave us positive feedback.</span><br>
			<img src="http://xsensys.com/img/5star_g.png" width="120px"><br></td></tr>
			</table>';
			$templateHtml .= '<div style="text-align:left; font-size:10px;">
			If however, for any reason you are not entirely happy with your order, please contact us to allow us to rectify any issues before leaving feedback.
			</div>';
			
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
		
		}		
		elseif($result == 'ebay'){			
			$templateHtml .='<!-- eBuyersDirect Invoice -->';			
			$templateHtml .='<div id="label" style="margin-top:20px;">';
			$templateHtml .='<div class="container">';			
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="50%" valign="top">
			4 Norwood Court<br>
La Rue Militaire<br>
St John, Jersey Channel Islands JE3 4DP<br>
United Kingdom
			</td>';
			$templateHtml .='<td  width="50%" valign="top" align="right">
			<img src=http://xsensys.com/img/ebuyerDirect.png width="300px">
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<hr style="color:#6bc15f">';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>'; 
			
			//mb_convert_encoding($SHIPPING_ADDRESS,'HTML-ENTITIES','UTF-8')			
			$t = mb_convert_encoding($SHIPPING_ADDRESS, "windows-1251", "utf-8");		
			$templateHtml .='<td  valign="top" width="37%"><h4>Delivery Address:</h4>'.$t.'</td>';
			$templateHtml .='<td  valign="top" width="38%"><h4>Billing Address:</h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';
			
			$templateHtml .='<td  width="25%" valign="top">
			<table>
			<tr>
			<td><b>Invoice ID:</b></td>
			<td align="right">'.$OrderId.'</td>
			</tr>
			<tr>
			<td><b>Invoice Date:</b></td>
			<td align="right">'.$INVOICE_DATE.'</td>
			</tr>
			</table>
			<div style="font-size:44px; font-weight:bold; text-align:right">INVOICE</div>
			</td>';
			
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			
			$templateHtml .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:625px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:2px solid #000;border-bottom:2px solid #000; background-color:#6bc15f" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
			foreach($items as $count => $item){
		    
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				}else{
					$b = '';
				}					
				
			  $templateHtml .='<tr>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->SKU.'</td>
				   <td style="'.$b.'" align="left" valign="top" class="">'.$item->Title.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Quantity.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->PricePerUnit.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->TaxRate.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Tax.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->Cost.'</td>
				   <td style="'.$b.'" valign="top" >'.$item->CostIncTax.'</td>
				</tr>';            
       
			}
			$templateHtml .= '</table>';

			$templateHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$templateHtml .='<tr>
			<th style="border-top:2px solid #000;" align="left" rowspan="4" width="60%"></th>
			<th style="border-top:2px solid #000;" align="left" width="25%">SUB TOTAL:</th>
			<td style="border-top:2px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>
			</tr>';

			$templateHtml .='<tr>
			<th style="" align="left" >POSTAGE(Ex TAX):</th>
			<td style="" align="right">'.$POSTAGE.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TAX:</th>
			<td style="" align="right">'.$TAX.'</td>
			</tr>';
			$templateHtml .='<tr>
			<th style="" align="left" >TOTAL:</th>
			<td style="" align="right">'.$TOTAL.'</td>
			</tr>';
			$templateHtml .='</table>';
			$templateHtml .='</div>';
			
			$templateHtml .='<hr style="color:#6bc15f">';
			$templateHtml .='<div class="footer row" style="font-size:15px; text-align:left; border-bottom:0px;" >
			<span style="font-size:28px; font-weight:bold">THANK YOU</span> <i>for using ebuyersDirect-2u</i><br><br>
			<b>Please take a moment to leave us positive feedback.</b><br>
			If however, for any reason you are not entirely happy with your
			order, please contact us to allow us to rectify<br>any issues before
			leaving feedback.</div>';
			$templateHtml .='</div>';
		}
		elseif($result == 'http'){		
			$templateHtml  ='<!-- storeforlife Invoice -->';			
			$templateHtml  .='<div id="label">';
			$templateHtml  .='<div class="container">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px"></table>';
			$templateHtml  .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  width="60%" valign="top">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml  .='<td  width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			$templateHtml  .='</td>';
			$templateHtml  .='<td  width="40%" valign="top" align="center">
			<img src="http://xsensys.com/img/sfl_logo.png"><br>			
			36 HARBOUR REACH, LA RUE DE CARTERET<br> ST HELIER, JERSEY, JE2 4HR</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			
			$templateHtml  .='<table>';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td width="80%"><span style="font-weight:bold; font-size:32px">INVOICE #'.$OrderId.'</span><br></td>';
			$templateHtml  .='<td width="20%" align="right"><b>Date:</b> '.$INVOICE_DATE.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';

			$templateHtml  .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:650px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:3px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="6%">Unit</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="5%">Tax</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="12%">Cost (ex, Tax)</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
				foreach($items as $item){
					
				  $templateHtml .='<tr style="background-color:#f2f2f2">
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$item->Title.'</td>
					   <td style="" valign="top">'.$item->Quantity.'</td>
					   <td style="" valign="top">'.$item->PricePerUnit.'</td>
					   <td style="" valign="top">'.$item->TaxRate.'</td>
					   <td style="" valign="top">'.$item->Tax.'</td>
					   <td style="" valign="top">'.$item->Cost.'</td>
					   <td style="" valign="top">'.$item->CostIncTax.'</td>
					</tr>';            
       
				}
			$templateHtml .= '</table>';
		 
		
			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-top:1px solid #000;border-bottom:3px solid #000;" align="left" rowspan="4" width="60%">';		
			$templateHtml .= '<div style="border-bottom:0px;" ><b>Thank you for your purchase. Happy Shopping!<br>StoreForLife</b></div>';
			$templateHtml .= '</th>';
			$templateHtml .= '<th style="border-top:1px solid #000;" align="left" width="25%">SUB TOTAL:</th>';
			$templateHtml .= '<td style="border-top:1px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>';
			$templateHtml .= '</tr>';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >POSTAGE(Ex TAX):</th>';
			$templateHtml .= '<td style="" align="right">'.$POSTAGE.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >TAX:</th>';
			$templateHtml .= '<td style="" align="right">'.$TAX.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-bottom:3px solid #000;" align="left" >TOTAL:</th>';
			$templateHtml .= '<td style="border-bottom:3px solid #000;" align="right">'.$TOTAL.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '</table>';
			$templateHtml .= '</div>';
				
			$templateHtml .= '<div class="row" style="text-align:center; border-bottom:0px;margin-left:20%; padding:0px" >';
			$templateHtml .= '<div style="width:70%; text-align:center; border:2px solid #000000; padding:10px;" >';
	
			$templateHtml .= '<table style="padding:0; text-align:center"><tr>
			<td width="15%"><img src="http://xsensys.com/img/happy_g.png" width="80px"></td>
			<td width="85%" align="center"><span style="font-size:24px">Happy with your order?</span> <br>
			<span style="font-size:10px;">Please take a moment to leave us positive feedback.</span><br>
			<img src="http://xsensys.com/img/5star_g.png" width="120px"><br></td>
			</tr>
			</table>';
			$templateHtml .= '<div style="text-align:left; font-size:10px;">
			If however, for any reason you are not entirely happy with your order, please contact us to allow us to rectify any issues before leaving feedback.
			</div>';

			
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
			
		}
				
				
		return $templateHtml;	
	}
	
	public function getTemplateDHL($order = null,$waybill_number = null )
	{
		$this->loadModel('ProductHscode');
		$this->loadModel('InvoiceAddress');	
		$this->loadModel('MergeUpdate');
		$this->loadModel('Product');	
		
		$SubSource		  = $order->GeneralInfo->SubSource;
		$result			  = strtolower(substr($SubSource, 0, 4));
		$items			  = $order->Items;
		$OrderId 		  = $order->NumOrderId;  		 		
			
		$conditions = array('InvoiceAddress.order_id' => $OrderId);
		
		$SHIPPING_ADDRESS = '';
		$BILLING_ADDRESS = '';
		
		if ($this->InvoiceAddress->hasAny($conditions)){
			$address_data = $this->InvoiceAddress->find('first', array('conditions' => $conditions));
			if($address_data['InvoiceAddress']['shipping_address'] != ''){
				$SHIPPING_ADDRESS = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['shipping_address'])));
			}
			if($address_data['InvoiceAddress']['billing_address'] != ''){
				$BILLING_ADDRESS  = nl2br(ucfirst($this->replaceFrenchChar($address_data['InvoiceAddress']['billing_address'])));
			}			
		}	
		if($BILLING_ADDRESS == ''){
			
			$BILLING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->FullName)).'<br>';
			if($order->CustomerInfo->BillingAddress->Address1 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address1)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address2 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address2)).'<br>';
			}
			if($order->CustomerInfo->BillingAddress->Address3 !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Address3)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Town !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Town)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->PostCode !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->BillingAddress->Region !=''){
			 $BILLING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->BillingAddress->Region)).'<br>';
			}			
			if($order->CustomerInfo->BillingAddress->Country != 'UNKNOWN'){
				$BILLING_ADDRESS .=		$order->CustomerInfo->BillingAddress->Country ? $order->CustomerInfo->BillingAddress->Country : '';
			}
		}	
		if($SHIPPING_ADDRESS == ''){
			
			$SHIPPING_ADDRESS  =  ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->FullName)).'<br>';
			if($order->CustomerInfo->Address->Address1 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address1)).'<br>';
			}
			if($order->CustomerInfo->Address->Address2 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address2)).'<br>';
			}
			if($order->CustomerInfo->Address->Address3 !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Address3)).'<br>';
			} 
			if($order->CustomerInfo->Address->Town !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Town)).'<br>';
			} 
			if($order->CustomerInfo->Address->PostCode !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->PostCode)).'<br>';
			} 
			if($order->CustomerInfo->Address->Region !=''){
				$SHIPPING_ADDRESS .= ucfirst($this->replaceFrenchChar($order->CustomerInfo->Address->Region)).'<br>';
			}			
			if($order->CustomerInfo->Address->Country != 'UNKNOWN'){
				$SHIPPING_ADDRESS .= $order->CustomerInfo->Address->Country ? $order->CustomerInfo->Address->Country : '';
			}			
		}		
		
		$Country		  = $order->CustomerInfo->Address->Country;
		$Currency		  = $order->TotalsInfo->Currency;		
		$SUB_TOTAL		  = $order->TotalsInfo->Subtotal;
		$POSTAGE		  = $order->TotalsInfo->PostageCost;
		$TAX			  = $order->TotalsInfo->Tax;
		$TOTAL			  = $order->TotalsInfo->TotalCharge; 
		 
			
		$INVOICE_DATE	  = date('Y-M-d', strtotime($order->GeneralInfo->ReceivedDate));
		$templateHtml 	  = NULL;
		
		/*
			Date 11 MAR 2019 on recommendations of Lalit.
			For Over LVCR Limit in Invoice Format need to remove Tax Rate and Tax column and need to modify heading
			
		*/
		$lvcr_limit = 17.99; //gbp
		
		$final_total = $TOTAL ;
		if($Currency == 'GBP'){
			$final_total = $TOTAL * (1.122);
		}
		if($final_total < $lvcr_limit){
			$POSTAGE = 4;
		}
		$ShipmentWeight  = 0.0;
		$swresult = $this->MergeUpdate->find('first',['conditions' => ['order_id' => $OrderId],'fields'=>['packet_weight','envelope_weight']]);
		if(count( $swresult ) > 0){				
			$ShipmentWeight = $swresult['MergeUpdate']['packet_weight'] + $swresult['MergeUpdate']['envelope_weight'];
		}
		$country_code = $this->getCountryCode($Country);
		$tax_rate = ''; $vat_amount = 0;
		$thsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => 'DEFAULT','country_code' => $country_code)));
		if(count( $thsresult ) > 0){				
			$tax_rate   = $thsresult['ProductHscode']['tax_rate'];
		}
		
		$currency_code = $this->getCurrencyCode($Currency);
								
		if($result == 'cost'){
			
			$EORI_Number = 'GB019020854000';
			$UK_VAT_Number = 'GB 304984295'; 
			$DE_VAT_Number = 'DE 321777974';
			$FR_VAT_Number = ''; 
				
			//$templateHtml ='<!-- CostBreaker Invoice -->';			
			$templateHtml  ='<div id="label">';
			$templateHtml .='<div class="container">';	
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top" style="font-size:15px;">
				EURACO GROUP LIMITED, 36 HARBOUR REACH<br>LA RUE DE CARTERET, ST HELIER<br>JERSEY, JE2 4HR</td>';
			$templateHtml .='<td  width="25%" valign="top" >';
			$templateHtml .='<span style="font-weight:bold; font-size:20px">INVOICE(CIF)</span>';
			$templateHtml .='<br><b>Invoice Number:</b>'.$OrderId;
			$templateHtml .='<br><b>EORI Number:</b>'.$EORI_Number;
			if($Country == 'United Kingdom' && $UK_VAT_Number != ''){
				$templateHtml .='<br><b>VAT Number:</b>'.$UK_VAT_Number;
			}else if($Country == 'Germany' && $DE_VAT_Number != '' ){
				$templateHtml .='<br><b>VAT Number:</b>'.$DE_VAT_Number;
			}
			if($waybill_number != ''){
				$templateHtml .='<br><b>WayBill Number:</b>'.$waybill_number;
			}
			
			$templateHtml .='<br><b>Date:</b>'.$INVOICE_DATE;
			$templateHtml .='</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';	
			$templateHtml .='<td width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';	
			$templateHtml .='</tr>';	
			$templateHtml .='</table>';
	
			$templateHtml  .='<div class="tablesection row" style="margin:5px 0 0 0; border-bottom:0px;height:700px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">';
  			
			foreach($items as $count => $item){ 
					
				$Title = $item->Title;
				if(strlen($item->Title) < 10){
					$Title = $item->ChannelTitle;
				}
				$pro_weight = 0; 
				$pro_res = $this->Product->find('first', array('conditions' => ['Product.product_sku' => $item->SKU],'fields' => ['ProductDesc.weight']));
 				if(count( $pro_res ) > 0){				
					$pro_weight = $pro_res['ProductDesc']['weight'];
				}
				$hs_code = '';
				$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $item->SKU,'country_code' => $country_code)));
				if(count( $hsresult ) > 0){				
					$hs_code = $hsresult['ProductHscode']['hs_code'];
				}
				if($count == 0){	
					  $templateHtml  .='<tr>';
						    $templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top">Item</th>';
							
							$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top">Qty</th>';
							$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top">Weight</th>';
						    if($hs_code != ''){
						   		 $templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" valign="top"">HSCode</th>';
							}
							$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Price</th>';
							$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Tax Rate</th>';
  						   $templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left">Tax</th>
						   <th style="border-top:2px solid #000;border-bottom:2px solid #000;" align="left" valign="top">Line Total</th>
						</tr>';
				}
				
				  $item_vat_amount = $item->Cost - ($item->Cost/(1 + ($tax_rate/100)));
				  $item_cost = $item->Cost - $item_vat_amount;
				  
				  $templateHtml .='<tr>';
  				  $templateHtml .='   <td align="left" valign="top" class="">'. $Title  .'</td>'; 			  
				  $templateHtml .='   <td valign="top">'.$item->Quantity.'</td>'; 
				  $templateHtml .='   <td valign="top">'.($pro_weight * $item->Quantity).'</td>'; 	
				  if($hs_code != ''){
					$templateHtml .='   <td valign="top">'.$hs_code.'</td>'; 
				  }		  
				  $templateHtml .='   <td valign="top">'.number_format($item_cost,2).'&nbsp;'.$currency_code.'</td>'; 
				  $templateHtml .='   <td valign="top">'.$tax_rate.'%</td>'; 				  
				  $templateHtml .='   <td valign="top">'.number_format($item_vat_amount,2).'&nbsp;'.$currency_code.'</td>'; 
				  $templateHtml .='   <td valign="top">'.number_format($item->Cost,2).'&nbsp;'.$currency_code.'</td>'; 
				$templateHtml .='</tr>'; 
        
			}
			
			 $postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
			 $postage_cost = $POSTAGE - $postage_vat_amount;
			  
			$templateHtml .='<tr>';
			$templateHtml .='<td align="left" valign="top" class="">Shipping & Handling Fee</td>'; 
  			$templateHtml .='<td valign="top">&nbsp;</td>'; 
			$templateHtml .='<td valign="top">&nbsp;</td>'; 
			if($hs_code != ''){
				$templateHtml .='<td valign="top">&nbsp;</td>'; 
			}
			$templateHtml .='<td valign="top">'.number_format($postage_cost,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='<td valign="top">'.$tax_rate.'%</td>'; 				  
			$templateHtml .='<td valign="top">'.number_format($postage_vat_amount,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='<td valign="top">'.number_format($POSTAGE,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='</tr>'; 
			
			$final_total =  $item_cost + $postage_cost + $item_vat_amount + $postage_vat_amount;
			
			$templateHtml .='<tr>';
				$templateHtml .='<td align="left" valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000"><strong>Total</strong></td>'; 
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>'; 
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>'; 
				
				if($hs_code != ''){
					$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>'; 
				}
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">'.number_format(($item_cost + $postage_cost),2).'&nbsp;'.$currency_code.'</td>'; 
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">'.$tax_rate.'%</td>'; 				  
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">'.number_format(($item_vat_amount + $postage_vat_amount),2).'&nbsp;'.$currency_code.'</td>'; 
				$templateHtml .='<td valign="top" style="border-top:1px solid #000;border-bottom:1px solid #000">'.number_format($final_total,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='</tr>'; 
 					
			$templateHtml .= '</table>';
			
			 
 			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
 			$templateHtml .='<tr>';			 
			$templateHtml .='<td align="left"><strong>Shipment Weight</strong>: '. number_format($ShipmentWeight,2) .'Kg</td>';	
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';	
			$templateHtml .='<td align="left" width="33%">'; 
			$templateHtml .='<div style="font-size:14px;"><strong>Declaration:</strong></div>
			<div class="row" style="font-size:12px;">
We hereby certify that the information contained <br>in this invoice is true and correct and that
the <br>contents of this shipment are as started above.</div>';
			$templateHtml .='</td>';
			$templateHtml .='</tr>';
 			$templateHtml .='</table>';	
 			$templateHtml .='</div>';
			

			$templateHtml .='<div class="footer row" style="text-align:center; border-bottom:0px;">
			<b>Thank you for your purchase. Happy Shopping!</div>';
		 	$templateHtml .='</div>'; 
		
		}
		elseif($result == 'mare'){
			
			$EORI_Number = 'GB019434034000';
			$UK_VAT_Number = 'GB 307817693'; 
			$DE_VAT_Number = '';
			$FR_VAT_Number = '';
			
			$templateHtml ='<!-- eBuyer Express(Marec) Invoice -->';			
			$templateHtml .='<div id="label" style="margin-top:20px;">';
			$templateHtml .='<div class="container">';			
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="75%" valign="top"><img src="'.WWW_ROOT.'img/ebuyerexpress.jpg" width="380px"></td>';
			$templateHtml .='<td  width="25%" valign="top">
			ESL LIMITED<br>
			THIRD FLOOR<br>
			40 ESPLANADE<br>
			ST HELIER, ST CLEMENT<br>
			JE4 9RJ			
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="35%"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml .='<td valign="top" width="35%"><h4><u>Billing Address:</u></h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';
			 
 			$templateHtml .='<td  width="29%" valign="top">';
			$templateHtml .='<table>';
			$templateHtml .='<tr><td><b>EORI No:</b></td>	<td>'.$EORI_Number.'</td></tr>';
			$templateHtml .='<tr><td><b>Invoice(CIF):</b></td>	<td>'.$OrderId.'</td></tr>';
			if($Country == 'United Kingdom' && $UK_VAT_Number != '' ){
				$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$UK_VAT_Number.'</td></tr>';
			}else if($Country == 'Germany' && $DE_VAT_Number != ''){
				$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$DE_VAT_Number.'</td></tr>';
			}
			if($waybill_number != ''){
				$templateHtml .='<tr><td><b>WayBill No:</b></td>	<td>'.$waybill_number.'</td></tr>';
			}
			
			$templateHtml .='<tr><td><b>Invoice Date:</b></td><td>'.$INVOICE_DATE.'</td></tr>';
			$templateHtml .='</table>';
			$templateHtml .='</td>';
			
 			
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			
			 
			$templateHtml .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:500px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">';
 			
			$country_code = $this->getCountryCode($Country);
			
			foreach($items as $count => $item){
		    	$b = '';
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				} 					
				$pro_weight = 0; 
				$pro_res = $this->Product->find('first', array('conditions' => ['Product.product_sku' => $item->SKU],'fields' => ['ProductDesc.weight']));
 				if(count( $pro_res ) > 0){				
					$pro_weight = $pro_res['ProductDesc']['weight'];
				} 
				$hs_code = '';
			    $hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $item->SKU,'country_code' => $country_code)));
				if(count( $hsresult ) > 0){				
					$hs_code = $hsresult['ProductHscode']['hs_code'];
				}
				
				if( $count == 0){
					
					$templateHtml  .='<tr>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">SKU</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Item</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Qty</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Weight</th>';
					if( $hs_code != ''){
						$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">HSCode</th>';
					}
 					
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Price</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Tax Rate</th>';				
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Tax</th>';
					$templateHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Line Total</th>';
					$templateHtml  .='</tr>';
				}
				
				$Title = $item->Title;
				if(strlen($item->Title) < 10){
					$Title = $item->ChannelTitle;
				}
					  
				$templateHtml .='<tr>';
					$templateHtml .=' <td style="'.$b.'" align="left" valign="top">'.$item->SKU.'</td>';
					$templateHtml .=' <td style="'.$b.'" align="left" valign="top">'.$Title.'</td>';
					$templateHtml .=' <td style="'.$b.'" valign="top">'.$item->Quantity.'</td>';
					$templateHtml .=' <td style="'.$b.'" valign="top">'.($pro_weight * $item->Quantity).'</td>';
					if($hs_code != ''){
						$templateHtml .=' <td style="'.$b.'" align="left" valign="top" >'.$hs_code.'</td>';
					}
					
					$item_vat_amount = $item->Cost - ($item->Cost/(1 + ($tax_rate/100)));
				    $item_cost = $item->Cost - $item_vat_amount;
				  
 					$templateHtml .=' <td style="'.$b.'" valign="top">'.number_format($item_cost,2).'&nbsp;'.$currency_code.'</td>';	
					$templateHtml .=' <td style="'.$b.'" valign="top">'.$tax_rate.'%</td>';				 
					$templateHtml .=' <td style="'.$b.'" valign="top" >'.number_format($item_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
					$templateHtml .=' <td style="'.$b.'" valign="top" >'.number_format($item->Cost,2).'&nbsp;'.$currency_code.'</td>';
				$templateHtml .='</tr>';            
       
			}
 			
			$postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
			$postage_cost = $POSTAGE - $postage_vat_amount;
			
			$b_bottom = 'border-bottom:2px solid #000;';  
			$templateHtml .='<tr>';
				$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top" colspan="2">SHIPPING HANDLING FEE</td>'; 
   				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				if($hs_code != ''){
					$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format($postage_cost,2).'&nbsp;'.$currency_code.'</td>';	
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.$tax_rate.'%</td>';				 
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top" >'.number_format($postage_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top" >'.number_format($POSTAGE,2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='</tr>';  
			
 			$final_total =  $item_cost + $postage_cost + $item_vat_amount + $postage_vat_amount;
			  
			$templateHtml .='<tr>';
				$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top" colspan="2"><strong>TOTAL</strong></td>'; 
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				if($hs_code != ''){
					$templateHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
  				 
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format(($item_cost+$postage_cost),2).'&nbsp;'.$currency_code.'</td>';	
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.$tax_rate.'%</td>';					 
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format(($item_vat_amount+$postage_vat_amount),2).'&nbsp;'.$currency_code.'</td>';
				$templateHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format($final_total,2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='</tr>';  
			    
			  
			$templateHtml .= '</table>';
			
			$templateHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$templateHtml .='<tr>';
			$templateHtml .='<td align="left"><strong>Shipment Weight</strong>: '. number_format($ShipmentWeight,2) .'Kg</td>';	
 			$templateHtml .='</tr>';	
			 $templateHtml .='<tr>';	
			$templateHtml .='<td align="left" width="33%">'; 
			$templateHtml .='<div style="font-size:14px;"><strong>Declaration:</strong></div>
			<div class="row" style="font-size:12px;">
We hereby certify that the information contained <br>in this invoice is true and correct and that
the <br>contents of this shipment are as started above.</div>';
			$templateHtml .='</td>';
			$templateHtml .='</tr>';
			
			$templateHtml .='</table>';
			
			$templateHtml .='</div>';
			
 			$templateHtml .='<div class="footer row" style="font-size:15px; text-align:center; border-bottom:0px;" >
			<h2>THANK YOU FOR YOUR PURCHASE</h2> 
			<b>Please take a moment to leave us positive feedback.</b><br>
			If however, for any reason you are not entirely happy with your
			order, please contact us<br>to allow us to rectify any issues before
			leaving feedback.</div>';			
		 	$templateHtml .='</div>';
					
		}
		elseif($result == 'rain'){
		
			$EORI_Number = 'GB318649182000';
			$UK_VAT_Number = 'GB 318649182'; 
			$DE_VAT_Number = '';
			$FR_VAT_Number = '';     
			
			//$templateHtml ='<!-- Rainbow Invoice -->';			
			$templateHtml ='<div id="label">';
			$templateHtml .='<div class="container">';
			$templateHtml .='<table class="header row" style="border-bottom:0px">';
			$templateHtml .='<tr>';
			$templateHtml .='<td valign="top" width="60%" style="text-align:center; font-weight:bold; font-size:28px">INVOICE(CIF)</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  width="50%" valign="top">';
			$templateHtml .='<img src=http://xsensys.com/img/rainbow_logo.png width="250px">';
			$templateHtml .='</td>';			
			$templateHtml .='<td  width="50%" valign="top" style="text-align:right">
				<b>FRESHER BUSINESS LIMITED</b><br>
				BEACHSIDE BUSINESS CENTRE<br>
				RUE DU HOCQ<br>
				ST CLEMENT<br>
				JERSEY<br>
				JE2 6LF
			</td>';
			$templateHtml .='</tr>';
			$templateHtml .='</table>';
			$templateHtml .='<table cellpadding="3" style="background-color:#333; color:#fff; margin-bottom:20px;" class="topborder bottomborder"><tr>';
			
			$templateHtml .='<td><span class="bold">Invoice ID:</span> '.$OrderId.'</td>';		
			$templateHtml .='<td><span class="bold">EORI No:'.$EORI_Number.'</td>';
			if($Country == 'United Kingdom' && $UK_VAT_Number != ''){
				$templateHtml .='<td><b>VAT No:</b>'.$UK_VAT_Number.'</td>';
			}else if($Country == 'Germany' && $DE_VAT_Number != '' ){
				$templateHtml .='<td><b>VAT No:</b>'.$DE_VAT_Number.'</td>';
			}
			if($waybill_number != ''){
				$templateHtml .='<td><b>WayBill No:</b>'.$waybill_number.'</td>';
			}
			$templateHtml .='<td><span class="bold">Date:</span> '.$INVOICE_DATE.'</td>';
			
			$templateHtml .='</tr></table>';
			
			
			$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml .='<tr>';
			$templateHtml .='<td  valign="top" width="50%"><h4 class="topborder bottomborder" style="background-color:#333; color:#fff; padding:3px">
			Billing Address:</h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml .='<td  width="50%" valign="top"><h4 class="topborder bottomborder" style="background-color:#333; color:#fff; padding:3px">
			Delivery Address:</h4>'.$SHIPPING_ADDRESS.'</td>';
			
			$templateHtml .='</tr>';		
			$templateHtml .='</table>';
	
			$templateHtml .='<div class="tablesection row" style="margin:5px 0 0 0; border-bottom:0px;height:600px">';
			$templateHtml .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse: collapse; margin:0px;">';
			$country_code = $this->getCountryCode($Country);
			foreach($items as $count => $item){
				
					$Title = $item->Title;
					if(strlen($item->Title) < 10){
						$Title = $item->ChannelTitle;
					}
					
					$pro_weight = 0; 
					$pro_res = $this->Product->find('first', array('conditions' => ['Product.product_sku' => $item->SKU],'fields' => ['ProductDesc.weight']));
					if(count( $pro_res ) > 0){				
						$pro_weight = $pro_res['ProductDesc']['weight'];
					} 
					$hs_code = '';
					$hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $item->SKU,'country_code' => $country_code)));
					if(count( $hsresult ) > 0){				
						$hs_code = $hsresult['ProductHscode']['hs_code'];
					}
				
					if($count == 0){
						$templateHtml .='<tr>';
						$templateHtml .='<th align="left" class="topborder bottomborder">SKU</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Item</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Qty</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Weight</th>';
						if($hs_code !='' ){				
							$templateHtml .='<th align="left" class="topborder bottomborder">HSCode</th>';
						}						
						$templateHtml .='<th align="left" class="topborder bottomborder">Price</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Tax Rate</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Tax</th>';
						$templateHtml .='<th align="left" class="topborder bottomborder">Line Cost</th>';
						$templateHtml .='</tr>';
					}
				
					$templateHtml .='<tr>';
					$templateHtml .='   <td align="left">'.$item->SKU.'</td>';
					$templateHtml .='   <td align="left">'.$Title.'</td>';
					$templateHtml .='   <td align="left">'.$item->Quantity.'</td>';
					$templateHtml .='   <td align="left">'.($pro_weight * $item->Quantity).'</td>';
					if($hs_code !='' ){	
						$templateHtml .='   <td align="left">'.$hs_code.'</td>';					
					}
 					   
 					$item_vat_amount = $item->Cost - ($item->Cost/(1 + ($tax_rate/100)));
					$item_cost = $item->Cost - $item_vat_amount;
  
  					$templateHtml .='<td style="" valign="top">'.number_format($item_cost,2).'&nbsp;'.$currency_code.'</td>';
  					$templateHtml .='<td style="" valign="top">'.$tax_rate.'%</td>';
					$templateHtml .='<td style="" valign="top">'.number_format($item_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
 					$templateHtml .='<td style="" valign="top">'.number_format($item->Cost,2).'&nbsp;'.$currency_code.'</td>'; 
					$templateHtml .='</tr>';            
				
			}
			
			$postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
			$postage_cost = $POSTAGE - $postage_vat_amount;
			
			$templateHtml .='<tr>';
			$templateHtml .='<td align="left" colspan="2">SHIPPING & HANDLING FEE</td>'; 
 			$templateHtml .='<td align="left">&nbsp;</td>';
			$templateHtml .='<td align="left">&nbsp;</td>';
			if($hs_code !='' ){	
				$templateHtml .='<td align="left">&nbsp;</td>';					
			}
  			$templateHtml .='<td align="left">'.number_format($postage_cost,2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='<td align="left">'.$tax_rate.'%</td>';
			$templateHtml .='<td align="left">'.number_format($postage_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='<td align="left">'.number_format($POSTAGE,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='</tr>'; 
			
			$final_total =  $item_cost + $postage_cost + $item_vat_amount + $postage_vat_amount;
			
			$templateHtml .='<tr>';
			$templateHtml .='   <td align="left" colspan="2" style="border-top:1px solid #000;border-bottom:1px solid"><strong>TOTAL</strong></td>'; 
			
			$templateHtml .='   <td style="border-top:1px solid #000;border-bottom:1px solid">&nbsp;</td>';
			$templateHtml .='   <td style="border-top:1px solid #000;border-bottom:1px solid">&nbsp;</td>';
			if($hs_code !='' ){	
				$templateHtml .='   <td style="border-top:1px solid #000;border-bottom:1px solid">&nbsp;</td>';					
			}
			$templateHtml .='<td style="border-top:1px solid #000;border-bottom:1px solid">'.number_format(($item_cost + $postage_cost),2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='<td style="border-top:1px solid #000;border-bottom:1px solid">'.$tax_rate.'%</td>';
			$templateHtml .='<td style="border-top:1px solid #000;border-bottom:1px solid">'.number_format(($item_vat_amount + $postage_vat_amount),2).'&nbsp;'.$currency_code.'</td>';
			$templateHtml .='<td style="border-top:1px solid #000;border-bottom:1px solid">'.number_format($final_total,2).'&nbsp;'.$currency_code.'</td>'; 
			$templateHtml .='</tr>';  
 					
			$templateHtml .='</table>';
		
			$templateHtml .='<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;">';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td align="left"><strong>Shipment Weight</strong>: '. number_format($ShipmentWeight,2) .'Kg</td>';	
			$templateHtml .='</tr>';
			
			$templateHtml .='<tr>';
			$templateHtml .='<td>';
			$templateHtml .='<div style="font-size:14px;"><strong>Declaration:</strong></div>
			<div class="row" style="font-size:12px;">We hereby certify that the information contained <br>in this invoice is true and correct and that
the <br>contents of this shipment are as started above.</div>';
			 $templateHtml .='<tr>';
			 $templateHtml .='</tr>';
			 			
			$templateHtml .='</table>';			
			$templateHtml .='</div>';

			

			$templateHtml  .= '<div class="footer row" style="text-align:center; border-bottom:0px;" >
				<b>Thank you for your purchase. Happy Shopping!<br>
				Rainbow Retail</b>';	
			 $templateHtml  .= '</div>';
		}
		else{
 		
 			$itmsHtml  ='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:500px">';
			$itmsHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">';
 			
			$country_code = $this->getCountryCode($Country);
			
			foreach($items as $count => $item){
		    	$b = '';
				if(count($items) >= $count){
					$b = 'border-bottom:1px solid #ccc;';
				} 					
				$pro_weight = 0; 
				$pro_res = $this->Product->find('first', array('conditions' => ['Product.product_sku' => $item->SKU],'fields' => ['ProductDesc.weight']));
 				if(count( $pro_res ) > 0){				
					$pro_weight = $pro_res['ProductDesc']['weight'];
				} 
				$hs_code = '';
			    $hsresult = $this->ProductHscode->find('first', array('conditions' => array('sku' => $item->SKU,'country_code' => $country_code)));
				if(count( $hsresult ) > 0){				
					$hs_code = $hsresult['ProductHscode']['hs_code'];
				}
				
				if( $count == 0){
					
					$itmsHtml  .='<tr>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">SKU</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Item</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Qty</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Weight</th>';
					if( $hs_code != ''){
						$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">HSCode</th>';
					}
 					
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Price</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Tax Rate</th>';				
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Tax</th>';
					$itmsHtml  .='<th style="border-top:2px solid #000;border-bottom:2px solid #000;">Line Total</th>';
					$itmsHtml  .='</tr>';
				}
				
				$Title = $item->Title;
				if(strlen($item->Title) < 10){
					$Title = $item->ChannelTitle;
				}
					  
				$itmsHtml .='<tr>';
					$itmsHtml .=' <td style="'.$b.'" align="left" valign="top">'.$item->SKU.'</td>';
					$itmsHtml .=' <td style="'.$b.'" align="left" valign="top">'.$Title.'</td>';
					$itmsHtml .=' <td style="'.$b.'" valign="top">'.$item->Quantity.'</td>';
					$itmsHtml .=' <td style="'.$b.'" valign="top">'.($pro_weight * $item->Quantity).'</td>';
					if($hs_code != ''){
						$itmsHtml .=' <td style="'.$b.'" align="left" valign="top" >'.$hs_code.'</td>';
					}
					
					$item_vat_amount = $item->Cost - ($item->Cost/(1 + ($tax_rate/100)));
				    $item_cost = $item->Cost - $item_vat_amount;
				  
 					$itmsHtml .=' <td style="'.$b.'" valign="top">'.number_format($item_cost,2).'&nbsp;'.$currency_code.'</td>';	
					$itmsHtml .=' <td style="'.$b.'" valign="top">'.$tax_rate.'%</td>';				 
					$itmsHtml .=' <td style="'.$b.'" valign="top" >'.number_format($item_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
					$itmsHtml .=' <td style="'.$b.'" valign="top" >'.number_format($item->Cost,2).'&nbsp;'.$currency_code.'</td>';
				$itmsHtml .='</tr>';            
       
			}
 			
			$postage_vat_amount = $POSTAGE - ($POSTAGE/(1 + ($tax_rate/100)));
			$postage_cost = $POSTAGE - $postage_vat_amount;
			
			$b_bottom = 'border-bottom:2px solid #000;';  
			$itmsHtml .='<tr>';
				$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top" colspan="2">SHIPPING HANDLING FEE</td>'; 
   				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				if($hs_code != ''){
					$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format($postage_cost,2).'&nbsp;'.$currency_code.'</td>';	
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.$tax_rate.'%</td>';				 
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top" >'.number_format($postage_vat_amount,2).'&nbsp;'.$currency_code.'</td>';
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top" >'.number_format($POSTAGE,2).'&nbsp;'.$currency_code.'</td>';
			$itmsHtml .='</tr>';  
			
 			$final_total =  $item_cost + $postage_cost + $item_vat_amount + $postage_vat_amount;
			  
			$itmsHtml .='<tr>';
				$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top" colspan="2"><strong>TOTAL</strong></td>'; 
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">&nbsp;</td>';
				if($hs_code != ''){
					$itmsHtml .=' <td style="'.$b_bottom.'" align="left" valign="top">&nbsp;</td>';
				}
  				 
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format(($item_cost+$postage_cost),2).'&nbsp;'.$currency_code.'</td>';	
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.$tax_rate.'%</td>';					 
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format(($item_vat_amount+$postage_vat_amount),2).'&nbsp;'.$currency_code.'</td>';
				$itmsHtml .=' <td style="'.$b_bottom.'" valign="top">'.number_format($final_total,2).'&nbsp;'.$currency_code.'</td>';
			$itmsHtml .='</tr>';  
			    
			  
			$itmsHtml .= '</table>';
			
			$itmsHtml .='<table align="right" cellpadding=5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
			$itmsHtml .='<tr>';
			$itmsHtml .='<td align="left"><strong>Shipment Weight</strong>: '. number_format($ShipmentWeight,2) .'Kg</td>';	
 			$itmsHtml .='</tr>';	
			 $itmsHtml .='<tr>';	
			$itmsHtml .='<td align="left" width="33%">'; 
			$itmsHtml .='<div style="font-size:14px;"><strong>Declaration:</strong></div>
			<div class="row" style="font-size:12px;">
We hereby certify that the information contained <br>in this invoice is true and correct and that
the <br>contents of this shipment are as started above.</div>';
			$itmsHtml .='</td>';
			$itmsHtml .='</tr>';
			
			$itmsHtml .='</table>';
			
			$itmsHtml .='</div>';
			
 			 
 		
			 if($result == 'tech'){
  			
				$EORI_Number = '';
				$UK_VAT_Number = ''; 
				$DE_VAT_Number = '';
				$FR_VAT_Number = '';
			
				$templateHtml  ='<!-- TechDrive Invoice -->';
				$templateHtml  .='<div id="label">';
				$templateHtml  .='<div class="container">';
				$templateHtml  .='<table class="header row" style="border-bottom:0px"></table>';
				$templateHtml  .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td  width="60%" valign="top">';
				$templateHtml  .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';
				$templateHtml  .='<td  width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
				$templateHtml  .='</td>';
				$templateHtml  .='<td width="40%" valign="top" align="center"><img src="'.WWW_ROOT.'img/techDriveSupplies2.jpg" width="280px"></td>';
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
				
				$templateHtml  .='<table>';
				$templateHtml  .='<tr>';
				$templateHtml  .='<td><span style="font-weight:bold; font-size:18px">INVOICE(CIF) #'.$OrderId.'</span><br></td>';
  				$templateHtml .='<td><b>EORI No:</b></td>	<td>'.$EORI_Number.'</td>';
				$templateHtml .='<td><b>Invoice(CIF):</b></td>	<td>'.$OrderId.'</td>';
				if($Country == 'United Kingdom' && $UK_VAT_Number != '' ){
					$templateHtml .='<td><b>VAT No:</b></td>	<td>'.$UK_VAT_Number.'</td>';
				}else if($Country == 'Germany' && $DE_VAT_Number != ''){
					$templateHtml .='<td><b>VAT No:</b></td>	<td>'.$DE_VAT_Number.'</td>';
				}
				if($waybill_number != ''){
					$templateHtml .='<td><b>WayBill No:</b></td>	<td>'.$waybill_number.'</td>';
				}
				$templateHtml  .='<td><b>Date:</b> '.$INVOICE_DATE.'</td>';
			
				$templateHtml  .='</tr>';
				$templateHtml  .='</table>';
	
				$templateHtml  .= $itmsHtml;
  					
				$templateHtml .= '<div class="row" style="text-align:center; border-bottom:0px;margin-left:20%; padding:0px" >';
				$templateHtml .= '<div style="width:70%; text-align:center; border:2px solid #000000; padding:10px;" >';
 				$templateHtml .= '<div style="text-align:left; font-size:10px;">
				If however, for any reason you are not entirely happy with your order, please contact us to allow us to rectify any issues before leaving feedback.
				</div>';
				
				$templateHtml .= '</div>';
				$templateHtml .= '</div>';
				$templateHtml .= '</div>';
			
			}		
			elseif($result == 'bbd_'){	
			
				$EORI_Number = '';
				$UK_VAT_Number = ''; 
				$DE_VAT_Number = '';
				$FR_VAT_Number = '';
					
				//$templateHtml ='<!-- BBD_EU Invoice -->';			
				$templateHtml  ='<div id="label" style="margin-top:20px;">';
				$templateHtml .='<div class="container">';			
				$templateHtml .='<table class="header row" style="border-bottom:0px">';
				$templateHtml .='</table>';
				$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
				$templateHtml .='<tr>';
				$templateHtml .='<td  width="75%" valign="top"><img src=http://xsensys.com/img/bbdEu.png></td>';
				$templateHtml .='<td  width="25%" valign="top">			
									Unit A1 21/F, Officeplus Among Kok<br>
									998 Canton Road<br>
									Hong Kong<br>
									KL<br>
									0000			
								</td>';
				$templateHtml .='</tr>';
				$templateHtml .='</table>';
				
				$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
				$templateHtml .='<tr>';
				$templateHtml .='<td valign="top" width="37%"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
				$templateHtml .='<td valign="top" width="38%"><h4><u>Billing Address:</u></h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';
	
				$templateHtml .='<td  width="25%" valign="top">';
				$templateHtml .='<table>';
				$templateHtml .='<tr><td><b>Invoice(CIF):</b></td>	<td>'.$OrderId.'</td></tr>';
				
 				if($Country == 'United Kingdom' && $UK_VAT_Number != '' ){
					$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$UK_VAT_Number.'</td></tr>';
				}else if($Country == 'Germany' && $DE_VAT_Number != ''){
					$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$DE_VAT_Number.'</td></tr>';
				}
				if($waybill_number != ''){
					$templateHtml .='<tr><td><b>WayBill No:</b></td>	<td>'.$waybill_number.'</td></tr>';
				}
				$templateHtml .='<tr><td><b>Invoice Date:</b></td><td>'.$INVOICE_DATE.'</td></tr>';
				
				$templateHtml .='</table>';
				$templateHtml .='</td>';
				$templateHtml .='</tr>';
				$templateHtml .='</table>';
				
				$templateHtml .= $itmsHtml;
 				
 				$templateHtml .='<div class="footer row" style="font-size:15px; text-align:center; border-bottom:0px;" >
				<h2>THANK YOU FOR YOUR PURCHASE</h2> 
				<b>Please take a moment to leave us positive feedback.</b><br>
				If however, for any reason you are not entirely happy with your
				order, please contact us<br>to allow us to rectify any issues before
				leaving feedback.</div>';			
				$templateHtml .='</div>';
						
			}
			elseif($result == 'ebay'){			
				//$templateHtml  ='<!-- eBuyersDirect Invoice -->';			
				$templateHtml  ='<div id="label" style="margin-top:20px;">';
				$templateHtml .='<div class="container">';			
				$templateHtml .='<table class="header row" style="border-bottom:0px">';
				$templateHtml .='</table>';
				$templateHtml .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
				$templateHtml .='<tr>';
				$templateHtml .='<td  width="50%" valign="top">
				4 Norwood Court<br>
	La Rue Militaire<br>
	St John, Jersey Channel Islands JE3 4DP<br>
	United Kingdom
				</td>';
				$templateHtml .='<td  width="50%" valign="top" align="right">
				<img src=http://xsensys.com/img/ebuyerDirect.png width="300px">
				</td>';
				$templateHtml .='</tr>';
				$templateHtml .='</table>';
				$templateHtml .='<hr style="color:#6bc15f">';
				
				$templateHtml .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
				$templateHtml .='<tr>'; 
				
				//mb_convert_encoding($SHIPPING_ADDRESS,'HTML-ENTITIES','UTF-8')			
				$t = mb_convert_encoding($SHIPPING_ADDRESS, "windows-1251", "utf-8");		
				$templateHtml .='<td  valign="top" width="37%"><h4>Delivery Address:</h4>'.$t.'</td>';
				$templateHtml .='<td  valign="top" width="38%"><h4>Billing Address:</h4>'.mb_convert_encoding($BILLING_ADDRESS,"windows-1251", "utf-8").'</td>';
				
				$templateHtml .='<td  width="25%" valign="top">';
				$templateHtml .='<table>';
				$templateHtml .='<tr><td><b>Invoice ID:</b></td><td align="right">'.$OrderId.'</td></tr>';
				if($Country == 'United Kingdom' && $UK_VAT_Number != '' ){
					$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$UK_VAT_Number.'</td></tr>';
				}else if($Country == 'Germany' && $DE_VAT_Number != ''){
					$templateHtml .='<tr><td><b>VAT No:</b></td>	<td>'.$DE_VAT_Number.'</td></tr>';
				}
				if($waybill_number != ''){
					$templateHtml .='<tr><td><b>WayBill No:</b></td>	<td>'.$waybill_number.'</td></tr>';
				}
				
				$templateHtml .='<tr><td><b>Invoice Date:</b></td><td align="right">'.$INVOICE_DATE.'</td></tr>';
				
				
				$templateHtml .='</table>				 
				</td>';
				
 				$templateHtml .='</tr>';
				$templateHtml .='</table>';
				
 				$templateHtml .= $itmsHtml; 
				 
				
				$templateHtml .='<hr style="color:#6bc15f">';
				$templateHtml .='<div class="footer row" style="font-size:15px; text-align:left; border-bottom:0px;" >
				<span style="font-size:28px; font-weight:bold">THANK YOU</span> <i>for using ebuyersDirect-2u</i><br><br>
				<b>Please take a moment to leave us positive feedback.</b><br>
				If however, for any reason you are not entirely happy with your
				order, please contact us to allow us to rectify<br>any issues before
				leaving feedback.</div>';
				$templateHtml .='</div>';
			}
			elseif($result == 'http'){		
			$templateHtml  ='<!-- storeforlife Invoice -->';			
			$templateHtml  .='<div id="label">';
			$templateHtml  .='<div class="container">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px"></table>';
			$templateHtml  .='<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 10px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  width="60%" valign="top">';
			$templateHtml  .='<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td  valign="top" width="50%"><h4><u>Billing Address:</u></h4>'.$BILLING_ADDRESS.'</td>';
			$templateHtml  .='<td  width="50%" valign="top"><h4><u>Delivery Address:</u></h4>'.$SHIPPING_ADDRESS.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			$templateHtml  .='</td>';
			$templateHtml  .='<td  width="40%" valign="top" align="center">
			<img src="http://xsensys.com/img/sfl_logo.png"><br>			
			36 HARBOUR REACH, LA RUE DE CARTERET<br> ST HELIER, JERSEY, JE2 4HR</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';
			
			$templateHtml  .='<table>';
			$templateHtml  .='<tr>';
			$templateHtml  .='<td width="80%"><span style="font-weight:bold; font-size:32px">INVOICE #'.$OrderId.'</span><br></td>';
			$templateHtml  .='<td width="20%" align="right"><b>Date:</b> '.$INVOICE_DATE.'</td>';
			$templateHtml  .='</tr>';
			$templateHtml  .='</table>';

			$templateHtml  .='<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px;height:650px">';
			$templateHtml  .='<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style="border-collapse:collapse; margin:0px;">
            <tr>
               <th style="border-top:3px solid #000;" align="left" class=" " width="14%">SKU</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="28%" class=" ">Item</th>
               <th style="border-top:3px solid #000;" valign="top" class=" " width="8%">Quantity</th>
               <th style="border-top:3px solid #000;" align="left" class=" " width="6%">Unit</th>';
			   
			   if($final_total < $lvcr_limit){
              	 $templateHtml  .='<th style="border-top:3px solid #000;" align="left" valign="top" width="8%" class=" ">Tax Rate</th>';
               	 $templateHtml  .='<th style="border-top:3px solid #000;" valign="top" class=" " width="5%">Tax</th>';
			   }
			   
               $templateHtml  .='<th style="border-top:3px solid #000;" align="left" class=" " width="12%">Cost</th>
               <th style="border-top:3px solid #000;" align="left" valign="top" width="9%" class=" ">Line Cost</th>
            </tr>';
				foreach($items as $item){
				
				$Title = $item->Title;
				if(strlen($item->Title) < 10){
					$Title = $item->ChannelTitle;
				}
					
				  $templateHtml .='<tr style="background-color:#f2f2f2">
					   <td style="" align="left" valign="top" class="">'.$item->SKU.'</td>
					   <td style="" align="left" valign="top" class="">'.$Title.'</td>
					   <td style="" valign="top">'.$item->Quantity.'</td>
					   <td style="" valign="top">'.$item->PricePerUnit.'</td>';
					   if($final_total < $lvcr_limit){
					    	$templateHtml .=' <td style="" valign="top">'.$item->TaxRate.'</td>';
					   		$templateHtml .=' <td style="" valign="top">'.$item->Tax.'</td>';
					   }
					    $templateHtml .=' <td style="" valign="top">'.$item->Cost.'</td>
					   <td style="" valign="top">'.$item->CostIncTax.'</td>
					</tr>';            
       
				}
			$templateHtml .= '</table>';
		 
		
			$templateHtml .= '<table align="right" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-top:1px solid #000;border-bottom:3px solid #000;" align="left" rowspan="4" width="60%">';		
			$templateHtml .= '<div style="border-bottom:0px;" ><b>Thank you for your purchase. Happy Shopping!<br>StoreForLife</b></div>';
			$templateHtml .= '</th>';
			$templateHtml .= '<th style="border-top:1px solid #000;" align="left" width="25%">SUB TOTAL:</th>';
			$templateHtml .= '<td style="border-top:1px solid #000;" align="right" width="15%">'.$SUB_TOTAL.'</td>';
			$templateHtml .= '</tr>';
	
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >POSTAGE(Inc VAT):</th>';
			$templateHtml .= '<td style="" align="right">'.$POSTAGE.'</td>';
			$templateHtml .= '</tr>';
			
			/*$templateHtml .= '<tr>';
			$templateHtml .= '<th style="" align="left" >TAX:</th>';
			$templateHtml .= '<td style="" align="right">'.$TAX.'</td>';
			$templateHtml .= '</tr>';*/
		 
			$templateHtml .= '<tr>';
			$templateHtml .= '<th style="border-bottom:3px solid #000;" align="left" >TOTAL INCLUDING VAT:</th>';
			$templateHtml .= '<td style="border-bottom:3px solid #000;" align="right">'.$TOTAL.'</td>';
			$templateHtml .= '</tr>';
			
			$templateHtml .= '</table>';
			$templateHtml .= '</div>';
				
			$templateHtml .= '<div class="row" style="text-align:center; border-bottom:0px;margin-left:20%; padding:0px" >';
			$templateHtml .= '<div style="width:70%; text-align:center; border:2px solid #000000; padding:10px;" >';
	
			$templateHtml .= '<table style="padding:0; text-align:center"><tr>
			<td width="15%"><img src="http://xsensys.com/img/happy_g.png" width="80px"></td>
			<td width="85%" align="center"><span style="font-size:24px">Happy with your order?</span> <br>
			<span style="font-size:10px;">Please take a moment to leave us positive feedback.</span><br>
			<img src="http://xsensys.com/img/5star_g.png" width="120px"><br></td>
			</tr>
			</table>';
			$templateHtml .= '<div style="text-align:left; font-size:10px;">
			If however, for any reason you are not entirely happy with your order, please contact us to allow us to rectify any issues before leaving feedback.
			</div>';
			
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
			$templateHtml .= '</div>';
			
		}
		}
		
				
		return $templateHtml;	
	}
	
	
	public function getDHLInvoice($order_id = null,$waybill_number = null){
				 
		$this->loadModel('MergeUpdate');
		$order = $this->getOrderByNumIdDhl( $order_id );	
		 
 
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();	
		$result 		=	'';
		$htmlTemplate	=	'';	

		$htmlTemplate	=	$this->getTemplateDHL($order,$waybill_number);
		$SubSource		=	$order->GeneralInfo->SubSource;
	
		$result			=	ucfirst(strtolower(substr($SubSource, 0, 4)));
		
		/**************** for tempplate *******************/
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 <meta content="" name="description"/>
			 <meta content="" name="author"/>
			 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
	 	$html .= '<body>'.$htmlTemplate.'</body>';
	 		
		$name	= $order_id.'.pdf';							
		$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
		$dompdf->render();			
						
		$file_to_save = WWW_ROOT .'img/dhl/invoice_'.$name;
		 
		//save the pdf file on the server
		file_put_contents($file_to_save, $dompdf->output()); 			 
		//$dhl_path = WWW_ROOT .'dhl/invoice_'.$name; 
		//file_put_contents($dhl_path, $dompdf->output()); 
			 
		exit;		 
			
	}	
	public function Address(){
	
		$this->loadModel('InvoiceAddress');

		$data['order_id'] 	= $this->request->data['order_id'];
		$field 				= $this->request->data['field'];
		$data[$field] 		= $this->request->data['val'];
				
		if($this->Auth->user('username')){
			$data['username']  = $this->Auth->user('username');
		}
		
		$conditions = array('InvoiceAddress.order_id' => $this->request->data['order_id']);
														
		if ($this->InvoiceAddress->hasAny($conditions)){
			foreach($data as $field => $val){
				$DataUpdate[$field] = "'".$val."'";								
			}
			$this->InvoiceAddress->updateAll( $DataUpdate, $conditions );
			$msg['msg'] = 'updated';
		}else{
			$this->InvoiceAddress->saveAll($data);
			$msg['msg'] = 'saved';			
		}
	echo json_encode($msg);
	exit;
	
	}
	public function getCountryCode($Country = null){
		$code = '';
		if($Country == 'United Kingdom'){
			$code = 'gb';
		}else if($Country == 'Germany'){
			$code = 'de';
		}else if($Country == 'France'){
			$code = 'fr';
		}else if($Country == 'Italy'){
			$code = 'it';
		}else if($Country == 'Spain'){
			$code = 'es';
		}else if($Country == 'Australia'){
			$code = 'au';
		}else if($Country == 'Canada'){
			$code = 'ca';
		}else if($Country == 'United States'){
			$code = 'us';
		}
		return $code;
	}
	public function getCurrencyCode($currency  = null){
		$code = $currency;
		if($currency  == 'GBP'){
			$code = '&pound;';
		}else if($currency  == 'EUR'){
			$code = '&euro;';
		}else if($currency  == 'USD'){
			$code = '&dollar;;';
		}else if($currency  == 'CAD'){
			$code = '&dollar;';
		} 
		return $code;
	}
	public function replaceFrenchChar($string = null){
			
		$unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E','Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Nº'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U','Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c','è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n','nº'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o','ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y','ü'=>'u','º'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}	
	
  	
}

?>

