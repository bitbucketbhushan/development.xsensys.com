<?php

	//Gobals vars
	/**
	 * DHL is an API to DHL shipping services
	 * @author Harish Chauhan
	 * @access Public
	 * @copyright Freeware
	 * @name DHL
	 */

	define( 'XML_ERROR_OFFSET',  2000 );

	//Normal services
	define('DHL_WORLDWIDE_EXPRESS','P');
	define('DHL_WORLWIDE_EXPRESS','E');
	define('DHL_SERVICE_NEXT_AFTERNOON','N');
	define('DHL_SERVICE_SECOND_DAY','S');
	define('DHL_SERVICE_GROUND','G');

	//Special services
	define('DHL_SERVICE_HOLD_AT_DHL','HAA'); //If the receiver will be picking up the shipment from the destination DHL station. ID will be required at the time of pickup
	define('DHL_SERVICE_HAZARDOUS_MATERIALS ','HAZ'); //DHL accepts some classes of hazardous materials for shipment.
	define('DHL_SERVICE_SAT ','SAT'); //-Express Saturday is represented in the API as a service type of "E" combined with a special service type of "SAT."  If this special service is included for a non-Express service type, it will be ignored
	define('DHL_SERVICE_EXPRESS_1030 ','1030'); //Express 10:30 AM is represented in the API as a service type of "E" combined with a special service type of "1030."  If this special service is included for a non-Express service type, it will be ignored

	//Speacial types
	define('DHL_REQUEST_WORLD_WIDE_CAP_QUOTE','Capability_Quote');
	define('DHL_REQUEST_WORLD_WIDE_SHIP_REG','Shipment_label');
	
	//Shipment type

	define('DHL_SHIP_PACKAGE','P');
	define('DHL_SHIP_LETTER','L');

	//Shipping Billing Type
	define('DHL_BILL_SENDER','S');
	define('DHL_BILL_RECEIVER','R');
	define('DHL_BILL_THIRD_PARTY','T');

	//Additional Protection 
	define('DHL_ASSET_PROTECTION','AP'); //All Risk Asset Protection is available for shipments beyond the published Limit of Liability.
	define('DHL_DECLARED_VALUE','DV'); //Asset Protection is available for shipments, beyond the published Limit of Liability. This is not All Risk Protection. 
	define('DHL_NOT_REQUIRED','NR'); // No additional protection is requested.

	//Collect on delivier
	define('DHL_COD_MONEY_ORDER','M'); //Money Order
	define('DHL_COD_CHECK','P'); //Personal or Company Check
	
class DhlPostalsController extends AppController
{
    
    var $name = "DhlPostals";    
    var $components = array('Session', 'Common', 'Upload','Auth');    
    var $helpers = array('Html','Form','Common','Session');
    
    var $_userid	= "";
	var $_pwd		= "";
	var $_shipkey 	= "";
	var $_actnumber = "";
	var $_zipcode	= "";
	var $_error		= "";
	var $_isTest	= false;

	var $_url = "";
	var $_testurl = "";
	
	var $_parser	= "";
	var $_result	= "";
	
	var $xmlData;
	var $currentTag;
	var $_xml = null;
	
	var $dhlService = "";
	var $dhlSpecialService="";
	var $dhlShipType="";
	var $dhlBillType="";
	var $_parentnode = null;
	
	var $_responseStore_ = "";
	var $_responseKeyUrl_ = "";
	var $_innerResponse = "";
	
	var $_outer_reponse_for_quote = "";
	var $_inner_reponse_for_quote = "";
	/*
	 * 
	 * 
	 * Dhl public start index
	 * 
	 * 
	 */ 
	public function dhlServiceStart( $typeOffset = null , $reqType = null, $product_details = null )
	{
		
		$this->layout = '';
		$this->autoRender = false;				
		date_default_timezone_set('Europe/Jersey');
		
		//Dhl service processing start and authentication
		try
		{
			$this->_innerResponse = $this->myDhlRequest( $typeOffset );
		}
		catch( Exception $textMessage )
		{
			throw new Exception( "Please check initial authentication!!" );
		}
		
		//set dhl service for initial purpose on the filter basis
		if( $this->_innerResponse == 1 )
		{			
			//Success then do stuff
			$this->dhlService = DHL_WORLDWIDE_EXPRESS;			
		}
		else if( $this->_innerResponse == 0 )
		{
			throw new Exception( "Opps, something went wrong in authentication...!" );
		}
		
		//setup xml request according to need
		$this->_outer_reponse_for_quote = $this->initialHeaderOfXml( $reqType , $product_details);
		
	return $this->_outer_reponse_for_quote;	
	}
			
    /********************************************************************************
	*** DHL Public / protected Functions for php package manager 				*****
	/********************************************************************************/
	
	public function myDhlRequest( $typeOffset = null )
	{
		
		//controller invocation
		/*App::import( 'Component' , 'DhlService' );
		$objDhlCompo = new DhlServiceComponent(new ComponentCollection());
		$this->_responseStore_ = $objDhlCompo->getUrlByType( $typeOffset );*/
		
		if( $typeOffset == 1 ){
			$type = "sandbox" ;
		}else if( $typeOffset == 2 ){  
			$type = "production" ;
		} 
		
		$this->_url = $this->_responseKeyUrl_ = $this->verifyDhlUrl( $type );
		
		//set user authentication
		$this->_userid = $this->getDhlAcountUser( $typeOffset );
		
		//set pass authentication
		$this->_pwd = $this->getDhlAcountPass($typeOffset);
		
		//set account authentication
		$this->_actnumber = $this->getDhlAccountId($typeOffset);
		
		if( $this->_actnumber != '' )
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
	
	/*************** Public access *****************/
	/*
	 * Version 1.0 
	 * @params
	 * initial xml setup
	 * 
	 */
	public function initialHeaderOfXml( $reqType = null , $_details = null)
	{
			
		//create xml request for dhl on the bais of dhl
		if( $reqType == "DHL_REQUEST_WORLD_WIDE_CAP_QUOTE" )
		{
			$this->_xml .= $this->setInitalXmlByRequestType( $reqType );
			return $this->createCompleteXmlForCap( "cap_quote", $_details );
		}
		
		if( $reqType == "DHL_REQUEST_WORLD_WIDE_SHIP_REG" )
		{
			$this->_xml .= $this->setInitalXmlByRequestType( $reqType );
			return $this->createCompleteXmlForShip( "ship", $_details );
		}	
	
	}
	
	/*
	 * 
	 * Version 1.0
	 * @params, xml creation
	 * on the basis of dhl
	 * 
	 */ 
	public function createCompleteXmlForShip( $strType = null, $Details = null )
	{
		
		//insider invocation for help		
		$this->_xml .= $this->shipRequestForRequestElem( $this->_userid , $this->_pwd );
		
		//set reqion elem
		$this->_xml .= $this->shipRequestForRegionElem( 'EU' );
		
		//set reqest pick time elem
		$this->_xml .= $this->shipRequestForReqPickTimenElem();
		
		//set reqest shipper elem
		$this->_xml .= $this->shipRequestForReqNewShipperElem();
		
		//set reqest biller elem
		$this->_xml .= $this->shipRequestForReqBillerElem( $this->_actnumber,$Details);
		
		//set reqest consignee / receiver elem
		$this->_xml .= $this->shipRequestForReqConsigneeDetailElem($Details);
		
		//set reqest duty / reference elem
		$this->_xml .= $this->shipRequestForReqDuty_ReferenceElem( $Details);
		
		//set reqest shipment elem
		$this->_xml .= $this->shipRequestForReqShipmentElem($Details);
		
		//set reqest shipper elem
		$this->_xml .= $this->shipRequestForReqShipperDetailsElem($Details);
		
		//Channel Islands Special Service
		$this->_xml .= $this->shipRequestForReqSpeacialServiceElem();
		
		//set reqest label elem
		$this->_xml .= $this->shipRequestForReqLabelElem();
		
		
		
		//set reqest end elem
		$this->_xml .= $this->shipRequestEndElem();
		
		(string)$this->_xml = (string)$this->_xml;
		
		//cron active and alive
		$this->_inner_reponse_for_quote = $this->processShippmentXml( $this->_url , $this->_xml, $Details );
		
	return $this->_inner_reponse_for_quote;	
	}
	
	/*
	 * 
	 * Version 1.0
	 * @params, xml creation
	 * on the basis of dhl
	 * 
	 */ 
	public function createCompleteXmlForCap( $strType = null , $details = null)
	{
		
		//insider invocation for help
		$this->_xml .= $this->capRequestForRequestElem( $this->_userid , $this->_pwd );
		
		//from elem
		$this->_xml .= $this->capRequestForFromElem();
		
		//bkg elem
		$this->_xml .= $this->capRequestForBkgElem($details);
		
		//receiver to elem
		$this->_xml .= $this->capRequestForToElem($details);
		
		//bkg End to elem
		$this->_xml .= $this->capRequestForBkgEndElem();
		
		//xml end request elem
		$this->_xml .= $this->capRequestEndElem();
		
		(string)$this->_xml = (string)$this->_xml;
		
		//cron active and alive
		$this->_inner_reponse_for_quote = $this->processRatesXml( $this->_url , $this->_xml, $details);
		
	return $this->_inner_reponse_for_quote;	
	}
    
    /*
     * 
     * Version 1.0
     * Shipping Rates
     * 
     */ 
    /*
	 * 
	 * Version 1.0
	 * @params, create complete xml but in partial
	 * return xml elements
	 * 
	 */ 
	public function capRequestForRequestElem( $user = null , $pass = null )
	{
		$xml = '';
		$xml .= '<Request>';
			$xml .= '<ServiceHeader>';
			$xml .= '<MessageTime>' . $this->getCurrentLocationDateWithTime() . '</MessageTime>';
			$xml .= '<MessageReference>1234567890123456789012345678901</MessageReference>';
			$xml .= '<SiteID>' . $user . '</SiteID>';
			$xml .= '<Password>' . $pass . '</Password>';			
		  $xml .= '</ServiceHeader>';
		$xml .= '</Request>';
		
	return $xml;
	}
	
	/*
	 * Version 1.0
	 * @params, create complete xml but in partial
	 * return xml elements
	 * 
	 */ 
	public function capRequestForFromElem( $fromDetails = null )
	{
		$xml = '';
		$xml .= '<From>';
		  $xml .= '<CountryCode>JE</CountryCode>';
		  $xml .= '<Postalcode>JE4 9RJ</Postalcode>';
		  $xml .= '<City>ST CLEMENT</City>';
		$xml .= '</From>';
		
	return $xml;
	}
	
	/*
	 * Version 1.0
	 * @params, create complete xml but in partial
	 * return xml elements
	 * 
	 */ 
	public function capRequestForBkgElem( $details = null )
	{
		
		$dt = new DateTime();
		$date = $dt->format('Y-m-d');
		
		$xml = '';
		$xml .= '<BkgDetails>';	
		  $xml .= '<PaymentCountryCode>JE</PaymentCountryCode>';		  
		  $xml .= '<Date>' . $date .  '</Date>';
		  $xml .= '<ReadyTime>PT10H21M</ReadyTime>';
		  $xml .= '<ReadyTimeGMTOffset>+01:00</ReadyTimeGMTOffset>';
		  $xml .= '<DimensionUnit>CM</DimensionUnit>';
		  $xml .= '<WeightUnit>KG</WeightUnit>';
		  $xml .= '<Pieces>';
			$xml .= '<Piece>';
			  $xml .= '<PieceID>1</PieceID>';
			  $xml .= '<Height>'.$details['height'].'</Height>';
			  $xml .= '<Depth>10</Depth>';
			  $xml .= '<Width>10/Width>';
			  $xml .= '<Weight>10</Weight>';
			$xml .= '</Piece>';
		  $xml .= '</Pieces>';
		  $xml .= '<PaymentAccountNumber>418735392</PaymentAccountNumber>';
		  $xml .= '<IsDutiable>Y</IsDutiable>';
		  $xml .= '<NetworkTypeCode>AL</NetworkTypeCode>';
		$xml .= '</BkgDetails>';
		//130000279
		//418735392
		//file_put_contents(WWW_ROOT."img/client_dhl/labels/RequestForBkg.log",$xml."\n\n", FILE_APPEND | LOCK_EX);
	return $xml;
	
	
	}
	
	/*
	 * Version 1.0
	 * @params, create complete xml but in partial
	 * return xml elements
	 * 
	 */ 
	public function capRequestForToElem( $Details = null )
	{
		$xml = '';
		$xml .= '<To>';
		  $xml .= '<CountryCode>'.$Details['CountryCode'].'</CountryCode>';
		  $xml .= '<Postalcode>'.$Details['PostCode'].'</Postalcode>';
		  $xml .= '<City>'.$Details['Town'].'</City>';
		$xml .= '</To>';
		
		if($Details['order_id']=='1209011-1'){
			$Details['order_currency'] == 'EUR';
		}
		
		$final_sub_total = $Details['sub_total'] ;
		if($Details['order_currency'] == 'GBP'){
			$final_sub_total = $Details['sub_total'] * (1.122);
		}		
		if($final_sub_total < 17.99){
			$Details['postage_cost'] = 4;
		}
					
		$xml .= '<Dutiable>';
		  $xml .= '<DeclaredCurrency>'.$Details['order_currency'].'</DeclaredCurrency>';
		  $xml .= '<DeclaredValue>'.number_format(($Details['sub_total'] + $Details['postage_cost']),2).'</DeclaredValue>';
		$xml .= '</Dutiable>';
		
	return $xml;
	}
	
	/*
	 * Version 1.0
	 * @params, create complete xml but in partial
	 * return xml elements
	 * 
	 */ 
	public function capRequestForDutyElem( $Details = null )
	{
		
		if($Details['order_id']=='1209011-1'){
			$Details['order_currency'] == 'EUR';
		}
		
		$final_sub_total = $Details['sub_total'] ;
		if($Details['order_currency'] == 'GBP'){
			$final_sub_total = $Details['sub_total'] * (1.122);
		}
		if($final_sub_total < 17.99){
			$Details['postage_cost'] = 4;
		}
			
		$xml = '';		
		$xml .= '<Dutiable>';
		  $xml .= '<DeclaredCurrency>'.$Details['order_currency'].'</DeclaredCurrency>';
		  $xml .= '<DeclaredValue>'. number_format(($Details['sub_total'] + $Details['postage_cost']),2).'</DeclaredValue>';
		$xml .= '</Dutiable>';
		
	return $xml;
	}
	
	/*
	 * Version 1.0
	 * @params, create complete xml but in partial
	 * return xml elements
	 * 
	 */ 
	public function capRequestForBkgEndElem()
	{
		$xml = '';
		$xml .= '</GetQuote>';
		
	return $xml;
	}
	
	/*
	 * Version 1.0
	 * @params, create complete xml but in partial
	 * return xml elements
	 * 
	 */ 
	public function capRequestEndElem()
	{
		$xml = '';
		$xml .= '</p:DCTRequest>';
		
	return $xml;
	}
	
	/*
	 * Version 1.0
	 * @param, cron access
	 * return appropriate response regarding rates
	 * 
	 */
	public function processRatesXml( $accessDhlServerUrl = null , $dhlShipXmlRequest = null,  $details = null)
	{
		
		//inittialize
		$curlInititiate = curl_init();
		
		if( !$curlInititiate )
		{
			$this->_error = "Curl has issue to open, please correct your curl library...!";
			return false;
		}
		else
		{
			
			curl_setopt($curlInititiate, CURLOPT_URL , $accessDhlServerUrl);
			curl_setopt($curlInititiate, CURLOPT_POST, 1);
			curl_setopt($curlInititiate, CURLOPT_HEADER, 0);
			curl_setopt($curlInititiate, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curlInititiate, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curlInititiate, CURLOPT_BINARYTRANSFER, 1);
			curl_setopt($curlInititiate, CURLOPT_POSTFIELDS , $dhlShipXmlRequest);
			curl_setopt($curlInititiate, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($curlInititiate);
			
			if(curl_error($curlInititiate) != "")
			{
				echo $this->_error= "Error In Curl installation: " . curl_error($curlInititiate) . "<br>";
				return false;
			}
			else
			{
				curl_close($curlInititiate);
				
				if( isset($response) && $response != '' )
				{					
					//location				
					$responseXmlPath = WWW_ROOT .'img/orders/response/'.$details['order_id'].'.txt'; 
					file_put_contents( $responseXmlPath , $response );			
					$xml = simplexml_load_string( $response ); 
					$xml = json_decode( json_encode($xml) ,0);
				}
				else
				{
					$xml = 'error';
				}
				
			return $xml;					
			}		
		}		
	}
	
	/*
	 * 
	 * Version 1.0
	 * shippment book
	 * 
	 */
		/*
	 * Version 1.0
	 * @params nothing
	 * return current location time
	 * 
	 */
	public function getCurrentLocationDateWithTime()
	{
		$dt = new DateTime();
		return str_replace(" ", "T" ,$dt->format('Y-m-d H:i:s-05:00'));
	} 
	  
	/*
	 * Version 1.0
	 * @params need set initial xml header	 
	 * 
	 */ 
	public function setInitalXmlByRequestType( $reqType = null )
	{
		
		if( $reqType == "DHL_REQUEST_WORLD_WIDE_CAP_QUOTE" )
		{
			return $this->setupXml( "cap_quote" );
		}
		
		if( $reqType == "DHL_REQUEST_WORLD_WIDE_SHIP_REG" )
		{
			return $this->setupXml( "ship" );
		}
		
	}
	
	/*
	 * Version 1.0
	 * @params, setup xml for initial request
	 * 
	 * 
	 */ 
	private function setupXml( $strText = null )
	{
		
		$xml = '';
		if( $strText == "ship" )
		{
			$xml .= '<?xml version="1.0" encoding="utf-8"?>';
			$xml .= '<req:ShipmentRequest xmlns:req="http://www.dhl.com"
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com
				ship-val-global-req.xsd" schemaVersion="4.0">';
		return $xml;
		}
		
		if( $strText == "cap_quote" )
		{
			$xml .= '<?xml version="1.0" encoding="UTF-8"?>';
			$xml .= '<p:DCTRequest xmlns:p="http://www.dhl.com" xmlns:p1="http://www.dhl.com/datatypes" xmlns:p2="http://www.dhl.com/DCTRequestdatatypes" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com DCT-req.xsd ">';
			$xml .= '<GetQuote>';			
		return $xml;	
		}
		
	}
	
	/*
	 * Version 1.0
	 * @params, create complete xml but in partial
	 * return xml elements
	 * 
	 */ 
	public function shipRequestForRequestElem( $user = null , $pass = null )
	{
		$xml = '';
		$xml .= '<Request>';
        $xml .= '<ServiceHeader>';
            $xml .= '<MessageTime>' . $this->getCurrentLocationDateWithTime() . '</MessageTime>';            
            $xml .= '<MessageReference>ShpVal_GL_withLabelImage_____</MessageReference>';
            $xml .= '<SiteID>' . $user . '</SiteID>';
            $xml .= '<Password>' . $pass . '</Password>';
        $xml .= '</ServiceHeader>';
		$xml .= '</Request>';
		
	return $xml;
	}
	
	/*
	 * Version 1.0
	 * @params, setup region elem
	 * set region elem
	 */ 
	public function shipRequestForRegionElem( $regionCode = null )
	{
		
		$xml = '';
		$xml .= '<RegionCode>' . $regionCode . '</RegionCode>';
	return $xml;	
	}
	
	/*
	 * Version 1.0
	 * @params, setup req pick time elem
	 * set region elem
	 */ 
	public function shipRequestForReqPickTimenElem()
	{
		
		$xml = '';
		$xml .= '<RequestedPickupTime>Y</RequestedPickupTime>';
	return $xml;	
	}
	
	/*
	 * Version 1.0
	 * @params, setup req new shipper elem
	 * set region elem
	 */ 
	public function shipRequestForReqNewShipperElem()
	{
		
		$xml = '';
		$xml .= '<NewShipper>N</NewShipper>';
		$xml .= '<LanguageCode>en</LanguageCode>';
		$xml .= '<PiecesEnabled>Y</PiecesEnabled>';
	return $xml;	
	}
	
	/*
	 * Version 1.0
	 * @params, setup req billing elem
	 * set region elem
	 */ 
	public function shipRequestForReqBillerElem( $actNumber = null, $Details = null )
	{
		
		$xml = '';
		$xml .= '<Billing>';
			$xml .= '<ShipperAccountNumber>' . $actNumber . '</ShipperAccountNumber>';
			$xml .= '<ShippingPaymentType>S</ShippingPaymentType>';
			
			$final_sub_total = $Details['sub_total'] ;
			if($Details['order_currency'] == 'GBP'){
				$final_sub_total = $Details['sub_total'] * (1.122);
			}		
			if($final_sub_total > 17.99 ){
				$xml .= '<DutyPaymentType>S</DutyPaymentType>';
				$xml .= '<DutyAccountNumber>418735392</DutyAccountNumber>';	
			}
		$xml .= '</Billing>';
	return $xml;	
	}
	
	/*
	 * Version 1.0
	 * @params, setup req consignee / receiver details elem
	 * set region elem
	 */ 
	public function shipRequestForReqConsigneeDetailElem( $Details = null )
	{
			
		$xml = '';
		$xml .= '<Consignee>';
		$Company = ($Details['Company']!='') ? $Details['Company'] : $Details['FullName'];
	 	$xml .= '<CompanyName>'.htmlentities($this->replaceFrenchChar($Company)).'</CompanyName>';	
		
		$lines = explode("\n", wordwrap(htmlentities($this->replaceFrenchChar($Details['Address1'])).' '.htmlentities($this->replaceFrenchChar($Details['Address2'])), '25'));
		
if(isset($lines[0]) && $lines[0] != ''){
				$xml .= '<AddressLine>'.$lines[0].'</AddressLine>';
			}
			if(isset($lines[1]) && $lines[1] != ''){
				$xml .= '<AddressLine>'.$lines[1].'</AddressLine>';
			}
			if(isset($lines[2]) && $lines[2] != ''){
				$xml .= '<AddressLine>'.$lines[2].'</AddressLine>';
			}
				
		//$xml .= '<AddressLine>'.htmlentities($this->replaceFrenchChar($Details['Address1'])).'</AddressLine>';
	//	$xml .= '<AddressLine>'.htmlentities($this->replaceFrenchChar($Details['Address2'])).'</AddressLine>';
		$xml .= '<City>'.htmlentities($this->replaceFrenchChar($Details['Town'])).'</City>';
		if($Details['Region']!='')	$xml .= '<Division>'.$this->replaceFrenchChar($Details['Region']).'</Division>';
		//$xml .= '<DivisionCode>NY</DivisionCode>';
		$xml .= '<PostalCode>'.$Details['PostCode'].'</PostalCode>';
		$xml .= '<CountryCode>'.$Details['CountryCode'].'</CountryCode>';
		$xml .= '<CountryName>'.$Details['CountryName'].'</CountryName>';
			$xml .= '<Contact>';
				$xml .= '<PersonName>'.htmlentities($this->replaceFrenchChar($Details['FullName'])).'</PersonName>';
				$PhoneNumber = ($Details['PhoneNumber']!='') ? $Details['PhoneNumber'] : '1234567890';
				$xml .= '<PhoneNumber>'.$PhoneNumber.'</PhoneNumber>';				
			$xml .= '</Contact>';
		$xml .= '</Consignee>';
		//file_put_contents(WWW_ROOT."img/client_dhl/labels/xml.log",print_r($xml, true)."\n\n", FILE_APPEND | LOCK_EX);
		return $xml;	
	}
	
	/*
	 * Version 1.0
	 * @params, setup req dutieble / reference elem
	 * set region elem
	 */ 
	public function shipRequestForReqDuty_ReferenceElem(   $Details = null )
	{
		if($Details['order_id']=='1209011-1'){
			$Details['order_currency'] == 'EUR';
		}
		//( $value == '' ) ? $value = '100.00' : $value;
		//( $currency == '' ) ? $currency = 'USD' : $currency;
		$final_sub_total = $Details['sub_total'] ;
		if($Details['order_currency'] == 'GBP'){
			$final_sub_total = $Details['sub_total'] * (1.122);
		}
		if($final_sub_total < 17.99){
			$Details['postage_cost'] = 4;
		}
		
		$xml = '';
		$xml .= '<Dutiable>';
			$xml .= '<DeclaredValue>'.number_format(($Details['sub_total'] + $Details['postage_cost']),2).'</DeclaredValue>';
			$xml .= '<DeclaredCurrency>'.$Details['order_currency'].'</DeclaredCurrency>';
		$xml .= '</Dutiable>';
		$xml .= '<Reference>';
			$xml .= '<ReferenceID>'.$Details['order_id'].'</ReferenceID>';
		$xml .= '</Reference>';
	return $xml;	
	}
	
	/*
	 * Version 1.0
	 * @params, setup req shipment elem
	 * set region elem
	 */ 
	public function shipRequestForReqShipmentElem( $Details = null  )
	{
		$dt = new DateTime();
		$date = $dt->format('Y-m-d');
		
		$xml = '';
		$xml .= '<ShipmentDetails>';
			 
				if(isset($Details['pieces']) && is_array($Details['pieces'])){
					$i = 0;
					$xml .= '<NumberOfPieces>'.count($Details['pieces']).'</NumberOfPieces>';
					$xml .= '<Pieces>';
					foreach($Details['pieces'] as $p){
					
						$i++;
						$xml .= '<Piece>';
						$xml .= '<PieceID>'.$i.'</PieceID>';
						$xml .= '<PackageType>EE</PackageType>';
						$xml .= '<Weight>'.$p['weight'].'</Weight>';
						$xml .= '<DimWeight>0.2</DimWeight>';
						$xml .= '<Width>10</Width>';
						$xml .= '<Height>10</Height>';
						$xml .= '<Depth>10</Depth>';
						$xml .= '</Piece>';
					}
					$xml .= '</Pieces>';
				
				}else{
					$xml .= '<NumberOfPieces>1</NumberOfPieces>';
					$xml .= '<Pieces>';
					$xml .= '<Piece>';
					$xml .= '<PieceID>1</PieceID>';
					$xml .= '<PackageType>EE</PackageType>';
					$xml .= '<Weight>'.$Details['weight'].'</Weight>';
					$xml .= '<DimWeight>'.$Details['weight'].'</DimWeight>';
					$xml .= '<Width>10</Width>';
					$xml .= '<Height>10</Height>';
					$xml .= '<Depth>10</Depth>';
					$xml .= '</Piece>';
					$xml .= '</Pieces>';
				}				
			
			$xml .= '<Weight>'.$Details['weight'].'</Weight>';
			$xml .= '<WeightUnit>K</WeightUnit>';
			$xml .= '<GlobalProductCode>P</GlobalProductCode>';
			$xml .= '<LocalProductCode>P</LocalProductCode>';
			$xml .= '<Date>' . $date . '</Date>';
			$xml .= '<Contents>'. htmlentities(str_replace("<br>"," ",$Details['products'])).'</Contents>';
			$xml .= '<DoorTo>DD</DoorTo>';
			$xml .= '<DimensionUnit>C</DimensionUnit>';
			//$xml .= '<InsuredAmount>10.00</InsuredAmount>';
			$xml .= '<PackageType>EE</PackageType>';
			$xml .= '<IsDutiable>Y</IsDutiable>';
			$xml .= '<CurrencyCode>'.$Details['order_currency'].'</CurrencyCode>';
		$xml .= '</ShipmentDetails>';
	return $xml;	
	}
	
	/*
	 * Version 1.0
	 * @params, setup req shipper elem
	 * set region elem
	 */ 
	public function shipRequestForReqShipperDetailsElem( $Details = null )
	{
		$dt = new DateTime();
		$date = $dt->format('Y-m-d');
		$ShipperID = $this->getDhlAccountId(1);
		$xml = '';
		
		$xml .= '<Shipper>';
			$xml .= '<ShipperID>'.$ShipperID.'</ShipperID>';
			$xml .= '<CompanyName>'.$Details['FromCompany'].'</CompanyName>';			
			$xml .= '<AddressLine>'.$Details['FromAddress1'].'</AddressLine>'; 
			$xml .= '<AddressLine>'.$Details['FromAddress2'].'</AddressLine>';
			$xml .= '<City>'.$Details['FromCity'].'</City>';
			$xml .= '<Division>'.$Details['FromDivision'].'</Division>';
			//$xml .= '<DivisionCode>'.$Details[''].'</DivisionCode>';
			$xml .= '<PostalCode>'.$Details['FromPostCode'].'</PostalCode>';
			$xml .= '<CountryCode>'.$Details['FromCountryCode'].'</CountryCode>';
			$xml .= '<CountryName>'.$Details['FromCountryName'].'</CountryName>';
			$xml .= '<Contact>';
				$xml .= '<PersonName>'.$Details['FromPersonName'].'</PersonName>';
				$xml .= '<PhoneNumber>'.$Details['FromPhoneNumber'].'</PhoneNumber>';
			$xml .= '</Contact>';
		$xml .= '</Shipper>';
	return $xml;	
	}
	
	/*
	 * Version 1.0
	 * @params, setup req special service and charge elem
	 * set region elem
	 */ 
	public function shipRequestForReqSpeacialServiceElem( $value = null , $currency = null )
	{
		$dt = new DateTime();
		$date = $dt->format('Y-m-d');
		
		$xml = '';
		$xml .= '<SpecialService>';
			$xml .= '<SpecialServiceType>PT</SpecialServiceType>';
			//$xml .= '<ChargeValue>200.00</ChargeValue>';
			//$xml .= '<CurrencyCode>GBP</CurrencyCode>';
		$xml .= '</SpecialService>';
	return $xml;	
	}
	
	/*
	 * Version 1.0
	 * @params, setup req dhl label with speacial parameters elem
	 * set region elem
	 */ 
	public function shipRequestForReqLabelElem( $value = null , $currency = null )
	{
		$dt = new DateTime();
		$date = $dt->format('Y-m-d');
		
		$xml = '';		
		$xml .= '<LabelImageFormat>PDF</LabelImageFormat>';
		
		$xml .= '<Label>';		
		$xml .= '<LabelTemplate>6X4_PDF</LabelTemplate>';
		$xml .= '</Label>';

	return $xml;	
	}
	
	
	
	 /* Version 1.0
	 * @params, setup req end elem
	 * set region elem
	 */ 
	public function shipRequestEndElem()
	{
		$dt = new DateTime();
		$date = $dt->format('Y-m-d');
		
		$xml = '';
		$xml .= '</req:ShipmentRequest>';
	return $xml;	
	}
	
	/*
	 * Version 1.0
	 * @param, cron access
	 * return appropriate response
	 * 
	 */ 
	public function processShippmentXml( $accessDhlServerUrl = null , $dhlShipXmlRequest = null, $Details = null )
	{
		
		if(!$ch=curl_init())
		{
			$this->_error="Curl is not initialized.";
			return false;
		}
		else
		{
			curl_setopt($ch, CURLOPT_URL , $accessDhlServerUrl);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS , $dhlShipXmlRequest);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			if(curl_error($ch) != "")
			{
				echo $this->_error= "Error with Curl installation: " . curl_error($ch) . "<br>";
				return false;
			}
			else
			{
				curl_close($ch);				
				//location
				$labelPdfPath    = WWW_ROOT .'img/dhl/label_'.$Details['order_id'].'.pdf'; 				
				$dhl_path        = WWW_ROOT .'dhl/label_'.$Details['order_id'].'.pdf';								
						
				$xml = $xmlMatchinProduct = simplexml_load_string( $response ); 
				$xml = json_decode( json_encode($xml) ,0);
				
				$data = base64_decode( $xml->LabelImage->OutputImage );
				
				file_put_contents($labelPdfPath , $data);
				file_put_contents($dhl_path , $data);
				
				file_put_contents(WWW_ROOT .'img/dhl/Request_'.$Details['order_id'].'.xml' , $dhlShipXmlRequest);
				file_put_contents(WWW_ROOT .'img/dhl/Response_'.$Details['order_id'].'.xml' , $response); 
				
			//	$filepath	 = WWW_ROOT .'img/orders/packagin_label/'.$Details['order_id'].'.png';

				//$pdf = $labelPdfPath.'[0]';
				//$image = new Imagick($pdf);
				////$image->resizeImage( 300, 500, imagick::FILTER_LANCZOS, 0);
				//$image->setImageFormat( "png" );
				//$image->writeImage(WWW_ROOT .'img/orders/packagin_label/'.$Details['order_id'].'.png');
				//file_put_contents(WWW_ROOT .'img/orders/packagin_label/'.$Details['order_id'].'.txt', $data);
			
			return $xml;					
			}
		}		
	}
	
	/*
	 * Version 1.0
	 * @params need offset type to verify
	 * Verify url
	 * 
	 */ 
	public function verifyDhlUrl( $typeKnown = null )
	{
		
		if( isset( $typeKnown ) && $typeKnown != '' )
		{
			if( $typeKnown == "sandbox" )
			{
				$url = Configure::read( '_dhlServiceUrlByType_' );
				$url = $url['_sand'];
				return $url;
			}
			
			if( $typeKnown == "production" )
			{
				$url = Configure::read( '_dhlServiceUrlByType_' );
				$url = $url['_prod'];
				return $url;
			}
			
		}
		
	}
	
	/*
	 * 
	 * Version 1.0
	 * @params need to set authentication of dhl account
	 * 
	 */ 
	public function getDhlAcountUser( $typeKnown = null )
	{
		
		if( isset(  $typeKnown ) && $typeKnown != '' )
		{
			
			$user = Configure::read( '_dhlAuthentication_' );
			$user = $user['_user'];
			return $user;
			
		}	
		
	}
	
	/*
	 * 
	 * Version 1.0
	 * @params need to set authentication of dhl account
	 * 
	 */ 
	public function getDhlAcountPass( $typeKnown = null )
	{
		
		if( isset(  $typeKnown ) && $typeKnown != '' )
		{
			
			$pass = Configure::read( '_dhlAuthentication_' );
			$pass = $pass['_pass'];
			return $pass;
			
		}	
		
	}
	
	/*
	 * 
	 * Version 1.0
	 * @params need to set authentication of dhl account
	 * 
	 */ 
	public function getDhlAccountId( $typeKnown = null )
	{
		
		if( isset(  $typeKnown ) && $typeKnown != '' )
		{
			
			$accountId = Configure::read( '_dhlShipperAccount_' );
			$accountId = $accountId['_shipper_account_'];
			return $accountId;
			
		}	
		
	}
	
	public function replaceFrenchChar($string = null){
			
		$unwanted_array = array('�'=>'S', '�'=>'s', '�'=>'Z', '�'=>'z', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E','�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', 'N�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U','�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c','�'=>'e', '�'=>'e', '�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n','n�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o','�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'b', '�'=>'y','�'=>'u','�'=>'','N�'=>'N');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}	
	
}
?>
