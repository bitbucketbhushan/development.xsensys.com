<?php
ini_set( 'memory_limit' , '-1' );
error_reporting(0);
class ReportsController extends AppController
{
    
    var $name = "Reports";
    var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
    
    
    public function index()
    {
		echo "Index";
		exit;
	}
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('getDailySellReport','SanDiskReport','SanDiskStockReport','getHeldOrderReport','getOrderReport','getAllInventory'));
    }
	
	public function getDailySellReport() 
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'ScanOrder' );
		$toDayDate	=	date( 'Y-m-d' );
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'SKU');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Barcode');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Quantity');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Date');
		
		$todaysSkus	=	$this->ScanOrder->find( 'all', array( 
								'conditions' => array( 'ScanOrder.scan_date' =>  $toDayDate ),
								'fields' => array( 'sum(ScanOrder.quantity)   AS Quantity' , 'ScanOrder.id, ScanOrder.split_order_id, ScanOrder.sku, ScanOrder.barcode', 'ScanOrder.scan_date'),
								'group' => 'ScanOrder.sku'
								 )
							);
							
		/*$todaysSkus	=	$this->ScanOrder->find( 'all', array( 
								'conditions' => array( 'ScanOrder.scan_date' =>  '2016-05-22' ),
								'fields' => array( 'sum(ScanOrder.quantity)   AS Quantity' , 'ScanOrder.id, ScanOrder.split_order_id, ScanOrder.sku, ScanOrder.barcode', 'ScanOrder.scan_date'),
								'group' => 'ScanOrder.sku'
								 )
							);*/
		
		$inc = 2;
		foreach( $todaysSkus as $todaysSku )
		{
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $todaysSku['ScanOrder']['sku']);     
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $todaysSku['ScanOrder']['barcode']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $todaysSku[0]['Quantity']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $todaysSku['ScanOrder']['scan_date']);
			$inc++;
		}
		
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		$uploadPath = WWW_ROOT .'img/daily_report/DailySellReport.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadPath);
		
		
		date_default_timezone_set('Europe/Jersey');
		// send email
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('');
		$email->from('webmaster@xsensys.com');
		//$email->to( array('lalitbhadula2010@gmail.com'));
		$email->to( array('LALITJIJ014@GMAIL.COM'));
		$email->subject('Daily Report Of Sold SKu');
		//$getBase = Router::url('/', true);
		$uploadRemote = WWW_ROOT .'img/daily_report/DailySellReport.csv';

		$email->attachments(array(
			'DailySellReport.csv' => array(
				'file' => $uploadPath,
				'mimetype' => 'text/csv',
				'contentId' => 'Euraco'
			)
		));
		
		$email->send( 'Please get attachment file.' );
		exit;
		
	}

	public function SanDiskReport() 
	{
	
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'ProductDesc' ); 
		$this->loadModel( 'MergeUpdate' ); 
		$this->loadModel( 'ScanOrder' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'PurchaseOrder' );
		
			
		$monday = date("Y-m-d", strtotime('monday this week'));   
		$sunday = date("Y-m-d", strtotime('sunday this week'));
		
		
		/*$date = '2016-11-28'; // sunday
		echo $monday =  date("Y-m-d", strtotime('monday this week', strtotime($date))), "\n";   
		echo "<br>";
		echo $sunday = date("Y-m-d", strtotime('sunday this week', strtotime($date))), "\n";
		*/


		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		
		$BStyle = array(
				  'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				  )
				);
		
		 $style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);
		
		$inc = 1;		
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$inc.':I'.$inc);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(24);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(19);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
			
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'SanDisk International - Retailer Weekly Reporting');				
		$objPHPExcel->getActiveSheet()->getRowDimension($inc)->setRowHeight(30);	
		$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':I'.$inc)->getFont()->setSize(20);		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':I'.$inc)->applyFromArray($style);
		 
						
		$inc++; $inc++;		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'POS file data requirements');	
		$objPHPExcel->getActiveSheet()->getStyle('A'.$inc)->getFont()->setBold(true);
		
		$inc++;	$inc++;	
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Retailer Name:');	
		$objPHPExcel->getActiveSheet()->getStyle('A'.$inc)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, 'Euraco');	
		
		$inc++;		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Reporting week start date:');	
		$objPHPExcel->getActiveSheet()->getStyle('A'.$inc)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $monday);	
		
		$inc++;		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Reporting week end date:');	
		$objPHPExcel->getActiveSheet()->getStyle('A'.$inc)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $sunday);	
				
		$inc++; $inc++;				
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Dasily Transaction Date');     
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, 'Invoice Number');
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, 'Sandisk Part Number');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 'Product Description');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, 'Country of Sale');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 'Sales Quantity');
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 'Quantity returned by Customer');			
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, 'Platform Sold On');
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 'If Other please state');
		$objPHPExcel->getActiveSheet()->getRowDimension($inc)->setRowHeight(32);			
		$objPHPExcel->getActiveSheet()->getStyle('G'.$inc.':I'.$inc)->getAlignment()->setWrapText(true);	
		$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':I'.$inc)->getFont()->setBold(true);		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':I'.$inc)->applyFromArray($BStyle);						
		$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':I'.$inc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('CCFFCC');
			
				
		$sandiskSkus	=	$this->ProductDesc->find( 'all', array( 
								'conditions' => array( 'ProductDesc.brand' => 'SANDISK' ),
								'fields' => array( 'ProductDesc.bin' , 'ProductDesc.short_description')
								 )
							);
							
		foreach($sandiskSkus as $sku){
		$san_sku = $sku['ProductDesc']['bin'];
	
		if($san_sku !=''){						
							
				$sandiskOrder =  $this->ScanOrder->find('all', array(
				'joins' => array(
					array(
						'table' => 'merge_updates',
						'alias' => 'MergeUpdate',
						'type' => 'INNER',
						'conditions' => array('ScanOrder.sku Like' => '%'.$san_sku.'%' , 'ScanOrder.scan_date BETWEEN ? and ?' => array($monday, $sunday),					'MergeUpdate.product_order_id_identify=ScanOrder.split_order_id'
						)
					)			
				),
		
				'fields' => array('sum(ScanOrder.quantity)   AS Quantity' , 'ScanOrder.order_id', 'ScanOrder.split_order_id', 'ScanOrder.sku', 'ScanOrder.barcode', 'ScanOrder.scan_date' ,'MergeUpdate.order_id','MergeUpdate.delevery_country','MergeUpdate.order_date'),	
				'order'=>'MergeUpdate.order_date ASC'
				
					
				));						
								
				foreach($sandiskOrder as $ord){
					if(count($ord['ScanOrder']['order_id'])>0) {
						$inc++;						
					
						$OpenOrder	=	$this->OpenOrder->find('first', array('conditions' => array('num_order_id' => $ord['MergeUpdate']['order_id']),'fields' => array('sub_source','status','num_order_id','open_order_date')) );
						
						
						$po	=	$this->PurchaseOrder->find('first', array('conditions' => array('purchase_sku' => $san_sku,'purchase_code !=' =>'NA'),'fields' => array('purchase_code'),'order'=>'date desc') );
						//pr($po);
						$q = 0;
						if($OpenOrder['OpenOrder']['status'] == 2){
							$q = $ord[0]['Quantity'];
						}
						
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, date('Y-m-d',strtotime($OpenOrder['OpenOrder']['open_order_date']))); 
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $ord['MergeUpdate']['order_id']);  
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $po['PurchaseOrder']['purchase_code']);  
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $sku['ProductDesc']['short_description']);   
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $ord['MergeUpdate']['delevery_country']);  
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $ord[0]['Quantity']);  
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $q);   
						$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $OpenOrder['OpenOrder']['sub_source']);   
						$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, 0);   
						
					 	$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':I'.$inc)->applyFromArray($BStyle);	
					
					}
				}		
							
			}
		}
		
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		$uploadPath = WWW_ROOT .'img/daily_report/sandisk.xlsx';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');	
		//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadPath);
		$msg = "report generated.";
		file_put_contents(WWW_ROOT."img/daily_report/sandisk.log", "-----".date('Y-m-d H:i:s')."-----\n".$msg."\n", FILE_APPEND | LOCK_EX);
							
	}
	
	public function SanDiskStockReport() 
	{
	
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'Product' ); 
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'PurchaseOrder' );
		$this->loadModel( 'CheckIn' );
		
			
		$monday = date("Y-m-d H:i:s", strtotime('monday this week')); 	
	    $sunday = date("Y-m-d H:i:s", strtotime('sunday this week'));
		
		
		//$date = '2016-11-10'; // sunday
		//echo $monday =  date("Y-m-d H:i:s", strtotime('monday this week', strtotime($date))), "\n";   
		//echo "<br>";
		//echo $sunday = date("Y-m-d H:i:s", strtotime('sunday this week', strtotime($date))), "\n";
		
		
		$sandiskSkus =  $this->Product->find('all', array(
			'joins' => array(
				array(
					'table' => 'product_descs',
					'alias' => 'ProductD',
					'type' => 'INNER',
					'conditions' => array('ProductD.brand' => 'SANDISK' ,'Product.id=ProductD.product_id'
					)
				)			
			),
		
			'fields' => array('Product.product_sku' , 'Product.product_name', 'Product.current_stock_level')
			));	
			
		foreach($sandiskSkus as $sku){
			$sk[] =  $sku['Product']['product_sku'] ;			
		}
		
		$supp	=	$this->PurchaseOrder->find( 'all', array( 
					'conditions' => array( 'purchase_sku IN' => $sk),
					'group' => 'po_name',
					'fields' => array( 'po_name')
					 )
				);
							
		$supplier = array();	
		foreach($supp as $sup){
		
			$str =	preg_replace("/[^a-zA-Z-]+/", "", strtolower($sup['PurchaseOrder']['po_name']));
			
			if (strpos($str, 'vow') !== false) {
				$supplier['vow'] = 'Vow';
			}else if (strpos($str, 'peak') !== false) {
				$supplier['peak'] = 'Peak';
			}else if (strpos($str, 'ufp') !== false) {
				$supplier['ufp'] = 'UFP-UK';
			}else{
				$supplier[$str] = ucfirst($str);
			}
		
		}
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		
		$BStyle = array(
				  'borders' => array(
					'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				  )
				);
		
		 $style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);
		
		foreach($supplier as $supplier_code =>  $supplier_name){
			
			
			$inc = 1;		
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$inc.':G'.$inc);
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(24);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(19);
				
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'SanDisk International - Retailer Weekly Reporting');				
			$objPHPExcel->getActiveSheet()->getRowDimension($inc)->setRowHeight(30);	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':G'.$inc)->getFont()->setSize(20);		
			$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':G'.$inc)->applyFromArray($style);
			 
							
			$inc++; $inc++;		
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Inventory file data requirements');	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$inc)->getFont()->setBold(true);
			
			$inc++;	$inc++;	
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Distributor Name');	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$inc)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $supplier_name);
			
			$inc++;	$inc++;	
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Retailer Name');	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$inc)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, 'EURACO GROUP LTD');	
			
			$inc++;		
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Date');	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$inc)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, date("Y-m-d",strtotime($sunday)));	
			
			$inc++;		
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Currency');	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$inc)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, 'GBP');	
					
			$inc++; $inc++;				
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, 'Sandisk Part Number');     
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, 'Product Description');
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, 'Effective Inventory Date');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, 'Quantity on Hand at close of reporting week');
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, 'Quantity Received from Distributor in reporting week');
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 'Quantity Returned to Distributor in reporting week');
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, 'Notes');	
			$objPHPExcel->getActiveSheet()->getRowDimension($inc)->setRowHeight(42);			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':G'.$inc)->getAlignment()->setWrapText(true);	
			$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':G'.$inc)->getFont()->setBold(true);		
			$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':G'.$inc)->applyFromArray($BStyle);						
			$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':G'.$inc)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('CCFFCC');
				
			$c=0;
			foreach($sandiskSkus as $sku){
			
				$san_sku = $sku['Product']['product_sku'];
			//	$monday =	'2016-11-09 00:00:00';				
				if($san_sku !=''){	
									
						$pskus	=	$this->CheckIn->find( 'all', array( 
								'conditions' => array( 'sku' =>  $san_sku,'po_name like' =>  '%'.$supplier_code.'%', 'date BETWEEN ? and ?' => array($monday, $sunday) ),
								'fields' => array('po_name','date','qty_checkIn')
								 )
							);	
						if(count($pskus)>0){							
							
							$purchase_code = $san_sku;
							$pc	=	$this->PurchaseOrder->find('first', array('conditions' => array('purchase_sku' => $san_sku,'purchase_code !=' =>'NA'),'fields' => array('purchase_code'),'order'=>'date desc') );
							if(count($pc) > 0) {
								 $purchase_code = $pc['PurchaseOrder']['purchase_code'];
							}
						
							foreach($pskus as $po){
								$c++;
								$inc++; 
								$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc,  $purchase_code); 
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $sku['Product']['product_name']);  
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc,  date("Y-m-d",strtotime($po['CheckIn']['date']))); 
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $sku['Product']['current_stock_level']);   
								$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $po['CheckIn']['qty_checkIn']);  
								$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, 0);  
								$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, '');  
								
								$objPHPExcel->getActiveSheet()->getStyle('A'.$inc.':G'.$inc)->applyFromArray($BStyle);	
							
							}
							
						}						
				 }
			}
			
			if($c>0){
				$objPHPExcel->getActiveSheet(0);
				$getBase = Router::url('/', true);
				$uploadPath = WWW_ROOT .'img/daily_report/sandiskstock_'.$supplier_name.'.xlsx';
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');	
				$objWriter->save($uploadPath);
				echo  $msg = "File generated for supplier : ".$supplier_name ;
				file_put_contents(WWW_ROOT."img/daily_report/sandiskstock.log", "-----".date('Y-m-d H:i:s')."-----\n".$msg."\n", FILE_APPEND | LOCK_EX);
			}else{
				echo $msg = "No data found for supplier : ".$supplier_name ;
				echo "<br>";				
				file_put_contents(WWW_ROOT."img/daily_report/sandiskstock.log", "-----".date('Y-m-d H:i:s')."-----\n".$msg."\n", FILE_APPEND | LOCK_EX);
	
			}
		}					
	}
	
	public function orderReport()
	{
			$this->layout="index";			
	}
	
	public function localUnlocakCancel()
	{
		$this->layout = '';
		$this->autorender = false;
		$this->loadModel('OrderNote');

		
		$getAllNotes	=	$this->OrderNote->find('all');
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderID'); 
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Comment'); 
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Type');       
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'User Name');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'date');
		$i = 2;
		foreach( $getAllNotes as $getAllNote )
		{
			$order_id			= 	$getAllNote['OrderNote']['order_id'];
			$note				= 	$getAllNote['OrderNote']['note'];
			$type				=	$getAllNote['OrderNote']['type'];
			$user				=	$getAllNote['OrderNote']['user'];
			$date				=	$getAllNote['OrderNote']['date'];
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $order_id); 
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $note); 
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $type);       
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $user);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $date);
			$i++;
	   }
		
		$objPHPExcel->getActiveSheet(0);
		$getBase 		= 	Router::url('/', true);
		$uploadUrl 		= 	WWW_ROOT .'img/Reports/lock_unlock_cancel.csv';
		$uploadRemote 	= 	$getBase.'img/Reports/lock_unlock_cancel.csv';
		$objWriter 		= 	PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'img/Reports/lock_unlock_cancel.csv';
		
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
	}
	
	public function getHeldOrderReport(  $string = null )
	{
		$this->layout = '';
		$this->autorender = false;
		$this->loadModel('MergeUpdate');
		App::uses('CakeEmail', 'Network/Email');
		$holidays=array("2017-01-01","2017-01-02","2017-04-14","2017-04-17","2017-05-01","2017-05-09","2017-05-29","2017-08-28","2017-12-25","2017-12-26","2018-01-01","2018-03-30","2018-04-02","2018-05-07","2018-05-09","2018-05-28","2018-08-27","2018-08-27","2018-12-25","2018-12-26");
		$startDate = date('Y-m-d');
		
		$getOpenOrders = $this->MergeUpdate->find('all', array( 'conditions' => array( 'status' => '0' ) ) );
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderID'); 
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Working Day'); 
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Sku');       
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Service Provider');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Service Code');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Delevery Country');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Date');
		$i = 2;
		$j = 0;
		foreach($getOpenOrders as $getOpenOrder)
		{
			$orderMoveDate	=	$getOpenOrder['MergeUpdate']['order_date'];
			$orderMoveDate	=   date("Y-m-d", strtotime($orderMoveDate));
			$getday 		= 	$this->getWorkingDays($orderMoveDate,$startDate,$holidays);
			
			$orderSplitId			=	$getOpenOrder['MergeUpdate']['product_order_id_identify'];
			$sku					=	$getOpenOrder['MergeUpdate']['sku'];
			$service_provider		=	$getOpenOrder['MergeUpdate']['service_provider'];
			$service_code			=	$getOpenOrder['MergeUpdate']['provider_ref_code'];
			$delivery_country		=	$getOpenOrder['MergeUpdate']['delevery_country'];
			$orderDate				=	$getOpenOrder['MergeUpdate']['order_date'];
			
			if( $getday > 2 )
			{
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $orderSplitId); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $getday); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $sku);       
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $service_provider);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $service_code);	
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $delivery_country);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $orderDate);
				$j++;
			}
			$i++;
		}
		
		$objPHPExcel->getActiveSheet(0);
		$getBase 		= 	Router::url('/', true);
		$uploadUrl 		= 	WWW_ROOT .'img/Reports/held_order.csv';
		$uploadRemote 	= 	$getBase.'img/Reports/held_order.csv';
		$objWriter 		= 	PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'img/Reports/held_order.csv';
		
		
		if( $string == 'custom' ){
			$contenttype = "application/force-download";
			header("Content-Type: " . $contenttype);
			header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
			readfile($uploadRemote);
			exit;
		} else {
			if($j > 0){
					$email = new CakeEmail('');
					$email->from('webmaster@xsensys.com');
					$email->to( array('amit.gaur@jijgroup.com','abhishek@euracogroup.co.uk') );
					$email->subject('Held Orders');
					$uploadRemote = WWW_ROOT .$file;
					$email->attachments(array(
						'held_order.xlsx' => array(
							'file' => $uploadRemote,
							'mimetype' => 'text/csv',
							'contentId' => 'Euraco'
						)
					));
					$email->send( 'Please get attachment file.' );
					exit;
				}
			}
		
	}
	
	public function getWorkingDays($startDate,$endDate,$holidays)
	{
			$endDate = strtotime($endDate);
			$startDate = strtotime($startDate);
			$days = ($endDate - $startDate) / 86400 + 1;
		
			$no_full_weeks = floor($days / 7);
			$no_remaining_days = fmod($days, 7);
		
			$the_first_day_of_week = date("N", $startDate);
			$the_last_day_of_week = date("N", $endDate);
			if ($the_first_day_of_week <= $the_last_day_of_week) {
				if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
				if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
			}
			else {
				if ($the_first_day_of_week == 7) {
					$no_remaining_days--;
		
					if ($the_last_day_of_week == 6) {
						$no_remaining_days--;
					}
				}
				else {
					$no_remaining_days -= 2;
				}
			}
		   $workingDays = $no_full_weeks * 5;
			if ($no_remaining_days > 0 )
			{
			  $workingDays += $no_remaining_days;
			}
			foreach($holidays as $holiday){
				$time_stamp=strtotime($holiday);
				if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
					$workingDays--;
			}
			return $workingDays;
	}
	
	public function getCustomerDetail()
	{
		
		$this->loadModel('Product');
		$this->loadModel('ScanOrder');
		$this->loadModel('OpenOrder');
		$brand = array( 'HP', 'Lexmark', 'Canon', 'Kodak', 'Epson', 'Dell', 'Brother' );
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderID'); 
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Reference Number'); 
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'SKU'); 
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Brand'); 
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Recipet Name');       
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Address1');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Address2');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Address3');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Town');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Resion');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Post Code');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Country');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Phone Number');


		$getOpenOrders	=	$this->OpenOrder->find('all', array( 
														 'conditions' => array( 'OpenOrder.destination' => 'United Kingdom'),
														 'fields' => array( 'OpenOrder.num_order_id','OpenOrder.customer_info','OpenOrder.items','OpenOrder.general_info' ) 
														 		) 
												  );
		
		$i = 2;
		foreach($getOpenOrders as $getOpenOrder)
		{
			$customer				=	unserialize( $getOpenOrder['OpenOrder']['customer_info'] );
			$items					=	unserialize( $getOpenOrder['OpenOrder']['items'] );
			$general_info			=	unserialize( $getOpenOrder['OpenOrder']['general_info'] );
			$orderID				=	$getOpenOrder['OpenOrder']['num_order_id'];
			$reference_number		=	$general_info->ReferenceNum;
			
			$recepent_name	=	$customer->Address->RecipentName;
			$address1		=	$customer->Address->Address1;
			$address2		=	$customer->Address->Address2;
			$address3		=	$customer->Address->Address3;
			$town			=	$customer->Address->Town;
			$region			=	$customer->Address->Region;
			$post_code		=	$customer->Address->PostCode;
			$country		=	$customer->Address->Country;
			$phone_number	=	$customer->Address->PhoneNumber;

			foreach($items as $item)
			{
				//pr($item);
				
				if( count( explode( '-', $item->SKU ) ) == 2 )
				{
					$sku =	$item->SKU;
				}
				else if( count( explode( '-', $item->SKU ) ) == 3 )
				{
					$splitskus = explode( '-' , $item->SKU);	
					$sku = 'S-'.$splitskus[1];
				}
				else if( count( explode( '-', $item->SKU ) ) > 3 )
				{
					$splitskus = explode( '-', $item->SKU );
					$in = 1; 
					while( $in <= count( $splitskus )-2 ):											
						
							$sku = 'S-'.$splitskus[$in];												
					$in++;
					endwhile;
								}
				
				//$this->Product->find('first', array( 'conditions' => array( 'Product.product_sku' => $sku  ), 'fields' => array( 'ProductDesc.barcode' ) ) );
			}
			$getbrand	=	$this->Product->find('first', array( 'conditions' => array( 'Product.product_sku' => $sku  ), 'fields' => array( 'ProductDesc.brand' ) ) );
			
			if(!empty($getbrand) && in_array($getbrand['ProductDesc']['brand'], $brand ))
			{
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $orderID); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $reference_number); 
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $sku); 
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $getbrand['ProductDesc']['brand']); 
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $recepent_name);       
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $address1);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $address2);	
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $address3);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $town);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $region);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $post_code);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $country);
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $phone_number);	
				$i++;
			}
			//pr($item);
		}
		
		$objPHPExcel->getActiveSheet(0);
		$getBase 		= 	Router::url('/', true);
		$uploadUrl 		= 	WWW_ROOT .'img/Reports/customer_report.xlsx';
		$uploadRemote 	= 	$getBase.'img/Reports/customer_report.xlsx';
		$objWriter 		= 	PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save($uploadUrl);
		
		$file = 'img/Reports/customer_report.xlsx';
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
	}
	
	public function getOrderReportDay($date = NULL)
	{
	
		$this->layout = 'index';
		$this->autorender = false;
		if($date != ''){
		$this->loadModel('MergeUpdate');
		App::uses('CakeEmail', 'Network/Email');		 
		//if($date == '') $date  = date('Y-m-d',strtotime("-1 day"));
		$date_range = array($date." 00:00:00" , $date." 23:55:00");	
		  
		$getOpenOrders = $this->MergeUpdate->find('all', array( 'conditions' => array( 'status' => '0','order_date BETWEEN ? AND ?' => $date_range ) ) );
		 
		 
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderID'); 
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Working Day'); 
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Sku');       
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Service Provider');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Service Code');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Delevery Country');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Date');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Sku');  
		$i = 2;
		foreach($getOpenOrders as $getOpenOrder)
		{			 
			$orderSplitId			=	$getOpenOrder['MergeUpdate']['product_order_id_identify'];
			$sku					=	$getOpenOrder['MergeUpdate']['sku'];
			$service_provider		=	$getOpenOrder['MergeUpdate']['service_provider'];
			$service_code			=	$getOpenOrder['MergeUpdate']['provider_ref_code'];
			$delivery_country		=	$getOpenOrder['MergeUpdate']['delevery_country'];
			$orderDate				=	$getOpenOrder['MergeUpdate']['order_date'];
			
			 
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $orderSplitId); 
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $date); 
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $sku);       
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $service_provider);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $service_code);	
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $delivery_country);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $orderDate);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $orderDate);
			 
			$i++;
		}
		
		$objPHPExcel->getActiveSheet(0);
		$getBase 		= 	Router::url('/', true);
		$uploadUrl 		= 	WWW_ROOT .'img/Reports/orders.xlsx';
		$uploadRemote 	= 	$getBase.'img/Reports/orders.xlsx';
		$objWriter 		= 	PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save($uploadUrl);
		$file = 'img/Reports/orders.xlsx';
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
		}
		 
	}
	
	public function getOrderReport()
	{
		$this->layout = '';
		$this->autorender = false;
		$this->loadModel('MergeUpdate');
		$this->loadModel('OpenOrder');
		date_default_timezone_set('Europe/Jersey');
		
		App::uses('CakeEmail', 'Network/Email');
		
		$saturday = date('Y-m-d', strtotime('saturday of week'));
		$sunday   = date('Y-m-d', strtotime('sunday of week'));
		
 		$holidays = array($saturday,$sunday, "2017-08-28","2017-12-25","2017-12-26","2018-01-01","2018-03-30","2018-04-02","2018-05-07","2018-05-09","2018-05-28","2018-08-27","2018-08-27","2018-12-25","2018-12-26"); 
		
		$uploadUrl 	= 	WWW_ROOT .'img/Reports/holidays.txt';		 
		
		if(in_array(date('Y-m-d'),$holidays)){		
			file_put_contents($uploadUrl,date('Y-m-d')."\t", FILE_APPEND | LOCK_EX);					
			return;			
		}
		else{
			//if(date('H') == '23' && date('i') > '50'){
			
			$mainarray = array();
			if(file_exists($uploadUrl)){
				$row = file($uploadUrl);			  
				$mainarray = explode("\t", rtrim($row[0],"\t"));			  
			}		
			$rep_date = date('Y-m-d', strtotime('-1 day'));date('Y-m-d') ;
			$mainarray[count($mainarray)] = $rep_date;			 
			$date_range = array(min($mainarray) ." 00:00:00" ,max($mainarray)." 23:55:00");	
			 
			 $order_id = array();  $itemMergeTotal = array(); $itemMergeTotal = array();
			 $mergeOrders  = $this->MergeUpdate->find('all', array( 'conditions' => array('order_date BETWEEN ? AND ?' => $date_range ) ) );
			 
			 foreach($mergeOrders as $mOrder)
			 {
				if($mOrder['MergeUpdate']['status'] == 0){
					$order_id[$mOrder['MergeUpdate']['order_id']] = $mOrder['MergeUpdate']['order_id'];					
					$itemMergeArray[] = $mOrder['MergeUpdate']['quantity'];	
					$oid	= $mOrder['MergeUpdate']['order_id'];											
				}
				$itemMergeTotal[] = $mOrder['MergeUpdate']['quantity'];		
			 }	 
			 
			 $itemArray = array(); $itemQtyArray = array(); 
			 if(count($order_id) > 1){
			 $getOpenOrders = $this->OpenOrder->find('all', array('fields' => array('num_order_id','process_date','items'), 'conditions' =>  array('num_order_id IN' => $order_id ) ) );
			 }else{
			  $getOpenOrders = $this->OpenOrder->find('all', array('fields' => array('num_order_id','process_date','items'), 'conditions' =>  array('num_order_id' => $oid ) ) );
			 }
			 
			 if(count($getOpenOrders) > 0)
			 {
			 
			 foreach($getOpenOrders as $getOpenOrder)
			 {
			 	$items = unserialize($getOpenOrder['OpenOrder']['items']); 				
				foreach($items as $item){
					$itemArray[$item->SKU] = $item->SKU;	
					$itemQtyArray[] = $item->Quantity;		
				}				
			 }
			 			 
			/*-------------  % of orders Exported more than 1 working day old ----------*/
			$dealy_orders = array(); 
			$dealy_sku    = array();  
			$processed_orders = array();  
			$process_orders = array();  
			$poid=0;			 
			$getProcessOrders = $this->OpenOrder->find('all', array('fields' => array('num_order_id','process_date','items'), 'conditions' =>  array('status' => 1,'process_date LIKE ' => $rep_date.'%') ) );
			
			foreach($getProcessOrders as $p){
				$process_orders[$p['OpenOrder']['num_order_id']] = $p['OpenOrder']['num_order_id'];
				$poid = $p['OpenOrder']['num_order_id'];
			} 
			 
			/*--------------------Get Scaned Orders------------------------------*/
			 if(count($process_orders) > 1){
				 $allProcess  = $this->MergeUpdate->find('all', array( 'conditions' => array( 'order_id IN' => $process_orders  ) ) );
			 }else{
			     $allProcess  = $this->MergeUpdate->find('all', array( 'conditions' => array( 'order_id' => $poid  ) ) );
			 }
			 
			$scan_orders = array(); $scanQty = array();
			foreach($allProcess as $sc){
				if($sc['MergeUpdate']['status'] == 1 && $sc['MergeUpdate']['sorted_scanned'] == 1){
					$scan_orders[$sc['MergeUpdate']['order_id']] = $sc['MergeUpdate']['order_id'];			
					$scanQty[] = $sc['MergeUpdate']['quantity'];
				}	
			}	
				
			 /*--------------------Get Delay Orders------------------------------*/				 
			$mergeOrders  = $this->MergeUpdate->find('all', array( 'conditions' => array( 'status' => '0' ) ) );
			
			foreach($mergeOrders as $md){
			
				$orderMoveDate	=   date("Y-m-d", strtotime($md['MergeUpdate']['order_date']));
				$getday 		= 	$this->getWorkingDays($orderMoveDate,$rep_date,$holidays);
				
				if( $getday > 2 )
				{  
					$dealy_sku[$md['MergeUpdate']['sku']] = $md['MergeUpdate']['sku'];
					$dealy_orders[$md['MergeUpdate']['order_id']] = $md['MergeUpdate']['order_id'];
				}
			}

			$dp = 0;
			if(count($dealy_orders) > 0){
				$dp = number_format(100 * count($dealy_orders) / count($getProcessOrders) ,2);
			}
			$Url = WWW_ROOT .'logs/dealy_orders.txt';			 		
			file_put_contents($Url,"----------".date('Y-m-d')."----------\n".print_r($dealy_orders,true), FILE_APPEND | LOCK_EX);					
			 
			
		 	$message = '
			<html>
			<head>
			  <title>Order Report Of '.date('Y-m-d',strtotime($rep_date)).'</title>
			</head>
			<body>
			  <p>Please find order report.</p>
			  <table cellpadding="10" cellspacing="0" border="1">
				 
				<tr> <th align="left">Date</th><td>'. date('Y-m-d',strtotime($rep_date)) .'</td></tr>
				<tr> <th align="left">Number of Orders Open  Till '. date('Y-m-d',strtotime($rep_date)) .'</th><td>'. count($order_id) .'</td></tr>
				<tr> <th align="left">Number of Items (SKU) / Quantity in Open</th><td>'. count($itemArray) .' /  Quantity Count : '. array_sum($itemQtyArray) .'</td></tr>
				<tr> <th align="left">Number of Items (SKU) / Quantity(After Split) in Open</th><td>'. count($itemMergeArray) .' /  Quantity Count : '. array_sum($itemMergeArray) .'</td></tr>
				<tr> <th align="left">Total Items (SKU) / Quantity('. date('Y-m-d',strtotime($rep_date)) .')</th><td>'. count($itemMergeTotal) .' /  Quantity Count : '. array_sum($itemMergeTotal) .'</td></tr>
				
				
				<tr> <th align="left">Number of Orders Exported / Quantity</th><td>'. count($scan_orders) .' /  Quantity Count : '. array_sum($scanQty) .'</td></tr>
				<tr> <th align="left">Number of Orders Processed</th><td>'. count($getProcessOrders) .'</td></tr>
				<tr> <th align="left">Exported orders more than 1 working day old</th><td>'. $dp."%  Order Count : ".count($dealy_orders) .'</td></tr>
 						 
			  </table>
			</body>
			</html>';
		//echo $message;
				$email = new CakeEmail('');
				$email->from('webmaster@xsensys.com');
				$email->emailFormat('html');
				$email->to( array('amit.gaur@jijgroup.com','avadhesh@euracogroup.co.uk','jake.shaw@euracogroup.co.uk','shashi.b.kumar@jijgroup.com') );
				//$email->to( array('avadhesh@euracogroup.co.uk') );
				$email->subject('Order Report Of '.date('Y-m-d'));
				$email->send($message);
				if(file_exists($uploadUrl)) unlink($uploadUrl);
				exit;
			}
		 }
		
	}
	
	public function getProcessedOrderReport( $sdate = null, $edate = null, $hours = null, $user_id = null, $download = null)
	{
		//date_default_timezone_set('Europe/Jersey');
		$this->set( "title","Processed Orders" );	
		$this->layout = 'report';
		$this->autorender = false;
		$this->loadModel('MergeUpdate'); 
 		
		if($sdate == '' && $edate == '' ){
			$date_range = array( date('Y-m-d H:i:s', strtotime(date('Y-m-d')." 00:00:00")) , date('Y-m-d H:i:s',strtotime(date('Y-m-d')." 23:55:00")));
			$hours = 24; 
		}else{
			$date_range = array( date('Y-m-d H:i:s', strtotime($sdate ." 00:00:00")) , date('Y-m-d H:i:s',strtotime($edate." 23:55:00"))); 
		}
		 
		$fields = array('order_id','product_order_id_identify','quantity','order_date','picked_date','process_date','user_id','assign_user','pc_name');
		
		if($hours == 24){
			if(is_numeric($user_id)){
				$conditions = array(
						'process_date BETWEEN ? AND ?' => $date_range,
						'user_name' => $user_id,
						'TIMESTAMPDIFF(HOUR, order_date, process_date) < ' => 24);
			}else{
				$conditions = array(
						'process_date BETWEEN ? AND ?' => $date_range,						
						'TIMESTAMPDIFF(HOUR, order_date, process_date) < ' => 24);
			}
			
			$orders = $this->MergeUpdate->find('all',array('conditions' => $conditions,'fields'=>$fields,'order' => 'order_date'));
			 
			$this->paginate = array('conditions' => $conditions,'fields'=>$fields,'order' => 'order_date','limit' => 100 );
			$all_orders = $this->paginate('MergeUpdate');
		
		}
		else{
			 if(is_numeric($user_id)){
				$conditions = array(
						'process_date BETWEEN ? AND ?' => $date_range,
						'user_name' => $user_id,
						'TIMESTAMPDIFF(HOUR, order_date, process_date) > ' => 24);
			}else{
				$conditions = array(
						'process_date BETWEEN ? AND ?' => $date_range,
						'TIMESTAMPDIFF(HOUR, order_date, process_date) > ' => 24);
			}
			
			$orders = $this->MergeUpdate->find('all',array('conditions' => $conditions,'fields'=>$fields,'order' => 'order_date'));
		   
		    $this->paginate = array('conditions' =>$conditions,'fields'=>$fields,'order' => 'order_date','limit' => 100 );
			$all_orders = $this->paginate('MergeUpdate'); 
		}
		
		if($download == 'download_sheet'){
		 
			$path     = WWW_ROOT.'logs/';
			$filename = "download_sheet.csv";
			$header   = "order_id,quantity,order_date(move date),picked_date,process_date,user\r\n";
	
			file_put_contents($path.$filename,$header, LOCK_EX);
			foreach($orders as $order){
				$content = $order['MergeUpdate']['product_order_id_identify'].",".$order['MergeUpdate']['quantity'].",".$order['MergeUpdate']['order_date'].",".$order['MergeUpdate']['picked_date'].",".$order['MergeUpdate']['process_date'].",".$order['MergeUpdate']['assign_user'].'('.$order['MergeUpdate']['pc_name'].')'."\r\n";
				file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);
			}			
			header('Content-Description: File Transfer');
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path.$filename));
			ob_clean();
			flush();
			readfile($path.$filename);
			exit;
		
		} 
		 
		$this->set( 'orders', $orders );	
		$this->set( 'all_orders', $all_orders );	 
	}	
	
	
	public function getUserDetails()
	{
		$this->loadModel('MergeUpdate'); 
		$User = $this->MergeUpdate->find('all',array('group' => 'assign_user','fields'=>array('user_id','assign_user','pc_name')));
		return $User;
	}
	
	public function getLockOrderReport()
	{
		$this->loadModel('OpenOrder');
		$this->loadModel('OrderNote');
		
		$lockOrders = $this->OpenOrder->find('all', array('conditions' => array('OpenOrder.status' => 3 ) ) );
	
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderID'); 
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Reference Number'); 
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Sub Source'); 
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Destination'); 
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Status'); 
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'User'); 
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Date');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Comment');
		$i = 2;
		foreach( $lockOrders as $lockOrder )
		{
			$usernamedate		=	$this->getUsername( $lockOrder['OpenOrder']['num_order_id'] );
			$name_date			=	explode('####', $usernamedate);
			$sub_source			=	$lockOrder['OpenOrder']['sub_source'];
			$status				=	$lockOrder['OpenOrder']['status'];
			$num_order_id		=	$lockOrder['OpenOrder']['num_order_id'];
			$country			=	$lockOrder['OpenOrder']['destination'];
			$info				=	unserialize( $lockOrder['OpenOrder']['general_info'] );
			$ref_number			=	$info->ReferenceNum;
			if($status = 3)
			{ $status = 'Lock';	} 
			else { $status = 'Please Check Order'; }
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $num_order_id); 
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $ref_number); 
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $sub_source);       
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $country);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $status);	
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $name_date[0]);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $name_date[1]);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $name_date[2]);
					$i++;
		}
		
		$objPHPExcel->getActiveSheet(0);
		$getBase 		= 	Router::url('/', true);
		$uploadUrl 		= 	WWW_ROOT .'img/Reports/lock_order_report.csv';
		$uploadRemote 	= 	$getBase.'img/Reports/lock_order_report.csv';
		$objWriter 		= 	PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'img/Reports/lock_order_report.csv';
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
		
	}
	
	public function getUsername( $orderid )
	{
		$ordernotes	=	$this->OrderNote->find('all', array( 'conditions' => array( 'OrderNote.order_id' =>  $orderid, 'OrderNote.type' => 'Lock'),'order' => array('OrderNote.date DESC') ) );
		if( count($ordernotes) > 0 )
		{
			$user_name	=	$ordernotes[0]['OrderNote']['user'].'####'.$ordernotes[0]['OrderNote']['date'].'####'.$ordernotes[0]['OrderNote']['note'];
		} else {
			$user_name	=	'---####---####---';
		}		
		return $user_name;
	}
	
	public function getOpenOrder($order_ids) 
	{
		$this->loadModel( 'OpenOrder' );//
		$getdetails = $this->OpenOrder->find('all', array('conditions' => array('OpenOrder.status' => 0,'OpenOrder.num_order_id IN' =>$order_ids),'fields' => array( 'num_order_id', 'sub_source') ) );
		
		foreach( $getdetails as $or )
		{			
			$data[$or['OpenOrder']['num_order_id']] = $or['OpenOrder']['sub_source'];
		}			
				
		return  $data;
	}
	public function getOrderLocation($order_ids) 
	{
		$this->loadModel( 'OrderLocation' );//
		$getdetails = $this->OrderLocation->find('all', array('conditions' => array('order_id IN' =>$order_ids),'fields' => array( 'order_id', 'bin_location', 'po_name') ) );
		
		foreach( $getdetails as $or )
		{			
			$data[$or['OrderLocation']['order_id']] = array('bin_location'=> $or['OrderLocation']['bin_location'],'po_name'=> $or['OrderLocation']['po_name']);
		}			
				
		return  $data;
	}
	
	public function getReport($search_sku = NULL) 
	{
	 
		$this->layout = "index";
		$this->loadModel( 'Category' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'BinLocation' );	
		$this->loadModel( 'UnprepareOrder' );
		$this->loadModel('MergeUpdate');		
		$location 		= 	array();
		date_default_timezone_set('Europe/Jersey');
		
		$binLocation	=	$this->BinLocation->find('all',array('fields' => array( 'bin_location', 'stock_by_location','barcode')));
		foreach($binLocation as $data)
		{
			 $location[$data['BinLocation']['barcode']][] = array('bin_location' => $data['BinLocation']['bin_location'],
																	'stock_by_location'=> $data['BinLocation']['stock_by_location']);
			
		}		
		$this->set('BinLocation',$location);
		$unp_data 	= 	array(); 
		$orderArray	= 	array(); 	
		$unpOrders	=	$this->UnprepareOrder->find('all',array('fields' => array('items','num_order_id','source_name','destination','date')));
		foreach($unpOrders as $data){
			$orderArray[$data['UnprepareOrder']['num_order_id']] = $data['UnprepareOrder']['num_order_id']; 
		}			
		
		//$sourceArray = $this->getOpenOrder($orderArray);
		$OrderLocationArray = $this->getOrderLocation($orderArray);
		foreach($unpOrders as $data){
			
			
				$order_location = ''; $po_name = '';
				if(isset($OrderLocationArray[$data['UnprepareOrder']['num_order_id']])){
					$order_location = $OrderLocationArray[$data['UnprepareOrder']['num_order_id']]['bin_location'];
					$po_name = $OrderLocationArray[$data['UnprepareOrder']['num_order_id']]['po_name'];
				}
						
						
				foreach( unserialize($data['UnprepareOrder']['items']) as $val){
								 
					$_sku = explode( '-' , $val->SKU);
					if( count($_sku ) == 3 ) // For Bundle (Single)
					{
						$sku = 'S-'.$_sku[1];
						$qty = $val->Quantity * ($_sku[2]);
						$unp_data[$sku][] = array('quantity'=>$qty, 'num_order_id'=>$data['UnprepareOrder']['num_order_id'],'source_name'=>$data['UnprepareOrder']['source_name'],'destination'=>$data['UnprepareOrder']['destination'],'order_date'=>$data['UnprepareOrder']['date'],'order_location'=>$order_location,'po_name'=>$po_name);
						
						
					}
					else if( count( $_sku ) > 3 ) // For Bundle (Bundle)
					{						
						$inc = 1;	$ij = 0;
						while( $ij < count($_sku)-2 )
						{						
							$sku = 'S-'.$_sku[$inc];
												
							$inc++;
							$ij++;
							$unp_data[$sku][] = array('quantity'=>$val->Quantity, 'num_order_id'=>$data['UnprepareOrder']['num_order_id'],'source_name'=>$data['UnprepareOrder']['source_name'],'destination'=>$data['UnprepareOrder']['destination'],'order_date'=>$data['UnprepareOrder']['date'],'order_location'=>$order_location,'po_name'=>$po_name);
						}
					}else{				
						$unp_data[$val->SKU][] = array('quantity'=>$val->Quantity, 'num_order_id'=>$data['UnprepareOrder']['num_order_id'],'source_name'=>$data['UnprepareOrder']['source_name'],'destination'=>$data['UnprepareOrder']['destination'],'order_date'=>$data['UnprepareOrder']['date'],'order_location'=>$order_location,'po_name'=>$po_name);
					}
				}			
		}  		
		$this->set('unp_data',$unp_data);
 		
        $productAllDescs = $this->Product->find('all');  
		        
		 if($search_sku){
			 $this->paginate = array('conditions' => array( 'OR' => array('Product.product_sku LIKE ' => "$search_sku%",'ProductDesc.barcode'=> $search_sku))); 
 		}else{
			$this->paginate = array('conditions' => array( 'Product.product_sku NOT LIKE ' => "B-%") ,
							'fields' => array(									
									'Product.product_sku',
									'ProductDesc.barcode',
									'Product.current_stock_level'
								),
				'order' => 'product_sku',
				'limit' => 50			
			);
		}
 		 
		$productAllDescs = $this->paginate('Product'); 
		
		$orderArray	= 	array(); 	
		$MergeUpdate = $this->MergeUpdate->find( 'all' , 
				array( 
				'conditions' => array('MergeUpdate.status' => 0,'MergeUpdate.order_date > ' => date("Y-m-d H:i:s", strtotime('-5 days') )),
				'fields' => array( 'sku', 'order_id', 'quantity','delevery_country','order_date','status','pick_list_status','picked_date','process_date','scan_date' )
					)
				);
			
		foreach( $MergeUpdate as $ov )
		{	
			$qty = 0;
			$allSku   =	$ov['MergeUpdate']['sku'];
			$coma_exp = explode(",",$allSku);				
			foreach($coma_exp as $pexp){
				 $texp     = explode("-",$pexp);					
				 $t = "S-".trim($texp[1]);				 
				 $quantity = (int)filter_var($texp[0], FILTER_SANITIZE_NUMBER_INT);
				 $qty = $qty + $quantity;								
				 $getdetails[$t][] = array('order_id'=>$ov['MergeUpdate']['order_id'],'quantity'=>$qty ,'destination'=>$ov['MergeUpdate']['delevery_country'],'order_date'=>$ov['MergeUpdate']['order_date'], 'status'=>$ov['MergeUpdate']['status'], 'pick_list_status'=>$ov['MergeUpdate']['pick_list_status'], 'process_date'=>$ov['MergeUpdate']['process_date'], 'scan_date'=>$ov['MergeUpdate']['scan_date'], 'delevery_country'=>$ov['MergeUpdate']['delevery_country'],  'picked_date'=>$ov['MergeUpdate']['picked_date']  );	
				 $orderArray[$ov['MergeUpdate']['order_id']] = $ov['MergeUpdate']['order_id'];
				 		
			}
		}	
		
		/*
		
		 pr( $or_details);
		 echo "</pre>";exit*/;
		$paid_open_unpicked_data = array();	 
		$stock_picked_picklist_data = array();
		$processed_order = array();	
		$scaned_order = array();	
		$sourceArray = $this->getOpenOrder($orderArray);
		$OrderLocationArray = $this->getOrderLocation($orderArray);
		 foreach($productAllDescs as $v){
			
			$sku = $v['Product']['product_sku'];
			$skuArray[$sku] = $sku; 
				 			 			 															
				if(isset($getdetails[$sku]) && count($getdetails[$sku]) > 0 )
				{
					foreach( $getdetails[$sku] as $merge_sku => $ov )
					{						
						$status = $ov['status'];
						$start  = date("Y-m-d")." 00:00:00";
						$end    = date("Y-m-d")." 23:59:59";
						$source_name = ''; $order_location = ''; $po_name = '';
						if(isset($sourceArray[$ov['order_id']])){
							  $source_name = $sourceArray[$ov['order_id']];
						}
						if(isset($OrderLocationArray[$ov['order_id']])){
							  $order_location = $OrderLocationArray[$ov['order_id']]['bin_location'];
							  $po_name = $OrderLocationArray[$ov['order_id']]['po_name'];
						}
					   
						if($status == 0 && $ov['pick_list_status'] == 0 ){
							$paid_open_unpicked_data[$sku][] = array('order_id'=>$ov['order_id'],'quantity'=>$ov['quantity'] ,'destination'=>$ov['delevery_country'], 'source_name'=>$source_name, 'order_date'=>$ov['order_date'], 'picked_date'=>$ov['picked_date'], 'order_location'=>$order_location,'po_name'=>$po_name );		
						}						
						if($status == 0 && $ov['pick_list_status'] == 1 ){
							$stock_picked_picklist_data[$sku][] = array('order_id'=>$ov['order_id'],'quantity'=>$ov['quantity'],'destination'=>$ov['delevery_country'],'source_name'=>$source_name, 'order_date'=>$ov['order_date'], 'picked_date'=>$ov['picked_date'], 'order_location'=>$order_location,'po_name'=>$po_name );		
						}
						if($status == 1 && ($ov['process_date'] > $start && $ov['process_date'] < $end) ){					
							$processed_order[$sku][] = array('order_id'=>$ov['order_id'],'quantity'=>$ov['quantity'],'destination'=>$ov['delevery_country'],'source_name'=>$source_name,'process_date'=>$ov['process_date'], 'order_location'=>$order_location,'po_name'=>$po_name );	
						}
						if($status == 1 && ($ov['scan_date'] > $start && $ov['scan_date'] < $end) ){
							$scaned_order[$sku][] = array('order_id'=>$ov['order_id'],'quantity'=>$ov['quantity'],'destination'=>$ov['delevery_country'],'source_name'=>$source_name,'scan_date'=>$ov['scan_date'], 'order_location'=>$order_location ,'po_name'=>$po_name);	
						}		
					 
					}
				}
		}  
		
		//foreach($skuArray
		
		/*	*/
		//pr($orderArray);
		
		
		//$this->set('sourceArray',$sourceArray); 
		$this->set('paid_open_unpicked_data',$paid_open_unpicked_data); 
		$this->set('stock_picked_picklist_data',$stock_picked_picklist_data);
		$this->set('processed_order',$processed_order);
		$this->set('scaned_order',$scaned_order);
		
		$this->set('productAllDescs',$productAllDescs);	       
		
		$this->set( 'role','SKU\'s Current Status Report' );        
	}
	
	
	public function getDhlOrders()
	{
		//$oid = array('605535','604437','602516','604730','601804','604990','604578','602818','602420','602491','602620','605704');
		//$oid = array('601804','602420','602491','602620','604990','605704');
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		$track_id = array();
		
 		$MergeUpdate = $this->MergeUpdate->find('all', array('conditions' => array('service_provider'=> 'DHL','status'=> '0'),  'fields' => array( 'order_id','track_id') ) );
		if( count($MergeUpdate) > 0)
		{
			$oid[] = 1; 
			foreach($MergeUpdate as $val){
				$oid[] = $val['MergeUpdate']['order_id'];
				$track_id[$val['MergeUpdate']['order_id']] = $val['MergeUpdate']['track_id'];
			}
			
			$getopenOrders = $this->OpenOrder->find('all', array('conditions' => array('num_order_id IN'=>$oid),  'fields' => array( 'OpenOrder.sub_source','OpenOrder.destination','OpenOrder.customer_info','OpenOrder.items','OpenOrder.num_order_id','OpenOrder.status') ) );
			 
			$path = $_SERVER['DOCUMENT_ROOT'].'/dhl/';
			$filename = "dhl_order.csv";
			$header = "Orderid,Sub Source,Name,Email,Address1,Address2,Address3,City,Region,Post Code,Destinetion,Phone,Sku,Weight,Qty,Title,Status,Tracking No\r\n";
	
			file_put_contents($path.$filename,$header, LOCK_EX);
			foreach( $getopenOrders as $getopenOrder )
			{
				$subsource		=	$getopenOrder['OpenOrder']['sub_source'];
				$destination	=	$getopenOrder['OpenOrder']['destination'];
				$orderid		=	$getopenOrder['OpenOrder']['num_order_id'];
				$status			=	$getopenOrder['OpenOrder']['status'];
				$date			=	'';
				$customerInfo	=	unserialize( $getopenOrder['OpenOrder']['customer_info'] );
				$cusEmail		=	$customerInfo->Address->EmailAddress;
				$Address1		=	str_replace(",",";",utf8_decode( $customerInfo->Address->Address1));
				$Address2		=	str_replace(",",";",utf8_decode($customerInfo->Address->Address2));
				$Address3		=	str_replace(",",";",utf8_decode($customerInfo->Address->Address3));
				$name			=	utf8_decode($customerInfo->Address->FullName);
				$city			=	utf8_decode($customerInfo->Address->Town);
				$Region			=	utf8_decode($customerInfo->Address->Region);
				$postCode		=	$customerInfo->Address->PostCode;
				$phone			=	$customerInfo->Address->PhoneNumber;
				$tracking_id	=   isset($track_id[$orderid]) ? $track_id[$orderid] :'';
				
				$items			=	unserialize( $getopenOrder['OpenOrder']['items'] );
				$sku = array();$qty = array();$title = array();
				foreach( $items as $item )
				{
					$sku[] 		= $item->SKU;
					$qty[] 		= $item->Quantity;
					$title[] 	= $item->Title;
				}
				$weight = $this->getWeight($orderid);
				if($status == 0 )
				{
					$statusstr = 'Open';
				} elseif($status == 1)
				{
					$statusstr = 'Processed';
				} elseif($status == 2) {
					$statusstr = 'Cancelled';
				} else {
					$statusstr = 'Locked';
				}
				
				$content = $orderid.",".$subsource.",".$name.",".$cusEmail.",".$Address1.",".$Address2.",".$Address3.",".$city.",".$Region.",".$postCode.",".$destination.",".$phone.",".implode( ';',$sku ).",".$weight.",".implode(';',$qty).",".implode(';',str_replace(",",";",$title)).",".$statusstr.",".$tracking_id."\r\n";
				file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);
			}
			
			
			header('Content-Description: File Transfer');
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path.$filename));
			ob_clean();
			flush();
			readfile($path.$filename);
			exit;
		}else{
			 $this->Session->setflash( 'No DHL orders found.', 'flash_danger' ); 
			 $this->redirect($this->referer());
		}
		
	}
	
	public function getWeight($order_id){
	
		$this->loadModel('MergeUpdate');
		
		$MergeUpdate	=	$this->MergeUpdate->find('all', array('conditions' => array('order_id' => $order_id),'fields' => array('packet_weight')) );
		
		foreach($MergeUpdate as $val){
			$weight[] = $val['MergeUpdate']['packet_weight'];
		}
		return array_sum($weight);
	}
	public function getCountry( )
	{
		$order_id = '610263';
		 
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'Country' );
		$this->loadModel( 'DhlMatrix' );
		$this->loadModel( 'PostalServiceDesc' );
		
		//$cInfo = $openOrder['customer_info']; 
		$openOrder = $this->OpenOrder->find('first',array('conditions' => array('OpenOrder.num_order_id' => $order_id)));		 
		$cInfo = $openOrder['OpenOrder']['customer_info']; 
		
		
		$_orders = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.order_id' => $order_id),'fields'=>array('sku','product_order_id_identify')));		
		
		$pos = strpos($_orders['MergeUpdate']['sku'],",");
		$mergeId = $_orders['MergeUpdate']['id'];
		
		if ($pos === false) {
			echo $val  = $_orders['MergeUpdate']['sku'];
			$s = explode("XS-", $val);
			echo "===";
			echo $_sku = "S-".$s[1];		
			$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name')));
			pr($product);
			$length = $product['ProductDesc']['length'] / 10;
			$width  = $product['ProductDesc']['width'] / 10;
			$height = $product['ProductDesc']['height'] / 10;
			$weight = $product['ProductDesc']['weight'];	
			$cat_name = $product['Product']['category_name'];	
					
		}else{			
			$sks = explode(",",$_orders['MergeUpdate']['sku']);
			foreach($sks as $val){
				$s = explode("X", $val);
				$_sku = $s[1]; 
				$product = $this->ProductDesc->find('first',array('conditions' => array('ProductDesc.product_defined_skus' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name')));
				$_length[] = $product['ProductDesc']['length'];
				$_width[]  = $product['ProductDesc']['width'];
				$_height[] = $product['ProductDesc']['height'];
				$_weight[] = $product['ProductDesc']['weight'];	
				$_catname[$product['Product']['category_name']] = $product['Product']['category_name'];				
			}		
			$length	= array_sum($_length) / 10;
			$width	= array_sum($_width) / 10;
			$height	= array_sum($_height) / 10;
			$weight	= array_sum($_weight);	
			$cat_name	= implode("<br>",$_catname);
		}
		 
		if($weight < 0.1) $weight = 0.1; 
		
		$FromCompany = 'EURACO GROUP LTD';
		$FromPersonName = 'EURACO GROUP LTD';
		$FromAddress1 = '36 HARBOUR REACH';
		$FromAddress2 = 'LA RUE DE CARTERET';
		$FromCity = 'ST HELIER';
		$FromDivision = 'JE';
		$FromPostCode = 'JE2 4HR';
		$FromCountryCode = 'GB';
		$FromCountryName = 'JERSEY';
		$FromPhoneNumber = '+44 7829 900766';
		 
	  	 App::import( 'Controller' , 'DhlPostals' );		
		 $objDhlController = new DhlPostalsController();
 
		 App::import( 'Controller' , 'Invoice' );		
		 $_invoice = new InvoiceController();	
		
		$order = $_invoice->getOrderByNumIdDhl( $order_id );	
		 		
	   // $pieces[1] = array('length'=>'3','width'=>'2','height'=>'2','weight'=>'.2');
		/*$_details = array('merge_id'=> $mergeId,'order_id'=> $_orders['MergeUpdate']['product_order_id_identify'],'sub_total' => number_format($order->TotalsInfo->Subtotal,2),'order_total'=>$order->TotalsInfo->TotalCharge,'order_currency'=>$order->TotalsInfo->Currency,'products'=> $cat_name,'length'=> ceil($length),'width'=> ceil($width),'height'=> ceil($height),'weight'=> number_format($weight,1), 'Company'=>'','FullName'=>'COURTIN Patrick','Address1' =>'13 rue Paul DEMANGE','Address2' =>'','Town' =>'MEUDON LA FORET','Region' =>'','PostCode' =>'92360','CountryCode' =>'FR','CountryName' =>'France','PhoneNumber'=>'0546020715','FromCompany'=>$FromCompany,'FromPersonName'=>$FromPersonName,'FromAddress1' =>$FromAddress1,'FromAddress2' =>$FromAddress2,'FromCity' =>$FromCity,'FromDivision' =>$FromDivision,'FromPostCode' =>$FromPostCode,'FromCountryCode' =>$FromCountryCode,'FromCountryName' =>$FromCountryName,'FromPhoneNumber'=>$FromPhoneNumber);*/
		
		 	$_details = array('merge_id'=> $mergeId,'order_id'=> $_orders['MergeUpdate']['product_order_id_identify'],'sub_total' => number_format($order->TotalsInfo->Subtotal,2),'order_currency'=>$order->TotalsInfo->Currency,'products'=> $cat_name,'length'=> ceil($length),'width'=> ceil($width),'height'=> ceil($height),'weight'=> number_format($weight,1),'Company'=>$cInfo->Address->Company,'FullName'=> utf8_decode($cInfo->Address->FullName),'Address1' =>utf8_decode($Address1),'Address2' =>utf8_decode($Address2),'Town' =>utf8_decode($cInfo->Address->Town),'Region' =>utf8_decode($cInfo->Address->Region),'PostCode' =>$PostCode,'CountryName'=>$cInfo->Address->Country,'CountryCode' =>$country_data['Country']['iso_2'],'PhoneNumber'=>$cInfo->Address->PhoneNumber,'FromCompany'=>$FromCompany,'FromPersonName'=>$FromPersonName,'FromAddress1' =>$FromAddress1,'FromAddress2' =>$FromAddress2,'FromCity' =>$FromCity,'FromDivision' =>$FromDivision,'FromPostCode' =>$FromPostCode,'FromCountryCode' =>$FromCountryCode,'FromCountryName' =>$FromCountryName,'FromPhoneNumber'=>$FromPhoneNumber);
		 	pr($_details);
 		//$getShippingRates = $objDhlController->dhlServiceStart( 1 , 'DHL_REQUEST_WORLD_WIDE_CAP_QUOTE',$_details);
	 	// pr( $getShippingRates);
		 
		 $SHIP_REG = $objDhlController->dhlServiceStart(2, 'DHL_REQUEST_WORLD_WIDE_SHIP_REG',$_details);
					
		 $mergeSubOrderId = $_orders['MergeUpdate']['product_order_id_identify'];
		 
		
		 
		 if(is_object($order)){			
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();	
				$result 		=	'';
				$htmlTemplate	=	'';	
		
				$htmlTemplate	=	$_invoice->getTemplateDHL($order);
				$SubSource		=	$order->GeneralInfo->SubSource;
				$result			=	ucfirst(strtolower(substr($SubSource, 0, 4)));
				
				/**************** for tempplate *******************/
				$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
					 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
					 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
					 <meta content="" name="description"/>
					 <meta content="" name="author"/>
					 <style>'.file_get_contents(WWW_ROOT .'css/pdfstyle.css').' body { font-family: times }</style>';
				$html .= '<body>'.$htmlTemplate.'</body>';
						
				$name	= $mergeSubOrderId.'.pdf';							
				$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
				$dompdf->render();			
								
				$file_to_save = WWW_ROOT .'img/dhl/invoice_'.$name;
				//save the pdf file on the server
				file_put_contents($file_to_save, $dompdf->output()); 
				$dhl_path = $_SERVER['DOCUMENT_ROOT'] .'/dhl/invoice_'.$name; 
				file_put_contents($dhl_path, $dompdf->output()); 
			}
         exit;
							
							
	}	
	public function replaceFrenchChar($string = null){
			
		$unwanted_array = array('�'=>'S', '�'=>'s', '�'=>'Z', '�'=>'z', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E','�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', 'N�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U','�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c','�'=>'e', '�'=>'e', '�'=>'e','�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n','n�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o','�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'b', '�'=>'y','�'=>'u','�'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}
	
	
	public function getOrderReports()
	{
		$this->loadModel( 'MergeUpdate' );
		$datetime = date("Y-m-d");
		$onemonth	=	date("Y-m-d", strtotime("-30 days",strtotime($datetime)));
		$getprodessedOrders	=	$this->MergeUpdate->find('all', array( 'conditions' => array('status' => 1, 'process_date >=' => $onemonth.' 00:00:00') ) );
		
		$path = WWW_ROOT.'/img/';
		
		$filename = "processedorder.csv";
		$header = "Orderid,Sub Orders,Sku,Quantity,User,OperaterID,Process date, Jersey Time\r\n";
		file_put_contents($path.$filename,$header, LOCK_EX);
		foreach($getprodessedOrders as $getprodessedOrder)
		{
			$orderid	=	$getprodessedOrder['MergeUpdate']['order_id'];
			$suborderid	=	$getprodessedOrder['MergeUpdate']['product_order_id_identify'];
			$sku		=	$getprodessedOrder['MergeUpdate']['sku'];
			$qty		=	$getprodessedOrder['MergeUpdate']['quantity'];
			$username	=	$this->getprocessedUser( $getprodessedOrder['MergeUpdate']['user_id'] );
			$operateid	=	$this->getOperaterId( $getprodessedOrder['MergeUpdate']['user_id'] );
			$processDate=	$getprodessedOrder['MergeUpdate']['process_date'];
			$jerseyTime	=	date("Y-m-d H:i:s", strtotime("+4 hours",strtotime($processDate)));
			$content = $orderid.",".$suborderid.",". str_replace(",","&",$sku).",".$qty.",".$username.",".$operateid.",".$processDate.",".$jerseyTime."\r\n";
			file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);
		}
			header('Content-Description: File Transfer');
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path.$filename));
			ob_clean();
			flush();
			readfile($path.$filename);
			exit;
		
	}
	
	public function getprocessedUser( $userid )
	{
		$this->loadModel( 'User' );
		$getuserdata	=	$this->User->find('first', array( 'conditions' => array( 'id' => $userid ) ) );
		
		if(count($getuserdata) == 1)
		{
			$username = $getuserdata['User']['first_name'].' '.$getuserdata['User']['last_name'];
		}else {
			$username = '----';
		}
		return $username;
	}
	public function getOperaterId( $userid )
	{
		$this->loadModel( 'User' );
		$getuserdata	=	$this->User->find('first', array( 'conditions' => array( 'id' => $userid ) ) );
		
		if(count($getuserdata) == 1)
		{
			$opraterid = $getuserdata['User']['username'];
		}else {
			$opraterid = '----';
		}
		return $opraterid;
	}
	
	public function savecustomerinfo()
	{
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'NegativeCustomerMark' );
		$orderid				=	$this->request->data['orderid'];
		$status				=	$this->request->data['status'];
		$orderidstatus 			=	explode('####',$orderid);
		
		$getorderdetail			=	$this->OpenOrder->find('first', array( 'conditions' => array('OpenOrder.num_order_id' => $orderid ) ) );
		$CustomerInfo			=	 unserialize($getorderdetail['OpenOrder']['customer_info']);
		$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
	   	$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
	 	$pcName = ( $this->Session->read('Auth.User.pc_name') != '' ) ? $this->Session->read('Auth.User.pc_name') : '_';
	    $userid = ( $this->Session->read('Auth.User.id') != '' ) ? $this->Session->read('Auth.User.id') : '_';
	    $username = $firstName.' '.$lastName;
		
		if($status == 'block' )
		{
			$email					=	$CustomerInfo->Address->EmailAddress;
			$data['order_id'] 		= 	$orderid;
			$data['amazon_email'] 	= 	$email;
			$data['user']			=	$username ;
			$this->NegativeCustomerMark->saveAll($data);
			echo "1";
			exit;
		} else { 
			$conditionsformergeupdate = array ('order_id' => $orderidstatus[0]);
			$this->NegativeCustomerMark->deleteAll( $conditionsformergeupdate );
			echo "2";
			exit;
		}
		
	}
	
	public function releaseuserorder()
	{
		$this->loadModel('MergeUpdate');
	    $this->loadModel('ScanOrder');
		$splitorderid	=	$this->request->data['splitorderid'];
		$this->MergeUpdate->updateAll( array( 'MergeUpdate.assign_user' => "''", 'MergeUpdate.user_name' => "''", 'MergeUpdate.pc_name' => "''"), array( 'MergeUpdate.product_order_id_identify' => $splitorderid ) );
		$this->ScanOrder->updateAll( array( 'ScanOrder.user_name' => "''", 'ScanOrder.pc_name' => "''", 'update_quantity' => '0'), array( 'ScanOrder.split_order_id' => $splitorderid ) );
		$update_log = "Order Id=".$splitorderid."\tUser=".$this->Session->read('Auth.User.username')."\t".date('Y-m-d H:i:s')."\n";
		file_put_contents(WWW_ROOT."img/releaseorder.log", $update_log, FILE_APPEND | LOCK_EX);
		echo "1";
		exit;
	}
	public function getAllInventory(){
 		 
		$this->loadModel( 'Product' );				
		$this->loadModel( 'BinLocation' );	
		 	 
		$Products = $this->Product->find( 'all',array('fields' => array('ProductDesc.barcode','Product.product_sku','Product.current_stock_level'),'order product_sku DESC'));
		$boday = ''; $count = 1; $data = array();
		foreach($Products as $sk){			
			$bin = $this->BinLocation->find('all', array( 'conditions' => array( 'barcode' => $sk['ProductDesc']['barcode'] ) ) );
			$bin_qty = 0 ;
			foreach($bin as $qty){
				$bin_qty += $qty['BinLocation']['stock_by_location'];
			}
			
			if($bin_qty != $sk['Product']['current_stock_level']){
				
				$count++;
				
				$final_qty = abs($bin_qty - $sk['Product']['current_stock_level']);
				
				$data[$final_qty][]  = array('details'=>'<tr><td>'.$sk['Product']['product_sku'] .'</td><td>'.$sk['ProductDesc']['barcode'] .'</td><td>'.$sk['Product']['current_stock_level'].'</td><td>'.$bin_qty.'</td><td>'.$final_qty.'</td></tr>');
				
			}
		
		}
		krsort($data);
		foreach($data as $d){
			foreach($d as $v){ 
				$boday .= $v['details'];
			}
		}		  
		 
		if($boday){ 
			$message  = '<html><body>';
			$message .= '<table border="1" bordercolor="#CCCCCC" cellpadding="5" cellspacing="0" width="700"> ';
			$message .= '<tr><th>Sku('.$count.')</th><th>Barcode</th><th>Current Stock </th><th>Bin Stock</th><th>Diff</div></tr>';
			$message .= $boday;
			$message .= '</table>';
			echo $message .= '</body></html><br>';
			
	
			App::uses('CakeEmail', 'Network/Email');
			$email = new CakeEmail('');
			$email->emailFormat('html');
			$email->from('info@xsensys.com');
			$email->to( array( 'avadhesh.kumar@jijgroup.com','amit.gaur@jijgroup.com' ) );		 
			$email->subject('Xsensys Inventorty Report');
			$email->send( $message );
		}else{
			echo 'All is good.';
		}					
		 
     exit;
	}
	
	
	/*public function DelayOrders($delay_hours = 24){
	
		$this->loadModel( 'MergeUpdate' );
  		 
		
		$sdate = date('Y-m-d H:i:s') 	;  
		$date_range = array( date("Y-m-d H:i:s",strtotime("-{$delay_hours} hours", strtotime($sdate))), date('Y-m-d H:i:s', strtotime($sdate )) ); 
		$fields = array('order_id','product_order_id_identify','quantity','order_date','picked_date','process_date','user_id','assign_user','pc_name');	 			
		$getLateOrders = $this->MergeUpdate->find('all', array('conditions' => array( 'MergeUpdate.status' => 0 ,'order_date BETWEEN ? AND ?' => $date_range) , 'fields' =>$fields ) );
		pr($getLateOrders);
		 
	}*/
	
	public function DelayOrders($days = 1){
	
		$this->loadModel( 'MergeUpdate' );
 		//$sdate = date('Y-m-d', strtotime("-24 hours")); 		
		$sdate = $this->nextDate( date('Y-m-d') ); 
		
		$fields = array('order_id','product_order_id_identify','quantity','order_date','picked_date','process_date','user_id','assign_user','pc_name');	 			
		
		$getLateOrders = $this->MergeUpdate->find('all', array('conditions' => array( 'MergeUpdate.status' => 0 ,'order_date < ' => $sdate) , 'fields' =>$fields ) );
		
		if(count($getLateOrders)){
 			$path = WWW_ROOT.'/logs/';
 			$filename = "DelayOrders.csv";
			$header = "Order id,Move Date,Process date\r\n";
			file_put_contents($path.$filename,$header, LOCK_EX);
			foreach($getLateOrders as $val)
			{
				$orderid		= $val['MergeUpdate']['order_id'];
				$order_date		= $val['MergeUpdate']['order_date'];
				$process_date	= $val['MergeUpdate']['process_date'];
				$content = $orderid.",".$order_date.",".$process_date."\r\n";
				file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);
			}
			header('Content-Description: File Transfer');
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path.$filename));
			ob_clean();
			flush();
			readfile($path.$filename);
			exit;
		}else{
			 $this->Session->setflash( 'No orders found.', 'flash_danger' ); 
			 $this->redirect($this->referer());
		} 		 
	}
	
	public function DelayOrdersCount($day = 1){
	
		$this->loadModel( 'MergeUpdate' );  
		
  	 	// $sdate = date('Y-m-d', strtotime("-24 hours")); 
		$sdate = $this->nextDate( date('Y-m-d') ); 	 
		//$date_range = array( date("Y-m-d H:i:s",strtotime("-{$delay_hours} hours", strtotime($sdate))), date('Y-m-d H:i:s', strtotime($sdate )) ); 
		 		
		/*$getLateOrders = $this->MergeUpdate->find('count', array('conditions' => array( 'MergeUpdate.status' => 0 ,'order_date BETWEEN ? AND ?' => $date_range)) );*/
 		$DelayOrders = $this->MergeUpdate->find('count', array('conditions' => array( 'MergeUpdate.status' => 0 ,'order_date < ' => $sdate)) );
  		return $DelayOrders;
 	}
	
	private function nextDate($_date = null)
	{   
		$thursday = date('Y-m-d', strtotime('thursday of week'));
		$sunday   = date('Y-m-d', strtotime('sunday of week'));
		
		$holidays = array($thursday,$sunday,"2017-08-04","2017-08-28","2017-12-25","2017-12-26","2018-01-01","2018-03-30","2018-04-02","2018-05-07","2018-05-09","2018-05-28","2018-08-27","2018-08-27","2018-12-25","2018-12-26"); 		 
		
		$_date = date('Y-m-d',strtotime($_date . "-1 day"));
					
		if(in_array($_date, $holidays)){				
			$_date = $this->nextDate($_date);
		} 	
				
		return  $_date;
	}
	
	public function LateDispatchRate($days = 30){
	
		$this->loadModel( 'MergeUpdate' );
		$sdate      = $this->nextDate(date('Y-m-d'));//date('Y-m-d',strtotime("-1 day")); 
		$hours 		= (strtotime(date("Y-m-d")) - strtotime($sdate))/3600;
		$date_range = array( date("Y-m-d H:i:s",strtotime("-{$days} days", strtotime($sdate))), date('Y-m-d H:i:s', strtotime($sdate )) ); 
		 
		$fields = array('order_id','product_order_id_identify','quantity','order_date','picked_date','process_date','manifest_date','user_id','assign_user','pc_name');
 			
 		$getLateOrders = $this->MergeUpdate->find('count', array( 'conditions' => array( 'MergeUpdate.status IN' => array(1,3) ,'process_date BETWEEN ? AND ?' => $date_range,'TIMESTAMPDIFF(HOUR, order_date, process_date) > ' => $hours ) ) );
		
		$allOrders = $this->MergeUpdate->find('count', array( 'conditions' => array( 'MergeUpdate.status IN' => array(1,3) ,'process_date BETWEEN ? AND ?' => $date_range ) ) );
		
		//pr($getLateOrders); echo "==";
		//pr($allOrders);
		//echo "==<br>";
		//echo ($getLateOrders * 100) / $allOrders; 
		return array('total_orders' => $allOrders, 'late_orders' => $getLateOrders,'date_range' => $date_range);
		exit;
	}
	
	public function TotalOrders($days = 30){
	
 		$this->loadModel( 'MergeUpdate' );
		
 		$sdate      = date('Y-m-d',strtotime("-1 day"));
		$date_range = array( date("Y-m-d H:i:s",strtotime("-{$days} days", strtotime($sdate))), date('Y-m-d H:i:s', strtotime($sdate )) ); 
		 
		$fields = array('order_id','product_order_id_identify','quantity','order_date','picked_date','process_date','user_id','assign_user','pc_name');
 			
 		$getLateOrders = $this->MergeUpdate->find('all', array( 'conditions' => array('MergeUpdate.status IN' => array(1,3) ,'process_date BETWEEN ? AND ?' => $date_range ),'fields' =>$fields ) );
		
		if(count($getLateOrders)){
 			$path = WWW_ROOT.'/logs/';
 			$filename = "TotalOrders.csv";
			$header = "Order id,Move Date,Process date\r\n";
			file_put_contents($path.$filename,$header, LOCK_EX);
			foreach($getLateOrders as $val)
			{
				$orderid		= $val['MergeUpdate']['order_id'];
				$order_date		= $val['MergeUpdate']['order_date'];
				$process_date	= $val['MergeUpdate']['process_date'];
				$content = $orderid.",".$order_date.",".$process_date."\r\n";
				file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);
			}
			header('Content-Description: File Transfer');
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path.$filename));
			ob_clean();
			flush();
			readfile($path.$filename);
			exit;
		}else{
			 $this->Session->setflash( 'No orders found.', 'flash_danger' ); 
			 $this->redirect($this->referer());
		} 		 
	}
	
	public function LateDispatch($days = 30){
 		$this->loadModel( 'MergeUpdate' );
 		$sdate      = $this->nextDate(date('Y-m-d'));		
		$hours 		= (strtotime(date("Y-m-d")) - strtotime($sdate))/3600;
 		//$sdate      = date('Y-m-d',strtotime("-1 day"));
		$date_range = array( date("Y-m-d H:i:s",strtotime("-{$days} days", strtotime($sdate))), date('Y-m-d H:i:s', strtotime($sdate )) ); 
		 
		$fields = array('order_id','product_order_id_identify','quantity','order_date','picked_date','process_date','manifest_date','user_id','assign_user','pc_name');
 			
 		$getLateOrders = $this->MergeUpdate->find('all', array( 'conditions' => array('MergeUpdate.status IN' => array(1,3) ,'process_date BETWEEN ? AND ?' => $date_range,'TIMESTAMPDIFF(HOUR, order_date, process_date) > ' => $hours ),'fields' =>$fields ) );
		
		if(count($getLateOrders)){
 			$path = WWW_ROOT.'/logs/';
 			$filename = "LateDispatch.csv";
			$header = "Order id,Move Date,Process date,Export date\r\n";
			file_put_contents($path.$filename,$header, LOCK_EX);
			foreach($getLateOrders as $val)
			{
				$orderid		= $val['MergeUpdate']['order_id'];
				$order_date		= $val['MergeUpdate']['order_date'];
				$process_date	= $val['MergeUpdate']['process_date'];
				$content = $orderid.",".$order_date.",".$process_date.",".$val['MergeUpdate']['manifest_date']."\r\n";
				file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);
			}
			header('Content-Description: File Transfer');
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path.$filename));
			ob_clean();
			flush();
			readfile($path.$filename);
			exit;
		}else{
			 $this->Session->setflash( 'No orders found.', 'flash_danger' ); 
			 $this->redirect($this->referer());
		} 		 
	}
	
	public function dashboardData()
	{
 		/*-----------------Checked in but not sold-----------------*/
		$this->layout = 'index';
 		$this->loadModel('SalesEod'); 
		$this->loadModel('StockEod');
		$this->loadModel('Product');  
  			 
 		$sale_skus = [];
		$not_sale_skus = [];
		
		$not_selling = 10;
		if(isset($_REQUEST['days'])){
			$not_selling = $_REQUEST['days'];
		}
 		
       	$getFrom 	= date('Y-m-d' ,strtotime("- {$not_selling} days"));
		$getEnd 	= date('Y-m-d');
		
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		 
 		$sales = []; $not_sale = []; $final_not_sale = [];
		$all_data =  $this->SalesEod->find( 'all', $paramHostory); 
 		if(count($all_data) > 0){
			foreach($all_data as $items){
  				$normal_stock = json_decode($items['SalesEod']['normal_sale'],1);
				foreach($normal_stock as $sku => $stock){ 
					if(is_numeric($stock) && $stock == 0){
						//$sold_out[$sku][$items['SalesEod']['sale_date']] = $stock;	
						$not_sale[$sku] = $sku;
 					}elseif(is_numeric($stock) && $stock > 0){
						$sales[$sku] = $sku;
					}
  				}	
			}	
   		}	
		
		foreach($not_sale as $sku){
			if(!in_array($sku,$sales)){
				$final_not_sale[$sku] = $sku;
			}
		}
					
		 /*-----------------Sold out--------------------*/
		$sold_out_days = 3;
		if(isset($_REQUEST['sold_out_days'])){
			$sold_out_days = $_REQUEST['sold_out_days'];
		}
 		
       	$getFrom 	= date('Y-m-d' ,strtotime("- {$sold_out_days} days"));
		$getEnd 	= date('Y-m-d');
		
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		 
 		$sales = []; $sold_out_sku = []; $sold_out = [];
		$all_data =  $this->SalesEod->find( 'all', $paramHostory); 
 		if(count($all_data) > 0){
			foreach($all_data as $items){
  				$normal_stock = json_decode($items['SalesEod']['normal_sale'],1);
				foreach($normal_stock as $sku => $stock){ 
					if($stock == '-'){
						//$sold_out[$sku][$items['SalesEod']['sale_date']] = $stock;	
						$sold_out_sku[$sku] = $sku;
 					}else{
						$sales[$sku] = $sku;
					}
  				}	
			}	
   		}	
		 	 
		foreach($sold_out_sku as $sku){ 
			if(!in_array($sku,$sales)){
				$sold_out[$sku] = $sku;
			}
		}
		 
 		/*-------------------Recomendation-------------------*/
		$this->loadModel('SalesAverage');
   		$stockDetail = $this->SalesAverage->find('all');
	  
		foreach($stockDetail as $inv )
		{
 			$recomendation[$inv['SalesAverage']['master_sku']] = trim($inv['SalesAverage']['master_sku']);
			 
   		}
  		echo json_encode(['skus'=>count($final_not_sale),'sold_out_sku'=> count($sold_out),'recomendation'=> $recomendation,'recomendation_count'=> count($recomendation)]);		
			 
		exit;
  	 
	}
	
  	public function recomendationReport()
	{
 		/*-----------------Checked in but not sold-----------------*/
		$this->layout = 'index';
 		$this->loadModel( 'CheckIn' );  
		$this->loadModel( 'SupplierMapping' ); 
 			 
 		$sep = ",";
		$path = WWW_ROOT.'logs/';
		 
 		/*-------------------Recomendation-------------------*/
		
		
		$filename = 'recomendation_'.date('dmY').'.csv'; 
  		$header = "sku,suppliers"."\r\n";
 		file_put_contents($path.$filename,$header, LOCK_EX); 
		
		$rec = [];
		$date = date("Y-m-d",strtotime("+1 day"));
  	 
 		$stockDetail = $this->CheckIn->find('all', array('conditions' => array( 'expiry_date >= ' => $date),'fields'=>['id','sku','date','expiry_date','qty_checkIn','selling_qty','damage_qty','expired_selling_qty']) );
	  
		foreach($stockDetail as $inv )
		{
 			$sku = trim($inv['CheckIn']['sku']);
			$qty = $inv['CheckIn']['qty_checkIn'] - ($inv['CheckIn']['selling_qty'] + $inv['CheckIn']['damage_qty'] + $inv['CheckIn']['expired_selling_qty']);
			$rec[$sku][] = $qty;
   		}
		$recom = [];
		foreach($rec as $sk => $arr){
			$recom[$sk] = array_sum($rec[$sk]);
		}
		$recomendation = [];
		foreach($recom as $sk => $qt){
			if($qt < 1){
				//$recomendation[$sk] = $qt;
				$smap = [];
				$sup = $this->SupplierMapping->find('all', array('conditions' => array( 'master_sku' => $sk),'fields'=>['sup_code','supplier_sku']) );
				if(count($sup) > 0){
					foreach($sup as $v){
						$smap[$v['SupplierMapping']['sup_code']] = $v['SupplierMapping']['sup_code'];
					}
				}	
				file_put_contents($path.$filename,$sk.",".implode(";",$smap)."\r\n", FILE_APPEND | LOCK_EX);	
			}
		}
 				
 		ob_clean();
		flush();
		header('Content-Description: File Transfer');
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename='.basename($filename));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$filename));
		
		readfile( WWW_ROOT .'logs/'.$filename);
			 
		exit;
  	 
	}
	
	public function notSellingReport()
	{
		$this->loadModel('SalesEod'); 
		$this->loadModel('StockEod');
		$this->loadModel('CheckIn');
		$this->loadModel('ScanOrder'); 
 		$not_selling = 5;
		if(isset($_REQUEST['not_selling'])){
			$not_selling = $_REQUEST['not_selling'];
		}
		
       	$getFrom 	= date('Y-m-d' ,strtotime("- {$not_selling} days"));
		$getEnd 	= date('Y-m-d');
		
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		 
 		$sales = []; $skusales = []; $all_skus = [];
		$all_data =  $this->SalesEod->find( 'all', $paramHostory); 
 		if(count($all_data) > 0){
			foreach($all_data as $items){
  				$normal_stock = json_decode($items['SalesEod']['merchant_sale'],1);
				foreach($normal_stock as $sku => $stock){ 
					if(is_numeric($stock) && $stock == 0){
						//$sales[$sku][$items['SalesEod']['sale_date']] = $stock;	
						$skusales[$sku] = $sku;	
					}else if(is_numeric($stock) && $stock > 0){
						$sales[$sku] = $sku;	
					}
					$all_skus[$sku] = $sku;
  				}	
			}	
   		}	
		$checkin_data = []; $last_checkin = []; $last_sale = [];  
		 
		$stockDetail = $this->CheckIn->find('all', array('conditions' => array('sku' =>$all_skus,  '`qty_checkIn` > `selling_qty`','date >' => '2021-01-01'),'fields'=>['id','sku','date','qty_checkIn','selling_qty','po_name']) );
	   
		if(count($stockDetail) > 0){
			foreach($stockDetail as $inv )
			{
				$sku    	= trim($inv['CheckIn']['sku']);
				$po_name    = trim($inv['CheckIn']['po_name']);
 				if($inv['CheckIn']['qty_checkIn'] > $inv['CheckIn']['selling_qty'] ){
					$checkin_data[$sku][$po_name] = $po_name;
				}
				
				$_days   = floor((strtotime(date("Y-m-d H:i:s")) -  strtotime($inv['CheckIn']['date'])) / (60 * 60 * 24)); 
				$last_checkin[$sku][$_days] = ['date'=>date('Y-m-d',strtotime($inv['CheckIn']['date'])),'days'=>$_days];
 			}
		}
		
		$lastSale = $this->ScanOrder->find('all', array('conditions' => array('sku' =>$all_skus),'fields'=>['sku','scan_date'],'order'=>['ScanOrder.id DESC']) );
		if(count($lastSale) > 0){					 
			foreach($lastSale as $sl){
				$sku   = trim($sl['ScanOrder']['sku']);
				$_days = floor((strtotime(date("Y-m-d H:i:s")) -  strtotime($sl['ScanOrder']['scan_date'])) / (60 * 60 * 24)); 
				$last_sale[$sku][$_days] = ['date'=>$sl['ScanOrder']['scan_date'],'days'=>$_days];
			}
		}
		
  		$sep = ",";
		$path = WWW_ROOT.'logs/';
		$filename = 'notSelling_'.date('dmY').'.csv'; 
		$header = "sku,last_checkin,last_checkin_days,lot numbers,last_sale,last_sale_days"."\r\n";
		file_put_contents($path.$filename,$header, LOCK_EX); 
		foreach($skusales as $sku => $saleArr){ 
  			//$skusales[$sku] = ceil(array_sum($sales[$sku])/count($sales[$sku]));
			$content = $sku.$sep ;
			if(!in_array($sku,$sales)){
				if(isset($last_checkin[$sku])){
				   $key = min( array_keys($last_checkin[$sku]));
 				   $content .= $last_checkin[$sku][$key]['date']. $sep .$last_checkin[$sku][$key]['days']. $sep;
				}else{
					$content .= '-'. $sep . '-'. $sep;
				}
				if(isset($checkin_data[$sku])){
 				   $content .= implode(";",$checkin_data[$sku]) . $sep;
				}else{
					$content .=  '-'. $sep;
				}
 				if(isset($last_sale[$sku])){	
  					$key = min( array_keys($last_sale[$sku]));
  				    $content .= $last_sale[$sku][$key]['date']. $sep .$last_sale[$sku][$key]['days'];
				}else{
					$content .= '-'. $sep . '-';
				}
  				file_put_contents($path.$filename,$content."\r\n", FILE_APPEND | LOCK_EX);	
			}
		}
		 
		ob_clean();
		flush();
		header('Content-Description: File Transfer');
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename='.basename($filename));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$filename));
		
		readfile( WWW_ROOT .'logs/'.$filename);
			 
		exit;
	} 
	
	public function notCheckInReport()
	{
 		/*-----------------Checked in but not sold-----------------*/
		$this->layout = 'index';
 		$this->loadModel( 'Product' );  
		$this->loadModel( 'CheckIn' );  
 			 
 		$not_checkin = 0;
		if(isset($_REQUEST['not_checkin'])){
			$not_checkin = $_REQUEST['not_checkin'];
		}
 		$not_checkin_date  = date('Y-m-d', strtotime("-{$not_checkin} days")); 
 		
		$sep = ",";
		$path = WWW_ROOT.'logs/'; 
 		$filename = 'notCheckInReport_'.date('dmY').'.csv'; 
  		$header = "sku".$sep."added date".$sep."days"."\r\n";
 		file_put_contents($path.$filename,$header, LOCK_EX); 
 	
		$stockDetail = $this->Product->find('all', array('conditions' => array( 'current_stock_level <=' => 0, 'added_date <' => $not_checkin_date),'fields'=>['id','product_sku','added_date'],'order' => 'Product.id ASC'));
	  
		foreach($stockDetail as $product )
		{
 			$main_sku = $product['Product']['product_sku'];  
 			$invResults = $this->CheckIn->find('first', array( 'conditions' => array( 'sku' => $main_sku), 'fields'=>['sku'],'order' => 'date DESC' ) );
  			if(count($invResults) == 0){ 
				$days = floor((strtotime(date("Y-m-d H:i:s")) -  strtotime($product['Product']['added_date'])) / (60 * 60 * 24));
				file_put_contents($path.$filename,$main_sku.$sep.date('Y-m-d',strtotime($product['Product']['added_date'])).$sep.$days."\r\n", FILE_APPEND | LOCK_EX);	
			}
 		}
		 
 		ob_clean();
		flush();
		header('Content-Description: File Transfer');
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename='.basename($filename));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$filename));
 		readfile( WWW_ROOT .'logs/'.$filename);
 		exit;
  	 
	}
	
	public function soldOutReport()
	{
		$this->loadModel('SalesEod'); 
		$this->loadModel('StockEod');
		$this->loadModel('CheckIn');
		$this->loadModel('ScanOrder'); 
		$this->loadModel('OrdersItem');
		$this->loadModel('Po');
		$this->loadModel('PurchaseOrder');
			
		$sold_out = 5;
		if(isset($_REQUEST['sold_out'])){
			$sold_out = $_REQUEST['sold_out'];
		}
		
       	$getFrom 	= date('Y-m-d' ,strtotime("- {$sold_out} days"));
		$getEnd 	= date('Y-m-d');
		
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		 
 		$sales = []; $skusales = []; $all_skus = [];
  		$all_data =  $this->SalesEod->find( 'all', $paramHostory); 
 		if(count($all_data) > 0){
			foreach($all_data as $items){
  				$normal_stock = json_decode($items['SalesEod']['merchant_sale'],1);
				foreach($normal_stock as $sku => $stock){ 
					if($stock == '-'){
						//$sales[$sku][$items['SalesEod']['sale_date']] = $stock;	
						$skusales[$sku] = $sku;	
					}else{
						$sales[$sku] = $sku;	
					}
					$all_skus[$sku] = $sku;	
  				}	
			}	
   		}	
		$last_checkin = []; $last_sale = [];  $purchase_skus = []; 
 		foreach($all_skus as $sku){
		
 			$lastChin = $this->CheckIn->find('first', array('conditions' => array('sku' =>$sku),'fields'=>['id','sku','date','qty_checkIn','selling_qty','damage_qty','po_name'],'order'=>['id ASC']) );
			if(count($lastChin) > 0){
				$_lot_no = trim($lastChin['CheckIn']['po_name']);
				$_days   = floor((strtotime(date("Y-m-d H:i:s")) -  strtotime($lastChin['CheckIn']['date'])) / (60 * 60 * 24)); 
				$last_checkin[$sku] = ['date'=>date('Y-m-d',strtotime($lastChin['CheckIn']['date'])),'days'=>$_days];
			}
 			
 			$lastSale = $this->ScanOrder->find('first', array('conditions' => array('sku' =>$sku),'fields'=>['sku','scan_date'],'order'=>['ScanOrder.id DESC']) );
 			if(count($lastSale) > 0){					 
				$_days = floor((strtotime(date("Y-m-d H:i:s")) -  strtotime($lastSale['ScanOrder']['scan_date'])) / (60 * 60 * 24)); 
				$last_sale[$sku] = ['date'=>$lastSale['ScanOrder']['scan_date'],'days'=>$_days];
			}
			
   			$params	=	array(
					'joins' => array(
							array(
								'table' => 'pos',
								'alias' => 'Po',
								'type' => 'INNER',
								'conditions' => array(
									'Po.id = PurchaseOrder.po_id'
								)
							)
						),
					'conditions' => array( 
									'Po.recived_status' => '0',
									'PurchaseOrder.purchase_sku' => $sku,
									),
					'fields' => array('PurchaseOrder.purchase_sku','PurchaseOrder.total_qty')
 				);
				
			$purchaseQty = 0;
			$skuCounts	=	$this->PurchaseOrder->find( 'all', $params );
 			foreach( $skuCounts as $skuCount )
			{ 				 
				$purchaseQty += $skuCount['PurchaseOrder']['total_qty'];
   			}
			$purchase_skus[$sku] = $purchaseQty;
			
		}
		 
 		$sep = ",";
		$path = WWW_ROOT.'logs/';
		$filename = 'soldOutReport_'.date('dmY').'.csv'; 
		$header = "sku,last_checkin,last_checkin_days,last_sale,last_sale_days,Intransit"."\r\n";
		file_put_contents($path.$filename,$header, LOCK_EX); 
		foreach($skusales as $sku => $saleArr){ 
  			//$skusales[$sku] = ceil(array_sum($sales[$sku])/count($sales[$sku]));
			$content = $sku.$sep ;
			if(!in_array($sku,$sales)){
				if(isset($last_checkin[$sku])){
 				   $content .= $last_checkin[$sku]['date']. $sep .$last_checkin[$sku]['days']. $sep;
				}else{
					$content .= '-'. $sep . '-'. $sep;
				}
 				if(isset($last_sale[$sku])){				   
				    $content .= $last_sale[$sku]['date']. $sep .$last_sale[$sku]['days']. $sep;
				}else{
					$content .= '-'. $sep . '-'. $sep;
				}
				if(isset($purchase_skus[$sku])){				   
				    $content .= $purchase_skus[$sku];
				}else{
					$content .= '0';
				}
  				file_put_contents($path.$filename,$content."\r\n", FILE_APPEND | LOCK_EX);	
			}
		}
		 
		ob_clean();
		flush();
		header('Content-Description: File Transfer');
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename='.basename($filename));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
 		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$filename));
		
		readfile( WWW_ROOT .'logs/'.$filename);
			 
		exit;
	}
	
	public function OverStock()
	{
		$this->loadModel('SalesEod'); 
		$this->loadModel('StockEod');
		$this->loadModel('Product');
	  			
		$runrate_days = 7;
		if(isset($_REQUEST['runrate_days'])){
			$runrate_days = $_REQUEST['runrate_days'];
		}
		$report_days = 30;
		if(isset($_REQUEST['report_days'])){
			$report_days = $_REQUEST['report_days'];
		}
		
       	$getFrom 	= date('Y-m-d' ,strtotime("- {$runrate_days} days"));
		$getEnd 	= date('Y-m-d');
		
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		$sales = []; $runrate = [];
		$all_data =  $this->SalesEod->find( 'all', $paramHostory); 
 		if(count($all_data) > 0){
			foreach($all_data as $items){
  				$normal_stock = json_decode($items['SalesEod']['merchant_sale'],1);
				foreach($normal_stock as $sku => $stock){ 
					if(is_numeric($stock)){
						$sales[$sku][$items['SalesEod']['sale_date']] = $stock;	
 					}
  				}	
			}	
   		}	 
		
 		foreach($sales as $sku => $saleArr){ 
 			$runrate[$sku] = ceil(array_sum($sales[$sku])/count($sales[$sku]));
		}
   
   		$products = $this->Product->find('all', array('conditions' => array('is_deleted' =>0),'fields'=>['product_sku','current_stock_level']) );
  		$sep = ",";
		$path = WWW_ROOT.'logs/';
		$filename = 'Over_Stock_Of_'.$report_days.'_days_'.date('dmY').'.csv'; 
		$header = "Sku,Current Stock,Days,Average Sale,Over Stock"."\r\n";
		file_put_contents($path.$filename,$header, LOCK_EX); 
		foreach($products as $arr){ 
			$sku = $arr['Product']['product_sku'];
			$_runrate = 0; $required_stock = 0;
			if(isset($runrate[$sku])){
				$_runrate = $runrate[$sku];
				$required_stock = $_runrate * $report_days;

			} 			 
			$over_stock  = $arr['Product']['current_stock_level'] - $required_stock;
			if($over_stock > 0){
  				$content = $sku . $sep . $arr['Product']['current_stock_level']. $sep . $report_days. $sep . $_runrate. $sep . $over_stock;
   				file_put_contents($path.$filename,$content."\r\n", FILE_APPEND | LOCK_EX);	
			}
		}
		 
		ob_clean();
		flush();
		header('Content-Description: File Transfer');
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename='.basename($filename));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$filename));
		
		readfile( WWW_ROOT .'logs/'.$filename);
			 
		exit;
	}
	//avadhesh
	public function getEodData()
	{
 		$this->layout = 'index'; 
 	}	
	
	public function downloadFbaMerchantSalesEod()
	{ 
		$this->layout = 'index';
		$this->autorender = false;
		$this->loadModel('Product');  
 		$this->loadModel('StockEod'); 
		$this->loadModel('SalesEod');  
		
       	$getFrom 	= date('Y-m-d' ,strtotime($this->request->query['start']));
		$getEnd 	= date('Y-m-d' ,strtotime($this->request->query['end']));
		
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		 
		//Processing now
		$all_data =  $this->SalesEod->find( 'all', $paramHostory); 
  		 
		ob_clean(); 
		$sep = ",";
		$content = 'SKU'; $salesData = []; $sales = [];
		$file_name = 'sale_merchant_fba_'.$this->Session->read('Auth.User.username').'.csv'; 
		file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");	
		if(count($all_data) > 0){
			foreach($all_data as $items){
 				$content .= $sep .  date('d M Y', strtotime($items['SalesEod']['sale_date'])) ; 
				file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");	
				
				$merchant = json_decode($items['SalesEod']['merchant_sale'],1);
				 
				foreach($merchant as $sku => $stock){ 
				 
					if(substr($sku,0,1) == 'B'){
						$_sk = explode("-",$sku);
						$ind = count($_sk) - 1;
						$_sku = '';
						for($b = 1; $b < $ind  ; $b++){
							$_sku = 'S-'.$_sk[$b];
							if(count($_sk ) > 3){
								$salesData[$items['SalesEod']['sale_date']][$_sku][] = $stock;	
							}else{
								$t = explode("-",$sku);
 								$salesData[$items['SalesEod']['sale_date']][$_sku][] = ($stock * $t[2]);	
							}
						}
  					}
					else{
  						$salesData[$items['SalesEod']['sale_date']][$sku][] = $stock	;	
					}
  				}
				
				$fba = json_decode($items['SalesEod']['fba_sale'],1);
				foreach($fba as $sku => $stock){ 
				 
					if(substr($sku,0,1) == 'B'){
						$_sk = explode("-",$sku);
						$ind = count($_sk) - 1;
						$_sku = '';
						for($b = 1; $b < $ind  ; $b++){
							$_sku = 'S-'.$_sk[$b];
							if(count($_sk ) > 3){
								$salesData[$items['SalesEod']['sale_date']][$_sku][] = $stock;	
							}else{
								$t = explode("-",$sku);
 								$salesData[$items['SalesEod']['sale_date']][$_sku][] = ($stock * $t[2]);	
							}
						}
  					}
					else{
  						$salesData[$items['SalesEod']['sale_date']][$sku][] = $stock	;	
					}
  				}	
			}	
   		}	 
		 
		foreach($salesData as $date => $data){ 
 		  foreach(array_keys($data) as $sku){
			 $sales[$date][$sku] =  array_sum($data[$sku])	;
		  }
 		}
		//pr($sales);
		//exit; 
 		$products = $this->Product->find('all', array('conditions'=>['is_deleted' => 0],'fields' =>['product_sku'],'order' => 'product_sku desc') ); 	
		foreach($products as $pr){
			$content =	trim($pr['Product']['product_sku']);
  			foreach($all_data as $items){								
				$date = $items['SalesEod']['sale_date'];  
 				$sale = '-';
  				if(isset($sales[$date][trim($pr['Product']['product_sku'])])){
					$sale = $sales[$date][trim($pr['Product']['product_sku'])];
				}
 				$content .= $sep . $sale; 
			}
 			file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n", FILE_APPEND | LOCK_EX);		
		}
			
		  
		header('Content-Description: File Transfer');
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename='.basename($file_name));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$file_name));
		ob_clean();
		flush();
		readfile( WWW_ROOT .'logs/'.$file_name);
		exit;
 	}
	
	public function downloadAsinEod()
	{  
		$this->layout = 'index';
		$this->autorender = false;
		$this->loadModel('Product');  
 		$this->loadModel('StockEod'); 
		$this->loadModel('SalesEod');  
		
       	$getFrom 	= date('Y-m-d' ,strtotime($this->request->query['start']));
		$getEnd 	= date('Y-m-d' ,strtotime($this->request->query['end']));
		
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		 
		//Processing now
		$all_data =  $this->SalesEod->find( 'all', $paramHostory); 
  		 
		ob_clean(); 
		$sep = ",";
		$content = 'SKU,CHANNEL SKU,ASIN'; $salesData = []; $sales = []; $datesArr = [];
		$file_name = 'sale_'.$this->Session->read('Auth.User.username').'.csv'; 
		file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");	
		if(count($all_data) > 0){
			foreach($all_data as $items){
				$content .= $sep .  date('d M Y', strtotime($items['SalesEod']['sale_date'])) ; 
				file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");
				
				$date = date('Y-m-d', strtotime($items['SalesEod']['sale_date']));
				$datesArr[$date] = $date;
				
 				$merchant = json_decode($items['SalesEod']['total_sale'],1);
				 
				foreach($merchant as $sku => $stock){ 
 					 
					$sales[$items['SalesEod']['sale_date']][$sku]['sku_sale']  = $stock['sku_sale']	;
					$sales[$items['SalesEod']['sale_date']][$sku]['channel_sku']  = $stock['channel_sku'];
					$sales[$items['SalesEod']['sale_date']][$sku]['asins']  = $stock['asins'];
   				}	
			}	
   		}	 
		 
 		//pr($sales);
		//exit; 
		 
  		$products = $this->Product->find('all', array('conditions'=>['is_deleted' => 0],'fields' =>['product_sku'],'order' => 'product_sku desc') ); 	
		foreach($products as $pr){
			$content =	$sku = trim($pr['Product']['product_sku']);
			$content .= ' , ,';
  			$asin_sku_d = []; $ch_sku_d = []; $asin_sku = [];
			foreach($all_data as $items){								
				$date = $items['SalesEod']['sale_date'];  
   				$sale = '-';
  				if(isset($sales[$date][$sku])){
					$ss = $sales[$date][$sku];
					$sale = str_replace(",",";", json_encode($ss['sku_sale']) );
					//$sale .= str_replace(",",";", json_encode($ss['channel_sku']) ). str_replace(",",";",json_encode($ss['asins']));
					
					foreach($ss['asins'] as $asn => $aqty){
						//$asin_sku[$sku][$asn][] = $aqty;
						$asin_sku_d[$sku][$date][$asn][] = $aqty;
						foreach($ss['channel_sku'] as $chs => $q){
							$asin_sku[$sku][$chs] = $asn;
						}
					}
					
					foreach($ss['channel_sku'] as $chs => $aqty){
						$ch_sku[$sku][$chs][] = $aqty;
						$ch_sku_d[$sku][$date][$chs][] = $aqty;
					}
					
 					//$sale = str_replace(",",";", json_encode($ss['channel_sku']) ). str_replace(",",";",json_encode($ss['asins']));
 				} 
				
 				$content .= $sep . $sale; 
			}
 			file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n", FILE_APPEND | LOCK_EX);
			
 			$str = '';
			foreach(array_keys($ch_sku[$sku]) as $chs){   
 				$str  = ' ,'.$chs . $sep . $asin_sku[$sku][$chs];  
				foreach($datesArr as $date){  
				 	$str  .= $sep . array_sum($ch_sku_d[$sku][$date][$chs]);
					//$str  .= $sep .  date('d M Y', strtotime($date)) ; 
					
				}
 				file_put_contents(WWW_ROOT.'logs/'.$file_name, $str."\r\n", FILE_APPEND | LOCK_EX);
			}
			
		}
		//exit;
		  
 		header('Content-Description: File Transfer');
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename='.basename($file_name));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$file_name));
		ob_clean();
		flush();
		readfile( WWW_ROOT .'logs/'.$file_name);
		exit;
 	}
	public function downloadAsinChannelSkuEod()
	{  
		$this->layout = 'index';
		$this->autorender = false;
		$this->loadModel('Product');  
 		$this->loadModel('StockEod'); 
		$this->loadModel('SalesEod'); 
		
       	$getFrom 	= date('Y-m-d' ,strtotime($this->request->query['start']));
		$getEnd 	= date('Y-m-d' ,strtotime($this->request->query['end']));
		
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		//Processing now
		$all_data =  $this->SalesEod->find( 'all', $paramHostory); 
		
		ob_clean(); 
		$sep = ",";
		$content = 'SKU,CHANNEL SKU,ASIN'; 
		$salesData = []; $sales = [];$msales = []; $fsales = []; $datesArr = [];
		$file_name = 'sale_fba_fbm_'.$this->Session->read('Auth.User.username').'.csv'; 
		file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");	
		if(count($all_data) > 0){
			foreach($all_data as $items){
				$content .= $sep .  date('d M Y', strtotime($items['SalesEod']['sale_date'])) ; 
				file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");
				
				$date = date('Y-m-d', strtotime($items['SalesEod']['sale_date']));
				$datesArr[$date] = $date;
				
 				$merchant = json_decode($items['SalesEod']['total_sale'],1);
 				foreach($merchant as $sku => $stock){ 
					$sales[$items['SalesEod']['sale_date']][$sku]['sku_sale']  = $stock['sku_sale']	;
					$sales[$items['SalesEod']['sale_date']][$sku]['channel_sku']  = $stock['channel_sku'];
					$sales[$items['SalesEod']['sale_date']][$sku]['asins']  = $stock['asins'];
   				}	
				$fba_sales = json_decode($items['SalesEod']['fba_total_sales'],1);
 				 
				foreach($fba_sales as $sku => $stock){ 
					if(isset($sales[$items['SalesEod']['sale_date']][$sku])){
						$sku_sale = $sales[$items['SalesEod']['sale_date']][$sku]['sku_sale'];
 						$sales[$items['SalesEod']['sale_date']][$sku]['sku_sale'] = ($sku_sale + $stock['sku_sale']);
						
						$mchannel_sku = $sales[$items['SalesEod']['sale_date']][$sku]['channel_sku'];
						$fchannel_sku = $stock['channel_sku'];
						$sums = [];
						foreach (array_keys($mchannel_sku + $fchannel_sku) as $key) {
							$sums[$key] = (isset($mchannel_sku[$key]) ? $mchannel_sku[$key] : 0) + (isset($fchannel_sku[$key]) ? $fchannel_sku[$key] : 0);
						}
  						$sales[$items['SalesEod']['sale_date']][$sku]['channel_sku'] = $sums;
						
						$masins = $sales[$items['SalesEod']['sale_date']][$sku]['asins'];
						$fasins = $stock['asins'];
						$sums = [];
						foreach (array_keys($masins + $fasins) as $key) {
							$sums[$key] = (isset($masins[$key]) ? $masins[$key] : 0) + (isset($fasins[$key]) ? $fasins[$key] : 0);
						}
  						$sales[$items['SalesEod']['sale_date']][$sku]['asins'] = $sums;
						
					}else{
						$sales[$items['SalesEod']['sale_date']][$sku]['sku_sale']  = $stock['sku_sale']	;
						$sales[$items['SalesEod']['sale_date']][$sku]['channel_sku']  = $stock['channel_sku'];
						$sales[$items['SalesEod']['sale_date']][$sku]['asins']  = $stock['asins'];
					}
   				}	
				
 			}	
   		}	 		 
		 		 
		/*foreach($salesData as $date => $data){ 
 		  foreach(array_keys($data) as $sku){
			 $sales[$date][$sku] =  array_sum($data[$sku])	;
		  }
 		}*/
 		//pr($sales);
		//exit; 
		$ch_sku =[]; 
  		$products = $this->Product->find('all', array('conditions'=>['is_deleted' => 0,'product_sku LIKE' => 'S-%'],'fields' =>['product_sku'],'order' => 'product_sku desc') ); 	
		foreach($products as $pr){
			$content =	$sku = trim($pr['Product']['product_sku']);
			$content .= ' , ,';
  			$asin_sku_d = []; $ch_sku_d = []; $asin_sku = [];
			foreach($all_data as $items){								
				$date = $items['SalesEod']['sale_date'];  
   				$sale = '-';
  				if(isset($sales[$date][$sku])){
					$ss = $sales[$date][$sku];
					$sale = str_replace(",",";", json_encode($ss['sku_sale']) );
					//$sale .= str_replace(",",";", json_encode($ss['channel_sku']) ). str_replace(",",";",json_encode($ss['asins']));
					
					foreach($ss['asins'] as $asn => $aqty){
						//$asin_sku[$sku][$asn][] = $aqty;
						$asin_sku_d[$sku][$date][$asn][] = $aqty;
						foreach($ss['channel_sku'] as $chs => $q){
							$asin_sku[$sku][$chs] = $asn;
						}
					}
					
					foreach($ss['channel_sku'] as $chs => $aqty){
						$ch_sku[$sku][$chs][] = $aqty;
						$ch_sku_d[$sku][$date][$chs][] = $aqty;
					}
					
 					//$sale = str_replace(",",";", json_encode($ss['channel_sku']) ). str_replace(",",";",json_encode($ss['asins']));
 				} 
				
 				$content .= $sep . $sale; 
			}
 			file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n", FILE_APPEND | LOCK_EX);
			
 			$str = '';
			foreach(array_keys($ch_sku[$sku]) as $chs){   
 				$str  = ' ,'.$chs . $sep . $asin_sku[$sku][$chs];  
				foreach($datesArr as $date){  
				 	$str  .= $sep . array_sum($ch_sku_d[$sku][$date][$chs]);
					//$str  .= $sep .  date('d M Y', strtotime($date)) ; 
					
				}
 				file_put_contents(WWW_ROOT.'logs/'.$file_name, $str."\r\n", FILE_APPEND | LOCK_EX);
			}
			
		}
		//exit;
		  
 		header('Content-Description: File Transfer');
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename='.basename($file_name));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$file_name));
		ob_clean();
		flush();
		readfile( WWW_ROOT .'logs/'.$file_name);
		exit;
 	}
	
}

?>


 
