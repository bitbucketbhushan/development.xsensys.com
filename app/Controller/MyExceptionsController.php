<?php

class MyExceptionsController extends AppController
{
    var $name = "MyExceptions";    
    var $components = array('Session','Upload','Common','Auth');    
    var $helpers = array('Html','Form','Common','Session');
   
    /* Get Pc's Name now */
    public function allowNotification( $authData = null )
    {
		$this->layout = '';
		$this->autoRander = false;
		
		App::import('Vendor', 'Notification/Pusher');		
		$wmsNOTIFIY = $this->getNotifyObject( true );

		$data['message'] = $authData['first_name'] . ' ' . $authData['last_name'] . ' has logged.';
		$wmsNOTIFIY->trigger('test_channel', 'my_event', $data);
				
	}
	
	public function getNotifyObject( $option = null )
	{
		
		$options = array(
			'encrypted' => $option
		);
		
		$key = Configure::read('notifyCenter');
		$key = $key['key'];
		$secret = Configure::read('notifyCenter');
		$secret = $secret['secret'];
		$appId = Configure::read('notifyCenter');		
		$appId = $appId['app_id'];				
		$wmsNOTIFIY = new Pusher(
			$key, 
			$secret, 
			$appId,
			$options
		);
		
	return $wmsNOTIFIY;
	}
	
	/* order notification */
    public function send_order_notification( $getDeleteCancel = null )
    {
		$this->layout = '';
		$this->autoRander = false;
		
		App::import('Vendor', 'Notification/Pusher');		
		$wmsNOTIFIY = $this->getNotifyObject( true );

		$data['message'] = "Please verify, {$getDeleteCancel} Cancel/Delete orders!";
		$wmsNOTIFIY->trigger('test_channel', 'my_event', $data);
	}
	
	/* Order Cancel / Delete notification enable */
    public function send_cancel_delete_order_notification( $getDeleteCancel = null )
    {
		$this->layout = '';
		$this->autoRander = false;
		
		App::import('Vendor', 'Notification/Pusher');		
		$wmsNOTIFIY = $this->getNotifyObject( true );
		
		$name  = ucfirst($getDeleteCancel[0]);
		$id  = $getDeleteCancel[1];
		$type  = $getDeleteCancel[2];
		
		$data['message'] = "Order ({$id}) has {$type} by ({$name})";
		$wmsNOTIFIY->trigger('test_channel', 'my_event', $data);
	}
	
	/* Sync start */
    public function syncCalling( $locationName = null )
    {
		$this->layout = '';
		$this->autoRander = false;
		
		App::import('Vendor', 'Notification/Pusher');		
		$wmsNOTIFIY = $this->getNotifyObject( true );

		$data['message'] = $locationName . ' sync is going to start now...';
		$wmsNOTIFIY->trigger('test_channel', 'my_event', $data);
				
	}
	
	/* Sync stop */
    public function syncComp( $locationName = null )
    {
		$this->layout = '';
		$this->autoRander = false;
		
		App::import('Vendor', 'Notification/Pusher');		
		$wmsNOTIFIY = $this->getNotifyObject( true );

		$data['message'] = $locationName . ' sync completed...';
		$wmsNOTIFIY->trigger('test_channel', 'my_event', $data);
				
	}
	
}

?>
