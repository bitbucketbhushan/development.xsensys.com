<?php

class ConfigsettingsController extends AppController
{
    
    var $name = "Configsettings";
    var $components = array('Session','Upload','Common','Auth');
    var $helpers = array('Html','Form','Common','Session');
	 
	public function setSplitRule()
	{
		$this->layout = 'index';
		$this->loadModel( 'SplitRule' );
		
		if( $this->request->is('post') )
			{
				$this->SplitRule->set( $this->request->data );
			if( $this->SplitRule->validates( $this->request->data ) )
					{
						$this->SplitRule->saveAll( $this->request->data );
						$this->Session->setFlash('Split rule changed successfully', 'flash_success');
					}
			}
		
		
		$splitRuleDetail	=	$this->SplitRule->find( 'first' );
		$this->request->data = $splitRuleDetail;
	}
	
	public function setConversionRate()
	{
		$this->layout = 'index';
		$this->loadModel( 'ConversionRule' );
		
		if( $this->request->is('post') )
			{
				$this->ConversionRule->set( $this->request->data );
			if( $this->ConversionRule->validates( $this->request->data ) )
				{
					 $this->ConversionRule->saveAll( $this->request->data );
					 $this->Session->setFlash('Conversion rate changed successfully', 'flash_success');
				}
			}
	
		$coversionRuleDetail	=	$this->ConversionRule->find( 'first' );
		$this->request->data 	= 	$coversionRuleDetail;
	}
    
}

?>
