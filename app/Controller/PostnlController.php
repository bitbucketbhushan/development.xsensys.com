<?php
error_reporting(1);
class PostnlController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Postnl";
    var $components = array('Session','Upload','Common','Auth','Paginator');
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
	public function beforeFilter()
    {
		parent::beforeFilter();
		$this->layout = false;
		$this->Auth->Allow(array('Barcode'));
		$this->Common = $this->Components->load('Common');   
    }
	public function index($order_id)
    {		
		$this->loadModel('OpenOrder');
		$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id,'OpenOrder.destination NOT IN' => ['Italy','United Kingdom','Netherlands','Brazil'] ) ) );
		
		$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
		$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
		$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
	//	pr($general_info);	
		pr($customer_info);	
		//pr($totals_info);	
		echo $_address1 = $customer_info->Address->Address1;
		echo " == ";
		echo $this->replaceChar(ltrim($_address1,","));
		
		exit;
	}
	
	public function Assistlabel($destination_country_code = 'FR')
    {
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('PostnlAssistlabel');
		
		$auth = Configure::read( 'postnl' );
		$CustomerCode = $auth['customer_code'];
		$CustomerNumber = $auth['customer_number'];
		$api_key = $auth['api_key']; 
		$server_url = $auth['server_url'];
		$url = $server_url."/api/assistlabel/generate"; 
 
		$pdata =	array (
			//'product_code' => $product_code,
			'destination_country_code' => $destination_country_code,
			'label_type' => 'PDF', 
		 );

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,			  
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>json_encode($pdata),
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"api_key: {$api_key}",
			"cache-control: no-cache"
		  ),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		$file = $destination_country_code .date('ymdhis');
		$imgPath = WWW_ROOT .'postnl/assistlabel/request_'.$file.'.json'; 				 
		file_put_contents($imgPath, json_encode($pdata));
		$imgPath = WWW_ROOT .'postnl/assistlabel/response_'.$file.'.json'; 				 
		file_put_contents($imgPath,$response);
				
		if ($err) {
		  //echo "cURL Error #:" . $err;
			$data['error_code'] = $err;
			$data['error_msg']  = 'Barcode not generated';
			$this->sendErrorMail($data);
		} else {
				$res = json_decode($response,1);
				pr($res);
				if($res['status'] == 'success'){	
					foreach($res['data']['assist_labels'] as $vs){ 	 
						$savedata['product_name'] 	= $vs['product_name'] ;
						$savedata['client_code'] 	= $vs['client_code'];
						$savedata['country_code'] 	= $vs['destination_country_code'];
						$savedata['country_name']	= $vs['destination_country_name'];
						$savedata['barcode'] 		= $vs['barcode'] ;
						$savedata['message'] 		= $vs['base64string'];						
						$savedata['added_date'] 	= date('Y-m-d H:i:s');
						$this->PostnlAssistlabel->saveAll($savedata); 
						 
						$content = base64_decode($vs['base64string']);
						$imgPath = WWW_ROOT .'postnl/assistlabel/'.$vs['barcode'].'.pdf'; 			 
						file_put_contents($imgPath,$content);
 					}
				}
		 // return $r['Barcode'];
		}
		exit;  
	}

  	public function Labelling($order_id)
 	{
		$this->loadModel('MergeUpdate'); 
		$this->loadModel('OpenOrder');
		$this->loadModel('Country');
		$this->loadModel('Product');
		$this->loadModel('ProductHscode'); 
		$this->loadModel('PurchaseOrder');  
		$country_of_origin = 'CN';
		$hs_code = '9603298000';//'8470300000';
		$product_name = '';		
 			
		$orders	= $this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $order_id, 'MergeUpdate.service_provider' => 'PostNL','MergeUpdate.brt' => 0,'MergeUpdate.country_code NOT IN' => ['IT','NL']),'fields'=>array('sku','product_order_id_identify','order_id','postnl_barcode','country_code','provider_ref_code','status','postal_service','quantity','price','envelope_weight') ) );
		 pr($orders);
 		if(count($orders) > 0){
		
			foreach($orders	 as $v){
		
			$provider_ref_code =  $v['MergeUpdate']['provider_ref_code'];
			
 			$this->getBarcodeOutside($v['MergeUpdate']['product_order_id_identify']);
 					
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $v['MergeUpdate']['order_id'],'OpenOrder.destination NOT IN' => ['Italy','Netherlands'] ) ) );
			if(count($openOrder) > 0){
			
				$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
				$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
				$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
				// pr( unserialize( $openOrder['OpenOrder']['items']));
				
				$totals_info->Subtotal;
				$totals_info->PostageCost;
				$totals_info->Tax;		
				$totals_info->Currency; 
				$total_of_up = 0; $Items = [];
				$country	=	$this->Country->find('first', array( 'conditions' => array('name' =>trim($customer_info->Address->Country)) ) );
				if(count($country) == 0){
					$country	=	$this->Country->find('first', array( 'conditions' => array('custom_name' =>trim($customer_info->Address->Country)) ) );
				}
				
				if(count($country) > 0){
					$_length = []; $_width = []; $_height = []; $_weight = [];
					$pkgweight = $v['MergeUpdate']['envelope_weight'];		
					$postnl_barcode = $v['MergeUpdate']['postnl_barcode'];  
					$po_total = 0;
					$skus = explode( ',', $v['MergeUpdate']['sku']);
					foreach( $skus as $_sku )
					{
						$newSku 	= explode( 'XS-', $_sku);  
						$getsku 	= 'S-'.$newSku[1];
						$qty 		= $newSku[0];
						$pos 		= $this->PurchaseOrder->find( 'first', array( 'conditions' => array('purchase_sku' => $getsku,'price >'=> 0 ),'order'=> 'id desc' ) );
						if(count($pos) > 0){
							$po_total += ($pos['PurchaseOrder']['price']*$qty);
						}  
 					}
					
					if($po_total == 0){
						$po_total = $v['MergeUpdate']['price'];
					}
					
					
					foreach($skus as $val){
						$s     = explode("XS-", $val);
						$_qqty = $s[0];
						$_sku = "S-".$s[1];	
						
						$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => $_sku,'country_code' =>  $country['Country']['iso_2'])));
						if(count($hscode) == 0){
							$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' =>  $country['Country']['iso_2'])));
						}
						if(count($hscode) == 0){
							$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' => 'GB')));
						}
						if(count($hscode) > 0){
							if($hscode['ProductHscode']['hs_code'] != ''){
								$hs_code = $hscode['ProductHscode']['hs_code'];
							}
						}
						
						$product = $this->Product->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.product_name','Product.country_of_origin')));
						
						if($product['Product']['country_of_origin'] != ''){
							$c_code= $this->Country->find('first',array('conditions' => array('Country.name' => $product['Product']['country_of_origin']),'fields'=>array('Country.iso_2'))); 
							if(count($c_code) > 0){
								$country_of_origin = $c_code['Country']['iso_2'];
							}
						}
						$price = $v['MergeUpdate']['price'];
						$pos = $this->PurchaseOrder->find( 'first', array( 'conditions' => array('purchase_sku' => $_sku,'price >'=> 0 ),'order'=> 'id desc' ) );
						if(count($pos) > 0){
							$price = $pos['PurchaseOrder']['price'];
						} 
						$product_name[] = substr($product['Product']['product_name'],0,50);
						$_length[] = $product['ProductDesc']['length'];
						$_width[]  = $product['ProductDesc']['width'];
						$_height[] = $product['ProductDesc']['height'];
						//$_weight[] = $product['ProductDesc']['weight'] * $_qqty;	
						$uw = $product['ProductDesc']['weight'] * 1000 * $_qqty;
						$_weight[] = $uw;
						$uweight 				=  explode(".",$uw)[0];
						$unit_weight			=  str_pad($uweight,4,'0',STR_PAD_LEFT) ;
 					  						
						$unitprice = number_format(( ($totals_info->TotalCharge / $po_total)*$price),2);
						 
						$total_of_up += (($totals_info->TotalCharge / $po_total)*$price);
						
						$Items[]  = [ 'goods_description' => $product['Product']['product_name'],
									  'quantity' => $_qqty,
									  'manufacture_country_code' => $country_of_origin,
									  'total_goods_value'=> number_format($unitprice,2),
									  'total_goods_value_eur' =>number_format($unitprice,2),
									  'total_weight_grams'=> $unit_weight,
									  'tariff' =>  $hs_code];				
					}		
					$length	= array_sum($_length) / 10;
					$width	= array_sum($_width) / 10;
					$height	= array_sum($_height) / 10;
					$weight	= array_sum($_weight) + $pkgweight;	
						
					//$data['total_charge']    	=  $total_of_up ; 
					$data['total_charge']    	=  $totals_info->TotalCharge ;
					$data['hs_code']  		 	=  $hs_code ;
					$data['sku']  		 		=  $v['MergeUpdate']['sku'] ;
					$data['country_of_origin']  =  $country_of_origin ;
					$data['product_name']   	=  $product_name ;
					$data['quantity']   		=  $v['MergeUpdate']['quantity'] ; 
					$data['eu_country']   		=  $country['Country']['eu_country'];
					$data['provider_ref_code']  =  $provider_ref_code ;
					$data['phone_number']   	=  $customer_info->Address->PhoneNumber ;
 					$data['weight'] 			=  str_pad($weight,4,'0',STR_PAD_LEFT) ;
					$data['length'] 			=  str_pad($length,4,'0',STR_PAD_LEFT) ;
					$data['width'] 				=  str_pad($width,4,'0',STR_PAD_LEFT) ;
					$data['height'] 			=  str_pad($height,4,'0',STR_PAD_LEFT) ;
					$data['postal_service'] 	=  $v['MergeUpdate']['postal_service'] ;		
					$data['country_code'] 		=  $country['Country']['iso_2'] ;	
					$data['country'] 			=  $customer_info->Address->Country;
					$data['sub_source'] 		=  strtolower($openOrder['OpenOrder']['sub_source']) ;
					$data['split_order_id'] 	=  $v['MergeUpdate']['product_order_id_identify'] ;	
					$data['description'] 		=  implode(",",$product_name) ;
					$data['status']				=  $v['MergeUpdate']['status'] ;
					$data['postal_service']		=  $v['MergeUpdate']['postal_service'] ; 
					$data['ioss_no']			=  $openOrder['OpenOrder']['ioss_no'] ;
 					$this->getLabel($data,$customer_info,$Items);
				
				}
				else{
					 file_put_contents(WWW_ROOT.'logs/postnl_api_country_'.date('dmY').'.log','Case_1'."\t".$data['split_order_id']."\t".$customer_info->Address->Country."\r\n", FILE_APPEND | LOCK_EX);	
					 
					$data['error_code'] = 'country not found';
					$data['error_msg']  = $customer_info->Address->Country;
 					$this->sendErrorMail($data);
			
				}
 			} 
			// exit;
		}
		}
		return 1;
	}  
	
 	public function getLabel($data, $customer_info,$Items)
	{
		$this->loadModel('PostnlBarcode');
		$this->loadModel('MergeUpdate');	  
		
		$EmailAddress = $customer_info->Address->EmailAddress;
 		$exp = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = strtoupper($exp[$index]);
		
		$sender = $this->getSenderInfo($data,$store_country);
 		/*$CustomerCode = 'HLRQ';
		$CustomerNumber = '10472686';
		$api_key = 'Z7J23nyK3BOEPkR3pgIVAAcOKheVCTDV';*/
 		$auth = Configure::read( 'postnl' );
		$CustomerCode = $auth['customer_code'];
		$CustomerNumber = $auth['customer_number'];
		$api_key = $auth['api_key'];
 		$server_url = $auth['server_url'];
 
  		//IMP(IOSS Mailbag Plus), IMR (IOSS Mailbag Registered) and IMB (IOSS Mailbag Plus Boxable). 
		$ProductCodeDelivery = 'IMP';
		if(in_array($data['provider_ref_code'],['sizeg-nonboxable','sizee-nonboxable'])){
			$ProductCodeDelivery = 'IMP';
		}
		if(in_array($data['postal_service'],['sizeg-boxable','sizee-boxable'])){
			$ProductCodeDelivery = 'IMB';
		}
		if(in_array($data['postal_service'],['sizeg-reg','sizee-reg'])){
			$ProductCodeDelivery = 'IMR';
  		}
    		 
		$_address1 = $this->replaceChar($customer_info->Address->Address1);
 		$address1 = ''; $address2  = ''; $address3 = '';
		$lines = explode("\n", wordwrap(htmlentities($_address1).' '.htmlentities($this->replaceChar($customer_info->Address->Address2)).' '.htmlentities($this->replaceChar($customer_info->Address->Address3)), '30'));
		
		if(isset($lines[0]) && $lines[0] != ''){
			$address1  =  trim($lines[0]) ;
		}
		if(isset($lines[1]) && $lines[1] != ''){
			$address2 =  trim($lines[1]) ;
		}
		if(isset($lines[2]) && $lines[2] != ''){
			$address3 = trim($lines[2]);
		}
		
		 $data['address1'] = $address1;
		 $data['address2'] = $address2;
		 $data['address3'] = $address3;
		 $contacts = [];
		 $customer_phone_number = '';
		 if(isset($customer_info->Address->PhoneNumber) && strlen($customer_info->Address->PhoneNumber) > 8){
  			 preg_match_all('/([\d]+)/', $customer_info->Address->PhoneNumber, $telmatch);
 			 $tel ='';
			 for($i = 0; $i< count($telmatch[0]); $i++){
				$tel .= $telmatch[0][$i];
 			 }
  			 $country_pcode = '';
			 if($customer_info->Address->Country == 'France'){
				 $country_pcode = '+33';
			 }else if($customer_info->Address->Country == 'Germany'){
				 $country_pcode = '+49';
			 }else if($customer_info->Address->Country == 'Spain'){
				 $country_pcode = '+34';
			 }else if($customer_info->Address->Country == 'Italy'){
				 $country_pcode = '+39';
			 }else if($customer_info->Address->Country == 'United Kingdom'){
				 $country_pcode = '+44';
			 }
			 if($country_pcode != ''){
				 $customer_phone_number = $country_pcode.substr($tel,-10);
			 }else{
			 	 $customer_phone_number = $tel;
			 }
			  
 		 } 
 		$IOSS_Number = '123456789'; 
		if(in_array(strtolower($data['sub_source']),['costbreaker_uk','costbreaker_de','costbreaker_es','costbreaker_fr','costbreaker_it','costbreaker_nl','marec_uk','marec_de','marec_es','marec_fr','marec_it','marec_nl','rainbow retail','rainbow retail de','rainbow_retail_fr','rainbow_retail_it'])){
			$IOSS_Number = 'IM4420001201';//Amazon Europe 			
		}else if(strtolower($data['sub_source']) == 'costbreaker_usabuyer'){
			$IOSS_Number = 'IM4420001234';//Amazon US 			
		}else if(strtolower($data['sub_source']) == 'costbreaker_ca'){
			$IOSS_Number = 'IM4420001223';//Amazon Canada  			
		}else if(strtolower($data['sub_source']) == 'costdropper'){
			$IOSS_Number = 'IM2760000924';//Real DE				
		}else if(strtolower($data['sub_source']) == 'cdiscount' || strtolower($data['sub_source']) == 'costbreaker'){
			$IOSS_Number = 'IM2500000295';
 		}
		if($data['ioss_no'] != ''){
			$IOSS_Number = $data['ioss_no'];
		}		
		
		$pdata = array (
			  'product_code' => $ProductCodeDelivery,
			  'label_type' => 'PDF',
			  'item_barcode' => '',
			  'customer_reference_number' => '',
			  'customer_item_id' => $data['split_order_id'],
			  'gross_weight_grams' => str_pad($data['weight']*1000,4,'0',STR_PAD_LEFT) ,
			  'declaration_type' => 'SaleOfGoods',
			  'return' => false,
			  'dangerous_goods' => false,
			  'currency' => 'EUR',
			  'transport_value' => 0,
			  'insurance_value' => 0,
			  'sender_details' => 
			  array (
				'name' => $sender['FromPersonName'],
				'company' => $sender['FromCompany'],
				'address' => $sender['FromAddress'],
				'postal_code' => $sender['FromPostCode'],
				'city' => $sender['FromCity'],
				'state' => '',
				'country' => $sender['FromCountry'],
				'country_code' => $sender['FromCountryCode'],
				'email' => $sender['email'],
				'phone' => '',
				'id_ref' => $IOSS_Number,
			  ),
			  'addressee_details' => 
  
			  array (
				'name' => ucwords(strtolower($this->replaceChar($customer_info->Address->FullName))),
				'address' => ucwords(strtolower($this->getCountryAddress($data))),
				'postal_code' => $customer_info->Address->PostCode,
				'city' => ucwords(strtolower($this->replaceChar($customer_info->Address->Town))),
				'state' => ucwords(strtolower($this->replaceChar($customer_info->Address->Region))),
				'country' => ucwords(strtolower($this->replaceChar($customer_info->Address->Country))),
				'country_code' => $data['country_code'],
				'phone' => $customer_phone_number,
			  ),
			  'content_pieces' => $Items
			  
		);
			
			  
			pr($pdata);
			//$pdata['LabelSignature'] = base64_encode(file_get_contents(Router::url('/', true).'img/signature.gif'));
			
  			file_put_contents(WWW_ROOT.'postnl/request/request_'.$data['split_order_id'].'.json',json_encode($pdata));
  		 
			$curl = curl_init();
 			curl_setopt_array($curl, array(
			  CURLOPT_URL => $server_url."/api/contentlabel/generate",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>json_encode($pdata),
			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"api_key: {$api_key}",
				"cache-control: no-cache"
			  ),
			));
			
			$response = curl_exec($curl);		
			$err = curl_error($curl);			
			curl_close($curl);
			
			file_put_contents(WWW_ROOT.'postnl/response/response_'.$data['split_order_id'].'.json',$response);
			$res = json_decode($response,1);
			pr($res); 
			 
			if ($err) {
			  //echo "cURL Error #:" . $err;
 			  $data['error_code'] = $err;
			  file_put_contents(WWW_ROOT.'logs/postnl_api_'.date('dmY').'.log','Case_1'."\t".$data['split_order_id']."\t".$data['error_code']."\r\n", FILE_APPEND | LOCK_EX);	
			  $this->sendErrorMail($data);
			} else {
				 // echo $response; 
 				  if(isset($res['status']) && $res['status'] == 'fail'){
 					 $data['error_code'] = $res['message']['code'] ;
					 $data['error_msg'] = $res['message']['payload'] ;
					 file_put_contents(WWW_ROOT.'logs/postnl_api_'.date('dmY').'.log','Case_2'."\t".$data['split_order_id']."\t".$data['error_code']."\r\n", FILE_APPEND | LOCK_EX);	
					 $this->lockOrder($data);	
					 $this->sendErrorMail($data);
				  }else{  
				 
				 	if($data['status'] == 3){ 
						$this->unLockOrder($data['split_order_id']);
					}
				  
					$content = base64_decode($res['data']['base64string']);
					$imgPath = WWW_ROOT .'postnl/labels/'.$data['split_order_id'].'.pdf'; 				 
					file_put_contents($imgPath,$content); 
					$this->pdfToImg($data['split_order_id']); 
					$postnl_barcode = $res['data']['item'];
					
					$savedata['split_order_id'] = $data['split_order_id'];
 					$savedata['barcode'] 		= $postnl_barcode;
 					$savedata['message'] 		= json_encode($res['message']);
					$savedata['shipments'] 		= json_encode($res['data']) ;
 					$savedata['timestamp'] 		= date('Y-m-d H:i:s');
					$this->PostnlBarcode->saveAll($savedata); 

					if(strtolower($data['postal_service']) == 'standard'){
			 			$this->MergeUpdate->updateAll( array( 'MergeUpdate.postnl_barcode' => "'".$postnl_barcode."'") , array( 'MergeUpdate.product_order_id_identify' => $data['split_order_id'] ) );	
					}else{
						$this->MergeUpdate->updateAll( array( 'MergeUpdate.track_id' => "'".$postnl_barcode."'",'MergeUpdate.reg_post_number' => "'".$postnl_barcode."'",'MergeUpdate.reg_num_img' => "'".$postnl_barcode.".png'") , array( 'MergeUpdate.product_order_id_identify' => $data['split_order_id'] ) );	
					}
			  		 
				 }
			} 
			return 1;
 	}
	
	public function LabelCloseout()
	{ 
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('MergeUpdate');
		$auth = Configure::read( 'postnl' );
		$CustomerCode = $auth['customer_code'];
		$CustomerNumber = $auth['customer_number'];
		$api_key = $auth['api_key'];
		$server_url = $auth['server_url'];
		$url = $server_url."/api/label/closeout";
			
		$orders	= $this->MergeUpdate->find('all', array( 'conditions' => array('MergeUpdate.service_provider' => 'PostNL','MergeUpdate.brt' => 0,'MergeUpdate.scan_date IS NOT NULL','MergeUpdate.manifest_date IS NULL','MergeUpdate.country_code NOT IN' => ['IT','NL']),'fields'=>array('sku','product_order_id_identify','order_id','postnl_barcode','country_code','provider_ref_code','status','postal_service','quantity','price','envelope_weight','scan_date') ) );
		$items = [];
		foreach($orders as $v){
			$items[] = $v['MergeUpdate']['postnl_barcode'];
		}
		$items[]= 'UD904752226NL';
   		$items[]= 'UD904752274NL';
		pr($items);
		$pdata = array (
			'type' => 'ITEM',
			'product_code' =>'IMP',
			'items' => $items,
		 );
		/* $pdata = array (
		  'type' => 'ASSISTLABEL',
		  'assistlabel_item' => 
		  array (
			'assistlabel' => 'FRB130900000000178',
			'receptacle_type' => 'BG',
			'format' => 'E',
			'product_code' => 'IMP',
			'items' => $items,			 
		  ),
		);*/
		pr($pdata );//echo json_encode($pdata);exit;
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,			  
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>json_encode($pdata),
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"api_key: {$api_key}",
			"cache-control: no-cache"
		  ),
		));
		
  		$response = curl_exec($curl);
		$err = curl_error($curl);
 		curl_close($curl);
		file_put_contents(WWW_ROOT.'postnl/response/label_closeout'.date('YmdHis').'.json',$response);
		file_put_contents(WWW_ROOT.'postnl/request/label_closeout'.date('YmdHis').'.json',print_r($pdata,1));
		if ($err) {
		  //echo "cURL Error #:" . $err;
			$data['error_code'] = $err;
			$data['error_msg']  = 'Barcode not generated';
			$this->sendErrorMail($data);
		} else {
		  	$res = json_decode($response,1);pr($res);
		  
			$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$manifest_username = $firstName.' '.$lastName;
			if($res['status'] == 'success'){
				$this->MergeUpdate->updateAll( 
					array(
						'MergeUpdate.manifest_date' => "'".date('Y-m-d H:i:s')."'",
						'MergeUpdate.manifest_username' => "'".$manifest_username."'"
					) , 
					array(
						'MergeUpdate.postnl_barcode' => $items 
					 ) 
				 );
			 }
 		}
	exit;  
	}	
	
    public function Barcode($Type = 'UD')
    {
			$this->layout = '';
			$this->autoRender = false;
			$auth = Configure::read( 'postnl' );
			$CustomerCode = $auth['customer_code'];
			$CustomerNumber = $auth['customer_number'];
			$api_key = $auth['api_key'];
			$server_url = $auth['server_url'];
 			$url = $server_url."/api/barcode/generate";
			
 			$pdata =	array (
				'barcode_type' => $Type,
				'amount_of_barcodes' => 1, 
			 );

			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,			  
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>json_encode($pdata),
  			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"api_key: {$api_key}",
				"cache-control: no-cache"
			  ),
			));
			
			 
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  //echo "cURL Error #:" . $err;
			  	$data['error_code'] = $err;
				$data['error_msg']  = 'Barcode not generated';
				$this->sendErrorMail($data);
			} else {
			  $r = json_decode($response,1);pr($r);
			 // return $r['Barcode'];
			}
		exit;  
	}
	
	public function TrackingBatches()
    { 
			$this->layout = '';
			$this->autoRender = false;
			$auth = Configure::read( 'postnl' );
			$CustomerCode = $auth['customer_code'];
			$CustomerNumber = $auth['customer_number'];
			$api_key = $auth['api_key'];
			$server_url = $auth['server_url'];
 			$url = $server_url."/api/tracking/batches";
			
 			$pdata =	array (
				'from' => date("Y-m-d").'T'.date("H:i:s",strtotime("-110 minutes")),
				'until' =>date("Y-m-d").'T'.date("H:i:s") 
			 );

			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,			  
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>json_encode($pdata),
  			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"api_key: {$api_key}",
				"cache-control: no-cache"
			  ),
			));
			
			 
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  //echo "cURL Error #:" . $err;
			  	$data['error_code'] = $err;
				$data['error_msg']  = 'Barcode not generated';
				$this->sendErrorMail($data);
			} else {
			  $r = json_decode($response,1);pr($r);
			 // return $r['Barcode'];
			}
		exit;  
	}
	
	public function TrackingMailbagbatches()
    { 
			$this->layout = '';
			$this->autoRender = false;
			$auth = Configure::read( 'postnl' );
			$CustomerCode = $auth['customer_code'];
			$CustomerNumber = $auth['customer_number'];
			$api_key = $auth['api_key'];
			$server_url = $auth['server_url'];
 			$url = $server_url."/api/tracking/mailbagbatches";
			
 			$pdata =	array (
				'from' => date("Y-m-d").'T'.date("H:i:s",strtotime("-110 minutes")),
				'until' =>date("Y-m-d").'T'.date("H:i:s") 
			 );

			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,			  
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>json_encode($pdata),
  			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"api_key: {$api_key}",
				"cache-control: no-cache"
			  ),
			));
			
			 
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  //echo "cURL Error #:" . $err;
			  	$data['error_code'] = $err;
				$data['error_msg']  = 'Barcode not generated';
				$this->sendErrorMail($data);
			} else {
			  $r = json_decode($response,1);pr($r);
			 // return $r['Barcode'];
			}
		exit;  
	}
	
	public function ReportEUexit()
	{ 
		$this->layout = '';
		$this->autoRender = false;
		$auth = Configure::read( 'postnl' );
		$CustomerCode = $auth['customer_code'];
		$CustomerNumber = $auth['customer_number'];
		$api_key = $auth['api_key'];
		$server_url = $auth['server_url'];
		$url = $server_url."/api/report/euexit";
		
		$pdata =	array (
			'from' => date("Y-m-d").'T'.date("H:i:s",strtotime("-110 minutes")),
			'until' =>date("Y-m-d").'T'.date("H:i:s") 
		 );
	
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,			  
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>json_encode($pdata),
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"api_key: {$api_key}",
			"cache-control: no-cache"
		  ),
		));
		
		 
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		
		if ($err) {
		  //echo "cURL Error #:" . $err;
			$data['error_code'] = $err;
			$data['error_msg']  = 'Barcode not generated';
			$this->sendErrorMail($data);
		} else {
		  $r = json_decode($response,1);pr($r);
		 // return $r['Barcode'];
		}
	exit;  
	}
	
	public function CancelLabel($barcode = '')
    {
			$this->layout = '';
			$this->autoRender = false;
			$auth = Configure::read( 'postnl' );
			$CustomerCode = $auth['customer_code'];
			$CustomerNumber = $auth['customer_number'];
			$api_key = $auth['api_key'];
			$server_url = $auth['server_url'];
 			$url = $server_url."/api/label/cancel";
			
 			$pdata =	array (
				'item' => $barcode,
 			 );

			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,			  
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>json_encode($pdata),
  			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"api_key: {$api_key}",
				"cache-control: no-cache"
			  ),
			));
			
			 
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  //echo "cURL Error #:" . $err;
			  	$data['error_code'] = $err;
				$data['error_msg']  = ' Label not Canceled';
				$this->sendErrorMail($data);
			} else {
			  $r = json_decode($response,1);pr($r);
			 // return $r['Barcode'];
			}
		exit;  
	}
	
  	private function getSplitOrderDetails($split_order_id = null){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
	    $data = array();
  		$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_order_id)));	
				
		if(count($orderItem) > 0)
		{
			$this->loadModel( 'Product' );
			$this->loadModel( 'ProductDesc' );
			$this->loadModel( 'Country' );	
		
 			$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
			$packet_length = $orderItem['MergeUpdate']['packet_length'];
			$packet_width  = $orderItem['MergeUpdate']['packet_width'];
			$packet_height = $orderItem['MergeUpdate']['packet_height']; 
			
			$_height = array(); $_weight = array(); $length = $width = 0; 
			
			$pos = strpos($orderItem['MergeUpdate']['sku'],",");
			if ($pos === false) {
				$val  = $orderItem['MergeUpdate']['sku'];
				$s    = explode("X", $val);
				$_qty = $s[0]; 
				$_sku = $s[1];		
  				$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.product_name','Product.category_name')));
				if(count($product) > 0){
					$_weight[] = $product['ProductDesc']['weight'] * $_qty;		
					$_height[] = $product['ProductDesc']['height'];		
					$length = $product['ProductDesc']['length'];
					$width = $product['ProductDesc']['width'];	
					$data['category_name'] = $product['Product']['category_name'];	
					$data['product_name'] = $product['Product']['product_name'];										
				}
  				
			}else{			
				$sks = explode(",",$orderItem['MergeUpdate']['sku']);
				$_weight = array();					 
				foreach($sks as $val){
					$s = explode("X", $val);
					$_qty = $s[0]; 
					$_sku = $s[1]; 
					$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.product_name','Product.category_name')));
					if(count($product) > 0)	{
						$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
						$_height[] = $product['ProductDesc']['height'];
						$length = $product['ProductDesc']['length'];	
						$width  = $product['ProductDesc']['width']; 
						$data['category_name'] = $product['Product']['category_name'];	
						$data['product_name'] = $product['Product']['product_name'];	
					}				
				}							 			
			}
			 
			if($packet_weight == 0){
				$packet_weight = array_sum($_weight);
			}
			if($packet_height == 0){
				$packet_height = array_sum($_height);
			}
			if($packet_length == 0){

				$packet_length = $length; 
			}
			if($packet_width == 0){
				$packet_width = $width;  
			}
				
			$data['packet_weight'] = $packet_weight; // KG to Gram
			$data['packet_height'] = $packet_height;
			$data['packet_length'] = $packet_length; 
			$data['packet_width'] = $packet_width;
   		}
		
		return $data;
		
 	}	
	
  	private function replaceChar($string = null){
  		//return iconv('UTF-8','ASCII//TRANSLIT',$string);	 
  		$unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E','Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Nº'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U','Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c','è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n','nº'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o','ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y','ü'=>'u','º'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	 
	}
		
	private function sendErrorMail($details = null){
		
		$web_store = Configure::read( 'web_store' );
		$subject   = 'PostNL API issue in '.$web_store;
		$mailBody  = '<p><strong>PostNL API have some issue please review and solve it.</strong></p>';
		
		if(isset($details['split_order_id'])){
			$subject   = $details['split_order_id'].' PostNL API orders issue in '.$web_store;
			$mailBody  = '<p><strong>'.$details['split_order_id'].' have below issue please review and solve it.</strong></p>';
		}	
		if(isset($details['error_code'])){
			$mailBody .= '<p>Error Code : '.$details['error_code'].'</p>';
			$mailBody .= '<p>Error Message : '.$details['error_msg'].'</p>';
		}
		 
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('');
		$email->emailFormat('html');
		$email->from('postnl@'.$web_store); 	
 		$email->to( array('avadhesh.kumar@jijgroup.com','pappu.k@euracogroup.co.uk'));	 
		$email->subject( $subject );
	  	$email->send( $mailBody );
	}
	
	private function lockOrder($details = null){
 		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderNote' ); 
		$md = $this->MergeUpdate->find('first', array( 'conditions' => array('product_order_id_identify' => $details['split_order_id']),'fields'=>'order_id' ) );
		if(count($md) > 0){
			$lock_note = $details['error_msg'];
			
			$firstName = 'PostNL API';
			$lastName = 'Cron';
			$mdata['postnl_barcode'] = '\'\'';
			$mdata['status']    = 3;
 			 
			$this->MergeUpdate->updateAll($mdata, array( 'product_order_id_identify' => $details['split_order_id'] ) );
			$this->OpenOrder->updateAll(array('OpenOrder.status' => 3), array('OpenOrder.num_order_id' => $md['MergeUpdate']['order_id']));
			
			$noteDate['order_id'] = $md['MergeUpdate']['order_id'];
			$noteDate['note'] = $lock_note;
			$noteDate['type'] = 'Lock';
			$noteDate['user'] = $firstName.' '.$lastName;
			$noteDate['date'] = date('Y-m-d H:i:s');
			$this->OrderNote->saveAll( $noteDate );  
		}
	}	
	
	public function unLockOrder($split_order_id = null)
	{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );			 
			$md = $this->MergeUpdate->find('first', array( 'conditions' => array('product_order_id_identify' => $split_order_id),'fields'=>'order_id' ) );
			if(count($md) > 0)
			{
 				$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 0) , array( 'MergeUpdate.product_order_id_identify' => $split_order_id ) );
				$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 0 ) , array( 'OpenOrder.num_order_id' => $md['MergeUpdate']['order_id'] ) );
			}
			 
	}
	public function testLabel()
	{				
		
		$this->loadModel( 'MergeUpdate' );
		$data['split_order_id'] = '3032615-1'; 
		$or = explode("-",$data['split_order_id']);  
		$barcode_type = 'RI';
		
		$main_height = 1550; $main_left = 500;
		if($barcode_type == 'RI'){
			$main_height = 1670;
			$main_left = 50;
		}
		
		//	$this->MergeUpdate->updateAll(array('service_provider' => '\'BRT\'', 'brt' => 1, 'brt_phone' => 1, 'brt_exprt' => '1'), array('product_order_id_identify' =>'2865407-1'));	
							 
		 $slip = $this->getSlip($data['split_order_id'],$or[0]);	
			
		// $file_name = 'label_'.$filename.'.pdf';
		
		// $response = file_get_contents(WWW_ROOT.'logs/response_'.$data['split_order_id'].'.json');
		 // $res = json_decode($response,1);
		// pr($res);
		 // $content = base64_decode($res['ResponseShipments']['0']['Labels']['0']['Content']);
			 
			 
			 
			 
		 $imgPath = WWW_ROOT .'logs/'.$data['split_order_id'].'.jpg'; 	
		  //file_put_contents(WWW_ROOT.'logs/response_'.$data['split_order_id'].'.json',$response);
		 // file_put_contents($imgPath,$content);
		
		
		/*--------------------------Re-Size Order Barcode----------------------------*/
		$cover_img = WWW_ROOT .'img/orders/barcode/'.$data['split_order_id'].'.png';
		$new_ord_barcode = WWW_ROOT .'logs/barcode_'.$data['split_order_id'].'.png';
		$width = 224; 
		$height = 59;
		$newwidth  = 400; 
		$newheight = 150;
		$src = imagecreatefrompng($cover_img);
		$dst = imagecreatetruecolor($newwidth, $newheight);
		imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagepng($dst,$new_ord_barcode);
		imagedestroy($dst);
		imagedestroy($src);
		/*--------------------------END Re-Size Order Barcode----------------------------*/
		/*--------------------------Append Order Barcode----------------------------*/
		$dest = imagecreatefromjpeg($imgPath);
		$src = imagecreatefrompng($new_ord_barcode);
		
		imagecopymerge($dest, $src, $main_left, $main_height, 0, 0, $newwidth , $newheight, 100); //have to play with these numbers for it to work for you, etc. 
		imagejpeg($dest,WWW_ROOT .'logs/label_'.$data['split_order_id'].'.jpg');
				
		imagedestroy($dest);
		imagedestroy($src);
		/*-------------------------End of Append Order Barcode----------------------------*/
		
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper('A4', 'portrait'); 
		
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
					 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
					 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
					 <meta content="" name="description"/>
					 <meta content="" name="author"/>';
			$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
				<tr><td style="height:600px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
			<tr><td valign="top" style="padding:0px 0px 5px 12px;">						
			<div style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
					transform-origin: 50% 50%;height:270px; width:450px;">
					<img src="'.Router::url('/', true).'logs/'.$data['split_order_id'].'.jpg" width="350px" style="margin-left: -110px; margin-right: -110px;margin-top: -70px;"></div></td><td><img src="'.Router::url('/', true).'img/orders/barcode/'.$data['split_order_id'].'.png" style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
					transform-origin: 50% 50%;height:60px; margin-left: -28px;" width="224px"> </td> 
					
					 
					</tr>
					 
					
			</table>';
			
		$cssPath = WWW_ROOT .'css/';
		$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
		echo $html;
		$dompdf->load_html($html, Configure::read('App.encoding'));
		$dompdf->render();
		$imgPath = WWW_ROOT .'logs/'; 
		$name	=	'Label_Slip_'.$data['split_order_id'].'.pdf';
		unlink($imgPath.$name); 
		file_put_contents($imgPath.$name, $dompdf->output());
		
		
		exit;
		
	}
	
	public function testSingleLabel()
	{				
		
		$this->loadModel( 'MergeUpdate' );
		$data['split_order_id'] = '3032615-1'; 
		$or = explode("-",$data['split_order_id']);  
		$barcode_type = 'RI';
		
		$main_height = 1550; $main_left = 500;
		if($barcode_type == 'RI'){
			$main_height = 1670;
			$main_left = 50;
		}
  		 $imgPath = WWW_ROOT .'logs/'.$data['split_order_id'].'.jpg'; 	
		  //file_put_contents(WWW_ROOT.'logs/response_'.$data['split_order_id'].'.json',$response);
		 // file_put_contents($imgPath,$content);
		
 		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		//$dompdf->set_paper('A4', 'portrait'); 
		$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');
	 	$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			<meta content="" name="description"/>
			<meta content="" name="author"/>';
			$html .= '<table style="width:330px; border:1px solid #CCC;"> 
			<tr><td valign="top"><div>
					<img src="'.Router::url('/', true).'logs/'.$data['split_order_id'].'.jpg" width="99%"></div><div><center><img src="'.Router::url('/', true).'img/orders/barcode/'.$data['split_order_id'].'.png" style="height:50px;"></center></div></td></tr> 								
			</table>';
			
		$cssPath = WWW_ROOT .'css/';
		$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
		echo $html;
		$dompdf->load_html($html, Configure::read('App.encoding'));
		$dompdf->render();
		$imgPath = WWW_ROOT .'logs/'; 
		$name	=	'Label__'.$data['split_order_id'].'.pdf';
		unlink($imgPath.$name); 
		file_put_contents($imgPath.$name, $dompdf->output()); 
		exit;
		
	}
	
 	 
	public function getAddress()
	{
	
		$this->loadModel('MergeUpdate'); 
		$this->loadModel('OpenOrder');
			
		$orders	=	$this->MergeUpdate->find('all', array('conditions'=> array('order_id'=>['3021618','2821505','3023714']),'fields'=>array('sku','product_order_id_identify','order_id','postnl_barcode','country_code','provider_ref_code','source_coming','delevery_country') ) );
 		
		//echo count($orders);
 		//echo '<br>';
 		unlink(WWW_ROOT.'logs/adr10.csv');
 
		foreach($orders	 as $v){
		
			$provider_ref_code =  $v['MergeUpdate']['provider_ref_code'];
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $v['MergeUpdate']['order_id'] ) ) );
			$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
			// pr($customer_info);
			 $sep = "\t";
			 $content = $customer_info->Address->Address1.$sep.
			 $customer_info->Address->Address2.$sep.
			 $customer_info->Address->Address3.$sep.
			 $customer_info->Address->Town.$sep.
			 $customer_info->Address->Region.$sep.
			 $customer_info->Address->Country;
			 
			 $addressArray['sub_source'] =  $v['MergeUpdate']['source_coming'];
			 $addressArray['country'] = $v['MergeUpdate']['delevery_country'];
			 $addressArray['address1'] = $customer_info->Address->Address1;
			 $addressArray['address2'] = $customer_info->Address->Address2;
			 $addressArray['address3'] = $customer_info->Address->Address3;
			 $addressArray['company'] = $customer_info->Address->Company;
			  
			 
			//echo $v['MergeUpdate']['order_id'] .' ==   ';
			 $adr = $this->getCountryAddress( $addressArray );
			//echo "<br>";
			//echo $content ;
			$content .= $sep.$adr;
			//echo "<br>";
			
		//$content = $v['CustomerOrder']['order_id'].$sep. $v['CustomerOrder']['sku'].$sep. $v['CustomerOrder']['barcode'].$sep. $v['CustomerOrder']['quantity'].$sep. $v['CustomerOrder']['unit_price'].$sep.$v['CustomerOrder']['status'].$sep.$v['CustomerOrder']['created_date'].$sep.$created_by .$sep.$customer_name;
 			
 			file_put_contents(WWW_ROOT.'logs/adr.csv',$content."\r\n", FILE_APPEND | LOCK_EX);	
			 

			
		}
		
		
		//$addressArray
	}
	
	public function getCountryAddress( $addressArray )
	{
			
			switch ($addressArray['country']) 
			{
			case "United States":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? ucwords(strtolower($addressArray['address1']))."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords(strtolower($addressArray['address2']))."\n" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords(strtolower($addressArray['address3']))."\n" : '' ; 
					return $address;
					break;
			case "Canada":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1'])."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']) : '' ;
					return $address;
					break;
			case "Germany":
			
					if(strtolower($addressArray['sub_source']) == 'costdropper'){
  					
						$address			 =		'';
						if($addressArray['address1'] != '' && $addressArray['address2'] != ''){
							$address			.=		ucwords($addressArray['address2']).' ' . ucwords($addressArray['address1']) ; 
						}
 						if($addressArray['address1'] == '' && $addressArray['address2'] != ''){						
							$address			.=		ucwords($addressArray['address2']) ; 					
						}
						if($addressArray['address1'] != '' && $addressArray['address2'] == ''){						
							$address			.=		ucwords($addressArray['address1']) ; 					
						}
						
					}else{
							$address			 =		'';
							$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1'])."\n" : '' ; 
							
							if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
								$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
								$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;						
							}
							
							if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
								$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ;
							}
							
							if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
								$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;
							}
 						}
					return $address;
					break;
				case "Spain":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? ucwords($addressArray['company'])."\n" : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1'])."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords($addressArray['address3'])."\n" : '' ;
					return $address;
					break;
			
			case "Italy":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? strtoupper($addressArray['address1'])."\n" : '' ; 
					
					 if($addressArray['address2'] != '' && $addressArray['address3'] != ''){
						$address			.=		($addressArray['address2'] != '' )   ? strtoupper($addressArray['address2'])."\n" : '';
						$address			.=		($addressArray['address3'] != '' )   ? strtoupper($addressArray['address3'])."\n" : '';
					}
					else if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
 						$address			.=		($addressArray['address2'] != '' )   ? strtoupper($addressArray['address2'])."\n" : '';
 					}
					else if($addressArray['address2'] == '' && $addressArray['address3'] != ''){
 						$address			.=		($addressArray['address3'] != '' )   ? strtoupper($addressArray['address3'])."\n" : '';
 					}
					return $address;
					break;
			case "France":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? strtoupper($addressArray['address1'])."\n" : '' ; 
					
					if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;						
					}
					
					if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ;
					}
					
					if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;
					}
					
					return $address;
					break;
			case "Denmark":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? $addressArray['company']."\n" : '' ;  
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1']."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3']."\n" : '' ; 
					return $address;
					break;
			case "Austria":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? $addressArray['company']."\n" : '' ; 
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1']."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3']."\n" : '' ; 
					return $address;
					break;
			default:
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1']."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3']."\n" : '' ;
					return $address;
					break;
			}
			exit;
	}
	
	public function getSenderInfo($data = [], $store_country = '')
	{ 
 		$country = $data['country'] ;
		if($store_country != ''){
			$country = $store_country;
		}
 		$sub_source = $data['sub_source'];
		$FromCountryCode = 'JE';
  		if(strpos($sub_source,'marec')!== false){
			$FromCompany = 'ESL LIMITED';
			$FromPersonName = 'ESL LIMITED';
			/*-------Updated on 12-02-2020-----*/
			$Building = 'Unit 4 Airport Cargo Centre';
			$FromAddress = 'L\'avenue De La Commune';
			$FromCity = 'St Peter';
			$FromDivision = 'JE';
			$FromPostCode = 'JE3 7BY'; 
			$FromCountryName = 'JERSEY';
			$FromPhoneNumber = '+443301170238';
			$email = 'accounts@esljersey.com'; 
			
			$VAT_Number = 'GB 307817693';  
 			if(in_array($country ,['United Kingdom','GB','UK'])){  
				$VAT_Number   = 'GB 307817693'; 
    		}else if(in_array($country ,['Germany','DE'])){  
 				$VAT_Number   = 'DE 323086773'; 
			}else if(in_array($country ,[ 'France','FR'])){  
				$VAT_Number   = 'FR 07879900934';  
			}else if(in_array($country ,['Italy','IT'])){  
				$VAT_Number   = 'IT 11062310963'; 
			} 
		 }	
		else if(strpos($sub_source,'rainbow')!== false){
			$FromCompany = 'FRESHER BUSINESS LIMITED';
			$FromPersonName = 'C/O ProFulfillment and Logistics';
			/*-------Updated on 12-02-2020-----*/
			$Building = 'Unit 4 Airport Cargo Centre';
			$FromAddress = 'L\'avenue De La Commune';
			$FromCity = 'St Peter';
			$FromDivision = 'JE';
			$FromPostCode = 'JE3 7BY';			
			$FromCountryName = 'JERSEY';
			$FromPhoneNumber = '0123456789';
			$email = 'accounts@fresherbusiness.co.uk'; 
			
			$VAT_Number = 'GB 318649182';  
 			if(in_array($country ,['United Kingdom','GB','UK'])){    
				$VAT_Number   = 'GB 318649182'; 
    		}else if(in_array($country ,['Germany','DE'])){ 
 				$VAT_Number   = 'DE 327173988'; 
			}else if(in_array($country ,['France','FR'])){ 
				$VAT_Number   = 'FR 48880490255';  
			}else if(in_array($country ,['Italy','IT'])){ 
				$VAT_Number   = 'IT 11061000961'; 
			} 
		}else{
			$FromCompany = 'EURACO GROUP LTD';
			/*-------Updated on 12-02-2020-----*/
			$FromPersonName = 'C/O ProFulfillment and Logistics';
			$Building = 'Unit 4 Airport Cargo Centre';
			$FromAddress = 'L\'avenue De La Commune';
			$FromCity = 'St Peter';
			$FromDivision = 'JE';
			$FromPostCode = 'JE3 7BY'; 
			$FromCountryName = 'JERSEY';
			$FromPhoneNumber = '+44 3301170104';
			$email = 'accounts@euracogroup.co.uk';
			 
 			$VAT_Number = 'GB 304984295';  
 			if(in_array($country ,['United Kingdom','GB','UK'])){  
				$VAT_Number   = 'GB 304984295'; 
    		}else if(in_array($country ,['Germany','DE'])){
 				$VAT_Number   = 'DE 321777974'; 
			}else if(in_array($country ,['France','FR'])){
				$VAT_Number   = 'FR 59850070509'; 
			}else if(in_array($country ,['Spain','ES'])){
				$VAT_Number   = 'ES N6061518D'; 
			}else if(in_array($country ,['Italy','IT'])){
				$VAT_Number   = 'IT 10905930961'; 
			}	
		}
		
		$senderData['email']	  	   =    $email;
		$senderData['FromCompany']	   =    $FromCompany;
		$senderData['FromPersonName']  =	$FromPersonName;
		$senderData['Building']	   	   =	$Building ;
		$senderData['FromAddress']	   =	$FromAddress ; 
		$senderData['FromCity'] 	   =	$FromCity;
		$senderData['FromDivision']    =	$FromDivision;
		$senderData['FromPostCode']    =	$FromPostCode;
		$senderData['FromCountryCode'] =	$FromCountryCode;
		$senderData['FromCountryName'] =	$FromCountryName;
		$senderData['FromPhoneNumber'] =	$FromPhoneNumber;
		$senderData['vat_number'] 	   =	$VAT_Number; 
 
 		return $senderData;	
	}
		
 	public function getSenderInfoNL($data  = [], $store_country = '')
	{ 
 		$country = $data['country'] ;
		if($store_country != ''){
			$country = $store_country;
		}
 		$sub_source = $data['sub_source'];
		
  		if(strpos($sub_source,'marec')!== false){
  		  	$senderData['FromPhoneNumber'] = '+443301170238';  
			$senderData['email'] = 'accounts@esljersey.com';  
		  	$senderData['FromCompany']	   =  'ESL LIMITED';
			$senderData['Building']	   	   =  '7115' ;
			$senderData['FromAddress']	   =  'Postbus 7115' ; 
			$senderData['FromPostCode']    =	'3109 AC';
			
 			$VAT_Number = 'GB 307817693';  
			if(in_array($country ,['United Kingdom','GB','UK'])){  
				$VAT_Number   = 'GB 307817693'; 
			}else if(in_array($country ,['Germany','DE'])){  
				$VAT_Number   = 'DE 323086773'; 
			}else if(in_array($country ,[ 'France','FR'])){  
				$VAT_Number   = 'FR 07879900934';  
			}else if(in_array($country ,['Italy','IT'])){  
				$VAT_Number   = 'IT 11062310963'; 
			} 
			$senderData['vat_number'] 	   =	$VAT_Number;
		 }	
		else if(strpos($sub_source,'rainbow')!== false){
 			$senderData['FromPhoneNumber'] = '0123456789';
			$senderData['email'] = 'accounts@fresherbusiness.co.uk'; 			
		  	$senderData['FromCompany']	   =  'FRESHER BUSINESS LIMITED';
			$senderData['Building']	   	   =  '7098' ;
			$senderData['FromAddress']	   =  'Postbus 7098' ; 
			$senderData['FromPostCode']    =	'3109 AB';
			 
 			$VAT_Number = 'GB 318649182';  
			if(in_array($country ,['United Kingdom','GB','UK'])){    
				$VAT_Number   = 'GB 318649182'; 
			}else if(in_array($country ,['Germany','DE'])){ 
				$VAT_Number   = 'DE 327173988'; 
			}else if(in_array($country ,['France','FR'])){ 
				$VAT_Number   = 'FR 48880490255';  
			}else if(in_array($country ,['Italy','IT'])){ 
				$VAT_Number   = 'IT 11061000961'; 
			} 
			$senderData['vat_number'] 	   =	$VAT_Number;
		}else{
 		  	$senderData['FromCompany']	   =  'EURACO GROUP LTD';			
			$senderData['Building']	   	   =  '7124' ;
			$senderData['FromAddress']	   =  'Postbus 7124' ; 
			$senderData['FromPhoneNumber'] =  '+443301170104';
			$senderData['email'] 		   =  'accounts@euracogroup.co.uk';
			$senderData['FromPostCode']    =	'3109 AC';
			
			$VAT_Number = 'GB 304984295';  
			if(in_array($country ,['United Kingdom','GB','UK'])){  
				$VAT_Number   = 'GB 304984295'; 
			}else if(in_array($country ,['Germany','DE'])){
				$VAT_Number   = 'DE 321777974'; 
			}else if(in_array($country ,['France','FR'])){
				$VAT_Number   = 'FR 59850070509'; 
			}else if(in_array($country ,['Spain','ES'])){
				$VAT_Number   = 'ES N6061518D'; 
			}else if(in_array($country ,['Italy','IT'])){
				$VAT_Number   = 'IT 10905930961'; 
			}	
			$senderData['vat_number'] 	   =	$VAT_Number;
		}
		/*$senderData['FromPersonName']  =  'C/O Postbus';
 		$senderData['FromCity'] 	   =	'Schiedam'; 		
		$senderData['FromCountryCode'] =	'NL';*/ 
		
		/*$senderData['FromPersonName']  =  'C/O Postbus';
 		$senderData['FromCity'] 	   =	'Minsk'; 		
		$senderData['FromCountry'] 	   =	'Belarus';
		$senderData['FromCountryCode'] =	'BY';*/
		
		$senderData['FromPersonName']  =  'C/O Postbus';
 		$senderData['FromCity'] 	   =	'Jersey'; 		
		$senderData['FromCountry'] 	   =	'Jersey';
		$senderData['FromCountryCode'] =	'JE';
  		 
  		return $senderData;	
	}
 
	
 	public function getBarcodeOutside($spilt_order_id  = null)
	{ 
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'AssignService' );
		$this->loadModel( 'MergeUpdate' );
		
		//$uploadUrl = $this->getUrlBase();
		$imgPath = WWW_ROOT .'img/orders/barcode/';   
		
		
		// $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.status' => '0')));
		
		if($spilt_order_id != ''){
		 $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => $spilt_order_id)));
		}else{
		 $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.status' => '0')));
		}
		
		
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
		//$font = new BCGFontFile(APP .'Vendor/barcodegen/font/Arial.ttf', 13);
		$colorFront = new BCGColor(0, 0, 0);
		$colorBack = new BCGColor(255, 255, 255);
		
		if( count($allSplitOrders) > 0 )	
		{
		  foreach( $allSplitOrders as $allSplitOrder )
		  {					  
			  $id 			= 		$allSplitOrder['MergeUpdate']['id'];
			  $openorderid	=	 	$allSplitOrder['MergeUpdate']['product_order_id_identify'];
			  $barcodeimage	=	$openorderid.'.png';
			  
				$orderbarcode=$openorderid;
				$code128 = new BCGcode128();
				$code128->setScale(2);
				$code128->setThickness(20);
				$code128->setForegroundColor($colorFront);
				$code128->setBackgroundColor($colorBack);
				$code128->setLabel(false);
				$code128->parse($orderbarcode);
									
				//Drawing Part
				$imgOrder128=$orderbarcode.".png";
				$imgOrder128path=$imgPath.'/'.$orderbarcode.".png";
				$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
				$drawing128->setBarcode($code128);
				$drawing128->draw();
				$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);
			  
			  if( $allSplitOrder['MergeUpdate']['product_order_id_identify'] != "" )
			  {
				  //$content = file_get_contents($uploadUrl.$openorderid);
				  //file_put_contents($imgPath.$barcodeimage, $content);
				  
				  $data['MergeUpdate']['id'] 	=  $id;
				  $data['MergeUpdate']['order_barcode_image'] 	=  $barcodeimage;
				  $this->MergeUpdate->save($data);
			  }
		  }
		}
		
		
	}
			 
	 public function updateManifestDate($merge_id)
	   {
		  $this->loadModel('MergeUpdate'); 
		  date_default_timezone_set('Europe/Jersey');
		  $firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		  $lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		  $manifest_username = $firstName.' '.$lastName;
		  $data['id'] = $merge_id;   
		  $data['manifest_date'] = date('Y-m-d H:i:s');   
		  $data['manifest_username'] 	= $manifest_username;   
		  $this->MergeUpdate->saveAll( $data);	    	
	   }
	   
     public function addressInfo($order_id)
	   {
			$this->loadModel('OpenOrder'); 
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id ) ) );
			$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
			$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
			pr($customer_info);
			
			
			preg_match_all('/([\d]+)/', $customer_info->Address->Address1, $match);

    		//pr($match);
			echo $match[0][0];
  			echo $int = (int) filter_var($customer_info->Address->Address1, FILTER_SANITIZE_NUMBER_INT);
			echo ' = '.str_replace($int,' ', $customer_info->Address->Address1);
			
			echo ' <br> ';
			echo $int = (int) filter_var($customer_info->Address->PhoneNumber, FILTER_SANITIZE_NUMBER_INT);
			preg_match_all('/([\d]+)/', $customer_info->Address->PhoneNumber, $telmatch);
 			echo '<br>';
    		// pr($match);
			 $tel ='';
			 for($i = 0; $i< count($telmatch[0]); $i++){
				$tel .= $telmatch[0][$i];
 			 }
  			 $country_pcode = '+';
			 if($customer_info->Address->Country == 'France'){
				 $country_pcode = '+33';
			 }else if($customer_info->Address->Country == 'Germany'){
				 $country_pcode = '+49';
			 }else if($customer_info->Address->Country == 'Germany'){
				 $country_pcode = '+49';
			 }
			 echo $country_pcode.substr($tel,-10);
			exit;
	}
	
	public function pdfToImg($split_ord_id = '3849671-1'){
  		
		$labelPdfPath = WWW_ROOT .'postnl/labels/'.$split_ord_id.'.pdf';  
		$imagick = new Imagick();
		$imagick->setResolution(250, 250);
		// Reads image from PDF
		$imagick->readImage($labelPdfPath);
		// Merges a sequence of images
		$imagick = $imagick->flattenImages(); 
		// Writes an image
		$imagick->writeImages(WWW_ROOT .'postnl/labels/'.$split_ord_id.'.jpg',false);
		//----------------Rotate image----------------------
		$filename = WWW_ROOT.'postnl/labels/'.$split_ord_id.'.jpg';
		$degrees = 90;  		
		$source = imagecreatefromjpeg($filename);		
		$rotate = imagerotate($source, $degrees, 0);		
		$r_label = WWW_ROOT .'postnl/labels/rotate_'.$split_ord_id.'.jpg';
 		imagejpeg($rotate,$r_label);
		// Free the memory
		imagedestroy($source);
		imagedestroy($rotate); 
		@unlink(WWW_ROOT .'postnl/labels/'.$split_ord_id.'.jpg');
		
		return 2;
 	}
}
?>
