<?php
//error_reporting(0);
class FbaListShipmentsController extends AppController
{
    
    var $name = "FbaListShipments";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
		parent::beforeFilter();
		$this->layout = false;
		$this->Auth->Allow(array('ListInboundShipmentItems'));		
		 
    }
 	
   public function index() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Shipments" );	
		$this->loadModel('FbaApiShipment');
		 
		$this->paginate = array('order'=>'FbaApiShipment.id DESC','limit' => 20 );
		$all_shipments = $this->paginate('FbaApiShipment');
		$this->set( 'all_shipments', $all_shipments );				
		
	} 
	public function ShipmentItems($shipment_id = null) 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Shipments" );	
		$this->loadModel('FbaApiShipment');
		 
		if(isset($shipment_id) && $shipment_id > 0 ){
			$shipment_items = $this->FbaApiShipment->find('first', array('conditions'=>array('FbaApiShipment.id'=>$shipment_id),'order'=>'FbaApiShipment.id DESC'));
			$this->set( 'shipment_items', $shipment_items );	
		}
		
		
	} 
	
	public function ListInboundShipments() 
	{	
		$this->loadModel('FbaApiShipment');	
		$fba_shipments = $this->FbaApiShipment->find('all', array('conditions'=>array('FbaApiShipment.shipment_status !=' => 'CLOSED')) );
			
		if(count($fba_shipments) > 0){
			
			App::import( 'vendor' , 'FBAInboundServiceMWS/Client' );
			App::import( 'vendor' , 'FBAInboundServiceMWS/Exception' );
			App::import( 'vendor' , 'FBAInboundServiceMWS/Mock' );
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model' );
			
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model/ListInboundShipmentsRequest' ); 
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model/ListInboundShipmentsResult' );
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model/ListInboundShipmentsResponse' );  
			
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model/Address' ); 
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model/ShipmentStatusList' );  
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model/ShipmentIdList' );
			 
			$serviceUrl = "https://mws.amazonservices.com/FulfillmentInboundShipment/2010-10-01";
			
			$config = array (
			'ServiceURL' => $serviceUrl,
			'ProxyHost' => null,
			'ProxyPort' => -1,
			'ProxyUsername' => null,
			'ProxyPassword' => null,
			'MaxErrorRetry' => 3,
			);
			
			$service = new FBAInboundServiceMWS_Client(
				AWS_ACCESS_KEY_ID,
				AWS_SECRET_ACCESS_KEY,
				APPLICATION_NAME,
				APPLICATION_VERSION,
				$config);
				
			$request = new FBAInboundServiceMWS_Model_ListInboundShipmentsRequest(); 
			$request->setSellerId(MERCHANT_ID);
			$request->setMarketplace(MARKETPLACE_ID);	
			
			$objStatusList = new FBAInboundServiceMWS_Model_ShipmentStatusList();	
			$shipmentStatus[] = 'WORKING';
			$shipmentStatus[] = 'SHIPPED'; 
			$shipmentStatus[] = 'IN_TRANSIT'; 
			$shipmentStatus[] = 'DELIVERED'; 
			$shipmentStatus[] = 'CHECKED_IN'; 
			$shipmentStatus[] = 'RECEIVING'; 
			$shipmentStatus[] = 'CANCELLED';
			$shipmentStatus[] = 'CLOSED';
			$shipmentStatus[] = 'DELETED'; 
			$shipmentStatus[] = 'ERROR';
			  
			$objStatusList->setmember($shipmentStatus) ;		
			$request->setShipmentStatusList($objStatusList);	
			
			$objIdList = new FBAInboundServiceMWS_Model_ShipmentIdList() ;	
			 
			foreach($fba_shipments  as $shipments){
				$shipmentId[] = $shipments['FbaApiShipment']['shipment_id'];
			}
			
		
			
			/*$shipmentId[] = 'FBA15C00Y7Q6';
			$shipmentId[] = 'FBA15BXD6P6Q';
			$shipmentId[] = 'FBA15C2J0Y5D' ; 
			$shipmentId[] = 'FBA15BXBQMYK' ;
			$shipmentId[] = 'FBA15BX991CK' ;
			$shipmentId[] = 'FBA15BT36HNJ' ;*/
			
			$objIdList->setmember($shipmentId) ;
			$request->setShipmentIdList($objIdList);	
		 
			$xml = $this->invokeListInboundShipments($service, $request);
			
			if(isset($xml->Error)){  
					file_put_contents(WWW_ROOT.'logs/fba_list_shipment_error_'.date('Ymd').'.log', 
					date('Y-m-d H:i:s')."\tErrorCode:".$xml->Error->Code."\tErrorMessage:".$xml->Error->Message."\r\n", 
					FILE_APPEND | LOCK_EX);		 	
			}else{    
				if(isset($xml->ListInboundShipmentsResult->ShipmentData)){
					$this->loadModel('FbaApiShipment');	
					foreach($xml->ListInboundShipmentsResult->ShipmentData->member as $items)
					{			
						$savedata = array();
						$savedata['shipment_id'] 			= $items->ShipmentId;
						$savedata['shipment_name'] 			= $items->ShipmentName;
						$savedata['name']					= $items->ShipFromAddress->Name;
						$savedata['address_line_1'] 		= $items->ShipFromAddress->AddressLine1;	
						$savedata['address_line_2'] 		= $items->ShipFromAddress->AddressLine2;	
						$savedata['city']		 			= $items->ShipFromAddress->City;	
						$savedata['state_or_province_code'] = $items->ShipFromAddress->StateOrProvinceCode;	
						$savedata['country_code']   		= $items->ShipFromAddress->CountryCode;
						$savedata['postal_code'] 			= $items->ShipFromAddress->PostalCode;
						$savedata['destination_fulfillment_center_id'] 	= $items->DestinationFulfillmentCenterId;
						$savedata['shipment_status'] 		= $items->ShipmentStatus;
						$savedata['label_prep_type'] 		= $items->LabelPrepType;
						$savedata['are_cases_required'] 	= $items->AreCasesRequired;
						$savedata['box_contents_source'] 	= $items->BoxContentsSource;
						
						if(isset($items->EstimatedBoxContentsFee)){
							$savedata['total_units'] 				= $items->EstimatedBoxContentsFee->TotalUnits;
							$savedata['fee_per_unit_currency_code'] = $items->EstimatedBoxContentsFee->FeePerUnit->CurrencyCode;
							$savedata['fee_per_unit_value'] 		= $items->EstimatedBoxContentsFee->FeePerUnit->Value;
							$savedata['total_fee_currency_code'] 	= $items->EstimatedBoxContentsFee->TotalFee->CurrencyCode;
							$savedata['total_fee_value'] 			= $items->EstimatedBoxContentsFee->TotalFee->Value;
						} 
						
						$fba_shipments = $this->FbaApiShipment->find('first', array('conditions'=>array('FbaApiShipment.shipment_id' => $items->ShipmentId)) );
						
						if(count($fba_shipments) > 0){
							$savedata['id'] = $fba_shipments['FbaApiShipment']['id'];						
						}else{					
							$savedata['added_date'] = date('Y-m-d H:i:s');														
						}				
						$this->FbaApiShipment->saveAll( $savedata );	 
					}						
				}
		}
 	 }else{
	 
	 file_put_contents(WWW_ROOT.'logs/fba_shipment_error_'.date('Ymd').'.log',date('Y-m-d H:i:s')."\tErrorCode:No shipment found.\r\n",FILE_APPEND | LOCK_EX);	
	 
	 }
	 	pr($xml);
		 
		exit;
	}
	 
	public function saveShipmentItems( $shipmentItemsObject, $shipment_inc_id = 0) 
	{	
		$this->loadModel('FbaApiShipmentItem');	
		
		foreach($shipmentItemsObject as $items)
		{			
		
			$savedata = array();
			$savedata['shipment_inc_id']		= $shipment_inc_id;
			$savedata['shipment_id'] 			= $items->ShipmentId;
			$savedata['seller_sku'] 			= $items->SellerSKU;
			$savedata['fulfillment_network_sku']= $items->FulfillmentNetworkSKU ; 					
			$savedata['quantity_shipped'] 		= $items->QuantityShipped;
			$savedata['quantity_received'] 		= $items->QuantityReceived;
			$savedata['quantity_in_case'] 		= $items->QuantityInCase; 
			
			if(isset($items->PrepDetailsList)){
				$savedata['prep_details_list'] = json_encode($items->PrepDetailsList);
				$savedata['prep_instruction']  = $items->PrepDetailsList->PrepDetails->PrepInstruction;
				$savedata['prep_owner'] 	   = $items->PrepDetailsList->PrepDetails->PrepOwner; 
			} 
			
			$fba_shipment_items = $this->FbaApiShipmentItem->find('first', array('conditions'=>array('FbaApiShipmentItem.shipment_id' => $items->ShipmentId,'FbaApiShipmentItem.fulfillment_network_sku' => $items->FulfillmentNetworkSKU)) );
			
			if(count($fba_shipment_items) > 0){
				$savedata['id'] = $fba_shipment_items['FbaApiShipmentItem']['id'];						
			}else{					
				$savedata['added_date'] = date('Y-m-d H:i:s');														
			}				
			$this->FbaApiShipmentItem->saveAll( $savedata );	 
		}	
	} 
	
	 
	public function ListInboundShipmentItems() 
	{	
		@unlink(WWW_ROOT.'logs/list_inbound_shipment_items_next_token.txt');
		
		$this->loadModel('FbaApiShipment');
		
		$fba_shipments = $this->FbaApiShipment->find('first', array('conditions'=>array('FbaApiShipment.shipment_status !=' => 'CLOSED'),
		'fields' => array('id','shipment_id'),'order' => 'item_fetch_date ASC'));
		
		if(count($fba_shipments) > 0)
		{
			$shipment_id = $fba_shipments['FbaApiShipment']['shipment_id'];
			$shipment_inc_id = $fba_shipments['FbaApiShipment']['id'];
			App::import( 'vendor' , 'FBAInboundServiceMWS/Client' );
			App::import( 'vendor' , 'FBAInboundServiceMWS/Exception' );
			App::import( 'vendor' , 'FBAInboundServiceMWS/Mock' );
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model' );
			
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model/ListInboundShipmentItemsRequest' ); 
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model/ListInboundShipmentItemsResult' );
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model/ListInboundShipmentItemsResponse' );  
			
			$serviceUrl = "https://mws.amazonservices.com/FulfillmentInboundShipment/2010-10-01";
			
			$config = array (
			'ServiceURL' => $serviceUrl,
			'ProxyHost' => null,
			'ProxyPort' => -1,
			'ProxyUsername' => null,
			'ProxyPassword' => null,
			'MaxErrorRetry' => 3,
			);
			
			$service = new FBAInboundServiceMWS_Client(
				AWS_ACCESS_KEY_ID,
				AWS_SECRET_ACCESS_KEY,
				APPLICATION_NAME,
				APPLICATION_VERSION,
				$config);
				
			$request = new FBAInboundServiceMWS_Model_ListInboundShipmentItemsRequest(); 
			$request->setSellerId(MERCHANT_ID);
			$request->setShipmentId($shipment_id);	
			$request->setLastUpdatedAfter( gmdate('Y-m-d\TH:i:s\Z', time()-100000));	
		 
			$xml = $this->invokeListInboundShipmentItems($service, $request);
			
			if(isset($xml->Error)){  
				file_put_contents(WWW_ROOT.'logs/fba_list_shipment_error_'.date('Ymd').'.log', 
				date('Y-m-d H:i:s')."\tErrorCode:".$xml->Error->Code."\tErrorMessage:".$xml->Error->Message."\r\n", 
				FILE_APPEND | LOCK_EX);		 	
			}else{    
				if(isset($xml->ListInboundShipmentItemsResult->ItemData)){
				
					if(isset($xml->ListInboundShipmentItemsResult->NextToken)){	
						
						file_put_contents(WWW_ROOT.'logs/list_inbound_shipment_items_next_token.txt',$shipment_inc_id."\t".$xml->ListInboundShipmentItemsResult->NextToken);	
					}					
					$this->saveShipmentItems($xml->ListInboundShipmentItemsResult->ItemData->member,$shipment_inc_id);
										
				}
			}		
			$shipData['item_fetch_date'] = date('Y-m-d H:i:s');		
			$shipData['id'] = $fba_shipments['FbaApiShipment']['id'];
			$this->FbaApiShipment->saveAll( $shipData );	
		}
		pr($xml);
		exit;
	}
  
	public function ListInboundShipmentItemsByNextToken() 
	{	
		if(file_exists(WWW_ROOT.'logs/list_inbound_shipment_items_next_token.txt'))
		{	
			App::import( 'vendor' , 'FBAInboundServiceMWS/Client' );
			App::import( 'vendor' , 'FBAInboundServiceMWS/Exception' );
			App::import( 'vendor' , 'FBAInboundServiceMWS/Mock' );
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model' );
			
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model/ListInboundShipmentItemsByNextTokenRequest' ); 
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model/ListInboundShipmentItemsByNextTokenResult' );
			App::import( 'vendor' , 'FBAInboundServiceMWS/Model/ListInboundShipmentItemsByNextTokenResponse' );  
			
			$serviceUrl = "https://mws.amazonservices.com/FulfillmentInboundShipment/2010-10-01";
			
			$config = array (
			'ServiceURL' => $serviceUrl,
			'ProxyHost' => null,
			'ProxyPort' => -1,
			'ProxyUsername' => null,
			'ProxyPassword' => null,
			'MaxErrorRetry' => 3,
			);
			
			$service = new FBAInboundServiceMWS_Client(
				AWS_ACCESS_KEY_ID,
				AWS_SECRET_ACCESS_KEY,
				APPLICATION_NAME,
				APPLICATION_VERSION,
				$config);
			
			$next_token_data = file_get_contents(WWW_ROOT.'logs/list_inbound_shipment_items_next_token.txt');	
			list($shipment_inc_id, $next_token) = explode("\t", $next_token_data);			 	
			 
 			$request = new FBAInboundServiceMWS_Model_ListInboundShipmentItemsByNextTokenRequest(); 
			$request->setSellerId(MERCHANT_ID);
			$request->setNextToken($next_token);	
		 
			$xml = $this->invokeListInboundShipmentItemsByNextToken($service, $request);
			 			
			if(isset($xml->Error)){  
				file_put_contents(WWW_ROOT.'logs/fba_list_shipment_error_'.date('Ymd').'.log', 
				date('Y-m-d H:i:s')."\tErrorCode:".$xml->Error->Code."\tErrorMessage:".$xml->Error->Message."\r\n", 
				FILE_APPEND | LOCK_EX);		 	
			}else{   
				@unlink(WWW_ROOT.'logs/list_inbound_shipment_items_next_token.txt');
				if(isset($xml->ListInboundShipmentItemsByNextTokenResult->ItemData)){
				 	
					if(isset($xml->ListInboundShipmentItemsByNextTokenResult->NextToken)){	
						file_put_contents(WWW_ROOT.'logs/list_inbound_shipment_items_next_token.txt',$shipment_inc_id."\t".$xml->ListInboundShipmentItemsByNextTokenResult->NextToken);	
					}
					
					$this->saveShipmentItems($xml->ListInboundShipmentItemsByNextTokenResult->ItemData->member,$shipment_inc_id);
										
				}
			}
		} 
		pr($xml); 
		exit;
	}
	
	public function getShipmentItems( $shipment_id = null) 
	{
		$this->loadModel('FbaApiShipmentItem');	
		 
		$quantity_shipped = $quantity_received = $quantity_in_case = 0;
		 
		$skus = 0;
		$fba_shipment_items = $this->FbaApiShipmentItem->find('all', array('conditions'=>array('FbaApiShipmentItem.shipment_id' => $shipment_id)) );
		if(count($fba_shipment_items) > 0 ){
			foreach($fba_shipment_items as $items){
				$quantity_shipped += $items['FbaApiShipmentItem']['quantity_shipped'];
				$quantity_received += $items['FbaApiShipmentItem']['quantity_received'];
				$quantity_in_case += $items['FbaApiShipmentItem']['quantity_in_case']; 
				$skus++;
			}
		}
		return  array('skus'=>$skus ,'quantity_shipped'=>$quantity_shipped ,'quantity_received' => $quantity_received, 'quantity_in_case' => $quantity_in_case);
	}
	
	 
	
	private function invokeListInboundShipments(FBAInboundServiceMWS_Interface $service, $request)
	{
		  try {
			$response = $service->ListInboundShipments($request);
			$dom = new DOMDocument();
			$dom->loadXML($response->toXML());
			$dom->preserveWhiteSpace = false;
			$dom->formatOutput = true;
			$getResponse = $dom->saveXML();			
			return simplexml_load_string($getResponse);
	
		 } catch (MarketplaceWebServiceOrders_Exception $ex) {
			 return simplexml_load_string($ex->getXML()) ;
		 }
	 }

	private function invokeListInboundShipmentItems(FBAInboundServiceMWS_Interface $service, $request)
	{
		  try {
			$response = $service->ListInboundShipmentItems($request);
			$dom = new DOMDocument();
			$dom->loadXML($response->toXML());
			$dom->preserveWhiteSpace = false;
			$dom->formatOutput = true;
			$getResponse = $dom->saveXML();			
			return simplexml_load_string($getResponse);
	
		 } catch (MarketplaceWebServiceOrders_Exception $ex) {
			 return simplexml_load_string($ex->getXML()) ;
		 }
	 } 
	private function invokeListInboundShipmentItemsByNextToken(FBAInboundServiceMWS_Interface $service, $request)
	  {
		  try {
			$response = $service->ListInboundShipmentItemsByNextToken($request);
	
			$dom = new DOMDocument();
			$dom->loadXML($response->toXML());
			$dom->preserveWhiteSpace = false;
			$dom->formatOutput = true;
			$getResponse = $dom->saveXML();			
			return simplexml_load_string($getResponse);
	
		 } catch (MarketplaceWebServiceOrders_Exception $ex) {
			 return simplexml_load_string($ex->getXML()) ;
		 }
	 }
	  
	 
}