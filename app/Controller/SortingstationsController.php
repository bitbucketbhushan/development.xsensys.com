<?php
error_reporting(0);
class SortingstationsController extends AppController
{
    /* controller used for linnworks api */
    var $name = "Sortingstations";
    var $components = array('Session','Upload','Common','Auth','Paginator');
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
   public function index()
   {
   		$this->layout = "profit";
		$this->loadModel('SortingStation');
		$this->loadModel('PostalServiceDesc');
		$this->loadModel('Location');
		$getshortorders		=	$this->SortingStation->find('all', array( 'order' => 'position ASC' ));
		$postalservices		=	$this->PostalServiceDesc->find('list');
		$j = 0;
		
		foreach( $getshortorders as $getshortorder )
		{
			$serviceids		=	explode(',', $getshortorder['SortingStation']['service_id']);
			$i = 0;
			$total = array();
			$scanned = array();
			foreach( $serviceids as $serviceid )
			{
				$conids				=	explode(',', $getshortorder['SortingStation']['country']);
				$country = array();
				foreach($conids as $conid)
				{
					$getcountry 	=	$this->Location->find('first', array('conditions' => array( 'id' => $conid ), 'fields' => array( 'Location.county_name' ) ) );
					$country[] 		= 	$getcountry['Location']['county_name'];
				}
				$getshortorders[$j]['SortingStation']['country']	=	implode(',', $country);
				$getpostalservices	=	$this->PostalServiceDesc->find('first',array( 'conditions' => array( 'PostalServiceDesc.id' => $serviceid ) ) );
				if(!empty($getpostalservices))
				{
					$getshortorders[$j]['SortingStation']['servicecodes'][$i]	=	$getpostalservices['PostalServiceDesc']['provider_ref_code'];
				}
				$total[] 		= 	$this->getTotalOrdercount($serviceid, 'Total');
				$scanned[] 	= 	$this->getTotalOrdercount($serviceid, 'scaneed');
				$i++;
			}
			$getshortorders[$j]['SortingStation']['totalcount'] = array_sum($total);
			$getshortorders[$j]['SortingStation']['scaneed'] = array_sum($scanned);
			$j++;
		}
		$this->set('getshortorders', $getshortorders);
		$this->set('postalservices', $postalservices);
		
		//$postalservices
   }
   
   public function getTotalOrdercount( $serviceid = null, $ordcount = null )
   {	
   		//$serviceid = '51';
   		//$ordcount = 'Total';
   		$this->loadModel('ServiceCounter');
		$getcounter	=	$this->ServiceCounter->find('first', array( 'conditions' => array( 'ServiceCounter.service_id'=> $serviceid), 'fields' => array( 'counter', 'original_counter' ) ) );
		if(count($getcounter) == 1)
		{
			if($ordcount == 'Total'){
				$getcounter	=	$this->ServiceCounter->find('all',array('conditions'=>array('ServiceCounter.service_id'=>$serviceid),'fields'=>array('SUM(original_counter) AS original_counter') ) );
				return ($getcounter[0][0]['original_counter'] != '') ? $getcounter[0][0]['original_counter'] : '0';	
			} else {
				$getcounter	=	$this->ServiceCounter->find('all', array('conditions'=>array('ServiceCounter.service_id'=>$serviceid),'fields'=>array('SUM(counter) AS counter')));
				return ($getcounter[0][0]['counter'] != '' ) ? $getcounter[0][0]['counter'] : '0';
			}
		} else {
			   return '0';
		}
   }
   
   public function manageGroupBox()
   {
   		$this->layout = "index";
		$this->loadModel('PostalServiceDesc');
		$this->loadModel('Location');
		$this->loadModel('PostalProvider');
		// 'fields' => array('User.id', 'User.name'),Location.county_name
		$postalservices	=	$this->PostalServiceDesc->find('all');
		foreach($postalservices as $val)
		{
			$servicedata[$val['PostalServiceDesc']['id']] =  $val['PostalServiceDesc']['provider_ref_code'].' [ '.$val['Location']['county_name'].' ] ';
		}
		$locationList	=	$this->Location->find('list', array( 'fields' => array( 'Location.county_name' ) ));
		$postalproviders =   $this->PostalProvider->find('list', array( 'fields' => array( 'PostalProvider.provider_name' ) ) );
		
		$this->set('locationList', $locationList);
		$this->set('postalservices', $servicedata);
		$this->set('postalproviders', $postalproviders);
   }
   
   public function getselectedservice()
   {
   		$this->layout = "";
		$this->autoRender = false;
		$this->loadModel('PostalServiceDesc');
		$this->loadModel('SortingStation');
		$seleid	=	$this->request->data['selected'];
		$popid	=	$this->request->data['postalprovideraval'];
		if(count($seleid) > 1)
		{
			$postalservices	=	$this->PostalServiceDesc->find('all', array('conditions'=> array( 'delevery_country IN' =>  $seleid, 'postal_provider_id' =>  $popid ) ) );
		} else {
			$postalservices	=	$this->PostalServiceDesc->find('all', array('conditions'=> array( 'delevery_country' =>  $seleid, 'postal_provider_id' =>  $popid ) ) );
		}
		foreach($postalservices as $val)
		{
			$servicedata[$val['PostalServiceDesc']['id']] =  $val['PostalServiceDesc']['provider_ref_code'].' [ '.$val['Location']['county_name'].' ] ';
		}
		
		if(!empty($servicedata)){
			echo json_encode(array( 'status' => '1', 'service' => $servicedata ) );
			exit;
		}
  }
   
   public function savegroupinfo()
   {
   		$this->layout = "";
		$this->autoRender = false;
		$this->loadModel('SortingStation');
		$this->loadModel('PostalServiceDesc');
	
		$servicepro	=	$this->request->data['postalprovideraval'];
		$countryid	=	implode(',', $this->request->data['countryandservice']);
		$serviceid	=	implode(',', $this->request->data['postalservice']);
		$boxname	=	$this->request->data['serviceboxname'];
		$color		=	$this->request->data['color'];
		$provname	=	($this->request->data['postalprovidname'] == 'Belgium Post') ? 'PostNL' : $this->request->data['postalprovidname'];
		$ids	=	explode(',', $serviceid);
		foreach($ids as $id)
		{	
			$getids	=	$this->SortingStation->find('all', array( 'fields' => array('service_id') ) );
			foreach( $getids as $getid )
			{
				$sids	=	$getid['SortingStation']['service_id'];
				$sis	=	explode(',',$sids);
				foreach($sis as $si)
				{
					if($si == $id)
					{
						$postalservices	=	$this->PostalServiceDesc->find('first',array('conditions'=> array('PostalServiceDesc.id'=> $id) ) );
						$servicedata 	=	$postalservices['PostalServiceDesc']['provider_ref_code'].' [ '. $postalservices['Location']['county_name'].' ] ';
						echo json_encode(array( 'status' => '4', 'service' => $servicedata ) );
						exit;
					}
				}
			}
			
		}
		$checkbox = $this->SortingStation->find('first', array( 'conditions' => array('group_box' => $boxname ) ) );
		if(count($checkbox) == 1)
		{
			echo json_encode(array( 'status' => '3', 'service' => '' ) );
			exit;		
		}
		$checkgroup = $this->SortingStation->find('first', array( 'conditions' => array( 
																		'postal_provider_id' => $servicepro,
																		'group_box' => $boxname,
																		'country' => $countryid,
																		'service_id' => $serviceid
																		 ) ) );
		if(count($checkgroup) == 1)
		{
			echo json_encode(array( 'status' => '2', 'service' => '' ) );
			exit;
		} else {
			$serdata['postal_provider_id'] = $servicepro;
			$serdata['group_box'] = $boxname;
			$serdata['country'] = $countryid;
			$serdata['service_id'] = $serviceid;
			$serdata['postal_provider_name'] = $provname;
			$serdata['color'] = $color;
			$this->SortingStation->saveAll( $serdata );
			echo json_encode(array( 'status' => '1', 'service' => '' ) );
			exit;
		}
   }
   
   public function showAllGroup() 
   {
   		$this->layout = "index";
		$this->loadModel('PostalServiceDesc');
		$this->loadModel('Location');
		$this->loadModel('PostalProvider');
		$this->loadModel('SortingStation');
		
		$getallgroups		=	$this->SortingStation->find('all', array('order' => 'section ASC, position ASC'));
		$getallleft			=	$this->SortingStation->find('all', array('conditions' => array('section' => 1),'order' => 'position ASC'));
		$getallkright		=	$this->SortingStation->find('all', array('conditions' => array('section' => 2),'order' => 'position ASC'));
		
		$j=0;
		foreach( $getallgroups as $getallgroup )
		{
				$country	=	explode(',', $getallgroup['SortingStation']['country']);
				$services	=	explode(',', $getallgroup['SortingStation']['service_id']);
				$counarray = array();
				foreach( $country as $val )
				{
					$getcountry 	=	$this->Location->find('first', array('conditions' => array( 'id' => $val ), 'fields' => array( 'Location.county_name' ) ) );
					$counarray[]	=	$getcountry['Location']['county_name'];
				}
				$getallgroups[$j]['SortingStation']['country_name']	=	implode(',',$counarray);
				$serarray = array();
				foreach($services as $service)
				{
					$getservice 	=	$this->PostalServiceDesc->find('first',array('conditions'=>array('PostalServiceDesc.id'=>$service),'fields'=>array('PostalServiceDesc.provider_ref_code', 'Location.county_name') ) );
					$serarray[]		=	$getservice['PostalServiceDesc']['provider_ref_code'].' [ '.$getservice['Location']['county_name'].' ] ';
				}
				$getallgroups[$j]['SortingStation']['service_name']	=	implode(',',$serarray);
				$j++;
		}
		$this->set( 'getallgroups',$getallgroups );
		$this->set( 'getallleft',$getallleft );
		$this->set( 'getallkright',$getallkright );
   }
   
   public function checkBarcodeForSortingOperator( $order_barcode = null)
	{
			$this->autoRender = false;
			$this->layout = '';
			//$order_barcode = '1682600-1';
			$this->loadModel('MergeUpdate');
			$this->loadModel('Product');
			$this->loadModel('SortingStation');
			$this->loadModel('OrderNote');
			$this->loadModel('OrderLocation');
			$this->loadModel( 'DynamicPicklist' );
			
			$firstName = ( $_SESSION['Auth']['User']['first_name'] != '' ) ? $_SESSION['Auth']['User']['first_name'] : '_';
			$lastName = ( $_SESSION['Auth']['User']['last_name'] != '' ) ? $_SESSION['Auth']['User']['last_name'] : '_';
			$pcName = ( $_SESSION['Auth']['User']['pc_name'] != '' ) ? $_SESSION['Auth']['User']['pc_name'] : '_';
			
			$username = $firstName .' '. $lastName;
			if($order_barcode == ''){
				$barcode  =  trim($this->request->data['barcode']); 
			} else {
				$barcode  =  $order_barcode; 
			}
			//$barcode  =  '1364705-1';
			
			if($barcode)
			{
				$results = $this->MergeUpdate->find('all', 
						array('conditions' => array('MergeUpdate.product_order_id_identify'=>$barcode, 
													'MergeUpdate.status' => '2' , 
													'MergeUpdate.sorted_scanned' => '0' , 
													'MergeUpdate.scan_status' => '0')));
				if(count($results) == 0)
				{
					$results = $this->MergeUpdate->find('all', 
						array('conditions' => array('MergeUpdate.postnl_barcode'=>$barcode, 
													'MergeUpdate.status' => '2' , 
													'MergeUpdate.sorted_scanned' => '0' , 
													'MergeUpdate.scan_status' => '0')));
													
													
				
				}
				
				if(count($results) == 1)
				{
					$bin_loc = array();
					$results = $this->MergeUpdate->find('first',array(
						'conditions'=>array('MergeUpdate.product_order_id_identify'=>$barcode,'MergeUpdate.status'=>'2'),
						'fields' => array( 'cancelled_user','cancelled_pc','batch_num','order_id','product_order_id_identify'))
					);
					
					if(count($results) == 0)
					{
						$results = $this->MergeUpdate->find('first', 
							array('conditions' => array('MergeUpdate.postnl_barcode'=>$barcode,'MergeUpdate.status' => '2'),
							'fields' => array( 'cancelled_user','cancelled_pc','batch_num','order_id','product_order_id_identify')));
														
														
					
					}
					
					
					//$oids		=	explode('-',$barcode);
					
					$order_id = $results['MergeUpdate']['order_id'];
					$odlocas	=	$this->OrderLocation->find('all', array( 'conditions' => array( 'order_id' => $order_id ) ) );
					foreach($odlocas as $loc){
						$bin_loc[] = $loc['OrderLocation']['bin_location'];
					}
					$user	=	$results['MergeUpdate']['cancelled_user'];
					$pc		=	$results['MergeUpdate']['cancelled_pc'];
					
					$cancelstatus			=	'1####'.$user.'####'.$pc.'####'.$barcode.'####'.implode(',',$bin_loc);
					echo $cancelstatus;
					exit;
				}
			}
			if($barcode)
			{
				$results = $this->MergeUpdate->find('all', 
						array('conditions' => array('OR' => array('MergeUpdate.product_order_id_identify'=>$barcode,'MergeUpdate.postnl_barcode'=>$barcode, 'MergeUpdate.brt_barcode'=>$barcode ), 
													'MergeUpdate.status' => '1' , 
													'MergeUpdate.sorted_scanned' => '0' , 
													'MergeUpdate.scan_status' => '0')));
			}
			
			$orderid		=	explode('-',$barcode);
			$check_spain 	= 	$this->MergeUpdate->find('all', array('conditions' => array('OR' => array('MergeUpdate.postnl_barcode'=>$barcode,'MergeUpdate.order_id'=>$orderid[0] ), 
													'MergeUpdate.delevery_country' => 'Spain',
													)));
			 
			
			if( count( $check_spain ) > 1 )
			{
				foreach($check_spain as $val) {
					if(date('Y-m-d',strtotime($val['MergeUpdate']['scan_date'])) == date('Y-m-d'))
					{
						echo "5";
						exit;
					}
				}
			}
			
			if( count($results) > 0 ){
				$dp = $this->DynamicPicklist->find('first',array('conditions' => array('id' => $results[0]['MergeUpdate']['picklist_inc_id'])));
				if(count($dp) > 0 && $dp['DynamicPicklist']['picklist_type'] == 'single'){
					if($results[0]['MergeUpdate']['batch_num'] != ''){
						$this->sortbatchorders($results[0]['MergeUpdate']['batch_num']);
					} 
				}	
			}
			// Update tables and create manifest according to services those alloted already with manifest feild
			if( count($results) > 0 )
			{
			 
				$i = 0;
				foreach($results as $result)
				{
					$splitskus	=	 explode(',', $results['MergeUpdate']['sku']);
					foreach( $splitskus as $splitsku )
					{
						$products 	= 	explode('XS-', $splitsku);
						$productsku = 	'S-'.$products[1];
						$productcat =  $this->Product->find( 'first', array( 'conditions' => array( 'Product.product_sku' => $productsku ), 'fields'=> array( 'category_name' ) ) );
						$cat_name[] =  $productcat['Product']['category_name'];
					}
					$openOrderId 		= $result['MergeUpdate']['id'];
					$serviceName 		= $result['MergeUpdate']['service_name'];
					$serviceProvider 	= $result['MergeUpdate']['service_provider'];
					$serviceCode 		= $result['MergeUpdate']['provider_ref_code'];
					$country 			= $result['MergeUpdate']['delevery_country'];
					$serviceid 			= $result['MergeUpdate']['service_id'];

					// Update Service table and applied counter value
					$this->loadModel( 'ServiceCounter' );
					
					
					if($serviceProvider == 'DHL'){
					
						$getCounterValue = $this->ServiceCounter->find( 'first' , 
							array( 'conditions' => array('ServiceCounter.service_id' => $serviceid )
						) );
					
					}else{
						$getCounterValue = $this->ServiceCounter->find( 'first' , 
						array( 'conditions' => 
						array( 
						 //'ServiceCounter.service_name' => $serviceName,
						 //'ServiceCounter.service_provider' => $serviceProvider,
						 //'ServiceCounter.service_code' => $serviceCode,
						  'ServiceCounter.destination' => $country,
						  'ServiceCounter.service_id' => $serviceid          
						)
						) );
					}
					
					//Manage Counter Section
					$counter = 0;
					if( $getCounterValue['ServiceCounter']['counter'] > 0 ):
					$counter = $getCounterValue['ServiceCounter']['counter'];
					$counter = $counter + 1;
					else:
					$counter = $counter + 1;
					endif;
					
					// Manage Order Id's also
					$orderCommaSeperated = '';
					if( $getCounterValue['ServiceCounter']['order_ids'] != '' ):
					$orderCommaSeperated = $getCounterValue['ServiceCounter']['order_ids'];
					$orderCommaSeperated = $orderCommaSeperated.','.$openOrderId;
					else:
					$orderCommaSeperated = $openOrderId;
					endif;

					 // Now, Updating Services ...... 
					$this->request->data['ServiceCounter']['ServiceCounter']['id'] = $getCounterValue['ServiceCounter']['id'];      
					$this->request->data['ServiceCounter']['ServiceCounter']['counter'] = $counter;
					$this->request->data['ServiceCounter']['ServiceCounter']['order_ids'] = $orderCommaSeperated;         
					$this->ServiceCounter->saveAll( $this->request->data['ServiceCounter'] );
					 
					 // Now, Updating Open Order ...... 
					$this->request->data['MergeUpdate']['MergeUpdate']['id'] = $openOrderId;            
					$this->request->data['MergeUpdate']['MergeUpdate']['sorted_scanned'] = 1;         
					$this->request->data['MergeUpdate']['MergeUpdate']['scan_status'] = 1;  
					$this->request->data['MergeUpdate']['MergeUpdate']['scan_date'] = date('Y-m-d H:i:s');  
					$this->request->data['MergeUpdate']['MergeUpdate']['scanned_user'] = $username;  
					$this->request->data['MergeUpdate']['MergeUpdate']['scanned_pc'] = $pcName;  
					$this->MergeUpdate->saveAll( $this->request->data['MergeUpdate'] );
					
					//one step more to store and calculate the soring items by user logged
					// Get Session User
				   $user_id = $this->Session->read('Auth.User.id');
				   
				   // Call Query
				   $paramOperator = array(
						'conditions' => array(
										'SortingoperatortimeCalculation.user_id' => $user_id,
										'SortingoperatortimeCalculation.today_date' => date( 'Y-m-d' )
						)
					);
				   
				   // Query Here
				   $this->loadModel('SortingoperatortimeCalculation');
				   $getOperator = $this->SortingoperatortimeCalculation->find( 'first', $paramOperator );
				   
				   //Update first out time
				   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['id'] = $getOperator['SortingoperatortimeCalculation']['id'];
				   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['user_id'] = $user_id;
				   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['out_time'] = date('Y-m-d G:i:s');
				   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['no_of_orders_sorted'] = $getOperator['SortingoperatortimeCalculation']['no_of_orders_sorted'] + 1;				   
				   //$this->SortingoperatortimeCalculation->saveAll( $this->request->data['SortingoperatortimeCalculation'] ); 
				   
				   // Query Here
				   $getOperator = $this->SortingoperatortimeCalculation->find( 'first', $paramOperator ); 
				   
				   // Calculation
				   $logout = strtotime($getOperator['SortingoperatortimeCalculation']['out_time']);
				   $login  = strtotime($getOperator['SortingoperatortimeCalculation']['in_time']);
				   $diff   = $logout - $login;                               
				   $timeCalculate = round( $diff / 3600 ) ." hour ". round($diff/60)." minutes ".($diff%60)." seconds";
				   
				   // Yes, calculate the time but we need to check one more time is that today or not
				   $dayString = $getOperator['SortingoperatortimeCalculation']['in_time'];
				   $dayStringSub = substr($dayString, 0, 10);

				   $isToday = ( strtotime('now') >= strtotime($dayStringSub . " 00:00") 
																  && strtotime('now') <  strtotime($dayStringSub . " 23:59") );
				   
					// Today
				   if( $isToday == 1 ):
												
						// Update a row again and again
						//Update calculate time
					   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['id'] = $getOperator['SortingoperatortimeCalculation']['id'];
					   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['user_id'] = $user_id;
					   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['total_time'] = $timeCalculate;
					   //$this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['no_of_orders_sorted'] = $glbalSortingCounter;
					   $this->SortingoperatortimeCalculation->saveAll( $this->request->data['SortingoperatortimeCalculation'] ); 
					else:
					
						// Insert new row, If today is not
						//Insert calculate time                                      
					    $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['user_id'] = $user_id;
					    $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['total_time'] = $timeCalculate;
					    //$this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['no_of_orders_sorted'] = $glbalSortingCounter;
					   $this->SortingoperatortimeCalculation->saveAll( $this->request->data['SortingoperatortimeCalculation'] ); 
												
				   endif; 
				   
					 // Manifest Creation
					 //$this->createManifestAccordingToServiceIfYes();
					 
					$i++;
				} 
				
				//$data = $this->SpecialOffer->find('all',array('conditions' => array('SpecialOffer.user_id' => $userId ,'FIND_IN_SET(\''. $storeId .'\',SpecialOffer.stores1)')));
				$getcombationids		=	$this->SortingStation->find('first', array('conditions' => array( 'FIND_IN_SET(\''. $serviceid .'\',SortingStation.service_id)' ) ) );
				$combinationid			=	$getcombationids['SortingStation']['id'].'---'.$getcombationids['SortingStation']['color'];//'---"'
				if(in_array( 'Alkaline Battery' , $cat_name ) && $serviceProvider == 'Jersey Post' && $country == 'Spain')
				{	$cate = 'Alkaline Battery'; }
				else{ $cate = ''; }
				
				$strRep = str_replace(')','-',str_replace('(','-',str_replace('<','-',str_replace(' ','-',$combinationid))));
				$strFormat = preg_replace('/[^a-zA-Z0-9\']/', '-', $strRep);                
				$fullformat = $strFormat.'####'.$cate; 
				return strtolower($fullformat);exit;
			}
			else
			{
				//Check scan status
				$results = json_decode(json_encode($this->MergeUpdate->find('all', 
							array('conditions' => array('MergeUpdate.product_order_id_identify'=>$barcode, 
														'MergeUpdate.status' => '1' )))),0);
				if(count($results) == 0 ){
					$results = json_decode(json_encode($this->MergeUpdate->find('all', 
							array('conditions' => array('MergeUpdate.postnl_barcode'=>$barcode, 
														'MergeUpdate.status' => '1' )))),0);
				
				}
														
				if( count($results) > 0 )
				{
					
					if( $results[0]->MergeUpdate->scan_status == 1 )
					{
						if(in_array( 'Alkaline Battery' , $cat_name ) && $serviceProvider == 'Jersey Post' && $country == 'Spain')
						{
							echo "scannedbattery"; 
							unset( $cat_name );
							exit;
						} else {
								echo "scanned"; exit; 
							}
					}
				}
				else
				{
					echo "none"; exit;	
				}				
			}
		} 
		
   public function processByPicklistBarcode( $picklist_barcode = null)
	{
		$this->autoRender = false;
		$this->layout = '';
		//$order_barcode = '1682600-1';
		$this->loadModel('MergeUpdate');
		$this->loadModel('Product');
		$this->loadModel('SortingStation');
		$this->loadModel('OrderNote');
		$this->loadModel('OrderLocation');
		$this->loadModel('DynamicPicklist' );		 
		$this->loadModel('ServiceCounter' );
		
		
		$firstName = ( $_SESSION['Auth']['User']['first_name'] != '' ) ? $_SESSION['Auth']['User']['first_name'] : '_';
		$lastName = ( $_SESSION['Auth']['User']['last_name'] != '' ) ? $_SESSION['Auth']['User']['last_name'] : '_';
		$pcName = ( $_SESSION['Auth']['User']['pc_name'] != '' ) ? $_SESSION['Auth']['User']['pc_name'] : '_';
		
		$username = $firstName .' '. $lastName;
		
		if($picklist_barcode == ''){
		   $picklist_barcode = $this->request->data['barcode'];
		}
		 
		$dpresults = $this->DynamicPicklist->find('first', 
					array('conditions' => array('picklist_barcode' => $picklist_barcode)));
		 //pr($dpresults);										
  		if(count($dpresults) > 0)
		{			  			
			if(empty($dpresults['DynamicPicklist']['completed_mark_user']))
			{
				$picklist_inc_id = $dpresults['DynamicPicklist']['id'];
				$users_array = array('avadhesh.kumar@jijgroup.com','amit.gaur@jijgroup.com','mmarecky82@gmail.com','patrycja.swiatek@onet.eu',$dpresults['DynamicPicklist']['assign_user_email']);
				 
				if(in_array($_SESSION['Auth']['User']['email'],$users_array)){
					App::import('Controller', 'DynamicPicklist');
					$dpObj = new DynamicPicklistController(); 
					
					//$msg['process']  = $dpObj->batckPicklistBulkOrderProcess($picklist_inc_id,'sorting');
					$msg['sorting']  = $dpObj->sortbatchorders($picklist_inc_id,'sorting');
					echo json_encode($msg); 	
				}else{
					$msg['process']['msg']  = 'This picklist is not assigned to you. So you can not process it.';
					$msg['sorting']  = '';
					echo json_encode($msg); 
				}
 			}else{
  				$msg['msg']  =  'This picklist is already mark as completed.';
				echo json_encode($msg); 	
				exit;
			}
		}else{
			$msg['msg']  =  'No picklist found for this barcode.';
			echo json_encode($msg); 
		}
		exit;
	}   
		
   public function sortbatchorders( $batchnum = null )
		{
			//$batchnum	=	'BPN20180830024154';
			$this->autoRender = false;
			$this->layout = '';
			$this->loadModel('MergeUpdate');
			$this->loadModel('Product');
			$this->loadModel('SortingStation');
			$this->loadModel('OrderNote');
			$this->loadModel('OrderLocation');
			$this->loadModel( 'ServiceCounter' );
			//$batchnum = 'BPN20180817';
			$firstName = ( $_SESSION['Auth']['User']['first_name'] != '' ) ? $_SESSION['Auth']['User']['first_name'] : '_';
			$lastName = ( $_SESSION['Auth']['User']['last_name'] != '' ) ? $_SESSION['Auth']['User']['last_name'] : '_';
			$pcName = ( $_SESSION['Auth']['User']['pc_name'] != '' ) ? $_SESSION['Auth']['User']['pc_name'] : '_';
			
			$username = $firstName .' '. $lastName;
			
			$getall_batchorders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.batch_num' => $batchnum ) ) );
			//pr($getall_batchorders);
			//exit;
			$o_id = array();
			$bin_loc = array();
			foreach( $getall_batchorders as $getall_batchorder ){ 
				if($getall_batchorder['MergeUpdate']['status'] == 2){ 
					$oids		=	explode('-',$getall_batchorder['MergeUpdate']['product_order_id_identify']);
					$o_id[] 	= 	$getall_batchorder['MergeUpdate']['product_order_id_identify']; 
					$user		=	$getall_batchorder['MergeUpdate']['cancelled_user'];
					$pc			=	$getall_batchorder['MergeUpdate']['cancelled_pc'];
					$odlocas	=	$this->OrderLocation->find('all', array( 'conditions' => array( 'order_id' => $oids[0] ) ) );
					foreach($odlocas as $loc){
						$bin_loc[] = $loc['OrderLocation']['bin_location'];
					}
					$i++;
				}
			}
			if($i > 0)
			{
				$soids		=	implode(',',$o_id);
				echo $cancelstatus		=	'2####'.$user.'####'.$pc.'####'.$soids.'####'.implode(',',$bin_loc);
				exit;
			}
			$results = $this->MergeUpdate->find('all', 
						array('conditions' => array('MergeUpdate.batch_num'=>$batchnum, 'MergeUpdate.status' => '1' , 
													'MergeUpdate.sorted_scanned' => '0' , 
													'MergeUpdate.scan_status' => '0')));
			
			if( count($results) > 0 )
			{
				$i = 0;
				foreach($results as $result)
				{
					$openOrderId 		= $result['MergeUpdate']['id'];
					$serviceName 		= $result['MergeUpdate']['service_name'];
					$serviceProvider 	= $result['MergeUpdate']['service_provider'];
					$serviceCode 		= $result['MergeUpdate']['provider_ref_code'];
					$country 			= $result['MergeUpdate']['delevery_country'];
					$serviceid 			= $result['MergeUpdate']['service_id'];
					
					if($serviceProvider == 'DHL'){
						//$getCounterValue = $this->ServiceCounter->find( 'first' , array( 'conditions' => array('ServiceCounter.service_id' => $serviceid ) ) );
					}else{
						$getCounterValue = $this->ServiceCounter->find( 'first' , 
						array( 'conditions' => array( 'ServiceCounter.destination' => $country,'ServiceCounter.service_id' => $serviceid )) );
					}
					
					$counter = 0;
					if( $getCounterValue['ServiceCounter']['counter'] > 0 ){
						$counter = $getCounterValue['ServiceCounter']['counter'];
						$counter = $counter + 1;
					} else {
						$counter = $counter + 1;
					}
					
					$orderCommaSeperated = '';
					if( $getCounterValue['ServiceCounter']['order_ids'] != '' ){
						$orderCommaSeperated = $getCounterValue['ServiceCounter']['order_ids'];
						$orderCommaSeperated = $orderCommaSeperated.','.$openOrderId;
					} else {
						$orderCommaSeperated = $openOrderId;
					}
					
					$this->request->data['ServiceCounter']['ServiceCounter']['id'] = $getCounterValue['ServiceCounter']['id'];      
					$this->request->data['ServiceCounter']['ServiceCounter']['counter'] = $counter;
					$this->request->data['ServiceCounter']['ServiceCounter']['order_ids'] = $orderCommaSeperated; 
					//pr($this->request->data['ServiceCounter']);        
					$this->ServiceCounter->saveAll( $this->request->data['ServiceCounter'] );

					$this->request->data['MergeUpdate']['MergeUpdate']['id'] = $openOrderId;            
					$this->request->data['MergeUpdate']['MergeUpdate']['sorted_scanned'] = 1;         
					$this->request->data['MergeUpdate']['MergeUpdate']['scan_status'] = 1;  
					$this->request->data['MergeUpdate']['MergeUpdate']['scan_date'] = date('Y-m-d H:i:s');  
					$this->request->data['MergeUpdate']['MergeUpdate']['scanned_user'] = $username;  
					$this->request->data['MergeUpdate']['MergeUpdate']['scanned_pc'] = $pcName;  
					//pr($this->request->data['MergeUpdate']);
					$this->MergeUpdate->saveAll( $this->request->data['MergeUpdate'] );
				   
				    // Call Query
					$i++;
					$getcombationids	=	$this->SortingStation->find('first', array('conditions' => array( 'FIND_IN_SET(\''. $serviceid .'\',SortingStation.service_id)' ) ) );
					$combinationid		=	$getcombationids['SortingStation']['id'].'---'.$getcombationids['SortingStation']['color'].'---'.$i;//'---"'
					$cate = '';
					$strRep = str_replace(')','-',str_replace('(','-',str_replace('<','-',str_replace(' ','-',$combinationid))));
					$strFormat = preg_replace('/[^a-zA-Z0-9\']/', '-', $strRep);                
					$fullformat = $strFormat.'####'.$i; 
					
					$assign_service_log = "Split_Order_Row_Id=".$orderCommaSeperated."\tService id=".$getCounterValue['ServiceCounter']['id']."\tService name=".$result['MergeUpdate']['service_name']."\tService Provider=".$result['MergeUpdate']['service_provider']."\tService code=".$result['MergeUpdate']['provider_ref_code']."\tBatch Number=".$batchnum."\tUser=".$_SESSION['Auth']['User']['first_name'].' '.$_SESSION['Auth']['User']['last_name']."\t".date('Y-m-d H:i:s')."\n";
					file_put_contents(WWW_ROOT."img/assign_service.log", $assign_service_log, FILE_APPEND | LOCK_EX);
					
				}
				echo strtolower($fullformat);exit;
			}
			else
			{
				$results = json_decode(json_encode($this->MergeUpdate->find('all', 
							array('conditions' => array('MergeUpdate.batch_num'=>$batchnum, 
														'MergeUpdate.status' => '1' )))),0);
														
				if( count($results) > 0 )
				{
					if( $results[0]->MergeUpdate->scan_status == 1 )
					{
						if(in_array( 'Alkaline Battery' , $cat_name ) && $serviceProvider == 'Jersey Post' && $country == 'Spain')
						{
							echo "scannedbattery"; 
							unset( $cat_name );
							exit;
						} else {
								echo "scanned"; exit; 
							}
					}
				}
				else
				{
					echo "none"; exit;	
				}	
				
			}
		}
   
    public function checkBarcodeForSortingOperator_240818()
		{
			$this->autoRender = false;
			$this->layout = '';

			$this->loadModel('MergeUpdate');
			$this->loadModel('Product');
			$this->loadModel('SortingStation');
			$this->loadModel('OrderNote');
			$this->loadModel('OrderLocation');
			date_default_timezone_set('Europe/Jersey');
			
			$firstName = ( $_SESSION['Auth']['User']['first_name'] != '' ) ? $_SESSION['Auth']['User']['first_name'] : '_';
			$lastName = ( $_SESSION['Auth']['User']['last_name'] != '' ) ? $_SESSION['Auth']['User']['last_name'] : '_';
			$pcName = ( $_SESSION['Auth']['User']['pc_name'] != '' ) ? $_SESSION['Auth']['User']['pc_name'] : '_';
			
			$username = $firstName .' '. $lastName;

			$barcode  =  $this->request->data['barcode']; 
			
			if($barcode)
			{
				$results = $this->MergeUpdate->find('all', 
						array('conditions' => array('MergeUpdate.product_order_id_identify'=>$barcode, 
													'MergeUpdate.status' => '2' , 
													'MergeUpdate.sorted_scanned' => '0' , 
													'MergeUpdate.scan_status' => '0')));
				if(count($results) == 1)
				{
					$bin_loc = array();
					$results = $this->MergeUpdate->find('first',array('conditions'=>array('MergeUpdate.product_order_id_identify'=>$barcode,'MergeUpdate.status'=>'2'),
																	'fields' => array( 'cancelled_user','cancelled_pc')));
					$oids		=	explode('-',$barcode);
					$odlocas	=	$this->OrderLocation->find('all', array( 'conditions' => array( 'order_id' => $oids[0] ) ) );
					foreach($odlocas as $loc){
						$bin_loc[] = $loc['OrderLocation']['bin_location'];
					}
					$user	=	$results['MergeUpdate']['cancelled_user'];
					$pc		=	$results['MergeUpdate']['cancelled_pc'];
					
					$cancelstatus			=	'1####'.$user.'####'.$pc.'####'.$barcode.'####'.implode(',',$bin_loc);
					echo $cancelstatus;
					exit;
				}
													
			}
			if($barcode)
			{
				$results = $this->MergeUpdate->find('all', 
						array('conditions' => array('OR' => array('MergeUpdate.product_order_id_identify'=>$barcode, 'MergeUpdate.brt_barcode'=>$barcode ), 
													'MergeUpdate.status' => '1' , 
													'MergeUpdate.sorted_scanned' => '0' , 
													'MergeUpdate.scan_status' => '0')));
			}
			
			
			$orderid	=	explode('-',$barcode);
			$check_spain 	= 	$this->MergeUpdate->find('all', array('conditions' => array('OR' => array('MergeUpdate.order_id'=>$orderid[0] ), 
													'MergeUpdate.delevery_country' => 'Spain',
													)));
			if( count( $check_spain ) > 1 )
			{
				foreach($check_spain as $val) {
					if(date('Y-m-d',strtotime($val['MergeUpdate']['scan_date'])) == date('Y-m-d'))
					{
						echo "5";
						exit;
					}
				}
			}
			
			
			// Update tables and create manifest according to services those alloted already with manifest feild
			if( count($results) > 0 )
			{
				$splitskus	=	 explode(',', $results['0']['MergeUpdate']['sku']);
				foreach( $splitskus as $splitsku )
				{
					$products 	= 	explode('XS-', $splitsku);
					$productsku = 	'S-'.$products[1];
					$productcat =  $this->Product->find( 'first', array( 'conditions' => array( 'Product.product_sku' => $productsku ), 'fields'=> array( 'category_name' ) ) );
					$cat_name[] =  $productcat['Product']['category_name'];
				}
			
				$i = 0;
				foreach($results as $result)
				{
					$openOrderId 		= $result['MergeUpdate']['id'];
					$serviceName 		= $result['MergeUpdate']['service_name'];
					$serviceProvider 	= $result['MergeUpdate']['service_provider'];
					$serviceCode 		= $result['MergeUpdate']['provider_ref_code'];
					$country 			= $result['MergeUpdate']['delevery_country'];
					$serviceid 			= $result['MergeUpdate']['service_id'];

					// Update Service table and applied counter value
					$this->loadModel( 'ServiceCounter' );
					
					
					if($serviceProvider == 'DHL'){
					
						$getCounterValue = $this->ServiceCounter->find( 'first' , 
							array( 'conditions' => array('ServiceCounter.service_id' => $serviceid )
						) );
					
					}else{
						$getCounterValue = $this->ServiceCounter->find( 'first' , 
						array( 'conditions' => 
						array( 
						 //'ServiceCounter.service_name' => $serviceName,
						 //'ServiceCounter.service_provider' => $serviceProvider,
						 //'ServiceCounter.service_code' => $serviceCode,
						  'ServiceCounter.destination' => $country,
						  'ServiceCounter.service_id' => $serviceid          
						)
						) );
					}
					
					//Manage Counter Section
					$counter = 0;
					if( $getCounterValue['ServiceCounter']['counter'] > 0 ):
					$counter = $getCounterValue['ServiceCounter']['counter'];
					$counter = $counter + 1;
					else:
					$counter = $counter + 1;
					endif;
					
					// Manage Order Id's also
					$orderCommaSeperated = '';
					if( $getCounterValue['ServiceCounter']['order_ids'] != '' ):
					$orderCommaSeperated = $getCounterValue['ServiceCounter']['order_ids'];
					$orderCommaSeperated = $orderCommaSeperated.','.$openOrderId;
					else:
					$orderCommaSeperated = $openOrderId;
					endif;

					 // Now, Updating Services ...... 
					$this->request->data['ServiceCounter']['ServiceCounter']['id'] = $getCounterValue['ServiceCounter']['id'];      
					$this->request->data['ServiceCounter']['ServiceCounter']['counter'] = $counter;
					$this->request->data['ServiceCounter']['ServiceCounter']['order_ids'] = $orderCommaSeperated;         
					$this->ServiceCounter->saveAll( $this->request->data['ServiceCounter'] );
					 
					 // Now, Updating Open Order ...... 
					$this->request->data['MergeUpdate']['MergeUpdate']['id'] = $openOrderId;            
					$this->request->data['MergeUpdate']['MergeUpdate']['sorted_scanned'] = 1;         
					$this->request->data['MergeUpdate']['MergeUpdate']['scan_status'] = 1;  
					$this->request->data['MergeUpdate']['MergeUpdate']['scan_date'] = date('Y-m-d H:i:s'); 
					$this->request->data['MergeUpdate']['MergeUpdate']['scanned_user'] = $username;  
					$this->request->data['MergeUpdate']['MergeUpdate']['scanned_pc'] = $pcName;       
					$this->MergeUpdate->saveAll( $this->request->data['MergeUpdate'] );
					
					//one step more to store and calculate the soring items by user logged
					// Get Session User
				   $user_id = $this->Session->read('Auth.User.id');
				   
				   // Call Query
				   $paramOperator = array(
						'conditions' => array(
										'SortingoperatortimeCalculation.user_id' => $user_id,
										'SortingoperatortimeCalculation.today_date' => date( 'Y-m-d' )
						)
					);
				   
				   // Query Here
				   $this->loadModel('SortingoperatortimeCalculation');
				   $getOperator = $this->SortingoperatortimeCalculation->find( 'first', $paramOperator );
				   
				   //Update first out time
				   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['id'] = $getOperator['SortingoperatortimeCalculation']['id'];
				   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['user_id'] = $user_id;
				   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['out_time'] = date('Y-m-d G:i:s');
				   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['no_of_orders_sorted'] = $getOperator['SortingoperatortimeCalculation']['no_of_orders_sorted'] + 1;				   
				   $this->SortingoperatortimeCalculation->saveAll( $this->request->data['SortingoperatortimeCalculation'] ); 
				   
				   // Query Here
				   $getOperator = $this->SortingoperatortimeCalculation->find( 'first', $paramOperator ); 
				   
				   // Calculation
				   $logout = strtotime($getOperator['SortingoperatortimeCalculation']['out_time']);
				   $login  = strtotime($getOperator['SortingoperatortimeCalculation']['in_time']);
				   $diff   = $logout - $login;                               
				   $timeCalculate = round( $diff / 3600 ) ." hour ". round($diff/60)." minutes ".($diff%60)." seconds";
				   
				   // Yes, calculate the time but we need to check one more time is that today or not
				   $dayString = $getOperator['SortingoperatortimeCalculation']['in_time'];
				   $dayStringSub = substr($dayString, 0, 10);

				   $isToday = ( strtotime('now') >= strtotime($dayStringSub . " 00:00") 
																  && strtotime('now') <  strtotime($dayStringSub . " 23:59") );
				   
					// Today
				   if( $isToday == 1 ):
												
						// Update a row again and again
						//Update calculate time
					   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['id'] = $getOperator['SortingoperatortimeCalculation']['id'];
					   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['user_id'] = $user_id;
					   $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['total_time'] = $timeCalculate;
					   //$this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['no_of_orders_sorted'] = $glbalSortingCounter;
					   $this->SortingoperatortimeCalculation->saveAll( $this->request->data['SortingoperatortimeCalculation'] ); 
					else:
					
						// Insert new row, If today is not
						//Insert calculate time                                      
					    $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['user_id'] = $user_id;
					    $this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['total_time'] = $timeCalculate;
					    //$this->request->data['SortingoperatortimeCalculation']['SortingoperatortimeCalculation']['no_of_orders_sorted'] = $glbalSortingCounter;
					    $this->SortingoperatortimeCalculation->saveAll( $this->request->data['SortingoperatortimeCalculation'] ); 
												
				   endif; 
				   
					 // Manifest Creation
					 //$this->createManifestAccordingToServiceIfYes();
					 
					$i++;
				} 
				
				//$data = $this->SpecialOffer->find('all',array('conditions' => array('SpecialOffer.user_id' => $userId ,'FIND_IN_SET(\''. $storeId .'\',SpecialOffer.stores1)')));
				$getcombationids		=	$this->SortingStation->find('first', array('conditions' => array( 'FIND_IN_SET(\''. $serviceid .'\',SortingStation.service_id)' ) ) );
				$combinationid			=	$getcombationids['SortingStation']['id'].'---'.$getcombationids['SortingStation']['color'];//'---"'
				if(in_array( 'Alkaline Battery' , $cat_name ) && $serviceProvider == 'Jersey Post' && $country == 'Spain')
				{	$cate = 'Alkaline Battery'; }
				else{ $cate = ''; }
				
				$strRep = str_replace(')','-',str_replace('(','-',str_replace('<','-',str_replace(' ','-',$combinationid))));
				$strFormat = preg_replace('/[^a-zA-Z0-9\']/', '-', $strRep);                
				$fullformat = $strFormat.'####'.$cate; 
			return strtolower($fullformat);exit;
			}
			else
			{
				//Check scan status
				$results = json_decode(json_encode($this->MergeUpdate->find('all', 
							array('conditions' => array('MergeUpdate.product_order_id_identify'=>$barcode, 
														'MergeUpdate.status' => '1' )))),0);
														
				if( count($results) > 0 )
				{
					
					if( $results[0]->MergeUpdate->scan_status == 1 )
					{
						if(in_array( 'Alkaline Battery' , $cat_name ) && $serviceProvider == 'Jersey Post' && $country == 'Spain')
						{
							echo "scannedbattery"; 
							unset( $cat_name );
							exit;
						} else {
							echo "scanned"; exit; 
							}
					}
				}
				else
				{
					//Response Blank         
					echo "none"; exit;	
				}				
			}
		} 
		
		
		public function getScannedLocation()
		{
			
			$this->autoRender = false;
			$this->layout = '';

			$this->loadModel('MergeUpdate');
			$this->loadModel('SortingStation');
			$barcode  =  $this->request->data['barcode']; 
			
			$results = $this->MergeUpdate->find('all', 
			array('conditions' => array('MergeUpdate.product_order_id_identify'=>$barcode, 
										'MergeUpdate.status' => '1' , 
										'MergeUpdate.sorted_scanned' => '1' , 
										'MergeUpdate.scan_status' => '1')));
	
			foreach($results as $result)
			{
				$openOrderId = $result['MergeUpdate']['id'];
				$serviceName = $result['MergeUpdate']['service_name'];
				$serviceProvider = $result['MergeUpdate']['service_provider'];
				$serviceCode = $result['MergeUpdate']['provider_ref_code'];
				$country = $result['MergeUpdate']['delevery_country'];
				$serviceid = $result['MergeUpdate']['service_id'];
				 
			}
		$results = $this->MergeUpdate->find('first',array('conditions'=>array('MergeUpdate.product_order_id_identify'=>$barcode,'MergeUpdate.status'=>'1'),
								 												'fields' => array( 'scanned_user','scanned_pc')));
								$user	=	$results['MergeUpdate']['scanned_user'];
								$pc		=	$results['MergeUpdate']['scanned_pc'];
		$getcombationids		=	$this->SortingStation->find('first', array('conditions' => array( 'FIND_IN_SET(\''. $serviceid .'\',SortingStation.service_id)' ) ) );
		//$combinationid			=	$getcombationids['SortingStation']['id'].'---'.$getcombationids['SortingStation']['color'];
		$combinationid			=	$getcombationids['SortingStation']['id'].'---'.$getcombationids['SortingStation']['color'].'---'.$user.'---'.$pc.'---'.$barcode;
		
		$strRep = str_replace(')','-',str_replace('(','-',str_replace('<','-',str_replace(' ','-',$combinationid))));
		$strFormat = preg_replace('/[^a-zA-Z0-9\']/', '-', $strRep);                	
		return strtolower($strFormat);exit;
		}
   
   		public function UpdateCombination( $id = null )
		{
			$this->layout = "index";
			$this->loadModel('PostalServiceDesc');
			$this->loadModel('Location');
			$this->loadModel('PostalProvider');
			$this->loadModel('SortingStation');
			
			$getcomb			=	$this->SortingStation->find('first', array( 'conditions' => array( 'id' => $id ) ) );
			$ids	=	explode(',', $getcomb['SortingStation']['service_id']);
			if(count($ids) == 1){
					$postalservices		=	$this->PostalServiceDesc->find('all', array( 'conditions' => array( 'PostalServiceDesc.id' => $ids[0] ) ) );
			} else {
					$postalservices		=	$this->PostalServiceDesc->find('all', array( 'conditions' => array( 'PostalServiceDesc.id IN' => $ids ) ) );
			}
			
			foreach($postalservices as $val)
			{
				$servicedata[$val['PostalServiceDesc']['id']] =  $val['PostalServiceDesc']['provider_ref_code'].' [ '.$val['Location']['county_name'].' ] ';
			}
			$locationList		=	$this->Location->find('list', array( 'fields' => array( 'Location.county_name' ) ));
			$postalproviders 	=   $this->PostalProvider->find('list', array( 'fields' => array( 'PostalProvider.provider_name' ) ) );
			
			
			
			$this->set('data', $getcomb);
			$this->set('locationList', $locationList);
			$this->set('postalservices', $servicedata);
			$this->set('postalproviders', $postalproviders);
		}
		
		public function updategroupinfo()
   		{
   		$this->layout = "";
		$this->autoRender = false;
		$this->loadModel('SortingStation');
		$this->loadModel('PostalServiceDesc');
	
		$combid		=	$this->request->data['id'];
		$servicepro	=	$this->request->data['postalprovideraval'];
		$countryid	=	implode(',', $this->request->data['countryandservice']);
		$serviceid	=	implode(',', $this->request->data['postalservice']);
		$boxname	=	$this->request->data['serviceboxname'];
		$color		=	$this->request->data['color'];
		$provname	=	($this->request->data['postalprovidname'] == 'Belgium Post') ? 'PostNL' : $this->request->data['postalprovidname'];
		$ids		=	explode(',', $serviceid);
		foreach($ids as $id)
		{	
			$getids	=	$this->SortingStation->find('all', array( 'fields' => array('service_id') ) );
			foreach( $getids as $getid )
			{
				$sids	=	$getid['SortingStation']['service_id'];
				$sis	=	explode(',',$sids);
				foreach($sis as $si)
				{
					if($si == $id)
					{
						$postalservices	=	$this->PostalServiceDesc->find('first',array('conditions'=> array('PostalServiceDesc.id'=> $id) ) );
						$servicedata 	=	$postalservices['PostalServiceDesc']['provider_ref_code'].' [ '. $postalservices['Location']['county_name'].' ] ';
						echo json_encode(array( 'status' => '4', 'service' => $servicedata ) );
						exit;
					}
				}
			}
			
		}
		$checkbox = $this->SortingStation->find('first', array( 'conditions' => array('group_box' => $boxname, 'id !=' => $combid ) ) );
		if(count($checkbox) == 1)
		{
			echo json_encode(array( 'status' => '3', 'service' => '' ) );
			exit;		
		}
		$checkgroup = $this->SortingStation->find('first', array( 'conditions' => array( 
																		'postal_provider_id' => $servicepro,
																		'group_box' => $boxname,
																		'country' => $countryid,
																		'service_id' => $serviceid
																		 ) ) );
		if(count($checkgroup) == 1)
		{
			echo json_encode(array( 'status' => '2', 'service' => '' ) );
			exit;
		} else {
			$serdata['id'] 						= $combid;
			$serdata['postal_provider_id'] 		= $servicepro;
			$serdata['group_box'] 				= $boxname;
			$serdata['country'] 				= $countryid;
			$serdata['service_id'] 				= $serviceid;
			$serdata['postal_provider_name'] 	= $provname;
			$serdata['color'] 					= $color;
			$this->SortingStation->saveAll( $serdata );
			echo json_encode(array( 'status' => '1', 'service' => '' ) );
			exit;
		}
   }
   
   public function updatecombinationposition()
   {
   		$this->loadModel('SortingStation');
		$id				=	$this->request->data['id'];
		$newposition	=	$this->request->data['newposition'];
		$section		=	$this->request->data['section'];
		$cpos			=	$this->request->data['cpos'];
		
		$currentpos		=	$this->SortingStation->find('first', array( 'conditions' => array( 'position' => $cpos, 'section' => $section, ) ) );
		$newposbdata	=	$this->SortingStation->find('first', array( 'conditions' => array( 'position' => $newposition, 'section' => $section, ) ) );
		
		$oldpos['id'] 		= 	$newposbdata['SortingStation']['id'];
		$oldpos['position'] = 	$currentpos['SortingStation']['position'];
		$oldpos['section'] 	= 	$currentpos['SortingStation']['section'];
		
		$newpos['id'] 		= 	$currentpos['SortingStation']['id'];
		$newpos['position'] = 	$newposbdata['SortingStation']['position'];
		$newpos['section'] = 	$newposbdata['SortingStation']['section'];
	
		$this->SortingStation->saveAll($newpos);
		
		$this->SortingStation->saveAll($oldpos);
		echo "1";
		exit;
		
   }
   
   public function updatecombinationposition_300418()
   {
   		$this->loadModel('SortingStation');
		$id				=	$this->request->data['id'];
		$newposition	=	$this->request->data['newposition'];
		$newposbdata	=	$this->SortingStation->find('first', array( 'conditions' => array( 'position' => $newposition ) ) );
		
		
		$oldposbdata		=	$this->SortingStation->find('first', array( 'conditions' => array( 'id' => $id ) ) );
		$oldpos['id'] 		= 	$oldposbdata['SortingStation']['id'];
		$oldpos['position'] = 	$newposbdata['SortingStation']['position'];
		
		$newpos['id'] 		= 	$newposbdata['SortingStation']['id'];
		$newpos['position'] = 	$oldposbdata['SortingStation']['position'];
		
		$this->SortingStation->saveAll($newpos);
		
		$this->SortingStation->saveAll($oldpos);
		echo "1";
		exit;
		
   }
   
   public function deleteCombination( $id = null )
   {
   		$this->loadModel('SortingStation');
		$resultMergeUpdate  	= 	$this->SortingStation->delete( $id );
   		$this->Session->setflash(  "Combination Deleted Successful.",  'flash_success' );
		$this->redirect(array('controller'=>'Sortingstations','action' => 'showAllGroup'));
   }
   
   
   public function getAllcountry()
   {
   }
   
   public function batchOrderPutBack()
   {
   		$this->layout = "";
		$this->autoRender = false;
		$this->loadModel('MergeUpdate');
		$spli_orderids	=	explode(',',$this->request->data['ordid']);
		foreach( $spli_orderids as $spli_orderid )
		{
			$result = $this->MergeUpdate->updateAll( array('MergeUpdate.batch_num' => 'NULL' ), array('MergeUpdate.product_order_id_identify' => $spli_orderid ) );
		}
		if($result){
			echo "1";
		} else {
			echo "2";
		}
		exit;
   }
   
	
}
?>
