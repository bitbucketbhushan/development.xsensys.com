<?php
//error_reporting(0); 
class FbaInventoryController extends AppController
{  
    
    var $name = "FbaInventory";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('fetchOrders','fetchOrderItems'));		
			
    }
 	
  	public function index() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","Europe FBA Orders" );	
		
		$this->loadModel('FbaUploadInventory'); 
		$this->loadModel('FbaUploadStockFile'); 
		 
		$conditions = [];
		if(isset($_REQUEST['searchkey']) && $_REQUEST['searchkey'] !=''){
			$conditions[] = array('OR' =>
						 array(
						 'FbaUploadInventory.sku' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.alert' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.country' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.fnsku' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.seller_sku' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.asin' => trim($_REQUEST['searchkey'])
						 ) 
					 ) ;
		}
		if(isset($_REQUEST['store']) && $_REQUEST['store'] !=''){
			$conditions[] = array('store'=> $_REQUEST['store']);
		}
		if(isset($_REQUEST['start_input']) && $_REQUEST['start_input'] !=''){
 			$sdate = date('Y-m-d',strtotime($this->request->query['start_input']));
			$edate = date('Y-m-d',strtotime($this->request->query['end_input']));
 			$conditions[] = array('stock_date BETWEEN ? AND ?'=>[$sdate,$edate.' 23:59:59']);
		}
		 //pr($conditions);
		 
		$this->paginate = array('conditions'=>$conditions,'order'=>'uploaded_date DESC','limit' => 100 );
		$all_orders = $this->paginate('FbaUploadStockFile');
		 
		$this->set( 'all_orders', $all_orders );	
		
	} 
	
	public function StockSkus($file_id = 0) 
    {	
		$this->layout = "index"; 	
		$this->set( "title","Europe FBA Orders" );	
		
		$this->loadModel('FbaUploadInventory'); 
		 
		$conditions[] = array('file_id'=> $file_id);
		if(isset($_REQUEST['searchkey']) && $_REQUEST['searchkey'] !=''){
			$conditions[] = array('OR' =>
						 array(
						 'FbaUploadInventory.sku' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.alert' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.country' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.fnsku' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.seller_sku' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.asin' => trim($_REQUEST['searchkey'])
						 ) 
					 ) ;
		}
		if(isset($_REQUEST['store']) && $_REQUEST['store'] !=''){
			$conditions[] = array('store'=> $_REQUEST['store']);
		}
		if(isset($_REQUEST['start_input']) && $_REQUEST['start_input'] !=''){
 			$sdate = date('Y-m-d',strtotime($this->request->query['start_input']));
			$edate = date('Y-m-d',strtotime($this->request->query['end_input']));
 			$conditions[] = array('added_date BETWEEN ? AND ?'=>[$sdate,$edate.' 23:59:59']);
		}
		 //pr($conditions);
		 
		$this->paginate = array('conditions'=>$conditions,'order'=>'FbaUploadInventory.added_date DESC','limit' => 100 );
		$all_orders = $this->paginate('FbaUploadInventory');
		 
		$this->set( 'all_orders', $all_orders );	
		
	}
	public function DeleteFile($id = '') 
	{
		$this->loadModel('FbaUploadStockFile');	 
		$this->loadModel('FbaUploadInventory');	 
		$file = $this->FbaUploadStockFile->find('first', array('conditions'=>array('id'=>$id)));
		if(count($file) > 0){ 
			$file_name = WWW_ROOT. 'fba_inventory'.DS.$file['FbaUploadStockFile']['file_name'];
			@unlink($file_name);
			
			$file_name = 'fba_inventory_DeleteFile_'.date('Ymd').'.log'; 
			file_put_contents(WWW_ROOT.'logs/'.$file_name,$file['FbaUploadStockFile']['file_name']."\t".date('Y-m-d H:i:s')."\t".$this->Session->read('Auth.User.username')."\r\n");
 		}

		$sql = "DELETE FROM `fba_upload_stock_files` WHERE `id` = {$id}"; 
		$this->FbaUploadStockFile->query($sql);
		$sql = "DELETE FROM `fba_upload_inventories` WHERE `file_id` = {$id}"; 
		$this->FbaUploadInventory->query($sql);
		$this->Session->setFlash("File and file data deleted sucessfully.", 'flash_danger');
		$this->redirect($this->referer());
	}
	
 	public function DownloadFile($id = 0) 
	{	
		$this->layout = "index"; 	
  		$this->loadModel('FbaUploadStockFile');	 
 		$file = $this->FbaUploadStockFile->find('first', array('conditions'=>array('id'=>$id)));
   		if(count($file) > 0){ 
			$file_name = WWW_ROOT. 'fba_inventory'.DS.$file['FbaUploadStockFile']['file_name'];
 			header('Content-Description: File Transfer');
			header('Content-Type: text/csv');
			header('Content-Disposition: attachment; filename='.basename($file_name));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file_name));   
			readfile($file_name); 
		}
		exit;
	
	}
	
	public function ChangeStatus($id = 0 , $status = 0)
	{	
		$this->layout = "index"; 	
  		$this->loadModel('FbaUploadStockFile');	 
		$this->loadModel('FbaUploadInventory');	 
		$sql = "UPDATE `fba_upload_stock_files` SET `status` = '".$status."'  WHERE `id` = {$id}"; 
		$this->FbaUploadStockFile->query($sql);
		$sql = "UPDATE `fba_upload_inventories` SET `status` = '".$status."' WHERE `file_id` = {$id}"; 
		$this->FbaUploadInventory->query($sql);
		$this->Session->setFlash("File and file data status change sucessfully.", 'flash_danger'); 
		$this->redirect($this->referer());
 		exit;
 	}
	
	public function ChangeInventoryStatus($id = 0 , $status = 0)
	{	
		$this->layout = "index"; 	
 		$this->loadModel('FbaUploadInventory');	 
 		$sql = "UPDATE `fba_upload_inventories` SET `status` = '".$status."' WHERE `id` = {$id}"; 
		$this->FbaUploadInventory->query($sql);
		$this->Session->setFlash("SKU status change sucessfully.", 'flash_danger'); 
		$this->redirect($this->referer());
   	
		exit;
	
	}
	public function download() 
	{	
		$this->layout = "index"; 	
  		$this->loadModel('FbaUploadInventory');	 
  		 
		ob_clean(); 
		$sep = ","; 		 
  		$sdate = date('Y-m-d',strtotime($this->request->query['start_input']));
		$edate = date('Y-m-d',strtotime($this->request->query['end_input']));
		
		$file_name = 'fba_stock_'.$sdate.$edate.'_'.$this->Session->read('Auth.User.username').'.csv'; 
		file_put_contents(WWW_ROOT.'logs/'.$file_name,'product_name'.$sep.'sku'.$sep.'seller_sku'.$sep.'fnsku'.$sep.'asin'.$sep.'condition'.$sep.'currency'.$sep.'price'.$sep.'sales_last_30_days'.$sep.'units_sold_last_30_days'.$sep.'total_units'.$sep.'inbound'.$sep.'available'.$sep.'fc_transfer'.$sep.'fc_processing'.$sep.'customer_order'.$sep.'unfulfillable'.$sep.'working'.$sep.'shipped'.$sep.'receiving'.$sep.'country'.$sep.'alert'.$sep.'rec_replenishment_qty'.$sep.'rec_ship_date'.$sep.'status'."\r\n");
		if($this->request->query['store'] != ''){  
			$products = $this->FbaUploadInventory->find('all', array('conditions'=>array('added_date BETWEEN ? AND ?'=>[$sdate,$edate.' 23:59:59'],'store'=>$this->request->query['store'])));
	 	}else{
			$products = $this->FbaUploadInventory->find('all', array('conditions'=>array('added_date BETWEEN ? AND ?'=>[$sdate,$edate.' 23:59:59'])));
		} 
  		if(count($products) > 0){
			foreach($products as $pro){
				file_put_contents(WWW_ROOT.'logs/'.$file_name,
				'"'.$pro['FbaUploadInventory']['product_name'].'"'.$sep.
				$pro['FbaUploadInventory']['sku'].$sep.
				$pro['FbaUploadInventory']['seller_sku'].$sep.
				$pro['FbaUploadInventory']['fnsku'].$sep.
				$pro['FbaUploadInventory']['asin'].$sep.
				$pro['FbaUploadInventory']['condition'].$sep.
				$pro['FbaUploadInventory']['currency'].$sep.				
				$pro['FbaUploadInventory']['price'].$sep.
				$pro['FbaUploadInventory']['sales_last_30_days'].$sep. 
				$pro['FbaUploadInventory']['units_sold_last_30_days'].$sep.  
				$pro['FbaUploadInventory']['total_units'].$sep.  
				$pro['FbaUploadInventory']['inbound'].$sep.  
				$pro['FbaUploadInventory']['available'].$sep. 
				$pro['FbaUploadInventory']['fc_transfer'].$sep.  
				$pro['FbaUploadInventory']['fc_processing'].$sep.
				$pro['FbaUploadInventory']['customer_order'].$sep.  
 				$pro['FbaUploadInventory']['unfulfillable'].$sep.
				$pro['FbaUploadInventory']['working'].$sep.   
				$pro['FbaUploadInventory']['shipped'].$sep.  
				$pro['FbaUploadInventory']['receiving'].$sep. 
				$pro['FbaUploadInventory']['country'].$sep.  
				$pro['FbaUploadInventory']['alert'].$sep.    
				$pro['FbaUploadInventory']['rec_replenishment_qty'].$sep. 
				$pro['FbaUploadInventory']['rec_ship_date'].$sep.  
				$pro['FbaUploadInventory']['status'].$sep 
					
				."\r\n", FILE_APPEND | LOCK_EX);	
			}
		} 	 
		 
		$result =  WWW_ROOT.'logs/'.$file_name; 
		header('Content-Description: File Transfer');
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename='.basename($result));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($result));   
		readfile($result); 
		exit;
	
	}
	
	public function uploadSave() 
    {	
 		
		$this->layout = 'index';		 
		$this->loadModel('FbaUploadStockFile');	
		$this->loadModel('FbaUploadInventory');	 
 		 
		$filetemppath =	$this->data['inventory']['uploadfile']['tmp_name']; 
		$pathinfo     = pathinfo($this->data['inventory']['uploadfile']['name']);
             
		$fileNameWithoutExt = $pathinfo['filename'];
		$fileExtension = strtolower($pathinfo['extension']);
		
		$filename = $this->data['inventory']['uploadfile']['name'];
		
		 
		//$filename = $fileNameWithoutExt.'_'.date('Ymd_his').'_'.$this->Session->read('Auth.User.username').".".$fileExtension;		
		$upload_file = WWW_ROOT. 'fba_inventory'.DS.$filename ; 
		if(empty($this->data['inventory']['store'])){
			$this->Session->setFlash("Please select store.", 'flash_danger');
 		 	$this->redirect($this->referer());
			exit;
		}
		if(empty($this->data['inventory']['uploadfile']['name'])){
			$this->Session->setFlash("Please select file.", 'flash_danger');
 		 	$this->redirect($this->referer());
			exit;
		}	
		
		if(file_exists($upload_file)){
			$this->Session->setFlash($filename ." is already uploaded.", 'flash_danger');
 		 	$this->redirect($this->referer());
			exit;
		}
			
 		
		if($fileExtension == 'csv')
		{
 		
			$fba_files = $this->FbaUploadStockFile->find('first', array('conditions'=>array('file_name' =>$filename,'store' =>$this->data['inventory']['store'],'stock_date' =>$this->data['stock_date'])) );
			
			if(count($fba_files) > 0){
				$this->Session->setFlash($filename ." is already uploaded for ".$this->data['stock_date'], 'flash_danger');
				$this->redirect($this->referer());
				exit;
			}
			
			$filedata = [];
			$filedata['store'] 		= $this->data['inventory']['store'];
			$filedata['file_name'] 	= $filename;
			$filedata['stock_date'] = date('Y-m-d', strtotime($this->data['stock_date'])); 
			$filedata['uploaded_date'] =	date('Y-m-d H:i:s');
			$filedata['uploaded_by']  = $this->Session->read('Auth.User.username');
			$this->FbaUploadStockFile->saveAll( $filedata );
			$file_id	 = $this->FbaUploadStockFile->getLastInsertId(); 
				
 			move_uploaded_file($filetemppath,$upload_file); 
			
			$sql = "UPDATE `fba_upload_inventories` SET `status` = '1' WHERE `store` = '". $this->data['inventory']['store']."'"; 
			$this->FbaUploadInventory->query($sql);
			
			$row = 0; 
			$handle = fopen($upload_file, "r");
 			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			  
				$row++;			
 				if($row > 1){
					$savedata = array();					
					$master = $this->getSkuMapping($data[3]); 
					$savedata['file_id'] 	= $file_id;
					$savedata['store'] 		= $this->data['inventory']['store'];
					$savedata['sku'] 		= $master;
					$savedata['country'] 	= $data[0]; 
					$savedata['product_name']= addslashes($data[1]);
					$savedata['fnsku'] 		= $data[2];
					$savedata['seller_sku'] = $data[3];
					$savedata['asin'] 		= $data[4];
					$savedata['condition'] 	= $data[5];
					$savedata['currency']	= $data[8];
					$savedata['price'] 		= $data[9];
					$savedata['sales_last_30_days'] = $data[10];
					$savedata['units_sold_last_30_days']= $data[11];
					$savedata['total_units']= $data[12];
					$savedata['inbound'] 	= $data[13];
					$savedata['available'] 		= $data[14];
					$savedata['fc_transfer']	= $data[15];
					$savedata['fc_processing'] 	= $data[16];
					$savedata['customer_order'] = $data[17];
					$savedata['unfulfillable']  = $data[18] ;
					$savedata['working']		= $data[19] ;
					$savedata['shipped']		= $data[20] ;
					$savedata['receiving'] 		= $data[21];					
					$savedata['alert'] 			= $data[25];
					$savedata['rec_replenishment_qty'] = $data[26];
					if(!empty($data[27])){
						$savedata['rec_ship_date'] 	= date('Y-m-d',strtotime($data[27])); 
					}
					$savedata['file_name'] 		= $this->data['inventory']['uploadfile']['name'];
					$savedata['upload_by'] 		= $this->Session->read('Auth.User.username');
					/*
					 echo "<pre>";
					 print_r($savedata);
					 echo "</pre>";  exit; */            
					/*$fba_order = $this->FbaUploadInventory->find('first', array('conditions'=>array('file_name' =>$this->data['inventory']['uploadfile']['name'])) );
						
					if(count($fba_order) > 0){
						$savedata['id'] = $fba_order['FbaUploadInventory']['id'];	
						$this->FbaUploadInventory->saveAll( $savedata );					 
						 
					}else{*/
						$savedata['added_date'] = date('Y-m-d H:i:s');							
						$this->FbaUploadInventory->saveAll( $savedata );
					//}    
				}					
			}   		
			fclose($handle);
			
			if($row > 0){
				$this->Session->setFlash($row . " inventory uploaded.", 'flash_success');
			} 
			
		 }
		 else{
			 $this->Session->setFlash("Invalid file format, Please upload CSV file.", 'flash_danger');
		 }
		 
		 $this->redirect($this->referer());
 		 
 	}
	
 	public function uploadSave333() 
    {	
 		
		$this->layout = 'index';		 
		$this->loadModel('FbaUploadInventory');	 
 		 
		$filetemppath =	$this->data['inventory']['uploadfile']['tmp_name']; 
		$pathinfo     = pathinfo($this->data['inventory']['uploadfile']['name']);
             
		$fileNameWithoutExt = $pathinfo['filename'];
		$fileExtension = strtolower($pathinfo['extension']);
		
		$filename = $this->data['inventory']['uploadfile']['name'];
		//$filename = $fileNameWithoutExt.'_'.date('Ymd_his').'_'.$this->Session->read('Auth.User.username').".".$fileExtension;		
		$upload_file = WWW_ROOT. 'fba_inventory'.DS.$filename ; 
		if(empty($this->data['inventory']['store'])){
			$this->Session->setFlash("Please select store.", 'flash_danger');
 		 	$this->redirect($this->referer());
			exit;
		}
		if(empty($this->data['inventory']['uploadfile']['name'])){
			$this->Session->setFlash("Please select file.", 'flash_danger');
 		 	$this->redirect($this->referer());
			exit;
		}	
		
		if(file_exists($upload_file)){
			$this->Session->setFlash($filename ." is already uploaded.", 'flash_danger');
 		 	$this->redirect($this->referer());
			exit;
		}
			
 		
		if($fileExtension == 'csv')
		{
 			move_uploaded_file($filetemppath,$upload_file); 
			
			$sql = "UPDATE `fba_upload_inventories` SET `status` = '1' WHERE `store` = '". $this->data['inventory']['store']."'"; 
			$this->FbaUploadInventory->query($sql);
			
			$row = 0; 
			$handle = fopen($upload_file, "r");
 			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			  
				$row++;			
 				if($row > 1){
					$savedata = array();					
					$master = $this->getSkuMapping($data[3]); 
					$savedata['store'] 		= $this->data['inventory']['store'];
					$savedata['sku'] 		= $master;
					$savedata['country'] 	= $data[0]; 
					$savedata['product_name']= addslashes($data[1]);
					$savedata['fnsku'] 		= $data[2];
					$savedata['seller_sku'] = $data[3];
					$savedata['asin'] 		= $data[4];
					$savedata['condition'] 	= $data[5];
					$savedata['currency']	= $data[8];
					$savedata['price'] 		= $data[9];
					$savedata['sales_last_30_days'] = $data[10];
					$savedata['units_sold_last_30_days']= $data[11];
					$savedata['total_units']= $data[12];
					$savedata['inbound'] 	= $data[13];
					$savedata['available'] 		= $data[14];
					$savedata['fc_transfer']	= $data[15];
					$savedata['fc_processing'] 	= $data[16];
					$savedata['customer_order'] = $data[17];
					$savedata['unfulfillable']  = $data[18] ;
					$savedata['working']		= $data[19] ;
					$savedata['shipped']		= $data[20] ;
					$savedata['receiving'] 		= $data[21];					
					$savedata['alert'] 			= $data[25];
					$savedata['rec_replenishment_qty'] = $data[26];
					if(!empty($data[27])){
						$savedata['rec_ship_date'] 	= date('Y-m-d',strtotime($data[27])); 
					}
					$savedata['file_name'] 		= $this->data['inventory']['uploadfile']['name'];
					$savedata['upload_by'] 		= $this->Session->read('Auth.User.username');
					/*
					 echo "<pre>";
					 print_r($savedata);
					 echo "</pre>";  exit; */            
					/*$fba_order = $this->FbaUploadInventory->find('first', array('conditions'=>array('file_name' =>$this->data['inventory']['uploadfile']['name'])) );
						
					if(count($fba_order) > 0){
						$savedata['id'] = $fba_order['FbaUploadInventory']['id'];	
						$this->FbaUploadInventory->saveAll( $savedata );					 
						 
					}else{*/
						$savedata['added_date'] = date('Y-m-d H:i:s');							
						$this->FbaUploadInventory->saveAll( $savedata );
					//}    
				}					
			}   		
			fclose($handle);
			
			if($row > 0){
				$this->Session->setFlash($row . " inventory uploaded.", 'flash_success');
			} 
			
		 }
		 else{
			 $this->Session->setFlash("Invalid file format, Please upload CSV file.", 'flash_danger');
		 }
		 
		 $this->redirect($this->referer());
 		 
 	}
	
	public function getSkuMapping($seller_sku = null)
	{		
		$this->loadModel( 'Skumapping' );	
		$_items = $this->Skumapping->find('first', array('conditions'=>array('Skumapping.channel_sku'=>$seller_sku),'fields'=>['sku']));
		if(count($_items) > 0){
			return  $_items['Skumapping']['sku'];//array('sku'=>$_items['Skumapping']['sku'],'barcode'=>$_items['Skumapping']['barcode'],'channel_name'=>$_items['Skumapping']['channel_name']);
		}else{
			return '';
		}
	}
	
	public function DownloadStock3() 
	{	
		$this->layout = "index"; 	
   		$this->loadModel('FbaUploadStockFile');	 
		$this->loadModel('FbaUploadInventory');	 
		$file = $this->FbaUploadStockFile->find('all', array('conditions'=>array('stock_date'=>$this->request->query['date'])));
		$file_ids = [];
		if(count($file) > 0){ 
			foreach($file as $v){
				$file_ids[] = $v['FbaUploadStockFile']['id'];
			}
		}
		 
		ob_clean(); 
		$sep = ","; 		 
  		
		$file_name = 'fba_stock_'.$this->request->query['date'].'_'.$this->Session->read('Auth.User.username').'.csv'; 
		file_put_contents(WWW_ROOT.'logs/'.$file_name,'product_name'.$sep.'sku'.$sep.'seller_sku'.$sep.'fnsku'.$sep.'asin'.$sep.'condition'.$sep.'currency'.$sep.'price'.$sep.'sales_last_30_days'.$sep.'units_sold_last_30_days'.$sep.'total_units'.$sep.'inbound'.$sep.'available'.$sep.'fc_transfer'.$sep.'fc_processing'.$sep.'customer_order'.$sep.'unfulfillable'.$sep.'working'.$sep.'shipped'.$sep.'receiving'.$sep.'country'.$sep.'alert'.$sep.'rec_replenishment_qty'.$sep.'rec_ship_date'.$sep.'status'."\r\n");
		 
		$products = $this->FbaUploadInventory->find('all', array('conditions'=>array('file_id'=>$file_ids)));
	 	$stock_data = [];
  		if(count($products) > 0){
			foreach($products as $pro){ 
				if($pro['FbaUploadInventory']['sku'] != ''){
					$stock_data[$pro['FbaUploadInventory']['sku']]['total_units'][] = $pro['FbaUploadInventory']['total_units'];
					$stock_data[$pro['FbaUploadInventory']['sku']]['inbound'][] = $pro['FbaUploadInventory']['inbound'];
					$stock_data[$pro['FbaUploadInventory']['sku']]['available'][] = $pro['FbaUploadInventory']['available'];
					$stock_data[$pro['FbaUploadInventory']['sku']]['fc_transfer'][] = $pro['FbaUploadInventory']['fc_transfer'];
					$stock_data[$pro['FbaUploadInventory']['sku']]['fc_processing'][] = $pro['FbaUploadInventory']['fc_processing'];
					$stock_data[$pro['FbaUploadInventory']['sku']]['customer_order'][] = $pro['FbaUploadInventory']['customer_order'];
					$stock_data[$pro['FbaUploadInventory']['sku']]['unfulfillable'][] = $pro['FbaUploadInventory']['unfulfillable'];
					
					$stock_data[$pro['FbaUploadInventory']['sku']]['working'][] = $pro['FbaUploadInventory']['working'];
					$stock_data[$pro['FbaUploadInventory']['sku']]['shipped'][] = $pro['FbaUploadInventory']['shipped'];
					$stock_data[$pro['FbaUploadInventory']['sku']]['receiving'][] = $pro['FbaUploadInventory']['receiving'];
				}
		 	}
		} 	 
		$stock = [];
		foreach(array_keys($stock_data) as $k => $sku){
			$stock[$sku]['total_units'] = array_sum($stock_data[$sku]['total_units']);
			$stock[$sku]['inbound'] = array_sum($stock_data[$sku]['inbound']);
 			$stock[$sku]['available'] = array_sum($stock_data[$sku]['available']);
			$stock[$sku]['fc_transfer'] = array_sum($stock_data[$sku]['fc_transfer']);
			$stock[$sku]['fc_processing'] = array_sum($stock_data[$sku]['fc_processing']);
			$stock[$sku]['customer_order'] = array_sum($stock_data[$sku]['customer_order']);
			$stock[$sku]['unfulfillable'] = array_sum($stock_data[$sku]['unfulfillable']);
			$stock[$sku]['working'] = array_sum($stock_data[$sku]['working']);
			$stock[$sku]['shipped'] = array_sum($stock_data[$sku]['shipped']);
			$stock[$sku]['receiving'] = array_sum($stock_data[$sku]['receiving']);
			 
					
		}pr($stock);
		
		
		 exit;
		$result =  WWW_ROOT.'logs/'.$file_name; 
		header('Content-Description: File Transfer');
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename='.basename($result));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($result));   
		readfile($result); 
		exit;
	
	}
	
	public function DownloadStock() 
	{	
		$this->layout = "index"; 	
   		$this->loadModel('FbaUploadStockFile');	 
		$this->loadModel('FbaUploadInventory');	 
		$file = $this->FbaUploadStockFile->find('all', array('conditions'=>array('stock_date'=>$this->request->query['date'])));
		$file_ids = [];
		if(count($file) > 0){ 
			foreach($file as $v){
				$file_ids[] = $v['FbaUploadStockFile']['id'];
			}
		}
		 
		ob_clean(); 
		$sep = ","; 		 
  		
		$file_name = 'fba_stock_'.$this->request->query['date'].'_'.$this->Session->read('Auth.User.username').'.csv'; 
		file_put_contents(WWW_ROOT.'logs/'.$file_name,'product_name'.$sep.'sku'.$sep.'seller_sku'.$sep.'fnsku'.$sep.'asin'.$sep.'condition'.$sep.'currency'.$sep.'price'.$sep.'sales_last_30_days'.$sep.'units_sold_last_30_days'.$sep.'total_units'.$sep.'inbound'.$sep.'available'.$sep.'fc_transfer'.$sep.'fc_processing'.$sep.'customer_order'.$sep.'unfulfillable'.$sep.'working'.$sep.'shipped'.$sep.'receiving'.$sep.'country'.$sep.'alert'.$sep.'rec_replenishment_qty'.$sep.'rec_ship_date'.$sep.'status'."\r\n");
		 
		$products = $this->FbaUploadInventory->find('all', array('conditions'=>array('file_id'=>$file_ids)));
	 	 
  		if(count($products) > 0){
			foreach($products as $pro){
				file_put_contents(WWW_ROOT.'logs/'.$file_name,
				'"'.$pro['FbaUploadInventory']['product_name'].'"'.$sep.
				$pro['FbaUploadInventory']['sku'].$sep.
				$pro['FbaUploadInventory']['seller_sku'].$sep.
				$pro['FbaUploadInventory']['fnsku'].$sep.
				$pro['FbaUploadInventory']['asin'].$sep.
				$pro['FbaUploadInventory']['condition'].$sep.
				$pro['FbaUploadInventory']['currency'].$sep.				
				$pro['FbaUploadInventory']['price'].$sep.
				$pro['FbaUploadInventory']['sales_last_30_days'].$sep. 
				$pro['FbaUploadInventory']['units_sold_last_30_days'].$sep.  
				$pro['FbaUploadInventory']['total_units'].$sep.  
				$pro['FbaUploadInventory']['inbound'].$sep.  
				$pro['FbaUploadInventory']['available'].$sep. 
				$pro['FbaUploadInventory']['fc_transfer'].$sep.  
				$pro['FbaUploadInventory']['fc_processing'].$sep.
				$pro['FbaUploadInventory']['customer_order'].$sep.  
 				$pro['FbaUploadInventory']['unfulfillable'].$sep.
				$pro['FbaUploadInventory']['working'].$sep.   
				$pro['FbaUploadInventory']['shipped'].$sep.  
				$pro['FbaUploadInventory']['receiving'].$sep. 
				$pro['FbaUploadInventory']['country'].$sep.  
				$pro['FbaUploadInventory']['alert'].$sep.    
				$pro['FbaUploadInventory']['rec_replenishment_qty'].$sep. 
				$pro['FbaUploadInventory']['rec_ship_date'].$sep.  
				$pro['FbaUploadInventory']['status'].$sep 
					
				."\r\n", FILE_APPEND | LOCK_EX);	
			}
		} 	 
		 
		$result =  WWW_ROOT.'logs/'.$file_name; 
		header('Content-Description: File Transfer');
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename='.basename($result));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($result));   
		readfile($result); 
		exit;
	
	}
	
	public function ViewStock() 
	{	
		$this->layout = "index"; 	
   		$this->loadModel('FbaUploadStockFile');	 
		$this->loadModel('FbaUploadInventory');	 
		$file = $this->FbaUploadStockFile->find('all', array('conditions'=>array('stock_date'=>$this->request->query['date'])));
		$file_ids = [];
		if(count($file) > 0){ 
			foreach($file as $v){
				$file_ids[] = $v['FbaUploadStockFile']['id'];
			}
		}
 	  		 
  		 
		$conditions[] = array('file_id'=> $file_ids);
		if(isset($_REQUEST['searchkey']) && $_REQUEST['searchkey'] !=''){
			$conditions[] = array('OR' =>
						 array(
						 'FbaUploadInventory.sku' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.alert' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.country' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.fnsku' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.seller_sku' => trim($_REQUEST['searchkey']),
						 'FbaUploadInventory.asin' => trim($_REQUEST['searchkey'])
						 ) 
					 ) ;
		}
		if(isset($_REQUEST['store']) && $_REQUEST['store'] !=''){
			$conditions[] = array('store'=> $_REQUEST['store']);
		}
		if(isset($_REQUEST['start_input']) && $_REQUEST['start_input'] !=''){
 			$sdate = date('Y-m-d',strtotime($this->request->query['start_input']));
			$edate = date('Y-m-d',strtotime($this->request->query['end_input']));
 			$conditions[] = array('added_date BETWEEN ? AND ?'=>[$sdate,$edate.' 23:59:59']);
		}
		 //pr($conditions);
		 
		$this->paginate = array('conditions'=>$conditions,'order'=>'FbaUploadInventory.added_date DESC','limit' => 100 );
		$all_orders = $this->paginate('FbaUploadInventory');
		 
		$this->set( 'all_orders', $all_orders );	 
  		 
 	}
	 
	
  	public function InventoryFiles() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","Europe FBA Orders" );	
		
		$this->loadModel('FbaInventoryFile'); 
		$this->loadModel('FbaInventoryDetail'); 
		 
		$conditions = [];
		if(isset($_REQUEST['searchkey']) && $_REQUEST['searchkey'] !=''){
			$conditions[] = array('OR' =>
						 array(
						 'FbaInventoryFile.sku' => trim($_REQUEST['searchkey']),
						 'FbaInventoryFile.alert' => trim($_REQUEST['searchkey']),
						 'FbaInventoryFile.country' => trim($_REQUEST['searchkey']),
						 'FbaInventoryFile.fnsku' => trim($_REQUEST['searchkey']),
						 'FbaInventoryFile.seller_sku' => trim($_REQUEST['searchkey']),
						 ) 
					 ) ;
		}
		if(isset($_REQUEST['store']) && $_REQUEST['store'] !=''){
			$conditions[] = array('store'=> $_REQUEST['store']);
		}
		if(isset($_REQUEST['start_input']) && $_REQUEST['start_input'] !=''){
 			$sdate = date('Y-m-d',strtotime($this->request->query['start_input']));
			$edate = date('Y-m-d',strtotime($this->request->query['end_input']));
 			$conditions[] = array('stock_date BETWEEN ? AND ?'=>[$sdate,$edate.' 23:59:59']);
		}
		 //pr($conditions);
		 
		$this->paginate = array('conditions'=>$conditions,'order'=>'uploaded_date DESC','limit' => 100 );
		$all_orders = $this->paginate('FbaInventoryFile');
		 
		$this->set( 'all_orders', $all_orders );	
		
	} 
	public function InventoryUploadSave() 
    {	
 		
		$this->layout = 'index';		 
		$this->loadModel('FbaInventoryFile'); 
		$this->loadModel('FbaInventoryDetail'); 
 		 
		$filetemppath =	$this->data['inventory']['uploadfile']['tmp_name']; 
		$pathinfo     = pathinfo($this->data['inventory']['uploadfile']['name']);
             
		$fileNameWithoutExt = $pathinfo['filename'];
		$fileExtension = strtolower($pathinfo['extension']);
 		$filename = $this->data['inventory']['uploadfile']['name'];
  		$saved_name = $fileNameWithoutExt.'_'.date('Ymd_his').'_'.$this->Session->read('Auth.User.username').".".$fileExtension;		
		$upload_file = WWW_ROOT. 'fba_inventory'.DS.$saved_name ; 
		
		if(empty($this->data['inventory']['store'])){
			$this->Session->setFlash("Please select store.", 'flash_danger');
 		 	$this->redirect($this->referer());
			exit;
		}
		if(empty($this->data['inventory']['uploadfile']['name'])){
			$this->Session->setFlash("Please select file.", 'flash_danger');
 		 	$this->redirect($this->referer());
			exit;
		}	
   		
		if($fileExtension == 'csv')
		{ 		 
			$filedata = [];
			$filedata['store'] 			= $this->data['inventory']['store'];
			$filedata['file_name'] 		= $filename;
			$filedata['saved_name'] 	= $saved_name;
 			$filedata['uploaded_date']  = date('Y-m-d H:i:s');
			$filedata['uploaded_by']  	= $this->Session->read('Auth.User.username');
			$this->FbaInventoryFile->saveAll( $filedata );
			$file_id	 = $this->FbaInventoryFile->getLastInsertId(); 
				
 			move_uploaded_file($filetemppath,$upload_file); 
			
			//$sql = "UPDATE `fba_upload_inventories` SET `status` = '1' WHERE `store` = '". $this->data['inventory']['store']."'"; 
			//$this->FbaInventoryDetail->query($sql);
			
			$row = 0; $skip = []; $up = 0;
			$handle = fopen($upload_file, "r");
 			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			  
				$row++;			
 				if($row > 1){
					if(strtotime($data[0]) > strtotime("-15 days")){
						$up++;
						$savedata = array();					
						$master = $this->getSkuMapping($data[2]); 
						$savedata['file_id'] 	= $file_id;
						$savedata['store'] 		= $this->data['inventory']['store'];
						$savedata['snapshot_date'] =  $data[0];
						$savedata['snapshotdate'] =  date('Y-m-d',strtotime($data[0]));
						$savedata['fnsku'] 		= $data[1];
						$savedata['channel_sku'] = $data[2]; 
						$savedata['sku'] 		= $master;
						$savedata['product_name']= addslashes($data[3]);
						$savedata['quantity']	= $data[4];
						$savedata['fulfillment_center']	= $data[5];
						$savedata['detailed_disposition']	= $data[6];
						$savedata['country']	= $data[7];
						$savedata['file_name'] 		= $saved_name;
						$savedata['upload_by'] 		= $this->Session->read('Auth.User.username');
						/*
						 echo "<pre>";
						 print_r($savedata);
						 echo "</pre>";  exit; */            
						$fba = $this->FbaInventoryDetail->find('first', array('conditions'=>array('snapshot_date' =>$data[0],'channel_sku' =>$data['2'],'fulfillment_center' =>$data['5'])) );
							
						if(count($fba) > 0){
							$savedata['id'] = $fba['FbaInventoryDetail']['id'];	
							$this->FbaInventoryDetail->saveAll( $savedata );					 
						}else{
							$savedata['added_date'] = date('Y-m-d H:i:s');							
							$this->FbaInventoryDetail->saveAll( $savedata );
						}    
					}	
					else{
						$skip[] = $data[0].' # '. $data[2];
					}		
				}		
			}   		
			fclose($handle);
			
			$skipstr = '';
			if(count($skip) > 0){
				$skipstr = count($skip).' skipped due to old date.';
			}
			
			if($row > 0){
				$this->Session->setFlash($up . " inventory uploaded. ".$skipstr, 'flash_success');
			}elseif(count($skip) > 0){
				$this->Session->setFlash($skipstr, 'flash_success');
			} 
			
		 }
		 else{
			 $this->Session->setFlash("Invalid file format, Please upload CSV file.", 'flash_danger');
		 }
		 
		 $this->redirect($this->referer());
 		 
 	}
	
	public function Inventory($file_id = 0) 
    {	
		$this->layout = "index"; 	
		$this->set( "title","Europe FBA Orders" );	
		
		$this->loadModel('FbaInventoryFile'); 
		$this->loadModel('FbaInventoryDetail'); 
		 
		$conditions[] = array('file_id'=> $file_id);
		if(isset($_REQUEST['searchkey']) && $_REQUEST['searchkey'] !=''){
			$conditions[] = array('OR' =>
						 array(
						 'FbaInventoryDetail.sku' => trim($_REQUEST['searchkey']),
						 'FbaInventoryDetail.fnsku' => trim($_REQUEST['searchkey']),
						 'FbaInventoryDetail.country' => trim($_REQUEST['searchkey']),
						 'FbaInventoryDetail.channel_sku' => trim($_REQUEST['searchkey']),
						 'FbaInventoryDetail.fulfillment_center' => trim($_REQUEST['searchkey']),
						 'FbaInventoryDetail.product_name' => trim($_REQUEST['searchkey'])
						 ) 
					 ) ;
		}
		if(isset($_REQUEST['store']) && $_REQUEST['store'] !=''){
			$conditions[] = array('store'=> $_REQUEST['store']);
		}
		if(isset($_REQUEST['start_input']) && $_REQUEST['start_input'] !=''){
 			$sdate = date('Y-m-d',strtotime($this->request->query['start_input']));
			$edate = date('Y-m-d',strtotime($this->request->query['end_input']));
 			$conditions[] = array('added_date BETWEEN ? AND ?'=>[$sdate,$edate.' 23:59:59']);
		}
		 //pr($conditions);
		 
		$this->paginate = array('conditions'=>$conditions,'order'=>'FbaInventoryDetail.added_date DESC','limit' => 100 );
		$all_orders = $this->paginate('FbaInventoryDetail');
		 
		$this->set( 'all_orders', $all_orders );	
		
	}
	
	public function FbaDeleteFile($id = '') 
	{
		$this->loadModel('FbaInventoryFile'); 
		$this->loadModel('FbaInventoryDetail');
		$file = $this->FbaInventoryFile->find('first', array('conditions'=>array('id'=>$id)));
		if(count($file) > 0){ 
			$file_name = WWW_ROOT. 'fba_inventory'.DS.$file['FbaInventoryFile']['file_name'];
			@unlink($file_name);
			
			$file_name = 'fba_newinventory_DeleteFile_'.date('Ymd').'.log'; 
			file_put_contents(WWW_ROOT.'logs/'.$file_name,$file['FbaInventoryFile']['file_name']."\t".date('Y-m-d H:i:s')."\t".$this->Session->read('Auth.User.username')."\r\n");
 		}

		$sql = "DELETE FROM `fba_inventory_files` WHERE `id` = {$id}"; 
		$this->FbaInventoryFile->query($sql);
		$sql = "DELETE FROM `fba_inventory_details` WHERE `file_id` = {$id}"; 
		$this->FbaInventoryDetail->query($sql);
		$this->Session->setFlash("File and file data deleted sucessfully.", 'flash_danger');
		$this->redirect($this->referer());
	}
	
	public function FbaChangeStatus($id = 0 , $status = 0)
	{	
		$this->layout = "index"; 	
  		$this->loadModel('FbaInventoryFile'); 
		$this->loadModel('FbaInventoryDetail');
		$sql = "UPDATE `fba_inventory_files` SET `status` = '".$status."'  WHERE `id` = {$id}"; 
		$this->FbaUploadStockFile->query($sql);
		$sql = "UPDATE `fba_inventory_details` SET `status` = '".$status."' WHERE `file_id` = {$id}"; 
		$this->FbaInventoryDetail->query($sql);
		$this->Session->setFlash("File and file data status change sucessfully.", 'flash_danger'); 
		$this->redirect($this->referer());
 		exit;
 	}
	
	public function FbaChangeInventoryStatus($id = 0 , $status = 0)
	{	
		$this->layout = "index"; 	
 		$this->loadModel('FbaInventoryFile'); 
		$this->loadModel('FbaInventoryDetail'); 	 
 		$sql = "UPDATE `fba_inventory_details` SET `status` = '".$status."' WHERE `id` = {$id}"; 
		$this->FbaInventoryDetail->query($sql);
		$this->Session->setFlash("SKU status change sucessfully.", 'flash_danger'); 
		$this->redirect($this->referer());
   	
		exit;
	
	}
	
	public function Fbadownload() 
	{	
		$this->layout = "index"; 	
  		$this->loadModel('FbaInventoryDetail');	 
  		 
		ob_clean(); 
		$sep = ","; 		 
 		$store = 'all';
		if($this->request->query['store'] != ''){  
			$store = $this->request->query['store'];
			$products = $this->FbaInventoryDetail->find('all', array('conditions'=>array('store'=>$this->request->query['store'])));
		/*	$products = $this->FbaInventoryDetail->find('all', array('conditions'=>array('added_date BETWEEN ? AND ?'=>[$sdate,$edate.' 23:59:59'],'store'=>$this->request->query['store'])));*/
	 	}else{
			$products = $this->FbaInventoryDetail->find('all');
			/*$products = $this->FbaInventoryDetail->find('all', array('conditions'=>array('added_date BETWEEN ? AND ?'=>[$sdate,$edate.' 23:59:59'])));*/
		} 
		$file_name = 'fba_stock_'.$store.'_'.$this->Session->read('Auth.User.username').'.csv'; 
		file_put_contents(WWW_ROOT.'logs/'.$file_name,'snapshot_date'.$sep.'sku'.$sep.'channel_sku'.$sep.'fnsku'.$sep.'product_name'.$sep.'quantity'.$sep.'fulfillment_center'.$sep.'detailed_disposition'.$sep.'country'.$sep.'upload_by'.$sep.'added_date'."\r\n");
		
  		if(count($products) > 0){
			foreach($products as $pro){
				file_put_contents(WWW_ROOT.'logs/'.$file_name,
				$pro['FbaInventoryDetail']['snapshot_date'].$sep.
				$pro['FbaInventoryDetail']['sku'].$sep.
				$pro['FbaInventoryDetail']['channel_sku'].$sep.
				$pro['FbaInventoryDetail']['fnsku'].$sep.
				'"'.$pro['FbaInventoryDetail']['product_name'].'"'.$sep.
				$pro['FbaInventoryDetail']['quantity'].$sep.
				$pro['FbaInventoryDetail']['fulfillment_center'].$sep.				
				$pro['FbaInventoryDetail']['detailed_disposition'].$sep.
				$pro['FbaInventoryDetail']['country'].$sep. 
				$pro['FbaInventoryDetail']['upload_by'].$sep.  
				$pro['FbaInventoryDetail']['added_date'].$sep 
 				."\r\n", FILE_APPEND | LOCK_EX);	
			}
		} 	 
		 
		$result =  WWW_ROOT.'logs/'.$file_name; 
		header('Content-Description: File Transfer');
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename='.basename($result));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($result));   
		readfile($result); 
		exit;
	
	}
	public function FbaDownloadFile($id = 0) 
	{	
		$this->layout = "index"; 	
  		$this->loadModel('FbaInventoryFile');	 
 		$file = $this->FbaInventoryFile->find('first', array('conditions'=>array('id'=>$id)));
   		if(count($file) > 0){ 
			$file_name =  $file['FbaInventoryFile']['file_name'];
			$saved_name = WWW_ROOT. 'fba_inventory'.DS.$file['FbaInventoryFile']['saved_name'];
			
 			header('Content-Description: File Transfer');
			header('Content-Type: text/csv');
			header('Content-Disposition: attachment; filename='.basename($file_name));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($saved_name));   
			readfile($saved_name); 
		}
		exit;
	
	}
	 
}