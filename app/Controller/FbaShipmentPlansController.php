<?php
//error_reporting(0);
class FbaShipmentPlansController extends AppController
{
    
    var $name = "FbaShipmentPlans";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('printTest'));		
			
    }
 	
   public function index() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Shipments" );	
		$this->loadModel('FbaShipment');
		 
		$this->paginate = array('order'=>'FbaShipment.id DESC','limit' => 20 );
		$all_shipments = $this->paginate('FbaShipment');
		$this->set( 'all_shipments', $all_shipments );				
		
	} 
	public function ShipmentAddress($id = 0)  
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Shipments" );	
		$this->loadModel('FbaApiShipmentAddress');  
		 
		$this->paginate = array('order'=>'FbaApiShipmentAddress.id DESC','limit' => 20 );
		$all_address = $this->paginate('FbaApiShipmentAddress');
		 
		$this->set( 'all_address', $all_address );	
		$address = array();
		if($id > 0){
			$address = $this->FbaApiShipmentAddress->find('first',array('conditions' => array('id' => $id)));
		}
		$this->set( 'address', $address );				
		
	} 
	
	public function ShipmentAddressDelete( $id = 0){ 
	
		$this->loadModel('FbaApiShipmentAddress'); 
		
		$this->FbaApiShipmentAddress->deleteAll(array('FbaApiShipmentAddress.id' => $id), false);
		
		$this->Session->setFlash("Address Deleted.", 'flash_success');
		
		$this->redirect($this->referer());	  
	}
	
	public function SaveShipmentAddress(){
		
		$this->loadModel('FbaApiShipmentAddress');  
		$error = array();
		if($this->request->data['address_line_1'] == ''){
			$error[] = 'Please enter address.';
		}
		
		if($this->request->data['city'] == ''){
			$error[] = 'Please enter city.';
		}
		
		if($this->request->data['country_code'] == ''){
			$error[] = 'Please enter country code.';
		}
		
		if($this->request->data['state_or_province_code'] == ''){
			$error[] = 'Please enter state/province code.';
		}
		
		if($this->request->data['postal_code'] == ''){
			$error[] = 'Please enter postal code.';
		}
		
		if(count($error) > 0){
			$msg ='';
			foreach($error as $err){
				$msg .= $err."<br>";
			}
			$this->Session->setFlash($msg, 'flash_danger');
		}else{
			$this->FbaApiShipmentAddress->saveAll( $this->request->data ); 
			if(isset($this->request->data['id'])){
				$this->Session->setFlash(" Address Updated.", 'flash_success');
			}else{
				$this->Session->setFlash(" Address added.", 'flash_success');
			}
		}
		
		$this->redirect($this->referer());	 
						
	}
	
	
	public function CreateInboundShipmentPlan() 
    {	
		App::import( 'vendor' , 'FBAInboundServiceMWS/Client' );
		App::import( 'vendor' , 'FBAInboundServiceMWS/Exception' );
		App::import( 'vendor' , 'FBAInboundServiceMWS/Mock' );
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model' );
		
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/CreateInboundShipmentPlanRequest' ); 
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/CreateInboundShipmentPlanResult' );
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/CreateInboundShipmentPlanResponse' ); 
		
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/Address' ); 
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/InboundShipmentPlanRequestItemList' ); 
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/InboundShipmentPlanItem' );
		
		$serviceUrl = "https://mws.amazonservices.com/FulfillmentInboundShipment/2010-10-01";
		
		$config = array (
		'ServiceURL' => $serviceUrl,
		'ProxyHost' => null,
		'ProxyPort' => -1,
		'ProxyUsername' => null,
		'ProxyPassword' => null,
		'MaxErrorRetry' => 3,
		);
		
		$service = new FBAInboundServiceMWS_Client(
			AWS_ACCESS_KEY_ID,
			AWS_SECRET_ACCESS_KEY,
			APPLICATION_NAME,
			APPLICATION_VERSION,
			$config);
			
			
		$request = new FBAInboundServiceMWS_Model_CreateInboundShipmentPlanRequest(); 
		$request->setSellerId(MERCHANT_ID);
		$request->setMarketplace(MARKETPLACE_ID);	
		
		$Address = new FBAInboundServiceMWS_Model_Address() ;	
		$Address->setName('Euraco Group Ltd') ;
		$Address->setAddressLine1('36 Harbour Reach') ;
		$Address->setAddressLine2('La Rue De Carteret') ;
		$Address->setDistrictOrCounty('Jersey') ;
		$Address->setCity('Jersey') ;
		$Address->setStateOrProvinceCode('JE2 4HR') ;
		$Address->setCountryCode('GB') ;
		$Address->setPostalCode('JE2 4HR') ;


//$Address = array('Name'=>'test1','AddressLine1'=>'LINE_1','City'=>'Seattle','StateOrProvinceCode'=>'WA','PostalCode'=>'98121','CountryCode'=>'US');


		$request->setShipFromAddress($Address);	
		
		
		$request->setLabelPrepPreference('SELLER_LABEL');	
		//$request->setShipToCountryCode('US');	
		//$request->setShipToCountrySubdivisionCode('WA');
		
		$InboundShipmentPlanRequestItemList = new FBAInboundServiceMWS_Model_InboundShipmentPlanRequestItemList() ;

		/*foreach($this->data[‘Product’][‘id’] as $key=>$val)
		{*/
		$InboundShipmentPlanRequestItem = new FBAInboundServiceMWS_Model_InboundShipmentPlanItem();
		$InboundShipmentPlanRequestItem->setSellerSKU('USEURA14107') ;
		$InboundShipmentPlanRequestItem->setQuantity(1) ;
		$array[] = $InboundShipmentPlanRequestItem ;
		//}
		$InboundShipmentPlanRequestItemList->setmember($array) ;	
		$request->setInboundShipmentPlanRequestItems($InboundShipmentPlanRequestItemList) ;
		
		
		//$Items = array('SellerSKU'=>'SKU00001' ,'Quantity'=>'1','PrepInstruction'=>'Taping','PrepOwner'=>'AMAZON'); 

	
		//$request->setInboundShipmentPlanRequestItems($Items); 
		 
		$xml = $this->invokeCreateInboundShipmentPlan($service, $request);
		pr($xml);
		if(isset($xml->CreateInboundShipmentPlanResult->InboundShipmentPlans->member)){
			
			$this->loadModel('FbaApiShipment');
			$this->loadModel('FbaApiShipmentItem');	
			
			foreach($xml->CreateInboundShipmentPlanResult->InboundShipmentPlans->member as $data)
			{			
						 
				$savedata = array();
				$savedata['shipment_id'] 				= $data->ShipmentId;
				$savedata['destination_fulfillment_center_id'] = $data->DestinationFulfillmentCenterId;
				if(isset($data->ShipToAddress->Name)){	
					$savedata['ship_to_name']			= $data->ShipToAddress->Name;
				}
				
				$savedata['ship_to_address_line_1'] 	= $data->ShipToAddress->AddressLine1;
				
				if(isset($data->ShipToAddress->AddressLine2)){	
					$savedata['ship_to_address_line_2'] = $data->ShipToAddress->AddressLine2;	
				}
				$savedata['ship_to_city'] 				= $data->ShipToAddress->City;	
				$savedata['ship_to_state_or_province_code'] = $data->ShipToAddress->StateOrProvinceCode;	
				$savedata['ship_to_country_code'] 		= $data->ShipToAddress->CountryCode;	
				$savedata['ship_to_postal_code'] 		= $data->ShipToAddress->PostalCode;	
				$savedata['label_prep_type'] 			= $data->LabelPrepType;	
				
				$savedata['total_units'] 				= $data->EstimatedBoxContentsFee->TotalUnits;	
				$savedata['fee_per_unit_currency_code'] = $data->EstimatedBoxContentsFee->FeePerUnit->CurrencyCode;	
				$savedata['fee_per_unit_value'] 		= $data->EstimatedBoxContentsFee->FeePerUnit->Value;	
				$savedata['total_fee_currency_code'] 	= $data->EstimatedBoxContentsFee->TotalFee->CurrencyCode;		
				$savedata['total_fee_unit_value']		= $data->EstimatedBoxContentsFee->TotalFee->Value;	
				
				
				$fba_shipment  = $this->FbaApiShipment->find('first', array('conditions'=>array('FbaApiShipment.shipment_id' => $data->ShipmentId)) );
				
				if(count($fba_shipment) > 0){
					$savedata['id'] = $fba_shipment['FbaOrderItem']['id'];						
				}else{					
					$savedata['added_date'] = date('Y-m-d H:i:s');														
				}				
				$this->FbaApiShipment->saveAll( $savedata );
				
				foreach($data->member as $items)
				{	
					$memberData['shipment_id'] =  $data->ShipmentId;	
					$memberData['seller_sku'] =  $items->SellerSKU;	
					$memberData['fnsku']	  =  $items->FulfillmentNetworkSKU;	
					$memberData['quantity']   =  $items->Quantity;	
					if(isset($items->PrepDetailsList)){
						$memberData['prep_instruction']   =  $items->PrepDetailsList->PrepDetails->PrepInstruction;	
						$memberData['prep_owner']   =  $items->PrepDetailsList->PrepDetails->PrepOwner;	
					}
					$fba_shipment  = $this->FbaApiShipmentItem->find('first', array('conditions'=>array('FbaApiShipmentItem.shipment_id' => $data->ShipmentId)) );
						 
				} 
			}	
		}
		exit;
	} 
	
	private  function invokeCreateInboundShipmentPlan(FBAInboundServiceMWS_Interface $service, $request)
	  {
		  try {
			$response = $service->CreateInboundShipmentPlan($request);
	
			$dom = new DOMDocument();
			$dom->loadXML($response->toXML());
			$dom->preserveWhiteSpace = false;
			$dom->formatOutput = true;
			$getResponse = $dom->saveXML();			
			return simplexml_load_string($getResponse);
	
		 } catch (MarketplaceWebServiceOrders_Exception $ex) {
			 return simplexml_load_string($ex->getXML()) ;
		 }
	 }
	 
	public function CreateInboundShipment() 
	{	
		App::import( 'vendor' , 'FBAInboundServiceMWS/Client' );
		App::import( 'vendor' , 'FBAInboundServiceMWS/Exception' );
		App::import( 'vendor' , 'FBAInboundServiceMWS/Mock' );
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model' );
		
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/CreateInboundShipmentRequest' ); 
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/CreateInboundShipmentResult' );
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/CreateInboundShipmentResponse' );  
		
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/Address' ); 
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/InboundShipmentHeader' ); 
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/InboundShipmentItemList' );
		App::import( 'vendor' , 'FBAInboundServiceMWS/Model/InboundShipmentItem' );
		
		$serviceUrl = "https://mws.amazonservices.com/FulfillmentInboundShipment/2010-10-01";
		
		$config = array (
		'ServiceURL' => $serviceUrl,
		'ProxyHost' => null,
		'ProxyPort' => -1,
		'ProxyUsername' => null,
		'ProxyPassword' => null,
		'MaxErrorRetry' => 3,
		);
		
		$service = new FBAInboundServiceMWS_Client(
			AWS_ACCESS_KEY_ID,
			AWS_SECRET_ACCESS_KEY,
			APPLICATION_NAME,
			APPLICATION_VERSION,
			$config);
			
		 $request = new FBAInboundServiceMWS_Model_CreateInboundShipmentRequest();

		 
		$request->setSellerId(MERCHANT_ID);
		$request->setMarketplace(MARKETPLACE_ID);	
		$request->setShipmentId('FBA15C2J0Y5D');	
		
		$objInboundShipment = new FBAInboundServiceMWS_Model_InboundShipmentHeader() ;	
		
		
			$Address = new FBAInboundServiceMWS_Model_Address() ;			
			$Address->setName('Euraco Group Ltd') ;
			$Address->setAddressLine1('36 Harbour Reach') ;
			$Address->setAddressLine2('La Rue De Carteret') ;
			$Address->setDistrictOrCounty('Jersey') ;
			$Address->setCity('Jersey') ;
			$Address->setStateOrProvinceCode('JE2 4HR') ;
			$Address->setCountryCode('GB') ;
			$Address->setPostalCode('JE2 4HR') ;
 
			$objInboundShipment->setShipFromAddress($Address) ;
			$objInboundShipment->setShipmentName('FBA-'.date('mdY')) ;
			$objInboundShipment->setShipmentStatus('WORKING') ;
			$objInboundShipment->setLabelPrepPreference('SELLER_LABEL') ;
			$objInboundShipment->setDestinationFulfillmentCenterId('PHX6') ;   

		$request->setInboundShipmentHeader($objInboundShipment);	
		 
		 
		$objItemList = new FBAInboundServiceMWS_Model_InboundShipmentItemList();
		
		$objItem = new FBAInboundServiceMWS_Model_InboundShipmentItem();
	  
		$objItem->setSellerSKU('USEURA14107') ;
		$objItem->setQuantityShipped(1) ;			
		//$objItem->setPrepInstruction('Taping') ;			
		$array[] = $objItem ;
		 
		$objItemList->setmember($array) ;	
		$request->setInboundShipmentItems($objItemList) ;
	 
		$xml = $this->invokeCreateInboundShipment($service, $request);
		pr($xml);
		 
		exit;
	} 


	private function invokeCreateInboundShipment(FBAInboundServiceMWS_Interface $service, $request)
	  {
		  try {
			$response = $service->CreateInboundShipment($request);
	
			$dom = new DOMDocument();
			$dom->loadXML($response->toXML());
			$dom->preserveWhiteSpace = false;
			$dom->formatOutput = true;
			$getResponse = $dom->saveXML();			
			return simplexml_load_string($getResponse);
	
		 } catch (MarketplaceWebServiceOrders_Exception $ex) {
			 return simplexml_load_string($ex->getXML()) ;
		 }
	 }


	 
	public function getSkuMapping($seller_sku = null)
	{		
		$this->loadModel( 'Skumapping' );	
		$_items = $this->Skumapping->find('first', array('conditions'=>array('Skumapping.channel_sku'=>$seller_sku)));
		if(count($_items) > 0){
			return  array('sku'=>$_items['Skumapping']['sku'],'barcode'=>$_items['Skumapping']['barcode']);
		}else{
			return 0;
		}
	}
	
	 
	
	 
	 
}