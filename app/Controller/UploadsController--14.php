<?php
error_reporting(1);
ini_set('max_execution_time', 15);
//ini_set('memory_limit', '-1');
 
class UploadsController extends AppController
{

    var $name = "Uploads";
    var $components = array('Session','Upload','Common','Auth');
    var $helpers = array('Html','Form','Common','Session');
	public $funcName;
	private static $fnm;
	 
	public function UploadsCsvFilesForInventery()
	{
  		$this->autoLayout = 'ajax';
		$this->autoRender = false;
		
		$this->loadModel('Store');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('ProductPrice');
		$this->loadModel('ProductImage');
		$this->loadModel('AttributeOption');
		$this->loadModel('ProductBarcode');
		$this->loadModel('ProductCsv');
		$this->loadModel('SkuFbmFba');
		$this->loadModel('ProductHscode'); 
 				
				//pr($this->request->data);
		// exit;
		$this->data['Product']['Import_file']['name'];
		if($this->data['Product']['Import_file']['name'] != '')
		{
				
			$filename =  WWW_ROOT. 'files'.DS.$this->request->data['Product']['Import_file']['name'];
			move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
			$name		=	 $this->request->data['Product']['Import_file']['name'];
			
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/'.$name);
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			
 			$header	=	array('Product Title','Brand','Short Description','Long Description','Contents/Information','Ingredients','Usage Information', 'Notes', 'Technical Specifications',
			 'UPC/Barcode', 'Image filename', 'Image filename2', 'Image filename3', 'Image filename4', 'Vegan', 'Vegetarian', 'Gluten Free', 'Corn Free', 'Wheat Free', 'Dairy Free', 
			 'Soya Free', 'Non-GM', 'Weight', 'Category', 'Price', 'Length', 'Width', 'Height', 'Status');
			 
			$count	= 0;
			$alreadycount	=	0;
			$countbarcode	=	0;
			$frizileMark	=	0;
			$exception 		=	[];	
 				
			for($i=2; $i<=$lastRow; $i++) 
			{ 				 	
				$global_barcode = '';
				for($j = 0; $j < 13; $j++) {
					$global_barcode .= mt_rand(1, 9);
				}
				$xsbarcode								=	$objWorksheet->getCellByColumnAndRow(6,$i)->getValue();	
				$productdata['product_sku']				=	trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
				$productdata['asin_no']					=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$productdata['product_name']			=	$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();				
				$productdata['category']				=	$objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
				$productdata['product_status']			=	$objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
				$productdata['country_of_origin']		=	$objWorksheet->getCellByColumnAndRow(20,$i)->getValue();
				$cell_val = $objWorksheet->getCellByColumnAndRow(19,$i)->getValue();
				$productdata['route']					=	is_null($cell_val) ? '1' : $cell_val;
				
 				$productdscdata['brand']				=	$objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
				$productdscdata['short_description']	=	addslashes($objWorksheet->getCellByColumnAndRow(4,$i)->getValue());
				$productdscdata['long_description']		=	addslashes($objWorksheet->getCellByColumnAndRow(4,$i)->getValue());
				$productdscdata['barcode'] 				=	$global_barcode;//$objWorksheet->getCellByColumnAndRow(6,$i)->getValue();				 
				
 				$productdscdata['weight']				=	$objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
				$productdscdata['price']				=	$objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
				$productdscdata['length'] 				=	$objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
				$productdscdata['width'] 				=	$objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
				$productdscdata['height']				=	$objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
				
 				$productdscdata['model_no']				=	$objWorksheet->getCellByColumnAndRow(25,$i)->getValue();
				$productdscdata['manufacturer_part_num']=	$objWorksheet->getCellByColumnAndRow(26,$i)->getValue();
				$productdscdata['type']					=	$objWorksheet->getCellByColumnAndRow(28,$i)->getValue();
				$product_type = 'Single';
				$product_identifier = 'Single';
				$product_defined_skus = '';
				//$productdata['product_sku'] = 'B-0621B026-0628B001-0620B001-3';
  				//echo $productdata['product_sku'] = 'B-06245#BC-4';
				if(substr($productdata['product_sku'],0,1) == 'B'){
					$_sk = explode("-",$productdata['product_sku']);
					$ind = count($_sk) - 1;
					$_skus = [];
					for($b = 1; $b < $ind  ; $b++){
						$_skus[] = 'S-'.$_sk[$b];
					}
					$product_type = 'Bundle';
					if(count($_sk ) > 3){
						$product_identifier = 'Multiple';
					}
 					$product_defined_skus = implode(":",$_skus);  
 				}
  				/*
 				 * Params, Inserting Product type and product defined skus list with colon based
 				 */ 
				$productdscdata['product_type']				=	$product_type;//$objWorksheet->getCellByColumnAndRow(31,$i)->getValue();
				$productdscdata['product_defined_skus']		=	$product_defined_skus;//$objWorksheet->getCellByColumnAndRow(32,$i)->getValue();
				$productdscdata['product_identifier']		=	$product_identifier;//$objWorksheet->getCellByColumnAndRow(33,$i)->getValue();
 				$productdscdata['frezile_mark']				=	$objWorksheet->getCellByColumnAndRow(16,$i)->getValue();
				
 				$hscode =[];
				$hscode['GB'] =	$objWorksheet->getCellByColumnAndRow(21,$i)->getValue();
				$hscode['DE'] =	$objWorksheet->getCellByColumnAndRow(22,$i)->getValue();
				$hscode['ES'] =	$objWorksheet->getCellByColumnAndRow(23,$i)->getValue();
				$hscode['FR'] =	$objWorksheet->getCellByColumnAndRow(24,$i)->getValue();
				$hscode['IT'] =	$objWorksheet->getCellByColumnAndRow(25,$i)->getValue();
				$hscode['US'] =	$objWorksheet->getCellByColumnAndRow(26,$i)->getValue();
				$hscode['CA'] =	$objWorksheet->getCellByColumnAndRow(27,$i)->getValue();
				
				$product_csv = [];
 				$product_csv['asin']				=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$product_csv['ref_fee']				=	$objWorksheet->getCellByColumnAndRow(29,$i)->getValue();
				$product_csv['margin']				=	$objWorksheet->getCellByColumnAndRow(30,$i)->getValue();
 				$product_csv['referral_category']	=	$objWorksheet->getCellByColumnAndRow(28,$i)->getValue();
				$store_csv =[]; $fba_csv =[];
 				
			 	//-----------Cost
				$col = 33;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_UK'] = is_null($cell_val) ? 'N' : $cell_val;
				
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_ES'] = is_null($cell_val) ? 'N' : $cell_val;
				
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_FR'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_DE'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_IT'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_NL'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_CA'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_US'] = $fba_csv['FBAUSA'] = is_null($cell_val) ? 'N' : $cell_val; //FBA
				 
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_USABuyer'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_UKFBA'] = $fba_csv['FBAUK'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
 				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_DEFBA'] = $fba_csv['FBADE'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_ESFBA'] = $fba_csv['FBAES'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_FRFBA'] = $fba_csv['FBAFR'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['CostBreaker_ITFBA'] = $fba_csv['FBAIT'] = is_null($cell_val) ? 'N' : $cell_val;
				
				//Marec	 
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_DE'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_DEFBA'] = $fba_csv['FBADE'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_ES'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_FR'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_FRFBA'] = $fba_csv['FBAFR'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_IT'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_ITFBA'] = $fba_csv['FBAIT'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_NL'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_UK'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Marec_UKFBA'] = $fba_csv['FBAUK'] = is_null($cell_val) ? 'N' : $cell_val;
				 $col++;
  				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow Retail'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();				
  				$store_csv['RAINBOW RETAIL DE'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_DE_FBA'] = $fba_csv['FBADE'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_FRFBA'] = $fba_csv['FBAFR'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_Retail_ES'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_Retail_FR'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_Retail_IT'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Rainbow_UK_FBA'] = $fba_csv['FBAUK'] = is_null($cell_val) ? 'N' : $cell_val;
				
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Cdiscount_FR'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Real_DE'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['eBay'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Onbuy'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Flubit'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Fnac'] = is_null($cell_val) ? 'N' : $cell_val;
				$col++;
				$cell_val = $objWorksheet->getCellByColumnAndRow($col,$i)->getValue();
  				$store_csv['Fyndiq'] = is_null($cell_val) ? 'N' : $cell_val;
					
 				$storeIds = [];
				
				$storedata = $this->Store->find('all', array(
																'conditions' => array('OR' =>[
 																	'store_name' => array_keys($store_csv) ,
																	'store_alias' => array_keys($store_csv)
																])
																)
															); 
															
				
  					
				if(count($storedata) > 0){
					foreach($storedata as $str){
						$storeIds[]  = 	$str['Store']['id'];
					}
 				}  
				
				$productdata['store_name']				=	implode(",",$storeIds); 
				$product_csv['description']				=	addslashes($objWorksheet->getCellByColumnAndRow(4,$i)->getValue());		 	
				$product_csv['store_allowed']			=	json_encode($store_csv);
				$product_csv['referral_category']		=	$objWorksheet->getCellByColumnAndRow(28,$i)->getValue();
				$product_csv['referral_fee']			=	$objWorksheet->getCellByColumnAndRow(29,$i)->getValue();
				$product_csv['margin']					=	$objWorksheet->getCellByColumnAndRow(30,$i)->getValue();
 				$product_csv['supplier']				=	$objWorksheet->getCellByColumnAndRow(31,$i)->getValue();
				$product_csv['supplier_code']			=	$objWorksheet->getCellByColumnAndRow(32,$i)->getValue();
 				$product_csv['file_name']				= 	$this->request->data['Product']['Import_file']['name'];
				
				foreach($productdscdata as $pd => $v){
					$product_csv[$pd] = $v;
				}
				
				foreach($productdata as $pd => $v){
					$product_csv[$pd] = $v;
				}
				 
				foreach($hscode as $ccode => $hs_code){
					$product_csv[$ccode] = $hs_code;
					//---------------Update Store data--------------------
					$store = [];
					if(!empty($product_csv['product_sku'])){
						$checHs = $this->ProductHscode->find('first', array(
																	'conditions' => array(
																		'sku' => $product_csv['product_sku'],
																		'country_code' => $ccode
																		),
																	 'fields' =>['id']
																	)
																); 
						
						$store['sku']    		= $product_csv['product_sku'];
						$store['country_code']  = $ccode;
						$store['hs_code']		= $hs_code;
						$store['username'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
						if(count($checHs) > 0)
						{
							$store['id']			= 	$checHs['ProductHscode']['id'];
							$store['timestamp']		= 	date('Y-m-d H:i:s');
							$this->ProductHscode->saveAll($store);
						}else{
							$store['added_date']	= 	date('Y-m-d H:i:s');
							$this->ProductHscode->saveAll($store);
						}
					}
					//-------End									
				} 
				
				foreach($fba_csv as $str => $v){
					$product_csv[$str] = $v;
					//---------------Update Store data--------------------
					$store = [];
 					if(!empty($product_csv['product_sku'])){
						$checFbm = $this->SkuFbmFba->find('first', array(
																	'conditions' => array(
																		'sku' => $product_csv['product_sku'],
																		'store' => $str),
																	'fields' => ['id']
																	)
																); 
						$store['store']  = 	$str;
						$store['sku']  = 	$product_csv['product_sku'];
						$store['status'] = 	$v;
						if(count($checFbm) > 0)
						{
							$store['id']				= 	$checFbm['SkuFbmFba']['id'];
							$store['updated_date']		= 	date('Y-m-d H:i:s');
							$store['updated_by']		= 	$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
							$this->SkuFbmFba->saveAll($store);
						}else{
							$store['added_date']	= 	date('Y-m-d H:i:s');
							$this->SkuFbmFba->saveAll($store);
						}
					} 
					//-------End									
				}  
				//pr($product_csv);
				if(!empty($product_csv['product_sku'])){
					$checPro		=	$this->ProductCsv->find('first', array(
																'conditions' => array('product_sku' => $product_csv['product_sku'],'asin' => $product_csv['asin']),
																'fields' => ['id']
																)
															); 
					 
					try{
						if(count($checPro) > 0)
						{
							$product_csv['id']				= 	$checPro['ProductCsv']['id'];
							$product_csv['updated_date']	= 	date('Y-m-d H:i:s');
							$product_csv['updated_by']		= 	$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
							$this->ProductCsv->saveAll($product_csv);
						}else{
							$product_csv['added_date']	= 	date('Y-m-d H:i:s');
							$product_csv['added_by']		= 	$this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
							$this->ProductCsv->saveAll($product_csv);
						}
					}
					catch (Exception $e) {
						$exception[] = 'Caught exception: '.  $e->getMessage(). "\n";
					}
				} 
				
								
				$arrayMarge = array();
				/*foreach($productdscdataAttr['attribute'] as $attr)
				{
					if( $attr != '')
					{
						$attributeOptions	=	$this->AttributeOption->find('first', array('conditions' => array('AttributeOption.attribute_option_name' => $attr)));
						$attributeId	=	(isset($attributeOptions['AttributeOption']['id']) ? $attributeOptions['AttributeOption']['id'] : '');
						array_push($arrayMarge, $attributeId);
					}
				}*/
				$productdscdata['attribute_id']	=	$attrbuteid	=	implode(',', $arrayMarge);
				
				$checkproduct		=	$this->Product->find('first', array(
															'conditions' => array('Product.product_sku' => $productdata['product_sku'])
															)
														);

				

				// Check also brand is exists or not
				$this->loadModel( 'Brand' );
				$getBrand = $this->Brand->find( 'first' , array( 'conditions' => array( 'Brand.brand_name' => $productdscdata['brand'] ) ) );
				
				if( count( $getBrand ) == 0 )
				{
					$brandName = $productdscdata['brand'];
					$brandAlias = strtolower($brandName);					
					$this->Brand->query( "insert into brands ( brand_name, brand_alias ) values ( '{$brandName}' , '{$brandAlias}' )" );					
					$productdscdata['brand'] = $brandName;
					
				}
				else
				{
					$productdscdata['brand'] = $productdscdata['brand'];
				}
				
  				// Check also Category is exist or note
				$this->loadModel( 'Category' );
				$getCategory = $this->Category->find( 'first' , array( 'conditions' => array( 'Category.category_name' => $productdata['category'] ) ) );
				if( count( $getCategory ) == 0 )
				{
					$categoryName = $productdata['category'];
					$catAlias = strtolower($categoryName);					
					$this->Category->query( "insert into categories ( category_name, category_alias ) values ( '{$categoryName}' , '{$catAlias}' )" );					
					$catId = $this->Category->getLastInsertId();
					$productdata['category_id'] = $catId;
					$productdata['category_name'] = $categoryName;
				}
				else
				{
					$productdata['category_id'] = $getCategory['Category']['id'];
					$productdata['category_name'] = $productdata['category'];
				}
				
 				//echo $frizileMark['frezile_mark'];
				//$checkproductdesc	=	$this->ProductDesc->find('first', array('conditions' => array('ProductDesc.barcode' => $productdscdata['barcode'])));
				
				if(count($checkproduct) == 0)
				{
					$this->Product->saveAll($productdata);
					$productid	=	$this->Product->getLastInsertId();
					
					$productdscdata['product_id'] = $productid;
					$this->ProductDesc->saveAll($productdscdata, array('validate' => false));
					$productDescid	=	$this->ProductDesc->getLastInsertId();
					
					$productprice['product_id'] = $productid;
					$this->ProductPrice->saveAll($productprice, array('validate' => false));
					 
					
					/*  generate barcode  */
					$shiftValue = 8;
					$moveValue = 2;
					//$count = 0;
					if( $xsbarcode == '' )
					{
						$barcode = $productDescid;
						$lengthOfId = strlen($barcode);					
						$mainDiffer = $lengthOfId - $moveValue; // ? - 2 = ?
						$differ = $shiftValue - $mainDiffer; // 8 - ? = ?
						$newSpaceVake = '';
						for( $sk = 0; $sk < $differ; $sk++ ) 
						{
							if( $sk == 0 )
							{
								$newSpaceVake .= '0';
							}
							else
							{
								$newSpaceVake = $newSpaceVake.'0';
							}
						}
						$newBarcode = rand(11,99) . $newSpaceVake . $barcode;
						$newSpaceVake = '';
						$data['ProductDesc']['id'] 				= 	$productDescid;
						$data['ProductDesc']['barcode']			=	$global_barcode;
						$this->ProductDesc->saveAll( $data, array('validate' => false) ); 
						/************************Save Global barcode*********************************/
						$barcodeData['global_barcode']  = $global_barcode;
						$barcodeData['barcode'] 		= $newBarcode;
						$barcodeData['username'] 		= $this->Session->read('Auth.User.username');
						$barcodeData['added_date'] 		= date('Y-m-d H:i:s');
						$this->ProductBarcode->saveAll($barcodeData);
					} else {
						/************************Save Global barcode*********************************/
						$barcodeData['global_barcode']  = $global_barcode;
						$barcodeData['barcode'] 		= $xsbarcode;
						$barcodeData['username'] 		= $this->Session->read('Auth.User.username');
						$barcodeData['added_date'] 		= date('Y-m-d H:i:s');
						$this->ProductBarcode->saveAll($barcodeData);
					}
					/*****************************************/
					$count++;
				}
				else
				{
					$alreadycount++;
 				}
				$frizileMark	=	$this->ProductDesc->find('count', array(
															'conditions' => array('ProductDesc.frezile_mark' => $productdscdata['frezile_mark'])
										)
									);

				
			} 
			 
			$this->Session->setFlash($count.' :- SKU Inserted <br>'.$countbarcode.' :- Barcode Already Exist <br>'. $alreadycount.' :- SKU Already Exist <br>'.$frizileMark.' :- Frizile Mark <br>', 'flash_danger');
			 $this->redirect($this->referer());
		}
		else
		{
			$this->Session->setFlash('Please Insert CSV File.', 'flash_danger');
			$this->redirect($this->referer());
		}
	}
	public function test()
	{
	
		$asins = ['B0064JWMTS','B0058NRT6W','B0058NS3E4','B0058NSI0I'];
		$url = 'http://www.techdrive.biz/api-get-asin-data-xsensys.php'; 
		$curl = curl_init(); 
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FAILONERROR, true); 
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
		curl_setopt($curl, CURLOPT_POST, count($asins));
		curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($asins));  
		$result = curl_exec($curl); 						 
		$error = curl_error($curl); 
		$info = curl_getinfo($curl);
		curl_close($curl);	
		$_result = json_decode($result,true);	
		 
		print_r($_result);
	//	file_put_contents(WWW_ROOT.'logs/'.$dfile, $asin .",". $location.",". implode(";",$fb)."\n", FILE_APPEND | LOCK_EX);	
		
		exit;
	}	
		
	public function GetAsinStatus()
	{
   
 		$this->autoLayout = 'ajax';
		$this->autoRender = false;
		
		$this->loadModel('Store');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
 		$this->loadModel('ProductCsv');
		$this->loadModel('SkuFbmFba');
  		 
		if($this->data['Product']['Import_file']['name'] != '')
		{
				
			$name		= $this->Session->read('Auth.User.first_name').'_'.$this->Session->read('Auth.User.last_name').'_'.$this->request->data['Product']['Import_file']['name'];
 			$filename   =  WWW_ROOT. 'logs'.DS.$name;
			move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
			
			
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel = $objReader->load('logs/'.$name);
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
		 
			$count	= 0;
			$alreadycount	=	0;
			$countbarcode	=	0;
 			$exception 		=	[];	
 			$dfile 			= 	$this->request->data['Product']['Import_file']['name'];
			
			file_put_contents(WWW_ROOT.'logs/'.$dfile, "ASIN,Location,Final Comment\n", LOCK_EX);	
			
			for($i=2; $i<=$lastRow; $i++) 
			{ 				 	
  				$asin 	  =	trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
				$location =	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				
				$checPro = $this->ProductCsv->find('all', array(
																'conditions' => array('asin' => $asin),
																'fields' => ['id','FBM','FBAUK','FBADE','FBAES','FBAFR','FBAIT','FBANL','FBAPL','FBAUSA','FBACAN']
																)
															); 
				$fb = [];											
				if(count($checPro) > 0){
					foreach($checPro as $val){
					
						foreach(['FBM','FBAUK','FBADE','FBAES','FBAFR','FBAIT','FBANL','FBAPL','FBAUSA','FBACAN'] as $f){
						
							if(isset($val['ProductCsv'][$f]) && $val['ProductCsv'][$f] == 'Y'){
								$fb[$f] =  $f;
							}		
						}
					}
				}
 			 
			 	file_put_contents(WWW_ROOT.'logs/'.$dfile, $asin .",". $location.",". implode(";",$fb)."\n", FILE_APPEND | LOCK_EX);											
 			} 
			 
 			header('Content-Description: File Transfer');
			header('Content-Type: application/force-download');
			header('Content-Disposition: attachment; filename='.basename($dfile));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize(WWW_ROOT.'logs/'.$dfile));
			ob_clean();
			flush();
			readfile(WWW_ROOT.'logs/'.$dfile);
			exit;
		
		
		}
		else
		{
			$this->Session->setFlash('Please Insert CSV File.', 'flash_danger');
			$this->redirect($this->referer());
		}
	}
	
	public function DownloadSample()
	{
		$this->layout = '';
		$this->autoRender = false;
		if( $this->request->data['Product']['Import_type'] != '' )
		{
			if($this->request->data['Product']['Import_type'] == 'products'){
			
				App::import('Vendor', 'PHPExcel/IOFactory');
				App::import('Vendor', 'PHPExcel');  
				$objPHPExcel = new PHPExcel();     
				$objPHPExcel->setActiveSheetIndex(0);
				$getBase = Router::url('/', true);
				$uploadRemote = $getBase.'files/AddNewProduct.csv';
				$file = 'AddNewProduct.csv';
				$contenttype = "application/force-download";
				header("Content-Type: " . $contenttype);
				header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
				readfile($uploadRemote);
			
			}else{
				$strName = "DownloadSample".$this->request->data['Product']['Import_type'];	
				$className = new UploadsController();		
				$className->{"$strName"}();
			}
		}
		else
		{
			$this->Session->setflash( 'Please select file type.', 'flash_danger' );  
			$this->redirect( array( "controller" => "JijGroup/UploadProducts" ) );
		}
	}
	
	public function DownloadSampleUploadStockQuantity()
	{
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$getBase = Router::url('/', true);
		$uploadRemote = $getBase.'files/UploadStockQuantity.csv';
		$file = 'UploadStockQssuantity.csv';
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
	}
	
	public function DownloadSampleInventery()
	{
		echo "Inventery";
	}
	
	public function DownloadSampleBinLocation()
	{
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$getBase = Router::url('/', true);
		$uploadRemote = $getBase.'files/UploadLocation.csv';
		$file = 'UploadLocation.csv';
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
	}

	public function DownloadSampleaAddNewProduct()
	{
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$getBase = Router::url('/', true);
		$uploadRemote = $getBase.'files/AddNewProduct.csv';
		$file = 'AddNewProduct.csv';
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
	}

	
	/*********************************************************************************/
	
	public function UploadCsvFile()
	{
		$this->layout = '';
		$this->autoRender = false;
		$file	=	explode('.' ,$this->data['Product']['Import_file']['name']);
		$fileName = $file[0];
		$strName = "UploadCsvFile".$file[0];
		$filearray = array( 'UploadLocation', 'UploadStockQuantity','AddNewProducts' );
		if( $fileName != '' && in_array( $fileName, $filearray ))
		{
			$className = new UploadsController();		
			$msg	=	$className->{"$strName"}( $this->request->data );
			$this->Session->setflash( $msg, 'flash_success' );       
			$this->redirect( array( "controller" => "JijGroup/UploadProducts" ) );
		}
		else
		{
			$this->Session->setflash( 'Please insert valid file with valid name', 'flash_danger' );  
			$this->redirect( array( "controller" => "JijGroup/UploadProducts" ) );
		}
	}
	
	
	public function UploadCsvFileUploadLocation( $data )
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'BinLocation' );
		$this->loadModel( 'Product' );
		$this->BinLocation->query( 'TRUNCATE bin_locations' );
		
		$filename = WWW_ROOT. 'files'.DS.$data['Product']['Import_file']['name']; 
		move_uploaded_file($data['Product']['Import_file']['tmp_name'],$filename);  
		$name		=	 $data['Product']['Import_file']['name'];
	
		App::import('Vendor', 'PHPExcel/IOFactory');
		$objPHPExcel = new PHPExcel();
		$objReader= PHPExcel_IOFactory::createReader('CSV');
		$objReader->setReadDataOnly(true);
		$objPHPExcel=$objReader->load('files/'. $name );
		$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
		$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
		$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
		$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
		
		$insertIncrement = 0;
		$notinsertIncrement = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$Sku	=	$objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
				if( $Sku != '')
				{
					$getProductDetail 	=	$this->Product->find( 'first', 
											array( 'conditions' => array( 'Product.product_sku' => $Sku ),
													'fields' => array( 'ProductDesc.barcode')
													) 
												);
					if( $objWorksheet->getCellByColumnAndRow(2,$i)->getValue() < 0 )
					{
						$binlocationQty = 0;
					}
					else
					{
						$binlocationQty = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
					}
					
					$barcode						=	$getProductDetail['ProductDesc']['barcode'];
					
					$data['barcode']				=	$barcode;
					$data['bin_location']			=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
					$data['stock_by_location']		=	$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
					
					$result	=	$this->BinLocation->saveAll( $data );
					if($result)
					{
						$insertIncrement++;
					}
					else
					{
						$notinsertIncrement++;
					}
					$msg	=	'(<b>'.$insertIncrement.'</b>) Bin Location Inserted Successfully.';
				}
				else
				{
					$msg	=	'Please fill all Sku.';
				}
			}
		
		return $msg; 
	}
	
	
	 
	
	public function UploadCsvFileUploadStockQuantity( $data )
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'Product' );
		$filename = WWW_ROOT. 'files'.DS.$data['Product']['Import_file']['name']; 
		move_uploaded_file($data['Product']['Import_file']['tmp_name'],$filename);  
		$name		=	 $data['Product']['Import_file']['name'];
	
		App::import('Vendor', 'PHPExcel/IOFactory');
		$objPHPExcel = new PHPExcel();
		$objReader= PHPExcel_IOFactory::createReader('CSV');
		$objReader->setReadDataOnly(true);
		$objPHPExcel=$objReader->load('files/'. $name );
		$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
		$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
		$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
		$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
		$insertIncrement = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$sku		=	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$qty		=	$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$this->Product->updateAll(array('Product.uploaded_stock' => $qty ,  'Product.current_stock_level' => $qty  ), array('Product.product_sku' => $sku));
				$insertIncrement++;
			}
		$msg	=	'(<b>'.$insertIncrement.'</b>) Sku Updates Successfully.';
		return $msg; 
	}
	
	public function getProcessedOrder_old240216()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'OpenOrder' );
		$getProdessOrderDetails	=	$this->OpenOrder->find('all', array( 'conditions' => array( 'OpenOrder.status' => 1 ), 'fields' => array( 'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date' ) ));
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderId');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'ProcessedDate');
		$inc = 2;
		foreach($getProdessOrderDetails as $getProdessOrderDetail )
		{
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['OpenOrder']['num_order_id'] );
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $getProdessOrderDetail['OpenOrder']['date'] );
			$inc++;
		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/stockUpdate/ProcessedOrder.csv';
		$uploadRemote = $getBase.'img/stockUpdate/ProcessedOrder.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'ProcessedOrder.csv';
		
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
		
	}

public function getProcessedOrder_old200416()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		$getProdessOrderDetails	=	$this->OpenOrder->find('all', array( 'conditions' => array( 'OpenOrder.status' => 1 ), 'fields' => array( 'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date', 'OpenOrder.general_info','OpenOrder.customer_info' ) ));
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderId');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Split Order Id');       
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ProcessedDate');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'ReferenceNum');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Source');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'SubSource');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'FullName');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Country');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'SKU');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Quantity');
		$inc = 2;
		foreach($getProdessOrderDetails as $getProdessOrderDetail )
		{
			$generalInfo	= unserialize($getProdessOrderDetail['OpenOrder']['general_info']);
			$customerInfo	= unserialize($getProdessOrderDetail['OpenOrder']['customer_info']);

			$numOrderId		=	 $getProdessOrderDetail['OpenOrder']['num_order_id'];
			$orderAfterSpliting	=	 $this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' =>  $numOrderId ) ) );
	

		foreach( $orderAfterSpliting as $orderAfterSplitingIndex => $orderAfterSplitingValuer )
		{
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['OpenOrder']['num_order_id'] );
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $orderAfterSplitingValuer['MergeUpdate']['product_order_id_identify'] );
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getProdessOrderDetail['OpenOrder']['date'] );
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $generalInfo->ReferenceNum );
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->Source );
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $generalInfo->SubSource );
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->FullName );
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $customerInfo->Address->Country );
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $orderAfterSplitingValuer['MergeUpdate']['sku'] );
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, $orderAfterSplitingValuer['MergeUpdate']['quantity'] );
			$inc++;
		}
		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/upload/ProcessedOrder.csv';
		$uploadRemote = $getBase.'img/upload/ProcessedOrder.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'ProcessedOrder.csv';
		
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;
		
	}
	public function getProcessedOrder()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'ScanOrder' );
		
		$getData = $this->request->data;		
		
		$getFrom = date('Y-m-d',strtotime($getData['getFrom']));
		$getEnd  = date('Y-m-d',strtotime($getData['getEnd']. "+1 days"));
		$getStoreSelection = '';//$getData['getStoreSelection'];
	
		
		 
		$path = WWW_ROOT .'img/upload/';
		$filename = "ProcessedOrder.txt";
		$uploadRemote = $path.$filename;
		
		$header = "OrderId\tSplit Order Id\tProcessedDate\tReferenceNum\tSubSource\tFullName\tCountry\tSKU\tQuantity\tService Name\tProvider_ref_Code\tEnvelope Name\tEnvelope Cost\tOrder weight\r\n";
		file_put_contents($path.$filename,$header, LOCK_EX);
		
 		
		$sql = "SELECT OpenOrder.status,OpenOrder.num_order_id,OpenOrder.amazon_order_id,OpenOrder.date,OpenOrder.process_date,OpenOrder.customer_info,OpenOrder.sub_source,MergeUpdate.product_order_id_identify,MergeUpdate.sku,MergeUpdate.service_name,MergeUpdate.provider_ref_code,MergeUpdate.envelope_name,MergeUpdate.envelope_cost,MergeUpdate.packet_weight,SUM(ScanOrder.quantity) as QTY FROM open_orders OpenOrder
 				JOIN merge_updates MergeUpdate
					ON  OpenOrder.num_order_id = MergeUpdate.order_id AND OpenOrder.status = 1 AND OpenOrder.process_date BETWEEN '".$getFrom."' AND '".$getEnd."'
				JOIN scan_orders ScanOrder
					ON ScanOrder.split_order_id = MergeUpdate.product_order_id_identify
				GROUP BY ScanOrder.split_order_id ORDER BY OpenOrder.process_date  ";
		 
		
		$data = $this->OpenOrder->query($sql);
		
		$inc = 0;
		$content = '';
		
		foreach($data as $getProdessOrderDetail )
		{
			 	$inc++;
			 
				$customerInfo	= unserialize($getProdessOrderDetail['OpenOrder']['customer_info']);
				
				$quantity		=	$getProdessOrderDetail['0']['QTY'];
				$orderid		=	$getProdessOrderDetail['OpenOrder']['num_order_id'];
				$suborderid		=	$getProdessOrderDetail['MergeUpdate']['product_order_id_identify'];				
				$processdate	=	$getProdessOrderDetail['OpenOrder']['process_date'];
				
				$refnum			=	$getProdessOrderDetail['OpenOrder']['amazon_order_id'];
				
				$subsource		=	$getProdessOrderDetail['OpenOrder']['sub_source'];
				$fullname		=	$customerInfo->Address->FullName;
				$country		=	$customerInfo->Address->Country;
				$sku 			=	$getProdessOrderDetail['MergeUpdate']['sku'];
				$quantity		=	$quantity;
				$servicename	=	$getProdessOrderDetail['MergeUpdate']['service_name'];
				$servicecode	=	$getProdessOrderDetail['MergeUpdate']['provider_ref_code'];
				$envelopname	=	$getProdessOrderDetail['MergeUpdate']['envelope_name'];
				$envelopcost	=	$getProdessOrderDetail['MergeUpdate']['envelope_cost'];
				$packit_weight	=	$getProdessOrderDetail['MergeUpdate']['packet_weight'];
				
				$content .= $orderid."\t".$suborderid."\t".$processdate."\t".$refnum."\t".$subsource."\t".$fullname."\t".$country."\t".$sku."\t".$quantity."\t".$servicename."\t".$servicecode."\t".$envelopname."\t".$envelopcost."\t".$packit_weight."\r\n";
				
				if($inc % 1000 == 0)
				{
					file_put_contents($path.$filename, $content, FILE_APPEND | LOCK_EX);
					$content = '';					
					$inc  ;
				}
			 
		}	
		file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);
		
		
		echo Router::url('/', true).'Uploads/downloadProcessedOrder';	 
	 
		exit;
	}
	
	public function downloadProcessedOrder()
	{
	
	 	$path = WWW_ROOT .'img/upload/';
		$filename = "ProcessedOrder.txt";
		header('Content-Description: File Transfer');
		header('Content-Type: text/plain');
		header('Content-Disposition: attachment; filename='.basename($filename));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($path.$filename));
		ob_clean();
		flush();
		readfile($path.$filename); 
		
	}
	
	public function getProcessedOrder_14MAY18()
	{
		
		 
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'ScanOrder' );
		
		$getData = $this->request->data;	 	
		
		$getFrom = date('Y-m-d',strtotime($getData['getFrom']));
		$getEnd = date('Y-m-d',strtotime($getData['getEnd']. "+1 days"));
		$getStoreSelection = '';//$getData['getStoreSelection'];
		 
		if( isset($getStoreSelection) && $getStoreSelection != '' )
		{
			$param = array(
			
				'conditions' => array( 
					'OpenOrder.status' => 1,
					'OpenOrder.process_date >=' => $getFrom,
					'OpenOrder.process_date <=' => $getEnd,
					'OpenOrder.sub_source' => $getStoreSelection
				),
				'fields' => array( 
					'OpenOrder.sub_source','OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date','OpenOrder.process_date'
				)
			
			);
		}
		else
		{
			
			$param = array(
			
				'conditions' => array( 
					'OpenOrder.status' => 1,
					'OpenOrder.process_date >=' => $getFrom,
					'OpenOrder.process_date <=' => $getEnd
				),
				'fields' => array( 
					'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date','OpenOrder.process_date'
				)
			
			);
			
		}
		
		$getProdessOrderDetails	=	$this->OpenOrder->find('all', $param );
	 
		
		$path = WWW_ROOT .'img/upload/';
		$uploadRemote = $path.'ProcessedOrder.txt';
		$filename = "ProcessedOrder.txt";
		$header = "OrderId\tSplit Order Id\tProcessedDate\tReferenceNum\tSource\tSubSource\tFullName\tCountry\tSKU\tQuantity\tService Name\tProvider_ref_Code\tEnvelope Name\tEnvelope Cost\tService Cost\r\n";
		file_put_contents($path.$filename,$header, LOCK_EX);
		$inc = 2;
		foreach($getProdessOrderDetails as $getProdessOrderDetail )
		{
			$generalInfo	= unserialize($getProdessOrderDetail['OpenOrder']['general_info']);
			$customerInfo	= unserialize($getProdessOrderDetail['OpenOrder']['customer_info']);

			$numOrderId		=	 $getProdessOrderDetail['OpenOrder']['num_order_id'];
			$orderAfterSpliting	=	 $this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' =>  $numOrderId ) ) );
	

			foreach( $orderAfterSpliting as $orderAfterSplitingIndex => $orderAfterSplitingValuer )
			{
				$SplitOrderIDInScan	= $orderAfterSplitingValuer['MergeUpdate']['product_order_id_identify'];
				$getSumOfQty	=	$this->ScanOrder->find( 'all', array( 
															'conditions' => array( 
															'ScanOrder.split_order_id' => $SplitOrderIDInScan 
															),
															'fields' => array('SUM(quantity) as QTY') 
														  ) 
														);
				
				$quantity		=	$getSumOfQty['0']['0']['QTY'];
				
				$orderid		=	$getProdessOrderDetail['OpenOrder']['num_order_id'];
				$suborderid		=	$orderAfterSplitingValuer['MergeUpdate']['product_order_id_identify'];
				$processdate	=	$getProdessOrderDetail['OpenOrder']['process_date'];
				$refnum			=	$generalInfo->ReferenceNum;
				$source			=	$generalInfo->Source;
				$subsource		=	$generalInfo->SubSource;
				$fullname		=	$customerInfo->Address->FullName;
				$country		=	$customerInfo->Address->Country;
				$sku 			=	$orderAfterSplitingValuer['MergeUpdate']['sku'];
				$quantity		=	$quantity;
				$servicename	=	$orderAfterSplitingValuer['MergeUpdate']['service_name'];
				$servicecode	=	$orderAfterSplitingValuer['MergeUpdate']['provider_ref_code'];
				$envelopname	=	$orderAfterSplitingValuer['MergeUpdate']['envelope_name'];
				$envelopcost	=	$orderAfterSplitingValuer['MergeUpdate']['envelope_cost'];
				$inc++;
				$content = $orderid."\t".$suborderid."\t".$processdate."\t".$refnum."\t".$source."\t".$subsource."\t".$fullname."\t".$country."\t".$sku."\t".$quantity."\t".$servicename."\t".$servicecode."\t".$envelopname."\t".$envelopcost."\r\n";
				file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);
				
				
			}
		}
		header('Content-Description: File Transfer');
		header('Content-Type: text/plain');
		header('Content-Disposition: attachment; filename='.basename($filename));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($path.$filename));
		ob_clean();
		flush();
		readfile($path.$filename);
		exit;
		
	}
	public function getProcessedOrder_171117()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'ScanOrder' );
		
		$getData = $this->request->data;		
		
		echo $getFrom = date('Y-m-d',strtotime('11/15/2017'));
		echo "<br>";
		echo $getEnd = date('Y-m-d',strtotime('11/17/2017'. "+1 days"));
		$getStoreSelection = $getData['getStoreSelection'];
		
		if( isset($getStoreSelection) && $getStoreSelection != '' )
		{
			$param = array(
			
				'conditions' => array( 
					'OpenOrder.status' => 1,
					'OpenOrder.process_date >=' => $getFrom,
					'OpenOrder.process_date <=' => $getEnd,
					'OpenOrder.sub_source' => $getStoreSelection
				),
				'fields' => array( 
					'OpenOrder.sub_source','OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date','OpenOrder.process_date'
				)
			
			);
		}
		else
		{
			
			$param = array(
			
				'conditions' => array( 
					'OpenOrder.status' => 1,
					'OpenOrder.process_date >=' => $getFrom,
					'OpenOrder.process_date <=' => $getEnd
				),
				'fields' => array( 
					'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date','OpenOrder.process_date'
				)
			
			);
			
		}
		
		$getProdessOrderDetails	=	$this->OpenOrder->find('all', $param );
		pr($getProdessOrderDetails);
		//exit;
		//$getProdessOrderDetails	=	$this->OpenOrder->find('all', array( 'conditions' => array( 'OpenOrder.status' => 1 ), 'fields' => array( 'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date', 'OpenOrder.general_info','OpenOrder.customer_info' ) ));
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderId');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Split Order Id');       
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ProcessedDate');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'ReferenceNum');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Source');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'SubSource');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'FullName');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Country');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'SKU');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Quantity');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Service Name');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Provider_ref_Code');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Envelope Name');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Envelope Cost');
		$objPHPExcel->getActiveSheet()->setCellValue('O1', 'Service Cost');
		
		$inc = 2;
		foreach($getProdessOrderDetails as $getProdessOrderDetail )
		{
			$generalInfo	= unserialize($getProdessOrderDetail['OpenOrder']['general_info']);
			$customerInfo	= unserialize($getProdessOrderDetail['OpenOrder']['customer_info']);

			$numOrderId		=	 $getProdessOrderDetail['OpenOrder']['num_order_id'];
			$orderAfterSpliting	=	 $this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' =>  $numOrderId ) ) );
	

			foreach( $orderAfterSpliting as $orderAfterSplitingIndex => $orderAfterSplitingValuer )
			{
				$SplitOrderIDInScan	= $orderAfterSplitingValuer['MergeUpdate']['product_order_id_identify'];
				$getSumOfQty	=	$this->ScanOrder->find( 'all', array( 
															'conditions' => array( 
															'ScanOrder.split_order_id' => $SplitOrderIDInScan 
															),
															'fields' => array('SUM(quantity) as QTY') 
														  ) 
														);
				
				$quantity	=	$getSumOfQty['0']['0']['QTY'];
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['OpenOrder']['num_order_id'] );
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $orderAfterSplitingValuer['MergeUpdate']['product_order_id_identify'] );
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getProdessOrderDetail['OpenOrder']['process_date'] );
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $generalInfo->ReferenceNum );
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->Source );
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $generalInfo->SubSource );
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->FullName );
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $customerInfo->Address->Country );
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $orderAfterSplitingValuer['MergeUpdate']['sku'] );
				//$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, $orderAfterSplitingValuer['MergeUpdate']['quantity'] );
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, $quantity );
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, $orderAfterSplitingValuer['MergeUpdate']['service_name'] );
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, $orderAfterSplitingValuer['MergeUpdate']['provider_ref_code'] );
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, $orderAfterSplitingValuer['MergeUpdate']['envelope_name'] );
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, $orderAfterSplitingValuer['MergeUpdate']['envelope_cost'] );
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$inc, '' );
				$inc++;
			}
		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/upload/ProcessedOrder.csv';
		$uploadRemote = $getBase.'img/upload/ProcessedOrder.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'ProcessedOrder.csv';
		
		//$contenttype = "application/force-download";
		//header("Content-Type: " . $contenttype);
		//header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		//readfile($uploadRemote);
		
		echo $uploadRemote;
		
		exit;
		
	}

	public function getOrdersDownload()
	{
		$this->layout = 'index';

	}

	public function getOpenOrder_old200416()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		$getProdessOrderDetails	=	$this->OpenOrder->find('all', array( 'conditions' => array( 'OpenOrder.status' => 0, 'OpenOrder.linn_fetch_orders !=' => 0), 'fields' => array( 'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info') ));
	
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderId');   
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Split OrderId');  
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ReceivedDate');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'ReferenceNum');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Source');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'SubSource');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'FullName');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Country');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'SKU');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Quantity');
		$inc = 2;
		foreach($getProdessOrderDetails as $getProdessOrderDetail )
		{

			$generalInfo	= unserialize($getProdessOrderDetail['OpenOrder']['general_info']);
			$customerInfo	= unserialize($getProdessOrderDetail['OpenOrder']['customer_info']);
			$orderReciveDate	=	$generalInfo->ReceivedDate;

			$numOrderId		=	 $getProdessOrderDetail['OpenOrder']['num_order_id'];
			$orderAfterSpliting	=	 $this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' =>  $numOrderId ) ) );
	
		if( !empty( $orderAfterSpliting ) )
{
		foreach( $orderAfterSpliting as $orderAfterSplitingIndex => $orderAfterSplitingValuer )
		{
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['OpenOrder']['num_order_id'] );
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $orderAfterSplitingValuer['MergeUpdate']['product_order_id_identify'] );
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $orderReciveDate );
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $generalInfo->ReferenceNum );
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->Source );
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $generalInfo->SubSource );
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->FullName );
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $customerInfo->Address->Country );
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $orderAfterSplitingValuer['MergeUpdate']['sku'] );
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, $orderAfterSplitingValuer['MergeUpdate']['quantity'] );
			$inc++;
		}
}
else
{
$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['OpenOrder']['num_order_id'] );
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, '' );
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getProdessOrderDetail['OpenOrder']['date'] );
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $generalInfo->ReferenceNum );
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->Source );
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $generalInfo->SubSource );
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->FullName );
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $customerInfo->Address->Country );
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, '' );
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, '' );
			$inc++;
}

		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/upload/OpenOrder.csv';
		$uploadRemote = $getBase.'img/upload/OpenOrder.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'OpenOrder.csv';
		
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;

	}
	
	public function getOpenOrder()
	{
		
		$this->layout = '';
		$this->autoRender = false;
		
		$getData = $this->request->data;		
		
		$getFrom = date('Y-m-d',strtotime($getData['getFrom']));
		$getEnd = date('Y-m-d',strtotime($getData['getEnd']. "+1 days"));
		$getStoreSelection = $getData['getStoreSelection'];
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		
		if( isset($getStoreSelection) && $getStoreSelection != '' )
		{
			$param = array(
			
				'conditions' => array( 
					'OpenOrder.status' => 0, 
					'OpenOrder.linn_fetch_orders !=' => 0,				
					'OpenOrder.date >=' => $getFrom,
					'OpenOrder.date <=' => $getEnd,
					'OpenOrder.sub_source' => $getStoreSelection
				),
				'fields' => array( 
					'OpenOrder.sub_source','OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date'
				)
			
			);
		}
		else
		{
			
			$param = array(
			
				'conditions' => array( 
					'OpenOrder.status' => 0, 
					'OpenOrder.linn_fetch_orders !=' => 0,				
					'OpenOrder.date >=' => $getFrom,
					'OpenOrder.date <=' => $getEnd
				),
				'fields' => array( 
					'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date'
				)
			
			);
			
		}
		
		$getProdessOrderDetails	=	$this->OpenOrder->find('all', $param );		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderId');   
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Split OrderId');  
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ReceivedDate');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'ReferenceNum');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Source');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'SubSource');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'FullName');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Country');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'SKU');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Quantity');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Service Provider');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Servece code');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', 'OpenOrderDate');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Move Date');
		
		$inc = 2;
		foreach($getProdessOrderDetails as $getProdessOrderDetail )
		{

			$generalInfo	= unserialize($getProdessOrderDetail['OpenOrder']['general_info']);
			$customerInfo	= unserialize($getProdessOrderDetail['OpenOrder']['customer_info']);
			$orderReciveDate	=	$generalInfo->ReceivedDate;

			$numOrderId		=	 $getProdessOrderDetail['OpenOrder']['num_order_id'];
			$orderAfterSpliting	=	 $this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' =>  $numOrderId ) ) );
			if( !empty( $orderAfterSpliting ) )
			{
					foreach( $orderAfterSpliting as $orderAfterSplitingIndex => $orderAfterSplitingValuer )
					{
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['OpenOrder']['num_order_id'] );
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $orderAfterSplitingValuer['MergeUpdate']['product_order_id_identify'] );
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $orderReciveDate );
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $generalInfo->ReferenceNum );
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->Source );
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $generalInfo->SubSource );
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->FullName );
						$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $customerInfo->Address->Country );
						$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $orderAfterSplitingValuer['MergeUpdate']['sku'] );
						$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, $orderAfterSplitingValuer['MergeUpdate']['quantity'] );
						$objPHPExcel->getActiveSheet()->setCellValue('K'.$inc, $orderAfterSplitingValuer['MergeUpdate']['service_provider'] );
						$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, $orderAfterSplitingValuer['MergeUpdate']['provider_ref_code'] );
						$objPHPExcel->getActiveSheet()->setCellValue('M'.$inc, $getProdessOrderDetail['OpenOrder']['date']);
						$objPHPExcel->getActiveSheet()->setCellValue('N'.$inc, $orderAfterSplitingValuer['MergeUpdate']['order_date'] );
						$inc++;
					}
			}
			else
			{
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['OpenOrder']['num_order_id'] );
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, '' );
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $getProdessOrderDetail['OpenOrder']['date'] );
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $generalInfo->ReferenceNum );
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->Source );
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $generalInfo->SubSource );
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->FullName );
						$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $customerInfo->Address->Country );
						$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, '' );
						$objPHPExcel->getActiveSheet()->setCellValue('J'.$inc, '' );
						$objPHPExcel->getActiveSheet()->setCellValue('L'.$inc, '' );
						$inc++;
			}

		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/upload/OpenOrder.csv';
		$uploadRemote = $getBase.'img/upload/OpenOrder.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'OpenOrder.csv';
		
		//$contenttype = "application/force-download";
		//header("Content-Type: " . $contenttype);
		//header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		//readfile($uploadRemote);
		echo $uploadRemote;
		exit;

	}

	public function getUnprepareOrder()
	{


$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'UnprepareOrder' );
		$getProdessOrderDetails	=	$this->UnprepareOrder->find('all', array( 'fields' => array( 'UnprepareOrder.status', 'UnprepareOrder.num_order_id','UnprepareOrder.date','UnprepareOrder.general_info','UnprepareOrder.customer_info') ));
	
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderId');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'ReceivedDate');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ReferenceNum');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Source');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'SubSource');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'FullName');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Country');
		$inc = 2;
		foreach($getProdessOrderDetails as $getProdessOrderDetail )
		{

			$generalInfo	= unserialize($getProdessOrderDetail['UnprepareOrder']['general_info']);
			$customerInfo	= unserialize($getProdessOrderDetail['UnprepareOrder']['customer_info']);


			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['UnprepareOrder']['num_order_id'] );
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $generalInfo->ReceivedDate );
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $generalInfo->ReferenceNum );
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $generalInfo->Source );
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->SubSource );
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $customerInfo->Address->FullName );
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->Country );
			$inc++;
		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/upload/UnprepareOrder.csv';
		$uploadRemote = $getBase.'img/upload/UnprepareOrder.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'UnprepareOrder.csv';
		
		//$contenttype = "application/force-download";
		//header("Content-Type: " . $contenttype);
		//header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		//readfile($uploadRemote);
		echo $uploadRemote;
		exit;


	}

	public function getCancelOrder_old200416()
	{
	

$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );

		$getProdessOrderDetails	=	$this->OpenOrder->find('all', array( 'conditions' => array( 'OpenOrder.status' => 2), 'fields' => array( 'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info') ));
	
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderId');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'ReceivedDate');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ReferenceNum');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Source');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'SubSource');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'FullName');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Country');
		$inc = 2;
		foreach($getProdessOrderDetails as $getProdessOrderDetail )
		{

			$generalInfo	= unserialize($getProdessOrderDetail['OpenOrder']['general_info']);
			$customerInfo	= unserialize($getProdessOrderDetail['OpenOrder']['customer_info']);

			$numOrderId		=	 $getProdessOrderDetail['OpenOrder']['num_order_id'];
		
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['OpenOrder']['num_order_id'] );
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $getProdessOrderDetail['OpenOrder']['date'] );
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $generalInfo->ReferenceNum );
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $generalInfo->Source );
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->SubSource );
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $customerInfo->Address->FullName );
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->Country );
			$inc++;
			
		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/upload/CancelOrder.csv';
		$uploadRemote = $getBase.'img/upload/CancelOrder.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'CancelOrder.csv';
		
		$contenttype = "application/force-download";
		header("Content-Type: " . $contenttype);
		header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		readfile($uploadRemote);
		exit;

	}
	
	public function getCancelOrder()
	{

		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'MergeUpdate' );
		$getData = $this->request->data;		
		
		$getFrom = date('Y-m-d',strtotime($getData['getFrom']));
		$getEnd = date('Y-m-d',strtotime($getData['getEnd']. "+1 days"));
		$getStoreSelection = $getData['getStoreSelection'];
		
		if( isset($getStoreSelection) && $getStoreSelection != '' )
		{
			$param = array(
			
				'conditions' => array( 
					'OpenOrder.status' => 2,
					'OpenOrder.date >=' => $getFrom,
					'OpenOrder.date <=' => $getEnd,
					'OpenOrder.sub_source' => $getStoreSelection
				),
				'fields' => array( 
					'OpenOrder.sub_source','OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date'
				)
			
			);
		}
		else
		{
			
			$param = array(
			
				'conditions' => array( 
					'OpenOrder.status' => 2,
					'OpenOrder.date >=' => $getFrom,
					'OpenOrder.date <=' => $getEnd
				),
				'fields' => array( 
					'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info','OpenOrder.date'
				)
			
			);
			
		}
		
		$getProdessOrderDetails	=	$this->OpenOrder->find('all', $param );
		
		//$getProdessOrderDetails	=	$this->OpenOrder->find('all', array( 'conditions' => array( 'OpenOrder.status' => 2), 'fields' => array( 'OpenOrder.status', 'OpenOrder.num_order_id','OpenOrder.date','OpenOrder.general_info','OpenOrder.customer_info') ));
	
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderId');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'ReceivedDate');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ReferenceNum');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Source');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'SubSource');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'FullName');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Country');
		$inc = 2;
		foreach($getProdessOrderDetails as $getProdessOrderDetail )
		{

			$generalInfo	= unserialize($getProdessOrderDetail['OpenOrder']['general_info']);
			$customerInfo	= unserialize($getProdessOrderDetail['OpenOrder']['customer_info']);

			$numOrderId		=	 $getProdessOrderDetail['OpenOrder']['num_order_id'];
		
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $getProdessOrderDetail['OpenOrder']['num_order_id'] );
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $getProdessOrderDetail['OpenOrder']['date'] );
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $generalInfo->ReferenceNum );
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $generalInfo->Source );
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $generalInfo->SubSource );
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $customerInfo->Address->FullName );
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $customerInfo->Address->Country );
			$inc++;
			
		}
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		
		$uploadUrl = WWW_ROOT .'img/upload/CancelOrder.csv';
		$uploadRemote = $getBase.'img/upload/CancelOrder.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'CancelOrder.csv';
		
		//$contenttype = "application/force-download";
		//header("Content-Type: " . $contenttype);
		//header("Content-Disposition: attachment; filename=\"" . basename($file) . "\";");
		//readfile($uploadRemote);
		
		echo $uploadRemote;
		exit;

	}
	
	public function serviceCounterBackUp()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'ServiceCounter' );
		$userId	=	$this->request->data['userId'];
		//$servicePost = $this->request->data['servicePost'];
		$serializeData = $this->ServiceCounter->find( 'all' );
		if( count($serializeData) > 0 )
		{
			$manifest = serialize( $serializeData );		
			$this->loadModel('ManifestBackup');
			$data['ManifestBackup']['manifest_bk'] = $manifest;
			$data['ManifestBackup']['service_provider'] = 'All';
			$data['ManifestBackup']['date'] = date('Y-m-d');
			$data['ManifestBackup']['user_id'] = $userId;
			$this->ManifestBackup->saveAll( $data );
		}
		
	}
	
	public function getStores()
	{
		
		$this->loadModel( 'Store' );		
		$this->layout = '';
		$this->autoRender = false;
		
		$sendStores = json_decode(json_encode($this->Store->find('list' , array( 'conditions' => array( 'Store.status' => 1 ) , 'fields' => array( 'Store.store_name', 'Store.store_name' ) ))),0);		
	return $sendStores;
	}
	
	public function updateBarcode()
		{
				$this->autoLayout = 'ajax';
				$this->autoRender = false;
				$this->loadModel('Product');
				$this->loadModel('ProductDesc');
				
				$name = 'GenerateNewBarcode.csv';
				App::import('Vendor', 'PHPExcel/IOFactory');
				
				$objPHPExcel = new PHPExcel();
				$objReader= PHPExcel_IOFactory::createReader('CSV');
				$objReader->setReadDataOnly(true);				
				$objPHPExcel=$objReader->load('img/upload/GenerateNewBarcode.csv');
				$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
				$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
				$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
				$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
				$countInserted = 0;
				$countNotInserted = 0;
				$shiftValue = 8;
				$moveValue = 2;
				$count = 0;
				for($i=2;$i<=$lastRow;$i++) 
				{
					$sku		=	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
					$productDetail = $this->Product->find('first', array( 'conditions' => array( 'Product.product_sku' =>  $sku ) ) );
					
					$barcode = $productDetail['ProductDesc']['id'];
					$lengthOfId = strlen($barcode);					
					$mainDiffer = $lengthOfId - $moveValue; // ? - 2 = ?
					$differ = $shiftValue - $mainDiffer; // 8 - ? = ?
					$newSpaceVake = '';
					for( $sk = 0; $sk < $differ; $sk++ ) 
					{
						if( $sk == 0 )
						{
							$newSpaceVake .= '0';
						}
						else
						{
							$newSpaceVake = $newSpaceVake.'0';
						}
					}
					$newBarcode = rand(11,99) . $newSpaceVake . $barcode;
					$newSpaceVake = '';
					$data['ProductDesc']['id'] 				= 	$productDetail['ProductDesc']['id'];
					$data['ProductDesc']['barcode']			=	$newBarcode;
					$this->ProductDesc->saveAll( $data, array('validate' => false) ); 
					$count++;
				}
				echo $count;
			exit;
			
		
		}
		
    public function getSellReport()
    {
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'ScanOrder' );
	
		$getData = $this->request->data;
		
		$getFrom = date('Y-m-d',strtotime($getData['getFrom']));
		$getEnd = date('Y-m-d',strtotime($getData['getEnd']));
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel');  
		
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'SKU');     
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Barcode');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Quantity');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Date');
		
		$param = array(
			'conditions' => array( 
					'ScanOrder.scan_date >=' => $getFrom,
					'ScanOrder.scan_date <=' => $getEnd
				),
				'fields' => array( 'sum(ScanOrder.quantity)   AS Quantity' , 'ScanOrder.id, ScanOrder.split_order_id, ScanOrder.sku, ScanOrder.barcode', 'ScanOrder.scan_date'),
				'group' => array('ScanOrder.sku, ScanOrder.scan_date'),
				'order'=> array( 'ScanOrder.scan_date ASC' )
			);
		
		$soldSkus = $this->ScanOrder->find( 'all', $param );
		
		$inc = 2;
		foreach( $soldSkus as $soldSku )
		{
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $soldSku['ScanOrder']['sku']);     
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $soldSku['ScanOrder']['barcode']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $soldSku[0]['Quantity']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $soldSku['ScanOrder']['scan_date']);
			$inc++;
		}
		
		$objPHPExcel->getActiveSheet(0);
		$getBase = Router::url('/', true);
		$uploadUrl = WWW_ROOT .'img/daily_report/CustomSellReport.csv';
		$uploadRemote = $getBase.'img/daily_report/CustomSellReport.csv';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->save($uploadUrl);
		$file = 'CustomSellReport.csv';
		
		echo $uploadRemote;
		exit;
	}
	
	//sku detailed report section
	public function getSku()
	{
		$this->layout = 'index';
	}
	
	public function processForDetailedSku()
	{
		
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'CheckIn' );
		$this->loadModel( 'InventoryRecord' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		
		//sheet generation
		App::import('Vendor', 'PHPExcel/IOFactory');
		App::import('Vendor', 'PHPExcel'); 
		$objPHPExcel = new PHPExcel();     
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'OrderID'); 
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Action');   
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Sku');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Barcode');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Current Stock');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Quantity');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'After Manipulation');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Status');  
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Date');
		
		$inputSku 	= $this->request->data['inputSku'];
		$getFrom 	= date('Y-m-d' ,strtotime($this->request->data['getFrom']));
		$getEnd 	= date('Y-m-d' ,strtotime($this->request->data['getEnd'] . "+1 days"));
		
		$paramHostory = array(
		
			'conditions' => array(
			
				'InventoryRecord.sku' => $inputSku,
				'InventoryRecord.date >=' => $getFrom,
				'InventoryRecord.date <=' => $getEnd,
			
			)
		
		);
		
		//Processing now
		$getSkuDetailedReport = json_decode(json_encode($this->InventoryRecord->find( 'all', $paramHostory )),0);
		
		if( count( $getSkuDetailedReport ) > 0 )		
		{
			
			//sheet setup
			$inc = 2;
			foreach( $getSkuDetailedReport as $inventoryRow )
			{
				if( $inventoryRow->InventoryRecord->action_type == '' )
				{
					$inventoryRow->InventoryRecord->action_type = 'N/A / Old Data';
				}
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$inc, $inventoryRow->InventoryRecord->split_order_id);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$inc, $inventoryRow->InventoryRecord->action_type);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$inc, $inventoryRow->InventoryRecord->sku);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$inc, $inventoryRow->InventoryRecord->barcode);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$inc, $inventoryRow->InventoryRecord->currentStock);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$inc, $inventoryRow->InventoryRecord->quantity);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$inc, $inventoryRow->InventoryRecord->after_maniplation);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$inc, $inventoryRow->InventoryRecord->status);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$inc, $inventoryRow->InventoryRecord->date);
				$inc++;
			}
			
			//report setup
			$objPHPExcel->getActiveSheet(0);
			$getBase = Router::url('/', true);
			$uploadUrl = WWW_ROOT .'img/daily_report/sku_dtailed_report_by_date.csv';
			$uploadRemote = $getBase.'img/daily_report/sku_dtailed_report_by_date.csv';
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
			$objWriter->save($uploadUrl);
			$file = 'sku_dtailed_report_by_date.csv';
			
			echo $uploadRemote;
			exit;
			
		}
		else
		{
			echo "0";
			exit;
		}
		
		
		
	}
	
	public function updateCategory()
		{
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('Product');
			$this->loadModel('ProductDesc');
			$this->loadModel( 'Category' );
		
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/Category_update.csv');
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			$notFound = 0;
			$count = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$sku	=	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$category = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				
				$getProductDetail	=	$this->Product->find('first', array( 'conditions'=> array( 'Product.product_sku' =>  $sku) ) );
				
				$getCategory = $this->Category->find( 'first' , array( 'conditions' => array( 'Category.category_name' => $category ) ) );
				
				if( !empty( $getProductDetail ))
				{
					if( count( $getCategory ) == 0 )
					{
						$categoryName = $category;
						$catAlias = strtolower($categoryName);					
						$this->Category->query( "insert into categories ( category_name, category_alias ) values ( '{$categoryName}' , '{$catAlias}' )" );					
						$catId = $this->Category->getLastInsertId();
						$productdata['id'] 				= $getProductDetail['Product']['id'];
						$productdata['category_id'] 	= $catId;
						$productdata['category_name'] 	= $categoryName;
					}
					else
					{
						$categoryName 					= $getCategory['Category']['category_name'];
						$productdata['Product']['id'] 				= $getProductDetail['Product']['id'];
						$productdata['Product']['category_id'] 		= $getCategory['Category']['id'];
						$productdata['Product']['category_name'] 	= $categoryName;
					}
					$this->Product->saveAll($productdata, array('validate' => false));
					$count++;
				}
				else
				{
					$notFound++;
				}
			}
			echo "Inserted >> ".$count."<br> NotFound >> ".$notFound; 
		}
		
		
		public function uploadRegisteredTrackingNumber()
		{
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('RegisteredNumber');
			
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/RegisteredNumbetPostnl/POSTNL_Barcodes_011217.csv');
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			$j = 0; $k = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$regData['reg_number']			=	 	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$regData['reg_baecode_number'] 	= 		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$regData['date']				= 		date('Y-m-d H:i:s');
				$chechTrackedID = $this->RegisteredNumber->find( 'first' , array( 'conditions' => array( 'RegisteredNumber.reg_number' => $regData['reg_baecode_number'] ) ) );
				if(count($chechTrackedID) == 0)
				{	
					$j++;
					$this->RegisteredNumber->saveAll( $regData );
				} else { 
					$k++;
				}
			}	
			echo $j."Tracking Id Inserted.".$k." Id Already Inserted";
		}
		
		public function uploadSubCategory()
		{
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('Product');
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/cat_subcat/SubCategory.csv');
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			$j = 0; $k = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$sku							=	 	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$subCategory 					= 		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$checkProduct = $this->Product->find( 'first' , array( 'conditions' => array( 'Product.product_sku' => $sku ) ) );
				
				if(count($checkProduct) == 4)
				{	
					$this->Product->updateAll(array('Product.sub_category' => "'".$subCategory."'" ), array('Product.product_sku' => $sku));
					$j++;
				} else { 
					$k++;
				}
			}
			echo $j.'Updated'.$k.'Not found';	
		}
		
		
		public function updatepoprice()
		{
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('PurchaseOrder');
			
			App::import('Vendor', 'PHPExcel/IOFactory');
			
			$objPHPExcel = new PHPExcel();
			$objReader= PHPExcel_IOFactory::createReader('CSV');
			$objReader->setReadDataOnly(true);				
			$objPHPExcel=$objReader->load('files/PurchaseOrderPriceUpdate/Other PO 2.csv');
			$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
			$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
			$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
			$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
			$j = 0; $k = 0;
			for($i=2;$i<=$lastRow;$i++) 
			{
				$po_name			=	 	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$purchase_sku 	    = 		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$price 	    		= 		$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
				//echo $po_name.'>>>>>'.$purchase_sku.'>>>>>'.$price."<br>";
				$chechposku = $this->PurchaseOrder->find( 'first' , array( 'conditions' => array('PurchaseOrder.po_name'=>$po_name,'PurchaseOrder.purchase_sku'=>$purchase_sku)));
				if(count($chechposku) == 1)
				{	
					$j++;
					$this->PurchaseOrder->updateAll( array('PurchaseOrder.price'=>$price), array('PurchaseOrder.po_name'=>$po_name,'PurchaseOrder.purchase_sku'=>$purchase_sku) );
				} else { 
					$k++;
				}
			}	
			echo $j."Po Sku Price Updated.".$k."Po Sku Price inserted.";
		}
		
		public function assignUserBulk()
		{
			$this->layout = "index";
		}
		
		public function UploadsCsvFilesForAssignUser()
		{
		
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('Product');
			$this->loadModel('ProductDesc');
			
			$this->data['Product']['Import_file']['name'];
			if($this->data['Product']['Import_file']['name'] != '')
				{
					$filename = WWW_ROOT. 'files'.DS.'assign_user_file'.DS.'uploaded'.DS.$this->request->data['Product']['Import_file']['name']; 
					move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
					$name		=	 $this->request->data['Product']['Import_file']['name'];
					App::import('Vendor', 'PHPExcel/IOFactory');
			
						$objPHPExcel = new PHPExcel();
						$objReader= PHPExcel_IOFactory::createReader('CSV');
						$objReader->setReadDataOnly(true);				
						$objPHPExcel=$objReader->load('files/assign_user_file/uploaded/'.$name);
						$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
						$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
						$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
						$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
						$count = 0; $k = 0;
						for($i=2;$i<=$lastRow;$i++) 
						{	
							$sku							=	 	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
							$listing_user 					= 		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
							$purchase_user 					= 		$objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
							echo $sku.'>>>>>>>>'.$listionUser.'>>>>>>>>'.$purchaseUser;
							echo "<br>";
							$checkProduct 	= $this->Product->find( 'first' , array( 'conditions' => array( 'Product.product_sku' => $sku ) ) );
							echo count($checkProduct);
							echo "<br>";
							$barcode 		= $checkProduct['ProductDesc']['barcode'];
							if(count($checkProduct) == 4){	
								$this->ProductDesc->updateAll(array('ProductDesc.user_id'=>$listing_user,'ProductDesc.purchase_user_id'=>$purchase_user),array('Product.product_sku' => $sku));
								$count++;
							} else { 
								$k++;
							}
							
						}
				}
				
			$this->Session->setFlash($count.' :- SKU Updated.'.$k.' :- SKU Not Update.', 'flash_danger');		
			$this->redirect($this->referer());
		}
		
	public function updateBulkTitle()
	{
		$this->layout = "index";
	}
	
	public function UploadsCsvFilesForUpdateTitle()
	{
			$this->autoLayout = 'ajax';
			$this->autoRender = false;
			$this->loadModel('Product');
			$this->loadModel('ProductDesc');
			
			$firstName 	= ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
			$lastName 	= ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
			$u_name		= strtolower($firstName).'_'.strtolower($lastName).'_'.date('Y-m-d_H:i:s');
			
			$this->data['Product']['Import_file']['name'];
			if($this->data['Product']['Import_file']['name'] != '')
				{
					$filename = WWW_ROOT. 'files'.DS.'update_title'.DS.'uploaded'.DS.$u_name.'_'.$this->request->data['Product']['Import_file']['name']; 
					move_uploaded_file($this->request->data['Product']['Import_file']['tmp_name'],$filename);  
					$name		=	 $this->request->data['Product']['Import_file']['name'];
					App::import('Vendor', 'PHPExcel/IOFactory');
			
						$objPHPExcel = new PHPExcel();
						$objReader= PHPExcel_IOFactory::createReader('CSV');
						$objReader->setReadDataOnly(true);				
						$objPHPExcel=$objReader->load('files/update_title/uploaded/'.$u_name.'_'.$name);
						$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
						$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
						$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
						$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
						$count = 0; $k = 0;
						for($i=2;$i<=$lastRow;$i++) 
						{	
							$sku							=	 	$objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
							$title 							= 		$objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
							$checkProduct 	= $this->Product->find( 'first' , array( 'conditions' => array( 'Product.product_sku' => $sku ) ) );
							$barcode 		= $checkProduct['ProductDesc']['barcode'];
							if(count($checkProduct) == 4){	
								$this->ProductDesc->updateAll(array('Product.product_name'=>$title,'ProductDesc.short_description'=>$title,'ProductDesc.long_description'=>$title),array('Product.product_sku' => $sku));
								$count++;
							} else { 
								$k++;
							}
							
						}
				}
			$this->Session->setFlash($count.' :- SKU Updated.'.$k.' :- SKU Not Update.', 'flash_danger');		
			$this->redirect($this->referer());
		
	}
	
}

?>
