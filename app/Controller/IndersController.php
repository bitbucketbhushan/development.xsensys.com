<?php

class IndersController extends AppController
{
    
    var $name = "Inders";
    
    var $helpers = array('Html','Form','Session');
    
    public function index()
    {
        
        /* Start here set custom title and breadcrumbs */
        $this->set( "title","Welcome to WMS administration" );
        $this->layout = "index";
		
    }
	
	public function testpdftohtml()
	{
		$this->layout  = '';
		$this->autoRender = false;
		
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper(array(0, 0, 280, 500), 'landscape');//landscape//portrait 
		
		$imgPath = WWW_ROOT .'css/';
		
		$html = '<body>
		<div id="label">
		<div class="container" style="font-size:40px; margin-top:40px;">
		B.P.I <br>
		E.M.C. - Building 829C<br>
		1931 Zaventem - Brucargo<br>
		Belgium
		</div>
		</div>
		
		</body>';
		
		$html.= '<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				$dompdf->stream("test.pdf");
	
		
		//$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));89848
		//$dompdf->render();
		//$dompdf->stream("test.pdf");
		
	}
    
}

?>

