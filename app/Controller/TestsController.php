<?php
//error_reporting(0);
class TestsController extends AppController
{
    
    var $name = "Tests";
    
    var $components = array('Session','Upload','Common','Auth');
    var $helpers = array('Html','Form','Common','Session');
	public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->Auth->Allow(array('index'));
	   $this->GlobalBarcode = $this->Components->load('Common'); 
	}
	
    public function index()
    {
		echo " Welcome in test Controller.";
		
		//require_once(APP . 'Vendor' . DS.'pdf_merge' . DS .'vendor' . DS .'autoload.php');
		//App::import('Vendor', array('file' => 'autoload'));
		require APP . 'Vendor' . DS.'pdf_merge' . DS .'vendor' . DS .'autoload.php';
		
		//use iio\libmergepdf\Merger; 
		
		//$merger = new Merger;
	//	$merger->addIterator(['A.pdf', 'B.pdf']);
		//$createdPdf = $merger->merge();
		
		//file_put_contents( 'maa.pdf', $createdPdf);
		
		exit;
	}
	
	public function testPdf()
	{
		
        $this->layout  = '';
		$this->autoRender = false;
		
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		//require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'class/BCGColor.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode39.barcode.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php'); 
		
				
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		//$dompdf->set_paper(array(0, 0, 394, 567), 'portrait');
		
		//$dompdf->set_paper(array(0, 0, 288, 567), 'portrait');
		//$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');
		$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');//landscape//portrait 
		
		$imgPath 			= WWW_ROOT .'css/';
		$barcodePath 		= WWW_ROOT .'img/orders/barcode/';   
		$barcodecorreosPath = WWW_ROOT .'img/orders/correos_barcode/';  
		#### CAlling Barcode class ####
		
		$colorFront = new BCGColor(0, 0, 0);
		$colorBack = new BCGColor(255, 255, 255);
		
		// Barcode Part
		$orderbarcode="300378-1";
		$code128 = new BCGcode128();
		$code128->setScale(2);
        $code128->setThickness(20);
		$code128->setForegroundColor($colorFront);
		$code128->setBackgroundColor($colorBack);
		$code128->parse($orderbarcode);
		// Drawing Part
		$imgOrder128=$orderbarcode;
		$imgOrder128path=$barcodePath.'/'.$imgOrder128.".png";
		$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
		$drawing128->setBarcode($code128);
		$drawing128->draw();
		$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
		
		
		############ Routing Barcode #######
		$destination ="28042";
		$origin ="90019";
		$routingcode=$destination.$origin;
		$routingcode128 = new BCGcode128();
		$routingcode128->setScale(2);
        $routingcode128->setThickness(30);
		$routingcode128->setForegroundColor($colorFront);
		$routingcode128->setBackgroundColor($colorBack);
		$routingcode128->parse(array(CODE128_C, $routingcode));
		//$routingcode128->parse($routingcode);
		
		// Drawing Part
		$routingimg =$routingcode;
		$routingdrawing128path=$barcodecorreosPath.'/'.$routingimg.".png";
		$routingdrawing128 = new BCGDrawing($routingdrawing128path, $colorBack);
		$routingdrawing128->setBarcode($routingcode128);
		$routingdrawing128->draw();
		$routingdrawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 

// Lethium Battery label html
/*
$html = '<div id="label">
   <div class="container">
      <table style="padding:2px 0; border-bottom:3px solid #000000">
         <tr>
   <td valign="middle"><img style="padding-top:10px" src="http://dev.xsensys.com/img/MarecPriority.jpg" width="210px"></td>
            <td align="right" height="80px">
               <table class="header1" align="right" >
                  <tr>
                     <td  valign="middle" class="aprior" style="padding-right:5px">
                        <img style="padding-top:0px" src="http://xsensys.com/img/priority.jpg" width="198px">
                     </td>
                     <td class="bpost" style="padding-left:5px">
						<img style="padding-top:5px" src="http://xsensys.com/img/postnl.jpg" width="150px">     
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table style="padding:0px 0;">
         <tr>
            <td>
               <table style="width:90%;">
                  <tr>
                     <td>
                        <table>
                           <tr>
                              <td valign="top" width="20%">
                       			<h4 style="font-size:16px">SHIP TO: </h4>
                    		  </td>
                              <td valign="top" class="leftside leftheading address" style="font-size:16px" width="80%">
								  Company Name<br>
								  Stamatoula Tetlow<br>
								  16, The Byway<br>
								  Potters Bar<br>
								  Herts<br>
								  EN6 2LW<br>
								  United Kingdom
							  </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
           <td align="right" valign="top">
		   	<img src="http://dev.xsensys.com/img/Lithium.jpg" width="160" height="160" style="padding-top:0px">	  
		   </td>
         </tr>
      </table>
      <div class="footer row" style="padding:1px; text-align: center; border-top:3px solid #000000; border-bottom:3px solid #000000">
    <table>
         <tr>
            <td class="center">
               <div class="countrycode">
                  <h3 style="font-size:60px;color:#666666">GB</h3>
               </div>
            </td>
            <td>
                <div class="barcode right">
            <div><img src="http://xsensys.com/img/100020-1.png" width="220px" style="margin-right:-2px"><br></div>
         </div>
            </td>
         </tr>
      </table>
   </div>
      <div class="footer row" style="padding:3px 0 0 0; font-size:12px; text-align: center; border-bottom:0px solid #000000">If undelivered, please return to: PostBus 7097, 3109 AC Schiedam, Netherlands</div>
   </div>
</div>';*/
/*


*/
	
$html = '
<div id="label">
   <div class="container">
    <table cellpadding="0" cellspacing="0" width="100%" height="300">
	<tr><td colspan="2" height="5px">&nbsp;</td></tr> 
	<tr>
		<td  width="50%" height="60px" valign="top">					
				<div style="font-size:12px; line-height:1.25">ESL LOGISTICS<br>CTL CHAMARTIN<br>AVDA. PIO XII 106-108<br>28070 MADRID</div>
				
				
			</td>
			<td align="right" width="40%" valign="top">
				<table cellpadding="0" cellspacing="0" width="100%" style="border:2px solid #000;">
				<tr>
					<td style="width:40%; border-right:2px solid #000;">	
						<div style="text-align: center; padding:2px;">
							<div style="width:100%; font-weight:bold;">'.utf8_encode('ESPA�A').'</div>
							<div style="width:100%;"><img src="http://dev.xsensys.com/img/correos.png" width="60px"></div>
						</div>
					</td>
					<td style="width:60%;">
						<div style="text-align: center;font-weight:bold;">
							<div style="border-bottom:2px solid #000; padding:2px 5px;width:100%;"> FRANQUEO <br> PAGADO </div>
							<div style="width:100%; padding:2px 5px;">Distribuci&#243;n <br> Internacional </div>
						</div>
					</td>				
				</tr>
				</table>
			</td> 	
	</tr>
	<tr>
	   <td></td>
	   <td></td>
	 </tr>
	 <tr><td colspan="2" align="left"> 
	 <div style="margin-top:10px;margin-bottom:10px;font-size:22px; ">
					<strong>Deliver to : </strong> <br>'.
					utf8_encode('David Gomez').'<br>'.
					utf8_encode('Av. De los Arces 7 P-C 2-C').'<br>'.
					$destination.'<br>
					Madrid<br>
					MADRID<br> 
					SPAIN
				</div>
	</td>
	</tr>
	<tr><td colspan="2" align="center"><div>'.utf8_encode('ENV�O ORDINARIO').'</div><div><img src="'.$imgOrder128.'"><div></td></tr> 
	<tr><td colspan="2" align="center"><div style="margin-top:5px;"><img src="'.$routingimg.'"></div></td></tr> 
 </table>
   </div>
</div> 
   ';
   
  /* $html ='<div id="label">
   <div class="container">
      <table style="padding:5px 0; border-bottom:0px solid #000000">
         <tr>
		 <td><img src="http://xsensys.com/img/CostBreaker_airmail.jpg" width="260px"></td>
            <td align="right" height="80px">
               <table class="header1" align="right" >
                  <tr>
                     <td class="aprior" style="padding-right:10px">
                        
                     </td>
                     <td class="bpost" style="padding-left:10px">
                        <img src="http://xsensys.com/img/postnlUK.jpg" height="80px">
                       
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table>
         <tr>
            <td>
               <table style="width:100%; margin:0px;">
                  <tr>

                     <td>
                        <table width="100%">
                           <tr>
						                        <td width="5%" valign="top">
                        
                     </td>
					 
                              <td width="78%" class="leftside leftheading address" style="font-size:16px">_ADDRESS_
<td class="right" width="17%">
               <div class="countrycode">
                  <h3 style="font-size:60px;color:#666666; border:2px solid #000000; padding:10px; width:120px; text-align:center">_COUNTRYCODE_</h3>
               </div>
            </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
           
         </tr>
      </table>
      <div class="footer row" style="padding:2px 0; text-align: center; border-top:0px solid #000000; border-bottom:0px solid #000000">
    <table>
         <tr>

            <td>
                <div class="barcode left">
            <div style="margin-left:-20px">_BARCODEIMAGE_
            <div style="width:250px; text-align: center;">_SPLITORDERID_</div>
            </div>
	     </div>
            </td>
			            <td class="right" valign="top">
               <div class="countrycode">
                  <h3 style="font-size:30px;color:#111111; margin-top:5px"><img src="http://xsensys.com/img/prioritaire.jpg" width="230px"></h3>
               </div>
            </td>
         </tr>
      </table>
   <div class="footer row" style="padding:5px 0px; font-size:12px; text-align: center; border-bottom:0px solid #000000">Returns: PostBus 7124, 3109 AC Schiedam, Netherlands</div>
   
        
      </div>
      
   </div>
</div>';*/


/*
$html='<!-- Recorded Service Label-->
<div id="label" style="margin-top:3px;">
	<div class="container" style=" margin-top:5px;height:342px; padding:3px;">
		<table style="padding:2px 0;">
			<tr>
				<td>
					<table style="width:100%" >
						<tr>
							<td width="45%" align="left" valign="top" style="font-size:10px; line-height:1.5">Eigen kenmerk :
								<br><br><u> PostBus 7124, 3109 AC Schiedam, Netherlands</u>
							</td>
							<td width="25%" valign="top" align="center">
								<img style="padding-top:1px" src="http://xsensys.com/img/priority.jpg" width="160px">
							</td>
							<td width="30%" valign="top" align="right" rowspan="2">
								<img style="padding-top:1px" src="http://xsensys.com/img/postnl.jpg" width="150px">
							</td>
						  </tr>
					 </table>
					</td>
				 </tr>
			</table>
			
			<table>
			
				<tr>
					<td  valign="bottom" align="left"></td>
					<td width="60%" Valign="right">
						<table>
							<tr>
								<td colspan="2">
									<div style="margin-top:0px; text-align: left; padding:0px; height:100px; width=300px;">
										<div style="padding:8px">
											 _ADDRESS_
										 </div>
									</div>
     							</td>
						      </tr>
							  
							</table>
						  </td>
					  </tr>
					  <tr>
						<td colspan="2" align="center" valign="bottom" style="height:80px; text">
									<div class="barcode left">
									<div style="margin-left:-20px">_BARCODEIMAGE_
									<div style="width:250px; text-align: center;">_SPLITORDERID_</div>
							</div>
						</td>
					</tr>
				 </table>
			</div>
		</div>';*/
				
		$html.= '<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
		//echo $html;
	    //exit;
		$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
		$dompdf->render();
		$dompdf->stream("test.pdf"); 

	}  
	
	
	public function getsearchlist()
	 {
			   $this->autoRender = false;
			   $this->layout = '';
			  
			   $this->loadModel('OpenOrder');
			   $this->loadModel('OrderItem');
			   $this->loadModel('MergeUpdate');
			   $this->loadModel('Product');
			   date_default_timezone_set('Europe/Jersey');
			  
			   $express1	=	array();
			   $express2	=	array();
			   $standerd1	=	array();
			   $standerd2	=	array();
			   $tracked1	=	array();
			   $tracked2	=	array();
			   
			   $barcode		=	 '7317400015392';//$this->request->data['barcode'];
			   //$barcode		=	 '4210201382355';//$this->request->data['barcode'];
			    /*************************************Apply code for Global Barcode*********************************************/
			   $barcodeg		=	$this->GlobalBarcode->getGlobalBarcode($barcode);
			   /**************************************Apply code for Global Barcode***********************************************/
			   $usertype = $this->Session->read('Auth.User.service_pkst');
			   
			   $getItems = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcodeg .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1','MergeUpdate.linn_fetch_orders IN' => array(1,4) ) ) );
			  //pr($getItems);
			  //exit;
			   $data = array();
			   
			   foreach($getItems as $getItem){
			  	 	
					$get_skus	=	explode(',',$getItem['MergeUpdate']['sku'] );
						$item_data[$getItem['MergeUpdate']['delevery_country']][ $getItem['MergeUpdate']['postal_service'] ][$getItem['MergeUpdate']['sku']][] = array( 'order_id' => $getItem['MergeUpdate']['product_order_id_identify'], 'sku' => $getItem['MergeUpdate']['sku'], 'barcode' => $getItem['MergeUpdate']['barcode'], 'packaging_type' => $getItem['MergeUpdate']['packaging_type'], 'service_name' => $getItem['MergeUpdate']['service_name']);
			   }
			  $html =   '<table class="table table-bordered table-striped dataTable" border = 1 >
						<tr>
							<th width="5%" >Country</th>
							<th width="10%" >P. Qty</th>
							<th width="5%" >Num. Orders</th>
							<th width="5%" >Total Qty Req.</th>
							<th width="10%">Barcode / SKU</th>
							<th width="25%" >Title</th>
							<th>Service</th>
							<th>Service Name</th>
							<th>Packaging Name</th>
							<th>Action</th>
						</tr>';	
				arsort($item_data);
			   foreach($item_data as $ck => $kv){
			    	foreach($kv as $sk => $sv){
						foreach($sv as $qk => $qv){	
							$_data = array();$_ddata = array();	
							$html .= '<tr>';					 
							foreach($qv as $k => $v){
								//$a[] = array($v['order_id'],$v['sku']);
								$_data[]  		= $v['order_id']; 
								$sku 	  		= $v['sku'];	
								$barcode 	  	= $v['barcode'];	
								$packaging_type = $v['packaging_type'];	
								$service_name 	= $v['service_name'];	
								$getProductdesc	= $this->Product->find('first', array( 'conditions' => array('Product.product_sku' => 'S-'.explode('S-',$sku)[1] )));
								$title			=	 $getProductdesc['Product']['product_name'];
						 	}
							//$f_data[$ck][$sk][$qk]  = $a; 		
							$number_orders	=	count($_data);			
							$skus			=	 explode( ',', $sku);
					   		$barcode		=	 explode( ',', $barcode);
							
					   		$i = 0;
							$title = array();
							//pr($skus);
							$newSku = array(); $po_barcode = array();
							foreach($skus as $sku)
							   {
									$newSku[]				=	 explode( 'X', $sku)[1];
								    $getProductdesc			=	 $this->Product->find('first', array( 'conditions' => array('Product.product_sku' => explode( 'X', $sku)[1])));
									//pr($getProductdesc);
								    $title[]				=	 $getProductdesc['Product']['product_name'];
									$po_barcode[]			=	 $getProductdesc['ProductDesc']['barcode'];
									$i++;
							   }
								   //pr($newSku);
								$f_data[$ck][$sk][$qk]['order_ids'] 	= implode(',', $_data);
								$f_data[$ck][$sk][$qk]['sku'] 			= 'S-'.explode('S-',$sku)[1];
								$f_data[$ck][$sk][$qk]['barcode'] 		= implode(',',$po_barcode);
								$f_data[$ck][$sk][$qk]['packaging']		= $packaging_type;
								$f_data[$ck][$sk][$qk]['title']			= implode(',',$title);
								$f_data[$ck][$sk][$qk]['nub_orders']	= $number_orders;
								$f_data[$ck][$sk][$qk]['tot_qty']		= $number_orders * $qk ;
								$f_data[$ck][$sk][$qk]['service_name']	= $service_name;
								$html .= '<td>'.$ck.'</td>
										<td>'.str_replace(',',"<br>",$qk).'</td>
										<td>'.$number_orders.'</td>
										<td>'.$number_orders * $qk.'</td>
										<td>'.implode(',',$newSku).'<br>'.implode(',',$po_barcode).'</td>
										<td>'.implode(',',$title).'</td>
										<td>'.$sk.'</td>
										<td>'.$packaging_type.'</td>
										<td>'.$service_name.'</td>
										<td>Select</td>';
										$html .= '</tr>';
						}
					}
			   }
			   
				$html .='</table>';

				echo $html;
			   //pr($item_data);
			   pr($f_data);
			   exit;
			   
			   
				
				
			   $this->set(compact('express1','standerd1','tracked1','express2','standerd2','tracked2'));
			   echo $this->render('scansearch');
			   exit;
		   }
	
	
	
		public function getsearchlist140818()
		{
			   $this->autoRender = false;
			   $this->layout = '';
			  
			   $this->loadModel('OpenOrder');
			   $this->loadModel('OrderItem');
			   $this->loadModel('MergeUpdate');
			   $this->loadModel('Product');
			   date_default_timezone_set('Europe/Jersey');
			  
			   $express1	=	array();
			   $express2	=	array();
			   $standerd1	=	array();
			   $standerd2	=	array();
			   $tracked1	=	array();
			   $tracked2	=	array();
			   
			   $barcode		=	 '7317400015392';//$this->request->data['barcode'];
			   //$barcode		=	 '4210201382355';//$this->request->data['barcode'];
			    /*************************************Apply code for Global Barcode*********************************************/
			   $barcodeg		=	$this->GlobalBarcode->getGlobalBarcode($barcode);
			   /**************************************Apply code for Global Barcode***********************************************/
			   $usertype = $this->Session->read('Auth.User.service_pkst');
			   
			   $getItems = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcodeg .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1', 'MergeUpdate.linn_fetch_orders IN' => array(1,4))));
			  //pr($getItems);
			   $data = array();
			   
			   foreach($getItems as $getItem){
			  	 	//$item_data[$getItem['MergeUpdate']['delevery_country']][ $getItem['MergeUpdate']['postal_service'] ][$getItem['MergeUpdate']['quantity']][] = array( 'order_id' => $getItem['MergeUpdate']['product_order_id_identify'] );
					$item_data[$getItem['MergeUpdate']['delevery_country']][ $getItem['MergeUpdate']['postal_service'] ][$getItem['MergeUpdate']['quantity']][] = array( 'order_id' => $getItem['MergeUpdate']['product_order_id_identify'], 'sku' => $getItem['MergeUpdate']['sku'], 'barcode' => $getItem['MergeUpdate']['barcode'], 'packaging_type' => $getItem['MergeUpdate']['packaging_type'], 'service_name' => $getItem['MergeUpdate']['service_name']);
					
			   }
			   
			   
				pr($item_data);
			  exit;
			  $html =   '<table class="table table-bordered table-striped dataTable" border = 1 >
						<tr>
							<th>Country</th>
							<th>P. Qty</th>
							<th>Num. Orders</th>
							<th>Total Qty Req.</th>
							<th>Barcode / SKU</th>
							<th>Title</th>
							<th>Service</th>
							<th>Service Name</th>
							<th>Packaging Name</th>
							<th>Action</th>
						</tr>';	
				arsort($item_data);
			   foreach($item_data as $ck => $kv){
			    	foreach($kv as $sk => $sv){
						foreach($sv as $qk => $qv){	
							$_data = array();$_ddata = array();	
							$html .= '<tr>';					 
							foreach($qv as $k => $v){
								//$a[] = array($v['order_id'],$v['sku']);
								$_data[]  		= $v['order_id']; 
								$sku 	  		= $v['sku'];	
								$barcode 	  	= $v['barcode'];	
								$packaging_type = $v['packaging_type'];	
								$service_name 	= $v['service_name'];	
								$getProductdesc	= $this->Product->find('first', array( 'conditions' => array('Product.product_sku' => 'S-'.explode('S-',$sku)[1] )));
								$title			=	 $getProductdesc['Product']['product_name'];
						 	}
							//$f_data[$ck][$sk][$qk]  = $a; 		
							$number_orders	=	count($_data);			
							$skus			=	 explode( ',', $sku);
					   		$barcode		=	 explode( ',', $barcode);
							
					   		$i = 0;
							$title = array();
							//pr($skus);
							$newSku = array(); $po_barcode = array();
							foreach($skus as $sku)
							   {
									$newSku[]				=	 explode( 'X', $sku)[1];
								    $getProductdesc			=	 $this->Product->find('first', array( 'conditions' => array('Product.product_sku' => explode( 'X', $sku)[1])));
									//pr($getProductdesc);
								    $title[]				=	 $getProductdesc['Product']['product_name'];
									$po_barcode[]			=	 $getProductdesc['ProductDesc']['barcode'];
									$i++;
							   }
								   //pr($newSku);
								$f_data[$ck][$sk][$qk]['order_ids'] 	= implode(',', $_data);
								$f_data[$ck][$sk][$qk]['sku'] 			= 'S-'.explode('S-',$sku)[1];
								$f_data[$ck][$sk][$qk]['barcode'] 		= implode(',',$po_barcode);
								$f_data[$ck][$sk][$qk]['packaging']		= $packaging_type;
								$f_data[$ck][$sk][$qk]['title']			= implode(',',$title);
								$f_data[$ck][$sk][$qk]['nub_orders']	= $number_orders;
								$f_data[$ck][$sk][$qk]['tot_qty']		= $number_orders * $qk ;
								$f_data[$ck][$sk][$qk]['service_name']	= $service_name;
								$html .= '<td>'.$ck.'</td>
										<td>'.$qk.'</td>
										<td>'.$number_orders.'</td>
										<td>'.$number_orders * $qk.'</td>
										<td>'.implode(',',$newSku).'<br>'.implode(',',$po_barcode).'</td>
										<td>'.implode(',',$title).'</td>
										<td>'.$sk.'</td>
										<td>'.$packaging_type.'</td>
										<td>'.$service_name.'</td>
										<td>Select</td>';
										$html .= '</tr>';
						}
					}
			   }
			   
				$html .='</table>';

				echo $html;
			   //pr($item_data);
			   pr($f_data);
			   exit;
			   if($barcode)
			   {
				   $new	=	array( $barcode );
				   
				   $param  = array(	'OR' => array(
										array( 'MergeUpdate.linn_fetch_orders' => '1' ),
										array( 'MergeUpdate.linn_fetch_orders' => '4' )	
								)
					);	
				   //$getItems = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcode .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1','MergeUpdate.out_sell_marker' => '0', $param )));
				  
				   $getItems_local = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcode .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1',  'MergeUpdate.linn_fetch_orders IN' => array(1,4) )));
				   
				   /*Apply code for Global Barcode*/
				   //if(count($getItems) == 0)
				  // {
				   		$getItems_global = $this->MergeUpdate->find('all' ,  array('conditions' => array('FIND_IN_SET(\''. $barcodeg .'\',MergeUpdate.barcode)','MergeUpdate.status' => '0', 'MergeUpdate.pick_list_status' => '1', 'MergeUpdate.linn_fetch_orders IN' => array(1,4))));
				  // }
				   
				  $getItems = array_merge($getItems_local,$getItems_global);
				   
				   $e = 0;
				   foreach( $getItems as $getItem )
				   {
					   $id		=	 $getItem['MergeUpdate']['order_id'];
					   $skus	=	 explode( ',', $getItem['MergeUpdate']['sku']);
					   $barcode	=	 explode( ',', $getItem['MergeUpdate']['barcode']);
					   $assing_user	=	 $getItem['MergeUpdate']['assign_user'];
					   
					   $i = 0;
					   foreach($skus as $sku)
					   {
						   $newSku[$i]	=	 explode( 'X', $sku);
						   $newSku[$i][2]		=	 $barcode[$i];
						   $getProductdesc	=	$this->Product->find('first', array( 'conditions' => array('Product.product_sku' => $newSku[$i][1])));
						   $title	=	 $getProductdesc['Product']['product_name'];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['split_qty']		=	$newSku[$i][0];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['split_sku']		=	$newSku[$i][1];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['split_barcode']	=	$newSku[$i][2];
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['title']			=	$title;
						   $getItems[$e]['MergeUpdate']['ItemList'][$i]['assign_user']		=	$assing_user;
						   $i++;
					   }
					   $e++;
				   }
			   }
			   
				if(isset($getItems))
				{
					foreach($getItems as $itemdetail)
					{
						$sundation_condi = false;
						$royh		= 	array(5,6,7);
						$days 		= 	array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
						$servicepro =   array( 'Correos','DHL','Jersey Post','Royalmail','Spain Post','PostNL');
						if(date("l") == 'Sunday' && in_array(date("H"),$royh))
						{
							$days 		= 	array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
							$servicepro =   array( 'Correos','DHL','Jersey Post','Spain Post','PostNL');
						}
						
						
						if(in_array($itemdetail['MergeUpdate']['service_provider'], $servicepro) && in_array(date("l"),$days)){
							$sundation_condi = true;
						} else {
							$sundation_condi = false;
						}
						
						if($sundation_condi)
					 	{	//start condiiotns
							if($usertype == 1)
							{
								if($itemdetail['MergeUpdate']['postal_service'] == 'Express' && count($itemdetail['MergeUpdate']['ItemList']) == 1)
								{
									$express1[]	=	$itemdetail;
								}
								if($itemdetail['MergeUpdate']['postal_service'] == 'Express' && count($itemdetail['MergeUpdate']['ItemList']) > 1)
								{
									$express2[]	=	$itemdetail;
								}
							} 
							else 
							{
								if(($itemdetail['MergeUpdate']['postal_service'] == 'Default' || $itemdetail['MergeUpdate']['postal_service']== 'Standard' || $itemdetail['MergeUpdate']['postal_service'] == 'Standard_Jpost' ) && count($itemdetail['MergeUpdate']['ItemList']) == 1)
								{
									$standerd1[]	=	$itemdetail;
								}
								if($itemdetail['MergeUpdate']['postal_service'] == 'Tracked' && count($itemdetail['MergeUpdate']['ItemList']) == 1)
								{
									 $tracked1[] = $itemdetail;
								}
								
								if(($itemdetail['MergeUpdate']['postal_service'] == 'Default' || $itemdetail['MergeUpdate']['postal_service'] == 'Standard' || $itemdetail['MergeUpdate']['postal_service'] == 'Standard_Jpost' ) && count($itemdetail['MergeUpdate']['ItemList']) > 1)
								{
									$standerd2[]	=	$itemdetail;
								}
								if($itemdetail['MergeUpdate']['postal_service'] == 'Tracked' && count($itemdetail['MergeUpdate']['ItemList']) > 1)
								{
									 $tracked2[] = $itemdetail;
								}
						 	}
						}//end condiiotns
					}
				}
				
			   $this->set(compact('express1','standerd1','tracked1','express2','standerd2','tracked2'));
			   echo $this->render('scansearch');
			   exit;
		   }
	
		public function genearateBulkSlipLabel_old()
		{
			$this->autoRender = false;
			$this->layout = '';
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'Product' );
			$this->loadMOdel( 'CategoryContant' );
			$this->loadMOdel( 'Template' );
			$this->loadMOdel( 'BulkLabel' );
			$this->loadModel('MergeUpdate');
			$this->loadModel('PackagingSlip');
			
			//pr($this->request->data['ids']);
			//$this->request->data['ids'] = "1001239-1,1001239-1";
			$this->request->data['ids'] = "1017342-1,1017326-1,1017251-1,1016781-1,1016643-1,1016639-1";
			$ids = explode(",",$this->request->data['ids']);
			foreach($ids as $id)
			{
				/*$get_sp_order	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.product_order_id_identify' => $id)));
				$get_sp_order['MergeUpdate']['service_provider'];
				if( $get_sp_order['MergeUpdate']['service_provider'] == 'Royalmail' ){
					$this->royalMailLabel()
					//echo "Royla mail";
				} else {
					echo "PostNL";
				}
				exit;*/
				
				$openOrderId	=	explode("-",$id)[0];
				$splitOrderId	=	$id;
				$slip 	=	$this->getSlip( $splitOrderId, $openOrderId );
				$label 	=	$this->getLabel( $splitOrderId, $openOrderId );
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				$dompdf->set_paper('A4', 'portrait');
				$date = date("m/d/Y H:i:s");	
				$finArray = array();
				$html ='';
				$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 			 <meta content="" name="description"/>
			 			 <meta content="" name="author"/>';
				//$html .= $slip.'<br><table style="margin-bottom: 37px;margin-left: 37px;position: relative;margin-top: 500px;">'.$label."</table>";			 
				//$html = '<table style="height:1020px;" width=500 height=1020 border=1><tr height=500 width=650 ><td  colspan=2 ></td></tr><tr height=415><td width=400></td><td width=100>111111</td></table>';	
				
				/*$html = '<table style="height:1090px; width:680px; border:1px solid gray;"  > 
						<tr><td style="height:690px; border:1px solid gray; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr><td style="height:377px; width:566px; border:1px solid gray; padding:0px 0px 5px 5px">'.$label.'</td><td style="width:110px;"></td></tr>
						</table>';			 */
				$html .= '<table style="height:1090px; width:680px; border:1px solid gray;"  > 
						<tr><td style="height:690px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr><td valign="top" style="height:377px; width:566px; padding:0px 0px 5px 12px;border:1px solid gray;">'.$label.'</td><td style="width:110px;"></td></tr>
						</table>';	
				echo $html;
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				
				
				$imgPath = WWW_ROOT .'img/printPDF/Bulk/'; 
				$path = Router::url('/', true).'img/printPDF/Bulk/';
				$name	=	'Label_Slip_'.$splitOrderId.'.pdf';
				file_put_contents($imgPath.$name, $dompdf->output());
				//exit;
			}
			
		}
		
		public function genearateBulkSlipLabel()
		{
			$this->autoRender = false;
			$this->layout = '';
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'Product' );
			$this->loadMOdel( 'CategoryContant' );
			$this->loadMOdel( 'Template' );
			$this->loadMOdel( 'BulkLabel' );
			$this->loadModel('MergeUpdate');
			$this->loadModel('PackagingSlip');
			
			//pr($this->request->data['ids']);
			//$this->request->data['ids'] = "1001239-1,1001239-1";
			/*$this->request->data['ids'] = "1356639-1,1358870-1,1359840-1,1361757-1,1362596-1,1362679-1,1362953-1,1363252-1,1363426-1,1363990-1,1364186-1,1365697-1,1365752-1,1366300-1,1366713-1,1366837-1,1367413-1,1367859-1,1367860-1,1368283-1,1369316-1,1370016-1,1370557-1,1371336-1";*/
			
			//$this->request->data['ids'] = "1356639-1,1308579-1";
			$this->request->data['ids'] = "1362922-1,1358188-1,1364082-1,1365855-1,1366319-1,1366595-1,1370098-1,1371900-1";
			
			$ids = explode(",",$this->request->data['ids']);
			foreach($ids as $id)
			{
				$openOrderId	=	explode("-",$id)[0];
				$splitOrderId	=	$id;
				$get_sp_order	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.product_order_id_identify' => $id)));
				$get_sp_order['MergeUpdate']['service_provider'];
				if( $get_sp_order['MergeUpdate']['service_provider'] == 'Royalmail' ){
					//$label = $this->royalMailLabel($splitOrderId, $openOrderId);
					//echo "Royla mail";
				} else {
					echo "PostNL";
				}
				
				
				$slip 	=	$this->getSlip( $splitOrderId, $openOrderId );
				
				$label 	=	$this->getLabel( $splitOrderId, $openOrderId );
				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				
				
				$dompdf->set_paper('A4', 'portrait');
				$date = date("m/d/Y H:i:s");	
				$finArray = array();
				$html = '';
				$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			 			 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			 			 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			 			 <meta content="" name="description"/>
			 			 <meta content="" name="author"/>';
						//$html .= $slip.'<br><table style="margin-bottom: 37px;margin-left: 37px;position: relative;margin-top: 500px;">'.$label."</table>";			 
						//$html = '<table style="height:1020px;" width=500 height=1020 border=1><tr height=500 width=650 ><td  colspan=2 ></td></tr><tr height=415><td width=400></td><td width=100>111111</td></table>';	
				
						/*$html = '<table style="height:1090px; width:680px; border:1px solid gray;"  > 
						<tr><td style="height:690px; border:1px solid gray; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr><td style="height:377px; width:566px; border:1px solid gray; padding:0px 0px 5px 5px">'.$label.'</td><td style="width:110px;"></td></tr>
						</table>';			 */
				/*$html .= '<table style="height:950px;  width:650px; border:1px solid #CCC;"  > 
						<tr><td style="height:600px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr><td valign="top" style="height:370px; width:500px; padding:0px 0px 5px 12px;border:1px solid gray;">'.$label.'</td><td></td></tr>
						</table>';*/
				$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
						<tr><td style="height:700px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
						<tr><td valign="top" style="height:367px; width:560px; padding:0px 0px 5px 12px;border:1px solid gray;">'.$label.'</td><td style="width:120px;"></td></tr>
						</table>';
				
				echo $html;
				$cssPath = WWW_ROOT .'css/';
				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
				//$dompdf->load_html($html, Configure::read('App.encoding'));
				$dompdf->render();
				
				
				$imgPath = WWW_ROOT .'img/printPDF/Bulk/'; 
				$path = Router::url('/', true).'img/printPDF/Bulk/';
				$name	=	'Label_Slip_'.$splitOrderId.'.pdf';
				file_put_contents($imgPath.$name, $dompdf->output());
				//exit;
			}	
			
		}
		
 		public function getDetail( $split_id = null, $order_id = null )
		{
			
			$this->loadModel( 'Country' );
			App::import( 'Controller' , 'Cronjobs' );		
			$objController 	= new CronjobsController();
			$getorderDetail	= $objController->getOpenOrderById($order_id);
			$cInfo = $getorderDetail['customer_info'];
			$Address1 = $cInfo->Address->Address1;
			$Address2 = $cInfo->Address->Address2;
			$order->TotalsInfo = $getorderDetail['totals_info'];
			$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
			
			$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_id,'MergeUpdate.service_provider' => 'Royalmail')));	
			$type = 2; 
			if($orderItem['MergeUpdate']['service_name'] == 'royalmail_24'){
				$type = 1;
			}
			$service_format = 'P';
			if($orderItem['MergeUpdate']['service_name'] == 'royalmail_LL_48'){
				/*  F	Inland Large Letter, L	Inland Letter, N	Inland format Not Applicable, P	Inland Parcel */
				$service_format = 'F';
			}
			
			$_details = array('merge_id'=> $order_id,
						'split_order_id'=> $split_id,
						'sub_total' => number_format($order->TotalsInfo->Subtotal,2),
						'order_total'=> number_format($order->TotalsInfo->TotalCharge,2),
						'postage_cost' => number_format($order->TotalsInfo->PostageCost,2),
						'order_currency'=>$order->TotalsInfo->Currency,			
						'Company'=>$cInfo->Address->Company,
						'FullName'=> utf8_decode($cInfo->Address->FullName),
						'Address1' =>utf8_decode($Address1),
						'Address2' =>utf8_decode($Address2),
						'Town' =>utf8_decode($cInfo->Address->Town),
						'Region' =>utf8_decode($cInfo->Address->Region),
						'PostCode' =>$cInfo->Address->PostCode,
						'CountryName'=>$cInfo->Address->Country,
						'CountryCode' =>$country_data['Country']['iso_2'],
						'PhoneNumber'=>$cInfo->Address->PhoneNumber	,
						'sub_source' => $getorderDetail['sub_source'],
						'type' 		=> $type,
						'service_format' => $service_format,
						'item_count'=> 1,
						'building_name' => '',
						'building_number' => 0,
					);
					return $_details;
			
		}
		
		public function royalMailLabel( $split_id = null, $order_id = null )
		{
			
			$details = $this->getDetail( $split_id, $order_id );
			$service = '48';
			if($details['type'] == 1 ){
				$service = '24';
			}
			
			$box_number = 824; 
			if(strpos($details['sub_source'],'CostBreaker')!== false){
				$box_number = 824;
			}
			else if(strpos($details['sub_source'],'RAINBOW')!== false){
				$box_number = 825;;
			}else if(strpos($details['sub_source'],'Marec')!== false){
				$box_number = 826;
			}	
			elseif(strpos($details['sub_source'],'BBD')!== false){
				$box_number = 827;
			}
			else if(strpos($details['sub_source'],'Tech_Drive')!== false){
				$box_number = 828;
			} 
			
			$html = '
					<div style=" width:320px; height:auto;  margin-left:60px;margin-top:40px;">
					<div class="container"  style=" border:1px solid #000;">
					<table  width="100%" cellpadding="0" cellspacing="10">
					<tbody>
					<tr>
					<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/Royal_Mail.png" width="115px"></td>
					<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/'.$service.'.jpg" width="50px"></td>
					<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/royal.png" width="100px" height="70px"></td>
					</tr>
					</tbody>
					</table>
					 
					
					<table width="100%" cellpadding="0" cellspacing="10" style="border-bottom:1px solid #000; border-top:1px solid #000;">
					<tbody>
					<tr>
					<td valign="top" width="50%" align="left" >
					<img src="'.WWW_ROOT.'royalmail/barcode/2d_matrix_'.$details['split_order_id'].'.png" width="83px" height="83px" style="margin-left:10px;">
					</td>
				
					</tr>
					</tbody>
					</table>
					
					<table width="100%"  cellpadding="0" cellspacing="10" >
					<tbody>
					<tr>
					<td valign="top" width="75%">';
					$html .='<strong>'. ucwords(strtolower($details['FullName'])).'</strong><br>';
		
					if(strlen($details['Address1']) > 25 || strlen($details['Address2']) > 25){
					
						$lines = explode("\n", wordwrap(htmlentities($details['Address1']) .' '.htmlentities($details['Address2']), '25'));
						if(isset($lines[0]) && $lines[0] != ''){
							$html .=  $lines[0].'<br>';
						}
						if(isset($lines[1]) && $lines[1] != ''){
							$html .=  $lines[1].'<br>';
						}
						if(isset($lines[2]) && $lines[2] != ''){
							$html .=  $lines[2].'<br>';
						}
					
					}else{
						$html .= ucfirst($details['Address1']).'<br>';
						if($details['Address2']) $html .= ucfirst($details['Address2']).'<br>';
					}
					
					
					$html .= ucfirst($details['Town']).'<br>';
					$html .= $details['PostCode'];
					$html .= '</div></td>';
					
					
					$html .='<td valign="bottom" width="25%" align="right">
					<img src="'.WWW_ROOT.'royalmail/img/'.$box_number.'.png" style="height:100px;float:right;">
					 </td>
					 </tr>
					</tbody>   
					 </table>
					
					 
					<table width="100%" style="border-top:1px solid #000;" >
					<tbody>
					<tr>
					<td valign="top" align="center" style="padding-top:5px;"><p style="font-family:chevin; font-size:12px;">Customer Reference:'.$details['split_order_id'].'</p>
					
					</td>
					</tr>
					</tbody>
					</table>
				 
					
					 </div>
					 <div  style="text-align:center;width:100%;padding-top:25px;">
						<img src="'.WWW_ROOT.'img/orders/barcode/'.$details['split_order_id'].'.png" style="text-align:center">
					 </div></div>';
					 
					return $html;
		}
 		
		public function getSlip($splitOrderId = null, $openOrderId = null)
		{
		
				App::import('Controller', 'Linnworksapis');
				$obj = new LinnworksapisController();
				$order	=	$obj->getOpenOrderById( $openOrderId );
				
				$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
				$itemPrice			=		$getSplitOrder['MergeUpdate']['price'];
				$itemQuantity		=		$getSplitOrder['MergeUpdate']['quantity'];
				$BarcodeImage		=		$getSplitOrder['MergeUpdate']['order_barcode_image'];
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				$subtotal			=		$order['totals_info']->Subtotal;
				$subsource			=		$order['sub_source'];
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$address			=		'';
				$address			.=		($company != '') ? $company.'<br>' : '' ; 
				$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
				$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
				$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
				$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
				$address			.=		($town != '') ? $town.'<br>' : '';
				$address			.=		($resion != '') ? $resion.'<br>' : '';
				$address			.=		($postcode != '') ? $postcode.'<br>' : '';
				$address			.=		($country != '' ) ? $country.'<br>' : '';
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				
				if($subsource == 'Marec_FR' || $subsource == 'Marec_DE' || $subsource == 'Marec_IT' || $subsource == 'Marec_ES' || $subsource == 'Marec_UK'){
						$company 	=  'Marec';
					} else if($subsource == 'CostBreaker_UK' || $subsource == 'CostBreaker_DE' || $subsource == 'CostBreaker_FR' || $subsource == 'CostBreaker_ES' || $subsource == 'CostBreaker_IT' ){
						$company 	=  'CostBreaker';
					} else if($subsource == 'Tech_Drive_UK' || $subsource == 'Tech_Drive_FR' || $subsource == 'Tech_drive_ES' || $subsource == 'Tech_drive_DE' || $subsource == 'Tech_Drive_IT' ){
						$company 	= 'Tech Drive Supplies';
					} else if($subsource == 'RAINBOW RETAIL DE' || $subsource == 'Rainbow Retail' || $subsource == 'Rainbow_Retail_ES' || $subsource == 'Rainbow_Retail_IT'){
						$company 	= 'Rainbow Retail';
					}  else if( $subsource == 'EBAY2' ){
						$company 	= 'EBAY2';
					}
				
				
				$i = 1;
				$str = '';
				$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
				$totalGoods = 0;
				$productInstructions = '';
				$ref_code = trim($getSplitOrder['MergeUpdate']['provider_ref_code']);
				$codearray = array('DP1', 'FR7');
				$this->loadModel( 'BulkSlip' );
				$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => $company ) ) );
				
				/*if( in_array($ref_code, $codearray)) 
				{
					
					$per_item_pcost 	= $postagecost/count($items);
					$j = 1;
					foreach($items as $item){
					$item_title 		= 	$item->Title;
					$quantity			=	$item->Quantity;
					$price_per_unit		=	$item->PricePerUnit;
					$str .= '<tr>
								<td style="border:1px solid #000;" align="left" valign="top" width="10%">'.$quantity.'</td>
								<td style="border:1px solid #000;" valign="top" width="20%">'.substr($item_title, 0, 15 ).'</td>
								<td style="border:1px solid #000;" valign="top" width="15%">'.$quantity * $price_per_unit.'</td>
								<td style="border:1px solid #000;" valign="top" width="15%">'.$per_item_pcost.'</td>
								<td style="border:1px solid #000;" valign="top" width="20%">'.(($price_per_unit * $quantity) + $per_item_pcost).'</td>
							</tr>';
					$j++;
					}
					$str .=	'<tr><td></td><td></td><td></td><td style="border:1px solid #000;">Total</td>
								<td style="border:1px solid #000;" valign="top" width="20%">'.$totalcharge.'</td>
							</tr>';
					echo $str;
					
				} 
				else 
				{
					
					foreach( $skus as $sku )
					{
					
						$newSkus[]				=	 explode( 'XS-', $sku);
						foreach($newSkus as $newSku)
							{
								
								$getsku = 'S-'.$newSku[1];
								$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
								
								$contentCat 	= 	$getOrderDetail['Product']['category_name'];
								$contentSubCat	=	$getOrderDetail['Product']['sub_category'];
								$productBarcode	=	$getOrderDetail['ProductDesc']['barcode'];
								$getContent	= $this->CategoryContant->find( 'first', array( 
																	'conditions'=>array('CategoryContant.sub_category'=>$contentSubCat,'CategoryContant.sub_source'=>$subsource)));
								
								
								if(count($getContent) > 0) {
									$productBarcode			=	$getOrderDetail['ProductDesc']['barcode'];
									$getbarcode				= 	$obj->getBarcode( $productBarcode );
									$productInstructions 	=  '<p style="font-size:14px;">'.$getContent['CategoryContant']['content'].'</p>';
								}
								
								$title	=	$getOrderDetail['Product']['product_name'];
								$totalGoods = $totalGoods + $newSku[0];
								$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
										<td valign="top" class="rightborder bottomborder">'.substr($title, 0, 15 ).'</td>
										<td valign="top" class="center norightborder bottomborder">'.$newSku[0].'</td>';
								$str .= '</tr>';
								$i++;
								
							}
							unset($newSkus);
					}
					
				}
				
				$countryArray = Configure::read('customCountry');
				if( ($country == 'Germany' || $country == 'France') && in_array($ref_code, $codearray))
				{
					$gethtml =	$this->PackagingSlip->find('first', array(
											'conditions' => array( 
													'PackagingSlip.name' => 'deutsche_post',
													'PackagingSlip.custom_slip_status' => 1
													)
												)
											);
				} 
				else 
				{
					
					if($country == 'Germany')
					{
						$getSourceName = $obj->getCompanyNameBySubsource( $subsource );
						
							$gethtml =	$this->PackagingSlip->find('first', array(
													'conditions' => array( 
															'PackagingSlip.company_name' => $getSourceName,
															'PackagingSlip.custom_slip_status' => 1
															)
														)
													);
					}
					else
					{
						
						$gethtml =	$this->PackagingSlip->find('first', array(
												'conditions' => array( 
														'PackagingSlip.store_name' => $subsource,
														'PackagingSlip.custom_slip_status' => 0
														)
													)
												);
					
					}
				}*/
								
								
				
				$html 			=	$gethtml['PackagingSlip']['html'];
				$paperHeight    =   $gethtml['PackagingSlip']['paper_height'];
				$paperWidth  	=   $gethtml['PackagingSlip']['paper_width'];
				$barcodeHeight  =   $gethtml['PackagingSlip']['barcode_height'];
				$barcodeWidth   =   $gethtml['PackagingSlip']['barcode_width'];
				$paperMode      =   $gethtml['PackagingSlip']['paper_mode'];
				//pr($gethtml);
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= $postcode;
				$setRepArray[] 					= $country;
				$setRepArray[] 					= $phone;
				$setRepArray[] 					= $ordernumber;
				$setRepArray[] 					= $courier;
				$setRepArray[] 					= $recivedate[0];
				$totalitem = $i - 1;
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totalGoods;
				$setRepArray[]	=	 $subtotal;
				$Path 			= 	'/wms/img/client/';
				$img			=	 '';
				$setRepArray[]	=	 $img;
				$setRepArray[]	=	 $postagecost;
				$setRepArray[]	=	 $tax;
				$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
				//$setRepArray[]	=	 $totalamount;
				$setRepArray[]	=	 $itemPrice;
				$setRepArray[]	=	 $address;
				$barcodePath  	=  Router::url('/', true).'img/orders/barcode/';
				$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.' width='. $barcodeWidth .'>';
				$barcodenum		=	explode('.', $barcode);
				
				$setRepArray[] 	=  $barcodeimg;
				$setRepArray[] 	=  $paymentmethod;
				$setRepArray[] 	=  $barcodenum[0];
				$setRepArray[] 	=  '';
				$img 			=	$gethtml['PackagingSlip']['image_name'];
				$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
				$setRepArray[] 	=  $returnaddress;
				$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
				$setRepArray[] 	= $splitOrderId;
				$setRepArray[] 	= 	utf8_decode( $productInstructions );
			//	$prodBarcodePath  	=  Router::url('/', true).'img/product/barcodes/';			
				$prodBarcodePath  	= WWW_ROOT.'img/product/barcodes/';
				$setRepArray[] 	=  '<img src='.$prodBarcodePath.$productBarcode.'.png width='. $barcodeWidth .'>';
				$setRepArray[] 	=  $totalamount;
				$imgPath = WWW_ROOT .'css/';
				//$html2 			= 	$gethtml['BulkSlip']['html'];
				/*$html2 = '<div id="label">

<div class="container">

<table class="header row" style="border-bottom:0px">
<tr>
<td valign="top" width="60%"><img src=http://xsensys.com/img/ebuyerexpress_logo.jpg width="200px">
</td>
<td valign="bottom" width="40%" style="text-align:right;" align="right">
<span class="bold">Date:</span>_RECIVEDATE_
</td>
</tr>
</table>
<table class="header row" style="width:100%; border-bottom:0px; margin:0px 0 20px 0;">
<tr>
<td  valign="top" width="50%"><h4>Delivery & Invoice Address:</h4>
_ADDRESS_
</td>

<td  width="50%" valign="top" style="text-align:center; font-size:14px"><br>
_BARCODE_
<span class="bold">Order #:</span>_SPLITORDERID_<br>
</td>

</tr>

</table>

<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px">

<table class="change_order_items" cellpadding="5px" cellspacing="0" border="1" style=" border-collapse: collapse; margin:0px;" >

<tr>

<th style="" align="left" class="" width="10%">S. No.</th>

<th style="" align="left" valign="top" width="75%" class="">Product Description</th>

<th style="" valign="top" class="" width="15%">Quantity</th>

</tr>



_ORDERSUMMARY_
<tr>
<td style="" align="left" class="" >&nbsp;</td>

<td style="" align="right" valign="top" class=""><span class="bold" >Total Quantity:</span></td>

<td style="" valign="top" class="">_TOTALITEM_</td>

</tr>

</table>

</div>

<div class="footer row" style="text-align:left; border-bottom:0px;" >
<b>Your order may be split in multiple packages.</b><br>
<b>Thank you for being eBuyer Express customer.</b> 

</div>

</div>';*/

	$html2 = '<table>
	<tr>
	<td valign="top"><img src="'.WWW_ROOT.'img/cost_stopper2.png" width="150px">
	</td>
	<td valign="top" style="text-align:right;" align="right">_BARCODE_
	</td>
	</tr>
	</table>
	<table class="header row" style="border-bottom:0px; margin:0px 0 20px 0;">
	<tr>
	<td  valign="top" width="50%"><h4><u>Delivery Address:</u></h4>
	 _ADDRESS_ 
	</td>
	
	<td  width="50%" valign="top">
	<h4><u>Bill To:</u></h4>
	 _ADDRESS_ 
	</td>
	
	</tr>

</table>


<table class="header row" style="margin:0 0 10px 0; border-bottom:0px">

<tr>

<td width="33%"><span class="bold">Order Reference:</span> _SPLITORDERID_
</td>

<td width="33%" align="right"><span class="bold">Date:</span> _RECIVEDATE_</td>
</tr>
</table>





<div class="tablesection row" style="margin:10px 0 0 0; border-bottom:0px">

<table class="change_order_items" cellpadding="5px" cellspacing="0" border="0" style=" border-collapse: collapse; margin:0px;" >

<tr>

<th style="" align="left" class="rightborder topborder bottomborder" width="15%">Line No.</th>

<th style="" align="left" valign="top" width="70%" class="topborder bottomborder rightborder">Description</th>

<th style="" valign="top" class="topborder bottomborder" width="15%">Quantity</th>

</tr>

_ORDERSUMMARY_

<tr>

<td  align="left" class="rightborder topborder bottomborder" >&nbsp;</td>

<td  align="right" valign="top" class="topborder bottomborder rightborder"><span class="bold" >Total Quantity:</span></td>

<td align="right" valign="top" class=" center topborder bottomborder">_TOTALITEM_</td>

</tr>

</table>

</div>

<div class="footer row" style="text-align:center; border-bottom:0px;" >
<b>Your order may be split in multiple packages.</b><br>
<b>Thank you from the Cost Breaker Team!</b> ';
				if( $productInstructions != '' )
				{
					$html2 .= '_PRODUCTINSTRUCTION_';
				} else {
					$html2 .= '</div>';
				}
				
				'</body>';
				
				//$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
				$html 		= 	$obj->setReplaceValue( $setRepArray, $html2 );
				return $html;
				
				
				
		}
		
		public function getLabel( $splitOrderId = null, $openOrderId = null )
		{
				
				$openOrderId	=	$openOrderId ;
				$splitOrderId	=	$splitOrderId ;
				App::import('Controller', 'Linnworksapis');
				$obj = new LinnworksapisController();
				
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				$order				=		$obj->getOpenOrderById( $openOrderId );
				$postalservices		=		$order['shipping_info']->PostalServiceName;
				$destination		=		$order['customer_info']->Address->Country;
				$total				=		$order['totals_info']->TotalCharge;
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$currency			=	 	$order['totals_info']->Currency;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$templateId			=	 	$order['template_id'];
				$subSource			=	 	$order['sub_source'];
				
				$str = ''; 
				
				$totlaWeight	=	0;
				$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
				$ref_code = trim($splitOrderDetail['MergeUpdate']['provider_ref_code']);
				$weight = 0;
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					
					foreach($newSkus as $newSku)
						{
							
							$totalvalue = $total;
							$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
							$getsku = 'S-'.$newSku[1];
							$qty = $newSku[0];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title	=	$getOrderDetail['Product']['product_name'];
							$weight	=	$weight + $getOrderDetail['ProductDesc']['weight'];
							$catName	=	$getOrderDetail['Product']['category_name'];
							if($country == 'United States')
							{
								$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.$catName.'</td>
									<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';
							}
							elseif($country == 'Spain')
								{
									$codearray = array("DP1", "FR7");
									$spaincode = array("mad_air","mad_road","cat_air","cat_road","sp_air","sp_road");
							
									if(in_array($ref_code, $spaincode))
									{
										$str .='<tr>
												<td  width="60%"  style="border:1px solid #000; padding:5px;">'.$catName.'</td>
												<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty * $getOrderDetail['ProductDesc']['weight'].'</td>
												<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">'.$qty.'</td>';
										$str .= '</tr>';
									} else {
										$str .= '<tr>
											<td valign="top" class="noleftborder rightborder bottomborder">'.$catName.'</td>
											<td valign="top" class="center noleftborder rightborder bottomborder">'.$qty.'</td>
											<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
										$str .= '</tr>';
									}
								}
							else
							{
								$str .= '<tr>
										<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.substr($title, 0, 25 ).'</td>
										<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
								$str .= '</tr>';
							}
						}
							unset($newSkus);
				}
				
				$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forl?-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia');
				
				$addData['company'] 	= 	$company;
				$addData['fullname'] 	= 	$fullname;
				$addData['address1'] 	= 	$address1;
				$addData['address2'] 	= 	$address2;
				$addData['address3'] 	= 	$address3;
				$addData['town'] 		= 	$town;
				
				if( $country == 'Italy' )
				{
					if(array_key_exists(strtoupper($resion),$provinces)){
						$addData['resion'] 		= 	$resion;
					}else {
						$addData['resion'] 		= 	array_search(ucfirst($resion), $provinces);
					}
				} else {
						$addData['resion'] 		= 	$resion;
				}
				
				$addData['postcode'] 	= 	$postcode;
				$addData['country'] 	= 	$country;
				
				
				$address 			= 		$obj->getCountryAddress( $addData );
				
				$CARTAS = '';
				
				if(trim($country) == 'Spain') { 
					$CARTAS = 'CARTAS '.substr($postcode, 0, 1);
					$routingcode = $obj->getCorreosBarcode( $postcode, $splitOrderId);
					$corriosImage		=		$routingcode.'.png';
					if(substr($postcode, 0, 1) == 0){
						$CARTAS = 'CARTAS '.substr($postcode, 1, 1);
					}	
				}
							
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				$currentdate		=	 	date("j, F  Y");
				$BarcodeImage		=		$splitOrderDetail['MergeUpdate']['order_barcode_image'];
				//$corriosImage		=		$postcode.'90019.png';
				$corriosImage		=		$routingcode.'.png';
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= str_replace( 'Spain' , 'ESPA?A', $address); 
				$setRepArray[] 					= $country;
				$barcode  						= $order['assign_barcode'];
				$barcodenum						= explode('.', $barcode);
				$barcodePath  					= Router::url('/', true).'img/orders/barcode/';
				$barcodeCorriosPath  			= Router::url('/', true).'img/orders/correos_barcode/';
				
				$barcodenum						= explode('.', $barcode);
			
				/**************** for tempplate *******************/
				$countryArray = Configure::read('customCountry');
				if($country == 'United Kingdom'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => $country, 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				} else if($country != 'United Kingdom'){
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Rest Of EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				} else {
					$labelDetail =	$this->BulkLabel->find('first', array('conditions' => array('BulkLabel.country' => 'Rest Of EU', 'BulkLabel.service_provider' => $splitOrderDetail['MergeUpdate']['service_provider']) ) );
				}
				//pr($labelDetail);
				if($subSource == 'Marec_FR' || $subSource == 'Marec_DE' || $subSource == 'Marec_IT' || $subSource == 'Marec_ES' || $subSource == 'Marec_UK'){
				$company 	=  'Marec';
					} else if($subSource == 'CostBreaker_UK' || $subSource == 'CostBreaker_DE' || $subSource == 'CostBreaker_FR' || $subSource == 'CostBreaker_ES' || $subSource == 'CostBreaker_IT' ){
						$company 	=  'CostBreaker';
					} else if($subSource == 'Tech_Drive_UK' || $subSource == 'Tech_Drive_FR' || $subSource == 'Tech_drive_ES' || $subSource == 'Tech_drive_DE' || $subSource == 'Tech_Drive_IT' ){
						$company 	= 'Tech Drive Supplies';
					} else if($subSource == 'RAINBOW RETAIL DE' || $subSource == 'Rainbow Retail' || $subSource == 'Rainbow_Retail_ES' || $subSource == 'Rainbow_Retail_IT'){
						$company 	= 'Rainbow Retail';
					}
					$this->loadModel( 'ReturnAddre' );
					$ret_add = $this->ReturnAddre->find('first', array( 'conditions' => array( 'company' =>  $company) ) );
					$return_address = $ret_add['ReturnAddre']['return_address'];
				/*
				$codearray = array('DP1', 'FR7');
				$spaincode = array("mad_air","mad_road","cat_air","cat_road","sp_air","sp_road");
				
					if(in_array($ref_code, $spaincode))
					{
						$labelDetail =	$this->Template->find('first', array(
															'conditions' => array(
																	'Template.location_name' => 'Spain',
																	'Template.company_name' => 'All'
																	)));
					}
					else
					{
						if(in_array($ref_code, $codearray))
						{
								$labelDetail =	$this->Template->find('first', array(
																'conditions' => array(
																		'Template.label_name' => 'deutsche_post',
																		'Template.location_name' => $country,
																		'Template.company_name' => $company
																		)));
						} 
						else 
						{
							if(  $splitOrderDetail['MergeUpdate']['reg_post_number'] != '' &&  $splitOrderDetail['MergeUpdate']['reg_num_img'] != '' )
							{
								$labelDetail =	$this->Template->find('first', array(
															'conditions' => array(
																	'Template.location_name' => $country,
																	'Template.reg_post' => '1',
																	)
																)
															);
															
									if( empty( $labelDetail ) )
									{
										
										$labelDetail =	$this->Template->find('first', array(
																'conditions' => array( 
																		'Template.location_name' => 'Rest Of EU',
																		'Template.store_name' => $subSource,
																		'Template.status' => '1',
																		'Template.reg_post' => '1'
																		)
																	)
															);
										 
									}
															
							}
							else
							{
								if( $splitOrderDetail['MergeUpdate']['custom_service_marked'] == 1 || (  $splitOrderDetail['MergeUpdate']['postal_service'] == 'Express' || $splitOrderDetail['MergeUpdate']['postal_service'] == 'Tracked' || $splitOrderDetail['MergeUpdate']['postal_service'] == 'Standard_Jpost') )
								{
									$labelDetail =	$this->Template->find('first', array(
																'conditions' => array(
																		'Template.location_name' => 'Other',
																		'Template.store_name' => $subSource, 
																		'Template.label_name' => $splitOrderDetail['MergeUpdate']['service_provider'],
																		'Template.status' => '1',
																		'Template.reg_post' => '0'
																		)
																	)
																);
									if( empty($labelDetail) )
									{
										$labelDetail =	$this->Template->find('first', array(
																'conditions' => array( 
																		'Template.location_name' => $country,
																		'Template.store_name' => $subSource,
																		'Template.status' => '1',
																		'Template.reg_post' => '0'
																		)
																	)
																);
									}
								}
								else
								{
							
									$labelDetail =	$this->Template->find('first', array(
																'conditions' => array( 
																		'Template.location_name' => $country,
																		'Template.store_name' => $subSource,
																		'Template.label_name' => $splitOrderDetail['MergeUpdate']['service_provider'],
																		'Template.status' => '1',
																		'Template.reg_post' => '0'
																		)
																	)
																);
									
									if( empty( $labelDetail ) )
									{
										if( in_array( $country , $countryArray ) && $country != 'United Kingdom' )
											{
												$labelDetail =	$this->Template->find('first', array(
																		'conditions' => array( 
																				'Template.location_name' => 'Rest Of EU',
																				'Template.store_name' => $subSource,
																				'Template.status' => '1',
																				'Template.reg_post' => '0'
																				)
																			)
																		);
											}
										else
											{
												$labelDetail =	$this->Template->find('first', array(
																		'conditions' => array( 
																				'Template.location_name' => 'Other',
																				'Template.store_name' => $subSource,
																				'Template.status' => '1',
																				'Template.reg_post' => '0'
																				)
																			)
																		);
											}
									}
								}
							}
						}
					}*/
			
				//pr($labelDetail);
				//exit;
				 $html           =  $labelDetail['BulkLabel']['html'];
				//$paperHeight    =  $labelDetail['Template']['paper_height'];
				//$paperWidth  	=  $labelDetail['Template']['paper_width'];
				//$barcodeHeight  =  $labelDetail['Template']['barcode_height'];
				//$barcodeWidth   =  $labelDetail['Template']['barcode_width'];
				//$paperMode      =  $labelDetail['Template']['paper_mode'];
				
				$barcodeimg = '<img src='.$barcodePath.$BarcodeImage.'>';
			
				$setRepArray[] 	=    $barcodeimg;
				$setRepArray[] 	=    $barcodenum[0];
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totlaWeight;
				$setRepArray[]	=	 $tax;
				$setRepArray[]	=	 $currentdate;
				$logopath		=	 Router::url('/', true).'img/';
				$setRepArray[]	=	 '<img src="'.$logopath.'logo.jpg" >';
				$logo			=	 '<img src="'.$logopath.'logo.jpg" >';
				$signature		=	 '<img src="'.$logopath.'sig.jpg" >';
				
				$setRepArray[]	=	 $logo;
				$setRepArray[]	=	 $signature;
				$setRepArray[]	=	 $splitOrderId;
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				
				$tempId			=	$splitOrderDetail['MergeUpdate']['lable_id'];
				$countryCode	=	$splitOrderDetail['MergeUpdate']['country_code'];
				
				$setRepArray[]	=	 $countryCode;
				$setRepArray[]	=	 $weight;
				$conversionRate	=	Configure::read( 'conversionRate' );
				if($currency == 'GBP')
				{
					$totalvalue = $totalvalue;
				}
				elseif($country == 'Spain' && $currency == 'EUR')
				{
					$totalvalue = $totalvalue;
				}
				elseif($country == 'United States')
				{
					$totalvalue = $totalvalue;
				}
				else
				{
					$totalvalue = number_format($totalvalue/$conversionRate, 2, '.' ,'');
				}
				
				if($totalvalue >= 1000  && $labelDetail['Template']['label_name'] == 'Jersey Post' && $country == 'United States' ){
					$numbers = array("97.11","98.32","98.52","99.72","99.94","150.27","200.45","270.62","370.82","422.11","472.31","530.71");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				else if($totalvalue >= 22  && $labelDetail['Template']['label_name'] == 'Jersey Post' && $country != 'United States'){
					$numbers = array("18.11","18.32","18.52","18.72","18.94","19.27","19.45","19.62","19.82","20.11","20.31","20.71","21.22","21.41", "21.61","21.51");
					shuffle($numbers);					
					$totalvalue = $numbers[0];
				}
				
				$setRepArray[]	=	 $totalvalue;
				if($splitOrderDetail['MergeUpdate']['track_id'] != '')
				{
					$trackingnumber = $splitOrderDetail['MergeUpdate']['track_id'];
				}
				else
				{
					$trackingnumber = 'A'.mt_rand(100000, 999999);
				}
				
				$setRepArray[]	=	 $trackingnumber;
				$setRepArray[]	=	 $subSource;
				$setRepArray[]	=	 $CARTAS;
				$setRepArray[]	=	 $splitOrderDetail['MergeUpdate']['reg_num_img'];
				$setRepArray[]	=	 '<img src='.$barcodeCorriosPath.$corriosImage.'>';
				$setRepArray[]	=	($phone != '') ? 'Telefono del cliente:'.$phone : '';
				$setRepArray[]	=	$return_address;
				$setRepArray[]	=	WWW_ROOT;
				$cssPath = WWW_ROOT .'css/';
				$imgPath = Router::url('/', true) .'img/';
/* POSTNL UK LABEL 
			    $html = '<body>
						  <table>
							<tr>
							 <td width="20%" valign="middle"> <img src="http://xsensys.com/img/priority.jpg" width="150px"></td>
							 <td width="80%" align="right" height="80px" style="padding-left:10px;text-align:right" valign="middle">
							    <img src="http://xsensys.com/img/postnlUK.jpg" height="80px">
							 </td>
							 </tr>
							<tr>
								<td width="20%" valign="top"> </td>
								<td width="80%" style="font-size:16px;height:180px;">_ADDRESS_  </td>
							</tr>				
       						  <tr>
								 <td width="20%" valign="middle">
									<div class="barcode left">
									<div style="text-align: center;">_BARCODEIMAGE_
									<div style="text-align: center; margin-top:-15px">_SPLITORDERID_</div>
									</div>
								 </div>
								</td>
			<td width="80%" valign="top">
			<div style="padding:5px 0 0 10px; font-size:12px; text-align: left; line-height:2;">If undelivered, please return to: <br>PostBus 7124, 3109 AC Schiedam, Netherlands</div>
			</td>
       </tr>
      </table>
	
   
</body>';

*/

/* POSTNL Germany LABEL 
$html = '<body>
<!-- Germany Service Label-->
<div id="label" style="margin-top:3px;">
		<table style="padding:2px 0;">
			<tr>
				<td>
					<table style="width:100%" >
						<tr>
							<td width="40%" align="left" valign="top" style="font-size:10px; line-height:1.5">
								<br>Postbus 7111, 3109 AC Schiedam, Netherlands</u>
							</td>
							<td width="20%" valign="top" align="center">
								<img style="padding-top:1px" src="http://xsensys.com/img/priority.jpg" width="140px">
							</td>
							<td width="40%" valign="top" align="right" rowspan="2">
								<img style="padding-top:1px" src="http://xsensys.com/img/postnl.jpg" width="140px">
							</td>
						  </tr>
					 </table>
					</td>
				 </tr>
			</table>
			
			<table>
			
				<tr>
					<td  valign="bottom" align="left"></td>
					<td width="70%" Valign="right">
						<table>
							<tr>
								<td colspan="2" Valign="top">
									<div style="margin-top:0px; text-align: left;height:150px;">
										<div style="font-size:16px;">
											 _ADDRESS_
										 </div>
									</div>
     							</td>
						      </tr>
							  
							</table>
						  </td>
					  </tr>
					  <tr>
						<td colspan="2" valign="middle" align="center">
									<div class="barcode left">
									<div style="text-align: center;">_BARCODEIMAGE_
									<div style="text-align: center; margin-top:-15px">_SPLITORDERID_</div>
									</div>
								 </div>
						</td>
					</tr>
				 </table>
			
		</div>
	
   
</body>';
*/


/* POSTNL EU LABEL 
 $html = '<body>
						  <!-- Germany EU Label-->
<div id="label" style="margin-top:3px;">

		<table style="padding:2px 0;">
			<tr>
				<td>
					<table style="width:100%" >
						<tr>
							<td width="45%" align="left" valign="top" style="font-size:10px; line-height:1.5">
								<br><u> Postbus 7111, 3109 AC Schiedam, Netherlands</u>
							</td>
							<td width="25%" valign="top" align="center">
								<img style="padding-top:1px" src="http://xsensys.com/img/priority.jpg" width="160px">
							</td>
							<td width="30%" valign="top" align="right" rowspan="2">
								<img style="padding-top:1px" src="http://xsensys.com/img/postnl.jpg" width="150px">
							</td>
						  </tr>
					 </table>
					</td>
				 </tr>
			</table>
			
			<table>
			
				<tr>
					<td  valign="bottom" align="left"></td>
					<td width="70%" Valign="right">
						<table>
							<tr>
								<td colspan="2" Valign="top">
									<div style="margin-top:0px; text-align: left;height:135px; width=300px;">
										<div style="padding:8px; font-size:16px;">
											 _ADDRESS_
										 </div>
									</div>
     							</td>
						      </tr>
							  
							</table>
						  </td>
					  </tr>
					  <tr>
						<td colspan="2" align="center" valign="bottom">
									<div style="padding-left: 70px;">_SPLITORDERID_</div>
									<div>_BARCODEIMAGE_</div>
									
							
						</td>
					</tr>
				 </table>

		</div>
	
   
</body>';*/

/* RR UK LABEL  
 $html = '<body>
                   <table>
							<tr>
							<td valign="middle" align="right" height="80px" style="text-align:right"> <img src="http://xsensys.com/img/priority.jpg" width="150px"></td>
							 <td valign="middle" align="right" height="80px" style="padding-left:5px;text-align:right">
							    <img src="http://xsensys.com/img/postnlUK.jpg" height="80px">
							 </td>
							 </tr>
							<tr>
								<td width="20%" valign="top"> </td>
								<td width="80%" style="font-size:16px;height:170px;">_ADDRESS_  </td>
							</tr>				
       						  <tr>
								 <td valign="middle">
									<div class="barcode left">
									<div style="text-align: center;">_BARCODEIMAGE_
									<div style="text-align: center; margin-top:-15px">_SPLITORDERID_</div>
									</div>
								 </div>
								</td>
			<td valign="top">
			  <div class="footer row" style="padding:5px 0 0 10px; font-size:12px; text-align: left; line-height:2; border-bottom:0px solid #000000; ">If undelivered, please return to: <br>PostBus 7097, 3109 AC Schiedam, Netherlands</div>
			</td>
       </tr>
      </table>
</body>';*/

/* Recorded Service Label   
 $html = '<body>
                  <!-- Recorded Service Label-->
		<div id="label" style="margin-top:3px;">
   <div class="container" style="border:2px solid #000000; margin-top:0px;height:342px; padding:0px;">
      <table style="padding:2px 0;">
         <tr>
            <td>
               <table style="width:100%" >
                  <tr>
                     <td width="41%" align="left" valign="top" style="font-size:12px; line-height:1.5">
					 If undelivered, please return to:<br><u>P.O. Box 7097, 3109 AA Schiedam,  Netherlands</u>                  
                     </td>
					 <td width="18%" valign="top" align="center"><img style="padding-top:1px" src="http://xsensys.com/img/priority.jpg" width="140px"></td>
					 <td width="41%" valign="top" align="right" rowspan="2"><img style="padding-top:1px" src="http://xsensys.com/img/postnl.jpg" width="140px">
					  </td>
                  </tr>
				  <tr>
				  <td colspan="2">
				  <div style="margin-top:1px; text-align: center; border:1px solid #000000; padding:0px; width=480px;">
                <div class="center" style="font-size:14px;">
				<table>
				<tr>
				<td width="5%" style="font-weight:bold;font-size:20px; ">R</td>
				<td width="95%" align="center">Registered/Recommande</td>
				</tr>
				</table>
            <div style="padding:2px;"><img src=http://xsensys.com/code39barcode/RD420524714NL.png width="480px" height="80" ></div>
         </div> 
		 </div>
				  </td>
				  </tr>
               </table>
            </td>
         </tr>
      </table>
     
    <table>
         <tr>
            <td width="50%" valign="bottom" align="left">
			<center>
			_SPLITORDERID_
			<br>
			_BARCODEIMAGE_
			<br>
			_PHONENUMBER_
			</center>
			</td>
			<td width="50%"><table>
                           <tr>
                              <td valign="top" class="leftside leftheading address" style="font-size:16px; padding-top:0px; height:100px">
                                 _ADDRESS_
                           </tr>
                        </table></td>
			
         </tr>
      </table>
   </div>
</div>
</body>';	*/ 	


/*$html = '<body>
                 			 
<table class="header row">
<tr>
<td width="55%"><img src="http://xsensys.com/img/jersey_logo.jpg" width="130px"></td>
<td width="45%" align="right">
<table class="jpoststamp ">
<tr>
	<td class="stampnumber">1</td>
	<td class="jerseyserial">Postage Paid<br>
	Jersey<br>
	Serial 216</td>
</tr>
<tr>
   <td colspan="2" class="vatnumber">UK Import VAT Pre-Paid Auth No 393</td>
</tr>
</table>
</td>
</tr>
</table>
<div class="cn22 row">
<table>
<tr>
<td class="leftside leftheading"><h1>CUSTOMS DECLARATION</h1>Great Britain </td>
<td> <h1 style="">Important!</h1> </td>
<td class="rightside rightheading"><h1>CN 22</h1> May be opened officially</td>
</tr>
</table>
	<div class="producttype">
			<table>
				<tr>
					<td width="15%"><span>&nbsp;</span>Gift</td>
					<td width="25%"><span>&nbsp;</span>Documents</td>
					<td width="30%"><span>&nbsp;</span>Commercial</td>
					<td width="30%"><span>X</span>Merchandise</td>
				</tr>
			</table>

	</div>
<table class="" cellpadding="0px" cellspacing="0" style="border-collapse:collapse" width="100%">
	<tr>
	<th class="noleftborder topborder rightborder bottomborder" width="60%">Description of contents</th>
	<th valign="top" class="center topborder norightborder bottomborder " width="20%">Weight (kg)</th>
	</tr>
_ORDERDETAIL_


	<tr>
	<td class="noleftborder topborder bottomborder rightborder " >HS Tariff, origin JE3 7BY, Jersey</td>
	<td valign="top" class="center topborder bottomborder norightborder bold" >Total Weight : _TOTALWEIGHT_</td>
	</tr>
	<tr>
	<td class="right noleftborder bottomborder rightborder "></td>
	<td valign="top" class="center bottomborder norightborder  " ><b>Total Value: &#163;_TOTALVALUE_</b></td>
	</tr>
	<tr>
	
	<td colspan="2" valign="top" class="bottomborder norightborder" >
	I the undersigned, whose name and address are given on the item, certify that the particulars given in this declaration are correct and that this item does not contain any dangerous article or articles prohibited by legislation or by postal or customs regulations.
<span class="date bold" style="margin-left:5px">&nbsp;&nbsp; _CURRENTDATE_ &nbsp;&nbsp;<img src="http://xsensys.com/img/signature.png" width="50" height="15" style="margin-top:-3px"></span>
	</td>
	</tr>
</table>
</div>
<div class="footer row">
<table>
<tr>
<td width="90%" class="leftside leftheading address"><h3>SHIP TO:</h3>
_ADDRESS_
</td>
<td width="10%" valign="bottom" align="right">
_SPLITORDERID_
</td>
</tr>

</table>
</div>
<div class="footer row">If undelivered please return to: Unit 4, Cargo Centre, Jersey Airport, L\'Avenue de la Commune, St Peter, JE3 7BY</div>

</body>';*/		
	$html = '<table>
						 	<tr >
								<td width=50% style="border: 1px solid #ccc">
									<table>
										<tr>									
											<td>	
												<div><strong>Remitente</strong></div>				
												<div><strong>ESL LOGISTICS</strong><br>CTL CHAMARTIN<br>AVDA. PIO XII 106-108<br>28070 MADRID</div>
											</td>
											<td>
												<table cellpadding="0" cellspacing="0" width="80%" style="border:1px solid #000;">
													<tr>					 
														<td>
															<div style="text-align: center;font-weight:bold;">
																<div style="border-bottom:1px solid #000; padding:2px;"> FRANQUEO <br> PAGADO </div>
																<div style="padding:2px 5px;">Distribuci&#243;n <br> Internacional </div>
															</div>
														</td>				
													</tr>
												</table>
											</td>	
										</tr>
										<tr>
											<td align="left" colspan="2" > 
												<div style="border-bottom:1px solid; width:80%;font-size:12px; margin-top: 10px; "><strong>Destinatario</strong></div>		
												 <div style="font-size:13px; ">_ADDRESS_</div>
											</td>
										</tr>
										<tr>
											<td colspan="2" align="center" style="padding-bottom:1px; padding-top:1px;"><div>ENVÍO ORDINARIO</div> <div style="text-align: center;"><center> _BARCODEIMAGE_</center> <div></td>
										</tr> 
										<tr>
											<td colspan="2" align="center"><div style="margin-top:1px;">_CORRIOSBARCODE_</div></td>
										</tr> 
									</table>
								</td>
								
								<td width=50% style="border: 1px solid #ccc" valign="top">
									<table style="border:1px solid #000;">
										<tr valign="top">
											<td width=48%><strong>CUSTOMS DECLARATION</strong></td>
											<td style="text-align:right;"><strong>CN 22</strong></td>
										</tr>
										<tr valign="top">
											<td width=48% style="font-size:10px;">DECLARATION EN DOUANE</td>
											<td style="text-align:right;">May be opened officially</td>
										</tr>
										<tr>
											<td>Great Britain / Important!</td>
											<td style="text-align:right;"></td>
										</tr>
										<tr>
											<td width="15%"><span style="height:25;width:25; border:2px solid gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Gift</td>
											<td width="25%"><span style="height:25;width:25; border:2px solid gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Documents</td>
										</tr>
										<tr>
											<td width="30%"><span style="height:25;width:25; border:2px solid gray">&nbsp;X&nbsp;</span>Commercial</td>
											<td width="30%"><span style="height:25;width:25; border:2px solid gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Merchandise</td>
										</tr>
									</table>
									<table class="" cellpadding="1px" cellspacing="0" style="border-collapse:collapse" width="100%">
										<tr>
											<th width="60%" style="border:1px solid #000; padding:5px;">Quantity and detailed</th>
											<th valign="top" width="20%"  style="border:1px solid #000; padding:5px;">Weight</th>
											<th valign="top" width="20%"  style="border:1px solid #000; padding:5px;">Qty</th>
										</tr>
										_ORDERDETAIL_
										
										<tr>
											<td  width="60%" style="border:1px solid #000; padding:10px; padding:5px;"> <span class="bold">For commercial items only</td>
											<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">WEIGHT _TOTALWEIGHT_</td>
											<td width="20%" style="text-align:center; border:1px solid #000; padding:5px;">VALUE &#8364; _TOTALVALUE_</td>
										</tr>
										</table>
											<div class="fullwidth" style="border:1px solid #000; padding:5px;"><p>I the undersigned, whose name and address are given on the item, certify that the particulars given in this declaration are correct and that this item does not contain any dangerous article or articles prohibited by legislation or by postal or customs regulations.</p>
										<table>
										<tr>
											<td class="date bold">_CURRENTDATE_</td>
											<td class="sign right"><img src="http://xsensys.com/img/signature.png" style="margin-top:-5px"></td>
										</tr>
										</table>	
										
										</table>
								</td>
							</tr>
						 </table>';
				
				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				
				$html 	= $obj->setReplaceValueLabel( $setRepArray, $html );
				return $html;
		}
		
		public function getSlipnLabel()
		{
			   	$this->layout = 'index';
			   	App::uses('Folder', 'Utility');
				App::uses('File', 'Utility');
				$imgPath = WWW_ROOT .'img/printPDF/Bulk/'; 
				$dir = new Folder($imgPath, true, 0755);
				$files = $dir->find('.*\.pdf');
				$files = Set::sort($files, '{n}', 'DESC');
				$this->set('files', $files);
			   	$this->set('title','Test Label and Slip');
			   
		}
	
		public function setServiceForEurapo( $splitOrderID = null, $ids = null, $productOrderId = null, $orderLength = null, $orderWidth = null, $orderHeight = null, $orderItemMain = null, $totalWeight = null,$destinationCountry = null )
		{
			
			$this->loadModel('PostalServiceDesc');
			$this->loadModel('MergeUpdate');
			
			$europ_ter	=	array( 'Liechtenstein','Switzerland','Iceland','Norway','Albania','Andorra','Bosnia and Herzegovina','Macedonia','Montenegro','San Marino','Serbia','Vatican City State','Belarus','Ukraine','Georgia','Gibraltar','Moldova Republic of','Greenland','Greenland','Faroe Islands','Russia','Svalbard and Jan Mayen Islands','Channel Islands','Isle of Man','Martinique','Guadaloupe' );
			
			$countryCode	=	 $isoCode[$destinationCountry];
			$serviceCode 	= 	 array_search($destinationCountry, $gatservicearray);
			if(in_array($destinationCountry, $europ_ter) )
			{
				$filterResults	=	$this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $totalWeight, 'PostalServiceDesc.max_length >=' => $orderLength,'PostalServiceDesc.max_width >=' => $orderWidth,'PostalServiceDesc.max_height >=' => $orderHeight, 'PostalServiceDesc.courier' => 'Jersey Post','PostalServiceDesc.provider_ref_code' => 'ERE' )));
			} 
				if($filterResults)
				{ 		
					$k = 0;	
					foreach($filterResults as $filterResult)
						{
							if($filterResult['ServiceLevel']['service_name'] == 'Standard')
							{
									if($filterResult['PostalServiceDesc']['courier'] == 'Jersey Post')
									{
										$perItem[$k] 		=	$filterResult['PostalServiceDesc']['per_item'];
										$perkilo[$k] 		=	$filterResult['PostalServiceDesc']['per_kilo'];
										$weightKilo[$k] 	=	$filterResult['PostalServiceDesc']['max_weight'];
										$postalid[$k] 		=	$filterResult['PostalServiceDesc']['id'];
										$ccyprice[$k] 		=	$filterResult['PostalServiceDesc']['ccy_prices'];
										$k++;														
									}
							}																					
						}
						$getPerItem_PerKilo = $this->getAdditionOfItem_PerKilo( $perItem , $perkilo , $weightKilo, $postalid, $ccyprice );
						$id	=	array_keys($getPerItem_PerKilo, min($getPerItem_PerKilo));
						unset($perItem); unset($perkilo); unset($weightKilo);
						$postalservicessel 		= 	 $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $id[0]) ));
						if($postalservicessel)
						{
							$postalServiceID		=	 $postalservicessel['PostalServiceDesc']['id'];
							$postalProvider			=	 $postalservicessel['PostalServiceDesc']['courier'];
							$providerRefCode		=	 $postalservicessel['PostalServiceDesc']['provider_ref_code'];
							$serviceLavel			=	 $postalservicessel['ServiceLevel']['service_name'];
							$serviceName			=	 $postalservicessel['PostalServiceDesc']['service_name'];
							$lvcr					=	 $postalservicessel['PostalServiceDesc']['lvcr'];
							$manifest				=	 $postalservicessel['PostalServiceDesc']['manifest'];
							$length					=	 $postalservicessel['PostalServiceDesc']['max_length'];
							$width					=	 $postalservicessel['PostalServiceDesc']['max_width'];
							$height					=	 $postalservicessel['PostalServiceDesc']['max_height'];
							$postalserviceID		=	 $postalservicessel['PostalServiceDesc']['id'];
							$warehouse				=	 $postalservicessel['PostalServiceDesc']['warehouse'];
							$warehouse				=	 $postalservicessel['PostalServiceDesc']['warehouse'];
							$templateid				=	 $postalservicessel['PostalServiceDesc']['template_id'];
							$manifest				=	 $postalservicessel['PostalServiceDesc']['manifest'];
							$lvcr					=	 $postalservicessel['PostalServiceDesc']['lvcr'];
							$cnrequired				=	 $postalservicessel['PostalServiceDesc']['cn_required'];
						}
						$data['MergeUpdate']['service_name'] 		= $serviceName;
						$data['MergeUpdate']['provider_ref_code'] 	= $providerRefCode;
						$data['MergeUpdate']['service_id'] 			= $postalServiceID;
						$data['MergeUpdate']['service_provider'] 	= $postalProvider;
						$data['MergeUpdate']['packet_weight'] 		= $totalWeight;
						$data['MergeUpdate']['packet_length'] 		= $orderLength;
						$data['MergeUpdate']['packet_width'] 		= $orderWidth;
						$data['MergeUpdate']['packet_height'] 		= $orderHeight;
						$data['MergeUpdate']['warehouse'] 			= $warehouse;
						$data['MergeUpdate']['delevery_country'] 	= $destinationCountry;
						$data['MergeUpdate']['template_id'] 		= $templateid;
						$data['MergeUpdate']['manifest'] 			= $manifest;
						$data['MergeUpdate']['lvcr'] 				= $lvcr;
						$data['MergeUpdate']['cn_required'] 		= $cnrequired;
						$data['MergeUpdate']['postal_service'] 		= 'Standard';
						$data['MergeUpdate']['country_code'] 		= $countryCode;
						$data['MergeUpdate']['id'] 					= $splitOrderID;
						$this->MergeUpdate->saveAll( $data );
				}
				else
				{
					$data['MergeUpdate']['service_name'] = "Over Weight";
					$data['MergeUpdate']['provider_ref_code'] = "Over Weight";
					$data['MergeUpdate']['service_id'] = "Over Weight";
					$data['MergeUpdate']['service_provider'] = "Over Weight";
					$data['MergeUpdate']['id'] 			= $splitOrderID;
					$this->MergeUpdate->saveAll( $data );
				}
		}
	
		public function glsManifest(){
			
			$this->loadModel( 'MergeUpdate' ); 
			$this->loadModel( 'OpenOrder' );
			$this->loadModel( 'GlsSpain' );
						
			$orders = $this->MergeUpdate->find('all', array('conditions' => array(	'MergeUpdate.status' => 1, 
																					'MergeUpdate.delevery_country'=>'Spain',
																					'MergeUpdate.scan_status' => 1,
																					'MergeUpdate.sorted_scanned' => 1,
																					'MergeUpdate.manifest_status' => 2,
																					'MergeUpdate.gls_shipment_barcode IS NOT NULL',
																					'MergeUpdate.gls_shipment_ref IS NOT NULL'
																					) ) );
																					
																					
																									
			$orders = $this->MergeUpdate->find('all', array( 'conditions' => array('gls_shipment_ref IS NOT NULL','gls_shipment_barcode IS NOT NULL')) );
			//pr($orders);
			App::import('Vendor', 'PHPExcel/IOFactory');
			App::import('Vendor', 'PHPExcel'); 
			$getBase 		= 	Router::url('/', true);
			$uploadUrl 		= 	WWW_ROOT .'gls/manifests/delivery_note.xlsx';
			$uploadRemote 	= 	$getBase.'gls/manifests/delivery_note.xlsx';
			$objPHPExcel = new PHPExcel();
			
			if(count($orders) > 0){
				
				
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->mergeCells('A1:R1');
				$objPHPExcel->getActiveSheet()->mergeCells('A2:R2');
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ), 'font'  => array('bold'  => true,'color' => array('rgb' => '000000'),'size'  => 14) );
				$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'GLS    Customer Number');
				$objPHPExcel->getActiveSheet()->SetCellValue('A2', 'Manifesto cierre: '.date('d/m/Y H:is'));
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
				
				$objPHPExcel->getActiveSheet()->SetCellValue('A6', 'Expedicion');
				$objPHPExcel->getActiveSheet()->SetCellValue('B6', 'Doc');
				$objPHPExcel->getActiveSheet()->SetCellValue('C6', 'Ref. GLS');
				$objPHPExcel->getActiveSheet()->SetCellValue('D6', 'Ref. Client');
				$objPHPExcel->getActiveSheet()->SetCellValue('E6', 'Date');
				$objPHPExcel->getActiveSheet()->SetCellValue('F6', 'Service');
				$objPHPExcel->getActiveSheet()->SetCellValue('G6', 'Hour');
				$objPHPExcel->getActiveSheet()->SetCellValue('H6', 'Parcels');
				$objPHPExcel->getActiveSheet()->SetCellValue('I6', 'Kgs');
				$objPHPExcel->getActiveSheet()->SetCellValue('J6', 'Refnd');
				$objPHPExcel->getActiveSheet()->SetCellValue('K6', 'Portes');
				$objPHPExcel->getActiveSheet()->SetCellValue('L6', 'DAC');
				$objPHPExcel->getActiveSheet()->SetCellValue('M6', 'Ret.');
				$objPHPExcel->getActiveSheet()->SetCellValue('N6', 'Addressee');
				$objPHPExcel->getActiveSheet()->SetCellValue('O6', 'Department');
				$objPHPExcel->getActiveSheet()->SetCellValue('P6', 'Address');
				$objPHPExcel->getActiveSheet()->SetCellValue('Q6', 'Location');
				$objPHPExcel->getActiveSheet()->SetCellValue('R6', 'Country');
				
				
				
				$BStyle = array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)));
				
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
				$objPHPExcel->getActiveSheet()->SetCellValue('F13', 'CUSTOMER NUMBER');
				$objPHPExcel->getActiveSheet()->mergeCells('F13:G13');
				$objPHPExcel->getActiveSheet()->getStyle('F13:G13')->getFont()->setSize(14);	
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('F13:G13')->applyFromArray($styleArray);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
				$objPHPExcel->getActiveSheet()->SetCellValue('F14', 'VAT NUMBER');
				$objPHPExcel->getActiveSheet()->mergeCells('F14:G14');
				$objPHPExcel->getActiveSheet()->getStyle('F14:G14')->getFont()->setSize(14);	
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('F14:G14')->applyFromArray($styleArray);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
				$objPHPExcel->getActiveSheet()->SetCellValue('F16', 'PRODUCT');
				$objPHPExcel->getActiveSheet()->mergeCells('F16:G16');
				$objPHPExcel->getActiveSheet()->getStyle('F16:G16')->getFont()->setSize(14);	
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('F16:G16')->applyFromArray($styleArray);
				
				$centerArray = array('horizontal'=>PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'=>PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'=> 0,'wrap'=> true);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
				$objPHPExcel->getActiveSheet()->SetCellValue('I11', 'ESL LIMITED');
				$objPHPExcel->getActiveSheet()->mergeCells('I11:P11');
				$objPHPExcel->getActiveSheet()->getStyle('I11:P11')->getFont()->setSize(14);	
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('I11:P11')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('I11:P11')->getAlignment()->applyFromArray($centerArray );
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
				$objPHPExcel->getActiveSheet()->SetCellValue('I12', '54024267');
				$objPHPExcel->getActiveSheet()->mergeCells('I12:P12');
				$objPHPExcel->getActiveSheet()->getStyle('I12:P12')->getFont()->setSize(14);	
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('I12:P12')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('I12:P12')->getAlignment()->applyFromArray($centerArray );
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
				$objPHPExcel->getActiveSheet()->SetCellValue('I13', '9980958776');
				$objPHPExcel->getActiveSheet()->mergeCells('I13:P13');
				$objPHPExcel->getActiveSheet()->getStyle('I13:P13')->getFont()->setSize(14);	
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('I13:P13')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('I13:P13')->getAlignment()->applyFromArray($centerArray );
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
				$objPHPExcel->getActiveSheet()->SetCellValue('I14', '0043148');
				$objPHPExcel->getActiveSheet()->mergeCells('I14:P14');
				$objPHPExcel->getActiveSheet()->getStyle('I14:P14')->getFont()->setSize(14);	
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('I14:P14')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('I14:P14')->getAlignment()->applyFromArray($centerArray );
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
				$objPHPExcel->getActiveSheet()->SetCellValue('I15', '');
				$objPHPExcel->getActiveSheet()->mergeCells('I15:P15');
				$objPHPExcel->getActiveSheet()->getStyle('I15:P15')->getFont()->setSize(14);	
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('I15:P15')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('I15:P15')->getAlignment()->applyFromArray($centerArray );
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
				$objPHPExcel->getActiveSheet()->SetCellValue('I16', utf8_encode('DISTRIBUCIÓN INTERNACIONAL (ORDINARY MAIL)'));
				$objPHPExcel->getActiveSheet()->mergeCells('I16:P16');
				$objPHPExcel->getActiveSheet()->getStyle('I16:P16')->getFont()->setSize(14);	
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('I16:P16')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('I16:P16')->getAlignment()->applyFromArray($centerArray );
				
				$objPHPExcel->getActiveSheet()->mergeCells('E20:Q21');
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('E20:Q21')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('E22:Q41')->applyFromArray($BStyle);
				
				$objPHPExcel->getActiveSheet()->SetCellValue('F23', 'PRODUCT');
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('F23')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('F23')->applyFromArray($BStyle);
				
				$objPHPExcel->getActiveSheet()->mergeCells('H23:K23');
				$objPHPExcel->getActiveSheet()->SetCellValue('H23', utf8_encode('DISTRIBUCIÓN INTERNACIONAL (OM)'));
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('H23:K23')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('H23:K23')->applyFromArray($BStyle);
				
				$objPHPExcel->getActiveSheet()->mergeCells('E25:I25');
				$objPHPExcel->getActiveSheet()->SetCellValue('E25', 'COUNTRY');
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('E25:I25')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('E25:I25')->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->getStyle('E25:I25')->getAlignment()->applyFromArray($centerArray );
				
				$objPHPExcel->getActiveSheet()->SetCellValue('J25', 'CODE');
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('J25')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('J25')->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->getStyle('J25')->getAlignment()->applyFromArray($centerArray );
				
				$objPHPExcel->getActiveSheet()->mergeCells('K25:M25');
				$objPHPExcel->getActiveSheet()->SetCellValue('K25', 'TOTAL WEIGHT IN GRAMS');
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('K25:M25')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('K25:M25')->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->getStyle('K25:M25')->getAlignment()->applyFromArray($centerArray );
				
				$objPHPExcel->getActiveSheet()->mergeCells('N25:Q25');
				$objPHPExcel->getActiveSheet()->SetCellValue('N25', 'NUMBER OF ITEMS');
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('N25:Q25')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('N25:Q25')->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->getStyle('N25:Q25')->getAlignment()->applyFromArray($centerArray );
				
				/*Dynamic code*/
				$objPHPExcel->getActiveSheet()->mergeCells('E26:I26');
				$objPHPExcel->getActiveSheet()->SetCellValue('E26', 'SPAIN');
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('E26:I26')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('E26:I26')->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->getStyle('E26:I26')->getAlignment()->applyFromArray($centerArray );
				
				$objPHPExcel->getActiveSheet()->SetCellValue('J26', 'ES');
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('J26')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('J26')->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->getStyle('J26')->getAlignment()->applyFromArray($centerArray );
				
				$objPHPExcel->getActiveSheet()->mergeCells('K26:M26');
				$objPHPExcel->getActiveSheet()->SetCellValue('K26', '1.25');
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('K26:M26')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('K26:M26')->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->getStyle('K26:M26')->getAlignment()->applyFromArray($centerArray );
				
				$objPHPExcel->getActiveSheet()->mergeCells('N26:Q26');
				$objPHPExcel->getActiveSheet()->SetCellValue('N26', '10');
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('N26:Q26')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('N26:Q26')->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->getStyle('N26:Q26')->getAlignment()->applyFromArray($centerArray );
				/***************************/
				
				/**************************/
				for($j=27; $j < 42 ; $j++)
				{
					$objPHPExcel->getActiveSheet()->mergeCells('E'.$j.':I'.$j.'');
					$objPHPExcel->getActiveSheet()->SetCellValue('N25', 'NUMBER OF ITEMS');
					$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
					$objPHPExcel->getActiveSheet()->getStyle('E'.$j.':I'.$j.'')->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$j.':I'.$j.'')->applyFromArray($BStyle);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$j.':I'.$j.'')->getAlignment()->applyFromArray($centerArray );
					
					$objPHPExcel->getActiveSheet()->SetCellValue('J'.$j.'', '');
					$objPHPExcel->getActiveSheet()->getStyle('J'.$j.'')->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('J'.$j.'')->applyFromArray($BStyle);
					$objPHPExcel->getActiveSheet()->getStyle('J'.$j.'')->getAlignment()->applyFromArray($centerArray );
					
					$objPHPExcel->getActiveSheet()->mergeCells('K'.$j.':M'.$j.'');
					$objPHPExcel->getActiveSheet()->SetCellValue('K'.$j.'', '');
					$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
					$objPHPExcel->getActiveSheet()->getStyle('K'.$j.':M'.$j.'')->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('K'.$j.':M'.$j.'')->applyFromArray($BStyle);
					$objPHPExcel->getActiveSheet()->getStyle('K'.$j.':M'.$j.'')->getAlignment()->applyFromArray($centerArray );
					
					$objPHPExcel->getActiveSheet()->mergeCells('N'.$j.':Q'.$j.'');
					$objPHPExcel->getActiveSheet()->SetCellValue('N'.$j.'', '');
					$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
					$objPHPExcel->getActiveSheet()->getStyle('N'.$j.':Q'.$j.'')->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('N'.$j.':Q'.$j.'')->applyFromArray($BStyle);
					$objPHPExcel->getActiveSheet()->getStyle('N'.$j.':Q'.$j.'')->getAlignment()->applyFromArray($centerArray );
				}
				
				/*************************/
				$objPHPExcel->getActiveSheet()->mergeCells('E43:Q44');
				$objPHPExcel->getActiveSheet()->SetCellValue('E43', utf8_encode('This document will not be valid without the mechanical confirmation or authorised stampo and signature from Correos y Telégrafos. Correos y Telégrafos reserves the right to modify this Delivery Note in case errors are detected at the time revenue protection procedures are carried out or by any other reason that may be considered relevant.'));
				$objPHPExcel->getActiveSheet()->getStyle('E43:Q44')->getFont()->setSize(9);
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('E43:Q44')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('E43:Q44')->getAlignment()->applyFromArray($centerArray );
				
				
				$objPHPExcel->getActiveSheet()->mergeCells('E46:M51');
				$objPHPExcel->getActiveSheet()->SetCellValue('E46', utf8_encode("On behalf of  Correos y Telégrafos SAE:\nADMISSION ENTRY:                                                DATE:"));
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('E46:M51')->applyFromArray($styleArray);
				$alignArray = array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,'rotation' => 0,'wrap' => true);
				$objPHPExcel->getActiveSheet()->getStyle('E46:M51')->getAlignment()->applyFromArray($alignArray );
				
				$objPHPExcel->getActiveSheet()->mergeCells('N46:Q51');
				$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ) ) );
				$objPHPExcel->getActiveSheet()->getStyle('N46:Q51')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setTitle('Delivery Note');
				
				
				
				
				$filename = date('Ymd_His')."_gls.csv";
				
				file_put_contents(WWW_ROOT."gls/manifests/".$filename, "Expedicion,Doc,Ref. GLS,Ref. Client,Date,Service,Hour,Parcels, Kgs, Refnd, Portes,DAC, Ret., Addressee, Department, Address, Location, Country\n");	
				
				                                                 
					App::import('Vendor', 'PHPExcel/IOFactory');
					App::import('Vendor', 'PHPExcel');                          
				
					foreach($orders as $val){
				
					$glsinfo = $this->GlsSpain->find('first', array( 'conditions' => array('shipment_barcode'=>$val['MergeUpdate']['gls_shipment_barcode'],'split_order_id'=>$val['MergeUpdate']['product_order_id_identify'])) );
					
					$sopen	=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.num_order_id'=>$val['MergeUpdate']['order_id'])));
					
					$general_info		=	unserialize( $sopen['OpenOrder']['general_info'] );
					$shipping_info		=	unserialize( $sopen['OpenOrder']['shipping_info'] );
					$customer_info		=	unserialize( $sopen['OpenOrder']['customer_info'] );
					$totals_info		=	unserialize( $sopen['OpenOrder']['totals_info'] );
					$items				=	unserialize( $sopen['OpenOrder']['items'] );
					
					$consignee_name 	=	rtrim($this->getcommaremovedata(utf8_decode($customer_info->Address->FullName)),' ');
					$address1 			= 	rtrim($this->getcommaremovedata(utf8_decode($customer_info->Address->Address1)),' ');
					$address2 			= 	rtrim($this->getcommaremovedata(utf8_decode($customer_info->Address->Address2)),' ');
					$consigee_zip_code 	= 	str_pad($customer_info->Address->PostCode,5,"`0",STR_PAD_LEFT);
					$consignee_city 	=	rtrim($this->getcommaremovedata(utf8_decode($customer_info->Address->Town)),' '); 
				echo	$consignee_country	=   $customer_info->Address->Country;
								
					$consignee_address  = 	rtrim($address1.' '.$address2,' ')." ".$consigee_zip_code ." ".$consignee_city;
					
					if($customer_info->Address->Region != ''){
						$consignee_address .= 	" ". $this->getcommaremovedata(utf8_decode($customer_info->Address->Region));
					}
					
					$expedicion = '771-'.$glsinfo['GlsSpain']['shipment_number'];
					
					file_put_contents(WWW_ROOT."gls/manifests/".$filename, "{$expedicion},Doc,".$val['MergeUpdate']['gls_shipment_barcode'].",".$val['MergeUpdate']['gls_shipment_ref'].",".$val['MergeUpdate']['merge_order_date'].", ".
					$val['MergeUpdate']['service_name'].",0,1,1,,P,,,{$consignee_name},,{$consignee_address},,{$consignee_country}"."\n", FILE_APPEND|LOCK_EX);
				}	
				
				
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objWriter->save($uploadUrl);
				echo $file = Router::url('/', true).'gls/manifests/delivery_note.xlsx';
				exit;
				
				
				//$uploadUrl = WWW_ROOT .'img/cut_off/'. $folderName . '/' .$service.'.csv';                                          
				//$uploadUrI = Router::url('/', true) . 'img/cut_off/'. $folderName . '/' .$service.'.csv';                                          
				//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
				//$objWriter->save($uploadUrl);		 
			}
			exit;
		}	
		public function getcommaremovedata($string = null){
				
			$special_chars = array(';'=>' ','"'=>' ',"'"=>' ','\n'=>'',','=>' ');
			$str = strtr( $string,$special_chars );
			return  $str;
		}

       public function openInfo($order_id = '3867707')
	  {	 
			$this->loadModel('OpenOrder'); 
			$this->loadModel('MergeUpdate'); 
			App::import('Controller', 'Linnworksapis');
			$linn = new LinnworksapisController();
			 
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id ) ) );
			$items = unserialize( $openOrder['OpenOrder']['items']);
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
			$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
			$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
			$shipping_info = unserialize( $openOrder['OpenOrder']['shipping_info']);
 			echo '====general_info=====';
			pr($general_info);
			echo '====items=====';
			pr($items);
			echo '====shipping_info=====';
			pr($shipping_info);
			echo '====customer_info=====';
			pr($customer_info);
		 	exit;
	}
	public function testOrder( $location = null , $locationName = null )
	{
		//die('Working on db cleanup');
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('OpenOrder');
		$this->loadModel('AssignService');
		$this->loadModel('Customer');
		$this->loadModel('OrderItem');
		$this->loadModel('Product');
		$this->loadModel('UnprepareOrder');
		
		/*App::import('Vendor', 'linnwork/api/Auth');
		App::import('Vendor', 'linnwork/api/Factory');
		App::import('Vendor', 'linnwork/api/Orders');
	
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$multi = AuthMethods::Multilogin($username, $password);
		
		$auth = AuthMethods::Authorize($username, $password, $multi[0]->Id);*/
		
		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
	
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
		//$multi = AuthMethods::Multilogin($username, $password);
		
		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

		$token = $auth->Token;	
		$server = $auth->Server;
		
	   // $openorder	=	OrdersMethods::GetOpenOrders('3000','1','','',$location,'',$token, $server);
	 
		$openorder	=	OrdersMethods::GetAllOpenOrders("","",$location,"",$token, $server);  
					
		//if($locationName == 'CostBreaker_IT'){echo 'CostBreaker_IT';} 
		/*
		$unPaids  = ['0'=>'ae4d4f66-1ab9-4909-94c4-745fe46eca6d','1'=>'7a607d6b-4efc-4eeb-9674-6a57a699a0ff','2'=>'61ae72d7-16e0-4a99-8548-3076df6d93b4','3'=>'d25d1df7-211a-475c-aa50-a10c3a9709b7'];
		
		$order_status = OrdersMethods::GetOrders($openorder,$location,true,true,$token, $server);
		pr($order_status );
		exit;*/
	 
		
		$orderids = '847f3310-e04a-4d4f-bf3b-668b2ab865e7'	;						
		$results[] = OrdersMethods::GetOrderById($orderids,$token, $server); 
		pr($results);		
		//$this->saveOpenOrder( $results , $locationName , $orders );	 
		exit;
		
		App::import('Controller', 'Virtuals');
		$virtualModel = new VirtualsController(); 
		//$virtualModel->creatFeedSheet_old( $locationName );
		$unPaid = [];	
		if(count( $openorder) > 0){
		
			$log_file = WWW_ROOT .'cron-logs/'.$locationName.".log";	
			@unlink($log_file);
		
			foreach($openorder as $orderids)
			{
					 
				file_put_contents($log_file, $orderids."\t".date('Y-m-d H:i:s')."\n",  FILE_APPEND|LOCK_EX);	
		
				//echo $orderids. "otderID <br>";
				
				//$orders[]	=	$orderids->OrderId;
				$result = $orderids;
				//exit;
				$checkOpenOrder 	=	$this->OpenOrder->find('first', array('conditions'=>array('OpenOrder.order_id' => $orderids)));
				//echo $checkOpenOrder."one <br>";
				//pr($checkOpenOrder);
				if( count($checkOpenOrder) == 0 )
				{
					//echo 'Insert >> '.$orderids->OrderId."<br>";
					$orders[]	=	$orderids;
					$orders[]	=	'187f48ef-42d2-4fc1-88dc-ff5aa759502c';
					$orders[]	= '7901a5f1-97a2-4023-9f89-4f9a1e0b2b1c';	
					
				//echo	$orderids = '420092f1-68e4-4291-a06f-f79fbee47fec'	;	
				echo	$orderids ='d16ad09c-f2b8-496e-ad25-20f8d717d6a6';					
					$results[] = OrdersMethods::GetOrderById($orderids,$token, $server); 
					$this->saveOpenOrder( $results , $locationName , $orders );	
										
				}
				else
				{
					//Clean Orders
					$this->cleanOrders();
					$unPaid[]	=	$orderids;
					//CHECK IF ORDER EXISTS OR NOT
					// ORDER STATUS -> PAID / UNPAID / RESEND / PENDING / HELD
					//$order_status = OrdersMethods::GetOrders($unPaid,$location,true,true,$token, $server);
					//pr($order_status);
					//$results = $order_status[0];
					$dataUpdate['OpenOrder']['id'] = $checkOpenOrder['OpenOrder']['id'];
					$dataUpdate['OpenOrder']['linn_fetch_orders'] = '1';//$results->GeneralInfo->Status;	
					//echo $NumOrderid.' Status-> '.$results->GeneralInfo->Status.'<br>';
					
					//$log_file = WWW_ROOT .'cron-logs/'.$locationName."_unpaid.log";		 
					//file_put_contents($log_file, print_r($order_status,true));	
		
		
					//exit;						
					//$dataUpdate['OpenOrder']['linn_fetch_orders'] = serialize($result->CustomerInfo);
					$this->OpenOrder->saveAll( $dataUpdate ); 
					//Now update into Merge Section
					$ordStatus  = '1';//$results->GeneralInfo->Status;	
					$NumOrderid = $checkOpenOrder['OpenOrder']['num_order_id'];
					//Update Query for merge section also for ensure those will present into Open order screen and Unpain etc screen
					$this->MergeUpdate->updateAll( array('MergeUpdate.linn_fetch_orders' => $ordStatus), array('MergeUpdate.order_id' => $NumOrderid) );
					unset( $unPaid );
				
				}
				
				//$orders[]	=	$orderids->OrderId;
			}
			//print_r($orders);
			//exit;
			$result = '';
			unset( $checkOpenOrder );
			
			//$orders[]	=	'd169ad13-d9b7-4653-a782-ebe561e885d5';
			
			if( !empty( $orders ) && count( $orders ) > 0 )
			{
				
				$results = OrdersMethods::GetOrders($orders,$location,true,true,$token, $server);
				////pr($results);
				//$locationName .'== '. count( $results );					
				$this->saveOpenOrder( $results , $locationName , $orders );	
				$log_file = WWW_ROOT .'cron-logs/ord_'.$locationName."_".date('Ymd').".log";		 
				file_put_contents($log_file, print_r($results,true), FILE_APPEND | LOCK_EX);				
				
			}
		}else{
		 echo 'no open orders';				
		}
		
		$log_file = WWW_ROOT .'cron-logs/crons_'.date('Ymd').".log";	
		file_put_contents($log_file, $locationName."\t".date('Y-m-d H:i:s')."\n",  FILE_APPEND | LOCK_EX);	
		exit;
	}
	public function updateSupp(){
		
		$this->loadModel('Product');
		$this->loadModel('SupplierDetail');
		$this->loadModel('SupplierMapping');
		
		$uploadUrl = WWW_ROOT .'rng_supplier_mapping(1).csv';
		
		$row = 1;
		if (($handle = fopen($uploadUrl, "r")) !== FALSE) {
		  while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
  			$row++;
			$save = [];
  			$pros = $this->Product->find('first', array('conditions'=>array('product_sku' => $data[1]),'fields'=>['product_name']));
			if(count($pros) > 0){
 				$save['master_sku'] = $data[1];
				$save['title']      = $pros['Product']['product_name'];
				$save['status']		= 1;
				$sup_code = $data[3];
				if($data[3] == 'oe_20181112oege'){
					$sup_code = 'oege';
				}
				$sm = $this->SupplierMapping->find('first', array('conditions'=>array('sup_code' => $sup_code,'master_sku'=>$data[1]),'fields'=>['id']));
				if(count($sm) == 0)
				{pr($sm );
					
					
					$supps = $this->SupplierDetail->find('first', array('conditions'=>array('supplier_code' => $sup_code),'fields'=>['id']));
					
					if(count($supps) > 0){
						$save['sup_inc_id'] = $supps['SupplierDetail']['id'];
						$save['sup_code']   = $sup_code;
						$save['supplier_sku']   = $data[2];
					}else{
						echo $data[3];
					}
					$save['added_date']		= date('Y-m-d H:i:s');
					
					$this->SupplierMapping->saveAll( $save );
					pr($save);
				}
				echo '<br>';
 			}
 			
			
		  }
		  
		  fclose($handle);
		}
		exit;
		$fileData  = file_get_contents($uploadUrl );
		$data = explode("\n",$fileData);
		pr($data);
		exit;
	
	}
}



?>

