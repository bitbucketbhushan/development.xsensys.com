<?php
class PostnlController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Postnl";
    var $components = array('Session','Upload','Common','Auth','Paginator');
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
    public function Barcode()
    {
		  $this->layout = '';
		  $this->autoRender = false;
		  $CustomerCode = 'HLRQ';
		  $CustomerNumber = '10472686';
		  $api_key = 'Z7J23nyK3BOEPkR3pgIVAAcOKheVCTDV';
		  
		  $curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api-sandbox.postnl.nl/shipment/v1_1/barcode?CustomerCode={$CustomerCode}&CustomerNumber={$CustomerNumber}&Type=3S&Serie=0000000-9999999",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(				 
				"apikey: {$api_key}",
				"cache-control: no-cache"
			  ),
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
			  $r = json_decode($response,1);
			  return $r['Barcode'];
			}
		exit;  
	}
	
	public function Shipment()
	{

$p = '{
    "Customer": {
        "CustomerNumber": "11223344",
        "CustomerCode": "DEVC",
        "CollectionLocation": "1234506",
        "Address": {
            "AddressType": "02",
            "City": "Hoofddorp",
            "Countrycode": "NL",
            "Area": "<string>",
            "Buildingname": "<string>",
            "CompanyName": "PostNL",
            "Department": "<string>",
            "Doorcode": "<string>",
            "FirstName": "Peter",
            "Floor": "<string>",
            "HouseNr": "42",
            "HouseNrExt": "<string>",
            "Name": "de Ruiter",
            "Region": "<string>",
            "Street": "Siriusdreef",
            "StreetHouseNrExt": "<string>",
            "Zipcode": "2132WT"
        },
        "ContactPerson": "Janssen",
        "Email": "email@company.com",
        "Name": "Janssen"
    },
    "Message": {
        "MessageID": "1",
        "MessageTimeStamp": "29-06-2016 12:00:00",
        "Printertype": "GraphicFile|PDF"
    },
    "Shipments": [
        {
            "Addresses": [
                {
                    "AddressType": "02",
                    "Countrycode": "NL",
                    "Area": "<string>",
                    "Buildingname": "<string>",
                    "City": "Hoofddorp",
                    "CompanyName": "PostNL",
                    "Department": "<string>",
                    "Doorcode": "<string>",
                    "FirstName": "Peter",
                    "Floor": "<string>",
                    "HouseNr": "42",
                    "HouseNrExt": "<string>",
                    "Name": "de Ruiter",
                    "Region": "<string>",
                    "Street": "Siriusdreef",
                    "StreetHouseNrExt": "<string>",
                    "Zipcode": "2132WT"
                },
                {
                    "AddressType": "02",
                    "Countrycode": "NL",
                    "Area": "<string>",
                    "Buildingname": "<string>",
                    "City": "Hoofddorp",
                    "CompanyName": "PostNL",
                    "Department": "<string>",
                    "Doorcode": "<string>",
                    "FirstName": "Peter",
                    "Floor": "<string>",
                    "HouseNr": "42",
                    "HouseNrExt": "<string>",
                    "Name": "de Ruiter",
                    "Region": "<string>",
                    "Street": "Siriusdreef",
                    "StreetHouseNrExt": "<string>",
                    "Zipcode": "2132WT"
                }
            ],
            "Barcode": "3SDEVC2016112104",
            "Dimension": {
                "Weight": "2000",
                "Height": "<string>",
                "Length": "<string>",
                "Volume": "<string>",
                "Width": "<string>"
            },
            "ProductCodeDelivery": "3085",
            "Amounts": [
                {
                    "AmountType": "01",
                    "Value": "100.00",
                    "AccountName": "<string>",
                    "BIC": "<string>",
                    "Currency": "EUR",
                    "IBAN": "<string>",
                    "Reference": "<string>",
                    "TransactionNumber": "<string>"
                },
                {
                    "AmountType": "01",
                    "Value": "100.00",
                    "AccountName": "<string>",
                    "BIC": "<string>",
                    "Currency": "EUR",
                    "IBAN": "<string>",
                    "Reference": "<string>",
                    "TransactionNumber": "<string>"
                }
            ],
            "CodingText": "<string>",
            "CollectionTimeStampStart": "<string>",
            "CollectionTimeStampEnd": "<string>",
            "Contacts": [
                {
                    "ContactType": "01",
                    "Email": "receiver@email.com",
                    "SMSNr": "+31612345678",
                    "TelNr": "+31301234567"
                },
                {
                    "ContactType": "01",
                    "Email": "receiver@email.com",
                    "SMSNr": "+31612345678",
                    "TelNr": "+31301234567"
                }
            ],
            "Content": "<string>",
            "CostCenter": "<string>",
            "CustomerOrderNumber": "<string>",
            "Customs": [
                {
                    "Content": [
                        {
                            "Description": "Powdered milk",
                            "Quantity": "2",
                            "Weight": "2600",
                            "Value": "20.00",
                            "EAN": "<string>",
                            "ProductURL": "<string>",
                            "HSTariffNr": "100878",
                            "CountryOfOrigin": "NL"
                        },
                        {
                            "Description": "Powdered milk",
                            "Quantity": "2",
                            "Weight": "2600",
                            "Value": "20.00",
                            "EAN": "<string>",
                            "ProductURL": "<string>",
                            "HSTariffNr": "100878",
                            "CountryOfOrigin": "NL"
                        }
                    ],
                    "Currency": "EUR",
                    "Certificate": "<boolean>",
                    "CertificateNr": "<string>",
                    "License": "<boolean>",
                    "LicenseNr": "<string>",
                    "Invoice": "<boolean>",
                    "InvoiceNr": "INV_0120330",
                    "HandleAsNonDeliverable": "<boolean>",
                    "ShipmentType": "Commercial Goods",
                    "TrustedShipperID": "<string>",
                    "ImporterReferenceCode": "<string>",
                    "TransactionCode": "<string>",
                    "TransactionDescription": "<string>"
                },
                {
                    "Content": [
                        {
                            "Description": "Powdered milk",
                            "Quantity": "2",
                            "Weight": "2600",
                            "Value": "20.00",
                            "EAN": "<string>",
                            "ProductURL": "<string>",
                            "HSTariffNr": "100878",
                            "CountryOfOrigin": "NL"
                        },
                        {
                            "Description": "Powdered milk",
                            "Quantity": "2",
                            "Weight": "2600",
                            "Value": "20.00",
                            "EAN": "<string>",
                            "ProductURL": "<string>",
                            "HSTariffNr": "100878",
                            "CountryOfOrigin": "NL"
                        }
                    ],
                    "Currency": "EUR",
                    "Certificate": "<boolean>",
                    "CertificateNr": "<string>",
                    "License": "<boolean>",
                    "LicenseNr": "<string>",
                    "Invoice": "<boolean>",
                    "InvoiceNr": "INV_0120330",
                    "HandleAsNonDeliverable": "<boolean>",
                    "ShipmentType": "Commercial Goods",
                    "TrustedShipperID": "<string>",
                    "ImporterReferenceCode": "<string>",
                    "TransactionCode": "<string>",
                    "TransactionDescription": "<string>"
                }
            ],
            "DeliveryAddress": "01",
            "DeliveryDate": "<string>",
            "DeliveryTimeStampStart": "<string>",
            "DeliveryTimeStampEnd": "<string>",
            "DownPartnerBarcode": "<string>",
            "DownPartnerID": "<string>",
            "DownPartnerLocation": "<string>",
			                         
            "ReceiverDateOfBirth": "<string>",
            "Reference": "<string>",
            "ReferenceCollect": "<string>",
            "Remark": "<string>",
            "ReturnBarcode": "<string>",
            "ReturnReference": "<string>", 
        } 
    ] 
}';
pr(json_decode($p,1));exit;
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api-sandbox.postnl.nl/v1/shipment?confirm=true",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>"{ }",
		  CURLOPT_HTTPHEADER => array(
			"apikey: <string>",
			"Content-Type: application/json"
		  ),
		));
		
		$response = curl_exec($curl);
		
		curl_close($curl);
		echo $response;
		exit;
	}
	
	public function Confirming()
	{
 		$api_key = 'Z7J23nyK3BOEPkR3pgIVAAcOKheVCTDV';
		
		
		$data = array( 
		
 		   'Customer' => array(
           'Address' => array(
                    'AddressType' => 02,
                    'City' => 'Hoofddorp',
                    'CompanyName' => 'PostNL',
                    'Countrycode' => 'NL',
                    'HouseNr' => 42,
                    'Street' => 'Siriusdreef',
                    'Zipcode' => '2132WT'
                ),

            'CollectionLocation' => '1234506',
            'ContactPerson' => 'Janssen',
            'CustomerCode' => 'HLRQ',
            'CustomerNumber' => '10472686',
            'Email' => 'email@company.com',
            'Name' => 'Janssen'
        ),

			'Message' => array(
					'MessageID' => '{{$guid}}',
					'MessageTimeStamp' => date('Y-m-d H:i:s')
				),
		
			'Shipments' => array
				(
					'0' => array
						(
							'Addresses' => array
								(
									'0' => array
										(
											'AddressType' => '01',
											'City' => 'Shanghai',
											'CompanyName' => 'PostNL',
											'Countrycode' => 'CN',
											'FirstName' => 'Peter',
											'HouseNr' => '137',
											'Name' => 'de Ruiter',
											'Street' => 'Nanjinglu',
											'Zipcode' => '310000'
										)
		
								),
		
							'Barcode' => '{{s10barcode}}',
							'Contacts' => array
								(
									'0' => array
										(
											'ContactType' => '01',
											'Email' => 'receiver@email.com',
											'SMSNr' => '+31612345678'
										)
		
								),
		
							'Customs' => array
								(
									'Content' => array
										(
											'0' => array
												(
													'CountryOfOrigin' =>' NL',
													'Description' =>' Powdered milk',
													'HSTariffNr' => '19019091',
													'Quantity' => '2',
													'Value' => '20.00',
													'Weight' => '4300'
												)
		
										),
		
									'Currency' => 'EUR',
									'HandleAsNonDeliverable' => false,
									'Invoice' => true,
									'InvoiceNr' => '22334455',
									'ShipmentType' => 'Commercial Goods'
								),
		
							'Dimension' => array
								(
									'Weight' => '4300'
								),
		
							'ProductCodeDelivery' => '4945'
						)
		
				)
		
		);

 		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api-sandbox.postnl.nl/shipment/v2/confirm",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => json_encode($data),
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"Postman-Token: cf1b0480-c8eb-4a20-ab15-cf86e06e4422",
			"apikey: {$api_key}",
			"cache-control: no-cache"
		  ),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		
		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
		
		exit;
	}  
	  
	public function Labelling()
	{
 		$CustomerCode = 'HLRQ';
		  $CustomerNumber = '10472686';
		  $api_key = 'Z7J23nyK3BOEPkR3pgIVAAcOKheVCTDV';
		echo  $barcode = $this->Barcode();	
			echo "<br>";
	$data =	array
	(
    'Customer' => array
        (
            'Address' => array
                (
                    'AddressType' => '02',
                    'City' => 'Hoofddorp',
                    'CompanyName' => 'PostNL',
                    'Countrycode' => 'NL',
                    'HouseNr' => '42',
                    'Street' => 'Siriusdreef',
                    'Zipcode' => '2132WT'
                ),

            'CollectionLocation' => '1234506',
            'ContactPerson' => 'Janssen',
            'CustomerCode' => $CustomerCode ,
            'CustomerNumber' => $CustomerNumber,
            'Email' => 'shashi@euracogroup.co.uk',
            'Name' => 'shashi'
        ),

    'Message' => array
        (
            'MessageID' => '{{$guid}}',
            'MessageTimeStamp' => date('Y-m-d H:i:s'),
            'Printertype' => 'GraphicFile|PDF'
        ),

    'Shipments' => array
        (
            '0' => array
                (
                    'Addresses' => array
                        (
                            '0' => array
                                (
                                    'AddressType' => '02',
									'City' => 'Hoofddorp',
									'CompanyName' => 'PostNL',
									'Countrycode' => 'NL',
									'HouseNr' => '42',
									'Street' => 'Siriusdreef',
									'Zipcode' => '2132WT'
                                ),
								'1' => array
                                (
                                    'AddressType' => '01',
                                    'City' => 'Hamburg',
                                    'Countrycode' => 'DE',
                                    'FirstName' => 'Andre',
                                    'HouseNr' => '4',
                                    'HouseNrExt' => 'A',
                                    'Name' => 'Vermeulen',
                                    'Street' => 'Steintorplatz',
                                    'Zipcode' => '20099'
                                )

                        ),

                    'Barcode' => $barcode,
                    'Contacts' => array
                        (
                            '0' => array
                                (
                                    'ContactType' => 01,
                                    'Email' =>' receiver@email.com',
                                    'SMSNr' => '+31612345678',
                                )

                        ),

                    'Dimension' => array
                        (
                            'Weight' => 4300
                        ),

                    'ProductCodeDelivery' => 4944
                )

        )

);

			
			
			 
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api-sandbox.postnl.nl/shipment/v2_2/label",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>json_encode($data),
			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Postman-Token: 0218cb1c-6b0e-48af-bbee-f67fe893b579",
				"apikey: {$api_key}",
				"cache-control: no-cache"
			  ),
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
			  //echo $response; 
			  $res = json_decode($response,1);
			  
			 $content = $res['ResponseShipments']['0']['Labels']['0']['Content'];
			 $file_name = 'label_'.$res['ResponseShipments']['0']['Barcode'].'.pdf';
			 file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");	
			} 
			
			exit;   	
	}
	
	 public function updateManifestDate($merge_id)
	   {
		  $this->loadModel('MergeUpdate'); 
		  date_default_timezone_set('Europe/Jersey');
		  $firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		  $lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		  $manifest_username = $firstName.' '.$lastName;
		  $data['id'] = $merge_id;   
		  $data['manifest_date'] = date('Y-m-d H:i:s');   
		  $data['manifest_username'] 	= $manifest_username;   
		  $this->MergeUpdate->saveAll( $data);	    	
	   }
    
}
?>
