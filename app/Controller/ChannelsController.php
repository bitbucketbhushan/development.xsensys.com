<?php
 error_reporting(1);
class ChannelsController extends AppController
{
    
    var $name = "Channels";
    
    var $components = array('Session','Upload','Common','Paginator');
    
    var $helpers = array('Html','Form','Session','Common' , 'Soap' , 'Paginator');
    
    public function beforeFilter()
	{
	    parent::beforeFilter();
	    $this->layout = false;
	    $this->Auth->Allow(array('updateSkuPerformance','getSkusFromAws','getFeed','UpdateStockCron'));
	}
    
    public function index() 
    {	
		$this->layout = "skumapping"; 		
		$this->loadModel('ProChannel');	 
 		$this->loadModel('Company');
		$this->loadModel('User');
		
		$users = $this->User->find('all',array('conditions'=>['role_type'=>[2,3,4,5],'is_deleted'=>0],'fields'=>['role_type','first_name','last_name','username']));
		$company = $this->Company->find('all');
		
		$perPage	=	20;//Configure::read( 'perpage' );	
		if(isset($this->params['pass'][0])){
			$this->paginate = array(
				 		  'conditions' => ['company_id'=>$this->params['pass'][0]],
						  'limit' => $perPage,	
						  'order' => 'ch_id DESC',			
			 );	

		}else if(isset($_GET['searchkey']) && $_GET['searchkey'] != ''){
			$this->paginate = array(
				 		  'conditions' => ['OR'=>['channel_title'=>$_GET['searchkey'],'country'=>'%'.$_GET['searchkey'].'%']],
						  'limit' => $perPage,	
						  'order' => 'ch_id DESC',			
			 );	

		}else{		
			$this->paginate = array(
						  'limit' => $perPage,	
						  'order' => 'ch_id DESC',			
			 );	
		 }	
		$channels = $this->paginate('ProChannel');		
 	 	$this->set('channels', $channels );	
		$this->set('company', $company);
		$this->set('users', $users);	
  	}
	
     public function SavesChannel(){
		$this->loadModel( 'ProChannel' );
 		$dataArray = array();
		foreach($this->request->data['values'] as $val){
			$data = explode("=",$val);
			$dataArray[$data[0]] = $data[1];			
		} 
		 	
		if($this->request->data['source'] == ''){
			$msg['msg'] = 'Please select a source!';
		}elseif($this->request->data['channel_type'] == ''){
			$msg['msg'] = 'Please select a channel_type!';
		}else if($this->request->data['country'] == ''){
			$msg['msg'] = 'Please select a country!';
		}else if($this->request->data['currency'] == ''){
			$msg['msg'] = 'Please select a currency!';
		}elseif($dataArray['channel_title'] == ''){
			$msg['msg'] = 'channel_title Value is Empty!';
		}else{
			$dataArray['channel_title'] = $this->request->data['sm_channel_title'];
			$get = $this->ProChannel->find('first', array('conditions' => array('channel_title' => $dataArray['channel_title'])));
			if(count($get) > 0 ){
 				$msg['msg'] =  $dataArray['channel_title'] . ' Already Exists!';
			}else{			
				$dataArray['source'] 	   = $this->request->data['source']; 
				$dataArray['channel_type'] = $this->request->data['channel_type']; 
				$dataArray['country'] 	   = $this->request->data['country'];
				$dataArray['currency'] 	   = $this->request->data['currency'];
				$dataArray['company_id'] 	   = $this->request->data['company_id'];
				$dataArray['listing_person'] 	   = $this->request->data['listing_person']; 
				$dataArray['sales_person'] 	   = $this->request->data['sales_person'];
 				 
				//$dataArray['weight_unit']  = $this->request->data['weight_unit'];
				//$dataArray['dim_unit'] 	   = $this->request->data['dim_unit'];
				$dataArray['added_date']   = date("Y-m-d H:i:s");
				$dataArray['username']     = $this->Session->read('Auth.User.username');	
				$this->ProChannel->saveAll($dataArray);	
				//pr($dataArray);//$dataArray['id'] = $this->Skumapping->getLastInsertId();  
				$msg['msg'] = 'Channel added successfully.';
			}
			
		}		
		echo json_encode($msg);
		exit;
	}
	
	 public function getChannelCsv($company_id = 0){
		
		$this->loadModel( 'Company' );
		$this->loadModel( 'ProChannel' );
 		$this->loadModel('User');
		
		$users = $this->User->find('all',array('conditions'=>['role_type'=>[2,3,4,5],'is_deleted'=>0],'fields'=>['role_type','first_name','last_name','username']));
		foreach($users as $us){
			$allusers[$us['User']['username']] = $us['User']['first_name'].' '.$us['User']['last_name'];
		}
		
		$get = $this->ProChannel->find('all'); 	
		if($company_id > 0){
 			$get = $this->ProChannel->find('all', array('conditions' => array('company_id' => $company_id)));
		}
		$ch_file = 'Channels.csv'; 	
		file_put_contents(WWW_ROOT. 'logs'.DS.$ch_file, "Company,Channel Title,Listing Person,Sales Person,Country,Channel Type,AppEagle Code,Note,Currency,Prefix,SKU Increment ID,Status,Added date,Updated date\n", LOCK_EX); 	
		foreach($get as $m){
			$company_name = ''; $listing_person = ''; $sales_person = '';
			$company = $this->Company->find('first', array('conditions' => array('id' => $m['ProChannel']['company_id'])));
			if(count($company) > 0){
				$company_name = $company['Company']['company_name'];
			}
			if(isset($allusers[$m['ProChannel']['listing_person']])){
				$listing_person = $allusers[$m['ProChannel']['listing_person']];
			}
			if(isset($allusers[$m['ProChannel']['sales_person']])){
				$sales_person = $allusers[$m['ProChannel']['sales_person']];
			}
			file_put_contents(WWW_ROOT. 'logs'.DS.$ch_file, 
			$company_name.",". 
			$m['ProChannel']['channel_title'].",". 
 			$listing_person.",".
			$sales_person.",".
			$m['ProChannel']['country'].",".
			$m['ProChannel']['channel_type'].",".
			$m['ProChannel']['channel_code'].",".
			'"'.$m['ProChannel']['note'].'"'.",".
			$m['ProChannel']['currency'].",".
 			$m['ProChannel']['prefix'].",".
			$m['ProChannel']['increment_id'].",".
			$m['ProChannel']['status'].",".
			$m['ProChannel']['added_date'].",".
			$m['ProChannel']['updated_date'].","."\n", FILE_APPEND | LOCK_EX); 		
		}
 	 	
 		 
  		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.basename($ch_file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT."logs/".$ch_file));
		header("Content-Type: application/force-download");
		readfile(WWW_ROOT."logs/".$ch_file);
		exit;
	}
	 
	public function Delete($id = 0) 
    {	
		$this->loadModel( 'ProChannel' );	
		
		$get = $this->ProChannel->find('first', array('conditions' => array('ch_id' => $id)));
 		file_put_contents(WWW_ROOT .'mapping_reconciliation/Channels.Delete.'.date('dmY').'.log', $get['ProChannel']['channel_title']."\t".$get['ProChannel']['channel_type']."\t".$get['ProChannel']['country']."\t".$this->Session->read('Auth.User.username')."\n",FILE_APPEND | LOCK_EX);
 
		$this->ProChannel->query("DELETE FROM `pro_channels` WHERE `ch_id` = '".$id."'");
 		$this->Session->setflash( 'Channel deleted successfully!', 'flash_danger' ); 
		 
   		$this->redirect($this->referer());	
		exit;
	}
	 
	public function Update()  
    {	
		$this->loadModel( 'ProChannel' );	
		$this->ProChannel->query("UPDATE `pro_channels` SET `".$_POST['name']."` =  '".$_POST['value']."' , `username` =  '".$this->Session->read('Auth.User.username')."' WHERE `ch_id` = '".$_POST['pk']."'");
		$msg['msg'] = $_POST['name'].' Updated Successfully!';	
		
  		file_put_contents(WWW_ROOT .'mapping_reconciliation/Channels.Update.'.date('dmY').'.log', $_POST['name']."\t".$_POST['value']."\t".$this->Session->read('Auth.User.username')."\n",FILE_APPEND | LOCK_EX);
		
		echo json_encode($msg);		
		exit;
	}
 
 
 	public function UpdateStatusSample()  {	
		$file = "status_update.txt";
  		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize(WWW_ROOT."sample_files/".$file));
		header("Content-Type: text/plain");
		readfile(WWW_ROOT."sample_files/".$file);
		exit;
	}
		
  	public function UpdateStockCron(){
		
		$this->loadModel( 'Product' );
		$this->loadModel( 'Skumapping' );
		$this->loadModel( 'FbaUploadInventory' ); 
		$dir = WWW_ROOT. 'logs'.DS; 
		$products =  $this->Product->find('all', array('conditions' => array('Product.is_deleted' => 0),'fields' => ['product_sku','current_stock_level']));
		if(count($products) > 0){
			foreach($products as $p){
				$status = 'out of stock';
				if($p['Product']['current_stock_level'] > 0){
					$status = 'instock';
				}
				$this->Skumapping->query("UPDATE `skumappings` SET `stock_status` = '".$status."' WHERE `sku` = '".$p['Product']['product_sku']."'"); 
 				file_put_contents($dir.'UpdateStockCron_Product_'.date('dmy').'.log', $p['Product']['product_sku']."\t".$status."\n", FILE_APPEND | LOCK_EX);
			 
 			}
		}	
		$fba = $this->FbaUploadInventory->find('all', array('conditions' => array('status' => 0),'fields' => ['seller_sku','total_units']) );
  		if(count($fba) > 0){
			foreach($fba as $f){
				$status = 'out of stock';
				if($f['FbaUploadInventory']['total_units'] > 0){
					$status = 'instock';
				}
				$this->Skumapping->query("UPDATE `skumappings` SET `stock_status` = '".$status."' WHERE `channel_sku` = '".$f['FbaUploadInventory']['seller_sku']."'"); 
 				file_put_contents($dir.'UpdateStockCron_FBA_'.date('dmy').'.log', $f['FbaUploadInventory']['seller_sku']."\t".$status."\n", FILE_APPEND | LOCK_EX);

			}
		}		
		 
		exit;
	}
	
	 
}

?>