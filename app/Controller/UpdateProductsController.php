<?php
//error_reporting(0);
ini_set('memory_limit', '-1');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
class UpdateProductsController extends AppController
{	
	var $name = "UpdateProducts";
	var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
	public function index()
	{
		echo "ihihi";
		exit;
	}
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('getBrtManifestFilesCustom','getBrtSheetManifest','getPhoneNumbreList','getDataForBrtPalletPrintPdf'));
    }
	
	public function updateProductTitle()
	{
		$this->autoRender = false;
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel( 'SkuPercentRecord' );
		//$this->Product->unbindModel( array('hasOne' => array('ProductDesc','ProductPrice','ProductLocation'),'hasMany' => array('ProductLocation')	) );
		$this->ProductDesc->unbindModel( array('hasOne' => array('Product') ) );
		
		App::import('Vendor', 'PHPExcel/IOFactory');
		
		$objPHPExcel = new PHPExcel();
		$objReader= PHPExcel_IOFactory::createReader('CSV');
		$objReader->setReadDataOnly(true);				
		$objPHPExcel=$objReader->load('files/reliapart_title_update0806.csv');
		$objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
		$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
		$colString	=	 $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
		$colNumber = PHPExcel_Cell::columnIndexFromString($colString);
		$j = 0; $k = 0;$l = 0;
		for($i=2;$i<=$lastRow;$i++) 
			{
				//echo $objWorksheet->getCellByColumnAndRow(0,$i)->getValue()."<br>";
				$product	=	$this->Product->find('first', array( 'conditions'=> array('product_sku' => trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue()) ) ) );
				//pr($product);
				if(count($product) > 1)
				{
					//echo "111111";
					$j++;
					$productdesc	=	$this->ProductDesc->find('first', array( 'conditions' => array('product_id' => $product['Product']['id'] ) ) );
					if(count($productdesc == 1))
					{
					//echo "22222";
						$k++;
						$feedsku =	$this->SkuPercentRecord->find('all', array( 'conditions'=> array('SkuPercentRecord.sku' => $product['Product']['product_sku'] ) ) );
						if(count($feedsku) > 0)
						{
							//echo "3333";
							$l++;	
							$product_id 					= 	$product['Product']['id'];
							$sku							=	$product['Product']['product_sku'];
							$title							=	addslashes(trim($objWorksheet->getCellByColumnAndRow(1,$i)->getValue()));
							
							$productdata['id'] 				= 	$product_id;
							$productdata['product_name'] 	= 	$title;
							//pr($productdata);
							$this->Product->saveAll($productdata, array('validate' => false));
							$this->ProductDesc->updateAll(array('ProductDesc.short_description'=>"'".$title."'",'ProductDesc.long_description'=>"'".$title."'"),array('ProductDesc.product_id'=> $product_id));
							$this->SkuPercentRecord->updateAll( array( 'SkuPercentRecord.product_title' => "'".$title."'"),array('SkuPercentRecord.sku' => $sku	) );
						}
					}
					
				  }
			}	
			
			echo "********".$j."******************".$k."*********************".$l."*************************";
	}
	
	
	

	function encode($x = null)
	{
	  if ( !$x )
		return '';
	  $key = "tZ4k3s3TSJe8FTRzGVL0THgO"; //internal unique key
	  $text = substr(";8AW5pW^c1^q^[nYf/gs2*=^`OEsyEr6",rand(0,28),4) . $x;  //add 4 random characters to string
	  $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);//encrypt
	  $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	  $crypttext = strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $text, MCRYPT_MODE_ECB, $iv)), '+/=', '-_');
	  echo $crypttext;
	  echo "<br>";
	  echo $this->decode( $crypttext );
	}

	//decodes a string
	function decode($x = null)
	{
	 if ( !$x )
		return '';
	  $key = "tZ4k3s3TSJe8FTRzGVL0THgO";//requires internal key to decode
	  $crypttext = $x; //decrypt text and remove the 4 random characters
	  $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
	  $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	  $y = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, substr(base64_decode(strtr($crypttext, '-_', '+/=')), 4-strlen($crypttext)), MCRYPT_MODE_ECB, $iv);
	  $decrypttext = rtrim(substr($y, 4-strlen($y)));
	  echo $decrypttext;	
	}
	
	
	public function getChannelDetail()
	{
		
			$this->loadModel('ChannelDetail');
			$getFrom 		= 	strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "-1 months");//date('Y-m-d');
			$getEnd			=	date('Y-m-d');
			$getChanneldetails	=	$this->ChannelDetail->find('all',array('conditions'=>array('ChannelDetail.order_date >='=>$getFrom.' 00:00:00', 'ChannelDetail.order_date <=' => $getEnd.' 11:59:59' ) ) );
			foreach($getChanneldetails as $getChanneldetail)
			{
				$channeldata[ $getChanneldetail['ChannelDetail']['sub_source'] ][$getChanneldetail['ChannelDetail']['sku']][$getChanneldetail['ChannelDetail']['channelSKU']][] = array('qty' => $getChanneldetail['ChannelDetail']['quantity']);
			}
			foreach($channeldata as $ck => $csv)
			{
				foreach($csv as $qsk => $qsv)
				{
					foreach($qsv as $qk => $qv)
					{
							$qty = array();
							foreach($qv as $k => $v)
							{
								$qty[] = $v['qty']; 
							}
							$channelqtydatasku[] = array('sub_source' => $ck, 'sku' => $qsk, 'channel_sku' => $qk, 'csqty' => array_sum($qty) );
					}
				}
			}
			
			$path = WWW_ROOT.'/img/';
			$filename = "channel_sku_qty.txt";
			$sep = "\t";
			$header = "Source".$sep."Sku".$sep."Channel_sku".$sep."Quantity\r\n";
			file_put_contents($path.$filename,$header, LOCK_EX);
			foreach( $channelqtydatasku as $channelqtydata )
			{
				$sub_source		=	$channelqtydata['sub_source'];
				$sku			=	$channelqtydata['sku'];
				$channel_sku	=	$channelqtydata['channel_sku'];
				$csqty			=	$channelqtydata['csqty'];
				$content = $sub_source.$sep.$sku.$sep.$channel_sku.$sep.$csqty."\r\n";
				file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);
			}
			exit;
			
					
			
	}
	
	
	
	
	
}

?>
