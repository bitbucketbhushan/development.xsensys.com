<?php
error_reporting(0);
ini_set('memory_limit', '2048M');
class PurchaserController extends AppController
{
    
    var $name = "Purchaser";
    
    var $components = array('Session','Upload','Common','Paginator');
    
    var $helpers = array('Html','Form','Session','Common' , 'Soap' , 'Paginator');
    
    public function beforeFilter()
	{
	    parent::beforeFilter();
	    $this->layout = false;
	    $this->Auth->Allow(array('getOverSellSku','getMagentoProducts'));
		$this->GlobalBarcode = $this->Components->load('Common'); 
 	}
 	
	public function index()
    { 
 		$this->layout = 'index'; 			 
		$this->loadModel('User');  		 
		$this->loadModel('Po');
		$this->loadModel('Quotation');
		
		$firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		$lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		$username = $firstName.' '.$lastName;
		   
 		if(isset($this->request->query['searchkey'])){
				$search_key = $this->request->query['searchkey'];
					 
				$this->paginate = array('conditions'=>['username'=>$username,'OR'=>['quotation_number' => $search_key,'file_name LIKE' => $search_key.'%']],'order'=>'id DESC','limit' => 50 );
				  
 		}else{
			$this->paginate = array('conditions' => array('username' =>$username),'order'=>'id DESC','limit' =>50 );
		}
		
 		$quotations = $this->paginate('Quotation');
		
		$this->set('quotations',$quotations);
		
		
		if(isset($this->request->query['posearchkey'])){
				$search_key = $this->request->query['posearchkey'];
					 
				$this->paginate = array('conditions'=>['user_name'=>$username,'OR'=>['quotation_number' => $search_key,'file_name LIKE' => $search_key.'%']],'order'=>'id DESC','limit' => 50 );
				  
 		}else{
			$this->paginate = array('conditions' => array('user_name' =>$username),'order'=>'id DESC','limit' =>50 );
		}
		
 		$pos = $this->paginate('Po');
		
		$this->set('pos',$pos);							
	 
	}
	
	public function uploadQuotation() 
	{
		$this->layout = 'index';

	}

	public function UploadQuotationFile()
	{
		$this->autoLayout = 'ajax';
		$this->autoRender = false;
		
		$this->loadModel('Quotation');
		$this->loadModel('QuotationItem');
		$this->loadModel('PurchaseOrder');
		$this->loadModel('Product');
		$this->loadModel('ProductDesc');
		$this->loadModel('SupplierDetail');
		$this->loadModel('SupplierMapping');
 		$this->loadModel('QuotationOurExpectedPrice');
				
		$qt = $this->Quotation->find('first', array('conditions' => array('id' => $this->request->data['qt_id']),'fields'=>array('id','supplier_code','uploaded_file')));
 		$supplier_code = $qt['Quotation']['supplier_code'];
		$uploaded_file = $qt['Quotation']['uploaded_file'];
		
		$sup = $this->SupplierDetail->find('first', array('conditions' => array('supplier_code' => $supplier_code),'fields'=>array('id')));
		$sup_inc_id = $sup['SupplierDetail']['id'];
		 
  		if($this->data['quotation']['Import_file']['name'] != '' && $this->data['quotation']['Import_file']['error'] == '0')
		{ 
			 
			$errors = array();
			$success = array();
			$warnings = array();
  	
			$pathinfo = pathinfo($this->data['quotation']['Import_file']["name"]);

            if (array_key_exists('filename', $pathinfo))
                $fileNameWithoutExt = $pathinfo['filename'];
            if (array_key_exists('extension', $pathinfo))
                $fileExtension = strtolower($pathinfo['extension']);
			
			if($fileExtension == 'csv'){
			
					$csvFile = $_FILES["supplierfile"]["tmp_name"];						
					$handle = fopen($csvFile, 'r');
					$cols   = array_flip(fgetcsv($handle));
					$dateTime = date('Y-m-d H:i:s');
					$r=1; $ssku = 0;
					$errors = array();		
							
					while($data = fgetcsv($handle))
					{	
						$r++;				
						if(trimStr($data[$cols['master_sku']]) == '') {						
							$errors[] =  $data[$cols['master_sku']]." Invalid Master SKU in the Row $r!" ;
						}
						
							if(array_key_exists('custom_price',$cols)) {	
									$sql = 'SELECT price FROM quotation_our_expected_prices WHERE 
											master_sku = "'.$data[$cols['master_sku']].'"';	
											
									$result = $this->Quotation->query($sql) or die($this->Quotation->error);
							
									if($result->num_rows > 0){			
										$sql = "UPDATE `quotation_our_expected_prices` SET 
													`price` = '".$data[$cols['custom_price']]."' 
												WHERE `master_sku` = '".$data[$cols['master_sku']]."'";
										$this->Quotation->query($sql) or die('Update Error:'.$this->Quotation->error);
										
									}else{																
										$sql = "INSERT INTO `quotation_our_expected_prices` (`master_sku`, `price`) 
													VALUES ('".$data[$cols['master_sku']]."','".$data[$cols['custom_price']]."');";								
										$this->Quotation->query($sql) or die($this->Quotation->error);	
									}
									$errors[] = $data[$cols['master_sku']].' custom Price updated.';	
							}
							if(array_key_exists('custom_qty',$cols)) {	
									$sql = 'SELECT qty FROM quotation_our_expected_prices WHERE 
											master_sku = "'.$data[$cols['master_sku']].'"';	
											
									$result = $this->Quotation->query($sql) or die($this->Quotation->error);
							
									if($result->num_rows > 0){			
										$sql = "UPDATE `quotation_our_expected_prices` SET 
													`qty` = '".$data[$cols['custom_qty']]."' 
												WHERE `master_sku` = '".$data[$cols['master_sku']]."'";
										$this->Quotation->query($sql) or die('Update Error:'.$this->Quotation->error);
										
									}else{																
										$sql = "INSERT INTO `quotation_our_expected_prices` (`master_sku`, `qty`) 
													VALUES ('".$data[$cols['master_sku']]."','".$data[$cols['custom_qty']]."');";								
										$this->Quotation->query($sql) or die($this->Quotation->error);	
									}
									$errors[] = $data[$cols['master_sku']].' custom qty updated.';	
						}		
											
						if(trimStr(strtolower($data[$cols['supplier_code']])) != '') {
							//$errors[] = $data[$cols['supplier_code']]." Invalid OUR CODE in the Row $r!";
					
						
							$suppliers = getAllSupplierCode();						
							if(!in_array(trimStr(strtolower($data[$cols['supplier_code']])),$suppliers)) {						
								$errors[] = $data[$cols['supplier_code']]." # $r Row have Invalid OUR CODE,Please check in Supplier list!" ;
							}						
							$rowdata[] = array(	'master_sku' =>trimStr($data[$cols['master_sku']]),
												'supplier_sku' =>trimStr($data[$cols['supplier_sku']]),
												'supplier_code' =>trimStr(strtolower($data[$cols['supplier_code']])),
												'qty' =>trimStr($data[$cols['qty']]),
												'price' =>trimStr($data[$cols['price']])
												);
						
							$supplier_code[$data[$cols['supplier_code']]] = $data[$cols['supplier_code']];
						}
						
					 }
				//	exit;
					if (empty($errors)) 
					{
						$a = 0;  $u = 0; 
						if(count($supplier_code)>0){
							foreach($supplier_code as $k => $code) {			
								$sql = "UPDATE `supplier_mappings` SET 
												`price` = 0, 
												`min_order_qty` = 0,
												`updated_date` = '".$dateTime."' WHERE `supplier_code` = '".$code."' ";
								$this->Quotation->query($sql) or die($this->Quotation->error);
							}
						}									
						foreach($rowdata as $k => $dataLine) {
							$sql = 'SELECT sm_id FROM supplier_mappings WHERE 
										master_sku = "'.$dataLine['master_sku'].'" AND 											
										supplier_code = "'.$dataLine['supplier_code'].'" ';	
						
							$result = $this->Quotation->query($sql) or die($this->Quotation->error);
						
							if($result->num_rows > 0){	
								$u++;
								$obj = $result->fetch_object();	
								$sm_id = $obj->sm_id;			
								$ssku = '';
								if($dataLine['supplier_sku']!=''){
									$ssku =  "`supplier_sku` = '".$dataLine['supplier_sku']."',"; 
								}								
								$sql = "UPDATE `supplier_mappings` SET 
											`price` = '".$dataLine['price']."', 
											 ".$ssku." 
											`min_order_qty` = '".$dataLine['qty']."',
											`updated_date` = '".$dateTime."'  
										WHERE `sm_id` = '".$obj->sm_id."'";
								$this->Quotation->query($sql) or die('Update Error:'.$this->Quotation->error);										
							}else{	
								
								$sql = "INSERT INTO `supplier_mappings` (`master_sku`, `supplier_sku`,`supplier_code`,`price`,`min_order_qty`) VALUES 
								('".$dataLine['master_sku']."', '".$dataLine['supplier_sku']."','".$dataLine['supplier_code']."','".$dataLine['price']."','".$dataLine['qty']."');";								
								
								$this->Quotation->query($sql) or die('INSERT Error:'.$this->Quotation->error);		
								$sm_id = $this->Quotation->insert_id; 	
								$a++;							
								//$warnings[] = 'New Master sku '.$dataLine['master_sku'].' and Supplier sku '.$dataLine['supplier_sku'].' Added!';									
							}	
							
								
						//	$supplier_code[$dataLine['supplier_code']] = $dataLine['supplier_code'];	
						// $supplier_del[] = $dataLine['master_sku'];	
							
						}									
						$success[] = $a . " INSERTED! ".$u.  " UPDATE!";
										
						$date  = date('Ymd_His');					
						move_uploaded_file($_FILES["supplierfile"]["tmp_name"],$output_dir. $fileNameWithoutExt.$date.'.'.$fileExtension); 
						$success[] = $_FILES["supplierfile"]["name"].' File Uploaded.';
					}
					
			 			 
			 }
			elseif($fileExtension == 'xlsx'){
			
					App::import('Vendor', 'PHPExcel/IOFactory');
					App::import('Vendor', 'PHPExcel');
					$objPHPExcel = new PHPExcel();			
					$xlFile = $this->data['quotation']['Import_file']["tmp_name"];	
					$date   = date('ymd_Hi');
					$xls = PHPExcel_IOFactory::load($xlFile);
					$xls->setActiveSheetIndex(0);
					$sheet = $xls->getActiveSheet();
					$highestRow = $sheet->getHighestRow(); 
					$highestColumn = $sheet->getHighestColumn();
					
					$supplier = $sheet->getCellByColumnAndRow(4, 8)->getValue();
					$exp = explode(" ",$supplier);
					$e   = explode("Date",$exp[3]);
 					$po_date = date('Y-m-d H:i:s', strtotime(str_replace(":","",$e[1])));
					
					
					$headings = $sheet->rangeToArray('A10:' . $highestColumn . 10, NULL, TRUE,FALSE);
					$insert_str = '';
					
  				    $dateTime = date('Y-m-d H:i:s');
					$update = 0; $insert = 0;
					
					/* $this->SupplierMapping->updateAll(['price'=>0,'updated_date'=>'"'.$dateTime.'"'],['sup_code'=>$supplier_code]);*/					
					
					for ($row = 11; $row <= $highestRow; $row++){ 
							
						$rowData   = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
						$finalData = array_combine($headings[0], $rowData[0]);	
						 
						if($finalData['OUR CODE']){	
						
							$price = (float) filter_var($finalData['UNIT PRICE'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
							$qty = filter_var($finalData['TOTAL QTY'], FILTER_SANITIZE_NUMBER_INT);
							$available_qty = filter_var($finalData['QTY AVAILABLE'], FILTER_SANITIZE_NUMBER_INT);
  							 
							$quep = $this->QuotationOurExpectedPrice->find('first', array('conditions' => array('master_sku' => $finalData['OUR CODE']),'fields'=>['id']));
							$ep['qty'] = $qty;
							$ep['master_sku'] = $finalData['OUR CODE'];
							$ep['price'] = $price;
							if(count($quep) > 0){
								$ep['id'] = $quep['QuotationOurExpectedPrice']['id'];
							}
							$this->QuotationOurExpectedPrice->saveAll($ep);
						
							/*------Update QuotationItem------*/
							$quoteItem = [];
							$quoteItem['supplier_quote_price'] = "'".$price."'"; 
							$quoteItem['supplier_available_qty'] = "'".$available_qty."'";
 							$this->QuotationItem->updateAll($quoteItem,['quotation_inc_id'=>$this->request->data['qt_id'],'supplier_itemcode'=>$finalData['SUPPLIER CODE']]);
							/*------End-----*/
								
 							$result = $this->SupplierMapping->find('first', array('conditions' => array('master_sku' => $finalData['OUR CODE'],'supplier_sku' => $finalData['SUPPLIER CODE'],'sup_code' => $supplier_code),'fields'=>['id']));
							 
							if(count($result) > 0){	
								$update++;
 								$ssku = '';
								$updArr = [];
  								if($finalData['SUPPLIER CODE']!=''){
									$updArr['supplier_sku'] = "'".$finalData['SUPPLIER CODE']."'"; 
								}								
								$updArr['price'] = "'".$price."'"; 
								$updArr['supplier_qty'] = "'".$available_qty."'";
								$updArr['updated_date'] = "'".$dateTime."'"; 
  								$this->SupplierMapping->updateAll($updArr,['id'=>$result['SupplierMapping']['id']]);
 							}else{		 							
								$insert_str .= "('".$sup_inc_id."','".$finalData['OUR CODE']."', '".$finalData['SUPPLIER CODE']."','".$supplier_code."','".$price."','".$available_qty."'),";	
								$insert++;
							}							
						}							
					}
					if($insert_str){
						$insert_str = rtrim($insert_str,',');	
						$sql = "INSERT INTO `supplier_mappings` (`sup_inc_id`,`master_sku`, `supplier_sku`,`sup_code`,`price`,`supplier_qty`) VALUES $insert_str";	
						$this->Quotation->query($sql);
					}							
					$success[] = '#'.$update.' Updated & #'.$insert.' Inserted.'; 		
					move_uploaded_file($this->data['quotation']['Import_file']["tmp_name"],WWW_ROOT.'quotation/'. $fileNameWithoutExt.$date.'.'.$fileExtension);
					$this->Quotation->updateAll(['uploaded_file'=>'"'.$fileNameWithoutExt.$date.'.'.$fileExtension.'"'],['id'=>$this->request->data['qt_id']]);
					@unlink(WWW_ROOT.'quotation/'.$uploaded_file);
   			}
			else{		 	
				$errors[] = 'Invalid file type!';
		 	}
			
			
			if($ssku > 0){
				$warnings[] = "#".$ssku." Invalid Supplier SKU!"; 
			}
			
			if(count($success)>0){
				$success_msg = '';
				foreach($success as $sLine) {					
					$success_msg .= $sLine."\n";
				} 
				$msg['error'] = '';			
				$msg['msg'] .= '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$success_msg.'</div>';
			}	
			if(count($warnings)>0){
				$warnings_msg = '';
				foreach($warnings as $wLine) {					
					$warnings_msg .= $wLine."\n";
				} 
				$msg['error'] = 'yes';
				$msg['msg'] .= '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$warnings_msg.'</div>';
			}	
			if(count($errors)>0){
				$errors_msg = '';
				foreach($errors as $eLine) {					
					$errors_msg .= $eLine."<br>";
				} 
				$msg['error'] = 'yes';			
				$msg['msg'] .= '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$errors_msg.'</div>';
			}	
					 
 			$this->Session->setFlash($msg['msg'], 'flash_danger');
			
		}
		$this->redirect($this->referer());
	}
	
	public function deleteQuotation($id = 0) 
	{
		$this->layout = 'index';
		$this->loadModel( 'Quotation' );
		$this->loadModel( 'QuotationItem' );
		
		$this->Quotation->query( "DELETE FROM `quotations` WHERE `quotations`.`id` = {$id} AND `po_status` = 0"); 
		$this->QuotationItem->query( "DELETE FROM `quotation_items` WHERE `quotation_items`.`quotation_inc_id` = {$id}"); 
		$this->Session->setflash(  "Quotation deleted successfully.",  'flash_success' );
	 	$this->redirect( Router::url( $this->referer(), true ) );
	}
	
	public function QuotationItems($qid = null) 
	{
		$this->layout = 'index';
		$this->loadModel( 'QuotationItem' );
		//$quotation_items = $this->QuotationItem->find( 'all',array('conditions'=>['quotation_inc_id'=>$qid],'order' => 'id DESC') ); 
		if(isset($this->request->query['search_key'])){
				$search_key = $this->request->query['search_key'];
					 
				$this->paginate = array('conditions'=>['quotation_inc_id'=>$qid,'OR'=>['master_sku' => $search_key,'supplier_itemcode' => $search_key,'barcode' => $search_key]],'order'=>'id DESC','limit' => 50 );
				  
 		}else{
		$this->paginate = array('conditions'=>['quotation_inc_id'=>$qid],
 				  'order'=>'id DESC','limit' => 50 );
		}
				  
 		$quotation_items = $this->paginate('QuotationItem');
 		
		$this->set('quotation_items',$quotation_items);	
	}
	
	public function deleteQuotationItem($id = 0) 
	{
		$this->layout = 'index';
		$this->loadModel( 'QuotationItem' );
		$this->QuotationItem->query( "DELETE FROM `quotation_items` WHERE `quotation_items`.`id` = {$id}"); 
		$this->Session->setflash(  "Item deleted successfully.",  'flash_success' );
	 	$this->redirect( Router::url( $this->referer(), true ) );
	}
	public function UploadFile()
    {
		$this->layout = 'index';
		$this->loadModel('Product');
		$this->loadModel('SkuPurchaser');
 		
		$name		= $this->request->data['upload']['import_file']['name'];
		$nameExplode= explode('.', $name);
		$nameNew 	= end( $nameExplode );
		$filename	= WWW_ROOT. 'files'.DS.$nameExplode[0].'__'.time().".".$nameNew; 
		if( $nameNew == 'csv')
		{
			move_uploaded_file($this->request->data['upload']['import_file']['tmp_name'],$filename); 
			$fileData = file_get_contents($filename);
			foreach(explode("\n",$fileData) as $k => $val){
				if($k > 0){
					$data = explode(",",$val);
					$sku  = trim($data[0]);
					$user = trim($data[1]); 
					$product = $this->SkuPurchaser->find('first', array('conditions' => array('sku' => $sku,'status' => 0),'fields'=>array('sku')));
					if(count($product) == 0){
						$save = [];
						$save['sku'] = $sku;
						$save['username'] = $user;
						$save['added_date'] = date('Y-m-d H:i:s');
						$this->SkuPurchaser->saveAll($save);
					}
				}
			}
 			$this->Session->setflash(  "File uploaded successfully.",  'flash_success' );
			$this->redirect($this->referer());	
		}
		else
		{
			$this->Session->setFlash('Please upload valid file( CSV ).', 'flash_danger');
			$this->redirect($this->referer());	
		}	
		exit;
		
 	}  
	
	public function DownloadSkus($username = '')
	{
 		$this->layout = 'index';
		$this->autorender = false;
		$this->loadModel('User');  
		$this->loadModel('SkuPurchaser');
	 
 		$filename = $username.'_'.date('dmY').'.csv'; 
		$sep = ",";
		file_put_contents( WWW_ROOT .'logs/'.$filename,'SKU'.$sep.'Username'."\n", LOCK_EX);
		$skus = $this->SkuPurchaser->find('all', array('conditions' => array('username' => $username ))); 
		
		if(count($skus) > 0){
   			foreach($skus as $v){
				file_put_contents( WWW_ROOT .'logs/'.$filename,$v['SkuPurchaser']['sku'].$sep.$v['SkuPurchaser']['username']."\n", FILE_APPEND | LOCK_EX);			
			}
			 
			header('Content-Description: File Transfer');
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$filename));
			ob_clean();
			flush();
			readfile( WWW_ROOT .'logs/'.$filename);
			exit;
			
		 }else{
			  $this->Session->setflash('Record not found!', 'flash_danger' );  
		 }
		 
		$this->redirect($this->referer());
		exit;	 
	}
	
	public function Users()
	{
		 
		$this->layout = 'index';
		$this->autorender = false;
		$this->loadModel('User');  
		
		$users =  $this->User->find( 'all');
 		$filename = 'Users_'.date('dmY').'.csv'; 
 		$sep = ",";
		
		 file_put_contents( WWW_ROOT .'logs/'.$filename,'Username'.$sep.'First Name'.$sep.'Last Name'.$sep.'Email'."\n", LOCK_EX);
		 
		 if(count($users) > 0){
			foreach($users as $v){
				file_put_contents( WWW_ROOT .'logs/'.$filename,$v['User']['username'].$sep.$v['User']['first_name'].$sep.$v['User']['last_name'].$sep.$v['User']['email']."\n", FILE_APPEND | LOCK_EX);			
			}
			 
			header('Content-Description: File Transfer');
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$filename));
			ob_clean();
			flush();
			readfile( WWW_ROOT .'logs/'.$filename);
			exit;
			
		 }else{
			  $this->Session->setflash('Record not found!', 'flash_danger' );  
		 }
		 
		$this->redirect($this->referer());
		exit;	 
	}
  	
  	public function Report()
    { 
 		$this->layout = 'index'; 			 
		$this->loadModel('User'); 
		$this->loadModel('SkuPurchaser');
		$this->loadModel('SupplierDetail');
		$this->loadModel('StockEod');
		$this->loadModel('SupplierMapping');
							
		$users = $this->User->find('all', array('conditions' => array('is_purchaser' =>1,'is_deleted' =>0),'fields' => array('id','first_name','last_name','username'),'order'=>'first_name'));
			
 		$suppliers = $this->SupplierDetail->find('all', array('fields' => array('id','supplier_code','supplier_name')));
		
		if(isset($_GET['user']) && $_GET['user'] != 'all'){ 
			$all_skus = $this->SkuPurchaser->find('all', array('conditions' => array('username' => trim($_GET['user']))));
		}else{
			$all_skus = $this->SkuPurchaser->find('all',array('fields' => array('id','sku','username')));
		}
		
		if(isset($_GET['supplier']) && $_GET['supplier'] != 'all'){ 
			$sup_skus = $this->SupplierMapping->find('all', array('conditions' => array('sup_code' => trim($_GET['supplier']))));
		}else{
			$sup_skus = $this->SupplierMapping->find('all',array('fields' => array('id','sup_code','master_sku')));
		}
		
		
		$skus = [];
		foreach($all_skus as $v){
			$skus[$v['SkuPurchaser']['sku']] = $v['SkuPurchaser']['sku'];
		}
		foreach($sup_skus as $v){
			$skus[$v['SupplierMapping']['master_sku']] = $v['SupplierMapping']['master_sku'];
		}
		
		
 		$getFrom 	= date('Y-m-d' ,strtotime($this->request->query['start']));
		$getEnd 	= date('Y-m-d' ,strtotime($this->request->query['end']));
		
		$paramHostory = array(
 			'conditions' => array(
 				'StockEod.stock_date  >=' => $getFrom.' 00:00:00',
				'StockEod.stock_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		 
		 
		$all_data =  $this->StockEod->find( 'all', $paramHostory); 
		
		if(isset($_GET['download']) && $_GET['download'] > 0){ 
			
			$filename = $_GET['user'].'_'.date('dmY').'.csv'; 
			$sep = ",";
			file_put_contents( WWW_ROOT .'logs/'.$filename,'DATE'.$sep.'Out of stock %'."\n", LOCK_EX);
			foreach( $all_data as $v){  
 					$normal_stock = json_decode($v['StockEod']['normal_stock'],1); 
					$out_stock_count = 0; $total = 0;
 					foreach($normal_stock as $sku => $st){
						if(in_array($sku,$skus)){
							 if($st < 1){
								$out_stock_count++;
							 }
							 $total++;
						}
 					}
				$per = number_format(((100 * $out_stock_count)/$total),2); 				
				file_put_contents( WWW_ROOT .'logs/'.$filename,$v['StockEod']['stock_date'].$sep.$per."\n", FILE_APPEND | LOCK_EX);		
			}
				 
			header('Content-Description: File Transfer');
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize(WWW_ROOT .'logs/'.$filename));
			ob_clean();
			flush();
			readfile( WWW_ROOT .'logs/'.$filename);
			exit;
					
		}  
		 
		$this->set( 'users', $users );
		$this->set( 'suppliers', $suppliers );	
		$this->set( 'all_data', $all_data );	
		$this->set( 'skus', $skus );      
	}
	
	 
	
	 
}
?>