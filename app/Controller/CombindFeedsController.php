<?php
error_reporting(0);
//ini_set('memory_limit', '-1');
	App::uses('Folder', 'Utility');
	App::uses('File', 'Utility');
class CombindFeedsController extends AppController
{	
	var $name = "CombindFeeds";
	var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
	public function index()
	{
		echo "ihihi";
		exit;
	}
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('generateFeedFile'));
    }
	
	public function generateCombindFeedFile()
	{
		$this->layout = '';
		$this->autoRender = false;
		
		$this->loadModel( 'SkuPercentRecord' );
		$this->loadModel( 'Store' );
		$this->loadModel( 'Product' );
		$this->loadModel( 'ProductDesc' );
		$this->loadModel('OpenOrder');
		$this->loadModel('Company');
		$this->Store->unbindModel(array('belongsTo' => array('Company')), true);
		$companies 	=	$this->Company->find('all');
		$sep = "\t";
		$get_qty_arr	=	$this->getOpenOrderQty();
		foreach($companies as $store)
		{
			//pr($store);
			$com_name	=	$store['Company']['company_name'];
			if($com_name == 'Rainbow Retail'){
				$com_name = 'Rainbow_Retail';
			} else if($com_name == 'Tech Drive Supplies'){
				$com_name = 'Tech_Drive_Supplies';
			} else if($com_name == 'eBuyer Express'){
				$com_name = 'eBuyer_Express';
			} 
			
			$company 	= 	array();
			$sto_name	=	array();
			$path 		= 	WWW_ROOT.'/img/Combindfeeds/';
			$filename 	= 	'StockFeed_'.$com_name.'.txt';;
			$header 	= 	"Product Sku".$sep."Stock".$sep."Location\r\n";
			
			file_put_contents($path.$filename,$header, LOCK_EX);
			
			foreach( $store['Store'] as $stores )
			{
				if( $stores['store_alias']	==	'SFL-Magento' ){
					$stores['store_alias'] = 'http://www_storeforlife_co_uk';
				} else if($stores['store_alias']	==	'Default'){
					$stores['store_alias'] = 'EBAY0';
				} else if($stores['store_alias']	==	'Rainbow-Amazon'){
					$stores['store_alias'] = 'Rainbow_Retail';
				}
				$company[]	=	$stores['company_id'];
				$sto_name[]	=	$stores['store_alias'];
			}
			
			$params 		= 	array('conditions'=> array( 'SkuPercentRecord.store_alies IN' => $sto_name, 'SkuPercentRecord.deleted_status' => 0),'order' => array('store_alies ASC'));
			$getAllFeeds	=	$this->SkuPercentRecord->find( 'all', $params );
			//pr($getAllFeeds);
			
			if(count($getAllFeeds) > 0)
			{
				foreach( $getAllFeeds as $getAllFeed )
					{
						
						
						$mainSku	 		= $getAllFeed['SkuPercentRecord']['sku'];
						$this->Product->unbindModel( array('hasMany' => array('ProductLocation') ) );
						$this->ProductDesc->unbindModel(array('belongsTo' => array('Product') )	);
						$params = array('conditions'=> array('Product.product_sku' => trim($mainSku)),'fields'=> array('Product.current_stock_level','ProductDesc.product_type',					                                                             'ProductDesc.product_identifier','ProductDesc.product_defined_skus'),	'order' => 'Product.id ASC'	);
						$stockDetail = json_decode(json_encode($this->Product->find( 'first' , $params )),0);
						if(!empty($get_qty_arr[$storeName][$mainSku])){
							$open_order_qty 				= 	$get_qty_arr[$storeName][$mainSku];
							$openOrderItemCount				=	$open_order_qty['t_qty'];
						} else {
							$open_order_qty 				= 	'0';
							$openOrderItemCount				=	$open_order_qty;
						}
						$locationCount			=	$this->getLoctionCount( $getAllFeed['SkuPercentRecord']['barcode'] );
							if( $locationCount == 0 )
							{
								
								if(is_object($stockDetail)){		
									$currentStock = $stockDetail->Product->current_stock_level;
								}else{
									//echo $mainSku;
								}
							} else {
								$currentStock = 0;
							}
							$storeName 			= $getAllFeed['SkuPercentRecord']['store_name'];
							
							
							$percentage 		= $getAllFeed['SkuPercentRecord']['percentage'];
							$buffer 			= $getAllFeed['SkuPercentRecord']['buffer'];
							$productTitle 		= $getAllFeed['SkuPercentRecord']['product_title'];
							$customMark 		= $getAllFeed['SkuPercentRecord']['custom_mark'];
							$skuStatus 			= $getAllFeed['SkuPercentRecord']['sku_status'];
							$barcode 			= $getAllFeed['SkuPercentRecord']['barcode'];
							$productType 		= $stockDetail->ProductDesc->product_type;
							$productIdentifier 	= $stockDetail->ProductDesc->product_identifier;
							$productDefinedSkus = $stockDetail->ProductDesc->product_defined_skus;
							$openOrderQty 		= ($openOrderItemCount > 0) ? $openOrderItemCount : '0';
							$marginValue 		= ($getAllFeed['SkuPercentRecord']['margin_value'] > 0) ? $getAllFeed['SkuPercentRecord']['margin_value'] : '0';
							if( strtolower( $productType ) === "single" ){
											//$title		= 	addslashes($productTitle) ;    
											$sku		=	trim($mainSku);    
											//$barcode	=	addslashes($barcode);
											if( $currentStock <= 0 ){
												//$stock 			= 0;
												$original_stock = 0;	
												$store_stock	= 0;
											} else {
												//$stock 			= $currentStock;
												$original_stock = $currentStock;
											}	
											if( $currentStock == 1 && $skuStatus != 1){
												$store_stock	= 0;
											} else if( $currentStock < 1 ){
												$store_stock	= 0;
											} else {
													if($currentStock <= $marginValue){
															if( $marginValue > 0 ){
																$store_stock = $currentStock + $openOrderQty;
															} else {
																$store_stock = 0;
															}
														} else {	
															if( $skuStatus == 1 ){
																$store_stock = $currentStock + $openOrderQty;
															} else {
																$actualQty 		= floor((($currentStock - $buffer) * $percentage)/100) + $openOrderQty;
																$store_stock 	= $actualQty;
															}
														}
													}
									
							}
							elseif( strtolower( $productType ) === "bundle" ){
								if( strtolower( $productIdentifier ) === "single" ){
									$this->Product->unbindModel( array('hasOne' => array('ProductDesc' , 'ProductPrice'),'hasMany' => array('ProductLocation')) );
									$this->ProductDesc->unbindModel(array('belongsTo' => array('Product')));
									$params = array('conditions' => array('Product.product_sku' => trim($productDefinedSkus)),
												'fields' => array('Product.current_stock_level as FindStockThrough__LimitFactor'),
												'order' => 'Product.id ASC'	);
									$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);
									$mainSkuNew = explode('-',$mainSku );
									$getDivisionNumberFromBundleSku = $mainSkuNew[2];						
									$index = 0;
									$getAvailableStockFor_Bundle_WithSameSku = $stockData[0][$index]->FindStockThrough__LimitFactor;
											//$title		= 	addslashes($productTitle) ;    
											$sku		=	trim($mainSku);    
											//$barcode	=	addslashes($barcode);
											//$stock		=	$getAvailableStockFor_Bundle_WithSameSku;
											if( $getAvailableStockFor_Bundle_WithSameSku >= 2 ){
												$getAvailableStockFor_Bundle_WithSameSkuNew = floor($getAvailableStockFor_Bundle_WithSameSku / $getDivisionNumberFromBundleSku);
													if( $getAvailableStockFor_Bundle_WithSameSkuNew == 1 )
													{
														$store_stock	=	 0;
													}
													else if( $getAvailableStockFor_Bundle_WithSameSkuNew < 1 )
													{
														$store_stock	=	 0;
													}
													else
													{
														
														if($getAvailableStockFor_Bundle_WithSameSkuNew <= $marginValue)
																{
																	if( $marginValue > 0 ){
																		$store_stock 	=	$getAvailableStockFor_Bundle_WithSameSkuNew + $openOrderQty;
																	} else {
																		$store_stock 	=	0;
																	}
																}
																else
																{	
																	if( $skuStatus == 1 ){
																		$store_stock 	= 	$getAvailableStockFor_Bundle_WithSameSkuNew + $openOrderQty;
																	} else {
																		$store_stock = floor((($getAvailableStockFor_Bundle_WithSameSkuNew-$buffer)*$percentage)/100)+$openOrderQty ;
																	}
																}
													}	
												} else {
														$store_stock = 0;
												}
												
												$original_stock 	=	$stockData[0][$index]->FindStockThrough__LimitFactor;
											} 
											elseif( strtolower( $productIdentifier ) === "multiple" ){
												$stockData = '';
												$params = array('conditions' => array('ProductDesc.product_defined_skus' => $productDefinedSkus	),
												'fields' => array('ProductDesc.product_defined_skus as Diff_Skus','Product.product_sku'),
												'order' => 'ProductDesc.id ASC');
											
												$stockData = json_decode(json_encode($this->Product->find( 'all' , $params )),0);	
												$diff_SkuList = explode(':',trim($stockData[0]->ProductDesc->Diff_Skus));					
											
												//Get Diff Sku stock value under min section according to limit factor
												//Set if same sku is there with single-bundle type
												$this->Product->unbindModel( array('hasOne' => array('ProductDesc' , 'ProductPrice' ),'hasMany' => array(	'ProductLocation')));
											 
											 	$this->ProductDesc->unbindModel(array('belongsTo' => array('Product')));
											
												$params = array('conditions' => array('Product.product_sku' => $diff_SkuList),
																'fields' => array('Product.id','Product.current_stock_level'),
																'order' => 'Product.id ASC');
												$getListStockValue = $this->Product->find( 'list' , $params );							
												$stockDataValue = min($getListStockValue);
												
												if( $stockDataValue > 2 ){
													//$stock 			= 0;
													//$original_stock = 0;	
													//$store_stock	= 0;
													
													//$title			=	addslashes($productTitle);    
													$sku			=	trim($mainSku);    
													//$barcode		=	addslashes($barcode);    
													//$stock			= 	$stockDataValue;
													$original_stock =	implode( $getListStockValue , ',' );
													if( ($stockDataValue-2) == 1 ){
															$store_stock	= 	0;
														} else if( ($stockDataValue-2) < 1 ){
															$store_stock	=	 0;
														} else {
															$store_stock 	=	floor(((($stockDataValue-$buffer)* $percentage)/100)+ $openOrderQty);
														}	
												}
												elseif( $stockDataValue <= 2 )
												{
													//$title			=	addslashes($productTitle);    
													$sku			=	trim($mainSku);    
													//$barcode		=	addslashes($barcode);    
													//$stock			=	$stockDataValue;
													$original_stock	= 	implode( $getListStockValue , ',' );						
													$store_stock	=	0;
												}
											}
								}
							$content = $sku.$sep.$store_stock.$sep.$storeName."\r\n";
							file_put_contents($path.$filename,$content, FILE_APPEND | LOCK_EX);		
							}
				
			}
			//pr($getAllFeeds);
			exit;
		}
		//pr($companies);
		
		//$getStores = $this->Store->find( 'all', array( 'conditions' => array( 'Store.status' => '1' ),'fields' => array( 'Company.id' ), 'group' => 'Company.company_name' ) );
		//pr($getStores);
		
		//$get_qty_arr	=	$this->getOpenOrderQty();
		
		
	}
	public function getOpenOrderQty()
	{
		$this->loadModel('OpenOrder');
		$getdetails	=	$this->OpenOrder->find( 'all' , array( 'conditions' => array( 'OpenOrder.status' => 0),'fields' => array('OpenOrder.items','OpenOrder.sub_source') ));
		$bundalQty = 0;	
				
				if( count($getdetails) > 0 )
				{
					foreach( $getdetails as $getdetail)
					{
							$items['sub_source']	 = $getdetail['OpenOrder']['sub_source'];
							$items['items']			 = unserialize($getdetail['OpenOrder']['items']);
							//pr($items['items']);
							$bundalQty = 0;
							foreach( $items['items'] as $item )
							{
								$bundalQty = $bundalQty + $item->Quantity;
								$skuqty_data[$items['sub_source']][$item->SKU][] = array('qty' => $item->Quantity);
							}
							//$skuqty_data[$items['sub_source']][$item->SKU] = array('qty' => $bundalQty);
					}
				}
				foreach($skuqty_data as $stok => $stov){
					foreach( $stov as $sk=>$sv )
					{	
						$total = 0;
						foreach($sv as $sq)
						{
							$total = $total + $sq['qty'];
						}
						
						$fin_date[$stok][$sk] = array('t_qty' => $total);
					}
				}
				return $fin_date;		
	}
	
	
	public function getSoldByStoreOpenOrder( $sku, $storeName )
	{
			
			//$this->loadModel('OpenOrder');
			$getFrom 		= 	strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "-7 days");
			$getEnd			=	date('Y-m-d');
			
			$this->OpenOrder->unbindModel( array( 'hasOne' => array( 'AssignService' ) ) );
			$getdetails	=	$this->OpenOrder->find( 'all' , array( 
																'conditions' => array( 'OpenOrder.items like' =>'%'.$sku.'%',
																					   'OpenOrder.sub_source like' =>"{$storeName}",
																					   'OpenOrder.open_order_date >= '=> $getFrom.' 00:00:00' ,
																					   'OpenOrder.open_order_date <= '=> $getEnd.' 23:59:59' ,
																					   'OpenOrder.status' => 0
																		),
																'fields' => array('OpenOrder.items','OpenOrder.sub_source') 
																	)
												);
			
				$bundalQty = 0;	
				
				if( count($getdetails) > 0 )
				{
					foreach( $getdetails as $getdetail)
					{
							$items['sub_source']	 = $getdetail['OpenOrder']['sub_source'];
							$items['items']			 = unserialize($getdetail['OpenOrder']['items']);
							
							foreach( $items['items'] as $item)
							{
								$bundalQty = $bundalQty + $item->Quantity;
							}
					}
				} else {
					$bundalQty = 0;
				}
			return $bundalQty;
			exit;
	}
	
	
	public function getLoctionCount( $barcode = null )
	{
		$this->loadModel( 'BinLocation' );
		$getAllLocations	=	$this->BinLocation->find('all', array( 
									'conditions' => array( 'barcode' => $barcode, 'BinLocation.stock_by_location >' => 0 ),
									'fields' => array( 'SUM(stock_by_location) as TotalItem' )
									)
								);
		
		if( $getAllLocations[0][0]['TotalItem'] != '' || $getAllLocations[0][0]['TotalItem'] >= 0 ){
			return '0';
		} else {
			return '1';
		}		
	}
	
	
	
	
	
	
}

?>
