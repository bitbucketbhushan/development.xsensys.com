<?php
error_reporting(1);
class JerseypostController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Jerseypost";
    var $components = array('Session','Upload','Common','Auth','Paginator');
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
	public function beforeFilter()
    {
		parent::beforeFilter();
		$this->layout = false;
		$this->Auth->Allow(array('Barcode'));
		$this->Common = $this->Components->load('Common');   
    }
	
	public function index($order_id)
    {		
		$this->loadModel('OpenOrder');
		$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id,'OpenOrder.destination NOT IN' => ['Italy','United Kingdom','Netherlands','Brazil'] ) ) );
		
		$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
		$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
		$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
	//	pr($general_info);	
		pr($customer_info);	
		//pr($totals_info);	
	echo	 $_address1 = $customer_info->Address->Address1;
		 echo " == ";
	echo	  $this->replaceChar(ltrim($_address1,","));
				
		exit;
	}
  
 	public function createShipment($order_id = '')
 	{
		$this->loadModel('MergeUpdate'); 
		$this->loadModel('OpenOrder');
		$this->loadModel('Country');
		$this->loadModel('Product');
		$this->loadModel('ProductHscode');  
		$this->loadModel('PurchaseOrder');  
		$country_of_origin = 'CN';
		$hs_code = '9603298000';//'8470300000';
		$product_name = '';		
 		//	'MergeUpdate.provider_ref_code NOT IN ' => ['GYE','FRU']
		
		
		if($order_id == ''){
			$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.merge_order_date >' => '2020-12-29','MergeUpdate.status' => 0,'MergeUpdate.track_id' => '','MergeUpdate.reg_post_number' => '', 'MergeUpdate.service_provider' => 'Jersey Post','MergeUpdate.delevery_country NOT IN' => ['United Kingdom','Jersey','Guernsey','Isle of Man'],'OR'=>['MergeUpdate.parent_id' => NULL,'MergeUpdate.parent_id ' => 0]),'fields'=>array('sku','product_order_id_identify','order_id','country_code','postal_service','provider_ref_code','quantity') ) );
		}else{
			$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $order_id,'MergeUpdate.status' => 0,'MergeUpdate.track_id' => '','MergeUpdate.reg_post_number' => '', 'MergeUpdate.service_provider' => 'Jersey Post','MergeUpdate.parent_id ' => 0,'MergeUpdate.delevery_country NOT IN' => ['United Kingdom','Jersey','Guernsey','Isle of Man']),'fields'=>array('sku','product_order_id_identify','order_id','country_code','postal_service','provider_ref_code','quantity') ) );
		}
  // pr($orders	);exit;
 /*$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $order_id, 'service_provider'=>'PostNL','delevery_country !='=>'Netherlands'),'fields'=>array('sku','product_order_id_identify','order_id','country_code','provider_ref_code') ) );
 */
		foreach($orders	 as $v){
			$provider_ref_code =  $v['MergeUpdate']['provider_ref_code'];
			
 			$this->getBarcodeOutside($v['MergeUpdate']['product_order_id_identify']);
 					
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $v['MergeUpdate']['order_id']) ) );
			if(count($openOrder) > 0){
			
				$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
				$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
				$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
				//$items_info =  unserialize( $openOrder['OpenOrder']['items']);
				
				$totals_info->Subtotal;
				$totals_info->PostageCost;
				$totals_info->Tax;		
				$totals_info->Currency; 
				$customer_info->Address->Country;
				$total_of_up = 0; $jpItems = [];
 				$country	=	$this->Country->find('first', array( 'conditions' => array('name' =>trim($customer_info->Address->Country)) ) );
				if(count($country) == 0){
					$country	=	$this->Country->find('first', array( 'conditions' => array('custom_name' =>trim($customer_info->Address->Country)) ) );
				}
				//pr($country);
				// exit;
				if(count($country) > 0){
 					
					$po_total =0;
					$skus = explode( ',', $v['MergeUpdate']['sku']);
					foreach( $skus as $_sku )
					{
						$newSku 	= explode( 'XS-', $_sku);  
						$getsku 	= 'S-'.$newSku[1];
						$qty 		= $newSku[0];
						$pos 		= $this->PurchaseOrder->find( 'first', array( 'conditions' => array('purchase_sku' => $getsku,'price >'=> 0 ),'order'=> 'id desc' ) );
						if(count($pos) > 0){
							$po_total += ($pos['PurchaseOrder']['price']*$qty);
						}  
									 
					}
					
					foreach($skus as $val){
						$s    = explode("XS-", $val);
						$_qqty = $s[0];
						$_sku = "S-".$s[1];	
						
						$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => $_sku,'country_code' =>  $country['Country']['iso_2'])));
						if(count($hscode) == 0){
							$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' =>  $country['Country']['iso_2'])));
						}
						if(count($hscode) == 0){
							$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' => 'GB')));
						}
						if(count($hscode) > 0){
							if($hscode['ProductHscode']['hs_code'] != ''){
								$hs_code = $hscode['ProductHscode']['hs_code'];
							}
						}
						
						$product = $this->Product->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.product_name','Product.country_of_origin')));
						
						if($product['Product']['country_of_origin'] != ''){
							$c_code= $this->Country->find('first',array('conditions' => array('Country.name' => $product['Product']['country_of_origin']),'fields'=>array('Country.iso_2'))); 
							if(count($c_code) > 0){
								$country_of_origin = $c_code['Country']['iso_2'];
							}
						}
						$price =0;
						$pos = $this->PurchaseOrder->find( 'first', array( 'conditions' => array('purchase_sku' => $_sku,'price >'=> 0 ),'order'=> 'id desc' ) );
						if(count($pos) > 0){
							$price = $pos['PurchaseOrder']['price'];
						}  
						$product_name[] = substr($product['Product']['product_name'],0,50);
						$_length[] = $product['ProductDesc']['length'];
						$_width[]  = $product['ProductDesc']['width'];
						$_height[] = $product['ProductDesc']['height'];
						$_weight[] = $product['ProductDesc']['weight'] * $_qqty;	
						$uw = $product['ProductDesc']['weight'] * $_qqty;
					  						
						$unitprice = number_format(( ($totals_info->TotalCharge / $po_total)*$price),2);
						 
						$total_of_up += (($totals_info->TotalCharge / $po_total)*$price);
						
						$jpItems[]  = ['description' => $product['Product']['product_name'],
							  'quantity' => $_qqty,
							  'country_of_origin' => $country_of_origin,
							  'unit_price'=>['value'=>intval($unitprice*100),'currency_code'=>$totals_info->Currency],
							  'unit_weight'=>['value'=>number_format(($uw / 10),2),'unit'=>'kg'],
							  'hs_code' =>  $hs_code];				
					}		
					$length	= array_sum($_length) / 10;
					$width	= array_sum($_width) / 10;
					$height	= array_sum($_height) / 10;
					$weight	= array_sum($_weight);	
						
					//$data['total_charge']    	=  $total_of_up ; 
					$data['total_charge']    	=  $totals_info->TotalCharge ;
					$data['hs_code']  		 	=  $hs_code ;
					$data['sku']  		 		=  $v['MergeUpdate']['sku'] ;
					$data['country_of_origin']  =  $country_of_origin ;
					$data['product_name']   	=  $product_name ;
					$data['quantity']   		=  $v['MergeUpdate']['quantity'] ; 
					$data['eu_country']   		=  $country['Country']['eu_country'];
					$data['provider_ref_code']  =  $provider_ref_code ;
					$data['phone_number']   	=  $customer_info->Address->PhoneNumber ;
 					$data['weight'] 			=  str_pad($weight,4,'0',STR_PAD_LEFT) ;
					$data['length'] 			=  str_pad($length,4,'0',STR_PAD_LEFT) ;
					$data['width'] 				=  str_pad($width,4,'0',STR_PAD_LEFT) ;
					$data['height'] 			=  str_pad($height,4,'0',STR_PAD_LEFT) ;
					$data['postal_service'] 	=  $v['MergeUpdate']['postal_service'] ;		
					$data['country_code'] 		=  $country['Country']['iso_2'] ;	
					$data['country'] 			=  $customer_info->Address->Country;
					$data['sub_source'] 		=  strtolower($openOrder['OpenOrder']['sub_source']) ;
					$data['split_order_id'] 	=  $v['MergeUpdate']['product_order_id_identify'] ;	
					$data['description'] 		=  implode(",",$product_name) ;
					 
 					$this->getShipment($data,$customer_info,$totals_info,$jpItems);
				
				}
				else{
					 file_put_contents(WWW_ROOT.'jerseypost/logs/api_country_'.date('dmY').'.log','Case_1'."\t".$v['MergeUpdate']['order_id']."\t".$customer_info->Address->Country."\r\n", FILE_APPEND | LOCK_EX);	
					 
					$data['error_code'] = 'country not found';
					$data['error_msg']  = $customer_info->Address->Country;
 					$this->sendErrorMail($data);
			
				}
 			} 
			  
		}
		//exit;
		return 1;
	}  
	
	public function customShipment($order_id, $reference = '')
 	{
		$this->loadModel('MergeUpdate'); 
		$this->loadModel('OpenOrder');
		$this->loadModel('Country');
		$this->loadModel('Product');
		$this->loadModel('ProductHscode');
		$this->loadModel('PurchaseOrder');  
		$country_of_origin = 'CN';
		$hs_code = '9603298000';//'8470300000';
		$product_name = '';		
 		//	
		$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $order_id,'MergeUpdate.status' => [0,1,3] ),'fields'=>array('sku','product_order_id_identify','order_id','country_code','postal_service','provider_ref_code','quantity','price') ) );
   pr($orders); //exit;
 /*$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.order_id' => $order_id, 'service_provider'=>'PostNL','delevery_country !='=>'Netherlands'),'fields'=>array('sku','product_order_id_identify','order_id','country_code','provider_ref_code') ) );
 */
		foreach($orders	 as $v){
			$provider_ref_code =  $v['MergeUpdate']['provider_ref_code'];
			
 			$this->getBarcodeOutside($v['MergeUpdate']['product_order_id_identify']);
 					
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $v['MergeUpdate']['order_id']) ) );
			if(count($openOrder) > 0){
			
				$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
				$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
				$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
				//$items_info =  unserialize( $openOrder['OpenOrder']['items']);
				
				$totals_info->Subtotal;
				$totals_info->PostageCost;
				$totals_info->Tax;		
				$totals_info->Currency; 
				$jpItems = []; $total_of_up = 0;
				
				$country	=	$this->Country->find('first', array( 'conditions' => array('name' =>trim($customer_info->Address->Country)) ) );
				if(count($country) == 0){
					$country	=	$this->Country->find('first', array( 'conditions' => array('custom_name' =>trim($customer_info->Address->Country)) ) );
				}
				 
				
				if(count($country) > 0){
 					
					$po_total =0;
					$skus = explode( ',', $v['MergeUpdate']['sku']);
					$po_total = $v['MergeUpdate']['price'];
					foreach( $skus as $_sku )
					{
						$newSku 	= explode( 'XS-', $_sku);  
						$getsku 	= 'S-'.$newSku[1];
						$qty 		= $newSku[0];
						$pos 		= $this->PurchaseOrder->find( 'first', array( 'conditions' => array('purchase_sku' => $getsku,'price >'=> 0 ),'order'=> 'id desc' ) );
						if(count($pos) > 0){
							$po_total += ($pos['PurchaseOrder']['price']*$qty);
						}  
									 
					}
					$_length = []; $_width = []; $_height = []; $_weight= [];
					 
					foreach($skus as $val){
						$s    = explode("XS-", $val);
						$_qqty = $s[0];
						$_sku = "S-".$s[1];	
						
						$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => $_sku,'country_code' =>  $country['Country']['iso_2'])));
						if(count($hscode) == 0){
							$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' =>  $country['Country']['iso_2'])));
						}
						if(count($hscode) == 0){
							$hscode = $this->ProductHscode->find('first',array('conditions' => array('sku' => 'DEFAULT','country_code' => 'GB')));
						}
						if(count($hscode) > 0){
							if($hscode['ProductHscode']['hs_code'] != ''){
								$hs_code = $hscode['ProductHscode']['hs_code'];
							}
						}
						
						$product = $this->Product->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('ProductDesc.length','ProductDesc.width','ProductDesc.height','ProductDesc.weight','Product.product_name','Product.country_of_origin')));
						if(count($product) > 0){
							if($product['Product']['country_of_origin'] != ''){
								$c_code= $this->Country->find('first',array('conditions' => array('Country.name' => $product['Product']['country_of_origin']),'fields'=>array('Country.iso_2'))); 
								if(count($c_code) > 0){
									$country_of_origin = $c_code['Country']['iso_2'];
								}
							}
							$product_name[] = substr($product['Product']['product_name'],0,50);
							$_length[] = $product['ProductDesc']['length'];
							$_width[]  = $product['ProductDesc']['width'];
							$_height[] = $product['ProductDesc']['height'];
							$_weight[] = $product['ProductDesc']['weight'] * $_qqty;	
							$uw = $product['ProductDesc']['weight'] * $_qqty;
							
							$_product_name = $product['Product']['product_name'];
						}else{
							$product_name[] = 'sample item';
							$_length[] = '53.0';
							$_width[]  = '41.1';
							$_height[] = '90.1';
							$_weight[] = '.12' * $_qqty;	
							$uw = '.12'  * $_qqty;
							$_product_name ='sample item';
						}
						$price =0;
						$pos = $this->PurchaseOrder->find( 'first', array( 'conditions' => array('purchase_sku' => $_sku,'price >'=> 0 ),'order'=> 'id desc' ) );
						if(count($pos) > 0){
							$price = $pos['PurchaseOrder']['price'];
						}else{
							$price = ($v['MergeUpdate']['price']/count($skus))/$_qqty;
						}
						
						
					  						
						$unitprice = number_format(( ($totals_info->TotalCharge / $po_total)*$price),4);
						 
						$total_of_up += (($totals_info->TotalCharge / $po_total)*$price);
						
						$jpItems[]  = ['description' => $_product_name,
							  'quantity' => $_qqty,
							  'country_of_origin' => $country_of_origin,
							  'unit_price'=>['value'=> ($unitprice*100),'currency_code'=>$totals_info->Currency],
							  'unit_weight'=>['value'=>number_format(($uw  ),2),'unit'=>'kg'],
							  'hs_code' =>  $hs_code];				
					}		
					$length	= array_sum($_length) / 10;
					$width	= array_sum($_width) / 10;
					$height	= array_sum($_height) / 10;
					$weight	= array_sum($_weight);	
						
					//$data['total_charge']    	=  $total_of_up ; 
					$data['total_charge']    	=  $totals_info->TotalCharge ;
					$data['hs_code']  		 	=  $hs_code ;
					$data['sku']  		 		=  $v['MergeUpdate']['sku'] ;
					$data['country_of_origin']  =  $country_of_origin ;
					$data['product_name']   	=  $product_name ;
					$data['quantity']   		=  $v['MergeUpdate']['quantity'] ; 
					$data['eu_country']   		=  $country['Country']['eu_country'];
					$data['provider_ref_code']  =  $provider_ref_code ;
					$data['phone_number']   	=  $customer_info->Address->PhoneNumber ;
 					$data['weight'] 			=  str_pad($weight,4,'0',STR_PAD_LEFT) ;
					$data['length'] 			=  str_pad($length,4,'0',STR_PAD_LEFT) ;
					$data['width'] 				=  str_pad($width,4,'0',STR_PAD_LEFT) ;
					$data['height'] 			=  str_pad($height,4,'0',STR_PAD_LEFT) ;
					$data['postal_service'] 	=  $v['MergeUpdate']['postal_service'] ;		
					$data['country_code'] 		=  $country['Country']['iso_2'] ;	
					$data['country'] 			=  $customer_info->Address->Country;
					$data['sub_source'] 		=  strtolower($openOrder['OpenOrder']['sub_source']) ;
					$data['split_order_id'] 	=  $v['MergeUpdate']['product_order_id_identify'] ;	
					$data['description'] 		=  implode(",",$product_name) ;
					$data['reference'] 			=  $reference.$v['MergeUpdate']['product_order_id_identify'] ;	
					
					 
 					$this->getShipment($data,$customer_info,$totals_info,$jpItems);
				
				}
				else{
					 file_put_contents(WWW_ROOT.'jerseypost/logs/api_country_'.date('dmY').'.log','Case_1'."\t".$data['split_order_id']."\t".$customer_info->Address->Country."\r\n", FILE_APPEND | LOCK_EX);	
					 
					$data['error_code'] = 'country not found';
					$data['error_msg']  = $customer_info->Address->Country;
 					$this->sendErrorMail($data);
			
				}
 			} 
			// exit;
		}
		
		return 1;
	} 
	 
	public function getCshipment()
	{
		 

					$data['total_charge']    	=  '65' ;
					$data['hs_code']  		 	=  '9603298000'; 
					$data['sku']  		 		=  'Metal';
					$data['country_of_origin']  =  'CN' ;
					$data['product_name']   	=  'Metal O-Ring' ;
					$data['quantity']   		=  1; 
					$data['eu_country']   		=  1;
					$data['provider_ref_code']  =  'Tr' ;
					$data['phone_number']   	=  '369852147';
 					$data['weight'] 			=  str_pad('0.5',4,'0',STR_PAD_LEFT) ;
					$data['length'] 			=  str_pad('0.3',4,'0',STR_PAD_LEFT) ;
					$data['width'] 				=  str_pad('0.5',4,'0',STR_PAD_LEFT) ;
					$data['height'] 			=  str_pad('0.1',4,'0',STR_PAD_LEFT) ;
					$data['postal_service'] 	=  'Tracked' ;		
					$data['country_code'] 		=  'GB' ;	
					$data['country'] 			=  'United Kingdom';
					$data['sub_source'] 		=  '' ;
					$data['split_order_id'] 	=  'C123456-1' ;		
					

		
 		 
		$EmailAddress = 'abc@eur.co.uk';
 		 
		$store_country = 'GB';
		
		$sender = $this->getSenderInfo($data,$store_country);
 	 
 		$auth = Configure::read( 'jerseypost' );
		$product_code = $auth['product_code']; 
		$api_account_id = $auth['api_account_id'];
 		$server_url = $auth['server_url'];
		$api_token = $auth['api_token'];
  		 
		$address1  = '17 Furzeham Rd.' ;
		$address2 =  '' ;		 
		$address3 = '';
 		
		$unitpricew = $data['total_charge']/$data['quantity'];
		$unitprice = (int)$unitpricew;
		$unitweight =  number_format($data['weight']/$data['quantity'],2);

		 $data['address1'] 		= $address1;
		 $data['address2'] 		= $address2;
		 $data['address3'] 		= $address3;
		 $customer_phone_number = '3692581478';
		 
     	 $from_address = array (
					'reference' => $data['split_order_id'],
					'address_line_1' => $sender['FromAddress'],
					'address_line_2' => NULL,
					'dependent_locality' =>$sender['FromCity'],
					'locality' => "A",					 
					'postal_code' => $sender['FromPostCode'],
					'country_code' => 'JE',
					'instructions' => 'Please go to side door.',
					'contact' => 
					array (
					  'organisation' =>$sender['FromCompany'],
					  'given_name' =>$sender['FromPersonName'],
					  'additional_name' => NULL,
					  'family_name' => NULL,
					  'phone' => $sender['FromPhoneNumber'], 
					  'email' =>  $sender['email'], 
 					  'tax_id' => $sender['vat_number'],
   					
					),
				  );
				  
			 
				$service_type = 'UNTRACKED';
			 
			 
			if($data['weight'] > 2){
				$service_type = 'EMS';
			}
	  	 

 		 $jpdata = array (
			  'data' => 
			  array (
				'type' => 'shipments',
				'attributes' => 
				array (
				  'product' => $product_code,
				  'service' => $service_type,
				  'from_address' => $from_address,				  
				  'to_address' => 
					  array (
					'reference' =>$data['split_order_id'],
					'address_line_1' => '17 Furzeham Rd.',
					'address_line_2' => NULL,
					'dependent_locality' => '',
					'locality' => 'West Drayton.',
					//'administrative_area' => $customer_info->Address->Town,
					'postal_code' => 'UB7 9DD ',
					'country_code' =>  $data['country_code'],
					'instructions' => 'Please go to side door.',
					'contact' => 
					array (
					  'organisation' => 'Jonathan Nicholls',
					  'given_name' =>  'Jonathan Nicholls',
					  'additional_name' => NULL,
					  'family_name' => NULL,
					  'phone' => $customer_phone_number, 
					  'email' =>'abc@eur.co.uk' 
					),
				  ),
				  'return_address' =>$from_address,
				  'parcels' => 
				 	 array (
					0 => 
					array (
					  'weight' => 
					  array (
						'value' => number_format($data['weight'],2),
						'unit' => 'kg',
					  ),
					  'dimensions' => 
					  array (
						'width' => number_format($data['width']/10,2) ,
						'height' => number_format($data['height']/10,2) ,
						'length' => number_format($data['length']/10,2) ,
						'unit' => 'cm',
					  ),
					  
  					
					  'items' => 
					  array (
						0 => 
						array (
						  'description' => $data['product_name'],
						  'quantity' => $data['quantity'],
						  'country_of_origin' => $data['country_of_origin'],
						  'unit_price'=>['value'=>$unitprice,'currency_code'=>'GBP'],
						  'unit_weight'=>['value'=>number_format($unitweight,2),'unit'=>'kg'],
						 /* 'unit_price' => 
						  array (
							'value' => 123,
							'currency_code' => 'GBP',
						  ),
						  'unit_weight' => 
						  array (
							'value' => $data['weight'],
							'unit' => 'kg',
						  ),
						  'country_of_origin' => $data['country_of_origin'],
						  'sku' =>$data['sku'],
						  'hs_code' =>  $data['hs_code'],*/
						),
					  ),
					  'description' => 'Xsensys_'.$data['product_name'],
					  'reference' => 'Xsensys_'.$data['split_order_id'],					  
					  'format' => 'P',
					),
				  ),
 				 'total_price'=>array('value'=>$data['total_charge'],'currency_code'=>'GBP'),
				  'export_reason' => 'sold',
				  'terms_of_trade'=>"DDP",
				  'reference_1'=>'Order '.$data['split_order_id']
				  
				//  'do_not_deliver_after' => date('Y-m-d\TH:i:s+00:00'),// '2020-11-19T14:00:30+00:00'
				),
			  ),
		); 
		
	 		//pr($jpdata);
			 
			file_put_contents(WWW_ROOT.'jerseypost/request/request_'.$data['split_order_id'].'.json',json_encode($jpdata));
  			// echo  $server_url."/v1/shipments";exit;
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $server_url."/v1/shipments?include=parcels.parcel-documents",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>json_encode($jpdata),
			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/vnd.api+json",
				"Accept: application/vnd.api+json",
				"Account-Id: {$api_account_id}",
				"Authorization: Bearer {$api_token}" 
			  ),
			));
			
			$response = curl_exec($curl);
			file_put_contents(WWW_ROOT.'jerseypost/response/response_'.$data['split_order_id'].'.json',$response);
			$err = curl_error($curl);
			//pr( curl_getinfo($curl));
			$responseArray = json_decode($response,true);
			curl_close($curl);
			 pr($responseArray);
 			
			if($err) {
  			  $data['error_code'] = $err;
			  file_put_contents(WWW_ROOT.'jerseypost/logs/'.date('dmY').'.log','Case_1'."\t".$data['split_order_id']."\t".$data['error_code']."\r\n", FILE_APPEND | LOCK_EX);	
			  
			} 
			return 1;
			exit;
 	
	}
	
 	public function getShipment($data, $customer_info,$totals_info,$jpItems)
	{
 		$this->loadModel('MergeUpdate');	  
		
		$EmailAddress = $customer_info->Address->EmailAddress;
 		$exp = explode(".",$EmailAddress);
		$index = count($exp) - 1;
		$store_country = strtoupper($exp[$index]);
		
		$sender = $this->getSenderInfo($data,$store_country);
 	 
 		$auth = Configure::read( 'jerseypost' );
		$product_code = $auth['product_code']; 
		$api_account_id = $auth['api_account_id'];
 		$server_url = $auth['server_url'];
		$api_token = $auth['api_token'];
 		
		preg_match_all('/([\d]+)/', $customer_info->Address->Address1, $match);
    	$house_nr = $match[0][0];
		 
 		$_address1 = ''; $_address2  = ''; $_address3 = '';
		
		
		if($customer_info->Address->Country == 'Germany'){
			$lines = explode("\n", wordwrap(htmlentities($this->replaceChar($customer_info->Address->Address2)).' '.htmlentities($this->replaceChar($customer_info->Address->Address3)).' '.htmlentities($this->replaceChar($customer_info->Address->Region)), '30'));
		 }else{
			 $lines = explode("\n", wordwrap(htmlentities($this->replaceChar($customer_info->Address->Address2)).' '.htmlentities($this->replaceChar($customer_info->Address->Address3)), '30'));
		 }
		 
		if(isset($lines[0]) && $lines[0] != ''){
			$_address1  =  trim($lines[0]) ;
		}
		if(isset($lines[1]) && $lines[1] != ''){
			$_address2 =  trim($lines[1]) ;
		}
		if(isset($lines[2]) && $lines[2] != ''){
			$_address3 = trim($lines[2]);
		}
 		$data['total_charge'] = $data['total_charge'] * 100;
		$unitpricew = $data['total_charge']/$data['quantity'];
		//$unitprice = (int)$unitpricew;
		$unitprice  =  intval($unitpricew);
		$unitweight =  number_format($data['weight']/$data['quantity'],2);

		$data['address2'] = $_address1.' '.$_address2.' '.$_address3;
		  
		 $customer_phone_number = $customer_info->Address->PhoneNumber;
		 $currency_code		  = $totals_info->Currency;	
		
		 if(isset($customer_info->Address->PhoneNumber) && strlen($customer_info->Address->PhoneNumber) > 8){
  			 preg_match_all('/([\d]+)/', $customer_info->Address->PhoneNumber, $telmatch);
 			 $tel ='';
			 for($i = 0; $i< count($telmatch[0]); $i++){
				$tel .= $telmatch[0][$i];
 			 }
  			 $country_pcode = '';  
			 if($customer_info->Address->Country == 'France'){
				 $country_pcode = '+33';
				 $product_code  = 'LAPOSTE';  
			 }else if($customer_info->Address->Country == 'Germany'){
				 $country_pcode = '+49';
			 }else if($customer_info->Address->Country == 'Spain'){
				 $country_pcode = '+34';
			 }else if($customer_info->Address->Country == 'Italy'){
				 $country_pcode = '+39';
			 }else if($customer_info->Address->Country == 'United Kingdom'){
				 $country_pcode = '+44';
			 }
			 if($country_pcode != ''){
				 $customer_phone_number = $country_pcode.substr($tel,-10);
			 }else{
			 	 $customer_phone_number = $tel;
			 }
  		 } 
 		 $reference = 'Dev-'.$data['reference'];			
    	 $from_address = array (
					'reference' => $reference,
					'address_line_1' => $sender['FromAddress'],
					'address_line_2' => NULL,
					'dependent_locality' =>$sender['FromCity'],
					'locality' => "A",					 
					'postal_code' => $sender['FromPostCode'],
					'country_code' => 'JE',
					'instructions' => '',
					'contact' => 
					array (
					  'organisation' =>$sender['FromCompany'],
					  'given_name' =>$sender['FromPersonName'],
					  'additional_name' => NULL,
					  'family_name' => NULL,
					  'phone' => $sender['FromPhoneNumber'], 
					  'email' =>  $sender['email'], 
					  'identifiers' => ['ioss_number' => '1234567890'],
 					  'tax_id' => $sender['vat_number'],
   					
					),
				  );
				  
			if($data['postal_service'] == 'Standard'){
				$service_type = 'UNTRACKED';
			}else{
				$service_type = 'TRACKED';
			}
			 
			if($data['weight'] > 2){
				$service_type = 'EMS';
			}
	  
			 $jpdata = array (
			  'data' => 
			  array (
				'type' => 'shipments',
				'attributes' => 
				array (
				  'product' => $product_code,
				  'service' => $service_type,
				  'from_address' => $from_address,				  
				  'to_address' => 
					  array (
					'reference' =>$reference,
					'address_line_1' =>$this->replaceChar($customer_info->Address->Address1),// $this->getCountryAddress($data),
					'address_line_2' => $data['address2'],
					'dependent_locality' => $this->replaceChar($customer_info->Address->Region),
					'locality' => $customer_info->Address->Town,
					//'administrative_area' => $customer_info->Address->Town,
					'postal_code' => $customer_info->Address->PostCode,
					'country_code' =>  $data['country_code'],
					'instructions' => '',
					'contact' => 
					array (
					  'organisation' => NULL ,
					  'given_name' =>  $this->replaceChar($customer_info->Address->FullName),
					  'additional_name' => NULL,
					  'family_name' => NULL,
					  'phone' => $customer_phone_number, 
					  'email' =>$customer_info->Address->EmailAddress 
					),
				  ),
				  'return_address' =>$from_address,
				  'parcels' => 
				 	 array (
					0 => 
					array (
					  'weight' => 
					  array (
						'value' => number_format($unitweight*$data['quantity'],2),
						'unit' => 'kg',
					  ),
					  'dimensions' => 
					  array (
						'width' => number_format($data['width']/10,2) ,
						'height' => number_format($data['height']/10,2) ,
						'length' => number_format($data['length']/10,2) ,
						'unit' => 'cm',
					  ),
  					  'items' => $jpItems,
					  'description' => $data['description'],
					  'reference' => $reference,					  
					  'format' => 'P',
					),
				  ),
 				 'total_price'=>array('value'=>intval($data['total_charge']),'currency_code'=>$currency_code),
				  'export_reason' => 'sold',
				  'terms_of_trade'=>"DDP",
				  'reference_1'=>$reference
				  
				//  'do_not_deliver_after' => date('Y-m-d\TH:i:s+00:00'),// '2020-11-19T14:00:30+00:00'
				),
			  ),
		); 
		
	 		pr($jpdata); //exit;
			//exit;
			file_put_contents(WWW_ROOT.'jerseypost/request/request_'.$data['split_order_id'].'.json',json_encode($jpdata));
  			// echo  $server_url."/v1/shipments";exit;
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $server_url."/v1/shipments?include=parcels.parcel-documents",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>json_encode($jpdata),
			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/vnd.api+json",
				"Accept: application/vnd.api+json",
				"Account-Id: {$api_account_id}",
				"Authorization: Bearer {$api_token}" 
			  ),
			));
			
			$response = curl_exec($curl);
			file_put_contents(WWW_ROOT.'jerseypost/response/response_'.$data['split_order_id'].'.json',$response);
			$err = curl_error($curl);
			//pr( curl_getinfo($curl));
			$responseArray = json_decode($response,true);
			curl_close($curl);
			 pr($responseArray);
 			
			if($err) {
  			  $data['error_code'] = $err;
			  file_put_contents(WWW_ROOT.'jerseypost/logs/'.date('dmY').'.log','Case_1'."\t".$data['split_order_id']."\t".$data['error_code']."\r\n", FILE_APPEND | LOCK_EX);	
			  $this->sendErrorMail($data);
			}else { 
				if(isset($responseArray['errors']) && count($responseArray['errors']) > 0){
					 $data['error_code'] = $responseArray['errors'][0]['status'] ;
					 $data['error_msg'] = $responseArray['errors'][0]['detail'] ;
					 file_put_contents(WWW_ROOT.'jerseypost/logs/'.date('dmY').'.log','Case_2'."\t".$data['split_order_id']."\t".$data['error_code']."\r\n", FILE_APPEND | LOCK_EX);	
					 $this->lockOrder($data);	
					 $this->sendErrorMail($data);
				}else{
					$this->saveResponse($data['split_order_id']);
					$this->pdfToImg($data['split_order_id']);
				}
			} 
			return 1;
 	}
	
	public function saveResponse($split_order_id = '')
	{
		$this->loadModel('JerseypostData');
		$this->loadModel('MergeUpdate');	  
 		 
		$response = file_get_contents(WWW_ROOT.'jerseypost/response/response_'.$split_order_id.'.json');
 		$responseArray = json_decode($response,true);
  		
		$parcel_id = $responseArray['included'][0]['id'];	 
		$save['split_order_id'] = $split_order_id;
		$save['shipment_id'] = $parcel_id;
		$save['barcode'] = $responseArray['data']['attributes']['tracking_number'];
		$save['status'] = $responseArray['data']['attributes']['status'];
		$save['service'] = $responseArray['data']['attributes']['service'];
		$save['created_at'] = $responseArray['included'][1]['attributes']['created_at']['date'];
		$save['data'] = json_encode($responseArray['data']);
		$save['included'] = json_encode($responseArray['included']);
		$save['timestamp'] = date('Y-m-d H:i:s');
		$this->JerseypostData->saveAll( $save );
		$content = base64_decode($responseArray['included']['0']['attributes']['documents']['0']['content']);
		$imgPath = WWW_ROOT .'jerseypost/labels/Label_'.$split_order_id.'.pdf'; 				 
		file_put_contents($imgPath,$content); 
		
		$this->MergeUpdate->updateAll( array( 'MergeUpdate.track_id' => "'".$responseArray['data']['attributes']['tracking_number']."'",'MergeUpdate.reg_post_number' => "'".$responseArray['data']['attributes']['tracking_number']."'",'MergeUpdate.reg_num_img' => "'".$responseArray['data']['attributes']['tracking_number'].".png'") , array( 'MergeUpdate.product_order_id_identify' => $split_order_id ) );	  
		 
 		 $this->markedParcels($parcel_id,$split_order_id);
		
		return 1;			 
			 
 	}
	
	public function markedParcels($parcel_id = null,$split_order_id = null){

 		$this->loadModel('MergeUpdate');
		$this->loadModel('JerseypostData');
		
		$auth = Configure::read( 'jerseypost' );
		$CustomerCode = $auth['product_code']; 
		$api_account_id = $auth['api_account_id'];
 		$server_url = $auth['server_url'];
		$api_token = $auth['api_token'];
   		//$parcelid[] = '1eb8cd32-6915-6a8c-859c-0242ac100802';
		//$parcelid[] = '1eb8cd32-692c-6142-9b4d-0242ac100802';
		 $pdata = array(
			'data' => array(
				'type'=> 'marked-parcels',
				'attributes'=> array('parcel_ids' => array($parcel_id))
			)
	    );
		// echo json_encode($pdata);
	 
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
			  CURLOPT_URL => $server_url."/v1/marked-parcels",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>json_encode($pdata),
			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/vnd.api+json",
				"Accept: application/vnd.api+json",
				"Account-Id: {$api_account_id}",
				"Authorization: Bearer {$api_token}" 
			  ),
			));
			
			$response = curl_exec($curl);
			file_put_contents(WWW_ROOT.'jerseypost/logs/marked_'.date('dmY').'.json',$response."\n",FILE_APPEND | LOCK_EX);
			$err = curl_error($curl);
			//pr( curl_getinfo($curl));
			$responseArray = json_decode($response,true);
			curl_close($curl);
			//pr($responseArray);
				
			$data['split_order_id'] = $split_order_id;
			if($err) {
  			  $data['error_msg'] = 'marked issue';
			  $data['error_code'] = $err;
			  file_put_contents(WWW_ROOT.'jerseypost/logs/'.date('dmY').'.log','marked'."\t".$split_order_id."\r\n", FILE_APPEND | LOCK_EX);	
			  $this->sendErrorMail($data);
			}else { 
				if(isset($responseArray['errors']) && count($responseArray['errors']) > 0){
					 $data['error_code'] = $responseArray['errors'][0]['status'] ;
					 $data['error_msg'] = $responseArray['errors'][0]['detail'] ;
					 file_put_contents(WWW_ROOT.'jerseypost/logs/'.date('dmY').'.log','marked'."\t".$split_order_id."\t".$data['error_code']."\r\n", FILE_APPEND | LOCK_EX);	
					
					 $this->sendErrorMail($data);
				}
			} 
			$this->unmarkedParcels();
		return 1;
	}
	
	public function unmarkedParcels(){
		
 		$this->loadModel('JerseypostData');
		$this->loadModel('MergeUpdate');
		
		$split_order_ids = [];
	 	 
		$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.merge_order_date >' => '2020-12-29','MergeUpdate.status' => 1,'MergeUpdate.scan_date IS NULL','MergeUpdate.scanned_user IS NULL', 'MergeUpdate.service_provider' => 'Jersey Post'),'fields'=>array('sku','product_order_id_identify','order_id','country_code','postal_service','provider_ref_code','quantity') ) );
		
		if(count($orders) > 0){
		
			foreach($orders as $val){
				$split_order_ids[] = $val['MergeUpdate']['product_order_id_identify'];
			}
		}
		//canceld orders
		$orders	=	$this->MergeUpdate->find('all', array( 'conditions' => array( 'MergeUpdate.merge_order_date >' => '2020-12-29','MergeUpdate.status' => 2, 'MergeUpdate.service_provider' => 'Jersey Post'),'fields'=>array('sku','product_order_id_identify','order_id','country_code','postal_service','provider_ref_code','quantity') ) );
		if(count($orders) > 0){
		
			foreach($orders as $val){
				$split_order_ids[] = $val['MergeUpdate']['product_order_id_identify'];
			}
		}
 		 
		//$split_order_ids = ['332691901072021-1','338022801072021-1','338123901072021-1'];
		
		$parcels = $this->JerseypostData->find('all', array( 'conditions' => array( 'JerseypostData.split_order_id' => $split_order_ids),'fields'=>array('shipment_id') ) );
  		 
		if(count($parcels) > 0){
		
			$auth = Configure::read( 'jerseypost' );
			$CustomerCode = $auth['product_code']; 
			$api_account_id = $auth['api_account_id'];
			$server_url = $auth['server_url'];
			$api_token = $auth['api_token'];
			
			foreach($parcels as $val){
				$parcel_ids[] = $val['JerseypostData']['shipment_id'];
			}
		 
			 $pdata = array(
				'data' => array(
					'type'=> 'unmarked-parcels',
					'attributes'=> array('parcel_ids' => $parcel_ids)
				)
			);
			//pr($pdata); 
		  
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
				  CURLOPT_URL => $server_url."/v1/unmarked-parcels",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS =>json_encode($pdata),
				  CURLOPT_HTTPHEADER => array(
					"Content-Type: application/vnd.api+json",
					"Accept: application/vnd.api+json",
					"Account-Id: {$api_account_id}",
					"Authorization: Bearer {$api_token}" 
				  ),
				));
				
			  	$response = curl_exec($curl);
				file_put_contents(WWW_ROOT.'jerseypost/logs/unmarked_'.date('dmY').'.json',$response."\n",FILE_APPEND | LOCK_EX);
			 	$err = curl_error($curl);
				curl_close($curl);
			//	pr( curl_getinfo($curl));
				$responseArray = json_decode($response,true);
 				//pr($responseArray);
				return 1;
			}else{
				return 2;
			}
	}
	
 	public function createManifest(){
		
		$this->loadModel('JerseypostData');
		$this->loadModel('MergeUpdate');
		$this->unmarkedParcels();
		
		$auth = Configure::read( 'jerseypost' );
		$CustomerCode = $auth['product_code']; 
		$api_account_id = $auth['api_account_id'];
 		$server_url = $auth['server_url'];
		$api_token = $auth['api_token'];
		$products = array('UPU','LAPOSTE');
		$this->Session->setflash('Manifest is created on Atlas', 'flash_success'); 
		$this->redirect($this->referer());
		die('done.');
		foreach($products as $product){
			$pdata = array(
				'data' => array(
					'type'=> 'manifests',
					'attributes'=> array('products' =>array(
										array('product'=> $product,
											 'services'=> array('TRACKED','UNTRACKED','EMS')
											))
									),
					'reference' => 'Xsen-'.date('dmyhis')
					)
			);
			//pr($pdata);
			//echo json_encode($pdata);
				 
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
				  CURLOPT_URL => $server_url."/v1/manifests?include=manifest-documents,shipments",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
			   CURLOPT_POSTFIELDS =>json_encode($pdata),
				  CURLOPT_HTTPHEADER => array(
					"Content-Type: application/vnd.api+json",
					"Accept: application/vnd.api+json",
					"Account-Id: {$api_account_id}",
					"Authorization: Bearer {$api_token}" 
				  ),
				));
				
				$response = curl_exec($curl);
				file_put_contents(WWW_ROOT.'jerseypost/logs/manifest_'.$product.'_'.date('dmyhis').'.json',$response);
				$err = curl_error($curl);
				//pr( curl_getinfo($curl));
				$responseArray = json_decode($response,true);
				curl_close($curl);
				//pr($responseArray);
					
				foreach($responseArray['included'] as $res){
					if($res['type'] == 'manifest-documents'){
						if(count($res['attributes']['documents']) == 0){
							$data['error_code'] = 'manifest issue';
							$data['error_msg'] = 'Jerseypost manifest have no data';
							$this->sendErrorMail($data);
						}else{
							$content = base64_decode($res['attributes']['documents']['0']['content']);
							$imgPath = WWW_ROOT .'jerseypost/manifests/'.date('dmYHis').'.pdf'; 				 
							file_put_contents($imgPath,$content); 
						}
					}
				}
			}
		return 1;
	}
	
	public function parcels(){
		
		$this->loadModel('JerseypostData');
		$this->loadModel('MergeUpdate');
		
		$auth = Configure::read( 'jerseypost' );
		$CustomerCode = $auth['product_code']; 
		$api_account_id = $auth['api_account_id'];
 		$server_url = $auth['server_url'];
		$api_token = $auth['api_token'];
		
		 	 
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
			  CURLOPT_URL => $server_url."/v1/parcels",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		//	  CURLOPT_CUSTOMREQUEST => "POST",
		//  CURLOPT_POSTFIELDS =>json_encode($pdata),
			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/vnd.api+json",
				"Accept: application/vnd.api+json",
				"Account-Id: {$api_account_id}",
				"Authorization: Bearer {$api_token}" 
			  ),
			));
			
		echo	$response = curl_exec($curl);
			file_put_contents(WWW_ROOT.'jerseypost/response/parcels_'.date('dmyhis').'.json',$response);
			$err = curl_error($curl);
			//pr( curl_getinfo($curl));
			$responseArray = json_decode($response,true);
			curl_close($curl);
			pr($responseArray);
	}

	public function manifestDocuments(){
		
		$this->loadModel('JerseypostData');
		$this->loadModel('MergeUpdate'); 
		
		$auth = Configure::read( 'jerseypost' );
		$CustomerCode = $auth['product_code']; 
		$api_account_id = $auth['api_account_id'];
 		$server_url = $auth['server_url'];
		$api_token = $auth['api_token'];
		$id = '1eb46b0a-3743-681c-a8d3-0242ac101602';
 
		 	 
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
			  CURLOPT_URL => $server_url."/v1/manifest-documents/{$id}",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		 //	  CURLOPT_CUSTOMREQUEST => "POST",
		//   CURLOPT_POSTFIELDS =>json_encode($pdata),
			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/vnd.api+json",
				"Accept: application/vnd.api+json",
				"Account-Id: {$api_account_id}",
				"Authorization: Bearer {$api_token}" 
			  ),
			));
			
		echo	$response = curl_exec($curl);
			file_put_contents(WWW_ROOT.'jerseypost/manifests/manifest-documents_'.date('dmyhis').'.json',$response);
			$err = curl_error($curl);
			//pr( curl_getinfo($curl));
			curl_close($curl);
			
 			$responseArray = json_decode($response,true);
				pr($responseArray); 
			if(count($responseArray['data']['attributes']['documents']) == 0){
				$data['error_code'] = 'manifest issue';
				$data['error_msg'] = 'Jerseypost manifest have no data';
				$this->sendErrorMail($data);
			}else{
				$content = base64_decode($responseArray['data']['attributes']['documents']['0']['content']);
				$imgPath = WWW_ROOT .'jerseypost/manifests/'.date('dmYHis').'.pdf'; 				 
				file_put_contents($imgPath,$content); 
			}
				 
			 
	}
	
	private function replaceChar($string = null){
  		//return iconv('UTF-8','ASCII//TRANSLIT',$string);	 
		 
			
		$unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E','Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Nº'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U','Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c','è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n','nº'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o','ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y','ü'=>'u','º'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	 
	}
		
	private function sendErrorMail($details = null){
		
		$web_store = Configure::read( 'web_store' );
		$subject   = 'Jerseypost API issue in '.$web_store;
		$mailBody  = '<p><strong>Jerseypost API have some issue please review and solve it.</strong></p>';
		
		if(isset($details['split_order_id'])){
			$subject   = $details['split_order_id'].' Jerseypost API orders issue in '.$web_store;
			$mailBody  = '<p><strong>'.$details['split_order_id'].' have below issue please review and solve it.</strong></p>';
		}	
		if(isset($details['error_code'])){
			$mailBody .= '<p>Error Code : '.$details['error_code'].'</p>';
			$mailBody .= '<p>Error Message : '.$details['error_msg'].'</p>';
		}
		 
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('');
		$email->emailFormat('html');
		$email->from('jerseypost@'.$web_store); 	
 		$email->to( array('avadhesh.jij@rediffmail.com'));	 
		$email->subject( $subject );
	  	$email->send( $mailBody );
	}
	
	private function lockOrder($details = null){
 		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderNote' ); 
		$md = $this->MergeUpdate->find('first', array( 'conditions' => array('product_order_id_identify' => $details['split_order_id']),'fields'=>'order_id' ) );
		if(count($md) > 0){
			$lock_note = $details['error_msg'];
			
			$firstName = 'Jerseypost API';
			$lastName = 'Cron';
		//	$mdata['jerseypost_barcode'] = '\'\'';
			$mdata['status']    = 3;
 			 
			$this->MergeUpdate->updateAll($mdata, array( 'product_order_id_identify' => $details['split_order_id'] ) );
			$this->OpenOrder->updateAll(array('OpenOrder.status' => 3), array('OpenOrder.num_order_id' => $md['MergeUpdate']['order_id']));
			
			$noteDate['order_id'] = $md['MergeUpdate']['order_id'];
			$noteDate['note'] = $lock_note;
			$noteDate['type'] = 'Lock';
			$noteDate['user'] = $firstName.' '.$lastName;
			$noteDate['date'] = date('Y-m-d H:i:s');
			$this->OrderNote->saveAll( $noteDate );  
		}
	}	
	
	public function unLockOrder($split_order_id = null)
	{
			$this->loadModel( 'MergeUpdate' );
			$this->loadModel( 'OpenOrder' );			 
			$md = $this->MergeUpdate->find('first', array( 'conditions' => array('product_order_id_identify' => $split_order_id),'fields'=>'order_id' ) );
			if(count($md) > 0)
			{
 				$this->MergeUpdate->updateAll( array( 'MergeUpdate.status' => 0) , array( 'MergeUpdate.product_order_id_identify' => $split_order_id ) );
				$this->OpenOrder->updateAll( array( 'OpenOrder.status' => 0 ) , array( 'OpenOrder.num_order_id' => $md['MergeUpdate']['order_id'] ) );
			}
			 
	}
	public function testLabel()
	{				
		
		$this->loadModel( 'MergeUpdate' );
		$data['split_order_id'] = '3032615-1'; 
		$or = explode("-",$data['split_order_id']);  
		$barcode_type = 'RI';
		
		$main_height = 1550; $main_left = 500;
		if($barcode_type == 'RI'){
			$main_height = 1670;
			$main_left = 50;
		}
		
		//	$this->MergeUpdate->updateAll(array('service_provider' => '\'BRT\'', 'brt' => 1, 'brt_phone' => 1, 'brt_exprt' => '1'), array('product_order_id_identify' =>'2865407-1'));	
							 
		 $slip = $this->getSlip($data['split_order_id'],$or[0]);	
			
		// $file_name = 'label_'.$filename.'.pdf';
		
		// $response = file_get_contents(WWW_ROOT.'logs/response_'.$data['split_order_id'].'.json');
		 // $res = json_decode($response,1);
		// pr($res);
		 // $content = base64_decode($res['ResponseShipments']['0']['Labels']['0']['Content']);
			 
			 
			 
			 
		 $imgPath = WWW_ROOT .'logs/'.$data['split_order_id'].'.jpg'; 	
		  //file_put_contents(WWW_ROOT.'logs/response_'.$data['split_order_id'].'.json',$response);
		 // file_put_contents($imgPath,$content);
		
		
		/*--------------------------Re-Size Order Barcode----------------------------*/
		$cover_img = WWW_ROOT .'img/orders/barcode/'.$data['split_order_id'].'.png';
		$new_ord_barcode = WWW_ROOT .'logs/barcode_'.$data['split_order_id'].'.png';
		$width = 224; 
		$height = 59;
		$newwidth  = 400; 
		$newheight = 150;
		$src = imagecreatefrompng($cover_img);
		$dst = imagecreatetruecolor($newwidth, $newheight);
		imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagepng($dst,$new_ord_barcode);
		imagedestroy($dst);
		imagedestroy($src);
		/*--------------------------END Re-Size Order Barcode----------------------------*/
		/*--------------------------Append Order Barcode----------------------------*/
		$dest = imagecreatefromjpeg($imgPath);
		$src = imagecreatefrompng($new_ord_barcode);
		
		imagecopymerge($dest, $src, $main_left, $main_height, 0, 0, $newwidth , $newheight, 100); //have to play with these numbers for it to work for you, etc. 
		imagejpeg($dest,WWW_ROOT .'logs/label_'.$data['split_order_id'].'.jpg');
				
		imagedestroy($dest);
		imagedestroy($src);
		/*-------------------------End of Append Order Barcode----------------------------*/
		
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		$dompdf->set_paper('A4', 'portrait'); 
		
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
					 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
					 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
					 <meta content="" name="description"/>
					 <meta content="" name="author"/>';
			$html .= '<table style="height:1090px;  width:680px; border:1px solid #CCC;"  > 
				<tr><td style="height:600px; padding-top:20px; padding:5px 0px 5px 5px;" colspan="2" valign="top" >'.$slip.'</td></tr> 
			<tr><td valign="top" style="padding:0px 0px 5px 12px;">						
			<div style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
					transform-origin: 50% 50%;height:270px; width:450px;">
					<img src="'.Router::url('/', true).'logs/'.$data['split_order_id'].'.jpg" width="350px" style="margin-left: -110px; margin-right: -110px;margin-top: -70px;"></div></td><td><img src="'.Router::url('/', true).'img/orders/barcode/'.$data['split_order_id'].'.png" style="-webkit-transform: rotate(170deg);transform: rotate(270deg);-webkit-transform-origin: 50% 50%;
					transform-origin: 50% 50%;height:60px; margin-left: -28px;" width="224px"> </td> 
					
					 
					</tr>
					 
					
			</table>';
			
		$cssPath = WWW_ROOT .'css/';
		$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
		echo $html;
		$dompdf->load_html($html, Configure::read('App.encoding'));
		$dompdf->render();
		$imgPath = WWW_ROOT .'logs/'; 
		$name	=	'Label_Slip_'.$data['split_order_id'].'.pdf';
		unlink($imgPath.$name); 
		file_put_contents($imgPath.$name, $dompdf->output());
		
		
		exit;
		
	}
	
	public function testSingleLabel()
	{				
		
		$this->loadModel( 'MergeUpdate' );
		$data['split_order_id'] = '3032615-1'; 
		$or = explode("-",$data['split_order_id']);  
		$barcode_type = 'RI';
		
		$main_height = 1550; $main_left = 500;
		if($barcode_type == 'RI'){
			$main_height = 1670;
			$main_left = 50;
		}
  		 $imgPath = WWW_ROOT .'logs/'.$data['split_order_id'].'.jpg'; 	
		  //file_put_contents(WWW_ROOT.'logs/response_'.$data['split_order_id'].'.json',$response);
		 // file_put_contents($imgPath,$content);
		
		
 
		
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		//$dompdf->set_paper('A4', 'portrait'); 
		$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');
	 	$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
			<meta content="" name="description"/>
			<meta content="" name="author"/>';
			$html .= '<table style="width:330px; border:1px solid #CCC;"> 
			<tr><td valign="top"><div>
					<img src="'.Router::url('/', true).'logs/'.$data['split_order_id'].'.jpg" width="99%"></div><div><center><img src="'.Router::url('/', true).'img/orders/barcode/'.$data['split_order_id'].'.png" style="height:50px;"></center></div></td></tr> 								
			</table>';
			
		$cssPath = WWW_ROOT .'css/';
		$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';					
		echo $html;
		$dompdf->load_html($html, Configure::read('App.encoding'));
		$dompdf->render();
		$imgPath = WWW_ROOT .'logs/'; 
		$name	=	'Label__'.$data['split_order_id'].'.pdf';
		unlink($imgPath.$name); 
		file_put_contents($imgPath.$name, $dompdf->output()); 
		exit;
		
	}
	
 	public function getSlip($splitOrderId = null, $openOrderId = null)
	{
	
		//echo 'xxxxxxxxxxxxxxx';exit;
		$this->loadModel('OrderLocation');
		$this->loadModel('MergeUpdate');
		$this->loadModel('CategoryContant');
		$this->loadModel('Product');
		
		$this->GlobalBarcode = $this->Components->load('Common');		
		App::import('Controller', 'Linnworksapis');
		$obj = new LinnworksapisController();
		$order	=	$obj->getOpenOrderById( $openOrderId );
		
		$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
		$itemPrice			=		$getSplitOrder['MergeUpdate']['price'];
		$itemQuantity		=		$getSplitOrder['MergeUpdate']['quantity'];
		$BarcodeImage		=		$getSplitOrder['MergeUpdate']['order_barcode_image'];
		$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
		$assignedservice	=	 	$order['assigned_service'];
		$courier			=	 	$order['courier'];
		$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
		$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
		$barcode			=	 	$order['assign_barcode'];
		$subtotal			=		$order['totals_info']->Subtotal;
		$subsource			=		$order['sub_source'];
		$totlacharge		=		$order['totals_info']->TotalCharge;
		$ordernumber		=		$order['num_order_id'];
		$fullname			=		$order['customer_info']->Address->FullName;
		$address1			=		$order['customer_info']->Address->Address1;
		$address2			=		$order['customer_info']->Address->Address2;
		$address3			=		$order['customer_info']->Address->Address3;
		/*$address1 			= 		wordwrap($order['customer_info']->Address->Address1, 30, "\n<br>", false);	
		$address2 			= 		wordwrap($order['customer_info']->Address->Address2, 30, "\n<br>", false);	
		$address3			=		$order['customer_info']->Address->Address3;
		if(strlen($order['customer_info']->Address->Address3) > 30)	{
			$address3 		= 		wordwrap($order['customer_info']->Address->Address3, 30, "\n<br>", false);
		}*/
		$town				=	 	$order['customer_info']->Address->Town;
		$resion				=	 	$order['customer_info']->Address->Region;
		$postcode			=	 	$order['customer_info']->Address->PostCode;
		$country			=	 	$order['customer_info']->Address->Country;
		$phone				=	 	$order['customer_info']->Address->PhoneNumber;
		$company			=	 	$order['customer_info']->Address->Company;
		$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
		$postagecost		=	 	$order['totals_info']->PostageCost;
		$tax				=	 	$order['totals_info']->Tax;
		$barcode  			=   	$order['assign_barcode'];
		$items				=	 	$order['items'];
		$address			=		'';
		$address			.=		($company != '') ? $company.'<br>' : '' ; 
		$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
		$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
		$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
		$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
		$address			.=		($town != '') ? $town.'<br>' : '';
		$address			.=		($resion != '') ? $resion.'<br>' : '';
		$address			.=		($postcode != '') ? $postcode.'<br>' : '';
		$address			.=		($country != '' ) ? $country.'<br>' : '';
		
		$provinces = array('AG' =>'Agrigento','GE'=>'Genoa','PN'=>'Pordenone','AL'=>'Alessandria','GO'=>'Gorizia','PZ'=>'Potenza','AN'=>'Ancona','GR'=>'Grosseto','PO'=>'Prato','AO'=>'Aosta','IM'=>'Imperia','RG'=>'Ragusa','AR'=>'Arezzo','IS'=>'Isernia','RA'=>'Ravenna','AP'=>'Ascoli Piceno','SP'=>'La Spezia','RC'=>'Reggio Calabria','AT'=>'Asti','AQ'=>'L\'Aquila','RE'=>'Reggio Emilia','AV'=>'Avellino','LT'=>'Latina','RI'=>'Rieti','BA'=>'Bari','LE'=>'Lecce','RN'=>'Rimini','BT'=>'Barletta-Andria-Trani','LC'=>'Lecco','RM'=>'Rome','BL'=>'Belluno','LI'=>'Livorno','RO'=>'Rovigo','BN'=>'Benevento','LO'=>'Lodi','SA'=>'Salerno','BG'=>'Bergamo','LU'=>'Lucca','SS'=>'Sassari','BI'=>'Biella','MC'=>'Macerata','SV'=>'Savona','BO'=>'Bologna','MN'=>'Mantua','SI'=>'Siena','BZ'=>'Bolzano','MS'=>'Massa and Carrara','SO'=>'Sondrio','BS'=>'Brescia','MT'=>'Matera','SR'=>'Syracuse','BR'=>'Brindisi','VS'=>'Medio Campidano','TA'=>'Taranto','CA'=>'Cagliari','ME'=>'Messina','TE'=>'Teramo','CL'=>'Caltanissetta','MI'=>'Milan','TR'=>'Terni','CB'=>'Campobasso','MO'=>'Modena','TP'=>'Trapani','CI'=>'Carbonia-Iglesias','MB'=>'Monza and Brianza','TN'=>'Trento','CE'=>'Caserta','NA'=>'Naples','TV'=>'Treviso','CT'=>'Catania','NO'=>'Novara','TS'=>'Trieste','CZ'=>'Catanzaro','NU'=>'Nuoro','TO'=>'Turin','CH'=>'Chieti','OG'=>'Ogliastra','UD'=>'Udine','CO'=>'Como','OT'=>'Olbia-Tempio','VA'=>'Varese','CS'=>'Cosenza','OR'=>'Oristano','VE'=>'Venice','CR'=>'Cremona','PD'=>'Padua','VB'=>'Verbano-Cusio-Ossola','KR'=>'Crotone','PA'=>'Palermo','VC'=>'Vercelli','CN'=>'Cuneo','PR'=>'Parma','VR'=>'Verona','EN'=>'Enna','PV'=>'Pavia','VV'=>'Vibo Valentia','FM'=>'Fermo','PG'=>'Perugia','VI'=>'Vicenza','FE'=>'Ferrara','PU'=>'Pesaro and Urbino','VT'=>'Viterbo','FI'=>'Florence','PE'=>'Pescara','FG'=>'Foggia','PC'=>'Piacenza','FC'=>'Forl�-Cesena','PI'=>'Pisa','FR'=>'Frosinone','PT'=>'Pistoia');
				
		$cana_provinces = array('AB' =>'Alberta','BC'=>'British Columbia','MB'=>'Manitoba','NB'=>'New Brunswick','NL'=>'Newfoundland and Labrador','NS'=>'Nova Scotia','NT'=>'Northwest Territories','NU'=>'Nunavut','ON'=>'Ontario','PE'=>'Prince Edward Island','QC'=>'Quebec','SK'=>'Saskatchewan','YT'=>'Yukon');
		
		$addData['sub_source'] 	= 	$order['sub_source'];
		$addData['company'] 	= 	$company;
		$addData['fullname'] 	= 	$fullname;
		$addData['address1'] 	= 	$address1;
		$addData['address2'] 	= 	$address2;
		$addData['address3'] 	= 	$address3;
		$addData['town'] 		= 	$town;
		if( $country == 'Italy' )
		{
			if(array_key_exists(strtoupper($resion),$provinces)){
				$addData['resion'] 		= 	$resion;
			}else {
				$addData['resion'] 		= 	array_search(ucfirst($resion), $provinces);
			}
		}
		else if( $country == 'Canada' )
		{
			if(array_key_exists(strtoupper($resion),$cana_provinces)){
				$addData['resion'] 		= 	$resion;
			}else {
				$addData['resion'] 		= 	array_search(ucfirst($resion), $cana_provinces);
			}
		}
		else {
			$addData['resion'] 		= 	$resion;
		}
		
		$addData['postcode'] 	= 	$postcode;
		$addData['country'] 	= 	$country;
				
		$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);//CostBreaker
		
		$subsource_lower = strtolower($subsource) ;
		$marecArray = array('marec_de','marec_es','marec_fr','marec_it','marec_uk','marec_nl');
		$costbreakerArray = array('costbreaker_de','costbreaker_nl','costbreaker_es','costbreaker_fr','costbreaker_it','costbreaker_uk','costbreaker','costbreaker_ca','costbreaker_usabuyer','onbuy','flubit','euraco.fyndiq','costdropper');
		$techdriveArray = array('tech_drive_de','tech_drive_es','tech_drive_fr','tech_drive_it','tech_drive_uk');				
		$rainbowArray = array('rainbow retail','rainbow retail de','rainbow_retail_es','rainbow_retail_it','rainbow_retail_fr');
		$bbdArray = array('bbd_eu_de');	
		
		if(in_array($subsource_lower,$marecArray)){
			$company 	=  'Marec';
		}else if(in_array($subsource_lower,$costbreakerArray)){
			$company 	=  'CostBreaker';
		}else if(in_array($subsource_lower,$techdriveArray)){
			$company 	= 'Tech Drive Supplies';
		}else if(in_array($subsource_lower,$rainbowArray)){
			$company 	= 'Rainbow Retail';
		}else if(in_array($subsource_lower,$bbdArray)){
			$company 	= 'BBD';
		}else if( $subsource == 'EBAY2' ){
			$company 	= 'EBAY2';
		}else if( $subsource == 'EBAY0' ){
			$company 	= 'EBAY0';
		}else if( $subsource == 'EBAY5' ){
			$company 	= 'Marec';
		}
		  
		
		if($splitOrderId == '2311053-1'){
			//echo $company  ;exit;
		}
		
		$i = 1;
		$str = '';
		$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
		$totalGoods = 0;
		$productInstructions = '';
		$ref_code = trim($getSplitOrder['MergeUpdate']['provider_ref_code']);
		$codearray = array('DP1', 'FR7');
		
		$this->loadModel( 'BulkSlip' );
		if($subsource_lower == 'costbreaker_ca'){
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => 'CostBreaker_CA' ) ) );		
		}elseif($subsource_lower == 'costbreaker_usabuyer'){
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => 'CostBreaker_USA' ) ) );		
		}else{
			$gethtml =	$this->BulkSlip->find('first', array('conditions' => array( 'BulkSlip.company' => $company ) ) );
		}
		 
		
		$p_barcode	 = '';
		$is_bulk_sku = 0;
		if( in_array($ref_code, $codearray)) 
		{
			
			$per_item_pcost 	= $postagecost/count($items);
			$j = 1;
			foreach($items as $item){
				$item_title 		= 	$item->Title;
				$quantity			=	$item->Quantity;
				$price_per_unit		=	$item->PricePerUnit;
				$str .= '<tr>
							<td style="border:1px solid #000;" align="left" valign="top" width="10%">'.$quantity.'</td>
							<td style="border:1px solid #000;" valign="top" width="20%">'.substr($item_title, 0, 30 ).'</td>
							<td style="border:1px solid #000;" valign="top" width="15%">'.$quantity * $price_per_unit.'</td>
						<td style="border:1px solid #000;" valign="top" width="15%">'.$per_item_pcost.'</td>
						<td style="border:1px solid #000;" valign="top" width="20%">'.(($price_per_unit * $quantity) + $per_item_pcost).'</td>
					</tr>';
				$j++;
			}
			 
			$str .=	'<tr><td></td><td></td><td></td><td style="border:1px solid #000;">Total</td>
						<td style="border:1px solid #000;" valign="top" width="20%">'.$totalcharge.'</td>
					</tr>';
			 
			
		} 
		else 
		{
			
			foreach( $skus as $sku )
			{
			
				$newSkus[]				=	 explode( 'XS-', $sku);
				$cat_name = array();
				$sub_can_name = array();
				foreach($newSkus as $newSku)
					{
						
						$getsku = 'S-'.$newSku[1];
						$getOrderDetail = 	$this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
						$OrderLocation 	= 	$this->OrderLocation->find( 'first', array( 'conditions' => array('order_id' => $openOrderId, 'sku' => $getsku ) )  );
						if( count($OrderLocation) > 0 ){
							$o_location 	=	$OrderLocation['OrderLocation']['bin_location'];
						} else {
							$o_location 	=	'Check Location' ;
						}
						
						$contentCat 	= 	$getOrderDetail['Product']['category_name'];
						$cat_name[] 	= 	$getOrderDetail['Product']['category_name'];
						$contentSubCat	=	$getOrderDetail['Product']['sub_category'];
						$sub_can_name[] =	$getOrderDetail['Product']['sub_category'];
						$productBarcode	=	$getOrderDetail['ProductDesc']['barcode'];
						$is_bulk_sku	=	$getOrderDetail['Product']['is_bulk_sku'];
						
						$getContent	= $this->CategoryContant->find( 'first', array( 
															'conditions'=>array('CategoryContant.sub_category'=>$contentSubCat,'CategoryContant.sub_source'=>$subsource)));
						$productBarcode			=	$getOrderDetail['ProductDesc']['barcode'];
						if(count($getContent) > 0) {
							$getbarcode				= 	$this->getBarcode( $productBarcode );
							$productInstructions 	=  '<p style="font-size:14px;">'.$getContent['CategoryContant']['content'].'</p>';
						}
						
						$product_local_barcode = $this->GlobalBarcode->getLocalBarcode($getOrderDetail['ProductDesc']['barcode']);
						
						$title	=	$getOrderDetail['Product']['product_name'];
						$totalGoods = $totalGoods + $newSku[0];
						$str .= '<tr>
								<td valign="top" class="rightborder bottomborder">'.$i.'</td>
								<td valign="top" class="center rightborder bottomborder">'.$newSku[1].'</td>
								<td valign="top" class="rightborder bottomborder">'.$title.'</td>
								<td valign="top" class="rightborder bottomborder" >'.$product_local_barcode.'</td>
								<td valign="top" class="rightborder bottomborder">'.$o_location.'</td>
								<td valign="top" class="center norightborder bottomborder">'.$newSku[0].'</td>';
						$str .= '</tr>';
						$i++;
						
					}
					unset($newSkus);
			}
			
		}
				
						
		/*$html 			=	$gethtml['PackagingSlip']['html'];
		$paperHeight    =   $gethtml['PackagingSlip']['paper_height'];
		$paperWidth  	=   $gethtml['PackagingSlip']['paper_width'];

		$barcodeHeight  =   $gethtml['PackagingSlip']['barcode_height'];
		$barcodeWidth   =   $gethtml['PackagingSlip']['barcode_width'];
		$paperMode      =   $gethtml['PackagingSlip']['paper_mode'];*/
		//
		if($splitOrderId == '1668658-1'){
			//pr($company );
		}
		$setRepArray = array();
		$setRepArray[] 					= $address1;
		$setRepArray[] 					= $address2;
		$setRepArray[] 					= $address3;
		$setRepArray[] 					= $town;
		$setRepArray[] 					= $resion;
		$setRepArray[] 					= $postcode;
		$setRepArray[] 					= $country;
		$setRepArray[] 					= $phone;
		$setRepArray[] 					= $ordernumber;
		$setRepArray[] 					= $courier;
		$setRepArray[] 					= $recivedate[0];
		$totalitem = $i - 1;
		$setRepArray[]	=	 $str;
		$setRepArray[]	=	 $totalGoods;
		$setRepArray[]	=	 $subtotal;
		$Path 			= 	'/wms/img/client/';
		$img			=	 '';
		$setRepArray[]	=	 $img;
		$setRepArray[]	=	 $postagecost;
		$setRepArray[]	=	 $tax;
		$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
		//$setRepArray[]	=	 $totalamount;
		$setRepArray[]	=	 $itemPrice;
		
		$address		= $obj->getCountryAddress( $addData  );
		
		$setRepArray[]	=	 $address;
		$barcodePath  	=  WWW_ROOT.'img/orders/barcode/';
		$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.' width='. $barcodeWidth .'>';
		$barcodenum		=	explode('.', $barcode);
		
		$setRepArray[] 	=  $barcodeimg;
		$setRepArray[] 	=  $paymentmethod;
		$setRepArray[] 	=  $barcodenum[0];
		$setRepArray[] 	=  '';
		$img 			=	$gethtml['PackagingSlip']['image_name'];
		$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
		$setRepArray[] 	=  $returnaddress;
		$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
		$setRepArray[] 	=  $splitOrderId;
		$setRepArray[] 	= 	utf8_decode( $productInstructions );
		//$prodBarcodePath  	=  Router::url('/', true).'img/product/barcodes/';			
		$prodBarcodePath  	= WWW_ROOT.'img/product/barcodes/';
		$setRepArray[] 	=  '<img src='.$prodBarcodePath.$productBarcode.'.png width='. $barcodeWidth .'>';
		$setRepArray[] 	=  $totalamount;
		$setRepArray[]	=	WWW_ROOT;
		/*--------------Ink------------------*/
		$this->loadMOdel( 'InkOrderLocation' );	 
		$order_ink	    = $this->InkOrderLocation->find('first', array('conditions' => array('order_id' => $openOrderId,'split_order_id'=>$openOrderId.'-1','status'=>'active')));		
		if(count($order_ink) > 0 ){			 
			$setRepArray[] 	=  '<div style="color:green;font-size:14px;"><center>#2 Envelope Required.</center></div><br><img src="'.WWW_ROOT.'img/ink_xl2.jpg" height="130">';
		}
		/*--------------End of Ink------------------*/
		$imgPath = WWW_ROOT .'css/';
		$html2 			= 	$gethtml['BulkSlip']['html'];
		//pr($gethtml);
		//$html2 			= 	'';
		if($splitOrderId == '1668658-1'){
			 //echo $html2;exit;
		}
		if( $company ==  'Marec' && in_array(  'Mobile Accessories', $cat_name ))
		{
			//$this->MobileAsseInstruction( $splitOrderId, $subsource  );
		}
		
		
		$get_country = array('marec_fr'=>'FR','marec_de'=>'DE','marec_it'=>'IT','marec_es'=>'ES','marec_uk'=>'UK');
		$source_country  = $get_country[$subsource_lower];
  			
		if( $company ==  'Marec' && $is_bulk_sku == 1)
		{	
			//$html2 	.= $this->BulkSkuNote($source_country);			 
		}
		
		$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
		$html 		= 	$obj->setReplaceValue( $setRepArray, $html2 );
		return $html;
		
	}
	
	public function getAddress()
	{
	
		$this->loadModel('MergeUpdate'); 
		$this->loadModel('OpenOrder');
			
		$orders	=	$this->MergeUpdate->find('all', array('conditions'=> array('order_id'=>['3021618','2821505','3023714']),'fields'=>array('sku','product_order_id_identify','order_id','jerseypost_barcode','country_code','provider_ref_code','source_coming','delevery_country') ) );
 		
		//echo count($orders);
 		//echo '<br>';
 		unlink(WWW_ROOT.'logs/adr10.csv');
 
		foreach($orders	 as $v){
		
			$provider_ref_code =  $v['MergeUpdate']['provider_ref_code'];
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $v['MergeUpdate']['order_id'] ) ) );
			$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
			// pr($customer_info);
			 $sep = "\t";
			 $content = $customer_info->Address->Address1.$sep.
			 $customer_info->Address->Address2.$sep.
			 $customer_info->Address->Address3.$sep.
			 $customer_info->Address->Town.$sep.
			 $customer_info->Address->Region.$sep.
			 $customer_info->Address->Country;
			 
			 $addressArray['sub_source'] =  $v['MergeUpdate']['source_coming'];
			 $addressArray['country'] = $v['MergeUpdate']['delevery_country'];
			 $addressArray['address1'] = $customer_info->Address->Address1;
			 $addressArray['address2'] = $customer_info->Address->Address2;
			 $addressArray['address3'] = $customer_info->Address->Address3;
			 $addressArray['company'] = $customer_info->Address->Company;
			  
			 
			//echo $v['MergeUpdate']['order_id'] .' ==   ';
			 $adr = $this->getCountryAddress( $addressArray );
			//echo "<br>";
			//echo $content ;
			$content .= $sep.$adr;
			//echo "<br>";
			
		//$content = $v['CustomerOrder']['order_id'].$sep. $v['CustomerOrder']['sku'].$sep. $v['CustomerOrder']['barcode'].$sep. $v['CustomerOrder']['quantity'].$sep. $v['CustomerOrder']['unit_price'].$sep.$v['CustomerOrder']['status'].$sep.$v['CustomerOrder']['created_date'].$sep.$created_by .$sep.$customer_name;
 			
 			file_put_contents(WWW_ROOT.'logs/adr.csv',$content."\r\n", FILE_APPEND | LOCK_EX);	
			 

			
		}
		
		
		//$addressArray
	}
	
	public function getCountryAddress( $addressArray )
	{
			
			switch ($addressArray['country']) 
			{
			case "United States":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? ucwords(strtolower($addressArray['address1']))."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords(strtolower($addressArray['address2']))."\n" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords(strtolower($addressArray['address3']))."\n" : '' ; 
					return $address;
					break;
			case "Canada":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1'])."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3']) : '' ;
					return $address;
					break;
			case "Germany":
			
					if(strtolower($addressArray['sub_source']) == 'costdropper'){
  					
						$address			 =		'';
						if($addressArray['address1'] != '' && $addressArray['address2'] != ''){
							$address			.=		ucwords($addressArray['address2']).' ' . ucwords($addressArray['address1']) ; 
						}
 						if($addressArray['address1'] == '' && $addressArray['address2'] != ''){						
							$address			.=		ucwords($addressArray['address2']) ; 					
						}
						if($addressArray['address1'] != '' && $addressArray['address2'] == ''){						
							$address			.=		ucwords($addressArray['address1']) ; 					
						}
						
					}else{
							$address			 =		'';
							$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1'])."\n" : '' ; 
							
							if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
								$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
								$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;						
							}
							
							if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
								$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ;
							}
							
							if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
								$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;
							}
 						}
					return $address;
					break;
				case "Spain":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '')   ? ucwords($addressArray['company'])."\n" : '' ; 
					$address			.=		($addressArray['address1'] != '' ) ? ucwords($addressArray['address1'])."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ; 
					$address			.=		($addressArray['address3'] != '' ) ? ','.ucwords($addressArray['address3'])."\n" : '' ;
					return $address;
					break;
			
			case "Italy":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? strtoupper($addressArray['address1'])."\n" : '' ; 
					
					 if($addressArray['address2'] != '' && $addressArray['address3'] != ''){
						$address			.=		($addressArray['address2'] != '' )   ? strtoupper($addressArray['address2'])."\n" : '';
						$address			.=		($addressArray['address3'] != '' )   ? strtoupper($addressArray['address3'])."\n" : '';
					}
					else if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
 						$address			.=		($addressArray['address2'] != '' )   ? strtoupper($addressArray['address2'])."\n" : '';
 					}
					else if($addressArray['address2'] == '' && $addressArray['address3'] != ''){
 						$address			.=		($addressArray['address3'] != '' )   ? strtoupper($addressArray['address3'])."\n" : '';
 					}
					return $address;
					break;
			case "France":
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' ) ? strtoupper($addressArray['address1'])."\n" : '' ; 
					
					if($addressArray['address2'] != '' && $addressArray['address3'] != ''){						
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2']).' ' : '' ; 
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;						
					}
					
					if($addressArray['address2'] != '' && $addressArray['address3'] == ''){
						$address			.=		($addressArray['address2'] != '' ) ? ucwords($addressArray['address2'])."\n" : '' ;
					}
					
					if($addressArray['address2'] == '' && $addressArray['address3'] != '')					{
						$address			.=		($addressArray['address3'] != '' ) ? ucwords($addressArray['address3'])."\n" : '' ;
					}
					
					return $address;
					break;
			case "Denmark":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? $addressArray['company']."\n" : '' ;  
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1']."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3']."\n" : '' ; 
					return $address;
					break;
			case "Austria":
					$address			 =		'';
					$address			.=		($addressArray['company'] != '') 	? $addressArray['company']."\n" : '' ; 
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1']."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3']."\n" : '' ; 
					return $address;
					break;
			default:
					$address			 =		'';
					$address			.=		($addressArray['address1'] != '' )	? $addressArray['address1']."\n" : '' ; 
					$address			.=		($addressArray['address2'] != '' ) 	? $addressArray['address2'].',' : '' ; 
					$address			.=		($addressArray['address3'] != '' ) 	? $addressArray['address3']."\n" : '' ;
					return $address;
					break;
			}
			exit;
		}
 
	
	public function getSenderInfo($data  = [], $store_country = '')
	{ 
 		$country = $data['country'] ;
		if($store_country != ''){
			$country = $store_country;
		}
 		$sub_source = $data['sub_source'];
		$FromCountryCode = 'JE';
  		if(strpos($sub_source,'marec')!== false){
			$FromCompany = 'ESL LIMITED';
			$FromPersonName = 'ESL LIMITED';
			/*-------Updated on 12-02-2020-----*/
			$Building = 'Unit 4 Airport Cargo Centre';
			$FromAddress = 'L\'avenue De La Commune';
			$FromCity = 'St Peter';
			$FromDivision = 'JE';
			$FromPostCode = 'JE3 7BY'; 
			$FromCountryName = 'JERSEY';
			$FromPhoneNumber = '+443301170238';
			$email = 'accounts@esljersey.com'; 
			
			$VAT_Number = 'GB 307817693';  
 			if(in_array($country ,['United Kingdom','GB','UK'])){  
				$VAT_Number   = 'GB 307817693'; 
    		}else if(in_array($country ,['Germany','DE'])){  
 				$VAT_Number   = 'DE 323086773'; 
			}else if(in_array($country ,[ 'France','FR'])){  
				$VAT_Number   = 'FR 07879900934';  
			}else if(in_array($country ,['Italy','IT'])){  
				$VAT_Number   = 'IT 11062310963'; 
			} 
		 }	
		else if(strpos($sub_source,'rainbow')!== false){
			$FromCompany = 'FRESHER BUSINESS LIMITED';
			$FromPersonName = 'C/O ProFulfillment and Logistics';
			/*-------Updated on 12-02-2020-----*/
			$Building = 'Unit 4 Airport Cargo Centre';
			$FromAddress = 'L\'avenue De La Commune';
			$FromCity = 'St Peter';
			$FromDivision = 'JE';
			$FromPostCode = 'JE3 7BY';			
			$FromCountryName = 'JERSEY';
			$FromPhoneNumber = '0123456789';
			$email = 'accounts@fresherbusiness.co.uk'; 
			
			$VAT_Number = 'GB 318649182';  
 			if(in_array($country ,['United Kingdom','GB','UK'])){    
				$VAT_Number   = 'GB 318649182'; 
    		}else if(in_array($country ,['Germany','DE'])){ 
 				$VAT_Number   = 'DE 327173988'; 
			}else if(in_array($country ,['France','FR'])){ 
				$VAT_Number   = 'FR 48880490255';  
			}else if(in_array($country ,['Italy','IT'])){ 
				$VAT_Number   = 'IT 11061000961'; 
			} 
		}else{
			$FromCompany = 'EURACO GROUP LTD';
			/*-------Updated on 12-02-2020-----*/
			$FromPersonName = 'C/O ProFulfillment and Logistics';
			$Building = 'Unit 4 Airport Cargo Centre';
			$FromAddress = 'L\'avenue De La Commune';
			$FromCity = 'St Peter';
			$FromDivision = 'JE';
			$FromPostCode = 'JE3 7BY'; 
			$FromCountryName = 'JERSEY';
			$FromPhoneNumber = '+44 3301170104';
			$email = 'accounts@euracogroup.co.uk';
			 
 			$VAT_Number = 'GB 304984295';  
 			if(in_array($country ,['United Kingdom','GB','UK'])){  
				$VAT_Number   = 'GB 304984295'; 
    		}else if(in_array($country ,['Germany','DE'])){
 				$VAT_Number   = 'DE 321777974'; 
			}else if(in_array($country ,['France','FR'])){
				$VAT_Number   = 'FR 59850070509'; 
			}else if(in_array($country ,['Spain','ES'])){
				$VAT_Number   = 'ES N6061518D'; 
			}else if(in_array($country ,['Italy','IT'])){
				$VAT_Number   = 'IT 10905930961'; 
			}	
		}
		
		$senderData['email']	  	   =    $email;
		$senderData['FromCompany']	   =    $FromCompany;
		$senderData['FromPersonName']  =	$FromPersonName;
		$senderData['Building']	   	   =	$Building ;
		$senderData['FromAddress']	   =	$FromAddress ; 
		$senderData['FromCity'] 	   =	$FromCity;
		$senderData['FromDivision']    =	$FromDivision;
		$senderData['FromPostCode']    =	$FromPostCode;
		$senderData['FromCountryCode'] =	$FromCountryCode;
		$senderData['FromCountryName'] =	$FromCountryName;
		$senderData['FromPhoneNumber'] =	$FromPhoneNumber;
		$senderData['vat_number'] 	   =	$VAT_Number;

 		return $senderData;	
	}
	
 	public function getBarcodeOutside($spilt_order_id  = null)
	{ 
		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'AssignService' );
		$this->loadModel( 'MergeUpdate' );
		
		//$uploadUrl = $this->getUrlBase();
		$imgPath = WWW_ROOT .'img/orders/barcode/';   
		
		
		// $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.status' => '0')));
		
		if($spilt_order_id != ''){
		 $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.product_order_id_identify' => $spilt_order_id)));
		}else{
		 $allSplitOrders	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.status' => '0')));
		}
		
		
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
		require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
		//$font = new BCGFontFile(APP .'Vendor/barcodegen/font/Arial.ttf', 13);
		$colorFront = new BCGColor(0, 0, 0);
		$colorBack = new BCGColor(255, 255, 255);
		
		if( count($allSplitOrders) > 0 )	
		{
		  foreach( $allSplitOrders as $allSplitOrder )
		  {					  
			  $id 			= 		$allSplitOrder['MergeUpdate']['id'];
			  $openorderid	=	 	$allSplitOrder['MergeUpdate']['product_order_id_identify'];
			  $barcodeimage	=	$openorderid.'.png';
			  
				$orderbarcode=$openorderid;
				$code128 = new BCGcode128();
				$code128->setScale(2);
				$code128->setThickness(20);
				$code128->setForegroundColor($colorFront);
				$code128->setBackgroundColor($colorBack);
				$code128->setLabel(false);
				$code128->parse($orderbarcode);
									
				//Drawing Part
				$imgOrder128=$orderbarcode.".png";
				$imgOrder128path=$imgPath.'/'.$orderbarcode.".png";
				$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
				$drawing128->setBarcode($code128);
				$drawing128->draw();
				$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);
			  
			  if( $allSplitOrder['MergeUpdate']['product_order_id_identify'] != "" )
			  {
				  //$content = file_get_contents($uploadUrl.$openorderid);
				  //file_put_contents($imgPath.$barcodeimage, $content);
				  
				  $data['MergeUpdate']['id'] 	=  $id;
				  $data['MergeUpdate']['order_barcode_image'] 	=  $barcodeimage;
				  $this->MergeUpdate->save($data);
			  }
		  }
		}
		
		
	}
			 
	 public function updateManifestDate($merge_id)
	   {
		  $this->loadModel('MergeUpdate'); 
		  date_default_timezone_set('Europe/Jersey');
		  $firstName = ( $this->Session->read('Auth.User.first_name') != '' ) ? $this->Session->read('Auth.User.first_name') : '_';
		  $lastName = ( $this->Session->read('Auth.User.last_name') != '' ) ? $this->Session->read('Auth.User.last_name') : '_';
		  $manifest_username = $firstName.' '.$lastName;
		  $data['id'] = $merge_id;   
		  $data['manifest_date'] = date('Y-m-d H:i:s');   
		  $data['manifest_username'] 	= $manifest_username;   
		  $this->MergeUpdate->saveAll( $data);	    	
	   }
	   
     public function addressInfo($order_id)
	   {
			$this->loadModel('OpenOrder'); 
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id ) ) );
			$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
			$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
			pr($customer_info);
			
			
			preg_match_all('/([\d]+)/', $customer_info->Address->Address1, $match);

    		//pr($match);
			echo $match[0][0];
  			echo $int = (int) filter_var($customer_info->Address->Address1, FILTER_SANITIZE_NUMBER_INT);
			echo ' = '.str_replace($int,' ', $customer_info->Address->Address1);
			
			echo ' <br> ';
			echo $int = (int) filter_var($customer_info->Address->PhoneNumber, FILTER_SANITIZE_NUMBER_INT);
			preg_match_all('/([\d]+)/', $customer_info->Address->PhoneNumber, $telmatch);
 			echo '<br>';
    		// pr($match);
			 $tel ='';
			 for($i = 0; $i< count($telmatch[0]); $i++){
				$tel .= $telmatch[0][$i];
 			 }
  			 $country_pcode = '+';
			 if($customer_info->Address->Country == 'France'){
				 $country_pcode = '+33';
			 }else if($customer_info->Address->Country == 'Germany'){
				 $country_pcode = '+49';
			 }else if($customer_info->Address->Country == 'Germany'){
				 $country_pcode = '+49';
			 }
			 echo $country_pcode.substr($tel,-10);
			exit;
	}
	public function whistlpdf(){
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF();
		//$dompdf->set_paper('A4', 'portrait'); 
		$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');
	 	$html = '<div style="position:absolute;left:50%;margin-left:-297px;top:0px;width:595px;height:842px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">logo</div>
<div style="position:absolute;left:56.28px;top:92.40px" class="cls_002"><span class="cls_002">Customer Identification Card</span></div>
<div style="position:absolute;left:202.08px;top:133.32px" class="cls_005"><span class="cls_005">Ì01PÇ*ÆI+-ÈZZFÎ</span></div>
<div style="position:absolute;left:219.12px;top:172.68px" class="cls_006"><span class="cls_006">01P1098411113ZZ</span></div>
<div style="position:absolute;left:56.28px;top:194.40px" class="cls_002"><span class="cls_002">Customer Name:</span></div>
<div style="position:absolute;left:58.92px;top:228.24px" class="cls_004"><span class="cls_004">EURACO GROUP LIMITED</span></div>
<div style="position:absolute;left:56.28px;top:288.72px" class="cls_002"><span class="cls_002">Customer Number e.g "P0001111" or "L0001111" or "N0001111"</span></div>
<div style="position:absolute;left:58.92px;top:312.24px" class="cls_004"><span class="cls_004">P1098411</span></div>
<div style="position:absolute;left:56.28px;top:359.04px" class="cls_002"><span class="cls_002">Service Type:</span></div>
<div style="position:absolute;left:57.72px;top:387.36px" class="cls_003"><span class="cls_003">1 - AllSort</span></div>
<div style="position:absolute;left:56.28px;top:430.56px" class="cls_002"><span class="cls_002">Mailing Service</span></div>
<div style="position:absolute;left:57.72px;top:459.96px" class="cls_003"><span class="cls_003">3 - UK Bound Packet</span></div>
<div style="position:absolute;left:56.28px;top:502.32px" class="cls_002"><span class="cls_002">Indicia Printing:</span></div>
<div style="position:absolute;left:292.80px;top:502.32px" class="cls_002"><span class="cls_002">Return Address Printing:</span></div>
<div style="position:absolute;left:56.52px;top:536.28px" class="cls_007"><span class="cls_007">1 - Print an indicia</span></div>
<div style="position:absolute;left:293.04px;top:536.28px" class="cls_007"><span class="cls_007">1 - Print a return address</span></div>
<div style="position:absolute;left:56.28px;top:572.64px" class="cls_002"><span class="cls_002">Return Address to be printed:</span></div>
<div style="position:absolute;left:292.80px;top:572.64px" class="cls_002"><span class="cls_002">Collection Date ( DD/MM/YY):</span></div>
<div style="position:absolute;left:53.88px;top:791.76px" class="cls_008"><span class="cls_008">OP.032</span></div>
<div style="position:absolute;left:284.16px;top:791.76px" class="cls_008"><span class="cls_008">Page 1 of 1</span></div>
<div style="position:absolute;left:53.88px;top:798.36px" class="cls_008"><span class="cls_008">V.6</span></div>
<div style="position:absolute;left:232.44px;top:798.36px" class="cls_008"><span class="cls_008">IF PRINTED THIS PAGE IS FOR ONE TIME USE ONLY</span></div>
</div>';
			
		$cssPath = WWW_ROOT .'css/';
		$html .= '<style>'.file_get_contents($cssPath.'pdfstylewish.css').'</style>';					
		echo $html;
		$dompdf->load_html($html, Configure::read('App.encoding'));
		$dompdf->render();
		$imgPath = WWW_ROOT .'logs/'; 
		$name	=	'Label_ravu_12_10.pdf';
		unlink($imgPath.$name); 
		file_put_contents($imgPath.$name, $dompdf->output()); 
		die;
	}




	public function pdfToImg($split_ord_id = '3283949-1'){
  		
		$labelPdfPath = WWW_ROOT .'jerseypost/labels/Label_'.$split_ord_id.'.pdf';  
		$imagick = new Imagick();
		$imagick->setResolution(250, 250);
		// Reads image from PDF
		$imagick->readImage($labelPdfPath);
		// Merges a sequence of images
		$imagick = $imagick->flattenImages(); 
		// Writes an image
		$imagick->writeImages(WWW_ROOT .'jerseypost/labels/'.$split_ord_id.'.jpg',false);
		//----------------Rotate image----------------------
		$filename = WWW_ROOT.'jerseypost/labels/'.$split_ord_id.'.jpg';
		$degrees = 270;  		
		$source = imagecreatefromjpeg($filename);		
		$rotate = imagerotate($source, $degrees, 0);		
		$r_label = WWW_ROOT .'jerseypost/labels/rotate_'.$split_ord_id.'.jpg';
 		imagejpeg($rotate,$r_label);
		// Free the memory
		imagedestroy($source);
		imagedestroy($rotate); 
		
		return 2;
 	}

}