<?php
//error_reporting(0);
class ClearLogsController extends AppController
{    
    var $name = "ClearLogs";	
    var $components = array('Session','Common','Auth','Paginator');    
    var $helpers = array('Html','Form','Common','Session','Paginator');
	
	public function beforeFilter()
    {
		parent::beforeFilter();
		$this->layout = false;
		$this->Auth->Allow(array('index'));
		//date_default_timezone_set('Asia/Kolkata');
    }
	
  	public function index(){	
			 
		$this->layout = "";
		$this->autoRender = false;
		$this->loadModel( 'Royalmail' );	
		$this->loadModel( 'RoyalmailManifest' );	
		
		$log_file = "clear_log_".date('dmy').".log";
		
		$ids  = array();	
		$manifest_table_id	= array();
		$date = date("Y-m-d H:i:s",strtotime(" -60 days"));
		$rmail = $this->Royalmail->find('all', array('conditions' => array('added_date < ' => $date ),'order'=>'added_date ASC'));
		if(count($rmail) > 0){
			foreach($rmail as $val){
				$ids[] = $val['Royalmail']['split_order_id'];
				$manifest_table_id[] = $val['Royalmail']['manifest_table_id'];
			}
			
			if(count($ids) > 0){
				$condition = array('split_order_id IN' => $ids);			
				$this->Royalmail->deleteAll($condition,false); 
				file_put_contents(WWW_ROOT."logs/".$log_file, print_r($ids,true), FILE_APPEND | LOCK_EX);		
			}	
			
			if(count($manifest_table_id) > 0){
				$condition = array('id IN' => $manifest_table_id);			
				$this->RoyalmailManifest->deleteAll($condition,false); 
				file_put_contents(WWW_ROOT."logs/".$log_file,print_r($manifest_table_id,true), FILE_APPEND | LOCK_EX);		
			}	
		}
		
		$barcode = NULL;
		$path = WWW_ROOT."img/orders/barcode"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 20000000; //231 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$barcode .=  $file."\n";
					}						
					 
				}
			}
		}	
			 
		if($barcode){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---orders barcode---\n".$barcode , FILE_APPEND | LOCK_EX);		
		} 
		/*--------correos_barcode---------*/
		$correos_barcode = NULL;
		$path = WWW_ROOT."img/orders/correos_barcode"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 15000000; //173 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$correos_barcode .=  $file."\n";
					}						
					 
				}
			}
		}
		
		if($correos_barcode){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---correos_barcode---\n".$correos_barcode , FILE_APPEND | LOCK_EX);		
		}	
		/*--------End correos_barcode---------*/
		
		/*--------correos_barcode---------*/
		$packaging_label = NULL;
		$path = WWW_ROOT."img/printPDF"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 1000000; //11 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$packaging_label .=  $file."\n";
					}						
					 
				}
			}
		}
		
		if($packaging_label){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---packaging_label---\n".$packaging_label , FILE_APPEND | LOCK_EX);		
		}	
		/*--------End correos_barcode---------*/
		
		
		/*--------logs---------*/
		$packaging_label = NULL;
		$path = WWW_ROOT."logs"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 1750000; //20 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$packaging_label .=  $file."\n";
					}						
					 
				}
			}
		}
		
		if($packaging_label){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---logs---\n".$packaging_label , FILE_APPEND | LOCK_EX);		
		}	
		/*--------End logs---------*/
		
		/*--------logs mapping---------*/
		$mapping = NULL;
		$path = WWW_ROOT."logs/mapping"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 1750000; //20 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$mapping .=  $file."\n";
					}						
					 
				}
			}
		}
		
		if($mapping){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---logs mapping---\n".$mapping , FILE_APPEND | LOCK_EX);		
		}	
		/*--------End mapping---------*/
		
		/*--------  dhl---------*/
		$dhl = NULL;
		$path = $_SERVER['DOCUMENT_ROOT']."/dhl"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 930000; //20 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$dhl .=  $file."\n";
					}						
					 
				}
			}
		}
		
		if($dhl){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---dhl---\n".$dhl , FILE_APPEND | LOCK_EX);		
		}	
		/*--------Enddhl---------*/
		
		
		$barcode = NULL;
		$path = WWW_ROOT."fba_shipments/barcode"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 930000; //10 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$barcode .=  $file."\n";
					}						
					 
				}
			}
		}
		
		if($barcode){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---FBA barcode---\n".$barcode , FILE_APPEND | LOCK_EX);		
		}
		
		$box_barcode = NULL;
		$path = WWW_ROOT."fba_shipments/box_barcode"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 930000; //10 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$box_barcode .=  $file."\n";
					}						
					 
				}
			}
		}
		
		if($box_barcode){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---box_barcode---\n".$box_barcode , FILE_APPEND | LOCK_EX);		
		}
		
		$box_details = NULL;
		$path = WWW_ROOT."fba_shipments/box_details"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 930000; //10 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$box_details .=  $file."\n";
					}						
					 
				}
			}
		}
		
		if($box_details){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---box_details---\n".$box_details , FILE_APPEND | LOCK_EX);		
		}
		
		$box_labels = NULL;
		$path = WWW_ROOT."fba_shipments/box_labels"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 930000; //10 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$box_labels .=  $file."\n";
					}						
					 
				}
			}
		}
		
		if($box_labels){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---box_labels---\n".$box_labels , FILE_APPEND | LOCK_EX);		
		}
		
		$labels = NULL;
		$path = WWW_ROOT."fba_shipments/labels"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 930000; //10 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$labels .=  $file."\n";
					}						
					 
				}
			}
		}
		
		if($labels){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---FBA labels---\n".$labels , FILE_APPEND | LOCK_EX);		
		}
		
		$slips = NULL;
		$path = WWW_ROOT."fba_shipments/slips"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 930000; //10 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$slips .=  $file."\n";
					}						
					 
				}
			}
		}
		
		if($slips){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---FBA slips---\n".$slips , FILE_APPEND | LOCK_EX);		
		}
		
		$response = NULL;
		$path = WWW_ROOT."royalmail/response"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 930000; //10 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {
					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$response .=  $file."\n";
					}						
					 
				}
			}
		}
		
		if($response){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---Royalmail response---\n".$response , FILE_APPEND | LOCK_EX);		
		}
		
		$barcode = NULL;
		$path = WWW_ROOT."royalmail/barcode"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 1750000; //20 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$barcode .=  $file."\n";
					}					
					 
				}
			}
		}
		
		if($barcode){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---Royalmail barcode---\n".$barcode , FILE_APPEND | LOCK_EX);		
		}
 		/*--------------------Remove Old Picklists-------------------------*/
		$picklist = NULL;
		$path = WWW_ROOT."img/printPickList"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 605000; //7 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$picklist .=  $file."\n";
					}					
					 
				}
			}
		}
		
		if($picklist){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---print PickList---\n".$picklist , FILE_APPEND | LOCK_EX);		
		}
		
		/*--------------------Remove Picklist Backup-------------------------*/
		$Picklist_BK = NULL;
		$path = WWW_ROOT."img/Picklist_BK"; 
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				$timer = 1000000; //11 days
				$file_path = $path.DS.$file;
				$count = time() - filectime($file_path);
				if($count > $timer) {					
					if($file !='..' && file_exists($file_path)){
						@unlink($file_path);
						$Picklist_BK .=  $file."\n";
					}					
					 
				}
			}
		}
		
		if($Picklist_BK){
			file_put_contents(WWW_ROOT."logs/".$log_file, "\n---Picklist_BK---\n".$Picklist_BK , FILE_APPEND | LOCK_EX);		
		}
		
				
	}   
    
}

?>
