<?php
ini_set( 'memory_limit' , '2048M' );
error_reporting(1);
class OrderUpdateController extends AppController
{
    
    var $name = "OrderUpdate";
    var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
   
 	public function beforeFilter()
	{
			parent::beforeFilter();
			$this->layout = false;
			$this->Auth->Allow(array('updateIoss','updateBuyerVat'));
			$this->GlobalBarcode = $this->Components->load('Common');
	}
	
	public function index()
    {
		$this->layout = "index";
		 
	}
  
	public function updateIoss()
	{
				
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('OpenOrder');
		
		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
	
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
 		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

		$token = $auth->Token;	
		$server = $auth->Server;
		
		$page_file = WWW_ROOT .'logs-ord/ioss_'.date('Ymd').".log";	
		$page = 1;
		if(file_exists($page_file)){
			$page = $page + 1;
		} 
		$order_date = date('Y-m-d',strtotime("-1 day"));		
		$orders = $this->OpenOrder->find( 'all', array( 'conditions' => array( 'destination !=' =>'United Kingdom','open_order_date >=' =>$order_date,'extended_properties IS NULL'),'fields' => array('order_id','num_order_id','id'),'limit'=> 50,'page'=> $page ) ); 
		//pr($orders);
		if(count( $orders ) > 0 )
		{ 	
 			foreach($orders as $v){
				$data = [];
 				$result = OrdersMethods::GetOrderById($v['OpenOrder']['order_id'],$token, $server);
				//pr($result);
				if(isset($result->Notes) && count($result->Notes) > 0){
 					$data['notes']			= "'".json_encode($result->Notes)."'";
				}
				if(isset($result->ExtendedProperties) && count($result->ExtendedProperties) > 0){
 					$data['extended_properties'] = "'".json_encode($result->ExtendedProperties)."'";
					foreach($result->ExtendedProperties as $p){
						if($p->Name == 'MARKETPLACE_IOSS'){
							$data['ioss_no'] = "'".$p->Value."'";
						}
					}
				}
				pr($data);
				if(count($data) > 0){
					$this->OpenOrder->updateAll( $data, ['order_id'=>$v['OpenOrder']['order_id']]);
				}
 			}
 			file_put_contents($page_file,$page);
  		}				 
		else{
			echo 'no order';
			@unlink($page_file);
		}
		//$this->updateBuyerVat();
		$this->addressInfo();
 		exit;
	}
	
	public function updateBuyerVat()
	{
				
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('OpenOrder');
		$this->loadModel('B2bCustomer');
		
		App::import('Vendor', 'Linnworks/src/php/Auth');
		App::import('Vendor', 'Linnworks/src/php/Factory');
		App::import('Vendor', 'Linnworks/src/php/Orders');
	
		$username = Configure::read('linnwork_api_username');
		$password = Configure::read('linnwork_api_password');
		
		$token = Configure::read('access_new_token');
		$applicationId = Configure::read('application_id');
		$applicationSecret = Configure::read('application_secret');
		
 		$auth = AuthMethods::AuthorizeByApplication($applicationId,$applicationSecret,$token);	

		$token = $auth->Token;	
		$server = $auth->Server;
		
		$page_file = WWW_ROOT .'logs-ord/vat_'.date('Ymd').".log";	
		$page = 1;
		if(file_exists($page_file)){
			$page = $page + 1;
		} 
		$order_date = date('Y-m-d',strtotime("-1 day"));		 
		
		//'amazon_order_id'=>['028-4141139-5337947','026-8279996-8019524','171-2956595-2451561','305-5099405-7679532']
		$orders = $this->OpenOrder->find( 'all', array( 'conditions' => array('open_order_date >=' =>$order_date,'buyer_vat_number IS NULL'),
		'fields' => array('order_id','num_order_id','amazon_order_id','general_info'),'limit'=> 10,'page'=> $page ) ); 
		pr($orders);
		if(count( $orders ) > 0 )
		{ 	
 			foreach($orders as $v){
				$data = [];
				$general_info = unserialize($v['OpenOrder']['general_info']);
			 	$location = $general_info->Location;
   				$result = OrdersMethods::GetOrder($v['OpenOrder']['order_id'],$location,true,true,$token, $server);
  				//$result = OrdersMethods::GetOrderById($v['OpenOrder']['order_id'],$token, $server);
				//pr($result);
				if(isset($result->TaxInfo) && count($result->TaxInfo) > 0){
					if(isset($result->TaxInfo->TaxNumber) && $result->TaxInfo->TaxNumber != ''){
						$data['buyer_vat_number'] = "'". $result->TaxInfo->TaxNumber."'";
						$data['notes'] = 1;
  						$res = $this->B2bCustomer->find('count', array('conditions' => array('amazon_order_id' => $v['OpenOrder']['amazon_order_id'] ) ) );
						if($res == 0){ 
							$savedata['amazon_order_id']   =  $v['OpenOrder']['amazon_order_id'] ;	
							$savedata['uploaded_date']     =  date('Y-m-d H:i:s');	 
							$savedata['upload_username']   =  $this->Session->read('Auth.User.username') ;	
							$this->B2bCustomer->saveAll( $savedata ); 
							file_put_contents(WWW_ROOT .'logs-ord/b2b_customers_'.date('ymd').'.log' , $v['OpenOrder']['amazon_order_id']."\n",  FILE_APPEND|LOCK_EX);
							$msg = 'b2b order added.';	
						}
 					}
 				}
				echo $v['OpenOrder']['amazon_order_id'].'<br>';
				 
				if(count($data) > 0){
					$this->OpenOrder->updateAll( $data, ['order_id'=>$v['OpenOrder']['order_id']]);
				}
 			}
 			file_put_contents($page_file,$page);
  		}				 
		else{
			echo 'no order';
			@unlink($page_file);
		}
 		return 1;
	}
	
	public function updateDim()
    {
		$this->loadModel('OpenOrder'); 
		$this->loadModel('MergeUpdate'); 
 		$this->loadModel('Product'); 
		$this->loadModel('Country'); 
		$this->loadModel('PostalServiceDesc'); 
 		
 		$log__file = WWW_ROOT .'logs/updateDim_'.date('dmY').".log";	
		$date = date('Y-m-d',strtotime("-1 day"));
 		 
		$sorders = $this->MergeUpdate->find( 'all', array( 'conditions' => array('merge_order_date >=' =>$date,'status' => '0','delevery_country' =>'null'),'fields'=>['order_id','product_order_id_identify'] ) ); 
		  		 
		if(count($sorders) > 0){
 			 foreach($sorders as $ord){
				$v = $this->OpenOrder->find( 'first', array( 'conditions' => array('num_order_id' =>$ord['MergeUpdate']['order_id'])));  
				$items = unserialize( $v['OpenOrder']['items']);
				$customer_info = unserialize( $v['OpenOrder']['customer_info']);
				$totals_info = unserialize( $v['OpenOrder']['totals_info']);
				$general_info = unserialize( $v['OpenOrder']['general_info']);
				$shipping_info = unserialize( $v['OpenOrder']['shipping_info']);
				//echo $openOrder['OpenOrder']['sub_source'];
				
				//pr($customer_info );pr($shipping_info );
				//exit;
				$mg = []; 				
  				$sku = [] ;$quantity = [];	$price = []; $barcode = [];	
				$length = [] ; $width = [] ; $height = [] ; $weight = [] ; $sqty = [];
							
				foreach($items  as $item){
				
					$_sku = [] ; $_qty = []; $_barcode = [];	
					$_length = [] ; $_width = [] ; $_height = [] ; $_weight = [] ; 
				
					if(substr($item->SKU,0,1) == 'B'){
						$sk = explode("-",$item->SKU);
						$ind = count($sk) - 1;
						$qty       = $sk[$ind];	
						if(count($sk) > 3){
							$qty = 1;
						}
 						for($i = 1; $i < $ind  ; $i++){
							$sku = 'S-'.$sk[$i];
							$productDetail	=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
							$_barcode[] = $productDetail['ProductDesc']['barcode'];
							$_length[] 	= $productDetail['ProductDesc']['length'];
							$_width[] 	= $productDetail['ProductDesc']['width'];
							$_height[] 	= $productDetail['ProductDesc']['height'];
							$_weight[] 	= $productDetail['ProductDesc']['weight'];
							$_sku[] 	= $qty.'X'.$sku;
							$_qty[]	    = $qty;
						}
					}else{
						$productDetail	=	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $item->SKU )));
						$_barcode[] = $productDetail['ProductDesc']['barcode'];
						$_length[] 	= $productDetail['ProductDesc']['length'];
						$_width[] 	= $productDetail['ProductDesc']['width'];
						$_height[] 	= $productDetail['ProductDesc']['height'];
						$_weight[] 	= $productDetail['ProductDesc']['weight'];
						$_sku[] 	= $item->Quantity.'X'.$item->SKU;
						$_qty[]	    = $item->Quantity;
					}
 					 
					$length[] = array_sum($_length);
					$width[]  = array_sum($_width);
					$height[] = array_sum($_height);
					$weight[] = array_sum($_weight);
					$quantity[] = array_sum($_qty);
					$price[] 	= $item->Cost;
					
 				}
				
				$mg['product_order_id_identify'] = $ord['MergeUpdate']['product_order_id_identify'];
				
 				$mg['price'] = array_sum($price);
				 
				if(max($length) == 0){
					$mg['packet_length'] = '30';
				}else{
 					$mg['packet_length'] = max($length);
				}
				
				if(max($width) == 0){
					$mg['packet_width'] = '30';
				}else{
 					$mg['packet_width'] = max($width);
				}
				 
  				$mg['packet_weight'] = array_sum($weight);
				$mg['packet_height'] = array_sum($height);
 				
				
				$filterResults	= $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey',
						'Location.county_name' => $customer_info->Address->Country , 'PostalServiceDesc.max_weight >=' => $mg['packet_weight'], 'PostalServiceDesc.max_length >=' => $mg['packet_length'], 'PostalServiceDesc.max_width >=' => $mg['packet_width'], 'PostalServiceDesc.max_height >= ' => $mg['packet_weight'] , 'PostalServiceDesc.courier' => 'Jersey Post', 'PostalServiceDesc.status' => '0')));
				
				$postid	= 554;
				if(count($filterResults) > 0){		
					$post = [] ;
					foreach($filterResults as $ser){
						$post[$ser['PostalServiceDesc']['id']] = $ser['PostalServiceDesc']['per_item'] + ($ser['PostalServiceDesc']['per_kilo'] * $mg['packet_weight']); 
					}
					
					$postid	= array_keys($post, min($post));
				}								
													
				$postal	= $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $postid)));
						
 				 
				$country = $customer_info->Address->Country;
				$ccode	 = $this->Country->find('first', array('conditions' => array('OR'=>['name' => $country,'custom_name' => $country])));
				$country_code = '';
				if(count($ccode)){
					$country_code = $ccode['Country']['iso_2'];
				}
				echo $sql = "UPDATE `merge_updates` SET 
					`price` = '".$mg['price']."'  ,
					`packet_length` = '".$mg['packet_length']."'  ,
					`packet_width`  = '".$mg['packet_width']."'  ,
					`packet_height` = '".$mg['packet_height']."'  ,
					`packet_weight` = '".$mg['packet_weight']."'  ,
					`delevery_country` = '".$country."'  ,
					`country_code` = '".$country_code."' , 
					`service_id`   = '".$postal['PostalServiceDesc']['id']."'  ,
					`service_name` = '".$postal['PostalServiceDesc']['service_name']."'  ,
					`provider_ref_code` = '".$postal['PostalServiceDesc']['provider_ref_code']."'  ,
					`service_provider`  = 'Jersey Post'  
				WHERE `product_order_id_identify` = '".$mg['product_order_id_identify']. "' ";
				
 				 $this->MergeUpdate->query( $sql );  
			
				echo '<br>'.$v['OpenOrder']['num_order_id'];
			 
 				file_put_contents($log__file,$v['OpenOrder']['num_order_id']."\n",  FILE_APPEND|LOCK_EX);
 			 }
		}
		else{
			echo 'No order';
		}
		exit;
	}
	
 	public function addressInfo($order_id = '3867707')
	{	 
			$this->loadModel('OpenOrder'); 
			$this->loadModel('MergeUpdate'); 
			App::import('Controller', 'Linnworksapis');
			$linn = new LinnworksapisController();
			 
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id ) ) );
			$items = unserialize( $openOrder['OpenOrder']['items']);
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
			$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
			$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
			$shipping_info = unserialize( $openOrder['OpenOrder']['shipping_info']);
			//echo $openOrder['OpenOrder']['sub_source'];
			//pr($items);pr($shipping_info);pr($customer_info);
		 	$date = date('Y-m-d',strtotime("-1 day"));
			//$date = '2021-08-18';
			$openOrder = $this->OpenOrder->query("SELECT `num_order_id`,`items`,`open_order_date` FROM `open_orders` as OpenOrder where `open_order_date` >= '".$date."' AND `status` IN(0,3) GROUP By `num_order_id` HAVING count(`num_order_id`) >1" );
			pr($openOrder);
			if(count($openOrder) > 0){
				 foreach($openOrder as $v){
					$log__file = WWW_ROOT .'logs-ord/duplicate_TR_'.date('dmY').".log";			
 					file_put_contents($log__file,$v['OpenOrder']['num_order_id']."\t".date('Y-m-d H:i:s')."\n",  FILE_APPEND|LOCK_EX);
					echo $v['OpenOrder']['num_order_id'] .'<br>';
					
					if(strtotime(date('Y-m-d H:i:s')) - strtotime($v['OpenOrder']['open_order_date']) > 4000){
 						$this->deleteOrder($v['OpenOrder']['num_order_id']); 
					}
					//$this->OpenOrder->query("UPDATE `open_orders` SET `status` = '3' WHERE `num_order_id` = {$v['OpenOrder']['num_order_id']}; " );
					//$this->OpenOrder->query("UPDATE `merge_updates` SET `status` = '3' WHERE `order_id` = {$v['OpenOrder']['num_order_id']}; " ); 
 				 }
			}
			echo '<br><br>=============================================<br>';
			$openOrder = $this->OpenOrder->query("SELECT`num_order_id`,`items` FROM `open_orders` as OpenOrder where `open_order_date` >= '".$date."' AND `status` IN (0,1)" );
			/*$openOrder = $this->OpenOrder->query("SELECT`num_order_id`,`items` FROM `open_orders` as OpenOrder where `num_order_id` IN( '3868131','3868018','3868248','3867755','3867975','3867905') AND `status` = 0" );*/
			 //pr($openOrder);
			 if(count($openOrder) > 0){
				 foreach($openOrder as $v){
					$log__file = WWW_ROOT .'logs-ord/duplicate_QR_'.date('dmY').".log";			
					//echo $v['OpenOrder']['num_order_id'];	
						
					$qty = 0; $available_qty = 0;
					$items = unserialize( $v['OpenOrder']['items']);
					foreach($items as $it){
					  	$it->SKU; 
						if(isset($it->SKU) && $it->SKU != ''){
							$st = explode("-",$it->SKU); 
							
							//pr($st);
							if($st[0] == 'B'){
 								
								if(count($st) == 3){
									$qty = $qty + ($st[2] * $it->Quantity);
									$sku = 'S-'.$st[1];
									$order_loc = $this->OpenOrder->query("SELECT `order_id`,`split_order_id`,`sku`,`available_qty_check_in`,`available_qty_bin` FROM `order_locations` as OpenLoc where `timestamp` >= '".$date."' AND `status` = 'active'  AND `SKU` = '".$sku."' AND `order_id` = '".$v['OpenOrder']['num_order_id']."' " );
									foreach($order_loc as $p){
										$available_qty = $available_qty + $p['OpenLoc']['available_qty_bin'];
									}
								
								
								}else if(count($st) > 3){
									for($i=1; $i < (count($st) - 1) ; $i++){
										$qty = $qty + $it->Quantity;
										//echo $sku = 'S-'.$st[$i];
										//echo '<br>';
										
										$sku = 'S-'.$st[$i];
										$order_loc = $this->OpenOrder->query("SELECT `order_id`,`split_order_id`,`sku`,`available_qty_check_in`,`available_qty_bin` FROM `order_locations` as OpenLoc where `timestamp` >= '".$date."' AND `status` = 'active'  AND `SKU` = '".$sku."' AND `order_id` = '".$v['OpenOrder']['num_order_id']."' " );
										foreach($order_loc as $p){
											$available_qty = $available_qty + $p['OpenLoc']['available_qty_bin'];
										}
									
									}
								}
							}else{
								$qty = $qty + $it->Quantity;
								
								$order_loc = $this->OpenOrder->query("SELECT `order_id`,`split_order_id`,`sku`,`available_qty_check_in`,`available_qty_bin` FROM `order_locations` as OpenLoc where `timestamp` >= '".$date."' AND `status` = 'active'  AND `SKU` = '".$it->SKU."' AND `order_id` = '".$v['OpenOrder']['num_order_id']."' " );
								foreach($order_loc as $p){
									$available_qty = $available_qty + $p['OpenLoc']['available_qty_bin'];
								}
							}
						}
						
					}
					
					if($available_qty != $qty){
						echo "========================<br>";
						echo ' LOC = '.$qty .' = '.$available_qty.' = '.$v['OpenOrder']['num_order_id'];
						echo "<br>";
					}
					
					
					
					$_ord = $this->MergeUpdate->find( 'all', array( 'conditions' => array('order_id' =>$v['OpenOrder']['num_order_id'],'quantity'=>$qty),'fields' => array('sku','order_id','product_order_id_identify') ) ); 
					//pr($_ord);
					if(count($_ord) == 0){
						echo 'Merge = '.$qty .' = '.$v['OpenOrder']['num_order_id'];
						echo "<br>";
						file_put_contents($log__file,$v['OpenOrder']['num_order_id']."\t".date('Y-m-d H:i:s')."\n",  FILE_APPEND|LOCK_EX);
						//$linn->deleteOrder($v['OpenOrder']['num_order_id']);	 
					}
					
					
  				 }
			}
			$this->getscandir();
			exit;
	}
	
	public function getscandir()
	{
		$this->loadModel('OpenOrder'); 
		$dir = WWW_ROOT .'logs-ord';		
		$files = scandir($dir, 1); $count = 1;
		foreach($files as $order){
			$ftime = date('Y-m-d H:i:s', filectime($dir.DS.$order));
			$time_diff = (strtotime(date('Y-m-d H:i:s')) - strtotime($ftime));
			//echo  ' === '. .' =='. $order.'<br>';
			$order_id = explode(".",$order)[0];
 			$openOrder = $this->OpenOrder->find('all',
				 array('conditions' => 
					 array('OpenOrder.num_order_id' => $order_id )
				  )
			  );
			  if(count($openOrder) == 0){
			 	 if(strlen($order_id) == 7){
 					 if($time_diff >  4800){ 
						  @unlink($dir.DS.$order);
 						  echo $order_id;
						  echo '===<br>';
						  $log__file = WWW_ROOT .'logs-ord/getscandir_'.date('dmY').".log";		
						  file_put_contents($log__file,$order_id."\t".date('Y-m-d H:i:s')."\n",  FILE_APPEND|LOCK_EX);
					 }else{
 						  echo   str_pad(($count++),2,'0',STR_PAD_LEFT) .' Time = '. intval($time_diff/60).'=='.$order_id.'<br>';
					 }
				 }
			  }else{
				 if($time_diff > 260000){  //260000 = 3 days
					  @unlink($dir.DS.$order);
				 }
			  }
			
		}
		echo 'getscandir';
		//pr($files);
		$this->updateDim();
		exit;
	}
	
	public function getOpenOrderById( $numOrderId = null )
	{
			$this->loadModel('OpenOrder');
			$order = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $numOrderId )));
			
			$data['id']					=	 $order['OpenOrder']['id'];
			$data['order_id']			=	 $order['OpenOrder']['order_id'];
			$data['destination']			=	 $order['OpenOrder']['destination'];
			$data['sub_source']			=	 $order['OpenOrder']['sub_source'];
			$data['num_order_id']		=	 $order['OpenOrder']['num_order_id'];
			$data['general_info']		=	 unserialize($order['OpenOrder']['general_info']);
			$data['shipping_info']		=	 unserialize($order['OpenOrder']['shipping_info']);
			$data['customer_info']		=	 unserialize($order['OpenOrder']['customer_info']);
			$data['totals_info']		=	 unserialize($order['OpenOrder']['totals_info']);
			$data['folder_name']		=	 unserialize($order['OpenOrder']['folder_name']);
			$data['items']				=	 unserialize($order['OpenOrder']['items']);
			$data['assigned_service']	=	 $order['AssignService']['assigned_service'];
			$data['assign_barcode']		=	 $order['AssignService']['assign_barcode'];
			$data['manifest']			=	 $order['AssignService']['manifest'];
			$data['cn22']				=	 $order['AssignService']['cn22'];
			$data['courier']			=	 $order['AssignService']['courier'];
			
			return $data;
		}
		
	public function deleteOrder( $orderId = null )
	{
			$this->layout = '';
			$this->autoRender = false;
			
			if( isset($orderId) && $orderId > 0 )
			{
				echo $id	= $orderId;
			}
			else
			{
				$id	= $this->request->data['openorderid'];
				$ord_flag = WWW_ROOT .'logs-ord/'.$id.".flag";	
				if(file_exists($ord_flag)){
					@unlink($ord_flag);
				} 
			}
			
			$this->loadModel('OpenOrder');
			$this->loadModel('OrderItem');
			$this->loadModel('Product');
			$this->loadModel('CancelOrder');
			$this->loadModel('MergeUpdate');
			$this->loadModel('ScanOrder');
			$this->loadModel('BinLocation');
			$this->loadModel('OrderLocation');
			$this->loadModel('CheckIn');
			$this->loadMOdel('InkOrderLocation' ); 
			$msg = 0;
			$getOpernOrder	=	$this->getOpenOrderById( $id );
			
			$order			=	$this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $id)));
			$orderid		=	$order['OpenOrder']['num_order_id'];
			$pkid			=	$order['OpenOrder']['order_id'];
			$orderItems 	=	$this->MergeUpdate->find('all', array('conditions' => array('MergeUpdate.order_id' => $id)));
			$pick_list_status = 0; 
			
			/*--------------Ink order delete---------------*/	
			$order_ink = $this->InkOrderLocation->find( 'first', array( 'conditions' => array( 'order_id' => $orderid,'status' => 'active' ) ) );
			 
			if(count($order_ink) > 0){ 
			
				$val 		= $order_ink;
 				$barcode 	= $val['InkOrderLocation']['barcode'];
				$sku		= $val['InkOrderLocation']['sku'];
				$ink_sku 	= $val['InkOrderLocation']['ink_sku'];
				$location 	= $val['InkOrderLocation']['bin_location'];
				$quantity 	= $val['InkOrderLocation']['quantity'];
				$qty_bin 	= $val['InkOrderLocation']['available_qty_bin'];						
				$po_id	 	= explode(",",$val['InkOrderLocation']['po_id'])[0];
				  
						 
				$this->BinLocation->updateAll(	array('BinLocation.stock_by_location' => "BinLocation.stock_by_location + $qty_bin"),
													array('BinLocation.bin_location' => $location, 'BinLocation.barcode' => $barcode));
										
					 
				$this->CheckIn->updateAll( array('CheckIn.selling_qty' => "CheckIn.selling_qty - $qty_bin"),array('CheckIn.id' => $po_id));							  
				$product =	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $ink_sku)));
				  
				if( count( $product ) > 0 )
				{
						$ordertype = 'Delete';
						
						$currentStickLevel	=	$product['Product']['current_stock_level'];
						$qtyForUpdate		=	(int)$currentStickLevel + (int)$qty_bin;								
						
						$data['Product']['product_sku']		=	$product['Product']['product_sku'];
						$data['ProductDesc']['barcode']		=	$product['ProductDesc']['barcode'];
						$data['Product']['CurrentStock']	=	$currentStickLevel;
						
						$productData	=	json_decode(json_encode($data,0));
						App::import('Controller', 'Cronjobs');
						$productCant = new CronjobsController();
						$productCant->storeInventoryRecord( $productData, $qty_bin, $id, $ordertype, $barcode);
						
						$this->Product->updateAll(array('Product.current_stock_level' => $qtyForUpdate), array('Product.product_sku' => $ink_sku));
				}
				
				$this->InkOrderLocation->updateAll( array( 'InkOrderLocation.status' =>"'deleted'") , array( 'order_id' => $orderid ) );
				$val['username'] = ( $this->Session->read('Auth.User.username') != '' ) ? $this->Session->read('Auth.User.username') : '_'; 
				file_put_contents(WWW_ROOT .'logs/ink_order_delete_'.date('dmY').'.log', print_r($val,true), FILE_APPEND | LOCK_EX);
			 
					
			}
			/*--------------End ink order delete---------------*/
			
			if( (count($orderItems) == 0) || (empty($orderItems) || $orderItems == '') )
			{
				
				$order_details	= $this->OrderLocation->find('all', array('conditions' => array('OrderLocation.order_id' => $orderid,'OrderLocation.status' => 'active')));
			
				if(count($order_details) > 0){
				
					$count = 0;			
					
					foreach($order_details as $val)
					{
				
						$barcode 	= $val['OrderLocation']['barcode'];
						$sku		= $val['OrderLocation']['sku'];
						$location 	= $val['OrderLocation']['bin_location'];
						$quantity 	= $val['OrderLocation']['quantity'];
						$qty_bin 	= $val['OrderLocation']['available_qty_bin'];	
						//$qty_chin 	= $val['OrderLocation']['available_qty_check_in'];
						$po_id	 	= explode(",",$val['OrderLocation']['po_id'])[0];	
								
						
						if($location != 'No Location' && $barcode != '')
						{  
							$this->BinLocation->updateAll(	array('BinLocation.stock_by_location' => "BinLocation.stock_by_location + $qty_bin"),
														array('BinLocation.bin_location' => $location, 'BinLocation.barcode' => $barcode));
						}					
						 
						$this->CheckIn->updateAll( array('CheckIn.selling_qty' => "CheckIn.selling_qty - $qty_bin"),array('CheckIn.id' => $po_id));							  
						$product =	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
						// look for sku if exist  
						if( count( $product ) > 0 )
						{
							$ordertype = 'Delete';
							
							$currentStickLevel	=	$product['Product']['current_stock_level'];
							$qtyForUpdate		=	(int)$currentStickLevel + (int)$qty_bin;								
							
							$data['Product']['product_sku']		=	$product['Product']['product_sku'];
							$data['ProductDesc']['barcode']		=	$product['ProductDesc']['barcode'];
							$data['Product']['CurrentStock']	=	$currentStickLevel;
							
							$productData	=	json_decode(json_encode($data,0));
							App::import('Controller', 'Cronjobs');
							$productCant = new CronjobsController();
							$productCant->storeInventoryRecord( $productData, $qty_bin, $id, $ordertype, $barcode);
							
							$this->Product->updateAll(array('Product.current_stock_level' => $qtyForUpdate), array('Product.product_sku' => $sku));
								
								
							}		
					}
				$this->OrderLocation->updateAll( array( 'OrderLocation.status' =>"'deleted'") , array( 'OrderLocation.order_id' => $orderid ) );
			   }
				$this->OpenOrder->query("delete from open_orders where num_order_id = '{$id}'");	
				
				$msg = 4;					
			}
			else
			{
 				/*-------RoyalMail Changes 13032018---------*/
				App::Import('Controller', 'RoyalMail'); 
				$royal = new RoyalMailController;
				/*----------------End---------------------*/
							 
 				$order_details	= $this->OrderLocation->find('all', array('conditions' => 
									array('OrderLocation.order_id' => $orderid,'OrderLocation.status' => 'active')));
				
				if(count($order_details) > 0){
					
					$count = 0;			
					
					foreach($order_details as $val){
					
						$barcode 	= $val['OrderLocation']['barcode'];
						$sku		= $val['OrderLocation']['sku'];
						$location 	= $val['OrderLocation']['bin_location'];
						$quantity 	= $val['OrderLocation']['quantity'];
						$qty_bin 	= $val['OrderLocation']['available_qty_bin'];	
						//$qty_chin 	= $val['OrderLocation']['available_qty_check_in'];
						$po_id	 	= explode(",",$val['OrderLocation']['po_id'])[0];	
 						
						if($location != 'No Location')
						{
							$this->BinLocation->updateAll(	array('BinLocation.stock_by_location' => "BinLocation.stock_by_location + $qty_bin"),
														array('BinLocation.bin_location' => $location, 'BinLocation.barcode' => $barcode));
						}					
						 
						$this->CheckIn->updateAll( array('CheckIn.selling_qty' => "CheckIn.selling_qty - $qty_bin"),array('CheckIn.id' => $po_id));							  
						$product =	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
						// look for sku if exist  
						if( count( $product ) > 0 )
						{
							$ordertype = 'Delete';
							
							$currentStickLevel	=	$product['Product']['current_stock_level'];
							$qtyForUpdate		=	(int)$currentStickLevel + (int)$qty_bin;								
							
							$data['Product']['product_sku']		=	$product['Product']['product_sku'];
							$data['ProductDesc']['barcode']		=	$product['ProductDesc']['barcode'];
							$data['Product']['CurrentStock']	=	$currentStickLevel;
							
							$productData	=	json_decode(json_encode($data,0));
							App::import('Controller', 'Cronjobs');
							$productCant = new CronjobsController();
							$productCant->storeInventoryRecord( $productData, $qty_bin, $id, $ordertype, $barcode);
							
							$result =	$this->Product->updateAll(array('Product.current_stock_level' => $qtyForUpdate), array('Product.product_sku' => $sku));
					
							//Putback items for shelf ensuring from warehouse keeper
							$this->loadModel( 'PutbackItem' );							
							$putBackItem = array();
							$authDetails = $this->Session->read('Auth.User'); 
							$putBackItem['user_id']	= $authDetails['id'];											
							$putBackItem['OrderID']	= $id;											
							$putBackItem['Sub-Order']= $splitID;												
							$putBackItem['SKU']		= $sku;										
							$putBackItem['Title']	= $product['Product']['product_name'];								
							$putBackItem['Barcode']	= $product['ProductDesc']['barcode'];					
							$putBackItem['Location']= $location;			
							$putBackItem['Quantity']= $qty_bin;	
							$putBackItem['action_type']= 'deleted';	
							if( $sku != '' )
							{
								$this->PutbackItem->saveAll( $putBackItem );
							}
								
						}	
						
						foreach( $orderItems as $getItem )
						{
							$splitID = $getItem['MergeUpdate']['product_order_id_identify'];
							//$this->deleteRegisteredNumber( $splitID );	
							
							/*Cancel Royal mail Shipment.*/
							if($getItem['MergeUpdate']['royalmail'] == 1){
								$royal->royalMailCancelShipment($splitID,1); 			
							} 
							
							$conditionsformergeupdate = array ('MergeUpdate.order_id' => $id);
							$resultMergeUpdate  	= 	$this->MergeUpdate->deleteAll( $conditionsformergeupdate );
							 
							$conditionsforscanorder = array ('ScanOrder.split_order_id' => $splitID);
							$resultScanOrder 		= 	$this->ScanOrder->deleteAll( $conditionsforscanorder );
								
						}
 						$this->OpenOrder->query("delete from open_orders where num_order_id = '{$id}'");							
 					}
 					$this->OrderLocation->updateAll( array( 'OrderLocation.status' =>"'deleted'") , array( 'OrderLocation.order_id' => $orderid ) );
					$msg = 2;
				}
				else{
					/*************In Case when order is not in order_location table but in Open_order and Merge_update table**************************/
					if(count($orderItems) > 0){
							foreach( $orderItems as $getItem )
							{
								$splitID = $getItem['MergeUpdate']['product_order_id_identify'];
								//$this->deleteRegisteredNumber( $splitID );	
								/*Cancel Royal mail Shipment.*/
								if($getItem['MergeUpdate']['royalmail'] == 1){
									$royal->royalMailCancelShipment($splitID,1); 			
								}
								$conditionsformergeupdate = array ('MergeUpdate.order_id' => $id);
								$resultMergeUpdate  	= 	$this->MergeUpdate->deleteAll( $conditionsformergeupdate );
								 
								$conditionsforscanorder =  array ('ScanOrder.split_order_id' => $splitID);
								$resultScanOrder 		=  $this->ScanOrder->deleteAll( $conditionsforscanorder );
								$pick_list_status 	    =  $getItem['MergeUpdate']['pick_list_status'];			
							}
							
						$msg = 9;
						$product =	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
						
						$this->OpenOrder->query("delete from open_orders where num_order_id = '{$id}'");
 					}			
				}	 
					
			}
			
			
   			file_put_contents(WWW_ROOT .'logs-ord/test_del_'.date('dmY').'.log',$orderId."\n", FILE_APPEND|LOCK_EX);
			echo 'msg'.$msg ;	 
			return $msg;
  	  }
 
  	public function SkuActive()
	{ 			
		$this->layout = '';
		$this->autoRender = false;
 		$this->loadModel('Product');
		
		
		echo $file =  WWW_ROOT.'Centresoft new skus-31AUG.csv';
		echo '<br>';
		$csvData = file_get_contents($file);
		
		$row = 0;
		if (($handle = fopen($file, "r")) !== FALSE) {
		
			$usql ="UPDATE `products` SET `is_deleted` = '1'";   
			//$this->Product->query($usql);
			
			$usql ="UPDATE `sku_percent_records` SET `deleted_status` = '1'";  
			//$this->Product->query($usql);	
 			
			echo $log__file = WWW_ROOT .'logs/xsensys_sku_not_found_'.date('dmY').".csv";	
			@unlink($log__file );
		
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$num = count($data);
				//echo "<p> $num fields in line $row: <br /></p>\n";
				
				for ($c=0; $c < $num; $c++) {
					$sku = $data[$c];
					
					$sql  =   "SELECT `product_sku` FROM `products`  WHERE `product_sku` LIKE '".$sku."' ";
					$results = $this->Product->query($sql);		
					if(count($results) > 0){
						$usql ="UPDATE `products` SET `is_deleted` = '0' WHERE `product_sku` = '".$sku."' "; 
						$this->Product->query($usql);	
						
						$ssql ="UPDATE `sku_percent_records` SET `deleted_status` = '0' WHERE `sku` = '".$sku."'";  
						$this->Product->query($ssql);
			
 						$row++;	
					}else{
						echo $sku. "  not found<br />\n";
   						file_put_contents($log__file,$sku."\n",  FILE_APPEND|LOCK_EX);
					}
					
				}
			}
			fclose($handle);
		}
		echo '<br>'.$row. ' Updated' ;
		exit;
	}
	
	public function openInfo($order_id = '3867707')
	{	 
			$this->loadModel('OpenOrder'); 
			$this->loadModel('MergeUpdate'); 
			App::import('Controller', 'Linnworksapis');
			$linn = new LinnworksapisController();
			 
			$openOrder = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $order_id ) ) );
			$items = unserialize( $openOrder['OpenOrder']['items']);
			$customer_info = unserialize( $openOrder['OpenOrder']['customer_info']);
			$totals_info = unserialize( $openOrder['OpenOrder']['totals_info']);
			$general_info = unserialize( $openOrder['OpenOrder']['general_info']);
			$shipping_info = unserialize( $openOrder['OpenOrder']['shipping_info']);
 			echo '====general_info=====';
			pr($general_info);
			echo '====items=====';
			pr($items);
			echo '====shipping_info=====';
			pr($shipping_info);
			echo '====customer_info=====';
			pr($customer_info);
		 	exit;
	}
 
			
	 
}

?>
