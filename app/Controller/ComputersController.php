<?php

class ComputersController extends AppController
{
    var $name = "Computers";    
    var $components = array('Session','Upload','Common','Auth');    
    var $helpers = array('Html','Form','Common','Session');
    
    public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->Auth->Allow(array('getPcName'));
	}
    
    /* Get Pc's Name now */
    public function getPcName()
    {
		$this->layout = '';
		$this->autoRander = false;
		
		App::import( 'Controller' , 'Coreprinters' );
		$Coreprinter = new CoreprintersController();
		$getFullDetail = json_decode(json_encode($Coreprinter->getFullPcDetail()),0);
		
		$this->storePcWithPrinter( $getFullDetail );
		
		$this->loadModel( 'PcStore' );
		exit;
	}
	
	//Store Pc's
	public function storePcWithPrinter( $computerDetail = null )
	{
		
		$this->layout = '';
		$this->autoRander = false;
		
		$this->loadModel( 'PcStore' );
		$this->PcStore->query( "TRUNCATE pc_stores" );
		
		foreach( $computerDetail as $computerDetailIndex => $computerDetailValue )
		{
			
			if( $computerDetailValue->name != "Fax" && $computerDetailValue->name != "Microsoft XPS Document Writer" )
			{
				$data['pc_name']		= $computerDetailValue->computer->name;	
				$data['pc_id']			= $computerDetailValue->computer->id;	
				$data['printer_id']		= $computerDetailValue->id;
				$data['printer_name']	= $computerDetailValue->name;
				$data['inet_address']	= $computerDetailValue->computer->inet;	
				$data['pc_state']		= $computerDetailValue->computer->state;	
				$data['printer_state']	= $computerDetailValue->state;	
				
				$this->PcStore->saveAll( $data );
			}
		}
	}
	
	/* Get Pc's Name now */
    public function getPcNameDymo()
    {
		$this->layout = '';
		$this->autoRander = false;
		
		App::import( 'Controller' , 'Coreprinters' );
		$Coreprinter = new CoreprintersController();
		$getFullDetail = json_decode(json_encode($Coreprinter->getFullPcDetailDymo()),0);
		
		$this->storePcWithPrinterDymo( $getFullDetail );
		
		$this->loadModel( 'PcStore' );
		exit;
	}
	
	//Store Pc's
	public function storePcWithPrinterDymo( $computerDetail = null )
	{
		
		$this->layout = '';
		$this->autoRander = false;
		
		$this->loadModel( 'PcStore' );
		//$this->PcStore->query( "TRUNCATE pc_stores" );
		
		foreach( $computerDetail as $computerDetailIndex => $computerDetailValue )
		{
			
			if( $computerDetailValue->name != "Fax" && $computerDetailValue->name != "Microsoft XPS Document Writer" )
			{
				$data['pc_name']		= $computerDetailValue->computer->name;	
				$data['pc_id']			= $computerDetailValue->computer->id;	
				$data['printer_id']		= $computerDetailValue->id;
				$data['printer_name']	= $computerDetailValue->name;
				$data['inet_address']	= $computerDetailValue->computer->inet;	
				$data['pc_state']		= $computerDetailValue->computer->state;	
				$data['printer_state']	= $computerDetailValue->state;	
				
				$this->PcStore->saveAll( $data );
			}
		}
	}
    
}

?>
