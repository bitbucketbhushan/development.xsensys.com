<?php
class ModulesController extends AppController
{
    
    var $name = "Modules";
    
    var $components = array('Session','Upload','Common','Auth');
    
    var $helpers = array('Html','Form','Common','Session');
    
	public function index()
	{
		$this->layout='index';
	}
	
	public function editPermission( $id = null) {
	
		$this->layout="index";
		$this->loadModel('User');
		$this->loadModel('ModulePermission');
		$getUsers	=	$this->User->find('list', array('fields' => array('User.id','User.username')));
		$getmodule	=	$this->ModulePermission->find('first', array('conditions' => array('ModulePermission.id' => $id)));
		$getmodule['ModulePermission']['user_id']	=	explode(',', $getmodule['ModulePermission']['user_id']);
		$this->set( 'getUsers', $getUsers);
		$this->request->data	=	$getmodule;

	}
	
	public function addModulePermission()
	{
		$this->layout = 'index';
		$this->loadModel('ModulePermission');
		if(isset($this->request->data['ModulePermission']['id']) && $this->request->data['ModulePermission']['id'])
		{
			$id = $this->request->data['ModulePermission']['id'];
			$this->set('title', 'Edit Postal Provider');
		}
		else
		{
			$id = '';
			$this->set('title', 'Add Postal Provider');
		}
		
		
		
		$flag = false;
        $setNewFlag = 0;
      
		if($this->request->is('Post'))
		{
			$this->ModulePermission->set( $this->request->data );
                if( $this->ModulePermission->validates( $this->request->data ) )
                {
                       $flag = true;                                      
                }
                else
                {
                   $flag = false;                   
                   $setNewFlag = 1;
                   $error = $this->ModulePermission->validationErrors;
                }
                if( $setNewFlag == 0 )
					{
						
						$locationIds	=	implode(',', $this->request->data['ModulePermission']['user_id']);
						$this->request->data['ModulePermission']['user_id'] = $locationIds;
						
						if($this->ModulePermission->save( $this->request->data ))
						{
							if($id)
							{
								$this->Session->setFlash('Module update successfully.', 'flash_success');
							}
							else
							{
								//$this->Session->setFlash('Postal service add successfully.', 'flash_success');
							}
						}
						
						$this->redirect(array('controller'=>'Modules','action' => 'showAllModule'));
						
					}	
		}
		
	}
	
	public function showAllModule() {
	
		$this->layout = 'index';
		$this->loadModel('ModulePermission');
		$getAllModules	=	$this->ModulePermission->find('all');
		$this->set('getAllModules', $getAllModules);

	}


	public function createManifestBackup()
	{
		   $this->layout = '';
		   $this->autoRender = false;
		   $dbhost = 'localhost';
		   $dbuser = 'xsensysc_dbuser';
		   $dbpass = 'Dbpass@2016#dev';
		   $date = date('Ymd_Hm');
		   $path = $_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/manifestBackup/service_counters_'.$date.'.sql';
		   exec("mysqldump xsensysc_dev service_counters -uxsensysc_dbuser -pDbpass@2016#dev  > 	".$path."");
	}	
     
}

?>
