<?php
class StaffOrdersController extends AppController
{
    var $name = "StaffOrders";
	var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
	
	public function beforeFilter()
	{
	   parent::beforeFilter();
	   $this->layout = false;
	   $this->Auth->Allow(array('update_amazon_order_id'));
	   $this->GlobalBarcode = $this->Components->load('Common'); 
	}
	
	public function index( $sku = null )
    {
		$this->layout = 'index';
		$this->loadModel('User');
		$this->loadModel('StaffOrder');
		 
		 
		$this->paginate = array('group' => 'order_id','order'=>'id DESC','limit' => 50);
		$getStaffOrders = $this->paginate('StaffOrder');
		$order_ids = [];
		foreach($getStaffOrders as $v){
			$order_ids[] = $v['StaffOrder']['order_id'];
		}
		
		$staffOrders = [];
		$staffOrder = $this->StaffOrder->find('all', array( 'conditions' => array( 'order_id' => $order_ids ) ) );
		foreach($staffOrder as $v){
			$staffOrders[$v['StaffOrder']['order_id']][] = $v;
		}	
		
		$users = $this->User->find('all', array('conditions' => array('country' =>[1,3]),'fields' => array('id','first_name','last_name','username','email','country'),'group' => 'email'));	
		//$this->set( 'getStaffOrders', $getStaffOrders );
		$this->set( compact('getStaffOrders','users','staffOrders') );
	}
	 
	
	public function checkProduct()
	{
		$this->layout = '';
		$this->loadModel('User');
		$this->loadModel('Product');	
		$this->loadModel('StaffOrder');		
		$this->loadModel('PurchaseOrder');
		
		$sdate = date('Y-m', strtotime(date('Y-m')." -1 month")).'-27';		
		$edate = date('Y-m').'-27';		
				
		$quantity = 1;
		if(isset($this->request->data['quantity']) && $this->request->data['quantity'] > 0){
			$quantity = $this->request->data['quantity'];
		}
		
		$user = $this->User->find('first', array('conditions' => array('email' =>$this->request->data['staff_person_email']),'fields' => array('id','first_name','last_name','username','email','country')));
		$user_first_last_name = $user['User']['first_name'].' '.$user['User']['last_name'];
			
		$orders = $this->StaffOrder->find('first', array('conditions' => array('staff_person_email' => $this->request->data['staff_person_email'],'status' => 'active','created_date BETWEEN ? AND ?'=>[$sdate,$edate]),'fields' => array('SUM(unit_price) as totalprice')));
		$totalprice	 = $orders[0]['totalprice'];
		
		$cart_total = 0; $row_total = 0;
		if(isset($_SESSION['staff_order'])){
			foreach($_SESSION['staff_order'] as $_barcode => $v){ 
				$qty	     = isset($v['qty']) ? $v['qty'] : 1;	
				$cart_total += $qty * $v['price'];
			}
		}
					
		 
 		$data = [];
		$product_barcode = trim($this->request->data['product_barcode']);
 	 	$product_global_barcode = $this->GlobalBarcode->getGlobalBarcode($product_barcode);
		$checkSku =	$this->Product->find('first', array( 'conditions' => array( 'ProductDesc.barcode LIKE' => $product_global_barcode ),'fields'=>['Product.product_name','Product.current_stock_level','Product.product_sku','ProductDesc.barcode'] ) );
	 
		if(count($checkSku) > 0){
 		 
 			if(substr($checkSku['Product']['product_sku'],0,2) == 'B-'){
				$data['msg'] = 'You can not purchase Bundle Items.'; 
			}else{
				$getPurchaseDetail	= $this->PurchaseOrder->find('first', array('conditions'=> array('PurchaseOrder.purchase_sku' => $checkSku['Product']['product_sku'],'PurchaseOrder.price >' => 0 ), 'order' => array('PurchaseOrder.date DESC') ) );
				
				if(count($getPurchaseDetail) > 0)
				{ 
 					$row_total = ($getPurchaseDetail['PurchaseOrder']['price'] * $quantity);
					
					$current_total = $cart_total + $row_total;
 					$_total 	   = $totalprice + $current_total ;
					
					if($_total >= 100){
						$data['msg'] = "\"{$user_first_last_name}\" can not purchase more than 100GBP in a month. Your total purchase of this month({$sdate} to {$edate}) is {$totalprice}GBP + {$current_total}GBP = {$_total}GBP"; 
					}else{
					
						$data['product_name'] = $checkSku['Product']['product_name'];
						$data['product_sku']  = $checkSku['Product']['product_sku'];	
						$data['current_stock']= $checkSku['Product']['current_stock_level'];
						$data['price'] 		  = $getPurchaseDetail['PurchaseOrder']['price']; 
						$data['po_name']      = $getPurchaseDetail['PurchaseOrder']['po_name'] ; 
						
						if($data['current_stock'] == 0){
							$data['msg'] = 'Staff Person can not purchase this item because current stock is 0.'; 
						}else{
							if(isset($_SESSION['staff_order'][$product_barcode])){
								if(isset($_SESSION['staff_order'][$product_barcode]['qty'])){
									$qty = $_SESSION['staff_order'][$product_barcode]['qty'] + 1;
									if($qty > $checkSku['Product']['current_stock_level']){
										$data['msg'] = 'Staff Person can not purchase more than current stock.'; 
									}else{ 
										if( $_SESSION['staff_order'][$product_barcode]['qty'] < 3){
											$data['qty'] = $_SESSION['staff_order'][$product_barcode]['qty'] + 1; 								
										}else{
											$data['qty'] = 3;
											$data['msg'] = 'Staff Person can not purchase more than 3 qty.';
										} 
								}
							}else{
								$data['qty'] = 1;
							} 
							} 
							$_SESSION['staff_order'][$product_barcode] = $data;
						}
					}
					
				}else{
					$data['msg'] = 'Purchase price not found of this item.'; 
				}
			} 
 		} 
		
		$html = '';	
		if(isset($_SESSION['staff_order'])){
		
			$html = '<tr>				
				<th>Poduct Barcode</th>
				<th>Poduct SKU</th>
				<th width="40%">Poduct Title</th>
				<th width="8%">Unit Price</th> 
				<th width="8%">Quantity</th> 
				<th width="8%">Current Stock</th> 
				<th width="8%">Total</th> 
				<th width="8%">Action</th>
			</tr>';
			foreach($_SESSION['staff_order'] as $_barcode => $v){ 
			
				$selected_1 = 'selected="selected"';
				$selected_2 = $selected_3 =''; $qty =1;
				if(isset($v['qty']) && $v['qty'] == 1){
					$selected_1 = 'selected="selected"';
					$qty = 1;
				}else if(isset($v['qty']) && $v['qty'] == 2){
					$selected_2 = 'selected="selected"';
					$qty = 2;
				}else if(isset($v['qty']) && $v['qty'] == 3){
					$selected_3 = 'selected="selected"';
					$qty = 3;
				}
			
				 $html .= '<tr>				
					<td>'.$_barcode.'</td>
					<td>'.$v['product_sku'].'</th>
					<td>'.$v['product_name'].'</td> 
					<td id="price_'.$_barcode.'">'.$v['price'].'</td> 
					<td id="qty_'.$_barcode.'"><select name="quantity" id="quantity_'.$_barcode.'" class="form-control" onchange="updateQty('.$_barcode.')"><option value="1" '.$selected_1.'>1</option><option value="2" '.$selected_2.'>2</option><option value="3" '.$selected_3.'>3</option></select></td> 
					<td id="current_stock_'.$_barcode.'">'. $v['current_stock'].'</td>
					<td id="total_'.$_barcode.'">'. number_format( $qty*$v['price'] ,2).'</td>
					<td><button type="button" class="btn btn-danger btn-sm" onclick="removeItem('.$_barcode.');">Remove Item</button></td>
				</tr>';
			}
		 
		$html .= '<tr>
					<td colspan="7" style="text-align: right;"><strong>Total : '.($cart_total + $row_total).'GBP</strong></td>
					<td style="text-align: right;"><button type="button" class="btn btn-success btn-sm" onclick="createOrder();">Create Order</button></td>
				</tr>';	
		}
		$data['html'] = $html;    
		echo json_encode($data);
		exit;
		
  	}
	
	public function updateQty()
	{
		$this->layout = '';
		$this->loadModel('User');	
		$this->loadModel('StaffOrder');		
		$this->loadModel('PurchaseOrder');
		
		$sdate = date('Y-m', strtotime(date('Y-m')." -1 month")).'-27';		
		$edate = date('Y-m').'-27';		
				
		$quantity = 1;
		if($this->request->data['quantity'] > 0){
			$quantity = $this->request->data['quantity'];
		}
		
		$user = $this->User->find('first', array('conditions' => array('email' =>$this->request->data['staff_person_email']),'fields' => array('id','first_name','last_name','username','email','country')));
		$user_first_last_name = $user['User']['first_name'].' '.$user['User']['last_name'];
		
		$orders = $this->StaffOrder->find('first', array('conditions' => array('staff_person_email' => $this->request->data['staff_person_email'],'status' => 'active','created_date BETWEEN ? AND ?'=>[$sdate,$edate]),'fields' => array('SUM(unit_price) as totalprice')));
		$totalprice	 = $orders[0]['totalprice'];
 		
		$product_barcode = $this->request->data['barcode'] ;
		$data = [];
		if($this->request->data['quantity'] > $this->request->data['current_stock']){
			$data['msg'] = 'Staff Person can not purchase more than current stock.';
		}else{ 
		
 			$current_total = $this->request->data['price'] * $this->request->data['quantity'];
			$_total = $totalprice + $current_total;
			if($_total >= 100){
				$data['msg'] = "\"{$user_first_last_name}\" can not purchase more than 100GBP in a month. Your total purchase of this month({$sdate} to {$edate}) is {$totalprice}GBP + {$current_total}GBP = {$_total}GBP"; 
			}else{
				$_SESSION['staff_order'][$product_barcode]['qty'] = $this->request->data['quantity'];
				$total = number_format($this->request->data['quantity'] * $this->request->data['price'],2);
				$data['total'] = $total;
				$data['msg'] = '';
			}
		}
 		echo json_encode($data); 
		exit;
  	}
	
	public function removeItem()
	{
		$this->layout = '';
		$this->autoRender = false;
		if(isset($this->request->data['barcode'])){
  			unset($_SESSION['staff_order'][$this->request->data['barcode']]);
		}
		$data = []; $cart_total = 0;
		if(isset($_SESSION['staff_order'])){	  
		$html = '<tr>				
				<th>Poduct Barcode</th>
				<th>Poduct SKU</th>
				<th width="40%">Poduct Title</th>
				<th width="8%">Unit Price</th> 
				<th width="8%">Quantity</th> 
				<th width="8%">Current Stock</th> 
				<th width="8%">Total</th> 
				<th width="8%">Action</th>
			</tr>';
		foreach($_SESSION['staff_order'] as $_barcode => $v){ 
			
			$selected_1 = 'selected="selected"';
			$selected_2 = $selected_3 =''; $qty =1;
			
			if(isset($v['qty']) && $v['qty'] == 1){
				$selected_1 = 'selected="selected"';
				$qty = 1;
			}else if(isset($v['qty']) && $v['qty'] == 2){
				$selected_2 = 'selected="selected"';
				$qty = 2;
			}else if(isset($v['qty']) && $v['qty'] == 3){
				$selected_3 = 'selected="selected"';
				$qty = 3;
			}
			$cart_total += $qty*$v['price'];
			 $html .= '<tr>				
				<td>'.$_barcode.'</td>
				<td>'.$v['product_sku'].'</th>
				<td>'.$v['product_name'].'</td> 
				<td id="price_'.$_barcode.'">'.$v['price'].'</td> 
					<td id="qty_'.$_barcode.'"><select name="quantity" id="quantity_'.$_barcode.'" class="form-control" onchange="updateQty('.$_barcode.')"><option value="1" '.$selected_1.'>1</option><option value="2" '.$selected_2.'>2</option><option value="3" '.$selected_3.'>3</option></select></td> 
				<td id="current_stock_'.$_barcode.'">'. $v['current_stock'].'</td>
				<td id="total_'.$_barcode.'">'. number_format( $qty*$v['price'] ,2).'</td> 
				<td><button type="button" class="btn btn-danger btn-sm" onclick="removeItem('.$_barcode.');">Remove Item</button></td>
			</tr>';
		}
		$html .= '<tr>
				<td colspan="7" style="text-align: right;"><strong>Total : '.$cart_total.' GBP</strong></td>
				<td style="text-align: right;"><button type="button" class="btn btn-success btn-sm" onclick="createOrder();">Create Order</button></td>
			</tr>';	
		$data['html'] = $html;    
		}
		echo json_encode($data);
		exit;
  	}
	
	public function createOrder()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('User');
		$this->loadModel('Product');		
		$this->loadModel('PurchaseOrder');
		$this->loadModel('StaffOrder');
			
 	   	$rand = '';
		$mailFormate = ''; 
		$order_total = 0;
		$mdata = [];
		for($i=0; $i < 2; $i++){
			$rand .= mt_rand(0, 9);
		}
 		$order_id = date("YmdHis") . $rand;
		
		$sdate = date('Y-m', strtotime(date('Y-m')." -1 month")).'-27';		
		$edate =  date('Y-m').'-27';
		
		$user = $this->User->find('first', array('conditions' => array('email' =>$this->request->data['staff_person_email']),'fields' => array('id','first_name','last_name','username','email','country')));
		$user_first_last_name = $user['User']['first_name'].' '.$user['User']['last_name'];
 	  		
		$orders = $this->StaffOrder->find('first', array('conditions' => array('staff_person_email' => $this->request->data['staff_person_email'],'status' => 'active','created_date BETWEEN ? AND ?'=>[$sdate,$edate]),'fields' => array('SUM(unit_price) as totalprice')));
		
		$totalprice = $orders[0]['totalprice'];
		$current_total = 0;
		
		foreach($_SESSION['staff_order'] as $_barcode => $v){ 
			$qty	 = isset($v['qty']) ? $v['qty'] : 1;	
			$current_total += $qty * $v['price'];
		}
		
 		$_total = $totalprice + $current_total;
		
		if($_total >= 100){
			$mdata['status'] = 1;
			$mdata['msg'] = "\"{$user_first_last_name}\" can not purchase more than 100GBP in a month. Your total purchase of this month({$sdate} to {$edate}) is {$totalprice}GBP + {$current_total}GBP = {$_total}GBP"; 
		}else{
			foreach($_SESSION['staff_order'] as $_barcode => $v){ 
				$data = [];
				$data['order_id']    = $order_id;	
				$data['quantity']	 = isset($v['qty']) ? $v['qty'] : 1;					
				$data['sku']     	 = $v['product_sku'];		 
				$data['barcode']  	 = $_barcode;
				$data['unit_price']	 = $v['price'];	
				$data['product_title']	 = $v['product_name'];	
				$data['staff_person_email']	 = trim($this->request->data['staff_person_email']);
				$this->manageQuantity($data);
				
				$mailFormate.='<tr>											 
							<td>'.$order_id.'</td>
							<td>'.$v['product_sku'].'</td>
							<td>'.$v['product_name'].'</td>
							<td>'.$_barcode.'</td>
							<td>'.$data['quantity'].'</td>
							<td>'.$data['unit_price'].'</td>
							</tr>';	
							
				$order_total += $data['quantity'] * $data['unit_price'];
			}
			unset($_SESSION['staff_order']);
		 
 		 
			
			if($this->request->data['staff_person_email'] != '' && $order_total >0 ){ 
			
				$mailBoday = '<p>Hi '. ucfirst($user['User']['first_name']).',<br><br>
				Order is created for you of following items:
				</p><br>';
				$mailBoday .= '<table border="1" style="border: 1px solid #c2c2c2;">
				<tr>
				<th>OrderID</th>
				<th>SKU</th>
				<th>Title</th>
				<th>Barcode</th>
				<th>Quantity</th>
				<th>Unit Price</th>
				</tr>'.$mailFormate .'</table>
				<p><strong>Order Total:</strong>GBP '. number_format($order_total,2).'<p>
				<br>Thanks,';
				
				App::uses('CakeEmail', 'Network/Email');
				$email = new CakeEmail('');
				$email->emailFormat('html');
				$email->from('info@euracogroup.co.uk');
				$email->to( array($this->request->data['staff_person_email'],'avadhesh.kumar@jijgroup.com'));					
				$email->subject('Staff Order');
				$email->send( $mailBoday );
				
				$mdata['status'] = 2;
				$mdata['msg'] = 'Order id is # '.$order_id;
			}
			
			
		}
 		echo json_encode($mdata);
		exit;
		
		 
	}
	
	public function ReturnOrder($order_id = 0)
	{
			$this->layout = '';
			$this->autoRender = false;
			$mdata = [];
			if( isset($this->request->data['order_id']) && $this->request->data['order_id'] > 0 )
			{
				$order_id	= $this->request->data['order_id'];
			}
			 
 			$this->loadModel('Product'); 
			$this->loadModel('BinLocation');		 
			$this->loadModel('CheckIn');
			$this->loadModel('StaffOrder');
			
 			$orders = $this->StaffOrder->find('all', array('conditions' => array('StaffOrder.order_id' => $order_id,'StaffOrder.status' => 'active')));
		 
  			$mailFormate = ''; $staff_person_email = '';
 			if(count($orders) > 0){
			 		
				foreach($orders as $val)
				{
					$mdata['msg'] = 'Order id # '.$order_id .' is returned successfully.';
					$staff_person_email 	= $val['StaffOrder']['staff_person_email'];
					$barcode 	= $val['StaffOrder']['barcode'];
					$sku		= $val['StaffOrder']['sku'];
					$location 	= $val['StaffOrder']['bin_location'];
					$quantity 	= $val['StaffOrder']['quantity'];
					$qty_bin 	= $val['StaffOrder']['available_qty_bin']; 
					$po_id	 	= explode(",",$val['StaffOrder']['po_id'])[0];	
							
					if($location != 'No Location')
					{
						$this->BinLocation->updateAll(	array('BinLocation.stock_by_location' => "BinLocation.stock_by_location + $qty_bin"),
													array('BinLocation.bin_location' => $location, 'BinLocation.barcode' => $barcode));
					}					
					 
					$this->CheckIn->updateAll( array('CheckIn.selling_qty' => "CheckIn.selling_qty - $qty_bin"),array('CheckIn.id' => $po_id));							  
					$product =	$this->Product->find('first', array('conditions' => array('Product.product_sku' => $sku)));
					// look for sku if exist  
					if( count( $product ) > 0 )
					{
						$ordertype = 'Delete';
						
						$currentStickLevel	=	$product['Product']['current_stock_level'];
						$qtyForUpdate		=	(int)$currentStickLevel + (int)$qty_bin;								
						
						$data['Product']['product_sku']		=	$product['Product']['product_sku'];
						$data['ProductDesc']['barcode']		=	$product['ProductDesc']['barcode'];
						$data['Product']['CurrentStock']	=	$currentStickLevel;
						
						$productData	=	json_decode(json_encode($data,0));
						App::import('Controller', 'Cronjobs');
						$productCant = new CronjobsController();
						$productCant->storeInventoryRecord( $productData, $qty_bin, $order_id, $ordertype, $barcode);
						
						$this->Product->updateAll(array('Product.current_stock_level' => $qtyForUpdate), array('Product.product_sku' => $sku));
							
					}
					
					$data = [];
					$return_date = date('Y-m-d H:i:s');
					$data['status'] =  "'return'";
 					$data['return_date'] = "'".$return_date. "'";
					$data['return_user'] = "'".$this->Session->read('Auth.User.id'). "'";
					$this->StaffOrder->updateAll($data , array( 'StaffOrder.order_id' => $order_id,'barcode'=>$val['StaffOrder']['barcode'] ) );
					
					$mailFormate.='<tr>											 
					<td>'.$order_id.'</td>
					<td>'.$sku.'</td>
					<td>'.$product['Product']['product_name'].'</td>
					<td>'.$product['ProductDesc']['barcode'].'</td>
					<td>'.$location.'</td>
					<td>'.$qty_bin.'</td>
					<td>'.$return_date.'</td>
					</tr>';		
				}
				
			}
			else{
				$mdata['msg'] = 'Order id # '.$order_id .' is not found.';
			}
			if($mailFormate != ''){ 
			 	 $mailBoday = 'Hi,<br><br>Your order has been successfully returned.<br><br><table style="border: 1px solid #c2c2c2;">
													<tr>
														<th>OrderID</th>
														<th>SKU</th>
														<th>Title</th>
														<th>Barcode</th>
														<th>Location</th>
														<th>Quantity</th>
														<th>Return Date</th>
													</tr>'.$mailFormate;
													
					App::uses('CakeEmail', 'Network/Email');
					$email = new CakeEmail('');
					$email->emailFormat('html');
					$email->from('info@euracogroup.co.uk');
					if($staff_person_email != ''){
						$email->to( array($staff_person_email,'avadhesh.kumar@jijgroup.com'));	
					}else{
						$email->to( array('avadhesh.kumar@jijgroup.com'));	
					}
									
					$email->subject('Staff Order returned');
					$email->send( $mailBoday );
			}
			echo json_encode($mdata);	 							
   	  }
	
 	public function manageQuantity($data = array()){
		
	 
		$this->loadModel('InventoryRecord');
		$this->loadModel('BinLocation');	
		$this->loadModel('StaffOrder');	
		$this->loadModel('CheckIn');
		
		if(count($data) > 0){
			 
			$order_id    = $data['order_id'];	
			$quantity 	 = $data['quantity'];					
			$sku     	 = $data['sku'];			
			$barcode  	 = $data['barcode'];
			$unit_price  = $data['unit_price'];
			$product_title	 = $data['product_title'];	
			$staff_person_email  =$data['staff_person_email']	;
			$po_name = array(); $posIds = array(); $pos = array(); $binname = array(); 
			$available_qty = 0; $available_qty_bin = 0;	
			
			$poDetail = $this->CheckIn->find( 'all', 
				array( 'conditions' => array( 'CheckIn.barcode' => $barcode, 'CheckIn.selling_qty != CheckIn.qty_checkIn','po_name !=""'),
						'order' => 'CheckIn.date  ASC ' ) );
			
			if(count($poDetail) > 0){
				$qt_count = 0; 				
				foreach($poDetail as $po){
					$poQty = $po['CheckIn']['qty_checkIn'] - $po['CheckIn']['selling_qty'];
					if($poQty > 0){						
						 for($i=0; $i <= $quantity ;$i++){						 
							 if($qt_count >= $quantity ){
								 break;
							 }else if(($poQty - $i)  > 0){							
								$po_name[$po['CheckIn']['id']][] = $po['CheckIn']['po_name'];	
								$available_qty++;							
								$qt_count++;
							}												
						 }						
					}
				}
			}
			 
			
			$getStock = $this->BinLocation->find( 'all', array( 'conditions' => array( 'BinLocation.barcode' => $barcode),'order' => 'priority ASC' ) );
			//pr($getStock );
			$bin_name = array();
			$finalData['bin_location'] = '';
			if(count($getStock) > 0){
				$qt_count = 0 ;
				foreach($getStock as $val){
					 $binData['id'] = $val['BinLocation']['id'];					
					 for($i=0; $i <= $quantity ;$i++){						 
						 if($qt_count >= $quantity ){
							 break;
						 }else if(($val['BinLocation']['stock_by_location'] - $i)  > 0){							
							$bin_name[$val['BinLocation']['id']][] = $val['BinLocation']['bin_location'];	
							$available_qty_bin++;						
							$qt_count++;
						}												
					 }							
				}				
			}							
						
			
			if(count($po_name) > 0){
				foreach(array_keys($po_name) as $k){
					$qts   = count($po_name[$k]);	
					$pos[] = $po_name[$k][0];
					$posIds[] = $k;
					$this->CheckIn->updateAll( array('CheckIn.selling_qty' =>  "CheckIn.selling_qty + $qts"),array('CheckIn.id' => $k));
				}
			}
							
			if(count($bin_name) > 0)
			{
				foreach(array_keys($bin_name) as $k){					
					$qts = count($bin_name[$k]);	
					$binname[] = $bin_name[$k][0];	
					$finalData = array();						 
					$finalData['available_qty_check_in'] = $available_qty;
					$finalData['available_qty_bin'] = $qts;
					$finalData['order_id']    	    = $order_id;	
					$finalData['quantity']      	= $quantity;			
					$finalData['sku']	   			= $sku;	
					$finalData['unit_price']	   	= $unit_price;	 							
					$finalData['barcode']  			= $barcode;	
					$finalData['product_title']  	= $product_title;	
					$finalData['po_name']  			= implode(",",$pos);	
					$finalData['po_id']    			= implode(",",$posIds);	
					$finalData['bin_location']  	= $bin_name[$k][0];	
					$finalData['staff_person_email']=$staff_person_email;
					$finalData['created_by_name'] 	= $this->Session->read('Auth.User.username').'_'.$this->Session->read('Auth.User.pc_name') ;
					$finalData['created_by_userid'] = $this->Session->read('Auth.User.id') ;					
					$finalData['created_date'] 		= date('Y-m-d H:i:s');		
					$this->StaffOrder->saveAll( $finalData );
				
					$this->BinLocation->updateAll( array('BinLocation.stock_by_location' =>  "BinLocation.stock_by_location - $qts"),array('BinLocation.id' => $k));
					
					/**
					Updating current stock of product.
					*/
					$current_qty = 0;
					$_product = $this->Product->find('first',array('conditions'=>array('Product.product_sku' => $sku), 'fields' => array('current_stock_level'),'order'=>'Product.id DESC'));
					
					if(count($_product) > 0){
					
						$current_stock = $_product['Product']['current_stock_level'];														
						$current_qty   = $_product['Product']['current_stock_level'] - $qts;
						if($current_qty < 0){
							$current_qty = 0;
						}
						
						$this->Product->updateAll( array('Product.current_stock_level' => $current_qty),array('Product.product_sku' => $sku));
						 
						
						$inv_data = array();
						$inv_data['action_type']  	 	= 'Staff order';
						$inv_data['sku'] 		  	 	= $sku;					
						$inv_data['barcode']	  	 	= $barcode;
						$inv_data['currentStock'] 		= $current_stock;
						$inv_data['quantity'] 	   		= $qts;
						$inv_data['status'] 	   		= 'staff_order';
						$inv_data['after_maniplation'] 	= $current_qty;
						$inv_data['location'] 			= $bin_name[$k][0];
						$inv_data['split_order_id']		= $order_id;
						$inv_data['date'] 		   		= date('Y-m-d' );
						$this->InventoryRecord->saveAll( $inv_data );
						 
 					}
					// end of stock updation.						
 				}
			} 
			else {
					$finalData = array();
					$finalData['available_qty_check_in'] = $available_qty;
					$finalData['available_qty_bin'] = '0';
					$finalData['order_id']    	    = $order_id;	
					$finalData['quantity']      	= $quantity;	
					$finalData['sku']	   			= $sku;		
					$finalData['unit_price']	   	= $unit_price;							
					$finalData['barcode']  			= $barcode;	
					$finalData['product_title']  	= $product_title;
					$finalData['po_name']  			= implode(",",$pos);	
					$finalData['po_id']    			= implode(",",$posIds);	
					$finalData['bin_location']  	= 'No_Location';		
					$finalData['staff_person_email']=$staff_person_email;	
					$finalData['created_by_name'] 	= $this->Session->read('Auth.User.username').'_'.$this->Session->read('Auth.User.pc_name') ;
					$finalData['created_by_userid'] = $this->Session->read('Auth.User.id') ;	
					$finalData['created_date'] 		= date('Y-m-d H:i:s');			
					$this->StaffOrder->saveAll( $finalData );
			}
			
			
		}					
	}
	
	public function downloadSheet($sdate = null,$edate = null)
	{
	
		$this->layout = '';
		$this->loadModel('User');	
		$this->loadModel('StaffOrder');		
		$this->loadModel('PurchaseOrder');
 		if($sdate == ''){
			$sdate = date('Y-m', strtotime(date('Y-m')." -1 month")).'-27';	
			$edate = date('Y-m').'-27';		
		}
		$user_byid = $user_byemail = [];
		$users = $this->User->find('all', array('fields' => array('id','first_name','last_name','email','country')));
		foreach($users as $user){
			$user_byid[$user['User']['id']] = $user['User']['first_name'].' '.$user['User']['last_name'];
			$user_byemail[$user['User']['email']] = $user['User']['first_name'].' '.$user['User']['last_name'];
		}
		
		$orders = $this->StaffOrder->find('all', array('conditions' => array('created_date BETWEEN ? AND ?'=>[$sdate,$edate])));
		 
		$sep = ",";
		$file_name = 'StaffOrders_'.$sdate.'_to_'.$edate.'.csv';
		$content ='Order Id'.$sep.'Sku' .$sep.'Barcode'.$sep.'Quantity'.$sep.'Unit Price'.$sep.'Status'.$sep.'Order Date'.$sep.'Created By'.$sep.'Staff Person'.$sep.'Return date'.$sep.'Return Taken By';
		file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n");	
		if(count($orders) > 0){	
			foreach($orders as $v){
			
			$created_by = $user_byid[$v['StaffOrder']['created_by_userid']];
			$staff_person = $user_byemail[$v['StaffOrder']['staff_person_email']];
			$return_user = '';
			if($v['StaffOrder']['return_user'] > 0){
				$return_user = $user_byid[$v['StaffOrder']['return_user']]; 
			}
			
 			$content = $v['StaffOrder']['order_id'].$sep. $v['StaffOrder']['sku'].$sep. $v['StaffOrder']['barcode'].$sep. $v['StaffOrder']['quantity'].$sep. $v['StaffOrder']['unit_price'].$sep.$v['StaffOrder']['status'].$sep.$v['StaffOrder']['created_date'].$sep.$created_by .$sep.$staff_person.$sep.$v['StaffOrder']['return_date'].$sep.$return_user;
			
			file_put_contents(WWW_ROOT.'logs/'.$file_name,$content."\r\n", FILE_APPEND | LOCK_EX);	
				
			}
 		}
		
		$result =  WWW_ROOT.'logs/'.$file_name; 
		header('Content-Description: File Transfer');
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename='.basename($result));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($result));   
		readfile($result); 
		exit; 
			
   	
	}
	    
}

?>
