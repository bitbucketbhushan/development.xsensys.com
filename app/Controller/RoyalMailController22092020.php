<?php
//error_reporting(0);
class RoyalMailController extends AppController
{ 
    
    var $name = "RoyalMail";    
    var $components = array('Session', 'Common', 'Upload','Auth');    
    var $helpers = array('Html','Form','Common','Session');
   
   public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('applyRoyalMail','createManifestCron','printManifest'));//customApply
    }
   
	private function royalMailAuth()
	{
			$data = Configure::read( 'royalmail_auth' );
			$x_rmg_password = $data['x_rmg_password'];			
			$nonce_date_pwd = pack("H*", sha1($x_rmg_password));			
			$data['x_rmg_password'] = base64_encode($nonce_date_pwd); 
			return $data;	
	}
	 
	public function uploadManifest(){
		 $this->layout = 'index';
		 
	}
	public function uploadManifestSave(){
		
		$this->layout = 'index';		 
		$this->loadModel('MergeUpdate');	 
		 
		$filetemppath =	$this->data['rm_manifest']['uploadfile']['tmp_name']; 
		$pathinfo     = pathinfo($this->data['rm_manifest']['uploadfile']['name']);
             
		$fileNameWithoutExt = $pathinfo['filename'];
		$fileExtension = strtolower($pathinfo['extension']);
		
		if($fileExtension == 'csv')
		{
 			$filename = $fileNameWithoutExt.'_'.date('ymd_his').'_'.$this->Session->read('Auth.User.username').".".$fileExtension;		
			$upload_file = WWW_ROOT. 'royalmail'.DS.'rm_tracking_ids'.DS.$filename ; 
			move_uploaded_file($filetemppath,$upload_file); 
			
			$row = 0; 
			$handle = fopen($upload_file, "r");
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			  
			   $row++;			
			   if($row > 1){
					//echo "<br>". $data[1];
					//echo " = ". $data[2];			    
					$dataUpdate['track_id']   = "'".$data[1]."'";				
					$conditions = array('MergeUpdate.product_order_id_identify' => $data[2]);	
					$this->MergeUpdate->updateAll( $dataUpdate, $conditions ); 
					file_put_contents(WWW_ROOT .'logs/rm_track_id_'.date('ymd').'.log' , $data[2]."\t".$data[1]."\n",  FILE_APPEND|LOCK_EX);					
				}					
			   		
			}
			
			fclose($handle);
			
			if($row > 0){
				$this->Session->setFlash($row . " items track_id updated.", 'flash_success');
			} 
			
		 }
		 else{
			 $this->Session->setFlash("Invalid file format, Please upload CSV file.", 'flash_danger');
		 }
		 
		$this->redirect($this->referer());
 		 
	} 
	 
	public function testDim(){
		$this->loadModel( 'ProductDesc' );
		$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name')));
		if(count($product) > 0)	{
			$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
			$_height[] = $product['ProductDesc']['height'];
			$length = $product['ProductDesc']['length'];	
			$width  = $product['ProductDesc']['width']; 
			}
	}
	
	public function applyRoyalMail($order_id = null){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'PostalServiceDesc' );
		$royal_sur_charge = 0.40;
		$fuel_percent     = 3;
		$dhl 			  = 1.20;
		$dhl_additional_cost = 0.42;
			
		$currency 		  = $this->getCurrencyRate();
		$exchange_rate	  = $currency ['EUR'];  
		$return 		  = array();
		$msg 			  = '';
		 
		$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id,'MergeUpdate.status' => 0 , 'MergeUpdate.royalmail' => 0, 'MergeUpdate.delevery_country' => 'United Kingdom')));	
		
		if(count($orders) >0){
		
			foreach($orders  as $orderItems){
			 
			$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $orderItems['MergeUpdate']['product_order_id_identify'],'MergeUpdate.status' => 0 ,'MergeUpdate.royalmail' => 0,'MergeUpdate.delevery_country' => 'United Kingdom')));	
					
				if(count($orderItem) > 0)
				{
						$this->loadModel( 'Product' );
						$this->loadModel( 'ProductDesc' );
						$this->loadModel( 'Country' );	
					
						$quantity 	   = $orderItem['MergeUpdate']['quantity'];
						$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
						$packet_length = $orderItem['MergeUpdate']['packet_length'];
						$packet_width  = $orderItem['MergeUpdate']['packet_width'];
						$packet_height = $orderItem['MergeUpdate']['packet_height'];
						$order_id 	   = $orderItem['MergeUpdate']['order_id'];
 						
						$_height = array(); $_weight = array(); $length = $width = 0; $data = array();
						
						$pos    = strpos($orderItem['MergeUpdate']['sku'],",");
						if ($pos === false) {
							$val  = $orderItem['MergeUpdate']['sku'];
							$s    = explode("X", $val);
							$_qty = $s[0]; 
							$_sku = $s[1];		
							$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name')));
							if(count($product) > 0){
								$_weight[] = $product['ProductDesc']['weight'] * $_qty;		
								$_height[] = $product['ProductDesc']['height'];		
								$length = $product['ProductDesc']['length'];
								$width = $product['ProductDesc']['width']; 
							 }
							
						}else{			
							$sks = explode(",",$orderItem['MergeUpdate']['sku']);
							$_weight = array();					 
							foreach($sks as $val){
								$s = explode("X", $val);
								$_qty = $s[0]; 
								$_sku = $s[1]; 
								$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name')));
								if(count($product) > 0)	{
									$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
									$_height[] = $product['ProductDesc']['height'];
									$length = $product['ProductDesc']['length'];	
									$width  = $product['ProductDesc']['width']; 
								}				
							}							 			
						}
						 
						if($packet_weight == 0){
							$packet_weight = array_sum($_weight);	
							$data['packet_weight'] = $packet_weight;
						}
						if($packet_height == 0){
							$packet_height = array_sum($_height);
							$data['packet_height'] = $packet_height;
						}
						if($packet_length == 0){
							$packet_length = $length; 
							$data['packet_length'] = $packet_length; 
						}
						if($packet_width == 0){
							$packet_width = $width;  
							$data['packet_width'] = $packet_width;
						}
						
						 
 						/*-------------------Royal Mail Calculations------------------------*/		
						 $final_dim = array();
						 if($packet_weight > 2){
							 $royalMailPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
									  
						 }else{
							 $dim = array($packet_width,$packet_height,$packet_length);
							 asort($dim);
							 $final_dim = array_values($dim) ;							
							
							 $royalMailPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
						 }
						 
						$all_royal = array(); 
						$all_postnl = array(); 
						$royalFee = 0; $postNLFee = 0;
						if(count($royalMailPostal) > 0)
						{
							foreach($royalMailPostal as $rv){
						 
								$per_item 			= $rv['PostalServiceDesc']['per_item'];
								$psd_id 			= $rv['PostalServiceDesc']['id'];
								
								$fuel_surcharge	 	= ($per_item  * $fuel_percent) / 100;
								$additional_fee 	= ($dhl + $dhl_additional_cost) * $packet_weight ;							
								$all_royal[$psd_id] = ($per_item + $fuel_surcharge + $additional_fee) ;	 
									 
							} 
							//pr($all_royal);
							$royalFee   = min($all_royal);	
							$post_id    = array_search($royalFee, $all_royal); 
							$post_royal = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));	
							
							//echo $royalFee;						 
							
							/*-------------------PostNL Calculations------------------------*/
							$postNlPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey',
											'Location.county_name' => 'United Kingdom', 'PostalServiceDesc.max_weight >=' => $packet_weight, 'PostalServiceDesc.max_length >=' => $packet_length, 'PostalServiceDesc.max_width >=' => $packet_width, 'PostalServiceDesc.max_height >=' => $packet_height, 'PostalServiceDesc.courier' => 'Belgium Post')));
											
							$all_postnl = array();
							foreach($postNlPostal as $pv){
								$per_item 	= $pv['PostalServiceDesc']['per_item'];
								$per_kg 	= $pv['PostalServiceDesc']['per_kilo'];
								$weightKilo = $pv['PostalServiceDesc']['max_weight'];						
								$psd_id 	= $pv['PostalServiceDesc']['id'];
							
								$all_postnl[$psd_id] = (($per_item + ($per_kg * $orderItem['MergeUpdate']['packet_weight'])) / $exchange_rate ) + ($dhl * $orderItem['MergeUpdate']['packet_weight']);
							} 
							
							if(count($all_postnl) >0){	
								$postNLFee  = min($all_postnl);	
							}
							// pr($postNlPostal);
							 // pr($all_postnl);
							//  echo $postNLFee .' royalFee='.$royalFee ." = ".$orderItem['MergeUpdate']['product_order_id_identify'];
									 
							 $weight = $packet_weight * 1000;
							
							/*-------------------Compare And Replace------------------------*/
							if(($postNLFee > $royalFee) || ($weight	>= 2000)) {
										
									 //echo $orderItem['MergeUpdate']['product_order_id_identify'].'=='.$postNLFee .'>'. $royalFee;exit;
									 //$data['royalmail'] 			= '1';
									 
									if($orderItem['MergeUpdate']['packet_weight'] == 0){
										$data['packet_weight'] 			= $packet_weight;
									}
									$data['service_name'] 		= $post_royal['PostalServiceDesc']['service_name'];
									$data['track_id'] 			= '';
									$data['reg_post_number'] 	= '';
									$data['reg_num_img'] 		= '';
									$data['postal_service'] 	= 'Standard';
									$data['provider_ref_code'] 	= $post_royal['PostalServiceDesc']['provider_ref_code'];
									$data['service_id'] 		= $post_royal['PostalServiceDesc']['id'];
									$data['service_provider'] 	= $post_royal['PostalServiceDesc']['courier'];								 
									$data['template_id'] 		= $post_royal['PostalServiceDesc']['template_id'];  
									$data['id'] 				= $orderItem['MergeUpdate']['id'];
									
									//file_put_contents(WWW_ROOT."logs/royalmail_".$orderItem['MergeUpdate']['product_order_id_identify'].".log",print_r($_details,true));
									 
									file_put_contents(WWW_ROOT .'logs/pnl_to_rm_app_'.date('ymd').'.log' , $orderItem['MergeUpdate']['product_order_id_identify']."\t".$orderItem['MergeUpdate']['service_id']."\n",  FILE_APPEND|LOCK_EX);	
									
									$this->loadModel( 'MergeUpdate' );				 
									$this->MergeUpdate->saveAll( $data );									
								echo	$msg .= $orderItem['MergeUpdate']['product_order_id_identify']."\t";
									 
							}else{
								 $msg =  ' No order found in condition - 1.<br>';
							}
							/*------------------end--------------------------------*/
						}else{
							 $msg = ' This order may have over weight or any other issue.';
							
						}
				 
			  }
				else{
				 $msg =  ' No order found.<br>';
				}
				
				file_put_contents(WWW_ROOT."logs/royalmail_app_by_order.log", date('Y-m-d H:i:s')."\t". $order_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);	
				 
			}
		}
	}
	
	public function applyRoyalMailByOrder($split_order_id = null){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' ); 
		$msg = '';
		 
		$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_order_id,'MergeUpdate.status' => 0, 'MergeUpdate.royalmail' => 0,'MergeUpdate.service_provider' => 'Royalmail')));	
		//'MergeUpdate.royalmail' => 0,
		//'MergeUpdate.status' => 0 
 		   
		 //966596 966653
		if(count($orderItem) > 0)
		{
			$this->loadModel( 'Product' );
			$this->loadModel( 'ProductDesc' );
			$this->loadModel( 'Country' );	
		
			$quantity 	   = $orderItem['MergeUpdate']['quantity'];
			$packet_weight = $orderItem['MergeUpdate']['packet_weight']; 
			$order_id 	   = $orderItem['MergeUpdate']['order_id'];
			$weight		   = $packet_weight * 1000 ; 
			
			//echo $orderItem['MergeUpdate']['product_order_id_identify'].'=='.$postNLFee .'>'. $royalFee;exit;
			App::import( 'Controller' , 'Cronjobs' );		
			$objController 	= new CronjobsController();
			$getorderDetail	= $objController->getOpenOrderById($order_id);
			$cInfo = $getorderDetail['customer_info'];
			$order->TotalsInfo = $getorderDetail['totals_info'];
			
			$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
			
			$Address1 = $cInfo->Address->Address1;
			$Address2 = $cInfo->Address->Address2;
			$type = 2; 
			if($orderItem['MergeUpdate']['service_name'] == 'royalmail_24'){
				$type = 1;
			}
			$service_format = 'P';
			if($orderItem['MergeUpdate']['service_name'] == 'royalmail_LL_48'){
				/*  F	Inland Large Letter, L	Inland Letter, N	Inland format Not Applicable, P	Inland Parcel */
				$service_format = 'F';
			}
			$_details = array('merge_id'=> $order_id,
				'split_order_id'=> $orderItem['MergeUpdate']['product_order_id_identify'],
				'sub_total' => number_format($order->TotalsInfo->Subtotal,2),
				'order_total'=> number_format($order->TotalsInfo->TotalCharge,2),
				'postage_cost' => number_format($order->TotalsInfo->PostageCost,2),
				'order_currency'=>$order->TotalsInfo->Currency,			
				'weight'=>  $weight ,
				'Company'=>$cInfo->Address->Company,
				'FullName'=> utf8_decode($cInfo->Address->FullName),
				'Address1' =>utf8_decode($Address1),
				'Address2' =>utf8_decode($Address2),
				'Town' =>utf8_decode($cInfo->Address->Town),
				'Region' =>utf8_decode($cInfo->Address->Region),
				'PostCode' =>$cInfo->Address->PostCode,
				'CountryName'=>$cInfo->Address->Country,
				'CountryCode' =>$country_data['Country']['iso_2'],
				'PhoneNumber'=>$cInfo->Address->PhoneNumber	,
				'sub_source' => $getorderDetail['sub_source'],
				'type' 		=> $type,
				'service_format' => $service_format,
				'item_count'=> 1,
				'building_name' => '',
				'building_number' => 0,
			); 		 
			
			$data['royalmail'] 			= '1';			 
			$data['id'] 				= $orderItem['MergeUpdate']['id'];
			
			//file_put_contents(WWW_ROOT."logs/royalmail_".$orderItem['MergeUpdate']['product_order_id_identify'].".log",print_r($_details,true));
			
			file_put_contents(WWW_ROOT .'logs/pnl_to_rm_'.date('ymd').'.log' , $orderItem['MergeUpdate']['product_order_id_identify']."\t".$orderItem['MergeUpdate']['service_id']."\n",  FILE_APPEND|LOCK_EX);	
			
			$this->royalMailServiceStart($_details, $data);
			$msg .= $orderItem['MergeUpdate']['product_order_id_identify']."\t";
		
		}
		else{
			$msg =  ' No order found in condition - 1.<br>';
		}
		 
		file_put_contents(WWW_ROOT."logs/royalmail_by_order.log", date('Y-m-d H:i:s')."\t". $split_order_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);	
	}
 		
	public function customApply($split_order_id = null){
		 //die('Uncomment Code.');
		
		$split_order_id = '1713299-1';
		$postcode = 'LE3 8PZ'; 
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'PostalServiceDesc' );
		$royal_sur_charge = 0.40;
		$fuel_percent     = 3;
		$dhl 			  = 1.20;
		$dhl_additional_cost = 0.42;
			
		$currency 		  = $this->getCurrencyRate();
		$exchange_rate	  = $currency ['EUR'];  
		$return 		  = array();
		$msg 			  = '';
		$ch_rm = $this->Royalmail->find('first',array('conditions' => array('Royalmail.split_order_id' => $split_order_id)));	 
		if(count($ch_rm ) == 0){ 
		$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $split_order_id,'MergeUpdate.delevery_country' => 'United Kingdom')));	
		//'MergeUpdate.royalmail' => 0,
		//'MergeUpdate.status' => 0 
		  
		   
		 //966596 966653
		if(count($orderItem) > 0)
		{
				$this->loadModel( 'Product' );
				$this->loadModel( 'ProductDesc' );
				$this->loadModel( 'Country' );	
  			
				$quantity 	   = $orderItem['MergeUpdate']['quantity'];
				$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
				$packet_length = $orderItem['MergeUpdate']['packet_length'];
				$packet_width  = $orderItem['MergeUpdate']['packet_width'];
				$packet_height = $orderItem['MergeUpdate']['packet_height'];
				$order_id 	   = $orderItem['MergeUpdate']['order_id'];
				
				$_height = array(); $_weight = array(); $length = $width = 0; $data = array(); 
						
				$pos    = strpos($orderItem['MergeUpdate']['sku'],",");
				
				if ($pos === false) {
					$val  = $orderItem['MergeUpdate']['sku'];
					$s    = explode("X", $val);
					$_qty = $s[0]; 
					$_sku = $s[1];		
					$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name')));
					if(count($product) > 0){
						$_weight[] = $product['ProductDesc']['weight'] * $_qty;		
						$_height[] = $product['ProductDesc']['height'];		
						$length = $product['ProductDesc']['length'];
						$width = $product['ProductDesc']['width'];										
					}
					
				}else{			
					$sks = explode(",",$orderItem['MergeUpdate']['sku']);
					$_weight = array();					 
					foreach($sks as $val){
						$s = explode("X", $val);
						$_qty = $s[0]; 
						$_sku = $s[1]; 
						$product = $this->ProductDesc->find('first',array('conditions' => array('Product.product_sku' => $_sku),'fields'=>array('length','width','height','weight','Product.category_name')));
						if(count($product) > 0)	{
							$_weight[] = $product['ProductDesc']['weight'] * $_qty;										
							$_height[] = $product['ProductDesc']['height'];
							$length = $product['ProductDesc']['length'];	
							$width  = $product['ProductDesc']['width']; 
						}				
					}		
								
				}
				
				if($packet_weight == 0){
					$packet_weight = array_sum($_weight);	
					$data['packet_weight'] = $packet_weight;
				}
				if($packet_height == 0){
					$packet_height = array_sum($_height);
					$data['packet_height'] = $packet_height;
				}
				if($packet_length == 0){
					$packet_length = $length; 
					$data['packet_length'] = $packet_length; 
				}
				if($packet_width == 0){
					$packet_width = $width;  
					$data['packet_width'] = $packet_width;
				}
				/*-------------------Royal Mail Calculations------------------------*/		
				
				 if($packet_weight > 2){
					 $royalMailPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
							  
							 
				 }else{
					 $dim = array($packet_width,$packet_height,$packet_length);
					 asort($dim);
					 $final_dim = array_values($dim) ;							
					
					 $royalMailPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
				 }
				 
				$all_royal = array(); 
				$all_postnl = array(); 
				$royalFee = 0; $postNLFee = 0;
				if(count($royalMailPostal) > 0)
				{
					foreach($royalMailPostal as $rv){
				 
						$per_item 			= $rv['PostalServiceDesc']['per_item'];
						$psd_id 			= $rv['PostalServiceDesc']['id'];
						
						$fuel_surcharge	 	= ($per_item  * $fuel_percent) / 100;
						$additional_fee 	= ($dhl + $dhl_additional_cost) * $packet_weight ;							
						$all_royal[$psd_id] = ($per_item + $fuel_surcharge + $additional_fee) ;	 
							 
					} 
					//pr($all_royal);
					$royalFee   = min($all_royal);	
					$post_id    = array_search($royalFee, $all_royal); 
					$post_royal = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));	
					
					//echo $royalFee;						 
					
					/*-------------------PostNL Calculations------------------------*/
					$postNlPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey',
									'Location.county_name' => 'United Kingdom', 'PostalServiceDesc.max_weight >=' => $packet_weight, 'PostalServiceDesc.max_length >=' => $packet_length, 'PostalServiceDesc.max_width >=' => $packet_width, 'PostalServiceDesc.max_height >=' => $packet_height, 'PostalServiceDesc.courier' => 'Belgium Post')));
					 
					foreach($postNlPostal as $pv){
						$per_item 	= $pv['PostalServiceDesc']['per_item'];
						$per_kg 	= $pv['PostalServiceDesc']['per_kilo'];
						$weightKilo = $pv['PostalServiceDesc']['max_weight'];						
						$psd_id 	= $pv['PostalServiceDesc']['id'];
					
						$all_postnl[$psd_id] = (($per_item + ($per_kg * $orderItem['MergeUpdate']['packet_weight'])) / $exchange_rate ) + ($dhl * $orderItem['MergeUpdate']['packet_weight']);
					} 
					
					$postNLFee  = min($all_postnl);	
					//echo $postNLFee .'== '.$royalFee ." = ".$orderItem['MergeUpdate']['product_order_id_identify'];
						 	 
					$weight	= $packet_weight * 1000;	
					 
					/*-------------------Compare And Replace------------------------*/
					if(($royalFee) || ($weight	>= 2000)) {
								
							 //echo $orderItem['MergeUpdate']['product_order_id_identify'].'=='.$postNLFee .'>'. $royalFee;exit;
							App::import( 'Controller' , 'Cronjobs' );		
							$objController 	= new CronjobsController();
							$getorderDetail	= $objController->getOpenOrderById($order_id);
							$cInfo = $getorderDetail['customer_info'];
							$order->TotalsInfo = $getorderDetail['totals_info'];
							
							$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
							
							$Address1 = $cInfo->Address->Address1;
							$Address2 = $cInfo->Address->Address2;
							$type = 2; 
							if($post_royal['PostalServiceDesc']['service_name'] == 'royalmail_24'){
								$type = 1;
							}
							
							$service_format = 'P';
							if($post_royal['PostalServiceDesc']['service_name'] == 'royalmail_LL_48'){
								/*  F	Inland Large Letter, L	Inland Letter, N	Inland format Not Applicable, P	Inland Parcel */
								$service_format = 'F';
							}
							
							$_details = array('merge_id'=> $order_id,
								'split_order_id'=> $orderItem['MergeUpdate']['product_order_id_identify'],
								'sub_total' => number_format($order->TotalsInfo->Subtotal,2),
								'order_total'=> number_format($order->TotalsInfo->TotalCharge,2),
								'postage_cost' => number_format($order->TotalsInfo->PostageCost,2),
								'order_currency'=>$order->TotalsInfo->Currency,			
								'weight'=>  $weight ,
								'Company'=>$cInfo->Address->Company,
								'FullName'=> $this->replaceFrenchChar($cInfo->Address->FullName),
								'Address1' =>utf8_decode($Address1),
								'Address2' =>utf8_decode($Address2),
								'Town' =>utf8_decode($cInfo->Address->Town),
								'Region' =>utf8_decode($cInfo->Address->Region),
								'PostCode' =>$postcode,//$cInfo->Address->PostCode,
								'CountryName'=>$cInfo->Address->Country,
								'CountryCode' =>$country_data['Country']['iso_2'],
								'PhoneNumber'=>$cInfo->Address->PhoneNumber	,
								'sub_source' => $getorderDetail['sub_source'],
								'type' 		=> $type,
								'service_format' => $service_format,
								'item_count'=> 1,
								'building_name' => '',
								'building_number' => 0,
							);
						
							$data['royalmail'] 			= '1';
							$data['service_name'] 		= $post_royal['PostalServiceDesc']['service_name'];
							$data['track_id'] 			= '';
							$data['reg_post_number'] 	= '';
							$data['reg_num_img'] 		= '';
							$data['postal_service'] 	= 'Standard';
							$data['provider_ref_code'] 	= $post_royal['PostalServiceDesc']['provider_ref_code'];
							$data['service_id'] 		= $post_royal['PostalServiceDesc']['id'];
							$data['service_provider'] 	= $post_royal['PostalServiceDesc']['courier'];								 
							$data['template_id'] 		= $post_royal['PostalServiceDesc']['template_id'];  
							$data['id'] 				= $orderItem['MergeUpdate']['id'];
							
							//file_put_contents(WWW_ROOT."logs/royalmail_".$orderItem['MergeUpdate']['product_order_id_identify'].".log",print_r($_details,true));
							 
							file_put_contents(WWW_ROOT .'logs/pnl_to_rm_'.date('ymd').'.log' , $orderItem['MergeUpdate']['product_order_id_identify']."\t".$orderItem['MergeUpdate']['service_id']."\n",  FILE_APPEND|LOCK_EX);	
							
							$this->royalMailServiceStart($_details, $data);
							$msg .= $orderItem['MergeUpdate']['product_order_id_identify']."\t";
							 
					}else{
						 $msg =  ' No order found in condition - 1.<br>';
					}
					/*------------------end--------------------------------*/
				}else{
					 $msg = ' This order may have over weight or any other issue.';
					
				}
		 
		  }
		  else{
			 $msg =  ' No order found.<br>';
			}
		
		file_put_contents(WWW_ROOT."logs/royalmail_by_order.log", date('Y-m-d H:i:s')."\t". $split_order_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);	
		}else{
			echo 'already exit. please cancel https://app.rmdmo.royalmail.com before generate new.';
		}
	}
	public function Check($order_id = null){
		
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'PostalServiceDesc' );
		$royal_sur_charge = 0.40;
		$fuel_percent     = 3;
		$dhl 			  = 1.20;
		$dhl_additional_cost = 0.42;
			
		$currency 		  = $this->getCurrencyRate();
		$exchange_rate	  = $currency ['EUR'];  
		$return 		  = array();
		$msg 			  = '';
		 if($order_id){
		 $orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id)));	
		 }else{
		$orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status' => 0 , 'MergeUpdate.royalmail' => 0, 'MergeUpdate.delevery_country' => 'United Kingdom')));	
		}
		if(count($orders) >0){
		
			foreach($orders  as $orderItems){
			 
			$orderItem = $this->MergeUpdate->find('first',array('conditions' => array('MergeUpdate.product_order_id_identify' => $orderItems['MergeUpdate']['product_order_id_identify'], 'MergeUpdate.delevery_country' => 'United Kingdom')));	
					
				if(count($orderItem) > 0)
				{
						$this->loadModel( 'Product' );
						$this->loadModel( 'ProductDesc' );
						$this->loadModel( 'Country' );	
					
						$quantity 	   = $orderItem['MergeUpdate']['quantity'];
						$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
						$packet_length = $orderItem['MergeUpdate']['packet_length'];
						$packet_width  = $orderItem['MergeUpdate']['packet_width'];
						$packet_height = $orderItem['MergeUpdate']['packet_height'];
						$order_id 	   = $orderItem['MergeUpdate']['order_id'];
						
						/*-------------------Royal Mail Calculations------------------------*/		
						
						 if($packet_weight > 2){
							 $royalMailPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
									  
									 
						 }else{
							 $dim = array($packet_width,$packet_height,$packet_length);
							 asort($dim);
							 $final_dim = array_values($dim) ;							
							
							 $royalMailPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
						 }
						 
						$all_royal = array(); 
						$all_postnl = array(); 
						$royalFee = 0; $postNLFee = 0;
						if(count($royalMailPostal) > 0)
						{
							foreach($royalMailPostal as $rv){
						 
								$per_item 			= $rv['PostalServiceDesc']['per_item'];
								$psd_id 			= $rv['PostalServiceDesc']['id'];
								
								$fuel_surcharge	 	= ($per_item  * $fuel_percent) / 100;
								$additional_fee 	= ($dhl + $dhl_additional_cost) * $packet_weight ;							
								$all_royal[$psd_id] = ($per_item + $fuel_surcharge + $additional_fee) ;	 
									 
							} 
							//pr($all_royal);
							$royalFee   = min($all_royal);	
							$post_id    = array_search($royalFee, $all_royal); 
							$post_royal = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $post_id)));	
							
							//echo $royalFee;						 
							
							/*-------------------PostNL Calculations------------------------*/
							$postNlPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey',
											'Location.county_name' => 'United Kingdom', 'PostalServiceDesc.max_weight >=' => $packet_weight, 'PostalServiceDesc.max_length >=' => $packet_length, 'PostalServiceDesc.max_width >=' => $packet_width, 'PostalServiceDesc.max_height >=' => $packet_height, 'PostalServiceDesc.courier' => 'Belgium Post')));
											
							$all_postnl = array();
							foreach($postNlPostal as $pv){
								$per_item 	= $pv['PostalServiceDesc']['per_item'];
								$per_kg 	= $pv['PostalServiceDesc']['per_kilo'];
								$weightKilo = $pv['PostalServiceDesc']['max_weight'];						
								$psd_id 	= $pv['PostalServiceDesc']['id'];
							
								$all_postnl[$psd_id] = (($per_item + ($per_kg * $orderItem['MergeUpdate']['packet_weight'])) / $exchange_rate ) + ($dhl * $orderItem['MergeUpdate']['packet_weight']);
							} 
							if(count($all_postnl)){		$postNLFee  = min($all_postnl);	}
							// pr($postNlPostal);
							 // pr($all_postnl);
							//  echo $postNLFee .' royalFee='.$royalFee ." = ".$orderItem['MergeUpdate']['product_order_id_identify'].'<br>';
							   
							$weight	= $packet_weight * 1000;	
							
							/*-------------------Compare And Replace------------------------*/
							if(($postNLFee > $royalFee) || ($weight	>= 2000)) {
										
									echo '<br>Order id ='.$orderItem['MergeUpdate']['product_order_id_identify'].' PostNLFee='. number_format($postNLFee,3) .' RoyalFee='. number_format($royalFee,3);
									echo '<br>';	//exit;
									 
									 
							}else{
								 $msg =  ' No order found in condition - 1.<br>';
							}
							/*------------------end--------------------------------*/
						}else{
							 $msg = ' This order may have over weight or any other issue.';
							
						}
				 
			  }
				else{
				 $msg =  ' No order found.<br>';
				}
				
				file_put_contents(WWW_ROOT."logs/royalmail_app_by_order.log", date('Y-m-d H:i:s')."\t". $order_id."\t".$msg ."\n", FILE_APPEND|LOCK_EX);	
				 
			}
		}
	}
	
 	public function royalMailServiceStart($details = null, $mergeupdate = null)
	{ 		  	
		$data = $this->royalMailGetToken($details);	
		if(isset($data['token'])){		
			$this->royalMailCreateShipment($details, $mergeupdate, $data['token']);
		} 
	}
	
	public function royalMailGetToken($details = null){
			$auth = $this->royalMailAuth();
			$x_ibm_client_id = $auth['x_ibm_client_id'];
			$x_ibm_client_secret = $auth['x_ibm_client_secret'];
			$x_rmg_user_name = $auth['x_rmg_user_name'];
			$x_rmg_password = $auth['x_rmg_password'];
			 
			  $curl = curl_init();
			  curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.royalmail.net/shipping/v2/token",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_SSL_VERIFYHOST=> false,
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLINFO_HEADER_OUT => true,			  
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
				"accept: application/json",
				"Accept-Encoding: gzip,deflate",
				"Host: api.royalmail.net",
				"Connection: Keep-Alive",
				"User-Agent: ".$_SERVER['HTTP_USER_AGENT'],
				"Content-Length: 0",
				"x-ibm-client-id: $x_ibm_client_id",
				"x-ibm-client-secret: $x_ibm_client_secret",
				"x-rmg-password: $x_rmg_password",
				"x-rmg-user-name: $x_rmg_user_name" 
			
			  ),
			)); 
 

		$response = curl_exec($curl);
		$err  = curl_error($curl);	
		$info = curl_getinfo($curl);
		//pr($info );	 
		curl_close($curl);		
		$tokenArray = array();
		
		if ($err) {			
			$responsePath = WWW_ROOT .'logs/royalmail_token_error.log'; 			
			file_put_contents($responsePath, "\n------".date("d-m-Y H:i:s")."-----\n" . $err, FILE_APPEND|LOCK_EX);
			/***********Send Error Email***************/
			$details['error_code'] = 'curl_error';
			$details['error_msg']  = $err;
			$this->sendErrorMail($details);
			
		} else {
			$res = json_decode($response);
		
			if(isset($res->httpCode)){				
				$tokenArray['error'] = $res->httpCode;
				$responsePath = WWW_ROOT .'logs/royalmail_token_error.log'; 
			 	file_put_contents($responsePath, "\n------".date("d-m-Y H:i:s")."-----\n" . $response, FILE_APPEND|LOCK_EX);
				
				/***********Send Error Email***************/				
				if(isset($res->errors)){
					$details['error_code'] = $res->errors[0]->errorCode;
					$details['error_msg']  = $res->errors[0]->errorDescription;
				}else{
					$details['error_code'] = $res->httpCode;
					$details['error_msg']  = $res->httpMessage;
				}
				$this->sendErrorMail($details);
				
			}else{ 
				$tokenArray['token'] = $res->token;							 
			}
		}
		return $tokenArray;
	} 
	
	public function royalMailCreateShipment( $details = null, $mergeupdate = null, $token = null ){
		
		 
 		$data = $this->royalDataFormat($details);
		//pr($data); 
		$this->loadModel( 'Royalmail' );
		$auth = $this->royalMailAuth();
		$x_ibm_client_id = $auth['x_ibm_client_id'];
		$x_ibm_client_secret = $auth['x_ibm_client_secret']; 
	
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.royalmail.net/shipping/v2/domestic",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLINFO_HEADER_OUT => true,
		CURLOPT_POSTFIELDS => json_encode($data),
		CURLOPT_HTTPHEADER => array(
			"accept: application/json",
			"content-type: application/json",
			"x-ibm-client-id: $x_ibm_client_id",
			"x-ibm-client-secret: $x_ibm_client_secret",
			"x-rmg-auth-token: $token"
			),
		));
		
		$response = curl_exec($curl);
	//echo "======<br><br><br>=<br>";
		$err = curl_error($curl);
		$info = curl_getinfo($curl);
		//pr($info );
		curl_close($curl);
		
		if ($err) {		 
			$responsePath = WWW_ROOT .'logs/royalmail_shipment_'.$details['split_order_id'].'.log'; 
			file_put_contents($responsePath , $err);
			/***********Send Error Email***************/
			$details['error_code'] = 'curl_error';
			$details['error_msg']  = $err;
			$this->sendErrorMail($details);
		} else {		
		  $res = json_decode($response);
		  if(isset($res->httpCode)){
				//echo "<br>".$res->httpMessage;
				//echo  $res->moreInformation;
				$responsePath = WWW_ROOT .'logs/royalmail/error_'.$details['split_order_id'].'.log'; 
				file_put_contents($responsePath , $response);
				$responsePath = WWW_ROOT .'logs/royalmail/details_'.$details['split_order_id'].'.log'; 
				file_put_contents($responsePath , print_r($details,true));
				/***********Send Error Email***************/
				if(isset($res->errors)){
					$details['error_code'] = $res->errors[0]->errorCode;
					$details['error_msg']  = $res->errors[0]->errorDescription;
				}else{
					$details['error_code'] = $res->httpCode;
					$details['error_msg']  = $res->httpMessage;
				}
				/*--------------Lock Order------------*/
				$this->lockOrder($details);				 
				/*-----------------------------------*/
				$this->sendErrorMail($details);				
								
		   }else{
				foreach($res as $val){
				 if(count($val) > 0){
				 foreach($val as $v){
					 if(count($v) > 0){
						foreach($v->shipmentItems as $ship){						
							$details['shipment_number'] 	= $ship->shipmentNumber;
							$dataArray['split_order_id']  	= $details['split_order_id']; 
							$dataArray['shipment_number'] 	= $details['shipment_number'];
							$dataArray['details'] 		  	= json_encode($details);
							$dataArray['added_date'] 		= date('Y-m-d H:i:s');
							
							$conditions = array('Royalmail.split_order_id' => $details['split_order_id']);	
							
							if ($this->Royalmail->hasAny($conditions)){
								foreach($dataArray as $field => $rval){
									 $dataUpdate[$field] = "'".$rval."'";										
								}					
								$this->Royalmail->updateAll( $dataUpdate, $conditions ); 
							}else{ 
								$this->Royalmail->saveAll($dataArray);
							}
							$this->royalMailGenerateLabel($details, $mergeupdate, $token);
						}			
					 }
					}
				 }
			 }
  		   }
	 	}
	 
	}	
	
	public function royalMailGenerateLabel($details = null, $mergeupdate = null, $token = null){
		
		$auth = $this->royalMailAuth();
		$x_ibm_client_id 	 = $auth['x_ibm_client_id'];
		$x_ibm_client_secret = $auth['x_ibm_client_secret']; 
		
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.royalmail.net/shipping/v2/".$details['shipment_number']."/label?outputFormat=DSPNG",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "put",	
		CURLINFO_HEADER_OUT => true,	 
		CURLOPT_HTTPHEADER => array(
		"accept: application/json",
			"content-type: application/json",
			"Accept-Encoding: gzip,deflate",
			"Host: api.royalmail.net",
			"Connection: Keep-Alive",
			"User-Agent: ".$_SERVER['HTTP_USER_AGENT'],
			"x-ibm-client-id: $x_ibm_client_id",
			"x-ibm-client-secret: $x_ibm_client_secret",
			"x-rmg-auth-token: $token"
			),
		));
		
		
        $response = curl_exec($curl);
		$err = curl_error($curl);
		$info = curl_getinfo($curl);		
		curl_close($curl);
		//pr($response );pr($info );
		$resPath = WWW_ROOT .'royalmail/response/gl_'.$details['split_order_id'].'.log'; 
		file_put_contents($resPath , $response);
				
		if ($err) {			
			$responsePath = WWW_ROOT .'logs/royalmail_error_'.$details['split_order_id'].'.log'; 
			file_put_contents($responsePath , $err);
			/***********Send Error Email***************/
			$details['error_code'] = 'curl_error';
			$details['error_msg']  = $err;
			$this->sendErrorMail($details);
				
		} else {
			//echo $response; 
			$res = json_decode($response); 
			if(isset($res->httpCode)){
				$responsePath = WWW_ROOT .'logs/royalmail_response_'.$details['split_order_id'].'.log'; 
				file_put_contents($responsePath , $response);
				/***********Send Error Email***************/
				if(isset($res->errors)){
					$details['error_code'] = $res->errors[0]->errorCode;
					$details['error_msg']  = $res->errors[0]->errorDescription;
				}else{
					$details['error_code'] = $res->httpCode;
					$details['error_msg']  = $res->httpMessage;
				}
				$this->sendErrorMail($details);			
				
			}else{
				//$data = base64_decode( $res->label );
				$details['trackingNumber'] = $res->labelData->trackingNumber;
				$image2DMatrix = base64_decode( $res->labelImages->image2DMatrix );
				$labelPdfPath = WWW_ROOT .'royalmail/barcode/2d_matrix_'.$details['split_order_id'].'.png'; 			 
				file_put_contents($labelPdfPath , $image2DMatrix);
				
				//$image1DBarcode = base64_decode( $res->labelImages->image1DBarcode );
				//$labelPdfPath = WWW_ROOT .'royalmail/barcode/barcode_'.$details['split_order_id'].'.png'; 			 
				//file_put_contents($labelPdfPath , $image1DBarcode);
				
				//$this->loadModel( 'RegisteredNumber' );		
				//$this->RegisteredNumber->updateAll(array('RegisteredNumber.split_order_id' => ""),array('RegisteredNumber.split_order_id' => $details['split_order_id']));
 				 				
				$this->loadModel( 'MergeUpdate' );					
				//$this->MergeUpdate->updateAll( array( 'royalmail' => 1 ), array( 'product_order_id_identify' => $details['split_order_id'] ) );			 
				$this->MergeUpdate->saveAll( $mergeupdate );
				
				$this->labelHtml($details);
				 
		
			}
		}		 
	}
	
	public function royalMailCancelShipment($split_order_id = null, $is_ajax = 0 ){
		
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'MergeUpdate' );
		$result = $this->Royalmail->find('first', array('conditions' => array( 'split_order_id' => $split_order_id )));
		
		if(count($result) > 0){
		
			$shipment_number = $result['Royalmail']['shipment_number'];			
			$details = json_decode($result['Royalmail']['details'],true);
			 
			$data  = $this->royalMailGetToken();	
			$token = $data['token'];
									
			$auth = $this->royalMailAuth();
			$x_ibm_client_id     = $auth['x_ibm_client_id'];
			$x_ibm_client_secret = $auth['x_ibm_client_secret']; 
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.royalmail.net/shipping/v2/".$shipment_number,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "DELETE",	
			CURLINFO_HEADER_OUT => true,	 
			CURLOPT_HTTPHEADER => array(
			"accept: application/json",
				"content-type: application/json",
				"Accept-Encoding: gzip,deflate",
				"Host: api.royalmail.net",
				"Connection: Keep-Alive",
				"User-Agent: ".$_SERVER['HTTP_USER_AGENT'],
				"x-ibm-client-id: $x_ibm_client_id",
				"x-ibm-client-secret: $x_ibm_client_secret",
				"x-rmg-auth-token: $token"
				),
			));
			
			$response = curl_exec($curl);
			$err      = curl_error($curl);
			$info     = curl_getinfo($curl);
			 
			curl_close($curl);
			
			if ($err) {			
				$responsePath = WWW_ROOT .'logs/royalmail_cancel_'.$split_order_id.'.log'; 
				file_put_contents($responsePath , $err);
				/***********Send Error Email***************/
				$details['error_code'] = 'curl_error_in_shipment_cancellation';
				$details['error_msg']  = $err;
				$this->sendErrorMail($details);
				$msg['msg']    = $err;	
				$msg['status'] = 'error';	
					
			} else {
				//echo $response; 
				$res = json_decode($response); 
				if(isset($res->httpCode)){
					$responsePath = WWW_ROOT .'logs/royalmail_cancel_'.$split_order_id.'.log'; 
					file_put_contents($responsePath , $response);
					/***********Send Error Email***************/
					if(isset($res->errors)){
						$details['error_code'] = $res->errors[0]->errorCode;
						$details['error_msg']  = $res->errors[0]->errorDescription;
					}else{
						$details['error_code'] = $res->httpCode;
						$details['error_msg']  = $res->httpMessage;
					}
					
					$this->sendErrorMail($details);	
					$msg['msg']    = $res->errors[0]->errorDescription;	
					$msg['status'] = 'error';		
				}else{					
					$this->Royalmail->query( "delete from royalmails where split_order_id = '".$split_order_id."'" );
					
					$this->MergeUpdate->updateAll( array( 'royalmail' => 0 ), array( 'product_order_id_identify' => $split_order_id ) );
					$msg['msg']    = 'Shipment number cancelled';
					$msg['status'] = 'ok';	
					file_put_contents( WWW_ROOT .'logs/royalmail_cancel.log', "\n".$split_order_id."\t".date('Y-m-d H:i:s'),FILE_APPEND|LOCK_EX);				
				}			
			}
		}else{
			$responsePath = WWW_ROOT .'logs/royalmail_cancel_'.$split_order_id.'.log'; 
			file_put_contents($responsePath ,'shipment_number not found');
			$msg['msg'] = 'Shipment number not found';
			$msg['status'] = 'error';	
		}
		if($is_ajax > 0){
			return $msg;
			//exit; 
		} 
		
	}
	
	public function royalMailUpdateShipment(){ 
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'MergeUpdate' );
		
		/*$all_orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status' => 1, 'MergeUpdate.pick_list_status' => 1,'MergeUpdate.royalmail' => 1,'MergeUpdate.scan_status' => 0, 'MergeUpdate.sorted_scanned' => 0,'MergeUpdate.manifest_status' => 0),'fields' => array('MergeUpdate.product_order_id_identify')));	*/
		
		$all_orders = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.royalmail' => 1,'MergeUpdate.scan_status' => 0, 'MergeUpdate.sorted_scanned' => 0),'fields' => array('MergeUpdate.status','MergeUpdate.product_order_id_identify')));	
		
 		if(count($all_orders) > 0){
			foreach( $all_orders as $result )
			{
				if($result['MergeUpdate']['status'] == 2){
					$this->royalMailCancelShipment($result['MergeUpdate']['product_order_id_identify']);
				}else{
					$this->updateShipment($result['MergeUpdate']['product_order_id_identify']);
				}
			}
		}
	}
		
	public function updateShipment($split_order_id = null){
 		$this->loadModel('Royalmail');
		$result = $this->Royalmail->find('first', array('conditions' => array( 'split_order_id' => $split_order_id )));
		
		if(count($result) > 0){
		
			$details = json_decode($result['Royalmail']['details'],true);
			
			$shipping_date = $this->nextShipmentDate(date('Y-m-d'));		
			 
			$shipdata = array(
				'shippingDate' => $shipping_date,				 
				'recipientContact' => array(
						'name' => $details['FullName'],
						'complementaryName' => $details['Company'],						 
				) 
		 
			);
			
			$shipment_number = $result['Royalmail']['shipment_number'];	
			$split_order_id  = $result['Royalmail']['split_order_id'];				 
					
			$auth = $this->royalMailAuth();
			$x_ibm_client_id     = $auth['x_ibm_client_id'];
			$x_ibm_client_secret = $auth['x_ibm_client_secret']; 
			
			$tData  = $this->royalMailGetToken();	
			$token = $tData['token'];
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.royalmail.net/shipping/v2/".$shipment_number,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLINFO_HEADER_OUT => true,
			CURLOPT_POSTFIELDS => json_encode($shipdata),
			CURLOPT_HTTPHEADER => array(
			"accept: application/json",
				"content-type: application/json",
				"Accept-Encoding: gzip,deflate",
				"Host: api.royalmail.net",
				"Connection: Keep-Alive",
				"User-Agent: ".$_SERVER['HTTP_USER_AGENT'],
				"x-ibm-client-id: $x_ibm_client_id",
				"x-ibm-client-secret: $x_ibm_client_secret",
				"x-rmg-auth-token: $token"
				),
			)); 
			
			$response = curl_exec($curl);
			json_encode($shipdata);
			$err = curl_error($curl);
			$info = curl_getinfo($curl);			
			curl_close($curl);
			
			if ($err) {		 
				$responsePath = WWW_ROOT .'logs/royalmail_shipment_update'.$split_order_id.'.log'; 
				file_put_contents($responsePath , $err);
				/***********Send Error Email***************/
				$details['error_code'] = 'curl_error';
				$details['error_msg']  = $err;
				$this->sendErrorMail($details);
			} else {		
			  $res = json_decode($response);
			  if(isset($res->httpCode)){
					//echo "<br>".$res->httpMessage;
					//echo  $res->moreInformation;
					$responsePath = WWW_ROOT .'logs/royalmail_shipment_update'.$split_order_id.'.log'; 
					file_put_contents($responsePath , $response);
					/***********Send Error Email***************/
					if(isset($res->errors)){
						$details['error_code'] = $res->errors[0]->errorCode;
						$details['error_msg']  = $res->errors[0]->errorDescription;
					}else{
						$details['error_code'] = $res->httpCode;
						$details['error_msg']  = $res->httpMessage;
					}
					$this->sendErrorMail($details);		
									
			   }else{					
			   		$this->Royalmail->updateAll( array('update_date' => "'".date('Y-m-d H:i:s')."'"),array('shipment_number' => $res->shipmentNumber));	   
			  		$responsePath = WWW_ROOT .'logs/royalmail_shipment_update_'.$split_order_id.'.log'; 
					file_put_contents($responsePath , date('Y-m-d H:i:s')."\t".$response);					
			   }
			}
		}
	} 
	public function createManifest(){
	
		/**
		*Update all order shipment that are in picklist and not processed till now.
		*/
		$this->royalMailUpdateShipment();
	 	/*---------------------------------*/
		
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'RoyalmailManifest' ); 
		 	
		$auth = $this->royalMailAuth();
		$x_ibm_client_id     = $auth['x_ibm_client_id'];
		$x_ibm_client_secret = $auth['x_ibm_client_secret']; 
		
		$tData = $this->royalMailGetToken();	
		$token = $tData['token'];
		
		$data  = array('yourDescription'=> 'Shipping manifest' ,'yourReference'=>'manifest_'.date('ymd'));
		
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.royalmail.net/shipping/v2/manifest",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => json_encode($data),
		CURLOPT_HTTPHEADER => array(
		"accept: application/json",
		"content-type: application/json",
		"x-ibm-client-id: $x_ibm_client_id",
		"x-ibm-client-secret: $x_ibm_client_secret",
		"x-rmg-auth-token: $token"
		),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		
		if ($err) {		 
				$responsePath = WWW_ROOT .'logs/royalmail_manifest.log'; 
				file_put_contents($responsePath , $err);
				/***********Send Error Email***************/
				$details['error_code'] = 'curl_error';
				$details['error_msg']  = $err;
				$this->sendErrorMail($details,1);
				$this->Session->setFlash($err, 'flash_danger');	
		}else {			
			 $res = json_decode($response); 			
			 if(isset($res->httpCode)){
				$responsePath = WWW_ROOT .'logs/royalmail_manifest.log'; 
				file_put_contents($responsePath , $response);
				/***********Send Error Email***************/
				if(isset($res->errors)){
					$details['error_code'] = $res->errors[0]->errorCode;
					$details['error_msg']  = $res->errors[0]->errorDescription;
				}else{
					$details['error_code'] = $res->httpCode;
					$details['error_msg']  = $res->httpMessage;
				}
				
				if($details['error_code'] != 'E1128'){
					$this->sendErrorMail($details,1);		
				}
				$this->Session->setFlash($res->errors[0]->errorDescription, 'flash_danger');	 		
								
		   }else{ 
				$dataArray['batch_number']  = $res->batchNumber; 
				$dataArray['count']			= $res->count;								
				$dataArray['added_date']    = date("Y-m-d H:i:s");						 
				$this->RoyalmailManifest->saveAll($dataArray);
				$manifest_table_id = $this->RoyalmailManifest->getLastInsertId();
				foreach($res->shipments as $val){
					$this->Royalmail->updateAll( array('manifest_table_id' => $manifest_table_id,'service_code'=>"'".$val->code."'",'manifest_date' => "'".$dataArray['added_date']."'"),array('shipment_number' => $val->shipmentNumber));
					$this->updateMergeUpdateTable($val->shipmentNumber);				
				}				
				//sleep(40);
				//$this->printManifest( $res->batchNumber );
			}
		}
		//$this->redirect($this->referer());
	}
	
	public function printManifest(){
	 
	    $this->loadModel( 'RoyalmailManifest' );
		$result = $this->RoyalmailManifest->find('first', array('conditions' => array( 'is_printed' => 0 )));
				 
		if(count($result) > 0){
			
			$batch_number = $result['RoyalmailManifest']['batch_number'];	
			
			$auth = $this->royalMailAuth();
			$x_ibm_client_id = $auth['x_ibm_client_id'];
			$x_ibm_client_secret = $auth['x_ibm_client_secret']; 
			
			$tData = $this->royalMailGetToken();	
			$token = $tData['token'];
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.royalmail.net/shipping/v2/manifest?manifestBatchNumber=$batch_number",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_HTTPHEADER => array(
			"accept: application/json",
			"x-ibm-client-id: $x_ibm_client_id",
			"x-ibm-client-secret: $x_ibm_client_secret",
			"x-rmg-auth-token: $token"
			),
			)); 
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {		 
					$responsePath = WWW_ROOT .'logs/royalmail_manifest.log'; 
					file_put_contents($responsePath , $err);
					/***********Send Error Email***************/
					$details['error_code'] = 'curl_error';
					$details['error_msg']  = $err;
					$this->sendErrorMail($details,1);
					$this->Session->setFlash($err, 'flash_danger');
				}else {			
					 $res = json_decode($response);
					 if(isset($res->httpCode)){
						$responsePath = WWW_ROOT .'logs/royalmail_manifest.log'; 
						file_put_contents($responsePath , $response);
						/***********Send Error Email***************/
						if(isset($res->errors)){
							$details['error_code'] = $res->errors[0]->errorCode;
							$details['error_msg']  = $res->errors[0]->errorDescription;
						}else{
							$details['error_code'] = $res->httpCode;
							$details['error_msg']  = $res->httpMessage;
						}
						$this->sendErrorMail($details,1);	
						$this->Session->setFlash($res->errors[0]->errorDescription, 'flash_danger');					 		
										
				   }else{
						$labelPdfPath = WWW_ROOT .'royalmail/manifests/'.date('Ymd').'_'.$batch_number.'.pdf'; 			 
						file_put_contents($labelPdfPath , base64_decode($res->manifest));
						
						$this->RoyalmailManifest->updateAll( array('is_printed' => 1),array('batch_number' => $batch_number));
						$this->Session->setFlash('Manifest generated.', 'flash_success');
					}
				
				}
			}else{
			 	$this->Session->setFlash('No order found for Manifest.', 'flash_danger');
			 	
			}
		 $this->redirect($this->referer());
	}
	
	public function createManifestCron(){
		
		$this->loadModel( 'RoyalmailManifest' ); 
		$result = $this->RoyalmailManifest->find('first', array('order'=>'id DESC'));
		$manifest_date = date('Y-m-d',strtotime($result['RoyalmailManifest']['added_date']));
		
		if(date('Y-m-d') > $manifest_date){
			$this->createManifest();
			$royalmail_manifest_cron = date('Y-m-d H:i:s')."\tin_cron_codition\t";			
		}else{
			$royalmail_manifest_cron = date('Y-m-d H:i:s')."\tnot_in_cron_codition\t";	
		}
		
		$path = WWW_ROOT .'logs/royalmail/manifest_cron.log'; 
		file_put_contents($path , $royalmail_manifest_cron."\n",  FILE_APPEND|LOCK_EX);
		exit;		 
	}
	
	private function royalDataFormat($details = null){
	
		date_default_timezone_set('Europe/Jersey');
		
		$this->loadModel( 'RoyalmailManifest' ); 
		
		$result = $this->RoyalmailManifest->find('first', array('order'=>'id DESC'));
		
		$manifest_date = date('Y-m-d',strtotime($result['RoyalmailManifest']['added_date']));
		
		$shipping_date = date('Y-m-d');
		
		/*if(($manifest_date == date('Y-m-d')) || (date('H') >= '14')){
			$shipping_date = $this->nextShipmentDate(date('Y-m-d'));
		}*/   
		if($manifest_date == date('Y-m-d')){
			$shipping_date = $this->nextShipmentDate(date('Y-m-d'));
		}
		
		$data = array(
		'shipmentType' => 'Delivery',
		'service' => array(
			 'format' => $details['service_format'],
			 'occurrence' => '1',
			 'offering' => 'CRL',
			 'type' => $details['type'],
		),
		'shippingDate' => $shipping_date,
		'items' => array (
					 0 =>array(
					 'count' => $details['item_count'],
					 'weight' =>array(
							   'unitOfMeasure' => 'g',
							   'value' => $details['weight'] > 0 ? $details['weight'] : 10,
						),
				),
		),
		'recipientContact' => array(
				'name' => $details['FullName'],
				'complementaryName' => $details['Company'],
				 
		),
		'recipientAddress' => array(
				'buildingName' => $details['building_name'],
				'buildingNumber' => $details['building_number'],
				'addressLine1' => $details['Address1'],
				'addressLine2' => $details['Address2'],
				'addressLine3' => '',
				'postTown' => $details['Town'],
				'postCode' => trim($details['PostCode']),
			),
			'senderReference' => $details['split_order_id']
		);
		
		return $data;
	}	
	
	
		
	public function labelHtml( $details = null ){		
		/*$details['sub_source'] = 'CostBreaker';
		$details['trackingNumber'] = 'TTT010102260GB';
		$details['item_count'] = '2';
		$details['weight'] = '51.00';
		$details['FullName'] = 'Alex Koupland';

		$details['Company'] = '';
		$details['building_name'] = '';
		$details['building_number'] = '0';
		  
		$details['Address1'] = 'Plus Accounting';
		$details['Address2'] = 'Preston Park House, South Road';
		$details['Town'] = 'Brighton';
		$details['PostCode'] = 'BN1 6SB';
		$details['split_order_id'] = '765138-1';*/
		
		$this->getBarcode( $details['split_order_id'] );
		$service = '48';
		if($details['type'] == 1 ){
			$service = '24';
		}
 		
		$box_number = 824; 
		if(strpos($details['sub_source'],'CostBreaker')!== false){
			$box_number = 824;
		}
		else if(strpos($details['sub_source'],'RAINBOW')!== false){
			$box_number = 825;;
		}else if(strpos($details['sub_source'],'Marec')!== false){
			$box_number = 826;
		}	
		elseif(strpos($details['sub_source'],'BBD')!== false){
			$box_number = 827;
		}
		else if(strpos($details['sub_source'],'Tech_Drive')!== false){
			$box_number = 828;
		} 
		 
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload'); 
		$dompdf = new DOMPDF(); 
		//$dompdf->set_paper('A4', 'portrait');
		$dompdf->set_paper(array(0, 0, 288, 500), 'portrait');
		/**************** for tempplate *******************/
		$html = '<!DOCTYPE html>';
		$html .= '<html>'; 
		$html .= '<head>';
		$html .= '<title>Label</title>'; 
		$html .= '<style>.container{margin:0px auto;}</style>'; 
		$html .= '</head>'; 
		$html .= '<body>';
		 
	 	$html .= '<div class="container"  style="width:371px; height:auto;  border:1px solid #000;  margin-left:-40px;margin-top:-39px;">';
		
		$html .= '<table  width="100%" cellpadding="0" cellspacing="10">';
		$html .= '<tbody>';
		$html .= '<tr>';
		$html .= '<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/Royal_Mail.png" width="115px"></td>';
		$html .= '<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/'.$service.'.jpg" width="50px"></td>';
		$html .= '<td valign="middle" align="left"><img src="'.WWW_ROOT.'royalmail/img/royal.png" width="100px" height="70px"></td>';
		$html .= '</tr>';
		$html .= '</tbody>';
		$html .= '</table>';
		 
		
		$html .= '<table width="100%" cellpadding="0" cellspacing="10" style="border-bottom:1px solid #000; border-top:1px solid #000;">';
		$html .= '<tbody>';
		$html .= '<tr>';
		$html .= '<td valign="top" width="50%" align="left" >';
		$html .= '<img src="'. Router::url('/', true) .'royalmail/barcode/2d_matrix_'.$details['split_order_id'].'.png" width="83px" height="83px" style="margin-left:10px;">';
		$html .= '</td>';
	
		$html .= '</tr>';
		$html .= '</tbody>';
		$html .= '</table>';
		
		$html .= '<table width="100%"  cellpadding="0" cellspacing="10" >';
		$html .= '<tbody>';
		$html .= '<tr>';
		$html .= '<td valign="top" width="75%">';
		$html .= '<div style="font-size:10pt; !important; font-family:Ariel regular light; margin-left:10px;">'; 
		$html .='<strong>'. ucwords(strtolower($details['FullName'])).'</strong><br>';
		
		if(strlen($details['Address1']) > 25 || strlen($details['Address2']) > 25){
		
			$lines = explode("\n", wordwrap(htmlentities($details['Address1']) .' '.htmlentities($details['Address2']), '25'));
			if(isset($lines[0]) && $lines[0] != ''){
				$html .=  $lines[0].'<br>';
			}
			if(isset($lines[1]) && $lines[1] != ''){
				$html .=  $lines[1].'<br>';
			}
			if(isset($lines[2]) && $lines[2] != ''){
				$html .=  $lines[2].'<br>';
			}
		
		}else{
			$html .= ucfirst($details['Address1']).'<br>';
			if($details['Address2']) $html .= ucfirst($details['Address2']).'<br>';
		}
		
		
		$html .= ucfirst($details['Town']).'<br>';
		$html .= $details['PostCode'];
		$html .= '</div>';
 		$html .= '</td>'; 
		
		$html .= '<td valign="bottom" width="25%" align="right">';
		$html .= '<img src="'.WWW_ROOT.'royalmail/img/'.$box_number.'.png" style="height:100px;float:right;">';
		$html .= ' </td>';
		$html .= ' </tr>';
		$html .= '</tbody>';   
		$html .= ' </table>';
		
		 
		$html .= '<table width="100%" style="border-top:1px solid #000;" >';
		$html .= '<tbody>';
		$html .= '<tr>';
		$html .= '<td valign="top" align="center" style="padding-top:5px;"><p style="font-family:chevin; font-size:12px;">Customer Reference:'.$details['split_order_id'].'</p>';
		
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '</tbody>';
		$html .= '</table>';
	 
 		$html .= ' </div>';
		 
		$html .= ' <div  style="text-align:center;width:100%;padding-top:25px;">';
		$html .= '<img src="'.Router::url('/', true) .'img/orders/barcode/'.$details['split_order_id'].'.png" style="text-align:center">';
		$html .= ' </div>';
		
		$html .= '</body>';
		$html .= '</html>';
	 	//echo $html;
					
		$dompdf->load_html(utf8_encode($html), Configure::read('App.encoding'));
		$dompdf->render();
		//$dompdf->stream();			
						
		$file_to_save = WWW_ROOT .'royalmail/labels/label_'.$details['split_order_id'].'.pdf';	
		 
		file_put_contents($file_to_save, $dompdf->output());  
		unset($html);
		unset($dompdf);
		
	}
	
	public function customLabelHtml( $details = null ){		
		die('Uncomment Code.');
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'Country' );	
		$order_ids = array(1096612);
		 
		foreach($order_ids as $order_id){
			 
			$orderItems = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.order_id' => $order_id )));
				foreach($orderItems as $val){
					App::import( 'Controller' , 'Cronjobs' );		
					$objController 	= new CronjobsController();
					$getorderDetail	=	$objController->getOpenOrderById($order_id);
					$cInfo = $getorderDetail['customer_info'];
					$order->TotalsInfo = $getorderDetail['totals_info'];
					
					$country_data = $this->Country->find('first',array('conditions' => array("Country.name" => $cInfo->Address->Country)));
					
					$Address1 = $cInfo->Address->Address1;
					$Address2 = $cInfo->Address->Address2;
					 
					
					$details['sub_source'] = $getorderDetail['sub_source'];			 
				 
					$details['FullName'] =  $this->replaceFrenchChar(utf8_decode($cInfo->Address->FullName));
					$details['Company'] =  '';
					$details['building_name'] =  '';
					$details['building_number'] = '0';
					  
					$details['Address1'] = utf8_decode($Address1);
					$details['Address2'] = utf8_decode($Address2);
					$details['Town'] = utf8_decode($cInfo->Address->Town);
					$details['PostCode'] = $cInfo->Address->PostCode;
					$details['CountryCode'] = $country_data['Country']['iso_2'];
					$details['split_order_id'] = $val['MergeUpdate']['product_order_id_identify']; 
					if($val['MergeUpdate']['service_name'] == 'royalmail_24'){
						$details['type'] = 1 ;
					} else{
						$details['type'] = 2 ;
					}
					
				
					$this->labelHtml($details);
				} 
		} 
		
	}
	private function lockOrder($details = null){
	
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'OpenOrder' );
		$this->loadModel( 'OrderNote' );	
		$this->loadModel( 'RoyalmailError' );
		$md = $this->MergeUpdate->find('first', array( 'conditions' => array('product_order_id_identify' => $details['split_order_id']),'fields'=>'order_id' ) );
		if(count($md) > 0){
			$lock_note = $details['error_msg'];
			
			$firstName = $_SESSION['Auth']['User']['first_name'];
			$lastName = $_SESSION['Auth']['User']['last_name'];
			$mdata['royalmail'] = 0;
			$mdata['status']    = 3;
			$mdata['rm_error_msg'] = "'".$lock_note."'";
			 
			$this->MergeUpdate->updateAll($mdata, array( 'product_order_id_identify' => $details['split_order_id'] ) );
			$this->OpenOrder->updateAll(array('OpenOrder.status' => 3), array('OpenOrder.num_order_id' => $md['MergeUpdate']['order_id']));
			
			$noteDate['order_id'] = $md['MergeUpdate']['order_id'];
			$noteDate['note'] = $lock_note;
			$noteDate['type'] = 'Lock';
			$noteDate['user'] = $firstName.' '.$lastName;
			$noteDate['date'] = date('Y-m-d H:i:s');
			$this->OrderNote->saveAll( $noteDate ); 
			$roe['split_order_id']  = $details['split_order_id'];
			$roe['error_msg'] 		= $lock_note;
			$this->RoyalmailError->saveAll( $roe ); 
		}
	}	
	
	private function sendErrorMail($details = null, $is_manifest = 0){
	
		if($is_manifest > 0){
			$subject   = 'RoyalMail create manifest issue in Xsensys';
			$mailBody  = '<p><strong>Create manifest have below issue please review and solve it.</strong></p>';
		}else{
			$subject   = $details['split_order_id'].' RoyalMail orders issue in Xsensys';
			$mailBody  = '<p><strong>'.$details['split_order_id'].' have below issue please review and solve it.</strong></p>';
		}	
		
		$mailBody .= '<p>Error Code : '.$details['error_code'].'</p>';
		$mailBody .= '<p>Error Message : '.$details['error_msg'].'</p>';
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('');
		$email->emailFormat('html');
		$email->from('info@euracogroup.co.uk');
		//$email->to( array('avadhesh.kumar@jijgroup.com','shashi@euracogroup.co.uk','amit@euracogroup.co.uk','abhishek@euracogroup.co.uk'));	
		if($details['error_code'] == 500 || $details['error_code'] == 'E1135'){
			$email->to( array('avadhesh.kumar@jijgroup.com','shashi@euracogroup.co.uk','amit@euracogroup.co.uk'));	
		}else if($details['error_code'] == 'E1001' ){
			$email->to( array('avadhesh.kumar@jijgroup.com','aakash.kushwah@jijgroup.com','abhishek@euracogroup.co.uk'));	
		}else{
			$email->to( array('avadhesh.kumar@jijgroup.com'));	
		}				  
		$getBase = Router::url('/', true);
		$email->subject( $subject );
	  	$email->send( $mailBody );
	}
	 
	private function getBarcode( $split_order_id ){ 
		
		$imgPath = WWW_ROOT .'img/orders/barcode/';   
 		
		if(!file_exists($imgPath.$split_order_id.'.png')){
		
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGFontFile.php'); 
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGDrawing.php');
			require_once(APP . 'Vendor' . DS . 'barcodegen' . DS . 'class/BCGcode128.barcode.php');
			
			$colorFront = new BCGColor(0, 0, 0);
			$colorBack = new BCGColor(255, 255, 255); 
			
			$code128 = new BCGcode128();
			$code128->setScale(2);
			$code128->setThickness(20);
			$code128->setForegroundColor($colorFront);
			$code128->setBackgroundColor($colorBack);
			$code128->setLabel(false);
			$code128->parse($split_order_id);
			
			//Drawing Part
			$imgOrder128 = $split_order_id.".png";
			$imgOrder128path = $imgPath.$split_order_id.".png";
			$drawing128 = new BCGDrawing($imgOrder128path, $colorBack);
			$drawing128->setBarcode($code128);
			$drawing128->draw();
			$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG);  
		}
		      
	}		
	 	 
	private function updateMergeUpdateTable($shipment_number = null){
		
		$this->loadModel('MergeUpdate');
		$this->loadModel('Royalmail');
		
		$result = $this->Royalmail->find('first', array('conditions' => array( 'shipment_number' => $shipment_number )));	
			
		$this->MergeUpdate->updateAll( array('royalmail'=>2,'manifest_date' => "'".date('Y-m-d H:i:s')."'"),array('product_order_id_identify' => $result['Royalmail']['split_order_id']));	
	}
	
	private function nextShipmentDate($shipping_date = null)
	{   
		$sunday   = date('Y-m-d', strtotime('sunday of week'));
		$holidays = array($sunday,"2017-08-04","2017-08-28","2017-12-25","2017-12-26","2018-01-01","2018-03-30","2018-04-02","2018-05-07","2018-05-09","2018-05-28","2018-08-27","2018-08-27","2018-12-25","2018-12-26"); 		 
		
		$shipping_date = date('Y-m-d',strtotime($shipping_date . "+1 day"));			
		if(in_array($shipping_date, $holidays)){				
			$shipping_date = $this->nextShipmentDate($shipping_date);
		} 	
				
		return  $shipping_date;
	}
	
	
	public function applyPostNL(){
		exit;
		$this->autoRender = false;
		$this->layout = '';
		$this->loadModel( 'MergeUpdate' );
		$this->loadModel( 'PostalServiceDesc' );
		$royal_sur_charge = 0.40;
		$fuel_percent     = 3;
		$dhl 			  = 1.20;
		$dhl_additional_cost = 0.42;
			
		$currency 		  = $this->getCurrencyRate();
		$exchange_rate	  = $currency ['EUR'];  
		$return 		  = array();
		$count = 0;
		//$orderItems = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status IN' =>array(0,1),'MergeUpdate.delevery_country' => 'United Kingdom'),'limit'=>'200','Order BY id ASC'));	
		 
		 $orderItems = $this->MergeUpdate->find('all',array('conditions' => array('MergeUpdate.status' => 0,'MergeUpdate.product_order_id_identify IN ' => array('1044828-1','1044834-1','1044851-1','1044845-1','1044857-1','1044857-2','1044924-1','1044925-1','1044878-1','1044850-1','1036515-1','1044926-1','1044911-1','1044912-1','1044899-1','1044900-1','1044901-1','1044880-1','1044882-1','1044881-1','1044874-1','1044875-1','1044865-1','1044866-1','1044867-1','1044955-1','1044943-1','1044944-1','1044934-1','1044954-1','1044970-1','1044970-2','1044953-1','1044968-1','1044969-1','1045007-1','1044994-1','1044980-1','1044991-1','1045006-1','1045029-1','1045030-1','1045031-1','1045027-1','1045048-1','1045026-1','1045025-1','1045050-1','1045028-1','1045120-1','1045073-1','1045077-1','1045078-1','1045079-1','1045049-1','1045074-1','1045100-1','1045076-1','1045098-1','1045097-1','1045118-1','1045119-1','1045137-1','1045114-1','1045145-1','1045146-1','1045117-1','1045116-1','1045212-1','1045140-1','1045143-1','1045142-1','1045142-2','1045156-1','1045157-1','1045158-1','1045139-1','1045155-1','1045168-1','1045169-1','1045218-1','1045189-1','1045188-1','1045166-1','1045220-1','1045191-1','1045167-1','1045285-1','1045243-1','1045242-1','1045219-1','1045190-1','1045214-1','1045216-1','1045217-1','1045237-1','1045241-1','1045239-1','1045240-1','1045215-1','1045259-1','1045258-1','1045318-1','1045318-2','1045262-1','1045263-1','1045257-1','1045260-1','1045261-1','1045358-1','1045282-1','1045283-1','1045362-1','1045304-1','1045303-1','1045316-1','1045302-1','1045317-1','1045341-1','1045342-1','1045343-1','1045396-1','1045396-2','1045338-1','1045365-1','1045339-1','1045340-1','1045320-1','1045380-1','1045359-1','1045344-1','1045376-1','1045378-1','1045377-1','1045379-1','1045444-1','1045397-1','1045399-1','1045398-1','1045415-1','1045443-1','1045510-1','1045445-1','1045418-1','1045436-1','1045416-1','1045435-1','1045439-1','1045534-1','1045437-1','1045438-1','1045440-1','1045470-1','1045442-1','1045441-1','1045488-1','1045489-1','1045469-1','1045467-1','1045487-1','1045506-1','1045486-1','1045504-1','1045508-1','1045509-1','1045533-1','1045559-1','1045555-1','1045536-1','1045537-1','1045620-1','1045538-1','1045584-1','1045557-1','1045581-1','1045582-1','966596-1','1044502-1','1044933-1','1045144-1','1045115-1','1045141-1','1045238-1','1045337-1','1045361-1','1045466-1','1045468-1','1045505-1','1045485-1','1045556-1'  ))));	
		
		//pr( $orderItems);exit; 
		 //966596 966653
		if(count($orderItems) > 0)
		{
 			$this->loadModel( 'Product' );
			$this->loadModel( 'ProductDesc' );
			$this->loadModel( 'Country' );	
			
			foreach($orderItems as $orderItem ) 
			{
				$quantity 	   = $orderItem['MergeUpdate']['quantity'];
				$packet_weight = $orderItem['MergeUpdate']['packet_weight'];
				$packet_length = $orderItem['MergeUpdate']['packet_length'];
				$packet_width  = $orderItem['MergeUpdate']['packet_width'];
				$packet_height = $orderItem['MergeUpdate']['packet_height'];
				$order_id 	   = $orderItem['MergeUpdate']['order_id'];
				
				/*-------------------Royal Mail Calculations------------------------*/		
				
				 if($packet_weight > 2){
					 $royalMailPostal = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
							  
							 
				 }else{
					 $dim = array($packet_width,$packet_height,$packet_length);
					 asort($dim);
					 $final_dim = array_values($dim) ;							
					
					 $royalMailPostal = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey','PostalServiceDesc.max_weight >=' => $packet_weight,'PostalServiceDesc.max_length >=' => $final_dim[2],'PostalServiceDesc.max_width >=' => $final_dim[1],'PostalServiceDesc.max_height >=' => $final_dim[0], 'PostalServiceDesc.courier' => 'Royalmail', 'PostalServiceDesc.status' => 1 ),'order'=>'PostalServiceDesc.per_item ASC'));
				 }
				$postNLFee = 0;  $all_postnl = array();
				if(count($royalMailPostal) > 0)
				{
					$per_item = $royalMailPostal['PostalServiceDesc']['per_item'];
					
					
					$fuel_surcharge = ($per_item  * $fuel_percent) / 100;
					$additional_fee = ($dhl + $dhl_additional_cost) * $packet_weight ;							
					$royalFee       = ($per_item + $fuel_surcharge + $additional_fee) ;	
					
					//echo $royalFee;		
					//$royalFee = $royalMailPostal['PostalServiceDesc']['per_item'] + ($packet_weight * $royal_sur_charge);
					
					/*-------------------PostNL Calculations------------------------*/
					$postNlPostal = $this->PostalServiceDesc->find('all', array('conditions' => array('PostalServiceDesc.warehouse' => 'Jersey',
									'Location.county_name' => 'United Kingdom', 'PostalServiceDesc.max_weight >=' => $packet_weight, 'PostalServiceDesc.max_length >=' => $packet_length, 'PostalServiceDesc.max_width >=' => $packet_width, 'PostalServiceDesc.max_height >=' => $packet_height, 'PostalServiceDesc.courier' => 'Belgium Post')));
					 
					foreach($postNlPostal as $pv){
						$per_item 	= $pv['PostalServiceDesc']['per_item'];
						$per_kg 	= $pv['PostalServiceDesc']['per_kilo'];
						$weightKilo = $pv['PostalServiceDesc']['max_weight'];
						$postalid 	= $pv['PostalServiceDesc']['id'];
					
						$all_postnl[$postalid] = (($per_item + ($per_kg * $orderItem['MergeUpdate']['packet_weight'])) / $exchange_rate ) + ($dhl * $orderItem['MergeUpdate']['packet_weight']);
					} 
					
					//pr($all_postnl);
					if(count($all_postnl) > 0){
						$postNLFee  = min($all_postnl);	
					
						$postnl_id = array_search($postNLFee, $all_postnl); 
						if($postnl_id != $orderItem['MergeUpdate']['service_id']){
							 echo " order_id==".$orderItem['MergeUpdate']['product_order_id_identify']; 
							 echo " postnl_id==".$postnl_id;
							 echo " service_id= ".$orderItem['MergeUpdate']['service_id'];
							 echo "</br>";
						 }
						//echo " postNLFee==".$postNLFee;
						//echo " royalFee==".$royalFee;
						 	
					}				 
  					
					/*-------------------Compare And Replace------------------------*/
					//if(($postNLFee > 0) && ($postNLFee > $royalFee)) {
					if($postnl_id != $orderItem['MergeUpdate']['service_id']){
							$count++;
							 
							
							$post_nl = $this->PostalServiceDesc->find('first', array('conditions' => array('PostalServiceDesc.id' => $postnl_id)));
						 
							$split_order_id 	   = $orderItem['MergeUpdate']['product_order_id_identify']; 
							
							echo " split_order_id=".$split_order_id;
							echo " postNLFee=".$postNLFee;
							echo " royalFee=".$royalFee;
							echo "===". $royalMailPostal['PostalServiceDesc']['id'];
							echo "</br>";	
							 
							$data['royalmail'] 			= 0;
							$data['service_name'] 		= $post_nl['PostalServiceDesc']['service_name'];
							$data['track_id'] 			= '';
							$data['reg_post_number'] 	= '';
							$data['provider_ref_code'] 	= $post_nl['PostalServiceDesc']['provider_ref_code'];
							$data['service_id'] 		= $post_nl['PostalServiceDesc']['id'];
							$data['service_provider'] 	= 'PostNL';								 
							$data['template_id'] 		= $post_nl['PostalServiceDesc']['template_id'];  
							$data['id'] 				= $orderItem['MergeUpdate']['id'];
							
							$responsePath = WWW_ROOT .'logs/royalmail_to_postnl.log'; 
							//file_put_contents($responsePath , $split_order_id."\n",  FILE_APPEND|LOCK_EX);	
							//$this->royalMailCancelShipment($split_order_id);
							//$this->MergeUpdate->saveAll($data);
							 pr($data);
							 
							 
					}else{
						//echo $order_id. ' No order found in condition.<br>';
					}
					/*------------------end--------------------------------*/
				}else{
					echo $msg = 'This order may have over weight or any other issue.'; 
				}
			}
		//return $return;
	  }
	  else{
		echo 'No order found.';
		}
		echo "count=".$count;
	}
	
	public function CancelShipment($shipment_number = null){
		
		exit;
		$this->loadModel( 'Royalmail' );
		$this->loadModel( 'MergeUpdate' );
		$result = $this->Royalmail->find('first', array('conditions' => array( 'shipment_number' => $shipment_number )));
		
		if(count($result) > 0){
		
			$shipment_number = $result['Royalmail']['shipment_number'];			
			$details = json_decode($result['Royalmail']['details'],true);
			 
			$data  = $this->royalMailGetToken();	
			$token = $data['token'];
									
			$auth = $this->royalMailAuth();
			$x_ibm_client_id     = $auth['x_ibm_client_id'];
			$x_ibm_client_secret = $auth['x_ibm_client_secret']; 
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.royalmail.net/shipping/v2/".$shipment_number,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "DELETE",	
			CURLINFO_HEADER_OUT => true,	 
			CURLOPT_HTTPHEADER => array(
			"accept: application/json",
				"content-type: application/json",
				"Accept-Encoding: gzip,deflate",
				"Host: api.royalmail.net",
				"Connection: Keep-Alive",
				"User-Agent: ".$_SERVER['HTTP_USER_AGENT'],
				"x-ibm-client-id: $x_ibm_client_id",
				"x-ibm-client-secret: $x_ibm_client_secret",
				"x-rmg-auth-token: $token"
				),
			));
			
		echo	$response = curl_exec($curl);
			$err      = curl_error($curl);
			$info     = curl_getinfo($curl);
			 
			curl_close($curl);
			
			pr($info);
		}
		exit;
	}
	
	public function getCurrencyRate()
	{	 			
		$this->loadModel('CurrencyExchangeRate');	
		
		$curencies = array('INR','EUR','USD');
		$url='http://www.floatrates.com/daily/gbp.xml';
		$xml = file_get_contents($url);
		$curArray = json_decode(json_encode(simplexml_load_string($xml)),true);
		if(count($curArray) > 0 ){
			$this->CurrencyExchangeRate->query("TRUNCATE currency_exchange_rates");
			foreach($curArray['item'] as $val) {
				if (in_array($val['targetCurrency'],$curencies)) {			
					$curency['base_currency'] = 'GBP';
					$curency['exchange_rate'] = $val['exchangeRate'];	
					$curency['target_currency'] = $val['targetCurrency'];					
					$this->CurrencyExchangeRate->saveAll($curency);	
				}
			}		
		}
		
		$curency = $this->CurrencyExchangeRate->find("all"); 
		foreach($curency as $val){
			$curencyArr[$val['CurrencyExchangeRate']['target_currency']] = $val['CurrencyExchangeRate']['exchange_rate'];
		}
				
		return $curencyArr; 			
 	}
	public function server($string = null){
		echo date('Y-m-d H:i:s');
	}			
	private function replaceFrenchChar($string = null){
			
		$unwanted_array = array('�'=>'S', '�'=>'s', '�'=>'Z', '�'=>'z', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E','�'=>'E', '�'=>'E', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', 'N�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U','�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y', '�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'c','�'=>'e', '�'=>'e', '�'=>'e', '�'=>'e', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n','n�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o','�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y', '�'=>'b', '�'=>'y','�'=>'u','�'=>'');
		
		$str = strtr( $string,$unwanted_array );

		return  $str;
	}	
	 
}


/*curl -O http://xsensys.com/RoyalMail/printManifest &>/dev/null 2>&1
curl -O http://wms.esljersey.com/RoyalMail/printManifest &>/dev/null 2>&1
*/
?>