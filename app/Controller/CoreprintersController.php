<?php

class CoreprintersController extends AppController
{    
    var $name = "CorePrinters";    
    public function startThread( $customFeilds = array() )
    {		
		App::import( 'Component','Coreprint' );
		$Coreprint = new CoreprintComponent(new ComponentCollection());		
		return $Coreprint->setCurlAuth( $customFeilds );
	}
	 
	public function run( $customFeilds = array() )
	{
		return CoreprintersController::startThread( $customFeilds );
	}
    
    public function toPrint( $customFeilds = array() )
    {
		CoreprintersController::run( $customFeilds );
	}
	
	public function toPrintDymo( $customFeilds = array() )
    {
		CoreprintersController::startThreadDymo( $customFeilds );
	}
	
	public function startThreadDymo( $customFeilds = array() )
    {		
		App::import( 'Component','Coreprint' );
		$Coreprint = new CoreprintComponent(new ComponentCollection());		
		return $Coreprint->setCurlAuth2( $customFeilds );
	}
        
    public function toAllPrinterNames()
    {
		App::import( 'Component','Coreprint' );
		$Coreprint = new CoreprintComponent(new View());
		$getPrinterData = $Coreprint->getAllOnPrinters();
				
		$setPrinterDataArray[] = array();
		$inc_ = 0;foreach( $getPrinterData as $indexPrinter => $indexPrinterValue )
		{
			$setPrinterDataArray[$inc_][0] = $indexPrinterValue->id;
			$setPrinterDataArray[$inc_][1] = $indexPrinterValue->name;
			$setPrinterDataArray[$inc_][2] = $indexPrinterValue->computer->state;
			$setPrinterDataArray[$inc_][3] = $indexPrinterValue->state;
		$inc_++;
		}	
		pr($setPrinterDataArray); exit;
	return $setPrinterDataArray; 		
	}
	
	public function toAllPcs()
    {
		App::import( 'Component','Coreprint' );
		$Coreprint = new CoreprintComponent(new View());
		$getPrinterData = $Coreprint->getAllOnComputers();
		pr($getPrinterData); exit;
				
		$setPrinterDataArray[] = array();
		$inc_ = 0;foreach( $getPrinterData as $indexPrinter => $indexPrinterValue )
		{
			$setPrinterDataArray[$inc_][0] = $indexPrinterValue->id;
			$setPrinterDataArray[$inc_][1] = $indexPrinterValue->name;
			$setPrinterDataArray[$inc_][2] = $indexPrinterValue->computer->state;
			$setPrinterDataArray[$inc_][3] = $indexPrinterValue->state;
		$inc_++;
		}	
		pr($setPrinterDataArray); exit;
	return $setPrinterDataArray; 		
	}
	
	public function getFullPcDetail()
    {
		App::import( 'Component','Coreprint' );
		$Coreprint = new CoreprintComponent(new View());
		$getPrinterData = $Coreprint->getAllOnPrinters();
				
		$setPrinterDataArray[] = array();
		/*$inc_ = 0;foreach( $getPrinterData as $indexPrinter => $indexPrinterValue )
		{
			$setPrinterDataArray[$inc_][0] = $indexPrinterValue->id;
			$setPrinterDataArray[$inc_][1] = $indexPrinterValue->name;
			$setPrinterDataArray[$inc_][2] = $indexPrinterValue->computer->state;
			$setPrinterDataArray[$inc_][3] = $indexPrinterValue->state;
		$inc_++;
		}*/
	return $getPrinterData; 		
	}
	
	public function getFullPcDetailDymo()
    {
		App::import( 'Component','Coreprint' );
		$Coreprint = new CoreprintComponent(new ComponentCollection());
		$getPrinterData = $Coreprint->getAllOnPrintersDymo();
				
		$setPrinterDataArray[] = array();
		/*$inc_ = 0;foreach( $getPrinterData as $indexPrinter => $indexPrinterValue )
		{
			$setPrinterDataArray[$inc_][0] = $indexPrinterValue->id;
			$setPrinterDataArray[$inc_][1] = $indexPrinterValue->name;
			$setPrinterDataArray[$inc_][2] = $indexPrinterValue->computer->state;
			$setPrinterDataArray[$inc_][3] = $indexPrinterValue->state;
		$inc_++;
		}*/
	return $getPrinterData; 		
	}
	
}

?>
