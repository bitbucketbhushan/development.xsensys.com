<?php
error_reporting(0);
set_time_limit(20);

class ReorderController extends AppController
{
    var $name = "Reorder";
    var $components = array('Session','Upload','Common','Auth','Paginator');
  	var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
  	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('virtualInTransitStock','lifeOfItem','chk_supplier_mappings','minStockLevel'));
			$this->GlobalBarcode = $this->Components->load('Common'); 
    } 
	
	public function index()
    {
		$this->layout = 'index';
		$this->loadModel( 'SupplierDetail' );
 		$suppliers = $this->SupplierDetail->find( 'all' ); 
		$this->set('suppliers',$suppliers);				
 	}
	
	public function Dashboard()
    {
		$this->layout = 'index';
 	} 
	
	public function UploadFile()
    {
		$this->layout = 'index';
		$this->loadModel('Product');
		$this->loadModel('PurchaseOrder');
		$this->loadModel('SalesAverage');
		
		$name		= $this->request->data['upload']['import_file']['name'];
		$nameExplode= explode('.', $name);
		$nameNew 	= end( $nameExplode );
		$filename	= WWW_ROOT. 'files'.DS.'reorder_lavel'.DS.$nameExplode[0].'__'.time().".".$nameNew; 
		if( $nameNew == 'csv')
		{
			move_uploaded_file($this->request->data['upload']['import_file']['tmp_name'],$filename); 
			$fileData = file_get_contents($filename);
			foreach(explode("\n",$fileData) as $k => $val){
				if($k > 0){
					$data = explode(",",$val);
					$sku = trim($data[0]);
					$qty = trim($data[1]); 
					$product = $this->Product->find('first', array('conditions' => array('product_sku' => $sku),'fields'=>array('product_sku','min_stock_level')));
					if(count($product) > 0){
						$this->Product->query("UPDATE `products` SET `min_stock_level` = '".$qty."' WHERE `product_sku` = '".$sku."';");
					}
				}
			}
 			$this->Session->setflash(  "File uploaded successfully.",  'flash_success' );
			$this->redirect($this->referer());	
		}
		else
		{
			$this->Session->setFlash('Please upload valid file( CSV ).', 'flash_danger');
			$this->redirect($this->referer());	
		}	
		exit;
		
 	}   
	public function getSellingStores() 
	{
 		$this->layout = 'index';
		$this->loadModel( 'SourceComany' );
		$company == '';
		if($this->request->data['buying_company'] == 'euraco'){
			$company = 'costbreaker';
		}else if($this->request->data['buying_company'] == 'fresher'){
			$company = 'rainbow';
		}else if($this->request->data['buying_company'] == 'esl'){
			$company = 'marec';
		} 
		if($company != ''){
			$buying_company = $this->SourceComany->find('all', array('conditions' => array('m_channel' => $company),'fields'=>array('channel_title','country','channel_type')));
		}else{
			$buying_company = $this->SourceComany->find('all', array('fields'=>array('channel_title','country','channel_type'),'order'=>'channel_title'));
		}
		
		$select = ''; $options = [];
		if(count($buying_company) > 0){
			/*$select = '<select name="stores" id="stores" class="form-control" multiple="multiple" onchange="loadajax()">
									<option value="all">ALL</option>';
			foreach($buying_company as $v){
				$select .='<option value="'.$v['SourceComany']['channel_title'].'">'.$v['SourceComany']['channel_title'].'</option>';
			}
			$select .= '</select>'; */
			foreach($buying_company as $v){
				$options[] = $v['SourceComany']['channel_title'];
			}
		}
		
 		//$data['html'] = $select;
		$data['options'] = $options;
		echo json_encode($data);
		exit;
	}
	
	public function Quotations() 
	{
 		$this->layout = 'index';
		$this->loadModel( 'Quotation' );
		//$quotations = $this->Quotation->find( 'all',array('order' => 'id DESC') ); 
 		$this->paginate = array('order'=>'id DESC','limit' => 50 );
 		$quotations = $this->paginate('Quotation');
		
		$this->set('quotations',$quotations);	
	}
	
	public function deleteQuotation($id = 0) 
	{
		$this->layout = 'index';
		$this->loadModel( 'Quotation' );
		$this->loadModel( 'QuotationItem' );
		
		$this->Quotation->query( "DELETE FROM `quotations` WHERE `quotations`.`id` = {$id} AND `po_status` = 0"); 
		$this->QuotationItem->query( "DELETE FROM `quotation_items` WHERE `quotation_items`.`quotation_inc_id` = {$id}"); 
		$this->Session->setflash(  "Quotation deleted successfully.",  'flash_success' );
	 	$this->redirect( Router::url( $this->referer(), true ) );
	}
	
	public function QuotationItems($qid = null) 
	{
		$this->layout = 'index';
		$this->loadModel( 'QuotationItem' );
		//$quotation_items = $this->QuotationItem->find( 'all',array('conditions'=>['quotation_inc_id'=>$qid],'order' => 'id DESC') ); 
		
		$this->paginate = array('conditions'=>['quotation_inc_id'=>$qid],
 				  'order'=>'id DESC','limit' => 50 );
 		$quotation_items = $this->paginate('QuotationItem');
 		
		$this->set('quotation_items',$quotation_items);	
	}
	
	public function deleteQuotationItem($id = 0) 
	{
		$this->layout = 'index';
		$this->loadModel( 'QuotationItem' );
		$this->QuotationItem->query( "DELETE FROM `quotation_items` WHERE `quotation_items`.`id` = {$id}"); 
		$this->Session->setflash(  "Item deleted successfully.",  'flash_success' );
	 	$this->redirect( Router::url( $this->referer(), true ) );
	}
	
 	public function getQuotation() 
	{
		$this->layout = 'ajax';
		$this->autoRender = false;
		$this->loadModel( 'Product' ); 
 		$this->loadModel( 'SalesAverage' ); 
		$this->loadModel( 'PurchaseOrder' );
		$this->loadModel( 'SupplierDetail' );
		$this->loadModel( 'CheckIn' );  
				
		$sup = $this->SupplierDetail->find('first', array('conditions' => array('supplier_code' => $this->request->data['supplier_code']),'fields'=>array('lead_time','fba_lead_time','fba_uk_lead_time')));
		$lead_time = 0;
		$fba_lead_time = 0;
		$fba_uk_lead_time = 0;
		if(count($sup) > 0){
			$lead_time = $sup['SupplierDetail']['lead_time'];
			$fba_lead_time = $sup['SupplierDetail']['fba_lead_time'];
			$fba_uk_lead_time = $sup['SupplierDetail']['fba_uk_lead_time'];
		}

 		$Where[] = "SupplierMapping.`sup_code` = '".$this->request->data['supplier_code']."'";
		if(isset($this->request->data['stock_type']) && $this->request->data['stock_type'] != 'all'){
			//$Where[] = " `nature_of_sku` = '".$this->request->data['stock_type']."' ";	
		}
		if(isset($this->request->data['master_sku']) && $this->request->data['master_sku'] != ''){
			$Where[] = " SalesAverage.`master_sku` = '".$this->request->data['master_sku']."' ";	
		}
 		if(isset($this->request->data['zero_vs']) && $this->request->data['zero_vs'] != ''){
			$Where[] = " `virtual_stock` = 0 ";	
		}
		if(isset($this->request->data['warehouse']) && $this->request->data['warehouse'] != ''){
			$Where[] = " SalesAverage.`warehouse` = '".$this->request->data['warehouse']."' ";	
		} 
 		if(count($Where) > 0){				  
		 	
			$sql_query = ''; $where_col ='';
			if(count($Where)>0){
				$w = 1;
				$count_lenght_of_where = count($Where);
				foreach ($Where as $k) {
					$where_col .= $k;
					if ($w < $count_lenght_of_where) {
						$where_col .=' AND ';
					}
					$w++;
				 }
				 $sql_query =' WHERE '.$where_col;			
			}	
		}
		$sql = "SELECT SalesAverage.master_sku, SalesAverage.product_name as title, SalesAverage.barcode, SalesAverage.`fresh_required_qty`,SalesAverage.`safety_stock`, SalesAverage.`virtual_stock`, SalesAverage.`stock_in_transit`, SalesAverage.`required_qty`, SalesAverage.`fba_req_qty`, SalesAverage.`fba_virtual_stock`, SalesAverage.`fba_stock_in_preparation`, SalesAverage.`fba_stock_in_tansit`			 							
									FROM sales_averages SalesAverage
 										JOIN  supplier_mappings SupplierMapping 
									ON SalesAverage.`master_sku` = SupplierMapping.`master_sku` $sql_query";
 		$product = $this->SalesAverage->query($sql); 
		 
		$dataArray = []; $case_size = 'case_size';
		if(isset($this->request->data['case_size'])){
			$case_size = $this->request->data['case_size'];
		}
		$all_skus = []; $all_packs = [];
		if(count($product)>0){
 			foreach($product as $val){
				$master_sku = $val['SalesAverage']['master_sku'];	
				$all_skus[$master_sku] = $master_sku;
 			}
			$pos = $this->PurchaseOrder->find('all', array('conditions' => array('purchase_sku' => $all_skus),'fields'=>array('purchase_qty','purchase_sku')));
			if(count($pos)>0){
				foreach($pos as $po){
					$all_packs[$po['PurchaseOrder']['purchase_sku']] = 1;//$po['PurchaseOrder']['purchase_qty'];
				}
			}
		}
		
		$min_stock_level = []; $fba_stock_level = []; 
		$pros = $this->Product->find('all', array('conditions' => array('product_sku' => $all_skus),'fields'=>array('min_stock_level','product_sku','fba_stock')));
 		foreach($pros as $pr){
			$min_stock_level[$pr['Product']['product_sku']] = $pr['Product']['min_stock_level'];
			$fba_stock_level[$pr['Product']['product_sku']] = 1;//$pr['Product']['fba_stock'];
 		}
		
		$stock_req   = $this->request->data['stock_req'];
		$expiry_date = date("Y-m-d", strtotime("+{$stock_req} days"));
		  
	/*	$stockDetail = $this->CheckIn->find('all', array('conditions' => array( 'expiry_date <= ' => $expiry_date,'expiry_date >= ' => date('Y-m-d'),'is_expired'=>0),'fields'=>['id','sku','date','expiry_date','qty_checkIn','selling_qty','damage_qty','expired_selling_qty']) );
	  
		$rec =[]; $recom = [];
		foreach($stockDetail as $inv )
		{
 			$sku = trim($inv['CheckIn']['sku']);
			$qty = $inv['CheckIn']['qty_checkIn'] - ($inv['CheckIn']['selling_qty'] + $inv['CheckIn']['damage_qty'] + $inv['CheckIn']['expired_selling_qty']);
			$rec[$sku][] = $qty;
   		}
		
		foreach($rec as $sk => $arr){
			$recom[$sk] = array_sum($rec[$sk]);
		}*/
		$sales_array = $this->getAllSkuSale($this->request->data['avr_sale_w']);
		$fba_array = $this->getAllSkuFbaSale($this->request->data['avr_sale_w']);
		//pr($sales_array );pr($fba_array );
		$weekly_sales = [];//$this->getWeeklySkuSale($this->request->data['weekly_sale']);
		$round_up 		= 1; 
		if( count($product) > 0 ){
		
			foreach($product as $val){
 				$master_sku = $val['SalesAverage']['master_sku'];
  				$total_days = $this->request->data['avr_sale_w'];
				if(isset($sales_array[$master_sku])){
				 $total_days_av_sale    = $sales_array[$master_sku];
				}else{
 			  	  $total_days_av_sale    = 0; 
				}
				if(isset($fba_array[$master_sku])){
				  $fba_days_av_sale  = $fba_array[$master_sku];
				}else{
 			  	  $fba_days_av_sale  = 0; 
				}
				//$total_days_av_sale = ceil($total_days_sale/$total_days);	
				$weekly_av_sale 	= 1;//ceil($total_weekly_sale/$this->request->data['weekly_sale']);			
				
				if($this->request->data['stock_type'] == 'merchant'){
					$stock_required = $total_days_av_sale * $this->request->data['stock_req'];
				}else if($this->request->data['stock_type'] == 'fba'){
					$stock_required = $fba_days_av_sale * $this->request->data['stock_req'];
				}else{
					$stock_required = ($total_days_av_sale * $this->request->data['stock_req']) + ($fba_days_av_sale * $this->request->data['stock_req']);
				}
				
 				$stock_to_be_expired = 0;
				if(isset($recom[$master_sku])){
					$stock_to_be_expired = $recom[$master_sku];
				}
				
 				$total_stock_req  	= $stock_required ;//+ $val['SalesAverage']['safety_stock'];
				
				if(isset($this->request->data['in_transit_excl']) && $this->request->data['in_transit_excl'] != ''){
					$po_qty = $total_stock_req - $val['SalesAverage']['virtual_stock'];	
				}else{
 					$po_qty = $total_stock_req - $val['SalesAverage']['virtual_stock'] - $val['SalesAverage']['stock_in_transit'];	
				}	
 				
				///////////
				$unit = 1; $order_case = '-';
				if(isset($all_packs[$master_sku])){
					$order_case = strtoupper($all_packs[$master_sku]);
  				}
				
				$final_qty = ($po_qty > 0) ?  $this->roundUpToAny($po_qty,$round_up) : $po_qty;
 				if($val['SalesAverage']['fresh_required_qty'] > 0){ $final_qty = $val['SalesAverage']['fresh_required_qty'];}
 				//if($stock_required) $final_qty = $stock_required; 
				
 				$min_sl = 0 ;$min_fba_sl = 0 ;
				if(isset($min_stock_level[$master_sku])){
					$min_sl = max($min_stock_level[$master_sku],0);
  				}
				if(isset($fba_stock_level[$master_sku])){
					$min_fba_sl = max($fba_stock_level[$master_sku],0);
  				}
  		 
 				if($this->request->data['stock_type'] == 'fba'){
					$min_sl = $min_fba_sl;
				}else{
					$min_sl = $min_sl + $min_fba_sl;
				}
				
				
				$stock_expire_with_in_lead_time = 0 ;
				
				if(isset($recom[$master_sku])){
					$stock_expire_with_in_lead_time = max($recom[$master_sku],0);
  				}
				 
				$reconsider_stock = $val['SalesAverage']['virtual_stock'] - $stock_to_be_expired;
				
				if($reconsider_stock > 0){
					$final_qty = $total_stock_req - ($reconsider_stock + $val['SalesAverage']['stock_in_transit']) + $min_sl;
				}else{
					$final_qty = $total_stock_req - $val['SalesAverage']['stock_in_transit'] + $min_sl;
				}
				 $final_qty = max($final_qty,0);
				 
					
 				$dataArray[$master_sku] = array(
				'sku' => $master_sku,
				'title' => $val['SalesAverage']['title'],
				'barcode' => $val['SalesAverage']['barcode'],
				'stock_to_be_expired' => $stock_to_be_expired,
				'stock_expire_with_in_lead_time' => $stock_expire_with_in_lead_time,
				'min_stock_level' => $min_sl,
				'order_case' => $order_case,
				'final_qty' => $final_qty,
 				'sales_qty' => $total_days_av_sale,
 				'total_weekly_sale' => $total_weekly_sale,
 				'weekly_av_sale' => $weekly_av_sale,
				'stock_required' => $stock_required,
				'safety_stock' =>$val['SalesAverage']['safety_stock'],
				'total_stock_req' => $total_stock_req,
				'virtual_stock' => $val['SalesAverage']['virtual_stock'],
				'stock_in_transit' => $val['SalesAverage']['stock_in_transit'],
				'po_qty' => $po_qty,
				'required_qty' => $stock_required,
				'fresh_required_qty' => $val['SalesAverage']['fresh_required_qty'],
				'quote_qty_in_transit' => $val['SalesAverage']['quote_qty_in_transit'],
				'sale_of_days_avr' => $this->request->data['avr_sale_w'],
				'fba_virtual_stock' => $val['SalesAverage']['fba_virtual_stock'],
				'fba_stock_in_preparation' => $val['SalesAverage']['fba_stock_in_preparation'],
				'fba_stock_in_tansit' => $val['SalesAverage']['fba_stock_in_tansit'],
				'fresh' => 0 );
			}
 			$sort_by = isset($this->request->data["name"])? $this->request->data["name"] :'sales_qty';		
			$order_by = $orderby = isset($this->request->data["orderby"]) ? $this->request->data["orderby"] : 'DESC';	
			if(isset($this->request->data["orderby"]) && $this->request->data["orderby"] == 'ASC'){ 			
				$sorted = $this->sksort($dataArray, $sort_by, true);
			}else{
				$sorted = $this->sksort($dataArray, $sort_by);
			}
			
 			if(isset($this->request->data['action']) && ($this->request->data['action']=='generate_po')){ 
		 		$this->generatePo($sorted, $this->request->data['stock_type'], $this->request->data['supplier_code'],$this->request->data['avr_sale_w']);
				exit;
			}
					
			if(isset($this->request->data["page"])){
				$page_number = filter_var($this->request->data["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); 
				if(!is_numeric($page_number)){die('Invalid page number!');} 
			}else{
				$page_number = 1; 
			}
			
			$item_per_page  = 50;
			$total_rows 	= count($sorted); 		
			$total_pages	= ceil($total_rows/$item_per_page);
			$page_position  = (($page_number-1) * $item_per_page);
			
			$sortedArry 	= array_slice( $sorted, $page_position,  $item_per_page );
		 
			$count 			= 0;			
			$msg['data']    = '';
			foreach($sortedArry as $sku => $v){	
			
 				$final_qty = $v['final_qty'];
 				if($v['fresh_required_qty'] > 0){ $final_qty = $v['fresh_required_qty'];}
 				//if($v['required_qty']) $final_qty = $v['required_qty']; 
				 	
   				$count++; 
				$msg['data'].='<tr>';
				$msg['data'].='<td><div>'.$v['sku'].'</div><div class="small"><small>'.$v['title'].'</small></div></td>';
				
				if($v['sales_qty']){
					$msg['data'].=' <td>'.$v['sales_qty'].'</td>';
				}else{
					$msg['data'].=' <td>-</td>';
				}
 				 					
				$msg['data'].='<td>'.$v['stock_required'] .'</td>';
				
				
				$msg['data'].='<td>'.$v['order_case'].'</td>';
				//$msg['data'].='<td>'.$unit.'</td>';
				//$msg['data'].='<td>'.$v['safety_stock'].'</td>';
				$msg['data'].='	<td>'.$v['stock_required'].'</td>';
				
				
			//	$msg['data'].='<td>'.$v['stock_expire_with_in_lead_time'] .'</td>';
				
				$msg['data'].='<td><center><input type="number" min="1" name="min_stock_level" class="form-control" id="min_stock_level_'.$v['sku'].'" value="'.$v['min_stock_level'].'" onClick="ChangeMinStock(\''.$v['sku'].'\')"></center></td>';  
				
				$msg['data'].='<td>'.$v['virtual_stock'].'</td>';
				$msg['data'].='<td>'.$v['fba_virtual_stock'].'</td>';
				$msg['data'].='<td>'.$v['fba_stock_in_preparation'].'</td>';
				//$msg['data'].='<td>'.$v['fba_stock_in_tansit'].'</td>';
				
				/*'fba_virtual_stock' => $val['SalesAverage']['fba_virtual_stock'],
				'fba_stock_in_preparation' => $val['SalesAverage']['fba_stock_in_preparation'],
				'fba_stock_in_tansit' =*/
				
				
 				$msg['data'].='<td>'.$v['stock_in_transit'].'</td>';
				if($v['fresh']){
					$msg['data'].='<td>'.$final_qty.'</td>';
				}else{
					$msg['data'].='<td><input type="text" value="'.$final_qty.'" id="'.$v['sku'].'" size="5" class="cqty" onchange="updateQty(\''.$v['sku'].'\')"></td>';
				}			
				$msg['data'].='</tr>';		
			}	
			
			$msg['paginate'] = '<div class="total_pages">Total Page: '.$total_pages.' Items #'.$count.'</div>
			<div class="paginate">'.$this->paginate_function($item_per_page, $page_number, $total_rows, $total_pages,$sort_by,$order_by,$orderby).	'</div>
			<div class="total_rows">Total Row: '.$total_rows.'</div>';	
		}else{
			$msg['paginate'] ='';		
			$msg['data']='<tr><td colspan="11"><div align="center"><div class="alert alert-danger fade in">No data found!</div></div></td></tr>';
		} 
		$msg['data'] = utf8_encode($msg['data']);
		echo json_encode($msg);
		exit;
	}
	 
	public function  updateRequiredStock(){	
 		
		$this->layout = '';
		$this->autoRender = false; 
		$this->loadModel('SalesAverage'); 
 		if($this->request->data['sku'] !='' && $this->request->data['stock_req'] !=''){		
			 $sql="UPDATE `sales_averages` SET `required_qty` =  '".$this->request->data['stock_req']."' WHERE `master_sku` = '".$this->request->data['sku']."'";
			 $this->SalesAverage->query($sql) ;
			 $msg['msg'] = 'Data updated';		
		}else{
			$msg['msg'] = '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Invalid '.$this->request->data['sku'].'!</div>';
			$msg['error'] = 'yes';
		}		
		echo json_encode($msg);
 		exit;
	}
	
	public function getAllSkuSale($days = 5){
	
		$this->autorender = false;
		$this->loadModel('SalesEod'); 
		$this->loadModel('StockEod');
		
       	$getFrom 	= date('Y-m-d' ,strtotime("- {$days} days"));
		$getEnd 	= date('Y-m-d');
		
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		 
 		$sales = []; $skusales = [];
		$all_data =  $this->SalesEod->find( 'all', $paramHostory); 
 		if(count($all_data) > 0){
			foreach($all_data as $items){
  				$normal_stock = json_decode($items['SalesEod']['merchant_sale'],1);
				foreach($normal_stock as $sku => $stock){ 
					if($stock != '-'){
						$sales[$sku][$items['SalesEod']['sale_date']] = $stock;	
						//$skusales[$sku][] = $stock;	
					}
  				}	
			}	
   		}	
		
		foreach($sales as $sku => $saleArr){ 
 			$skusales[$sku] = ceil(array_sum($sales[$sku])/count($sales[$sku]));
		}
  		return $skusales;
 	}
	public function getAllSkuFbaSale($days = 5){
	
		$this->autorender = false;
		$this->loadModel('SalesEod'); 
		$this->loadModel('StockEod');
		
       	$getFrom 	= date('Y-m-d' ,strtotime("- {$days} days"));
		$getEnd 	= date('Y-m-d');
		
		$paramHostory = array(
 			'conditions' => array(
 				'SalesEod.sale_date  >=' => $getFrom.' 00:00:00',
				'SalesEod.sale_date  <=' => $getEnd.' 23:59:59',
 			)
 		);
		 
 		$sales = []; $skusales = [];
		$all_data =  $this->SalesEod->find( 'all', $paramHostory); 
 		if(count($all_data) > 0){
			foreach($all_data as $items){
  				$normal_stock = json_decode($items['SalesEod']['fba_sale'],1);
				foreach($normal_stock as $sku => $stock){ 
					if($stock != '-'){
						$sales[$sku][$items['SalesEod']['sale_date']] = $stock;	
						//$skusales[$sku][] = $stock;	
					}
  				}	
			}	
   		}	
		
		foreach($sales as $sku => $saleArr){ 
 			$skusales[$sku] = ceil(array_sum($sales[$sku])/count($sales[$sku]));
		}
  		return $skusales;
 	}

	public function getAllSkuSale04102021($days = 10){
	
		$this->loadModel('Order'); 
		$this->loadModel('OrdersItem'); 
		$order_ids = array(); $order_items = array();
		$this->Order->unbindModel( array('hasMany' => array( 'OrdersItem' ) ) );
		$sales_qty = [];
	 	$start_date = date('Y-m-d',strtotime( "-{$days}days"));	
		$last_date  = date('Y-m-d');
		 
 	 	$query = "SELECT  `OrdersItem`.`qty_ordered`,`OrdersItem`.`sku` FROM  `orders_items` AS `OrdersItem` WHERE `OrdersItem`.`added_date` BETWEEN '".$start_date."' AND  '".$last_date."' ";
		
		$orders = $this->OrdersItem->query($query); 
 
		if(count($orders) > 0){
			foreach($orders as $order){
				$sales[$order['OrdersItem']['sku']][] = $order['OrdersItem']['qty_ordered'];
				
			}
			foreach(array_keys($sales) as $sku ){
				$sales_qty[$sku] = array_sum($sales[$sku]); 
 			}
		} 
		  
		return  $sales_qty;
 	}
	
	public function getWeeklySkuSale($days = 7){
	
		$this->loadModel('Order'); 
		$this->loadModel('OrdersItem'); 
		$order_ids = array(); $order_items = array();
		$this->Order->unbindModel( array('hasMany' => array( 'OrdersItem' ) ) );
		$sales_qty = [];
	 	$start_date = date('Y-m-d',strtotime( "-{$days}days"));	
		$last_date  = date('Y-m-d');
		 
 	 	$query = "SELECT  `OrdersItem`.`qty_ordered`,`OrdersItem`.`sku` FROM  `orders_items` AS `OrdersItem` WHERE `OrdersItem`.`added_date` BETWEEN '".$start_date."' AND  '".$last_date."' ";
		
		$orders = $this->OrdersItem->query($query); 
 
		if(count($orders) > 0){
			foreach($orders as $order){
				$sales[$order['OrdersItem']['sku']][] = $order['OrdersItem']['qty_ordered'];
				
			}
			foreach(array_keys($sales) as $sku ){
				$sales_qty[$sku] = array_sum($sales[$sku]); 
 			}
		} 
		  
		return  $sales_qty;
		 
	}
	
	public function getPreviousMSale($sku,$days = 5){
 		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('OrdersItem');
		$start_date = date('Y-m-d',strtotime( "-{$days}days"));	
		$last_date  = date('Y-m-d');
		 
 	 	$query = "SELECT SUM(`OrdersItem`.`qty_ordered`) as qty_ordered FROM  `orders_items` AS `OrdersItem` WHERE `OrdersItem`.`added_date` BETWEEN '".$start_date."' AND  '".$last_date."' AND `OrdersItem`.`sku` = '".$sku."' ";
		
		$orders = $this->OrdersItem->query($query);
 		$qty = 0;
		if(count($orders) > 0){
			$qty = $orders[0][0]['qty_ordered'] ;	 
		} 
 		return $qty;
 	}
	public function ChangeMinStockLavel()
	{
		$this->layout = 'ajax';
		$this->autoRender = false;
		$this->loadModel( 'Product' ); 
		$savedata = [];
		 
		$product =  $this->Product->find('first', array('conditions' => array('Product.product_sku' => $this->request->data['sku'])));
		if(count($product) > 0){
 			$savedata['sku'] = $product['Product']['product_sku'] ;
			$savedata['msg'] = 'ok';
 			$this->Product->query("UPDATE `products` SET `min_stock_level` = '".$this->request->data['qty']."' WHERE  `product_sku` ='".$product['Product']['product_sku']."'");
			file_put_contents(WWW_ROOT .'logs/Reorder.min_stock_level_'.date('dmY').'.log', $product['Product']['product_sku']."\t".$product['Product']['min_stock_level']."\t".$this->request->data['qty']."\t".$this->Session->read('Auth.User.username')."\n",FILE_APPEND | LOCK_EX);
 		}
		 
		echo json_encode($savedata);
		exit;
	}
	
 	public function chk_supplier_mappings(){
 		
		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('SupplierMapping');
		$results = $this->SupplierMapping->find('all',array('order'=>'master_sku')); //,'limit'=>3000,'page'=>1
		foreach($results as $v){
		
			if($v['SupplierMapping']['sup_code'] == 'oa-fcg-' || $v['SupplierMapping']['sup_code'] == 'oa-fcg--'){
				$sup_code = 'oa-fcg' ;
			}
		
			$result = $this->SupplierMapping->find('all', array('conditions' => array("sup_code LIKE" => $sup_code.'%','master_sku' => $v['SupplierMapping']['master_sku'],'supplier_sku' => $v['SupplierMapping']['supplier_sku']),'order'=>'id desc')); 
			if(count($result) > 0){
				foreach($result as $c => $v){
					if($c > 0){						
						$this->SupplierMapping->query("DELETE FROM `supplier_mappings` WHERE `id` = '".$v['SupplierMapping']['id']."'");
						echo '<br>delete = '.$v['SupplierMapping']['id'] .'= '.$v['SupplierMapping']['master_sku'];
					}else{
						echo '<br>keep '.$c.' = '.$v['SupplierMapping']['id'] ;
					}
				}
			}
		}
/*WHERE `sup_code` LIKE 'management-all' 
DELETE FROM `a1b20310_wmsdev`.`supplier_mappings` WHERE `sup_code` LIKE 'management-all' 
UPDATE `a1b20310_wmsdev`.`supplier_mappings` SET `sup_code` = 'DCS' WHERE `sup_code` LIKE '%_dcs%';
*/
 	  exit;
 		 
 	}
	//$this->generatePo($sorted, $this->request->data['stock_type'], $this->request->data['sup_code']);
	public function generatePo($dataArray = [], $nature_of_po = '', $sup_code = [],$sale_of_days_avr = ''){

		$this->layout = '';
		$this->autoRender = false;
		$this->loadModel('Quotation');
		$this->loadModel('QuotationItem');
		$this->loadModel('SupplierDetail'); 
 		$quotation = array();
		 
		foreach($dataArray as $sku => $v){	
			 
			if( $v['fresh'] > 0 ){
				$v['required_qty'] = 1;	
			} 
			
			if($v['po_qty'] > 0 || $v['required_qty'] > 0 ){	
				
				if($v['required_qty'] ){
					$v['po_qty'] = $v['required_qty'] ;
				}					
  				$quotation[] = array('title' => $v['title'], 'sku' => $v['sku'],'barcode' => $v['barcode'],'po_qty'=>$v['po_qty'],'virtual_stock'=>$v['virtual_stock'],'stock_in_transit'=>$v['stock_in_transit'],'final_qty'=>$v['final_qty'],'sales_qty' => $v['sales_qty']); 
 								
			}	
		}	
 		 
  		/**************************************Generate Quate*****************************************************/ 
		if(count($quotation)> 0){
			 
			// print_r($suppliers); exit;
			   
			App::import('Vendor', 'PHPExcel/IOFactory');
			App::import('Vendor', 'PHPExcel');
			$objPHPExcel = new PHPExcel();
			
			$file_folder = 'quotation/'; $zip_file_name = '';						
			if(extension_loaded('zip'))
			{
				//$zip = new ZipArchive(); // Load zip library 
				//$zip_file_name = "quotation".date('dmYHis').".zip"; // Zip name
				//$zip_name  = $file_folder.$zip_file_name; 
				//$zip->open($zip_name, ZipArchive::CREATE);
			}
			$f_name = '';
								
			$quotation = $this->sksort($quotation, 'sales_qty');	
			
 			$results = $this->SupplierDetail->find('all', array('conditions' => array('supplier_code' => $sup_code),'ORDER'=>'supplier_name'));
		 		
			if(count($results) > 0){			
				foreach($results as $sup){		 			
 				
					$row = 1;
					$rightBorder = array(
								  'borders' => array(
										'right' => array(
										  'style' => PHPExcel_Style_Border::BORDER_THIN
										),	
										'left' => array(
										  'style' => PHPExcel_Style_Border::BORDER_THIN
										)																
								  )
								);									
					
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':K'.$row);
					
					$row++;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Euraco Group Ltd')
							->setCellValue('E'.$row, 'Purchase Order');				
					$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);	
					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(70);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->getFont()->setSize(20)->setBold(true);					
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('EB984E');
 					$row++;		
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':K'.$row);		
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Euraco Group Ltd 49 Oxford Road St. Helier Jersey JE2 4LJ United Kingdom");
					 
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
 					$row++;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':K'.$row);
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Tel :0845 557 5775, Fax :0845 557 5776, Email : accounts@euracogroup.co.uk');
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
 					$row++;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':K'.$row);
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'GST No: 	33859');
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
 					$row++;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':K'.$row);
					
					$row++;				
					$BStyle = array(
								  'borders' => array(
									'top' => array(
									  'style' => PHPExcel_Style_Border::BORDER_THIN
									),
									'bottom' => array(
									  'style' => PHPExcel_Style_Border::BORDER_THIN
									)
								  )
								);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->getFont()->setSize(12)->setBold(true);	
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Purchase From')
							->setCellValue('B'.$row, 'Deliver To')	
							->setCellValue('E'.$row, 'Tender Details');					
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($BStyle);																						
					unset($BStyle);
					
					$row++;
					$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(80);	
	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->getAlignment()->applyFromArray(
						array(
								 'vertical'   => PHPExcel_Style_Alignment::VERTICAL_TOP,
								 'rotation'   => 0,
								 'wrap'       => true)
					);
	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($rightBorder);	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $sup['SupplierDetail']['address']);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setWrapText(true);
					
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.$row.':D'.$row);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, "Euraco Group Ltd c/o Pro Fulfilment & Logistics,   
	Warehouse 4 Airport Cargo & Commercial Units,  
	L'avenue De La Commune,
	St Peter,
	Jersey, JE3 7BY");
	
					$quotation_number =  date('dmyHis').'_'.strtoupper($sup['SupplierDetail']['sup_code']).'_'.strtoupper($nature_of_po);
  					
					$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setWrapText(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B'.$row)->setWidth(45);
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E'.$row.':K'.$row);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, "PO. No# ".$quotation_number.																 																	    "\nDate:". date('m/d/Y'));
					$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setWrapText(true);
						
					$row++;		
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($rightBorder);					
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':K'.$row);
					$row++;
					
					$BStyle = array(
					  'borders' => array(
						'allborders' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
					);
					 
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($BStyle);
					
					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$row, 'DESCRIPTION')
								->setCellValue('B'.$row, 'OUR CODE')
								->setCellValue('C'.$row, 'BARCODE')
								->setCellValue('D'.$row, 'SUPPLIER CODE')								
 								->setCellValue('E'.$row, 'REQUIRED QTY')
								->setCellValue('F'.$row, 'CASE SIZE')	
								->setCellValue('G'.$row, 'CASE QTY')
								->setCellValue('H'.$row, 'TOTAL QTY')	
								->setCellValue('I'.$row, 'UNIT PRICE')  
								->setCellValue('J'.$row, 'CASE PRICE') 
 								->setCellValue('K'.$row, 'TOTAL') ;	
					$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(11);	
					$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);	
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->getFont()->setSize(12)->setBold(true);	
					
					/********************Save Quotation************************/
					//$file_name = $quotation_number.'.xlsx';
					$file_name = $sup['SupplierDetail']['supplier_code'].'_'.strtoupper($nature_of_po).'_'.$sale_of_days_avr.'_AVER_'.date('dmY').'.xlsx';
 					$quotation['quotation_number'] = $quotation_number;		
					$quotation['file_name'] 	   = $file_name;		
					$quotation['sup_code']    = $sup['SupplierDetail']['supplier_code'];	
					$quotation['username'] = $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');	
					$this->Quotation->saveAll( $quotation );	
					$quotation_id = $this->Quotation->getLastInsertId();
					/********************End Save Quotation**********************/	
 					
					$items = 0;
					foreach($quotation as $val){
							$supp_data = $this->getSupplierSku($sup['SupplierDetail']['supplier_code'],$val['sku']);							
							if(count($supp_data) > 0 && ($val['sku'] != $val['barcode'])){	 // filter supplier products						
								
 								foreach($supp_data as $supp_sku){
									$row++;	
									$items++;
									$case_size = 0; $case_qty = 0; $unit_price = 0; $case_price = 0; $order_case = 1;
									$po_data = $this->PurchaseOrders($supp_sku['supplier_sku'],$val['sku']);	
									if(count($po_data) > 0 )
									{
										$case_size  = ($po_data['total_qty']/$po_data['order_case']); 
										
										$unit_price = $po_data['price']; 
										$case_price = $po_data['price']; 
										$order_case = $po_data['purchase_qty']; 
										$case_qty   = round($val['final_qty']/$order_case); 
										
									} 
  									
									$final_qty = $case_size * $case_qty;
									$objPHPExcel->setActiveSheetIndex(0)
												->setCellValue('A'.$row, $val['title'])														
												->setCellValue('B'.$row, $val['sku'])
												->setCellValue('C'.$row, $val['barcode'])
												->setCellValue('D'.$row, $supp_sku['supplier_sku'])
												->setCellValue('E'.$row, $val['final_qty'])												
												->setCellValue('F'.$row, $order_case)
												->setCellValue('G'.$row, $case_qty)
												->setCellValue('H'.$row, round($case_qty*$order_case)) 
 												->setCellValue('I'.$row, $unit_price)
												->setCellValue('J'.$row, $case_price)
												->setCellValue('K'.$row, number_format(($case_qty * $case_price),2));
												
									$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($BStyle);	
									
									if(count($supp_data) > 1){
										$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('EB984E');
									}
									
									$quote = [];
									$quote['quotation_inc_id']  = $quotation_id;
									$quote['quotation_number']  = $quotation_number;
									$quote['supplier_itemcode'] = $supp_sku['supplier_sku'];
									$quote['master_sku'] 	    = $val['sku'];
									$quote['barcode'] 		    = $val['barcode'];
									$quote['calculated_qty'] 	= $val['po_qty'];								
									$quote['case_size'] 		= $case_size;
									$quote['qty'] 			    = $final_qty;
									$quote['virtual_stock'] 	= $val['virtual_stock'];
									$quote['stock_in_transit']  = $val['stock_in_transit'];
 									$this->QuotationItem->saveAll( $quote ); 
						 		}
							}
					}
					/*$row++;			
					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$row, '')														
								->setCellValue('B'.$row, '')
								->setCellValue('C'.$row, '')
								->setCellValue('D'.$row, '')
								->setCellValue('E'.$row, '')							
								->setCellValue('F'.$row, '') ;
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray($BStyle);	*/
					if($items > 0){
								
						if($sup['SupplierDetail']['supplier_code']){	
							$objPHPExcel->getActiveSheet()->setTitle($sup['SupplierDetail']['supplier_code']);	
						}									
						$f_name	.= '<li><a href="'.Router::url('/', true).'quotation/'.$file_name.'" target="_blank">'.ucfirst($sup['SupplierDetail']['supplier_code']).' '.strtoupper($nature_of_po).'</a></li>'; 
						$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');		
						$objWriter->save(WWW_ROOT.'quotation/'.$file_name);	
						//$zip->addFile( 'quotation/'.$file_name);
						
					}else{
  						 $this->Quotation->query( "DELETE FROM `quotations` WHERE `quotation_number` = '".$quotation_number."' AND `supplier_code` = '".$sup['SupplierDetail']['supplier_code']."' " ); 
						 $f_name	.= '<li class="nf">Items not found for '.ucfirst($sup['SupplierDetail']['supplier_code']).'</li>'; 
					}
							
				} 
			}			
			$msg['msg'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Report Generated.</div>';	
  			$f_name	.= '<li><a href="'.Router::url('/', true).'quotation/'.$zip_file_name.'" target="_blank">'.$zip_file_name.'</a></li>'; 
			$msg['file_link'] = $f_name;	
		//	$zip->close();		
			echo json_encode($msg);	
		}
		else{
			$msg['file_link'] = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Items not found.</div>';	
			echo json_encode($msg);	
		}

	}
  
	public function PurchaseOrders($supplier_p_code,$msku){
		$this->loadModel('PurchaseOrder');
 		$result = $this->PurchaseOrder->find('first', array('conditions' => array('supplier_code' => $supplier_p_code,'purchase_sku' => $msku),'fields'=>['price','purchase_qty']),'order DESC');
		if(count($result) > 0){			
			return	array('price'=>$result['PurchaseOrder']['price'],'purchase_qty'=> $result['PurchaseOrder']['purchase_qty']);
		}else{
			return [];
		}
	}
	
	public function getSupplierMapId($supplier_code,$msku){
		$this->loadModel('SupplierMapping');
 		$result = $this->SupplierMapping->find('first', array('conditions' => array('sup_code' => $supplier_code,'master_sku' => $msku),'fields'=>['id','supplier_sku']));
		if(count($result) > 0){			
			return	1;
		}else{
			return 0;
		}
	}
	public function getSupplierDetails($code){
		$this->loadModel('SupplierDetail');
		$sql = "SELECT * FROM supplier_details WHERE supplier_code = '".$code."'";
		$result =  $this->SupplierDetail->query($sql);	
		if(count($result) > 0){							
			return $result;			
		}else{
			return 0;
		}
	}
	public function getSupplierSku($code,$msku){
		$this->loadModel('SupplierMapping');	
		$data = [];
		$result = $this->SupplierMapping->find('all', array('conditions' => array('sup_code' => $code,'master_sku' => $msku),'fields'=>['id','supplier_sku']));
  		if(count($result) > 0){	
 			foreach($result as $val){
 				if($val['SupplierMapping']['supplier_sku']){	
					 $data[] = array('supplier_sku'=>$val['SupplierMapping']['supplier_sku'], 'id'=>$val['SupplierMapping']['id']);
				}else{
					$data[] = array('supplier_sku'=>'NoSKU', 'id' => $val['SupplierMapping']['id']);
				}
 			}	
 		}else{
			$data = [];// array('supplier_sku'=>'-', 'id'=>0);
		}
		return $data;
	}
	
	public function sksort(&$array, $subkey="id", $sort_ascending=false) {

    if (count($array))
        $temp_array[key($array)] = array_shift($array);

		foreach($array as $key => $val){
			$offset = 0;
			$found = false;
			foreach($temp_array as $tmp_key => $tmp_val)
			{
				if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey]))
				{
					$temp_array = array_merge(    (array)array_slice($temp_array,0,$offset),
												array($key => $val),
												array_slice($temp_array,$offset)
											  );
					$found = true;
				}
				$offset++;
			}
			if(!$found) $temp_array = array_merge($temp_array, array($key => $val));
		}

    if ($sort_ascending) $array = array_reverse($temp_array);

    else $array = $temp_array;
	
	return $array;
	}
	
	public function viewRecomendation()
	{
 		$this->layout = 'index';
 		$this->loadModel( 'SalesAverage' ); 
 		$this->loadModel( 'SupplierMapping' ); 
 	 	$this->loadModel( 'SupplierDetail' );  
		$chk_sa = $this->SupplierDetail->find('all', array('fields'=>array('supplier_code','lead_time','min_order_amount','supplier_name')));
		$supplier_data = [];
		if(count($chk_sa) > 0){ 
 			foreach($chk_sa as $val){
				$supplier_data[$val['SupplierDetail']['supplier_code']] = $val['SupplierDetail']['lead_time'];
			}
		}
			
		$path = WWW_ROOT.'logs/';
 		
 		$stockDetail = $this->SalesAverage->find('all');
	   
		$suppliers = [];$supplier_skus = [];
		foreach($stockDetail as $val){
			 
			$sku = $val['SalesAverage']['master_sku'];
			$smap = 'no_supplier';
			$sup = $this->SupplierMapping->find('first', array('conditions' => array( 'master_sku' => $sku),'fields'=>['sup_code','supplier_sku'],'order' => 'id') );
			if(count($sup) > 0 && !empty($sup['SupplierMapping']['sup_code']) ){
				$smap = $sup['SupplierMapping']['sup_code'] ;
				$suppliers[$smap] = $smap;	
				$supplier_skus[$smap][] = $sku;		
			} else{
				$supplier_skus[$smap][] = $sku;	
 				file_put_contents(WWW_ROOT .'logs/Recomendation_nsupp_'.date('ymd').'.log',$sku."\r\n", FILE_APPEND | LOCK_EX);	
			}
 		}
 		file_put_contents(WWW_ROOT .'logs/Recomendation.log',print_r($other,1));	 		
  				
 			//	pr($pp);exit;
	 
		$this->set('suppliers',$suppliers);	
		$this->set('supplier_data',$supplier_data);
		$this->set('supplier_skus',$supplier_skus);
		
	}
	
	public function recomendationStock()
	{
 		/*-----------------Checked in but not sold-----------------*/
		$this->layout = 'index';
 		
 	 
		$path = WWW_ROOT.'logs/';
		 
 		$rec = [];
		$date = date("Y-m-d",strtotime("+1 day"));
		
		$productArr = [];
		$products = $this->Product->find('all', array('fields'=>['product_sku','nature_of_sku']) );
  	 	foreach($products as $pro )
		{
			$productArr[$pro['Product']['product_sku']] = $pro['Product']['nature_of_sku'];
 		}
		
 		$stockDetail = $this->CheckIn->find('all', array('conditions' => array( 'expiry_date >= ' => $date),'fields'=>['id','sku','date','expiry_date','qty_checkIn','selling_qty','damage_qty','expired_selling_qty']) );
	  
		foreach($stockDetail as $inv )
		{
 			$sku = trim($inv['CheckIn']['sku']);
			$qty = $inv['CheckIn']['qty_checkIn'] - ($inv['CheckIn']['selling_qty'] + $inv['CheckIn']['damage_qty'] + $inv['CheckIn']['expired_selling_qty']);
			$rec[$sku][] = $qty;
   		}
		$recom = [];
		foreach($rec as $sk => $arr){
			$recom[$sk] = array_sum($rec[$sk]);
		}
		$ambient = []; $fresh = [];$frozen = []; $other =[]; $suppliers = []; 
		foreach($recom as $sk => $qt){
			if($qt < 1){
				
				$smap = 'no supplier';
				$sup = $this->SupplierMapping->find('first', array('conditions' => array( 'master_sku' => $sk),'fields'=>['sup_code','supplier_sku'],'order' => 'id') );
  				if(count($sup) > 0 && !empty($sup['SupplierMapping']['sup_code']) ){
 					$smap = $sup['SupplierMapping']['sup_code'] ;
  				} 
				$nature_of_sku = 'not found';
				if(isset($productArr[$sk]) && !empty($productArr[$sk])){
					$nature_of_sku = $productArr[$sk];
				}
				if($nature_of_sku == 'ambient'){
					$ambient[$smap][] = $sk ;
				}
				else if($nature_of_sku == 'fresh'){
					$fresh[$smap][] = $sk ;
				}
				else if($nature_of_sku == 'frozen'){
					$frozen[$smap][] = $sk ;
				}else{
					$other[$smap][] = $sk ;
				}
 				$suppliers[$smap][$nature_of_sku] = $smap;	
 				
 			}
		}
 				
		$this->set('ambient',$ambient);
		$this->set('frozen',$frozen);		
		$this->set('fresh',$fresh);	
		$this->set('other',$other);	
		$this->set('suppliers',$suppliers);	
	}
	
	public function minStockLevel()
    {
		//min stock lavel 
   		$this->layout = 'ajax';
		$this->autoRender = false;
 		
  		$this->loadModel('Product');
		$this->loadModel('PurchaseOrder');
  	 
 		$pack_size = []; 
 		 
		$products = $this->Product->find('all', array('fields'=>array('id','product_sku','current_stock_level','min_stock_level','barcode','product_name','nature_of_sku') ));
  		if(count($products) > 0){		
			foreach($products as $val){
				$product_sku =  $val['Product']['product_sku'];
				
				$purchase_ord = $this->PurchaseOrder->find('first', array('conditions' => array('purchase_sku'=>$product_sku),'fields'=>array('id','purchase_sku','pack_size','order_case','purchase_qty'),'order' => 'id DESC'));
 		 		$order_case = 0;
				if(count($purchase_ord) > 0){  
 					$psz = explode("X", strtoupper($purchase_ord['PurchaseOrder']['pack_size']));
					$pack_size[] = trim($psz['0']);
					$order_case = $purchase_ord['PurchaseOrder']['purchase_qty'];
 				}
				$minsl = 1; $cal_minsl = 0; 
				$_pack_size = min($pack_size);
				if($order_case > 0){
					$cal_minsl = $order_case * '0.5';
					echo $product_sku.' =='.$minsl.' == '.$cal_minsl.' oc = '.$order_case;echo '<br>';
					//$this->Product->updateAll(array('pack_size' => "'".$_pack_size."'",'min_stock_level'=>"'".$minsl."'"), array('product_sku' => $product_sku));
					//exit;
				} 
				if(empty($_pack_size)){
					//echo $product_sku.'<br>';
				}
				
				$log_file =  WWW_ROOT .'logs/minStockLevel_'.date('dmY').'.log';			 
				file_put_contents($log_file,$product_sku."\t".$_pack_size."\tm=".$cal_minsl."\r\n", FILE_APPEND | LOCK_EX);	 		
  				$this->Product->updateAll(array('pack_size' => "'". addslashes($_pack_size)."'",'min_stock_level'=>"'".max($minsl,$cal_minsl)."'"), array('product_sku' => $product_sku));
				
   			}
 		}
  		exit;
 	} 

 	public function  virtualInTransitStock(){	
 		$this->layout = 'ajax';
		$this->autoRender = false;
		$this->loadModel('Po'); 
		$this->loadModel('Product');
		$this->loadModel('PurchaseOrder');
		$this->loadModel('SalesAverage');
 		$this->loadModel('CheckIn');
		$this->loadModel('FbaShipment'); 
 		$this->loadModel('FbaShipmentItem');  
  		$this->loadModel('Skumapping');
  		$this->loadModel('FbaUploadInventory');

 		$sql = "SELECT FbaItemLocation.sku,FbaItemLocation.shipped_qty,FbaItemLocation.master_sku,FbaShipment.status  FROM fba_shipments FbaShipment LEFT JOIN fba_item_locations FbaItemLocation  ON FbaShipment.shipment_id = FbaItemLocation.shipment_id WHERE FbaShipment.status  IN('preparing','exported')";

  		$shipments = $this->FbaShipment->query($sql);
		//pr($shipments);
		//exit;
		$exported = []; $preparing = [];
	 	if(count($shipments) > 0){
			foreach($shipments as $v){
 				if($v['FbaShipment']['status'] == 'preparing'){
					$preparing[$v['FbaItemLocation']['sku']][] = $v['FbaItemLocation']['available_qty_check_in'];
				}
 				if($v['FbaShipment']['status'] == 'exported'){
					$exported[$v['FbaItemLocation']['sku']][] = $v['FbaItemLocation']['available_qty_check_in'];
				}
			}
		} 
		$fbaPreparingStock = [];
		foreach(array_keys($preparing) as $sku){
			$fbaPreparingStock[$sku] = array_sum($preparing[$sku]);
		}
		$fbaExportedStock = [];
		foreach(array_keys($exported) as $sku){
			$fbaExportedStock[$sku] = array_sum($exported[$sku]);
		}
		$fbaVstock = [];
		$fba_inv = $this->FbaUploadInventory->find('all', array('conditions'=>array('status' => 0),'fields'=>array('sku','total_units','available')));  
		if(count($fba_inv)){
			foreach($fba_inv as $v){
				$fbaVstock[$v['FbaUploadInventory']['sku']][] = $v['FbaUploadInventory']['total_units'];			
			}
		}
		$fbaVirtualStock = [];
		foreach(array_keys($fbaVstock) as $sku){
			$fbaVirtualStock[$sku] = array_sum($fbaVstock[$sku]);
		}
   		$this->Po->query("TRUNCATE sales_averages") ;
		
		$date = date("Y-m-d",strtotime("+1 day"));
		
		$po_ids = [];$purchase_qty = [];$quote_ids = [];$quote_qty = [];
		
 		$pos = $this->Po->find('list', array('conditions' => array('Po.status' => 1),'fields'=>array('id')));
 		
		if(count($pos) > 0){		
 			$purchase_ord = $this->PurchaseOrder->find('all', array('conditions' => array('PurchaseOrder.po_id' => $pos),'fields'=>array('purchase_sku','purchase_qty')));
			if(count($purchase_ord) > 0){  
				foreach($purchase_ord as $pod){
					$purchase_qty[$pod['PurchaseOrder']['purchase_sku']][] = $pod['PurchaseOrder']['purchase_qty'];
				}
			}		 
 		 
			if(count($purchase_qty) > 0){
				foreach(array_keys($purchase_qty) as $sku){
					$po_qty[$sku] = array_sum($purchase_qty[$sku]);
				}
			}
		}
 		
		
 		$avr_sale_w  = 5;//Sale Of Days	
		$weekly_sale = 2;//Weekly Sales Average
		$stock_req   = 10;//Stock Required For;
		
		$sales_array = $this->getAllSkuSale($avr_sale_w);
		$fba_array = $this->getAllSkuFbaSale($avr_sale_w);
		//$weekly_sales= $this->getWeeklySkuSale($weekly_sale);
		
		// '`Product.min_stock_level` >= `Product.current_stock_level`'
		$products = $this->Product->find('all', array('conditions' => array( '`Product.current_stock_level` < 2' ),'fields'=>array('id','Product.product_sku','Product.current_stock_level','Product.min_stock_level','ProductDesc.barcode','Product.product_name')));
		 
		foreach($products as $val){
				
			$SalesAvg = [];
			$SalesAvg['virtual_stock'] = 0 ;
			if($val['Product']['current_stock_level'] > 0){
				$SalesAvg['virtual_stock'] = $val['Product']['current_stock_level'];
			}
			$SalesAvg['safety_stock']  = 1;
			if(!empty($val['Product']['min_stock_level'])){
				$SalesAvg['safety_stock']  = $val['Product']['min_stock_level'];
			}
			$SalesAvg['barcode'] 	   = $val['ProductDesc']['barcode'];
			$SalesAvg['product_name']  = $val['Product']['product_name'];
 			$SalesAvg['master_sku']    = $val['Product']['product_sku'];
			$SalesAvg['warehouse']     = 'jersey';
			$SalesAvg['fba_virtual_stock']    	  = $fbaVirtualStock[$val['Product']['product_sku']];
			$SalesAvg['fba_stock_in_preparation'] = $fbaPreparingStock[$val['Product']['product_sku']];
			$SalesAvg['fba_stock_in_tansit']      = $fbaExportedStock[$val['Product']['product_sku']];
			
 			$product_sku =  $val['Product']['product_sku'];
 			
			//////
			 
			$stock_req   = 10; //days
 			
			$total_days 		= $avr_sale_w;
			if(isset($sales_array[$product_sku])){
			 $total_days_av_sale    = $sales_array[$product_sku];
			}else{
			  $total_days_av_sale    = 1; 
			}
			
			if(isset($fba_array[$product_sku])){
			  $fba_days_av_sale  = $fba_array[$product_sku];
			}else{
			  $fba_days_av_sale  = 0; 
			}
			
			//$total_days_av_sale = ceil($total_days_sale/$total_days);	
		//	$weekly_av_sale 	= ceil($total_weekly_sale/$weekly_sale);			
			$stock_required   	= $total_days_av_sale * $stock_req;
			$fba_stock_required = $fba_days_av_sale * $stock_req;
			
			$total_stock_req  	= $stock_required + $fba_stock_required + $SalesAvg['safety_stock'];
			////
			if($total_stock_req >= $val['Product']['min_stock_level']){
				$SalesAvg['required_qty'] = $total_stock_req ;
				
				if(isset($po_qty[$product_sku])){		
					$qty = $po_qty[$product_sku];
					$SalesAvg['stock_in_transit'] = $qty;
				} 
				
				$chk_sa = $this->SalesAverage->find('first', array('conditions' => array('SalesAverage.master_sku' => $product_sku),'fields'=>array('id')));
				if(count($chk_sa) > 0){ 
					$SalesAvg['id'] = $chk_sa['SalesAverage']['id'];
				}
				
				try {
				 $this->SalesAverage->saveAll($SalesAvg) ;
				 $msg = 'Virtual & InTransit Stock Refreshed.';
				}						
				catch(Exception $e) {
				   $msg .= $e->getMessage();
				}
			}
				 
			
		}
		
		 
		$ret['msg'] = $msg;
		echo json_encode($ret);
		exit;
	}
	
	public function lifeOfItem(){	
 		$this->layout = 'ajax';
		$this->autoRender = false;
 		$this->loadModel('Product');
		$this->loadModel('CheckIn'); 
 		 
		$skus = []; 
 		 
		$products = $this->Product->find('all', array('fields'=>array('id','product_sku','current_stock_level','life_of_item','min_stock_level','barcode','product_name','nature_of_sku') ));
  		if(count($products) > 0){		
			foreach($products as $val){
				$product_sku =  $val['Product']['product_sku'];
				$checkins = $this->CheckIn->find('all', array('conditions' => array('sku' => $product_sku),'fields'=>array('id','sku','expiry_date','date')));
				if(count($checkins) > 0){
					foreach($checkins as $v){
						$time_diff = strtotime($v['CheckIn']['expiry_date']) - strtotime($v['CheckIn']['date']);
					/*	echo	$v['CheckIn']['expiry_date'];
						echo  ' = '.$v['CheckIn']['date'];
						echo  ' = ';*/
						 $days = floor($time_diff/86400);  
						/*echo  ' = '.$product_sku;
						echo '<br>';*/
						$skus[$product_sku][] = $days ;
					}
				
				}
   			}
 		}
		
		foreach($skus as $sk => $v){
			//echo $sk .' === ' ;
			 $life_of_item = max($skus[$sk]);
			//echo '<br>';
			$this->Product->updateAll(array('life_of_item' => $life_of_item), array('product_sku' => $sk));
				 
		}
 		exit;
	}
	
 	
	public function roundUpToAny($n,$x=5) {
		return (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x;
	}
	
	public function paginate_function($item_per_page, $current_page, $total_records, $total_pages,$data_name=false,$data_value=false,$sort_by=false)
	{
		$pagination = '';
		if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
			$pagination .= '<ul class="pagination">';
			
			$right_links    = $current_page + 3; 
			$previous       = $current_page - 3; //previous link 
			$next           = $current_page + 1; //next link
			$first_link     = true; //boolean var to decide our first link
			
			if($current_page > 1){
				$previous_link = ($previous > 0)? $previous : 1;
				$pagination .= '<li class="first"><a href="#" data-page="1" data-name="'.$data_name.'" order-by="'.$sort_by.'" data-value="'.$data_value.'" title="First">&laquo;</a></li>'; //first link
				$pagination .= '<li><a href="#" data-page="'.$previous_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Previous">&lt;</a></li>'; //previous link
					for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
						if($i > 0){
							$pagination .= '<li><a href="#" data-page="'.$i.'"  data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Page'.$i.'">'.$i.'</a></li>';
						}
					}     
				$first_link = false; //set first link to false
			} 
			
			if($first_link){ //if current active page is first link
				$pagination .= '<li class="first active"><span>'.$current_page.'</span></li>';
			}elseif($current_page == $total_pages){ //if it's the last active link
				$pagination .= '<li class="last active"><span>'.$current_page.'</span></li>';
			}else{ //regular current link
				$pagination .= '<li class="active"><span>'.$current_page.'</span></li>';
			}
					
			for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
				if($i<=$total_pages){
					$pagination .= '<li><a href="#" data-page="'.$i.'" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" title="Page '.$i.'">'.$i.'</a></li>';
				}
			}
			if($current_page < $total_pages){ 
					$next_link = ($i > $total_pages) ? $total_pages : $i;
					$pagination .= '<li><a href="#" data-page="'.$next_link.'" data-name="'.$data_name.'" order-by="'.$sort_by.'" data-value="'.$data_value.'" title="Next">&gt;</a></li>'; //next link
					$pagination .= '<li class="last"><a href="#" data-name="'.$data_name.'" order-by="'.$sort_by.'"  data-value="'.$data_value.'" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
			}
			
			$pagination .= '</ul>'; 
		}
		return $pagination; //return pagination links
	}
	
	public function sendEmail($data = [])
	{ 			 
		$mailSubject = $data['subject'];
		$mailBoday = $data['boday'];
		$to ='avadhesh.kumar@jijgroup.com';
		 $bcc = 'pappu.k@euracogroup.co.uk';
		//$to = $data['email'];
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail('smtp');
		$email->emailFormat('html');
		$email->from('webmaster@firstchoice.je');
		$email->to($to);	
		$email->bcc($bcc);				
		$email->subject($mailSubject);
		if($email->send( nl2br($mailBoday) ))
		{
			$_file = WWW_ROOT .'logs/Stock.'.$mailSubject.date('dmY').".log";
			$content =  $to."\t".$mailSubject."\t".$mailBoday."\t".date('Y-m-d H:i:s'); 
			file_put_contents($_file, $content."\n", FILE_APPEND|LOCK_EX);	
		}	
			
	}
 
		
}
	
	

?>


 
