<?php
//error_reporting(0); 
class FbaListInventoryController extends AppController
{
    
    var $name = "FbaListInventory";
	
    var $components = array('Session','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session');
	
	public function beforeFilter()
    {
            parent::beforeFilter();
            $this->layout = false;
            $this->Auth->Allow(array('ListInventorySupply','ListInventorySupplyByNextToken'));		
			 
    }
 	
   public function index() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Inventory" );	
		$this->loadModel('FbaInventory');	
		 
		$this->paginate = array('order'=>'FbaInventory.total_supply_quantity DESC','limit' => 50 );
		$all_inventory = $this->paginate('FbaInventory');
		$this->set( 'all_inventory', $all_inventory );	
	} 
	
	public function Search() 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA Inventory" );	
		$this->loadModel('FbaInventory');	
		  
		$all_inventory =  $this->FbaInventory->find('all', array(
			
			'conditions' =>  array('OR' =>
						 array(
						 'FbaInventory.fnsku' => trim($_REQUEST['searchkey']),
						 'FbaInventory.seller_sku' => trim($_REQUEST['searchkey']),
						 'FbaInventory.asin' => trim($_REQUEST['searchkey'])
						 ) 
					 ) 
		 ) 
		);
		 
		$this->set( 'all_inventory', $all_inventory );  		
	} 
	
	public function SupplyDetail($inventory_inc_id) 
    {	
		$this->layout = "index"; 	
		$this->set( "title","FBA SupplyDetail" );	
		
		$this->loadModel('FbaInventorySupplyDetail');	
		$fba_inventory = $this->FbaInventorySupplyDetail->find('all', array('conditions'=>array('inventory_inc_id' => $inventory_inc_id)) );
		$this->set( 'fba_inventory', $fba_inventory );		
	}
	
	public function ListInventorySupply() 
	{	
		file_put_contents(WWW_ROOT.'logs/cron_list_inventory_supply.log',"ListInventorySupply\t".date('Y-m-d H:i:s')."\r\n", FILE_APPEND | LOCK_EX);
		
		App::import( 'vendor' , 'FBAInventoryServiceMWS/Client' );
		App::import( 'vendor' , 'FBAInventoryServiceMWS/Exception' );
		App::import( 'vendor' , 'FBAInventoryServiceMWS/Mock' );
		App::import( 'vendor' , 'FBAInventoryServiceMWS/Model' );
		
		App::import( 'vendor' , 'FBAInventoryServiceMWS/Model/ListInventorySupplyRequest' ); 
		App::import( 'vendor' , 'FBAInventoryServiceMWS/Model/ListInventorySupplyResult' );
		App::import( 'vendor' , 'FBAInventoryServiceMWS/Model/ListInventorySupplyResponse' );  
		
		$serviceUrl = "https://mws.amazonservices.com/FulfillmentInventory/2010-10-01";
		
		$config = array (
		'ServiceURL' => $serviceUrl,
		'ProxyHost' => null,
		'ProxyPort' => -1,
		'ProxyUsername' => null,
		'ProxyPassword' => null,
		'MaxErrorRetry' => 3,
		);
		
		$service = new FBAInventoryServiceMWS_Client(
			AWS_ACCESS_KEY_ID,
			AWS_SECRET_ACCESS_KEY,
			APPLICATION_NAME,
			APPLICATION_VERSION,
			$config);
			
		$request = new FBAInventoryServiceMWS_Model_ListInventorySupplyRequest(); 
		$request->setSellerId(MERCHANT_ID);
		$request->setMarketplace(MARKETPLACE_ID);	
		$request->setQueryStartDateTime( gmdate('Y-m-d\TH:i:s\Z', time()-7500000));
		$request->setResponseGroup('Detailed');
		 
  		$xml = $this->invokeListInventorySupply($service, $request);
		 
		if(isset($xml->Error)){  
				file_put_contents(WWW_ROOT.'logs/fba_list_inventory_error_'.date('Ymd').'.log', 
				date('Y-m-d H:i:s')."\tErrorCode:".$xml->Error->Code."\tErrorMessage:".$xml->Error->Message."\r\n", 
				FILE_APPEND | LOCK_EX);		 	
		}else{    
		    
			if(isset($xml->ListInventorySupplyResult->NextToken)){	
				file_put_contents(WWW_ROOT.'logs/list_inventory_supply_next_token.txt',$xml->ListInventorySupplyResult->NextToken);	
			}
					
			if(isset($xml->ListInventorySupplyResult->InventorySupplyList)){
				$this->SaveInventoryItems($xml->ListInventorySupplyResult->InventorySupplyList->member);
			}
		}
 		//pr($xml);		 
		exit;
	}
	
	public function ListInventorySupplyByNextToken() 
	 {	
		if(file_exists(WWW_ROOT.'logs/list_inventory_supply_next_token.txt'))
		{	
			file_put_contents(WWW_ROOT.'logs/cron_list_inventory_supply.log',"ListInventorySupplyByNextToken\t".date('Y-m-d H:i:s')."\r\n", FILE_APPEND | LOCK_EX);
		
			App::import( 'vendor' , 'FBAInventoryServiceMWS/Client' );
			App::import( 'vendor' , 'FBAInventoryServiceMWS/Exception' );
			App::import( 'vendor' , 'FBAInventoryServiceMWS/Mock' );
			App::import( 'vendor' , 'FBAInventoryServiceMWS/Model' );
			
			App::import( 'vendor' , 'FBAInventoryServiceMWS/Model/ListInventorySupplyByNextTokenRequest' ); 
			App::import( 'vendor' , 'FBAInventoryServiceMWS/Model/ListInventorySupplyByNextTokenResult' );
			App::import( 'vendor' , 'FBAInventoryServiceMWS/Model/ListInventorySupplyByNextTokenResponse' );  
			
			$serviceUrl = "https://mws.amazonservices.com/FulfillmentInventory/2010-10-01";
			
			$config = array (
			'ServiceURL' => $serviceUrl,
			'ProxyHost' => null,
			'ProxyPort' => -1,
			'ProxyUsername' => null,
			'ProxyPassword' => null,
			'MaxErrorRetry' => 3,
			);
			
			$service = new FBAInventoryServiceMWS_Client(
				AWS_ACCESS_KEY_ID,
				AWS_SECRET_ACCESS_KEY,
				APPLICATION_NAME,
				APPLICATION_VERSION,
				$config);
			
			$next_token = file_get_contents(WWW_ROOT.'logs/list_inventory_supply_next_token.txt');						 	
			 
 			$request = new FBAInventoryServiceMWS_Model_ListInventorySupplyByNextTokenRequest(); 
			$request->setSellerId(MERCHANT_ID);
			$request->setNextToken($next_token);	
		 
			$xml = $this->invokeListInventorySupplyByNextToken($service, $request);
			 			
			if(isset($xml->Error)){  
				file_put_contents(WWW_ROOT.'logs/fba_list_inventory_error_'.date('Ymd').'.log', 
				date('Y-m-d H:i:s')."\tErrorCode:".$xml->Error->Code."\tErrorMessage:".$xml->Error->Message."\r\n", 
				FILE_APPEND | LOCK_EX);		 	
			}else{   
				@unlink(WWW_ROOT.'logs/list_inventory_supply_next_token.txt');
				if(isset($xml->ListInventorySupplyByNextTokenResult->InventorySupplyList)){
				 	
					if(isset($xml->ListInventorySupplyByNextTokenResult->NextToken)){	
						file_put_contents(WWW_ROOT.'logs/list_inventory_supply_next_token.txt',$xml->ListInventorySupplyByNextTokenResult->NextToken);	
					}
					
					$this->SaveInventoryItems($xml->ListInventorySupplyByNextTokenResult->InventorySupplyList->member);
										
				}
			}
			//pr($xml);
		} 		 
		exit;
	}
	 
	private function SaveInventoryItems( $shipmentItemsObject) 
	{	
		$this->loadModel('FbaInventory');	
		foreach($shipmentItemsObject as $items)
		{			
			$savedata = array();
			$savedata['seller_sku'] 			= $items->SellerSKU;
			$savedata['fnsku'] 					= $items->FNSKU;
			$savedata['asin']					= $items->ASIN;
			$savedata['condition'] 				= $items->Condition;
			$savedata['total_supply_quantity'] 	= $items->TotalSupplyQuantity;
			$savedata['in_stock_supply_quantity']= $items->InStockSupplyQuantity;
			$savedata['earliest_availability'] 	= $items->EarliestAvailability->TimepointType;
			if(isset($items->SupplyDetail->member)){
				$savedata['supply_detail'] 			= json_encode($items->SupplyDetail);
			}
			 
			$fba_inventory = $this->FbaInventory->find('first', array('conditions'=>array('FbaInventory.seller_sku' => $items->SellerSKU,'FbaInventory.fnsku' => $items->FNSKU)) );
			
			$inventory_inc_id = 0;
			
			if(count($fba_inventory) > 0){
				$savedata['id']   = $fba_inventory['FbaInventory']['id'];	
				$inventory_inc_id = $fba_inventory['FbaInventory']['id'];						
			}else{					
				$savedata['added_date'] = date('Y-m-d H:i:s');														
			}				
			$this->FbaInventory->saveAll( $savedata );	
			
			if($inventory_inc_id < 1){
				 $inventory_inc_id = $this->FbaInventory->getLastInsertId();
			} 
			
			if(isset($items->SupplyDetail->member)){
				$this->SaveSupplyDetail($items->SupplyDetail->member, $inventory_inc_id, $items->SellerSKU);
			} 
		}						
	} 
	
	private function SaveSupplyDetail( $supplyDetailObject, $inventory_inc_id, $seller_sku) 
	{	
		if(count($supplyDetailObject) > 0){
		
			$this->loadModel('FbaInventorySupplyDetail');	
			 pr($supplyDetailObject);
			
			$fba_supply = $this->FbaInventorySupplyDetail->find('first', array('conditions'=>array('seller_sku' => $seller_sku)) );
			
			if(count($fba_supply) > 0){
				$this->FbaInventorySupplyDetail->deleteAll(array('FbaInventorySupplyDetail.seller_sku' => $seller_sku), false);				
			} 			
				
			foreach($supplyDetailObject as $items)
			{			
				$savedata = array();
				$savedata['inventory_inc_id'] 			= $inventory_inc_id;
				$savedata['seller_sku'] 				= $seller_sku;
				$savedata['quantity']					= $items->Quantity;
				$savedata['supply_type'] 				= $items->SupplyType;
				$savedata['earliest_available_to_pick_timepoint_type'] = $items->EarliestAvailableToPick->TimepointType;
				$savedata['latest_available_to_pick_timepoint_type']   = $items->LatestAvailableToPick->TimepointType;
				if(isset($items->EarliestAvailableToPick->DateTime)){
					$savedata['earliest_available_to_pick_date_time']   = gmdate("Y-m-d H:i:s", strtotime($items->EarliestAvailableToPick->DateTime));
				}
				if(isset($items->LatestAvailableToPick->DateTime)){
					$savedata['latest_available_to_pick_date_time']		= gmdate("Y-m-d H:i:s", strtotime($items->LatestAvailableToPick->DateTime));
				}
				$savedata['added_date']					= date('Y-m-d H:i:s');														
								
				$this->FbaInventorySupplyDetail->saveAll( $savedata );	 
			}	
		} 					
	} 

	private function invokeListInventorySupply(FBAInventoryServiceMWS_Interface $service, $request)
	{
		  try {
			$response = $service->ListInventorySupply($request);
			$dom = new DOMDocument();
			$dom->loadXML($response->toXML());
			$dom->preserveWhiteSpace = false;
			$dom->formatOutput = true;
			$getResponse = $dom->saveXML();			
			return simplexml_load_string($getResponse);
	
		 } catch (MarketplaceWebServiceOrders_Exception $ex) {
			 return simplexml_load_string($ex->getXML()) ;
		 }
	 }

 
	private function invokeListInventorySupplyByNextToken(FBAInventoryServiceMWS_Interface $service, $request)
	  {
		  try {
			$response = $service->ListInventorySupplyByNextToken($request);
	
			$dom = new DOMDocument();
			$dom->loadXML($response->toXML());
			$dom->preserveWhiteSpace = false;
			$dom->formatOutput = true;
			$getResponse = $dom->saveXML();			
			return simplexml_load_string($getResponse);
	
		 } catch (MarketplaceWebServiceOrders_Exception $ex) {
			 return simplexml_load_string($ex->getXML()) ;
		 }
	 }
	  
	 
}