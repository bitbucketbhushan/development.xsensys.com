<?php
// header('Location: html/index.php');

require('class/BCGFontFile.php');
//require('class/BCGColor.php');
require('class/BCGDrawing.php');
require('class/BCGcode39.barcode.php');
require('class/BCGcode128.barcode.php');

$font = new BCGFontFile('./font/Arial.ttf', 13);
$colorFront = new BCGColor(0, 0, 0);
$colorBack = new BCGColor(255, 255, 255);

// Barcode Part
$regbarcode="RD409000057NL";
$code = new BCGcode39();
$code->setScale(2);
$code->setThickness(30);
$code->setForegroundColor($colorFront);
$code->setBackgroundColor($colorBack);
$code->setFont($font);
$code->setChecksum(true);
$code->parse($regbarcode);

// Drawing Part
$imgreg =$regbarcode.".png";
$drawing = new BCGDrawing($imgreg, $colorBack);
$drawing->setBarcode($code);
$drawing->draw();
$drawing->finish(BCGDrawing::IMG_FORMAT_PNG); 

// Barcode Part
$orderbarcode="12456738-1";
$code128 = new BCGcode128();
$code128->setScale(2);
$code128->setForegroundColor($colorFront);
$code128->setBackgroundColor($colorBack);
$code128->parse($orderbarcode);

// Drawing Part
$imgOrder128=$orderbarcode.".png";
$drawing128 = new BCGDrawing($imgOrder128, $colorBack);
$drawing128->setBarcode($code128);
$drawing128->draw();
$drawing128->finish(BCGDrawing::IMG_FORMAT_PNG); 
?>

<!-- Recorded Service Label -->
		<div id="label" style="margin-top:2px;">
        <div class="container" style="border:2px solid #000000; margin-top:5px;height:342px; padding:2px;">
        <table width="100%">
		<!-- First Row with Registered Barcode / ISO CODE / POSTNL LABEL -->
		 <tr>
		    <td>
			 <table width="100%">
            <tr>
            <td align="left">
			     <div style="text-align: center; border:1px solid #000000;width:auto;padding:3px;">
				 <span style="font-weight:bold;font-size:18px;"> R </span> Registered/Recommande </br>
				 <img src="<?php echo $imgreg?>">
			     </div>
            </td>
			 <td align="right">
			 <span style="font-size:50px;color:#666666; border:2px solid #000000; padding:15px; width:150px; text-align:center">FR</span>
             </td>
			 <td align="right">
             <img style="padding-left:5px" src="http://xsensys.com/img/postnl.jpg" width="165px">
            </td>
           
         </tr>
		 
		 </table>
		 </td>
		  </tr>
		 <!-- END First Row with Registered Barcode / ISO CODE / POSTNL LABEL --> 
		  <!-- Address and Priority Logo --> 
		 <tr>
		 <td>
		<table width="100%">
         <tr>
		  <td  width="80%" valign="top" class="leftside leftheading address" style="font-size:24px; padding-top:15px;">
MR MICHEL PIOT<br>
2 RUE DE KIRRBERG<br>
67320 BAERENDORF<br>
FRANCE</td>
            <td width="20%">
                <img style="padding:2px" src="http://xsensys.com/img/priority.jpg" width="160px">
            </td>
		  </tr>
           </table>
		   </td>
         </tr>
 <!-- End Address and Priority Logo -->
  <!-- Order Barcode and Return Address -->
		 
		  <tr>
            <td>
			  <table width="100%">
			    <tr>
				 <td>
                  <img src="<?php echo $imgOrder128?>" width="200px"> 
                </td>
			     <td>
                If undelivered, please return to:<br><u>P.O. Box 7008, 3109 AA Schiedam, Netherlands</u>
               </td>
			  </tr>
			  </table>
			  </td>
           
         </tr>
		 
        </table>
   </div>
</div>




<!-- USA Service Label Jersey POST -->

<div id="label" style="margin-top:10px;">
 <div class="container">
      <table cellspacing="0" cellpadding="0">
         <tr>
            <td style="border:1px solid #000000">
               <table>
                  <tr>
                     <td width="62%" style="padding:1px;"><img src="http://xsensys.com/img/jersey_logo.jpg" width="100px"></td>
                     <td width="38%" align="right">
                        <table class="jpoststamp " >
                           <tr>
                              <td style="text-align:center; font-size:13px">Postage Paid<br>
                                 Jersey Serial 216
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td style="border:1px solid #000000; text-align:center">
               <table>
                  <tr>
                     <td width="25%" class="rightborder center">
                        <div class="countrycode">
                           <h3 style="font-size:70px;color:#000000;">P</h3>
                        </div>
                     </td>
                     <td width="75%" class="center">
                        <table>
                           <tr>
                              <td style="text-align:right; font-size:8px;">0503 8555 7490 0000 0027 0060 5001 0012 0260</td>
                           </tr>
                           <tr>
                              <td>
                                 <table cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td valign="top" style="font-size:8px;">POSTAGE PAID</td>
                                       <td style="padding-top:2px; text-align:right"><img src="http://euro-inks.com/img/pdf417.jpg" width="170px"></td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <table>
                                    <tr>
                                       <td width="20%" style="font-size:8px;">08/23/04</td>
                                       <td width="20%" style="font-size:8px;">1 lb 0 oz</td>
                                       <td width="35%" style="font-size:8px;">Mailed from 22203</td>
                                       <td width="25%" style="font-size:8px;">071V00501225</td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td style="border:1px solid #000000; font-size:30px;font-weight:bold" class="center">
               PRIORITY MAIL
            </td>
         </tr>
         <tr>
            <td style="border:1px solid #000000" >
               <table width="90%" align="center">
                  <tr>
                     <td style="font-size:10px">COSTBREAKER US<br>
                        JERSEY, JE24HR, CI
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <table style="margin-top:70px;">
                           <tr>
                              <td valign="top">SHIP TO:</td>
                              <td>CARIN DOPIERALSKI<br>
                                 RM 425<br>
                                 475 LENFANT PLZ SW<br>
                                 <span style="font-size:15px">WASHINGTON DC 20260-0004</span><br>
                                 <img src="http://euro-inks.com/img/addressBar.jpg" width="220px">
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td style="border:1px solid #000000; border-top:4px solid #0000000; border-bottom:4px solid #0000000;">
               <table style="border:1px solid #000000; border-top:4px solid #0000000; border-bottom:4px solid #0000000;">
                  <tr>
                     <td width="45%" style="text-align:center">
                        <div style="font-weight:bold; margin-bottom:10px; font-size:14px;">ZIP - e/ INSURED</div>
                        <img src="http://euro-inks.com/img/100020-1.png" width="140px">
                        <div style="margin-top:10px">420202691050385</div>
                     </td>
                     <td width="55%">
                        <table style="border:1px solid #000000; font-size:10px" cellpadding="0" cellspacing="0">
                           <tr>
                              <td class="bottomborder">
                                 <table cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td style="padding:1px 2px;font-weight:bold; width:50%; font-size:13px">CUSTOMS DECLARATION </td>
                                       <td style="padding:1px 2px;font-weight:bold; width:50%; text-align:center; font-size:13px">CN22</td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td class="bottomborder">
                                 <table cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td style="padding:1px 2px; font-size:10px">Designated operator</td>
                                       <td style="padding:1px 2px; font-weight:bold; font-size:10px">Jersey Post</td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td  class="bottomborder">
                                 <table cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td width="5%" class="bottomborder rightborder">&nbsp;</td>
                                       <td width="40%" style="font-size:10px">&nbsp;Gift</td>
                                       <td width="5%" class="bottomborder rightborder leftborder">&nbsp;</td>
                                       <td width="50%" style="font-size:10px">&nbsp;Comm. sample</td>
                                    </tr>
                                    <tr>
                                       <td class="rightborder">&nbsp;</td>
                                       <td style="font-size:10px">&nbsp;Documents</td>
                                       <td class="rightborder leftborder center">x</td>
                                       <td style="font-size:10px">&nbsp;Other</td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td  class="bottomborder">
                                 <table cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td style="padding:1px 2px;font-size:10px">Description of Contents</td>
                                    </tr>
                                    <tr>
                                       <td style="padding:1px 2px; font-weight:bold; font-size:10px">Office Consumables</td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td  class="bottomborder">
                                 <table cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td style="padding:1px 2px;font-size:10px; width:20%">Total Weight</td>
                                       <td style="padding:1px 2px;font-weight:bold; font-size:10px; width:30%">0.025 Kg</td>
                                       <td style="padding:1px 2px;font-size:10px; width:20%">Total Value</td>
                                       <td style="padding:1px 2px;font-weight:bold; font-size:10px; width:30%">&#36;2.2</td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td  class="bottomborder">
                                 <table cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td style="padding:1px 2px;font-size:10px">HS Tariff, origin</td>
                                    </tr>
                                    <tr>
                                       <td style="padding:1px 2px; font-weight:bold; font-size:10px">3304910000, Jersey</td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td  class="">
                                 <table cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td style="padding:1px 2px;font-size:10px; width:25%">Signature</td>
                                       <td style="padding:1px 2px;font-size:11px;width:75%"><img src="http://euro-inks.com/img/squiggle.png" width="62px" ></td>
                                    </tr>
                                    <tr>
                                       <td colspan="2" style="padding:1px 2px; font-weight:bold; font-size:10px">22/09/2016, Costbreaker US</td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td style="border:1px solid #000000; text-align:center">
               <div style="padding:10px; font-size:14px;">Electronic Rate Approved #216</div>
            </td>
         </tr>
      </table>
   </div>
</div>

<!-- Recorded Service Label Approved -->
<!-- Recorded Service Label-->
		<div id="label" style="margin-top:3px;">
   <div class="container" style="border:2px solid #000000; margin-top:5px;height:342px; padding:3px;">
   
   
 
      <table style="padding:2px 0;">
         <tr>
            <td>
               <table style="width:100%" >
                  <tr>
                     <td width="30%" style="font-size:11px">
					 If undelivered, please return to:<br><u>P.O. Box 7008, 3109 AA Schiedam, Netherlands</u>
					 <div class="" style="margin-top:10px; text-align: center; border:1px solid #000000; padding:0px; width:250px;">
					 
                <div class="center" style="font-size:14px;">
				<table>
				<tr>
				<td width="5%" style="font-weight:bold;font-size:16px; ">R</td>
				<td width="95%" align="center">Registered/Recommande</td>
				</tr></table>
            <div><img src="http://euro-inks.com/img/_RD411180030NL_.png" width="200px"></div>
         </div>
		 </div>
                        
                     </td>
					 <td width="2%" valign="top"></td>
					 <td width="30%" valign="top" align="right"><img style="padding-top:10px" src="http://xsensys.com/img/priority.jpg" width="160px"></td>
					 <td width="38%" valign="top" align="right"><img style="padding-top:10px" src="http://xsensys.com/img/postnl.jpg" width="165px">
					  </td>
                  </tr>
               </table>
            </td>
           
         </tr>
      </table>
     
    <table>
         <tr>
            <td width="40%">
			 
            </td>
			<td width="60%"><table>
                           <tr>
                              <td valign="top" class="leftside leftheading address" style="font-size:16px; padding-top:15px; height:110px">
                                 MR MICHEL PIOT<br>
2 RUE DE KIRRBERG<br>
67320 BAERENDORF<br>
FRANCE</td>
                           </tr>
                        </table></td>
			
         </tr>
      </table>
   
   
     <img src="http://euro-inks.com/img/100020-1.png" width="200px"> 
   </div>
</div>

