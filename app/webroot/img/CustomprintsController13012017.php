<?php

class CustomprintsController extends AppController
{
    /* controller used for linnworks api */
    
    var $name = "Customprints";
    
    var $components = array('Session','Upload','Common','Auth','Paginator');
    
    var $helpers = array('Html','Form','Common','Session','Soap','Number','Paginator');
    var $virtualFields;
    
    public function index()
    {
		$this->layout = 'index';
		$this->autoRender = false;
		
		$this->render( '/Products/custom_label' );
	}
    
   
		
    public function getOpenOrderById( $numOrderId = null )
	{
		
		$this->loadModel('OpenOrder');
		$order = $this->OpenOrder->find('first', array('conditions' => array('OpenOrder.num_order_id' => $numOrderId )));
		$data['id']					=	 $order['OpenOrder']['id'];
		$data['order_id']			=	 $order['OpenOrder']['order_id'];
		$data['num_order_id']		=	 $order['OpenOrder']['num_order_id'];
		$data['sub_source']		=	 $order['OpenOrder']['sub_source'];
		$data['general_info']		=	 unserialize($order['OpenOrder']['general_info']);
		$data['shipping_info']		=	 unserialize($order['OpenOrder']['shipping_info']);
		$data['customer_info']		=	 unserialize($order['OpenOrder']['customer_info']);
		$data['totals_info']		=	 unserialize($order['OpenOrder']['totals_info']);
		$data['folder_name']		=	 unserialize($order['OpenOrder']['folder_name']);
		$data['items']				=	 unserialize($order['OpenOrder']['items']);
		$data['assigned_service']	=	 $order['AssignService']['assigned_service'];
		$data['assign_barcode']		=	 $order['AssignService']['assign_barcode'];
		$data['manifest']			=	 $order['AssignService']['manifest'];
		$data['cn22']				=	 $order['AssignService']['cn22'];
		$data['courier']			=	 $order['AssignService']['courier'];
		$data['template_id']		=	 $order['OpenOrder']['template_id'];
		return $data;
		
	}

	
	
	
	public function customLabelPrint( $splitOrderId = null )
			{
				
				$this->layout = '';
				$this->autoRender = false;
				$this->loadModel('Template');
				$this->loadModel('MergeUpdate');
				$this->loadModel('Product');
				
				$splitOrderId = $this->request->data['orderid'];
				$orderArray = explode( '-', $splitOrderId );
				$id				=	$orderArray[0];
				$openOrderId	=	$orderArray[0];
				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
							
				$order				=		$this->getOpenOrderById( $openOrderId );
				$postalservices		=		$order['shipping_info']->PostalServiceName;
				$destination		=		$order['customer_info']->Address->Country;
				$total				=		$order['totals_info']->TotalCharge;
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$currency			=	 	$order['totals_info']->Currency;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$templateId			=	 	$order['template_id'];
				$subSource			=	 	$order['sub_source'];
				
				$str = ''; 
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				$totlaWeight	=	0;
				$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
				$weight = 0;
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					
					foreach($newSkus as $newSku)
						{
							
							$totalvalue = $splitOrderDetail['MergeUpdate']['price'];
							$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
							$getsku = 'S-'.$newSku[1];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title	=	$getOrderDetail['Product']['product_name'];
							$weight	=	$weight + $getOrderDetail['ProductDesc']['weight'];
							$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.substr($title, 0, 25 ).'</td>
									<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
							$str .= '</tr>';
						}
							unset($newSkus);
				}
				
				$address			=		'';
				$address			.=		($company != '') ? '<b>'.$company.'</b><br>' : '' ; 
				$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
				$address			.=		($address1 != '' ) ? $address1.'<br>' : '' ; 
				$address			.=		($address2 != '' ) ? $address2.',' : '' ; 
				$address			.=		($address3 != '' ) ? $address3.'<br>' : '' ;
				$address			.=		($town != '' ) ? $town.'<br>' : '';
				$address			.=		($resion != '' ) ? $resion.'<br>' : '';
				$address			.=		($postcode != '' ) ? $postcode.'<br>' : '';
				$address			.=		($country != '' ) ? $country.'<br>' : '';
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				$currentdate		=	 	date("j, F  Y");
				$BarcodeImage		=		$splitOrderDetail['MergeUpdate']['order_barcode_image'];
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= str_replace( 'Spain' , 'ESPAÑA', $address); 
				$setRepArray[] 					= $country;
				$barcode  						= $order['assign_barcode'];
				$barcodenum						= explode('.', $barcode);
				$barcodePath  					= Router::url('/', true).'img/orders/barcode/';
				
				$barcodenum						= explode('.', $barcode);
			
				/**************** for tempplate *******************/
				$countryArray = Configure::read('customCountry');
				/*$labelDetail =	$this->Template->find('first', array(
											'conditions' => array( 
													'Template.location_name' => $country,
													'Template.store_name' => $subSource,
													'Template.status' => '1'
													)
												)
											);
				
				if( empty( $labelDetail ) )
				{
					if( in_array( $country , $countryArray ) && $country != 'United Kingdom' )
						{
							$labelDetail =	$this->Template->find('first', array(
													'conditions' => array( 
															'Template.location_name' => 'Rest Of EU',
															'Template.store_name' => $subSource,
															'Template.status' => '1'
															)
														)
													);
						}
					else
						{
							$labelDetail =	$this->Template->find('first', array(
													'conditions' => array( 
															'Template.location_name' => 'Other',
															'Template.store_name' => $subSource,
															'Template.status' => '1'
															)
														)
													);
						}
				}*/
				
				
				if( $splitOrderDetail['MergeUpdate']['custom_service_marked'] == 1 || (  $splitOrderDetail['MergeUpdate']['postal_service'] == 'Express' || $splitOrderDetail['MergeUpdate']['postal_service'] == 'Tracked' || $splitOrderDetail['MergeUpdate']['postal_service'] == 'Standard_Jpost') )
				{
					$labelDetail =	$this->Template->find('first', array(
												'conditions' => array(
														'Template.location_name' => 'Other',
														'Template.store_name' => $subSource, 
														'Template.label_name' => $splitOrderDetail['MergeUpdate']['service_provider'],
														'Template.status' => '1'
														)
													)
												);
					if( empty($labelDetail) )
					{
						$labelDetail =	$this->Template->find('first', array(
												'conditions' => array( 
														'Template.location_name' => $country,
														'Template.store_name' => $subSource,
														'Template.status' => '1'
														)
													)
												);
					}
				}
				else
				{
					$labelDetail =	$this->Template->find('first', array(
												'conditions' => array( 
														'Template.location_name' => $country,
														'Template.store_name' => $subSource,
														'Template.status' => '1'
														)
													)
												);
					
					if( empty( $labelDetail ) )
					{
						if( in_array( $country , $countryArray ) && $country != 'United Kingdom' )
							{
								$labelDetail =	$this->Template->find('first', array(
														'conditions' => array( 
																'Template.location_name' => 'Rest Of EU',
																'Template.store_name' => $subSource,
																'Template.status' => '1'
																)
															)
														);
							}
						else
							{
								$labelDetail =	$this->Template->find('first', array(
														'conditions' => array( 
																'Template.location_name' => 'Other',
																'Template.store_name' => $subSource,
																'Template.status' => '1'
																)
															)
														);
							}
					}
				}
				
				$html           =  $labelDetail['Template']['html'];
				$paperHeight    =  $labelDetail['Template']['paper_height'];
				$paperWidth  	=  $labelDetail['Template']['paper_width'];
				$barcodeHeight  =  $labelDetail['Template']['barcode_height'];
				$barcodeWidth   =  $labelDetail['Template']['barcode_width'];
				$paperMode      =  $labelDetail['Template']['paper_mode'];
				/********************************/
				
				$dompdf->set_paper(array( 0, 0, $paperWidth, $paperHeight ), $paperMode );
				$barcodeimg = '<img src='.$barcodePath.$BarcodeImage.' width = '.$barcodeWidth.' >';
			
				$setRepArray[] 	=    $barcodeimg;
				$setRepArray[] 	=    $barcodenum[0];
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totlaWeight;
				$setRepArray[]	=	 $tax;
				$setRepArray[]	=	 $currentdate;
				$logopath		=	 Router::url('/', true).'img/';
				$setRepArray[]	=	 '<img src="'.$logopath.'logo.jpg" >';
				$logo			=	 '<img src="'.$logopath.'logo.jpg" >';
				$signature		=	 '<img src="'.$logopath.'sig.jpg" >';
				
				$setRepArray[]	=	 $logo;
				$setRepArray[]	=	 $signature;
				$setRepArray[]	=	 $splitOrderId;
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				
				$tempId			=	$splitOrderDetail['MergeUpdate']['lable_id'];
				$countryCode	=	$splitOrderDetail['MergeUpdate']['country_code'];
				
				$setRepArray[]	=	 $countryCode;
				$setRepArray[]	=	 $weight;
				$conversionRate	=	Configure::read( 'conversionRate' );
				if($currency == 'GBP')
				{
					$totalvalue = $totalvalue;
				}
				else
				{
					$totalvalue = number_format($totalvalue/$conversionRate, 2, '.' ,'');
				}
				
				$setRepArray[]	=	 $totalvalue;
				if($splitOrderDetail['MergeUpdate']['track_id'] != '')
				{
					$trackingnumber = $splitOrderDetail['MergeUpdate']['track_id'];
				}
				else
				{
					$trackingnumber = 'A'.mt_rand(100000, 999999);
				}
				
				$setRepArray[]	=	 $trackingnumber;
				$setRepArray[]	=	 $subSource;
			
				$cssPath = WWW_ROOT .'css/';
				$imgPath = Router::url('/', true) .'img/';
			
			    $html = '<meta charset="utf-8">
						 <meta http-equiv="X-UA-Compatible" content="IE=edge">
						 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
						 <meta content="" name="description"/>
						 <meta content="" name="author"/><body>'.$html.'</body>';
				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				
				$html 	= $this->setReplaceValueLabel( $setRepArray, $html );
				//echo $html;
				//exit;
				//$html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				$dompdf->stream("OrderIdslip-$splitOrderId.pdf");
				
				$imgPath = WWW_ROOT .'img/printPDF/'; 
				$path = Router::url('/', true).'img/printPDF/';
				$date = new DateTime();
				$timestamp = $date->getTimestamp();
				$name	=	'Packagin_Label_'.$splitOrderId.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				$serverPath  	= 	$path.$name;
				$printerId		=	$this->getZebra();
				
				$sendData = array(
					'printerId' => $printerId,
					'title' => 'Now Print',
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'
				);
				
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				$d = $Coreprinter->toPrint( $sendData );
				pr($d); exit;
		}
		
		
		function setReplaceValueLabel( $setRepArray, $htmlString )
		{
	
			$constArray = array();
			$constArray[] = '_ADDRESS1_';
			$constArray[] = '_ADDRESS2_';
			$constArray[] = '_ADDRESS3_';
			$constArray[] = '_TOWN_';
			$constArray[] = '_RESION_';
			$constArray[] = '_ADDRESS_';
			$constArray[] = '_COUNTRY_';
			$constArray[] = '_BARCODEIMAGE_';
			$constArray[] = '_BARCODENUMBER_';
			$constArray[] = '_ORDERDETAIL_';
			$constArray[] = '_TAX_';
			$constArray[] = '_DATE_';
			$constArray[] = '_CURRENTDATE_';
			$constArray[] = '_LOGO_';
			$constArray[] = '';
			$constArray[] = '_SIGNATURE_';
			$constArray[] = '_SPLITORDERID_';
			$constArray[] = '_COUNTRYCODE_';
			$constArray[] = '_TOTALWEIGHT_';
			$constArray[] = '_TOTALVALUE_';
			$constArray[] = '_TRACKINGNUMBER_';
			$constArray[] = '_SUBSOURCE_';
			$constArray[] = '_CARTAS_';
			
			$getRep = $htmlString;
			$getRep = str_replace( $constArray[0],$setRepArray[0],$getRep );
			$getRep = str_replace( $constArray[1],$setRepArray[1],$getRep );
			$getRep = str_replace( $constArray[2],$setRepArray[2],$getRep );
			$getRep = str_replace( $constArray[3],$setRepArray[3],$getRep );
			$getRep = str_replace( $constArray[4],$setRepArray[4],$getRep );
			$getRep = str_replace( $constArray[5],$setRepArray[5],$getRep );
			$getRep = str_replace( $constArray[6],$setRepArray[6],$getRep );
			$getRep = str_replace( $constArray[7],$setRepArray[7],$getRep );
			$getRep = str_replace( $constArray[8],$setRepArray[8],$getRep );
			$getRep = str_replace( $constArray[9],$setRepArray[9],$getRep );
			$getRep = str_replace( $constArray[10],$setRepArray[10],$getRep );
			$getRep = str_replace( $constArray[11],$setRepArray[11],$getRep );
			$getRep = str_replace( $constArray[12],$setRepArray[12],$getRep );
			$getRep = str_replace( $constArray[13],$setRepArray[13],$getRep );
			$getRep = str_replace( $constArray[14],$setRepArray[14],$getRep );
			$getRep = str_replace( $constArray[15],$setRepArray[15],$getRep );
			$getRep = str_replace( $constArray[16],$setRepArray[16],$getRep );
			$getRep = str_replace( $constArray[17],$setRepArray[17],$getRep );
			$getRep = str_replace( $constArray[18],$setRepArray[18],$getRep );
			$getRep = str_replace( $constArray[19],$setRepArray[19],$getRep );
			$getRep = str_replace( $constArray[20],$setRepArray[20],$getRep );
			$getRep = str_replace( $constArray[21],$setRepArray[21],$getRep );
			$getRep = str_replace( $constArray[22],$setRepArray[22],$getRep );
			return $getRep;
			
		}
		
		public function getZebra()
		{
			$this->layout = '';
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );						
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name']; //getenv('COMPUTERNAME');
			$paramsZDesigner = array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name' => 'ZDesigner'
				)
			);
			//ZDesigner
			$pcDetailZDesigner = json_decode(json_encode($this->PcStore->find('all', $paramsZDesigner)),0);						
			$zDesignerPrinter = $pcDetailZDesigner[0]->PcStore->printer_id;
		
		return $zDesignerPrinter;	
		}
		
		
		public function customSlipPrint( $splitOrderId = null )
		{
			
				$this->loadModel( 'PackagingSlip' );
				$this->loadModel( 'MergeUpdate' );
				$this->loadModel( 'Product' );
				
				$splitOrderId = $this->request->data['orderid'];
				$orderArray = explode( '-', $splitOrderId );
				$id				=	$orderArray[0];
				
				$openOrderId	=	$orderArray[0];
				$splitOrderId	=	$splitOrderId;
		
				$this->layout = '';
				$this->autoRender = false;
				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				
				$dompdf->set_paper(array(0, 0, 245, 450), 'portrait');
				$order	=	$this->getOpenOrderById( $openOrderId );
				
				
				$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
				$itemPrice		=	$getSplitOrder['MergeUpdate']['price'];
				$itemQuantity	=	$getSplitOrder['MergeUpdate']['quantity'];
				$BarcodeImage	=	$getSplitOrder['MergeUpdate']['order_barcode_image'];
				
				
				$k = '10';
				$companyname = '';
				$returnaddress = '';
				switch ($k) {
								case 0:
									echo "";
									break;
								case 1:
									echo "";
									break;
								case 2:
									echo "";
									break;
								default:
								   $companyname 	 =  'Jij Group';
								   $returnaddress 	 =  'Address Line 1 <br> Address Line 2 <br>Some Town Some Region 123 ABC <br>United Country';
							}
				
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$subsource			=		$order['sub_source'];
				
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$address			=		'';
				$address			.=		($company != '') ? $company.'<br>' : '' ; 
				$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
				$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
				$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
				$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
				$address			.=		($town != '') ? $town.'<br>' : '';
				$address			.=		($resion != '') ? $resion.'<br>' : '';
				$address			.=		($postcode != '') ? $postcode.'<br>' : '';
				$address			.=		($country != '' ) ? $country.'<br>' : '';
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= $postcode;
				$setRepArray[] 					= $country;
				$setRepArray[] 					= $phone;
				$setRepArray[] 					= $ordernumber;
				$setRepArray[] 					= $courier;
				$setRepArray[] 					= $recivedate[0];
				$i = 1;
				$str = '';
				$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
				$totalGoods = 0;
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					foreach($newSkus as $newSku)
						{
							
							$getsku = 'S-'.$newSku[1];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title	=	$getOrderDetail['Product']['product_name'];
							$totalGoods = $totalGoods + $newSku[0];
							$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
									<td valign="top" class="rightborder bottomborder">'.substr($title, 0, 15 ).'</td>
									<td valign="top" class="center norightborder bottomborder">'.$newSku[0][0].'</td>';
							$str .= '</tr>';
							$i++;
						}
						unset($newSkus);
				}
				
				/**********************************************/
				$countryArray = Configure::read('customCountry');
				$gethtml =	$this->PackagingSlip->find('first', array(
											'conditions' => array( 
													'PackagingSlip.store_name' => $subsource,
													)
												)
											);
				
				
				$html 			=	$gethtml['PackagingSlip']['html'];
				$paperHeight    =   $gethtml['PackagingSlip']['paper_height'];
				$paperWidth  	=   $gethtml['PackagingSlip']['paper_width'];
				$barcodeHeight  =   $gethtml['PackagingSlip']['barcode_height'];
				$barcodeWidth   =   $gethtml['PackagingSlip']['barcode_width'];
				$paperMode      =   $gethtml['PackagingSlip']['paper_mode'];
				
				
				/**********************************************/
				
					$dompdf->set_paper(array(0, 0, $paperWidth, $paperHeight ), $paperMode);
				
				$totalitem = $i - 1;
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totalGoods;
				$setRepArray[]	=	 $subtotal;
				$Path 			= 	'/wms/img/client/';
				$img			=	 '<img src=http://localhost/wms/img/client/demo.png height="50" width="50">';
				$setRepArray[]	=	 $img;
				$setRepArray[]	=	 $postagecost;
				$setRepArray[]	=	 $tax;
				$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
				$setRepArray[]	=	 $itemPrice;
				$setRepArray[]	=	 $address;
				$barcodePath  	=  Router::url('/', true).'img/orders/barcode/';
				$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.' width='. $barcodeWidth .'>';
				$barcodenum		=	explode('.', $barcode);
				
				$setRepArray[] 	=  $barcodeimg;
				$setRepArray[] 	=  $paymentmethod;
				$setRepArray[] 	=  $barcodenum[0];
				$setRepArray[] 	=  '';
				
				
				
				$img 			=	$gethtml['PackagingSlip']['image_name'];
				$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
				$setRepArray[] 	=  $returnaddress;
				$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
				$setRepArray[] 	= $splitOrderId;
				
				$imgPath = WWW_ROOT .'css/';
				
				$html2 = '<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				<meta content="" name="description"/>
				<meta content="" name="author"/><body>'.$html.'</body>';
				
				$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
				$html 		= 	$this->setReplaceValue( $setRepArray, $html2 );
				//$html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
				$dompdf->stream("OrderId-$splitOrderId.pdf");
				
				$imgPath = WWW_ROOT .'img/printPDF/'; 
				$path = Router::url('/', true).'img/printPDF/';
				$name	=	'Packagin_Slip_'.$splitOrderId.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				$serverPath  	= 	$path.$name ;
				$printerId	=	$this->getEpson();
				
				
				$sendData = array(
					'printerId' => $printerId,
					'title' => 'Now Print',
					'contentType' => 'pdf_uri',
					'content' => $serverPath,
					'source' => 'Direct'					
				);
				
				App::import( 'Controller' , 'Coreprinters' );
				$Coreprinter = new CoreprintersController();
				$d = $Coreprinter->toPrint( $sendData );
				pr($d); exit;
			
		}
		
		function setReplaceValue( $setRepArray, $htmlString )
		{
	
			$constArray = array();
			$constArray[] = '_ADDRESS1_';
			$constArray[] = '_ADDRESS2_';
			$constArray[] = '_ADDRESS3_';
			$constArray[] = '_TOWN_';
			$constArray[] = '_RESION_';
			$constArray[] = '_POSTCODE_';
			$constArray[] = '_COUNTRY_';
			$constArray[] = '_PHONE_';
			$constArray[] = '_ORDERNUMBER_';
			$constArray[] = '_COURIER_';
			$constArray[] = '_RECIVEDATE_';
			$constArray[] = '_ORDERSUMMARY_';
			$constArray[] = '_TOTALITEM_';
			$constArray[] = '_SUBTOTAL_';
			$constArray[] = '_IMAGE_';
			$constArray[] = '_POSTAGECOST_';
			$constArray[] = '_TAX_';
			$constArray[] = '_TOTALAMOUNT_';
			$constArray[] = '_ADDRESS_';
			$constArray[] = '_BARCODE_';
			$constArray[] = '_PAYMENTMETHOD_';
			$constArray[] = '_BARCODENUMBER_';
			$constArray[] = '_COMPANYNAME_';
			$constArray[] = '_RETURNADDRESS_';
			$constArray[] = '_LOGO_';
			$constArray[] = '_SPLITORDERID_';
			
			
			$getRep = $htmlString;
			$getRep = str_replace( $constArray[0],$setRepArray[0],$getRep );
			$getRep = str_replace( $constArray[1],$setRepArray[1],$getRep );
			$getRep = str_replace( $constArray[2],$setRepArray[2],$getRep );
			$getRep = str_replace( $constArray[3],$setRepArray[3],$getRep );
			$getRep = str_replace( $constArray[4],$setRepArray[4],$getRep );
			$getRep = str_replace( $constArray[5],$setRepArray[5],$getRep );
			$getRep = str_replace( $constArray[6],$setRepArray[6],$getRep );
			$getRep = str_replace( $constArray[7],$setRepArray[7],$getRep );
			$getRep = str_replace( $constArray[8],$setRepArray[8],$getRep );
			$getRep = str_replace( $constArray[9],$setRepArray[9],$getRep );
			$getRep = str_replace( $constArray[10],$setRepArray[10],$getRep );
			$getRep = str_replace( $constArray[11],$setRepArray[11],$getRep );
			$getRep = str_replace( $constArray[12],$setRepArray[12],$getRep );
			$getRep = str_replace( $constArray[13],$setRepArray[13],$getRep );
			$getRep = str_replace( $constArray[14],$setRepArray[14],$getRep );
			$getRep = str_replace( $constArray[15],$setRepArray[15],$getRep );
			$getRep = str_replace( $constArray[16],$setRepArray[16],$getRep );
			$getRep = str_replace( $constArray[17],$setRepArray[17],$getRep );
			$getRep = str_replace( $constArray[18],$setRepArray[18],$getRep );
			$getRep = str_replace( $constArray[19],$setRepArray[19],$getRep );
			$getRep = str_replace( $constArray[20],$setRepArray[20],$getRep );
			$getRep = str_replace( $constArray[21],$setRepArray[21],$getRep );
			$getRep = str_replace( $constArray[22],$setRepArray[22],$getRep );
			$getRep = str_replace( $constArray[23],$setRepArray[23],$getRep );
			$getRep = str_replace( $constArray[24],$setRepArray[24],$getRep );
			$getRep = str_replace( $constArray[25],$setRepArray[25],$getRep );
			return $getRep;
			
		}
		
		public function getEpson()
		{
			$this->layout = '';
			$this->autoRander = false;
			
			$this->loadModel( 'PcStore' );
			$userId = $this->Session->read('Auth.User.id');
			
			$this->loadModel( 'User' );
			$getUser = $this->User->find('first', array( 'conditions' => array( 'User.id' => $userId ) ));
			
			//Get Pc name according to User for short term
							
			$getPcText = $getUser['User']['pc_name']; //getenv('COMPUTERNAME');
			$paramsEpson = array(
				'conditions' => array(
					'PcStore.pc_name' => $getPcText,
					'PcStore.printer_name' => 'EPSONTM-T88V'
				)
			);
			
			//EPSONTM-T88V			
			$pcDetailEpson = json_decode(json_encode($this->PcStore->find('all', $paramsEpson)),0);			
			$epsonT88 = $pcDetailEpson[0]->PcStore->printer_id;
			
		return $epsonT88;	
		}
		
		public function varifyLabelPrint()
			{
				
				$this->layout = '';
				$this->autoRender = false;
				$this->loadModel('Template');
				$this->loadModel('MergeUpdate');
				$this->loadModel('Product');
				
				$splitOrderId = $this->request->data['orderid'];
				$orderArray = explode( '-', $splitOrderId );
				$id				=	$orderArray[0];
				$openOrderId	=	$orderArray[0];
				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
							
				$order				=		$this->getOpenOrderById( $openOrderId );
				$postalservices		=		$order['shipping_info']->PostalServiceName;
				$destination		=		$order['customer_info']->Address->Country;
				$total				=		$order['totals_info']->TotalCharge;
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$currency			=	 	$order['totals_info']->Currency;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$templateId			=	 	$order['template_id'];
				$subSource			=	 	$order['sub_source'];
				
				$str = ''; 
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				$totlaWeight	=	0;
				$skus = explode( ',', $splitOrderDetail['MergeUpdate']['sku']);
				$weight = 0;
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					
					foreach($newSkus as $newSku)
						{
							
							$totalvalue = $splitOrderDetail['MergeUpdate']['price'];
							$totalGoods = $splitOrderDetail['MergeUpdate']['quantity'];
							$getsku = 'S-'.$newSku[1];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title	=	$getOrderDetail['Product']['product_name'];
							$weight	=	$weight + $getOrderDetail['ProductDesc']['weight'];
							$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$newSku[0].'X'.substr($title, 0, 25 ).'</td>
									<td valign="top" class="center norightborder bottomborder">'.$getOrderDetail['ProductDesc']['weight'].'</td>';
							$str .= '</tr>';
						}
							unset($newSkus);
				}
				$CARTAS              = 		'';
				$address			 =		'';
				$address			.=		($company != '') ? '<b>'.$company.'</b><br>' : '' ; 
				$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
				$address			.=		($address1 != '' ) ? $address1.'<br>' : '' ; 
				$address			.=		($address2 != '' ) ? $address2.',' : '' ; 
				$address			.=		($address3 != '' ) ? $address3.'<br>' : '' ;
				$address			.=		($town != '' ) ? $town.'<br>' : '';
				$address			.=		($resion != '' ) ? $resion.'<br>' : '';
				$address			.=		($postcode != '' ) ? $postcode.'<br>' : '';
				$address			.=		($country != '' ) ? $country.'<br>' : '';
				if(trim($country) == 'Spain'){
					$address		     =		'';
					$address			.=		($company != '')   ? '<b>'.ucwords($company).'</b><br>' : '' ; 
					$address			.=		($fullname != '')  ? ucwords($fullname).'<br>' : '' ; 
					$address			.=		($address1 != '' ) ? ucwords($address1).'<br>' : '' ; 
					$address			.=		($address2 != '' ) ? ucwords($address2) : '' ; 
					$address			.=		($address3 != '' ) ? ','.ucwords($address3).'<br>' : '<br>' ;
					$address			.=		($postcode != '' ) ? strtoupper($postcode) : '';					
					$address			.=		($town != '' )     ? ' '.strtoupper($town) : '';
					$address			.=		($resion != '' )   ? '<br> '. strtoupper($resion) : '';					
					$address			.=		($postcode != '' ) ? '<br>' : '';
					$address			.=		($country != '' )  ? $country.'<br>' : '';
					
					$CARTAS = 'CARTAS '.substr($postcode, 0, 1);
					if(substr($postcode, 0, 1) == 0){
						$CARTAS = 'CARTAS '.substr($postcode, 1, 1);
					}					
				}
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				$currentdate		=	 	date("j, F  Y");
				$BarcodeImage		=		$splitOrderDetail['MergeUpdate']['order_barcode_image'];
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= str_replace( 'Spain' , 'ESPAÑA', $address); 
				$setRepArray[] 					= $country;
				$barcode  						= $order['assign_barcode'];
				$barcodenum						= explode('.', $barcode);
				$barcodePath  					= Router::url('/', true).'img/orders/barcode/';
				$barcodenum						= explode('.', $barcode);
			
				/**************** for tempplate *******************/
				$countryArray = Configure::read('customCountry');
				if( $splitOrderDetail['MergeUpdate']['custom_service_marked'] == 1 || (  $splitOrderDetail['MergeUpdate']['postal_service'] == 'Express' || $splitOrderDetail['MergeUpdate']['postal_service'] == 'Tracked' || $splitOrderDetail['MergeUpdate']['postal_service'] == 'Standard_Jpost') )
				{
					$labelDetail =	$this->Template->find('first', array(
												'conditions' => array(
														'Template.location_name' => 'Other',
														'Template.store_name' => $subSource, 
														'Template.label_name' => $splitOrderDetail['MergeUpdate']['service_provider'],
														'Template.status' => '1'
														)
													)
												);
					if( empty($labelDetail) )
					{
						$labelDetail =	$this->Template->find('first', array(
												'conditions' => array( 
														'Template.location_name' => $country,
														'Template.store_name' => $subSource,
														'Template.status' => '1'
														)
													)
												);
					}
				}
				else
				{
					$labelDetail =	$this->Template->find('first', array(
												'conditions' => array( 
														'Template.location_name' => $country,
														'Template.store_name' => $subSource,
														'Template.status' => '1'
														)
													)
												);
					
					if( empty( $labelDetail ) )
					{
						if( in_array( $country , $countryArray ) && $country != 'United Kingdom' )
							{
								$labelDetail =	$this->Template->find('first', array(
														'conditions' => array( 
																'Template.location_name' => 'Rest Of EU',
																'Template.store_name' => $subSource,
																'Template.status' => '1'
																)
															)
														);
							}
						else
							{
								$labelDetail =	$this->Template->find('first', array(
														'conditions' => array( 
																'Template.location_name' => 'Other',
																'Template.store_name' => $subSource,
																'Template.status' => '1'
																)
															)
														);
							}
					}
				}
				
				$html           =  $labelDetail['Template']['html'];
				$paperHeight    =  $labelDetail['Template']['paper_height'];
				$paperWidth  	=  $labelDetail['Template']['paper_width'];
				$barcodeHeight  =  $labelDetail['Template']['barcode_height'];
				$barcodeWidth   =  $labelDetail['Template']['barcode_width'];
				$paperMode      =  $labelDetail['Template']['paper_mode'];
				/********************************/
				
				$dompdf->set_paper(array( 0, 0, $paperWidth, $paperHeight ), $paperMode );
				$barcodeimg = '<img src='.$barcodePath.$BarcodeImage.' width = '.$barcodeWidth.' >';
			
				$setRepArray[] 	=    $barcodeimg;
				$setRepArray[] 	=    $barcodenum[0];
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totlaWeight;
				$setRepArray[]	=	 $tax;
				$setRepArray[]	=	 $currentdate;
				$logopath		=	 Router::url('/', true).'img/';
				$setRepArray[]	=	 '<img src="'.$logopath.'logo.jpg" >';
				$logo			=	 '<img src="'.$logopath.'logo.jpg" >';
				$signature		=	 '<img src="'.$logopath.'sig.jpg" >';
				
				$setRepArray[]	=	 $logo;
				$setRepArray[]	=	 $signature;
				$setRepArray[]	=	 $splitOrderId;
				$splitOrderDetail =	$this->MergeUpdate->find('first', array('conditions' => array( 'MergeUpdate.product_order_id_identify' => $splitOrderId )));
				
				$tempId			=	$splitOrderDetail['MergeUpdate']['lable_id'];
				$countryCode	=	$splitOrderDetail['MergeUpdate']['country_code'];
				
				$setRepArray[]	=	 $countryCode;
				$setRepArray[]	=	 $weight;
				$conversionRate	=	Configure::read( 'conversionRate' );
				if($currency == 'GBP')
				{
					$totalvalue = $totalvalue;
				}
				else
				{
					$totalvalue = number_format($totalvalue/$conversionRate, 2, '.' ,'');
				}
				
				$setRepArray[]	=	 $totalvalue;
				if($splitOrderDetail['MergeUpdate']['track_id'] != '')
				{
					$trackingnumber = $splitOrderDetail['MergeUpdate']['track_id'];
				}
				else
				{
					$trackingnumber = 'A'.mt_rand(100000, 999999);
				}
				
				$setRepArray[]	=	 $trackingnumber;
				$setRepArray[]	=	 $subSource;
				$setRepArray[]	=	 $CARTAS;
			
				$cssPath = WWW_ROOT .'css/';
				$imgPath = Router::url('/', true) .'img/';
			
			    $html = '<meta charset="utf-8">
						 <meta http-equiv="X-UA-Compatible" content="IE=edge">
						 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
						 <meta content="" name="description"/>
						 <meta content="" name="author"/><body>'.$html.'</body>';
				$html .= '<style>'.file_get_contents($cssPath.'pdfstyle.css').'</style>';
				$html 	= $this->setReplaceValueLabel( $setRepArray, $html );
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
			
				$imgPath = WWW_ROOT .'img/printPDF/'; 
				$path = Router::url('/', true).'img/printPDF/';
				$date = new DateTime();
				$timestamp = $date->getTimestamp();
				$name	=	'Packagin_Label_'.$splitOrderId.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				echo $serverPath  	= 	$path.$name;
				exit;
		}
		
	
		
		public function varifyLabelSlip()
		{
			
				$this->loadModel( 'PackagingSlip' );
				$this->loadModel( 'MergeUpdate' );
				$this->loadModel( 'Product' );
				
				$splitOrderId = $this->request->data['orderid'];
				$orderArray = explode( '-', $splitOrderId );
				$id				=	$orderArray[0];
				
				$openOrderId	=	$orderArray[0];
				$splitOrderId	=	$splitOrderId;
		
				$this->layout = '';
				$this->autoRender = false;
				
				require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
				
				spl_autoload_register('DOMPDF_autoload'); 
				$dompdf = new DOMPDF();
				
				$dompdf->set_paper(array(0, 0, 245, 450), 'portrait');
				$order	=	$this->getOpenOrderById( $openOrderId );
				
				
				$getSplitOrder	=	$this->MergeUpdate->find('first', array('conditions' => array('MergeUpdate.order_id' => $openOrderId, 'MergeUpdate.product_order_id_identify' => $splitOrderId)));
				$itemPrice		=	$getSplitOrder['MergeUpdate']['price'];
				$itemQuantity	=	$getSplitOrder['MergeUpdate']['quantity'];
				$BarcodeImage	=	$getSplitOrder['MergeUpdate']['order_barcode_image'];
				
				
				$k = '10';
				$companyname = '';
				$returnaddress = '';
				switch ($k) {
								case 0:
									echo "";
									break;
								case 1:
									echo "";
									break;
								case 2:
									echo "";
									break;
								default:
								   $companyname 	 =  'Jij Group';
								   $returnaddress 	 =  'Address Line 1 <br> Address Line 2 <br>Some Town Some Region 123 ABC <br>United Country';
							}
				
				$serviceLevel		=	 	$order['shipping_info']->PostalServiceName;
				$assignedservice	=	 	$order['assigned_service'];
				$courier			=	 	$order['courier'];
				$manifest			=	 	($order['manifest'] == 1) ? '1' : '0';
				$cn22				=	 	($order['cn22'] == 1) ? '1' : '0';
				$barcode			=	 	$order['assign_barcode'];
				
				$subtotal			=		$order['totals_info']->Subtotal;
				$subsource			=		$order['sub_source'];
				
				$totlacharge		=		$order['totals_info']->TotalCharge;
				$ordernumber		=		$order['num_order_id'];
				$fullname			=		$order['customer_info']->Address->FullName;
				$address1			=		$order['customer_info']->Address->Address1;
				$address2			=		$order['customer_info']->Address->Address2;
				$address3			=		$order['customer_info']->Address->Address3;
				$town				=	 	$order['customer_info']->Address->Town;
				$resion				=	 	$order['customer_info']->Address->Region;
				$postcode			=	 	$order['customer_info']->Address->PostCode;
				$country			=	 	$order['customer_info']->Address->Country;
				$phone				=	 	$order['customer_info']->Address->PhoneNumber;
				$company			=	 	$order['customer_info']->Address->Company;
				$paymentmethod		=	 	$order['totals_info']->PaymentMethod;
				$postagecost		=	 	$order['totals_info']->PostageCost;
				$tax				=	 	$order['totals_info']->Tax;
				$barcode  			=   	$order['assign_barcode'];
				$items				=	 	$order['items'];
				$address			=		'';
				$address			.=		($company != '') ? $company.'<br>' : '' ; 
				$address			.=		($fullname != '') ? $fullname.'<br>' : '' ; 
				$address			.=		($address1 != '') ? $address1.'<br>' : '' ; 
				$address			.=		($address2 != '') ? $address2.'<br>' : '' ; 
				$address			.=		($address3 != '') ? $address3.'<br>' : '' ;
				$address			.=		($town != '') ? $town.'<br>' : '';
				$address			.=		($resion != '') ? $resion.'<br>' : '';
				$address			.=		($postcode != '') ? $postcode.'<br>' : '';
				$address			.=		($country != '' ) ? $country.'<br>' : '';
											
				$recivedate			=	 	explode('T', $order['general_info']->ReceivedDate);
				
				$setRepArray = array();
				$setRepArray[] 					= $address1;
				$setRepArray[] 					= $address2;
				$setRepArray[] 					= $address3;
				$setRepArray[] 					= $town;
				$setRepArray[] 					= $resion;
				$setRepArray[] 					= $postcode;
				$setRepArray[] 					= $country;
				$setRepArray[] 					= $phone;
				$setRepArray[] 					= $ordernumber;
				$setRepArray[] 					= $courier;
				$setRepArray[] 					= $recivedate[0];
				$i = 1;
				$str = '';
				$skus = explode( ',', $getSplitOrder['MergeUpdate']['sku']);
				$totalGoods = 0;
				foreach( $skus as $sku )
				{
					$newSkus[]				=	 explode( 'XS-', $sku);
					foreach($newSkus as $newSku)
						{
							
							$getsku = 'S-'.$newSku[1];
							$getOrderDetail = $this->Product->find( 'first', array( 'conditions' => array('Product.product_sku' => $getsku ) ) );
							$title	=	$getOrderDetail['Product']['product_name'];
							$totalGoods = $totalGoods + $newSku[0];
							$str .= '<tr>
									<td valign="top" class="noleftborder rightborder bottomborder">'.$i.'</td>
									<td valign="top" class="rightborder bottomborder">'.substr($title, 0, 15 ).'</td>
									<td valign="top" class="center norightborder bottomborder">'.$newSku[0][0].'</td>';
							$str .= '</tr>';
							$i++;
						}
						unset($newSkus);
				}
				
				/**********************************************/
				$countryArray = Configure::read('customCountry');
				$gethtml =	$this->PackagingSlip->find('first', array(
											'conditions' => array( 
													'PackagingSlip.store_name' => $subsource,
													)
												)
											);
				
				$html 			=	$gethtml['PackagingSlip']['html'];
				$paperHeight    =   $gethtml['PackagingSlip']['paper_height'];
				$paperWidth  	=   $gethtml['PackagingSlip']['paper_width'];
				$barcodeHeight  =   $gethtml['PackagingSlip']['barcode_height'];
				$barcodeWidth   =   $gethtml['PackagingSlip']['barcode_width'];
				$paperMode      =   $gethtml['PackagingSlip']['paper_mode'];
			
				/**********************************************/
				
				$dompdf->set_paper(array(0, 0, $paperWidth, $paperHeight ), $paperMode);
				
				$totalitem = $i - 1;
				$setRepArray[]	=	 $str;
				$setRepArray[]	=	 $totalGoods;
				$setRepArray[]	=	 $subtotal;
				$Path 			= 	'/wms/img/client/';
				$img			=	 '<img src=http://localhost/wms/img/client/demo.png height="50" width="50">';
				$setRepArray[]	=	 $img;
				$setRepArray[]	=	 $postagecost;
				$setRepArray[]	=	 $tax;
				$totalamount	=	 (float)$subtotal + (float)$postagecost + (float)$tax;
				$setRepArray[]	=	 $itemPrice;
				$setRepArray[]	=	 $address;
				$barcodePath  	=  Router::url('/', true).'img/orders/barcode/';
				$barcodeimg 	=  '<img src='.$barcodePath.$BarcodeImage.' width='. $barcodeWidth .'>';
				$barcodenum		=	explode('.', $barcode);
				
				$setRepArray[] 	=  $barcodeimg;
				$setRepArray[] 	=  $paymentmethod;
				$setRepArray[] 	=  $barcodenum[0];
				$setRepArray[] 	=  '';
				
				
				
				$img 			=	$gethtml['PackagingSlip']['image_name'];
				$returnaddress 	=	str_replace(',', '<br>', $gethtml['PackagingSlip']['address']);
				$setRepArray[] 	=  $returnaddress;
				$setRepArray[] 	=  '<img src ='.$imgPath = Router::url('/', true) .'img/'.$img.' height = 36 >';
				$setRepArray[] 	= $splitOrderId;
				
				$imgPath = WWW_ROOT .'css/';
				
				$html2 = '<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
				<meta content="" name="description"/>
				<meta content="" name="author"/><body>'.$html.'</body>';
				
				$html2 	   .= 	'<style>'.file_get_contents($imgPath.'pdfstyle.css').'</style>';
				$html 		= 	$this->setReplaceValue( $setRepArray, $html2 );
				
				$dompdf->load_html(utf8_decode($html), Configure::read('App.encoding'));
				$dompdf->render();
			
				$imgPath = WWW_ROOT .'img/printPDF/'; 
				$path = Router::url('/', true).'img/printPDF/';
				$date = new DateTime();
				$timestamp = $date->getTimestamp();
				$name	=	'Packagin_Slip_'.$splitOrderId.'.pdf';
				
				file_put_contents($imgPath.$name, $dompdf->output());
				echo $serverPath  	= 	$path.$name;
				exit;
		}
	
	 
}
?>
