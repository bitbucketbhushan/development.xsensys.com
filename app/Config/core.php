<?php
/**
 * This is core configuration file.
 *
 * Use it to configure core behavior of Cake.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * CakePHP Debug Level:
 *
 * Production Mode:
 * 	0: No error messages, errors, or warnings shown. Flash messages redirect.
 *
 * Development Mode:
 * 	1: Errors and warnings shown, model caches refreshed, flash messages halted.
 * 	2: As in 1, but also with full debug messages and SQL output.
 *
 * In production mode, flash messages redirect after a time interval.
 * In development mode, you need to click the flash message to continue.
 */
	Configure::write('debug', 2);
/**
 * Configure the Error handler used to handle errors for your application. By default
 * ErrorHandler::handleError() is used. It will display errors using Debugger, when debug > 0
 * and log errors with CakeLog when debug = 0.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle errors. You can set this to any callable type,
 *   including anonymous functions.
 *   Make sure you add App::uses('MyHandler', 'Error'); when using a custom handler class
 * - `level` - integer - The level of errors you are interested in capturing.
 * - `trace` - boolean - Include stack traces for errors in log files.
 *
 * @see ErrorHandler for more information on error handling and configuration.
 */
	Configure::write('Error', array(
		'handler' => 'ErrorHandler::handleError',
		'level' => E_ALL & ~E_DEPRECATED,
		'trace' => true
	));

/**
 * Configure the Exception handler used for uncaught exceptions. By default,
 * ErrorHandler::handleException() is used. It will display a HTML page for the exception, and
 * while debug > 0, framework errors like Missing Controller will be displayed. When debug = 0,
 * framework errors will be coerced into generic HTTP errors.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle exceptions. You can set this to any callback type,
 *   including anonymous functions.
 *   Make sure you add App::uses('MyHandler', 'Error'); when using a custom handler class
 * - `renderer` - string - The class responsible for rendering uncaught exceptions. If you choose a custom class you
 *   should place the file for that class in app/Lib/Error. This class needs to implement a render method.
 * - `log` - boolean - Should Exceptions be logged?
 * - `skipLog` - array - list of exceptions to skip for logging. Exceptions that
 *   extend one of the listed exceptions will also be skipped for logging.
 *   Example: `'skipLog' => array('NotFoundException', 'UnauthorizedException')`
 *
 * @see ErrorHandler for more information on exception handling and configuration.
 */
	Configure::write('Exception', array(
		'handler' => 'ErrorHandler::handleException',
		'renderer' => 'ExceptionRenderer',
		'log' => true
	));

/**
 * Application wide charset encoding
 */
	Configure::write('App.encoding', 'UTF-8');

	//Configure::write('Config.timezone', 'Europe/Jersey');
	
	//Configure::write('Config.timezone', 'Asia/Kolkata');
	
	

/**
 * To configure CakePHP *not* to use mod_rewrite and to
 * use CakePHP pretty URLs, remove these .htaccess
 * files:
 *
 * /.htaccess
 * /app/.htaccess
 * /app/webroot/.htaccess
 *
 * And uncomment the App.baseUrl below. But keep in mind
 * that plugin assets such as images, CSS and JavaScript files
 * will not work without URL rewriting!
 * To work around this issue you should either symlink or copy
 * the plugin assets into you app's webroot directory. This is
 * recommended even when you are using mod_rewrite. Handling static
 * assets through the Dispatcher is incredibly inefficient and
 * included primarily as a development convenience - and
 * thus not recommended for production applications.
 */
	//Configure::write('App.baseUrl', env('SCRIPT_NAME'));

/**
 * To configure CakePHP to use a particular domain URL
 * for any URL generation inside the application, set the following
 * configuration variable to the http(s) address to your domain. This
 * will override the automatic detection of full base URL and can be
 * useful when generating links from the CLI (e.g. sending emails)
 */
	//Configure::write('App.fullBaseUrl', 'http://example.com');

/**
 * Web path to the public images directory under webroot.
 * If not set defaults to 'img/'
 */
	//Configure::write('App.imageBaseUrl', 'img/');

/**
 * Web path to the CSS files directory under webroot.
 * If not set defaults to 'css/'
 */
	//Configure::write('App.cssBaseUrl', 'css/');

/**
 * Web path to the js files directory under webroot.
 * If not set defaults to 'js/'
 */
	//Configure::write('App.jsBaseUrl', 'js/');

/**
 * Uncomment the define below to use CakePHP prefix routes.
 *
 * The value of the define determines the names of the routes
 * and their associated controller actions:
 *
 * Set to an array of prefixes you want to use in your application. Use for
 * admin or other prefixed routes.
 *
 * 	Routing.prefixes = array('admin', 'manager');
 *
 * Enables:
 *	`admin_index()` and `/admin/controller/index`
 *	`manager_index()` and `/manager/controller/index`
 *
 */
	//Configure::write('Routing.prefixes', array('admin'));

/**
 * Turn off all caching application-wide.
 *
 */
	//Configure::write('Cache.disable', true);

/**
 * Enable cache checking.
 *
 * If set to true, for view caching you must still use the controller
 * public $cacheAction inside your controllers to define caching settings.
 * You can either set it controller-wide by setting public $cacheAction = true,
 * or in each action using $this->cacheAction = true.
 *
 */
	//Configure::write('Cache.check', true);

/**
 * Enable cache view prefixes.
 *
 * If set it will be prepended to the cache name for view file caching. This is
 * helpful if you deploy the same application via multiple subdomains and languages,
 * for instance. Each version can then have its own view cache namespace.
 * Note: The final cache file name will then be `prefix_cachefilename`.
 */
	//Configure::write('Cache.viewPrefix', 'prefix');

/**
 * Session configuration.
 *
 * Contains an array of settings to use for session configuration. The defaults key is
 * used to define a default preset to use for sessions, any settings declared here will override
 * the settings of the default config.
 *
 * ## Options
 *
 * - `Session.cookie` - The name of the cookie to use. Defaults to 'CAKEPHP'
 * - `Session.timeout` - The number of minutes you want sessions to live for. This timeout is handled by CakePHP
 * - `Session.cookieTimeout` - The number of minutes you want session cookies to live for.
 * - `Session.checkAgent` - Do you want the user agent to be checked when starting sessions? You might want to set the
 *    value to false, when dealing with older versions of IE, Chrome Frame or certain web-browsing devices and AJAX
 * - `Session.defaults` - The default configuration set to use as a basis for your session.
 *    There are four builtins: php, cake, cache, database.
 * - `Session.handler` - Can be used to enable a custom session handler. Expects an array of callables,
 *    that can be used with `session_save_handler`. Using this option will automatically add `session.save_handler`
 *    to the ini array.
 * - `Session.autoRegenerate` - Enabling this setting, turns on automatic renewal of sessions, and
 *    sessionids that change frequently. See CakeSession::$requestCountdown.
 * - `Session.ini` - An associative array of additional ini values to set.
 *
 * The built in defaults are:
 *
 * - 'php' - Uses settings defined in your php.ini.
 * - 'cake' - Saves session files in CakePHP's /tmp directory.
 * - 'database' - Uses CakePHP's database sessions.
 * - 'cache' - Use the Cache class to save sessions.
 *
 * To define a custom session handler, save it at /app/Model/Datasource/Session/<name>.php.
 * Make sure the class implements `CakeSessionHandlerInterface` and set Session.handler to <name>
 *
 * To use database sessions, run the app/Config/Schema/sessions.php schema using
 * the cake shell command: cake schema create Sessions
 *
 */
	Configure::write('Session', array(
		'defaults' => 'php'
	));

/**
 * A random string used in security hashing methods.
 */
	Configure::write('Security.salt', 'DYhG93b0qyJfIxfs2yuVoUubWwvniR2G0FgaC9mi');

/**
 * A random numeric string (digits only) used to encrypt/decrypt strings.
 */
	Configure::write('Security.cipherSeed', '76859309658453542496749683645');
   
   
    
/* Start here creating san array of js file paths */
Configure::write('roles_key',array(    
    '0'=>'Administrator',
    '1'=>'Warehouse Manager',
    '2'=>'Client'
));

/* Start here creating arra for min value for rack section and level */
Configure::write('rack_detail',array(    
    'min_rack'=>'25',
    'min_section'=>'25',
    'min_level'=>'25'
));

/* Start here creating san array of js file paths */
Configure::write('status_key',array(    
    '0'=>'Active',
    '1'=>'Deactive'
));

Configure::write('attribute_type_key',array(    
    'Text'=>'Text',
    'Text Area'=>'Text Area',
    'Select'=>'Select',
    'File'=>'File',
    'Radio'=>'Radio',
    'Checkbox'=>'Check',
));

Configure::write('attribute_type',array(    
    'Text'=>'Text Field',
    'Text Area'=>'Text Area',
    'Date'=>'Date',
    'Radio'=>'Yes/No',
    'Multiple Select'=>'Multiple Select',
    'Dropdown'=>'Dropdown',
    'Price'=>'Price',
    'Media Image'=>'Media Image',
    'Fixed Product Tax'=>'Fixed Product Tax'
));

Configure::write( 'conversionRate', '1.38' );
//Configure::write( 'access_token', '1770B90328E24F3FB0C2E0E231CD8D44' );
//Configure::write( 'access_token', '424225C3D22D' );

//Configure::write( 'linnwork_api_username', 'jijgrouptest@gmail.com' );
//Configure::write( 'linnwork_api_username', 'testjijgrouptest@gmail.com' );
//Configure::write( 'linnwork_api_password', '#noida15' );

//Configure::write( 'linnwork_api_username', 'test2jijgroup@gmail.com' );
//Configure::write( 'linnwork_api_password', '#noida15' );
//Configure::write( 'access_token', '3428E2A3FF92' );
Configure::write( 'access_token', 'F57A83B91148' );

Configure::write( 'access_new_token', 'a40fa7ce93d912c26604fbcd909496ca' );
Configure::write( 'application_id', '9b475d33-0534-4847-ab8b-1163156da0b3' );
Configure::write( 'application_secret', '28bab04f-edad-4f54-909a-5ac2437510de' );

Configure::write( 'linnwork_api_username', 'jake.shaw@euracogroup.co.uk' );
//Configure::write( 'linnwork_api_password', '#aniket55' );
Configure::write( 'linnwork_api_password', '#aniket2016' );

//Configure::write( 'linnwork_api_username', 'jake.shaw@euracogroup.co.uk' );
//Configure::write( 'linnwork_api_password', '#aniket55' );
//Configure::write( 'access_token', 'F57A83B91148' );

/**
 * Apply timestamps with the last modified time to static assets (js, css, images).
 * Will append a query string parameter containing the time the file was modified. This is
 * useful for invalidating browser caches.
 *
 * Set to `true` to apply timestamps when debug > 0. Set to 'force' to always enable
 * timestamping regardless of debug value.
 */
	//Configure::write('Asset.timestamp', true);

/**
 * Compress CSS output by removing comments, whitespace, repeating tags, etc.
 * This requires a/var/cache directory to be writable by the web server for caching.
 * and /vendors/csspp/csspp.php
 *
 * To use, prefix the CSS link URL with '/ccss/' instead of '/css/' or use HtmlHelper::css().
 */
	//Configure::write('Asset.filter.css', 'css.php');

/**
 * Plug in your own custom JavaScript compressor by dropping a script in your webroot to handle the
 * output, and setting the config below to the name of the script.
 *
 * To use, prefix your JavaScript link URLs with '/cjs/' instead of '/js/' or use JsHelper::link().
 */
	//Configure::write('Asset.filter.js', 'custom_javascript_output_filter.php');

/**
 * The class name and database used in CakePHP's
 * access control lists.
 */
	Configure::write('Acl.classname', 'DbAcl');
	Configure::write('Acl.database', 'default');

/**
 * Uncomment this line and correct your server timezone to fix
 * any date & time related errors.
 */
	//date_default_timezone_set('UTC');

/**
 * `Config.timezone` is available in which you can set users' timezone string.
 * If a method of CakeTime class is called with $timezone parameter as null and `Config.timezone` is set,
 * then the value of `Config.timezone` will be used. This feature allows you to set users' timezone just
 * once instead of passing it each time in function calls.
 */
	//Configure::write('Config.timezone', 'Europe/Paris');

/**
 * Cache Engine Configuration
 * Default settings provided below
 *
 * File storage engine.
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'File', //[required]
 *		'duration' => 3600, //[optional]
 *		'probability' => 100, //[optional]
 * 		'path' => CACHE, //[optional] use system tmp directory - remember to use absolute path
 * 		'prefix' => 'cake_', //[optional]  prefix every cache file with this string
 * 		'lock' => false, //[optional]  use file locking
 * 		'serialize' => true, //[optional]
 * 		'mask' => 0664, //[optional]
 *	));
 *
 * APC (http://pecl.php.net/package/APC)
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Apc', //[required]
 *		'duration' => 3600, //[optional]
 *		'probability' => 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 *	));
 *
 * Xcache (http://xcache.lighttpd.net/)
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Xcache', //[required]
 *		'duration' => 3600, //[optional]
 *		'probability' => 100, //[optional]
 *		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional] prefix every cache file with this string
 *		'user' => 'user', //user from xcache.admin.user settings
 *		'password' => 'password', //plaintext password (xcache.admin.pass)
 *	));
 *
 * Memcached (http://www.danga.com/memcached/)
 *
 * Uses the memcached extension. See http://php.net/memcached
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Memcached', //[required]
 *		'duration' => 3600, //[optional]
 *		'probability' => 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 		'servers' => array(
 * 			'127.0.0.1:11211' // localhost, default port 11211
 * 		), //[optional]
 * 		'persistent' => 'my_connection', // [optional] The name of the persistent connection.
 * 		'compress' => false, // [optional] compress data in Memcached (slower, but uses less memory)
 *	));
 *
 *  Wincache (http://php.net/wincache)
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Wincache', //[required]
 *		'duration' => 3600, //[optional]
 *		'probability' => 100, //[optional]
 *		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 *	));
 */

/**
 * Configure the cache handlers that CakePHP will use for internal
 * metadata like class maps, and model schema.
 *
 * By default File is used, but for improved performance you should use APC.
 *
 * Note: 'default' and other application caches should be configured in app/Config/bootstrap.php.
 *       Please check the comments in bootstrap.php for more info on the cache engines available
 *       and their settings.
 */
$engine = 'File';

// In development mode, caches should expire quickly.
$duration = '+999 days';
if (Configure::read('debug') > 0) {
	$duration = '+10 seconds';
}

// Prefix each application on the same server with a different string, to avoid Memcache and APC conflicts.
$prefix = 'myapp_';

/**
 * Configure the cache used for general framework caching. Path information,
 * object listings, and translation cache files are stored with this configuration.
 */
Cache::config('_cake_core_', array(
	'engine' => $engine,
	'prefix' => $prefix . 'cake_core_',
	'path' => CACHE . 'persistent' . DS,
	'serialize' => ($engine === 'File'),
	'duration' => $duration
));

/**
 * Configure the cache for model and datasource caches. This cache configuration
 * is used to store schema descriptions, and table listings in connections.
 */
Cache::config('_cake_model_', array(
	'engine' => $engine,
	'prefix' => $prefix . 'cake_model_',
	'path' => CACHE . 'models' . DS,
	'serialize' => ($engine === 'File'),
	'duration' => $duration
));

Configure::write( 'username', '72b154e71a740ba5ca548ca045c38c9f789e0284' );
Configure::write( 'usernameachin', '72b154e71a740ba5ca548ca045c38c9f789e0284' );
//Configure::write( 'usernameachin', '8421ff640887cd50da442435289aeeec8c076bd6' );
//Configure::write( 'username', '30d51c7205696e65913e2ecf3eae924b8742008d' ); //ashish sir
Configure::write( 'password', '' );
Configure::write( 'perpage', '100' );

//Custom color
Configure::write('manifestColor',array(    
    'A1'=>'ffff00',
    'A2'=>'99ffcc',
    'A3'=>'ffa065',
    'A4'=>'ffffe5',
    'A5'=>'ffccff',
    'A6'=>'33cc33',
    'A7'=>'fff7d5',
    'A8'=>'b9b9ff',
    'A9'=>'fac090',
    'A10'=>'ffdc97',
    'A11'=>'ff4215',
    'A12'=>'ffffb3',
    'A13'=>'ffd72f',
    'A14'=>'d9f2ff',
    'A15'=>'ffff8b',
    'A16'=>'f7edf4',
    'A17'=>'ffde9b',
    'A18'=>'2ef233',
    'A19'=>'ffff00',
    'A20'=>'99ffcc',
    'A21'=>'ffa065',
    'A22'=>'ffffe5',
    'A23'=>'ffccff',
    'A24'=>'33cc33',
    'A25'=>'fff7d5',
    'A26'=>'b9b9ff',
    'A27'=>'fac090',
    'A28'=>'ffff00',
    'A29'=>'99ffcc',
    'A30'=>'ffa065',
    'A31'=>'ffffe5',
    'A32'=>'ffccff',
    'A33'=>'33cc33',
    'A34'=>'fff7d5',
    'A35'=>'b9b9ff',
    'A36'=>'fac090',
    'A37'=>'ffdc97',
    'A38'=>'ff4215',
    'A39'=>'ffffb3',
    'A40'=>'ffd72f',
    'A41'=>'d9f2ff',
    'A42'=>'ffff8b',
    'A43'=>'f7edf4',
    'A44'=>'ffde9b',
    'A45'=>'2ef233',
    'A46'=>'ffff00',
    'A47'=>'99ffcc',
    'A48'=>'ffa065',
    'A49'=>'ffffe5',
    'A50'=>'ffccff',
    'A51'=>'33cc33',
    'A52'=>'fff7d5',
    'A53'=>'b9b9ff',
    'A54'=>'fac090'      
));

//Custom color
Configure::write('customIsoCodes',array(
			'Austria' => 'AT',
			'Belgium' => 'BE',
			'Bulgaria' => 'BG',
			'Croatia' => 'HR',
			'Cyprus' => 'CY',
			'Czech Republic' => 'CZ',
			'Denmark' => 'DK',
			'Estonia' => 'EE',
			'Jersey' => 'JE',
			'Finland' => 'FI',
			'France' => 'FR',
			'Germany' => 'DE',
			'Greece' => 'GR',
			'Ireland' => 'IE',
			'Hungary' => 'HU',
			'Italy' => 'IT',
			'Latvia' => 'LV',
			'Lithuania' => 'LT',
			'Luxembourg' => 'LU',
			'Malta' => 'MT',
			'Netherlands' => 'NL',
			'Poland' => 'PL',
			'Portugal' => 'PT',
			'Romania' => 'RO',
			'Slovakia' => 'SK',
			'Slovenia' => 'SI',
			'Spain' => 'ES',
			'Sweden' => 'SE',
			'United Kingdom' => 'GB'
		)
);

//Custom color
Configure::write('customCountry',array(
		'Austria' => 'Austria',
		'Belgium' => 'Belgium',
		'Bulgaria' => 'Bulgaria',
		'Croatia' => 'Croatia',
		'Cyprus' => 'Cyprus',
		'Czech Republic' => 'Czech Republic',
		'Denmark' => 'Denmark',
		'Estonia' => 'Estonia',
		'Finland' => 'Finland',
		'France' => 'France',
		'Germany' => 'Germany',
		'Greece' => 'Greece',
		'Hungary' => 'Hungary',
		'Ireland' => 'Ireland',
		'Italy' => 'Italy',
		'Latvia' => 'Latvia',
		'Lithuania' => 'Lithuania',
		'Luxembourg' => 'Luxembourg',
		'Malta' => 'Malta',
		'Netherlands' => 'Netherlands',
		'Poland' => 'Poland',
		'Portugal' => 'Portugal',
		'Romania' => 'Romania',
		'Slovakia' => 'Slovakia',
		'Slovenia' => 'Slovenia',
		//'Spain' => 'Spain', //11-01-2017 by Avadhesh
		'Sweden' => 'Sweden',
		'United Kingdom' => 'United Kingdom'
	)
);
	
Configure::write( 'unwanted', array( 'Amazon' => 'Amazon' , 'Direct' => 'Direct')  );	
				
//inner credentials
Configure::write('notifyCenter',array(   
	'app_id' => '212999',
	'key' => '86e27f98837275186a41',
	'secret' => '01882f4d30f605e55757'
));

//core option
Configure::write('coreOption',array(   
	'Customer' => 'Customer',
	'Staff' => 'Staff'
));


//dhl service
Configure::write('_dhlServiceUrlByType_',array(
		'_sand' => 'https://xmlpitest-ea.dhl.com/XMLShippingServlet',
		'_prod' => 'https://xmlpi-ea.dhl.com/XMLShippingServlet',
		
	)
);


//authentication for testing
/*Configure::write('_dhlAuthentication_',array(
		'_user' => 'CIMGBTest',
		'_pass' => 'DLUntOcJma'		
	)
);*/
//authentication for production
Configure::write('_dhlAuthentication_',array(
		'_user' => 'ESLXMLPI',
		'_pass' => 'gYkrAB1VFa'		
	)
);

//authentication
/*Configure::write('_dhlShipperAccount_',array(
		'_shipper_account_' => '130000279'	
	)
);*/
Configure::write('_dhlShipperAccount_',array(
		'_shipper_account_' => '418038613'	
	)
);

//-------DHL client Auth------------ 

Configure::write( 'dhlAuth' , array(
									'server' => '188.65.112.228',
									'port' => '21',
									'user' => 'dhl@euracogroup.com',
									'pass' => 'et*{G4KCkxgF')
		);
		
		
/*Configure::write( 'royalmail_auth' , 
	array(
		'x_ibm_client_id' => 'c4e06b7b-4054-40b7-9764-7fd241011d91',
		'x_ibm_client_secret' => 'qT6sK1qT2qN5tB2sS3kV2kE3qY5dT8dL5aX5wO5oI0lC5xR6hV',
		'x_rmg_password' => 'Password1*',
		'x_rmg_user_name' => 'ESLSPRINGAPI'
	)
);
*///22 SEP 2020
// dev

Configure::write( 'royalmail_auth' , 
	array(
		'x_ibm_client_id' => '0d52a033-933b-49eb-aa39-2d46d569801f',
		'x_ibm_client_secret' => 'qO7oI8vM8wU8jF1rP5bL0sQ8aY0mL0yD7kS1sX5hB1wN7kB2cT',
		'x_rmg_user_name' => '0490919000API',
		'x_rmg_password' => 'S0490API919000B',
		
	)
);
// live
/*Configure::write( 'royalmail_auth' , 
	array(
		'x_ibm_client_id' => 'c4e06b7b-4054-40b7-9764-7fd241011d91',
		'x_ibm_client_secret' => 'T1aN8fY5dC4tF6sD3kJ6oY4yH3vH2jU6qX6eU1mN4lC8kW4iB2',
		'x_rmg_user_name' => '0490919000API',
		'x_rmg_password' => '#APIJersey20900',
		
	)
);*/
/*-----------Sandbox(Test)--------*/
/*Configure::write( 'postnl' , 
	array(
		'customer_code' => 'HLRQ',
		'customer_number' => '10472686',
		'server_url' => 'https://api-sandbox.postnl.nl',
 		'api_key' => 'Z7J23nyK3BOEPkR3pgIVAAcOKheVCTDV' 
	)
);*/

Configure::write( 'postnl' , 
	array(
		'customer_code' => 'HLRQ',
		'customer_number' => '10472686',
		'server_url' => 'https://clients.postnl.a02.cldsvc.net/v7',
		'api_key' =>'G0r6x7y8IDLdKQkXER3UZgBEJ7wfgJw6rKs48EZtcytHh3tSWKPOHNu6vYoAifUWSgdfcuSoS0kBSJmagyeFk0uCrP68rQmudYYqccn86EDccx4IpNQzP8NYrjrd6VVpcEMhpjgLnN5b2dYeJibYX26lMfcdp6u6R9NEZlQZn9Hwb5PMOFiry9dvqthPkPUo0211b4Bsg06JbdcRS6Nlz3CE6YIbwVjZEFxUkxeO0LkdPWWYfNEkfb3VrLDNDskyOFKRWNBFa1UMxcfqgkFUxoftwFCMh5qhsrDhzZfN7i95'
 		//'api_key' => 'Z7J23nyK3BOEPkR3pgIVAAcOKheVCTDV' 
	)
);

 /*-----------Jerseypost Sandbox(Test)--------*/
 //https://docs.jerseypost-atlas.com
Configure::write( 'jerseypost' , 
	array(
		'product_code' => 'UPU', 
		'server_url' =>'https://api.staging.jerseypost-atlas.com',
 		'api_account_id' => '1eb1f85f-031d-6e18-bbdd-0242ac100006' ,
		'api_token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiODIyOGQ4NTRjYzc4MzA4ZDA0NzgwNGE3ZThhMDY5ZGM0MjQ4NTM3Njg0NzFmZTk2MTczYWNiMWMyOTQ5NmY4YTI0ZDJhYzFiN2ZhNzI0ODciLCJpYXQiOjE2NDQyNDY4MDkuMzU4OTY2LCJuYmYiOjE2NDQyNDY4MDkuMzU4OTcsImV4cCI6NDc2ODM4NDQwOS4zMjQxMjEsInN1YiI6IjE5NiIsInNjb3BlcyI6W119.szb0jLeO9SWj82_i0VtQLZgQhTzdimQm2HJyHJdUl1k3ScQ5vVJQPklZ60RJL6H12dSfVggoFc7WsAt90D2k9QlS3PJiEln1TVsjsOrKMxTp5maOCavTF2tZ7DBJ7yf8BVesCZCIpXCxn4nMGOQVo6ig0YMCaPb6ke848nCH5KtvHgsE8NrT3B6e6pwnXK9_xt1QKa5CTIIB5F98UDK2EAKuZ-__qvp9d_xk7rUKhwEFhi0pgEqL7CslyTYDa8iXKl7IesAWm3wtH6IV4faKxNcaFpTiRYRBWzHxVEcCKdjcPoNH1z2N6Km7tPJZyJxcNqM3dZTHvXaYY8trUvKAyKLBdd2U-76VCbaS2-oXF-H-PDg8nYzMpqvzK0NqBS-tHtvJ23LuqUemcJZro_7y3SkUx2uUOgNNkFnMK2N5vlJlBKjOSoLnTSoqQd-FA2OUvGp4Wm6YYHTrFCHb1S1TFswk2YikxQnKIITJl_i18L3oLPrhKste1WDMrbMnFomc-Be-6PdPZ-YmcBpYLxLtVoEM961oVFaJ9lP1By3EdctyPqzUv6_2JiqVXkNT5iY9PYaX8EZkPMV2wSdPF8GHJ__1vLE68FlE0RE2u_OpxpdwvQfvMZM0o4WCLY9gtf-_sLqpyYZeZkMbOETXVKpUk-Rir4k629v4g1XjfT-S-OE'
		/*'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNzFjODdjOWFhMGJiODMyZjMxNzZjYWRkZDE5NzU3ZGJiYjc1OWNkNGNiMDg0ZjNhNDBjNzcyYTg4ZTZlZjgxMTc4N2I0Njc4NmJiZjk2MWEiLCJpYXQiOjE2MDQ1OTY3OTksIm5iZiI6MTYwNDU5Njc5OSwiZXhwIjoxNjM2MTMyNzk5LCJzdWIiOiI5NCIsInNjb3BlcyI6W119.WKrWfsm0VDKe-6remuCypxPO6pGA4NI6TuiKVgunfv2Xw665jvhZjQmxjRYyF8pNbtBViEXuur_a2axOips3syLZxw-KtB0AKV2GLBw9zzHAA8CEshrloZvij7KQG2oyWVc7Ggz2BPIssBhIsxGBqyv-E9L1OFtigP2bNHftU-p636u-N29mOHyBKmijpi7qmauJ8wXB1C5-N-vzd379EJKA4cfdWMhEkDf5JoHPDmw0rxtcJyO8VV7jHkHKGdb1uN-igyTzp73mQV47ZKmUT2xtC25j3YL6PhSNfeYlsbLC35fXzUP3z05M0ukJYHeXhBaw0Xbe_TSApeytZc3jIEPggzy0eopBpd1xqjTVQy0yZ3sk99v9h46zi9Fc4IftnlT9qc3jr5a9842qOChyYWzhfk37DLq7OiJvYJmbzha5u9_noqokhHnD5b20BsgmdY1toRABDqqF7USpNFLDuPGKT8_XVGADtjQZOCz75wrjOCrjke6_pn3XJaTDL5ILxEU6g1Gch6vDPZNw3Lsbtn87vdVQRYW_vKaRWYqPcJhfwjB1f3W_cdmlte-B3tjJmerkQyjkkF-HHymJPyMAmVq99ADl_sZ6KfSVWJ8_it002f9iXtRq7I5UoKQEKYOap8o2Q9UosU-cyQ-1Cfea5ISdgYIod7c5D-gDaVXrtVU'*/
	)
);





 //changes by amit rawat
Configure::write( 'refundoptions' , 
	array(
		'fullamountoforder' => 'Full Amount Of Order',
		'productcost' => 'Product Cost',
		'shippingcost' => 'Shipping Cost',
		'additionalamount' => 'Additional Amount',
		'discount'=>'Discount'
	)
);
 //changes by amit rawat
Configure::write('web_store' ,'development.xsensys.com');
Configure::write('no_postnl_country' ,array('Italy','United Kingdom','Netherlands','Brazil'));

Configure::write( 'whistl_orion' , 
	array(
		'email' => 'je-operations@esljersey.com', 
 		'password' => 'password001�' ,
		'server_url' =>'https://api.orionuat.whistl.co.uk/kmukservice.svc/json/getSubshipmentLabelsInternational',
		'consignor_address1' => 'Unit 4 Airport Cargo Centre' ,
		'consignor_address2' => 'L avenue De La Comune' ,
		'consignor_address3' => '' ,
		'consignor_city'=>'Saint Helier',
		'consignor_country' => 'JE',
		'consignor_name' => 'ESL Limited',
		'consignor_postcode' => 'JE3 7BY',
		'consignor_state' => 'Jersey',
		'routing_code' => 'GTW-WHIS-603',
 	)
);

Configure::write('techdrive_url' ,'http://techdrive.biz/staging/');

