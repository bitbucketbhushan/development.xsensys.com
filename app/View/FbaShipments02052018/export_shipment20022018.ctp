<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px; }
	.highlight{border:1px #FF0000 solid;background:#FF3300;}
	.notequal{border:1px #993300 solid; background:#CCFF33;}
 </style>
<?php 
	App::Import('Controller', 'FbaShipments'); 
	$obj = new FbaShipmentsController;
	 
	$po_items = array(); $picklist = '';
	$shipment_po  = $obj->getExportShipment($this->params['pass'][0]);
	if(count($shipment_po) > 0){								  
		foreach($shipment_po['FbaShipmentItemsPo'] as $v){
			$po_items[$v['fnsku'].'_'.$v['master_sku']] = $v;
		}
		 
		$picklist = $shipment_po['FbaShipmentsPo']['picklist'];
	}		 
?>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>				
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>				
				<h4 style="float:left;">Export Shipment</h4>
				<div class="col-lg-3"  style="float:right;">
					<div class="col-lg-6">
					
					<?php  if($picklist) {	
						echo '<a href="'.Router::url('/', true).'FbaPicklists/downlodPicklist/'.$picklist.'" class="btn btn-info btn-sm">Download PickList</a>';  
					}else{
						echo '<a href="'.Router::url('/', true) .'FbaPicklists/createPickList/'.$this->params['pass'][0].'" class="btn btn-warning" style="float:left;">Generate PickList</a>';
					}
					//pr($shipment_items['FbaShipment']['status'] == 'exported');
					?>
					</div>
					<div class="col-lg-6">
					<a href="<?php echo Router::url('/', true) ?>FbaShipments" class="btn btn-primary btn-sm" style="float:right;">Back</a>
					</div>					
				</div>
				<br /><br />
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							 
						
					<div class="panel-body no-padding-top bg-white">
					</div>	
					
						<?php							
                              print $this->form->create( 'Shipment', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/FbaShipments/SaveExportShipment', 'type'=>'post','id'=>'shipment_frm' ) );
                               
                            ?>
						<div class="row" id="r1">	
						<div class="col-sm-12" style="padding:5px; background-color:#F2F2F2;">
							<?php  if(count($shipment_items['FbaShipment']) > 0) {	  ?>
								<div class="col-sm-3"><strong>Plan Id:</strong> <small><?php print $shipment_items['FbaShipment']['plan_id']; ?></small></div>
								<div class="col-sm-3"><small><strong>Total Merchant SKU:</strong> <?php print $shipment_items['FbaShipment']['total_skus']; ?></small></div>
								<div class="col-sm-2"><small><strong>Total Units:</strong> <?php print $shipment_items['FbaShipment']['total_units']; ?> </small></div>	
								<div class="col-sm-2"><small><strong>Pack List:</strong> <?php print $shipment_items['FbaShipment']['pack_list']; ?></small></div>	
								<div class="col-sm-2"><small><strong>Date:</strong> <?php print $shipment_items['FbaShipment']['added_date']; ?></small></div>	
								
								<div class="col-sm-3"><small><strong>Shipment:</strong> <?php print $shipment_items['FbaShipment']['shipment_name']; ?></small></div>		
								<div class="col-sm-7"><small><strong>Ship To:</strong> <?php print $shipment_items['FbaShipment']['ship_to']; ?></small></div>
								<div class="col-sm-2"><small><strong>Status:</strong> <?php print ucfirst($shipment_items['FbaShipment']['status']); ?></small></div>		
								
								<input type="hidden" name="_po[shipment_name]" id="shipment_name" value="<?php print $shipment_items['FbaShipment']['shipment_name']; ?>" />
								<input type="hidden" name="_po[shipment_id]" id="shipment_id" value="<?php print $shipment_items['FbaShipment']['shipment_id']; ?>" />
								<?php }if(count($shipment_po) > 0){?>
							    <input type="hidden" name="_po[shipments_po_inc_id]" id="shipments_po_inc_id" value="<?php print $shipment_po['FbaShipmentsPo']['id']; ?>" />
							   <?php } ?>
							   <input type="hidden" name="_po[shipment_inc_id]" id="shipment_inc_id" value="<?php print $this->params['pass'][0]; ?>" />
							   
						</div> 
						</div> 
						<div class="row head" style="clear:both;" id="r2">
		 					  <div class="col-sm-12">
								<div class="col-sm-2">Merchant Sku</div>
								<div class="col-sm-2">Master Sku</div>
								<div class="col-sm-2">ASIN / FNSKU</div>
								<div class="col-sm-3">Title</div>		
								<div class="col-sm-1">P. Qty<br /><small style="font-size:10px;">(Preparation)</small></div>	
								<div class="col-sm-1">Qty to Label</div>	
								<div class="col-sm-1">Qty to Send</div>	
							</div>								
						</div>	
						<div class="body" id="results">
							<div id="r3">
							<?php  if(count($shipment_items['FbaShipmentItem']) > 0) {	 ?>
							
								<?php foreach( $shipment_items['FbaShipmentItem'] as $items ) {   ?>
									<div class="row sku" id="r_<?php echo $items['id']; ?>">
										<div class="col-lg-12">	
											<?php  
												$master_sku = $obj->getSkuMapping($items['merchant_sku']);
												$packaging  = $obj->getFbaPackaging($items['merchant_sku']);											
											?>
											<div class="col-sm-2"><?php echo $items['merchant_sku']; ?>	
											
												<div>
												
												
													<?php foreach($envelope as $id => $pack){
															if($packaging != '' && $packaging == $pack){
																$sel = '';
															}else{
																$sel = 'selected="selected"';
															}
														}
														?>	
														
													<select name="packaging[<?php echo $items['merchant_sku'] ?>]" style="font-size:12px; padding:5px; border:1px solid #c6c6c6; width:100%;" onchange="updatePackaging(this,'<?php echo $items['merchant_sku'] ?>')">
													
													
														
														
														<option value="" <?php echo $sel?>>--No Packaging--</option>
														<?php foreach($envelope as $id => $pack){
															if($packaging != '' && $packaging == $pack){
																echo '<option value="'.$pack.'" selected="selected">'.$pack.'</option>';
															}else{
																echo '<option value="'.$pack.'">'.$pack.'</option>';
															}
														}
														?>		
														<option value="add_new" style="font-weight:bold; color:#FF0000;">Add New Packaging</option>												 
													</select>
												</div>
												<div id="p_<?php echo $items['merchant_sku'] ?>" style="display:none;"><input type="text" id="pin_<?php echo $items['merchant_sku'] ?>"/>&nbsp;<button type="button" class="btn btn-success btn-xs" title="Save" onclick="addPackaging('<?php echo $items['merchant_sku'] ?>')"><i class="fa  fa-check-square"></i></button></div>
											</div>
											<div class="col-sm-2"><?php echo $master_sku  ?></div>
											
											<div class="col-sm-2">
												<div><a href="https://www.amazon.com/dp/<?php echo $items['asin']; ?>" target="_blank" style="color:#333399;"><?php echo $items['asin']; ?></a></div>
												<div style="border-top:1px dashed #bbb; width:55%;"><?php echo $items['fnsku'] ?></div>
											</div>	
											<div class="col-sm-3" style="font-size:13px;"><?php echo (strlen($items['title']) > 70) ? substr($items['title'],0,70).'...' : $items['title']; 	?></div>	
											
											
																						
											<div class="col-sm-1"> 
											 <input type="hidden" name="<?php echo $items['fnsku']?>[merchant_sku]" id="merchant_sku_<?php echo $items['id']; ?>" value="<?php echo $items['merchant_sku'] ?>" />
											 <input type="hidden" name="<?php echo $items['fnsku']?>[master_sku]" id="master_sku_<?php echo $items['id'];?>"  value="<?php echo $master_sku ?>" />
											 <input type="hidden" name="<?php echo $items['fnsku']?>[prep_qty]" id="prep_qty_<?php echo $items['id']; ?>"   value="<?php echo $items['shipped'] ?>" />
											 <input type="hidden" name="<?php echo $items['fnsku']?>[items_inc_id]" id="items_inc_id_<?php echo $items['id']; ?>" value="<?php echo $items['id'] ?>" />
											 <input type="hidden" name="<?php echo $items['fnsku']?>[fnsku]" id="fnsku_<?php echo $items['id']; ?>" value="<?php echo $items['fnsku'] ?>" />
											
											<?php 								
											 
												echo "1";
												$shipped_qty = '';
												if(isset($po_items[$items['fnsku'].'_'.$master_sku])){
													$shipped_qty = $po_items[$items['fnsku'].'_'.$master_sku]['shipped_qty'];
													$inc_id = $po_items[$items['fnsku'].'_'.$master_sku]['id'];
													echo '<input type="hidden" name="'.$items['fnsku'].'[shipments_items_po_inc_id]" value="'.$inc_id.'" />';
												} 
											  
											?>
											</div>
											
											<div class="col-sm-2">
											<div style="text-align:left; float:left;" id="l_<?php echo $items['id']; ?>">
											<?php 
											if(isset($po_items[$items['fnsku'].'_'.$master_sku])){
												echo $qt=$po_items[$items['fnsku'].'_'.$master_sku]['prep_qty'];
											}else{
												echo $qt=$items['shipped'];
											} 
											if($shipped_qty == ''){
												$fqty = $qt;
											}else{
												$fqty = $shipped_qty;
											}
											?>
											</div>
											<div style="float:right">
											
												<div style="text-align:left; float:left;"><input type="number" name="<?php echo $items['fnsku']?>[qty]" id="input_<?php echo $items['id']?>" value="<?php echo $fqty?>" style="width:55px;"onchange="enterNumber('<?php echo $items['id'];?>')"/>
												<br /><center>
												<a href="javascript:void(0);" title="Update Quantity" class="btn btn-info btn-xs" id="upd_<?php echo $items['id']; ?>" onclick="updateQuantity('<?php echo $items['id']?>')"><small>Update Qty</small></a></center>
												</div>
												<div style="text-align:right; float:right;"><a href="javascript:void(0);" title="Print Label" class="btn btn-success btn-xs" id="label_<?php echo $items['id']; ?>" onclick="printLabel(<?php echo $items['id']; ?>)"><i class="fa fa-print"></i></a>&nbsp; <a href="javascript:void(0);" title="Print Slip" class="btn btn-warning btn-xs" id="slip_<?php echo $items['id']; ?>" onclick="printSlip(<?php echo $items['id']; ?>)" ><i class="fa fa-print"></i></a></div>
											</div>
											</div>
											
										 
											
										</div>		
										
										 <?php 
											
											if($master_sku){
												if(isset($bundle_mapping[$master_sku])){
												
												?>
												<div class="col-sm-2">&nbsp;&nbsp;&nbsp;&nbsp;</div>
												
												<div class="col-sm-10"><div style=" margin-bottom:5px; background-color:#BAEEF9; font-size:11px; padding-left:0px !important;padding-right:0px !important;">
												<?php  
													 
													foreach($bundle_mapping[$master_sku] as $sku => $val){
													
													$shipped_qty = '';
													if(isset($po_items[$items['fnsku'].'_'.$sku])){
														$shipped_qty = $po_items[$items['fnsku'].'_'.$sku]['shipped_qty'];
														$inc_id = $po_items[$items['fnsku'].'_'.$sku]['id'];
														echo '<input type="hidden" name="'.$items['fnsku'].'[shipments_items_po_inc_id]" value="'.$inc_id.'" />';
													}
													
													$id = $items['id'].'_'.$val['bid'];
													$name = $items['fnsku'].'[qty]['.$val['bid'].']';
													echo '<div class="col-sm-12" style="background-color:#BAEEF9; margin-left:0px !important;margin-right:0px !important; font-size:11px;border-top:1px dashed #bbb;">'; 
													echo '<div class="col-sm-2" style="font-size:11px;">'.$sku.'</div>'; 												
													echo '<div class="col-sm-6" style="text-align:left;font-size:11px; padding-left:32px">'.$val['title'].'</div>';
													echo '<div class="col-sm-1 bundle_'.$items['id'].'" for="'.$id .'" style="text-align:center; font-size:11px;">'.$val['qty'].'</div>';
													echo '<div class="col-sm-1" style="text-align:right; padding-right:15px; font-size:11px;" id="l_'.$id.'">' . $val['qty'] * $items['shipped'] .'</div>';
													echo '<div class="col-sm-2" style="text-align:right;padding-right:68px;"><span name="'.$name.'" id="span_'.$id.'">-</span></div>';
													echo  '</div>';
													} 
												?></div>  
												</div>
												<?php
												  }
											}
											?>	
									</div>
									 
								<?php }?>
							
								<div class="row">
										<div class="col-lg-12">												
											<div class="col-sm-2" style="float:right; text-align:right;">
											
											<?php
											echo $this->Form->button('Save Values', array(
												'type' => 'submit',
												'id' => 'save_values',
												'name' => 'save_values',
												'value' => 'save_values',
												'escape' => true,
												'class'=>'btn btn-info btn-sm'
												 ));	
										?>
											<?php /*?><a href="javascript:void(0);" class="btn btn-warning  btn-sm finalise_shipment">FINALISE SHIPMENT</a><?php */?></div>
										</div>
								</div>
							</div>	
							
						<div class="row sku"><div class="col-lg-12">	</div></div>	
						
						<div id="r4" style="<?php  if(count($shipment_po) == 0){ echo 'display:none;';}?>">	
						 
							
							<div class="row sku">		
								<div class="col-lg-12 col-sm-6">	
									<div class="col-sm-1">Ref.No. :</div><div class="col-sm-4"><input type="text" name="_po[ref]" value="<?php  if(count($shipment_po) > 0){ print $shipment_po['FbaShipmentsPo']['ref'];}?>" class="form-control" /></div>
									<div class="col-sm-1">Courier :</div><div class="col-sm-4"><input type="text" name="_po[courier]" value="<?php  if(count($shipment_po) > 0){ print $shipment_po['FbaShipmentsPo']['courier'];}?>" class="form-control" /></div>
								</div>
							</div>
							 
							<div class="row sku">	
								<div class="col-lg-12 col-sm-6">		
									<div class="col-sm-1">Tracking :</div><div class="col-sm-4"><input type="text" name="_po[tracking]" value="<?php  if(count($shipment_po) > 0){ print $shipment_po['FbaShipmentsPo']['tracking'];}?>" class="form-control" /></div>
									<div class="col-sm-1">Contact :</div><div class="col-sm-4"><input type="text" name="_po[contact]" value="<?php  if(count($shipment_po) > 0){ print $shipment_po['FbaShipmentsPo']['contact'];}?>" class="form-control" /></div>
								</div>
							</div>
							
							<div class="row sku">	
								<div class="col-lg-12 col-sm-6">		
									<div class="col-sm-1">Documents :</div>
									<div class="col-sm-4">
									
									<?php
									print $this->form->input( '_po.Documents1', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false) );
									?>
									<?php
									print $this->form->input( '_po.Documents2', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false) );
									?>
									</div>	
									
									<?php  if(count($shipment_po['FbaShipmentsPo']) > 0){
									
											if($shipment_po['FbaShipmentsPo']['documents1']){
											 print'<div class="col-sm-6"><a href="'. Router::url('/', true).'fba_shipments/documents/'.$shipment_po['FbaShipmentsPo']['documents1'].'" target="_blank" style="text-decoration:underline; color:#0000FF">'. $shipment_po['FbaShipmentsPo']['documents1'].'</a>&nbsp;&nbsp;<a href="'. Router::url('/', true).'FbaShipments/RemoveDocument/'.$shipment_po['FbaShipmentsPo']['id'].'/documents1" title="Remove Document"><i class="fa fa-times" aria-hidden="true"  style="color:#FF0000"></i></a>	</div>';
											}
											if($shipment_po['FbaShipmentsPo']['documents2']){
											 print'<div class="col-sm-6"><a href="'. Router::url('/', true).'fba_shipments/documents/'.$shipment_po['FbaShipmentsPo']['documents2'].'" target="_blank" style="text-decoration:underline; color:#0000FF">'. $shipment_po['FbaShipmentsPo']['documents2'].'</a>&nbsp;&nbsp;<a href="'. Router::url('/', true).'FbaShipments/RemoveDocument/'.$shipment_po['FbaShipmentsPo']['id'].'/documents2" title="Remove Document"><i class="fa fa-times" aria-hidden="true"  style="color:#FF0000"></i></a>	</div>';
											}
											
									 }?>				
									 							 
								</div>
							</div>
							
							 
						 
							<div class="row">
										<div class="col-lg-12">	
											<div class="col-sm-2" style="float:right; text-align:right;">
											 
											<?php
											echo $this->Form->button('Export Shipment', array(
												'type' => 'submit',
												'id' => 'next',
												'name' => 'export_shipment',
												'value' => 'export_shipment',
												'escape' => true,
												'class'=>'btn btn-warning btn-sm next'
												 ));	
										?>
											<br /><br />
											 <a href="<?php echo Router::url('/', true) ?>FbaShipments/ExportInvoice/<?php print $this->params['pass'][0]; ?>" class="btn btn-primary" style="float:right;">Export Invoice</a> </div>
											 
										</div>
								</div>
						</div>		
						
						<div class="row"><div class="col-sm-8"></div><div class="col-sm-4" id="msg"></div></div>	
						</form>			
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script language="javascript">	

 $("#r3 input[type=number]").each(function(){ 
 
 	var q = $(this).val();
	var id = $(this).attr("id"); 
	var v = id.replace("input_", "");

 	$(".bundle_"+v).each(function(){ 
	 var sp = $(this).attr("for");
	  var bq = parseInt($(this).text()); 
	   $("#span_"+sp).text(bq*q) ;
	   
	   $("#span_"+sp).text(bq+'x'+q+'='+bq*q);

	});
 
 });
 
  $("#r3 select").each(function(){ 
 
 		var pack = $(this).find("option:selected").val() ; 
		
		if(pack == 'add_new'){
			var name = $(this).attr('name')  ;  
			var res  = name.replace("packaging[", "");
			var v    = res.replace("]", "");		
			 $("#p_"+v).show();				
		}
 
 });
 
$(".shipment_items").click(function(){  
     $("#r1").show();
	 $("#r2").show();
	 $("#r3").show();
	 $("#r4").hide();
});

function enterNumber(v){
  	var q = $("#input_"+v).val();
 	$(".bundle_"+v).each(function(){ 
	 var sp = $(this).attr("for");
	  var bq = $(this).text();
	   $("#span_"+sp).text(bq+'x'+q+'='+bq*q);

	});
 
  var id = 'input_' + v;
  var l_value = $('#l_' + v).text(); 
  var e = document.getElementById(id);
 
	if (!/^[0-9]+$/.test(e.value)) 
	{ 	
		alert("Please enter only number.");
	 	$("#"+id).addClass("highlight");
		e.value = e.value.substring(0,e.value.length-1);
	}else{
        $("#"+id).removeClass("highlight");		
		
		$("#slip_"+v).css('visibility', 'hidden');		
		$("#label_"+v).css('visibility', 'hidden');
				 		
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaShipments/SaveExportShipmentAjax',
			type: "POST",				
			cache: false,			
			data : {id:v,shipped_qty:$('#input_'+v).val(),fnsku:$('#fnsku_'+v).val(),merchant_sku:$('#merchant_sku_'+v).val(),master_sku:$('#master_sku_'+v).val(),prep_qty:$('#prep_qty_'+v).val(), items_inc_id:$('#items_inc_id_'+v).val() ,shipment_id:$('#shipment_id').val(),shipment_inc_id:$('#shipment_inc_id').val(),shipments_po_inc_id:$('#shipments_po_inc_id').val(),shipment_name:$('#shipment_name').val()},
			success: function(data, textStatus, jqXHR)
			{			
				$("#slip_"+v).css('visibility', 'visible');		
				$("#label_"+v).css('visibility', 'visible'); 
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				}											
			} 
		}); 		
   }	
}    

function updateQuantity(v){	
		$("#slip_"+v).css('visibility', 'hidden');		
		$("#label_"+v).css('visibility', 'hidden');		 
		$("#upd_"+v+" i").addClass('fa-spin');	
			
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaShipments/SaveExportShipmentAjax',
			type: "POST",				
			cache: false,			
			data : {id:v,shipped_qty:$('#input_'+v).val(),fnsku:$('#fnsku_'+v).val(),merchant_sku:$('#merchant_sku_'+v).val(),master_sku:$('#master_sku_'+v).val(),prep_qty:$('#prep_qty_'+v).val(), items_inc_id:$('#items_inc_id_'+v).val() ,shipment_id:$('#shipment_id').val(),shipment_inc_id:$('#shipment_inc_id').val(),shipments_po_inc_id:$('#shipments_po_inc_id').val(),shipment_name:$('#shipment_name').val()},
			success: function(data, textStatus, jqXHR)
			{			
				$("#slip_"+v).css('visibility', 'visible');		
				$("#label_"+v).css('visibility', 'visible');	
				$("#upd_"+v+" i").removeClass('fa-spin');	
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				}	
											
		} 
	}); 
} 

function printLabel(v){	
	var qty = $('#input_'+v).val();
	var label_qty = parseInt($('#l_'+v).text());  
	if(qty < 1){
		alert('Please enter quantity.');
		$('#input_'+v).focus();
	}else{

		if(confirm("Are going to print #"+qty+" quantity?")){
			$("#label_"+v+" i").removeClass('fa-print');	
			$("#label_"+v+" i").addClass('fa-spin fa-refresh');	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaShipments/printLabel',
				type: "POST",				
				cache: false,			
				data : {id:v, qty:qty},
				success: function(data, textStatus, jqXHR)
				{	
					$("#label_"+v+" i").removeClass('fa-spin fa-refresh');	
					$("#label_"+v+" i").addClass('fa-print');	
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					}	
						 						
				} 
			});
		}	
	 }	 
}

function printSlip(v){	
	var qty = $('#input_'+v).val();
	$("#slip_"+v+" i").removeClass('fa-print');	
	$("#slip_"+v+" i").addClass('fa-spin fa-refresh');	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaShipments/printSlip',
		type: "POST",				
		cache: false,			
		data : {id:v,qty:qty},
		success: function(data, textStatus, jqXHR)
		{	
			$("#slip_"+v+" i").removeClass('fa-spin fa-refresh');	
			$("#slip_"+v+" i").addClass('fa-print');	
			console.log(data.msg);		
			if(data.error){
				alert(data.msg);
			} 								
		}		
 	});  		 
}

function addPackaging(v){	 
	
	if($("#pin_"+v).val() == ''){	
		alert('Please enter Packaging.');
	}else{
	
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaShipments/addPackaging',
			type: "POST",				
			cache: false,			
			data : {packaging:$("#pin_"+v).val(),merchant_sku:v },
			success: function(data, textStatus, jqXHR)
			{			 	
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				} 							
			}	
		});  
	}		 
}

function updatePackaging(t,v){	
	 
	var pack = $(t).find("option:selected").val() ;  
	if(pack == 'add_new'){
		$("#p_"+v).show();		
	}else{
		$("#p_"+v).hide();
		
		if(pack !=''){	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaShipments/updatePackaging',
				type: "POST",				
				cache: false,			
				data : {packaging:pack,merchant_sku:v },
				success: function(data, textStatus, jqXHR)
				{			 	
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					} 							
				}	
			});  
		}	
	}	 
}
</script>
