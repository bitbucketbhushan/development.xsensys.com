<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
 	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Generate Shipment</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							  
						</div>	
						
					<div class="panel-body no-padding-top bg-white">
						<div class="panel-tools" style="margin-bottom: 10px;clear: right;text-align: center; float: right;">
							<ul class="pagination">
							<?php
							   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
							   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							?>
							</ul>
						</div>
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-sm-2">Master SKU</div>		
								<div class="col-sm-2">Channel SKU</div>		
								<div class="col-sm-3">-</div>
								<div class="col-sm-2">-</div>														
								<div class="col-sm-1">-</div>	
								<div class="col-sm-1">-</div>	
								<div class="col-sm-1">Action</div>	
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  if(count($sku_mapping) > 0) {	 ?>
							<?php foreach( $sku_mapping as $items ) {     ?>
								<div class="row sku" id="r_<?php echo $items['AwsMapping']['seller_sku']; ?>">
									<div class="col-lg-12">	
										<div class="col-sm-2"><?php echo $items['AwsMapping']['master_sku']; ?></div>	
										<div class="col-sm-2"><?php echo $items['AwsMapping']['seller_sku']; ?></div><?php /*?>			
										<div class="col-sm-3"><?php echo $items['AwsMapping']['ship_to'] ?></div>											
										<div class="col-sm-2"><?php echo $items['AwsMapping']['plan_id'] ?></div> 
										<div class="col-sm-1"><?php echo $items['AwsMapping']['total_skus'];  ?> / <?php echo $items['FbaShipment']['total_units']; ?></div>
										<div class="col-sm-1"><?php echo $items['AwsMapping']['pack_list']; ?></div><?php */?>	
										 
									</div>												
								</div>
								 
							<?php }?>
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	

$('#filter_shipment').click(function(){  
	document.getElementById("shipmentFrm").submit();		 
});
	
$(".downloadfeed").click(function(){	
	if($("#category option:selected").val() == 'all'){
		alert("Please Select Category");	
	}else{
		 
		$(".outerOpac").show();		 	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>MapSku/getFeedCsv',
				type: "POST",				
				cache: false,			
				data : {country:$("#country option:selected").val(),listing_type:'inactive_listing'},
				success: function(data, textStatus, jqXHR)
				{	
					$(".outerOpac").hide();		
					jQuery(".msg").html(data.file);
					$('#msg').delay(200000).fadeOut('slow');							
			}		
		 });
	 }
		 
});
$(".filter").click(function(){
	if($("#country option:selected").val() == 'all'){
		alert("Please Select Country");	
	}else{
		window.location.href = '<?php echo Router::url('/', true) ?>MapSku/getInactiveListing?country='+$("#country option:selected").val();
	}
});

$(".searchKey").click(function(){	
	if($(".searchString").val()){
		ajaxSearch();
	}else{
		alert('Enter Search Keyword.');
		event.preventDefault();
	}
		 
});

$(".mapmaster").click(function(){	
if($("#country option:selected").val() == 'all'){
		alert("Please Select Country");	
	}else{
	
		var country = $("#country option:selected").val();
		
		$(".mapmaster i").removeClass('glyphicon glyphicon-upload');
		$(".mapmaster i").addClass('fa fa-spin fa-refresh');
		
		
		country = country == 'GB' ? 'UK' : country;
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>MapSku/getDataAws/CostBreaker_'+country,
			type: "POST",				
			cache: false,			
			//data : { },
			success: function(data, textStatus, jqXHR)
			{	
				
				$(".mapmaster i").removeClass('fa fa-spin fa-refresh');	
				$(".mapmaster i").addClass('glyphicon glyphicon-upload');	
				$(".msg").show();		
				$(".msg").html(data.msg);	
				$('.msg').delay(20000).fadeOut('slow');		
				window.location.href = '<?php echo Router::url('/', true) ?>MapSku/getInactiveListing?country='+$("#country option:selected").val();				
			}		
	 });	
	} 
	 
 });
function ajaxSearch(){	
	 <?php /*?>  $("#mapFrm").attr("action", "<?php echo Router::url('/', true) ?>MapSku/Search");
	   $("#mapFrm").submit();<?php */?>
	   
	$(".outerOpac").show();		 	
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>MapSku/Search',
			type: "POST",				
			cache: false,			
			data : {searchkey:$(".searchString").val()},
			success: function(data, textStatus, jqXHR)
			{	//alert(data.data);	
				$(".outerOpac").hide();		
				//jQuery(".msg").html(data.msg);
				jQuery('.panel-body').html(data.data);
					
				//$('#message').delay(20000).fadeOut('slow');							
		}		
	 });		 
}

 function delSku(key){
	if( confirm('Are you sure want to delete?')){
	$(".outerOpac").show();		 
 	$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>MapSku/Delete',
			type: "POST",				
			cache: false,			
			data : {seller_sku:key},
			success: function(data, textStatus, jqXHR)
			{	//alert(data.data);	
				$(".outerOpac").hide();		
				jQuery(".msg").html(data.msg);
				jQuery("#r_"+key).remove();	
				//$('#message').delay(20000).fadeOut('slow');							
		}		
	 });
	 }
 }
 

</script>
