<style>
.head{ background:#ddd;}
.head div{ padding:5px ; font-weight:bold;}
.body div{ padding:5px ;}
.body div div{ padding:1px ;}
.url{overflow:scroll;}
.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
.body div.sku div{padding:2px 3px; font-size:14px;} 
.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
.txt{float:left;}
.xedit{cursor: pointer;}
.delete{color:#CC0000; cursor:pointer; font-size:12px;}
.separator{border-bottom: 1px solid #ccc; margin-bottom: 10px;}
.normalbox{background-color:#ffffff;}
/* .col-8 {width: 8%; float: left; }
.col-10 {width: 10%; float: left;}
.col-11 {width: 6%; float: left;}
.col-12 { width: 12%; float: left; } */
</style>
<div class="rightside bg-grey-100">
<div class="page-head bg-grey-100">        
        <h1 class="page-title"></h1>
        <div class="panel-title no-radius bg-green-500 color-white no-border"></div>
            <div class="submenu">
                <div class="navbar-header">
                </div>
            </div>
            <div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
            <h4>Set Store Sku</h4>
            <span>Please enter comma separated values for multiple skus.</span>
        </div>
        <div class="container-fluid">							
        <div class="row">  
            <div class="col-lg-12">
                <div class="panel no-border ">
                    <div class="panel-title bg-white no-border">
                    <form method="post" name="setStoreSku" id="setStoreSku" action="./setStoreSku" onSubmit="return setSku()">	
                        <div class="col-sm-12 no-padding">   
                            <div class="col-lg-3 col-lg-offset-2" id="checkboxoption" style="padding-top: 9px;width: 17%;"></div>
                            
                                <div class="col-lg-4" style="width: 24%;"> 
                                    <input class='form-control' type='text' name='skus' id='skus' placeholder='Enter skus' autocomplete='off' />
                                    <span class="text-danger hidden" id="skus-no-error">Please enter skus</span>
                                </div>
                                <div class="col-lg-2"> 
                                    <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Set Store Skus</button>
                                </div> 
                            </div>
                        </div>
                    </form> 	
                </div>	    
            </div>	 
        </div>			
</div> 
<script>
    function setSku() {   
      var submitFlag = true;
      if($("#skus").val()=='') {
        $("#skus-no-error").removeClass('hidden');
        submitFlag = false;
      }

	  return submitFlag;
    }
</script>