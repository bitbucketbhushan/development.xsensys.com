<style>.bulk td {padding:2px 7px 2px 7px;}</style>
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php echo "Dynamic Bulk Despatch Console"; ?></h1>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
			<?php print $this->Session->flash(); ?>
              <div class="panel"> 
						<div class="panel-title">
							<div class="panel-head">Barcode Scan Section</div>
						</div>
					 <div class="col-lg-12 gillete" style="display:none; height: 30px;font-size: 16px; background:red; color: white;"><center><strong>Check Correct Version :- </strong>Fusion Proglide , Fusion 5 Proglide ,Fusion5  ProShield etc..</center></div>
					<div class="panel-body">
					<div class="row">
					    <div class="col-lg-4 col-lg-offset-1">
								<div class="input text"><label for="LinnworksapisBarcode">Barcode</label> <input type="text" id="LinnworksapisBarcode" index="0" value="" class="get_sku_string form-control" name="data[Linnworksapis][barcode]">
								<p class="help-block">Please scan an item to start processing order</p>
								</div>
								
							</div>
							
							<div class="col-lg-4 col-lg-offset-1"><img alt="" title="Scanner" src="/img/bar-code.png"></div>
						</div>
					<div class="outer_bin_addMore">
					</div>	
				</div>
			</div>
			
		</div>  

</div>						
                          
	</div>
	
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
	
<script>

$(function() {
  $("#LinnworksapisBarcode").focus();  
});

$(document).keypress(function(e) {
    if(e.which == 13) {
        submitSanningValue();
    }
});
$(document).keypress(function(e) {
    if(e.which == 13) {
		var url = $('.btn-success').attr('href');
	    window.open(url,'_self');
    }
});


$("body").on("click", ".printbulk", function(){
	var ids = $(this).attr('for');
	$.ajax({
				'url'            : '<?php echo Router::url('/', true); ?>DynamicPicklist/genearateBulkSlipLabel',
				'type'           : 'POST',
				'data'           : { ids : ids },
				'dataType' 		 : 'json',
				'beforeSend'	 : function() {
												$('.outerOpac').show();
											  },
				'success' 		 : function( msgArray )
									{
										$('.outerOpac').hide();
										//location.reload();
										//$('#LinnworksapisBarcode').val('');
										//$('.outer_bin_addMore').html(msgArray.html);
									}  
				}); 
});

$("body").on("click", ".processbulk", function(){
	var ids = $(this).attr('for');
	$.ajax({
				'url'            : '<?php echo Router::url('/', true); ?>DynamicPicklist/processBulkOrders',
				'type'           : 'POST',
				'data'           : { ids : ids },
				'dataType' 		 : 'json',
				'beforeSend'	 : function() {
												//$('.outerOpac').show();
											  },
				'success' 		 : function( msgArray )
									{
										$('.outerOpac').hide();
										if(msgArray == 1){
											swal('Order processed successfully.', '' , 'success');
											location.reload();
										} else {
											swal('There is some error. Please check', '' , 'success');
										}
									}  
				}); 
});


function submitSanningValue()
{
	var Barcode	=	$('#LinnworksapisBarcode').val();
	$('.searchbarcode').text(Barcode);
	$.ajax({
						'url'            : '<?php echo Router::url('/', true); ?>DynamicPicklist/getsearchlist',
						'type'           : 'POST',
						'data'           : { barcode : Barcode },
						'dataType' 		 : 'json',
						'beforeSend'	 : function() {
															$('.outerOpac').show();
														},
						'success' 		 : function( msgArray )
											{
												$('.outerOpac').hide();
												$('#LinnworksapisBarcode').val('');

												$('.outer_bin_addMore').html(msgArray.html);
												var gilval	=	$('.bulk').attr('for');
												if(gilval == 'gillete'){$('.gillete').show();} else { $('.gillete').hide(); }
											}  
						});  
					
}
</script>
