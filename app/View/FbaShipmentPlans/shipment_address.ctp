<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px; }
	.highlight{border:1px #FF0000 solid;background:#FF3300;}
	.notequal{border:1px #993300 solid; background:#CCFF33;}
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>				
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>				
				<h4 style="float:left;">Shipment Address</h4>
				 <br />
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							 
						
					<div class="panel-body no-padding-top bg-white">
					</div>	
					
					 <div class="body" id="results">
						<?php							
                              print $this->form->create( 'Shipment', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/FbaShipmentPlans/SaveShipmentAddress', 'type'=>'post','id'=>'shipment_frm' ) );
                              // pr( $address);
                            ?>
						
						<div class="row sku">
							
							<div class="col-lg-12">	
								<div class="col-sm-1 text-right">Name</div>
								<div class="col-sm-4"><input type="text" name="name" id="name" value="<?php if(isset($address['FbaApiShipmentAddress']['name'])) echo $address['FbaApiShipmentAddress']['name']?>" class="form-control"/></div>
								<div class="col-sm-2 text-right">Address Line 1</div>
								<div class="col-sm-4"><input type="text" name="address_line_1" id="address_line_1" value="<?php if(isset($address['FbaApiShipmentAddress']['address_line_1'])) echo $address['FbaApiShipmentAddress']['address_line_1']?>" class="form-control"></div>
								 
							</div>
							
							<div class="col-lg-12">	
								<div class="col-sm-1 text-right">City</div>
								<div class="col-sm-4"><input type="text" name="city" id="city" value="<?php if(isset($address['FbaApiShipmentAddress']['city'])) echo $address['FbaApiShipmentAddress']['city']?>" class="form-control" /></div>
								<div class="col-sm-2 text-right">Address Line 2</div>
								<div class="col-sm-4"><input type="text"  name="address_line_2" id="address_line_2" value="<?php if(isset($address['FbaApiShipmentAddress']['address_line_2'])) echo $address['FbaApiShipmentAddress']['address_line_2']?>" class="form-control"></div>
							</div>
							
							<div class="col-lg-12">	
								<div class="col-sm-1 text-right">Country Code</div>
								<div class="col-sm-4"><input type="text" name="country_code" id="country_code" value="<?php if(isset($address['FbaApiShipmentAddress']['country_code'])) echo $address['FbaApiShipmentAddress']['country_code']?>" class="form-control" /></div>
								<div class="col-sm-2 text-right">State/ProvinceCode</div>
								<div class="col-sm-4"><input type="text"  name="state_or_province_code" value="<?php if(isset($address['FbaApiShipmentAddress']['state_or_province_code'])) echo $address['FbaApiShipmentAddress']['state_or_province_code']?>"class="form-control"></div>
							</div>
							<div class="col-lg-12">	
								<div class="col-sm-1 text-right">Postal Code</div>
								<div class="col-sm-4"><input type="text" name="postal_code" id="postal_code" value="<?php if(isset($address['FbaApiShipmentAddress']['postal_code'])) echo $address['FbaApiShipmentAddress']['postal_code']?>" class="form-control" /></div>
							 <?php
							 
							  $action = 'Add Address';
							 
							  if(isset($this->request->params['pass'][0])){
								 $action = 'Update Address';
							 ?>
								 <input type="hidden" name="id" value="<?php echo $this->request->params['pass'][0];?>"> 
							 <?php }?>
							</div>
							
							<div class="row">
									<div class="col-lg-12">												
										<div class="col-sm-9">&nbsp;</div>
										<div class="col-sm-2">
										<a href="<?php echo Router::url('/', true) ?>FbaShipmentPlans/ShipmentAddress" class="btn btn-primary btn-sm">Back</a>
										<?php
										echo $this->Form->button( $action, array(
											'type' => 'submit',
											'id' => 'save_address',
											'name' => 'save_address',
											'value' => 'save_address',
											'escape' => true,
											'class'=>'btn btn-info btn-sm'
											 ));	
									?>
										 </div>
										 <div class="col-sm-2">&nbsp;</div>
									</div>
							</div>
						</div>	
							
						</form>													
						 
						<div class="row head">
		 					  <div class="col-sm-12">
								<div class="col-sm-2"><small>Name</small></div>		
								<div class="col-sm-4"><small>Address</small></div>	
								<div class="col-sm-1"><small>City</small></div>
								<div class="col-sm-1"><small>Postal Code</small></div>																													
								<div class="col-sm-2"><small>State/Province Code</small></div>								 
								<div class="col-sm-1"><small>Country Code</small></div>
								<div class="col-sm-1"><small>Action</small></div>	
							</div>								
						</div>	
						 
 						<?php if(count($all_address) > 0){?>
							<?php foreach($all_address as $val){  ?>
								<div class="row sku">
								<div class="col-sm-12">
									<div class="col-sm-2"><?php echo $val['FbaApiShipmentAddress']['name']?></div>		
									<div class="col-sm-4"><?php echo $val['FbaApiShipmentAddress']['address_line_1']?>
									<?php echo '&nbsp;'.$val['FbaApiShipmentAddress']['address_line_2']?></div>	
									<div class="col-sm-1"><?php echo $val['FbaApiShipmentAddress']['city']?></div>
									<div class="col-sm-1"><?php echo $val['FbaApiShipmentAddress']['postal_code']?></div>
									<div class="col-sm-2"><?php echo $val['FbaApiShipmentAddress']['state_or_province_code']?></div>																													
									<div class="col-sm-1"><?php echo $val['FbaApiShipmentAddress']['country_code']?></div>
									<div class="col-sm-1"><small><a href="<?php echo Router::url('/', true) ?>FbaShipmentPlans/ShipmentAddress/<?php echo $val['FbaApiShipmentAddress']['id']; ?>">Edit</a>&nbsp;|&nbsp;<a href="<?php echo Router::url('/', true) ?>FbaShipmentPlans/ShipmentAddressDelete/<?php echo $val['FbaApiShipmentAddress']['id']; ?>">Delete</a><small></div>	
								</div>
								</div>
							<?php }	?>
						<?php }	?>
						<br /><br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
				</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script language="javascript">	

 $("#r3 input[type=number]").each(function(){ 
 
 	var q = $(this).val();
	var id = $(this).attr("id"); 
	var v = id.replace("input_", "");

 	$(".bundle_"+v).each(function(){ 
	 var sp = $(this).attr("for");
	  var bq = parseInt($(this).text()); 
	   $("#span_"+sp).text(bq*q) ;
	   
	   $("#span_"+sp).text(bq+'x'+q+'='+bq*q);

	});
 
 });
 
$(".shipment_items").click(function(){  
     $("#r1").show();
	 $("#r2").show();
	 $("#r3").show();
	 $("#r4").hide();
});

function enterNumber(v){
  	var q = $("#input_"+v).val();
 	$(".bundle_"+v).each(function(){ 
	 var sp = $(this).attr("for");
	  var bq = $(this).text();
	   $("#span_"+sp).text(bq+'x'+q+'='+bq*q);

	});
 
  var id = 'input_' + v;
  var l_value = $('#l_' + v).text(); 
  var e = document.getElementById(id);
 
	if (!/^[0-9]+$/.test(e.value)) 
	{ 	
		alert("Please enter only number.");
	 	$("#"+id).addClass("highlight");
		e.value = e.value.substring(0,e.value.length-1);
	}else{
        $("#"+id).removeClass("highlight");		
		
		$("#slip_"+v).css('visibility', 'hidden');		
		$("#label_"+v).css('visibility', 'hidden');
				 		
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaShipments/SaveExportShipmentAjax',
			type: "POST",				
			cache: false,			
			data : {id:v,shipped_qty:$('#input_'+v).val(),fnsku:$('#fnsku_'+v).val(),merchant_sku:$('#merchant_sku_'+v).val(),master_sku:$('#master_sku_'+v).val(),prep_qty:$('#prep_qty_'+v).val(), items_inc_id:$('#items_inc_id_'+v).val() ,shipment_id:$('#shipment_id').val(),shipment_inc_id:$('#shipment_inc_id').val(),shipments_po_inc_id:$('#shipments_po_inc_id').val(),shipment_name:$('#shipment_name').val()},
			success: function(data, textStatus, jqXHR)
			{			
				$("#slip_"+v).css('visibility', 'visible');		
				$("#label_"+v).css('visibility', 'visible'); 
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				}	
											
		} 
	}); 
		
   }	
}    

function updatePdf(v){	
		$("#slip_"+v).css('visibility', 'hidden');		
		$("#label_"+v).css('visibility', 'hidden');		 
		$("#upd_"+v+" i").addClass('fa-spin');	
			
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaShipments/SaveExportShipmentAjax',
			type: "POST",				
			cache: false,			
			data : {id:v,shipped_qty:$('#input_'+v).val(),fnsku:$('#fnsku_'+v).val(),merchant_sku:$('#merchant_sku_'+v).val(),master_sku:$('#master_sku_'+v).val(),prep_qty:$('#prep_qty_'+v).val(), items_inc_id:$('#items_inc_id_'+v).val() ,shipment_id:$('#shipment_id').val(),shipment_inc_id:$('#shipment_inc_id').val(),shipments_po_inc_id:$('#shipments_po_inc_id').val(),shipment_name:$('#shipment_name').val()},
			success: function(data, textStatus, jqXHR)
			{			
				$("#slip_"+v).css('visibility', 'visible');		
				$("#label_"+v).css('visibility', 'visible');	
				$("#upd_"+v+" i").removeClass('fa-spin');	
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				}	
											
		} 
	}); 
} 

function printLabel(v){	
	var qty = $('#input_'+v).val();
	var label_qty = parseInt($('#l_'+v).text());  
	if(qty < 1){
		alert('Please enter quantity.');
		$('#input_'+v).focus();
	}else{

		if(confirm("Are going to print #"+qty+" quantity?")){
			$("#label_"+v+" i").removeClass('fa-print');	
			$("#label_"+v+" i").addClass('fa-spin fa-refresh');	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaShipments/printLabel',
				type: "POST",				
				cache: false,			
				data : {id:v, qty:qty},
				success: function(data, textStatus, jqXHR)
				{	
					$("#label_"+v+" i").removeClass('fa-spin fa-refresh');	
					$("#label_"+v+" i").addClass('fa-print');	
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					}	
						 						
				} 
			});
		}	
	 }	 
}

function printSlip(v){	
	var qty = $('#input_'+v).val();
	$("#slip_"+v+" i").removeClass('fa-print');	
	$("#slip_"+v+" i").addClass('fa-spin fa-refresh');	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaShipments/printSlip',
		type: "POST",				
		cache: false,			
		data : {id:v,qty:qty},
		success: function(data, textStatus, jqXHR)
		{	
			$("#slip_"+v+" i").removeClass('fa-spin fa-refresh');	
			$("#slip_"+v+" i").addClass('fa-print');	
			console.log(data.msg);		
			if(data.error){
				alert(data.msg);
			} 								
		}		
 	});  		 
}

function updatePackaging(t,v){	
	 
	var pack = $(t).find("option:selected").val() ;
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaShipments/updatePackaging',
		type: "POST",				
		cache: false,			
		data : {packaging:pack,merchant_sku:v },
		success: function(data, textStatus, jqXHR)
		{			 	
			console.log(data.msg);	
			if(data.error){
				alert(data.msg);
			} 							
		}	
 	});  		 
}

</script>
