<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php //print $role;?></h1>
        <div class="panel-title no-radius bg-green-500 color-white no-border"></div>
        
				<div class="submenu">
					<div class="navbar-header">
						<!--<ul class="nav navbar-nav pull-left">
							<li>
								<a class="switch btn btn-success" href="/JijGroup/showallpostalprovider" data-toggle="modal" data-target=""><i class=""></i> Show Postal Provider </a>
							</li>
							<li>
								<a class="switch btn btn-success" href="/JijGroup/showallmatrix" data-toggle="modal" data-target=""><i class=""></i> Show Postal Service </a>
							</li>
						</ul>-->
					</div>
				</div>
					<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		</div>
		<div class="container-fluid">
				<div class="row">
				<?php
					print $this->form->create( 'Company', array( 'class'=>'form-horizontal', 'url' => '/Companies/addCompany', 'type'=>'post','id'=>'deleverymatrix' ) );
					print $this->form->input( 'Company.id', array( 'type'=>'hidden') );
				?>
            <div class="col-lg-12 warehouseDetails">
              <div class="panel">
						<div class="panel-title">
							<div class="panel-head"><?php echo $title; ?></div>
						</div>
					<div class="panel-body">
                    <div class="row">
								<div class="col-lg-12">
									  <div class="form-group" >
										<label for="username" class="control-label col-lg-3">Company Name</label>                                        
										<div class="col-lg-6">                                            
										   <?php
												print $this->form->input( 'Company.company_name', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
										   ?> 
										</div>
									  </div>
									  
									    
								  <div class="form-group" >
									<label for="Company.RegisteredAddress" class="control-label col-lg-3">Company Registered Address</label>                                        
									<div class="col-lg-6">                                            
									   <?php
											print $this->form->input( 'Company.registered_address', array( 'type'=>'textarea', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'rows'=>2) );
									   ?> 
									</div>
								  </div>
								  
								  <div class="form-group" >
									<label for="Company.Cin" class="control-label col-lg-3">Company CIN</label>                                        
									<div class="col-lg-6">                                            
									   <?php
											print $this->form->input( 'Company.cin', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
									   ?> 
									</div>
								  </div>
 								   <div class="form-group" >
									<label for="Company.OperationalAddress" class="control-label col-lg-3">Company Operational address</label>                                        
									<div class="col-lg-6">                                            
									   <?php
											print $this->form->input( 'Company.operational_address', array( 'type'=>'textarea', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'rows'=>2) );
									   ?> 
									</div>
								  </div>
								  
								  <div class="form-group" >
									<label for="Company.PhoneNo" class="control-label col-lg-3">Company Phone no</label>                                        
									<div class="col-lg-6">                                            
									   <?php
											print $this->form->input( 'Company.phone_no', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
									   ?> 
									</div>
								  </div>
								    <div class="form-group" >
									<label for="Company.Email" class="control-label col-lg-3">Company Email</label>                                        
									<div class="col-lg-6">                                            
									   <?php
											print $this->form->input( 'Company.email', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
									   ?> 
									</div>
								  </div>
								  
							 </div>
						</div>
						
						<div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100" style="clear:both">                                                                            
                               	<?php
									echo $this->Form->button( $title , array(
										'type' => 'submit',
										'escape' => true,
										'class'=>'add_attribute btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
										));	
								?>
						</div>
					</div>
				</div>	
			</form>
	    </div>
	</div>        
</div>
	


