<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"><?php print $title;?></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
    </div>
    <div class="container-fluid">
		<div class="row">
			            <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">									
									<div class="panel-tools">																	
									</div>
								</div>
							    <div class="panel-body no-padding-top bg-white">											
									<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Id: activate to sort column descending">S.No.</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Warehouse Name: activate to sort column descending">Company Name</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Warehouse Name: activate to sort column descending">Store Name</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="County Name: activate to sort column ascending">Action</th>
										</thead>
									<tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <?php $i = 1; foreach( $showStoreDetails as $showStoreDetail ) { 
													foreach( $showStoreDetail['Store'] as $store ) 
													 {
														 $status = ($store['status'] == 0 ) ? 'alert-danger' : '';
										?>
                                            <tr class="odd <?php echo $status; ?> ">
												<td class="sorting_1"><?php print $i; ?></td>
												<td class="sorting_1"><?php print $showStoreDetail['Company']['company_name']; ?></td>                                                
												<td class="sorting_1">
													<?php
													 
														print $store['store_name']; 
													
													?>
												</td>                                                
											    <td class="sorting_1">
													<ul id="icons" class="iconButtons">
														<li>
															<a href="/companies/editStore/<?php print $store['id']; ?>" class="btn btn-success btn-xs margin-right-10" title="Edit Company" ><i class="fa fa-pencil"></i></a>
														</li>
														<li>
															<?php if( $store['status'] == 1 ) { ?>
																<a href="javascript:void(0);" for="<?php print $store['id']; ?>/active" class="activeDeactiveStore btn btn-info btn-xs margin-right-10" title="" ><i class="fa fa-unlock"></i></a>
															<?php } else { ?>
																<a href="javascript:void(0);" for="<?php print $store['id']; ?>/deactive" class="activeDeactiveStore btn btn-info btn-xs margin-right-10" title="" ><i class="fa fa-lock"></i></a>
															<?php } ?>
														</li>
													</ul>
                                                </td>
                                            </tr>
                                            <?php $i++; } } ?>
                                    </tbody>
									</table>		
								</div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
				<!-- BEGIN FOOTER -->
				<?php echo $this->element('footer'); ?>
				<!-- END FOOTER -->
            </div>
    </div>
</div>

<div class="outerOpac" style="display:none">
	<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; "></span>
	<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>


<script>
$( "body" ).on( "click", ".activeDeactiveStore", function(){
	
				var attValue = $( this ).attr( 'for' )
				var id = attValue.split("/"); 
				$.ajax(
				{
					'url'            : '/companies/activeDeactiveStore',
					'type'           : 'POST',
					'data'           : { id : id[0], status : id[1] },
					'beforeSend'	 : function() 
												{
													$( ".outerOpac" ).attr( 'style' , 'display:block');
												}, 	
					'success' 		 : function( msgArray )
										{
											if(msgArray == 1)
											{
												$( ".outerOpac" ).attr( 'style' , 'display:none');
												location.reload();
											}
											if(msgArray == 2)
											{
												$( ".outerOpac" ).attr( 'style' , 'display:none');
												location.reload();
											}
										}  
								  
				});            
	});

</script>

