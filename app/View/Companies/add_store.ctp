<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php //print $role;?></h1>
        <div class="panel-title no-radius bg-green-500 color-white no-border"></div>
        	<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		</div>
		<div class="container-fluid">
				<div class="row">
				<?php
					print $this->form->create( 'Store', array( 'class'=>'form-horizontal', 'url' => '/Companies/addStore', 'type'=>'post' ) );
					print $this->form->input( 'Store.id', array( 'type'=>'hidden' ) );
				?>
            <div class="col-lg-12 warehouseDetails">
              <div class="panel">
						<div class="panel-title">
							<div class="panel-head"><?php echo $title; ?></div>
						</div>
					<div class="panel-body">
                    <div class="row">
						<div class="col-lg-12">
						 	   <div class="form-group">
								  <label class="col-sm-3 control-label">Company</label>
									<div class="col-sm-6">                                               
										<?php
											if( count( $companyList ) > 0 )
											print $this->form->input( 'Store.company_id', array( 'type'=>'select', 'empty'=>'Choose Company','options'=>$companyList,'class'=>'form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
										?>  
									</div>
							  </div>
						  
							  <div class="form-group" >
								<label for="username" class="control-label col-lg-3">Store Name</label>                                        
									<div class="col-lg-6">                                            
									   <?php
											print $this->form->input( 'Store.store_name', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
									   ?> 
									</div>
								</div>
								
								<div class="form-group" >
								<label for="username" class="control-label col-lg-3">Store Alias</label>                                        
									<div class="col-lg-6">                                            
									   <?php
											print $this->form->input( 'Store.store_alias', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
									   ?> 
									</div>
								</div>
								
						   </div>
						</div>
						
						<div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100" style="clear:both">                                                                            
                              <?php
								echo $this->Form->button( $title , array(
									'type' => 'submit',
									'escape' => true,
									'class'=>'add_attribute btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
									));	
							  ?>
						</div>
					</div>
				</div>	
			</form>
	    </div>
	</div>        
</div>
	


