<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php //print $role;?></h1>
        <div class="panel-title no-radius bg-green-500 color-white no-border"></div>
        	<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		</div>
		<div class="container-fluid">
				<div class="row">
				<?php
					print $this->form->create( 'Company', array( 'class'=>'form-horizontal', 'url' => '/Companies/addLocation/'. $this->params['pass'][0], 'type'=>'post','id'=>'deleverymatrix' , 'enctype'=>"multipart/form-data" ) );
					 
				?>
            <div class="col-lg-12 warehouseDetails">
              <div class="panel">
						<div class="panel-title">
							<div class="panel-head"><?php echo $title; ?></div>
						</div>
					<div class="panel-body">
                    <div class="row">
							<div class="col-lg-12">
								  <div class="form-group" >
									<label for="username" class="control-label col-lg-3">Store Name</label>                                        
									<div class="col-lg-6">  
									<input type="hidden" name="CompanyLocation[company_id]" value="<?php echo $this->params['pass'][0];?>" />
									                                         
									   <?php
											//print $this->form->input( 'CompanyLocation.company_id', array( 'type'=>'hidden') );
											print $this->form->input( 'CompanyLocation.store_name', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
									   ?> 
									</div>
								  </div>

								  
								  <div class="form-group" >
									<label for="Company.CountryName" class="control-label col-lg-3">Country Name</label>                                        
									<div class="col-lg-6">  
 									 <select name="data[CompanyLocation][country_name]" class="form-control" id="country_name" onchange="getIso();">    
 									   <?php 
									   foreach($country as $c){
											 echo  '<option value="'.$c['Country']['name'].'" iso="'.$c['Country']['iso_2'].'">'.$c['Country']['name'].'</option>  ';
									   }
 									   ?> 
									   </select>   
 									</div>
								  </div>
								  
								  <div class="form-group" >
									<label for="CompanyLocation.Cin" class="control-label col-lg-3">Country ISO Code</label>                                        
									<div class="col-lg-6">                                            
									   <?php
											print $this->form->input( 'CompanyLocation.country_iso_code', array( 'type'=>'text','disabled'=>false, 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
									   ?> 
									</div>
								  </div>
 								   <div class="form-group" >
									<label for="CompanyLocation.OperationalAddress" class="control-label col-lg-3">VAT Number</label> 
									<div class="col-lg-6">                                            
									   <?php
											print $this->form->input( 'CompanyLocation.vat_number', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
									   ?> 
									</div>
								  </div>
								  
								  <div class="form-group" >
									<label for="CompanyLocation.PhoneNo" class="control-label col-lg-3">VAT Filling Frequency</label>
									<div class="col-lg-6"> 									   
									   <?php $options = ['monthly'=>'Monthly', 'quarterly'=>'Quarterly' ,'yearly'=>'Yearly'];
									   echo $this->Form->input('CompanyLocation.vat_filling_frequency', array('type'=>'select', 'label'=>false, 'options'=>$options, 'class'=>'form-control'));	   
											//print $this->form->input( 'CompanyLocation.vat_filling_frequency', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
									   ?> 
									</div>
								  </div>
								  
								   <div class="form-group" >
									<label for="CompanyLocation.OperationalAddress" class="control-label col-lg-3">End month of first filling period</label>
									<div class="col-lg-6">                                            
									   <?php
											print $this->form->input( 'CompanyLocation.end_month_of_first', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
									   ?> 
									</div>
								  </div>
								   	 		
								
								<div class="form-group" >
									<label class="control-label  col-lg-3">VAT Certificate</label>								
									<div class="input-group col-lg-6">
									<span class="input-group-btn">
										<span class="btn btn-primary btn-file">
											Browse Certificate
											<?php
												print $this->form->input( 'CompanyLocation.certificate', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'', 'required'=>false) );
												
											 ?>
 											  <!--<input type="file" class="form-control" id="vat_certificate" name="vat_certificate"/>-->
 									
										</span>
									</span>
									<input type="text" placeholder="No file selected" readonly="" class="form-control">
									</div>
									  
								</div> 
							 
								 
								  
								  
						 </div>
						</div>
						
						<div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100" style="clear:both">                                                                            
                               	<?php
									echo $this->Form->button( $title , array(
										'type' => 'submit',
										'escape' => true,
										'class'=>'add_attribute btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
										));	
								?>
						</div>
					</div>
				</div>	
			</form>
	    </div>
	</div>        
</div>
	
<script>
function getIso(){ 
	$("#CompanyLocationCountryIsoCode").val($("#country_name option:selected").attr('iso'));
}
  getIso();
</script>

