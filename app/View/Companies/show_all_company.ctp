<style>
#icons li{width:26px; margin-bottom:5px;}
.iconButtons li{padding:0px 10px 0px 0px !important }
</style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"><?php print $title;?></h1>
			<div class="submenu">
					<div class="navbar-header">
					<a href="<?php echo Router::url('/', true)?>Manage/Company/addNewComapany" class="btn btn-success btn-sm" title="Add Company">Add Company</a>
					</div>
				</div>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
    </div>
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">									
									<div class="panel-tools">																	
									</div>
								</div>
							    <div class="panel-body no-padding-top bg-white">											
									<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th class="sorting_asc">Company Name</th>
 												<th class="sorting_asc">Registered Address</th>
												<th class="sorting_asc">Company CIN</th>
												<th class="sorting_asc">Operational Address</th>
												<th class="sorting_asc">Company Phone No</th>
												<th class="sorting_asc">Company Email</th>
												<th class="sorting_asc" style="width:10%">Action</th>
										</thead>
									<tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <?php $i = 1; foreach( $allCompanyDetails as $allCompanyDetail) {
										$status = ($allCompanyDetail['Company']['status'] == 0 ) ? 'alert-danger' : '';
										 ?>
                                            <tr class="odd <?php echo $status; ?> "  >
												<!--<td class="sorting_1"><?php print $i; ?></td>-->
												<td class="sorting_1"><a href="<?php echo Router::url('/', true)?>Companies/Location/<?php print $allCompanyDetail['Company']['id']; ?>"   title="Add Company Locations" style="color:#0000CC; text-decoration:underline;" ><?php print $allCompanyDetail['Company']['company_name']; ?></a></td>
												<td class="sorting_1"><?php print $allCompanyDetail['Company']['registered_address']; ?></td>
												<td class="sorting_1"><?php print $allCompanyDetail['Company']['cin']; ?></td>
												<td class="sorting_1"><?php print $allCompanyDetail['Company']['operational_address']; ?></td>
												<td class="sorting_1"><?php print $allCompanyDetail['Company']['phone_no']; ?></td>
												<td class="sorting_1"><?php print $allCompanyDetail['Company']['email']; ?></td> 
												                                                
											    <td class="sorting_1"  style="width:10%">
													<ul id="icons" class="iconButtons">
														<li>
															<a href="/companies/editCompany/<?php print $allCompanyDetail['Company']['id']; ?>" class="btn btn-success btn-xs margin-right-10" title="Edit Company" ><i class="fa fa-pencil"></i></a>
														</li>
														<li>
															<?php if( $allCompanyDetail['Company']['status'] == 1 ) { ?>
																<a href="javascript:void(0);" for="<?php print $allCompanyDetail['Company']['id']; ?>/active" class="deleteCompany btn btn-info btn-xs margin-right-10" title="" ><i class="fa fa-unlock"></i></a>
															<?php } else { ?>
																<a href="javascript:void(0);" for="<?php print $allCompanyDetail['Company']['id']; ?>/deactive" class="deleteCompany btn btn-info btn-xs margin-right-10" title="" ><i class="fa fa-lock"></i></a>
															<?php } ?>
														</li>
														
														 
													</ul><a href="/Channels/index/<?php print $allCompanyDetail['Company']['id']; ?>" class="btn btn-success btn-xs margin-right-10" title="Edit Company" >View Channels</i></a>
                                                </td>
                                            </tr>
                                            <?php $i++; } ?>
                                    </tbody>
									</table>		
								</div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
				<!-- BEGIN FOOTER -->
				<?php echo $this->element('footer'); ?>
				<!-- END FOOTER -->
            </div>
    </div>
</div>

<div class="outerOpac" style="display:none">
	<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; "></span>
	<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

<script>
$( "body" ).on( "click", ".deleteCompany", function(){
	
				var attValue = $( this ).attr( 'for' )
				var id = attValue.split("/"); 
				$.ajax(
				{
					'url'            : '/companies/activeDeactiveCompany',
					'type'           : 'POST',
					'data'           : { id : id[0], status : id[1] },
					'beforeSend'	 : function() 
												{
													$( ".outerOpac" ).attr( 'style' , 'display:block');
												}, 	
					'success' 		 : function( msgArray )
										{
											if(msgArray == 1)
											{
												$( ".outerOpac" ).attr( 'style' , 'display:none');
												location.reload();
											}
											if(msgArray == 2)
											{
												$( ".outerOpac" ).attr( 'style' , 'display:none');
												location.reload();
											}
										}  
								  
				});            
	});

</script>



