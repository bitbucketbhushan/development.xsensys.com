<style>
#icons li{width:26px; margin-bottom:5px;}
.iconButtons li{padding:0px 10px 0px 0px !important }
</style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"><?php print $title;  ?></h1>
			<div class="submenu">
					<div class="navbar-header">
					<a href="<?php echo Router::url('/', true)?>Companies/addLocation/<?php echo $this->params['pass'][0];?>" class="btn btn-success btn-sm" title="Company Add Location">Add Location</a>
					</div>
				</div>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
 			 
    </div>
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">									
									<div class="panel-tools">																	
									</div>
								</div>
							    <div class="panel-body no-padding-top bg-white">											
									<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th class="sorting_asc">Store Name</th>
 												<th class="sorting_asc">Country Name</th>
												<th class="sorting_asc">Country ISO Code</th>
												<th class="sorting_asc">VAT Number</th>
												<th class="sorting_asc">VAT Filling Frequency</th>
												<th class="sorting_asc">End month of first filling period</th>
												<th class="sorting_asc">VAT Certificate</th>
												<th class="sorting_asc">Action</th>
										</thead>
									<tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <?php   foreach( $allCompanyDetails as $allCompanyDetail) {
 										 ?>
                                            <tr class=""  >
 												<td class="sorting_1"><?php print $allCompanyDetail['CompanyLocation']['store_name']; ?></td>
												<td class="sorting_1"><?php print $allCompanyDetail['CompanyLocation']['country_name']; ?></td>
												<td class="sorting_1"><?php print $allCompanyDetail['CompanyLocation']['country_iso_code']; ?></td>
												<td class="sorting_1"><?php print $allCompanyDetail['CompanyLocation']['vat_number']; ?></td>
												<td class="sorting_1"><?php print $allCompanyDetail['CompanyLocation']['vat_filling_frequency']; ?></td>
												<td class="sorting_1"><?php print $allCompanyDetail['CompanyLocation']['end_month_of_first']; ?></td> 
												<td class="sorting_1"><?php  
												 
												if(!empty($allCompanyDetail['CompanyLocation']['vat_certificate']) && file_exists( WWW_ROOT. 'files'.DS.$allCompanyDetail['CompanyLocation']['vat_certificate'])){
 									  				echo ' <a href="'.Router::url('/', true).'files/'.$allCompanyDetail['CompanyLocation']['vat_certificate'].'" style="color:#0033FF" target="_blank">Download Certificate</a>';
													}
 									   			?></td> 
												                                                
											    <td class="sorting_1">
													<ul id="icons" class="iconButtons" style="width:107%">
														<li>
															<a href="/companies/editLocation/<?php print $allCompanyDetail['CompanyLocation']['id']; ?>" class="btn btn-success btn-xs margin-right-10" title="Edit Location"><i class="fa fa-pencil"></i></a>
														</li>
														
														 
													</ul>
                                                </td>
                                            </tr>
                                            <?php  } ?>
                                    </tbody>
									</table>		
								</div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
				<!-- BEGIN FOOTER -->
				<?php echo $this->element('footer'); ?>
				<!-- END FOOTER -->
            </div>
    </div>
</div>

<div class="outerOpac" style="display:none">
	<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; "></span>
	<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

<script>
$( "body" ).on( "click", ".deleteCompany", function(){
	
				var attValue = $( this ).attr( 'for' )
				var id = attValue.split("/"); 
				$.ajax(
				{
					'url'            : '/companies/activeDeactiveCompany',
					'type'           : 'POST',
					'data'           : { id : id[0], status : id[1] },
					'beforeSend'	 : function() 
												{
													$( ".outerOpac" ).attr( 'style' , 'display:block');
												}, 	
					'success' 		 : function( msgArray )
										{
											if(msgArray == 1)
											{
												$( ".outerOpac" ).attr( 'style' , 'display:none');
												location.reload();
											}
											if(msgArray == 2)
											{
												$( ".outerOpac" ).attr( 'style' , 'display:none');
												location.reload();
											}
										}  
								  
				});            
	});

</script>



