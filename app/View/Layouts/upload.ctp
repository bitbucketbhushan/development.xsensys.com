<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<!-- Mirrored from demo.yakuzi.eu/maniac/1.2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 May 2015 06:14:06 GMT -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>	
	<title>WMS - Dashboard</title>
	
	<!-- BEGIN CORE FRAMEWORK -->
        <?php print $this->html->css(array('plugins/bootstrap-treeview/bootstrap-treeview','plugins/bootstrap/css/bootstrap.min','plugins/ionicons/css/ionicons.min','plugins/font-awesome/css/font-awesome.min','plugins/bootstrap-select/css/bootstrap-select.min','plugins/jquery-easyAccordion/css/jquery.hash-tabs','plugins/bootstrapValidator/bootstrapValidator.min', 'plugins/bootstrap-verticaltabs/bootstrap.vertical-tabs', 'plugins/smoothDivScroll/css/smoothDivScroll' ,'lobibox', 'plugins/datepicker/css/datepicker')); ?>
	<!-- END CORE FRAMEWORK -->
	
	<!-- BEGIN PLUGIN STYLES -->
        <?php print $this->html->css(array('plugins/animate/animate','plugins/queryLoader/queryLoader','plugins/datatables/dataTables.bootstrap','plugins/bootstrap-slider/css/slider','plugins/iCheck/skins/all','plugins/rickshaw/rickshaw.min','plugins/jquery-jvectormap/jquery-jvectormap-1.2.2','plugins/bootstrap-daterangepicker/daterangepicker-bs3')); ?>	
	<!-- END PLUGIN STYLES -->
	
	<!-- BEGIN SweetAlert Box STYLES -->
        <?php print $this->html->css(array('plugins/dist/sweetalert')); ?>	
	<!-- END PLUGIN STYLES -->
	
	<!-- BEGIN THEME STYLES -->
        <?php print $this->html->css(array('material','style','plugins','helpers','responsive','autoSuggest')); ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>	
	<!-- END THEME STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-leftside fixed-header sidebar-sm">
	<!-- BEGIN HEADER -->
        <?php 			  
			 $authDetails = $this->session->read('Auth.User'); 
			 $usernameText = $authDetails['username'];
			 $roleType = $authDetails['role_type'];	
			  
			  if( $roleType == '2' )
			  {
					print $this->element( 'header' ); 
			  }
			  else
			  {
					print $this->element( 'header_user' ); 
			  } 

 		?>
	<!-- END HEADER -->
		 
	<div class="wrapper">
		<!-- BEGIN LEFTSIDE -->
            <?php //print $this->element( 'side_menu' ); ?>
            
            <?php print $this->element( 'side_new_menu' ); ?>
		<!-- END LEFTSIDE -->

		<!-- BEGIN RIGHTSIDE -->
            <?php print $this->fetch( 'content' ); ?>
        <!-- /.wrapper -->
	<!-- END CONTENT -->
		
		
	<!-- BEGIN JAVASCRIPTS -->
	
	<!-- BEGIN CORE PLUGINS -->
        <?php print $this->html->script(array('plugins/bootstrap-treeview/bootstrap-treeview','plugins/jquery-1.11.1.min','plugins/queryLoader/queryLoader','plugins/iCheck/icheck.min','plugins/bootstrap/js/bootstrap.min','plugins/bootstrap/js/holder','plugins/pace/pace.min','plugins/slimScroll/jquery.slimscroll.min','plugins/bootstrap-select/js/bootstrap-select.min',  'plugins/jquery-easyAccordion/js/jquery.hash-tabs','plugins/bootstrapValidator/bootstrapValidator.min','plugins/bootstrap-modal-popover/bootstrap-modal-popover','plugins/smoothDivScroll/js/jquery-ui-1.10.3.custom.min','plugins/smoothDivScroll/js/jquery.mousewheel.min','plugins/smoothDivScroll/js/jquery.kinetic.min','plugins/smoothDivScroll/js/jquery.smoothdivscroll-1.3-min' ,'core','custom','custom_datatable', 'plugins/datepicker/js/bootstrap-datepicker')); ?>
	<!-- END CORE PLUGINS -->
	
	<!-- flot chart  
    <?php print $this->html->script(array('plugins/flot/jquery.flot.min','plugins/flot/jquery.flot.grow','plugins/flot/jquery.flot.resize.min')); ?> -->

	<!-- sparkline  
    <?php print $this->html->script(array('plugins/sparkline/jquery.sparkline.min')); ?> -->
	
	<!-- bootstrap slider 
    <?php print $this->html->script(array('plugins/bootstrap-slider/js/bootstrap-slider')); ?>-->
	
	<!-- datepicker -->
    <?php print $this->html->script(array('plugins/bootstrap-daterangepicker/moment.min','plugins/bootstrap-daterangepicker/daterangepicker')); ?>
	
	<!-- vectormap  
    <?php print $this->html->script(array('plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min','plugins/jquery-jvectormap/jquery-jvectormap-europe-merc-en')); ?>-->
	
	<!-- counter  
    <?php print $this->html->script(array('plugins/jquery-countTo/jquery.countTo')); ?>-->
	
	<!-- rickshaw  
        <?php print $this->html->script(array('plugins/rickshaw/vendor/d3.v3','plugins/rickshaw/rickshaw.min')); ?>-->
	
	<!-- datatables -->
	<?php print $this->html->script(array('plugins/datatables/jquery.dataTables','plugins/datatables/dataTables.bootstrap')); ?>
	
	
	<!-- maniac -->
        <?php print $this->html->script(array('maniac')); ?>

	<!--inputmask -->
    <?php print $this->html->script(array('plugins/input-mask/jquery.inputmask','plugins/input-mask/jquery.inputmask.date.extensions','plugins/input-mask/jquery.inputmask.numeric.extensions')); ?>
	
	<!-- SweetAlert Customization -->
	<?php print $this->html->script(array('plugins/dist/sweetalert.min' , 'plugins/mousee')); ?>
	
	<!-- SweetAlert Customization -->
	<?php print $this->html->script(array('lobibox')); ?>
	
	<!-- dashboard -->
	<script type="text/javascript">
		//maniac.loadchart();
		//maniac.loadvectormap();
		//maniac.loadbsslider();
		//maniac.loadrickshaw();
		//maniac.loadcounter();
		//maniac.loadprogress();
		maniac.loaddaterangepicker();
		
		/* Call custom datatable js */
		maniac.loadinputmask();
		$('#PoAdvisedDate').datepicker({
				format: "dd/mm/yyyy"
			});
	</script> 
<script>
$(document).ready(function(){
    $('#popup-scroller1').slimScroll({
        height: '450px',
		width:'100%',
		alwaysVisible: true
    });
 $('#popup-scroller2').slimScroll({
        height: '450px',
		width:'100%',
		alwaysVisible: true
    });
  $('#popup-scroller3').slimScroll({
        height: '300px',
		width:'100%',
		alwaysVisible: true
    });
    $('#popup-scroller5').slimScroll({
        height: '300px',
		wheelStep: 1,
		animate: true,
		width:'100%',
		alwaysVisible: true
    });
	   
    $('.attr_show').slimScroll({
        height: '485px',
		width:'100%',
		size:'3',
		alwaysVisible: false
    });
	
	$('.sortingSerivesLeft>.panel-body').slimScroll({
        height: '430px',
		width:'100%',
		size:'3',
		alwaysVisible: true
    });
	
	$('.sortingSerivesRight>.panel-body').slimScroll({
        height: '430px',
		width:'100%',
		size:'3',
		alwaysVisible: true
    });
  
	/* Call datatable */
	$('table.display').dataTable();
	
	/*window.setTimeout(function() {
    $(".alert").fadeTo(1500, 0).slideUp(500, function(){
 		$(this).remove(); 
    });
}, 90000);*/
 
 
 $(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});


    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
	
	$('input').iCheck({
			handle: 'radio',
			radioClass: 'iradio_square-green'
		});
		
	$('input').iCheck({
			handle: 'checkbox',
			checkboxClass: 'icheckbox_square-green'
		});

});


/* $(document).on('show','.accordion', function (e) {
         //$('.accordion-heading i').toggleClass(' ');
         $(e.target).prev('.accordion-heading').addClass('accordion-opened');
    });
    
    $(document).on('hide','.accordion', function (e) {
        $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
        //$('.accordion-heading i').toggleClass('fa-chevron-right fa-chevron-down');
    });

	
*/
</script>
<script>
 
$(function(){
 
	$(document).on( 'scroll', function(){
 
		if ($(window).scrollTop() > 100) {
			$('.scroll-top-wrapper').addClass('show');
		} else {
			$('.scroll-top-wrapper').removeClass('show');
		}
	});
 
	$('.scroll-top-wrapper').on('click', scrollToTop);
});
 
function scrollToTop() {
	verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
	element = $('body');
	offset = element.offset();
	offsetTop = offset.top;
	$('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
}
</script>
	<!-- END JAVASCRIPTS -->
	
	<div class="scroll-top-wrapper ">
	<span class="scroll-top-inner">
		<i class="fa fa-2x fa-arrow-circle-up"></i>
	</span>
</div>

<script>
  //QueryLoader.selectorPreload = "body";
  //QueryLoader.init();
</script>
 
<script type="text/javascript">
		maniac.loaddatatables();
	</script>	

  
  <script>

	$(document).ready(function()
	{
		
		Mousetrap.bind('ctrl+shift+f7', function(e)
		{
			
			// manipultaion here
			$.ajax(
			{
				url     : '<?php echo Router::url('/', true); ?>' + '/products/globalSearch',
				type    : 'POST',
				data    : {},
				success :	function( msgArray  )
				{
					$( 'div.global_search' ).html( msgArray );						
				}                
			}); 
			
		});
		
		//delete order globally
		$("body").on("click", ".deleteOpenOrder", function()
        {			
			var id 				=	$(this).attr("for");
			swal({
					title: "Are you sure?",
					text: "You want to delete open order !",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						$.ajax({
							
								'url' 					: 	'/Linnworksapis/deleteOrder',
								'type'					: 	'POST',
								'data' 					: 	{ openorderid : id},
								'beforeSend'			: function() {
																			$('.loading-image_'+id).show();
																		 },
								'success' 				  : function( msgArray )
									{
										if ( msgArray == 1 )
										{
											$('.close').click();
											swal("Open order delete successfully.", "" , "success");											
											$('.loading-image_'+id).hide();
											$('#forRemove_'+id ).remove();
											location.reload();
										}
										if( msgArray == 2 )
										{
											$('.close').click();
											alert('There is an error.');
										}
									} 
								
							});
					}
					else
					{
						$('.close').click();
						swal("Cancelled", "Your order is safe :)", "error");
					}
			});
		});
		
		//camcel custom bath order
		//cancel order
		$("body").on("click", ".cancelOpenOrder", function()
		{			
			var id 				=	$(this).attr("for");
			var refundNote		=	$("#refund_notes_"+id). val();
			var refundamount	=	$("#refund_amount_"+id). val();
			var warehohuseId	=	$("#warehouse_id_"+id). val();
			
			
			swal({
					title: "Are you sure?",
					text: "You want to cancel open order !",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, cancel it!",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: true,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						$.ajax({
								'url' 					: 	'/Linnworksapis/cancelOrderNotePopup',
								'type'					: 	'POST',
								'data' 					: 	{ orderid : id},
								'success' 				:   function( msgArray )
										{
											$('.showPopupOrderNote').html( msgArray );
										} 
										
							});		
					}
					else
					{
						$('.close').click();
						swal("Cancelled", "Your order is safe :)", "error");
					}
			});
		});
		
		$('body').on('click', '.addordernote', function(){
			var id 				=	$(this).attr("for");
			var refundNote		=	$("#refund_notes_"+id). val();
			var refundamount	=	$("#refund_amount_"+id). val();
			var warehohuseId	=	$("#warehouse_id_"+id). val();
			var cancelNote		=	$("#cancel_note").val();
			
			if(cancelNote == '')
			{
				alert('Please fill cancel note');
				return false;
			} 
			
			$.ajax({
							
								'url' 					: 	'/Linnworksapis/cancelOrderXsensys',
								'type'					: 	'POST',
								'dataType'				:  'json',
								'data' 					: 	{openorderid : id, refundNote:refundNote, refundamount:refundamount,warehohuseId:warehohuseId, cancelNote : cancelNote},
								'beforeSend'			: function() {
																			$('.loading-image_'+id).show();
																		 },
								'success' 				  : function( msgArray )
									{ 
										
										
										if ( msgArray.delete_count > 0 )
										{
											$('.close').click();
											
											if(msgArray.partial_fulfillment > 0){
												swal("Open order canceled successfully. Also check for partial fulfillment.", "" , "success");
											}else{											
												swal("Open order canceled successfully.", "" , "success");	
											}										
											$('.loading-image_'+id).hide();
											location.reload();
										}else if ( msgArray.partial_fulfillment > 0)
										{
											$('.close').click();											
											swal("Open order canceled successfully. Please check for partial fulfillment.", "" , "success");																					
											$('.loading-image_'+id).hide();
											//location.reload();
										}
										if( msgArray.delete_count == 0 && msgArray.partial_fulfillment == 0 )
										{
											$('.close').click();
											alert('There is an error.');
										}
									} 
								
							});
			
		
		});
		
		/*$("body").on("click", ".cancelOpenOrder", function()
		{			
			var id 				=	$(this).attr("for");
			var refundNote		=	$("#refund_notes_"+id). val();
			var refundamount	=	$("#refund_amount_"+id). val();
			var warehohuseId	=	$("#warehouse_id_"+id). val();
				
			swal({
					title: "Are you sure?",
					text: "You want to cancel open order !",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, cancel it!",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						$.ajax({
							
								'url' 					: 	'/Linnworksapis/cancelOrderXsensys',
								'type'					: 	'POST',
								'dataType'				:  'json',
								'data' 					: 	{ openorderid : id, refundNote:refundNote, refundamount:refundamount,warehohuseId:warehohuseId},
								'beforeSend'			: function() {
																			$('.loading-image_'+id).show();
																		 },
								'success' 				  : function( msgArray )
									{ 
										
										
										if ( msgArray.delete_count > 0 )
										{
											$('.close').click();
											
											if(msgArray.partial_fulfillment > 0){
												swal("Open order canceled successfully. Also check for partial fulfillment.", "" , "success");
											}else{											
												swal("Open order canceled successfully.", "" , "success");	
											}										
											$('.loading-image_'+id).hide();
											location.reload();
										}else if ( msgArray.partial_fulfillment > 0)
										{
											$('.close').click();											
											swal("Open order canceled successfully. Please check for partial fulfillment.", "" , "success");																					
											$('.loading-image_'+id).hide();
											//location.reload();
										}
										if( msgArray.delete_count == 0 && msgArray.partial_fulfillment == 0 )
										{
											$('.close').click();
											alert('There is an error.');
										}
									} 
								
							});
					}
					else
					{
						$('.close').click();
						swal("Cancelled", "Your order is safe :)", "error");
					}
			});
		});*/
				
		$("body").on("click", ".unlockOrder", function()
		{			
			var id 				=	$(this).attr("for");
			var lockNote 		=	'Unlock';
			$.ajax({
					'url' 					: 	'/products/unlockOrderNotePopup',
					'type'					: 	'POST',
					'data' 					: 	{ id : id },
					'success' 				: 	function( msgArray )
												 {
													$(".showPopupunLockNote").html( msgArray );
												 }
			
					});
				
		});
		
		$('body').on("click",".addunlocknote", function(){
			
			var unlockNote 	= 	$("#unlock_note").val();
			var id 		 	=	$(this).attr("for");
			if( unlockNote == '' ) {
					alert('Please fill unlock note');
					return false;
				}
			$.ajax({
							
				'url' 					: 	'/products/unSetOrderMarked',
				'type'					: 	'POST',
				'data' 					: 	{ id : id, unlockNote:unlockNote },
				'beforeSend'			: function() {
												$('.loading-image_'+id).show();
											 },
				'success' 				  : function( msgArray )
				 {
					if ( msgArray == 1 )
					{
						$('.close').click();
						swal("Order has been unlocked marked.", "" , "success");											
						$('.loading-image_'+id).hide();
						//location.reload();
					}
					if( msgArray == 2 )
					{
						$('.close').click();
						alert('There is an error.');
					}
				 } 
				
			});
		
		});
		//unlock_unlock order
		/*$("body").on("click", ".unlockOrder", function()
		{			
			var id 				=	$(this).attr("for");
			
			//direct batch processin for cancel order
			$.ajax({
							
				'url' 					: 	'/products/unSetOrderMarked',
				'type'					: 	'POST',
				'data' 					: 	{ id : id },
				'beforeSend'			: function() {
												$('.loading-image_'+id).show();
											 },
				'success' 				  : function( msgArray )
				 {
					if ( msgArray == 1 )
					{
						$('.close').click();
						swal("Order has been unlocked marked.", "" , "success");											
						$('.loading-image_'+id).hide();
						//location.reload();
					}
					if( msgArray == 2 )
					{
						$('.close').click();
						alert('There is an error.');
					}
				 } 
				
			});
			
		});*/
		
		$("body").on("click", ".lockOrder", function()
		{			
			var id 				=	$(this).attr("for");
			$.ajax({
					'url' 					: 	'/products/lockOrderNotePopup',
					'type'					: 	'POST',
					'data' 					: 	{ id : id },
					'success' 				: 	function( msgArray )
												 {
													$(".showPopupLockNote").html( msgArray );
												 }
			
					});
			
		});
		
		$('body').on('click',".addlocknote",function(){
				var lockNote = 	$("#lock_note").val();
				var id 		 =	$(this).attr("for");
				
				if( lockNote == '' ) {
					alert('Please fill lock note');
					return false;
				}
				$.ajax({
				'url' 					: 	'/products/setOrderMarked',
				'type'					: 	'POST',
				'data' 					: 	{ id : id, lockNote:lockNote },
				'success' 				  : function( msgArray )
				 {
					if ( msgArray == 1 )
					{
						$('.close').click();
						swal("Order has been lock marked.", "" , "success");											
						$('.loading-image_'+id).hide();
						//location.reload();
					}
					if( msgArray == 2 )
					{
						$('.close').click();
						alert('There is an error.');
					}
				 } 
				
			});
		});
		
		//lock_unlock order
		/*$("body").on("click", ".lockOrder", function()
		{			
			var id 				=	$(this).attr("for");
			
			//direct batch processin for cancel order
			$.ajax({
							
				'url' 					: 	'/products/setOrderMarked',
				'type'					: 	'POST',
				'data' 					: 	{ id : id },
				'beforeSend'			: function() {
												$('.loading-image_'+id).show();
											 },
				'success' 				  : function( msgArray )
				 {
					if ( msgArray == 1 )
					{
						$('.close').click();
						swal("Order has been lock marked.", "" , "success");											
						$('.loading-image_'+id).hide();
						//location.reload();
					}
					if( msgArray == 2 )
					{
						$('.close').click();
						alert('There is an error.');
					}
				 } 
				
			});
			
		});*/
		
		//cancel order
		$("body").on("click", ".cancelOpenOrder_prevent", function()
		{			
			var id 				=	$(this).attr("for");
			var refundNote		=	$("#refund_notes_"+id). val();
			var refundamount	=	$("#refund_amount_"+id). val();
			var warehohuseId	=	$("#warehouse_id_"+id). val();
				
			swal({
					title: "Are you sure?",
					text: "You want to cancel open order !",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, cancel it!",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						$.ajax({
							
								'url' 					: 	'/Linnworksapis/cancelOrder',
								'type'					: 	'POST',
								'data' 					: 	{ openorderid : id, refundNote:refundNote, refundamount:refundamount,warehohuseId:warehohuseId},
								'beforeSend'			: function() {
																			$('.loading-image_'+id).show();
																		 },
								'success' 				  : function( msgArray )
									{
										if ( msgArray == 1 )
										{
											$('.close').click();
											swal("Open order canceled successfully.", "" , "success");											
											$('.loading-image_'+id).hide();
											location.reload();
										}
										if( msgArray == 2 )
										{
											$('.close').click();
											alert('There is an error.');
										}
									} 
								
							});
					}
					else
					{
						$('.close').click();
						swal("Cancelled", "Your order is safe :)", "error");
					}
			});
		});
		
		/* Assign postal service */
			
		$( 'body' ).on( 'click', '.assign_custom_service', function()
		{              
			var orderid    = $(this).attr('for'); 
			$.ajax(
			{
				'url'            : '/linnworksapis/assignServicePopup',
				'type'           : 'POST',
				'data'           : { orderid : orderid },                                    
				'beforeSend'     : function()
				{
								//$('.loading-image').show();
				},
				'success'        : function( msgArray ){ $( 'div.showPopupForcancelOrder' ).html( msgArray ); }
			});
		}); 
		
		$( 'body' ).on( 'click', '.assign_packaging_slip', function()
		{              
			var orderid    = $(this).attr('for');
			$.ajax(
			{
				'url'            : '/linnworksapis/assignPackagingSlipPopup',
				'type'           : 'POST',
				'data'           : { orderid : orderid },                                    
				'beforeSend'     : function()
				{
								//$('.loading-image').show();
				},
				'success'        : function( msgArray ){ $( 'div.showPopupForAssignSlip' ).html( msgArray ); }
			});
		});
		
		/* For update custom Packagin slip */			
		$("body").on("click", ".CustomSlip", function()
		{					
			var orderId 		= $(this).attr( 'for' );
			var StoreId			=	$( '#StoreStoreName' ).val();
			var StoreIdText		=	$( '#StoreStoreName' ).find(":selected").text();
			var mergeUpdateId	=	$( '#mergeupdateId' ).val();
			var TemplateStoreId	=	$( '#TemplateStoreId' ).val();
			
			$.ajax(
			{
				'url'          : '/linnworksapis/assignPackagingSlip',
				'type'         : 'POST',
				'data'         : { orderId : orderId, 
								   StoreId : StoreId,
								   StoreIdText : StoreIdText,
								   mergeUpdate : mergeUpdateId,
								   TemplateStoreId:TemplateStoreId
								 },						
				'success' 	   : function( msgArray )
								{
									//alert( msgArray );
									if( msgArray == 1 )
									{
										swal( "New Packaging Slip assigned." );
										$( ".close" ).click();
										location.reload();		
									}
									else
									{
										swal( 'Packaging Slip exist.' );
									}
								}  
			});  
			
		});
		
		$("body").on("click", ".CustomPostalService", function(){
					
			var customMark 				= 	$('#MarkedCustomPostalService').prop('checked');
			var orderId 				= 	$(this).attr( 'for' );
			var serviceID 				= 	$('#postalServiceid_'+orderId).val();
			var serviceIDPrevious 		= 	$('#postalServiceidHidden_'+orderId).val();
			
			/*
			if( serviceIDPrevious ==  serviceID)
			{
				//continue;
			}
			else
			{
				if( customMark == true )
				{
					//continue;
				}
				else
				{
					swal( 'Override Default Service' );
				}
			}
			return false;
			*/
			$.ajax(
			{
				'url'          : '/linnworksapis/setPostalservice',
				'type'         : 'POST',
				'data'         : { id			:	orderId, 
								   serviceID	:	serviceID,
								   customMark 	: 	customMark
								 },						
				'success' 	   : function( msgArray )
								{
									
									var json 		= 	JSON.parse(msgArray);
									var getData 	= 	json.data;
									
									if( json.status == 1 )
									{
										var mergeUpdateId 		= 	getData.id;
										var serviceName 		= 	getData.service_name;
										var proRefCode 			= 	getData.provider_ref_code;
										var serviceProvider 	= 	getData.service_provider;
									
										$('.service_provider_'+mergeUpdateId).text( serviceProvider );
										$('.service_code_'+mergeUpdateId).text( proRefCode );
										$('.service_name_'+mergeUpdateId).text( serviceName );
									
										swal( "New postal service assigned." );
										$('.dismiss').attr( 'for','1');
										//$( ".close" ).click();
										//location.reload();		
									}
									else if(json.status == 3)
									{
										swal( 'Please check service id for country ' );
									}
									else
									{
										swal( 'Service Id does not exist' );
									}
								}  
			});  
		
		});
		
		//Open popover for oversells orders
		Mousetrap.bind('ctrl+shift+f8', function(e)
		{
			
			// manipultaion here
			$.ajax(
			{
				url     : '<?php echo Router::url('/', true); ?>' + '/products/getOverSellOrderList',
				type    : 'POST',
				data    : {},
				success :	function( msgArray  )
				{
					$( 'div.oversells_order' ).html( msgArray );						
				}                
			}); 
			
		});

		
	});
	
	$('body').on('click','.after_process_held', function(){
		var orderid    = $(this).attr('for');
		$.ajax({
					'url'            : '/linnworksapis/holdCancelPopup',
					'type'           : 'POST',
					'data'           : { orderid : orderid },                                    
					'success'        : function( msgArray )
										{ 
											$( 'div.showPopupForHoldcancelOrder' ).html( msgArray ); 
										}
				});
	});
	
	$('body').on('click','.addCancelHeldordernote', function(){
	
				var cancelHeldNote 	= 	$("#cancel_held_note").val();
				var id 		 		=	$(this).attr("for");
				
				if( cancelHeldNote == '' ) {
					swal('Please fill cancel note');
					return false;
				}
				$.ajax({
				'url' 					: 	'/products/setHeldCancelOrderMarked',
				'type'					: 	'POST',
				'data' 					: 	{ id : id, cancelHeldNote:cancelHeldNote },
				'success' 				  : function( msgArray )
				 {
					if ( msgArray == 1 )
					{
						$('.close').click();
						swal("Order has been cancelled marked.", "" , "success");											
						$('.loading-image_'+id).hide();
						//location.reload();
					}
					if( msgArray == 2 )
					{
						$('.close').click();
						alert('There is an error.');
					}
				 } 
				
			});
	
	});
	
	
 	 
 
	
</script>
<!-- show cancel order popup -->    
<div class="global_search"></div> 

<!-- show oversell order popup -->    
<div class="oversells_order"></div> 

<!-- show cancel order popup -->    
<div class="showPopupForcancelOrder"></div> 
<div class="showPopupForAssignSlip"></div> 

<!-- show assign service popup -->    
<div class="showPopupForAssign"></div> 
<div class="showOPenOrderSearchResult"></div> 
<div class="showPopupForBinLOcation"></div> 
<div class="showPopupOrderNote"></div> 
<div class="showPopupLockNote"></div>
<div class="showPopupunLockNote"></div>
<div class="showPopupForHoldcancelOrder"></div>

</body>
<!-- END BODY -->

<!-- Mirrored from demo.yakuzi.eu/maniac/1.2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 May 2015 06:15:20 GMT -->
</html>
