<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<!-- Mirrored from demo.yakuzi.eu/maniac/1.2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 May 2015 06:14:06 GMT -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	
	<title>WMS - Dashboard</title>
	
	<!-- BEGIN CORE FRAMEWORK -->
        <?php print $this->html->css(array('plugins/bootstrap-treeview/bootstrap-treeview','plugins/bootstrap/css/bootstrap.min')); ?>
	<!-- END CORE FRAMEWORK -->
	
	<!-- BEGIN PLUGIN STYLES -->
        <?php print $this->html->css(array('plugins/datatables/dataTables.bootstrap','plugins/bootstrap-slider/css/slider')); ?>	
	<!-- END PLUGIN STYLES -->
	
	<!-- BEGIN SweetAlert Box STYLES -->
        <?php print $this->html->css(array('plugins/dist/sweetalert' , 'plugins/jquery-easyAccordion/css/jquery.hash-tabs')); ?>	
	<!-- END PLUGIN STYLES -->
	
	<!-- BEGIN THEME STYLES -->
        <?php print $this->html->css(array('material','style','plugins','helpers','responsive')); ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>	
	<!-- END THEME STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-leftside fixed-header sidebar-sm">
	<!-- BEGIN HEADER -->
        <?php 			  
                         $authDetails = $this->session->read('Auth.User'); 
                         $usernameText = $authDetails['username'];

			  if( $usernameText !== "jake.shaw" )
                          {
					print $this->element( 'header_user' ); 
                          }
			  else
                          {
					print $this->element( 'header' ); 
                          }

                         //print $this->element( 'header' ); 

 		?>
	<!-- END HEADER -->
		 
	<div class="wrapper">
		<!-- BEGIN LEFTSIDE -->
            <?php print $this->element( 'side_menu' ); ?>
		<!-- END LEFTSIDE -->

		<!-- BEGIN RIGHTSIDE -->
            <?php print $this->fetch( 'content' ); ?>
        <!-- /.wrapper -->
	<!-- END CONTENT -->
		
	<!-- BEGIN JAVASCRIPTS -->
	
	<!-- BEGIN CORE PLUGINS -->
        <?php //print $this->html->script(array('plugins/bootstrap-treeview/bootstrap-treeview','plugins/jquery-1.11.1.min','plugins/queryLoader/queryLoader','plugins/iCheck/icheck.min','plugins/bootstrap/js/bootstrap.min','plugins/bootstrap/js/holder','plugins/pace/pace.min','plugins/slimScroll/jquery.slimscroll.min','plugins/bootstrap-select/js/bootstrap-select.min',  'plugins/jquery-easyAccordion/js/jquery.hash-tabs','plugins/bootstrapValidator/bootstrapValidator.min','plugins/bootstrap-modal-popover/bootstrap-modal-popover','core','custom','custom_datatable')); ?>
	<!-- END CORE PLUGINS -->
	
	<!-- flot chart  
    <?php //print $this->html->script(array('plugins/flot/jquery.flot.min','plugins/flot/jquery.flot.grow','plugins/flot/jquery.flot.resize.min')); ?> -->

	<!-- sparkline  
    <?php //print $this->html->script(array('plugins/sparkline/jquery.sparkline.min')); ?> -->
	
	<!-- bootstrap slider 
    <?php //print $this->html->script(array('plugins/bootstrap-slider/js/bootstrap-slider')); ?>-->
	
	<!-- datepicker 
    <?php //print $this->html->script(array('plugins/bootstrap-daterangepicker/moment.min','plugins/bootstrap-daterangepicker/daterangepicker')); ?>-->
	
	<!-- vectormap  
    <?php //print $this->html->script(array('plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min','plugins/jquery-jvectormap/jquery-jvectormap-europe-merc-en')); ?>-->
	
	<!-- counter  
    <?php //print $this->html->script(array('plugins/jquery-countTo/jquery.countTo')); ?>-->
	
	<!-- rickshaw  
        <?php //print $this->html->script(array('plugins/rickshaw/vendor/d3.v3','plugins/rickshaw/rickshaw.min')); ?>-->
	
	<!-- datatables -->
	<?php //print $this->html->script(array('plugins/datatables/jquery.dataTables','plugins/datatables/dataTables.bootstrap')); ?>
	
	<!-- maniac -->
        <?php //print $this->html->script(array('maniac')); ?>

	<!--inputmask -->
    <?php //print $this->html->script(array('plugins/input-mask/jquery.inputmask','plugins/input-mask/jquery.inputmask.date.extensions','plugins/input-mask/jquery.inputmask.numeric.extensions')); ?>
	<?php
		print $this->html->script( array( 'plugins/jquery-easyAccordion/js/jquery.hash-tabs' ) );
	?>
	<!-- SweetAlert Customization -->
	<?php print $this->html->script(array('plugins/dist/sweetalert.min')); ?>
	<?php //echo $this->element('sql_dump'); ?>
	<!-- dashboard -->
	<script type="text/javascript">
		//maniac.loadchart();
		//maniac.loadvectormap();
		//maniac.loadbsslider();
		//maniac.loadrickshaw();
		//maniac.loadcounter();
		//maniac.loadprogress();
		//maniac.loaddaterangepicker();
		
		/* Call custom datatable js */
		//maniac.loadinputmask();
	</script> 
<script>
$(document).ready(function(){
    /*$('#popup-scroller1').slimScroll({
        height: '450px',
		width:'100%',
		alwaysVisible: true
    });
 $('#popup-scroller2').slimScroll({
        height: '450px',
		width:'100%',
		alwaysVisible: true
    });
  $('#popup-scroller3').slimScroll({
        height: '300px',
		width:'100%',
		alwaysVisible: true
    });
    $('#popup-scroller5').slimScroll({
        height: '300px',
		wheelStep: 1,
		animate: true,
		width:'100%',
		alwaysVisible: true
    });
	   
    $('.attr_show').slimScroll({
        height: '485px',
		width:'100%',
		size:'3',
		alwaysVisible: false
    });
	
	$('.sortingSerivesLeft>.panel-body').slimScroll({
        height: '430px',
		width:'100%',
		size:'3',
		alwaysVisible: true
    });
	
	$('.sortingSerivesRight>.panel-body').slimScroll({
        height: '430px',
		width:'100%',
		size:'3',
		alwaysVisible: true
    });*/
  
	/* Call datatable */
	//$('table.display').dataTable();

});

</script>

	<!-- END JAVASCRIPTS -->
	
	<div class="scroll-top-wrapper ">
	<span class="scroll-top-inner">
		<i class="fa fa-2x fa-arrow-circle-up"></i>
	</span>
</div>

<script>
  //QueryLoader.selectorPreload = "body";
  //QueryLoader.init();
</script>
 
<script type="text/javascript">
		//maniac.loaddatatables();
	</script>
</body>
<!-- END BODY -->

<!-- Mirrored from demo.yakuzi.eu/maniac/1.2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 May 2015 06:15:20 GMT -->
</html>
