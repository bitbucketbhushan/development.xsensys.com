<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.separator{border-bottom: 1px solid #ccc; margin-bottom: 10px;}
	.normalbox{background-color:#ffffff;}
	/* .col-8 {width: 8%; float: left; }
	.col-10 {width: 10%; float: left;}
	.col-11 {width: 6%; float: left;}
	.col-12 { width: 12%; float: left; } */
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Return Processed Orders</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row"> 
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						<form method="get" name="searchfrm" id="searchfrm" action="./ReturnProcessedOrders" enctype="multipart/form-data" onSubmit="return search()">	
						<div class="col-sm-5 no-padding">
						    <div class="col-lg-5">
									<select name="searchtype" id="searchtype" class="form-control">
									  <option value="">Select search type</option>
									  <option value="orderid" <?php echo ( isset($_REQUEST['searchtype']) && $_REQUEST['searchtype']=='orderid')?'selected':''; ?> >By Order ID</option>
										<option value="referenceid" <?php echo ( isset($_REQUEST['searchtype']) && $_REQUEST['searchtype']=='referenceid')?'selected':''; ?>>By Reference ID</option>
										<option value="customer" <?php echo ( isset($_REQUEST['searchtype']) && $_REQUEST['searchtype']=='customer')?'selected':''; ?>>By Customer Name</option>
									</select>
									<span class="text-danger hidden" id="searchtype-error">Select a type!</span>
								</div>
							  <div class="col-lg-3 ">
									<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Search" id="searchkey"  name="searchkey" class="form-control searchString" />
								  <span class="text-danger hidden" id="searchkey-error">Enter text search!</span>
								</div>
								<div class="col-lg-2 no-padding"> 
									<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
								</div>
								<div class="col-lg-2" style="z-index: 2;"> 
									<button type="button" class="btn btn-warning" id="clearsearch" style="margin-left: 4px;"><i class="glyphicon glyphicon-search"></i>&nbsp;Clear Search</button>
								</div>
						</div>
						</form>
						<div class="col-sm-7 text-right" style="z-index: 1;">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						</div>	
					<div class="panel-body no-padding-top bg-white">
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-sm-2">Order ID</div>					 
								<div class="col-sm-2">Sku Details</div> 
								<div class="col-sm-1">Postal Service</div> 
								<!-- <div class="col-sm-1">Delevery country</div> -->
								<div class="col-sm-1">Order</div>	
								<div class="col-sm-1">Customer Details</div>
								<div class="col-sm-1">Packaging Details</div>	
								<!-- <div class="col-sm-1">Processed By</div>	-->
								<div class="col-sm-2">Status</div>
								<div class="col-sm-1">Grading Status</div> 
								<div class="col-sm-1">Action</div>	
								
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						 ini_set('memory_limit',-1);
						 if(count($Returnorders) > 0) {	 ?>
							<?php foreach( $Returnorders as $items ) {   
								$customerInfo = unserialize($items['OpenOrder']['customer_info']);
								$itemDetails  = unserialize($items['OpenOrder']['items']);
								$totalDetails = unserialize($items['OpenOrder']['totals_info']);  
								$generalInfo  = unserialize($items['OpenOrder']['general_info']);
								$titles = '';
							?>
								<div class="row sku" id="r_<?php echo $items['MergeUpdate']['id']; ?>">
									<div class="col-lg-12">	
										<div class="col-sm-2"> 
											<small>
												<span><b>O</b>: <?php echo $items['MergeUpdate']['product_order_id_identify']; ?></span><br/>
												<span><b>R</b>: <?php echo $items['OpenOrder']['amazon_order_id']; ?></span>
											</small> 
										</div>
										<div class="col-sm-2"> 
											<small>
												<span><b>I</b>: <?php echo $items['MergeUpdate']['sku']; ?></span><br/>
												<span><b>B</b>: 
												<?php
													$productBarcode = explode(',',$items['MergeUpdate']['barcode']); 
													foreach($productBarcode as $prokey=>$proval) { 
														echo $proval.'<br/>'; 
													}
												?>
												</span><br/>
												<span><b>T</b>: 
												<?php 
												 foreach($itemDetails as $itemDet) { 
													if(isset($itemDet->Title)) {
														$titles .= $itemDet->Title.', ';
													} 
												 }
												 echo trim($titles,", ");
												?></span>
											</small> 
										</div>
										<div class="col-sm-1"> 
											<small>  
												<span><b>P</b>: <?php echo $items['MergeUpdate']['service_provider']; ?></span><br/>
												<span><b>S</b>: <?php echo $items['MergeUpdate']['service_name']; ?></span><br/> 
												<span><b>C</b>: <?php echo $items['MergeUpdate']['delevery_country']; ?></span><br/>
											</small> 
										</div>  
										<!-- <div class="col-sm-1"><small><?php //echo $items['MergeUpdate']['delevery_country']; ?></small></div> -->
                    
										<div class="col-sm-1">
										  <small>  
												<span><b>OD</b>: <?php echo $items['MergeUpdate']['order_date']; ?></span><br/>
												<span><b>PD</b>: <?php echo $items['MergeUpdate']['process_date']; ?></span><br/> 
											</small> 
										</div>	
									
									  <div class="col-sm-1">
										  <small>  
												<span><b>Name</b>: <?php echo $customerInfo->Address->FullName; ?></span><br/>
												<span><b>Address</b>: <?php echo $customerInfo->Address->Address1.'<br/>'.$customerInfo->Address->Address2.'<br/>'.$customerInfo->Address->Address3; ?></span><br/> 
											</small> 
										</div>
										<div class="col-sm-1">
										  <small>  
												<span><b>PS</b>: <?php echo $items['MergeUpdate']['pc_name']; ?></span><br/>
												<span><b>Pckg. B</b>: <?php echo $items['User']['first_name'].' '.$items['User']['last_name']; ?></span><br/> 
												<span><b>Proc. B</b>: <?php echo $items['MergeUpdate']['assign_user']; ?></span><br/> 
											</small> 
										</div>
 	
										<!-- <div class="col-sm-1"><small><?php //echo $items['MergeUpdate']['assign_user']; ?></small></div>	  -->

										<div class="col-sm-2">
										  <small>  
												<span><b>Status</b>: 
													<?php 
													  $resend = '';
													  if($items['MergeUpdate']['is_resend']==1) {
															$resend = '& Resend';
														}
														if($items['MergeUpdate']['status']=='4') {
															echo 'Processed (Returned '.$resend.')';
														} else {
															echo 'Processed '.$resend;
														} 
														?> 
												</span><br/>
												<?php if($items['OpenOrder']['parent_id']!='' && $items['OpenOrder']['parent_id']!='0') {?><span><b>Parent Order ID</b>: <?php echo $items['OpenOrder']['parent_id']; ?></span><br/><?php } ?>
												<?php if(!empty($Orderrelation[$items['OpenOrder']['num_order_id']])) {?><span><b>Child Order ID</b>: <?php echo $Orderrelation[$items['OpenOrder']['num_order_id']]; ?></span><br/><?php } ?> 
												<?php if($items['MergeUpdate']['return_reason']!='') {?><span><b>Return Reason</b>: <?php echo $items['MergeUpdate']['return_reason']; ?></span><br/><?php } ?>
												<?php if($items['MergeUpdate']['comments']!='') {?><span><b>Return Comments</b>: <?php echo  $items['MergeUpdate']['comments']; ?></span><br/><?php } ?>
												<?php if($items['MergeUpdate']['return_by_user']!='') {?><span><b>Return By</b>: <?php echo $items['MergeUpdate']['return_by_user']; ?></span><br/><?php } ?>
												<?php if($items['MergeUpdate']['return_date']!='') {?><span><b>Return Date</b>: <?php echo  date('d-m-Y H:i:s',strtotime($items['MergeUpdate']['return_date'])); ?></span><br/><?php } ?>
												<?php if(isset($Refamountlist[$items['OpenOrder']['num_order_id']]['refund_by_user'])) {?><span><b>Refund By</b>: <?php echo  $Refamountlist[$items['OpenOrder']['num_order_id']]['refund_by_user']; ?></span><br/><?php } ?>
												<?php if(isset($Refamountlist[$items['OpenOrder']['num_order_id']]['created_at'])) {?><span><b>Refund Date</b>: <?php echo  date('d-m-Y H:i:s',strtotime($Refamountlist[$items['OpenOrder']['num_order_id']]['created_at'])); ?></span><br/><?php } ?>
												<?php if(isset($Refamountlist[$items['OpenOrder']['num_order_id']]['refund_amount'])) { ?>
												  <span style="color: #07f50b;"><b>Refunded</b>: <?php echo $Refamountlist[$items['OpenOrder']['num_order_id']]['refund_amount'].' '.$totalDetails->Currency; ?>(<?php echo ucwords($Refamountlist[$items['OpenOrder']['num_order_id']]['refund_option']); ?>)</span><br/>  
											  <?php } ?>
												<?php if(isset($Refamountlist[$items['OpenOrder']['num_order_id']]['refund_reason'])) { ?>
												  <span><b>Refund Reason</b>: <?php echo $Refamountlist[$items['OpenOrder']['num_order_id']]['refund_reason']; ?></span><br/>
											  <?php } ?>
												<?php if($items['MergeUpdate']['resend_by_user']!='') {?><span><b>Resend By</b>: <?php echo  $items['MergeUpdate']['resend_by_user']; ?></span><br/><?php } ?>
												<?php if($items['MergeUpdate']['resend_reason']!='') {?><span><b>Resend Reason</b>: <?php echo  $items['MergeUpdate']['resend_reason']; ?></span><br/><?php } ?>
												<?php if($items['MergeUpdate']['resend_comments']!='') {?><span><b>Resend Comments</b>: <?php echo  $items['MergeUpdate']['resend_comments']; ?></span><br/><?php } ?>
												<?php if($items['MergeUpdate']['resend_date']!='') {?><span><b>Resend Date</b>: <?php echo date('d-m-Y H:i:s',strtotime($items['MergeUpdate']['resend_date'])); ?></span><br/><?php } ?>
											</small> 
										</div> 
										<div class="col-sm-1"> 
											<small>
												<span><b>Item Condition</b>:<?php echo $items['MergeUpdate']['item_condition']; ?></span>
											</small> 
										</div>
										<div class="col-sm-1">
										  <?php if($items['MergeUpdate']['status']=='1') {  ?>
											  <a href="javascript:void(0);" onclick="returnOrder('<?php echo $items['MergeUpdate']['product_order_id_identify']; ?>')" class="btn btn-warning btn-xs" role="button" title="Return"><i class="glyphicon glyphicon-repeat"></i> R</a>
											<?php } ?>
											<?php if($items['MergeUpdate']['status']=='4') {  ?>
											  <a href="javascript:void(0);" onclick="editOrder('<?php echo $items['MergeUpdate']['product_order_id_identify']; ?>','<?php echo $items['MergeUpdate']['return_reason']; ?>','<?php echo $items['MergeUpdate']['item_condition']; ?>','<?php echo $items['MergeUpdate']['comments']; ?>')" class="btn btn-warning btn-xs" role="button" title="Edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
											<?php } ?>
											<?php if(isset($totalDetails->Subtotal) && isset($totalDetails->PostageCost) && isset($totalDetails->TotalCharge) && isset($totalDetails->Currency)) { ?>
												<a href="javascript:void(0);" onclick="refundOrder('<?php echo $items['MergeUpdate']['product_order_id_identify']; ?>','<?php echo $totalDetails->Subtotal; ?>','<?php echo $totalDetails->PostageCost; ?>','<?php echo $totalDetails->TotalCharge; ?>','<?php echo $totalDetails->Currency; ?>')" class="btn btn-primary btn-xs" role="button" title="Refund"><i class="glyphicon glyphicon-retweet"></i> Refund</a>
										  <?php } ?> 
											<?php if(strtolower($items['MergeUpdate']['order_type'])!='resend' && $items['MergeUpdate']['is_refund']==0 && isset($totalDetails->TotalCharge)) {  ?>
											  <a href="javascript:void(0);" onclick="resendOrder('<?php echo $items['MergeUpdate']['order_id']; ?>','<?php echo $items['MergeUpdate']['product_order_id_identify']; ?>','<?php echo $generalInfo->Source; ?>','<?php echo $generalInfo->SubSource; ?>','<?php echo $items['MergeUpdate']['order_date']; ?>','<?php echo $customerInfo->Address->FullName; ?>','<?php echo $items['MergeUpdate']['process_date']; ?>','<?php echo $items['MergeUpdate']['postal_service']; ?>','<?php echo $totalDetails->TotalCharge; ?>','<?php echo $items['MergeUpdate']['parent_id']; ?>')" class="btn btn-success btn-xs" style="margin-top:1px;" role="button" title="Resend"><i class="glyphicon glyphicon-send"></i> Resend</a> 
											<?php } ?>
										</div> 
									</div>												
								</div>
								 
							<?php }?>
						</div>	
						
						<div class="col-sm-12 text-right" style="margin-top:10px;margin-bottom:10px;">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						 <?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>

 <!-- Modal -->
 <div class="modal fade" id="myModalReturnOrder" role="dialog">
		<div class="modal-dialog">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Return Order - Order ID: <span id="orderidtextreturn"></span></h4>
			</div>
			<div class="modal-body">
			   <div class="text-danger hidden alert-danger" id="form-error-popup-return" style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;"></div>
				 <div class="row" style="margin-bottom:5px;">
				 	<div class="col-sm-4">Return Reason*:</div>
					<div class="col-sm-8">
 					<select name="returnreason" id="returnreason" class="form-control" >
					  <option value="">Please select a reason</option>
						<option value="Accidental Order">Accidental Order</option>
						<option value="Better price available">Better price available</option>
						<option value="The shipping box or envelope is not damaged but the item is damaged">The shipping box or envelope isn't damaged but the item is damaged</option>
						<option value="Missing estimated delivery date">Missing estimated delivery date</option>
						<option value="Missing parts or accessories">Missing parts or accessories</option>
						<option value="The shipping box or envelope and item are both damaged">The shipping box or envelope and item are both damaged</option>
						<option value="Different from what was ordered">Different from what was ordered</option>
						<option value="Defective/ Does not work properly">Defective/ Does not work properly</option>
						<option value="Arrived in addition to what was ordered">Arrived in addition to what was ordered</option>
						<option value="No longer needed/ wanted">No longer needed/ wanted</option>
						<option value="Unauthorized purchase">Unauthorized purchase</option>
						<option value="Different from website description">Different from website description</option> 
					</select>
					<span class="text-danger hidden" id="reason-error">Return reason is a required field!</span>
 					</div>
				 </div>
				 
				 <div class="row" style="margin-bottom:5px;">
				 	<div class="col-sm-4">Item Condition*:</div>
					<div class="col-sm-8">
						<select name="itemcondition" id="itemcondition" class="form-control" >
						  <option value="">Please select an item condition</option>
							<option value="New box intact - A++">New box intact - A++</option>
							<option value="New box open - B++">New box open - B++</option>
							<option value="New packaging damaged - A">New packaging damaged - A</option>
							<option value="Used no packaging - B">Used no packaging - B</option>
							<option value="Danger/ Faulty">Danger/ Faulty</option>
							<option value="Working/ Box open">Working/ Box open</option>
							<option value="Packed New">Packed New</option>   
						</select>
						<span class="text-danger hidden" id="condition-error">Item condition is a required field!</span>
					</div>
				 </div>
				 
				  <div class="row">
				 	<div class="col-sm-4">Comments:</div>
					<div class="col-sm-8">
					  <textarea name="comments" id="comments" class="form-control"></textarea>
					</div>
				 </div>
			 	 <input type="hidden" id="order_id" value="" />
				 <input type="hidden" id="returnType" value="Add" />
				 
				 <div class="row"><div class="col-sm-12"><button type="button" class="btn btn-info" onclick="confirmReturn()" >SAVE</button></div></div>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		  </div>
		</div>
	  </div>

		<!-- refund order -->
		<!-- Modal -->
  <div class="modal fade" id="myModalRefund" role="dialog">
		<div class="modal-dialog">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Refund Order- Order ID: <span id="orderidtextrefund"></h4>
			</div>
			<div class="modal-body">
		   	 <div class="text-danger hidden alert-danger" id="form-error-popup-refund" style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;"></div> 
					
					<div class="row" id="refundTablelist">
					  
					</div>
				 
				 <div class="row">
				 	<div class="col-sm-4"><p>Product Price:<p></div>
					<div id="productprice" class="col-sm-8">
            
 					</div>
				 </div>
				 
				 <div class="row">
				 	<div class="col-sm-4"><p>Shipping Cost:</p></div>
					<div id="shippingcost" class="col-sm-8"> 
					</div>
				 </div>
				 
				  <!-- <div class="row">
				 	<div class="col-sm-4">Additional Price:</div>
					<div class="col-sm-8">
					  
					</div>
				 </div> -->

				 <div class="row">
				 	<div class="col-sm-4"><p>Total Price:</p></div>
					<div id="totalprice" class="col-sm-8">
					  
					</div>
				 </div>
         <input type="hidden" name="totalpricehidden" id="totalpricehidden" value="" /> 
				 <div class="row" style="margin-bottom:5px;">
				 	<div class="col-sm-4"><p>Refund*:</p></div>
					<div class="col-sm-8">
				  	<select name="refundoptions" id="refundoptions" class="form-control" >
						  <option value="">Please select a value</option>
							<option value="fullamountoforder">Full Amount Of Order</option>
							<option value="productcost">Product Cost</option>    
							<option value="shippingcost">Shipping Cost</option>    
							<option value="additionalamount">Additional Amount</option>
							<option value="discount">Discount</option>   
						</select> 
						<span class="text-danger hidden" id="refund-error">Please select a refund option!</span>
					</div>
				 </div>
				 <div class="row hidden" id="customrefundblock" style="margin-bottom:5px;">
				 	<div class="col-sm-4"><p>Custom Refund Amount*:</p></div>
					<div class="col-sm-8">
					  <input onkeypress="return isNumber(event)" type="text" id="custom_refund_amount" class="form-control" placeholder="Enter Amount" value="" /> 
						<span class="text-danger hidden" id="refund-custom-error">Please enter a value!</span>
					</div>
				 </div> 
				 <div class="row hidden" id="customrefundreason" style="margin-bottom: 5px;">
				 	<div class="col-sm-4"><p>Reason*:</p></div>
					<div class="col-sm-8">
					  <textarea id="custom_refund_reason" class="form-control" placeholder="Enter a reason"></textarea> 
						<span class="text-danger hidden" id="refund-custom-reason-error">Please enter a reason!</span>
					</div>
				 </div>
				 <div class="row hidden" id="totalrefundamount">
				 	<div class="col-sm-4"><p><b>Total Refund:</b></p></div>
					<div class="col-sm-8">  
						<span id="totalrefundtext"></span>
					</div>
				 </div>
			 	  <input type="hidden" id="refund_order_id" value="" /> 
					<input type="hidden" id="product_price" value="" />
					<input type="hidden" id="shipping_cost" value="" />
					<input type="hidden" id="total_price" value="" />
				 <div class="row">
				  <div class="col-sm-12" style="text-align:right;">
					 <button type="button" class="btn btn-info" onclick="confirmRefund()" >REFUND</button>
					</div>
				 </div>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		  </div>
		</div>
	  </div>


<!-- resend order -->
		<!-- Modal -->
		<div class="modal fade" id="myModalResendOrder" role="dialog"  style="overflow-x: hidden;overflow-y: auto;">
		<div class="modal-dialog modal-lg">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Resend Order - Order ID: <span id="orderidtext"></span></h4>
			</div>
			<div class="modal-body">

				 <div style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;" class="msgalert alert-danger" id="form-error-popup-resend-error"></div>
				 <div style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;" class="msgalert alert-success" id="form-error-popup-resend-success"></div>

				 <div class="row">
				 	<!--<div class="col-sm-4"><p>Product Price:<p></div>
					<div id="productprice" class="col-sm-8">
            
 					</div>-->
          
					<form class="form-horizontal" id="frmcreatepackage">
						<div class="col-md-12 separator">
						   <label class="col-sm-4" style="padding: 0;"><b>Basic Details</b></label>
						</div>
					  <div class="col-md-6">
						
						<div class="form-group">
							<label class="col-sm-4" for="Source">Source:</label>
							<div class="col-sm-8">
								<input readonly type="email" class="form-control" id="source" placeholder="Source">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4" for="Sub source">Sub source:</label>
							<div class="col-sm-8"> 
								<input readonly type="text" class="form-control" id="sub_source" placeholder="Sub source">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4" for="Order Date">Order Date:</label>
							<div class="col-sm-8"> 
								<input readonly class="form-control textbox-n" type="text" id="order_date" placeholder="order date">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4" for="Customer">Customer:</label>
							<div class="col-sm-8"> 
								<input readonly type="text" class="form-control" id="full_name" placeholder="Customer">
							</div>
						</div>
						</div>
          
			    	<div class="col-md-6">
             
						<div class="form-group">
							<label class="col-sm-4" for="Processed on">Processed on:</label>
							<div class="col-sm-8">
								<input readonly type="email" class="form-control" id="processed_date" placeholder="Processed on">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4" for="Service:">Service:</label>
							<div class="col-sm-8"> 
								<input readonly type="text" class="form-control" id="service" placeholder="Service:">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4" for="Total">Total:</label>
							<div class="col-sm-8"> 
								<input readonly type="text" class="form-control" id="total" placeholder="Total">
							</div>
						</div>  

						<div class="form-group">
							<label class="col-sm-4" for="Total"></label>
							<div class="col-sm-8"> 
							<button id="confirmfullorderbutton" type="button" style="margin-left: 13px;" class="btn btn-info pull-right" onclick="confirmFullOrderResend()" >RESEND FULL ORDER</button>
							</div>
						</div> 
						
          </div> 
				 
					
          <div class="col-md-12 separator">
						<label class="col-sm-4" style="padding: 0;"><b>Sku Details</b></label>
					</div> 
				 	<div class='col-md-12' id="skulist">
					 
					</div>
          <div class="col-md-12"> 
						<label class="col-sm-2 col-sm-offset-10" style="padding: 0;"><button type="button" class="btn btn-success pull-right" onclick="addMore()">+ Add More</button></label>
					</div> 
					<input type="hidden" name="mainorderId" id="mainorderId" value="" />
					<input type="hidden" name="resend_order_id" id="resend_order_id" value="" /> 
					<input type="hidden" name="parent_order_id" id="parent_order_id" value="" />  
					<input type="hidden" name="new_order_id" id="neworderid" value="" />


					<div class="col-md-12 separator">
						<label class="col-sm-4" style="padding: 0;"><b>Resend Order Details</b></label>
					</div>   
					
					<div class="col-md-6">
					<div class="form-group">
							 <label class="col-sm-4" for="Service:">Full Name*:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="fullname" name="fullname" placeholder="Full Name" />
								 <span class="text-danger hidden" id="fullname-error">Please enter a value!</span>
							 </div>
						</div>
					 <div class="form-group">
						<label class="col-sm-4" for="Service:">Reason*:</label>
						 <div class="col-sm-8">
							<select name="resend_reason" id="resend_reason" class="form-control normalbox" >
								<option value="">Please select a value</option>
								<option value="Not Delivered">Not Delivered</option>	
                                                                                                                                                     <option value="Wrong Item">Wrong Item</option>								
								<option value="Wrong Item(Warehouse Mistake)">Wrong Item(Warehouse Mistake)</option>
								<option value="Wrong Item(Listing Issue)">Wrong Item(Listing Issue)</option>
								<option value="Damaged Item">Damaged Item</option>
								<option value="Defective Item">Defective Item</option>
								<option value="Quantity mismatch">Quantity mismatch</option> 
							</select>  
							<span class="text-danger hidden" id="resend-reason-error">Please select a value!</span>
						 </div>
						</div> 
						
					  <div class="form-group">
							 <label class="col-sm-4" for="Service:">Comments:</label>
							 <div class="col-sm-8"> 
								 <textarea class="form-control normalbox" id="resend_comments" name="resend_comments" placeholder="Reason"></textarea> 
							 </div>
						 </div> 
 
             <div class="form-group">
							 <label class="col-sm-4" for="Service:">Town*:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="town" name="town" placeholder="Town" />
								 <span class="text-danger hidden" id="town-error">Please enter a value!</span>
							 </div>
						</div>
						
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Region:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="region" name="region" placeholder="Region" />
 							 </div>
						</div>
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Post Code*:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="postcode" name="postcode" placeholder="Post Code" />
								 <span class="text-danger hidden" id="postcode-error">Please enter a value!</span>
							 </div>
						</div> 
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Phone Number:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="phonenumber" name="phonenumber" placeholder="phonenumber" />
							 </div>
						</div>
						</div> 

						<div class='col-md-6'>
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Country*:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="country" name="country" placeholder="Country" />
								 <span class="text-danger hidden" id="country-error">Please enter a value!</span>
							 </div>
						</div> 
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Company:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="company" name="company" placeholder="Company" /> 
							 </div>
						</div>

            </div>

            

					<div class="col-md-6">
             
					<!-- <div class="form-group">
							 <label class="col-sm-4" for="Processed on">New Order Date*:</label>
							 <div class="col-sm-8">
							  <input type="text" class="form-control normalbox " id="neworderdate" placeholder="New Order Date" />
							 </div>
						 </div> -->

						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Address1*:</label>
							 <div class="col-sm-8"> 
							   <input type="text" class="form-control normalbox " id="address1" name="address1" placeholder="Address 1" />
								 <span class="text-danger hidden" id="resend-address1-error">Please enter an address!</span>
							 </div>
						 </div>

						 <div class="form-group">
							 <label class="col-sm-4" for="Service:">Address2:</label>
							 <div class="col-sm-8"> 
							  <input type="text" class="form-control normalbox " id="address2" name="address2" placeholder="Address 2" />
							 </div>
						 </div>

						 <div class="form-group">
							 <label class="col-sm-4" for="Service:">Address3:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="address3" name="address3" placeholder="Address 3" />
							 </div>
						 </div> 
					 </div> 
					</form>  
				 </div>
				 
				 

				 <div class="row">
				  <div class="col-sm-12">
					<label class="col-sm-1 col-sm-offset-9" style="padding: 0; margin-left: 81%;"><button type="button" class="btn btn-warning pull-right" onclick="saveTemp('0')">Save</button></label>
					<button type="button" class="btn btn-info pull-right" onclick="saveTemp('1')">RESEND</button>
					</div>
				 </div>
			</div> 
		  </div>
		</div>
	  </div>

		<div class="modal fade" id="myModalResendOrderStatus" role="dialog">
		<div class="modal-dialog modal-lg">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Alert</h4>
			</div>
			<div class="modal-body" id='liststatusorder'>
		
		  </div>
	</div>
</div>
</div>
<!--changesnew -->
<div class="modal fade" id="myModalWarning" role="dialog">
		<div class="modal-dialog">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Warning:</h4>
			</div>
			<div class="modal-body">
			    <div style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;" class="alert-danger" id="form-error-popup-resend-warning">
				   <b>Low Inventory! Would you still like to proceed?</b>
				</div>	
				<div class="row" id="stockTablelist">
					
				</div>  

				<div class="row">
				  <div class="col-sm-12" style="text-align:right;">
					<button type="button" class="btn btn-info" id="continueresend">Yes</button>
					<button type="button" class="btn btn-danger" id="continueclose" data-dismiss="modal" >No</button>
				  </div>
				</div> 
			</div> 
		  </div>
		</div>
	  </div>

<!--changesnew -->
<script>	
    function search() {
			var searchFlag = true;
      if($("#searchtype").val()=='') {
        $("#searchtype-error").removeClass('hidden');
				searchFlag = false;
			}
			if($("#searchkey").val()=='') {
				$("#searchkey-error").removeClass('hidden');
				searchFlag = false;
			}   
			return searchFlag;
		}

		$(document).delegate('.pricebox','keyup',function() { 
			$('#costskurow-'+$(this).attr('data-row')).html($(".qtyrow-"+$(this).attr('data-row')).val() * $(this).val()); 
		});

		$(document).delegate('.qtybox','keyup',function() { 
			$('#costskurow-'+$(this).attr('data-row')).html($(".pricerow-"+$(this).attr('data-row')).val() * $(this).val()); 
		});

    $("#clearsearch").click(function() {
			$("#searchtype").val('');
			$("#searchkey").val('');
			window.location.href = './ReturnProcessedOrders';
		});

		$('#searchtype').on('change',function() {
			if($(this).val()!='') {
			 $("#searchtype-error").addClass('hidden');
			}
		});

		$('#searchkey').on('keyup',function() {
			if($(this).val()!='') {
			 $("#searchkey-error").addClass('hidden');
			}
		});

		$("#refundoptions").on('change',function() {
			$("#refund-error").addClass('hidden');
			$("#totalrefundtext").html('');
			$("#custom_refund_amount").val('');
			$("#custom_refund_reason").val('');
			$("#customrefundreason").addClass('hidden');
			if($(this).val()=='additionalamount' || $(this).val()=='productcost' || $(this).val()=='shippingcost' || $(this).val()=='discount') {
        $("#customrefundblock").removeClass('hidden');
				$("#customrefundreason").addClass('hidden');
				$("#totalrefundamount").addClass('hidden');
				$("#customrefundreason").removeClass('hidden');
				// if($(this).val()=='discount') { //
				// 	$("#customrefundreason").removeClass('hidden');//
				// }//
				if($(this).val()=='additionalamount') {
				  $("#totalrefundamount").removeClass('hidden');
				}
			} else if($(this).val()=='') {	
				$("#refund-error").removeClass('hidden'); 
				$("#customrefundblock").addClass('hidden');	
				$("#customrefundreason").addClass('hidden');
				$("#totalrefundamount").addClass('hidden');
			} else {
				$("#customrefundreason").removeClass('hidden');
			  $("#customrefundblock").addClass('hidden');
				$("#totalrefundamount").addClass('hidden');	
			}
			
		});
 
    $("#custom_refund_amount").on('keyup',function() {
			if($("#refundoptions").val()=='additionalamount') {
				var totalAdditionalAmount = 0.00;
				totalAdditionalAmount = parseFloat($(this).val())+parseFloat($("#totalpricehidden").val());
				if(isNaN(totalAdditionalAmount)) {
				  totalAdditionalAmount = 0.00; 
				}
				$("#totalrefundtext").html('<b>'+totalAdditionalAmount.toFixed(2)+'</b>');
			}
		});

		$(document).delegate('.removebutton','click',function() {  
			$(this).parent('td').parent('tr').remove();
		}); 

    $(document).delegate('.checkrow','click',function() {
			if($(this).is(':checked')==false) {
			  $('.skurow-'+$(this).attr('data-row')).attr('disabled',true);	
			} else {
				$('.skurow-'+$(this).attr('data-row')).attr('disabled',false);	
			}
		});
    
    function addMore() {
			var index = parseInt($('.productlistcontainer').length) + 1;
			$("#skulistbody").append('<tr class="productlistcontainer"><td>&nbsp;</td><td><input type="text" name="sku[]" class="requiredfield form-control normalbox skurow-'+index+'" data-row="'+index+'" /></td><td><input data-row="'+index+'" onkeypress="return isNumber(event)" type="text" name="quantity[]" class="requiredfield form-control normalbox qtybox skurow-'+index+' qtyrow-'+index+'" /></td><td><input data-row="'+index+'" onkeypress="return isNumber(event)" class="requiredfield pricebox form-control normalbox skurow-'+index+' pricerow-'+index+'" type="text" value="" name="price[]"></td><td id="costskurow-'+index+'"></td><td><button type="button" class="btn btn-danger pull-right removebutton">-</button></td></tr>');
		}

  	function returnOrder(orderId='') {
			$('#myModalReturnOrder').modal('show');
			$('#orderidtextreturn').html(orderId);
			$("#order_id").val(orderId);
			$("#returnreason").val('');
			$("#itemcondition").val('');
			$("#comments").val('');	    	
			$("#returnType").val('Add');
			$("#reason-error").addClass('hidden');
			$("#condition-error").addClass('hidden');
			$("#form-error-popup-return").html('').addClass('hidden'); 
		}

		function editOrder(orderId='',reason='',condition='',comments='') {
			$('#myModalReturnOrder').modal('show');
			$('#orderidtextreturn').html(orderId);
			$("#order_id").val(orderId);
			$("#returnreason").val(reason);
			$("#itemcondition").val(condition);
			$("#comments").val(comments);			
			$("#reason-error").addClass('hidden');
			$("#condition-error").addClass('hidden');
			$("#returnType").val('Edit');
		}

		function resendOrder(mainorderId='',orderId='',source='',subsource='',orderdate='',fullname='',processeddate='',service='',total='',parentid='') {
			$(".msgalert").hide();
			$('#myModalResendOrder').modal('show');
			$("#resend_order_id").val(orderId);
			$("#mainorderId").val(mainorderId);
			$("#parent_order_id").val(parentid);
			$("#orderidtext").html(orderId);
			$("#source").val(source);
			$("#sub_source").val(subsource);
			$("#order_date").val(orderdate);
			$("#full_name").val(fullname); 
			$('#processed_date').val(processeddate); 
			$("#resend-address1-error").addClass('hidden');
			$("#resend-reason-error").addClass('hidden');
			$('#total').val(total);
			$('#service').val(service);
			$("#form-error-popup-resend-error").html('').hide();
			$("#form-error-popup-resend-success").html('').hide();
			$("#town-error").addClass('hidden');
			$("#postcode-error").addClass('hidden');
			$("#country-error").addClass('hidden');
			$("#fullname-error").addClass('hidden');
      $.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/getDetails',
				type: "POST",				
				cache: false,			
				data : {orderid:$("#resend_order_id").val()},
				success: function(data, textStatus, jqXHR)
				{	  
					// $('#myModalResendOrder').modal('hide');  
          // $('#resendorderlocation').html(data.storelocation);
					$("#confirmfullorderbutton").hide();
					if(data.showConfirmFullOrderButton == 1) {
						$("#confirmfullorderbutton").show();
					}
					$('#skulist').html(data.skuslist);
					$('#resend_reason').val(data.resendreason);
					$('#resend_comments').val(data.resendcomments);
					$('#address1').val(data.address1);
					$('#address2').val(data.address2);
					$('#address3').val(data.address3);
					$('#town').val(data.town),
					$('#region').val(data.region),
					$('#postcode').val(data.postcode),
					$('#country').val(data.country),
					$('#fullname').val(data.fullname),
					$('#company').val(data.company),
					$('#phonenumber').val(data.phonenumber)
				}	
			});
		}
function resendFullOrder() {
		 
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/confirmFullOrderResend',
				type: "POST",				
				cache: false,
				data :$("#frmcreatepackage").serialize(), 
				success: function(data, textStatus, jqXHR)
				{	 
					if(data.status==1) {
						$('#myModalResendOrder').modal('hide');
							swal({
								title: "Confirm Resend",
								text: "New Order Id, ( < "+ data.neworderid +" >).",
								type: "success",
								confirmButtonText: "OK",
								allowEscapeKey:false
							},
							function(isConfirm){
								if (isConfirm) {
									location.reload();
								}
							});
					} else if(data.status==2) {
						$('#liststatusorder').html(data.msg);
						$('#myModalResendOrderStatus').modal('show');
					} else {
						swal({
								title: "Alert",
								text: data.msg,
								type: "warning",
								confirmButtonText: "OK",
								allowEscapeKey:false
							},
							function(isConfirm){
								// if (isConfirm) {
								// 	location.reload();
								// }
						});
						// $("#form-error-popup-resend-error").html(data.msg).show();
						// setTimeout(function(){ $(".msgalert").hide(); }, 3000);
					} 							
				}	
			}); 
		}
		function confirmFullOrderResend() { 
			$(".msgalert").hide();
			var ajaxFlag=true;
			var reqfielderror=false; 

			if($("#resend_reason").val()=='') {
				$("#resend-reason-error").removeClass('hidden'); 
			} else {
				$("#resend-reason-error").addClass('hidden'); 
			}

			if($("#address1").val()=='') {
				$("#resend-address1-error").removeClass('hidden'); 
			} else {
				$("#resend-address1-error").addClass('hidden'); 
			}

			if($("#town").val()=='') {
				$("#town-error").removeClass('hidden'); 
			} else {
				$("#town-error").addClass('hidden'); 
			}

			if($("#postcode").val()=='') {
				$("#postcode-error").removeClass('hidden'); 
			} else {
				$("#postcode-error").addClass('hidden'); 
			}

			if($("#postcode").val()=='') {
				$("#postcode-error").removeClass('hidden'); 
			} else {
				$("#postcode-error").addClass('hidden'); 
			}
			if($("#country").val()=='') {
				$("#country-error").removeClass('hidden'); 
			} else {
				$("#country-error").addClass('hidden'); 
			}
			if($("#fullname").val()=='') {
				$("#fullname-error").removeClass('hidden'); 
			} else {
				$("#fullname-error").addClass('hidden'); 
			}
			
			if($("#resend_reason").val()=='' 
			  || $("#address1").val()=='' 
				|| $("#town").val()=='' 
				|| $("#postcode").val()==''
				|| $("#country").val()==''
				|| $("#fullname").val()==''
				|| reqfielderror==true) {
				ajaxFlag = false;
				$("#form-error-popup-resend-error").html('Please fill the required fields.').show();
			} 
  
	    if(ajaxFlag==true) {
			$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/checkInventory',
			type: "POST",				
			cache: false,			
			data : {orderid:$("#mainorderId").val(),'type':'fullorder'},
			success: function(sdata, textStatus, jqXHR)
			{	    
				if(sdata.status == 0) { 
				   resendFullOrder();
				} else {
				   //show warning
                   $("#stockTablelist").html(sdata.msg)
				   $('#myModalWarning').modal('show');
				   $("#continueresend").unbind('click');
				   $("#continueresend").bind( "click", function() {
				    $('#myModalWarning').modal('hide'); 
					resendFullOrder();
				   });
				}
			}	
			}); 
		 }
			
		}

		function refundOrder(orderId='',subtotal='',postagecost='',totalcharge='',currency='') {
			$("#refundTablelist").html('');
      $('#myModalRefund').modal('show');
			$('#orderidtextrefund').html(orderId); 
			$("#refund_order_id").val(orderId);
			$("#product_price").val(subtotal);
			$("#shipping_cost").val(postagecost);
			$("#total_price").val(totalcharge);
			$("#refundoptions").val('');
			$("#productprice").html(subtotal+' '+currency);
			$("#shippingcost").html(postagecost+' '+currency);
			$("#totalprice").html(totalcharge+' '+currency);
			$("#totalpricehidden").val(totalcharge);
			$("#custom_refund_amount").val('');
			$("#refund-error").addClass('hidden');
			$("#refund-custom-error").addClass('hidden');
			$("#customrefundblock").addClass('hidden');
			$("#totalrefundamount").addClass('hidden');
			$("#customrefundreason").addClass('hidden');
			$("#refund-custom-reason-error").addClass('hidden');
			$("#form-error-popup-refund").html('').addClass('hidden');
			$("#custom_refund_reason").val('');
			$("#totalrefundtext").html('');
			$("#custom_refund_amount").val(''); 
			$('#refundoptions').find("option[value='fullamountoforder']").attr('disabled',false).css({'background': '#ffffff'});
			//get all refund given in the order 
			$.ajax({
					dataType: 'json',
					url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/getRefunds',
					type: "POST",				
					cache: false,			
					data : {orderid:orderId,currency:currency},
					success: function(data, textStatus, jqXHR)
					{	  
						$("#refundTablelist").html(data.refunds);   					
						if(data.showfullamountorderflag==0) {
							$('#refundoptions').find("option[value='fullamountoforder']").attr('disabled',true).css({'background': '#dddddd'});
						} 
					}	
			});
		}

		function isNumber(event) {
			if(event.which == 8 || event.which == 0){ 
				return true;
			}

			// prevent if not number/dot
			if(event.which < 46 || event.which > 59) {
				event.preventDefault();
			} 

			// prevent if already dot 
			if(event.which == 46 && event.target.value.indexOf('.') != -1) {
				event.preventDefault();
			} 
    }
 
    $('#returnreason').on('change',function() {
			if($(this).val()!='') {
			 $("#reason-error").addClass('hidden');
			}
		});

		$('#itemcondition').on('change',function() {
			if($(this).val()!='') {
			 $("#condition-error").addClass('hidden');
			}
		});

		

    function confirmRefund() {
			$("#form-error-popup-refund").html('').addClass('hidden');
			var ajaxFlag=true;
			if($("#refundoptions").val()=='') {
				$("#refund-error").removeClass('hidden');
				ajaxFlag = false;
			} else if($("#refundoptions").val()=='additionalamount' || $("#refundoptions").val()=='productcost' || $("#refundoptions").val()=='shippingcost' || $("#refundoptions").val()=='discount') {
        if($("#custom_refund_amount").is(":visible") && $("#custom_refund_amount").val()=='') {
					$("#refund-custom-error").removeClass('hidden')
					ajaxFlag = false;
				} else {
					$("#refund-custom-error").addClass('hidden')
					ajaxFlag = true;
				}
				if($("#custom_refund_reason").is(":visible") && $("#custom_refund_reason").val()=='') {
					$("#refund-custom-reason-error").removeClass('hidden')
					ajaxFlag = false;
				} else {
					$("#refund-custom-reason-error").addClass('hidden')
					ajaxFlag = true;
				} 
			} else {
				$("#refund-error").addClass('hidden');
				ajaxFlag = true; 
			} 
			if(ajaxFlag==true) {
				$.ajax({
					dataType: 'json',
					url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/confirmRefund',
					type: "POST",				
					cache: false,			
					data : {orderid:$("#refund_order_id").val(),customerefundreason:$('#custom_refund_reason').val(),customrefundamount:$("#custom_refund_amount").val(),productprice:$("#product_price").val(),refundoptions:$("#refundoptions option:selected").val(),shippingcost:$("#shipping_cost").val(),totalprice:$("#total_price").val()},
					success: function(data, textStatus, jqXHR)
					{	 
						if(data.status==1) {
							$('#myModalReturnOrder').modal('hide');
							location.reload();
						} else {
							$("#form-error-popup-refund").html(data.msg).removeClass('hidden');
						} 							
					}	
				});  
			}
		} 
		
   function resendSingle(savetype='') {
			 
			$.ajax({
					dataType: 'json',
					url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/saveTemp',
					type: "POST",				
					cache: false,
					data :$("#frmcreatepackage").serialize(), 
					success: function(data, textStatus, jqXHR)
					{	  
						if(data.status==1) {
							$("#form-error-popup-resend-success").html(data.msg).show();  
							$("#neworderid").val(data.neworderid);							
							//timing the alert box to close after 3 seconds 
							if(savetype=='1') {
								confirmResend();
							}
							setTimeout(function(){ $(".msgalert").hide(); }, 3000);
						} else {
							$("#form-error-popup-resend-error").html(data.msg).show();
						} 
					}	
				});
		}

        function saveTemp(savetype='0') {
		$(".msgalert").hide();
			// $("#form-error-popup-resend-error").html().show();
			var ajaxFlag=true;
			var reqfielderror=false;
            $('.reqerror').remove();
			$(".requiredfield").each(function() { 
				if($(this).val()=='' && $(this).is(':disabled')==false) {
					$(this).parent('td').append('<span class="text-danger reqerror" >Please enter a value!</span>');
					reqfielderror = true;
				} 
			});
			if(savetype=='1') { //validate only if user clicked confirm
				if($("#resend_reason").val()=='') {
					$("#resend-reason-error").removeClass('hidden'); 
				} else {
					$("#resend-reason-error").addClass('hidden'); 
				}
			}

			if($("#address1").val()=='') {
				$("#resend-address1-error").removeClass('hidden'); 
			} else {
				$("#resend-address1-error").addClass('hidden'); 
			}

			if($("#town").val()=='') {
				$("#town-error").removeClass('hidden'); 
			} else {
				$("#town-error").addClass('hidden'); 
			}

			if($("#postcode").val()=='') {
				$("#postcode-error").removeClass('hidden'); 
			} else {
				$("#postcode-error").addClass('hidden'); 
			}

			if($("#postcode").val()=='') {
				$("#postcode-error").removeClass('hidden'); 
			} else {
				$("#postcode-error").addClass('hidden'); 
			}
			if($("#country").val()=='') {
				$("#country-error").removeClass('hidden'); 
			} else {
				$("#country-error").addClass('hidden'); 
			}
			if($("#fullname").val()=='') {
				$("#fullname-error").removeClass('hidden'); 
			} else {
				$("#fullname-error").addClass('hidden'); 
			}
			
			if($("#address1").val()=='' 
				|| $("#town").val()=='' 
				|| $("#postcode").val()==''
				|| $("#country").val()==''
				|| $("#fullname").val()==''
				|| reqfielderror==true) {
				ajaxFlag = false;
			}
  
			if($("#resend_reason").val()=='' && savetype=='1'){
				ajaxFlag = false;
			}
 
	    if(ajaxFlag==true) {

			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/checkInventory',
				type: "POST",				
				cache: false,			
				data :  $("#frmcreatepackage").serialize(),
				success: function(sdata, textStatus, jqXHR)
				{	    
					if(sdata.status == 0) { 
						resendSingle(savetype);
					} else {
					//show warning
					$("#stockTablelist").html(sdata.msg)
					$('#myModalWarning').modal('show');
					$("#continueresend").unbind('click');
					$("#continueresend").bind( "click", function() {
						$('#myModalWarning').modal('hide'); 
						resendSingle(savetype);
					});
					}
				}	
				});  
			}
		}

		function confirmResend() {
			$(".msgalert").hide();  
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/confirmResend',
				type: "POST",				
				cache: false,
				data :$("#frmcreatepackage").serialize(), 
				success: function(data, textStatus, jqXHR)
				{	 
					if(data.status==1) {
						$('#myModalResendOrder').modal('hide'); 
						swal({
							title: "Confirm Resend",
							text: "New Order Id, ( < "+ data.neworderid +" >).",
							type: "success",
							confirmButtonText: "OK",
							allowEscapeKey:false
						},
						function(isConfirm){
							if (isConfirm) {
								location.reload();
							}
						});
					} else {
						swal({
							title: "Alert",
							text: data.msg,
							type: "warning",
							confirmButtonText: "OK",
							allowEscapeKey:false
						},
						function(isConfirm){
							// if (isConfirm) {
							// 	location.reload();
							// }
						});
						// $("#form-error-popup-resend-error").html(data.msg).show();
						// setTimeout(function(){ $(".msgalert").hide(); }, 3000);
					} 							
				}	
			});
		}

    function confirmReturn(){
			$("#form-error-popup-return").html('').addClass('hidden');
			if($("#returnType").val()=='Add') {
					//add reason
					var ajaxFlag=true;
					if($("#returnreason").val()=='') {
						$("#reason-error").removeClass('hidden');
					} else {
						$("#reason-error").addClass('hidden');
					}
					if($("#itemcondition").val()=='') {
						$("#condition-error").removeClass('hidden');
					} else {
						$("#condition-error").addClass('hidden');
					}

					if($("#returnreason").val()=='' || $("#itemcondition").val()=='') {
					  ajaxFlag = false;
					}

					if(ajaxFlag==true) {
						$.ajax({
							dataType: 'json',
							url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/confirmReturn',
							type: "POST",				
							cache: false,			
							data : {orderid:$("#order_id").val(),returnreason:$("#returnreason option:selected").val(),itemcondition:$("#itemcondition option:selected").val(),comments:$("#comments").val() },
							success: function(data, textStatus, jqXHR)
							{	 
								if(data.status==1) {
									$('#myModalReturnOrder').modal('hide');
									location.reload();
								} else {
									$("#form-error-popup-return").html(data.msg).show();
								} 							
							}	
						});   		
					}
			} else {
					//edit reason
					var ajaxFlag=true;
					if($("#returnreason").val()=='') {
						$("#reason-error").removeClass('hidden'); 
					} else {
						$("#reason-error").addClass('hidden'); 
					}
					if($("#itemcondition").val()=='') {
						$("#condition-error").removeClass('hidden'); 
					} else {
						$("#condition-error").addClass('hidden'); 
					}

					if($("#returnreason").val()=='' || $("#itemcondition").val()=='') {
					  ajaxFlag = false;
					}

					if(ajaxFlag==true) {
						$.ajax({
							dataType: 'json',
							url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/editReturn',
							type: "POST",				
							cache: false,			
							data : {orderid:$("#order_id").val(),returnreason:$("#returnreason option:selected").val(),itemcondition:$("#itemcondition option:selected").val(),comments:$("#comments").val() },
							success: function(data, textStatus, jqXHR)
							{	 
								if(data.status==1) {
									$('#myModalReturnOrder').modal('hide');
									location.reload();
								} else {
									$("#form-error-popup-return").html(data.msg).show();
								} 							
							}	
						});   		
					}
			}
    } 
</script>