<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.separator{border-bottom: 1px solid #ccc; margin-bottom: 10px;}
	.normalbox{background-color:#ffffff;}
	/* .col-8 {width: 8%; float: left; }
	.col-10 {width: 10%; float: left;}
	.col-11 {width: 6%; float: left;}
	.col-12 { width: 12%; float: left; } */
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Download Return/ Resend Orders Report</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">  
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						<form method="post" name="downloadReport" id="downloadReport" action="./showResendRefundReport" enctype="multipart/form-data" onSubmit="return search()">	
						<div class="col-sm-12 no-padding">
						   <!-- <div class="col-lg-5">
									 <select name="searchtype" id="searchtype" class="form-control">
									  <option value="">Select search type</option>
									  <option value="orderid" <?php //echo ( isset($_REQUEST['searchtype']) && $_REQUEST['searchtype']=='orderid')?'selected':''; ?> >By Order ID</option>
										<option value="referenceid" <?php //echo ( isset($_REQUEST['searchtype']) && $_REQUEST['searchtype']=='referenceid')?'selected':''; ?>>By Reference ID</option>
										<option value="customer" <?php //echo ( isset($_REQUEST['searchtype']) && $_REQUEST['searchtype']=='customer')?'selected':''; ?>>By Customer Name</option>
									</select> -->
									<!-- <span class="text-danger hidden" id="searchtype-error">Select a type!</span> 
								</div>-->
								<input type='hidden' id='resendflag' name='resendflag' value='0' />
								<input type='hidden' id='refundflag' name='refundflag' value='0' />
								<input type='hidden' id='rangestart' name='rangestart' value='' />
								<input type='hidden' id='rangeend' name='rangeend' value='' />
							  <div class="col-lg-3 col-lg-offset-2" id="checkboxoption" style="padding-top: 9px;width: 17%;"></div>
								<div class="col-lg-5" style="width: 24%;"> 
								<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
								</div>
								
								<div class="col-lg-2"> 
									<button onclick="downLoadReport()" type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Generate Report</button>
								</div>
								<!-- <div class="col-lg-2" style="z-index: 2;"> 
									<button type="button" class="btn btn-warning" id="clearsearch" style="margin-left: 4px;"><i class="glyphicon glyphicon-search"></i>&nbsp;Clear Search</button>
								</div> -->
						</div>
						</form>
						<div class="col-sm-7 text-right" style="z-index: 1;">
						<ul class="pagination" style="display:inline-block">
							  <?php
								  //  echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								  //  echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								  //  echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						</div>	
					<!--<div class="panel-body no-padding-top bg-white">
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-sm-2">Order Number</div>					 
								<div class="col-sm-2">Order Date</div> 
								<div class="col-sm-1">Action Date</div>  
								<div class="col-sm-1">Refund/ Resend</div>	
								<div class="col-sm-1">Amount</div>
								<div class="col-sm-1">Currency</div>	 
								<div class="col-sm-2">GBP</div>
								<div class="col-sm-1">Refund Option</div> 
								<div class="col-sm-1">Reason</div>	
								
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						//  ini_set('memory_limit',-1);
						//  if(count($Returnorders) > 0) {	 ?>
            //                 <?php 
            //                   foreach( $Returnorders as $items ) { 
            //                     $totalDetails = unserialize($items['OpenOrder']['totals_info']); 
                            ?>

								<div class="row sku" id="r_<?php //echo $items['MergeUpdate']['id']; ?>">
									<div class="col-lg-12">	
										<div class="col-sm-2"> 
											<small>
                                                <?php //echo $items['MergeUpdate']['product_order_id_identify']; ?>
											</small> 
										</div>
										<div class="col-sm-2"> 
											<small>
                                                <?php //echo $items['MergeUpdate']['order_date']; ?>
											</small> 
										</div>
										<div class="col-sm-1"> 
											<small>  
                                                <?php //echo $items['Refund']['created_at']; ?>
											</small> 
										</div>   
										<div class="col-sm-1">
											<small>  
												<?php 
														// if($items['MergeUpdate']['is_refund']==1 && $items['MergeUpdate']['is_resend']==1){
														// 	echo 'Refund/ Resend'; 
														// } else if($items['MergeUpdate']['is_refund']==1 && $items['MergeUpdate']['is_resend']==0){
														// 	echo 'Refund'; 
														// } else if($items['MergeUpdate']['is_refund']==0 && $items['MergeUpdate']['is_resend']==1){
														// 	echo 'Resend'; 
														// }  
												?>
											</small> 
										</div>	 
									    <div class="col-sm-1">
										    <small>  
													<?php //echo $items['Refund']['refund_amount']; ?>
											</small> 
										</div>
										<div class="col-sm-1">
										    <small>  
												<?php //echo $totalDetails->Currency; ?>
											</small> 
										</div> 
 
										<div class="col-sm-2">
										    <small>   
												<?php //echo $items['Refund']['refund_amount_converted']; ?>  
											</small> 
										</div> 
										<div class="col-sm-1"> 
                                            <small>   
												<?php //if(!empty($Refundoptions[$items['Refund']['refund_option']])) { echo $Refundoptions[$items['Refund']['refund_option']]; } ?>  
											</small>
                                        </div> 
                                        <div class="col-sm-1"> 
                                            <small>   
												<?php //echo $items['Refund']['reason']; ?>  
											</small>
										</div> 
									</div>												
								</div> 
							<?php // } ?>
						</div>	
						
						<div class="col-sm-12 text-right" style="margin-top:10px;margin-bottom:10px;">
						<ul class="pagination" style="display:inline-block">
							  <?php
								  //  echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								  //  echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								  //  echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						 <?php //} else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php //}  ?>
						</div>-->
						<br />
						<!-- <div style="position:absolute; top:100%; right:0; left:0;"><?php //echo $this->element('footer'); ?></div> -->
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>

 
<script>
    function search() {
        var searchFlag = true;
        if($("#searchtype").val()=='') {
           $("#searchtype-error").removeClass('hidden');
            searchFlag = false;
        }
        if($("#searchkey").val()=='') {
           $("#searchkey-error").removeClass('hidden');
            searchFlag = false;
        }   
        return searchFlag;
    }   
		$(document).ready(function() {
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/loadFilters',
				type: "POST",				
				cache: false,			 
				success: function(data, textStatus, jqXHR)
				{	   
					$('#checkboxoption').html(data.checkOptions); 
				}	
			});

			$(document).delegate('#checkfilterresend','click',function() { 
				$("#resendflag").val('0'); 
				if($(this).is(':checked')==true) {
			    $("#resendflag").val('1');
				}
			});

			$(document).delegate('#checkfilterrefund','click',function() { 
				$("refundflag").val('0'); 
				if($(this).is(':checked')==true) {
			    $("#refundflag").val('1');
				}
			});
			
		});

	function downLoadReport()
	{ 
		var isResend = $("#resendflag").val();
		var isRefund = $("#refundflag").val();
		var getFrom  = $('div.daterangepicker_start_input input:text').val();
		var getEnd   = $('div.daterangepicker_end_input input:text').val();
		$("#rangestart").val(getFrom);
		$("#rangeend").val(getEnd);
		$("#downloadReport").submit();
		//Ajax Perform with cache false
		// $.ajax(
		// {
		// 	url     : '<?php //echo Router::url('/', true) ?>ReturnProcessedOrders/downloadResendRefundReport',
		// 	type    : 'POST',
		// 	data    : {getFrom:getFrom, getEnd:getEnd, isRefund:isRefund, isResend:isResend},
		// 	'beforeSend' : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },  
		// 	success :	function( msgArray  )
		// 	{
		// 		$( ".outerOpac" ).attr( 'style' , 'display:none');
				
		// 		if( msgArray != 0 )
		// 		{
		// 			// $( ".linkFile" ).attr( 'style' , 'display:block' );
		// 			// $( "span.downloadSkuReport a" ).attr( 'href' , msgArray );
		// 		}
		// 		else
		// 		{
		// 			// $( ".linkFile" ).attr( 'style' , 'display:none' );
		// 			// $( "span.downloadSkuReport a" ).attr( 'href' , '' );
		// 			swal( "Oops, No result found!" );
		// 			return false;
		// 		}
		// 	}                
		// });			
	}
		
</script>