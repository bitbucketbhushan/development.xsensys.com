<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php print 'Templates' ;?></h1>
		
			<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		
    </div>
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">									
									<div class="panel-tools">																	
									</div>
								</div>
                                <div class="panel-body no-padding-top bg-white">											
									<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 177px;" aria-sort="ascending" aria-label="Id: activate to sort column descending">S.No.</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 177px;" aria-sort="ascending" aria-label="Client: activate to sort column descending">Name</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 177px;" aria-sort="ascending" aria-label="Client: activate to sort column descending">Action</th>
											</tr>
										</thead>
										<tbody role="alert" aria-live="polite" aria-relevant="all">
											<?php $i = 1; foreach( $templates as $template ) { ?>
												<tr class="odd">
													<td class="  sorting_1"><?php echo $i; ?></td>
													<td class="  sorting_1"><?php echo $template['Template']['name']; ?></td>                                                
													<td class="  sorting_1">
														<a href="/templates/editTemplate/<?php print $template['Template']['id']; ?>" class="btn btn-success btn-xs margin-right-10"><i class="fa fa-pencil"></i></a>
													</td>                                                
												</tr>
											<?php $i++;	} ?>
										</tbody>
									</table>		
								</div>
                            </div>
                        </div>
                    </div>
					<?php echo $this->element('footer'); ?>
				</div>
			</div>
		</div>

