<?php print $this->form->input( 'Template.ids', array( 'type'=>'hidden') ); ?>
<?php $i = 0; foreach( $postalservices as $postalservice ) { ?>
	<div class="input-group col-lg-12">
		<div class="col-sm-1">
			<div class="checkbox checkbox-theme display-inline-block">
				<?php
					print $this->form->input( 'Template.selected_courier.'.$i.'', array( 'type'=>'checkbox','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false, 'style'=>'display:block;'  ) );
					print $this->form->input( 'Template.'.$i.'.selected_courier_id', array( 'type'=>'hidden', 'value' => $postalservice['PostalServiceDesc']['id']) );
				?>
			</div>
		</div>
		<label class="control-label col-lg-11" style="text-align:left"><?php echo '('.$postalservice['PostalServiceDesc']['provider_ref_code'].')'; ?>&nbsp;&nbsp;<?php echo $postalservice['PostalServiceDesc']['service_name']; ?></label>
		<?php
			//print $this->form->input( 'Template.'.$i.'.subsource_id', array( 'type'=>'hidden' ) );
			//print $this->form->input( 'Template.'.$i.'.client_id', array( 'type'=>'hidden' ) );
			//print $this->form->input( 'Template.'.$i.'.id', array( 'type'=>'hidden') );
			//print $this->form->input( 'Template.'.$i.'.source_id', array( 'type'=>'hidden' ) );
		?> 
	</div>
<?php $i++; } ?>

<script>

$('input[type="checkbox"]').change(function() {
	var arr = [];
	$('input[type="checkbox"]:checked').each(function() {
			arr.push($(this).next().val());
	});
		$('#TemplateIds').val( arr );
 });


</script>
