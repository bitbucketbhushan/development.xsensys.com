<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px; }
	.highlight{border:1px #FF0000 solid;background:#FF3300;}
	.notequal{border:1px #993300 solid; background:#CCFF33;}
	.fl-pad{float:left; width:70px;}
	.pl{padding-left:5px !important;width:110px; text-align:center;}
	 body div.sku.bt{border-bottom:2px solid #bbb;border-top:2px solid #bbb;} 
 </style>
  
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>				
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>				
				<h4 style="float:left;">Prepare Shipment</h4>
				<div class="col-lg-3"  style="float:right;">
					<div class="col-lg-6">
					 
					</div>
					<div class="col-lg-6">
					<a href="<?php echo Router::url('/', true) ?>FbaShipments/ExportShipment/<?php echo $this->params['pass'][0]?>" class="btn btn-primary btn-sm" style="float:right;">Back</a>
					</div>					
				</div>
				<br /><br />
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							 
						
						<div class="body" id="results">
								
						<?php				
										
                              print $this->form->create( 'Shipment', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/FbaBoxing/SaveShipmentPacking', 'type'=>'post','id'=>'shipment_packing_frm' ) );
							App::Import('Controller', 'FbaBoxing'); 
							$obj = new FbaBoxingController;	
							$packdata = $obj->getShipmentPacking($this->params['pass'][0]); 
                              
                            ?>
							 <input type="hidden" name="shipment_id" value="<?php echo isset($packdata['FbaShipmentPacking']) ? $packdata['FbaShipmentPacking']['shipment_id']:'';?>" id="shipment_id" /> 
							<input type="hidden" name="shipment_inc_id" value="<?php print $this->params['pass'][0]; ?>" id="shipment_inc_id" /> 
						
						<div id="r4" >	
						 
							<div class="row sku">
								<div class="col-lg-12">
									<h3>Shipping Services	</h3>								
								</div>
							</div>
							<div class="row sku">	
								<div class="col-lg-12 shipping_type">
								
									<div class="col-sm-4 checkInp"> 
							
							  
									<input type="radio" name="shipping_type" value="spd" id="spd" <?php if(isset($packdata['FbaShipmentPacking']) && $packdata['FbaShipmentPacking']['shipping_type'] == 'spd') echo 'checked="checked"'; ?> onclick="chkShippingType();"  /> &nbsp;
									<label for="spd">Small Parcel delivery (SPD)</label>	</div>	
									<div class="col-sm-6">
									<input type="radio" name="shipping_type" value="pcs" id="pcs" <?php if(isset($packdata['FbaShipmentPacking']) && $packdata['FbaShipmentPacking']['shipping_type'] == 'pcs') echo 'checked="checked"'; ?> onclick="chkShippingType();"/> &nbsp;
									<label for="pcs">Palletized consolidated shipment (PCS)</label>	</div>								
								</div>
							</div>
							
							
							<div class="row sku">
								<div class="col-lg-12">
									<h4>Shippent Packing</h4>								
								</div>
								<div class="col-sm-12"> 
								
									<div class="col-sm-4" id="div_pallet" style="display:none;"><label for="no_boxes">Enter No of Pallets : </label>	<input type="text" name="no_pallets" value="<?php echo isset($packdata['FbaShipmentPacking']['no_pallets']) ? $packdata['FbaShipmentPacking']['no_pallets']:'';?>" id="no_pallets" /></div>
								
									<div class="col-sm-4"><label for="no_boxes">Enter No of Boxes : </label>	
									<input type="text" name="no_boxes" value="<?php echo isset($packdata['FbaShipmentPacking']) ? $packdata['FbaShipmentPacking']['no_boxes']:'';?>" id="no_boxes" /> &nbsp;</div>
									
									
									<div class="col-sm-2"><button type="button" class="btn btn-success btn-xs" title="Submit" onclick="shippingBoxes();"><i class="fa  fa-check-square"></i>&nbsp;Submit</button></div>
									
								</div>	
							</div>
							
							<div id="grid"></div>
							  
							<div class="row">
								<div class="col-lg-12">												
									<div class="col-sm-2" style="float:right; text-align:right;">											
									<button type="button" class="btn btn-success btn-sm" title="Save Values" id="save_box_values"><i class="fa  fa-check-square"></i>&nbsp;Save Box Values</button>
									 </div>
								</div>
							</div>
							<div class="row sku">		
								<div class="col-lg-12 col-sm-6">	
									<div class="col-sm-1">Ref.No. :</div><div class="col-sm-4"><input type="text" name="ref" value="<?php  if(count($packdata) > 0){ print $packdata['FbaShipmentPacking']['ref'];}?>" class="form-control" /></div>
									<div class="col-sm-1">Courier :</div><div class="col-sm-4"><input type="text" name="courier" value="<?php  if(count($packdata) > 0){ print $packdata['FbaShipmentPacking']['courier'];}?>" class="form-control" /></div>
								</div>
							</div>
							 
							<div class="row sku">	
								<div class="col-lg-12 col-sm-6">		
									<div class="col-sm-1">Tracking :</div><div class="col-sm-4"><input type="text" name="tracking" value="<?php  if(count($packdata) > 0){ print $packdata['FbaShipmentPacking']['tracking'];}?>" class="form-control" /></div>
									<div class="col-sm-1">Contact :</div><div class="col-sm-4"><input type="text" name="contact" value="<?php  if(count($packdata) > 0){ print $packdata['FbaShipmentPacking']['contact'];}?>" class="form-control" /></div>
								</div>
							</div>
							
							<div class="row sku">	
								<div class="col-lg-12 col-sm-6">		
									<div class="col-sm-1">Documents :</div>
									<div class="col-sm-4">
									
									<?php
									print $this->form->input( 'Documents1', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false) );
									?>
									
									</div>	
										<?php  if(isset($packdata['FbaShipmentPacking']) && count($packdata['FbaShipmentPacking']) > 0){
									 		print'<div class="col-sm-4">';
											if($packdata['FbaShipmentPacking']['documents1']){
											 print'<a href="'. Router::url('/', true).'fba_shipments/documents/'.$packdata['FbaShipmentPacking']['documents1'].'" target="_blank" style="text-decoration:underline; color:#0000FF">'. $packdata['FbaShipmentPacking']['documents1'].'</a>&nbsp;&nbsp;<a href="'. Router::url('/', true).'FbaBoxing/RemoveDocument/'.$packdata['FbaShipmentPacking']['id'].'/documents1" title="Remove Document"><i class="fa fa-times" aria-hidden="true" style="color:#FF0000"></i></a>';
											}else{
												echo 'No Document Uploaded';
											}
											echo '</div>';
									 }?>	
									
 								</div>
								
								<div class="col-lg-12 col-sm-6">		
									<div class="col-sm-1">&nbsp;</div>
									<div class="col-sm-4">
									<?php
									print $this->form->input('Documents2', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false) );
									?>
									</div>	
										<?php  if(isset($packdata['FbaShipmentPacking']) && count($packdata['FbaShipmentPacking']) > 0){
									 		
											print'<div class="col-sm-4">';
											if($packdata['FbaShipmentPacking']['documents2']){
											 print'<a href="'. Router::url('/', true).'fba_shipments/documents/'.$packdata['FbaShipmentPacking']['documents2'].'" target="_blank" style="text-decoration:underline; color:#0000FF">'. $packdata['FbaShipmentPacking']['documents2'].'</a>&nbsp;&nbsp;<a href="'. Router::url('/', true).'FbaBoxing/RemoveDocument/'.$packdata['FbaShipmentPacking']['id'].'/documents2" title="Remove Document"><i class="fa fa-times" aria-hidden="true"  style="color:#FF0000"></i></a>';
											}else{
												echo 'No Document Uploaded';	
											}
											echo '</div>';
											
									 }?>	
									
 								</div>
							</div>
							
							<div class="row" style="padding-top:10px;">
								<div class="col-lg-12">	
								<div class="col-sm-7">&nbsp;&nbsp;</div>
								<div class="col-sm-3 text-right">	
								<?php $status = $obj->checkStatus($this->params['pass'][0]); ?>
								<input type="checkbox" name="mark_status" id="mark_status" value="exported" <?php if($status == 'exported') echo 'checked="checked"';?> />&nbsp;<label for="mark_status">Mark Status as Exported</label>	
								</div>											
									<div class="col-sm-2 text-right">											
									 <?php
											echo $this->Form->button('Save All Data', array(
												'type' => 'submit',
												'id' => 'save_all_values',
												'name' => 'save_all_values',
												'value' => 'save_all_values',
												'escape' => true,
												'class'=>'btn btn-info btn-sm'
												 ));	
										?>
									 </div>
								</div>
							</div>
							
							
						</div>		
						</form>		
						<div class="row"><div class="col-sm-8"></div><div class="col-sm-4" id="msg"></div></div>	
							
						
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script language="javascript">	
<?php 
if(isset($packdata['FbaShipmentPacking']) && $packdata['FbaShipmentPacking']['no_boxes'] > 0){
	echo 'shippingBoxes();';
}
?>
 
showPcs();
function showPcs()
{	if($("input[name='shipping_type']:checked").val() == 'pcs')
	{ 
		$("#div_pallet").show() ; 
	}else{
		$("#div_pallet").hide() ; 
		$("#no_pallets").val(0) ; 
		
	}
}
function chkShippingType()
{	showPcs();
}
 

function checkBoxDim(){ 
	var total_box = 0;	
	$("#box_dim_length").val() ;  
	$("#box_dim_width").val() ;  
	$("#box_dim_height").val() ; 
	 $(".box_dim").each(function(){ 	
		if($(this).is(':checked')) {
			total_box++;
		}	 	
	});	 
	if(total_box >1){
		$("#total_box").text(total_box+' Boxes') ; 
	}else{
		$("#total_box").text(total_box+' Box') ; 
	}
}

function checkBoxWeight(){ 
	var box_weight = 0;	
	$(".box_weight").each(function(){ 	
		if($(this).val() > 0) {
			box_weight += parseInt($(this).val()); 
		}	 	
	});	 
	$("#total_box_weight").text(box_weight+' lb') ; 
}
function checkBoxes( _this ){ 
	var v = parseInt(0);
	var id = $(_this).attr("for"); 
	$(".box_qty_"+id).each(function(){ 	
		if($(this).val() > 0) {
			v += parseInt($(this).val()); 	
		}	
	});
	
	$("#boxed_qty_"+id).text(v) ;  
	 
	var total_qty = parseInt(0);	
	$(".total_qty").each(function(){ 	
		total_qty += parseInt($(this).text()); 			 	
	});
	$("#total_qty").text(total_qty) ;   
}

$("#save_box_values").click(function(){	
	 
	var formData = new FormData($('#shipment_packing_frm')[0]);

		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaBoxing/SaveShipmentPacking',
		type: "POST",
		async: false,		
		cache: false,
		contentType: false,
		processData: false,
		data : formData,
		success: function(data, textStatus, jqXHR)
		{		
				console.log(data.msg);	
				//if(data.error){
					alert(data.msg);
				//}						
		}		
	});		 
});

function shippingBoxes(){
	$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaBoxing/shippingBoxes',
			type: "POST",				
			cache: false,			
			data : {shipment_inc_id: $('#shipment_inc_id').val(),no_boxes:$('#no_boxes').val(), no_pallets:$('#no_pallets').val(),shipment_id:$('#shipment_id').val(),shipping_type:$("input[name='shipping_type']:checked").val()},
			success: function(data, textStatus, jqXHR)
			{			
				 
				$("#grid").html(data.data);
				 
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				}											
			} 
		}); 	
}

 

</script>
