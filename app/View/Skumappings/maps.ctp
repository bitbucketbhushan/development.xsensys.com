<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-top:1px solid #666666; padding:0px;} 
	.body div.sku div{padding:4px 3px; font-size:12px;} 
	/*.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.pl-2{padding-left:4px;}
	.fa-spin-custom, .glyphicon-spin {
    -webkit-animation: spin 1000ms infinite linear;
    animation: spin 1000ms infinite linear;
}@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
@keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
	</style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title">Channel SKU Mapping</h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?>
					
				</div>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							
							 
							<div class="row">	
								
								<form method="post" name="mapFrm" id="mapFrm" class="form-horizontal" enctype="multipart/form-data" action="<?php echo Router::url('/', true) ?>Skumappings/UploadMaps">				 		
								
								<div class="col-lg-4">
									<label>Bulk Add and Update</label>								
									<div class="input-group">
									<span class="input-group-btn">
										<span class="btn btn-primary btn-file">
											Browse... <input type="file" class="form-control" id="upload" name="upload"  onchange="document.mapFrm.submit();"/>
									
										</span>
									</span>
									<input type="text" placeholder="No file selected" readonly="" class="form-control">
									</div>
									 <small style="font-size:9px;"><a href="<?php echo Router::url('/', true) ?>sample_files/skumappings_sample.csv" style="color:#FF0000;">Upload Sample File</a></small>
								</div> 
								</form>
								<div  class="col-lg-4" style="text-align:right; padding-top:25px;">
 									<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus"></i>&nbsp;ADD ASIN & CH. SKU</button></div>
								
								<div class="col-lg-4" style="text-align:right; padding-top:25px;">
									 
									 <div class="col-70">
										<select type="text" class="form-control" id="ch" name="ch">
										 <option value="all"> All Channels</option>
										 <option value="CostBreaker" <?php if(isset($_REQUEST['ch']) && ($_REQUEST['ch'] == 'CostBreaker')) echo 'selected=selected'?>> All CostBreaker</option>
										 <option value="Marec"<?php if(isset($_REQUEST['ch']) && ($_REQUEST['ch'] == 'Marec')) echo 'selected=selected'?>> All Marec</option>
										 <option value="Rainbow"<?php if(isset($_REQUEST['ch']) && ($_REQUEST['ch'] == 'Rainbow')) echo 'selected=selected'?>> All Rainbow</option>
										<?php foreach( $all_stores as $getStore ) { ?>
										<option value="<?php echo trim($getStore['SourceComany']['channel_title']); ?>" <?php if(isset($_REQUEST['ch']) && ($_REQUEST['ch'] == trim($getStore['SourceComany']['channel_title']))) echo 'selected=selected'?>><?php echo $getStore['SourceComany']['channel_title']; ?>(<?php echo strtoupper( $getStore['SourceComany']['channel_type']); ?>)</option>
										<?php } ?>														
									 </select>
									 
									 </div>									 
									  <div class="col-15">
										 <a href="javascript:void(0);" class="btn btn-info filter" role="button" title="Filter Data"><i class="fa fa-filter"></i></a>
									 </div>
									 <div class="col-15">									 
										 <a href="javascript:void(0);" class="btn btn-primary downloadfeed" role="button" title="Download Feed" ><i class="glyphicon glyphicon-download"></i></a>									
									 </div>								
								</div>
							</div>	
							
							
							
						</div>	
						
								
							
					<div class="panel-body no-padding-top bg-white">
						<!--<div class="adbtn" style="float:left">
									<a class="btn btn-success" href="javascript:void(0);" onclick="showDiv('add_div');"><i class="glyphicon glyphicon-plus"></i>&nbsp;ADD ASIN & CH. SKU</a>
						</div>-->
						
						<form method="get" name="search" id="searchfrm" action="<?php echo Router::url('/', true) ?>Skumappings/maps">				 		

								<div class="col-lg-3" style="margin-left:-15px">
									<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Enter Search Keyword..." name="searchkey" class="form-control searchString" />
								</div>
								
								<div class="col-lg-3"> 
									<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
								</div>
						</form>
						
						
						<div class="panel-tools" style="margin-bottom: 10px;clear: right;text-align: center; float: right;">
							<ul class="pagination">
							<?php
							   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
							   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							?>
							</ul>
						</div> 
 						<div id="myModal" class="modal fade" role="dialog">
					  <div class="modal-dialog modal-lg">
					
						<!-- Modal content-->
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Add SKU and Channel sku details</h4>
						  </div>
						  <div class="modal-body">
						  
						  
							<div class="form-group row">
								<label for="source" class="col-sm-2 col-form-label">Source</label>
								<div class="col-sm-10">
										 <select class="form-control" name="source" id="source">
											<option value="amazon">Amazon</option>
											<option value="ebay">eBay</option>
											<option value="realde">Real DE</option>
											<option value="CDISCOUNT">CDISCOUNT</option>
											 <option value="Onbuy">Onbuy</option>
										</select>
								</div>
							</div>
						  
							<div class="form-group row">
								<label for="sub_source" class="col-sm-2 col-form-label">SubSource</label>
								<div class="col-sm-10">
								 <select type="text" class="form-control" id="sub_source" name="sub_source">
									 <option value="all"> All SubSource</option>
									<?php foreach( $all_stores as $getStore ) { ?>
									<option value="<?php echo trim($getStore['SourceComany']['channel_title']); ?>" <?php if(isset($_REQUEST['ch']) && ($_REQUEST['ch'] == trim($getStore['SourceComany']['channel_title']))) echo 'selected=selected'?>><?php echo $getStore['SourceComany']['channel_title']; ?>(<?php echo strtoupper( $getStore['SourceComany']['channel_type']); ?>)</option>
									<?php } ?>														
								 </select>
								</div>
							</div>
							<!--<div class="form-group row">
								<label for="channel_type" class="col-sm-2 col-form-label">Channel Type</label>
								<div class="col-sm-10">
										<select class="form-control" name="channel_type" id="channel_type">
										<option value="fba">FBA</option>
										<option value="merchant">Merchant</option>
										</select>
								</div>
							</div>-->
							<div class="form-group row">
								<label for="channel_type" class="col-sm-2 col-form-label">SKU</label>
								<div class="col-sm-10">
									<input name="sku" title="SKU" placeholder="Enter SKU" type="text" class="form-control txt">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="channel_type" class="col-sm-2 col-form-label">ASIN</label>
								<div class="col-sm-10">
									<input id="asin" name="asin" title="ASIN" placeholder="Enter ASIN" type="text" class="form-control txt">
								</div>
							</div>
							<!--<div class="form-group row">
								<label for="channel_sku" class="col-sm-2 col-form-label">Channel SKU</label>
								<div class="col-sm-10">
									<input id="channel_sku" name="channel_sku" title="Channel SKU" placeholder="Enter Channel SKU" type="text" class="form-control txt">
								</div>
							</div>-->
							
							<div class="form-group row">
								<label for="channel_sku_title" class="col-sm-2 col-form-label">Channel Sku Title</label>
								<div class="col-sm-10">
									<input id="channel_sku_title" name="channel_sku_title" title="Channel Sku Title" placeholder="Enter Channel Sku Title" type="text" class="form-control txt">
								</div>
							</div>
  
							<div class="form-group row">
								<label for="referral_fee" class="col-sm-2 col-form-label">Referral Fee</label>
								<div class="col-sm-10">
									<input name="referral_fee" title="Referral Fee" placeholder="Enter Referral Fee" type="text" class="form-control txt"></div>
							</div>
							<div class="form-group row">
								<label for="min_referral_fee" class="col-sm-2 col-form-label">Min Referral Fee</label>
								<div class="col-sm-10">
									<input name="min_referral_fee" title="Min Referral Fee" placeholder="Enter Min Referral Fee" type="text" class="form-control txt"></div>
							</div>
							<div class="form-group row">
								<label for="margin" class="col-sm-2 col-form-label">Margin</label>
								<div class="col-sm-10">
									<input id="margin" name="margin" title="Margin" placeholder="Enter Margin" type="text" class="form-control txt">
								</div>
							</div>
 
							<div class="form-group row">
								<label for="fba_fee" class="col-sm-2 col-form-label">FBA Fees</label>
								<div class="col-sm-10">
									<input id="fba_fee" name="fba_fee" title="FBA Fees" placeholder="Enter FBA Fees" type="text" class="form-control txt">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="light_programme_fba" class="col-sm-2 col-form-label">SmallLightProgramme-FBA</label>
								<div class="col-sm-10">
									<input id="light_programme_fba" name="light_programme_fba" title="SmallLightProgramme-FBA" placeholder="Enter SmallLightProgramme-FBA" type="text" class="form-control txt">
								</div>
							</div>
							<div class="form-group row">
								<label for="variable_fee" class="col-sm-2 col-form-label">Variable Fee </label>
								<div class="col-sm-10">
									<input id="variable_fee" name="variable_fee" title="Variable Fee" placeholder="Enter Variable Fee" type="text" class="form-control txt">
								</div>
							</div>
							
 							
							<div class="form-group row">
								<label for="listing_status" class="col-sm-2 col-form-label">Listing Status</label>
								<div class="col-sm-10">
										 <select class="form-control" name="listing_status" id="listing_status">
											<option value="active">Active</option>
											<option value="inactive">InActive</option> 
										</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="stock_status" class="col-sm-2 col-form-label">Stock Status</label>
								<div class="col-sm-10">
										 <select class="form-control" name="stock_status" id="stock_status">
											<option value="npa">npa</option>
											<option value="instock">instock</option>
											<option value="out of stock">out of stock</option>
											<option value="over stock">over stock</option>
										</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="margin" class="col-sm-2 col-form-label">&nbsp;</label>
								<div class="col-sm-2"><button type="button" class="btn btn-warning" onclick="saveData();"><i class="glyphicon glyphicon-check"></i>&nbsp;Save</button></div>
								<div class="col-sm-7 save-msg"></div>
							</div>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						  </div>
						</div>
					
					  </div>
					</div>
					
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-16">SKU/<br />Title</div>		
								<div class="col-15">Channel Sku/<br />ASIN</div>		
 								<!--<div class="col-18">Title</div>	
								<div class="col-10">Source</div>-->	
								<div class="col-11">SubSource</div>	
								<div class="col-6">Channel Type</div>
								<div class="col-5">Referral Fee %</div>
								<div class="col-5">Min Referral Fee</div>
								<div class="col-6">Margin %</div>
 								<div class="col-5">FBA Fees</div>
								<div class="col-5">Variable Fees</div>								
								<div class="col-6">SmallLight Programme - FBA</div>
								<div class="col-5">Listing Status</div>
								<div class="col-5">Stock Status</div>
								<div class="col-5">Action</div>								
 							</div>								
						</div>	
							
						<div class="body" id="results">
						<div id="p_1"></div>
						<?php if(count($all_product) > 0){
  						foreach( $all_product as $product ) { 
 						///$channel = $channelArray[$product['Skumapping']['channel_name']];
 						?>
 						<div class="row sku"> 
							<div class="col-lg-12">
								<div class="col-16">
								
								<section><span class="xedit" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="sku" data-title="SKU"  data-value="<?php echo $product['Skumapping']['sku']; ?>"><?php echo $product['Skumapping']['sku']; ?></span></section>
								
 								<br /><section><span class="xedit" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="channel_sku_title" data-title="Title" data-value="<?php echo $product['Skumapping']['channel_sku_title']; ?>"><?php echo $product['Skumapping']['channel_sku_title']; ?></span></section></div>
								<div class="col-15"> 
								
								<section><span class="xedit" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="channel_sku" data-title="Channel SKU"  data-value="<?php echo $product['Skumapping']['channel_sku']; ?>"><?php echo $product['Skumapping']['channel_sku']; ?></span></section>
								
								 <br />
								<span title="ASIN"><?php echo trim($product['Skumapping']['asin']); ?></span></div>		
 								<?php /*?><div class="col-18"><section><span class="xedit" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="channel_sku_title" data-title="Title" data-value="<?php echo $product['Skumapping']['channel_sku_title']; ?>"><?php echo $product['Skumapping']['channel_sku_title']; ?></span></section></div>		<?php */?>
								<!--<div class="col-10"><section><span class="xedit source" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="source" data-title="Source" data-type="select" data-value="<?php echo $channel['source']; ?>"><?php echo $channel['source']; ?></span></section></div>
							-->	
								<div class="col-11"><section><span class="xedit sub_source" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="channel_name" data-title="Sub Source" data-type="select" data-value="<?php echo $product['Skumapping']['channel_name']; ?>"><?php echo $product['Skumapping']['channel_name']; ?></span></section> </div>							 
								<div class="col-6"><section><span class="xedit channel_type" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="channel_type" data-title="Channel Type" data-type="select"  data-value="<?php echo $product['Skumapping']['channel_type']; ?>" ><?php echo $product['Skumapping']['channel_type']; ?></span></section> </div>
								
								<div class="col-5"><section><span class="xedit" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="referral_fee" data-title="Referral fee" data-value="<?php echo $product['Skumapping']['referral_fee']; ?>"><?php echo trim($product['Skumapping']['referral_fee']); ?></span></section></div>
								
								<div class="col-5"><section><span class="xedit" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="min_referral_fee" data-title="Min Referral fee" data-value="<?php echo $product['Skumapping']['min_referral_fee']; ?>"><?php echo trim($product['Skumapping']['min_referral_fee']); ?></span></section></div>
								<div class="col-6"><section><span class="xedit" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="margin" data-title="Margin" data-value="<?php echo $product['Skumapping']['margin']; ?>"><?php echo trim($product['Skumapping']['margin']); ?></span></section></div>
								<div class="col-5"><section><span class="xedit" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="fba_fee" data-title="fba_fee" data-value="<?php echo $product['Skumapping']['fba_fee']; ?>"><?php echo trim($product['Skumapping']['fba_fee']); ?></span></section></div>
								
								<div class="col-6"><section><span class="xedit" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="variable_fee" data-title="variable_fee" data-value="<?php echo $product['Skumapping']['variable_fee']; ?>"><?php echo trim($product['Skumapping']['variable_fee']); ?></span></section></div>
								
 								<div class="col-6"><section><span class="xedit" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="light_programme_fba" data-title="SmallLightProgramme-FBA" data-value="<?php echo $product['Skumapping']['light_programme_fba']; ?>"><?php echo trim($product['Skumapping']['light_programme_fba']); ?></span></section></div>
								
								<div class="col-5"><section><span class="xedit listing-status" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="listing_status" data-title=" Listing status" data-type="select" data-value="<?php echo $product['Skumapping']['listing_status']; ?>"><?php echo $product['Skumapping']['listing_status']; ?></span></section></div>
								<div class="col-5"><section><span class="xedit stock-status" pk="<?php echo $product['Skumapping']['id']; ?>" data-name="stock_status" data-title="Stock status" data-type="select" data-value="<?php echo $product['Skumapping']['stock_status']; ?>"><?php echo $product['Skumapping']['stock_status']; ?></span></section></div>
								
								<div class="col-5"><a href="<?php echo Router::url('/', true) ?>Skumappings/DeleteMap/<?php echo $product['Skumapping']['id']; ?>" type="button" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete this?')">Delete</a>
</div>	
							</div>
							 						
						</div>
						
						<?php }?>
						<?php }else{?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php } ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
						
			</div>			
		
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>
 
<script>	
function prefilled(){
 	$(".outerOpac").show();		 	
	$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>Skumappings/Prefilled',
				type: "POST",				
				cache: false,			
				data : {channel:$("#sub_source option:selected").val()},
				success: function(data, textStatus, jqXHR)
				{	//alert(data.data);	
					$(".outerOpac").hide();		
					$("#channel_sku").val(data.prefix); 
  			}		
		 });
}

jQuery('.listing-status').editable({ 
		value:"active",
		source:[{value:"active", text:"active"},
			{value:"inactive", text:"inactive"} 
		],
		validate: function(value) {if($.trim(value) == "") return "This field is required"; } }
);
jQuery('.stock-status').editable({ 
		value:"npa",
		source:[{value:"npa", text:"npa"},
			{value:"instock", text:"instock"},
			{value:"out of stock", text:"out of stock"},
			{value:"over stock", text:"over stock"}  
 		],
		validate: function(value) {if($.trim(value) == "") return "This field is required"; } }
);
jQuery('.source').editable({ 
		value:"amazon",
		source:[{value:"amazon", text:"Amazon"},
			{value:"ebay", text:"eBay"},
			{value:"realde", text:"Real DE"},
			{value:"CDISCOUNT", text:"CDISCOUNT"},
			{value:"Onbuy", text:"Onbuy"}
		],
		validate: function(value) {if($.trim(value) == "") return "This field is required"; } }
);
 										
jQuery('.sub_source').editable({ 
		value:"CostBreaker_DE",
		source:[ 
 			<?php foreach( $all_stores as $getStore ) { ?> 
			<?php echo '{value:"'.$getStore['SourceComany']['channel_title'].'", text:"'.$getStore['SourceComany']['channel_title'].'"},';  ?>
			<?php } ?>	
		],
		validate: function(value) {if($.trim(value) == "") return "This field is required"; } }
);
jQuery('.channel_type').editable({ 
		 
		source:[{value:"FBA", text:"FBA"},
			{value:"Merchant", text:"Merchant"} 
 		],
		validate: function(value) {if($.trim(value) == "") return "This field is required"; } }
);
jQuery(document).ready(function() {  
       // $.fn.editable.defaults.mode = 'popup';
        //jQuery('.xedit').editable();		
		$(document).on('click','.editable-submit',function(){
			var x = $(this).closest('section').children('span').attr('pk');			
			var y = $('.input-sm').val();
			var z = $(this).closest('section').children('span'); 
			var fname = $(this).closest('section').children('span').attr('data-name');
						
			$.ajax({
				url: "<?php echo Router::url('/', true) ?>Skumappings/Update",
				type: "POST",
				data : {value:y,pk:x,name:fname},
				success: function(s){
					if(s == 'status'){
					$(z).html(y);}
					if(s == 'error') {
					alert('Error Processing your Request!');}
				},
				error: function(e){
					alert('Error Processing your Request!!');
				}
			});
		});
		
		
});

jQuery('.xedit').editable();

$(".filter").click(function(){
 	window.location.href = '<?php echo Router::url('/', true) ?>Skumappings/maps?ch='+$("#ch option:selected").val();
 });

$(".downloadfeed").click(function(){	
	if($("#ch option:selected").val() == 'all'){
		alert("Please Select Channel");	
	}else{
		 
		$(".outerOpac").show();		 	
		$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>Skumappings/getMapCsv',
				type: "POST",				
				cache: false,			
				data : {channel:$("#ch option:selected").val()},
				success: function(data, textStatus, jqXHR)
				{	//alert(data.data);	
					$(".outerOpac").hide();		
					jQuery(".msg").html(data.file);
					//jQuery(".msg").html(data.msg);
					//jQuery('.panel-body').html(data.data);
					//alert(data.msg);						
					$('#msg').delay(200000).fadeOut('slow');							
			}		
		 });
	 }
		 
});

$( ".searchString" ).on( "keydown", function(event) {
	
	 if(event.which == 13) {
	 	if($(".searchString").val()){
			 //ajaxSearch();
		 }else{
			 alert('Enter Search Keyword.');
			// event.preventDefault();
		 }
		// event.preventDefault();
	}
	
	 
	
});

$(".searchKey").click(function(){	
	if($(".searchString").val()){
		//ajaxSearch();
	}else{
		alert('Enter Search Keyword.');
		//event.preventDefault();
	}
		 
});
function ajaxSearch(){	
	 <?php /*?>  $("#mapFrm").attr("action", "<?php echo Router::url('/', true) ?>Skumappings/Search");
	   $("#mapFrm").submit();<?php */?>
	   
	$(".outerOpac").show();		 	
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Skumappings/Search',
			type: "POST",				
			cache: false,			
			data : {searchkey:$(".searchString").val()},
			success: function(data, textStatus, jqXHR)
			{	//alert(data.data);	
				$(".outerOpac").hide();		
				//jQuery(".msg").html(data.msg);
				jQuery('.panel-body').html(data.data);
					
				//$('#message').delay(20000).fadeOut('slow');							
		}		
	 });		 
}

 function delCh(key){
 	$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Skumappings/Delete',
			type: "POST",				
			cache: false,			
			data : {id:key},
			success: function(data, textStatus, jqXHR)
			{	//alert(data.data);	
				$(".outerOpac").hide();		
				jQuery(".msg").html(data.msg);
				jQuery("#r_"+key).remove();	
				//$('#message').delay(20000).fadeOut('slow');							
		}		
	 });
 }
 
$("#upload3").change(function(){	
	
	$(".outerOpac").show();		
	var formData = new FormData($('#mapFrm')[0]);
		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Skumappings/UploadMaps',
		type: "POST",		 	
		cache: false,
		contentType: false,
		processData: false,
		data : formData,
		success: function(data, textStatus, jqXHR)
		{		
			$(".outerOpac").hide();		
			jQuery(".msg").html(data.msg);
				
			//$('#message').delay(20000).fadeOut('slow');							
		}		
	});		 
});
 

function saveData() {	
	
	if($("#sub_source option:selected").val() == 'all' ){
		alert("Please select sub source!");
		$( "#sub_source" ).focus();
		document.getElementById("sub_source").focus();
		return false;
	}else{
		var inputValues = [];
		jQuery('.modal-body .txt').each(function() {           
			inputValues.push($(this).attr('name')+'='+$(this).val());
		})	
 		jQuery.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>Skumappings/Savesku',
				type: "POST",
				data : {values:inputValues,source:$("#source option:selected").val(),sub_source:$("#sub_source option:selected").val(),channel_type:$("#channel_type option:selected").val(),listing_status:$("#listing_status option:selected").val(),stock_status:$("#stock_status option:selected").val()},
				success: function(data, textStatus, jqXHR)
				{		
					jQuery(".save-msg").show();		
					if(data.status == 'ok'){		
						jQuery(".save-msg").html('<div class="alert alert-success" role="alert">'+data.msg+'</div>');
					}else{
						jQuery(".save-msg").html('<div class="alert alert-danger" role="alert">'+data.msg+'</div>');
					}
					 
				}		
			});
		}
	
}
</script>
