<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-top:1px solid #666666; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:12px;} 
	/*.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.fa-spin-custom, .glyphicon-spin {
    -webkit-animation: spin 1000ms infinite linear;
    animation: spin 1000ms infinite linear;
}@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
@keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
	</style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?>
					
				</div>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							
							<form method="post" name="mapFrm" id="mapFrm" enctype="multipart/form-data">	 
							<div class="row">	
								
													
								<div class="col-lg-3">
									 <div style="width: 60%;float: left;margin-right: 1px;">
										<select type="text" class="form-control" id="ch" name="ch">
										 <option value="all"> All Channels</option>
										<?php foreach( $all_stores as $getStore ) { ?>
										<option value="<?php echo trim($getStore['Store']['store_name']); ?>" <?php if(isset($_REQUEST['ch']) && ($_REQUEST['ch'] == trim($getStore['Store']['store_name']))) echo 'selected=selected'?>><?php echo $getStore['Store']['store_name']; ?></option>
										<?php } ?>														
									 </select>
									 </div>									 
									  <div style="float:left; padding:0 2% 0 1%;">
										 <a href="javascript:void(0);" class="btn btn-info filter" role="button" title="Filter Data"><i class="fa fa-filter"></i></a>
									 </div>
									 <div style="float:left;">									 
										 <a href="javascript:void(0);" class="btn btn-primary downloadfeed" role="button" title="Download Feed"><i class="glyphicon glyphicon-download"></i></a>									
									 </div>								
								</div>
								<div class="col-lg-3">
									 <input type="file" class="form-control" id="upload" name="upload" />
								</div>
							
								<div class="col-lg-3">
									<input type="text" value="<?php echo isset($_POST['searchkey']) ? $_POST['searchkey'] :''; ?>" placeHolder="Enter Search Keyword..." name="searchkey" class="form-control searchString" />
								</div>
								
								<div class="col-lg-3"> 
									<button type="button" class="btn btn-success searchKey"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>&nbsp; &nbsp;<button type="button" class="btn btn-warning" onclick="window.location.href='<?php echo Router::url('/', true) ?>Skumappings'"><i class="glyphicon glyphicon-refresh"></i>&nbsp;Refresh</button>
								</div>
							</div>	
							</form>
							<div class="row" style="margin-top:5px; padding:10px 0px; background-color:#DDDDDD; border-radius:5px;width: 94%;
margin-left: 0px;">	
								<div class="col-lg-3">
									<label for="top_qty_percent">% Qty:</label> 
									<input type="text" class="form-control" placeholder="Percent Qty" id="top_qty_percent" value="<?php print $channel_min['qty_percent'] ?>" style="width:80%; float:right;" />
									
								</div>
								<div class="col-lg-3">
									<label for="top_min_qty">Min Qty:</label>
									<input type="text" class="form-control" placeholder="Min Qty" id="top_min_qty" value="<?php print $channel_min['min_qty'] ?>" style="width:74%; float:right;"/>
									
								</div>
								<div class="col-lg-2">		
									<label for="top_max_qty">Max Qty:</label>							
									<input type="text" class="form-control" placeholder="Max Qty" id="top_max_qty" value="<?php print $channel_min['max_qty'] ?>" style="width:58%; float:right;" />
										
								</div>
								<div class="col-lg-2">	
									<label for="top_buffer_qty">Buffer:</label>								
									<input type="text" class="form-control" placeholder="buffer Qty" id="top_buffer_qty" value="<?php print $channel_min['buffer'] ?>" style="width:68%; float:right;"/>
									
								</div>
								<div class="col-lg-2">
									<button type="button" class="btn btn-primary btn-block update-qty"><i class="glyphicon glyphicon-check"></i>&nbsp;Save</button>
								</div>
							</div>
							
						</div>	
						
								
							
					<div class="panel-body no-padding-top bg-white">
							
						<div class="panel-tools" style="margin-bottom: 10px;clear: right;text-align: center; float: right;">
							<ul class="pagination">
							<?php
							   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
							   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							?>
							</ul>
						</div> 
					
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-sm-2">SKU</div>		
								<div class="col-sm-5">Title</div>		
								<div class="col-sm-2">Barcode</div>
								<div class="col-sm-1">Stock</div>								
								<div class="col-sm-2">&nbsp;</div>
							</div>								
						</div>	
							
						<div class="body" id="results">
						<?php if(count($all_product) > 0){?>
							<?php foreach( $all_product as $product ) { ?>
						<div class="row sku">
							<div class="skudiv col-lg-12">
								<div class="col-sm-2"><?php echo $sku = trim($product['Product']['product_sku']); ?></div>		
								<div class="col-sm-5"><?php echo trim($product['Product']['product_name']); ?></div>		
								<div class="col-sm-2"><?php echo $product['ProductDesc']['barcode']?></div>
								<div class="col-sm-1">Stock:<?php echo $product['Product']['current_stock_level']?></div>							 
								<div class="col-sm-2">	
								
									<div class="adbtn">
									<a class="btn btn-success" href="javascript:void(0);" onclick="addMoreRows('<?php echo trim($product['Product']['id']); ?>','<?php echo $sku; ?>');"><i class="glyphicon glyphicon-plus"></i>&nbsp;ADD ASIN & CH. SKU</a>
									</div>	
								</div>	
							</div>
							<div class="col-lg-12">	
							
							<?php 
									
									$skuPercentArray = $sku_percent;
									$pid = '';
									echo'<div class="options" id="row_'.trim($product['Product']['id']).'"></div>';
									
							?>						
								<?php if(isset($mappped_sku[$sku])){
								
											$pid = $product['Product']['id'];
											echo'<div class="col-lg-12" id="p_'.$pid.'">';										
											echo '<div class="col-sm-1"><strong>ASIN</strong></div>';
											echo '<div class="col-sm-2"><strong>Channel Name</strong></div>';
											echo '<div class="col-sm-2"><strong>Channel SKU</strong></div>';
											echo '<div class="col-sm-3"><strong>Channel SKU Title</strong></div>';
											echo '<div class="col-sm-3">';
												echo '<div class="w10" style="width:18%; float: left;text-align:center;"><strong>Buffer</strong></div>';
												echo '<div class="w10" style="width:20%; float: left;text-align:center;"><strong>% Q</strong></div>';				
												echo '<div class="w10" style="width:20%; float: left;text-align:center;"><strong>Min Q</strong></div>';
												echo '<div class="w10" style="width:20%; float: left;text-align:center;"><strong>Max Q</strong></div>';
												echo '<div class="w10" style="width:20%; float: left; text-align:center;"><strong>Q.T.R</strong></div>';		
											echo '</div>';
																				
											echo '<div class="col-sm-1">';
												echo '<div class="w10" style="width:60%; float: left;text-align:center;"><strong>Status</strong></div>';
												echo '<div class="w10" style="width:40%; float: left;text-align:center;"><strong>Action</strong></div>';
											echo'</div>';
										//	echo '<div class="col-sm-1"><strong>Xsensys Status</strong></div>';
											//echo '<div class="col-sm-1"><strong>Action</strong></div>';											
											echo'</div>	';
											
										foreach($mappped_sku[$sku] as $val){ 
										
										
										   // print_r($OpenOrder[$sku][$val['channel_sku']]);
											//$openQty = isset($SkuOpenOrder[$sku]) ? $SkuOpenOrder[$sku] : 0;
										
											echo'<div class="col-lg-12" id="r_'.$val['id'].'">';
										
											echo '<div class="col-sm-1"><section><span class="xedit" pk="'.$val['id'].'" data-name="asin" data-title="Asin" data-value="'.$val['asin'].'">'.$val['asin'].'</span></section></div>';
											echo '<div class="col-sm-2">'.$val['channel_name'].'</div>';
											
											echo '<div class="col-sm-2"><section><span class="xedit" pk="'.$val['id'].'" data-name="channel_sku" data-title="Channel SKU" data-value="'.$val['channel_sku'].'">'.$val['channel_sku'].'</span></section></div>';
											
											echo '<div class="col-sm-3"><section><span class="xedit" pk="'.$val['id'].'" data-name="channel_sku_title" data-title="Channel SKU Title" data-value="'.$val['channel_sku_title'].'">'.$val['channel_sku_title'].'</span></section></div>';
											
											echo '<div class="col-sm-3">';
											
												echo '<div class="w10" style="width:18%; float: left;text-align:center;"><section><span class="xedit" pk="'.$val['id'].'" data-name="buffer" data-title="Buffer Qty" data-value="'.$val['buffer'].'">'.$val['buffer'].'</span></section></div>';
												echo '<div class="w10" style="width:20%; float: left;text-align:center;"><section><span class="xedit" pk="'.$val['id'].'" data-name="qty_percent" data-title="Qty Percent" data-value="'.$val['qty_percent'].'">'.$val['qty_percent'].'</span></section></div>';											
												echo '<div class="" style="width:20%; float: left;text-align:center;"><section><span class="xedit" pk="'.$val['id'].'" data-name="min_qty" data-title="Min Qty" data-value="'.$val['min_qty'].'">'.$val['min_qty'].'</span></section></div>';
												echo '<div class="" style="width:20%; float: left;text-align:center;"><section><span class="xedit" pk="'.$val['id'].'" data-name="max_qty" data-title="Max Qty" data-value="'.$val['max_qty'].'">'.$val['max_qty'].'</span></section></div>';
												$qtyToReport = 0; $qty = 0; $pty = 0;											
												if(isset($product['Product']['current_stock_level']) && $product['Product']['current_stock_level'] > 0){
													//$qty =  floor(($product['Product']['current_stock_level'] - $openQty - $buffer) * $val['qty_percent']/100 );
													$pty =  floor(($product['Product']['current_stock_level'] - $val['buffer']) * $val['qty_percent']/100 );
													$qty = ($product['Product']['current_stock_level'] - $val['buffer']) ;
													if(($qty > 0) && ($qty < $val['min_qty'])){
													
														if($val['performance_status'] == 'fast_moving'){
															 $qtyToReport = $qty;
														}else if($val['performance_status'] == 'slow_moving'){
															 $qtyToReport = $qty;
														}else{
															$qtyToReport = 0;
														}
																			
													}elseif($qty > $val['max_qty']){
														$qtyToReport = $val['max_qty'];
													}else{
														$qtyToReport = $pty > 0 ? $pty : 0;
													}
												}											
												echo '<div class="" style="width:20%; float: left;text-align:center;">'.$qtyToReport .'</div>';
													
											echo '</div>';
																	
											echo '<div class="col-sm-1">';
												echo '<div class="w10" style="width:60%; float: left;text-align:center;"><section><span class="xedit" pk="'.$val['id'].'" data-name="amazon_status" data-title="Status" data-value="'.$val['amazon_status'].'">'.$val['amazon_status'].'</span></section></div>';
												
												echo '<div class="w10" style="width:40%; float: left; text-align:center;"><i class="glyphicon glyphicon-remove delete" onclick="delCh('.$val['id'].');"></i></div>';
											echo '</div>';
																					
											//echo '<div class="col-sm-1"><section><span class="xedit" pk="'.$val['id'].'" data-name="xsensys_status" data-title="Xsensys Status" data-value="'.$val['xsensys_status'].'">'.$val['xsensys_status'].'</span></section></div>';
											//echo '<div class="col-sm-1"><i class="glyphicon glyphicon-remove delete" onclick="delCh('.$val['id'].');"></i></div>';
											
											echo'</div>	';
										}
										
								}else{
									echo'<div class="col-lg-12" id="p_'.$pid.'">&nbsp;</div>';		
								}
								
								?>								
								
								
								
							</div>						
						</div>
						
						<?php }?>
						<?php }else{?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php } ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
						
			</div>			
		
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>
 
<script>	

jQuery(document).ready(function() {  
       // $.fn.editable.defaults.mode = 'popup';
        //jQuery('.xedit').editable();		
		$(document).on('click','.editable-submit',function(){
			var x = $(this).closest('section').children('span').attr('pk');			
			var y = $('.input-sm').val();
			var z = $(this).closest('section').children('span'); 
			var fname = $(this).closest('section').children('span').attr('data-name');
						
			$.ajax({
				url: "<?php echo Router::url('/', true) ?>Skumappings/Update",
				type: "POST",
				data : {value:y,pk:x,name:fname},
				success: function(s){
					if(s == 'status'){
					$(z).html(y);}
					if(s == 'error') {
					alert('Error Processing your Request!');}
				},
				error: function(e){
					alert('Error Processing your Request!!');
				}
			});
		});
		
		
});

jQuery('.xedit').editable();

$(".filter").click(function(){
	//if($("#ch option:selected").val() == 'all'){
	//	alert("Please Select Channel");	
	//}else{
		window.location.href = '<?php echo Router::url('/', true) ?>Skumappings/?ch='+$("#ch option:selected").val();
	//}
});

$(".downloadfeed").click(function(){	
	if($("#ch option:selected").val() == 'all'){
		alert("Please Select Channel");	
	}else{
		 
		$(".outerOpac").show();		 	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>Skumappings/getFeedCsv',
				type: "POST",				
				cache: false,			
				data : {channel:$("#ch option:selected").val()},
				success: function(data, textStatus, jqXHR)
				{	//alert(data.data);	
					$(".outerOpac").hide();		
					jQuery(".msg").html(data.file);
					//jQuery(".msg").html(data.msg);
					//jQuery('.panel-body').html(data.data);
					//alert(data.msg);						
					$('#msg').delay(200000).fadeOut('slow');							
			}		
		 });
	 }
		 
});

$( ".searchString" ).on( "keydown", function(event) {
	
	 if(event.which == 13) {
	 	if($(".searchString").val()){
			 ajaxSearch();
		 }else{
			 alert('Enter Search Keyword.');
			 event.preventDefault();
		 }
		 event.preventDefault();
	}
	
	 
	
});

$(".searchKey").click(function(){	
	if($(".searchString").val()){
		ajaxSearch();
	}else{
		alert('Enter Search Keyword.');
		event.preventDefault();
	}
		 
});
$(".update-qty").click(function(){
	
	var	error = false;			
	if($("#ch option:selected").val() == 'all'){
		alert("Please Select Channel");	
		$("#ch").focus();
		error = true;	
		return false;
	}else if($("#top_qty_percent").val() == ''){
		alert("Please Enter qty percent!");
		$("#top_qty_percent").focus();
		error = true;	
		return false;
	}else if($("#top_min_qty").val() == ''){
		alert("Please Enter min qty!");
		$("#top_min_qty").focus();
		error = true;	
		return false;
	}else if($("#top_max_qty").val() == ''){
		alert("Please Enter max qty!");
		$("#top_max_qty").focus();
		error = true;	
		return false;
	}
	
	if($("#top_qty_percent").val() != ''){
		if(!/^[0-9]+$/.test($("#top_qty_percent").val())){
			alert('Please enter integer value(Ex:60).');
			error = true;	
			return false;			
		}else if($("#top_qty_percent").val() > 100 ){
			alert('Please enter value between 1 to 100');
			error = true;	
			return false;	
		}	
		
	}
	
	if(error == false){
		
		$(".outerOpac").show();		
			$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Skumappings/UpdateQty',
			type: "POST",			
			cache: false,
			data : {channel:$("#ch option:selected").val(),qty_percent:$("#top_qty_percent").val(),min_qty:$("#top_min_qty").val(),max_qty:$("#top_max_qty").val(),buffer:$("#top_buffer_qty").val()},
			success: function(data, textStatus, jqXHR)
			{		
				$(".outerOpac").hide();		
				jQuery(".msg").html(data.msg);
				window.location.href = '<?php echo Router::url('/', true) ?>Skumappings/?ch='+$("#ch option:selected").val();	
				//$('#message').delay(20000).fadeOut('slow');							
			}		
		});	
	
	
	}
});
function ajaxSearch(){	
	 <?php /*?>  $("#mapFrm").attr("action", "<?php echo Router::url('/', true) ?>Skumappings/Search");
	   $("#mapFrm").submit();<?php */?>
	   
	$(".outerOpac").show();		 	
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Skumappings/Search',
			type: "POST",				
			cache: false,			
			data : {searchkey:$(".searchString").val()},
			success: function(data, textStatus, jqXHR)
			{	//alert(data.data);	
				$(".outerOpac").hide();		
				//jQuery(".msg").html(data.msg);
				jQuery('.panel-body').html(data.data);
					
				//$('#message').delay(20000).fadeOut('slow');							
		}		
	 });		 
}

 function delCh(key){
 	$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Skumappings/Delete',
			type: "POST",				
			cache: false,			
			data : {id:key},
			success: function(data, textStatus, jqXHR)
			{	//alert(data.data);	
				$(".outerOpac").hide();		
				jQuery(".msg").html(data.msg);
				jQuery("#r_"+key).remove();	
				//$('#message').delay(20000).fadeOut('slow');							
		}		
	 });
 }
 
$("#upload").change(function(){	
	
	$(".outerOpac").show();		
	var formData = new FormData($('#mapFrm')[0]);
		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Skumappings/Uploadcsv',
		type: "POST",		 	
		cache: false,
		contentType: false,
		processData: false,
		data : formData,
		success: function(data, textStatus, jqXHR)
		{		
			$(".outerOpac").hide();		
			jQuery(".msg").html(data.msg);
				
			//$('#message').delay(20000).fadeOut('slow');							
		}		
	});		 
});
var rowCount = 0;
function addMoreRows(id,sku) {
	rowCount ++;
	var recRow = '<div class="col-sm-12" id="rowCount'+rowCount+'">';	
		recRow += '<div class="col-sm-2"><input name="asin" title="ASIN" placeholder="Enter ASIN" type="text" class="form-control txt" />';
		recRow += '<input name="sku" type="hidden" value="'+sku+'" /></div>';
		recRow += '<div class="col-sm-2"><input name="channel_sku" title="CH. SKU" placeholder="Enter CH. SKU" type="text" class="form-control txt" /></div>';	
		recRow += '<div class="col-sm-2"><input name="channel_sku_title" title="Channel Sku Title" placeholder="Channel Sku Title" type="text" class="form-control txt" /></div>';
		recRow += '<div class="col-sm-2"><input name="qty_percent" title="% Qty" placeholder="Percent Qty" type="text" class="form-control txt" /></div>';
		recRow += '<div class="col-sm-1"><input name="min_qty" title="Min Qty" placeholder="Min Qty" type="text" class="form-control txt" /></div>';
		recRow += '<div class="col-sm-1"><input name="max_qty" title="Max Qty" placeholder="Max Qty" type="text" class="form-control txt" /></div>';
		recRow += '<div class="col-sm-2"><button type="button" class="btn btn-warning" id="sv_'+id+'"  onclick="saveRow('+rowCount+','+id+');"><i class="glyphicon glyphicon-check"></i>&nbsp;Save</button>&nbsp;<button type="button" class="btn btn-danger" onclick="removeRow('+rowCount+');"><i class="glyphicon glyphicon-remove"></i>&nbsp;Delete</button></div>';

	recRow += '</div>';
	
	jQuery('#row_'+id).append(recRow);
	//document.getElementById('row_'+id).innerHTML = recRow;
	jQuery('#row_'+id).show();
	//alert(jQuery('#row_'+id).html());
}

function removeRow(removeNum) {
	jQuery('#rowCount'+removeNum).remove();
}

function saveRow(Num,id) {	
	
	if($("#ch option:selected").val() == 'all' ){
		alert("Please select channel!");
		$( "#ch" ).focus();
		document.getElementById("ch").focus();
		return false;
	}else{
		var inputValues = [];
		jQuery('#rowCount'+Num+' input').each(function() {           
			inputValues.push($(this).attr('name')+'='+$(this).val());
		})	
		jQuery("#sv_"+ id +" i").addClass("glyphicon-spin glyphicon-refresh");
		jQuery("#sv_"+ id +" i").removeClass("glyphicon-check");
		jQuery.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>Skumappings/Mapsku',
				type: "POST",
				data : {values:inputValues,channel:$("#ch option:selected").val()},
				success: function(data, textStatus, jqXHR)
				{		
					jQuery(".msg").show();				
					jQuery("#sv_"+ id +" i").addClass("glyphicon-check");
					jQuery("#sv_"+ id +" i").removeClass("glyphicon-spin glyphicon-refresh");
					jQuery("#p_"+ id).after(data.msg);
					jQuery(".msg").html(data.msg);
				}		
			});
		}
	
}
</script>
