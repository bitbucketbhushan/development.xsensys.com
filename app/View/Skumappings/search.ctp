
							
						<div class="panel-tools" style="margin-bottom: 10px;clear: right;text-align: center; float: right;">
							<ul class="pagination">
							<?php
							   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
							   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							?>
							</ul>
						</div> 
					
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-sm-2">SKU</div>		
								<div class="col-sm-4">Title</div>		
								<div class="col-sm-2">Barcode</div>
								<div class="col-sm-1">Stock</div>
								<div class="col-sm-1">Selling Category</div>
								<div class="col-sm-2">Action</div>
							</div>								
						</div>	
							
						<div class="body" id="results">
						<?php if(count($all_product) > 0){?>
							<?php foreach( $all_product as $product ) { ?>
						<div class="row sku">
							<div class="skudiv col-lg-12">
								<div class="col-sm-2"><?php echo $sku = trim($product['Product']['product_sku']); ?></div>		
								<div class="col-sm-4"><?php echo trim($product['Product']['product_name']); ?></div>		
								<div class="col-sm-2"><?php echo $product['ProductDesc']['barcode']?></div>
								<div class="col-sm-1"><?php echo $product['Product']['current_stock_level']?></div>
								<div class="col-sm-1"><?php echo ucwords(str_replace('_',' ',$product['Product']['selling_category']))?></div>
								<div class="col-sm-2">	
								
									<div class="adbtn">
									<a class="btn btn-success" href="javascript:void(0);" onclick="addMoreRows('<?php echo trim($product['Product']['id']); ?>','<?php echo $sku; ?>');"><i class="glyphicon glyphicon-plus"></i>&nbsp;ADD ASIN & CH. SKU</a>
									</div>	
								</div>	
							</div>
							<div class="col-lg-12">	
							<?php
								echo'<div class="options col-sm-12" id="row_'.trim($product['Product']['id']).'"></div>';

									//$skuPercentArray = $sku_percent;
									$pid = '';
							?>						
								<?php if(isset($mappped_sku[$sku])){
								
											$pid = $product['Product']['id'];
											echo'<div class="col-lg-12" id="p_'.$pid.'">';										
											echo '<div class="col-sm-1"><strong>ASIN</strong></div>';
											echo '<div class="col-sm-2"><strong>Channel Name</strong></div>';
											echo '<div class="col-sm-2"><strong>Channel SKU</strong></div>';
											echo '<div class="col-sm-3"><strong>Channel SKU Title</strong></div>';
											echo '<div class="col-sm-3">';
												echo '<div class="w10" style="width:18%; float: left;text-align:center;"><strong>Buffer</strong></div>';
												echo '<div class="w10" style="width:20%; float: left;text-align:center;"><strong>% Q</strong></div>';				
												echo '<div class="w10" style="width:20%; float: left;text-align:center;"><strong>Min Q</strong></div>';
												echo '<div class="w10" style="width:20%; float: left;text-align:center;"><strong>Max Q</strong></div>';
												echo '<div class="w10" style="width:20%; float: left; text-align:center;"><strong>Q.T.R</strong></div>';		
											echo '</div>';
																				
											echo '<div class="col-sm-1">';
												echo '<div class="w10" style="width:60%; float: left;text-align:center;"><strong>Status</strong></div>';
												echo '<div class="w10" style="width:40%; float: left;text-align:center;"><strong>Action</strong></div>';
											echo'</div>';
										//	echo '<div class="col-sm-1"><strong>Xsensys Status</strong></div>';
											//echo '<div class="col-sm-1"><strong>Action</strong></div>';											
											echo'</div>	';
											
										foreach($mappped_sku[$sku] as $val){ 
										
										
										   // print_r($OpenOrder[$sku][$val['channel_sku']]);
											//$openQty = isset($SkuOpenOrder[$sku]) ? $SkuOpenOrder[$sku] : 0;
										
											echo'<div class="col-lg-12" id="r_'.$val['id'].'">';
										
											echo '<div class="col-sm-1"><section><span class="xedit" pk="'.$val['id'].'" data-name="asin" data-title="Asin" data-value="'.$val['asin'].'">'.$val['asin'].'</span></section></div>';
											echo '<div class="col-sm-2">'.$val['channel_name'].'</div>';
											
											echo '<div class="col-sm-2"><section><span class="xedit" pk="'.$val['id'].'" data-name="channel_sku" data-title="Channel SKU" data-value="'.$val['channel_sku'].'">'.$val['channel_sku'].'</span></section></div>';
											
											echo '<div class="col-sm-3"><section><span class="xedit" pk="'.$val['id'].'" data-name="channel_sku_title" data-title="Channel SKU Title" data-value="'.$val['channel_sku_title'].'">'.$val['channel_sku_title'].'</span></section></div>';
											
											echo '<div class="col-sm-3">';
											
												echo '<div class="w10" style="width:18%; float: left;text-align:center;"><section><span class="xedit" pk="'.$val['id'].'" data-name="buffer" data-title="Buffer Qty" data-value="'.$val['buffer'].'">'.$val['buffer'].'</span></section></div>';
												echo '<div class="w10" style="width:20%; float: left;text-align:center;"><section><span class="xedit" pk="'.$val['id'].'" data-name="qty_percent" data-title="Qty Percent" data-value="'.$val['qty_percent'].'">'.$val['qty_percent'].'</span></section></div>';											
												echo '<div class="" style="width:20%; float: left;text-align:center;"><section><span class="xedit" pk="'.$val['id'].'" data-name="min_qty" data-title="Min Qty" data-value="'.$val['min_qty'].'">'.$val['min_qty'].'</span></section></div>';
												echo '<div class="" style="width:20%; float: left;text-align:center;"><section><span class="xedit" pk="'.$val['id'].'" data-name="max_qty" data-title="Max Qty" data-value="'.$val['max_qty'].'">'.$val['max_qty'].'</span></section></div>';
												$qtyToReport = 0; $qty = 0;	 $pty = 0;										
												if(isset($product['Product']['current_stock_level']) && $product['Product']['current_stock_level'] > 0){
													//$qty =  floor(($product['Product']['current_stock_level'] - $openQty - $buffer) * $val['qty_percent']/100 );
													$pty =  floor(($product['Product']['current_stock_level'] - $val['buffer']) * $val['qty_percent']/100 );
													$qty = ($product['Product']['current_stock_level'] - $val['buffer']) ;
													if(($qty > 0) && ($qty < $val['min_qty'])){
													
														if($val['performance_status'] == 'fast_moving'){
															 $qtyToReport = $qty;
														}else if($val['performance_status'] == 'slow_moving'){
															 $qtyToReport = $qty;
														}else{
															$qtyToReport = 0;
														}
																			
													}elseif($qty > $val['max_qty']){
														$qtyToReport = $val['max_qty'];
													}else{
														$qtyToReport = $pty > 0 ? $pty : 0;
													}
												}											
												echo '<div class="" style="width:20%; float: left;text-align:center;">'.$qtyToReport.'</div>';
													
											echo '</div>';
																	
											echo '<div class="col-sm-1">';
												echo '<div class="w10" style="width:60%; float: left;text-align:center;"><section><span class="xedit" pk="'.$val['id'].'" data-name="amazon_status" data-title="Status" data-value="'.$val['amazon_status'].'">'.$val['amazon_status'].'</span></section></div>';
												
												echo '<div class="w10" style="width:40%; float: left; text-align:center;"><i class="glyphicon glyphicon-remove delete" onclick="delCh('.$val['id'].');"></i></div>';
											echo '</div>';
																					
											//echo '<div class="col-sm-1"><section><span class="xedit" pk="'.$val['id'].'" data-name="xsensys_status" data-title="Xsensys Status" data-value="'.$val['xsensys_status'].'">'.$val['xsensys_status'].'</span></section></div>';
											//echo '<div class="col-sm-1"><i class="glyphicon glyphicon-remove delete" onclick="delCh('.$val['id'].');"></i></div>';
											
											echo'</div>	';
										}
										
									}else{
										echo'<div class="col-lg-12" id="p_'.$pid.'">&nbsp;</div>';		
									}
								
								?>								
								
								
								
							</div>						
						</div>
						
						<?php }?>
						<?php }else{?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php } ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
		
		
					 	
					<script>	
					
					jQuery(document).ready(function() {  
						   // $.fn.editable.defaults.mode = 'popup';
							//jQuery('.xedit').editable();		
							$(document).on('click','.editable-submit',function(){
								var x = $(this).closest('section').children('span').attr('pk');			
								var y = $('.input-sm').val();
								var z = $(this).closest('section').children('span'); 
								var fname = $(this).closest('section').children('span').attr('data-name');
											
								$.ajax({
									url: "<?php echo Router::url('/', true) ?>Skumappings/Update",
									type: "POST",
									data : {value:y,pk:x,name:fname},
									success: function(s){
										if(s == 'status'){
										$(z).html(y);}
										if(s == 'error') {
										alert('Error Processing your Request!');}
									},
									error: function(e){
										alert('Error Processing your Request!!');
									}
								});
							});
							
							
					});
					
					jQuery('.xedit').editable();
					</script>






