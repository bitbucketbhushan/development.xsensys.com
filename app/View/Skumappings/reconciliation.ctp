<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-top:1px solid #666666; padding:0px;} 
	.body div.sku div{padding:4px 3px; font-size:12px;} 
	/*.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.pl-2{padding-left:4px;}
	.fa-spin-custom, .glyphicon-spin {
    -webkit-animation: spin 1000ms infinite linear;
    animation: spin 1000ms infinite linear;
}@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
@keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
	</style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title">Channel SKU Listing Status Reconciliation</h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
 				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?></div>
				
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							
							 
							<div class="row">	
								
								<form method="post" name="mapFrm" id="mapFrm" enctype="multipart/form-data" action="<?php echo Router::url('/', true) ?>Skumappings/UploadReconciliation" onsubmit="return checkFrm()">				 		
								
								<div  class="col-lg-2">
									<select type="text" class="form-control" id="file_type" name="file_type">
										 <option value="">Select File Type</option>
										 <optgroup label="Sub Sources">
											<option value="CostBreaker_DE">CostBreaker_DE(FBA & FBM)</option>
											<option value="CostBreaker_CA">CostBreaker_CA(FBA & FBM)</option>
											<option value="CostBreaker_ES">CostBreaker_ES(FBA & FBM)</option>
											<option value="CostBreaker_FR">CostBreaker_FR(FBA & FBM)</option>
											<option value="CostBreaker_IT">CostBreaker_IT(FBA & FBM)</option>
											<option value="CostBreaker_NL">CostBreaker_NL(FBA & FBM)</option>
											<option value="CostBreaker_UK">CostBreaker_UK(FBA & FBM)</option>
											<option value="CostBreaker_US">CostBreaker_US(FBA & FBM)</option> 
											
											<option value="Marec_DE">Marec_DE(FBA & FBM)</option>
											<option value="Marec_ES">Marec_ES(FBA & FBM)</option>
											<option value="Marec_FR">Marec_FR(FBA & FBM)</option>
											<option value="Marec_IT">Marec_IT(FBA & FBM)</option>
											<option value="Marec_NL">Marec_NL(FBA & FBM)</option>
											<option value="Marec_uk">Marec_uk(FBA & FBM)</option>
											
											<option value="Rainbow Retail">Rainbow Retail UK(FBA & FBM)</option>
											<option value="RAINBOW RETAIL DE">RAINBOW RETAIL DE(FBA & FBM)</option>
											<option value="Rainbow_Retail_ES">Rainbow_Retail_ES(FBA & FBM)</option>
											<option value="Rainbow_Retail_FR">Rainbow_Retail_FR(FBA & FBM)</option>
											<option value="Rainbow_Retail_IT">Rainbow_Retail_IT(FBA & FBM)</option>
 											
											<option value="EBAY0">EBAY0</option>
											<option value="EBAY2">EBAY2</option>
											<option value="EBAY5">EBAY5</option>
											<option value="EBAY6">EBAY6</option>
											<option value="CDISCOUNT">CDISCOUNT</option>
											<option value="Costdropper_Fnac">Costdropper_Fnac</option>
											<option value="real.de">KAUFLAND Costdropper(real.de)</option>
											<option value="Onbuy">Onbuy</option>
											<option value="wayfair_costdropper">wayfair_costdropper</option>
											<option value="Euraco.fyndiq">Euraco.fyndiq</option>
 											</optgroup>
										  <optgroup label="Status Update File">
										 	<option value="status">Update Status File</option>	
										 </optgroup>												
									 </select>
									 <small style="font-size:9px;"><a href="<?php echo Router::url('/', true) ?>Skumappings/UpdateStatusSample" style="color:#FF0000;">Update Status Sample</a></small>
								</div>
								<div class="col-lg-4">
  									 
									 <div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												Browse... <input type="file" class="form-control" id="upload" name="upload"/>
										
											</span>
										</span>
										<input type="text" placeholder="No file selected" readonly="" class="form-control">
									</div>
 								</div>
								
								<div  class="col-lg-2">
									<button type="submit" class="btn btn-success">Upload</button>
								</div>
								</form>
								 
							</div>	 
							
						</div>	
						
								
							
					<div class="panel-body no-padding-top bg-white">
					 
						<div class="panel-tools" style="margin-bottom: 10px;clear: right;text-align: center; float: right;">
							<ul class="pagination">
							<?php
							   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
							   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							?>
							</ul>
						</div> 
						 
						 	
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-15">Channel (SubSource)</div>		
								<div class="col-12">Uploaded By Name</div>		
 								<div class="col-25">File Name</div>
								<div class="col-13">File Upload Date</div>	 
								<div class="col-8">Status Matched</div>
								<div class="col-8">Status Mismatch</div>		 
								<div class="col-8">Not Uploaded on Amazon</div>
								<div class="col-8">On Amazon But Not in System</div>	 
 							</div>								
						</div>		
						<div class="body" id="results">
						 <?php if(count($reconciliation) > 0){?>
							<?php foreach( $reconciliation as $data ) { ?> 
							<div class="row sku"> 
								<div class="col-lg-12">
									<div class="col-15"><?php echo ($data['ReconciliationLog']['sub_source']); ?></div> 
									<div class="col-12"><?php echo ($data['ReconciliationLog']['username']); ?></div>	
									<div class="col-25"><a href="<?php echo Router::url('/', true) ?>Skumappings/DownloadReconciliationFile/<?php echo $data['ReconciliationLog']['id']; ?>" title="Download File" style="text-decoration:underline;color:#0000CC"><?php echo ($data['ReconciliationLog']['file_name']); ?></a></div>	
									<div class="col-13"><?php echo ($data['ReconciliationLog']['added_date']); ?></div>	
									<div class="col-8"><a href="<?php echo Router::url('/', true) ?>Skumappings/DownloadMathched/<?php echo $data['ReconciliationLog']['id']; ?>" title="Download File" style="text-decoration:underline;color:#0000CC"><?php echo count(json_decode($data['ReconciliationLog']['mathched'],1)); ?></a></div>	
									<div class="col-8"><a href="<?php echo Router::url('/', true) ?>Skumappings/DownloadNotMathched/<?php echo $data['ReconciliationLog']['id']; ?>" title="Download File" style="text-decoration:underline;color:#0000CC"><?php echo count(json_decode($data['ReconciliationLog']['not_mathched'],1)); ?></a></div>	
									<div class="col-8"><a href="<?php echo Router::url('/', true) ?>Skumappings/DownloadNotUploaded/<?php echo $data['ReconciliationLog']['id']; ?>" title="Download File" style="text-decoration:underline;color:#0000CC"><?php echo count(json_decode($data['ReconciliationLog']['not_uploaded_marketplace'],1)); ?></a></div>	
									<div class="col-8"><a href="<?php echo Router::url('/', true) ?>Skumappings/DownloadNotInSystem/<?php echo $data['ReconciliationLog']['id']; ?>" title="Download File" style="text-decoration:underline; color:#0000CC"><?php echo count(json_decode($data['ReconciliationLog']['not_in_system'],1)); ?></a></div>	
 								</div>
 							</div>
						
						<?php }?>
						<?php }else{?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php } ?>
						</div>
						<br /><br /><br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php //echo $this->element('footer'); ?></div>
						
			</div>			
		
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>
 
<script>	
 	function checkFrm(){
		if($("#file_type option:selected").val() == ''){
			alert("Please Select File Type.");	
			return false;
		}
		
	}							
 
</script>
