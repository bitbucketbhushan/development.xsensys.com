<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-top:1px solid #666666; padding:0px;} 
	.body div.sku div{padding:4px 3px; font-size:12px;} 
	/*.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.pl-2{padding-left:4px;}
	.fa-spin-custom, .glyphicon-spin {
    -webkit-animation: spin 1000ms infinite linear;
    animation: spin 1000ms infinite linear;
}@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
@keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
	</style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title">Channel SKU Mapping Report</h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?>
					
				</div>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							
							 
							<div class="row">	
								 
								
						<form method="get" name="search" id="searchfrm" action="<?php echo Router::url('/', true) ?>Skumappings/ChannelSkus">				 		

								<div class="col-lg-3" style="margin-left:-15px">
									<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Search by sku OR Channel sku" name="searchkey" class="form-control searchString" />
								</div>
								
								<div class="col-lg-2"> 
									<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
								</div>
						</form> 
						<div class="col-lg-2">	
							<select class="form-control" id="filter_by" name="filter_by" onchange="filterBy()">
									 <option value="store" <?php if(isset($_REQUEST['by']) && ($_REQUEST['by'] == 'store')) echo 'selected=selected'?>>By Store</option>
									 <option value="channel" <?php if(isset($_REQUEST['by']) && ($_REQUEST['by'] == 'channel')) echo 'selected=selected'?>>By Channel</option>
									 <option value="purchaser" <?php if(isset($_REQUEST['by']) && ($_REQUEST['by'] == 'purchaser')) echo 'selected=selected'?>>By Purchaser</option>
									 <option value="supplier" <?php if(isset($_REQUEST['by']) && ($_REQUEST['by'] == 'supplier')) echo 'selected=selected'?>>By Supplier</option>
							 </select>  
						</div>
						
								<div class="col-lg-4" style="text-align:right;">
									 
									 <div class="col-70" id="filter-div">
 										  
									 
									 </div>									 
									  <div class="col-15">
										 <a href="javascript:void(0);" class="btn btn-info filter" role="button" title="Filter Data"><i class="fa fa-filter"></i></a>
									 </div>
									 <div class="col-15">									 
										 <a href="javascript:void(0);" class="btn btn-primary downloadfeed" role="button" title="Download Feed" ><i class="glyphicon glyphicon-download"></i></a>									
									 </div>	
 								</div>
								
								 <div class="col-lg-1">									 
										 <a href="<?php echo Router::url('/', true) ?>Skumappings/ChannelSkus" type="button" class="btn btn-warning">Go Back</a>					
									 </div>	
							</div>	
  						</div>	
						
 					<div class="panel-body no-padding-top bg-white">
						 
						<div class="panel-tools" style="margin-bottom: 10px;clear: right;text-align: center; float: right;">
							<ul class="pagination">
							<?php
							   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
							   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							?>
							</ul>
						</div> 
 						
					
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-10">Purchaser</div>	
								<div class="col-15">SKU</div>		
								<div class="col-15">Channel Sku</div>		
 								<div class="col-15">Core Upload Fields</div>	
								<div class="col-15">Detailed Product Information</div>	
								<div class="col-15">Channel SKU Mapping</div>	
								<div class="col-15">Channel SKU Management</div>
								<!--<div class="col-15">Timestamp</div> 
								<div class="col-5">Action</div>		-->						
 							</div>								
						</div>	
							
						<div class="body" id="results">
 						<?php if(count($all_product) > 0){
  						foreach( $all_product as $product ) { 
						$sku = $product['Skumapping']['sku'];
						$channel_sku = $product['Skumapping']['channel_sku'];
						$purchaser = 'NA';
						if(isset($purchaserSku[$sku])){
 							$purchaser = $purchaserSku[$sku];
						}
						$core_n = $core_d = '';
						$product_info_n = $product_info_d = '';
						$channel 		= $channel_manage = '';
						
						if(isset($pro_logs[$sku])){
							$core_n = $pro_logs[$sku]['core_upload'];
							$core_d = $pro_logs[$sku]['core_update'];
							$product_info_n = $pro_logs[$sku]['info_upload'];
							$product_info_d = $pro_logs[$sku]['info_update'];
						}
						/*if(isset($productFiles[$sku])){
 							if(isset($productFiles[$sku]['core'])){
								$core_n = $productFiles[$sku]['core'][0];
								$core_d = $productFiles[$sku]['core'][1];
							}
							if(isset($productFiles[$sku]['product_info'])){
								$product_info_n = $productFiles[$sku]['product_info'][0];
								$product_info_d = $productFiles[$sku]['product_info'][1];
							} 
						}*/
						if($product['Skumapping']['map_change_by'] != ''){
							$channel = '<a href="'. Router::url('/', true).'Skumappings/ProductFilesLog/'.$sku.'/ChannelSKUMapping" style="color:#0000CC">'.$product['Skumapping']['map_change_by'].'</a>'.'<br>'.$product['Skumapping']['map_change'];
 						}
						if($product['Skumapping']['change_margin_by'] != ''){
							$channel_manage ='<a href="'. Router::url('/', true).'Skumappings/ProductFilesLog/'.$channel_sku.'/ChannelSKUManagement" style="color:#0000CC">'.$product['Skumapping']['change_margin_by'].'</a>'.'<br>'.$product['Skumapping']['change_margin'];
							//$product['Skumapping']['change_margin_by'].'<br>'.$product['Skumapping']['change_margin'];
						}
						
 						?>
 						<div class="row sku"> 
							<div class="col-lg-12">
								<div class="col-10"><?php echo $purchaser; ?></div>
								<div class="col-15"><?php echo trim($product['Skumapping']['sku']); ?></div>
								<div class="col-15"><?php echo trim($product['Skumapping']['channel_sku']); ?></div>		
 								<div class="col-15"><a href="<?php echo Router::url('/', true) ?>Skumappings/ProductFilesLog/<?php echo $sku; ?>/CoreUploadFields" style="color:#0000CC"><?php echo $core_n; ?></a><?php echo '<br>'.$core_d; ?></div>
								<div class="col-15"><a href="<?php echo Router::url('/', true) ?>Skumappings/ProductFilesLog/<?php echo $sku; ?>/DetailedProductInformation" style="color:#0000CC"><?php echo $product_info_n; ?></a><?php echo '<br>'.$product_info_d; ?></div>
								<div class="col-15"><?php echo $channel; ?></div>								
								<div class="col-15"><?php echo $channel_manage; ?></div>
								<?php /*?><div class="col-12"><?php echo trim($product['Skumapping']['added_date']); ?></div><?php */?>
							<?php /*?>	<div class="col-5"><a href="<?php echo Router::url('/', true) ?>Skumappings/DeleteMap/<?php echo $product['Skumapping']['id']; ?>" type="button" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete this?')">Delete</a>
</div>	<?php */?>
							</div>
							 						
						</div>
						
						<?php }?>
						<?php }else{?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php } ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
						
			</div>			
		
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>
 
<script>	
   
function filterBy() {
        try { 
               var url = '<?php echo Router::url("/", true) ?>Skumappings/getFiltersData/';

                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: "json",
                    cache: false,
					beforeSend  : function() { 
						//$('#not_selling_sku').html('<img src="<?php echo Router::url('/', true); ?>img/sm_loading.gif"/>'); 
						//$('#sold_out_sku').html('<img src="<?php echo Router::url('/', true); ?>img/sm_loading.gif"/>'); 
						//$('#recomendation').html('<img src="<?php echo Router::url('/', true); ?>img/sm_loading.gif"/>'); 
						 $('.outerOpac').show();
 					},
                    data: { filter_by:$("#filter_by option:selected").val(),ch:'<?php echo $_GET['ch']?>'},
                    success: function (data) {
                        $('.outerOpac').hide();
						$('#filter-div').html(data.supplier); 
						// $('#sold_out_sku').html(data.sold_out_sku);
						// $('#recomendation').html(data.recomendation_count);
 						 $('.selectpicker').selectpicker('refresh');
                    },
                    error: function (xhr, status, error) {
                        alert(error);
                    }

                });
            
        } catch (e) {
            alert(e.toString());
        }
 }

jQuery(document).ready(function() {  
		filterBy();
       // $.fn.editable.defaults.mode = 'popup';
        //jQuery('.xedit').editable();		
		$(document).on('click','.editable-submit',function(){
			var x = $(this).closest('section').children('span').attr('pk');			
			var y = $('.input-sm').val();
			var z = $(this).closest('section').children('span'); 
			var fname = $(this).closest('section').children('span').attr('data-name');
						
			$.ajax({
				url: "<?php echo Router::url('/', true) ?>Skumappings/Update",
				type: "POST",
				data : {value:y,pk:x,name:fname},
				success: function(s){
					if(s == 'status'){
					$(z).html(y);}
					if(s == 'error') {
					alert('Error Processing your Request!');}
				},
				error: function(e){
					alert('Error Processing your Request!!');
				}
			});
		});
		
		
});

jQuery('.xedit').editable();

$(".filter").click(function(){ 
   	window.location.href = '<?php echo Router::url('/', true) ?>Skumappings/ChannelSkus?by='+$("#filter_by option:selected").val()+'&ch='+$("#ch option:selected").val();
 });

$(".downloadfeed").click(function(){	
	if($("#ch option:selected").val() == 'all'){
		alert("Please Select Option");	
	}else{
		 
		$(".outerOpac").show();		 	
		$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>Skumappings/getMapReportCsv',
				type: "POST",				
				cache: false,			
				data : {channel:$("#ch option:selected").val(),by:$("#filter_by option:selected").val()},
				success: function(data, textStatus, jqXHR)
				{	//alert(data.data);	
					$(".outerOpac").hide();		
					jQuery(".msg").html(data.file);
					//jQuery(".msg").html(data.msg);
					//jQuery('.panel-body').html(data.data);
					//alert(data.msg);						
					$('#msg').delay(200000).fadeOut('slow');							
			}		
		 });
	 }
		 
});

 
 
</script>
