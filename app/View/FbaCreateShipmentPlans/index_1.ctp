<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Fba Create Shipment Plan </h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							  
						</div>	
						
					<div class="panel-body no-padding-top bg-white">
						 
						<div class="body" id="results">
							<?php  	
							App::Import('Controller', 'FbaCreateShipmentPlans'); 
							$obj = new FbaCreateShipmentPlansController;
							 
						    $Address = $obj->getShipmentAllAddress();
							?>
						<div class="row sku">
							<div class="col-lg-12">	
								<div style="width:15%; float:left;">Select Pickup Address:</div>	
								<div style="width:30%; float:left;"> 
								<form action="<?php echo Router::url('/', true) ?>FbaCreateShipmentPlans/ShipmentPlan" method="get" name="address_frm" id="address_frm">
										<select name="address" id="address" style="font-size:12px; padding:5px; border:1px solid #c6c6c6; width:100%;">
										<option value="">--Shipment Address--</option>
										<?php foreach($Address as $val){ ?>
										<option value="<?php echo $val['FbaApiShipmentAddress']['id'];?>">
										<?php echo $val['FbaApiShipmentAddress']['name'].", ".$val['FbaApiShipmentAddress']['address_line_1'].", ".$val['FbaApiShipmentAddress']['address_line_2'].", ".$val['FbaApiShipmentAddress']['city']." ".$val['FbaApiShipmentAddress']['postal_code'].", ".$val['FbaApiShipmentAddress']['country_code']?></option>
										<?php }?>
										</select> 
							</form>
										</div>	
						
						</div>
																				
						</div>
								 	 
							 
						 
							<!--<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>-->
						 
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	

$('#address').change(function(){  
	document.getElementById("address_frm").submit();		 
});
	
</script>
