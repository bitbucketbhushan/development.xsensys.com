<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	 .outstock{background-color:#FF3300;}
	 .instock{background-color:#00CC00;}
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Fba Create Shipment Plan </h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							  
						</div>	
						<form action="<?php echo Router::url('/', true) ?>FbaCreateShipmentPlans/ShipmentPlan" method="get" name="address_frm" id="address_frm">
					<div class="panel-body no-padding-top bg-white">
						 
						
							<?php  	
							App::Import('Controller', 'FbaCreateShipmentPlans'); 
							$obj = new FbaCreateShipmentPlansController;
							 
						    $Address = $obj->getShipmentAllAddress();
							?>
						<div class="row" style="margin-bottom:6px;">
							<div class="col-lg-12">	
								<div class="col-lg-2">Select Pickup Address:</div>	
								<div class="col-lg-5"> 
										
										<select name="address" id="address" class="form-control">
										<option value="">--Shipment Address--</option>
										<?php foreach($Address as $val){ ?>
										<option value="<?php echo $val['FbaApiShipmentAddress']['id'];?>">
										<?php echo $val['FbaApiShipmentAddress']['name'].", ".$val['FbaApiShipmentAddress']['address_line_1'].", ".$val['FbaApiShipmentAddress']['address_line_2'].", ".$val['FbaApiShipmentAddress']['city']." ".$val['FbaApiShipmentAddress']['postal_code'].", ".$val['FbaApiShipmentAddress']['country_code']?></option>
										<?php }?>
										</select> 
										 
										</div>	
								<div class="col-lg-1">Shipment Name:</div>
								<div class="col-lg-4"><input type="text" name="" value="<?php echo 'FBA'.date('dmY')?>" class="form-control" /></div>	
							</div>
						</div>
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div style="float:left; width:11%">Seller SKU</div>	
								<div style="float:left; width:16%">Master SKU</div>																	
								<div style="float:left; width:11%">ASIN</div>
								<div style="float:left; width:5%">Sale Price</div>
								<div style="float:left; width:6%">PO Price</div>
								<div style="float:left; width:9%">Sale Of 
								<select name="sale_days" id="sale_days" onchange="loadajax();">
									<?php 
									   $day = 31;
										for ($i = 1; $i <= $day; $i++)
										{						
											echo '<option value="'.$i.'">'.$i.'</option>';
										}									 
										?>													
									</select> Days</div>	
									
								<div style="float:left; width:10%">AverageSale <span id="avsale"> 10 days</span></div> 
								<div style="float:left; width:6%">InStock</div>																		 	 
								<div style="float:left; width:7%">In Transit </div>	
								<div style="float:left; width:7%">Xsensys Stock</div>	
								<div style="float:left; width:10%">Stock Required For 
								<select name="stock_required" id="stock_required" onchange="loadajax();">
									<?php 
									   $day = 31;
										for ($i = 1; $i <= $day; $i++)
										{						
											echo '<option value="'.$i.'">'.$i.'</option>';
										}									 
										?>													
									</select> Days</div>	 
								
							</div>								
						</div>
						 
						<div class="body" id="results">
							<div><center> Loading...</center></div>																				
						</div>
								 	 
							 
						 
							<!--<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>-->
						 
						</div>
						</form>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	

function loadajax(){	
		$(".outerOpac").show();
		$("#avsale").html(' Of ' + $("#sale_days option:selected").val()+' Days');
			
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaCreateShipmentPlans/ShipmentPlanAjax/',
			type: "POST",				
			cache: false,			
			data : {sale_days:$("#sale_days option:selected").val(),stock_required:$("#stock_required option:selected").val()},
			success: function(data, textStatus, jqXHR)
			{			
				$(".outerOpac").hide();
				$("#results").html(data.htmldata);
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				}	
											
		} 
	}); 
} 
loadajax();
</script>
