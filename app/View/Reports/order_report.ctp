<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Reports</h1>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                                <div class="panel-body padding-bottom-40">
										<div class="form-group">                                        
                                        <div class="col-lg-8">                                                                                         
											<?php
												$getLocationArray = array();
												$getLocationArray["lock_unlock_cancel"] = 'Lock Unlock Cancel';
												$getLocationArray["held_orders"] = 'Held Orders';
												$getLocationArray["lock_orders"] = 'Lock Orders';
												$getLocationArray["processed_orders"] = 'Processed Orders';
												print $this->form->input( 'downloadReport', array( 'type'=>'select', 'empty'=>'Choose Report','options'=>$getLocationArray,'class'=>'form-control selectreport','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );	
											?>
                                        </div>                                      
                                      </div>
                                     
                                    </div>
									<!--local unlocak cancel start-->
									<div class="panel-body padding-bottom-40 reports lock_unlock_cancel" style="display:none;">
										<div class="form-group">                                        
                                        <div class="col-lg-8">                                                                                         
											<a href="/Reports/localUnlocakCancel" class="btn btn-success color-white btn-dark" style="float:right; margin-right:8px; auto; text-align: center;">Generate</a>
                                        </div>                                      
                                      </div>
                                    </div>
									<!--local unlocak cancel end-->
									
									<!--local unlocak cancel start-->
									<div class="panel-body padding-bottom-40 reports held_orders" style="display:none;">
										<div class="form-group">                                        
                                        <div class="col-lg-8">                                                                                         
											<a href="/Reports/getHeldOrderReport/custom" class="btn btn-success color-white btn-dark" style="float:right; margin-right:8px; auto; text-align: center;">Generate</a>
                                        </div>                                      
                                      </div>
                                    </div>
									<!--local unlocak cancel end-->
									
									<!--local start-->
									<div class="panel-body padding-bottom-40 reports lock_orders" style="display:none;">
										<div class="form-group">                                        
                                        <div class="col-lg-8">                                                                                         
											<a href="/Reports/getLockOrderReport" class="btn btn-success color-white btn-dark" style="float:right; margin-right:8px; auto; text-align: center;">Generate</a>
                                        </div>                                      
                                      </div>
                                    </div>
									<!--local end-->
									
									<!--local start-->
									<div class="panel-body padding-bottom-40 reports processed_orders" style="display:none;">
										<div class="form-group">                                        
                                        <div class="col-lg-8">                                                                                         
											<a href="/Reports/getOrderReports" class="btn btn-success color-white btn-dark" style="float:right; margin-right:8px; auto; text-align: center;">Generate</a>
                                        </div>                                      
                                      </div>
                                    </div>
									<!--local end-->
									
                            </div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>


<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; "></span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

<script>

$('body').on('change', '#downloadReport', function(){
 	var reportclass = $( "#downloadReport" ).val();
	$('.reports').hide();
	$('.'+reportclass).show();
});

</script>
