<div class="col-lg-12">
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Orders</h1>
    </div>
<!--<table class="table table-bordered table-striped dataTable col-lg-12">
	<tr>
		<td width=25%><input type="text" class="po_name" placeholder="PO NAME" style="height: 36px; width: 269px; font-size: 18px;" /></td>
		<td width=25%><input type="text" class="supplier_code" placeholder="SUPPLIER CODE" style="height: 36px; width: 269px; font-size: 18px;"/></td>
		<td width=25%>
		<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
		</td>
		<td width=25%>
			<!--<a class="submit_search_query btn bg-green-500 color-white btn-dark padding-left-10 padding-right-10" href="javascript:void(0);" data-toggle="modal">Search</a>-->
			<!--<a class="btn bg-green-500 color-white btn-dark padding-left-10 padding-right-10" href="/Virtuals/showAllPurchaseOrder" data-toggle="modal">Refresh</a>
			<a class="generate_selected_sku btn bg-green-500 color-white btn-dark padding-left-10 padding-right-10" href="javascript:void(0);" data-toggle="modal">Generate</a>
		</td>
	</tr>
</table>-->
<div class="set_result">
<table class="table table-bordered table-striped dataTable" >
	<tr>
		<th width=1%>Order id</th>
		<th width=1%>SKU</th>
		<th width=15%>PO Name</th>
		<th width=10%>Invoice No</th>
		<th width=10%>Supplier Code</th>
		<th width=6%>Po date</th>
		<th width=6%>Uploaded date</th>
		<th width=16%>Status( View | Lock | Unlocak | Recived By)</th>
	</tr>		
		 <tr>
		 	<td>Test</td>
			<td>Test</td>
			<td>Test</td>
			<td>Test</td>
			<td>Test</td>
			<td>Test</td>
			<td>Test</td>
			<td>Test</td>
		</tr>
</table>


</div>
</div>
</div>
<div class="outerOpac" style="display:none">
	<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; "></span>
	<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>
$(document).keypress(function(e) {
   if(e.which == 13) {
   			var getFrom = $('div.daterangepicker_start_input input:text').val();
			var getEnd = $('div.daterangepicker_end_input input:text').val();
			
			var poName			=	$('.po_name').val();
			var supplierCode	=	$('.supplier_code').val();
				$.ajax({
										url     	: '/Virtuals/getSearchedPo',
										type    	: 'POST',
										data    	: { poName : poName, supplierCode : supplierCode,getFrom : getFrom,getEnd:getEnd },
										beforeSend  : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },   			  
										success 	:	function( data  )
										{
										
											$('.set_result').html('');
											$('.set_result').append( data );
											$( ".outerOpac" ).attr( 'style' , 'display:none');
										}                
								});				
    }
});


$('body').on('click', '.generate_selected_sku', function(){

		var selectedId = [];
		$('input:checked').each(function() {
			selectedId.push($(this).attr('name'));
		});
		
		if(selectedId == '')
		{
			swal('Please select any one po.')
			return false;
		}
		$.ajax({
								url     	: '/Virtuals/generatePurchaseSkuCsv',
								type    	: 'POST',
								data    	: { selectedId : selectedId},
								beforeSend  : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },   			  
								success 	:	function( data  )
								{
									$( ".outerOpac" ).attr( 'style' , 'display:none');
									window.open(data,'_blank');
								}                
						});
		
});
</script>
