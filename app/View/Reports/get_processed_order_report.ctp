<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	 
 </style><div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        	<h1 class="page-title"><?php print $title;?></h1>
         
			 <div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
							 
							 
							 
						</ul>
					</div>
				</div>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		
    </div>
   <?php 
							
	App::Import('Controller', 'Reports'); 
	$obj = new ReportsController;	
	$userData = $obj->getUserDetails(); 
	 
	?>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">
									<div class="row"> 
										<div class="col-lg-12">
										<div class="col-md-5"><div style="float:left;width:43%;">Order Process Duration:</div>
											<select name="process_duration" id="process_duration" class="form-control " onchange="getProcessedOrder(this)" style="float:left;width:55%;">
											<option value="24" <?php if(isset($this->params['pass'][2]) && $this->params['pass'][2] == 24) echo 'selected="selected"';?>>With In 24 Hours</option> 
											<option value="48" <?php if(isset($this->params['pass'][2]) && $this->params['pass'][2] == 48) echo 'selected="selected"';?>>After 24 Hours</option>
											</select></div>
											
										<div class="col-md-2">&nbsp;</div>	
																
										<div class="col-md-5">
											
											<div class="col-md-9">
											
											<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
		<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
		<span></span> <b class="caret"></b>
	</div>
						</div>
										<div class="col-md-3  text-right"><button type="button" class="btn btn-success btn-sm btn-block" onclick="getProcessedOrder(this)">Filter</button></div> 
										</div>
									</div>
								 </div>
									 	
									<div class="row" style="margin-top:5px;"> 
										<div class="col-lg-12"> 
											<div class="col-md-5"><div style="float:left;width:43%;">User:</div>
											
											<select name="user" id="user" class="form-control " onchange="getProcessedOrder(this)" style="float:left;width:55%;">
											<option value="all" <?php if(isset($this->params['pass'][3]) && $this->params['pass'][3] == 'all') echo 'selected="selected"';?>>All Users</option> 
											<?php foreach($userData as $uval){?>
												<?php if($uval['MergeUpdate']['assign_user'] != ''){?>
												<option value="<?php echo $uval['MergeUpdate']['user_id'];?>" <?php if(isset($this->params['pass'][3]) && $this->params['pass'][3] == $uval['MergeUpdate']['user_id']) echo 'selected="selected"';?>><?php echo $uval['MergeUpdate']['assign_user'].' ('.$uval['MergeUpdate']['pc_name'].')'?></option>
												<?php }?>
											<?php }?>
											
											</select>
											
											</div>
											
											
											<div class="col-md-2">&nbsp;</div>	
											<div class="col-md-3" style="padding-left:3%;">Total No. of orders:<?php echo count($orders) ;?></div>
											<div class="col-md-2  text-right" style="padding-right:3%;"><button type="button" class="btn btn-warning btn-sm btn-block" id="download_sheet" action="download_sheet" onclick="getProcessedOrder(this);">Download Sheet</button></div> 
											
									 	</div>
									 </div>
								</div>
								
								 
                                <div class="panel-body no-padding-top bg-white">											
											<div class="row head">
												<div class="col-md-12">
													<div class="col-md-2">Order Id</div>
													<div class="col-md-1">Qty</div>
													<div class="col-md-2">Order Date<small>(Move Date)</small></div>
													<div class="col-md-2">Picked Date</div>
													<div class="col-md-2">Processed Date</div>													
													<div class="col-md-3">Username</div> 
											</div>	
										</div>
								 
									 <div class="body" id="results">
										<?php  
										/*if (date_default_timezone_get()) {
  echo date('Y-m-d H:i:s'); echo '  date_default_timezone_set: ' . date_default_timezone_get() . '<br />';
}date_default_timezone_set('Europe/Jersey');
if (date_default_timezone_get()) {
    echo date('Y-m-d H:i:s'); echo '  date_default_timezone_set: ' . date_default_timezone_get() . '<br />';
}*/
										foreach($all_orders as $order) {  
											$sum_order = 0;  
											?>
                                            <div class="row sku">
												<div class="col-md-12">
												<div class="col-md-2"><?php echo $order['MergeUpdate']['product_order_id_identify']; ?></div>
												<div class="col-md-1"><?php echo $order['MergeUpdate']['quantity']; ?> </div>
												<div class="col-md-2"><?php echo $order['MergeUpdate']['order_date']; ?> </div>
												<div class="col-md-2"><?php //echo $order['MergeUpdate']['picked_server_date'];
												// echo '<br />';
												 echo date('Y-m-d H:i:s', strtotime('-3 hours',strtotime($order['MergeUpdate']['picked_date']))); ?> </div>                                                
												<div class="col-md-2"><?php echo $order['MergeUpdate']['process_date']; ?> </div>
												<div class="col-md-3"><?php echo $order['MergeUpdate']['assign_user'].'('.$order['MergeUpdate']['pc_name'].')'; ?> </div>
												</div>
											</div>	 
												 
											<?php } ?>
									</div>	 	 
									<div class="row">
												<div class="col-md-12">&nbsp;</div>
											</div>
									<div class="" style="margin:0 auto; width:350px">
										 <ul class="pagination">
										  <?php
											   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
											   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
											   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										  ?>
										 </ul>
									</div>
									</div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
				<!-- BEGIN FOOTER -->
				
				 
				<footer class="bg-white">
					<div class="pull-left">
						<span class="pull-left margin-right-15">&copy; 2015 WMS by JijGroup.</span>
						<ul class="list-inline pull-left">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
						</ul>
					</div>
				</footer>
				<!-- END FOOTER -->
            </div>
    </div>
</div>
 
 
<script>
   
function getProcessedOrder(obj){
	    var action = $(obj).attr('action');  
		var process_duration 	= $('#process_duration option:selected').val(); 
		var user 				= $('#user option:selected').val();  user
  		var getFrom 	= $('div.daterangepicker_start_input input:text').val();
		var getEnd 		= $('div.daterangepicker_end_input input:text').val();
		var dateArEnd 	= getEnd.split('/');
		var newDateEnd 	= dateArEnd[2] + '-' + dateArEnd[0].slice(-2) + '-' + dateArEnd[1];
		var dateArFrom 	= getFrom.split('/');
		var newDateFrom = dateArFrom[2] + '-' + dateArFrom[0].slice(-2) + '-' + dateArFrom[1];
 		 
 		var selectedDate = newDateFrom;
		var startDate = newDateEnd;
		window.location = "<?php echo Router::url('/', true) ?>Reports/getProcessedOrderReport/"+selectedDate+"/"+startDate+"/"+process_duration+"/"+user+"/"+action;
}
 
$('body').on('click', '.applyBtn', function(){ 
	  getProcessedOrder(this);
  });
  
	
</script>
