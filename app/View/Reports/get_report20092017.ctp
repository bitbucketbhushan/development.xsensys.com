<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        	<h1 class="page-title"><?php print $role;?></h1>
         
			 <div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
							 
							<li>
								<a  href="<?php echo Router::url('/', true); ?>Reports/getReport" class="btn btn-success btn-xs margin-right-15 color-white"><i class="fa fa-refresh"></i> Reset</a>
							</li>
							 
						</ul>
					</div>
				</div>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		
    </div>
    <?php
		//pr($productAllDescs); exit;
    ?>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">
								
									<?php
										print $this->form->input( 'Product.search', array( 'placeholder'=>'Search Product by Product SKU','type'=>'text','class'=>'form-control searchtext selectpicker bg-white','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
									?> 										
									<div class="panel-tools">
									
									<!--<a class="downloadStockFile btn btn-success btn-xs margin-right-10 color-white"><i class="fa fa-download"></i> Download CSV</a>-->
									</div>
								</div>
								
								 
                                <div class="panel-body no-padding-top bg-white">											
											<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
												<thead>
												<tr role="row">
												<th style="width: 5%;">Product Sku</th>
												<th style="width: 10%;">Locations</th>
												<th style="width: 5%;">PAID Open Orders Unpicked</th>
												<th style="width: 5%;">UNPAID Pending Orders</th>
												<th style="width: 5%;">Stock Picked (picklist generated)</th>
												<th style="width: 5%;">Sum of Orders</th>
												<th style="width: 5%;">Net AVB Stock</th>
												<th style="width: 5%;">Stock on Shelf</th>
												<th style="width: 5%;">Stock Processed (processed in dispatch console)</th>
												<th style="width: 5%;">Stock Sorted (sortation scanned) </th> 				
													 <!--  <th style="width: 5%;">Stock in Processing</th> -->
										</thead>
								 
									<tbody role="alert" aria-live="polite" aria-relevant="all">
										<?php 
										
										foreach($productAllDescs as $productAllDesc) {  
											$sum_order = 0; 
											?>
                                            <tr class="odd ">
												<td class="sorting_1"><?php echo $productAllDesc['Product']['product_sku']; ?></td>
												<td class="sorting_1">
												
												<?php 
												$net_stock = 0;
												if(isset($BinLocation[$productAllDesc['ProductDesc']['barcode']])){
													$blArray = $BinLocation[$productAllDesc['ProductDesc']['barcode']];
													
													foreach($blArray  as $val){
													$net_stock += $val['stock_by_location'];
													echo '<div style="font-size:11px;">'.$val['bin_location'] .' : '.$val['stock_by_location'].'</div>';
													}
												
												}
												 											
												echo'<div style="border-top:1px solid #996600; color:#CC6600; min-width:135px"><strong>Sum_of_Locations : </strong>'.$net_stock.'</div>';
												 
												?> 
												 
												</td>                                               
												<td class="sorting_1">
												
												
													<div style="float:left"><?php 
													$unpicked = 0;
													/*if(isset($paid_open_unpicked[$productAllDesc['Product']['product_sku']])){
														 $q = $paid_open_unpicked[$productAllDesc['Product']['product_sku']];
													
													}*/
													
													if(isset($paid_open_unpicked_data[$productAllDesc['Product']['product_sku']])){
													 $unpicked = count($paid_open_unpicked_data[$productAllDesc['Product']['product_sku']]);
													}
													echo  $unpicked ; 
																
													$sum_order = $sum_order + $unpicked;		
													
																			
												?>
												</div>
												
												 
												<?php 
												$qq = 0;
												if(isset($paid_open_unpicked_data[$productAllDesc['Product']['product_sku']])){
													$paidArray = $paid_open_unpicked_data[$productAllDesc['Product']['product_sku']];
													if(count($paidArray) > 0 ){
													echo '<div style="float:right; cursor:pointer;"><i class="glyphicon glyphicon-plus" data-toggle="modal" data-target="#myModal'. $productAllDesc['Product']['id'].'"></i></div>';
												
													$ttt ='';	$total_qty = 0;	 $count = 1;		
													foreach($paidArray  as $val){ 	
  														
														$ttt .= "<div style='border-bottom:1px solid #ddd;padding:3px 0' class='row'>";
														$ttt .= "<div class='col-sm-1'>".$count++ ."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['order_id']."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['quantity'] ."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['destination']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['source_name']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['order_location']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['po_name']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['order_date'] ."</div>";
														$ttt .= "</div>";    
														$total_qty += $val['quantity'];
														 
												}
												
												print ' <div class="modal fade " id="myModal'.$productAllDesc['Product']['id'].'" role="dialog">
												<div class="modal-dialog modal-lg">
												<div class="modal-content">
												<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">'.$productAllDesc['Product']['product_sku'].' PAID Open Orders(Unpicked)</h4>
												</div>
												<div class="modal-body">
														<div style="background:#ddd;padding:5px 0" class="row">
															<div class="col-sm-1"><strong>S.N.</strong></div>
															<div class="col-sm-1"><strong>OrderId</strong></div>
															<div class="col-sm-1"><strong>Quantity</strong></div>
															<div class="col-sm-1"><strong>Destination</strong></div>
															<div class="col-sm-2"><strong>Source</strong></div>
															<div class="col-sm-2"><strong>Location</strong></div>
															<div class="col-sm-2"><strong>Po Name</strong></div>
															<div class="col-sm-2"><strong>OrderDate</strong></div>
														</div>'.$ttt. '
												
													<div class="row">
													<div class="col-sm-2"></div>
													 <div class="col-sm-1"><strong>Total Qty : '.$total_qty .'</strong></div>
													</div>
												</div>
												<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
												</div>												
												</div>
												</div>';
												unset($data);
												}
											}	
											?>

											</td>
												<td class="sorting_1">
												<div style="float:left">
												<?php  
												$un_paid = 0;
												if(isset($unp_data[$productAllDesc['Product']['product_sku']])){
														$unpArray = $unp_data[$productAllDesc['Product']['product_sku']];
																									
														/*foreach($unpArray  as $val){
															$qq += $val['quantity'];
														}*/
														$un_paid = count($unpArray);
													}
													 echo  $un_paid ; 													  
													 $sum_order = $sum_order + $un_paid;
												?></div>
												 
												 
												<?php 
												$qq = 0;
												if(isset($unp_data[$productAllDesc['Product']['product_sku']])){
													$unpArray = $unp_data[$productAllDesc['Product']['product_sku']];
														
													if(count($unpArray) > 0 ){
												echo '<div style="float:right; cursor:pointer;"><i class="glyphicon glyphicon-plus" data-toggle="modal" data-target="#myModalunp'. $productAllDesc['Product']['id'].'"></i></div>';		
													$ttt ='';	$total_qty = 0;	 $count = 1;							
													foreach($unpArray  as $val){
														$ttt .= "<div style='border-bottom:1px solid #ddd;padding:3px 0' class='row'>";
														$ttt .= "<div class='col-sm-1'>".$count++ ."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['num_order_id']."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['quantity'] ."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['destination']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['source_name']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['order_location']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['po_name']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['order_date'] ."</div>"; 
														$ttt .= "</div>";   
														$total_qty += $val['quantity'];
												  }
												
												print ' <div class="modal fade " id="myModalunp'.$productAllDesc['Product']['id'].'" role="dialog">
												<div class="modal-dialog modal-lg">
												<div class="modal-content">
												<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">'.$productAllDesc['Product']['product_sku'].' Unprepare Orders </h4>
												</div>
												<div class="modal-body">
														<div style="background:#ddd;padding:5px 0" class="row">
															<div class="col-sm-1"><strong>S.N.</strong></div>
															<div class="col-sm-1"><strong>OrderId</strong></div>
															<div class="col-sm-1"><strong>Quantity</strong></div>
															<div class="col-sm-1"><strong>Destination</strong></div>
															<div class="col-sm-2"><strong>Source</strong></div>
															<div class="col-sm-2"><strong>Location</strong></div>
															<div class="col-sm-2"><strong>Po Name</strong></div>
															<div class="col-sm-2"><strong>OrderDate</strong></div>
														</div>
														'.$ttt. '
													<div class="row">
													<div class="col-sm-2"></div>
													 <div class="col-sm-1"><strong>Total Qty : '.$total_qty .'</strong></div>
													</div>
												
												</div>
												<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
												</div>												
												</div>
												</div>';
												 
												}
											}	
													 
												?> 
												</td>
												<td class="sorting_1">
												<div style="float:left">
												<?php 
													$picked_count = 0;
													/*if(isset($stock_picked_picklist_generated[$productAllDesc['Product']['product_sku']])){
														 $q = $stock_picked_picklist_generated[$productAllDesc['Product']['product_sku']];
													
													}*/
													if(isset($stock_picked_picklist_data[$productAllDesc['Product']['product_sku']])){
													 $picked_count = count($stock_picked_picklist_data[$productAllDesc['Product']['product_sku']]);
													}
													echo  $picked_count ; 
													$sum_order = $sum_order + $picked_count;	
																								
												?></div>
												<?php 
												$qq = 0;
												if(isset($stock_picked_picklist_data[$productAllDesc['Product']['product_sku']])){
													$paidArray = $stock_picked_picklist_data[$productAllDesc['Product']['product_sku']];
																 					
													if(count($paidArray) > 0 ){
												echo '<div style="float:right; cursor:pointer;"><i class="glyphicon glyphicon-plus" data-toggle="modal" data-target="#myModalstock_'. $productAllDesc['Product']['id'].'"></i></div>';	
													 $ttt ='';	$total_qty = 0; $count = 1;
													 foreach($paidArray  as $val){ 	
													    $ttt .= "<div style='border-bottom:1px solid #ddd;padding:3px 0' class='row'>";
														$ttt .= "<div class='col-sm-1'>".$count++ ."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['order_id']."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['quantity'] ."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['destination']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['source_name']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['order_location']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['po_name']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['picked_date'] ."</div>";
														$ttt .= "</div>";  
														$total_qty += $val['quantity'];
												}
												
												print ' <div class="modal fade " id="myModalstock_'.$productAllDesc['Product']['id'].'" role="dialog">
												<div class="modal-dialog modal-lg">
												<div class="modal-content">
												<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">'.$productAllDesc['Product']['product_sku'].' Picked Orders</h4>
												</div>
												<div class="modal-body">
														<div style="background:#ddd;padding:5px 0" class="row">
															<div class="col-sm-1"><strong>S.N.</strong></div>
															<div class="col-sm-1"><strong>OrderId</strong></div>
															<div class="col-sm-1"><strong>Quantity</strong></div>
															<div class="col-sm-1"><strong>Destination</strong></div>
															<div class="col-sm-2"><strong>Source</strong></div>
															<div class="col-sm-2"><strong>Location</strong></div>
															<div class="col-sm-2"><strong>Po Name</strong></div>
															<div class="col-sm-2"><strong>PickedDate</strong></div>
														</div>
												
												'.$ttt. '<div class="row">
													<div class="col-sm-2"></div>
													 <div class="col-sm-1"><strong>Total Qty : '.$total_qty .'</strong></div>
													</div></div>
												<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
												</div>												
												</div>
												</div>';
												 
												}
											}	
													 
												?></div></td>
																						
												<!--<td class="sorting_1"></td>-->
												 
												<td class="sorting_1"><?php print $sum_order; ?></td>
												
												<?php if( $productAllDesc['Product']['current_stock_level'] != $net_stock ){ ?>
												<td class="sorting_1" style="background:#FF6600"><?php print $productAllDesc['Product']['current_stock_level'];//print $net_stock;?></td><?php }else{?><td class="sorting_1"><?php print $productAllDesc['Product']['current_stock_level'];?></td><?php }?>
												<td class="sorting_1"><?php print $net_stock + $unpicked;?></td>
												<td class="sorting_1">												
												<div style="float:left">
												<?php 
												
											
													$processed = 0;													 
													if(isset($processed_order[$productAllDesc['Product']['product_sku']])){
													 $processed = count($processed_order[$productAllDesc['Product']['product_sku']]);
													}
													echo $processed ; 
													 
																								
												?></div>
												
												<?php												 
												if(isset($processed_order[$productAllDesc['Product']['product_sku']])){
													$processedArray = $processed_order[$productAllDesc['Product']['product_sku']];
																 					
													if(count($processedArray) > 0 ){
												echo '<div style="float:right; cursor:pointer;"><i class="glyphicon glyphicon-plus" data-toggle="modal" data-target="#processed'. $productAllDesc['Product']['id'].'"></i></div>';	
													 $ttt ='';	$total_qty = 0; $count = 1;
													 foreach($processedArray  as $val){ 
													    $ttt .= "<div style='border-bottom:1px solid #ddd;padding:3px 0' class='row'>";
														$ttt .= "<div class='col-sm-1'>".$count++ ."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['order_id']."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['quantity'] ."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['destination']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['source_name']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['order_location']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['po_name']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['process_date'] ."</div>"; 
														$ttt .= "</div>";  
														$total_qty += $val['quantity'];
												}
												 
												print '<div class="modal fade " id="processed'.$productAllDesc['Product']['id'].'" role="dialog">
												<div class="modal-dialog modal-lg">
												<div class="modal-content">
												<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">'.$productAllDesc['Product']['product_sku'].' ProcessedOrders</h4>
												</div>
												<div class="modal-body"> 
														<div style="background:#ddd;padding:5px 0" class="row">
															<div class="col-sm-1"><strong>S.N.</strong></div>
															<div class="col-sm-1"><strong>OrderId</strong></div>
															<div class="col-sm-1"><strong>Quantity</strong></div>
															<div class="col-sm-1"><strong>Destination</strong></div>
															<div class="col-sm-2"><strong>Source</strong></div>
															<div class="col-sm-2"><strong>Location</strong></div>
															<div class="col-sm-2"><strong>Po Name</strong></div>
															<div class="col-sm-2"><strong>ProcessDate</strong></div>
														</div>
														'.$ttt. '
														<div class="row">
														 <div class="col-sm-2"></div>
														 <div class="col-sm-1"><strong>Total Qty : '.$total_qty .'</strong></div>
														</div>
													</div>
												<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
												</div>												
												</div>
												</div>';
												 
												}
											}	
													 
												?>
												</td>	
												 <td class="sorting_1">												
												<div style="float:left">
												<?php 
												
											
													$scaned = 0;													 
													if(isset($scaned_order[$productAllDesc['Product']['product_sku']])){
													 $scaned = count($scaned_order[$productAllDesc['Product']['product_sku']]);
													}
													echo $scaned ; 
													 
																								
												?></div>
												
												<?php												 
												if(isset($scaned_order[$productAllDesc['Product']['product_sku']])){
													$scanedArray = $scaned_order[$productAllDesc['Product']['product_sku']];
																 					
													if(count($scanedArray) > 0 ){
												echo '<div style="float:right; cursor:pointer;"><i class="glyphicon glyphicon-plus" data-toggle="modal" data-target="#scaned'. $productAllDesc['Product']['id'].'"></i></div>';	
													 $ttt ='';	$total_qty = 0; $count =1;
													 foreach($scanedArray  as $val){ 
													    $ttt .= "<div style='border-bottom:1px solid #ddd;padding:3px 0' class='row'>";
														$ttt .= "<div class='col-sm-1'>".$count++ ."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['order_id']."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['quantity'] ."</div>";
														$ttt .= "<div class='col-sm-1'>".$val['destination']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['source_name']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['order_location']."</div>";
														$ttt .= "<div class='col-sm-2'>".$val['po_name']."</div>";
														$ttt .= "<div class='col-sm-2'>". $val['scan_date'] ."</div>";
														$ttt .= "</div>";  
														$total_qty += $val['quantity'];
												}
												 
												print '<div class="modal fade " id="scaned'.$productAllDesc['Product']['id'].'" role="dialog">
												<div class="modal-dialog modal-lg">
												<div class="modal-content">
												<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">'.$productAllDesc['Product']['product_sku'].' Scaned Orders</h4>
												</div>
												<div class="modal-body"> 
														<div style="background:#ddd;padding:5px 0" class="row">
															<div class="col-sm-1"><strong>S.N.</strong></div>
															<div class="col-sm-1"><strong>OrderId</strong></div>
															<div class="col-sm-1"><strong>Quantity</strong></div>
															<div class="col-sm-1"><strong>Destination</strong></div>
															<div class="col-sm-2"><strong>Source</strong></div>
															<div class="col-sm-2"><strong>Location</strong></div>
															<div class="col-sm-2"><strong>Po Name</strong></div>
															<div class="col-sm-2"><strong>ScanDate</strong></div>
														</div>
														'.$ttt. '
														<div class="row">
														 <div class="col-sm-2"></div>
														 <div class="col-sm-1"><strong>Total Qty : '.$total_qty .'</strong></div>
														</div>
													</div>
												<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
												</div>												
												</div>
												</div>';
												 
												}
											}	
													 
												?>
												</td>
                                                										
                                            </tr>
											<?php } ?>
										</tbody>
									</table>
									
									<div class="" style="margin:0 auto; width:350px">
										 <ul class="pagination">
										  <?php
											   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
											   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
											   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										  ?>
										 </ul>
									</div>
									</div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
				<!-- BEGIN FOOTER -->
				
				 
				<footer class="bg-white">
					<div class="pull-left">
						<span class="pull-left margin-right-15">&copy; 2015 WMS by JijGroup.</span>
						<ul class="list-inline pull-left">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
						</ul>
					</div>
				</footer>
				<!-- END FOOTER -->
            </div>
    </div>
</div>
 
 
<script>

 
$('#ProductSearch').keypress(function(e){ 
	if(e.which == 13){//Enter key pressed
		if($("#ProductSearch").val()){
		 window.location.href = '<?php echo Router::url('/', true); ?>Reports/getReport/'+$("#ProductSearch").val(); 
		}
	}
});
  
	
</script>
