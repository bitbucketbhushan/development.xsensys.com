<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Get EOD(Sales/Stock) Data </h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div> 
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-9 col-lg-offset-2">
                                <div class="panel-body padding-bottom-40">
									
											
									 <div class="form-group">  
										                                      
                                        <div class="col-lg-7"> 
										<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600  border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
										<form name="frm" id="frm" method="post" action="<?php echo Router::url('/', true) ?>Reports/downloadSalesEod" onsubmit="return downloadSalesEod();">   
 											 
											<input name="start_input" type="hidden" id="start_input">
											<input name="end_input" type="hidden" id="end_input">
										</form>
                                        </div>   
										
 
                                       
										 
                                        <div class="linkFile col-lg-2">                                                      
											<button class="btn btn-success" onclick="downloadSalesEod()"><span class="glyphicon glyphicon-save"></span> Download Sales Eod Data</button>								<br>
											<button class="btn btn-info" onclick="downloadAsinEod()" style="margin:10px 0px;"><span class="glyphicon glyphicon-save"></span> Download ASIN Sales Data</button>
											<br>
											<button class="btn btn-primary" onclick="downloadFbaEod()" style="margin-bottom:10px"><span class="glyphicon glyphicon-save"></span> Download FBA Sales Data</button> <br>
											<button class="btn btn-success" onclick="downloadFbaMerchantSalesEod()" style="margin-bottom:10px"><span class="glyphicon glyphicon-save"></span> Download FBA & FBM Sales Data</button> 
											
											<br>
											<button class="btn btn-info" onclick="downloadAsinChannelSkuEod()" style="margin:10px 0px;"><span class="glyphicon glyphicon-save"></span>Download FBA & FBM Channel SKU Sales Data</button>
											
										
										</div>
                                        
                                      </div>
									  
									 <div class="form-group">  
										                                      
                                      <div class="col-lg-7"> 
										  <!--<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600  border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
										<form name="stock_frm" id="stock_frm" method="post" action="<?php echo Router::url('/', true) ?>Reports/downloadStockEod" onsubmit="return downloadStockEod();">   
 											 
											<input name="stock_start_input" type="hidden" id="stock_start_input">
											<input name="stock_end_input" type="hidden" id="stock_end_input">
										</form> -->
                                        </div>  
										
 
                                       
										 
                                        <div class="linkFile col-lg-2">                                                      
											<button class="btn btn-warning" onclick="downloadStockEod()"><span class="glyphicon glyphicon-save"></span> Download  Stock Eod Data</button> 											 
													 </div>
										
                                        
                                      </div>
										 
                                      <div class="form-group">  
										                                      
                                      <div class="col-lg-7"> &nbsp;
										 
                                        </div>  
										
 
                                       
										 
                                        <div class="linkFile col-lg-2">                                                      
 																					 
                                        </div>
                                        
                                      </div>
									  
                                    </div>
                            </div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
 
<script>
 
	function downloadSalesEod()
	{
			var start = $('div.daterangepicker_start_input input:text').val();
			var end  = $('div.daterangepicker_end_input input:text').val();
			$( '#start_input' ).val(start);
			$( '#end_input' ).val(end);
			window.location.href = "<?php echo Router::url('/', true) ?>Reports/downloadSalesEod?start="+start+"&end="+end;
			
			//document.getElementById("frm").submit();
		 
			return false; 
	}
	
	function downloadStockEod()
	{
		 	var start = $('div.daterangepicker_start_input input:text').val();
			var end  = $('div.daterangepicker_end_input input:text').val();
			$( '#start_input' ).val(start);
			$( '#end_input' ).val(end);
			window.location.href = "<?php echo Router::url('/', true) ?>Reports/downloadStockEod?start="+start+"&end="+end;
			
			/*$( '#stock_start_input' ).val($('div.daterangepicker_start_input input:text').val());
			$( '#stock_end_input' ).val($('div.daterangepicker_end_input input:text').val());
			document.getElementById("stock_frm").submit();*/
		 
			return false; 
	}
	function downloadFbaEod()
	{
		 	var start = $('div.daterangepicker_start_input input:text').val();
			var end  = $('div.daterangepicker_end_input input:text').val();
			$( '#start_input' ).val(start);
			$( '#end_input' ).val(end);
			window.location.href = "<?php echo Router::url('/', true) ?>Reports/downloadFbaEod?start="+start+"&end="+end;
  			return false; 
	}
	function downloadAsinEod()
	{
		 	var start = $('div.daterangepicker_start_input input:text').val();
			var end  = $('div.daterangepicker_end_input input:text').val();
			$( '#start_input' ).val(start);
			$( '#end_input' ).val(end);
			window.location.href = "<?php echo Router::url('/', true) ?>Reports/downloadAsinEod?start="+start+"&end="+end;
  			return false; 
	}
	function downloadAsinChannelSkuEod()
	{
		 	var start = $('div.daterangepicker_start_input input:text').val();
			var end  = $('div.daterangepicker_end_input input:text').val();
			$( '#start_input' ).val(start);
			$( '#end_input' ).val(end);
			window.location.href = "<?php echo Router::url('/', true) ?>Reports/downloadAsinChannelSkuEod?start="+start+"&end="+end;
  			return false; 
	}
	function downloadFbaMerchantSalesEod()
	{
		 	var start = $('div.daterangepicker_start_input input:text').val();
			var end  = $('div.daterangepicker_end_input input:text').val();
			$( '#start_input' ).val(start);
			$( '#end_input' ).val(end);
			window.location.href = "<?php echo Router::url('/', true) ?>Reports/downloadFbaMerchantSalesEod?start="+start+"&end="+end;
  			return false; 
	} 
</script>
