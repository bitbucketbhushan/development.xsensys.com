<div class="modal fade pop-up-4" tabindex="-1" role="dialog" id="pop-up-4" aria-labelledby="myLargeModalLabel-4" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Search</h4>
         </div>
         <div class="modal-body bg-grey-100">
            <div class="panel no-margin" style="clear:both">
     <div class="panel-body padding-top-20" >
     <div class="row">
		<div class="col-lg-12">			 
				<div class="form-group">
					<label class="control-label col-lg-3" for="username">Service Id</label>                                        
					<div class="col-lg-7">                                            
						<input type="text" id="searchGlobal" value="" class="form-control" name="data[OpenOrder][searchGlobal]">                                            
					</div>
				</div>
		</div>
	   <div class="row">
         <div class="col-lg-12" style="text-align:center; padding-bottom: 7px;"> 
         
         </div>
        </div>
       </div>
      </div> 
     </div>
    </div>
   </div>
  </div>
 </div>
 <div class="showOPenOrderGlobalSearchResult" ></div>
 
<script>
   $(document).ready(function()
   {
       $('#pop-up-4').modal('show');
   });
   
   $('#searchGlobal').bind('keypress', function(e)   
	{
		
	   if(e.which == 13) 
	   {
		   if($('#searchGlobal').val() != '')
			{
				submitglobalSearchKey();
			}
			else
			{
				
			}
		}
	});
	
	function submitglobalSearchKey()
	{
		
		var searchKey	=	$('#searchGlobal').val();
		$.ajax({
			url     	: '/Linnworksapis/getOpenOrderSearchResult',
			type    	: 'POST',
			data    	: {  searchKey : searchKey },
			beforeSend	 : function() {
			$( ".outerOpac" ).attr( 'style' , 'display:block');
			},   			  
			success 	:	function( data  )
			{
				if( data == 1 )
				{
					$( ".outerOpac" ).attr( 'style' , 'display:none');
					swal( "Oops!!!, ( < "+ searchKey +" >) not found here.");
				}
				else
				{
					$( ".outerOpac" ).attr( 'style' , 'display:none');
					$('.showOPenOrderGlobalSearchResult').html( data );
				}
			}                
		});
	}
</script>

