<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.col-60 {    width: 60%;    float: left;}
	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>FBA Inventory</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
  						<form method="get" name="searchfrm" id="searchfrm" action="<?php echo Router::url('/', true) ?>FbaInventory/ViewStock" enctype="multipart/form-data">	
						<div class="col-sm-12 no-padding">
							<div class="col-lg-3 no-padding">
									<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Enter Order Id, SKU Or ASIN." name="searchkey" class="form-control searchString" />
									<input type="hidden" value="<?php echo isset($_REQUEST['date']) ? $_REQUEST['date'] :''; ?>"  name="date" />
								</div>
								
								<div class="col-10"> &nbsp;
									<button type="button" class="btn btn-success" onclick="changeStore()">&nbsp;<i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
								</div> 
								
						</div>
						</form>
						 	
						</div>	
					<div class="panel-body no-padding-top bg-white">
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
  								<div class="col-12">Sku</div>											
 								<div class="col-12">Seller sku</div>
								<div class="col-12">Fnsku</div>											
 								<div class="col-10">Asin</div>
								<div class="col-7">Total Units</div> 
								<div class="col-7">Inbound</div> 
								<div class="col-5">Country</div>
								<div class="col-10">Alert</div>	
								<div class="col-7">Price</div>
								<div class="col-5">Status</div>
 								<div class="col-12">Added Date</div>	
 							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						 if(count($all_orders) > 0) {	 ?>
							<?php foreach( $all_orders as $items ) {   ?>
								<div class="row sku" id="r_<?php echo $items['FbaUploadInventory']['id']; ?>">
									<div class="col-lg-12">	
  										 
  										<div class="col-12"><small><?php echo $items['FbaUploadInventory']['sku']?></small></div>
										<div class="col-12"><small><?php echo $items['FbaUploadInventory']['seller_sku']?></small></div>
										<div class="col-12"><small><?php echo $items['FbaUploadInventory']['fnsku']?></small></div>
										<div class="col-10"><small><?php echo $items['FbaUploadInventory']['asin']?></small></div>
										<div class="col-7"><small><?php echo $items['FbaUploadInventory']['total_units']?></small></div>
										<div class="col-7"><small><?php echo $items['FbaUploadInventory']['inbound']?></small></div>									
										<div class="col-5"><small><?php echo $items['FbaUploadInventory']['country']?></small></div>
										<div class="col-10"><small><?php echo $items['FbaUploadInventory']['alert']?></small></div>
 										<div class="col-7"><small><?php echo $items['FbaUploadInventory']['price']?></small></div> 
										
										<div class="col-5"><small>
										<?php if($items['FbaUploadInventory']['status'] > 0){?>
											<a href="<?php echo Router::url('/', true)?>FbaInventory/ChangeInventoryStatus/
										<?php echo $items['FbaUploadInventory']['id']?>/0" style="color:#CC3333;text-decoration:underline;" title="Click To Make Active">InActive</a>
										<?php }else{?>
											<a href="<?php echo Router::url('/', true)?>FbaInventory/ChangeInventoryStatus/
										<?php echo $items['FbaUploadInventory']['id']?>/1" style="color:#009900;text-decoration:underline;" title="Click To Make InActive">Active</a>
										<?php }?>
										
										</small></div> 
										
 										<div class="col-12"><small><?php echo $items['FbaUploadInventory']['added_date']; ?></small></div>	
 										 
									</div>												
								</div>
								 
							<?php }?>
							
						<div class="col-sm-12 text-right" style="margin-bottom: 10px;padding-bottom: 10px;">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>
 
<script>	
 
function changeStore()
{
	var start = $('div.daterangepicker_start_input input:text').val();
	var end  = $('div.daterangepicker_end_input input:text').val();
	$( '#start_input' ).val(start);
	$( '#end_input' ).val(end);
	document.getElementById("searchfrm").action = "<?php echo Router::url('/', true) ?>FbaInventory/ViewStock?";
	document.searchfrm.submit();
 }
function download()
{
		var start = $('div.daterangepicker_start_input input:text').val();
		var end  = $('div.daterangepicker_end_input input:text').val();
		$( '#start_input' ).val(start);
		$( '#end_input' ).val(end);
		document.getElementById("searchfrm").action = "<?php echo Router::url('/', true) ?>FbaInventory/download?";
		document.searchfrm.submit();
		//window.location.href = "<?php echo Router::url('/', true) ?>FbaInventory/download?start="+start+"&end="+end;
		//return false; 
}
</script>
