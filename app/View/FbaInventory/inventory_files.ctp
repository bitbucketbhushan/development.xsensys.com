<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.col-60 {    width: 60%;    float: left;}
	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>FBA Inventory Files<?php print $this->Session->flash(); ?> </h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						
						  	   <?php							
											print $this->form->create( 'inventory', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/FbaInventory/InventoryUploadSave', 'type'=>'post','id'=>'inventory', 'onSubmit' => 'return Upload()' ) );
										?>
										<div class="form-group" style="padding: 10px; background-color: #ddd; border-radius: 10px; margin-bottom:0px;">
											<label class="control-label col-10">Amazon File</label> 
											<div class="col-lg-2"><select name="inventory[store]" id="store" class="form-control">
												<option value="">Select Store</option>
												<option value="costbreaker">Costbreaker</option>
												<option value="rainbow">Rainbow</option>
												<option value="marec">Marec</option>
											
											</select>      
											</div>     
											<!--<div class="col-lg-2">
 												<div class="input-group date" data-provide="datepicker">
													<input type="text" id="stock_date" name="stock_date" class="form-control datepicker">
													<div class="input-group-addon">
														<span class="glyphicon glyphicon-th"></span>
													</div>
												</div> 
											</div>-->	
											<div class="col-lg-4">
												<div class="input-group">
													<span class="input-group-btn">
														<span class="btn btn-primary btn-file">
															Browse  <?php
																		print $this->form->input('uploadfile', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'', 'required'=>false) );
																	  ?>
														</span>
													</span>
															<input type="text" placeholder="No file selected" readonly="" class="form-control">
															<span class="input-group-addon no-padding-top no-padding-bottom">
													
													</span>
											</div>
											
                                        </div>
										<div class="col-lg-2">
										<?php
											echo $this->Form->button('Upload', array(
											'type' => 'submit',
 											'escape' => true,
											'class'=>'btn bg-green-500 color-white btn-dark padding-left-10 padding-right-10'
											 ));	
										?>
										</div>
											<div class="col-lg-2 text-right"><button type="button" class="btn btn-success" onclick="DownloadStock();">Download Stock</button></div>
                                      </div>
                          </form>
						<!--<div class="col-lg-12" style="padding-top:10px;" >
									 
									<div class="col-lg-2 stock">
										<div class="input-group date" data-provide="datepicker">
											<input type="text" id="stock_date" name="stock_date" class="form-control datepicker">
											<div class="input-group-addon">
												<span class="glyphicon glyphicon-th"></span>
											</div>
										</div> 
									</div>	
 									<div class="col-lg-2"><button type="button" class="btn btn-primary" onclick="ViewStock();">View Stock</button></div>
									<div class="col-lg-2"><button type="button" class="btn btn-success" onclick="DownloadStock();">Download Stock</button></div>
							  </div>-->	   
					</div>	
					
					 
					<div class="panel-body no-padding-top bg-white">
						 
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
  								<div class="col-20">File Name</div>
								<div class="col-20">Store</div>				 								
  								<div class="col-20">Uploaded Date</div> 
								<!--<div class="col-10">Status</div>-->
 								<div class="col-20">Action</div>	
 							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						 if(count($all_orders) > 0) {	 ?>
							<?php foreach( $all_orders as $items ) {   ?>
								<div class="row sku" id="r_<?php echo $items['FbaInventoryFile']['id']; ?>">
									<div class="col-lg-12">	
  										<div class="col-20"><small><?php echo $items['FbaInventoryFile']['file_name']?></small></div>
										<div class="col-20"><small><?php echo $items['FbaInventoryFile']['store']?></small></div>
 										<div class="col-20"><small><?php echo $items['FbaInventoryFile']['uploaded_date']?></small></div> 
 										<!--<div class="col-10"><small>
										<?php if($items['FbaInventoryFile']['status'] > 0){?>
											<a href="<?php echo Router::url('/', true)?>FbaInventory/ChangeStatus/
										<?php echo $items['FbaInventoryFile']['id']?>/0" style="color:#CC3333;text-decoration:underline;" title="Click To Make Active">InActive</a>
										<?php }else{?>
											<a href="<?php echo Router::url('/', true)?>FbaInventory/ChangeStatus/
										<?php echo $items['FbaInventoryFile']['id']?>/1" style="color:#009900;text-decoration:underline;" title="Click To Make InActive">Active</a>
										<?php }?>
										
										</small></div> -->
  										<div class="col-20"><small><a href="<?php echo Router::url('/', true)?>FbaInventory/Inventory/
<?php echo $items['FbaInventoryFile']['id']?>" class="btn btn-info btn-sm" role="button">View</a> &nbsp;<a href="<?php echo Router::url('/', true)?>FbaInventory/FbaDeleteFile/
<?php echo $items['FbaInventoryFile']['id']?>" class="btn btn-danger btn-sm" role="button" onclick="return confirm('Are you sure want to delete?')">Delete</a> &nbsp;<a href="<?php echo Router::url('/', true)?>FbaInventory/FbaDownloadFile/
<?php echo $items['FbaInventoryFile']['id']?>" class="btn btn-success btn-sm" role="button">Download File</a></small></div>	
 									</div>												
								</div>
								 
							<?php }?>
							
						<div class="col-sm-12 text-right" style="margin-bottom: 10px;padding-bottom: 10px;">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	
function Upload(){
 	if($("#store option:selected").val() == ''){
		alert('Please select store.');
		$("#store").focus();
		return false;
	}if($("#stock_date").val() == ''){
		alert('Please select date.');
		$("#stock_date").focus();
		return false;
	}
}
$("body").on("change", ".invoice", function()
{
	$(".outerOpac").show();
	//$('.outerOpac span').css({'z-index':'10000'});
	
	//var id			=	$(this).attr("id");
	var orderid			=	$(this).attr("for");	
	var field_name  	=	$(this).attr("name");	
	var amazon_order_id =	$(this).attr("amz");
	var vals			=	$(this).val();
	
	$.ajax({
			dataType: 'json',
			url :   '<?php echo Router::url('/', true)?>Invoice/Address',
			type: "POST",
			data : {order_id:orderid,field:field_name,val:vals,amazon_order_id:amazon_order_id},
			success: function(txt, textStatus, jqXHR)
			{	 
				$(".outerOpac").hide();
				 
			}		
		});

}); 		
		
function updateStatus(t,v){	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaInventoryFiles/updateStatus',
		type: "POST",				
		cache: false,			
		data : {status:$(t).find("option:selected").val(),id:v },
		success: function(data, textStatus, jqXHR)
		{	
			 	
			if(data.error){
				alert(data.msg);
			} 							
		}		
 	});  		 
}



 
function changeStore()
{
	var start = $('div.daterangepicker_start_input input:text').val();
	var end  = $('div.daterangepicker_end_input input:text').val();
	$( '#start_input' ).val(start);
	$( '#end_input' ).val(end);
	document.getElementById("searchfrm").action = "<?php echo Router::url('/', true) ?>FbaInventory/index?";
	document.searchfrm.submit();
 }
function DownloadStock()
{ 
	window.location.href = "<?php echo Router::url('/', true) ?>FbaInventory/Fbadownload?store="+$("#store option:selected").val();;
		//return false; 
}
</script>
