<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>All Shipments</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							  
						</div>	
						
					<div class="panel-body no-padding-top bg-white">
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-sm-2">Shipment Name</div>		
								<div class="col-sm-1">Shipment Id</div>	
								<div class="col-sm-1">Plan Id</div>			
								<div class="col-sm-4">Ship To</div>																				
								<div class="col-sm-1"><center>SKU / Units</center></div>	
								<div class="col-sm-1"><center>Pack List</center></div>	
								<div class="col-sm-1">Status</div>
								<div class="col-sm-1">Action</div>	
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						 if(count($all_shipments) > 0) {	 ?>
							<?php foreach( $all_shipments as $items ) {     ?>
								<div class="row sku" id="r_<?php echo $items['FbaShipment']['id']; ?>">
									<div class="col-lg-12">	
										<div class="col-sm-2"><small><?php echo $items['FbaShipment']['shipment_name']; ?></small></div>	
										<div class="col-sm-1"><small><?php echo $items['FbaShipment']['shipment_id']; ?></small></div>	
										<div class="col-sm-1"><small><?php echo $items['FbaShipment']['plan_id'] ?></small></div> 		
										<div class="col-sm-4"><small><?php echo $items['FbaShipment']['ship_to'] ?></small></div>											
										
										<div class="col-sm-1"><small><center><?php echo $items['FbaShipment']['total_skus'];  ?> / <?php echo $items['FbaShipment']['total_units']; ?></center></small></div>
										<div class="col-sm-1"><small><center><?php echo $items['FbaShipment']['pack_list']; ?></center></small></div>	
										<div class="col-sm-1"> <select name="status" id="status" style="font-size:12px; padding:5px; border:1px solid #c6c6c6; width:100%;" onchange="updateStatus(this,<?php echo $items['FbaShipment']['id']; ?>)">
														<option value="">--Shipment Status--</option>
														<option value="ready_to_prepare" <?php if($items['FbaShipment']['status'] == 'ready_to_prepare') echo'selected=selected';?>>Ready to Prepare</option>
														<option value="preparing" <?php if($items['FbaShipment']['status'] == 'preparing') echo'selected=selected';?>>Preparing</option>
														<option value="exported" <?php if($items['FbaShipment']['status'] == 'exported') echo'selected=selected';?>>Exported</option>
														<option value="delivered" <?php if($items['FbaShipment']['status'] == 'delivered') echo'selected=selected';?>>Delivered</option>
														<option value="reconciled_complete" <?php if($items['FbaShipment']['status'] == 'reconciled_complete') echo'selected=selected';?>>Reconciled-Complete</option>
														 												 
													</select> 
										 

</div>
										<div class="col-sm-1"> <a href="<?php echo Router::url('/', true) ?>FbaShipments/ExportShipment/<?php echo $items['FbaShipment']['id']; ?>" title="View Shipment Items" style="font-size:10px; color:#000099; text-decoration:underline;">View Items</a><br /> 
										<a href="<?php echo Router::url('/', true) ?>FbaExportConsole/index/<?php echo $items['FbaShipment']['id']; ?>" title="Export Shipment" style="font-size:10px; color:#000099; text-decoration:underline;">Export Shipment</a>
										
										<?php /*?><a href="<?php echo Router::url('/', true) ?>FbaShipments/ExportShipment/<?php echo $items['FbaShipment']['id']; ?>" title="Export Shipment" style="font-size:10px; color:#000099; text-decoration:underline;">Export Shipment</a><?php */?></div>	
									</div>												
								</div>
								 
							<?php }?>
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	

$('#filter_shipment').click(function(){  
	document.getElementById("shipmentFrm").submit();		 
});
	
function updateStatus(t,v){	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaShipments/updateStatus',
		type: "POST",				
		cache: false,			
		data : {status:$(t).find("option:selected").val(),id:v },
		success: function(data, textStatus, jqXHR)
		{	
			 	
			if(data.error){
				alert(data.msg);
			} 							
		}		
 	});  		 
}
</script>
