<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px; }
	.highlight{border:1px #FF0000 solid;background:#FF3300;}
	.notequal{border:1px #993300 solid; background:#CCFF33;}
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>				
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>				
				<h4 style="float:left;">Export Shipment</h4>
				<a href="<?php echo Router::url('/', true) ?>FbaShipments" class="btn btn-primary" style="float:right;">Back</a>
				<br /><br />
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							 
						
					<div class="panel-body no-padding-top bg-white">
					</div>	
					<?php 
								App::Import('Controller', 'FbaShipments'); 
								$obj = new FbaShipmentsController;
								 
							    $po_items = array();
								$shipment_po  = $obj->getExportShipment($this->params['pass'][0]);
								if(count($shipment_po) > 0){								  
									foreach($shipment_po['FbaShipmentItemsPo'] as $v){
										$po_items[$v['fnsku'].'_'.$v['master_sku']] = $v;
									}
								}
								 
					?> 
						<?php							
                              print $this->form->create( 'Shipment', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/FbaShipments/SaveExportShipment', 'type'=>'post','id'=>'shipment_frm' ) );
                               
                            ?>
						<div class="row" id="r1">	
						<div class="col-sm-12" style="padding:5px; background-color:#F2F2F2;">
							<?php  if(count($shipment_items['FbaShipment']) > 0) {	  ?>
								<div class="col-sm-2"><strong>Plan Id:</strong> <?php print $shipment_items['FbaShipment']['plan_id']; ?></div>
								<div class="col-sm-2"><strong>Total Merchant SKU:</strong> <?php print $shipment_items['FbaShipment']['total_skus']; ?></div>
								<div class="col-sm-2"><strong>Total Units:</strong> <?php print $shipment_items['FbaShipment']['total_units']; ?> </div>	
								<div class="col-sm-3"><strong>Pack List:</strong> <?php print $shipment_items['FbaShipment']['pack_list']; ?></div>	
								<div class="col-sm-3"><strong>Date:</strong> <?php print $shipment_items['FbaShipment']['added_date']; ?></div>	
								
								<div class="col-sm-4"><strong>Shipment Name:</strong> <?php print $shipment_items['FbaShipment']['shipment_name']; ?></div>		
								<div class="col-sm-8"><strong>Ship To:</strong> <?php print $shipment_items['FbaShipment']['ship_to']; ?></div>
								<input type="hidden" name="_po[shipment_name]" value="<?php print $shipment_items['FbaShipment']['shipment_name']; ?>" />
								<input type="hidden" name="_po[shipment_id]" value="<?php print $shipment_items['FbaShipment']['shipment_id']; ?>" />
							 <?php }if(count($shipment_po) > 0){?>
							  <input type="hidden" name="_po[shipments_po_inc_id]" value="<?php print $shipment_po['FbaShipmentsPo']['id']; ?>" />
							   <?php } ?>
							   <input type="hidden" name="_po[shipment_inc_id]" value="<?php print $this->params['pass'][0]; ?>" />
							   
						</div> 
						</div> 
						<div class="row head" style="clear:both;" id="r2">
		 					  <div class="col-sm-12">
								<div class="col-sm-2">Merchant Sku</div>
								<div class="col-sm-2">Master Sku</div>
								<div class="col-sm-2">ASIN / FNSKU</div>
								<div class="col-sm-3">Title</div>		
								<div class="col-sm-1">P. Qty<br /><small style="font-size:10px;">(Preparation)</small></div>	
								<div class="col-sm-1">Qty to Label</div>	
								<div class="col-sm-1">Qty to Send</div>	
							</div>								
						</div>	
						<div class="body" id="results">
							<div id="r3">
							<?php  if(count($shipment_items['FbaShipmentItem']) > 0) {	 ?>
							
								<?php foreach( $shipment_items['FbaShipmentItem'] as $items ) {   ?>
									<div class="row sku" id="r_<?php echo $items['id']; ?>">
										<div class="col-lg-12">	
											<?php  
												$master_sku    = $obj->getAwsMapping($items['merchant_sku']);
												$packaging_id  = $obj->getFbaPackaging($items['merchant_sku']);											
											?>
											<div class="col-sm-2"><?php echo $items['merchant_sku']; ?>	
											
												<div>
													<select name="packaging[<?php echo $items['merchant_sku'] ?>]" style="font-size:12px; padding:5px; border:1px solid #c6c6c6; width:100%;" onchange="updatePackaging(this,'<?php echo $items['merchant_sku'] ?>')">
														<option value="">--Assign Packaging--</option>
														<?php foreach($envelope as $id => $pack){
															if($packaging_id == $id){
																echo '<option value="'.$id.'" selected="selected">'.$pack.'</option>';
															}else{
																echo '<option value="'.$id.'">'.$pack.'</option>';
															}
														}
														?>														 
													</select>
												</div>
											</div>
											<div class="col-sm-2"><?php echo $master_sku  ?></div>
											
											<div class="col-sm-2">
												<div><?php echo $items['asin']; ?></div>
												<div style="border-top:1px dashed #bbb; width:55%;"><?php echo $items['fnsku'] ?></div>
											</div>	
											<div class="col-sm-3" style="font-size:13px;"><?php echo (strlen($items['title']) > 70) ? substr($items['title'],0,70).'...' : $items['title']; 	?></div>	
											
											
																						
											<div class="col-sm-1"> 
											 <input type="hidden" name="<?php echo $items['fnsku']?>[merchant_sku]" value="<?php echo $items['merchant_sku'] ?>" />
											 <input type="hidden" name="<?php echo $items['fnsku']?>[master_sku]" value="<?php echo $master_sku ?>" />
											 <input type="hidden" name="<?php echo $items['fnsku']?>[prep_qty]" value="<?php echo $items['shipped'] ?>" />
											
											<?php 								
											 
												echo "1";
												$shipped_qty = '';
												if(isset($po_items[$items['fnsku'].'_'.$master_sku])){
													$shipped_qty = $po_items[$items['fnsku'].'_'.$master_sku]['shipped_qty'];
													$inc_id = $po_items[$items['fnsku'].'_'.$master_sku]['id'];
													echo '<input type="hidden" name="'.$items['fnsku'].'[shipments_items_po_inc_id]" value="'.$inc_id.'" />';
												}
												
											 
											  
											?>
											</div>
											
											<div class="col-sm-2">
											<div style="text-align:left; float:left;" id="l_<?php echo $items['id']; ?>"><?php echo $items['shipped']; ?></div>
											<div style="float:right">
											
												<div style="text-align:left; float:left;"><input type="number" name="<?php echo $items['fnsku']?>[qty]" id="input_<?php echo $items['id']?>" value="<?php echo $shipped_qty?>" style="width:55px;" onchange="enterNumber('<?php echo $items['id'];?>')"/></div>
												<div style="text-align:right; float:right;"><a href="javascript:void(0);" title="Print Label" class="btn btn-success btn-xs" id="label_<?php echo $items['id']; ?>"><i class="fa fa-print" onclick="printLabel(<?php echo $items['id']; ?>)"  ></i></a>&nbsp; <a href="javascript:void(0);" title="Print Slip" class="btn btn-warning btn-xs" id="slip_<?php echo $items['id']; ?>"><i class="fa fa-print" onclick="printSlip(<?php echo $items['id']; ?>)" ></i></a></div>
											</div>
											</div>
											
										 
											
										</div>		
										
										 <?php 
											
											if($master_sku){
												if(isset($bundle_mapping[$master_sku])){
												
												?>
												<div class="col-sm-2">&nbsp;&nbsp;&nbsp;&nbsp;</div>
												
												<div class="col-sm-10"><div style=" margin-bottom:5px; background-color:#BAEEF9; font-size:11px; padding-left:0px !important;padding-right:0px !important;">
												<?php  
													 
													foreach($bundle_mapping[$master_sku] as $sku => $val){
													
													$shipped_qty = '';
													if(isset($po_items[$items['fnsku'].'_'.$sku])){
														$shipped_qty = $po_items[$items['fnsku'].'_'.$sku]['shipped_qty'];
														$inc_id = $po_items[$items['fnsku'].'_'.$sku]['id'];
														echo '<input type="hidden" name="'.$items['fnsku'].'[shipments_items_po_inc_id]" value="'.$inc_id.'" />';
													}
													
													$id = $items['id'].'_'.$val['bid'];
													$name = $items['fnsku'].'[qty]['.$val['bid'].']';
													echo '<div class="col-sm-12" style="background-color:#BAEEF9; margin-left:0px !important;margin-right:0px !important; font-size:11px;border-top:1px dashed #bbb;">'; 
													echo '<div class="col-sm-2" style="font-size:11px;">'.$sku.'</div>'; 												
													echo '<div class="col-sm-6" style="text-align:left;font-size:11px; padding-left:32px">'.$val['title'].'</div>';
													echo '<div class="col-sm-1 bundle_'.$items['id'].'" for="'.$id .'" style="text-align:center; font-size:11px;">'.$val['qty'].'</div>';
													echo '<div class="col-sm-1" style="text-align:right; padding-right:15px; font-size:11px;" id="l_'.$id.'">' . $val['qty'] * $items['shipped'] .'</div>';
													echo '<div class="col-sm-2" style="text-align:right;padding-right:68px;"><span name="'.$name.'" id="span_'.$id.'">-</span></div>';
													echo  '</div>';
													} 
												?></div>  
												</div>
												<?php
												  }
											}
											?>	
									</div>
									 
								<?php }?>
							
								<div class="row" style="<?php  if(count($shipment_po) > 0){ echo 'display:none;';}?>">
										<div class="col-lg-12">												
											<div class="col-sm-2" style="float:right; text-align:right;"><a href="javascript:void(0);" class="btn btn-warning  btn-sm finalise_shipment">FINALISE SHIPMENT</a></div>
										</div>
								</div>
							</div>	
						<div id="r4" style="<?php  if(count($shipment_po) == 0){ echo 'display:none;';}?>">	
						
								<div class="row" style="<?php  if(count($shipment_po) > 0){ echo 'display:none;';}?>">
										<div class="col-lg-12">	
											<div class="col-sm-2" style="float:right; text-align:right;"><a href="javascript:void(0);" class="btn btn-info btn-xs shipment_items"> << SHOW SHIPMENT ITEMS </a></div>
										</div>
								</div> 
							
							<div class="row sku">		
								<div class="col-lg-12 col-sm-6">	
									<div class="col-sm-1">Ref.No. :</div><div class="col-sm-4"><input type="text" name="_po[ref]" value="<?php  if(count($shipment_po) > 0){ print $shipment_po['FbaShipmentsPo']['ref'];}?>" class="form-control" /></div>
									<div class="col-sm-1">Courier :</div><div class="col-sm-4"><input type="text" name="_po[courier]" value="<?php  if(count($shipment_po) > 0){ print $shipment_po['FbaShipmentsPo']['courier'];}?>" class="form-control" /></div>
								</div>
							</div>
							 
							<div class="row sku">	
								<div class="col-lg-12 col-sm-6">		
									<div class="col-sm-1">Tracking :</div><div class="col-sm-4"><input type="text" name="_po[tracking]" value="<?php  if(count($shipment_po) > 0){ print $shipment_po['FbaShipmentsPo']['tracking'];}?>" class="form-control" /></div>
									<div class="col-sm-1">Contact :</div><div class="col-sm-4"><input type="text" name="_po[contact]" value="<?php  if(count($shipment_po) > 0){ print $shipment_po['FbaShipmentsPo']['contact'];}?>" class="form-control" /></div>
								</div>
							</div>
							
							<div class="row sku">	
								<div class="col-lg-12 col-sm-6">		
									<div class="col-sm-1">Documents :</div>
									<div class="col-sm-4">
									
									<?php
									print $this->form->input( '_po.Documents1', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false) );
									?>
									<?php
									print $this->form->input( '_po.Documents2', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false) );
									?>
									</div>	
									
									<?php  if(count($shipment_po['FbaShipmentsPo']) > 0){
									
											if($shipment_po['FbaShipmentsPo']['documents1']){
											 print'<div class="col-sm-6"><a href="'. Router::url('/', true).'fba_shipments/documents/'.$shipment_po['FbaShipmentsPo']['documents1'].'" target="_blank" style="text-decoration:underline; color:#0000FF">'. $shipment_po['FbaShipmentsPo']['documents1'].'</a></div>';
											}
											if($shipment_po['FbaShipmentsPo']['documents2']){
											 print'<div class="col-sm-6"><a href="'. Router::url('/', true).'fba_shipments/documents/'.$shipment_po['FbaShipmentsPo']['documents2'].'" target="_blank" style="text-decoration:underline; color:#0000FF">'. $shipment_po['FbaShipmentsPo']['documents2'].'</a></div>';
											}
											
									 }?>								
									 							 
								</div>
							</div>
							
							 
						 
							<div class="row">
										<div class="col-lg-12">	
											<div class="col-sm-2" style="float:right; text-align:right;">
											<?php  $txt='NEXT'; if(count($shipment_po) > 0){ $txt = 'UPDATE'; } ?> 
											<?php
											echo $this->Form->button($txt, array(
												'type' => 'submit',
												'id' => 'next',
												'escape' => true,
												'class'=>'btn btn-warning btn-sm next'
												 ));	
										?>
											<br /><br />
											 <a href="<?php echo Router::url('/', true) ?>FbaShipments/ExportInvoice/<?php print $this->params['pass'][0]; ?>" class="btn btn-primary" style="float:right;">Export Invoice</a> </div>
											 
										</div>
								</div>
						</div>		
						
						<div class="row"><div class="col-sm-8"></div><div class="col-sm-4" id="msg"></div></div>	
						</form>			
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script language="javascript">	


 $("#r3 input[type=number]").each(function(){ 
 
 	var q = $(this).val();
	var id = $(this).attr("id"); 
	
	 
	var v = id.replace("input_", "");

 	$(".bundle_"+v).each(function(){ 
	 var sp = $(this).attr("for");
	  var bq = parseInt($(this).text()); 
	   $("#span_"+sp).text(bq*q) ;
	   
	   $("#span_"+sp).text(bq+'x'+q+'='+bq*q);

	});
 
 });
 
function validation_step1(){
  
   var isValid = true;  var notequal = 1 ;
   $("select").each(function(){
		var pack = $(this).find("option:selected").val() ;
		if ($.trim(pack).length == 0){
		$(this).addClass("highlight");
			isValid = false;        
		}else{
			$(this).removeClass("highlight"); 
		}
	});
		
   $("#r3 input[type=number]").each(function(){
   
		  var v = $(this).attr("id"); 
		  var res = v.replace("input_", "");
		  var l_value = $('#l_' + res).text(); 
		  
			if ($.trim($(this).val()).length == 0){
				$(this).addClass("highlight");
				isValid = false;        
			}
			else{
				$(this).removeClass("highlight"); 
			}  
			
			if(($.trim($(this).val()).length > 0) && (parseInt($(this).val()) > parseInt(l_value))){  		
				$(this).addClass("highlight");
				isValid = false;    	
			}else if(($.trim($(this).val()).length > 0) && (parseInt($(this).val()) < parseInt(l_value))){ 
				$(this).removeClass("highlight");
				$(this).addClass("notequal");	
				notequal++;    	
			}else if($.trim($(this).val()).length > 0) {        
				$(this).removeClass("notequal");
				$(this).removeClass("highlight");
			}
			
  });
  
	if(isValid){
	
		if(notequal > 1 ){
			if(confirm("Some shipment values are not equal. Please check before finalise.")){
				$("#r1").hide();
				$("#r2").hide();
				$("#r3").hide();
				$("#r4").show();
			}
		}else{
				$("#r1").hide();
				$("#r2").hide();
				$("#r3").hide();
				$("#r4").show();
		}
		
	}else{
		$('#msg .alert').remove();
		$("#msg").append('<div class="alert alert-danger" style="text-align: center;padding: 5px;">Please check above errors and try again.</div>');
		$('#msg .alert').delay(10000).fadeOut('slow');
	}
  
}

function validation_step2(){
		var isValid = false ;  
		$("select").each(function(){
		  var pack = $(this).find("option:selected").val() ;
		  if ($.trim(pack).length == 0){
			$(this).addClass("highlight");
				isValid = true;        
			}else{
				$(this).removeClass("highlight"); 
			}
		});
   
	   $("#r4 input[type=text]").each(function(){
	   if ($.trim($(this).val()).length == 0){
			$(this).addClass("highlight");
			isValid = true;        
		}
		else{
			$(this).removeClass("highlight"); 
		}  
	});
	return isValid;
	
}
$(".finalise_shipment").click(function(){
	validation_step1();
});



$(".shipment_items").click(function(){  
     $("#r1").show();
	 $("#r2").show();
	 $("#r3").show();
	 $("#r4").hide();
});

$("#next").click(function(){  
		validation_step1();
		var isValid = validation_step2();
		
		if(isValid){
			$('#msg .alert').remove();
			$("#msg").append('<div class="alert alert-danger" style="text-align: center;padding: 5px;">Please check above errors and try again.</div>');
			$('#msg .alert').delay(10000).fadeOut('slow');
	 	return false;
	 }
		
});

function enterNumber(v){
  	var q = $("#input_"+v).val();
 	$(".bundle_"+v).each(function(){ 
	 var sp = $(this).attr("for");
	  var bq = $(this).text();
	   $("#span_"+sp).text(bq+'x'+q+'='+bq*q);

	});

 
	
  var id = 'input_' + v;
  var l_value = $('#l_' + v).text(); 
  var e = document.getElementById(id);
 
	if (!/^[0-9]+$/.test(e.value)) 
	{ 	//alert("Please enter only number.");
	 	$("#"+id).addClass("highlight");
		e.value = e.value.substring(0,e.value.length-1);
	}else{
        $("#"+id).removeClass("highlight");
		
		
		 var isValid = true;
		  
			if ($.trim($("#"+id).val()).length == 0){
				$("#"+id).addClass("highlight");
				isValid = false;        
			}
			else{
				$("#"+id).removeClass("highlight"); 
			}  
			
			if(($.trim($("#"+id).val()).length > 0) && (parseInt($("#"+id).val()) > parseInt(l_value))){  		
				$("#"+id).addClass("highlight");
				isValid = false;    	
			}
			
		if(isValid){
			var formData = new FormData($('#shipment_frm')[0]);

	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaShipments/SaveExportShipment',
				type: "POST",				
				async: false,		
				cache: false,
				contentType: false,
				processData: false,
				data : formData,
				success: function(data, textStatus, jqXHR)
				{	
						
					if(data.error){
						alert(data.msg);
					} 							
				}
			});
		}else{
			alert('Please check qty!');
		}
		 
    }	
}    

function printLabel(v){	
	var qty = $('#input_'+v).val();
	var label_qty = parseInt($('#l_'+v).text());  
	if(qty < 1){
		alert('Please enter quantity.');
		$('#input_'+v).focus();
	}else if(qty > label_qty){
		alert('Please check entered quantity it is more than required!');
		$('#input_'+v).focus();
	}else{

		if(confirm("Are going to print #"+qty+" quantity?")){
			$("#label_"+v+" i").removeClass('fa-print');	
			$("#label_"+v+" i").addClass('fa-spin fa-refresh');	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaShipments/printLabel',
				type: "POST",				
				cache: false,			
				data : {id:v, qty:qty},
				success: function(data, textStatus, jqXHR)
				{	
					$("#label_"+v+" i").removeClass('fa-spin fa-refresh');	
					$("#label_"+v+" i").addClass('fa-print');	
					if(data.error){
						alert(data.msg);
					}			 						
				} 
			});
		}	
	 }	 
}

function printSlip(v){	
	var qty = $('#input_'+v).val();
	$("#slip_"+v+" i").removeClass('fa-print');	
	$("#slip_"+v+" i").addClass('fa-spin fa-refresh');	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaShipments/printSlip',
		type: "POST",				
		cache: false,			
		data : {id:v,qty:qty},
		success: function(data, textStatus, jqXHR)
		{	
			$("#slip_"+v+" i").removeClass('fa-spin fa-refresh');	
			$("#slip_"+v+" i").addClass('fa-print');	
			if(data.error){
				alert(data.msg);
			} 							
		}		
 	});  		 
}
function updatePackaging(t,v){	
	 
	var pack = $(t).find("option:selected").val() ;
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaShipments/updatePackaging',
		type: "POST",				
		cache: false,			
		data : {packaging:pack,merchant_sku:v },
		success: function(data, textStatus, jqXHR)
		{	
			 	
			if(data.error){
				alert(data.msg);
			} 							
		}		
 	});  		 
}

</script>
