<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>				
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>				
				<h4 style="float:left;">Shipment Items/SKUs</h4>
				<a href="<?php echo Router::url('/', true) ?>FbaShipments" class="btn btn-primary" style="float:right;">Back</a>
				<br /><br />
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							  <?php /*?><form method="get" name="shipmentFrm" id="shipmentFrm" enctype="multipart/form-data">	 
								<div class="row">	
								 <div class="col-lg-5">
									 <div style="width: 30%;float: left;margin-right: 1px;">
									 	 <select class="form-control" id="shipment" name="shipment">
											 <option value="">All FBA Shipments</option>										
											 <?php foreach($all_shipments as $shipments) { ?>
											 <option value="<?php echo $shipments['FbaShipment']['id']; ?>" <?php if(isset($_REQUEST['shipment']) && ($_REQUEST['shipment'] == $shipments['FbaShipment']['id'])) echo 'selected=selected'?>><?php echo $shipments['FbaShipment']['shipment_id']; ?></option>
											 <?php } ?>
										 </select>
									 </div>
									 
									 <div style="float:left; padding:0 2% 0 1%;">
									 	<a href="javascript:void(0);" id="filter_shipment" class="btn btn-info" role="button" title="Filter Shipment"><i class="fa fa-filter"></i></a>
									 </div>
									 <div style="float:left;">									 
										 <a href="javascript:void(0);" class="btn btn-primary download_shipment" title="Download Shipment"><i class="glyphicon glyphicon-download"></i></a>
									 </div>
								 </div>
								 <div class="col-lg-3">
								 	<input type="text" value="" placeHolder="Enter Search Keyword..." name="searchkey" class="form-control searchString" />
								 </div>									
								 <div class="col-lg-4"> 
									<button type="button" class="btn btn-success searchKey"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>&nbsp; 
									<button type="button" class="btn btn-warning" onclick="window.location.href='<?php echo Router::url('/', true) ?>MapSku/getInactiveListing'"><i class="glyphicon glyphicon-refresh"></i>&nbsp;Refresh</button>
								</div>
								</div>	
								 </form><?php */?>  
						</div>	
						
					<div class="panel-body no-padding-top bg-white">
						<div class="row">	
						<div class="col-sm-12" style="padding:5px; background-color:#F2F2F2;">
							<?php  if(count($shipment_items['FbaShipment']) > 0) {	  ?>
								<div class="col-sm-3"><strong>Plan Id:</strong> <?php print $shipment_items['FbaShipment']['plan_id']; ?></div>
								<div class="col-sm-3"><strong>Total Merchant SKU:</strong> <?php print $shipment_items['FbaShipment']['total_skus']; ?></div>
								<div class="col-sm-2"><strong>Total Units:</strong> <?php print $shipment_items['FbaShipment']['total_units']; ?> </div>	
								<div class="col-sm-2"><strong>Pack List:</strong> <?php print $shipment_items['FbaShipment']['pack_list']; ?></div>	
								<div class="col-sm-2"><strong>Date:</strong> <?php print $shipment_items['FbaShipment']['added_date']; ?></div>	
								
								<div class="col-sm-3"><strong>Shipment:</strong> <?php print $shipment_items['FbaShipment']['shipment_name']; ?></div>		
								<div class="col-sm-7"><strong>Ship To:</strong> <?php print $shipment_items['FbaShipment']['ship_to']; ?></div>
								<div class="col-sm-2"><strong>Status:</strong> <?php print ucfirst($shipment_items['FbaShipment']['status']); ?></div>		
							 <?php }?>
						</div> 
						</div> 
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-sm-2">Merchant Sku</div>		
								<div class="col-sm-3">Title</div>		
								<div class="col-sm-2">ASIN</div>
								<div class="col-sm-2">FNSKU</div>
								<div class="col-sm-2">External Id</div>	
								<div class="col-sm-1">Shipped</div>	
							</div>								
						</div>	
						<?php 
								App::Import('Controller', 'FbaShipments'); 
								$obj = new FbaShipmentsController;
								 
							    $po_items = array();
								$shipment_po  = $obj->getExportShipment($this->params['pass'][0]);
								if(count($shipment_po) > 0){								  
									foreach($shipment_po['FbaShipmentItemsPo'] as $v){
										$po_items[$v['fnsku'].'_'.$v['master_sku']] = $v;
									}
								}
								
								 
					?>
						<div class="body" id="results">
						<?php  if(count($shipment_items['FbaShipmentItem']) > 0) {	 ?>
							<?php foreach( $shipment_items['FbaShipmentItem'] as $items ) {     ?>
								<div class="row sku" id="r_<?php echo $items['id']; ?>">
									<div class="col-lg-12">	
										<div class="col-sm-2"><?php echo $items['merchant_sku']; ?></div>		
										<div class="col-sm-3"><?php echo $items['title'] ?></div>	
										<div class="col-sm-2"><?php echo $items['asin'];  ?></div>		
										<div class="col-sm-2"><?php echo $items['fnsku'] ?></div>	
										<div class="col-sm-2"><?php echo $items['external_id']; ?></div>										 							
										<div class="col-sm-1"><?php 
											$master_sku    = $obj->getAwsMapping($items['merchant_sku']);
											if(isset($po_items[$items['fnsku'].'_'.$master_sku])){
												echo $po_items[$items['fnsku'].'_'.$master_sku]['shipped_qty'];
											}else{
												echo $items['shipped'];
											} 
											?></div>
										 							
									</div>												
								</div>
								 
							<?php }?>
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	

$('#filter_shipment').click(function(){  
	document.getElementById("shipmentFrm").submit();		 
});
	
$(".downloadfeed").click(function(){	
	if($("#category option:selected").val() == 'all'){
		alert("Please Select Category");	
	}else{
		 
		$(".outerOpac").show();		 	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>MapSku/getFeedCsv',
				type: "POST",				
				cache: false,			
				data : {country:$("#country option:selected").val(),listing_type:'inactive_listing'},
				success: function(data, textStatus, jqXHR)
				{	
					$(".outerOpac").hide();		
					jQuery(".msg").html(data.file);
					$('#msg').delay(200000).fadeOut('slow');							
			}		
		 });
	 }
		 
});
$(".filter").click(function(){
	if($("#country option:selected").val() == 'all'){
		alert("Please Select Country");	
	}else{
		window.location.href = '<?php echo Router::url('/', true) ?>MapSku/getInactiveListing?country='+$("#country option:selected").val();
	}
});

$(".searchKey").click(function(){	
	if($(".searchString").val()){
		ajaxSearch();
	}else{
		alert('Enter Search Keyword.');
		event.preventDefault();
	}
		 
});

$(".mapmaster").click(function(){	
if($("#country option:selected").val() == 'all'){
		alert("Please Select Country");	
	}else{
	
		var country = $("#country option:selected").val();
		
		$(".mapmaster i").removeClass('glyphicon glyphicon-upload');
		$(".mapmaster i").addClass('fa fa-spin fa-refresh');
		
		
		country = country == 'GB' ? 'UK' : country;
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>MapSku/getDataAws/CostBreaker_'+country,
			type: "POST",				
			cache: false,			
			//data : { },
			success: function(data, textStatus, jqXHR)
			{	
				
				$(".mapmaster i").removeClass('fa fa-spin fa-refresh');	
				$(".mapmaster i").addClass('glyphicon glyphicon-upload');	
				$(".msg").show();		
				$(".msg").html(data.msg);	
				$('.msg').delay(20000).fadeOut('slow');		
				window.location.href = '<?php echo Router::url('/', true) ?>MapSku/getInactiveListing?country='+$("#country option:selected").val();				
			}		
	 });	
	} 
	 
 });
function ajaxSearch(){	
	 <?php /*?>  $("#mapFrm").attr("action", "<?php echo Router::url('/', true) ?>MapSku/Search");
	   $("#mapFrm").submit();<?php */?>
	   
	$(".outerOpac").show();		 	
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>MapSku/Search',
			type: "POST",				
			cache: false,			
			data : {searchkey:$(".searchString").val()},
			success: function(data, textStatus, jqXHR)
			{	//alert(data.data);	
				$(".outerOpac").hide();		
				//jQuery(".msg").html(data.msg);
				jQuery('.panel-body').html(data.data);
					
				//$('#message').delay(20000).fadeOut('slow');							
		}		
	 });		 
}

 function delSku(key){
	if( confirm('Are you sure want to delete?')){
	$(".outerOpac").show();		 
 	$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>MapSku/Delete',
			type: "POST",				
			cache: false,			
			data : {seller_sku:key},
			success: function(data, textStatus, jqXHR)
			{	//alert(data.data);	
				$(".outerOpac").hide();		
				jQuery(".msg").html(data.msg);
				jQuery("#r_"+key).remove();	
				//$('#message').delay(20000).fadeOut('slow');							
		}		
	 });
	 }
 }
 

</script>
