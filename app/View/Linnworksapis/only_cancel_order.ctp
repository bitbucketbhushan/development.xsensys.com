<?php 
	App::import('Controller', 'Linnworksapis');
	$checkproduct =	new LinnworksapisController;
?>
<?php if( isset($results) ) { foreach($results as $result) { ?>
<div class="modal fade pop-up-4" tabindex="-1" role="dialog" id="pop-up-4search" aria-labelledby="myLargeModalLabel-4" aria-hidden="true">
	<div class="modal-dialog" style="width:1250px;">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Order Id ( <?php echo $result->NumOrderId; ?> )</h4>
         </div>
         <div class="modal-body bg-grey-100">
           	<div class="no-margin" style="clear:both">
					<div class="panel-body no-padding-top" >
					
					<div class="row" style="padding:20px 0; border: 5px solid #c0c0c0; box-shadow: 0 0 30px -18px rgba(66, 66, 66, 1) inset;">
                        <div class="col-lg-6">
							<div class="panel no-margin-bottom">
                                <div class="panel-title no-border">
									<div class="panel-head">Order Detail</div>
									<div class="panel-tools">
								</div>
								</div>
                                <div class="panel-body no-padding ">
									
									<table class="table table-striped" cellpadding="5px;">									
										<tbody>
											<tr> 
												<td class="vertical-middle">Order Status</td>
												<td class="vertical-middle"><?php if( $result->OpenStatus == 0 ){print "Open";}else if($result->OpenStatus == 1 ){ print "Processed";} ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Received Date</td>
												<td class="vertical-middle"><?php print $result->GeneralInfo->ReceivedDate; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Source</td>
												<td class="vertical-middle"><?php echo $result->GeneralInfo->Source . '('.$result->GeneralInfo->SubSource .')'; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Process Date</td>
												<td class="vertical-middle"><?php if( $result->OpenStatus != 0 ){ echo $result->date; }else { print "--";} ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Reference No.</td>
												<td class="vertical-middle"><?php print $result->GeneralInfo->ReferenceNum; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Service</td>
												<td class="vertical-middle"><?php print $result->ShippingInfo->PostalServiceName; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Email</td>
												<td class="vertical-middle"><?php echo $result->CustomerInfo->Address->EmailAddress; ?></td>
											</tr>
											<tr>
											<?php
												$address = '';
												$address .= $result->CustomerInfo->Address->Address1.', ';
												$address .= $result->CustomerInfo->Address->Address2.', ';
												$address .= $result->CustomerInfo->Address->Address3.', ';
												$address .= $result->CustomerInfo->Address->Town.', ';
												$address .= $result->CustomerInfo->Address->PostCode.', ';
												$address .= $result->CustomerInfo->Address->Region.', ';
												$address .= $result->CustomerInfo->Address->Country;
											?>
												<td class="vertical-middle">Address</td>
												<td class="vertical-middle"><?php echo $address; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">FullName</td>
												<td class="vertical-middle"><?php echo $result->CustomerInfo->Address->FullName; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">PhoneNumber</td>
												<td class="vertical-middle"><?php echo $result->CustomerInfo->Address->PhoneNumber; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Subtotal</td>
												<td class="vertical-middle"><?php echo $result->TotalsInfo->Subtotal .'&nbsp; ('. $result->TotalsInfo->Currency .')'; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Tax</td>
												<td class="vertical-middle"><?php echo $result->TotalsInfo->Tax .'&nbsp; ('. $result->TotalsInfo->Currency .')' ; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">TotalCharge</td>
												<td class="vertical-middle"><?php echo $result->TotalsInfo->TotalCharge .'&nbsp; ('. $result->TotalsInfo->Currency .')' ; ?></td>
											</tr>
											
										</tbody>
									</table>
                                </div>
                            </div>
                        </div><!-- /.col -->
					
                        </div>
                        <div class="row">
                        		<div class="col-lg-12" >
								<table class="table">
									<tr>
										<td><label>Cancel Note:</label><textarea class="form-control" rows="1" cols="20" id ="refund_notes_<?php echo $result->NumOrderId; ?>" > </textarea></td>
										<td><label>Refund:</label> <input type ="text" class="form-control" id ="refund_amount_<?php echo $result->NumOrderId; ?>"  value =<?php echo $result->TotalsInfo->Subtotal?> > 
										<input type ="hidden" id ="warehouse_id_<?php echo $result->NumOrderId; ?>"  value = "00000000-0000-0000-0000-000000000000"> 
										</td>
									</tr>
									
									<?php
									if( $result->OpenStatus == 0 )
									{
									?>
										<tr>
											<td colspan="2" align="center">
												<a href="javascript:void(0);" for = "<?php echo $result->NumOrderId; ?>" for = "" class="cancelOpenOrder btn bg-orange-500 color-white btn-dark padding-left-20 padding-right-20" style="margin:0 auto; text-align: center;">Cancel Order</a>
												<a href="" class="loading-image_<?php echo $result->NumOrderId; ?>" style="display:none;"><img src="http://xsensys.com/app/webroot/img/image_889915.gif" hight ="20" width="20" /></a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->id; ?>###<?php echo $item->MergeUpdate->product_order_id_identify; ?>" class="assign_packaging_slip btn bg-green-500 color-white btn-dark padding-left-20 padding-right-20" href="javascript:void(0);" title = "Assign Packaging Service" data-toggle="modal" >Assign (Slip / Label)</a>
												<a href="javascript:void(0);" for = "<?php echo $result->NumOrderId; ?>" for = "" class="deleteOpenOrder btn bg-red-500 color-white btn-dark padding-left-20 padding-right-20" style="margin:0 auto; text-align: center;">Delete Order</a>
											</td>
											
										</tr>
										
									<?php 
									}
									?>
									</table>
									<div class="col-lg-12" style="text-align:center; padding-bottom: 7px;">	
										<a href="javascript:void(0);" aria-hidden="true" class="dismiss btn bg-orange-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;">OK</a>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } } ?>
<script>
	
   $(document).ready(function()
   {
       $('#pop-up-4search').modal('show');
	   
	   	$('.subOrderBlock>.panel-body').slimScroll({
        height: '275px',
		width:'100%',
		size:'3',
		alwaysVisible: true
    });
   });
   
   $("body").on("click", ".dismiss", function(){
			var forValue 		= $(this).attr( 'for' );
			if( forValue == 1)
			{
				$( ".close" ).click();
				location.reload();
			}	 
		});
		
</script>

