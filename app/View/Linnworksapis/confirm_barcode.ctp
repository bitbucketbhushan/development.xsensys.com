<?php 
$id = $getItems[0]['MergeUpdate']['id'];
$splitOrderId = $getItems[0]['MergeUpdate']['product_order_id_identify'];
$OrderId = $getItems[0]['MergeUpdate']['order_id'];

App::import('Controller', 'Linnworksapis');
$checkproduct =	new LinnworksapisController;
$pkOrderid	=	$checkproduct->getPkOrderid( $OrderId );

?> 

<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php echo "Confirm"; ?></h1>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">
			  <div class="panel-title"> 
					<div class="panel-head">Barcode Scan Section</div>
					<div class="panel-tools">
						<a href="" class="loading-image" style="display:none;"><?php echo $this->Html->image('image_889915.gif', array('width' => '20', 'height' => '20')); ?></a>
						<a href="/linnworksapis/dispatchConsole"  class="btn bg-orange-500 color-white btn-dark ">Skip</a>
						<a href="javascript:void(0);"  for = "<?php echo $OrderId; ?>" class="printlable btn bg-orange-500 color-white btn-dark"  style="opacity : .5;" >Print Label</a>
						<a href="javascript:void(0);"  for = "<?php echo $OrderId; ?>" class="printorder btn bg-orange-500 color-white btn-dark"  style="opacity : .5;" >Print Slip</a>
						<a href="javascript:void(0);" for = "<?php echo $pkOrderid; ?>" class="processedorder processedorder_<?php echo $id; ?> btn bg-green-500 color-white btn-dark" style="display:none;" >Process</a>
					</div>
			  </div>
			   <?php 
				 $customeremail	=	$customerInfo->Address->EmailAddress; 
				 $checkCus	=	 $this->Common->checknegetiveCustomer($customeremail);
				 if($checkCus == 1) {
			 ?>
			  <div class="col-lg-12" style="height: 30px;font-size: 16px;background: red;color: white;"><center><strong>Do not Process :- </strong>This customer marked as negative due to some malicious activity please contact customer support to cancel this order.</center></div>
			  <?php } ?>
			  <?php
			  	//pr($getItems);
			  	//exit;
			  	$gillet = array('S-73624#DCS','S-73512#DCS','S-81471666#DCS','S-78571#DCS','S-81612450#DCS','S-81668920#DCS','S-73510#DCS','S-81609465#DCS','S-73623#DCS','S-707779#DCS','S-GILFUSPROPW10X8#MC','S-GILFUSPROSTYLER#Mc','S-GLT74041#DCS','S-81641948#DCS','S-81307323','S-81641984#DCS','S-81521780#DCS','S-81543872#DCS','S-81469674#DCS','S-81521781#DCS','S-81641939#DCS','S-91269#DCS','S-97179#DCS','S-73506#DCS','S-73508#DCS','S-73357#DCS','S-91268#DCS','S-73354#DCS','S-81631492#DCS','S-81604016#DCS','S-81521763#DCS');
			  	$skus	=	explode(',', $getItems[0]['MergeUpdate']['sku']); 
				$i = 0;
				foreach($skus as $k => $v){
					$sku_qty = 'S-'.explode('XS-', $v)[1];
					if(in_array( $sku_qty, $gillet )){
						$i++;
					}
				}
				if($i > 0) {
			  ?>
			  <div class="col-lg-12 gillete" style="height: 30px;font-size: 16px;color: white;"><center><strong>Check Correct Version :- </strong>Fusion Proglide , Fusion 5 Proglide ,Fusion5  ProShield etc..</center></div>
			 <?php } ?>
			 <?php 
			 	$envrlop_sku = array('S-CC640EE','S-CC641EE','S-CH561EE','S-CH562EE','S-CH563EE','S-CH564EE','S-F6U66AE','S-F6U65AE','S-F6U68AE','S-F6U67AE','S-T6N02AE','S-T6N01AE','S-T6N03AE','S-N9K06AE','S-N9K05AE','S-N9K08AE','S-N9K07AE','S-C2P04AE','S-C2P06AE','S-C2P05AE','S-C2P07AE','S-CC653AE','S-CC656AE','S-CC654AE','S-CN684EE');
				$skus	=	explode(',', $getItems[0]['MergeUpdate']['sku']); 
				$e_s = 0;
				foreach($skus as $k => $v){
					$sku_qty = 'S-'.explode('XS-', $v)[1];
					if(in_array( $sku_qty, $envrlop_sku ) && $getItems[0]['MergeUpdate']['delevery_country']){
						$e_s++;
					}
				}
			 
			 ?>
			  <div class="panel-body">
              <div class="barcodemsg" ></div>                            
                    <div class="row">
                        <div class="col-lg-4 col-lg-offset-1">
                            
								<div class="input text"><label for="LinnworksapisBarcode">Barcode</label>
									<input type="text" id="LinnworksapisBarcode" index="0" class="form-control get_sku_string" value="" name="data[Linnworksapis][barcode]">
									<input type="hidden" id="split_order_id" index="0" class="form-control get_sku_string" value="<?php echo $splitOrderId; ?>" name="data[Linnworksapis][split_order_id]">
									<input type="hidden" id="open_order_id" index="0" class="form-control open_order_id" value="<?php echo $OrderId; ?>" name="data[Linnworksapis][split_order_id]">
									<p class="help-block">Please scan again to processing order</p>
									</div>
									<input type ="hidden" id="Linnworksapisid" value="<?php echo $id; ?>"/>
							</div>
							<div class="tracking col-lg-4 col-lg-offset-1" style="display:none;">
								<div class="input text"><label for="Linnworksapispostal">Postal Tracking</label>
									<input type="text" id="Linnworksapispostaltracking" index="0" class="form-control" value="" name="data[Linnworksapis][barcode]" >
									<p class="help-block">Please fill postal tracking here</p>
									</div>
									<input type ="hidden" id="Linnworksapispkorderid" value="<?php echo $id; ?>"/>
									<input type ="hidden" id="Linnworksapispostalservice" value="<?php echo $getItems[0]['MergeUpdate']['sku']; ?>"/>
								</div>
							</div>
							
							<div class="col-lg-12 multiqtymsg" style="display:none;" ><div style="width: 50%;margin: 0 auto; text-align:center; padding:5px; background-color: #f94828; color: #fff; font-size:16px;"> Multiple Quantity Order <?php echo ($type == 'Bundle') ? '[ '.$type.' ]' : ''; ?></div></div>
							
							<label><span>Orders containing barcode : <span><span class="searchbarcode"></span></label>
						<div class="outer_bin_addMore">
					<table class="table table-bordered table-striped dataTable" >
							<tr>
								<th>Order</th>
								<th>SKU</th>
								<th>Title</th>
								<th>BarCode</th>
								<th>Packaging Id</th>
								<th>Packaging Name</th>
								<th>Scanned Qty</th>
								<th>Quantity</th>
								<th>Postal Service</th>
								<th>Completed</th>
							</tr>
							<?php 
								$i = 0; foreach($getItems as $getItem )
								{ 
									
									$ord_Id = $getItem['MergeUpdate']['order_id'];
									$ordSplit_Id = $getItem['MergeUpdate']['product_order_id_identify'];
									$barcode		=	$getItem['MergeUpdate']['barcode'];
									$splitOrderId	=	$getItem['MergeUpdate']['product_order_id_identify'];
								
									App::import('Controller', 'Linnworksapis');
									$checkproduct =	new LinnworksapisController;
								    $qty	=	$checkproduct->getQuantity( $id, $splitOrderId );
									
									App::import('Controller', 'Reserves');
									$checkproductAll =	new ReservesController;
									$qtyAll	=	$checkproductAll->getAllQuantity( $id, $splitOrderId );
									
									
							?>
							<tr class="quantity" id="<?php echo $getItem['MergeUpdate']['id']; ?>" >
									<td><?php echo $ordSplit_Id .'('. $ordSplit_Id .')'; ?></td>
									<td class="skupackage"><?php echo str_replace(',', '<br>',$getItem['MergeUpdate']['sku']); ?></td>
									<td class="productTitle">
									<?php foreach( $getItem['MergeUpdate']['ItemList'] as $title) { ?>
									<?php echo '<span>'.$title['title'].'<br><br><strong>Amazon Title :- </strong><br>'.$title['source_title'].'<br><b>['.$title['channel_sku'].']</b></span>'; ?>
									
									<?php } ?>
									</td>
									<td>
									<?php 
									$mer_barcodes = explode(',',$getItem['MergeUpdate']['barcode']);
														foreach( $mer_barcodes as $mer_barcode )
														{
															echo $this->Common->getLatestBarcode( $mer_barcode ).'<br>';
														}
									//echo str_replace(',', '<br>', $getItem['MergeUpdate']['barcode']); 
									?>
									</td>
									<td><?php echo $getItem['MergeUpdate']['envelope_id']; ?></td>
									<td><?php echo $getItem['MergeUpdate']['envelope_name']; ?></td>
									<td style="font-size: 50px;text-align: center;" for = "<?php echo $i ?>" id = "barco_<?php echo $getItem['MergeUpdate']['id']; ?>" class="update_quantity upqty_<?php echo $i; ?>_<?php echo $getItem['MergeUpdate']['id']; ?>"><?php echo $qty;?></td>
									<td for = "<?php echo $i ?>" class="multiqty qty_<?php echo $i; ?>_<?php echo $getItem['MergeUpdate']['id']; ?>"><?php echo $qtyAll; //echo $getItem['MergeUpdate']['quantity']; ?></td>
									<td class="postal_service" ><?php echo $getItem['MergeUpdate']['postal_service']; ?> &nbsp;</td>
									<td class="complete_<?php echo $i; ?>_<?php echo $getItem['MergeUpdate']['id'];; ?>">No
									<a rel="tooltip" class="for_tooltip btn btn-xs btn-success" href="javascript:void(0); " title = "Assign Custom Service" data-toggle="modal" data-target=".delivery_labels_pop-up" >Packaging Assign</a>
									</td>
							</tr>
							<?php $i++; } ?>
						</table>
						</div>	
					</div>
				</div>
			</div>
		</div>
	 </div>
	</div>        
</div>


<!--Start Popup for delevery service-->
<div class="modal fade servicePop delivery_labels_pop-up"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Order Id ( <?php echo  $ordSplit_Id ; ?> )</h4>
         </div>
         <div class="modal-body bg-grey-100">
            <div class="panel no-margin" style="clear:both">
     <div class="panel-body padding-top-20" >
     
     <div class="row">
		
                        <div class="col-lg-12">
							 <form accept-charset="utf-8" method="post" enctype="multipart/form-data" class="form-horizontal bv-form" action="">
								<div class="form-group">
                                       <label class="control-label col-lg-3" for="username">Packaging Class </label>                                        
                                        <div class="col-lg-7"> 
											<?php
												if( count( $packageEnvelop ) > 0 )
												print $this->form->input( 'User.role_type', array( 'type'=>'select', 'empty'=>'Packaging Class','options'=>$packageEnvelop,'class'=>'className form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'value'=> $getPackaeClass['getPackageClassId'] )  );
											?>                               
                                        </div>
                                      </div>
                                      
                                      <div class="form-group pasteVariant">
                                       <label class="control-label col-lg-3" for="username">Packaging Name</label>                                        
                                        <div class="col-lg-7"> 
											<?php
												if( count( $envelop ) > 0 )
												print $this->form->input( 'User.role_type', array( 'type'=>'select', 'empty'=>'Packaging Name','options'=>$envelop,'class'=>'packageName form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'default'=>$getItem['MergeUpdate']['envelope_id'] ) );
											?>                               
                                        </div>
                                      </div>
                                      
                                      </form>
                        </div>
      
                        <div class="row">
                         
         <div class="col-lg-12" style="text-align:center; padding-bottom: 7px;"> 
         <!--<a href="javascript:void(0);" class="cancelPostal btn bg-orange-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;">Cancel</a>-->
         <a href="javascript:void(0);" for="<?php print $getItem['MergeUpdate']['id']; ?>" class="assignCustomPackaging btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;">Packaging Assign</a>
         <a href="" class="loading-image_<?php //echo $result->NumOrderId; ?>" style="display:none;"><img src="http://localhost/app/webroot/img/image_889915.gif" hight ="20" width="20" /></a>
         </div>
        </div>
       </div>
      </div> 
     </div>
    </div>
   </div>
  </div>
 </div>
</div>

<!--End Popup for check delevery service-->



<script>
function orderprocess()
{
	var id	=	'';
	var splitOrderId 	= 	$('#split_order_id').val( );
	var openOrderId 	= 	$('#open_order_id').val( );
	var user	=	'<?php echo $this->session->read('Auth.User.id'); ?>'
	$.ajax(
		{
			'url'          : '/linnworksapis/processorder',
			'type'         : 'POST',
			'data'         : { id : id,  userID : user, splitOrderId : splitOrderId, openOrderId : openOrderId },
			'beforeSend'	 : function() {
												$('.loading-image').show();
												},
			'success' 	   : function( msgArray )
									{
										if(msgArray == 1){
											swal('Open order ( '+openOrderId+' ) processed successfully.', '' , 'success');
											$('.loading-image').hide();
											 window.setTimeout(window.location.href = "/linnworksapis/dispatchConsole",5000);
										} else if(msgArray == 2 ) {
											$('.loading-image').hide();
											window.location.href = "/linnworksapis/dispatchConsole";
										} else if(msgArray == 3 ) {
											swal('There ia an error..', '' , 'success');
										}
									}  
		}); 	
}


$(function() { 
<?php if($e_s > 0){ ?>
	alert("You need to add recycle envelope.");
<?php } ?>
<?php if( $checkCus == 1) { ?>
//$('.multiqtymsg').show();
$('#LinnworksapisBarcode').css('background', '#fe9d9d');
<?php } ?>
var trackID = '<?php echo ($getItems[0]['MergeUpdate']['track_id'] != '') ? $getItems[0]['MergeUpdate']['track_id'] : ''; ?>';
var packsku = $('.skupackage').text();
var res = packsku.match(/S-55DL02/g);
$( ".gillete" ).animate({backgroundColor: "#aa0000", color: "#fff",}, 1000 );


  $("#LinnworksapisBarcode").focus();
  if($('.multiqty').text() > 1)
  {
  	$('.multiqtymsg').show();
	//$('.multiqty').css("font-size", "50px");
	$(".multiqty").animate({fontSize: "50px"},"50000");//background-color: #f94828
	$('.multiqty').css("background-color", "Green");
	$('.multiqty').css("color", "White");
  } else {
  	$('.update_quantity').css("fontSize", "12px");
  }
   if( res == 'S-55DL02' )
	  {
			swal("liquid Inside- Extra Packaging required");
	  }
	//var Barcode	=	$('#LinnworksapisBarcode').val();
	//var pkorder	=	$('#Linnworksapisid').val();
	//var count = $('#barco_'+Barcode).attr('for');
	var splitOrderId 	= 	$('#split_order_id').val( );
	var openOrderId 	= 	$('#open_order_id').val( );
  
 			var innercount = 0;
			var outercount = 0;
			var i=0;
			$('.quantity ').each(function()
				{
					
						innercount++;
						var id = $(this).attr('id');
						if($('.upqty_'+i+'_'+id).text() == $('.qty_'+i+'_'+id).text())
						{ 
							$('.complete_'+i+'_'+ id ).html('<a class=" count btn btn-success btn-xs margin-right-10" title="" href=""><i class="ion-checkmark"></i></a>');
						}
						i = i+1;
				});
				
			$('.count').each(function()
				{
					outercount++;
						var id = $(this).attr('id');
						
				});
				if(innercount == outercount)
				{
					var id = $('.printlable').attr('for');
					var service	=	$('.postal_service').text();
					var serviceResult	=	service.split(' ');
					
					if(serviceResult[0] == 'Express' || serviceResult[0] == 'Tracked' )
														{
															$('.tracking').show();
															$("#Linnworksapispostaltracking").focus();
															if(trackID == 'No' || trackID == '')
															{
																var r = confirm("Please insert tracking ID");
																	if (r == true) {
																		 $("#Linnworksapispostaltracking").val();
																		 if( $("#Linnworksapispostaltracking").val() == '' ){
																			return false;
																		 }
																	} else {
																	  return false;
																	} 
																//alert('Please insert tracking ID');
																
															}
															else
															{
																$("#Linnworksapispostaltracking").val( trackID );
																$("#Linnworksapispostaltracking").attr( 'readonly', true );
															}
														}
														
					$('.loading-image').hide();
					$.ajax({
						
							'url'            : getUrl() + '/generatepdfdirect',
							'type'           : 'POST',
							'data'           : { pkorderid : id, openOrderId : openOrderId, splitOrderId : splitOrderId},
							'beforeSend'	 : function() {
															$('.loading-image').show();
														  },
							'success'		 : function( data )
												{
													var result = data.split("#");
												
													$('.processedorder').css('opacity','1');
													$('.printlable').css('opacity','1');
													$('.printorder').css('opacity','1');
													var service	=	$('.postal_service').text();
													var serviceResult	=	service.split(' ');
													if(serviceResult[0] == 'Express' || serviceResult[0] == 'Tracked' )
														{
															$('.tracking').show();
															$("#Linnworksapispostaltracking").focus();
															/*if(trackID == 'No' || trackID == '')
															{
																alert('Please insert tracking ID');
																$("#Linnworksapispostaltracking").val();
															}
															else
															{
																$("#Linnworksapispostaltracking").val( trackID );
																$("#Linnworksapispostaltracking").attr( 'readonly', true );
															}*/
														}
													$('.loading-image').hide();
													$('.processedorder').show();
													
												}
						});
						
					$.ajax({
																			
								'url'            : getUrl() + '/domeshippingnew',
								'type'           : 'POST',
								'data'           : { pkorderid : id, openOrderId : openOrderId, splitOrderId : splitOrderId },
								'beforeSend'	 : function() {
																$('.loading-image').show();
																},
								'success'		 : function( data )
													{
														var result = data.split("#");
														$('.printlable').css('opacity','1');
														$('.printorder').css('opacity','1');
														$('.loading-image').hide();
														$('.processedorder').show();
													}	
						});
				}
				
			$("body").on("click", ".processedorder", function(){
				var id	=	$(this).attr('for');
				var splitOrderId 	= 	$('#split_order_id').val( );
				var openOrderId 	= 	$('#open_order_id').val( );
				var user	=	'<?php echo $this->session->read('Auth.User.id'); ?>'
				
				
				$.ajax(
					{
						'url'          : '/linnworksapis/processorder',
						'type'         : 'POST',
						'data'         : { id : id,  userID : user, splitOrderId : splitOrderId, openOrderId : openOrderId },
						'beforeSend'	 : function() {
															$('.loading-image').show();
															},
						'success' 	   : function( msgArray )
													{
														if(msgArray == 1)
														{
															//alert( 'Open order ( '+openOrderId+' ) processed successfully.' );
															swal('Open order ( '+openOrderId+' ) processed successfully.', '' , 'success');
															$('.loading-image').hide();
															//window.location.href = "/linnworksapis/dispatchConsole";
															 window.setTimeout(window.location.href = "/linnworksapis/dispatchConsole",5000);
														}
														else if(msgArray == 2 )
														{
															$('.loading-image').hide();
															window.location.href = "/linnworksapis/dispatchConsole";
														}
														else if(msgArray == 3 )
														{
															//alert( 'There ia an error.' );
															swal('There ia an error..', '' , 'success');
															
														}
													}  
					});  
				});
				
				
				$("body").on("click", ".printorder", function(){
				var id	=	$(this).attr('for');
				var splitOrderId 	= 	$('#split_order_id').val( );
				var openOrderId 	= 	$('#open_order_id').val( );
				
				$.ajax(
					{
						'url'          : getUrl() + '/domeshippingnew',
						'type'         : 'POST',
						'data'         : { pkorderid : id,  splitOrderId : splitOrderId, openOrderId : openOrderId },
						'success' 	   : function( msgArray )
													{
														if(msgArray == 1)
														{
															
														}
														else
														{
															
														}
													}  
					});  
				});
				
				$("body").on("click", ".printlable", function(){
				var id	=	$(this).attr('for');
				var splitOrderId 	= 	$('#split_order_id').val( );
				var openOrderId 	= 	$('#open_order_id').val( );
				
				$.ajax(
					{
						'url'          : getUrl() + '/generatepdfdirect',
						'type'         : 'POST',
						'data'         : { pkorderid : id,  splitOrderId : splitOrderId, openOrderId : openOrderId },
						'success' 	   : function( msgArray )
													{
														if(msgArray == 1)
														{
													
														}
														else
														{
															
														}
													}  
					});  
				});
				       
				/* For update custom posta service */			
				$("body").on("click", ".assignCustomPackaging", function(){
					
					var orderId 		= $(this).attr( 'for' );
					var packClass 		= $('.className').find(":selected").text();
					var envID 			= $('.packageName').val();
					var envName 		= $('.packageName').find(":selected").text();
					
					$.ajax(
						{
							'url'          : '/System/Packaging',
							'type'         : 'POST',
							'data'         : { orderId : orderId,
												packClass : packClass,
												envID : envID,
												envName : envName
											 },						
							'success' 	   : function( msgArray )
											{
												if( msgArray == 1 )
												{
													location.reload();
												}
												else
												{
													swal( "Error found to assign packaging type!!" );
													return false;
												}
											}  
						});  
					});
					
					
					$("body").on("change", ".className", function(){
					
					var packClassId 		= $(this).val();
					
					$.ajax(
						{
							'url'          : '/System/Get/getAllVariantById',
							'type'         : 'POST',
							'data'         : { packClassId : packClassId
											 },						
							'success' 	   : function( msgArray )
											{
												$( '.pasteVariant' ).html( msgArray );
											}  
						});  
					});
			/* For close popup */
				
				
});
		
$(document).keypress(function(e) {
   if(e.which == 13) {
	   if($('#Linnworksapispostaltracking').val() == '')
		{
			submitSanningValue();
		}
		else
		{
			submitTrackingSanningValue();
		}
    }
});

function submitTrackingSanningValue()
{
	
	var trackingBarcode 	=	$('#Linnworksapispostaltracking').val();
	var pkorder				=	$('#Linnworksapispkorderid').val();
	var postalservice		=	$('#Linnworksapispostalservice').val();
	var splitOrderId 	= 	$('#split_order_id').val( );
	var openOrderId 	= 	$('#open_order_id').val( );
	$('#LinnworksapisBarcode').val('');
		$.ajax({
						'url'            : getUrl() + '/asigntrackid',
						'type'           : 'POST',
						'data'           : { trackingBarcode : trackingBarcode,  pkorder : pkorder, postalservice : postalservice },
						'beforeSend'	 : function(){
														$('.loading-image').show();
													 },
						'success' 		 : function( msgArray )
													{
															orderprocess();
														   $('.loading-image').hide();
														   $('.processedorder').show();
														   $('.printlable').css('opacity','1');
														   $('.printorder').css('opacity','1');
														   $('.loading-image').hide();
														   
														   /* print slip and label after asing the track id */
														   
														   $.ajax({
						
																	'url'            : getUrl() + '/generatepdfdirect',
																	'type'           : 'POST',
																	'data'           : { pkorderid : pkorder, splitOrderId : splitOrderId, openOrderId : openOrderId},
																	'beforeSend'	 : function() {
																									$('.loading-image').show();
																								  },
																	'success'		 : function( data )
																						{
																							var result = data.split("#");
																						
																							$('.processedorder').css('opacity','1');
																							$('.printlable').css('opacity','1');
																							$('.printorder').css('opacity','1');
																							var service	=	$('.postal_service').text();
																							var serviceResult	=	service.split(' ');
																							if(serviceResult[0] == 'Express' || serviceResult[0] == 'Tracked' )
																								{
																									$('.tracking').show();
																									$("#Linnworksapispostaltracking").focus();
																									if(trackID == 'No' || trackID == '')
																									{
																										alert('Please insert tracking ID');
																										$("#Linnworksapispostaltracking").val();
																									}
																									else
																									{
																										$("#Linnworksapispostaltracking").val( trackID );
																										$("#Linnworksapispostaltracking").attr( 'readonly' , true);
																									}
																								}
																							$('.loading-image').hide();
																							$('.processedorder').show();
																							
																						}
																			});
																		
																	$.ajax({
																															
																				'url'            : getUrl() + '/domeshippingnew',
																				'type'           : 'POST',
																				'data'           : { pkorderid : pkorder, splitOrderId : splitOrderId, openOrderId : openOrderId},
																				'beforeSend'	 : function() {
																												$('.loading-image').show();
																												},
																				'success'		 : function( data )
																									{
																										var result = data.split("#");
																										$('.printlable').css('opacity','1');
																										$('.printorder').css('opacity','1');
																										$('.loading-image').hide();
																										$('.processedorder').show();
																									}	
																		});
																	}
																	
																	
																});
																									
}

function submitSanningValue()
{
	var trackID = '<?php echo ($getItems[0]['MergeUpdate']['track_id'] != '') ? $getItems[0]['MergeUpdate']['track_id'] : ''; ?>';
	
	var Barcode	=	$('#LinnworksapisBarcode').val();
	var id				=	$('#Linnworksapisid').val();
	var count 			= 	$('#barco_'+id).attr('for');
	var splitOrderId 	= 	$('#split_order_id').val( );
	var openOrderId 	= 	$('#open_order_id').val( );
	
	$('#LinnworksapisBarcode').val('');
	$.ajax({
						'url'            : getUrl() + '/completethebarcode',
						'type'           : 'POST',
						'dataType'		 : "json",
						'data'           : { barcode : Barcode,  id : id, splitOrderId: splitOrderId},
						'success' 		 : function( msgArray )
														{
															
															$('.upqty_'+count+'_'+msgArray.id).text(msgArray.msg);
															//$('.upqty_'+count+'_'+msgArray.id).css('background-color', 'yellow');
															 $( '.upqty_'+count+'_'+msgArray.id ).removeClass('bg-white').addClass('bg-yellow-400');
															 $('.barcodemsg').html('')
															 $( '#LinnworksapisBarcode' ).addClass('form-control');
															 $( '#LinnworksapisBarcode' ).css('border', '1px solid #c6c6c6');
															 
															setTimeout(function ()
																				{
																								$( '.upqty_'+count+'_'+msgArray.id ).removeClass('bg-yellow-400').addClass('bg-white');
																				}, 1000); 
															
															
															if(msgArray.status == 1)
															{
																var innercount = 0;
																var outercount = 0;
																var flag;
																var i = 0;
																
																$('.quantity ').each(function()
																	{
																		
																			innercount++;
																			var id = $(this).attr('id');
																			
																			if($('.upqty_'+i+'_'+id).text() == $('.qty_'+i+'_'+id).text())
																			{ 
																				$('.complete_'+i+'_'+id).html('<a class=" count btn btn-success btn-xs margin-right-10" title="" href=""><i class="ion-checkmark"></i></a>');
																			}
																			i = i+1;
																	});
																	
																$('.count').each(function()
																	{
																		outercount++;
																			var id = $(this).attr('id');
																			
																	});
																	if(innercount == outercount)
																	{
																		
																		var id = $('.printlable').attr('for');
																		var service	=	$('.postal_service').text();
																										var serviceResult	=	service.split(' ');
																										if(serviceResult[0] == 'Express' || serviceResult[0] == 'Tracked' )
																											{
																												$('.tracking').show();
																												$("#Linnworksapispostaltracking").focus();
																												if(trackID == 'No' || trackID == '')
																												{
																													alert('Please insert tracking ID');
																													$("#Linnworksapispostaltracking").val();
																												}
																												else
																												{
																													$("#Linnworksapispostaltracking").val( trackID );
																													$("#Linnworksapispostaltracking").attr( 'readonly', true );
																												}
																											}
																		
																		if(serviceResult[0] != 'Express' && serviceResult[0] != 'Tracked' )
																		 {
																		 orderprocess();
																		$.ajax({
																			
																				'url'            : getUrl() + '/generatepdfdirect',
																				'type'           : 'POST',
																				'data'           : { pkorderid : id, splitOrderId : splitOrderId, openOrderId :openOrderId },
																				'beforeSend'	 : function() {
																												$('.loading-image').show();
																												},
																				'success'		 : function( data )
																									{
																										var result = data.split("#");
																										$('.printlable').css('opacity','1');
																										$('.printorder').css('opacity','1');
																										$('.loading-image').hide();
																										$('.processedorder').show();
																									}
																			});
																		
																		$.ajax({
																			
																				'url'            : getUrl() + '/domeshippingnew',
																				'type'           : 'POST',
																				'data'           : { pkorderid : id, splitOrderId : splitOrderId, openOrderId : openOrderId },
																				'beforeSend'	 : function() {
																												$('.loading-image').show();
																												},
																				'success'		 : function( data )
																									{
																										var result = data.split("#");
																										$('.printlable').css('opacity','1');
																										$('.printorder').css('opacity','1');
																										$('.loading-image').hide();
																										$('.processedorder').show();
																									}
																		});
																	}
																	}
															}
															if(msgArray == 2)
															{
																$('.barcodemsg').html('<p style = "color: red; font-size: 16px; font-weight: bold; text-align: left;" >Item Completed  OR Barcode dose not exist.</p>')
																$( '#LinnworksapisBarcode' ).css('border', '1px solid red');
															}
															
														}  
						});  
					
}

$(document).keypress(function(e) {
    if(e.which == 32) {
        processOrder();
    }
});
function processOrder()
{
	var id	=	$('.processedorder').attr('for');
	$.ajax({
				'url'          : getUrl() + '/processorder',
				'type'         : 'POST',
				'data'         : { id : id },
				'beforeSend'	 : function() {
													$('.loading-image').show();
													},
				'success' 	   : function( msgArray )
											{
												if(msgArray == 1)
												{
													$('.loading-image').hide();
													window.location.href = getUrl()+"/dispatchConsole";
												}
												else
												{
													
												}
											}  
			});
	
}
		
		
</script>



