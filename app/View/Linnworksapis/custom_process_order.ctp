<?php 
	App::import('Controller', 'Linnworksapis');
	$checkproduct =	new LinnworksapisController;
	$qty	=	$checkproduct->checkInventery();
	
?>
<?php if( isset($results) ) { foreach($results as $result) { ?>
<div class="modal fade pop-up-4" tabindex="-1" role="dialog" id="pop-up-4" aria-labelledby="myLargeModalLabel-4" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Order Id ( <?php echo $result->NumOrderId; ?> )</h4>
         </div>
         <div class="modal-body bg-grey-100">
           	<div class="panel no-margin" style="clear:both">
					<div class="panel-body no-padding-top" >
					
					<div class="row" style="border: 5px solid #c0c0c0; box-shadow: 0 0 30px -18px rgba(66, 66, 66, 1) inset;">
                        <div class="col-lg-5">
							<div class="panel no-margin-bottom">
                                <div class="panel-title bg-white no-border">
									<div class="panel-head">Order Detail</div>
									<div class="panel-tools">
								</div>
								</div>
                                <div class="panel-body no-padding bg-white">
									
									<table class="table table-bordered" cellpadding="5px;">
										<thead>
											<tr>
												<th class="vertical-middle">Label</th>
												<th class="vertical-middle">Detail</th>
											</tr>
										</thead>
										<tbody>
											<tr> 
												<td class="vertical-middle">Source</td>
												<td class="vertical-middle"><?php echo $result->GeneralInfo->Source . '('.$result->GeneralInfo->SubSource .')'; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Email</td>
												<td class="vertical-middle"><?php echo $result->CustomerInfo->Address->EmailAddress; ?></td>
											</tr>
											<tr>
											<?php
												$address = '';
												$address .= $result->CustomerInfo->Address->Address1.', ';
												$address .= $result->CustomerInfo->Address->Address2.', ';
												$address .= $result->CustomerInfo->Address->Address3.', ';
												$address .= $result->CustomerInfo->Address->Town.', ';
												$address .= $result->CustomerInfo->Address->PostCode.', ';
												$address .= $result->CustomerInfo->Address->Region.', ';
												$address .= $result->CustomerInfo->Address->Country;
											?>
												<td class="vertical-middle">Address</td>
												<td class="vertical-middle"><?php echo $address; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">FullName</td>
												<td class="vertical-middle"><?php echo $result->CustomerInfo->Address->FullName; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">PhoneNumber</td>
												<td class="vertical-middle"><?php echo $result->CustomerInfo->Address->PhoneNumber; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Subtotal</td>
												<td class="vertical-middle"><?php echo $result->TotalsInfo->Subtotal .'&nbsp; ('. $result->TotalsInfo->Currency .')'; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Tax</td>
												<td class="vertical-middle"><?php echo $result->TotalsInfo->Tax .'&nbsp; ('. $result->TotalsInfo->Currency .')' ; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">TotalCharge</td>
												<td class="vertical-middle"><?php echo $result->TotalsInfo->TotalCharge .'&nbsp; ('. $result->TotalsInfo->Currency .')' ; ?></td>
											</tr>
									
										</tbody>
									</table>
                                </div>
                            </div>
                        </div><!-- /.col -->
						
						
                        <div class="col-lg-7">
							<div class="panel">
                                <div class="panel-title bg-white no-border">
									<div class="panel-head">Sub Order</div>
									<div class="panel-tools">
								</div>
								</div>
                                <div class="panel-body no-padding bg-white">
									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th class="vertical-middle">Sub Order id</th>
													<th class="vertical-middle">SKU</th>
													<th class="vertical-middle">Barcode</th>
													<!--<th class="vertical-middle">Product Type</th>-->
													<th class="vertical-middle">Price</th>
													<!--<th class="vertical-middle">Qty</th>
													<th class="vertical-middle">Bin/Rack</th>-->
												</tr>
											</thead>
											<tbody>
											<?php $items	=	 $checkproduct->getorderItem( $result->NumOrderId );
												foreach($result->Items as $item ) 
													{  ?>
												<tr>
													<td>
													<?php
														print $item->MergeUpdate->product_order_id_identify;
													?>
													</td>
													<td>
													<?php
														print str_replace(',', '<br>', $item->MergeUpdate->sku);
													?>
													</td>
													<td>
													<?php
														print str_replace(',', '<br>', $item->MergeUpdate->barcode);
													?>
													</td>
													<td>
													<?php
														print $item->MergeUpdate->price;
													?>
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="row">
                        		
								<div class="col-lg-12" >
								<table class="table">
									<tr>
										<td>Cancel Note: </td><td>Refund: </td>
									</tr>
									<tr>
									<td>
										<div class="form-group no-margin">
										<textarea class="form-control" rows="1" cols="20" id ="refund_notes_<?php echo $result->NumOrderId; ?>" > </textarea> 
									</td>
									<td>
										<div class="form-group no-margin">
											<input type ="text" class="form-control" id ="refund_amount_<?php echo $result->NumOrderId; ?>"  value =<?php echo $result->TotalsInfo->Subtotal?> > 
										</div>
									</div>
									<input type ="hidden" id ="warehouse_id_<?php echo $result->NumOrderId; ?>"  value = "00000000-0000-0000-0000-000000000000"> 
									</td>
									</tr>
									</table>
									</div>
									<div class="col-lg-12" style="text-align:center; padding-bottom: 7px;">	
									<a href="javascript:void(0);" for = "<?php echo $item->MergeUpdate->product_order_id_identify; ?>" for = "" class="customOrderProcess btn bg-red-400 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;">Custom Process</a>									
									</div>
								</div>
							</div>
                    </div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } } ?>



<script>
   $(document).ready(function()
   {
       $('#pop-up-4').modal('show');
   });
</script>

