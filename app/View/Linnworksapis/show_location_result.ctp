<div class="modal popUpbinLocation fade pop-up-4" tabindex="-1" role="dialog" id="pop-up-4location" aria-labelledby="myLargeModalLabel-4" aria-hidden="true">
	<div class="modal-dialog" style="width:1250px;">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close closeLocation" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Order Id ( <?php echo $orderID; ?> )</h4>
            <input type = "hidden" class="getOpenOrderID" value="<?php echo $orderID; ?>"/>
         </div>
         <div class="modal-body bg-grey-100">
           	<div class="no-margin" style="clear:both">
					<input class="radioyes" type="radio" name="radio" value="Yes" checked>Yes
					<input class="radiono" type="radio" name="radio" value="No">No
					<div class="showlocation">
					<div class="panel-body no-padding-top" >
					<div class="row" style="padding:20px 0; border: 5px solid #c0c0c0; box-shadow: 0 0 30px -18px rgba(66, 66, 66, 1) inset;">
                      <div class="col-lg-6">
							<div class="panel subOrderBlock">
                               </div>
                                <div class="panel-body padding-10">
								<div class="panel subOrderRows padding-bottom-5 margin-bottom-10" style="background-color: #ccc;" >
									<div class="table-responsive">
									</div>
									<div class="row">
										<div class="col-lg-12 margin-left-5">
										<?php 
											$i = 1;$k = 0;
											foreach( $skuLocationData as $LocationData) { 
								         ?>
										<div class="forClassLocation">
										<table class="table margin-top-5 skudata_<?php echo $i; ?>" >
											<tbody>
												<tr>
													<td>
													Split Order ID
													</td>
													<td class="orderid_<?php echo $i; ?>" >
														<?php  echo $LocationData['splitorder_id'];  ?>
													</td>
												</tr>
												<tr>
													<td>
													Sku
													</td>
													<td class="sku_<?php echo $i; ?>" >
														<?php  echo $LocationData['product_sku'];  ?>
													</td>
												</tr>
												<tr>
													<td>
													Barcode
													</td>
													<td class="barcode_<?php echo $i; ?>" >
														<?php  echo $LocationData['product_barcode'];  ?>
													</td>
												</tr>
												<tr>
													<td>
													Quantity
													</td>
													<td class="quanlity_<?php echo $i; ?>" >
													<?php  echo $LocationData['sku_qty'];  ?>
													</td>
												</tr>
												<tr>
													<td>
													Locations
													</td>
													<td  >
													<?php 
													$options[] = $LocationData['sku_loacation'];
													
													echo $this->Form->select('location',  $options, array( 'class' =>"location_$i", 'escape' => false,'empty' => 'Choose location'));
													 ?>
													</td>
												</tr>
												<tr>
												<td></td>
												<td>
												<?php 
												$k=0;
												foreach( $LocationData['sku_loacation'] as $option ) {
													echo $option.' => '.$LocationData['stock_by_location'][$k]."<br>";
													
													$k++;} ?>
												</td>
												</tr>
												<tr>
													<td>
														<a href="javascript:void(0);" aria-hidden="true" onclick="updateLocation( <?php echo $i; ?> );" class="updateSingleLocation btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;">Save</a>
													</td>
												</tr>
											</tbody>
										</table>
										<?php $i++; $options = ''; $option = '';} ?>
										</div>
										</div>
											
									</div>
								</div>
					        </div>
                        </div><!-- /.col -->
						<div class="col-lg-12" style="text-align:center; padding-bottom: 7px;">	
							<a href="javascript:void(0);" aria-hidden="true" class="cancelWhileOrder btn bg-orange-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;">Cancel</a>
							<a href="javascript:void(0);" aria-hidden="true" class="closepopup btn bg-red-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;">Back</a>
						</div>
                        </div>
                       </div>	
					</div>
					</div>
					
					
					</div>
					<div class="nolocation" style="padding:20px 0; border: 5px solid #c0c0c0; box-shadow: 0 0 30px -18px rgba(66, 66, 66, 1) inset; display:none">
						<div class="col-lg-12" style="text-align:center; padding-bottom: 7px;">	
							<a href="javascript:void(0);" aria-hidden="true" class="cancelWhileOrder btn bg-orange-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;">Cancel</a>
							<a href="javascript:void(0);" aria-hidden="true" class="closepopup btn bg-red-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;">Back</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	
	
	
   $(document).ready(function()
   {
				$('input[type="radio"]').click(function(){
					if($(this).attr("value")=="No"){
						$(".showlocation").hide();
						$(".nolocation").show();
					}
					if($(this).attr("value")=="Yes"){
						$(".showlocation").show();
						$(".nolocation").hide();
					}
				});
				$('#pop-up-4location').modal('show');
   });
   
   $("body").on("click", ".closepopup", function(){
			$( ".close" ).click();
		});
		
	function updateLocation( count )
		{
			var orderId 	= 	$( '.getOpenOrderID' ).val();
			var sku 		= 	$( '.sku_'+count ).text();
			var barcode 	= 	$( '.barcode_'+count ).text();
			var quantity 	= 	$( '.quanlity_'+count ).text();
			var location	=	$( '.location_'+count ).find(":selected").text();
			
			if( location == 'Choose location' )
			{
				swal( 'Please select location' );
				return false;
			}
			
			$.ajax({
				'url'            : '/Products/updateLocation',
				'type'           : 'POST',
				'data'           : { 
										orderId		: orderId,
										sku      	: sku,
										barcode     : barcode,
										quantity    : quantity,
										location    : location,
									},
				'beforeSend'	 : function() 
												{
													$( ".outerOpac" ).attr( 'style' , 'display:block');
												}, 	
				'success' 		 : function( msgArray )
									{
										$(".skudata_"+count).hide();
										$( ".outerOpac" ).attr( 'style' , 'display:none');
									}  
								  
				}); 
			
		}
		
		$('body').on('click', '.cancelWhileOrder', function(){
			
				var openOrderId	=	$('.getOpenOrderID').val();
				var optionValue	=	$('input[type=radio]:checked').attr("value");
				$.ajax({
				'url' 					: 	'/products/setCancelOrderStatusAfterPopup',
				'type'					: 	'POST',
				'data' 					: 	{ 
												openOrderId : openOrderId,
												optionValue	: optionValue
											},
				'success' 				  : function( msgArray )
				 {
					if( msgArray == '1' )
					{
						$( ".close" ).click();
						swal( 'Order Cancelled Successfully' );
						location.reload();
					}
					else
					{
						swal( 'Opps! there is an error' );
					} 
					
				 } 
				
			});
			
		});
</script>

