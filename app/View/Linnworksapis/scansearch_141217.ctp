<table class="table table-bordered table-striped dataTable" >
	<tr>
		<th>OrderID</th>
		<th>Order</th>
		<th>Postal Service</th>
		<th>Quantity</th>
		<th>SKU</th>
		<th>Title</th>
		<th>Barcode</th>
		<th>Packaging id</th>
		<th>Packaging Name</th>
		<th>Action</th>
	</tr>	
<?php foreach($express1 as $exp ) { $exqty = 0; //pr( $exp ); ?>
		 <tr>
			<td><?php echo $exp['MergeUpdate']['order_id']; ?></td>
			<td><?php echo $exp['MergeUpdate']['product_order_id_identify']; ?></td>
			<td><?php echo $exp['MergeUpdate']['postal_service']; ?></td>
			<?php foreach($exp['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td>'.$expitem['split_qty']."<br>".'</td>'; } ?>
			<?php foreach($exp['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td>'.$expitem['split_sku']."<br>".'</td>'; } ?>
			<?php foreach($exp['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td class="productTitle"><span>'.$expitem['title']."</span>".'</td>'; } ?>
			<?php foreach($exp['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td>'.$expitem['split_barcode']."<br>".'</td>';
			} ?>
			<td><?php echo $exp['MergeUpdate']['envelope_id']; ?></td>
			<td><?php echo $exp['MergeUpdate']['envelope_name']; ?></td>
			<td><a class="btn btn-xs btn-success" href="/linnworksapis/confirmBarcode/<?php echo $exp['MergeUpdate']['id']; ?>" ><i class="ion-clipboard"></i></td>
		</tr>
<?php } ?>

<?php foreach($tracked1 as $tra ) {  $traqty = 0; ?>
		<tr>
			<td><?php echo $tra['MergeUpdate']['order_id']; ?></td>
			<td><?php echo $tra['MergeUpdate']['product_order_id_identify']; ?></td>
			<td><?php echo $tra['MergeUpdate']['postal_service']; ?></td>
			<?php foreach($tra['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td>'.$expitem['split_qty']."<br>".'</td>'; } ?>
			<?php foreach($tra['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td>'.$expitem['split_sku']."<br>".'</td>'; } ?>
			<?php foreach($tra['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td class="productTitle"><span>'.$expitem['title']."</span>".'</td>'; } ?>
			<?php foreach($tra['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td>'.$expitem['split_barcode']."<br>".'</td>';
			} ?>
			<td><?php echo $tra['MergeUpdate']['envelope_id']; ?></td>
			<td><?php echo $tra['MergeUpdate']['envelope_name']; ?></td>
			<td><a class="btn btn-xs btn-success" href="/linnworksapis/confirmBarcode/<?php echo $tra['MergeUpdate']['id']; ?>" ><i class="ion-clipboard"></i></td>
		</tr>
<?php } ?>


<?php foreach($standerd1 as $sta ) {  $stqty2 = 0 ?>
		<tr>
			<td><?php echo $sta['MergeUpdate']['order_id']; ?></td>
			<td><?php echo $sta['MergeUpdate']['product_order_id_identify']; ?></td>
			<td><?php echo $sta['MergeUpdate']['postal_service']; ?></td>
			<td><?php foreach($sta['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_qty']."<br>"; } ?></td>
			<td><?php foreach($sta['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_sku']."<br>"; } ?></td>
			<td class="productTitle"><?php foreach($sta['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<span>'.$expitem['title']."</span>"; } ?></td>
			<td><?php foreach($sta['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_barcode']."<br>";
			} ?>
			</td>
			<td><?php echo $sta['MergeUpdate']['envelope_id']; ?></td>
			<td><?php echo $sta['MergeUpdate']['envelope_name']; ?></td>
			<td><a class="btn btn-xs btn-success" href="/linnworksapis/confirmBarcode/<?php echo $sta['MergeUpdate']['id']; ?>" ><i class="ion-clipboard"></i></td>
		</tr>
<?php } ?>


<!-------------------------------------------- Start Code for group 2 ------------------------------------------>

<?php foreach($express2 as $exp2 ) { $exqty2 = 0; ?>
		 <tr>
			<td><?php echo $exp2['MergeUpdate']['order_id']; ?></td>
			<td><?php echo $exp2['MergeUpdate']['product_order_id_identify']; ?></td>
			<td><?php echo $exp2['MergeUpdate']['postal_service']; ?></td>
			<td><?php foreach($exp2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_qty']."<br>"; } ?></td>
			<td><?php foreach($exp2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_sku']."<br>"; } ?></td>
			<td class="productTitle"><?php foreach($exp2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<span>'.$expitem['title']."</span>"; } ?></td>
			<td><?php foreach($exp2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_barcode']."<br>";
			} ?>
			</td>
			<td><?php echo $exp2['MergeUpdate']['envelope_id']; ?></td>
			<td><?php echo $exp2['MergeUpdate']['envelope_name']; ?></td>
			<td><a class="btn btn-xs btn-success" href="/linnworksapis/confirmBarcode/<?php echo $exp2['MergeUpdate']['id']; ?>" ><i class="ion-clipboard"></i></td>
		</tr>
<?php } ?>


<?php foreach($tracked2 as $tra2 ) {  $traqty2 = 0; ?>
		<tr>
			<td><?php echo $tra2['MergeUpdate']['order_id']; ?></td>
			<td><?php echo $tra2['MergeUpdate']['product_order_id_identify']; ?></td>
			<td><?php echo $tra2['MergeUpdate']['postal_service']; ?></td>
			<td><?php foreach($tra2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_qty']."<br>"; } ?></td>
			<td><?php foreach($tra2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_sku']."<br>"; } ?></td>
			<td class="productTitle"><?php foreach($tra2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<span'.$expitem['title']."</span>"; } ?></td>
			<td><?php foreach($tra2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_barcode']."<br>";
			} ?>
			</td>
			<td><?php echo $tra2['MergeUpdate']['envelope_id']; ?></td>
			<td><?php echo $tra2['MergeUpdate']['envelope_name']; ?></td>
			<td><a class="btn btn-xs btn-success" href="/linnworksapis/confirmBarcode/<?php echo $tra2['MergeUpdate']['id']; ?>" ><i class="ion-clipboard"></i></td>
		</tr>
<?php } ?>



<?php foreach($standerd2 as $sta2 ) {  $stqty2 = 0 ?>
		<tr>
			<td><?php echo $sta2['MergeUpdate']['order_id']; ?></td>
			<td><?php echo $sta2['MergeUpdate']['product_order_id_identify']; ?></td>
			<td><?php echo $sta2['MergeUpdate']['postal_service']; ?></td>
			<td><?php foreach($sta2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_qty']."<br>"; } ?></td>
			<td><?php foreach($sta2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_sku']."<br>"; } ?></td>
			<td class="productTitle"><?php foreach($sta2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<span>'.$expitem['title']."</span>"; } ?></td>
			<td><?php foreach($sta2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_barcode']."<br>";
			} ?>
			</td>
			<td><?php echo $sta2['MergeUpdate']['envelope_id']; ?></td>
			<td><?php echo $sta2['MergeUpdate']['envelope_name']; ?></td>
			<td><a class="btn btn-xs btn-success" href="/linnworksapis/confirmBarcode/<?php echo $sta2['MergeUpdate']['id']; ?>" ><i class="ion-clipboard"></i></td>
		</tr>
<?php } ?>
<!-------------------------------------------- End Code for group 2 ------------------------------------------>
</tbody>
</table>
<script>
$(function() {
	$('table tbody tr:nth-child(2)').css('background-color','#c4ebff'); 	
});
</script>


