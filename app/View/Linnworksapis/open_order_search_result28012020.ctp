﻿<?php 
	App::import('Controller', 'Linnworksapis');
	$checkproduct =	new LinnworksapisController;
	$userID = $this->session->read('Auth.User.id');
	
?>
<style>
.Archive{
background-color:#99FFCC !important;
}
</style>
<?php if( isset($results) ) { foreach($results as $result) { ?>
<div class="modal fade pop-up-4" tabindex="-1" role="dialog" id="pop-up-4search" aria-labelledby="myLargeModalLabel-4" aria-hidden="true">
	<div class="modal-dialog" style="width:1250px;">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Order Id ( <?php echo $result->NumOrderId; ?><?php if($archive){ echo ' ['.$archive.']';} ?> )</h4>
         </div>
         <div class="modal-body bg-grey-100 <?php if($archive){ echo  $archive;} ?>">
           	<div class="no-margin" style="clear:both">
					<div class="panel-body no-padding-top" >
					
					<div class="row" style="padding:20px 0; border: 5px solid #c0c0c0; box-shadow: 0 0 30px -18px rgba(66, 66, 66, 1) inset;">
                        <div class="col-lg-6 <?php if( $result->OpenStatus == 2 ){ printf( "%s", "col-lg-offset-3" ); } ?>">
							<div class="panel no-margin-bottom">
                                <div class="panel-title no-border">
									<div class="panel-head">Order Detail</div>
									<div class="panel-tools">
								</div>
								</div>
                                <div class="panel-body no-padding ">
									
									<table class="table table-striped" cellpadding="5px;">									
										<tbody>
											<tr> 
												<?php 
											 
											 if( in_array($result->NumOrderId, $oversell_items)){
												  print "<tr><td class=\"vertical-middle\" colspan=\"2\" style=\"background:#FF6633;\">OverSell</td></tr>";
											  } 
											  
											  ?>
												<td class="vertical-middle">Order Type</td>
												<td class="vertical-middle"><?php if( $result->linn_fetch_orders == 0 ){print "Not Paid";}else if($result->linn_fetch_orders == 1 ){ print "Paid";}else if($result->linn_fetch_orders == 4 ){ print "Resend";}?></td>
											</tr>
													<!--changes by amit rawat-->
											<?php if($result->parent_id!='' && $result->parent_id!='0') { ?>
											<tr> 
										  	<td class="vertical-middle">Parent Order Id:</td>
												<td class="vertical-middle"><a style="color:#3f88c5;text-decoration:underline;" data-id='<?php  echo $result->parent_id; ?>' href="javascript:void(0);" class='orderlinkrelation' ><?php  echo $result->parent_id; ?></a></td>
											</tr>
											<?php } ?> 

											<?php if($childorderid!='' && $childorderid!='0') { ?>
											<tr> 
										  	<td class="vertical-middle">Child Order Id:</td>
												<td class="vertical-middle"><a style="color:#3f88c5;text-decoration:underline;" data-id='<?php  echo $childorderid; ?>' href="javascript:void(0);" class='orderlinkrelation' ><?php  echo $childorderid; ?></a></td>
											</tr>
											<?php } ?> 
											<!--changes by amit rawat-->
											<tr> 
												<?php
													//pr($result);													 
													$royal_error = $checkproduct->royalMailErrorCheck($result->NumOrderId); 
												?>
												<td class="vertical-middle">Order Status</td>
												<td class="vertical-middle"><?php if( $result->OpenStatus == 0 ){print "Open";}else if($result->OpenStatus == 1 ){  print 'Processed';}else if($result->OpenStatus == 2 ){ print "Cancelled";}else if( $result->OpenStatus == 3 &&  $order_note ){ print 'Locked <span style="background-color:#FF0000;padding:2px;color:#fff;">[ '.$order_note. ']</span>'; }  

?>
												<?php if($royal_error ){  echo '<span style="background-color:#FF0000;padding:2px;color:#fff;">RoyalMail Error:'.$royal_error.'.</span>';}?>
												</td>
											</tr>
											<tr> 
												<td class="vertical-middle">Received Date</td>
												<td class="vertical-middle"><?php print $result->GeneralInfo->ReceivedDate; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Source</td>
												<td class="vertical-middle"><?php if(isset($result->sub_source) &&  $result->sub_source != ''){ printf( "%s", $result->sub_source );} else{ printf( "%s", $result->GeneralInfo->SubSource ); }; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Process Date</td>
												<td class="vertical-middle"><?php if( $result->OpenStatus != 0 ){ 
												echo '<b>Server Time :-'.$result->process_date."<br>"; 
												echo '<b>Jersey Time :-'.date("Y-m-d H:i:s", strtotime("+5 hours",strtotime($result->process_date))).'</b>';
												 }else { print "--";} ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Reference No.</td>
												<td class="vertical-middle"><?php print $result->GeneralInfo->ReferenceNum; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Service</td>
												<td class="vertical-middle"><?php print $result->ShippingInfo->PostalServiceName; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Action</td>
												<td class="vertical-middle"><?php $checkCus	=	 $this->Common->checknegetiveCustomer($result->CustomerInfo->Address->EmailAddress);
												if( $checkCus == 1){ ?>
												<a href="javascript:void(0);" id="markasnegative" class="btn bg-green-500 color-white btn-dark padding-left-20 padding-right-20" style="margin:0 auto; text-align: center;" onclick="markasnegative('<?php echo $result->NumOrderId; ?>', 'unblock');">Unblock Customer</a>
												<?php } else { ?>
												<a href="javascript:void(0);" id="markasnegative" class="btn bg-red-500 color-white btn-dark padding-left-20 padding-right-20" style="margin:0 auto; text-align: center;" onclick="markasnegative('<?php echo $result->NumOrderId; ?>', 'block');">Block Customer</a>
												<?php } ?></td>
											</tr>
											<tr> 
												<?php 
												$userIds		=	explode(',', $getModuleRole['SuperAdmin']);
												if(in_array( $userID, $userIds)){ ?>
												<td class="vertical-middle">Email</td>
												<td class="vertical-middle">
												<?php echo $result->CustomerInfo->Address->EmailAddress; } ?>
												</td>
											</tr>
											<tr>
											<?php
											//pr($result);
											
												$address = '';
												$address .= $result->CustomerInfo->Address->Address1.', ';
												$address .= $result->CustomerInfo->Address->Address2.', ';
												$address .= $result->CustomerInfo->Address->Address3.', ';
												$address .= $result->CustomerInfo->Address->Town.', ';
												$address .= $result->CustomerInfo->Address->PostCode.', ';
												$address .= $result->CustomerInfo->Address->Region.', ';
												$address .= $result->CustomerInfo->Address->Country;
											?>
												<td class="vertical-middle">Address</td>
												<td class="vertical-middle"><?php echo $address; ?> 
												<!-- edit address code starts -->
												<?php if($result->parent_id != '' && $result->parent_id != null && $result->OpenStatus == 0 ) { ?>
													<a onclick="editAddress('<?php echo $result->NumOrderId; ?>')" href="#" class="btn btn-primary a-btn-slide-text btn-sm" style="padding: 1px 2px 1px 2px;">
														<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
														<span><strong>Edit</strong></span>            
													</a>
												<!-- edit address code ends -->
												<?php } ?>
												</td>
												
											</tr>
											<tr> 
												<td class="vertical-middle">FullName</td>
												<td class="vertical-middle"><?php echo $result->CustomerInfo->Address->FullName; ?></td>
											</tr>
											<tr> 
											<?php 
												$userIds		=	explode(',', $getModuleRole['SuperAdmin']);
												if(in_array( $userID, $userIds)){ ?>
												<td class="vertical-middle">PhoneNumber</td>
												<td class="vertical-middle"><?php echo $result->CustomerInfo->Address->PhoneNumber; } ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Subtotal</td>
												<td class="vertical-middle"><?php echo $result->TotalsInfo->Subtotal .'&nbsp; ('. $result->TotalsInfo->Currency .')'; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Tax</td>
												<td class="vertical-middle"><?php echo $result->TotalsInfo->Tax .'&nbsp; ('. $result->TotalsInfo->Currency .')' ; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">TotalCharge</td>
												<td class="vertical-middle"><?php echo $result->TotalsInfo->TotalCharge .'&nbsp; ('. $result->TotalsInfo->Currency .')' ; ?></td>
											</tr>
											
										</tbody>
									</table>
                                </div>
                            </div>
                        </div><!-- /.col -->
					
					
						<?php
						if( $result->OpenStatus != 2 ):
						?>
					    <div class="col-lg-6">
							<div class="panel subOrderBlock" style="margin-bottom: 0px;">
                                <div class="panel-title no-border panel-tools">
									<table width = 100%>
										<tr>
										  <td width = 25% style="text-align: left;">Sub Order id</td>
										  <td width = 32% style="text-align: left;">SKU</td>
										  <td width = 33% style="text-align: left;">Barcode</td>
										  <td width = 15% style="text-align: left;">Price</td>
										</tr>
									</table>
								</div>
								</div>
								<div style="background-color: #ccc;" class="panel-body padding-5">
									<table width = 100%>
										<tr>
										  <td width = 15% style="text-align: left;">Order Status</td>
										  <td width = 1% style="text-align: left;">&nbsp;</td>
										  <td width = 44% style="float:right">
										 	<?php 
											foreach($result->Items as $scanitem )
											{
												if( $result->OpenStatus == 1 && $scanitem->MergeUpdate->service_provider != 'DHL' )
												{
													if( $scanitem->MergeUpdate->scan_status == 0 && $scanitem->MergeUpdate->sorted_scanned == 0)
													{	
													echo 'Ready To Sorting';	
													?>
													<td width = 40% style="text-align: left;"><a href="javascript:void(0);" for = "<?php echo $result->NumOrderId; ?>" class="after_process_held btn btn-success color-white btn-dark padding-left-10 padding-right-10  col-lg-10" style="margin:0 auto; text-align: center;">Cancel</a><td>
													<?php
													
													}
													else 
													{	
													echo "Scaned"; 
													}
												} else { echo "Open"; }
											}
											
											?>
										  </td>
										  
										</tr>
									</table>
								</div>
                                <div class="panel-body padding-10">
								<?php $items	=	 $checkproduct->getorderItem( $result->NumOrderId );
												foreach($result->Items as $item ) 
													{  
													$pickListStatus = $item->MergeUpdate->pick_list_status;
													if($pickListStatus == 1){
														$pickStatusClass = "style=background-color:#f9dfb9";
													} else {
														$pickStatusClass = "style=background-color:#ccc";
													}
								?>
								<div class="panel subOrderRows padding-bottom-5 margin-bottom-10" style="background-color: #ccc;" >
									<div class="table-responsive">
										<table class="table table-striped margin-bottom-5">
											<tbody>
											<?php //pr( $item ); ?>
												<tr <?php echo $pickStatusClass; ?> >
													<td>
													<?php
														print $item->MergeUpdate->product_order_id_identify;
													?>
													</td>
													<td>
													<?php
														print str_replace(',', '<br>', $item->MergeUpdate->sku);
													?>
													</td>
													<td>
													<?php
														$mer_barcodes = explode(',',$item->MergeUpdate->barcode);
														foreach( $mer_barcodes as $mer_barcode )
														{
															print $this->Common->getLatestBarcode( $mer_barcode ).'<br>';
														}
														//print str_replace(',', '<br>', $this->Common->getLatestBarcode($item->MergeUpdate->barcode));
													?>
													</td>
													<td>
													<?php
														print $item->MergeUpdate->price;
													?>
													</td>
												</tr>
												
											</tbody>
										</table>
									</div>
									
									<div class="row">
										<div class="col-lg-7 margin-left-5">
											<input type="text" id="postalServiceid_<?php echo $item->MergeUpdate->id; ?>" value="<?php echo $item->MergeUpdate->service_id; ?>" class="form-control" name="service_id">
											<input type="hidden" id="postalServiceidHidden_<?php echo $item->MergeUpdate->id; ?>" value="<?php echo $item->MergeUpdate->service_id; ?>" class="form-control" name="service_id">
										</div>
										<div class="col-lg-7"> 
											<label>Override Default Service</label>
											<?php 	print $this->form->input( 'Marked.custom_postal_service', array( 'type'=>'checkbox', 'div'=>false,'label'=>false,'class'=>'', 'required'=>false ) ); ?>
										</div>								
										<a href="javascript:void(0);" for = "<?php echo $item->MergeUpdate->id; ?>" class="CustomPostalService btn btn-success color-white btn-dark padding-left-10 padding-right-10  col-lg-4" style="margin:0 auto; text-align: center;">Service Assign</a>
									</div>
									
									<div class="row">
										<div class="col-lg-12 margin-left-5">
										
										<table class="table margin-top-5">
											<tbody>
												
												<tr >
													<td>
													Order Status (Custom Process)
													</td>
													<td class="move_date" >

														<?php  
															//changes by amit rawat
														if( ($item->MergeUpdate->status == 1) && $item->MergeUpdate->custom_marked == 0) {
														  echo "Processed";	
														} elseif($item->MergeUpdate->status == 1 && $item->MergeUpdate->custom_marked == 1) { 
														  echo "Custom Processed";		
														} elseif($item->MergeUpdate->status == 4 && $item->MergeUpdate->custom_marked == 1) { 
                                                                                                                                                                                                                                                                        
														    echo "Custom Processed (Returned)";	
													                    			
														 } elseif($item->MergeUpdate->status == 4 && $item->MergeUpdate->custom_marked == 0) { 
 														  
														    echo "Processed - Returned";	
													                      
														 }  else {
														   echo "----";
														 }
														if($item->MergeUpdate->is_refund==1) {
														 echo " (Refunded)";
														}
if($item->MergeUpdate->is_resend==1) {
														 echo " (Resend)";
														}
 																//changes by amit rawat
                                                     												?>
													</td>
													
												</tr>
												<tr>
													<td>Packet Weight & Dimentions</td>
													<td>
														<?php print $item->MergeUpdate->packet_weight ." Kg  L-".  $item->MergeUpdate->packet_length .' MM, W-'. $item->MergeUpdate->packet_width .' MM, H-'. $item->MergeUpdate->packet_height .' MM' ?>
													</td>
													
												</tr>
												<tr>
													<td>Envelope Weight </td>
													<td>
														<?php print $item->MergeUpdate->envelope_weight." Kg";   ?>
													</td>
													
												</tr>
												
												<tr>
													<td>
													Service Provider
													</td>
													<td class="service_provider_<?php echo $item->MergeUpdate->id; ?>" >
														<?php  printf( "%s" , $item->MergeUpdate->service_provider ); print " (".$item->MergeUpdate->postal_service.")"; ?>
													</td>
													
												</tr>
												<tr>
													<td>
													Service Code
													</td>
													<td class="service_code_<?php echo $item->MergeUpdate->id; ?>" >
														<?php  printf( "%s" , $item->MergeUpdate->provider_ref_code ); ?>
													</td>
													
												</tr>
												<tr>
													<td>
													Service Name
													</td>
													<td class="service_name_<?php echo $item->MergeUpdate->id; ?>" >
														<?php  printf( "%s" , $item->MergeUpdate->service_name ); ?>
													</td>
													
												</tr>
												<tr>
													<td>
													Move Date
													</td>
													<td class="move_date" >
														<?php  printf( "%s" , $item->MergeUpdate->order_date ); ?>
													</td>
													
												</tr>
												<tr>	
													<td>
														Processed By
													</td>
													<td class="move_date" >
														<?php  
															echo $checkproduct->getUserDetail( $item->MergeUpdate->user_id );
														?>
													</td>
												</tr>
												<tr>	
													<td>Sorting Time</td>
													<td><?php echo $item->MergeUpdate->scan_date; ?></td>
												</tr>
												<tr>	
													<td>
														Tracking ID
													</td>
													<td class="div_tracking_id" >
														<div id="tracking_id_<?php echo $item->MergeUpdate->product_order_id_identify; ?>"><?php  printf( "%s" , $item->MergeUpdate->reg_post_number ); ?></div>
														<?php  if($item->MergeUpdate->reg_post_number == '' && $item->MergeUpdate->service_provider == 'PostNL' && $item->MergeUpdate->postal_service == 'Tracked' ){ ?>
														
														
													 <a href="javascript:void(0);" id="href_tracking_id_<?php echo $item->MergeUpdate->product_order_id_identify; ?>" class="btn btn-success color-white btn-dark padding-left-10 padding-right-10  col-lg-6" style="margin:0 auto; text-align: center;" onclick="assignTrackingId('<?php echo $item->MergeUpdate->product_order_id_identify; ?>')">Assign Tracking Id</a>													 
														
														<?php }?>
													</td>
													
													
												</tr>
												<tr>	
													<td>Order assigned</td>
													<td class="assign_user" >
													<?php
													if($item->MergeUpdate->assign_user == '' && $item->MergeUpdate->status == 0 ){
														echo "----";
													} else { 
														echo $item->MergeUpdate->assign_user;
													}
													$userIds		=	explode(',', $getModuleRole['ReleaseOrder']);
													?>
													<?php if($item->MergeUpdate->assign_user != '' && $item->MergeUpdate->status == 0 ){
																if(in_array( $userID, $userIds)) {
													 ?>
													<a href="javascript:void(0);" class="releaseor btn bg-green-500 color-white btn-dark padding-left-20 padding-right-20" onclick="releaseorder('<?php echo $item->MergeUpdate->product_order_id_identify ?>');" style="margin:0 auto; text-align: center;">Release</a>
													<?php } } ?> 
													
													
													</td>
												</tr>
												
												
											</tbody>
										</table>
										
										</div>
											
									</div>
									<!-- changes by amit rawat -->
								  <div class="row">
								    <div class="col-lg-12">
								      <div class="col-lg-12" style="text-align:right; padding-bottom: 5px; margin-top:5px;">	
											<?php if($item->MergeUpdate->status == 1) {  ?>
													<a style="margin:0 auto; text-align: right; padding: 6px 70px;" href="javascript:void(0);" onclick="returnSearchOrder('<?php echo $item->MergeUpdate->product_order_id_identify; ?>')" class="btn-warning btn bg-info-500 color-white btn-dark padding-left-20 padding-right-20" role="button" title="Return"><i class="glyphicon glyphicon-repeat"></i> Return Order</a>
											<?php } ?>
											<?php if($item->MergeUpdate->status == 4) {  ?>
													<a style="margin:0 auto; text-align: right; padding: 6px 70px;" href="javascript:void(0);" onclick="editSearchOrder('<?php echo $item->MergeUpdate->product_order_id_identify; ?>','<?php echo $item->MergeUpdate->return_reason; ?>','<?php echo $item->MergeUpdate->item_condition; ?>','<?php echo $item->MergeUpdate->comments; ?>')" class="btn-warning btn bg-info-500 color-white btn-dark padding-left-20 padding-right-20" role="button" title="Edit"><i class="glyphicon glyphicon-edit"></i> Edit Return</a>
											<?php } ?>
											<?php if(($item->MergeUpdate->status == 4 || $item->MergeUpdate->status == 1) && isset($result->TotalsInfo->Subtotal) && isset($result->TotalsInfo->PostageCost) && isset($result->TotalsInfo->TotalCharge) && isset($result->TotalsInfo->Currency)) { ?>
												  <a style="margin:0 auto; text-align: right; padding: 6px 70px;" href="javascript:void(0);" onclick="refundSearchOrder('<?php echo $item->MergeUpdate->product_order_id_identify; ?>','<?php echo $result->TotalsInfo->Subtotal; ?>','<?php echo $result->TotalsInfo->PostageCost; ?>','<?php echo $result->TotalsInfo->TotalCharge; ?>','<?php echo $result->TotalsInfo->Currency; ?>')" class="btn btn-primary padding-left-20 padding-right-20" role="button" title="Refund"><i class="glyphicon glyphicon-retweet"></i> Refund</a>
										  <?php } ?>
											<?php if(($item->MergeUpdate->status == 4 || $item->MergeUpdate->status == 1) && strtolower($item->MergeUpdate->order_type)!='resend' && $item->MergeUpdate->is_refund==0 && isset($result->TotalsInfo->TotalCharge)) {  ?>
											    <a style="margin:0 auto; text-align: right; padding: 6px 70px;" href="javascript:void(0);" onclick="resendSearchOrder('<?php echo $item->MergeUpdate->order_id; ?>','<?php echo $item->MergeUpdate->product_order_id_identify; ?>','<?php echo $result->GeneralInfo->Source; ?>','<?php echo $result->GeneralInfo->SubSource; ?>','<?php echo $item->MergeUpdate->order_date; ?>','<?php echo $result->customerInfo->Address->FullName; ?>','<?php echo $item->MergeUpdate->process_date; ?>','<?php echo $item->MergeUpdate->postal_service; ?>','<?php echo $result->TotalsInfo->TotalCharge; ?>','<?php echo $item->MergeUpdate->parent_id; ?>')" class="btn btn-success padding-left-20 padding-right-20" style="margin-top:1px;" role="button" title="Resend"><i class="glyphicon glyphicon-send"></i> Resend</a> 
											<?php } ?>
										 </div>
										</div>
									</div>
									<!-- changes by amit rawat -->
								</div>

								  <?php } ?>
					        </div>
                        </div><!-- /.col -->
                       <?php
						endif;
                       ?>
                        
                         <div class="col-lg-6">
							  <div class="col-lg-6">
								 <label for="ba_<?php echo $result->NumOrderId; ?>">Billing Address</label> 
								<textarea class="invoice" name="billing_address" id="ba_<?php echo $result->NumOrderId; ?>" cols="40" rows="4" for="<?php echo $result->NumOrderId; ?>"></textarea>	
							 </div>	
							  <div class="col-lg-6">
								 <label for="sa_<?php echo $result->NumOrderId; ?>">Shipping Address</label>						 
								  <textarea class="invoice" name="shipping_address" id="sa_<?php echo $result->NumOrderId; ?>" cols="40" rows="4" for="<?php echo $result->NumOrderId; ?>"></textarea>				  
							 </div>
						 </div>
                        
                        </div>
                        <div class="row">
                        		<div class="col-lg-12" >
								<?php
									if( $result->OpenStatus == 0 || $result->OpenStatus == 3 ):
								?>	
								<table class="table" style="padding-bottom:5px;">
									<tr>
										<td><label>Cancel Note:</label><textarea class="form-control" rows="1" cols="20" id ="refund_notes_<?php echo $result->NumOrderId; ?>"> </textarea></td>
										<td><label>Refund:</label> <input type ="text" class="form-control" id ="refund_amount_<?php echo $result->NumOrderId; ?>"  value =<?php echo $result->TotalsInfo->Subtotal?> > 
										<input type ="hidden" id ="warehouse_id_<?php echo $result->NumOrderId; ?>"  value = "00000000-0000-0000-0000-000000000000"> 
										</td>
									</tr>
									
									<?php
									if( $result->OpenStatus == 0 )
									{
									?>
										<tr>
											<td colspan="2" align="center">
												<a href="javascript:void(0);" for = "<?php echo $result->NumOrderId; ?>" class="lockOrder btn bg-red-500 color-white btn-dark padding-left-20 padding-right-20" style="margin:0 auto; text-align: center;">Lock Order</a>
												<a href="javascript:void(0);" for = "<?php echo $result->NumOrderId; ?>" class="cancelOpenOrder btn bg-orange-500 color-white btn-dark padding-left-20 padding-right-20" style="margin:0 auto; text-align: center;">Cancel Order</a>
												<a href="" class="loading-image_<?php echo $result->NumOrderId; ?>" style="display:none;"><img src="http://xsensys.com/app/webroot/img/image_889915.gif" hight ="20" width="20" /></a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->id; ?>###<?php echo $item->MergeUpdate->product_order_id_identify; ?>" class="assign_packaging_slip btn bg-green-500 color-white btn-dark padding-left-20 padding-right-20" href="javascript:void(0);" title = "Assign Packaging Service" data-toggle="modal" >Assign (Slip / Label)</a>
												<a href="javascript:void(0);" for = "<?php echo $result->NumOrderId; ?>" class="deleteOpenOrder btn bg-red-500 color-white btn-dark padding-left-20 padding-right-20" style="margin:0 auto; text-align: center;">Delete Order</a>
												
											</td>
											
										</tr>
										
									<?php
									}
									else if( $result->OpenStatus == 3 )
									{
									?>
										<tr>
											<td colspan="2" align="center">												
												<a href="javascript:void(0);" for ="<?php echo $result->NumOrderId; ?>" class="unlockOrder btn bg-red-500 color-white btn-dark padding-left-20 padding-right-20" style="margin:0 auto; text-align: center;">Unlock Order</a>
												<a href="javascript:void(0);" for = "<?php echo $result->NumOrderId; ?>" class="cancelOpenOrder btn bg-orange-500 color-white btn-dark padding-left-20 padding-right-20" style="margin:0 auto; text-align: center;">Cancel Order</a>
												<a href="" class="loading-image_<?php echo $result->NumOrderId; ?>" style="display:none;"><img src="http://xsensys.com/app/webroot/img/image_889915.gif" hight ="20" width="20" /></a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->id; ?>###<?php echo $item->MergeUpdate->product_order_id_identify; ?>" class="assign_packaging_slip btn bg-green-500 color-white btn-dark padding-left-20 padding-right-20" href="javascript:void(0);" title = "Assign Packaging Service" data-toggle="modal" >Assign (Slip / Label)</a>
												<a href="javascript:void(0);" for = "<?php echo $result->NumOrderId; ?>" class="deleteOpenOrder btn bg-red-500 color-white btn-dark padding-left-20 padding-right-20" style="margin:0 auto; text-align: center;">Delete Order</a>
												<?php /*?><a href="<?php echo Router::url('/', true)?>Invoice/Generate/<?php echo $result->NumOrderId; ?>" target="_blank" for = "<?php echo $result->NumOrderId; ?>" class="btn-info btn bg-info-500 color-white btn-dark padding-left-20 padding-right-20 print-invoice" style="margin:0 auto; text-align: center;">Generate Invoice</a><?php */?>
											</td>
											
										</tr>
										
									<?php
									}
									
									?>
									</table>
									<?php
										endif;
									?>
									<div class="col-lg-12" style="text-align:center; padding-bottom: 5px; margin-top:5px;">	
										<a href="javascript:void(0);" aria-hidden="true" class="dismiss btn bg-green-500 color-white btn-dark" style="margin:0 auto; text-align: center; padding: 6px 70px;">OK</a>
										<a href="<?php echo Router::url('/', true)?>Invoice/Generate/<?php echo $result->NumOrderId; ?>" target="_blank" for = "<?php echo $result->NumOrderId; ?>" class="btn-info btn bg-info-500 color-white btn-dark padding-left-20 padding-right-20 print-invoice" style="margin:0 auto; text-align: center;">Generate Invoice</a>
 
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } }?>
<!-- changes by amit rawat starts -->
<!-- Modal -->
<div class="modal fade" id="myModalReturnSearchOrder" role="dialog">
		<div class="modal-dialog">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Return Order - Order ID: <span id="orderidtextreturn"></span></h4>
			</div>
			<div class="modal-body">
			   <div class="text-danger hidden alert-danger" id="form-error-popup-return" style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;"></div>
				 <div class="row" style="margin-bottom:5px;">
				 	<div class="col-sm-4">Return Reason*:</div>
					<div class="col-sm-8">
 					<select name="returnreason" id="returnreason" class="form-control" >
					  <option value="">Please select a reason</option>
						<option value="Accidental Order">Accidental Order</option>
						<option value="Better price available">Better price available</option>
						<option value="The shipping box or envelope is not damaged but the item is damaged">The shipping box or envelope isn't damaged but the item is damaged</option>
						<option value="Missing estimated delivery date">Missing estimated delivery date</option>
						<option value="Missing parts or accessories">Missing parts or accessories</option>
						<option value="The shipping box or envelope and item are both damaged">The shipping box or envelope and item are both damaged</option>
						<option value="Different from what was ordered">Different from what was ordered</option>
						<option value="Defective/ Does not work properly">Defective/ Does not work properly</option>
						<option value="Arrived in addition to what was ordered">Arrived in addition to what was ordered</option>
						<option value="No longer needed/ wanted">No longer needed/ wanted</option>
						<option value="Unauthorized purchase">Unauthorized purchase</option>
						<option value="Different from website description">Different from website description</option> 
					</select>
					<span class="text-danger hidden" id="reason-error">Return reason is a required field!</span>
 					</div>
				 </div>
				 
				 <div class="row" style="margin-bottom:5px;">
				 	<div class="col-sm-4">Item Condition*:</div>
					<div class="col-sm-8">
						<select name="itemcondition" id="itemcondition" class="form-control" >
						  <option value="">Please select an item condition</option>
							<option value="New box intact - A++">New box intact - A++</option>
							<option value="New box open - B++">New box open - B++</option>
							<option value="New packaging damaged - A">New packaging damaged - A</option>
							<option value="Used no packaging - B">Used no packaging - B</option>
							<option value="Danger/ Faulty">Danger/ Faulty</option>
							<option value="Working/ Box open">Working/ Box open</option>
							<option value="Packed New">Packed New</option>   
						</select>
						<span class="text-danger hidden" id="condition-error">Item condition is a required field!</span>
					</div>
				 </div>
				 
				  <div class="row">
				 	<div class="col-sm-4">Comments:</div>
					<div class="col-sm-8">
					  <textarea name="comments" id="comments" class="form-control"></textarea>
					</div>
				 </div>
			 	 <input type="hidden" id="order_id" value="" />
				 <input type="hidden" id="returnType" value="Add" />
				 
				 <div class="row"><div class="col-sm-12"><button type="button" class="btn btn-info" onclick="confirmSearchReturn()" >SAVE</button></div></div>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		  </div>
		</div>
	  </div>

<!-- refund order -->
		<!-- Modal -->
		<div class="modal fade" id="myModalSearchRefund" role="dialog">
		<div class="modal-dialog">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Refund Order- Order ID: <span id="orderidtextrefund"></h4>
			</div>
			<div class="modal-body">
		   	 <div class="text-danger hidden alert-danger" id="form-error-popup-refund" style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;"></div> 
					
					<div class="row" id="refundTablelist">
					  
					</div>
				 
				 <div class="row">
				 	<div class="col-sm-4"><p>Product Price:<p></div>
					<div id="productprice" class="col-sm-8">
            
 					</div>
				 </div>
				 
				 <div class="row">
				 	<div class="col-sm-4"><p>Shipping Cost:</p></div>
					<div id="shippingcost" class="col-sm-8"> 
					</div>
				 </div>
				 
				  <!-- <div class="row">
				 	<div class="col-sm-4">Additional Price:</div>
					<div class="col-sm-8">
					  
					</div>
				 </div> -->

				 <div class="row">
				 	<div class="col-sm-4"><p>Total Price:</p></div>
					<div id="totalprice" class="col-sm-8">
					  
					</div>
				 </div>
         <input type="hidden" name="totalpricehidden" id="totalpricehidden" value="" /> 
				 <div class="row" style="margin-bottom:5px;">
				 	<div class="col-sm-4"><p>Refund*:</p></div>
					<div class="col-sm-8">
				  	<select name="refundoptions" id="refundoptions" class="form-control" >
						  <option value="">Please select a value</option>
							<option value="fullamountoforder">Full Amount Of Order</option>
							<option value="productcost">Product Cost</option>    
							<option value="shippingcost">Shipping Cost</option>    
							<option value="additionalamount">Additional Amount</option>
							<option value="discount">Discount</option>   
						</select> 
						<span class="text-danger hidden" id="refund-error">Please select a refund option!</span>
					</div>
				 </div>
				 <div class="row hidden" id="customrefundblock" style="margin-bottom:5px;">
				 	<div class="col-sm-4"><p>Custom Refund Amount*:</p></div>
					<div class="col-sm-8">
					  <input onkeypress="return isNumber(event)" type="text" id="custom_refund_amount" class="form-control" placeholder="Enter Amount" value="" /> 
						<span class="text-danger hidden" id="refund-custom-error">Please enter a value!</span>
					</div>
				 </div> 
				 <div class="row hidden" id="customrefundreason" style="margin-bottom: 5px;">
				 	<div class="col-sm-4"><p>Reason*:</p></div>
					<div class="col-sm-8">
					  <textarea id="custom_refund_reason" class="form-control" placeholder="Enter a reason"></textarea> 
						<span class="text-danger hidden" id="refund-custom-reason-error">Please enter a reason!</span>
					</div>
				 </div>
				 <div class="row hidden" id="totalrefundamount">
				 	<div class="col-sm-4"><p><b>Total Refund:</b></p></div>
					<div class="col-sm-8">  
						<span id="totalrefundtext"></span>
					</div>
				 </div>
			 	  <input type="hidden" id="refund_order_id" value="" /> 
					<input type="hidden" id="product_price" value="" />
					<input type="hidden" id="shipping_cost" value="" />
					<input type="hidden" id="total_price" value="" />
				 <div class="row">
				  <div class="col-sm-12" style="text-align:right;">
					 <button type="button" class="btn btn-info" onclick="confirmSearchRefund()" >REFUND</button>
					</div>
				 </div>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		  </div>
		</div>
	  </div>
<!-- resend order -->
		<!-- Modal -->
		<div class="modal fade" id="myModalSearchResendOrder" role="dialog" style="overflow-x: hidden;overflow-y: auto;">
		<div class="modal-dialog modal-lg">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Resend Order - Order ID: <span id="orderidtext"></span></h4>
			</div>
			<div class="modal-body">

				 <div style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;" class="msgalert alert-danger" id="form-error-popup-resend-error"></div>
				 <div style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;" class="msgalert alert-success" id="form-error-popup-resend-success"></div>

				 <div class="row"> 
          
					<form class="form-horizontal" id="frmcreatepackage">
						<div class="col-md-12 separator">
						   <label class="col-sm-4" style="padding: 0;"><b>Basic Details</b></label>
						</div>
					  <div class="col-md-6">
						
						<div class="form-group">
							<label class="col-sm-4" for="Source">Source:</label>
							<div class="col-sm-8">
								<input readonly type="email" class="form-control" id="source" placeholder="Source">
								
								 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4" for="Sub source">Sub source:</label>
							<div class="col-sm-8"> 
								<input readonly type="text" class="form-control" id="sub_source" placeholder="Sub source">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4" for="Order Date">Order Date:</label>
							<div class="col-sm-8"> 
								<input readonly class="form-control textbox-n" type="text" id="order_date" placeholder="order date">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4" for="Customer">Customer:</label>
							<div class="col-sm-8"> 
								<input readonly type="text" class="form-control" id="full_name" placeholder="Customer">
							</div>
						</div>
						</div>
          
			    	<div class="col-md-6">
             
						<div class="form-group">
							<label class="col-sm-4" for="Processed on">Processed on:</label>
							<div class="col-sm-8">
								<input readonly type="email" class="form-control" id="processed_date" placeholder="Processed on">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4" for="Service:">Service:</label>
							<div class="col-sm-8"> 
								<input readonly type="text" class="form-control" id="service" placeholder="Service:">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4" for="Total">Total:</label>
							<div class="col-sm-8"> 
								<input readonly type="text" class="form-control" id="total" placeholder="Total">
							</div>
						</div>  

						<div class="form-group">
							<label class="col-sm-4" for="Total"></label>
							<div class="col-sm-8"> 
							<button  id="confirmfullordersearchbutton"  type="button" style="margin-left: 13px;" class="btn btn-info pull-right" onclick="confirmFullOrderSearchResend()" >RESEND FULL ORDER</button>
							</div>
						</div> 
						
          </div> 
				 
					
          <div class="col-md-12 separator">
						<label class="col-sm-4" style="padding: 0;"><b>Sku Details</b></label>
					</div> 
				 	<div class='col-md-12' id="skulist">
					 
					</div>
          <div class="col-md-12"> 
						<label class="col-sm-2 col-sm-offset-10" style="padding: 0;"><button type="button" class="btn btn-success pull-right" onclick="addMoreSearch()">+ Add More</button></label>
					</div> 
					<input type="hidden" name="mainorderId" id="mainorderId" value="" />
					<input type="hidden" name="resend_order_id" id="resend_order_id" value="" /> 
					<input type="hidden" name="parent_order_id" id="parent_order_id" value="" />  
					<input type="hidden" name="new_order_id" id="neworderid" value="" />
					<input type="text"   name="archive" id="archive" value="<?php echo $archive?>">

					<div class="col-md-12 separator">
						<label class="col-sm-4" style="padding: 0;"><b>Resend Order Details</b></label>
					</div>   
					
					<div class="col-md-6">
					<div class="form-group">
							 <label class="col-sm-4" for="Service:">Full Name*:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="fullname" name="fullname" placeholder="Full Name" />
								 <span class="text-danger hidden" id="fullname-error">Please enter a value!</span>
							 </div>
						</div>
					 <div class="form-group">
						<label class="col-sm-4" for="Service:">Reason*:</label>
						 <div class="col-sm-8">
							<select name="resend_reason" id="resend_reason" class="form-control normalbox" >
								<option value="">Please select a value</option>
								<option value="Not Delivered">Not Delivered</option>	
                                                                                                                                                     <option value="Wrong Item">Wrong Item</option>								
									<optgroup label="Warehouse Mistake">
								 <option value="Warehourse error">Warehourse error </option>
								 <option value="Listing error">Listing error </option>
								</optgroup>
								<option value="Wrong Item(Listing Issue)">Wrong Item(Listing Issue)</option>
								<option value="Damaged Item">Damaged Item</option>
								<option value="Defective Item">Defective Item</option>
								<option value="Quantity mismatch">Quantity mismatch</option> 
							</select>  
							<span class="text-danger hidden" id="resend-reason-error">Please select a value!</span>
						 </div>
						</div> 
						
					  <div class="form-group">
							 <label class="col-sm-4" for="Service:">Comments:</label>
							 <div class="col-sm-8"> 
								 <textarea class="form-control normalbox" id="resend_comments" name="resend_comments" placeholder="Reason"></textarea> 
							 </div>
						 </div> 
 
             <div class="form-group">
							 <label class="col-sm-4" for="Service:">Town*:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="town" name="town" placeholder="Town" />
								 <span class="text-danger hidden" id="town-error">Please enter a value!</span>
							 </div>
						</div>
						
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Region:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="region" name="region" placeholder="Region" />
 							 </div>
						</div>
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Post Code*:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="postcode" name="postcode" placeholder="Post Code" />
								 <span class="text-danger hidden" id="postcode-error">Please enter a value!</span>
							 </div>
						</div> 
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Phone Number:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="phonenumber" name="phonenumber" placeholder="phonenumber" />
							 </div>
						</div>
						</div> 

						<div class='col-md-6'>
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Country*:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="country" name="country" placeholder="Country" />
								 <span class="text-danger hidden" id="country-error">Please enter a value!</span>
							 </div>
						</div> 
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Company:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="company" name="company" placeholder="Company" /> 
							 </div>
						</div>

            </div> 

					<div class="col-md-6">
              
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Address1*:</label>
							 <div class="col-sm-8"> 
							   <input type="text" class="form-control normalbox " id="address1" name="address1" placeholder="Address 1" />
								 <span class="text-danger hidden" id="resend-address1-error">Please enter an address!</span>
							 </div>
						 </div>

						 <div class="form-group">
							 <label class="col-sm-4" for="Service:">Address2:</label>
							 <div class="col-sm-8"> 
							  <input type="text" class="form-control normalbox " id="address2" name="address2" placeholder="Address 2" />
							 </div>
						 </div>

						 <div class="form-group">
							 <label class="col-sm-4" for="Service:">Address3:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="address3" name="address3" placeholder="Address 3" />
							 </div>
						 </div> 
					 </div> 
					</form>  
				 </div>
				 
				 

				 <div class="row">
				  <div class="col-sm-12">
					<label class="col-sm-1 col-sm-offset-9" style="padding: 0; margin-left: 81%;"><button type="button" class="btn btn-warning pull-right" onclick="saveSearchTemp('0')">Save</button></label>
					<button type="button" class="btn btn-info pull-right" onclick="saveSearchTemp('1')">RESEND</button>
					</div>
				 </div>
			</div> 
		  </div>
		</div>
	  </div>

		<div class="modal fade" id="myModalResendSearchOrderStatus" role="dialog">
		<div class="modal-dialog modal-lg">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Alert</h4>
			</div>
			<div class="modal-body" id='liststatusorder'>
		
		  </div>
	</div>
</div>
</div>
<!-- changes by amit rawat ends -->
<!--changesnew -->
<div class="modal fade" id="myModalSearchWarning" role="dialog">
		<div class="modal-dialog">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Warning:</h4>
			</div>
			<div class="modal-body">
			    <div style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;" class="alert-danger" id="form-error-popup-resend-warning">
				  <b>Low Inventory! Would you still like to proceed?</b>
				</div>	
				<div class="row" id="stockTablelist">
					
				</div>  

				<div class="row">
				  <div class="col-sm-12" style="text-align:right;">
					<button type="button" class="btn btn-info" id="continueresend">Yes</button>
					<button type="button" class="btn btn-danger" id="continueclose" data-dismiss="modal" >No</button>
				  </div>
				</div> 
			</div> 
		  </div>
		</div>
	  </div>

	  <!-- edit address code starts -->
	  <div class="modal fade" id="myModalEditOrderAddress" role="dialog" style="overflow-x: hidden;overflow-y: auto;">
		<div class="modal-dialog modal-lg">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Edit Address - Order ID: <span id="addressorderidtext"></span></h4>
			</div>
			<div class="modal-body">

				 <div style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;" class="msgalert alert-danger" id="form-error-popup-address-error"></div>
				 <!-- <div style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;" class="msgalert alert-success" id="form-error-popup-address-success"></div> -->

				 <div class="row"> 
          
					<form class="form-horizontal" id="frmaddresschange">
				    
					<input type="hidden" name="address_order_id" id="address_order_id" value="" /> 

					<div class="col-md-12 separator">
						<label class="col-sm-4" style="padding: 0;"><b>Resend Order Details</b></label>
					</div>   
					
					<div class="col-md-6">  
						 
 
                           <div class="form-group">
							 <label class="col-sm-4" for="Service:">Town*:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="town" name="town" placeholder="Town" />
								 <span class="text-danger hidden" id="town-error">Please enter a value!</span>
							 </div>
						</div>
						
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Region:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="region" name="region" placeholder="Region" />
 							 </div>
						</div>
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Post Code*:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="postcode" name="postcode" placeholder="Post Code" />
								 <span class="text-danger hidden" id="postcode-error">Please enter a value!</span>
							 </div>
						</div>  
						</div> 

						<div class='col-md-6'>
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Country*:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="country" name="country" placeholder="Country" />
								 <span class="text-danger hidden" id="country-error">Please enter a value!</span>
							 </div>
						</div>  

            </div> 

					<div class="col-md-6">
              
						<div class="form-group">
							 <label class="col-sm-4" for="Service:">Address1*:</label>
							 <div class="col-sm-8"> 
							   <input type="text" class="form-control normalbox " id="address1" name="address1" placeholder="Address 1" />
								 <span class="text-danger hidden" id="resend-address1-error">Please enter an address!</span>
							 </div>
						 </div>

						 <div class="form-group">
							 <label class="col-sm-4" for="Service:">Address2:</label>
							 <div class="col-sm-8"> 
							  <input type="text" class="form-control normalbox " id="address2" name="address2" placeholder="Address 2" />
							 </div>
						 </div>

						 <div class="form-group">
							 <label class="col-sm-4" for="Service:">Address3:</label>
							 <div class="col-sm-8"> 
								 <input type="text" class="form-control normalbox " id="address3" name="address3" placeholder="Address 3" />
							 </div>
						 </div> 
					 </div> 
					</form>  
				 </div>
				 
				 

				 <div class="row">
				  <div class="col-sm-12">
 					<button type="button" class="btn btn-info pull-right" onclick="updateAddress()">Save</button>
					</div>
				 </div>
			</div> 
		  </div>
		</div>
	  </div>
	  <!-- edit address code ends -->
<script>	
	function assignTrackingId(split_order_id){
	
			$.ajax({
				dataType: 'json',
				url :   '<?php echo Router::url('/', true)?>Apiactions/assignTracking/'+split_order_id,
				type: "POST",
				data : {order_id:split_order_id},
				success: function(txt, textStatus, jqXHR)
				{	
					if(txt.status > 1){
						alert(txt.msg);
					}else{ 
						$("#tracking_id_"+split_order_id).html(txt.msg);
						$("#href_tracking_id_"+split_order_id).hide();
					} 
				}		
			});				
	}
	
   $(document).ready(function()
   {
       $('#pop-up-4search').modal('show');
	   
	   	$('.subOrderBlock>.panel-body').slimScroll({
        height: '275px',
		width:'100%',
		size:'3',
		alwaysVisible: true
    });
   });
   
   
   
   function releaseorder( splitorderid )
   {
   		swal({
				title: "Are you sure?",
				text: "You want to release order!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						$.ajax({
									dataType	: 	'json',
									url 		:   '<?php echo Router::url('/', true)?>Reports/releaseuserorder',
									type		: 	"POST",
									data 		: 	{ splitorderid : splitorderid },
									success: function(txt, textStatus, jqXHR)
									{	
										$(".releaseor").hide();
										$(".assign_user").text('');
									}		
								});
					}
					else
					{
						$('.close').click();
						swal("Safe", "Order is safe :)", "success");
					}
			});	
   }
   
   
   
   function markasnegative(orderid, status)
	{
		swal({
				title: "Are you sure?",
				text: "You want to "+status+" Customer !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						$.ajax({
									dataType	: 	'json',
									url 		:   '<?php echo Router::url('/', true)?>Reports/savecustomerinfo',
									type		: 	"POST",
									data 		: 	{orderid:orderid, status:status},
									success: function(txt, textStatus, jqXHR)
									{	
										$("#markasnegative").hide();
									}		
								});
					}
					else
					{
						$('.close').click();
						swal("Safe", "Customer is safe :)", "error");
					}
			});		
   } 
   
   
   $("body").on("click", ".dismiss", function(){
			//var forValue 		= $(this).attr( 'for' );
			$( ".close" ).click();
			//location.reload();
			/*if( forValue == 1)
			{
				$( ".close" ).click();
				location.reload();
			}*/	 
		});
		
		$("body").on("change", ".invoice", function()
        {
			$(".outerOpac").show();
			//$('.outerOpac span').css({'z-index':'10000'});
			
			//var id			=	$(this).attr("id");
			var orderid		=	$(this).attr("for");	
			var field_name  =	$(this).attr("name");	
			var vals		=	$(this).val();
			
			$.ajax({
					dataType: 'json',
					url :   '<?php echo Router::url('/', true)?>Invoice/Address',
					type: "POST",
					data : {order_id:orderid,field:field_name,val:vals},
					success: function(txt, textStatus, jqXHR)
					{	
									
						//$("#msg").html('');				
						//$("#msg").html('&nbsp;'+txt.data);
						$(".outerOpac").hide();
						//$('.outerOpac span').css({'z-index':'999'});
					}		
				});
		
		});

//changes by amit rawat
		$(document).ready(function() {
       $('.orderlinkrelation').on('click',function() { 
var searchKey = $(this).attr('data-id');
				$.ajax({
					url     	: '/Linnworksapis/searchAnyOrderInList',
					type    	: 'POST',
					data    	: {  searchKey : searchKey },
					beforeSend	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
					},   			  
					success 	:	function( data  )
					{
						if( data == 1 )
						{
							$( ".outerOpac" ).attr( 'style' , 'display:none');
							swal( "Oops!!!, ( < "+ searchKey +" >) not found here.");
						}
						else
						{
							$( ".outerOpac" ).attr( 'style' , 'display:none');
							$('.showOPenOrderGlobalSearchResult').html( data );
						}
					}                
			});
		 });
	$('#myModalReturnSearchOrder #returnreason').on('change',function() {
			if($(this).val()!='') {
			 $("#reason-error").addClass('hidden');
			}
		});

		$('#myModalReturnSearchOrder #itemcondition').on('change',function() {
			if($(this).val()!='') {
			 $("#condition-error").addClass('hidden');
			}
		});

$(document).delegate('#myModalSearchResendOrder .removebutton','click',function() {  
			$(this).parent('td').parent('tr').remove();
		}); 

    $(document).delegate('#myModalSearchResendOrder .checkrow','click',function() {
			if($(this).is(':checked')==false) {
			  $('#myModalSearchResendOrder .skurow-'+$(this).attr('data-row')).attr('disabled',true);	
			} else {
				$('#myModalSearchResendOrder .skurow-'+$(this).attr('data-row')).attr('disabled',false);	
			}
		});
$(document).delegate('.pricebox','keyup',function() { 
			$('#costskurow-'+$(this).attr('data-row')).html($(".qtyrow-"+$(this).attr('data-row')).val() * $(this).val()); 
		});

		$(document).delegate('.qtybox','keyup',function() { 
			$('#costskurow-'+$(this).attr('data-row')).html($(".pricerow-"+$(this).attr('data-row')).val() * $(this).val()); 
		});
		});

 function addMoreSearch() {
			var index = parseInt($('#myModalSearchResendOrder .productlistcontainer').length) + 1;
			$("#myModalSearchResendOrder #skulistbody").append('<tr class="productlistcontainer"><td>&nbsp;</td><td><input type="text" name="sku[]" class="requiredfield form-control normalbox skurow-'+index+'" data-row="'+index+'" /></td><td><input data-row="'+index+'" onkeypress="return isNumber(event)" type="text" name="quantity[]" class="requiredfield form-control normalbox qtybox skurow-'+index+' qtyrow-'+index+'" /></td><td><input data-row="'+index+'" onkeypress="return isNumber(event)" class="requiredfield pricebox form-control normalbox skurow-'+index+' pricerow-'+index+'" type="text" value="" name="price[]"></td><td id="costskurow-'+index+'"></td><td><button type="button" class="btn btn-danger pull-right removebutton">-</button></td></tr>');
		}

	function returnSearchOrder(orderId='') {
			$('#myModalReturnSearchOrder').modal('show');
			$('#myModalReturnSearchOrder #orderidtextreturn').html(orderId);
			$("#myModalReturnSearchOrder #order_id").val(orderId);
			$("#myModalReturnSearchOrder #returnreason").val('');
			$("#myModalReturnSearchOrder #itemcondition").val('');
			$("#myModalReturnSearchOrder #comments").val('');	    	
			$("#myModalReturnSearchOrder #returnType").val('Add');
			$("#myModalReturnSearchOrder #reason-error").addClass('hidden');
			$("#myModalReturnSearchOrder #condition-error").addClass('hidden');
			$("#myModalReturnSearchOrder #form-error-popup-return").html('').addClass('hidden'); 
		}

		function confirmSearchReturn(){
			$("#myModalReturnSearchOrder #form-error-popup-return").html('').addClass('hidden');
			if($("#myModalReturnSearchOrder #returnType").val()=='Add') {
					//add reason
					var ajaxFlag=true;
					if($("#myModalReturnSearchOrder #returnreason").val()=='') {
						$("#myModalReturnSearchOrder #reason-error").removeClass('hidden');
					} else {
						$("#myModalReturnSearchOrder #reason-error").addClass('hidden');
					}
					if($("#myModalReturnSearchOrder #itemcondition").val()=='') {
						$("#myModalReturnSearchOrder #condition-error").removeClass('hidden');
					} else {
						$("#myModalReturnSearchOrder #condition-error").addClass('hidden');
					}

					if($("#myModalReturnSearchOrder #returnreason").val()=='' || $("#myModalReturnSearchOrder #itemcondition").val()=='') {
					  ajaxFlag = false;
					}

					if(ajaxFlag==true) {
						$.ajax({
							dataType: 'json',
							url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders<?php echo $archive?>/confirmReturn',
							type: "POST",				
							cache: false,			
							data : {orderid:$("#myModalReturnSearchOrder #order_id").val(),returnreason:$("#myModalReturnSearchOrder #returnreason option:selected").val(),itemcondition:$("#myModalReturnSearchOrder #itemcondition option:selected").val(),comments:$("#myModalReturnSearchOrder #comments").val(),archive:'<?php echo $archive?>' },
							success: function(data, textStatus, jqXHR)
							{	 
								if(data.status==1) {
									$('#myModalReturnSearchOrder #myModalReturnSearchOrder').modal('hide');
									location.reload();
								} else {
									$("#myModalReturnSearchOrder #form-error-popup-return").html(data.msg).show();
								} 							
							}	
						});   		
					}
			} else {
					//edit reason
					var ajaxFlag=true;
					if($("#myModalReturnSearchOrder #returnreason").val()=='') {
						$("#myModalReturnSearchOrder #reason-error").removeClass('hidden'); 
					} else {
						$("#reason-error").addClass('hidden'); 
					}
					if($("#myModalReturnSearchOrder #itemcondition").val()=='') {
						$("#myModalReturnSearchOrder #condition-error").removeClass('hidden'); 
					} else {
						$("#myModalReturnSearchOrder #condition-error").addClass('hidden'); 
					}

					if($("#myModalReturnSearchOrder #returnreason").val()=='' || $("#myModalReturnSearchOrder #itemcondition").val()=='') {
					  ajaxFlag = false;
					}

					if(ajaxFlag==true) {
						$.ajax({
							dataType: 'json',
							url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders<?php echo $archive?>/editReturn',
							type: "POST",				
							cache: false,			
							data : {orderid:$("#myModalReturnSearchOrder #order_id").val(),returnreason:$("#myModalReturnSearchOrder #returnreason option:selected").val(),itemcondition:$("#myModalReturnSearchOrder #itemcondition option:selected").val(),comments:$("#myModalReturnSearchOrder #comments").val() },
							success: function(data, textStatus, jqXHR)
							{	 
								if(data.status==1) {
									$('#myModalReturnSearchOrder').modal('hide');
									location.reload();
								} else {
									$("#myModalReturnSearchOrder #form-error-popup-return").html(data.msg).show();
								} 							
							}	
						});   		
					}
			}
    } 

		function editSearchOrder(orderId='',reason='',condition='',comments='') {
			$('#myModalReturnSearchOrder').modal('show');
			$('#myModalReturnSearchOrder #orderidtextreturn').html(orderId);
			$("#myModalReturnSearchOrder #order_id").val(orderId);
			$("#myModalReturnSearchOrder #returnreason").val(reason);
			$("#myModalReturnSearchOrder #itemcondition").val(condition);
			$("#myModalReturnSearchOrder #comments").val(comments);			
			$("#myModalReturnSearchOrder #reason-error").addClass('hidden');
			$("#myModalReturnSearchOrder #condition-error").addClass('hidden');
			$("#myModalReturnSearchOrder #returnType").val('Edit');
		}

function refundSearchOrder(orderId='',subtotal='',postagecost='',totalcharge='',currency='') {
			$("#myModalSearchRefund #refundTablelist").html('');
            $('#myModalSearchRefund').modal('show');
			$('#myModalSearchRefund #orderidtextrefund').html(orderId); 
			$("#myModalSearchRefund #refund_order_id").val(orderId);
			$("#myModalSearchRefund #product_price").val(subtotal);
			$("#myModalSearchRefund #shipping_cost").val(postagecost);
			$("#myModalSearchRefund #total_price").val(totalcharge);
			$("#myModalSearchRefund #refundoptions").val('');
			$("#myModalSearchRefund #productprice").html(subtotal+' '+currency);
			$("#myModalSearchRefund #shippingcost").html(postagecost+' '+currency);
			$("#myModalSearchRefund #totalprice").html(totalcharge+' '+currency);
			$("#myModalSearchRefund #totalpricehidden").val(totalcharge);
			$("#myModalSearchRefund #custom_refund_amount").val('');
			$("#myModalSearchRefund #refund-error").addClass('hidden');
			$("#myModalSearchRefund #refund-custom-error").addClass('hidden');
			$("#myModalSearchRefund #customrefundblock").addClass('hidden');
			$("#myModalSearchRefund #totalrefundamount").addClass('hidden');
			$("#myModalSearchRefund #customrefundreason").addClass('hidden');
			$("#myModalSearchRefund #refund-custom-reason-error").addClass('hidden');
			$("#myModalSearchRefund #form-error-popup-refund").html('').addClass('hidden');
			$("#myModalSearchRefund #custom_refund_reason").val('');
			$("#myModalSearchRefund #totalrefundtext").html('');
			$("#myModalSearchRefund #custom_refund_amount").val(''); 
			$('#myModalSearchRefund #refundoptions').find("option[value='fullamountoforder']").attr('disabled',false).css({'background': '#ffffff'});
			//get all refund given in the order 
			$.ajax({
					dataType: 'json',
					url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders<?php echo $archive?>/getRefunds',
					type: "POST",				
					cache: false,			
					data : {orderid:orderId,currency:currency},
					success: function(data, textStatus, jqXHR)
					{	  
						$("#myModalSearchRefund #refundTablelist").html(data.refunds);   		
						if(data.showfullamountorderflag==0) {
							$('#myModalSearchRefund #refundoptions').find("option[value='fullamountoforder']").attr('disabled',true).css({'background': '#dddddd'});
						} 			
					}	
			});
		}

		function isNumber(event) {
			if(event.which == 8 || event.which == 0){ 
				return true;
			}

			// prevent if not number/dot
			if(event.which < 46 || event.which > 59) {
				event.preventDefault();
			} 

			// prevent if already dot 
			if(event.which == 46 && event.target.value.indexOf('.') != -1) {
				event.preventDefault();
			} 
    }

		function confirmSearchRefund() {
			$("#myModalSearchRefund #form-error-popup-refund").html('').addClass('hidden');
			var ajaxFlag=true;
			if($("#myModalSearchRefund #refundoptions").val()=='') {
				$("#myModalSearchRefund #refund-error").removeClass('hidden');
				ajaxFlag = false;
			} else if($("#myModalSearchRefund #refundoptions").val()=='additionalamount' || $("#myModalSearchRefund #refundoptions").val()=='productcost' || $("#myModalSearchRefund #refundoptions").val()=='shippingcost' || $("#myModalSearchRefund #refundoptions").val()=='discount') {
        if($("#myModalSearchRefund #custom_refund_amount").is(":visible") && $("#myModalSearchRefund #custom_refund_amount").val()=='') {
					$("#myModalSearchRefund #refund-custom-error").removeClass('hidden')
					ajaxFlag = false;
				} else {
					$("#myModalSearchRefund #refund-custom-error").addClass('hidden')
					ajaxFlag = true;
				}
				if($("#myModalSearchRefund #custom_refund_reason").is(":visible") && $("#myModalSearchRefund #custom_refund_reason").val()=='') {
					$("#myModalSearchRefund #refund-custom-reason-error").removeClass('hidden')
					ajaxFlag = false;
				} else {
					$("#myModalSearchRefund #refund-custom-reason-error").addClass('hidden')
					ajaxFlag = true;
				} 
			} else {
				$("#myModalSearchRefund #refund-error").addClass('hidden');
				ajaxFlag = true; 
			} 
			if(ajaxFlag==true) {
				$.ajax({
					dataType: 'json',
					url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders<?php echo $archive?>/confirmRefund',
					type: "POST",				
					cache: false,			
					data : {orderid:$("#myModalSearchRefund #refund_order_id").val(),customerefundreason:$('#myModalSearchRefund #custom_refund_reason').val(),customrefundamount:$("#myModalSearchRefund #custom_refund_amount").val(),productprice:$("#myModalSearchRefund #product_price").val(),refundoptions:$("#myModalSearchRefund #refundoptions option:selected").val(),shippingcost:$("#myModalSearchRefund #shipping_cost").val(),totalprice:$("#myModalSearchRefund #total_price").val()},
					success: function(data, textStatus, jqXHR)
					{	 
						if(data.status==1) {
							$('#myModalSearchRefund #myModalReturnOrder').modal('hide');
							location.reload();
						} else {
							$("#myModalSearchRefund #form-error-popup-refund").html(data.msg).removeClass('hidden');
						} 							
					}	
				});  
			}
		}

	$("#myModalSearchRefund #refundoptions").on('change',function() {
			$("#myModalSearchRefund #refund-error").addClass('hidden');
			$("#myModalSearchRefund #totalrefundtext").html('');
			$("#myModalSearchRefund #custom_refund_amount").val('');
			$("#myModalSearchRefund #custom_refund_reason").val('');
			$("#myModalSearchRefund #customrefundreason").addClass('hidden');
			if($(this).val()=='additionalamount' || $(this).val()=='productcost' || $(this).val()=='shippingcost' || $(this).val()=='discount') {
        $("#myModalSearchRefund #customrefundblock").removeClass('hidden');
				$("#myModalSearchRefund #customrefundreason").addClass('hidden');
				$("#myModalSearchRefund #totalrefundamount").addClass('hidden');
				$("#myModalSearchRefund #customrefundreason").removeClass('hidden');
				// if($(this).val()=='discount') { //
				// 	$("#customrefundreason").removeClass('hidden');//
				// }//
				if($(this).val()=='additionalamount') {
				  $("#myModalSearchRefund #totalrefundamount").removeClass('hidden');
				}
			} else if($(this).val()=='') {	
				$("#myModalSearchRefund #refund-error").removeClass('hidden'); 
				$("#myModalSearchRefund #customrefundblock").addClass('hidden');	
				$("#myModalSearchRefund #customrefundreason").addClass('hidden');
				$("#myModalSearchRefund #totalrefundamount").addClass('hidden');
			} else {
				$("#myModalSearchRefund #customrefundreason").removeClass('hidden');
			  $("#myModalSearchRefund #customrefundblock").addClass('hidden');
				$("#myModalSearchRefund #totalrefundamount").addClass('hidden');	
			}
			
		});
 
    $("#myModalSearchRefund #custom_refund_amount").on('keyup',function() {
			if($("#myModalSearchRefund #refundoptions").val()=='additionalamount') {
				var totalAdditionalAmount = 0.00;
				totalAdditionalAmount = parseFloat($(this).val())+parseFloat($("#myModalSearchRefund #totalpricehidden").val());
				if(isNaN(totalAdditionalAmount)) {
				  totalAdditionalAmount = 0.00; 
				}
				$("#myModalSearchRefund #totalrefundtext").html('<b>'+totalAdditionalAmount.toFixed(2)+'</b>');
			}
		});

	function resendSearchOrder(mainorderId='',orderId='',source='',subsource='',orderdate='',fullname='',processeddate='',service='',total='',parentid='') {
			$("#myModalSearchResendOrder .msgalert").hide();
			$('#myModalSearchResendOrder').modal('show');
			$("#myModalSearchResendOrder #resend_order_id").val(orderId);
			$("#myModalSearchResendOrder #mainorderId").val(mainorderId);
			$("#myModalSearchResendOrder #parent_order_id").val(parentid);
			$("#myModalSearchResendOrder #orderidtext").html(orderId);
			$("#myModalSearchResendOrder #source").val(source);
			$("#myModalSearchResendOrder #sub_source").val(subsource);
			$("#myModalSearchResendOrder #order_date").val(orderdate);
			$("#myModalSearchResendOrder #full_name").val(fullname); 
			$('#myModalSearchResendOrder #processed_date').val(processeddate); 
			$("#myModalSearchResendOrder #resend-address1-error").addClass('hidden');
			$("#myModalSearchResendOrder #resend-reason-error").addClass('hidden');
			$('#myModalSearchResendOrder #total').val(total);
			$('#myModalSearchResendOrder #service').val(service);
			$("#myModalSearchResendOrder #form-error-popup-resend-error").html('').hide();
			$("#myModalSearchResendOrder #form-error-popup-resend-success").html('').hide();
			$("#myModalSearchResendOrder #town-error").addClass('hidden');
			$("#myModalSearchResendOrder #postcode-error").addClass('hidden');
			$("#myModalSearchResendOrder #country-error").addClass('hidden');
			$("#myModalSearchResendOrder #fullname-error").addClass('hidden');
            $.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders<?php echo $archive?>/getDetails',
				type: "POST",				
				cache: false,			
				data : {orderid:$("#myModalSearchResendOrder #resend_order_id").val()},
				success: function(data, textStatus, jqXHR)
				{	   
					$("#myModalSearchResendOrder #confirmfullordersearchbutton").hide();
					if(data.showConfirmFullOrderButton == 1) {
						$("#myModalSearchResendOrder #confirmfullordersearchbutton").show();
					}
					$('#myModalSearchResendOrder #skulist').html(data.skuslist);
					$('#myModalSearchResendOrder #resend_reason').val(data.resendreason);
					$('#myModalSearchResendOrder #resend_comments').val(data.resendcomments);
					$('#myModalSearchResendOrder #address1').val(data.address1);
					$('#myModalSearchResendOrder #address2').val(data.address2);
					$('#myModalSearchResendOrder #address3').val(data.address3);
					$('#myModalSearchResendOrder #town').val(data.town);
					$('#myModalSearchResendOrder #region').val(data.region);
					$('#myModalSearchResendOrder #postcode').val(data.postcode);
					$('#myModalSearchResendOrder #country').val(data.country);
					$('#myModalSearchResendOrder #fullname').val(data.fullname);
					$('#myModalSearchResendOrder #company').val(data.company);
					$('#myModalSearchResendOrder #phonenumber').val(data.phonenumber);
				}	
			});
		}

		//newchangesstarts
	function resendFullSearchOrder() {
			$.ajax({
					dataType: 'json',
					url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders<?php echo $archive?>/confirmFullOrderResend',
					type: "POST",				
					cache: false,
					data :$("#myModalSearchResendOrder #frmcreatepackage").serialize(), 
					success: function(data, textStatus, jqXHR)
					{	 
						if(data.status==1) {
							$('#myModalSearchResendOrder #myModalResendOrder').modal('hide');
							swal({
								title: "Confirm Resend",
								text: "New Order Id, ( < "+ data.neworderid +" >).",
								type: "success",
								confirmButtonText: "OK",
								allowEscapeKey:false
							},
							function(isConfirm){
								if (isConfirm) {
									location.reload();
								}
							});
						} else if(data.status==2){
							$('#myModalResendSearchOrderStatus #liststatusorder').html(data.msg);
							$('#myModalResendSearchOrderStatus').modal('show');
						} else {
							swal({
									title: "Alert",
									text: data.msg,
									type: "warning",
									confirmButtonText: "OK",
									allowEscapeKey:false
								},
								function(isConfirm){
									// if (isConfirm) {
									// 	location.reload();
									// }
							});
							// $("#myModalSearchResendOrder #form-error-popup-resend-error").html(data.msg).show();
							// setTimeout(function(){ $("#myModalSearchResendOrder .msgalert").hide(); }, 3000);
						} 							
					}	
				});
		}
		function confirmFullOrderSearchResend() {  
			$("#myModalSearchResendOrder .msgalert").hide();
			var ajaxFlag=true;
			var reqfielderror=false; 

			if($("#myModalSearchResendOrder #resend_reason").val()=='') {
				$("#myModalSearchResendOrder #resend-reason-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #resend-reason-error").addClass('hidden'); 
			}

			if($("#myModalSearchResendOrder #address1").val()=='') {
				$("#myModalSearchResendOrder #resend-address1-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #resend-address1-error").addClass('hidden'); 
			}

			if($("#myModalSearchResendOrder #town").val()=='') {
				$("#myModalSearchResendOrder #town-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #town-error").addClass('hidden'); 
			}

			if($("#myModalSearchResendOrder #postcode").val()=='') {
				$("#myModalSearchResendOrder #postcode-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #postcode-error").addClass('hidden'); 
			}

			if($("#myModalSearchResendOrder #postcode").val()=='') {
				$("#myModalSearchResendOrder #postcode-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #postcode-error").addClass('hidden'); 
			}
			if($("#myModalSearchResendOrder #country").val()=='') {
				$("#myModalSearchResendOrder #country-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #country-error").addClass('hidden'); 
			}
			if($("#myModalSearchResendOrder #fullname").val()=='') {
				$("#myModalSearchResendOrder #fullname-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #fullname-error").addClass('hidden'); 
			}
			
			if($("#myModalSearchResendOrder #resend_reason").val()=='' 
			  || $("#myModalSearchResendOrder #address1").val()=='' 
				|| $("#myModalSearchResendOrder #town").val()=='' 
				|| $("#myModalSearchResendOrder #postcode").val()==''
				|| $("#myModalSearchResendOrder #country").val()==''
				|| $("#myModalSearchResendOrder #fullname").val()==''
				|| reqfielderror==true) {
				ajaxFlag = false;
				$("#myModalSearchResendOrder #form-error-popup-resend-error").html('Please fill the required fields.').show();
			} 
  
	    if(ajaxFlag==true) {  

			$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders<?php echo $archive?>/checkInventory',
			type: "POST",				
			cache: false,			
			data : {orderid:$("#mainorderId").val(),'type':'fullorder'},
			success: function(sdata, textStatus, jqXHR)
			{	    
				if(sdata.status == 0) { 
					resendFullSearchOrder();
				} else {
				   //show warning
                   $("#myModalSearchWarning #stockTablelist").html(sdata.msg)
				   $('#myModalSearchWarning').modal('show');
				   $("#myModalSearchWarning #continueresend").unbind('click');
				   $("#myModalSearchWarning #continueresend").bind( "click", function() {
				    $('#myModalSearchWarning').modal('hide'); 
					resendFullSearchOrder();
				   });
				}
			}	
			});  

		}
		}

		function resendSearchSingle(savetype='') {
			 
			$.ajax({
					dataType: 'json',
					url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders<?php echo $archive?>/saveTemp',
					type: "POST",				
					cache: false,
					data :$("#myModalSearchResendOrder #frmcreatepackage").serialize(), 
					success: function(data, textStatus, jqXHR)
					{	  
						if(data.status==1) {
							$("#myModalSearchResendOrder #form-error-popup-resend-success").html(data.msg).show();  
							$("#myModalSearchResendOrder #neworderid").val(data.neworderid);							
							//timing the alert box to close after 3 seconds 
							if(savetype=='1') {
								confirmSearchResend();
							}
							setTimeout(function(){ $("#myModalSearchResendOrder .msgalert").hide(); }, 3000);
						} else {
							$("#myModalSearchResendOrder #form-error-popup-resend-error").html(data.msg).show();
						} 
					}	
				});
		 }

		function saveSearchTemp(savetype='0') { 

			$("#myModalSearchResendOrder .msgalert").hide();
			$("#myModalSearchResendOrder #form-error-popup-resend-error").html('Please fill the required fields.').show();
			var ajaxFlag=true;
			var reqfielderror=false;
             $('#myModalSearchResendOrder .reqerror').remove();
			$("#myModalSearchResendOrder .requiredfield").each(function() { 
				if($(this).val()=='' && $(this).is(':disabled')==false) {
					$(this).parent('td').append('<span class="text-danger reqerror" >Please enter a value!</span>');
					reqfielderror = true;
				} 
			});
			if(savetype=='1') { //validate only if user clicked confirm
				if($("#myModalSearchResendOrder #resend_reason").val()=='') {
					$("#myModalSearchResendOrder #resend-reason-error").removeClass('hidden'); 
				} else {
					$("#myModalSearchResendOrder #resend-reason-error").addClass('hidden'); 
				}
			}

			if($("#myModalSearchResendOrder #address1").val()=='') {
				$("#myModalSearchResendOrder #resend-address1-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #resend-address1-error").addClass('hidden'); 
			}

			if($("#myModalSearchResendOrder #town").val()=='') {
				$("#myModalSearchResendOrder #town-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #town-error").addClass('hidden'); 
			}

			if($("#myModalSearchResendOrder #postcode").val()=='') {
				$("#myModalSearchResendOrder #postcode-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #postcode-error").addClass('hidden'); 
			}

			if($("#myModalSearchResendOrder #postcode").val()=='') {
				$("#myModalSearchResendOrder #postcode-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #postcode-error").addClass('hidden'); 
			}
			if($("#myModalSearchResendOrder #country").val()=='') {
				$("#myModalSearchResendOrder #country-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #country-error").addClass('hidden'); 
			}
			if($("#myModalSearchResendOrder #fullname").val()=='') {
				$("#myModalSearchResendOrder #fullname-error").removeClass('hidden'); 
			} else {
				$("#myModalSearchResendOrder #fullname-error").addClass('hidden'); 
			}
			
			if($("#myModalSearchResendOrder #address1").val()=='' 
				|| $("#myModalSearchResendOrder #town").val()=='' 
				|| $("#myModalSearchResendOrder #postcode").val()==''
				|| $("#myModalSearchResendOrder #country").val()==''
				|| $("#myModalSearchResendOrder #fullname").val()==''
				|| reqfielderror==true) {
				ajaxFlag = false;
			}
  
			if($("#myModalSearchResendOrder #resend_reason").val()=='' && savetype=='1'){
				ajaxFlag = false;
			}
 
	        if(ajaxFlag==true) {
				$.ajax({
					dataType: 'json',
					url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders<?php echo $archive?>/checkInventory',
					type: "POST",				
					cache: false,			
					data : $("#myModalSearchResendOrder #frmcreatepackage").serialize(),
					success: function(sdata, textStatus, jqXHR)
					{	    
						if(sdata.status == 0) { 
							resendSearchSingle(savetype);
						} else {
						//show warning
						$("#myModalSearchWarning #stockTablelist").html(sdata.msg)
						$('#myModalSearchWarning').modal('show');
						$("#myModalSearchWarning #continueresend").unbind('click');
						$("#myModalSearchWarning #continueresend").bind( "click", function() {
							$('#myModalSearchWarning #myModalWarning').modal('hide'); 
							resendSearchSingle(savetype);
						});
						}
					}	
				}); 
			}
		}

		function confirmSearchResend() {
			$("#myModalSearchResendOrder .msgalert").hide();  
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders<?php echo $archive?>/confirmResend',
				type: "POST",
				cache: false,
				data :$("#myModalSearchResendOrder #frmcreatepackage").serialize(), 
				success: function(data, textStatus, jqXHR)
				{	 
					if(data.status==1) {
						$('#myModalSearchResendOrder #myModalResendOrder').modal('hide'); 
						swal({
							title: "Confirm Resend",
							text: "New Order Id, ( < "+ data.neworderid +" >).",
							type: "success",
							confirmButtonText: "OK",
							allowEscapeKey:false
						},
						function(isConfirm){
							if (isConfirm) {
								location.reload();
							}
						});
					} else {
						 	swal({
							title: "Alert",
							text: data.msg,
							type: "warning",
							confirmButtonText: "OK",
							allowEscapeKey:false
						},
						function(isConfirm){
							// if (isConfirm) {
							// 	location.reload();
							// }
						});
					} 							
				}	
			});
		} 
		//changes by amit rawat

        // edit address code starts 
		function editAddress(orderId='') {
			$("#myModalEditOrderAddress .msgalert").hide();
			$('#myModalEditOrderAddress').modal('show'); 
			$("#myModalEditOrderAddress #addressorderidtext").html(orderId);
			$("#myModalEditOrderAddress #address_order_id").val(orderId);
			$("#myModalEditOrderAddress #resend-address1-error").addClass('hidden');  
			$("#myModalEditOrderAddress #form-error-popup-address-error").html('').hide(); 
			$("#myModalEditOrderAddress #town-error").addClass('hidden');
			$("#myModalEditOrderAddress #postcode-error").addClass('hidden');
			$("#myModalEditOrderAddress #country-error").addClass('hidden');
			$("#myModalEditOrderAddress #fullname-error").addClass('hidden');
            $.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders<?php echo $archive?>/getAddressDetails',
				type: "POST",				
				cache: false,			
				data : {orderid:$("#myModalEditOrderAddress #address_order_id").val()},
				success: function(data, textStatus, jqXHR)
				{	    
					$('#myModalEditOrderAddress #address1').val(data.address1);
					$('#myModalEditOrderAddress #address2').val(data.address2);
					$('#myModalEditOrderAddress #address3').val(data.address3);
					$('#myModalEditOrderAddress #town').val(data.town);
					$('#myModalEditOrderAddress #region').val(data.region);
					$('#myModalEditOrderAddress #postcode').val(data.postcode);
					$('#myModalEditOrderAddress #country').val(data.country);   
				}	
			});
		} 

		function updateAddress() {
			$("#myModalEditOrderAddress .msgalert").hide();
			var ajaxFlag=true;
			var reqfielderror=false; 

			if($("#myModalEditOrderAddress #resend_reason").val()=='') {
				$("#myModalEditOrderAddress #resend-reason-error").removeClass('hidden'); 
			} else {
				$("#myModalEditOrderAddress #resend-reason-error").addClass('hidden'); 
			}

			if($("#myModalEditOrderAddress #address1").val()=='') {
				$("#myModalEditOrderAddress #resend-address1-error").removeClass('hidden'); 
			} else {
				$("#myModalEditOrderAddress #resend-address1-error").addClass('hidden'); 
			}

			if($("#myModalEditOrderAddress #town").val()=='') {
				$("#myModalEditOrderAddress #town-error").removeClass('hidden'); 
			} else {
				$("#myModalEditOrderAddress #town-error").addClass('hidden'); 
			}

			if($("#myModalEditOrderAddress #postcode").val()=='') {
				$("#myModalEditOrderAddress #postcode-error").removeClass('hidden'); 
			} else {
				$("#myModalEditOrderAddress #postcode-error").addClass('hidden'); 
			}

			if($("#myModalEditOrderAddress #postcode").val()=='') {
				$("#myModalEditOrderAddress #postcode-error").removeClass('hidden'); 
			} else {
				$("#myModalEditOrderAddress #postcode-error").addClass('hidden'); 
			}
			if($("#myModalEditOrderAddress #country").val()=='') {
				$("#myModalEditOrderAddress #country-error").removeClass('hidden'); 
			} else {
				$("#myModalEditOrderAddress #country-error").addClass('hidden'); 
			}
			if($("#myModalEditOrderAddress #fullname").val()=='') {
				$("#myModalEditOrderAddress #fullname-error").removeClass('hidden'); 
			} else {
				$("#myModalEditOrderAddress #fullname-error").addClass('hidden'); 
			}
			
			if($("#myModalEditOrderAddress #resend_reason").val()=='' 
			  || $("#myModalEditOrderAddress #address1").val()=='' 
				|| $("#myModalEditOrderAddress #town").val()=='' 
				|| $("#myModalEditOrderAddress #postcode").val()==''
				|| $("#myModalEditOrderAddress #country").val()==''
				|| $("#myModalEditOrderAddress #fullname").val()==''
				|| reqfielderror==true) {
				ajaxFlag = false;
				$("#myModalEditOrderAddress #form-error-popup-address-error").html('Please fill the required fields.').show();
			} 
  
			if(ajaxFlag==true) {  

				$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders<?php echo $archive?>/updateAddress',
				type: "POST",
				cache: false,
				data :$("#myModalEditOrderAddress #frmaddresschange").serialize(), 
				success: function(data, textStatus, jqXHR)
				{	 
					if(data.status==1) {
						$('#myModalEditOrderAddress').modal('hide'); 
						swal({
							title: "Confirm Resend",
							text: "Address updated successfully!",
							type: "success",
							confirmButtonText: "OK",
							allowEscapeKey:false
						},
						function(isConfirm){
							//reload search order modal
							var searchKey = $("#myModalEditOrderAddress #address_order_id").val();
							$.ajax({
								url     	: '/Linnworksapis/searchAnyOrderInList',
								type    	: 'POST',
								data    	: {  searchKey : searchKey },
								beforeSend	 : function() {
								$( ".outerOpac" ).attr( 'style' , 'display:block');
								},   			  
								success 	:	function( data  )
								{
									if( data == 1 )
									{
										$( ".outerOpac" ).attr( 'style' , 'display:none');
										swal( "Oops!!!, ( < "+ searchKey +" >) not found here.");
									}
									else
									{
										$( ".outerOpac" ).attr( 'style' , 'display:none');
										$('.showOPenOrderGlobalSearchResult').html( data );
									}
								}                
							});
						});
					} else {
						$("#myModalEditOrderAddress #form-error-popup-address-error").html('Unable to update address.').show(); 
					} 							
				}	
			}); 
			}
		}
		// edit address code ends
</script>

