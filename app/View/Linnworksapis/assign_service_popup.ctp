<?php if( isset($popupOrders) && count($popupOrders > 0) ) { foreach($popupOrders as $result) { ?>
<div class="modal fade pop-up-4 servicePop-<?php echo $result->MergeUpdate->id; ?>" tabindex="-1" role="dialog" id="pop-up-4" aria-labelledby="myLargeModalLabel-4" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Order Id ( <?php echo $result->MergeUpdate->product_order_id_identify; ?> )</h4>
         </div>
         <div class="modal-body bg-grey-100">
            <div class="panel no-margin" style="clear:both">
     <div class="panel-body padding-top-20" >
     <div class="row">
	                    <div class="col-lg-12">
							 <form accept-charset="utf-8" method="post" enctype="multipart/form-data" class="form-horizontal bv-form" action="">
								<div class="form-group">
                                        <label class="control-label col-lg-3" for="username">Service Id</label>                                        
                                        <div class="col-lg-7">                                            
                                            <input type="text" id="postalService" value="<?php print $result->MergeUpdate->service_id; ?>" class="form-control" name="service_id">
                                            <input type="hidden" id="id"  value="<?php print $result->MergeUpdate->id; ?>" name="id">
                                        </div>
                                      </div>
                                      </form>
                        </div>
                       <div class="row">
         <div class="col-lg-12" style="text-align:center; padding-bottom: 7px;"> 
         <a href="javascript:void(0);" for="<?php print $result->MergeUpdate->id; ?>" class="assignCustomPostal btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;">Assign Service</a>
         </div>
        </div>
       </div>
      </div> 
     </div>
    </div>
   </div>
  </div>
 </div>
<?php } } ?>


<script>
   $(document).ready(function()
   {
       $('#pop-up-4').modal('show');
   });
</script>

