
<div class="modal fade pop-up-4 servicePop-<?php echo $splitOrderId; ?>" tabindex="-1" role="dialog" id="pop-up-4service" aria-labelledby="myLargeModalLabel-4" aria-hidden="true" style="z-index:9999999" >
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Order Id ( <?php echo $splitOrderId; ?> )</h4>
         </div>
         <div class="modal-body bg-grey-100">
            <div class="panel no-margin" style="clear:both">
     <div class="panel-body padding-top-20" >
     <div class="row">
	                    <div class="col-lg-12">
							 <form accept-charset="utf-8" method="post" enctype="multipart/form-data" class="form-horizontal bv-form" action="">
								<div class="form-group">
									    <label class="control-label col-lg-3" for="username">Packaging Slip Id </label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                            		if( count( $popupOrders ) > 0 )
													print $this->form->input( 'Store.store_name', array( 'type'=>'select', 'empty'=>'Choose Store','options'=>$popupOrders,'class'=>'form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
												?>
                                        </div>
                                        <input type="hidden" value="<?php echo $mergeUpdateOrderId; ?>" id="mergeupdateId"/>
                                      </div>
                                      
								<div class="form-group addlabel">
									    <label class="control-label col-lg-3" for="username">Delevery label </label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
													if( count( $getTemplate ) > 0 )
													print $this->form->input( 'Template.store_id', array( 'type'=>'select', 'empty'=>'Choose Store','options'=>$getTemplate,'class'=>'form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
												?>
                                        </div>
                                      </div>
                                      </form>
                        </div>
                       <div class="row">
         <div class="col-lg-12" style="text-align:center; padding-bottom: 7px;"> 
         <a href="javascript:void(0);" for="<?php print $splitOrderId; ?>" class="CustomSlip btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;">Assign Packaging Slip  </a>
         </div>
        </div>
       </div>
      </div> 
     </div>
    </div>
   </div>
  </div>
 </div>

<script>
   $(document).ready(function()
   {
       $('#pop-up-4service').modal('show');
   });
   
   $('body').on('change', '#StoreStoreName', function(){
	   
	   var storeId = $( '#StoreStoreName' ).val();
	   	   $.ajax({
							url     	: '/Linnworksapis/getdeleveryLabel',
							type    	: 'POST',
							data    	: {  storeId : storeId },  			  
							success 	:	function( data  )
							{
								 $('.addlabel').html( data );
							}                
						});	
	   
	   });
</script>

