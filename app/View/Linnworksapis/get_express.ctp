<?php
$userID = $this->session->read('Auth.User.id');
?>
<div class="top_panel rightside bg-grey-100" style="display:none;">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php echo "Open Order"; ?></h1>
        		<div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
							<li>
									<?php
										echo  $this->form->create('Linnworksapis', array( 'class'=>'form-horizontal bv-form', 'type'=>'post','id'=>'getFilterOrderCsv','enctype'=>'multipart/form-data', 'action' => 'generatePickList'));
										echo  $this->form->hidden('Linnworksapis.orderid', array('class' => 'get_sku_string' ));
										//echo  $this->Form->button('Pick List', array('type' => 'submit', 'escape' => true,'class'=>'btn btn-success'));
										echo  $this->form->end();
									?>
							</li>
							<!--<li>
								<a class="downloadPickList btn btn-success btn-xs margin-right-10 color-white" href="javascript:void(0)" ><i class="fa fa-download"></i> Pick List</a>
							</li>-->
							<li>
								<a class="downloadStockFile btn btn-success btn-xs margin-right-10 color-white"><i class="fa fa-download"></i> Download CSV</a>
							</li>
							<li>
								<a class="switch btn btn-success" href="javascript:void(0)" data-toggle="modal" data-target=""><i class=""></i> Switch </a>
							</li>
							<li>
								<a href="/linnworksapis" type="submit" class="printorder btn bg-orange-500 color-white btn-dark padding-left-40 padding-right-40" >Go Back</a>
							</li>
						</ul>
					</div>
				</div>
				
			<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			<div class="api_message" style="display:none; color:green; padding 10px 0 0 10px;"></div>
    </div>
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">									
									<div class="panel-tools">																	
									</div>
								</div>
							   <div class="panel-body no-padding-top bg-white">
							    		<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
										<thead class="parentCheck" >
											<tr role="row" class="parentInner" >
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand: activate to sort column descending">Order no.</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand: activate to sort column descending">SKU</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand: activate to sort column descending">Currency</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand: activate to sort column descending">Total</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand: activate to sort column descending">Action</th>
											</tr>
										</thead>
										<tbody role="alert" aria-live="polite" aria-relevant="all">
                                            <?php 
												App::import('Controller', 'Linnworksapis');
												$checkproduct =	new LinnworksapisController;
												$qty	=	$checkproduct->checkInventery();
												$getSumOrder	=	$checkproduct->getStoreOrderCount();
											?>
                                            <?php $i = 1;
												foreach($results as $result)
													{
														$id	=	$result->OrderId;
                                            ?>
                                            <tr class="odd">
												<td class="  sorting_1">
												<!--<input type="hidden" class ="case" name="skutouploadID" id="<?php echo $result->NumOrderId; ?>" onclick="get_sku();" >-->
												<?php echo $result->NumOrderId; ?>
												</td>
												<td class="sorting_1 pick_sku" >
													<?php 
														 foreach($result->Items as $item)
														  { 
														   //echo "<b>".$item->MergeUpdate->quantity."</b> X&nbsp; ".$item->MergeUpdate->sku ."</br>";
                                                                                                                   echo $item->MergeUpdate->sku ."</br>";  
														   echo "<input type=hidden class=get_hidden_sku id=" .$item->MergeUpdate->sku ."  />";
														 ?>
														 	<input type="hidden" class ="case" name="skutouploadID" id="<?php echo $item->MergeUpdate->product_order_id_identify; ?>" > 
														 
													
													<?php   
														}
													?>
												</td>
												<td class="  sorting_1"><?php echo $result->TotalsInfo->Currency; ?></td>
												<td class="  sorting_1"><?php echo $result->TotalsInfo->TotalCharge; ?></td>
												<td class="  sorting_1">
													<ul id="icons" class="iconButtons">
														<input type="hidden" value="<?php echo $id; ?>" class="processid" />
														<a href="/Linnworksapis/getOrderDetail/<?php echo $result->NumOrderId; ?>" title="Show Order Description" class="btn btn-success btn-xs margin-right-10"><i class="ion-android-desktop"></i></a>
														<!--<a href="javascript:void(0);" for="<?php echo $id; ?>##$#<?php echo "00000000-0000-0000-0000-000000000000"; ?>##$#<?php echo $result->TotalsInfo->TotalCharge; ?>" title="Cancel Order" class="ordercancel btn btn-danger btn-xs margin-right-10" ><i class="ion-minus-round"></i></a>
														<a class="loading-image_<?php echo $id; ?>" style="display:none;"><img src="http://localhost/app/webroot/img/image_889915.gif" hight ="20" width="20" /></a>-->
															<?php 	
															$instock = '0';
															$outstock = '0';
															foreach($result->Items as $item)
																{ 
																	$qty	=	$checkproduct->checkInventery( $item->MergeUpdate->sku );
																	if($qty == 1)
																	{
																		$instock++;
																	}
																	elseif($qty == 2)
																	{
																		$outstock++;
																	}
																} ?>
															
															<?php if($instock > 0 && $outstock == 0) { ?>
																 <!--<a href="javascript:void(0);" for="" title="In Stock" class="btn btn-success btn-xs margin-right-10" ><i class="fa fa-sign-in"></i></a>-->
															<?php }	elseif($outstock > 0 && $instock == 0) {  ?>
																 <!--<a href="javascript:void(0);" for="" title="Out Stock" class="btn btn-danger btn-xs margin-right-10" ><i class="fa fa-sign-out"></i></a>-->
															<?php }	elseif($instock > 0 && $outstock > 0) { ?>
																 <!--<a href="javascript:void(0);" for="" title="Out Stock" class="btn palette-sun-flower btn-xs margin-right-10" ><i class="fa fa-sign-in"></i></a>-->
															<?php }  ?>
															
													</ul>
												</td>
										    </tr>
                                            <?php $i++; } ?>
                                      </tbody>
									</table>		
								</div>
                            </div>
                        </div>
                    </div>
					<?php echo $this->element('footer'); ?>
            </div>
    </div>

<!------------------------------------ Open Order -------------------------------------------------------------->

<!------------------------------------ Start for arrange order ------------------------------------------------->
<div class="bottom_panel rightside bg-grey-100" style="display:block;" >
    <div class="page-head bg-grey-100">
		 <h1 class="page-title"><?php echo "Open Order";
					     ?></h1>
				<div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
							<li>
							<?php
								echo  $this->form->create('Linnworksapis', array( 'class'=>'form-horizontal bv-form', 'type'=>'post','id'=>'getFilterOrderCsv','enctype'=>'multipart/form-data', 'action' => 'generatePickList'));
								echo  $this->form->hidden('Linnworksapis.orderid', array('class' => 'get_sku_string' ));
								//echo  $this->Form->button('Pick List', array('type' => 'submit', 'escape' => true,'class'=>'btn btn-success'));
								echo  $this->form->end();
							?>
							</li>
							
							<li>
								<a class="btn btn-xs btn-success" href="/Linnworksapis/GetExpress" data-toggle="modal" data-target=""><i class=""></i> Express </a>
							</li>
							
							<li>
							<?php 
							
							$userIds		=	explode(',', $getModuleRole['Picklist']);
							$module			=	key($getModuleRole);
							if($module == 'Picklist' && in_array( $userID, $userIds))
							{
							
							?>
										<div class="btn-group">
										 <button type="button"  href="javascript:void(0);" class="downloadPickList btn btn-xs btn-success no-margin">Pick List</button>
										  <button type="button" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										  </button>
										  <?php if(count($files) > 0 ) { ?>
										  <ul class="dropdown-menu scrollable-menu" role="menu">
										  <?php 
											$pdfPath = '/img/printPickList/'; 
											foreach( $files as $file ) {
												$time = explode('_', $file) ;
												
												$minute = explode('.', $time[7]) ;
												$picklistDate	=	 $time[3].'-'.$time[4].'-'.$time[5];
											?>
											<li><a href="<?php echo $pdfPath.$file; ?>" target ="_blank" >Pick List <b>[ <?php echo $picklistDate.' '.$time[6].' : '.$minute[0] ?> ]</b></a></li>
											<?php } ?>
											<li role="separator"></li>
										  </ul>
										  <?php } ?>
										</div>
										<?php } ?>
							</li>
							<!--<li>
								<a class="downloadPickList btn btn-success btn-xs margin-right-10 color-white" href="javascript:void(0)" ><i class="fa fa-download"></i> Pick List</a>
							</li>-->
							<li>
								<a class="btn btn-xs btn-success" href="/JijGroup/Generic/Order/GetOpenFilter" data-toggle="modal" data-target=""><i class=""></i> Refresh </a>
							</li>
							<li>
								<a class="btn btn-xs btn-success" href="/cronjobs/getBarcodeOutside" data-toggle="modal" data-target=""><i class=""></i> Custom Barcode </a>
							</li>
							<li>
								<a class="btn btn-xs btn-success" href="/System/Custom/Update/__Quantity__/Orders" data-toggle="modal" data-target=""><i class=""></i> Update Order Quantity </a>
							</li>
							
							<!--<li>
								<a class="btn btn-success" href="/cronjobs/getBarcodeOutside" data-toggle="modal" data-target=""><i class=""></i> Update Order Quantity </a>
							</li>-->
							
							<li> 
								<a class="downloadStockFile_prevent btn btn-xs btn-success" href="/img/stockUpdate//virtualStock.csv" data-toggle="modal" data-target=""><i class=""></i> Virtual Stock </a>
							</li>
							<!--<li>
								<a class="switch btn btn-success" href="javascript:void(0)" data-toggle="modal" data-target=""><i class=""></i> Switch </a>
							</li>-->
							<li>
								<a class="btn btn-xs btn-success" href="/linnworksapis/dispatchConsole" data-toggle="modal" data-target="">Despatch Console</a>
							</li>
							
							<li>
								<div class="btn-group">
								  <button type="button"  href="#" class="btn btn-xs btn-success no-margin">Orders</button>
								  <button type="button" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="caret"></span>
									<span class="sr-only">Toggle Dropdown</span>
								  </button>
								  <ul class="dropdown-menu scrollable-menu" role="menu">
								  <table style="border: 1px solid #c3c3c3;" >
								  <?php 
											
											foreach( $getSumOrder as $value ) {
									?>
									<tr>
										<td ><?php echo $value['OpenOrder']['sub_source'];?></td>
										<td style="text-align: left;" > <b><?php echo $value[0]['Quantity']; ?></b></td>
									</tr>
									<?php } ?>
									
									</table>
								  </ul>
								  
								</div>
							</li>
								
							<li>
								<a href="/linnworksapis" type="submit" class="printorder btn btn-xs bg-orange-500 color-white btn-dark" >Go Back</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="panel-title no-radius bg-green-500 color-white no-border">
					<div class="panel-head"><?php print $this->Session->flash(); ?></div>
				</div>
    </div>
   
    <div class="container-fluid" >
        <div class="row">
		
		    <div class="col-lg-12 rackDetails">
				
              <div class="panel">
					<div class="" style="margin:0 auto; width:350px; padding-top:10px">
						 <ul class="pagination">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>
					  </div>
					  <div class="search-wrapper">
					  <div class="input-holder">
					   <?php
							//print $this->form->input( 'OpenOrder.searchtext', array( 'placeholder'=>'Search Open Order','type'=>'text','class'=>'search-input searchtext','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
						?>
                    <!--<button class="search-icon" onclick="searchToggle(this, event);"><span></span></button>-->
                </div>
                <!--<span class="close" onclick="searchToggle(this, event);"></span>
                <div class="result-container">

                </div>-->
            </div>
            <div style="display:none; background: #ffffff none repeat scroll 0 0;    border: 1px solid #f2f2f2;    padding: 3px;    position: fixed;    right: 50px;    top: 160px;    width: 200px;    z-index: 999;">
								<?php
									print $this->form->input( 'OpenOrder.searchtext', array( 'placeholder'=>'Search Open Order','type'=>'text','class'=>'form-control searchtext','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
								?>
					</div>
					<div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12 ">
						<section id="blur-in">
          <div class="animated-tab">
            <article class="tab-container">
              <nav class="tab-nav">
                <li><a href="#group1" title="Tab 1">Group 1</a></li>
                <li><a href="#group2" title="Tab 2">Group 2</a></li>
				<li><a href="#group3" title="Tab 3">Express</a></li>
				
              </nav>
              <div class="tab-pane-container">
                <section id="group1">
						
						<div class="overlay">&nbsp;</div>
							<table class="table table-bordered table-striped dataTable">
								<tr>
									<th width="9%">OrderID</th>
									<th width="9%">Days</th>
									<th width="9%">Store</th>
									<th width="9%">Postal</th>
									<th width="10%">SKU</th>
									<th width="8%">Bin Location</th>
									<th width="20%">Title</th>
									<th width="6%">Total (EUR)</th>
									<th width="5%">Total </th>
									<th width="5%">Cur.</th>
									<th width="7%">Date</th>
									<th width="7%">Move Date</th>
									<!--<th width="21%">Action</th>-->
								</tr>
								
								<!-- For Express with single SKU Group A -->
								<?php 
								App::import('controller', 'Currents');
								$currentObj	=	new CurrentsController();
								
								App::import('controller', 'Linnworksapis');
								$linnworkObj	=	new LinnworksapisController();
								
								App::import( "Component","Extra" );
								$extraComponent = new ExtraComponent(new ComponentCollection());   
								
								$unwanted	=	Configure::read('unwanted');
																
								?>
								
								<!-- For Tracked with single SKU Group A -->
								<?php 
								foreach($tracked1 as $trackedOne)
								{
									
									$expressDate=$trackedOne->GeneralInfo->ReceivedDate;
									$trackedDate=explode('T', $expressDate);
									
									$getTime = $extraComponent->getResponseTime( $trackedDate[0] );
									$calculatDays = explode(' ' , $getTime);

									if( $calculatDays[1] == 'days' || $calculatDays[1] == 'months' || $calculatDays[1] == 'years' )
									{
										if( $calculatDays[0] >= 2 )
										{
											//$timeCalculator = "alert-danger";
											$timeCalculator = " color:#a94442";
										}
										else
										{
											$timeCalculator = "";
										}
									}
									else
									{
										$timeCalculator = "";
									}
								?> 
									<?php
									foreach($trackedOne->Items as $item):
									$titles = $linnworkObj->getTitle( $item->MergeUpdate->sku );
									$pickListStatus	=	$checkproduct->checkPickList( $item->MergeUpdate->product_order_id_identify );
									if( $pickListStatus == 1)
									{
										$backgroud = "bg-orange-100";
									}
									else
									{
										$backgroud = "";
									}
									?>
										<tr style="border-bottom: 1px solid #ccc; <?php echo $timeCalculator; ?>" class ="<?php echo $backgroud; ?>" >									
											<td>
											<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_id; ?></div></td><td class='row'><div class='col-sm-6'><b>Postal Service</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Warehouse</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->warehouse; ?></div></td><td class='row'><div class='col-sm-6'><b>Delivery Country</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->delevery_country; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Delivery Label</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Service Provider</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Provider Code</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->provider_ref_code; ?></div></td><td class='row'><div class='col-sm-6'><b>Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_weight; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Length</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_length; ?></div></td><td class='row'><div class='col-sm-6'><b>Width</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Height</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_height; ?></div></td><td class='row'><div class='col-sm-6'><b>Packaging Slip</b></div><div class='col-sm-6'><?php echo $item->PackagingSlip->name; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b>Service Name</b></div><div class='col-sm-11'><?php echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'><table><tr><td width='25%' class='row'><div class='col-sm-6'><b>Packaging ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_id; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_weight; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Cost</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_cost; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Class</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packaging_type; ?></div></td></tr>
											<tr><td colspan='4' width='100%' class='row'><div class='col-sm-2'><b>Packaging Name</b></div><div class='col-sm-10'><?php echo $item->MergeUpdate->envelope_name; ?></div></td></tr></table></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
												</a>
												<?php if( $item->MergeUpdate->out_sell_marker  == 3) { ?>
													<i class="fa fa-warning"></i>
												<?php } ?>
											<a class="moreDetails" href="#" >
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														echo $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														echo $item->MergeUpdate->order_id;
													}
												?>
												</a>
												<?php 	
											if( in_array($item->MergeUpdate->order_id, $oversell_items)){
												echo "<br>Over Sell";
											}
											?>
											</td>
											<td><?php echo $calculatDays[0].' '.$calculatDays[1]; ?></td>
											<td><?php printf( "%s", $trackedOne->GeneralInfo->SubSource ); // $item->MergeUpdate->postal_service; ?></td>
											<td><?php echo $trackedOne->ShippingInfo->PostalServiceName; ?></td>								
											<td>
												<?php
													//pack_order_quantity
													//pr($standardOne->Items); exit;
													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($trackedOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($trackedOne->Items) == 1 )
															{
																echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															}
															else
															{
																echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															}
														}
													}
													$mer_barcodes = explode(',',$item->MergeUpdate->barcode);
													foreach( $mer_barcodes as $mer_barcode )
													{
														print "<br>".$this->Common->getLatestBarcode( $mer_barcode );
													}
													//echo '<br />'. str_replace( ',','<br />',$item->MergeUpdate->barcode );
												?>
											</td>
											<td>
											<?php
												$objectSkuLocations = $this->Common->getSkuBinLocation( $item->MergeUpdate->barcode );	
												foreach( $objectSkuLocations as $objectSkuLocationsIndex => $objectSkuLocationsValues)
												{
													echo $objectSkuLocationsValues['binLocation']."<br>";
												}
											?>
											
											</td>
											<td class="productTitle">
											<?php foreach($titles as $title) {  ?>
											<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>
											
											
											</td>
											<td>
												<?php 
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($trackedOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $trackedOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $trackedOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $trackedOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $trackedOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($trackedOne->Items) == 1 )
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price , $trackedOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $trackedOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
															else
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $trackedOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $trackedOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
														}
													}
												?>
											</td>
											<td>
											<?php
												//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
												if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
												{
													if( count($trackedOne->Items) == 1 )
													{
														echo $item->MergeUpdate->price;
													}
													else
													{
														echo $item->MergeUpdate->price;
													}
												}
												else
												{
													if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($trackedOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
												}										
											?>
											</td>
											<td>
											<?php echo $trackedOne->TotalsInfo->Currency; ?>
											</td>
											<td>
											<?php 
												$expressDate=$trackedOne->GeneralInfo->ReceivedDate;
												$trackedDate=explode('T', $expressDate);
												echo $trackedDate[0]."<br>(".$trackedDate[1].")";
											?>
											</td>
											<td><?php echo $item->MergeUpdate->order_date; ?></td>
										<!--<td>
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														$ord_id = $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														$ord_id = $item->MergeUpdate->order_id;
													} ?>
												<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".pop-up-<?php print $ord_id; ?>">
													<i class="ion-clipboard"></i>
												</a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->order_id; ?>" class="calcel_order btn btn-xs btn-danger" href="javascript:void(0); " title = "Cancel Order" data-toggle="modal" data-target=".cancel_pop-up-<?php echo $trackedOne->OrderId; ?>" >Cancel Order</a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->product_order_id_identify; ?>" class="assign_custom_service for_tooltip btn btn-xs btn-success" href="javaactipt:void(0)" title = "Assign Custom Service" data-toggle="modal" >Service Assign</a>
											</td>-->
										</tr>
								<?php
								endforeach;	
								?>
								<?php } ?>
								
								<!-- For Standard with single SKU Group A  -->
								<?php 
								foreach($standerd1 as $standardOne) 
								{
									
									$expressDate=$standardOne->GeneralInfo->ReceivedDate;
									$trackedDate=explode('T', $expressDate);
									
									$getTime = $extraComponent->getResponseTime( $trackedDate[0] );
									$calculatDays = explode(' ' , $getTime);

									if( $calculatDays[1] == 'days' || $calculatDays[1] == 'months' || $calculatDays[1] == 'years' )
									{
										if( $calculatDays[0] >= 2 )
										{
											//$timeCalculator = "alert-danger";
											$timeCalculator = " color:#a94442";
										}
										else
										{
											$timeCalculator = "";
										}
									}
									else
									{
										$timeCalculator = "";
									}
									
								
									foreach($standardOne->Items as $item):
									
									//condition for investigate either Direct or Amazon should not.
										if( ($standardOne->sub_source == 'DIRECT' || $standardOne->sub_source == 'AMAZON' ) || $standardOne->sub_source == ''  )
										{
											$assignStore = true;
										}
										else
										{
											$assignStore = false;
										}
									
									$titles = $linnworkObj->getTitle( $item->MergeUpdate->sku );
									$pickListStatus	=	$checkproduct->checkPickList( $item->MergeUpdate->product_order_id_identify );
									if( $pickListStatus == 1)
									{
										$backgroud = "bg-orange-100";
									}
									else
									{
										$backgroud = "";
									}
									?>
										<tr style="border-bottom: 1px solid #ccc; <?php echo $timeCalculator; ?>" class ="<?php echo $backgroud; ?>" >									
											<td>
											<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_id; ?></div></td><td class='row'><div class='col-sm-6'><b>Postal Service</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Warehouse</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->warehouse; ?></div></td><td class='row'><div class='col-sm-6'><b>Delivery Country</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->delevery_country; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Delivery Label</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Service Provider</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Provider Code</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->provider_ref_code; ?></div></td><td class='row'><div class='col-sm-6'><b>Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_weight; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Length</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_length; ?></div></td><td class='row'><div class='col-sm-6'><b>Width</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Height</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_height; ?></div></td><td class='row'><div class='col-sm-6'><b>Packaging Slip</b></div><div class='col-sm-6'><?php echo $item->PackagingSlip->name; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b>Service Name</b></div><div class='col-sm-11'><?php echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'><table><tr><td width='25%' class='row'><div class='col-sm-6'><b>Packaging ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_id; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_weight; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Cost</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_cost; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Class</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packaging_type; ?></div></td></tr>
											<tr><td colspan='4' width='100%' class='row'><div class='col-sm-2'><b>Packaging Name</b></div><div class='col-sm-10'><?php echo $item->MergeUpdate->envelope_name; ?></div></td></tr></table></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
												</a>
												<?php if( $item->MergeUpdate->out_sell_marker  == 3) { ?>
													<i class="fa fa-warning"></i>
												<?php } ?>
											<a class="moreDetails" href="#" >
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														echo $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														echo $item->MergeUpdate->order_id;
													}
												?>
												</a>
												<?php 	
											if( in_array($item->MergeUpdate->order_id, $oversell_items)){
												echo "<br>Over Sell";
											}
											?>
											</td>
											<td><?php echo $calculatDays[0].' '.$calculatDays[1]; ?></td>
											<td><?php echo $standardOne->sub_source; ?></td>
											<td><?php echo $item->MergeUpdate->postal_service; ?></td>								
											<td>
												<?php
													//pack_order_quantity
													//pr($standardOne->Items); exit;
													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($standardOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($standardOne->Items) == 1 )
															{
																echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															}
															else
															{
																echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															}
														}
													}
													
													$mer_barcodes = explode(',',$item->MergeUpdate->barcode);
													foreach( $mer_barcodes as $mer_barcode )
													{
														print "<br>".$this->Common->getLatestBarcode( $mer_barcode );
													}
													
													//echo '<br />'. str_replace( ',','<br />',$item->MergeUpdate->barcode );
												?>
											</td>
											<td>
											<?php
												$objectSkuLocations = $this->Common->getSkuBinLocation( $item->MergeUpdate->barcode );	
												foreach( $objectSkuLocations as $objectSkuLocationsIndex => $objectSkuLocationsValues)
												{
													echo $objectSkuLocationsValues['binLocation']."<br>";
												}
											?>
											</td>
											<td class="productTitle">
											<?php foreach($titles as $title) {  ?>
											<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>
											</td>
											<td>
												<?php 
												//echo $standardOne->TotalsInfo->Currency .'EUR';
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($standardOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $standardOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $standardOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($standardOne->Items) == 1 )
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $standardOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
															else
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $standardOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
														}
													}
												?>
											</td>
											<td>
											<?php
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($standardOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($standardOne->Items) == 1 )
															{
																echo $item->MergeUpdate->price;
															}
															else
															{
																echo $item->MergeUpdate->price;
															}
														}
													}										
											?>
											</td>
											<td>
											<?php echo $standardOne->TotalsInfo->Currency; ?>
											</td>											
											<td>
											<?php 
												$expressDate=$standardOne->GeneralInfo->ReceivedDate;
												$trackedDate=explode('T', $expressDate);
												echo $trackedDate[0]."<br>(".$trackedDate[1].")";
											?>
											</td>
											<td><?php echo $item->MergeUpdate->order_date; ?></td>
											<!--<td>
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														$ord_id = $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														$ord_id = $item->MergeUpdate->order_id;
													} ?>
												<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".pop-up-<?php print $ord_id; ?>">
													<i class="ion-clipboard"></i>
												</a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->order_id; ?>" class="calcel_order btn btn-xs btn-danger" href="javascript:void(0); " title = "Cancel Order" data-toggle="modal" data-target=".cancel_pop-up-<?php echo $standardOne->OrderId; ?>" >Cancel Order</a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->product_order_id_identify; ?>" class="assign_custom_service for_tooltip btn btn-xs btn-success" href="javascript:void(0); " title = "Assign Custom Service" data-toggle="modal" >Service Assign</a>
												
												<?php  if( $assignStore == true ) { ?>
													<a rel="tooltip" for="<?php echo $item->MergeUpdate->id; ?>###<?php echo $item->MergeUpdate->product_order_id_identify; ?>" class="assign_packaging_slip for_tooltip btn btn-xs btn-success" href="javascript:void(0);" title = "Assign Packaging Service" data-toggle="modal" >Service Slip</a>
												<?php } ?>
												
											</td>-->
										</tr>
								<?php
								endforeach;	
								?>
								<?php } ?>
								
							</table>
							<br><br><br>
							</section>
				<section id="group2">
							<div class="overlay">&nbsp;</div>
							<table class="table table-bordered table-striped dataTable">
								<tr>
									<th width="9%">OrderID</th>
									<th width="9%">Days</th>
									<th width="9%">Store</th>
									<th width="9%">Postal</th>
									<th width="10%">SKU</th>
									<th width="8%">Bin Location</th>
									<th width="20%">Title</th>
									<th width="6%">Total (EUR)</th>
									<th width="5%">Total</th>
									<th width="5%">Cur.</th>
									<th width="7%">Date</th>
									<th width="7%">Move Date</th>
									<!--<th width="21%">Action</th>-->
								</tr>
								
								<!-- For Express with single SKU Group B -->
								
								<!-- For Tracked with single SKU Group B -->
								<?php 
								foreach($tracked2 as $expressOne)
								{
									$expressDate=$expressOne->GeneralInfo->ReceivedDate;
									$trackedDate=explode('T', $expressDate);
									
									$getTime = $extraComponent->getResponseTime( $trackedDate[0] );
									$calculatDays = explode(' ' , $getTime);

									if( $calculatDays[1] == 'days' || $calculatDays[1] == 'months' || $calculatDays[1] == 'years' )
									{
										if( $calculatDays[0] >= 2 )
										{
											//$timeCalculator = "alert-danger";
											$timeCalculator = " color:#a94442";
										}
										else
										{
											$timeCalculator = "";
										}
									}
									else
									{
										$timeCalculator = "";
									}
								
									foreach($expressOne->Items as $item):
									$titles = $linnworkObj->getTitle( $item->MergeUpdate->sku );
									$pickListStatus	=	$checkproduct->checkPickList( $item->MergeUpdate->product_order_id_identify );
									if( $pickListStatus == 1)
									{
										$backgroud = "bg-orange-100";
									}
									else
									{
										$backgroud = "";
									}
									?>
									
										<tr style="border-bottom: 1px solid #ccc; <?php echo $timeCalculator; ?>" class ="<?php echo $backgroud; ?>" >									
											
											<td>
											<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_id; ?></div></td><td class='row'><div class='col-sm-6'><b>Postal Service</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Warehouse</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->warehouse; ?></div></td><td class='row'><div class='col-sm-6'><b>Delivery Country</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->delevery_country; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Delivery Label</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Service Provider</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Provider Code</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->provider_ref_code; ?></div></td><td class='row'><div class='col-sm-6'><b>Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_weight; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Length</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_length; ?></div></td><td class='row'><div class='col-sm-6'><b>Width</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Height</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_height; ?></div></td><td class='row'><div class='col-sm-6'><b>Packaging Slip</b></div><div class='col-sm-6'><?php echo $item->PackagingSlip->name; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b>Service Name</b></div><div class='col-sm-11'><?php echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'><table><tr><td width='25%' class='row'><div class='col-sm-6'><b>Packaging ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_id; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_weight; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Cost</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_cost; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Class</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packaging_type; ?></div></td></tr><tr><td colspan='4' width='100%' class='row'><div class='col-sm-2'><b>Packaging Name</b></div><div class='col-sm-10'><?php echo $item->MergeUpdate->envelope_name; ?></div></td></tr></table></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
												</a>
												<?php if( $item->MergeUpdate->out_sell_marker  == 3) { ?>
													<i class="fa fa-warning"></i>
												<?php } ?>
											<a class="moreDetails" href="#" >
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														echo $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														echo $item->MergeUpdate->order_id;
													}
												?>
												</a>
												<?php 	
											if( in_array($item->MergeUpdate->order_id, $oversell_items)){
												echo "<br>Over Sell";
											}
											?>
											</td>
											<td><?php echo $calculatDays[0].' '.$calculatDays[1]; ?></td>
											<td><?php printf( "%s", $expressOne->GeneralInfo->SubSource ); // $item->MergeUpdate->postal_service; ?></td>
											<td><?php echo $expressOne->ShippingInfo->PostalServiceName; ?></td>								
											<td>
												<?php
													//pack_order_quantity
													//pr($standardOne->Items); exit;
													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													echo '<br />'. str_replace( ',','<br />',$item->MergeUpdate->barcode );
													
												?>
											</td>
											<td>
											<?php
												$objectSkuLocations = $this->Common->getSkuBinLocation( $item->MergeUpdate->barcode );	
												foreach( $objectSkuLocations as $objectSkuLocationsIndex => $objectSkuLocationsValues)
												{
													echo $objectSkuLocationsValues['binLocation']."<br>";
												}
											?>
											</td>
											<td class="productTitle">
											<?php foreach($titles as $title) {  ?>
											<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>
											</td>
											<td>
												<?php 
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency((($item->MergeUpdate->quantity / $item->MergeUpdate->pack_order_quantity) * $item->MergeUpdate->price), $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price * $item->MergeUpdate->quantity, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
															else
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
														}
													}
												?>
											</td>
											<td>
											<?php
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}													
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																echo $item->MergeUpdate->price * $item->MergeUpdate->quantity;
															}
															else
															{
																echo $item->MergeUpdate->price;
															}
														}
													}										
											?>
											</td>
											<td>
											<?php echo $expressOne->TotalsInfo->Currency; ?>
											</td>
											<td>
											<?php 
												$expressDate=$expressOne->GeneralInfo->ReceivedDate;
												$trackedDate=explode('T', $expressDate);
												echo $trackedDate[0]."<br>(".$trackedDate[1].")";
											?>
											</td>
											<td><?php echo $item->MergeUpdate->order_date; ?></td>
											<!--<td>
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														$ord_id = $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														$ord_id = $item->MergeUpdate->order_id;
													} ?>
												<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".pop-up-<?php print $ord_id; ?>">
													<i class="ion-clipboard"></i>
												</a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->order_id; ?>" class="calcel_order btn btn-xs btn-danger" href="javascript:void(0); " title = "Cancel Order" data-toggle="modal" data-target=".cancel_pop-up-<?php echo $expressOne->OrderId; ?>" >Cancel Order</a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->product_order_id_identify; ?>" class="assign_custom_service for_tooltip btn btn-xs btn-success" href="javascript:void(0); " title = "Assign Custom Service" data-toggle="modal" >Service Assign</a>
											</td>-->
										</tr>
								<?php
								endforeach;	
								?>
								<?php } ?>
								
								<!-- For Standard with single SKU Group B -->
								<?php
								foreach($standerd2 as $expressOne)
								{
									
									$expressDate=$expressOne->GeneralInfo->ReceivedDate;
									$trackedDate=explode('T', $expressDate);
									
									$getTime = $extraComponent->getResponseTime( $trackedDate[0] );
									$calculatDays = explode(' ' , $getTime);

									if( $calculatDays[1] == 'days' || $calculatDays[1] == 'months' || $calculatDays[1] == 'years' )
									{
										if( $calculatDays[0] >= 1 )
										{
											//$timeCalculator = "alert-danger";
											$timeCalculator = " color:#a94442";
										}
										else
										{
											$timeCalculator = "";
										}
									}
									else
									{
										$timeCalculator = "";
									}
								?> 
									<?php
									foreach($expressOne->Items as $item):
									$titles = $linnworkObj->getTitle( $item->MergeUpdate->sku );
									$pickListStatus	=	$checkproduct->checkPickList( $item->MergeUpdate->product_order_id_identify );
									if( $pickListStatus == 1)
									{
										$backgroud = "bg-orange-100";
									}
									else
									{
										$backgroud = "";
									}
									?>
										<tr style="border-bottom: 1px solid #ccc; <?php echo $timeCalculator; ?>" class ="<?php echo $backgroud; ?>" >									
											<td>
											<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_id; ?></div></td><td class='row'><div class='col-sm-6'><b>Postal Service</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Warehouse</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->warehouse; ?></div></td><td class='row'><div class='col-sm-6'><b>Delivery Country</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->delevery_country; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Delivery Label</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Service Provider</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Provider Code</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->provider_ref_code; ?></div></td><td class='row'><div class='col-sm-6'><b>Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_weight; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Length</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_length; ?></div></td><td class='row'><div class='col-sm-6'><b>Width</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Height</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_height; ?></div></td><td class='row'><div class='col-sm-6'><b>Packaging Slip</b></div><div class='col-sm-6'><?php echo $item->PackagingSlip->name; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b>Service Name</b></div><div class='col-sm-11'><?php echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'><table><tr><td width='25%' class='row'><div class='col-sm-6'><b>Packaging ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_id; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_weight; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Cost</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_cost; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Class</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packaging_type; ?></div></td></tr><tr><td colspan='4' width='100%' class='row'><div class='col-sm-2'><b>Packaging Name</b></div><div class='col-sm-10'><?php echo $item->MergeUpdate->envelope_name; ?></div></td></tr></table></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
											</a>
											<?php if( $item->MergeUpdate->out_sell_marker  == 3) { ?>
													<i class="fa fa-warning"></i>
											<?php } ?>
											<a class="moreDetails" href="#" >
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														echo $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														echo $item->MergeUpdate->order_id;
													}
												?>
												</a>
												<?php 	
											if( in_array($item->MergeUpdate->order_id, $oversell_items)){
												echo "<br>Over Sell";
											}
											?>
											</td>
											<td><?php echo $calculatDays[0].' '.$calculatDays[1]; ?></td>
											<td><?php printf( "%s", $expressOne->GeneralInfo->SubSource ); // $item->MergeUpdate->postal_service; ?></td>
											<td><?php echo $expressOne->ShippingInfo->PostalServiceName; ?></td>								
											<td>
												<?php
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo $item->MergeUpdate->quantity .'X'. $item->MergeUpdate->sku;
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																echo ($item->MergeUpdate->quantity) .'X'. $item->MergeUpdate->sku;
															}
															else
															{
																echo $item->MergeUpdate->quantity .'X'. $item->MergeUpdate->sku;
															}
														}
													}
													echo '<br />'. str_replace( ',','<br />',$item->MergeUpdate->barcode );
												?>
											</td>
											<td>
											<?php
												$objectSkuLocations = $this->Common->getSkuBinLocation( $item->MergeUpdate->barcode );	
												foreach( $objectSkuLocations as $objectSkuLocationsIndex => $objectSkuLocationsValues)
												{
													echo $objectSkuLocationsValues['binLocation']."<br>";
												}
											?>
											</td>
											<td class="productTitle">
											<?php foreach($titles as $title) {  ?>
											<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>
											
											<!--<table class="table table-responsive table-hover">
    <tbody>
        <tr class="clickable" data-toggle="collapse" id="row1" data-target=".row1">
            <td width="1%"><i class="glyphicon glyphicon-plus"></i></td>
            <td width="20%">data</td>
          	<td width="79%">data</td>  
        </tr>
        <tr class="collapse row1">
            <td></td>
            <td>data</td>
          	<td>data</td>  
        </tr>
        <tr class="collapse row1">
            <td></td>
            <td>data</td>
          	<td>data</td>  
        </tr>
    </tbody>
</table>-->
											
											</td>
											
											<td>
												<?php 
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price * $item->MergeUpdate->quantity, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
															else
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
														}
													}
												?>
											</td>
											<td>
											<?php
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																echo $item->MergeUpdate->price * $item->MergeUpdate->quantity;
															}
															else
															{
																echo $item->MergeUpdate->price;
															}
														}
													}										
											?>
											</td>											
											<td>
											<?php echo $expressOne->TotalsInfo->Currency; ?>
											</td>
											<td>
											<?php 
												$expressDate=$expressOne->GeneralInfo->ReceivedDate;
												$trackedDate=explode('T', $expressDate);
												echo $trackedDate[0]."<br>(".$trackedDate[1].")";
											?>
											</td>
											<td><?php echo $item->MergeUpdate->order_date; ?></td>
											<!--<td>
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														$ord_id = $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														$ord_id = $item->MergeUpdate->order_id;
													} ?>
												<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".pop-up-<?php print $ord_id; ?>">
													<i class="ion-clipboard"></i>
												</a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->order_id; ?>" class="calcel_order btn btn-xs btn-danger" href="javascript:void(0); " title = "Cancel Order" data-toggle="modal" data-target=".cancel_pop-up-<?php echo $expressOne->OrderId; ?>" >Cancel Order</a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->product_order_id_identify; ?>" class="assign_custom_service for_tooltip btn btn-xs btn-success" href="#" title = "Assign Custom Service" data-toggle="modal" >Service Assign</a>
												<?php if( $item->PackagingSlip->name == '' ) { ?>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->id; ?>###<?php echo $item->MergeUpdate->product_order_id_identify; ?>" class="assign_packaging_slip for_tooltip btn btn-xs btn-success" href="javascript:void(0);" title = "Assign Packaging Service" data-toggle="modal" >Service Slip</a>
												<?php } ?>
												
											</td>-->
										</tr>
								<?php
								endforeach;	
								?>
								<?php } ?>								
							</table>
							<br><br><br>
							</section>
				<?php if( count($express1) > 0 &&  count($express2) > 0) { ?>
				<section id="group3">
							<div class="overlay">&nbsp;</div>
							<table class="table table-bordered table-striped dataTable">
								<tr>
									<th width="9%">OrderID</th>
									<th width="9%">Days</th>
									<th width="9%">Store</th>
									<th width="9%">Postal</th>
									<th width="10%">SKU</th>
									<th width="8%">Bin Location</th>
									<th width="20%">Title</th>
									<th width="6%">Total (EUR)</th>
									<th width="5%">Total</th>
									<th width="5%">Cur.</th>
									<th width="7%">Date</th>
									<th width="7%">Move Date</th>
									<!--<th width="21%">Action</th>-->
								</tr>
								
								<!-- For Express with single SKU Group B -->
								<?php 
								App::import('controller', 'Currents');
								$currentObj	=	new CurrentsController();
								
								App::import('controller', 'Linnworksapis');
								$linnworkObj	=	new LinnworksapisController();
								
								App::import( "Component","Extra" );
								$extraComponent = new ExtraComponent(new ComponentCollection());   
								
								$unwanted	=	Configure::read('unwanted');
																
								foreach($express1 as $expressOne)
								{
									
									$expressDate=$expressOne->GeneralInfo->ReceivedDate;
									$trackedDate=explode('T', $expressDate);
									
									$getTime = $extraComponent->getResponseTime( $trackedDate[0] );
									$calculatDays = explode(' ' , $getTime);

									if( $calculatDays[1] == 'days' || $calculatDays[1] == 'months' || $calculatDays[1] == 'years' )
									{
										if( $calculatDays[0] >= 2 )
										{
											//$timeCalculator = "alert-danger";
											$timeCalculator = " color:#a94442";
										}
										else
										{
											$timeCalculator = "";
										}
									}
									else
									{
										$timeCalculator = "";
									}
								
									foreach($expressOne->Items as $item):
									$titles = $linnworkObj->getTitle( $item->MergeUpdate->sku );
									$pickListStatus	=	$checkproduct->checkPickList( $item->MergeUpdate->product_order_id_identify );
									if( $pickListStatus == 1)
									{
										$backgroud = "bg-orange-100";
									}
									else
									{
										$backgroud = "";
									}
								
									?>
									
										<tr style="border-bottom: 1px solid #ccc; <?php echo $timeCalculator; ?>" class ="<?php echo $backgroud; ?>" >									
											<td>
											<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_id; ?></div></td><td class='row'><div class='col-sm-6'><b>Postal Service</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Warehouse</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->warehouse; ?></div></td><td class='row'><div class='col-sm-6'><b>Delivery Country</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->delevery_country; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Delivery Label</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Service Provider</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Provider Code</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->provider_ref_code; ?></div></td><td class='row'><div class='col-sm-6'><b>Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_weight; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Length</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_length; ?></div></td><td class='row'><div class='col-sm-6'><b>Width</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Height</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_height; ?></div></td><td class='row'><div class='col-sm-6'><b>Packaging Slip</b></div><div class='col-sm-6'><?php echo $item->PackagingSlip->name; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b>Service Name</b></div><div class='col-sm-11'><?php echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'><table><tr><td width='25%' class='row'><div class='col-sm-6'><b>Packaging ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_id; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_weight; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Cost</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_cost; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Class</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packaging_type; ?></div></td></tr>
											<tr><td colspan='4' width='100%' class='row'><div class='col-sm-2'><b>Packaging Name</b></div><div class='col-sm-10'><?php echo $item->MergeUpdate->envelope_name; ?></div></td></tr></table></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
												</a>
												<?php if( $item->MergeUpdate->out_sell_marker  == 3) { ?>
													<i class="fa fa-warning"></i>
												<?php } ?>
											<a class="moreDetails" href="#" >
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														echo $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														echo $item->MergeUpdate->order_id;
													}
												?>
												</a>
												<?php 	
											if( in_array($item->MergeUpdate->order_id, $oversell_items)){
												echo "<br>Over Sell";
											}
											?>
											</td>
											<td><?php echo $calculatDays[0].' '.$calculatDays[1]; ?></td>
											<td><?php printf( "%s", $expressOne->GeneralInfo->SubSource ); // $item->MergeUpdate->postal_service; ?></td>
											<td><?php echo $expressOne->ShippingInfo->PostalServiceName; ?></td>								
											<td>										
												<?php
													//pack_order_quantity
													//pr($standardOne->Items); exit;
													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															 
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															}
															else
															{
																echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															}
														}
													}
													$mer_barcodes = explode(',',$item->MergeUpdate->barcode);
													foreach( $mer_barcodes as $mer_barcode )
													{
														print "<br>".$this->Common->getLatestBarcode( $mer_barcode );
													}
													//echo '<br />'. str_replace( ',','<br />',$item->MergeUpdate->barcode );
												?>
											</td>
											<td>
											<?php
												$objectSkuLocations = $this->Common->getSkuBinLocation( $item->MergeUpdate->barcode );	
												foreach( $objectSkuLocations as $objectSkuLocationsIndex => $objectSkuLocationsValues)
												{
													echo $objectSkuLocationsValues['binLocation']."<br>";
												}
											?>
											
											</td>
											<td class="productTitle">
											<?php foreach($titles as $title) {  ?>
											<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>											
											</td>
											<td>
												<?php 
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price * $item->MergeUpdate->quantity, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
															else
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
														}
													}
												?>
											</td>
											<td>
											<?php
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																echo $item->MergeUpdate->price * $item->MergeUpdate->quantity;
															}
															else
															{
																echo $item->MergeUpdate->price;
															}
														}
													}										
											?>
											</td>											
											<td>
											<?php echo $expressOne->TotalsInfo->Currency; ?>
											</td>
											<td>
											<?php 
												$expressDate=$expressOne->GeneralInfo->ReceivedDate;
												$trackedDate=explode('T', $expressDate);
												echo $trackedDate[0]."<br>(".$trackedDate[1].")";
											?>
											</td>
											<td><?php echo $item->MergeUpdate->order_date; ?></td>
											<!--<td>
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														$ord_id = $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														$ord_id = $item->MergeUpdate->order_id;
													} ?>
												<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".pop-up-<?php print $ord_id; ?>">
													<i class="ion-clipboard"></i>
												</a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->order_id; ?>" class="calcel_order btn btn-xs btn-danger" href="javascript:void(0); " title = "Cancel Order" data-toggle="modal" data-target=".cancel_pop-up-<?php echo $expressOne->OrderId; ?>"  >Cancel Order</a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->product_order_id_identify; ?>" class="assign_custom_service for_tooltip btn btn-xs btn-success" href="javascript:void(0); " title = "Assign Custom Service" data-toggle="modal" >Service Assign</a>
											</td>-->
										</tr>
								<?php
								endforeach;	
								?>
								<?php } ?>
								
								<?php 
								foreach($express2 as $expressOne)
								{
									$expressDate=$expressOne->GeneralInfo->ReceivedDate;
									$trackedDate=explode('T', $expressDate);
									
									$getTime = $extraComponent->getResponseTime( $trackedDate[0] );
									$calculatDays = explode(' ' , $getTime);

									if( $calculatDays[1] == 'days' || $calculatDays[1] == 'months' || $calculatDays[1] == 'years' )
									{
										if( $calculatDays[0] >= 2 )
										{
											//$timeCalculator = "alert-danger";
											$timeCalculator = " color:#a94442";
										}
										else
										{
											$timeCalculator = "";
										}
									}
									else
									{
										$timeCalculator = "";
									}
									
								?> 
									<?php
									foreach($expressOne->Items as $item):
									$titles = $linnworkObj->getTitle( $item->MergeUpdate->sku );
									$pickListStatus	=	$checkproduct->checkPickList( $item->MergeUpdate->product_order_id_identify );
									if( $pickListStatus == 1)
									{
										$backgroud = "bg-orange-100";
									}
									else
									{
										$backgroud = "";
									}
									?>
										<tr style="border-bottom: 1px solid #ccc; <?php echo $timeCalculator; ?>"  class ="<?php echo $backgroud; ?>">									
											<td>
											<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_id; ?></div></td><td class='row'><div class='col-sm-6'><b>Postal Service</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Warehouse</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->warehouse; ?></div></td><td class='row'><div class='col-sm-6'><b>Delivery Country</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->delevery_country; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Delivery Label</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Service Provider</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Provider Code</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->provider_ref_code; ?></div></td><td class='row'><div class='col-sm-6'><b>Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_weight; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Length</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_length; ?></div></td><td class='row'><div class='col-sm-6'><b>Width</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Height</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_height; ?></div></td><td class='row'><div class='col-sm-6'><b>Packaging Slip</b></div><div class='col-sm-6'><?php echo $item->PackagingSlip->name; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b>Service Name</b></div><div class='col-sm-11'><?php echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'><table><tr><td width='25%' class='row'><div class='col-sm-6'><b>Packaging ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_id; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_weight; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Cost</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_cost; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Class</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packaging_type; ?></div></td></tr>
											<tr><td colspan='4' width='100%' class='row'><div class='col-sm-2'><b>Packaging Name</b></div><div class='col-sm-10'><?php echo $item->MergeUpdate->envelope_name; ?></div></td></tr></table></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
												</a>
												<?php if( $item->MergeUpdate->out_sell_marker  == 3) { ?>
													<i class="fa fa-warning"></i>
												<?php } ?>
											<a class="moreDetails" href="#" >
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														echo $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														echo $item->MergeUpdate->order_id;
													}
												?>
												</a>
												<?php 	
											if( in_array($item->MergeUpdate->order_id, $oversell_items)){
												echo "<br>Over Sell";
											}
											?>
											</td>
											<td><?php echo $calculatDays[0].' '.$calculatDays[1]; ?></td>
											<td><?php printf( "%s", $expressOne->GeneralInfo->SubSource ); // $item->MergeUpdate->postal_service; ?></td>
											<td><?php echo $expressOne->ShippingInfo->PostalServiceName; ?></td>								
											<td>
												<?php
													//pack_order_quantity
													//pr($standardOne->Items); exit;
													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													echo '<br />'. str_replace( ',','<br />',$item->MergeUpdate->barcode );
													
												?>
											</td>
											<td>
												<?php
													$objectSkuLocations = $this->Common->getSkuBinLocation( $item->MergeUpdate->barcode );	
													foreach( $objectSkuLocations as $objectSkuLocationsIndex => $objectSkuLocationsValues)
													{
														echo $objectSkuLocationsValues['binLocation']."<br>";
													}
												?>
											</td>
											<td class="productTitle">
											<?php foreach($titles as $title) {  ?>
											<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>
											</td>
											<td>
												<?php 
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency((($item->MergeUpdate->quantity / $item->MergeUpdate->pack_order_quantity) * $item->MergeUpdate->price), $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price * $item->MergeUpdate->quantity, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
															else
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
														}
													}
												?>
											</td>
											<td>
											<?php
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																echo $item->MergeUpdate->price * $item->MergeUpdate->quantity;
															}
															else
															{
																echo $item->MergeUpdate->price;
															}
														}
													}
																						
											?>
											</td>
											<td>
											<?php echo $expressOne->TotalsInfo->Currency; ?>
											</td>
											<td>
											<?php 
												$expressDate=$expressOne->GeneralInfo->ReceivedDate;
												$trackedDate=explode('T', $expressDate);
												echo $trackedDate[0]."<br>(".$trackedDate[1].")";
											?>
											</td>
											<td><?php echo $item->MergeUpdate->order_date; ?></td>
											<!--<td>
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														$ord_id = $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														$ord_id = $item->MergeUpdate->order_id;
													} ?>
												<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".pop-up-<?php print $ord_id; ?>">
													<i class="ion-clipboard"></i>
												</a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->order_id; ?>" class="calcel_order btn btn-xs btn-danger" href="javascript:void(0); " title = "Cancel Order" data-toggle="modal" data-target=".cancel_pop-up-<?php echo $expressOne->OrderId; ?>" >Cancel Order</a>
												<a rel="tooltip" for="<?php echo $item->MergeUpdate->product_order_id_identify; ?>" class="assign_custom_service for_tooltip btn btn-xs btn-success" href="javascript:void(0); " title = "Assign Custom Service" data-toggle="modal" >Service Assign</a>
											</td>-->
										</tr>
								<?php
								endforeach;	
								?>
								<?php } ?>
							</table>
							<br><br><br>
							</section>
				<?php } else { ?>
				<section id="group3">
					<div class="overlay">&nbsp;</div>
					<table class="table table-bordered table-striped dataTable">
						<tr>
							<th width="100%" calspan="12" style="color:red;"><center>There is no express orders</center></th>
						</tr>
					</table>
					<br><br><br>
				</section>
				<?php } ?>
				
							<div class="" style="margin:0 auto; width:350px">
								 <ul class="pagination">
									  <?php
										   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
										   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									  ?>
								 </ul>
							  </div>
						</div>
					</div>
				</div>
            </div>
	    </div>

	</div>        
</div>


<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<!-- show cancel order popup -->    
<div class="showPopupForcancelOrder"></div> 
<div class="showPopupForAssignSlip"></div> 

<!-- show assign service popup -->    
<div class="showPopupForAssign"></div> 
<div class="showOPenOrderSearchResult"></div> 


<script>
/*
$(document).keypress(function(e) {
    if(e.which == 13) {
        submitSanningValue();
    }
});

function submitSanningValue()
{
	var Barcode	=	$('#LinnworksapisBarcode').val();
	
	$.ajax(
				{
						'url'            : getUrl() + '/Linnworksapis/getsearchlist',
						'type'           : 'POST',
						'data'           : { barcode : Barcode },
						'beforeSend'	 : function() {
															$('.loading-image').show();
														},
						'success' 		 : function( msgArray )
														{
															$('.outer_bin_addMore').html(msgArray)
														}  
						});  
					
}*/


$(function() {
  $("#LinnworksapisBarcode").focus();
});
	$(document).ready(function()
        { 
		var $animationExamples;
        $animationExamples = $(".animated-tab").hashTabs();
		$("[rel='tooltip']").tooltip();
			$('.moreDetails').popover({html:true});
			$('.moreDetails i').mouseover(function() {
				//$('.rackDetails .tab-pane-container').addClass('overlay');
				$('div.overlay').addClass('on');
				//$('.table-striped > tbody > tr:nth-of-type(2n+1)').css('background-color','transparent');
				//$('.overlay .btn').css('display','none');
				$('.moreDetails i').css({"z-index": "9999999999", "position": "relative", "color": "#ffffff"});
			});
			$('.moreDetails i').mouseout(function() {
				$('div.overlay').removeClass('on');
				//$('.table-striped > tbody > tr:nth-of-type(2n+1)').css('background-color','#f9f9f9');
				//$('.overlay .btn').css('display','inline-block');
				$('.moreDetails i').css({"z-index": "9999999999", "position": "relative", "color": "#444444"});
			});

			<!--set product id-->
			
					 var skuArray = [];
					 var countryArraynew = [];
					 var blkstr = [];
					 var i = 0;
					 $('.case').each(function()
									{
										skuArray[i]	=	$(this).attr('id');
										i++;
									});
			
					$.each(skuArray, function(idx2,val2) {                    
							 var str =  val2;
							 blkstr.push(str);
							 
						});
					
						var skuStr	=	blkstr.join("---");
						$('.get_sku_string').val(skuStr);
			
			<!--set product id-->
			
			$("body").on("click", ".assignpostelservice", function(){
				var id	=	$(this).attr('for');
				$.ajax(
				{
						'url'            : getUrl() + '/Linnworksapis/assignService',
						'type'           : 'POST',
						'data'           : { id : id },
						'beforeSend'	 : function() {
															$('.loading-image_'+id).show();
														},
						'success' 		 : function( msgArray )
														{
															$('.pop-up-'+id+' h4').text(msgArray);
															$('.loading-image_'+id).hide();
															$('.processedorder_'+id).show();
															$('.assignpostelservice_'+id).hide();
														}  
						});  
					});
			
			$("body").on("click", ".processedorder", function(){
				var id	=	$(this).attr('for');
				$.ajax(
					{
						'url'          : getUrl() + '/Linnworksapis/labelAssign',
						'type'         : 'POST',
						'data'         : { id : id },
						'beforeSend'   : function() {
														$('.loading-image_'+id).show();
													 },
						'success' 	   : function( msgArray )
													{
														var result = msgArray.split("#");
														$('.printorder_'+id).attr('href', result[0]);
														$('.printlable_'+id).attr('href', result[1]);
														$('.loading-image_'+id).hide();
														$('.printorder_'+id).show();
														$('.printlable_'+id).show();
														$('.processedorder_'+id).hide();
													}  
					});  
				});
				
				/* For update custom posta service */			
				$("body").on("click", ".assignCustomPostal", function(){
					
					var orderId 		= $(this).attr( 'for' );
					
					var str = $( '.servicePop-'+orderId ).find('form.form-horizontal').serialize();
					
					$.ajax(
						{
							'url'          : '/System/Service',
							'type'         : 'POST',
							'data'         : { id : orderId, 
											   query:str
											 },						
							'success' 	   : function( msgArray )
											{
												if( msgArray == 1 )
												{
													swal( "New postal service assigned." );
													$( ".close" ).click();
													//location.reload();		
												}
												else
												{
													swal( 'Service Id does not exist' );
												}
											}  
						});  
					});
			
			/* For close popup */	
			
			/* For update custom Packagin slip */			
				$("body").on("click", ".CustomSlip", function(){
					
					var orderId 		= $(this).attr( 'for' );
					var StoreId			=	$( '#StoreStoreName' ).val();
					var StoreIdText		=	$( '#StoreStoreName' ).find(":selected").text();
					var mergeUpdateId	=	$( '#mergeupdateId' ).val();
					var TemplateStoreId	=	$( '#TemplateStoreId' ).val();
					
					$.ajax(
						{
							'url'          : '/linnworksapis/assignPackagingSlip',
							'type'         : 'POST',
							'data'         : { orderId : orderId, 
											   StoreId : StoreId,
											   StoreIdText : StoreIdText,
											   mergeUpdate : mergeUpdateId,
											   TemplateStoreId:TemplateStoreId
											 },						
							'success' 	   : function( msgArray )
											{
												//alert( msgArray );
												if( msgArray == 1 )
												{
													swal( "New Packaging Slip assigned." );
													$( ".close" ).click();
													location.reload();		
												}
												else
												{
													swal( 'Packaging Slip exist.' );
												}
											}  
						});  
					});
			
			
				/*$("body").on("click", ".CustomPostalService", function(){
					
						var orderId 		= $(this).attr( 'for' );
						var serviceID 		= $('#postalServiceid_'+orderId).val();
					
						$.ajax(
						{
							'url'          : '/linnworksapis/setPostalservice',
							'type'         : 'POST',
							'data'         : { id	:	orderId, 
											   serviceID	:	serviceID
											 },						
							'success' 	   : function( msgArray )
											{
												if( msgArray == 1 )
												{
													swal( "New postal service assigned." );
													//$('.dismiss').attr( 'for','1');
													$( ".close" ).click();
													location.reload();		
												}
												else
												{
													swal( 'Service Id does not exist' );
												}
											}  
						});  
					
					});*/
					
					
			/* For close popup */	
				
				
				
			$("body").on("click", ".switch", function(){
				if($(".top_panel").is(":visible") == true)
				{
					$('.top_panel').hide();
					$('.bottom_panel').show();
				}
				else
				{
					$('.top_panel').show();
					$('.bottom_panel').hide();
				}
				
				});
			
			/*$('div#example1_wrapper table#example1 thead.parentCheck tr.parentInner th.sorting_asc').click(function()
			{ 		
				 if( $(this).children().hasClass( 'checked' ) )
				 {
					$(this).children().removeClass( 'checked' )
					$('.icheckbox_square-green').removeClass('checked');
					$('.download_button').css('display', 'none');								
				 }
				 else
				 {
					$('.icheckbox_square-green').addClass('checked');
					$('.download_button').css('display', 'block');				
				 }
				 
					 var skuArray = [];
					 var countryArraynew = [];
					 var blkstr = [];
					 var i = 0;
					 $('div.checked .case').each(function()
									{
											skuArray[i]	=	$(this).attr('id');
										i++;
									});
			
					$.each(skuArray, function(idx2,val2) {                    
							 var str =  val2;
							 blkstr.push(str);
						});
					
						var skuStr	=	blkstr.join("---");
						$('.get_sku_string').val(skuStr);
					});*/
			
						
			$("body").on("click", ".sortingChild", function ()
				{
					
					 var skuArray = [];
					 var countryArraynew = [];
					 var blkstr = [];
					 $('.download_button').css('display', 'none');
				
					 var i = 0;
					 $('div.checked .case').each(function()
						{
								skuArray[i]	=	$(this).attr('id');
							i++;
							if( i >= 1)
							{
								$('.download_button').css('display', 'block');
							}
						});
					 $.each(skuArray, function(idx2,val2) 
						{
							 var str =  val2;
							 blkstr.push(str);
						});
					
						var skuStr	=	blkstr.join("---");
						$('.get_sku_string').val(skuStr);
						 
					});
						
			
			
			/* start for download pick list */
			$( 'body' ).on( 'click', '.downloadPickList', function()
				  {
					 var orderId	=	'';
					 
				   //Abort asynchronous Ajax Calls
				   $.ajax({
					 url      : '/Aborts/utilizeAbortSync',
					 type     : 'POST',
					 data     : {},
					 beforeSend : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },         
					 success  : function( data )
					 {
						if( data == 1 )	
						{
							$( ".outerOpac" ).attr( 'style' , 'display:none');
							swal( "Asynchronous calls not allowed!!!!" );
							return false;
						}
						else if( data == 0 )	
						{
							$.ajax({
								 //url      : '/Linnworksapis/generatePickList',
								 url      : '/Linnworksapis/createPickList',
								 type     : 'POST',
								 data     : { orderId : orderId },
								 dataType: 'json',      
								 success  : function( data  )
								 {
									if( data.msg == 'ok')
									{
										$( ".outerOpac" ).attr( 'style' , 'display:none');
										location.reload();
										window.open(data.pdfurl,'_self' );
									}
									else
									{
										$( ".outerOpac" ).attr( 'style' , 'display:none');
										swal("OOPS, No more SKU found for picklist!.", "" , "success");											
										return false;
									}
								 }                
							});
						}
					 }                
				});   
			});
			/* end for download pick list */
			
			/* Code for order cancel */			
			$("body").on("click", ".ordercancel", function(){
				var content	=	$(this).attr('for');
				var res 	= content.split("##$#");
				var id		=	res[0];
				$.ajax(
				{
					'url'                     : getUrl() + '/jijGroup/Order/orderCancel',
					'type'                    : 'POST',
					'data'                    : { content : content },
					'beforeSend'			  : function() {
																$('.loading-image_'+id).show();
															 },
					'success' 				  : function( msgArray )
												{
													
													if (msgArray == 1 )
													{
														$('.panel-head').html('<p class="alert alert-success"> Order Cancelled Successfully.</p>').show(500).delay(5000).fadeOut();
														$('.loading-image_'+id).hide();
														location.reload();
													}
												}  
				});  
			});
		});
		
		
		//Download Stock file now
		$( 'body' ).on( 'click', '.downloadStockFile', function()
		{
			//Check If input rack name exists in table or not
			$.ajax({
					url     	: '/Products/prepareVirtualStock',
					type    	: 'POST',
					data    	: {},  			  
					success 	:	function( data  )
					{
						 window.open(data,'_self' );
					}                
				});				
		});	
		
		$(function()
		{
			$( 'body' ).on( 'click', '.calcel_order', function()
			{              
				var orderid    = $(this).attr('for');          
				$.ajax(
				{
					'url'            : '/linnworksapis/cancelOrderPopup',
					'type'           : 'POST',
					'data'           : { orderid : orderid },                                    
					'beforeSend'     : function()
					{
									//$('.loading-image').show();
					},
					'success'        : function( msgArray ){ $( 'div.showPopupForcancelOrder' ).html( msgArray ); }
				});
			});  
			
			/* Assign postal service */
			
			$( 'body' ).on( 'click', '.assign_custom_service', function()
			{              
				var orderid    = $(this).attr('for'); 
				$.ajax(
				{
					'url'            : '/linnworksapis/assignServicePopup',
					'type'           : 'POST',
					'data'           : { orderid : orderid },                                    
					'beforeSend'     : function()
					{
									//$('.loading-image').show();
					},
					'success'        : function( msgArray ){ $( 'div.showPopupForcancelOrder' ).html( msgArray ); }
				});
			}); 
			
			$( 'body' ).on( 'click', '.assign_packaging_slip', function()
			{              
				var orderid    = $(this).attr('for');
				$.ajax(
				{
					'url'            : '/linnworksapis/assignPackagingSlipPopup',
					'type'           : 'POST',
					'data'           : { orderid : orderid },                                    
					'beforeSend'     : function()
					{
									//$('.loading-image').show();
					},
					'success'        : function( msgArray ){ $( 'div.showPopupForAssignSlip' ).html( msgArray ); }
				});
			});                                
		});
		
		$( 'body' ).on( 'click', '.search-icon', function()
		{ 
			 if($('#OpenOrderSearchtext').val() != '')
				{
					submitSearchKey();
				}
				else
				{
					
				}             
		});
		
		
		$(document).keypress(function(e)
		{
		   if(e.which == 13) 
		   {
			   if($('#OpenOrderSearchtext').val() != '')
				{
					submitSearchKey();
				}
				else
				{
					
				}
			}
		});
	
	function submitSearchKey()
	{
		var searchKey	=	$('#OpenOrderSearchtext').val();
		$.ajax({
					url     	: '/Linnworksapis/getOpenOrderSearchResult',
					type    	: 'POST',
					data    	: {  searchKey : searchKey },
					beforeSend	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
					},   			  
					success 	:	function( data  )
					{
						if( data == 1 )
						{
							$( ".outerOpac" ).attr( 'style' , 'display:none');
							swal( "Oops!!!, ( < "+ searchKey +" >) not found here.");
						}
						if( data == 2 )
						{
							$( ".outerOpac" ).attr( 'style' , 'display:none');
							swal( "Oops!!!, ( < "+ searchKey +" >) not found here.");
						}
						else
						{
							$( ".outerOpac" ).attr( 'style' , 'display:none');
							$('.showOPenOrderSearchResult').html( data );
						}
					}                
				});
	}

		
</script>



<style type="text/css">
           
            .search-wrapper {
                position: absolute;
                -webkit-transform: translate(-50%, -50%);
                -moz-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
                top:80px;
                right:0px;
				z-index:1;
            }
            .search-wrapper.active {}

            .search-wrapper .input-holder {
                overflow: hidden;
                height: 50px;
                background: rgba(255,255,255,0);
                border-radius:6px;
                position: relative;
                width:50px;
                -webkit-transition: all 0.3s ease-in-out;
                -moz-transition: all 0.3s ease-in-out;
                transition: all 0.3s ease-in-out;
            }
            .search-wrapper.active .input-holder {
                border-radius: 50px;
                width:580px;
                background: rgba(0,0,0,0.5);
                -webkit-transition: all .5s cubic-bezier(0.000, 0.105, 0.035, 1.570);
                -moz-transition: all .5s cubic-bezier(0.000, 0.105, 0.035, 1.570);
                transition: all .5s cubic-bezier(0.000, 0.105, 0.035, 1.570);
            }

            .search-wrapper .input-holder .search-input {
                width:100%;
                height: 30px;
                padding:0px 70px 0 20px;
                opacity: 0;
                position: absolute;
                top:0px;
                left:0px;
                background: transparent;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                border:none;
                outline:none;
                font-family:"Open Sans", Arial, Verdana;
                font-size: 16px;
                font-weight: 400;
                line-height: 20px;
                color:#FFF;
                -webkit-transform: translate(0, 60px);
                -moz-transform: translate(0, 60px);
                transform: translate(0, 60px);
                -webkit-transition: all .3s cubic-bezier(0.000, 0.105, 0.035, 1.570);
                -moz-transition: all .3s cubic-bezier(0.000, 0.105, 0.035, 1.570);
                transition: all .3s cubic-bezier(0.000, 0.105, 0.035, 1.570);

                -webkit-transition-delay: 0.3s;
                -moz-transition-delay: 0.3s;
                transition-delay: 0.3s;
            }
            .search-wrapper.active .input-holder .search-input {
                opacity: 1;
                -webkit-transform: translate(0, 10px);
                -moz-transform: translate(0, 10px);
                transform: translate(0, 10px);
            }

            .search-wrapper .input-holder .search-icon {
                width:50px;
                height:50px;
                border:none;
                border-radius:6px;
                background: #f2f2f2;
                padding:0px;
                outline:none;
                position: relative;
                z-index: 2;
                float:right;
                cursor: pointer;
                -webkit-transition: all 0.3s ease-in-out;
                -moz-transition: all 0.3s ease-in-out;
                transition: all 0.3s ease-in-out;
            }
            .search-wrapper.active .input-holder .search-icon {
                width: 40px;
                height:40px;
                margin: 5px;
                border-radius: 30px;
            }
            .search-wrapper .input-holder .search-icon span {
                width:22px;
                height:22px;
                display: inline-block;
                vertical-align: middle;
                position:relative;
                -webkit-transform: rotate(45deg);
                -moz-transform: rotate(45deg);
                transform: rotate(45deg);
                -webkit-transition: all .4s cubic-bezier(0.650, -0.600, 0.240, 1.650);
                -moz-transition: all .4s cubic-bezier(0.650, -0.600, 0.240, 1.650);
                transition: all .4s cubic-bezier(0.650, -0.600, 0.240, 1.650);

            }
            .search-wrapper.active .input-holder .search-icon span {
                -webkit-transform: rotate(-45deg);
                -moz-transform: rotate(-45deg);
                transform: rotate(-45deg);
            }
            .search-wrapper .input-holder .search-icon span::before, .search-wrapper .input-holder .search-icon span::after {
                position: absolute;
                content:'';
            }
            .search-wrapper .input-holder .search-icon span::before {
                width: 4px;
                height: 11px;
                left: 9px;
                top: 18px;
                border-radius: 2px;
                background: #27ae60;
            }
            .search-wrapper .input-holder .search-icon span::after {
                width: 20px;
                height: 20px;
                left: 0px;
                top: 0px;
                border-radius: 16px;
                border: 4px solid #27ae60;
            }

            .search-wrapper .close {
                position: absolute;
                z-index: 1;
                top:14px;
                right:20px;
                width:25px;
                height:25px;
                cursor: pointer;
                -webkit-transform: rotate(-180deg);
                -moz-transform: rotate(-180deg);
                transform: rotate(-180deg);
                -webkit-transition: all .3s cubic-bezier(0.285, -0.450, 0.935, 0.110);
                -moz-transition: all .3s cubic-bezier(0.285, -0.450, 0.935, 0.110);
                transition: all .3s cubic-bezier(0.285, -0.450, 0.935, 0.110);
                -webkit-transition-delay: 0.2s;
                -moz-transition-delay: 0.2s;
                transition-delay: 0.2s;
				opacity:1;
            }
            .search-wrapper.active .close {
                right:-40px;
                -webkit-transform: rotate(45deg);
                -moz-transform: rotate(45deg);
                transform: rotate(45deg);
                -webkit-transition: all .6s cubic-bezier(0.000, 0.105, 0.035, 1.570);
                -moz-transition: all .6s cubic-bezier(0.000, 0.105, 0.035, 1.570);
                transition: all .6s cubic-bezier(0.000, 0.105, 0.035, 1.570);
                -webkit-transition-delay: 0.5s;
                -moz-transition-delay: 0.5s;
                transition-delay: 0.5s;
            }
            .search-wrapper .close::before, .search-wrapper .close::after {
                position:absolute;
                content:'';
                background: #27ae60;
                border-radius: 2px;
            }
            .search-wrapper .close::before {
                width: 5px;
                height: 25px;
                left: 10px;
                top: 0px;
            }
            .search-wrapper .close::after {
                width: 25px;
                height: 5px;
                left: 0px;
                top: 10px;
            }
            .search-wrapper .result-container {
                width: 100%;
                position: absolute;
                top:80px;
                left:0px;
                text-align: center;
                font-family: "Open Sans", Arial, Verdana;
                font-size: 14px;
                display:none;
                color:#B7B7B7;
            }


            @media screen and (max-width: 560px) {
                .search-wrapper.active .input-holder {width:200px;}
            }

        </style>
        
         <script type="text/javascript">
        function searchToggle(obj, evt){
            var container = $(obj).closest('.search-wrapper');
			$("#OpenOrderSearchtext").focus();
            if(!container.hasClass('active')){
                  container.addClass('active');
                  evt.preventDefault();
            }
            else if(container.hasClass('active') && $(obj).closest('.input-holder').length == 0){
                  container.removeClass('active');
                  // clear input
                  container.find('.search-input').val('');
                  // clear and hide result container when we press close
                  container.find('.result-container').fadeOut(100, function(){$(this).empty();});
            }
        }

        function submitFn(obj, evt){
            value = $(obj).find('.search-input').val().trim();

            _html = "Yup yup! Your search text sounds like this: ";
            if(!value.length){
                _html = "Yup yup! Add some text friend :D";
            }
            else{
                _html += "<b>" + value + "</b>";
            }

            $(obj).find('.result-container').html('<span>' + _html + '</span>');
            $(obj).find('.result-container').fadeIn(100);

            evt.preventDefault();
        }
        
        $("body").on("click", ".deleteOpenOrder", function()
        {			
			var id 				=	$(this).attr("for");
			swal({
					title: "Are you sure?",
					text: "You want to delete open order !",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						$.ajax({
							
								'url' 					: 	'/Linnworksapis/deleteOrder',
								'type'					: 	'POST',
								'data' 					: 	{ openorderid : id},
								'beforeSend'			: function() {
																			$('.loading-image_'+id).show();
																		 },
								'success' 				  : function( msgArray )
									{
										if ( msgArray == 1 )
										{
											$('.close').click();
											swal("Open order delete successfully.", "" , "success");											
											$('.loading-image_'+id).hide();
											$('#forRemove_'+id ).remove();
											location.reload();
										}
										if( msgArray == 2 )
										{
											$('.close').click();
											alert('There is an error.');
										}
									} 
								
							});
					}
					else
					{
						$('.close').click();
						swal("Cancelled", "Your order is safe :)", "error");
					}
			});
		});
        </script>




