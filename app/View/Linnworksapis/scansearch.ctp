<div class="form-group">
	<!--<label class="col-lg-4">Display Item Picked: <strong style="color:red;" class="picked">----</strong></label>--> 
	<label class="col-lg-4">Remaning Quantity: <strong style="color:red;" class="remaning"><?php echo $remaning_qty;?></strong></label>  
	<label class="col-lg-4">Cancelled : <strong style="color:red;" class="canceled"><?php echo $cancel_qty;?></strong></label>  
	<label class="col-lg-4">Locked : <strong style="color:red;" class="lock"><?php echo $locked_qty;?></strong></label>  
</div>
					
<table class="table table-bordered table-striped dataTable" >
	<tr>
		<th>OrderID</th>
		<th>Order</th>
		<th>Postal Service</th>
		<th>Quantity</th>
		<th>SKU</th>
		<th>Title</th>
		<th>Barcode</th>
		<th>Packaging id</th>
		<th>Packaging Name</th>
		<th>Action</th>
	</tr>	
<?php 
$userid = ( $this->Session->read('Auth.User.id') != '' ) ? $this->Session->read('Auth.User.id') : '_';

?>	
<?php  $expordr = 0; foreach($express1 as $exp ) { ?>
		 <tr <?php if($exp['MergeUpdate']['user_name'] == '' && $expordr == 0) { $expordr++; ?>  class="assigned_tr" <?php } ?>>
			<td><?php echo $exp['MergeUpdate']['order_id']; ?></td>
			<td><?php echo $exp['MergeUpdate']['product_order_id_identify']; ?></td>
			<td><?php echo $exp['MergeUpdate']['postal_service']; ?></td>
			<?php foreach($exp['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td>'.$expitem['split_qty']."<br>".'</td>'; } ?>
			<?php foreach($exp['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td>'.$expitem['split_sku']."<br>".'</td>'; } ?>
			<?php foreach($exp['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td class="productTitle"><span>'.$expitem['title']."</span>".'</td>'; } ?>
			<?php foreach($exp['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td>'.$this->Common->getLatestBarcode($expitem['split_barcode'])."<br>".'</td>';
			} ?>
			<td><?php echo $exp['MergeUpdate']['envelope_id']; ?></td>
			<td><?php echo $exp['MergeUpdate']['envelope_name']; ?></td>
			<td class="assign">
			<?php  if($exp['MergeUpdate']['user_name'] == $userid || $exp['MergeUpdate']['user_name'] == '') { ?>
			<a class="btn btn-xs btn-success" href="/linnworksapis/confirmBarcode/<?php echo $exp['MergeUpdate']['id']; ?>" ><i class="ion-clipboard"></i>
			<?php } ?>
			</td>
		</tr>
<?php } ?>



<?php $tracOrder = 0; foreach($tracked1 as $tra ) { ?>
		<tr <?php if($tra['MergeUpdate']['user_name'] == '' && $tracOrder == 0) { $tracOrder++; ?>  class="assigned_tr" <?php } ?>>
			<td><?php echo $tra['MergeUpdate']['order_id']; ?></td>
			<td><?php echo $tra['MergeUpdate']['product_order_id_identify']; ?></td>
			<td><?php echo $tra['MergeUpdate']['postal_service']; ?></td>
			<?php foreach($tra['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td>'.$expitem['split_qty']."<br>".'</td>'; } ?>
			<?php foreach($tra['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td>'.$expitem['split_sku']."<br>".'</td>'; } ?>
			<?php foreach($tra['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td class="productTitle"><span>'.$expitem['title']."</span>".'</td>'; } ?>
			<?php foreach($tra['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<td>'.$this->Common->getLatestBarcode($expitem['split_barcode'])."<br>".'</td>';
			} ?>
			<td><?php echo $tra['MergeUpdate']['envelope_id']; ?></td>
			<td><?php echo $tra['MergeUpdate']['envelope_name']; ?></td>
			<td class="assign">
			<?php  if($tra['MergeUpdate']['user_name'] == $userid || $tra['MergeUpdate']['user_name'] == '') { ?> 
			<a class="btn btn-xs btn-success" href="/linnworksapis/confirmBarcode/<?php echo $tra['MergeUpdate']['id']; ?>" ><i class="ion-clipboard"></i>
			<?php } ?>
			</td>
		</tr>
<?php } ?>


<?php  $staorder = 0; foreach($standerd1 as $sta ) { ?>
		<tr <?php if($sta['MergeUpdate']['user_name'] == ''  && $staorder == 0) { $staorder++; ?>  class="assigned_tr" <?php } ?>>
			<td><?php echo $sta['MergeUpdate']['order_id']; ?></td>
			<td><?php echo $sta['MergeUpdate']['product_order_id_identify']; ?></td>
			<td><?php echo $sta['MergeUpdate']['postal_service']; ?></td>
			<td><?php foreach($sta['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_qty']."<br>"; } ?></td>
			<td><?php foreach($sta['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_sku']."<br>"; } ?></td>
			<td class="productTitle"><?php foreach($sta['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<span>'.$expitem['title']."</span>"; } ?></td>
			<td><?php foreach($sta['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $this->Common->getLatestBarcode($expitem['split_barcode'])."<br>";
			} ?>
			</td>
			<td><?php echo $sta['MergeUpdate']['envelope_id']; ?></td>
			<td><?php echo $sta['MergeUpdate']['envelope_name']; ?></td>
			<td>
			<?php  //if($sta['MergeUpdate']['user_name'] == $userid || $sta['MergeUpdate']['user_name'] == '') { ?>
			<a class="btn btn-xs btn-success" href="/linnworksapis/confirmBarcode/<?php echo $sta['MergeUpdate']['id']; ?>" ><i class="ion-clipboard"></i>
			<?php //} ?>
			</td>
		</tr>
<?php 
			//if($sta['MergeUpdate']['user_name'] == '') { $countsum[] = 1; } 
			
			//$staorder++;
}
?>



<!-------------------------------------------- Start Code for group 2 ------------------------------------------>

<?php $expordr2 = 0; foreach($express2 as $exp2 ) { ?>
		 <tr <?php if($exp2['MergeUpdate']['user_name'] == '' && $expordr2 == 0) { $expordr2++; ?>  class="assigned_tr" <?php } ?>>
			<td><?php echo $exp2['MergeUpdate']['order_id']; ?></td>
			<td><?php echo $exp2['MergeUpdate']['product_order_id_identify']; ?></td>
			<td><?php echo $exp2['MergeUpdate']['postal_service']; ?></td>
			<td><?php foreach($exp2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_qty']."<br>"; } ?></td>
			<td><?php foreach($exp2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_sku']."<br>"; } ?></td>
			<td class="productTitle"><?php foreach($exp2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<span>'.$expitem['title']."</span>"; } ?></td>
			<td><?php foreach($exp2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $this->Common->getLatestBarcode($expitem['split_barcode'])."<br>";
			} ?>
			</td>
			<td><?php echo $exp2['MergeUpdate']['envelope_id']; ?></td>
			<td><?php echo $exp2['MergeUpdate']['envelope_name']; ?></td>
			<td class="assign">
			<?php  if($exp2['MergeUpdate']['user_name'] == $userid || $exp2['MergeUpdate']['user_name'] == '') { ?>
			<a class="btn btn-xs btn-success" href="/linnworksapis/confirmBarcode/<?php echo $exp2['MergeUpdate']['id']; ?>" ><i class="ion-clipboard"></i>
			<?php } ?>
			</td>
		</tr>
<?php } ?>


<?php $tracOrders2 = 0; foreach($tracked2 as $tra2 ) { ?>
		<tr <?php if($tra2['MergeUpdate']['user_name'] == '' && $tracOrders2 == 0) {$tracOrders2++;  ?>  class="assigned_tr" <?php } ?>>
			<td><?php echo $tra2['MergeUpdate']['order_id']; ?></td>
			<td><?php echo $tra2['MergeUpdate']['product_order_id_identify']; ?></td>
			<td><?php echo $tra2['MergeUpdate']['postal_service']; ?></td>
			<td><?php foreach($tra2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_qty']."<br>"; } ?></td>
			<td><?php foreach($tra2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_sku']."<br>"; } ?></td>
			<td class="productTitle"><?php foreach($tra2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<span'.$expitem['title']."</span>"; } ?></td>
			<td><?php foreach($tra2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $this->Common->getLatestBarcode($expitem['split_barcode'])."<br>";
			} ?>
			</td>
			<td><?php echo $tra2['MergeUpdate']['envelope_id']; ?></td>
			<td><?php echo $tra2['MergeUpdate']['envelope_name']; ?></td>
			<td class="assign">
			<?php  if($tra2['MergeUpdate']['user_name'] == $userid || $tra2['MergeUpdate']['user_name'] == '') { ?>
			<a class="btn btn-xs btn-success" href="/linnworksapis/confirmBarcode/<?php echo $tra2['MergeUpdate']['id']; ?>" ><i class="ion-clipboard"></i>
			<?php } ?>
			</td>
		</tr>
<?php } ?>


<?php $sta2order = 0; foreach($standerd2 as $sta2 ) {  ?>
		<tr <?php if($sta2['MergeUpdate']['user_name'] == '' && $sta2order == 0) { $sta2order++; ?>  class="assigned_tr" <?php } ?>>
			<td><?php echo $sta2['MergeUpdate']['order_id']; ?></td>
			<td><?php echo $sta2['MergeUpdate']['product_order_id_identify']; ?></td>
			<td><?php echo $sta2['MergeUpdate']['postal_service']; ?></td>
			<td><?php foreach($sta2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_qty']."<br>"; } ?></td>
			<td><?php foreach($sta2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $expitem['split_sku']."<br>"; } ?></td>
			<td class="productTitle"><?php foreach($sta2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo '<span>'.$expitem['title']."</span>"; } ?></td>
			<td><?php foreach($sta2['MergeUpdate']['ItemList'] as $expitem ) 
			{ 
			echo $this->Common->getLatestBarcode($expitem['split_barcode'])."<br>";
			} ?>
			</td>
			<td><?php echo $sta2['MergeUpdate']['envelope_id']; ?></td>
			<td><?php echo $sta2['MergeUpdate']['envelope_name']; 
			echo $sta2['MergeUpdate']['user_id'];
			?></td>
			<td class="assign">
			<?php  //if($sta2['MergeUpdate']['user_name'] == $userid || $sta2['MergeUpdate']['user_name'] == '') { ?>
			<a class="assigned_btn btn btn-xs btn-success" href="/linnworksapis/confirmBarcode/<?php echo $sta2['MergeUpdate']['id']; ?>" ><i class="ion-clipboard"></i>
			<?php //} ?>
			</td>
		</tr>
<?php } ?>
<!-------------------------------------------- End Code for group 2 ------------------------------------------>
</tbody>
</table>
 
<style>
/*.assigned_tr{background-color:rgb(196, 235, 255) !important;}*/
.assigned_tr:nth-child(2){background-color:rgb(196, 235, 255) !important;}
</style>

