<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Incomplete Orders</h1>
			<?php print $this->Session->flash();  ?>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		
    </div>
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-body no-padding-top bg-white">											
									<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th style="width: 2%;" >Order Id</th>
												<th style="width: 8%;">Reference No.</th>
												<th style="width: 8%;">Sub Source</th>
												<th style="width: 8%;">Date</th>
												<th style="width: 8%;">Action</th>
											</tr>	
										</thead>
									<tbody role="alert" aria-live="polite" aria-relevant="all">
										    <?php foreach( $getdetails as $index => $value ) { //pr( $value ); ?>
										    <tr class="odd">
												<td class="  sorting_1"><?php echo $value['num_order_id']; ?></td>
												<td class="  sorting_1"><?php echo $value['general_info']->ReferenceNum; ?></td>                                                
												<td class="  sorting_1"><?php echo $value['general_info']->SubSource; ?></td>                                                
												<td class="  sorting_1"><?php echo $value['general_info']->ReceivedDate; ?></td>                                                
												<td class="  sorting_1">
													<a href="/Linnworksapis/deleteIncompleteOrders/<?php echo $value['num_order_id']; ?>" class="btn-danger btn-xs margin-right-10"><i class="ion-minus-circled"></i></a>
												</td>                                                
										    </tr>
										    <?php } ?>
										</tbody>
									</table>
								</div>
						    </div>
                        </div>
                    </div>
				<footer class="bg-white">
					<div class="pull-left">
						<span class="pull-left margin-right-15">&copy; 2015 WMS by JijGroup.</span>
						<ul class="list-inline pull-left">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
						</ul>
					</div>
				</footer>
		    </div>
    </div>
</div>
<div class="showPopupForAssignSlip"></div> 
<div class="showPopupForcomment"></div> 

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>







