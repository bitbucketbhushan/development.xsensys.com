<div class="modal oversell_popup fade pop-up-4" tabindex="-1" role="dialog" id="pop-up-4" aria-labelledby="myLargeModalLabel-4" aria-hidden="true">
	<div class="modal-dialog" style="width:1250px;">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Over Sell Order List</h4>
         </div>
         <div class="modal-body bg-grey-100">
            <div class="panel no-margin subOrderBlock" style="clear:both">
     <div class="panel-body padding-top-20" >
     <div class="row">
		 <table class="table table-bordered table-striped dataTable">
						<tr>
							<th width="9%">OrderID</th>
							<th width="9%">Store</th>
							<th width="9%">Postal</th>																
							<th width="9%">Location</th>																
							<th width="10%">SKU</th>									
							<th width="20%">Title</th>
							<th width="6%">Total (EUR)</th>
							<th width="5%">Total</th>
							<th width="5%">Curr</th>
							<th width="7%">Date</th>
						</tr>
						
						<?php
							
							App::import('controller', 'Currents');
							$currentObj	=	new CurrentsController();
							
							App::import('controller', 'Linnworksapis');
							$linnworkObj	=	new LinnworksapisController();
							
							if( $result != 0 )
							{															
								foreach( $result as $data ):
						?>
									<tr style="border-bottom: 1px solid #ccc;" class ="<?php echo $backgroud; ?>" >									
										<td>
											<?php										
												printf("%s", $data->MergeUpdate->product_order_id_identify );
											?>	
										</td>
										<td>
											<?php
												App::import( 'Controller' , 'Products' );
												$prod = new ProductsController();
												$openOrderData = $prod->getPostalName( $data->MergeUpdate->order_id );
												$curr = unserialize( $openOrderData->OpenOrder->totals_info )->Currency;
												$rDate = unserialize( $openOrderData->OpenOrder->general_info )->ReceivedDate;										
												printf("%s", $openOrderData->OpenOrder->sub_source );
											?>	
										</td>
										<td>
											<?php
												printf("%s", $data->MergeUpdate->postal_service );
											?>	
										</td>
										<td>
											<?php
												$objectSkuLocations = $this->Common->getSkuBinLocation( $data->MergeUpdate->barcode );	
												$binLocationNow = $objectSkuLocations[0]['binLocation'];
												printf("%s", $binLocationNow );
											?>	
										</td>
										<td>
											<?php
												printf("%s", $data->MergeUpdate->sku );
											?>	
										</td>
										<td class="productTitle">
											<?php $titles = $linnworkObj->getTitle( $data->MergeUpdate->sku ); ?>
											<?php foreach($titles as $title) {  ?>
												<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>											
										</td>
										<td>
											<?php
												echo $currentObj->getCurrentRate( $curr , 'EUR', $data->MergeUpdate->price );
											?>	
										</td>
										<td>
											<?php
												printf("%s", $data->MergeUpdate->price );
											?>	
										</td>
										<td>
											<?php
												printf("%s", $curr );
											?>	
										</td>
										<td>
											<?php
												printf("%s", $rDate );
											?>	
										</td>
										
									</tr>	
						<?php
								endforeach;
							}
							else
							{								
						?>	
								<tr style="border-bottom: 1px solid #ccc;" class ="<?php echo $backgroud; ?>" >									
										<td>
											<?php										
												printf("%s", $data->MergeUpdate->product_order_id_identify );
											?>	
										</td>
										<td>
											<?php
												App::import( 'Controller' , 'Products' );
												$prod = new ProductsController();
												//$openOrderData = $prod->getPostalName( $data->MergeUpdate->order_id );
												//$curr = unserialize( $openOrderData->OpenOrder->totals_info )->Currency;
												//$rDate = unserialize( $openOrderData->OpenOrder->general_info )->ReceivedDate;										
												//printf("%s", $openOrderData->OpenOrder->sub_source );
											?>	
										</td>
										<td>
											<?php
												//printf("%s", $data->MergeUpdate->postal_service );
											?>	
										</td>
										<td>
											<?php
												$objectSkuLocations = $this->Common->getSkuBinLocation( $data->MergeUpdate->barcode );	
												$binLocationNow = $objectSkuLocations[0]['binLocation'];
												//printf("%s", $binLocationNow );
											?>	
										</td>
										<td>
											<?php
												//printf("%s", $data->MergeUpdate->sku );
											?>	
										</td>
										<td class="productTitle">
											<?php $titles = $linnworkObj->getTitle( $data->MergeUpdate->sku ); ?>
											<?php foreach($titles as $title) {  ?>
												<?php printf("%s", "No records found" ); ?>
											<?php } ?>											
										</td>
										<td>
											<?php
												//echo $currentObj->getCurrentRate( $curr , 'EUR', $data->MergeUpdate->price );
											?>	
										</td>
										<td>
											<?php
												//printf("%s", $data->MergeUpdate->price );
											?>	
										</td>
										<td>
											<?php
												//printf("%s", $curr );
											?>	
										</td>
										<td>
											<?php
												//printf("%s", $rDate );
											?>	
										</td>
										
									</tr>	
						<?php
							}
						?>	
						
								
					</table>  
		 </div>

		
	   <div class="row">
         <div class="col-lg-12" style="text-align:center; padding-bottom: 7px;"> 
         
         </div>
        </div>
       </div>
      </div> 
     </div>
    </div>
   </div>
  </div>
 </div>






<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>



<script>
	
   $(document).ready(function()
   {
       $('.oversell_popup').modal('show');
       
       $('.subOrderBlock>.panel-body').slimScroll({
			height: '475px',
			width:'100%',
			size:'3',
			alwaysVisible: true
		});
    	 
   });
 
</script>
