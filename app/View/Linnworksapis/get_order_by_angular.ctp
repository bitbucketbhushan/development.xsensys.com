<div class="top_panel rightside bg-grey-100" style="display:none;">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php echo "Open Order"; ?></h1>
        		<div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
							<li>
									<?php
										echo  $this->form->create('Linnworksapis', array( 'class'=>'form-horizontal bv-form', 'type'=>'post','id'=>'getFilterOrderCsv','enctype'=>'multipart/form-data', 'action' => 'generatePickList'));
										echo  $this->form->hidden('Linnworksapis.orderid', array('class' => 'get_sku_string' ));
										//echo  $this->Form->button('Pick List', array('type' => 'submit', 'escape' => true,'class'=>'btn btn-success'));
										echo  $this->form->end();
									?>
							</li>
							<!--<li>
								<a class="downloadPickList btn btn-success btn-xs margin-right-10 color-white" href="javascript:void(0)" ><i class="fa fa-download"></i> Pick List</a>
							</li>-->
							<li>
								<a class="downloadStockFile btn btn-success btn-xs margin-right-10 color-white"><i class="fa fa-download"></i> Download CSV</a>
							</li>
							<li>
								<a class="switch btn btn-success" href="javascript:void(0)" data-toggle="modal" data-target=""><i class=""></i> Switch </a>
							</li>
							<li>
								<a href="/linnworksapis" type="submit" class="printorder btn bg-orange-500 color-white btn-dark padding-left-40 padding-right-40" >Go Back</a>
							</li>
						</ul>
					</div>
				</div>
				
			<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			<div class="api_message" style="display:none; color:green; padding 10px 0 0 10px;"></div>
    </div>
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">									
									<div class="panel-tools">																	
									</div>
								</div>
							   <div class="panel-body no-padding-top bg-white">
							    		<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
										<thead class="parentCheck" >
											<tr role="row" class="parentInner" >
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand: activate to sort column descending">Order no.</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand: activate to sort column descending">SKU</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand: activate to sort column descending">Currency</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand: activate to sort column descending">Total</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand: activate to sort column descending">Action</th>
											</tr>
										</thead>
										<tbody role="alert" aria-live="polite" aria-relevant="all">
                                            <?php 
												App::import('Controller', 'Linnworksapis');
												$checkproduct =	new LinnworksapisController;
												$qty	=	$checkproduct->checkInventery();
											?>
                                            <?php $i = 1;
												foreach($results as $result)
													{
														$id	=	$result->OrderId;
                                            ?>
                                            <tr class="odd">
												<td class="  sorting_1">
												<!--<input type="hidden" class ="case" name="skutouploadID" id="<?php echo $result->NumOrderId; ?>" onclick="get_sku();" >-->
												<?php echo $result->NumOrderId; ?>
												</td>
												<td class="sorting_1 pick_sku" >
													<?php 
														 foreach($result->Items as $item)
														  { 
														   //echo "<b>".$item->MergeUpdate->quantity."</b> X&nbsp; ".$item->MergeUpdate->sku ."</br>";
                                                                                                                   echo $item->MergeUpdate->sku ."</br>";  
														   echo "<input type=hidden class=get_hidden_sku id=" .$item->MergeUpdate->sku ."  />";
														 ?>
														 	<input type="hidden" class ="case" name="skutouploadID" id="<?php echo $item->MergeUpdate->product_order_id_identify; ?>" > 
														 
													
													<?php   
														}
													?>
												</td>
												<td class="  sorting_1"><?php echo $result->TotalsInfo->Currency; ?></td>
												<td class="  sorting_1"><?php echo $result->TotalsInfo->TotalCharge; ?></td>
												<td class="  sorting_1">
													<ul id="icons" class="iconButtons">
														<input type="hidden" value="<?php echo $id; ?>" class="processid" />
														<a href="/Linnworksapis/getOrderDetail/<?php echo $result->NumOrderId; ?>" title="Show Order Description" class="btn btn-success btn-xs margin-right-10"><i class="ion-android-desktop"></i></a>
														<!--<a href="javascript:void(0);" for="<?php echo $id; ?>##$#<?php echo "00000000-0000-0000-0000-000000000000"; ?>##$#<?php echo $result->TotalsInfo->TotalCharge; ?>" title="Cancel Order" class="ordercancel btn btn-danger btn-xs margin-right-10" ><i class="ion-minus-round"></i></a>
														<a class="loading-image_<?php echo $id; ?>" style="display:none;"><img src="http://localhost/app/webroot/img/image_889915.gif" hight ="20" width="20" /></a>-->
															<?php 	
															$instock = '0';
															$outstock = '0';
															foreach($result->Items as $item)
																{ 
																	$qty	=	$checkproduct->checkInventery( $item->MergeUpdate->sku );
																	if($qty == 1)
																	{
																		$instock++;
																	}
																	elseif($qty == 2)
																	{
																		$outstock++;
																	}
																} ?>
															
															<?php if($instock > 0 && $outstock == 0) { ?>
																 <!--<a href="javascript:void(0);" for="" title="In Stock" class="btn btn-success btn-xs margin-right-10" ><i class="fa fa-sign-in"></i></a>-->
															<?php }	elseif($outstock > 0 && $instock == 0) {  ?>
																 <!--<a href="javascript:void(0);" for="" title="Out Stock" class="btn btn-danger btn-xs margin-right-10" ><i class="fa fa-sign-out"></i></a>-->
															<?php }	elseif($instock > 0 && $outstock > 0) { ?>
																 <!--<a href="javascript:void(0);" for="" title="Out Stock" class="btn palette-sun-flower btn-xs margin-right-10" ><i class="fa fa-sign-in"></i></a>-->
															<?php }  ?>
															
													</ul>
												</td>
										    </tr>
                                            <?php $i++; } ?>
                                      </tbody>
									</table>		
								</div>
                            </div>
                        </div>
                    </div>
					<?php echo $this->element('footer'); ?>
            </div>
    </div>

<!------------------------------------ Open Order -------------------------------------------------------------->

<!------------------------------------ Start for arrange order ------------------------------------------------->
<div class="bottom_panel rightside bg-grey-100" style="display:block;" >
    <div class="page-head bg-grey-100">
		 <h1 class="page-title"><?php echo "Open Order";
					     ?></h1>
				<div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
							<li>
							<?php
								echo  $this->form->create('Linnworksapis', array( 'class'=>'form-horizontal bv-form', 'type'=>'post','id'=>'getFilterOrderCsv','enctype'=>'multipart/form-data', 'action' => 'generatePickList'));
								echo  $this->form->hidden('Linnworksapis.orderid', array('class' => 'get_sku_string' ));
								//echo  $this->Form->button('Pick List', array('type' => 'submit', 'escape' => true,'class'=>'btn btn-success'));
								echo  $this->form->end();
							?>
							</li>
							<li>
										<div class="btn-group">
										  <button type="button"  href="javascript:void(0);" class="downloadPickList btn btn-success no-margin">Pick List</button>
										  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										  </button>
										  <?php if(count($files) > 0 ) { ?>
										  <ul class="dropdown-menu scrollable-menu" role="menu">
										  <?php 
											$pdfPath = '/img/printPickList/'; 
											foreach( $files as $file ) {
												$time = explode('_', $file) ;
												$minute = explode('.', $time[7]) ;
											?>
											<li><a href="<?php echo $pdfPath.$file; ?>" target ="_blank" >Pick List ( <?php echo $time[6].' : '.$minute[0]; ?> )</a></li>
											<?php } ?>
											<li role="separator"></li>
										  </ul>
										  <?php } ?>
										</div>
							</li>
							<!--<li>
								<a class="downloadPickList btn btn-success btn-xs margin-right-10 color-white" href="javascript:void(0)" ><i class="fa fa-download"></i> Pick List</a>
							</li>-->
							<li>
								<a class="btn btn-success" href="/JijGroup/Generic/Order/GetOpenFilter" data-toggle="modal" data-target=""><i class=""></i> Refresh </a>
							</li>
							<li>
								<a class="downloadStockFile btn btn-success" href="javascript:void(0)" data-toggle="modal" data-target=""><i class=""></i> Virtual Stock </a>
							</li>
							<li>
								<a class="switch btn btn-success" href="javascript:void(0)" data-toggle="modal" data-target=""><i class=""></i> Switch </a>
							</li>
							<li>
								<a class="btn btn-xs btn-success" href="/linnworksapis/dispatchConsole" data-toggle="modal" data-target="">Despatch Console</a>
							</li>	
							<li>
								<a href="/linnworksapis" type="submit" class="printorder btn bg-orange-500 color-white btn-dark padding-left-40 padding-right-40" >Go Back</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="panel-title no-radius bg-green-500 color-white no-border">
					<div class="panel-head"><?php print $this->Session->flash(); ?></div>
				</div>
    </div>
   
    <div class="container-fluid" >
        <div class="row">
		
		    <div class="col-lg-12 rackDetails">
				
              <div class="panel">
					<div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12 ">
						<section id="blur-in">
          <div class="animated-tab">
            <article class="tab-container">
              <nav class="tab-nav">
                <li><a href="#group1" title="Tab 1">Group 1</a></li>
                <li><a href="#group2" title="Tab 2">Group 2</a></li>
              </nav>
              <div class="tab-pane-container">
                <section id="group1">
						
						<div class="overlay">&nbsp;</div>
							<table class="table table-bordered table-striped dataTable">
								<tr>
									<th width="9%">OrderID</th>
									<th width="10%">Postal</th>
									<th width="13%">SKU</th>
									<th width="21%">Title</th>
									<th width="9%">Total (EUR)</th>
									<th width="5%">Total</th>
									<th width="5%">Cur.</th>
									<th width="9%">Date</th>
									<th width="19%">Action</th>
								</tr>
								
								<!-- For Express with single SKU Group A -->
								<?php 
								App::import('controller', 'Currents');
								$currentObj	=	new CurrentsController();
								
								App::import('controller', 'Linnworksapis');
								$linnworkObj	=	new LinnworksapisController();
																
								foreach($express1 as $expressOne)
								{
								
									foreach($expressOne->Items as $item):
									$titles = $linnworkObj->getTitle( $item->MergeUpdate->sku );
									$pickListStatus	=	$checkproduct->checkPickList( $item->MergeUpdate->product_order_id_identify );
									if( $pickListStatus == 1)
									{
										$backgroud = "bg-orange-100";
									}
									else
									{
										$backgroud = "";
									}
								
									?>
									
										<tr style="border-bottom: 1px solid #ccc;" class ="<?php echo $backgroud; ?>" >									
											<td>
											<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_id; ?></div></td><td class='row'><div class='col-sm-6'><b>Postal Service</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Warehouse</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->warehouse; ?></div></td><td class='row'><div class='col-sm-6'><b>Delivery Country</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->delevery_country; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Delivery Label</b></div><div class='col-sm-6'><?php echo $item->Template->label_name; ?></div></td><td class='row'><div class='col-sm-6'><b>Service Provider</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Provider Code</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->provider_ref_code; ?></div></td><td class='row'><div class='col-sm-6'><b>Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_weight; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Length</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_length; ?></div></td><td class='row'><div class='col-sm-6'><b>Width</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Height</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_height; ?></div></td><td class='row'><div class='col-sm-6'><b>Packaging Slip</b></div><div class='col-sm-6'><?php echo $item->PackagingSlip->name; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b>Service Name</b></div><div class='col-sm-11'><?php echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'><table><tr><td width='25%' class='row'><div class='col-sm-6'><b>Packaging ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_id; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_weight; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Cost</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_cost; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Class</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packaging_type; ?></div></td></tr>
											<tr><td colspan='4' width='100%' class='row'><div class='col-sm-2'><b>Packaging Name</b></div><div class='col-sm-10'><?php echo $item->MergeUpdate->envelope_name; ?></div></td></tr></table></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
												</a>
											<a class="moreDetails" href="#" >
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														echo $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														echo $item->MergeUpdate->order_id;
													}
												?>
												</a>
											</td>
											<td><?php echo $expressOne->ShippingInfo->PostalServiceName; ?></td>								
											<td>										
												<?php
													//pack_order_quantity
													//pr($standardOne->Items); exit;
													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															}
															else
															{
																echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															}
														}
													}
													
												?>
											</td>
											<td class="productTitle">
											<?php foreach($titles as $title) {  ?>
											<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>											
											</td>
											<td>
												<?php 
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price * $item->MergeUpdate->quantity, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
															else
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
														}
													}
												?>
											</td>
											<td>
											<?php
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																echo $item->MergeUpdate->price * $item->MergeUpdate->quantity;
															}
															else
															{
																echo $item->MergeUpdate->price;
															}
														}
													}										
											?>
											</td>											
											<td>
											<?php echo $expressOne->TotalsInfo->Currency; ?>
											</td>
											<td>
											<?php 
												$expressDate=$expressOne->GeneralInfo->ReceivedDate;
												$trackedDate=explode('T', $expressDate);
												echo $trackedDate[0]."<br>(".$trackedDate[1].")";
											?>
											</td>
											<td>
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														$ord_id = $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														$ord_id = $item->MergeUpdate->order_id;
													} ?>
												<!--<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".pop-up-<?php print $ord_id; ?>">
													<i class="ion-clipboard"></i>
												</a>-->
												<a rel="tooltip" class="calcel_order btn btn-xs btn-danger" href="javascript:void(0); " title = "Cancel Order" data-toggle="modal" data-target=".cancel_pop-up-<?php echo $expressOne->OrderId; ?>" for = "<?php echo $expressOne->NumOrderId; ?>" >Cancel Order</a>
												<a rel="tooltip" class="for_tooltip btn btn-xs btn-success" href="javascript:void(0); " title = "Assign Custom Service" data-toggle="modal" data-target=".delivery_labels_pop-up-<?php echo $item->MergeUpdate->id; ?>" >Service Assign</a>
											</td>
										</tr>
								<?php
								endforeach;	
								?>
								<?php } ?>
								
								<!-- For Tracked with single SKU Group A -->
								<?php 
								foreach($tracked1 as $trackedOne)
								{
								?> 
									<?php
									foreach($trackedOne->Items as $item):
									$titles = $linnworkObj->getTitle( $item->MergeUpdate->sku );
									$pickListStatus	=	$checkproduct->checkPickList( $item->MergeUpdate->product_order_id_identify );
									if( $pickListStatus == 1)
									{
										$backgroud = "bg-orange-100";
									}
									else
									{
										$backgroud = "";
									}
									?>
										<tr style="border-bottom: 1px solid #ccc;" class ="<?php echo $backgroud; ?>" >									
											<td>
											<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_id; ?></div></td><td class='row'><div class='col-sm-6'><b>Postal Service</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Warehouse</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->warehouse; ?></div></td><td class='row'><div class='col-sm-6'><b>Delivery Country</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->delevery_country; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Delivery Label</b></div><div class='col-sm-6'><?php echo $item->Template->label_name; ?></div></td><td class='row'><div class='col-sm-6'><b>Service Provider</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Provider Code</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->provider_ref_code; ?></div></td><td class='row'><div class='col-sm-6'><b>Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_weight; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Length</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_length; ?></div></td><td class='row'><div class='col-sm-6'><b>Width</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Height</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_height; ?></div></td><td class='row'><div class='col-sm-6'><b>Packaging Slip</b></div><div class='col-sm-6'><?php echo $item->PackagingSlip->name; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b>Service Name</b></div><div class='col-sm-11'><?php echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'><table><tr><td width='25%' class='row'><div class='col-sm-6'><b>Packaging ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_id; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_weight; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Cost</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_cost; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Class</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packaging_type; ?></div></td></tr>
											<tr><td colspan='4' width='100%' class='row'><div class='col-sm-2'><b>Packaging Name</b></div><div class='col-sm-10'><?php echo $item->MergeUpdate->envelope_name; ?></div></td></tr></table></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
												</a>
											<a class="moreDetails" href="#" >
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														echo $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														echo $item->MergeUpdate->order_id;
													}
												?>
												</a>
											</td>
											<td><?php echo $trackedOne->ShippingInfo->PostalServiceName; ?></td>								
											<td>
												<?php
													//pack_order_quantity
													//pr($standardOne->Items); exit;
													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($trackedOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($trackedOne->Items) == 1 )
															{
																echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															}
															else
															{
																echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															}
														}
													}
													
												?>
											</td>
											<td class="productTitle">
											<?php foreach($titles as $title) {  ?>
											<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>
											
											
											</td>
											<td>
												<?php 
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($trackedOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $trackedOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $trackedOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $trackedOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $trackedOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($trackedOne->Items) == 1 )
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price , $trackedOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $trackedOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
															else
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $trackedOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $trackedOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
														}
													}
												?>
											</td>
											<td>
											<?php
												//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
												if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
												{
													if( count($trackedOne->Items) == 1 )
													{
														echo $item->MergeUpdate->price;
													}
													else
													{
														echo $item->MergeUpdate->price;
													}
												}
												else
												{
													if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($trackedOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
												}										
											?>
											</td>
											<td>
											<?php echo $trackedOne->TotalsInfo->Currency; ?>
											</td>
											<td>
											<?php 
												$expressDate=$trackedOne->GeneralInfo->ReceivedDate;
												$trackedDate=explode('T', $expressDate);
												echo $trackedDate[0]."<br>(".$trackedDate[1].")";
											?>
											</td>
											<td>
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														$ord_id = $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														$ord_id = $item->MergeUpdate->order_id;
													} ?>
												<!--<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".pop-up-<?php print $ord_id; ?>">
													<i class="ion-clipboard"></i>
												</a>-->
												<a rel="tooltip" class="calcel_order btn btn-xs btn-danger" href="javascript:void(0); " title = "Cancel Order" data-toggle="modal" data-target=".cancel_pop-up-<?php echo $trackedOne->OrderId; ?>" for = "<?php echo $expressOne->NumOrderId; ?>" >Cancel Order</a>
												<a rel="tooltip" class="for_tooltip btn btn-xs btn-success" href="javascript:void(0); " title = "Assign Custom Service" data-toggle="modal" data-target=".delivery_labels_pop-up-<?php echo $item->MergeUpdate->id; ?>" >Service Assign</a>
											</td>
										</tr>
								<?php
								endforeach;	
								?>
								<?php } ?>
								
								<!-- For Standard with single SKU Group A  -->
								<?php 
								foreach($standerd1 as $standardOne) 
								{
									
								
									foreach($standardOne->Items as $item):
									$titles = $linnworkObj->getTitle( $item->MergeUpdate->sku );
									$pickListStatus	=	$checkproduct->checkPickList( $item->MergeUpdate->product_order_id_identify );
									if( $pickListStatus == 1)
									{
										$backgroud = "bg-orange-100";
									}
									else
									{
										$backgroud = "";
									}
									?>
										<tr style="border-bottom: 1px solid #ccc;" class ="<?php echo $backgroud; ?>" >									
											<td>
											<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_id; ?></div></td><td class='row'><div class='col-sm-6'><b>Postal Service</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Warehouse</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->warehouse; ?></div></td><td class='row'><div class='col-sm-6'><b>Delivery Country</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->delevery_country; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Delivery Label</b></div><div class='col-sm-6'><?php echo $item->Template->label_name; ?></div></td><td class='row'><div class='col-sm-6'><b>Service Provider</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Provider Code</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->provider_ref_code; ?></div></td><td class='row'><div class='col-sm-6'><b>Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_weight; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Length</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_length; ?></div></td><td class='row'><div class='col-sm-6'><b>Width</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Height</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_height; ?></div></td><td class='row'><div class='col-sm-6'><b>Packaging Slip</b></div><div class='col-sm-6'><?php echo $item->PackagingSlip->name; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b>Service Name</b></div><div class='col-sm-11'><?php echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'><table><tr><td width='25%' class='row'><div class='col-sm-6'><b>Packaging ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_id; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_weight; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Cost</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_cost; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Class</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packaging_type; ?></div></td></tr>
											<tr><td colspan='4' width='100%' class='row'><div class='col-sm-2'><b>Packaging Name</b></div><div class='col-sm-10'><?php echo $item->MergeUpdate->envelope_name; ?></div></td></tr></table></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
												</a>
											<a class="moreDetails" href="#" >
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														echo $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														echo $item->MergeUpdate->order_id;
													}
												?>
												</a>
											</td>
											<td><?php echo $item->MergeUpdate->postal_service; ?></td>								
											<td>
												<?php
													//pack_order_quantity
													//pr($standardOne->Items); exit;
													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($standardOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($standardOne->Items) == 1 )
															{
																echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															}
															else
															{
																echo str_replace( ',','<br />',$item->MergeUpdate->sku );
															}
														}
													}
													
												?>
											</td>
											<td class="productTitle">
											<?php foreach($titles as $title) {  ?>
											<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>
											</td>
											<td>
												<?php 
												//echo $standardOne->TotalsInfo->Currency .'EUR';
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($standardOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $standardOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $standardOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($standardOne->Items) == 1 )
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $standardOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
															else
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $standardOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
														}
													}
												?>
											</td>
											<td>
											<?php
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($standardOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($standardOne->Items) == 1 )
															{
																echo $item->MergeUpdate->price;
															}
															else
															{
																echo $item->MergeUpdate->price;
															}
														}
													}										
											?>
											</td>
											<td>
											<?php echo $standardOne->TotalsInfo->Currency; ?>
											</td>											
											<td>
											<?php 
												$expressDate=$standardOne->GeneralInfo->ReceivedDate;
												$trackedDate=explode('T', $expressDate);
												echo $trackedDate[0]."<br>(".$trackedDate[1].")";
											?>
											</td>
											<td>
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														$ord_id = $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														$ord_id = $item->MergeUpdate->order_id;
													} ?>
												<!--<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".pop-up-<?php print $ord_id; ?>">
													<i class="ion-clipboard"></i>
												</a>-->
												<a rel="tooltip" class="calcel_order btn btn-xs btn-danger" href="javascript:void(0); " title = "Cancel Order" data-toggle="modal" data-target=".cancel_pop-up-<?php echo $standardOne->OrderId; ?>" for = "<?php echo $expressOne->NumOrderId; ?>" >Cancel Order</a>
												<a rel="tooltip" class="for_tooltip btn btn-xs btn-success" href="javascript:void(0); " title = "Assign Custom Service" data-toggle="modal" data-target=".delivery_labels_pop-up-<?php echo $item->MergeUpdate->id; ?>" >Service Assign</a>
											</td>
										</tr>
								<?php
								endforeach;	
								?>
								<?php } ?>
								
							</table>
							<br><br><br>
							</section>
							<section id="group2">
							<div class="overlay">&nbsp;</div>
							<table class="table table-bordered table-striped dataTable">
								<tr>
									<th width="9%">OrderID</th>
									<th width="10%">Postal</th>
									<th width="13%">SKU</th>
									<th width="21%">Title</th>
									<th width="9%">Total (EUR)</th>
									<th width="5%">Total</th>
									<th width="5%">Cur.</th>
									<th width="9%">Date</th>
									<th width="19%">Action</th>
								</tr>
								
								<!-- For Express with single SKU Group B -->
								<?php 
								foreach($express2 as $expressOne)
								{
								?> 
									<?php
									foreach($expressOne->Items as $item):
									$titles = $linnworkObj->getTitle( $item->MergeUpdate->sku );
									$pickListStatus	=	$checkproduct->checkPickList( $item->MergeUpdate->product_order_id_identify );
									if( $pickListStatus == 1)
									{
										$backgroud = "bg-orange-100";
									}
									else
									{
										$backgroud = "";
									}
									?>
										<tr style="border-bottom: 1px solid #ccc;"  class ="<?php echo $backgroud; ?>">									
											<td>
											<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_id; ?></div></td><td class='row'><div class='col-sm-6'><b>Postal Service</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Warehouse</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->warehouse; ?></div></td><td class='row'><div class='col-sm-6'><b>Delivery Country</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->delevery_country; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Delivery Label</b></div><div class='col-sm-6'><?php echo $item->Template->label_name; ?></div></td><td class='row'><div class='col-sm-6'><b>Service Provider</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Provider Code</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->provider_ref_code; ?></div></td><td class='row'><div class='col-sm-6'><b>Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_weight; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Length</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_length; ?></div></td><td class='row'><div class='col-sm-6'><b>Width</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Height</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_height; ?></div></td><td class='row'><div class='col-sm-6'><b>Packaging Slip</b></div><div class='col-sm-6'><?php echo $item->PackagingSlip->name; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b>Service Name</b></div><div class='col-sm-11'><?php echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'><table><tr><td width='25%' class='row'><div class='col-sm-6'><b>Packaging ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_id; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_weight; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Cost</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_cost; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Class</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packaging_type; ?></div></td></tr>
											<tr><td colspan='4' width='100%' class='row'><div class='col-sm-2'><b>Packaging Name</b></div><div class='col-sm-10'><?php echo $item->MergeUpdate->envelope_name; ?></div></td></tr></table></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
												</a>
											<a class="moreDetails" href="#" >
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														echo $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														echo $item->MergeUpdate->order_id;
													}
												?>
												</a>
											</td>
											<td><?php echo $expressOne->ShippingInfo->PostalServiceName; ?></td>								
											<td>
												<?php
													//pack_order_quantity
													//pr($standardOne->Items); exit;
													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else
													{
														
													}
													
												?>
											</td>
											<td class="productTitle">
											<?php foreach($titles as $title) {  ?>
											<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>
											</td>
											<td>
												<?php 
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency((($item->MergeUpdate->quantity / $item->MergeUpdate->pack_order_quantity) * $item->MergeUpdate->price), $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price * $item->MergeUpdate->quantity, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
															else
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
														}
													}
												?>
											</td>
											<td>
											<?php
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																echo $item->MergeUpdate->price * $item->MergeUpdate->quantity;
															}
															else
															{
																echo $item->MergeUpdate->price;
															}
														}
													}
																						
											?>
											</td>
											<td>
											<?php echo $expressOne->TotalsInfo->Currency; ?>
											</td>
											<td>
											<?php 
												$expressDate=$expressOne->GeneralInfo->ReceivedDate;
												$trackedDate=explode('T', $expressDate);
												echo $trackedDate[0]."<br>(".$trackedDate[1].")";
											?>
											</td>
											<td>
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														$ord_id = $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														$ord_id = $item->MergeUpdate->order_id;
													} ?>
												<!--<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".pop-up-<?php print $ord_id; ?>">
													<i class="ion-clipboard"></i>
												</a>-->
												<a rel="tooltip" class="calcel_order btn btn-xs btn-danger" href="javascript:void(0); " title = "Cancel Order" data-toggle="modal" data-target=".cancel_pop-up-<?php echo $expressOne->OrderId; ?>" for = "<?php echo $expressOne->NumOrderId; ?>" >Cancel Order</a>
												<a rel="tooltip" class="for_tooltip btn btn-xs btn-success" href="javascript:void(0); " title = "Assign Custom Service" data-toggle="modal" data-target=".delivery_labels_pop-up-<?php echo $item->MergeUpdate->id; ?>" >Service Assign</a>
											</td>
										</tr>
								<?php
								endforeach;	
								?>
								<?php } ?>
								
								<!-- For Tracked with single SKU Group B -->
								<?php 
								foreach($tracked2 as $expressOne)
								{
								
									foreach($expressOne->Items as $item):
									$titles = $linnworkObj->getTitle( $item->MergeUpdate->sku );
									$pickListStatus	=	$checkproduct->checkPickList( $item->MergeUpdate->product_order_id_identify );
									if( $pickListStatus == 1)
									{
										$backgroud = "bg-orange-100";
									}
									else
									{
										$backgroud = "";
									}
									?>
									
										<tr style="border-bottom: 1px solid #ccc;" class ="<?php echo $backgroud; ?>" >									
											
											<td>
											<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_id; ?></div></td><td class='row'><div class='col-sm-6'><b>Postal Service</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Warehouse</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->warehouse; ?></div></td><td class='row'><div class='col-sm-6'><b>Delivery Country</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->delevery_country; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Delivery Label</b></div><div class='col-sm-6'><?php echo $item->Template->label_name; ?></div></td><td class='row'><div class='col-sm-6'><b>Service Provider</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Provider Code</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->provider_ref_code; ?></div></td><td class='row'><div class='col-sm-6'><b>Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_weight; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Length</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_length; ?></div></td><td class='row'><div class='col-sm-6'><b>Width</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Height</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_height; ?></div></td><td class='row'><div class='col-sm-6'><b>Packaging Slip</b></div><div class='col-sm-6'><?php echo $item->PackagingSlip->name; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b>Service Name</b></div><div class='col-sm-11'><?php echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'><table><tr><td width='25%' class='row'><div class='col-sm-6'><b>Packaging ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_id; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_weight; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Cost</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_cost; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Class</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packaging_type; ?></div></td></tr><tr><td colspan='4' width='100%' class='row'><div class='col-sm-2'><b>Packaging Name</b></div><div class='col-sm-10'><?php echo $item->MergeUpdate->envelope_name; ?></div></td></tr></table></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
												</a>
											<a class="moreDetails" href="#" >
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														echo $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														echo $item->MergeUpdate->order_id;
													}
												?>
												</a>
											</td>
											
											<td><?php echo $expressOne->ShippingInfo->PostalServiceName; ?></td>								
											<td>
												<?php
													//pack_order_quantity
													//pr($standardOne->Items); exit;
													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else
													{
														
													}
													
												?>
											</td>
											<td class="productTitle">
											<?php foreach($titles as $title) {  ?>
											<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>
											</td>
											<td>
												<?php 
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency((($item->MergeUpdate->quantity / $item->MergeUpdate->pack_order_quantity) * $item->MergeUpdate->price), $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price * $item->MergeUpdate->quantity, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
															else
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
														}
													}
												?>
											</td>
											<td>
											<?php
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}													
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																echo $item->MergeUpdate->price * $item->MergeUpdate->quantity;
															}
															else
															{
																echo $item->MergeUpdate->price;
															}
														}
													}										
											?>
											</td>
											<td>
											<?php echo $expressOne->TotalsInfo->Currency; ?>
											</td>
											<td>
											<?php 
												$expressDate=$expressOne->GeneralInfo->ReceivedDate;
												$trackedDate=explode('T', $expressDate);
												echo $trackedDate[0]."<br>(".$trackedDate[1].")";
											?>
											</td>
											<td>
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														$ord_id = $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														$ord_id = $item->MergeUpdate->order_id;
													} ?>
												<!--<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".pop-up-<?php print $ord_id; ?>">
													<i class="ion-clipboard"></i>
												</a>-->
												<a rel="tooltip" class="calcel_order btn btn-xs btn-danger" href="javascript:void(0); " title = "Cancel Order" data-toggle="modal" data-target=".cancel_pop-up-<?php echo $expressOne->OrderId; ?>" for = "<?php echo $expressOne->NumOrderId; ?>" >Cancel Order</a>
												<a rel="tooltip" class="for_tooltip btn btn-xs btn-success" href="javascript:void(0); " title = "Assign Custom Service" data-toggle="modal" data-target=".delivery_labels_pop-up-<?php echo $item->MergeUpdate->id; ?>" >Service Assign</a>
											</td>
										</tr>
								<?php
								endforeach;	
								?>
								<?php } ?>
								
								<!-- For Standard with single SKU Group B -->
								<?php
								foreach($standerd2 as $expressOne)
								{
								?> 
									<?php
									foreach($expressOne->Items as $item):
									$titles = $linnworkObj->getTitle( $item->MergeUpdate->sku );
									$pickListStatus	=	$checkproduct->checkPickList( $item->MergeUpdate->product_order_id_identify );
									if( $pickListStatus == 1)
									{
										$backgroud = "bg-orange-100";
									}
									else
									{
										$backgroud = "";
									}
									?>
										<tr style="border-bottom: 1px solid #ccc;" class ="<?php echo $backgroud; ?>" >									
											<td>
											<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_id; ?></div></td><td class='row'><div class='col-sm-6'><b>Postal Service</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Warehouse</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->warehouse; ?></div></td><td class='row'><div class='col-sm-6'><b>Delivery Country</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->delevery_country; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Delivery Label</b></div><div class='col-sm-6'><?php echo $item->Template->label_name; ?></div></td><td class='row'><div class='col-sm-6'><b>Service Provider</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->service_provider; ?></div></td><td class='row'><div class='col-sm-6'><b>Provider Code</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->provider_ref_code; ?></div></td><td class='row'><div class='col-sm-6'><b>Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_weight; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Length</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_length; ?></div></td><td class='row'><div class='col-sm-6'><b>Width</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Height</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packet_height; ?></div></td><td class='row'><div class='col-sm-6'><b>Packaging Slip</b></div><div class='col-sm-6'><?php echo $item->PackagingSlip->name; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b>Service Name</b></div><div class='col-sm-11'><?php echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'><table><tr><td width='25%' class='row'><div class='col-sm-6'><b>Packaging ID</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_id; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Weight</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_weight; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Cost</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->envelope_cost; ?></div></td><td width='25%' class='row'><div class='col-sm-6'><b>Packaging Class</b></div><div class='col-sm-6'><?php echo $item->MergeUpdate->packaging_type; ?></div></td></tr><tr><td colspan='4' width='100%' class='row'><div class='col-sm-2'><b>Packaging Name</b></div><div class='col-sm-10'><?php echo $item->MergeUpdate->envelope_name; ?></div></td></tr></table></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
												</a>
											<a class="moreDetails" href="#" >
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														echo $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														echo $item->MergeUpdate->order_id;
													}
												?>
												</a>
											</td>
											<td><?php echo $expressOne->ShippingInfo->PostalServiceName; ?></td>								
											<td>
												<?php
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo str_replace( ',','<br />',$item->MergeUpdate->sku );
														}
														else
														{
															echo $item->MergeUpdate->quantity .'X'. $item->MergeUpdate->sku;
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																echo ($item->MergeUpdate->quantity) .'X'. $item->MergeUpdate->sku;
															}
															else
															{
																echo $item->MergeUpdate->quantity .'X'. $item->MergeUpdate->sku;
															}
														}
													}
													
												?>
											</td>
											<td class="productTitle">
											<?php foreach($titles as $title) {  ?>
											<?php echo "<span>".$title."</span>"; ?>
											<?php } ?>
											
											<!--<table class="table table-responsive table-hover">
    <tbody>
        <tr class="clickable" data-toggle="collapse" id="row1" data-target=".row1">
            <td width="1%"><i class="glyphicon glyphicon-plus"></i></td>
            <td width="20%">data</td>
          	<td width="79%">data</td>  
        </tr>
        <tr class="collapse row1">
            <td></td>
            <td>data</td>
          	<td>data</td>  
        </tr>
        <tr class="collapse row1">
            <td></td>
            <td>data</td>
          	<td>data</td>  
        </tr>
    </tbody>
</table>-->
											
											</td>
											
											<td>
												<?php 
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
														else
														{
															//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
															echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price * $item->MergeUpdate->quantity, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
															else
															{
																//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $expressOne->TotalsInfo->Currency), 2, '.' ,''); 													
																echo $currentObj->getCurrentRate( $expressOne->TotalsInfo->Currency , 'EUR', $item->MergeUpdate->price );
															}
														}
													}
												?>
											</td>
											<td>
											<?php
													//echo number_format($checkproduct->changeCurrency($item->MergeUpdate->price, $standardOne->TotalsInfo->Currency), 2, '.' ,''); 													
													if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "single" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else if( ( $item->MergeUpdate->product_type == "bundle" && $item->MergeUpdate->product_sku_identifier == "multiple" ) )
													{
														if( count($expressOne->Items) == 1 )
														{
															echo $item->MergeUpdate->price;
														}
														else
														{
															echo $item->MergeUpdate->price;
														}
													}
													else
													{
														if( ( $item->MergeUpdate->product_type == "single" && $item->MergeUpdate->product_sku_identifier == "single" ) )
														{
															if( count($expressOne->Items) == 1 )
															{
																echo $item->MergeUpdate->price * $item->MergeUpdate->quantity;
															}
															else
															{
																echo $item->MergeUpdate->price;
															}
														}
													}										
											?>
											</td>											
											<td>
											<?php echo $expressOne->TotalsInfo->Currency; ?>
											</td>
											<td>
											<?php 
												$expressDate=$expressOne->GeneralInfo->ReceivedDate;
												$trackedDate=explode('T', $expressDate);
												echo $trackedDate[0]."<br>(".$trackedDate[1].")";
											?>
											</td>
											<td>
												<?php 
													if( $item->MergeUpdate->product_order_id_identify != '' )
													{
														$ord_id = $item->MergeUpdate->product_order_id_identify;
													}
													else
													{
														$ord_id = $item->MergeUpdate->order_id;
													} ?>
												<!--<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target=".pop-up-<?php print $ord_id; ?>">
													<i class="ion-clipboard"></i>
												</a>-->
												<a rel="tooltip" class="calcel_order btn btn-xs btn-danger" href="javascript:void(0); " title = "Cancel Order" data-toggle="modal" data-target=".cancel_pop-up-<?php echo $expressOne->OrderId; ?>" for = "<?php echo $expressOne->NumOrderId; ?>" >Cancel Order</a>
												<a rel="tooltip" class="for_tooltip btn btn-xs btn-success" href="javascript:void(0); " title = "Assign Custom Service" data-toggle="modal" data-target=".delivery_labels_pop-up-<?php echo $item->MergeUpdate->id; ?>" >Service Assign</a>
												
											</td>
										</tr>
								<?php
								endforeach;	
								?>
								<?php } ?>								
							</table>
							<br><br><br>
							</section>
						</div>
					</div>
				</div>
            </div>
	    </div>

	</div>        
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<!--Start despatch consol popup-->

<div class="modal modal-wide fade pop-up-despatch" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Despatch Console</h4>
         </div>
         <div class="modal-body bg-grey-100">
           	<div class="panel" style="clear:both">
					<div class="panel-body" ></div>
					
					<div class="input text"><label for="LinnworksapisBarcode">Barcode</label><input type="text" id="LinnworksapisBarcode" index="0" class="get_sku_string" name="data[Linnworksapis][barcode]"></div>
					<button class="button_for_scanning add_search_field btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40" type="submit">Submit</button>
				</div>
				<div class="outer_bin_addMore">
				
			    </div>	
			</div>
		</div>
	</div>
</div>




<!--End despatch consol popup-->

<!--Start Popup for all order-->
<!--
<?php if( !empty($results) && count($results > 0) ) {  foreach($results as $result) { ?>
<div class="modal modal-wide fade pop-up-<?php echo $result->Items[0]->MergeUpdate->product_order_id_identify ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1"><?php echo $result->ShippingInfo->PostalServiceName; ?></h4>
         </div>
         <div class="modal-body bg-grey-100">
           	<div class="panel" style="clear:both">
					<div class="panel-body" >
						<table>
							<tr>
								<td>Order ID</td>
								<td><?php echo $result->NumOrderId; ?></td>
							</tr>
							<tr>
								<td>Postal Service Name</td>
								<td><?php echo $result->ShippingInfo->PostalServiceName; ?></td>
							</tr>
							<tr>
								<td>Total</td>
								<td><?php echo $result->TotalsInfo->TotalCharge; ?></td>
							</tr>
							<tr>
								<td>SKU</td>
								<td>
								<?php foreach($result->Items as $item ) {  ?>
								<?php echo $item->MergeUpdate->sku."<br>"; 
								} ?>
								</td>
							</tr>
							<tr>
								<td>Country</td>
								<td><?php echo $result->CustomerInfo->Address->Country; ?></td>
							</tr>
							<tr>
								<td>TotalWeight</td>
								<td><?php echo $result->ShippingInfo->TotalWeight; ?></td>
							</tr>
						</table>
					 </div>
				</div>
				<div class="outer_bin_addMore">
			    </div>	
				<div class="row">
					<!--<div class="col-lg-10 col-lg-offset-1">
						<a href="javascript:void(0);" for = "<?php echo $result->OrderId; ?>" class="assignpostelservice assignpostelservice_<?php echo $result->OrderId; ?> btn bg-orange-500 color-white btn-dark padding-left-40 padding-right-40" >Assign Postal Service</a>
						<a href="javascript:void(0);" for = "<?php echo $result->OrderId; ?>" class="processedorder processedorder_<?php echo $result->OrderId; ?> btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40" style="display:none;" >Processed</a>
						<a href="" class="loading-image_<?php echo $result->OrderId; ?>" style="display:none;"><img src="http://localhost/app/webroot/img/image_889915.gif" hight ="20" width="20" /></a>
						<a href=""  target="_blank" for = "<?php echo $result->OrderId; ?>" class="printorder_<?php echo $result->OrderId; ?> btn bg-orange-500 color-white btn-dark padding-left-40 padding-right-40" style="display:none;" target="_blank" >Print Label</a>
						<a href=""  target="_blank" for = "<?php echo $result->OrderId; ?>" class="printlable_<?php echo $result->OrderId; ?> btn bg-orange-500 color-white btn-dark padding-left-40 padding-right-40" style="display:none;" target="_blank" >Print Slip</a>
					</div>-->
				<!--</div>
			</div>
		</div>
	</div>
</div>
<?php } } ?>-->
<!--End Popup for all order-->

<script>

$(document).keypress(function(e) {
    if(e.which == 13) {
        submitSanningValue();
    }
});

function submitSanningValue()
{
	var Barcode	=	$('#LinnworksapisBarcode').val();
	
	$.ajax(
				{
						'url'            : getUrl() + '/Linnworksapis/getsearchlist',
						'type'           : 'POST',
						'data'           : { barcode : Barcode },
						'beforeSend'	 : function() {
															$('.loading-image').show();
														},
						'success' 		 : function( msgArray )
														{
															$('.outer_bin_addMore').html(msgArray)
														}  
						});  
					
}


$(function() {
  $("#LinnworksapisBarcode").focus();
});
	$(document).ready(function()
        { 
		var $animationExamples;
        $animationExamples = $(".animated-tab").hashTabs();
		//$("[rel='tooltip']").tooltip();
			$('.moreDetails').popover({html:true});
			$('.moreDetails i').mouseover(function() {
				//$('.rackDetails .tab-pane-container').addClass('overlay');
				$('div.overlay').addClass('on');
				//$('.table-striped > tbody > tr:nth-of-type(2n+1)').css('background-color','transparent');
				//$('.overlay .btn').css('display','none');
				$('.moreDetails i').css({"z-index": "9999999999", "position": "relative", "color": "#ffffff"});
			});
			$('.moreDetails i').mouseout(function() {
				$('div.overlay').removeClass('on');
				//$('.table-striped > tbody > tr:nth-of-type(2n+1)').css('background-color','#f9f9f9');
				//$('.overlay .btn').css('display','inline-block');
				$('.moreDetails i').css({"z-index": "9999999999", "position": "relative", "color": "#444444"});
			});

			<!--set product id-->
			
					 var skuArray = [];
					 var countryArraynew = [];
					 var blkstr = [];
					 var i = 0;
					 $('.case').each(function()
									{
										skuArray[i]	=	$(this).attr('id');
										i++;
									});
			
					$.each(skuArray, function(idx2,val2) {                    
							 var str =  val2;
							 blkstr.push(str);
							 
						});
					
						var skuStr	=	blkstr.join("---");
						$('.get_sku_string').val(skuStr);
			
			<!--set product id-->
			
			$("body").on("click", ".assignpostelservice", function(){
				var id	=	$(this).attr('for');
				$.ajax(
				{
						'url'            : getUrl() + '/Linnworksapis/assignService',
						'type'           : 'POST',
						'data'           : { id : id },
						'beforeSend'	 : function() {
															$('.loading-image_'+id).show();
														},
						'success' 		 : function( msgArray )
														{
															$('.pop-up-'+id+' h4').text(msgArray);
															$('.loading-image_'+id).hide();
															$('.processedorder_'+id).show();
															$('.assignpostelservice_'+id).hide();
														}  
						});  
					});
			
			$("body").on("click", ".processedorder", function(){
				var id	=	$(this).attr('for');
				$.ajax(
					{
						'url'          : getUrl() + '/Linnworksapis/labelAssign',
						'type'         : 'POST',
						'data'         : { id : id },
						'beforeSend'   : function() {
														$('.loading-image_'+id).show();
													 },
						'success' 	   : function( msgArray )
													{
														var result = msgArray.split("#");
														$('.printorder_'+id).attr('href', result[0]);
														$('.printlable_'+id).attr('href', result[1]);
														$('.loading-image_'+id).hide();
														$('.printorder_'+id).show();
														$('.printlable_'+id).show();
														$('.processedorder_'+id).hide();
													}  
					});  
				});
				
				/* For update custom posta service */			
				$("body").on("click", ".assignCustomPostal", function(){
					
					var orderId 		= $(this).attr( 'for' );
					
					var str = $( '.servicePop-'+orderId ).find('form.form-horizontal').serialize();
					
					$.ajax(
						{
							'url'          : '/System/Service',
							'type'         : 'POST',
							'data'         : { id : orderId, 
											   query:str
											 },						
							'success' 	   : function( msgArray )
											{
												if( msgArray == 1 )
												{
													$( ".close" ).click();
													location.reload();		
												}
												else
												{
													alert( 'Service Id does not exist' );
												}
											}  
						});  
					});
			
			/* For close popup */	
				
				
				
			$("body").on("click", ".switch", function(){
				if($(".top_panel").is(":visible") == true)
				{
					$('.top_panel').hide();
					$('.bottom_panel').show();
				}
				else
				{
					$('.top_panel').show();
					$('.bottom_panel').hide();
				}
				
				});
			
			/*$('div#example1_wrapper table#example1 thead.parentCheck tr.parentInner th.sorting_asc').click(function()
			{ 		
				 if( $(this).children().hasClass( 'checked' ) )
				 {
					$(this).children().removeClass( 'checked' )
					$('.icheckbox_square-green').removeClass('checked');
					$('.download_button').css('display', 'none');								
				 }
				 else
				 {
					$('.icheckbox_square-green').addClass('checked');
					$('.download_button').css('display', 'block');				
				 }
				 
					 var skuArray = [];
					 var countryArraynew = [];
					 var blkstr = [];
					 var i = 0;
					 $('div.checked .case').each(function()
									{
											skuArray[i]	=	$(this).attr('id');
										i++;
									});
			
					$.each(skuArray, function(idx2,val2) {                    
							 var str =  val2;
							 blkstr.push(str);
						});
					
						var skuStr	=	blkstr.join("---");
						$('.get_sku_string').val(skuStr);
					});*/
			
						
			$("body").on("click", ".sortingChild", function ()
				{
					
					 var skuArray = [];
					 var countryArraynew = [];
					 var blkstr = [];
					 $('.download_button').css('display', 'none');
				
					 var i = 0;
					 $('div.checked .case').each(function()
						{
								skuArray[i]	=	$(this).attr('id');
							i++;
							if( i >= 1)
							{
								$('.download_button').css('display', 'block');
							}
						});
					 $.each(skuArray, function(idx2,val2) 
						{
							 var str =  val2;
							 blkstr.push(str);
						});
					
						var skuStr	=	blkstr.join("---");
						$('.get_sku_string').val(skuStr);
						 
					});
						
			
			
			/* start for download pick list */
			$( 'body' ).on( 'click', '.downloadPickList', function()
				  {
					 var orderId	=	'';
					 
					 
					 /*if( orderId == '' )
					 {
						 //alert( "OOPS, No more SKU found for picklist!" );
						 swal("OOPS, No more SKU found for picklist!.", "" , "success");											
						 return false;
					 }*/
					 
				   $.ajax({
					 url      : '/Linnworksapis/generatePickList',
					 type     : 'POST',
					 data     : { orderId : orderId },       
					 success  : function( data  )
					 {
							
							var url=data;
							var arr=url.split('.')[2];
							if( arr == 'pdf')
							{
								location.reload();
								window.open(data,'_self' );
							}
							else
							{
								swal("OOPS, No more SKU found for picklist!.", "" , "success");											
								return false;
							}
							
							//location.reload();
							//window.open(data,'_self' );
					 }                
					});    
				  });
			/* end for download pick list */
			
					
			
			/* Code for order cancel */			
			$("body").on("click", ".ordercancel", function(){
				var content	=	$(this).attr('for');
				var res 	= content.split("##$#");
				var id		=	res[0];
				$.ajax(
				{
					'url'                     : getUrl() + '/jijGroup/Order/orderCancel',
					'type'                    : 'POST',
					'data'                    : { content : content },
					'beforeSend'			  : function() {
																$('.loading-image_'+id).show();
															 },
					'success' 				  : function( msgArray )
												{
													
													if (msgArray == 1 )
													{
														$('.panel-head').html('<p class="alert alert-success"> Order Cancelled Successfully.</p>').show(500).delay(5000).fadeOut();
														$('.loading-image_'+id).hide();
														location.reload();
													}
												}  
				});  
			});
		});
		
		$("body").on("click", ".cancelOpenOrder", function(){
			
			var id 				=	$(this).attr("for");
			var refundNote		=	$("#refund_note_"+id). val();
			var refundamount	=	$("#refund_amount_"+id). val();
			var warehohuseId	=	$("#warehouse_id_"+id). val();
				
			swal({
					title: "Are you sure?",
					text: "You want to cancel open order !",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, cancel it!",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						$.ajax({
							
								'url' 					: 	'/Linnworksapis/cancelOrder',
								'type'					: 	'POST',
								'data' 					: 	{ openorderid : id, refundNote:refundNote, refundamount:refundamount,warehohuseId:warehohuseId},
								'beforeSend'			: function() {
																			$('.loading-image_'+id).show();
																		 },
								'success' 				  : function( msgArray )
									{
										if ( msgArray == 1 )
										{
											$('.close').click();
											swal("Open order canceled successfully.", "" , "success");											
											$('.loading-image_'+id).hide();
											location.reload();
										}
										if( msgArray == 2 )
										{
											$('.close').click();
											alert('There is an error.');
										}
									} 
								
							});
					}
					else
					{
						$('.close').click();
						swal("Cancelled", "Your order is safe :)", "error");
					}
			});
		});
		
		
		//Download Stock file now
		$( 'body' ).on( 'click', '.downloadStockFile', function()
		{
			//Check If input rack name exists in table or not
			$.ajax({
					url     	: '/Products/prepareVirtualStock',
					type    	: 'POST',
					data    	: {},  			  
					success 	:	function( data  )
					{
						 window.open(data,'_self' );
					}                
				});				
		});	
		
		
</script>



