<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Location</h1>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12 col-lg-offset-0">
                             <div class="form-horizontal panel-body padding-bottom-40 padding-top-40">
								   <div class="form-group">
										<label class="col-sm-3 control-label">Search Key</label>
										<div class="col-sm-8">                                               
											<?php
												print $this->form->input( 'Product.searchkey', array( 'type'=>'text','class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false,'placeholder' => 'SKU / BARCODE' ) );
											?>
										</div>
									</div>
                                 <div class="form-horizontal panel-body" >
									<table class="sku_date col-sm-12" >
									</table>
							    </div>  
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>

<script>

$(document).keypress(function(e)
	{
		if(e.which == 13) {
			getLocationValue();
		}
	});


	function getLocationValue()
	{
		var location  = $('#ProductSearchkey').val();
		var str ='';
		$.ajax({
						url     	: '/Binlocations/getproductdetail',
						type    	: 'POST',
						data    	: { location : location },  			  
						success 	:	function( msgArray )
						{
							
							var json 		= 	JSON.parse(msgArray);
							var getStatus 	= 	json.status;
							var getData 	= 	json.data;
							str	+= '<table width = 100%><tr style="height: 30px;background-color: #cbc7c7;"><th>SKU</th><th>Barcode</th><th>Title</th><th>Bin Location</th><th>Stock</th><th>Unpick</th><th>Action</th></tr>';
							if( getStatus == 1 )
							{
								for (var i = 0, max = getData.length; i < max; i++)
									{
										str	+=	'<tr style="font-weight: bold; border-bottom: 2px solid #ccc;height: 50px;" id="row_'+getData[i].BinLocation.id+'">';
										str += '<td id="sku_'+getData[i].BinLocation.id+'">'+getData[i].Product.product_sku+'</td>';
										str += '<td id="barcode_'+getData[i].BinLocation.id+'" for="'+getData[i].ProductDesc.Barcode+'">'+getData[i].ProductDesc.loc_barcode+'</td>';
										str += '<td>'+getData[i].Product.product_name+'</td>';
										str += '<td id="location_'+getData[i].BinLocation.id+'">'+getData[i].BinLocation.bin_location+'</td>';
										str += '<td id="stock_'+getData[i].BinLocation.id+'">'+getData[i].BinLocation.stock_by_location+'</td>';
										str += '<td id="unpick_'+getData[i].BinLocation.id+'">'+getData[i].Orderqty.unpick_qty+'</td>';
										str += '<td><strong><button class="update_stock btn bg-red-500 color-white btn-dark" id="" for="" onclick = "deleteAction('+getData[i].BinLocation.id+')" data="Delete" title="Delete" group=""><span class="glyphicon glyphicon-trash"></span></button></td>';
										str	+=	'</tr>';
									}
									str	+= '</table>';
									$('.sku_date').html('');
									$('.sku_date').append( str );
							} else {
									$('.sku_date').html('');
									alert('There is no location');
							}
						}                
				});	
	}
	
	function deleteAction( id )
	{
		var row 		=	$('#row_'+id).text();
		var sku 		=	$('#sku_'+id).text();
		var barcode 	=	$('#barcode_'+id).attr('for');
		var location 	=	$('#location_'+id).text();
		var stock 		=	$('#stock_'+id).text();
		var unpick 		=	$('#unpick_'+id).text();
		//alert(sku+'>>>>'+barcode+'>>>>>'+location+'>>>>'+stock+'>>>>>'+unpick);
		if(stock > 0){
			alert('Stock is available on this location');
			return false;
		}
		if(unpick > 0){
			alert('Unpick stock is available on this location');
			return false;
		}
				swal({
					title: "Are you sure?",
					text: "You want to delete location !",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						$.ajax({
									url     	: '/Binlocations/freeLocationdeletion',
									type    	: 'POST',
									data    	: { id : id, barcode : barcode, location : location, stock : stock },  			  
									success 	:	function( msgArray )
									{
										var json 		= 	JSON.parse( msgArray );
										var getStatus 	= 	json.status;
										var rid			=	json.data;
										if( getStatus == 1 )
										{
											$('#row_'+id).remove();
											$('.close').click();
											swal("Your location deleted successfully :)", "" ,"success");
										}
									}
				
								});
					}
					else
					{
						$('.close').click();
						swal("Cancelled", "Your location is safe :)", "error");
					}
				});
	}

</script>