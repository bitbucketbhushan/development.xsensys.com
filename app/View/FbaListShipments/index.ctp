<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>All Shipments</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							  
						</div>	
						
					<div class="panel-body no-padding-top bg-white">
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div style="width:12%; float:left;">Shipment Name</div>		
								<div style="width:12%; float:left;">Shipment Id</div>	
								<div style="width:12%; float:left;">Created On</div>	
								<div style="width:12%; float:left;">Last Updated</div>			
								<div style="width:8%; float:left;"><center>SKUs</center></div>	
								<div style="width:7%; float:left;"><center>Shipped</center></div>																				
								<div style="width:8%; float:left;"><center>Received</center></div>	
								<div style="width:8%; float:left;"><center>Destination</center></div>	
								<div style="width:10%; float:left;">Status</div>
								<div style="width:10%; float:left;">Action</div>	
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  	
							App::Import('Controller', 'FbaListShipments'); 
							$obj = new FbaListShipmentsController;
								
							// pr($all_shipments);
							 //echo "</pre>";
						 if(count($all_shipments) > 0) {	 
						 	foreach( $all_shipments as $_shipments ) {     
							 
						 		$item_data = $obj->getShipmentItems($_shipments['FbaApiShipment']['shipment_id']);
								
								?>
								<div class="row sku" id="r_<?php echo $_shipments['FbaApiShipment']['id']; ?>">
									<div class="col-lg-12">	
										<div style="width:12%; float:left;"><a href="<?php echo Router::url('/', true) ?>FbaListShipments/ShipmentItems/<?php echo $_shipments['FbaApiShipment']['id']; ?>" title="View Shipment Items" style="font-size:11px; color:#000099; text-decoration:underline;"><?php echo $_shipments['FbaApiShipment']['shipment_name']; ?></a></div>	
										<div style="width:12%; float:left;"><small><?php echo $_shipments['FbaApiShipment']['shipment_id']; ?></small></div>	
										<div style="width:12%; float:left;"><small><?php echo $_shipments['FbaApiShipment']['added_date'] ?></small></div> 		
										<div style="width:12%; float:left;"><small><?php echo $_shipments['FbaApiShipment']['timestamp']  ?></small></div>		
																			
										<div style="width:8%; float:left;"><small><center><?php  echo $item_data['skus'] ?></center></div>			
										
										<div style="width:7%; float:left;"><small><center><?php echo $item_data['quantity_shipped'] ;?></center></small></div> 
										<div style="width:8%; float:left;"><small><center><?php echo $item_data['quantity_received'] ;?></center></small></div>
										
										 
										<div style="width:8%; float:left;"><small><center><?php  echo $_shipments['FbaApiShipment']['destination_fulfillment_center_id']; ?></center></small></div>	
										<div style="width:10%; float:left;"> <?php  echo $_shipments['FbaApiShipment']['shipment_status']?>
										
										 <?php /*?><select name="status" id="status" style="font-size:12px; padding:5px; border:1px solid #c6c6c6; width:100%;" onchange="updateStatus(this,<?php echo $items['id']; ?>)">
														<option value="">--Shipment Status--</option>
														<option value="ready_to_prepare" <?php if($items['shipment_status'] == 'ready_to_prepare') echo'selected=selected';?>>Ready to Prepare</option>
														<option value="preparing" <?php if($items['shipment_status'] == 'preparing') echo'selected=selected';?>>Preparing</option>
														<option value="exported" <?php if($items['shipment_status'] == 'exported') echo'selected=selected';?>>Exported</option>
														<option value="delivered" <?php if($items['shipment_status'] == 'delivered') echo'selected=selected';?>>Delivered</option>
														<option value="reconciled_complete" <?php if($items['shipment_status'] == 'reconciled_complete') echo'selected=selected';?>>Reconciled-Complete</option>
														 												 
													</select> <?php */?>
										 

</div>
										<div style="width:10%; float:left;">
										<a href="<?php echo Router::url('/', true) ?>FbaApiShipments/DownloadSkuList/<?php echo $_shipments['FbaApiShipment']['id']; ?>" title="Export Shipment" style="font-size:10px; color:#000099; text-decoration:underline;">Download SKU list</a></div>	
									</div>												
								</div>
								 	 
							<?php }?>
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	

$('#filter_shipment').click(function(){  
	document.getElementById("shipmentFrm").submit();		 
});
	
function updateStatus(t,v){	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaApiShipments/updateStatus',
		type: "POST",				
		cache: false,			
		data : {status:$(t).find("option:selected").val(),id:v },
		success: function(data, textStatus, jqXHR)
		{	
			 	
			if(data.error){
				alert(data.msg);
			} 							
		}		
 	});  		 
}
</script>
