<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Upload Shipment</h1>	
			
			<div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
							<!--<li>
								<a class="btn btn-xs btn-success" href="/productsNew/AddBundle" data-toggle="modal" data-target=""><i class=""></i>Add Bundle</a>
							</li>
							
							<li>
								<div class="btn-group">
								  <button type="button"  href="#" class="btn btn-xs btn-success no-margin">Sample File</button>
								  <button type="button" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="caret"></span>
									<span class="sr-only">Toggle Dropdown</span>
								  </button>
								  <ul class="dropdown-menu scrollable-menu" role="menu">
								 	<li><a href="/files/bundle_sku_sample.csv" target ="_blank">Bundle File</a></li>
									<li><a></a></li>
								  </ul>
								</div>
							</li>-->
						</ul>
					</div>
				</div>
			
			
			
			
				
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>		
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
			
			  <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="panel-body check_image_extension padding-bottom-40 padding-top-40">
                                	   <?php							
											print $this->form->create( 'Shipment', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/FbaShipments/Uploadfile', 'type'=>'post','id'=>'shipment' ) );
										?>
										<div class="form-group">
											<label for="username" class="control-label col-lg-3">TSV File</label>                                        
											<div class="col-lg-7">
												<div class="input-group">
													<span class="input-group-btn">
														<span class="btn btn-primary btn-file">
															Browse…  <?php
																		print $this->form->input( 'Shipment.Uploadfile', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'', 'required'=>false) );
																	  ?>
														</span>
													</span>
															<input type="text" placeholder="No file selected" readonly="" class="form-control">
															<span class="input-group-addon no-padding-top no-padding-bottom">
													
													</span>
											</div>
											
                                        </div>
                                      </div>
								     
								       
								    
								    <div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100">                                                                            
                                    <div class="radio radio-theme min-height-auto no-margin no-padding">
											<?php
											echo $this->Form->button('Upload', array(
											'type' => 'submit',
											'escape' => true,
											'class'=>'add_brand btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
											 ));	
										?>
									</div>
													
                                    
									</div>
                                </div>
                               </form>
						</div>
					</div>        
				</div>
