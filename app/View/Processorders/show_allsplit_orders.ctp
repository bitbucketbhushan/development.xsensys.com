<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
		<h1 class="page-title"><?php print $title;?>  ( Total : <?php echo $countSplitOrder; ?> ) </h1>
		<div class="panel-title no-radius bg-green-500 color-white no-border">
		</div>
		<div class="panel-head"><?php print $this->Session->flash(); ?></div>
    </div>
  
    <div class="container-fluid">
		<div class="row">
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">									
						</div>

						<div class="" style="margin:0 20px 20px;">
						
							<div class="pagination">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->prev( 'Previous', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled myclass1', 'tag' => 'li' ) );
            echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'disabled myclass' ) );
            echo $this->Paginator->next( 'Next', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled myclass1', 'tag' => 'li' ) );
        ?>
    </ul><?php echo $this->Paginator->counter(); ?>
</div>
							
						</div>
						<div class="panel-body no-padding-top bg-white">											
									<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
											<thead>
									<tr role="row">
										<th style="width: 2%;" >Order Id</th>
										<th style="width: 2%;" >Order Identifier</th>
										<th style="width: 2%;" >Sku</th>
										<th style="width: 2%;" >Barcode</th>
										<th style="width: 9%;">Service Name</th>
										<th style="width: 9%;">Provider Ref Code</th>
										<th style="width: 9%;">Service Provider</th>
										<th style="width: 9%;">Operater</th>
									</tr>
								</thead>
							<tbody role="alert" aria-live="polite" aria-relevant="all">
										<?php if( !empty( $getAllSplitOrders ) && count($getAllSplitOrders) > 0 )
										{
											App::import('Controller', 'Processorders');
											$getname 	=	new ProcessordersController;
											$fullname	=	$getname->getUserDetail();
										foreach( $getAllSplitOrders as $splitindex	=>	$splitvalue)
										
										{
										?>
									<tr class="odd">
										<td class="sorting_1"><?php echo '<b>'.$splitvalue['MergeUpdate']['order_id'].'</b>'; ?></td>
										<td class="sorting_1"><?php echo $splitvalue['MergeUpdate']['product_order_id_identify']; ?></td>
										<td class="sorting_1"><?php echo $splitvalue['MergeUpdate']['sku']; ?></td>
										<td class="sorting_1"><?php echo $splitvalue['MergeUpdate']['barcode']; ?></td>
										<td class="sorting_1"><?php echo $splitvalue['MergeUpdate']['service_name']; ?></td>
										<td class="sorting_1"><?php echo $splitvalue['MergeUpdate']['provider_ref_code']; ?></td>
										<td class="sorting_1"><?php echo $splitvalue['MergeUpdate']['service_provider']; ?></td>
										<td class="sorting_1"><?php echo $fullname; ?></td>
									</tr>
									<?php } } ?>
								</tbody>
							</table>
							
							<div class="" style="margin:40px 20px 0px;">
						
							<div class="pagination">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->prev( 'Previous', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled myclass1', 'tag' => 'li' ) );
            echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'disabled myclass' ) );
            echo $this->Paginator->next( 'Next', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled myclass1', 'tag' => 'li' ) );
        ?>
    </ul><?php echo $this->Paginator->counter(); ?>
</div>
							
						</div>
						</div>
					</div>
				</div>
              </div>
				<!-- BEGIN FOOTER -->
					<?php echo $this->element( 'footer' ); ?>
				<!-- END FOOTER -->
         </div>
    </div>
</div>


