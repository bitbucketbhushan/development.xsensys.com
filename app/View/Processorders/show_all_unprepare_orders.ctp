<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
		<h1 class="page-title"><?php print $title;?> ( Total : <?php echo  $countprocess; ?> )</h1>
		<div class="panel-title no-radius bg-green-500 color-white no-border">
		</div>
		<div class="panel-head"><?php print $this->Session->flash(); ?></div>
    </div>
  
    <div class="container-fluid">
		<div class="row">
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">									
						</div>
						
						<div class="" style="margin:0 auto; width:350px">
								 <ul class="pagination">
								  <?php
									   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
									   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								  ?>
								 </ul>
						</div>
						<div class="panel-body no-padding-top bg-white">											
									<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
											<thead>
									<tr role="row">
										<th style="width: 2%;" >Order Id</th>
										<th style="width: 2%;" >Ref No</th>
										<th style="width: 2%;" >Name</th>
										<th style="width: 2%;" >Country</th>
										<th style="width: 2%;" >Source</th>
										<th style="width: 9%;">Curr.</th>
										<th style="width: 9%;">Cost</th>
										<th style="width: 9%;">Item</th>
										<th style="width: 9%;">Title</th>
										<th style="width: 9%;">Qty</th>
										<th style="width: 9%;">Order Date</th>
										<th style="width: 9%;">Order Status</th>
										<th style="width: 9%;">Action</th>
									</tr>
								</thead>
							<tbody role="alert" aria-live="polite" aria-relevant="all">
										<?php if( !empty( $processOrderDetails ) && count($processOrderDetails) > 0 )
										{
											App::import('Controller', 'Processorders');
											$getname 	=	new ProcessordersController;
											$fullname	=	$getname->getUserDetail();
										foreach( $processOrderDetails as $processindex	=>	$processvalue)
										//pr( $processvalue );
										
										{
										?>
									<tr class="odd unpraperorder_<?php echo $processvalue['num_order_id']; ?>">
										<td class="  sorting_1"><?php echo $processvalue['num_order_id']; ?></td>
										<td class="  sorting_1"><?php echo $processvalue['general_info']->ReferenceNum; ?></td>
										<td class="  sorting_1"><?php echo $processvalue['customer_info']->Address->FullName; ?></td>
										<td class="  sorting_1">
											<?php 
											echo($processvalue['customer_info']->Address->Address1 != '') ? $processvalue['customer_info']->Address->Address1."<br>" : '--<br />'; 
											echo ($processvalue['customer_info']->Address->Address2 != '') ? $processvalue['customer_info']->Address->Address2."<br>" : '--<br />'; 
											echo ($processvalue['customer_info']->Address->Address3 != '') ? $processvalue['customer_info']->Address->Address3."<br>" : '--<br />'; 
											echo ($processvalue['customer_info']->Address->Town != '' ) ? $processvalue['customer_info']->Address->Town."<br>" : '--<br />'; 
											echo ($processvalue['customer_info']->Address->Region != '') ? $processvalue['customer_info']->Address->Region."<br>" : '--<br />'; 
											echo ($processvalue['customer_info']->Address->PostCode != '') ? $processvalue['customer_info']->Address->PostCode."<br>" : '--<br />';
											echo ($processvalue['customer_info']->Address->Country != '') ? $processvalue['customer_info']->Address->Country."<br>" : '--<br />'; 
											echo ($processvalue['customer_info']->Address->PhoneNumber) ? $processvalue['customer_info']->Address->PhoneNumber."<br>" : '--<br />'; 
											
											?> 
										</td>
										<td class="  sorting_1"><?php echo $processvalue['general_info']->Source .'<br>'. $processvalue['general_info']->SubSource; ?></td>                                                
										<td class="  sorting_1"><?php echo $processvalue['totals_info']->Currency; ?></td>
										<td class="  sorting_1"><?php echo $processvalue['totals_info']->Subtotal; ?></td>
										<td class="  sorting_1">
											<?php foreach( $processvalue['items'] as $items) {  
											echo $items->SKU."<br>";
											} ?>
										</td>
										<td class="productTitle sorting_1">
											<?php foreach( $processvalue['items'] as $items) {  
											echo '<div class ="for_tooltip" rel="tooltip" title = "'.$items->Title.'" ><span>' . substr($items->Title, 0, 15 )."</div></span><br>";
											} ?>
										</td>
										<td class="  sorting_1">
											<?php 
											$total = 0; 
											foreach( $processvalue['items'] as $items) {  
											$total = $total + $items->Quantity;
											}
												echo $total;
											?>
										</td>
										<td class="sorting_1" ><?php echo str_replace('T','<br>',$processvalue['general_info']->ReceivedDate); ?></div></td>
										<td class="sorting_1 changeorderstatus_<?php echo $processvalue['num_order_id']; ?>" >
											<?php 
											if( $processvalue['unprepare_status'] == '' )
											{
												echo '----';
											}
											else if( $processvalue['unprepare_status'] == '0' )
											{
												echo 'Order in process';
											}
											else if( $processvalue['unprepare_status'] == '1' )
											{
												echo 'cancel or processed';
											}
											else if( $processvalue['unprepare_status'] == '2' )
											{
												echo 'not found';
											}
											?>
										</td>
										<td class="sorting_1" >
											<!--<input type='hidden' class="subsource" value="<?php echo $processvalue['general_info']->SubSource ?>" >--> 
											<input type='hidden' class="pkorderid" value="<?php echo $processvalue['order_id'] ?>" > 
											<input type='hidden' class="numorderid" value="<?php echo $processvalue['num_order_id'] ?>" > 
											<!--<a href="javascript:void(0);" alt="Verify" title="Current Order Status In linnworks" class="varifyorder btn btn-success btn-xs margin-bottom-5">Verify Order</a> <br />-->
											<?php if( $processvalue['unprepare_status'] == 1 || $processvalue['unprepare_status'] == 2 ) { ?>
											<a style="display:block;" href="javascript:void(0);" alt="Delete" for="<?php echo $processvalue['num_order_id'] ?>" class="deleteunprapere_<?php echo $processvalue['num_order_id'] ?> deleteunprapereorder btn btn-danger btn-xs">Delete</a>	
											<?php }else{ ?>
											<a style="display:none;" href="javascript:void(0);" alt="Delete" for="<?php echo $processvalue['num_order_id'] ?>" class="deleteunprapere_<?php echo $processvalue['num_order_id'] ?> deleteunprapereorder btn btn-danger btn-xs">Delete</a>	
											<?php } ?>	
											
											<a href="javascript:void(0);" for="<?php echo $processvalue['num_order_id'] ?>" class="cancelUnpraperOrder btn bg-red-500 color-white btn-dark padding-left-20 padding-right-20" style="margin:0 auto; text-align: center;">Cancel</a>
										</td>
									</tr>
									<?php } } ?>
								</tbody>
							</table>
							
							<div class="" style="margin:0 auto; width:350px">
								 <ul class="pagination">
								  <?php
									   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
									   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								  ?>
								 </ul>
								 
							</div>
						</div>
					</div>
				</div>
              </div>
				<!-- BEGIN FOOTER -->
					<?php echo $this->element( 'footer' ); ?>
				<!-- END FOOTER -->
         </div>
    </div>
</div>
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

<script>

$(document).ready(function()
        { 
			$("[rel='tooltip']").tooltip();
		});
		
	$( 'body' ).on( 'click', '.cancelUnpraperOrder', function(){
			
		var orderID		=	$( this ).attr('for');
		swal({
					title: "Are you sure?",
					text: "You want to cancel Unprapered order !",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						$.ajax({
									url     	: '/Outers/cancelUnpraperOrder',
									type    	: 'POST',
									data    	: { orderID : orderID },  			  
									success 	:	function( data  )
									{
										swal("Cancelled", data, "error");
										//location.reload();
									}                
							  });
					}
				});
		
	});
	
	
	
	$( 'body' ).on( 'click', '.varifyorder', function()
		{
			var pkOrderId	=	$(this).prev().prev('input').val();
			var numOrderId	=	$(this).prev('input').val();
			
			//alert($(this).find("tr > td > .numorderid").val());
			//return false;
			
			$.ajax({
								url     	: '/verify/=B2BTR/Id/Order',
								type    	: 'POST',
								data    	: { pkOrderId : pkOrderId }, 
								beforeSend	 : function() { $( ".outerOpac" ).attr( 'style' , 'display:block');}, 			  
								success 	:	function( data  )
								{
									
									
									$( ".outerOpac" ).attr( 'style' , 'display:none');
									if( data == '0' || data == '1' || data == '2' )
									{
										if( data == '0' )
										{
											$('.changeorderstatus_'+numOrderId).html('<p>Order in process</p>');
										}
										else if( data == '1' )
										{
											$('.changeorderstatus_'+numOrderId).html('<p>cancel or processed</p>');
											$('.deleteunprapere_'+numOrderId).attr('style' , 'display:block');
											
										}
										else if( data == '2' )
										{
											$('.changeorderstatus_'+numOrderId).html('<p>not found</p>');
											$('.deleteunprapere_'+numOrderId).attr('style' , 'display:block');
										}
										$('.unpraperorder_'+numOrderId).css( 'background-color', 'yellow' );
										setTimeout(function (){ $('.unpraperorder_'+numOrderId).css( 'background-color', 'white' );}, 1000);													
									}
									else
									{
										swal('Opps! There is some error');
									}
								}                
						});
		});
		
		
		$( 'body' ).on( 'click', '.deleteunprapereorder', function()
		{
			var numOrderId = $(this).attr('for');
			
			$.ajax({
								url     	: '/verify/=B25TO/Delete/Id/Order/',
								type    	: 'POST',
								data    	: { numOrderId : numOrderId }, 
								beforeSend	 : function() { $( ".outerOpac" ).attr( 'style' , 'display:block');}, 			  
								success 	:	function( data  )
								{
									$( ".outerOpac" ).attr( 'style' , 'display:none');
									if( data == '1' )
									{
										$('.unpraperorder_'+numOrderId).remove();
										swal( 'Your unprepare order ( ' +  numOrderId + ' ) is removed.'  );
									}
									else
									{
										swal('Oops', 'There is an error');
									}
								}                
						});
		});

</script>
