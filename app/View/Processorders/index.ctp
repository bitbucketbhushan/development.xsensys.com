<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Processed Orders</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row"> 
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						<form method="get" name="searchfrm" action="<?php echo Router::url('/', true) ?>Processorders" enctype="multipart/form-data">	
						<div class="col-sm-5 no-padding">
							<div class="col-lg-8 no-padding">
									<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Enter Order Id" name="searchkey" class="form-control searchString" />
								</div>
								
								<div class="col-lg-4"> 
									<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
								</div>
						</div>
						</form>
						<div class="col-sm-7 text-right">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						</div>	
					<div class="panel-body no-padding-top bg-white">
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-8">Order Id</div>								 
								<div class="col-12">Sku</div>   
								<div class="col-10">Barcode</div>	
								<div class="col-8">Service provider</div>
								<div class="col-11">Service name</div>
								<div class="col-10">Delevery country</div>																
								<div class="col-11">Purchase Date</div>	
								<div class="col-10">User</div>	
								<div class="col-11">Process Date</div> 
								<div class="col-8">Action</div>	
								
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						 if(count($Processorders) > 0) {	 ?>
							<?php foreach( $Processorders as $items ) {   ?>
								<div class="row sku" id="r_<?php echo $items['MergeUpdate']['id']; ?>">
									<div class="col-lg-12">	
										<div class="col-8"><small><?php echo $items['MergeUpdate']['order_id']; ?></small></div>	
										<div class="col-12"><small><?php echo $items['MergeUpdate']['sku']; ?></small></div>										
										<div class="col-10"><small><?php echo $items['MergeUpdate']['barcode']; ?></small></div>
										<div class="col-8"><small><?php echo $items['MergeUpdate']['service_provider']; ?></small></div>
										<div class="col-11"><small><?php echo $items['MergeUpdate']['service_name']; ?></small></div>
										<div class="col-10"><small><?php echo $items['MergeUpdate']['delevery_country']; ?></small></div>
										<div class="col-11"><small><?php echo $items['MergeUpdate']['order_date']; ?></small></div>	
										<div class="col-10"><small><?php echo $items['MergeUpdate']['assign_user']; ?></small></div>	
										<div class="col-11"><small><?php echo $items['MergeUpdate']['process_date']; ?></small></div>	
										<div class="col-8"> 
										
										<a href="javascript:void(0);" onclick="prinSlip('<?php echo $items['MergeUpdate']['order_id']; ?>','<?php echo $items['MergeUpdate']['product_order_id_identify']; ?>')" class="btn btn-info btn-xs" role="button" title="Print Slip" target="_blank"><i class="glyphicon glyphicon-print"></i> S</a>
										<a href="javascript:void(0);" onclick="prinLabel('<?php echo $items['MergeUpdate']['order_id']; ?>','<?php echo $items['MergeUpdate']['product_order_id_identify']; ?>')" class="btn btn-warning btn-xs" role="button" title="Print Label"><i class="glyphicon glyphicon-print"></i> L</a>
										
										 </div>
										 
									</div>												
								</div>
								 
							<?php }?>
						</div>	
						
						<div class="col-sm-12 text-right" style="margin-top:10px;margin-bottom:10px;">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	
  		
function prinLabel(oid,split_id){	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Linnworksapis/domeshippingnew',
		type: "POST",				
		cache: false,			
		data : {openOrderId:oid,splitOrderId:split_id },
		success: function(data, textStatus, jqXHR)
		{	
			 	 
			if(data.error){
				alert(data.msg);
			} 							
		}		
 	});  		 
}		
function prinSlip(oid,split_id){	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Linnworksapis/generatepdfdirect',
		type: "POST",				
		cache: false,			
		data : {openOrderId:oid,splitOrderId:split_id },
		success: function(data, textStatus, jqXHR)
		{	
 			if(data.error){
				alert(data.msg);
			} 							
		}		
 	});  		 
}
</script>
