<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
		<h1 class="page-title"><?php print $title;?> ( Total : <?php echo  $countprocess; ?> )</h1>
		<div class="panel-title no-radius bg-green-500 color-white no-border">
		</div>
		<div class="panel-head"><?php print $this->Session->flash(); ?></div>
    </div>
  
    <div class="container-fluid">
		<div class="row">
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">									
						</div>
						
						<div class="" style="margin:0 20px 20px;">
						
							<div class="pagination">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->prev( 'Previous', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled myclass1', 'tag' => 'li' ) );
            echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'disabled myclass' ) );
            echo $this->Paginator->next( 'Next', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled myclass1', 'tag' => 'li' ) );
        ?>
    </ul><?php echo $this->Paginator->counter(); ?>
</div>
							
						</div>
						<div class="panel-body no-padding-top bg-white">											
									<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
											<thead>
									<tr role="row">
										<th style="width: 2%;" >Order Id</th>
										<th style="width: 2%;" >Name</th>
										<th style="width: 2%;" >Address</th>
										<th style="width: 2%;" >Source</th>
										<th style="width: 9%;">Curr.</th>
										<th style="width: 9%;">Cost</th>
										<th style="width: 9%;">Item</th>
										<th style="width: 9%;">Title</th>
										<th style="width: 9%;">Qty</th>
										<th style="width: 9%;">Order Date</th>
										<th style="width: 9%;">Processed Date</th>
										<th style="width: 9%;">Operater</th>
									</tr>
								</thead>
							<tbody role="alert" aria-live="polite" aria-relevant="all">
										<?php if( !empty( $processOrderDetails ) && count($processOrderDetails) > 0 )
										{
											App::import('Controller', 'Processorders');
											$getname 	=	new ProcessordersController;
											$fullname	=	$getname->getUserDetail();
										foreach( $processOrderDetails as $processindex	=>	$processvalue)
										//pr( $processvalue );
										{
										?>
									<tr class="odd <?php //echo $alertClass; ?>">
										<td class="  sorting_1"><?php echo $processvalue['num_order_id']; ?></td>
										<td class="  sorting_1"><?php echo $processvalue['customer_info']->Address->FullName; ?></td>
										<td class="  sorting_1">
											<?php 
											echo ($processvalue['customer_info']->Address->Address1 != '') ? $processvalue['customer_info']->Address->Address1."<br>" : ''; 
											//echo ($processvalue['customer_info']->Address->Address2 != '') ? $processvalue['customer_info']->Address->Address2."<br>" : ''; 
											//echo ($processvalue['customer_info']->Address->Address3 != '') ? $processvalue['customer_info']->Address->Address3."<br>" : ''; 
											//echo ($processvalue['customer_info']->Address->Town != '' ) ? $processvalue['customer_info']->Address->Town."<br>" : ''; 
											//echo ($processvalue['customer_info']->Address->Region != '') ? $processvalue['customer_info']->Address->Region."<br>" : ''; 
											echo ($processvalue['customer_info']->Address->PostCode != '') ? $processvalue['customer_info']->Address->PostCode."<br>" : ''; 
											//echo ($processvalue['customer_info']->Address->Country != '') ? $processvalue['customer_info']->Address->Country."<br>" : ''; 
											echo ($processvalue['customer_info']->Address->PhoneNumber) ? $processvalue['customer_info']->Address->PhoneNumber."<br>" : ''; 
											
											?>
										</td>
										<td class="  sorting_1"><?php echo $processvalue['general_info']->Source .'<br>'. $processvalue['general_info']->SubSource; ?></td>                                                
										<td class="  sorting_1"><?php echo $processvalue['totals_info']->Currency; ?></td>
										<td class="  sorting_1"><?php echo $processvalue['totals_info']->Subtotal; ?></td>
										<td class="  sorting_1">
											<?php foreach( $processvalue['items'] as $items) {  
											echo $items->SKU."<br>";
											} ?>
										</td>
										<td class="productTitle sorting_1">
											<?php foreach( $processvalue['items'] as $items) {  
											echo '<div class ="for_tooltip" rel="tooltip" title = "'.$items->Title.'" ><span>' . substr($items->Title, 0, 15 )."</div></span><br>";
											} ?>
										</td>
										<td class="  sorting_1">
											<?php 
											$total = 0; 
											foreach( $processvalue['items'] as $items) {  
											$total = $total + $items->Quantity;
											}
												echo $total;
											?>
										</td>
										<td class="sorting_1" ><?php echo str_replace('T','<br>',$processvalue['general_info']->ReceivedDate); ?></div></td>
										<td class="sorting_1"><?php echo $processvalue['date']; ?></td>
										<td class="sorting_1"><?php echo $fullname; ?></td>
									</tr>
									<?php } } ?>
								</tbody>
							</table>
							
							<div class="" style="margin:0 20px 20px;">
						
							<div class="pagination">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->prev( 'Previous', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled myclass1', 'tag' => 'li' ) );
            echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'disabled myclass' ) );
            echo $this->Paginator->next( 'Next', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled myclass1', 'tag' => 'li' ) );
        ?>
    </ul><?php echo $this->Paginator->counter(); ?>
</div>
							
						</div>
						</div>
					</div>
				</div>
              </div>
				<!-- BEGIN FOOTER -->
					<?php echo $this->element( 'footer' ); ?>
				<!-- END FOOTER -->
         </div>
    </div>
</div>
<script>

$(document).ready(function()
        { 
		$("[rel='tooltip']").tooltip();
		});

</script>


