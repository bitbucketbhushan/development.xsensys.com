<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Show All Combination </h1>
		<?php print $this->Session->flash();  ?>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head">
					<?php print $this->form->create( 'User', array( 'class'=>'form-horizontal') ); ?>
				</div>
				<div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
							<li>
							<a class="switch btn btn-success generateDemoPofile" href="/Sortingstations/showAllGroup/1" data-toggle="modal" data-target=""><i class=""></i>Section 1</a>
							</li>
							<li>
								<a class="switch btn btn-success generateDemoPofile" href="/Sortingstations/showAllGroup/2" data-toggle="modal" data-target=""><i class=""></i>Section 2</a>
							</li>
						</ul>
					</div>
				</div>	
			</div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
       <div class="row">
				<div class="col-lg-12">
						<div class="panel no-border ">
							<div class="panel-title bg-white no-border">
							<div class="panel-body no-padding-top bg-white">																		
								<div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">
									<div class="row">
										<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Id</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Name: activate to sort column ascending">Position </th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Type: activate to sort column ascending">Combination Name</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Type: activate to sort column ascending">Section</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Type: activate to sort column ascending">Postal Provider</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Type: activate to sort column ascending">Country</th>
												<th role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" title = "Service" >Service</th>
												<th role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" title = "Color" >Color</th>
												<th role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" title = "Edit Role Name" >Action</th>
										</thead>
											<tbody role="alert" aria-live="polite" aria-relevant="all">
												<?php $i = 1; foreach( $getallgroups as $getallgroup ) { 
															$color = $getallgroup['SortingStation']['color'];
												?>
													<tr class="odd">
														<td class="sorting_1"><?php echo $i; ?></td>
														<td class="sorting_1">
														<?php echo $getallgroup['SortingStation']['position']; ?>
														<?php if( $getallgroup['SortingStation']['section'] == '1' ) { ?>
														<select class="form-control" onchange = "manageposition(<?php echo $getallgroup['SortingStation']['id']; ?>);" id = "pid_<?php echo $getallgroup['SortingStation']['id']; ?>" for="<?php echo $getallgroup['SortingStation']['section']; ?>" data="<?php echo $getallgroup['SortingStation']['position']; ?>"> 
														<option value="Select">Select</option>
														<?php 
														foreach( $getallleft as $val ) { 
														 if( $val['SortingStation']['position'] != $getallgroup['SortingStation']['position']) { ?>
														<option value="<?php echo $val['SortingStation']['position']; ?>"><?php echo $val['SortingStation']['position']; ?></option>
														<?php } } ?>
														 </select>
														 <?php } ?>
														 <?php if( $getallgroup['SortingStation']['section'] == '2' ) { ?>
														<select class="form-control" onchange = "manageposition(<?php echo $getallgroup['SortingStation']['id']; ?>);" id = "pid_<?php echo $getallgroup['SortingStation']['id']; ?>" for="<?php echo $getallgroup['SortingStation']['section']; ?>" > 
														<option value="Select">Select</option>
														<?php 
														foreach( $getallkright as $valr ) { 
														 if( $valr['SortingStation']['position'] != $getallgroup['SortingStation']['position']) { ?>
														<option value="<?php echo $valr['SortingStation']['position']; ?>"><?php echo $valr['SortingStation']['position']; ?></option>
														<?php } } ?>
														 </select>
														 <?php } ?>
														</td>
														<td class="sorting_1"><?php echo $getallgroup['SortingStation']['group_box']; ?></td>
														<td class="sorting_1"><?php echo ($getallgroup['SortingStation']['section'] == '1') ? 'Left' : 'Right'; ?></td>
														<td class="sorting_1"><?php echo $getallgroup['SortingStation']['postal_provider_name']; ?></td>
														<td class="sorting_1"><?php echo str_replace(",","<br>",$getallgroup['SortingStation']['country_name']); ?></td>
														<td class="sorting_1"><?php echo str_replace(",","<br>",$getallgroup['SortingStation']['service_name']); ?></td>
														<td class="sorting_1" style="background-color: #<?php echo $color; ?>" ></td>
														<td class="sorting_1">
															<a href="UpdateCombination/<?php echo $getallgroup['SortingStation']['id'] ?>" title="Edit" class="btn btn-success btn-xs margin-right-10"><i class="fa fa-pencil"></i></a>
															<a href="deleteCombination/<?php echo $getallgroup['SortingStation']['id'] ?>" title="Edit" class="btn btn-danger btn-xs margin-right-10"><i class="fa fa-close"></i></a>
														</td>
												    </tr>
												<?php $i++; } ?>
										</tbody>
									</table>
							</div>
						</div>
					</div><!-- /.col -->	
			</div><!-- /. row -->
			
		</div> 
    </div><!-- BEGIN FOOTER -->
			<?php echo $this->element('footer'); ?>
			<!-- END FOOTER -->
</div>


<script>
function manageposition( id )
{
		  var newposition 	= 	$( "#pid_"+id+" option:selected" ).val();
		  var section 		= 	$( '#pid_'+id ).attr('for');
		  var cpos			=	$( '#pid_'+id ).attr('data');
		  
		  /*alert(newposition);
		  alert(section);
		  alert(cpos);
		  return false;*/
		  
		  $.ajax({
				dataType: 'json',
				url : '/Sortingstations/updatecombinationposition',
				type: "POST",				
				cache: false,			
				data : {id:id,newposition:newposition, section:section,cpos:cpos },
				success: function(data)
				{	
					if(data == 1)
					{
						location.reload( true );
					}
				}
			});
	
}
</script>


