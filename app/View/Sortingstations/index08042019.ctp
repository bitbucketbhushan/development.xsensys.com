<div class="rightside bg-grey-100" style="margin-left: 0px;">
   <!-- BEGIN PAGE HEADING -->
   <div class="page-head bg-grey-100">
      <h1 class="page-title">Sorting Station &nbsp;<span id="timer" style="font-size: 13px; color: green; display: inline;">( You can refresh your screen to download NEW orders. )</span>
      </h1>
   </div>
   <!-- END PAGE HEADING -->
   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-5 no-padding">
            <div class="panel sortingSerivesLeft">
               <div class="panel-title bg-theme color-white no-border">
                  <div class="panel-head">Services List</div>
                  <div class="panel-tools">                                                                                                             </div>
               </div>
			   
               <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 100%; height: 430px;">
                  <div class="panel-body bg-grey-400" style="overflow: hidden; width: 100%; height: 430px; padding-top:0px;">
                     <!-- Now, Service would be render -->   
				
                    <div class="col-sm-12" style="height:141px; padding-right: 0px; padding-left: 0px;">
						<!--SortingStation-->
							<?php  
								$row = 0;
								foreach( $getshortorders as $getshortorder ) { 
					 				if($getshortorder['SortingStation']['section'] == '1') {  
									
								     if($row >0 && $row % 6 == 0){ echo '</div> <div class="col-sm-12" style="height:141px; padding-right: 0px; padding-left: 0px;">';}
							?>  
								  
									<div class="col-sm-2 sortingBox  margin-top-5" id="<?php echo $getshortorder['SortingStation']['id'];  ?>" >
									   <div data-customclass="ffff00" class="panel" >
										  <div class="panel-body padding-5">
											 <div class="clearfix ">
												 <div class="top margin-right-5">
												   <span class="font-size-12 color-blue-800"><?php echo $getshortorder['SortingStation']['group_box']  ?></span>
												</div>
												<div class="">
												   <strong>Total:&nbsp;</strong>
												   <div class="font-size-12 font-weight-600 line-height-15 customTotal"><?php echo $getshortorder['SortingStation']['totalcount']  ?></div>
												</div>
												<div class="">
												   <strong>Scanned:&nbsp;</strong>
												   <div class="font-size-12 font-weight-600 line-height-15 custom"><?php echo $getshortorder['SortingStation']['scaneed']  ?></div>
												</div>
											 </div>
										  </div>
									   </div>
									  </div>
								   
						 <?php $row++; } ?>
						  <?php } ?>
                           <!-- /.panel -->
						 </div> 
					
					
					
					
					
					 <!--<div class="row">
					 <?php foreach( $getshortorders as $getshortorder ) { 
					 		if($getshortorder['SortingStation']['postal_provider_id'] == '2' || $getshortorder['SortingStation']['postal_provider_id'] == '9') { 
					 ?>
                        <div class="col-sm-2 sortingBox  margin-top-5" id="<?php echo $getshortorder['SortingStation']['id'];  ?>" >
                           <div data-customclass="ffff00" class="panel margin-bottom-5" >
                              <div class="panel-body padding-5">
                                 <div class="clearfix ">
								 	 <div class="top margin-right-5">
                                       <span class="font-size-12 color-blue-800"><?php echo $getshortorder['SortingStation']['group_box']  ?></span>
                                    </div>
                                    <div class="">
									   <strong>Total:&nbsp;</strong>
                                       <div class="font-size-12 font-weight-600 line-height-15 customTotal"><?php echo $getshortorder['SortingStation']['totalcount']  ?></div>
                                    </div>
                                    <div class="">
									   <strong>Scanned:&nbsp;</strong>
                                       <div class="font-size-12 font-weight-600 line-height-15 custom"><?php echo $getshortorder['SortingStation']['scaneed']  ?></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                          
                        </div>
						<?php } } ?>
				      </div>-->
                  </div>
                  <div class="slimScrollBar" style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 430px;"></div>
                  <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
               </div>
            </div>
         </div>
         <div class="col-lg-2 padding-left-5 padding-right-5">
            <div class="panel">
               <div class="panel-title bg-green-600 color-white no-border">
                  <div class="panel-head">Sorting Station</div>
                  <div class="panel-tools">                                                                                                             </div>
               </div>
               <div class="panel-body panelBody bg-green-100" style="height:430px">
                  <a href="#" class="panel padding-5 color-grey-400 display-block text-center margin-bottom-20">
                  <input id="LinnworksapisBarcode" index="0" value="" class="get_sku_string form-control" name="data[Linnworksapis][barcode]" type="text">
                  </a>
                  <a href="#" class="panel padding-md btn-dark bg-red-500 color-white display-block text-center">
                     <div class="bold">
                        ESL                     </div>
                  </a>
                  <form action="/cronjobs/createCutOffList" class="form-horizontal" id="sortinStation" method="post" accept-charset="utf-8">
                     <div style="display:none;"><input name="_method" value="POST" type="hidden"></div>
                     <button formaction="/JijGroup/System/Manifest/Create" class="btn bg-orange-500 color-white btn-dark col-sm-12 margin-bottom-10" type="submit">Manifest</button>
					 <button formaction="/Sortingstations" class="btn bg-green-500 color-white btn-dark col-sm-12 margin-bottom-10" type="submit">Refresh</button>																			
                     <!--<button type="button" onclick="window.print();" class="btn bg-orange-500 color-white btn-dark col-sm-12 margin-bottom-10">Screen Shot</button>-->
                  </form>
               </div>
            </div>
         </div>
         <div class="col-lg-5 no-padding">
            <div class="panel sortingSerivesLeft">
               <div class="panel-title bg-theme color-white no-border">
                  <div class="panel-head">Services List</div>
                  <div class="panel-tools">                                                                                                             </div>
               </div>
              <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 100%; height: 430px;">
                  <div class="panel-body bg-grey-400" style="overflow: hidden; width: 100%; height: 430px; padding-top:0px;">
                     <!-- Now, Service would be render -->     
					 
					 <div class="col-sm-12" style="height:141px; padding-right: 0px; padding-left: 0px;">
						<!--SortingStation-->
							<?php  
								$row = 0;
								foreach( $getshortorders as $getshortorder ) { 
					 				if($getshortorder['SortingStation']['section'] == '2' ) {  
									
								     if($row >0 && $row % 6 == 0){ echo '</div> <div class="col-sm-12" style="height:141px; padding-right: 0px; padding-left: 0px;">';}
							?>  
								  
									<div class="col-sm-2 sortingBox  margin-top-5" style="height:136px;" id="<?php echo $getshortorder['SortingStation']['id'];  ?>" >
									   <div data-customclass="ffff00" class="panel"  style="height:137px;" >
										  <div class="panel-body padding-5">
											 <div class="clearfix ">
												 <div class="top margin-right-5">
												   <span class="font-size-12 color-blue-800"><?php echo $getshortorder['SortingStation']['group_box']  ?></span>
												</div>
												<div class="">
												   <strong>Total:&nbsp;</strong>
												   <div class="font-size-12 font-weight-600 line-height-15 customTotal"><?php echo $getshortorder['SortingStation']['totalcount']  ?></div>
												</div>
												<div class="">
												   <strong>Scanned:&nbsp;</strong>
												   <div class="font-size-12 font-weight-600 line-height-15 custom"><?php echo $getshortorder['SortingStation']['scaneed']  ?></div>
												</div>
											 </div>
										  </div>
									   </div>
									  </div>
								   
						 <?php $row++; } ?>
						  <?php } ?>
                           <!-- /.panel -->
						 </div> 
					 
					 
                     <!--<div class="row">
					 <?php foreach( $getshortorders as $getshortorder ) { 
					 		if($getshortorder['SortingStation']['postal_provider_id'] == '1') { 
					 ?>
                        <div class="col-sm-2 sortingBox  margin-top-5" id="<?php echo $getshortorder['SortingStation']['id'];  ?>" >
                           <div data-customclass="ffff00" class="panel" >
                              <div class="panel-body padding-5">
                                 <div class="clearfix ">
								 	 <div class="top margin-right-5">
                                       <span class="font-size-12 color-blue-800"><?php echo $getshortorder['SortingStation']['group_box']  ?></span>
                                    </div>
                                    <div class="">
									   <strong>Total:&nbsp;</strong>
                                       <div class="font-size-12 font-weight-600 line-height-15 customTotal"><?php echo $getshortorder['SortingStation']['totalcount']  ?></div>
                                    </div>
                                    <div class="">
									   <strong>Scanned:&nbsp;</strong>
                                       <div class="font-size-12 font-weight-600 line-height-15 custom"><?php echo $getshortorder['SortingStation']['scaneed']  ?></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- /.panel -->
                        </div>
						<?php } } ?>
				      </div>-->
                  </div>
                  <div class="slimScrollBar" style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 430px;"></div>
                  <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<style>
.sortingBox {
    padding: 0px 1px;
    width: 86px;
}
</style>

<script>

 	 $(function() {                     
					console.log( "****************************" );                    
					console.log( "Euraco is ready to processed" );
					console.log( "****************************" );
					$("#LinnworksapisBarcode").focus();
                });

				$(document).keypress(function(e) {
                                if(e.which == 13) {
                                                checkOperatorBarcodeValue();
                                }
                });
                $(document).keypress(function(e) {
                                if(e.which == 32) {
                                                var url = $('.btn-success').attr('href');
                                                window.open(url,'_self');
                                }
                });
				
	
	function checkOperatorBarcodeValue()
                {
                    var Barcode        =             $('#LinnworksapisBarcode').val();                                             
					if( Barcode == '' )
					{
						console.log( "****************************" );
						var number = 1 + Math.floor(Math.random() * 20000000000000000000);
						console.log( "Oops, Your barcode is blank![ " + number + " ]" );
						console.log( "****************************" );
						return false;
					}
                                
                                $('.searchbarcode').text(Barcode);
                                $.ajax(
                                {
									'url'            : '/Sortingstations/checkBarcodeForSortingOperator',
									'type'           : 'POST',
									'data'           : { barcode : Barcode },                                            
									'beforeSend'      : function() {
														},
									'success'         : function( strArray )
														{   
															var str 		= strArray;
															var splitStr 	= str.split('####');
															var msgArray 	= splitStr[0];
															if( msgArray == 1 ){
																var user1 		= splitStr[1];
																var Pc1 		= splitStr[2];
																var orddid1 	= splitStr[3];
																var loc1 		= splitStr[4];
																
															} else {
																var cat 		= splitStr[1];
															}
															
															if( cat == 'alkaline battery')
														    {
																alert( 'This Spain Order Have Battery.' );
															}
														   if( msgArray == 1)
														    {
																swal('This order ( '+orddid1+' ) have been cancelled by: '+user1+' on pkst: '+Pc1+'. Please put on Location:'+loc1);
																return false;
															}
														  if( msgArray == 2)
														    {
																var user2 		= splitStr[1];
																var Pc2 		= splitStr[2];
																var orddid2 	= splitStr[3];
																var loc2 		= splitStr[4];
																	swal({
																		  title: "Order Cancelled",
																		  text: 'This order ( '+orddid2+' ) have been cancelled by: '+user2+' on pkst: '+Pc2+'. Please put on Location:'+loc2+' Kindely search that order and confirmed.',
																		  type: "warning",
																		  showCancelButton: true,
																		  confirmButtonClass: "btn-danger",
																		  confirmButtonText: "Confirmed",
																		  closeOnConfirm: false
																		},
																		function(){
																			 $.ajax({
																			 		'url'            : '/Sortingstations/batchOrderPutBack',
																					'type'           : 'POST',
																					'data'           : { ordid : orddid2 }, 
																					'success'        : function( strArray )
																											{
																											   if(strArray == 1){
																											   		swal("Put Back!", "Order put back successfully.", "success");
																											   } else {
																											   		swal("There is an error. Please check");
																											   }
																											}
																					
																			 		});
																		});
																
																//swal('This order ( '+orddid2+' ) have been cancelled by: '+user2+' on pkst: '+Pc2+'. Please put on Location:'+loc2+' Kindely search that order and confirmed.');
																return false;
															}																					
														   if( msgArray != 'scanned' && msgArray != 'none'  && msgArray != 'scannedbattery')
														   {          
																var res 	= msgArray.split("---"); 																							   																msgArray 	= res[0];
																var color 	= res[1];
																var ocount 	= res[2];
																var customIncrementer = $( '#'+msgArray ).find( 'div.custom' ).html();
																//alert(res+'>>>>'+msgArray+'>>>>>'+color);
																//alert(customIncrementer);
																
																if( customIncrementer == "0" )
																{
																	$( '#'+msgArray ).find( 'div.panel' ).removeAttr('style');
																	$('#LinnworksapisBarcode').val('');
																	customIncrementer = 0;
																	customIncrementer = customIncrementer + parseInt(ocount);
																	
																	$( '#'+msgArray ).find( 'div.custom' ).html("");
																	$( '#'+msgArray ).find( 'div.custom' ).html( customIncrementer );
																													
																	setTimeout(function ()
																	{
																		$( 'div.panel' ).removeAttr( 'style' )
																		var customColor = $( '#'+msgArray ).attr( 'data-customclass' );
																		//$('#'+msgArray).attr( 'style' , 'background-color:#'+color+';' )
																		$('#'+msgArray).find( 'div.panel' ).attr( 'style' , 'background-color:#'+color+';')
																		$( '.cut_off' ).removeAttr( 'disabled' );
																					
																	}, 250);                                                                                                                                                                  
																}
																else
																{
																	$( '#'+msgArray ).find( 'div.panel' ).removeAttr('style');
																	$('#LinnworksapisBarcode').val('');
																	customIncrementer = parseInt(customIncrementer) + parseInt(ocount);
																	
																	$( '#'+msgArray ).find( 'div.custom' ).html("");                                                                 	$( '#'+msgArray ).find( 'div.custom' ).html( customIncrementer );
																	
																	setTimeout(function ()
																	{
																		$( 'div.panel' ).removeAttr( 'style' )
																		var customColor = $( '#'+msgArray ).attr( 'data-customclass' );
																		//$('#'+msgArray).attr( 'style' , 'background-color:#'+color+';' )
																		$('#'+msgArray).find( 'div.panel' ).attr( 'style' , 'background-color:#'+color+';')
																		$( '.cut_off' ).removeAttr( 'disabled' );
																	}, 250);                                                                                                                                                                  
																}
															}
														   else
														   {
														   		$('#LinnworksapisBarcode').val('');
																if( msgArray == 'none' )
																{
																	swal("Oops, Barcode not found in our records!!" , "" , "error");	
																	return false;
																}
																else if( msgArray == 'scanned' || msgArray == 'scannedbattery')
																{
																	if( msgArray == 'scannedbattery' )
																	{
																		alert( 'This order have battery' );
																	}
																	$.ajax(
																	{
																		'url'            : '/Sortingstations/getScannedLocation',
																		'type'           : 'POST',
																		'data'           : { barcode : Barcode },                                            
																		'beforeSend'      : function() {
																											//$('.loading-image').show();
																							},
																		'success'         : function( msgArray )
																							{
																								var res 	= msgArray.split("---"); 	
																								msgArray 	= res[0];
																								var color 	= res[1];
																								var user 	= res[2];
																								var pc 		= res[3];
																								var oid 	= res[4].toUpperCase();
																								
																								setTimeout(function ()
																								{
																									$( 'div.panel' ).removeAttr( 'style' )
																									var customColor = $( '#'+msgArray ).attr( 'data-customclass' );
																									$('#'+msgArray).attr( 'style' , 'background-color:#'+color+';' )
																									$('#'+msgArray).find('div.panel').attr('style','background-color:#'+color+';')
																									$('#'+msgArray).attr('style','background-color:136px;')
																									$( '.cut_off' ).removeAttr( 'disabled' );	
																									swal( 'Order ( '+oid+' ) is already scanned by: '+user+' on pkst: '+pc );
																								}, 250);
																								
																							}
																	});
																}
															}
										   				}  
                                					});                                                                                           
                								}
	
	
	

</script>
