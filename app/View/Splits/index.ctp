<style>
.head-row{background:#ccc; font-weight:bold;padding: 7px;}
.normal-row{padding: 5px; border-bottom:1px solid #CCCCCC;}
.split-div{margin-top: 35px;padding: 5px;background: #f8f8f8;border-radius: 10px; } 
 
</style> 

<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Order Split</h1>
       	<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
    </div>
    <div class="container-fluid  bg-white ">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel no-border ">
				<div class="panel-title no-border">
				 <form action="<?php echo Router::url('/', true); ?>Splits/index/" method="get">
					<div class="col-sm-3">
					 <input type="text" class="form-control" id="searchkey" name="searchkey" placeholder="Search By Order Id" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey']: '' ;?>"> 
					</div>
					
					<div class="col-sm-2">
						<button type="submit" class="btn btn-success">Search</button>
					</div>
					 </form>								
					<div class="panel-tools">
					</div>
				</div>
			</div>
		</div> 	
	</div> 
	
	<?php if(count($order_detail))	{  ?> 
	<div class="row">
		<div class="col-lg-12 head-row">	 
			<div class="col-sm-1">ORDER ID</div>
			<div class="col-sm-1">SOURCE</div>
			<div class="col-sm-1">COUNTRY</div>
			<div class="col-sm-2">SKU</div>
			<div class="col-sm-1">PRICE</div>
			<div class="col-12">POSTAL PROVIDER</div>
			<div class="col-10">SERVICE</div>
			<div class="col-sm-1">WEIGHT</div>
			<div class="col-10">DIMENTIONS(LxWxH)</div>
    </div>
  </div>
   
   <?php
     foreach( $order_detail as  $o_detail ) {  ?>
	<div class="row">
		<div class="col-lg-12 normal-row">	 
		<div class="col-sm-1"><?php echo $o_detail['MergeUpdate']['product_order_id_identify']."<br>"; ?></div>
		<div class="col-sm-1"><?php echo $o_detail['MergeUpdate']['source_coming']; ?></div>
		<div class="col-sm-1"><?php echo $o_detail['MergeUpdate']['delevery_country']; ?></div>
		<div class="col-sm-2"><?php echo $o_detail['MergeUpdate']['sku']; ?></div>
        <div class="col-sm-1"><?php echo $o_detail['MergeUpdate']['price']; ?></div>
      	<div class="col-12"><?php echo $o_detail['MergeUpdate']['service_provider']."(".$o_detail['MergeUpdate']['postal_service'].")"; ?> </div>
	    <div class="col-10">&nbsp;<?php echo $o_detail['MergeUpdate']['service_name']?$o_detail['MergeUpdate']['service_name']:'-'; ?></div>
	    <div class="col-sm-1"><?php echo $o_detail['MergeUpdate']['packet_weight']; ?></div>
      <div class="col-10"><?php echo 'L:'.$o_detail['MergeUpdate']['packet_length'].' x W:'.$o_detail['MergeUpdate']['packet_width'].' x H:'.$o_detail['MergeUpdate']['packet_height']; ?></div>
    </div>
	 </div>
	<?php } ?>
	<div class="split-div">
	<div class="row">
		<div class="col-lg-12">	
		  <form action="<?php echo Router::url('/', true); ?>Splits/index/" method="get" id="frm" name="frm">
 			<div class="col-sm-2"> <strong>Select Split Count:</strong></div>
			 <div class="col-sm-1">
			 <select name="split_count" class="form-control">
				<option value="2" <?php if(isset($_GET['split_count']) && $_GET['split_count'] == 2){ echo ' selected="selected"';}?>>2</option>
				<option value="3" <?php if(isset($_GET['split_count']) && $_GET['split_count'] == 3){ echo ' selected="selected"';}?>>3</option>
				<option value="4" <?php if(isset($_GET['split_count']) && $_GET['split_count'] == 4){ echo ' selected="selected"';}?>>4</option>
				<option value="5" <?php if(isset($_GET['split_count']) && $_GET['split_count'] == 5){ echo ' selected="selected"';}?>>5</option>		 
			 </select>	
			 <input type="hidden" name="searchkey" value="<?php echo $_REQUEST['searchkey']?>" />	 	
			 </div>
		 
		 <div class="col-sm-1"><button type="submit" class="btn btn-primary">Go</button></div>
		 </form>
		  
    </div>
	</div>
	<?php 
	if(isset($_REQUEST['split_count'])){
	
	 echo  '<form action="'. Router::url('/', true) .'Splits/Verify/" method="post" name="order" id="order">';
	 
		for($i=1; $i<=$_REQUEST['split_count']; $i++){
		
			echo '<div class="row" style="padding-top:2px;padding-bottom:2px;">';
			echo '<div class="col-lg-12">';		
				echo' <div class="col-sm-1">'. $_REQUEST['searchkey']. '-'.$i.'</div>';
				echo' <div class="col-sm-4"> <input type="text" name="sku[]" class="form-control" placeholder="2XS-SKU1,1XS-SKU2"/></div>';
			echo '</div>';
			echo '</div>';
		
		}
		echo '<input type="hidden" name="searchkey" value="'.$_REQUEST['searchkey'].'" />'; 
		echo '<button type="button" class="btn btn-danger" onClick="VerifyOrder();">Verify</button>';
		echo ' </form>';
	}
	?>
	</div>
	<div id="sp-orders"></div>
	
		
 <?php
 }
 
	 else{
	 	echo 'No order found.';
	 }
 
   ?>
		 		
			
    </div>
</div>
<script>
	
	function VerifyOrder()
	{
		$("#sp-orders").html('<center><img src="<?php echo Router::url('/', true) ?>img/ajax-loader.gif" alt="Loading..." /></center>');
		
		var formData = new FormData($('#order')[0]);
	
			$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Splits/VerifyOrder/',
			type: "POST",
			async: false,		
			cache: false,
			contentType: false,
			processData: false,
			data : formData,
			success: function(data, textStatus, jqXHR)
			{		
				 $("#sp-orders").html(data.html);					 						
				//$('#message').delay(20000).fadeOut('slow');							
			}		
		});	
	}
	
	function SaveSubOrder()
	{
		var formData = new FormData($('#order')[0]);
	
			$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Splits/SaveSubOrder/',
			type: "POST",
			async: false,		
			cache: false,
			contentType: false,
			processData: false,
			data : formData,
			success: function(data, textStatus, jqXHR)
			{		
				$("#msg").show();
				$("#msg").html(data.msg);					 						
				$('#msg').delay(20000).fadeOut('slow');							
			}		
		});	
	}
	
	$('#SplitOrderid').bind('keypress', function(e)
	{
	   if(e.which == 13) { 
		   window.location.href = '<?php echo Router::url('/', true) ?>Splits/index/'+$("#SplitOrderid").val();
		}
	});

</script>
