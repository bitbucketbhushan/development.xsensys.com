<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Order Split</h1>
       	<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
    </div>
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">
									<?php
										print $this->form->input( 'Split.orderid', array( 'placeholder'=>'Search Order by order id','type'=>'text','class'=>'form-control searchtext selectpicker bg-white','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
									?> 										
									<div class="panel-tools">
									</div>
								</div>
                            </div>
                        </div>
						 
						
						
                    </div>
					<!--<div class="container-fluid">
					<?php foreach( $o_details as  $o_detail ) {  ?>
					   <div class="row">
						  <div class="col-sm-3 panel">
						  	<?php echo $o_detail['MergeUpdate']['source_coming']."<br>"; ?>
							<?php echo $o_detail['MergeUpdate']['delevery_country']."<br>"; ?>
							<?php echo $o_detail['MergeUpdate']['source_coming']."<br>"; ?>
							<?php echo $o_detail['MergeUpdate']['source_coming']."<br>"; ?>
						  </div>
						  <div class="col-sm-3 panel">
						  	<?php echo $o_detail['MergeUpdate']['sku']."<br>"; ?>
							<?php echo $o_detail['MergeUpdate']['sku']."<br>"; ?>
							<?php echo $o_detail['MergeUpdate']['sku']."<br>"; ?>
							<?php echo $o_detail['MergeUpdate']['sku']."<br>"; ?>
						  </div>
						  <div class="col-sm-3 panel">
						  	<?php echo $o_detail['MergeUpdate']['service_provider']."<br>"; ?>
							<?php echo $o_detail['MergeUpdate']['service_name']."<br>"; ?>
							<?php echo $o_detail['MergeUpdate']['provider_ref_code']."<br>"; ?>
							<?php echo $o_detail['MergeUpdate']['postal_service']."<br>"; ?>
						  </div>
						  <div class="col-sm-3 panel">
							<?php echo "Weight: ".$o_detail['MergeUpdate']['packet_weight']."<br>"; ?>
							<?php echo "Length: ".$o_detail['MergeUpdate']['packet_length']."<br>"; ?>
							<?php echo "Width: ".$o_detail['MergeUpdate']['packet_width']."<br>"; ?>
							<?php echo "Height: ".$o_detail['MergeUpdate']['packet_height']."<br>"; ?>
						  </div>
						</div> 
					<?php } ?>
					</div>-->
					
					
					
		   
			
			<table class="table">
  <thead>
    <tr>
	  <th scope="col">id</th>
      <th scope="col">Detail</th>
      <th scope="col">SKU</th>
      <th scope="col">Service detail</th>
      <th scope="col">Dimension</th>
    </tr>
  </thead>
  <tbody>
   <?php foreach( $o_details as  $o_detail ) {  ?>
	<tr>
	 <td scope="row"><?php echo $o_detail['MergeUpdate']['product_order_id_identify']."<br>"; ?></td>
	 <td scope="row">
			<?php echo $o_detail['MergeUpdate']['source_coming']."<br>"; ?>
			<?php echo $o_detail['MergeUpdate']['delevery_country']."<br>"; ?>
			<?php echo $o_detail['MergeUpdate']['price']."<br>"; ?>
	  </td>
      <td>
	  	<?php echo $o_detail['MergeUpdate']['sku']."<br>"; ?>
	  </td>
      <td>
	  		<?php echo $o_detail['MergeUpdate']['service_provider']."<br>"; ?>
			<?php echo $o_detail['MergeUpdate']['service_name']."<br>"; ?>
			<?php echo $o_detail['MergeUpdate']['provider_ref_code']."<br>"; ?>
			<?php echo $o_detail['MergeUpdate']['postal_service']."<br>"; ?>
	  </td>
      <td>
	  		<?php echo "Weight: ".$o_detail['MergeUpdate']['packet_weight']."<br>"; ?>
			<?php echo "Length: ".$o_detail['MergeUpdate']['packet_length']."<br>"; ?>
			<?php echo "Width: ".$o_detail['MergeUpdate']['packet_width']."<br>"; ?>
			<?php echo "Height: ".$o_detail['MergeUpdate']['packet_height']."<br>"; ?>
	  </td>
    </tr>
	<?php } ?>
	<tr>
		 <td scope="row"></td>
		 <td scope="row">
		 	<input type="text" class="form-control" id="sku_qty" placeholder = "Put Sku like 3XS-MLTD111SELS,1XS-VR2541LY">
		 </td>
		 <td><button type="button" class="btn btn-primary" onclick="showsuborder( <?php echo $o_detail['MergeUpdate']['order_id']; ?> )" >Check</button></td>
		 <td></td>
		 <td></td>
    </tr>
	
  </tbody>
</table>
		<div id="suborder"></div>	
		 </div>		
				
    </div>
</div>
<script>
	
	function showsuborder( id )
	{
		var skuqty	=	$('#sku_qty').val();
		var skuqty	=	$('#sku_qty').val();
		var tab = '';
		$.ajax({
					url     	: '/Splits/showSubOrder',
					type    	: 'POST',
					dataType   	: 'JSON',
					data    	: { id : id, skuqty : skuqty },  			  
					success 	:	function( data  )
					{
						tab = '<table class="table"><tr><th>Sub Order ID</th><th>SKU</th><th>Weight</th><th>Price</th><tr>';
						tab += '<tr><td>'+data.mainorder.MergeUpdate.product_order_id_identify+'</td><td>'+data.mainorder.MergeUpdate.sku+'</td><td>'+data.mainorder.MergeUpdate.packet_weight+'</td><td>'+data.mainorder.MergeUpdate.price+'</td></tr>';
						tab += '<tr><td>'+data.neworder.MergeUpdate.product_order_id_identify+'</td><td>'+data.neworder.MergeUpdate.sku+'</td><td>'+data.neworder.MergeUpdate.packet_weight+'</td><td>'+data.neworder.MergeUpdate.price+'</td></tr>'
						tab +='<tr><td>&nbsp</td><td>&nbsp</td><td>&nbsp</td><td><button type="button" class="btn btn-primary">Create</td></tr></table>';
						$('#suborder').html(tab);
					}
			  });
			 
	}
	
	
	$('#SplitOrderid').bind('keypress', function(e)
	{
	   if(e.which == 13) { 
		   window.location.href = '<?php echo Router::url('/', true) ?>Splits/index/'+$("#SplitOrderid").val();
		}
	});

</script>
