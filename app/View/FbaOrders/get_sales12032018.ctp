<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.qtbr{min-height: 15px;border-bottom: 1px dotted #444;border-top: 1px dotted #444;font-size: 12px;text-align: center;color: #990000;}
	.cdiv{width:2.85%;text-align:center; float:left;}
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Sales Report</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
					 	<form method="get" name="salefrm" id="salefrm" action="<?php echo Router::url('/', true) ?>FbaOrders/getSales">	
						<div class="col-sm-6 no-padding ">
							 <div class="col-sm-3">					  
								  <select name="report_month" id="report_month" class="form-control" onChange="loadajax()">
								  <?php 
										for ($i = 1; $i <= 12; $i++)
										{	$sel ='';
											$month_name = date('F', mktime(0, 0, 0, $i, 1, 2016));
											
											if(isset($_REQUEST["report_month"])){
												if($_REQUEST['report_month'] == $i){ $sel = 'selected="selected"'; }
											}else{
												if(date('n') == $i){ $sel = 'selected="selected"'; }
											}											
											echo '<option value="'.$i.'" '.$sel.'>'.$month_name.'</option>';
										}
										?>						
									</select>
									
								 </div>
								  <div class="col-sm-3">					  
								  <select name="report_year" id="report_year" class="form-control" onChange="loadajax()">
								  <?php for ($i = 2017; $i <= date('Y'); $i++)
										{	
											$sel ='';
											if(isset($_REQUEST["report_year"])){
												if($_REQUEST['report_year'] == $i){ $sel = 'selected="selected"'; }
											}else{
												if(date('Y') == $i){ $sel = 'selected="selected"'; }
											}
											echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
										}?>						
									</select>					
						 </div>
						 <div class="col-sm-3">	
						 <?php
							if(isset($_REQUEST["report_year"]) && isset($_REQUEST["report_month"]) ){
								$report_year = $_REQUEST["report_year"];
								$report_month = str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT);
							}else{
								$report_year  = date('Y');
								$report_month = date('m'); 
							}
						 ?>				  
							<a href="<?php echo Router::url('/', true) ?>FbaOrders/getSalesReport/<?php echo $report_year."/".$report_month ?>" class="btn btn-success">Get Report</a>							
						 </div>	
						</div>
						 </form>
						<div class="col-sm-6 text-right">
						<ul class="pagination" style="display:inline-block">
							  <?php
							   if(count($all_orders) > 0) {
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   }
								   
							  ?>
						 </ul>  
					</div>	
					<div class="panel-body no-padding-top bg-white" style=" width:130%">
						
						 <div class="row head" style="clear:both;" >		 					   							
							<?php 
							
							App::Import('Controller', 'FbaOrders'); 
							$obj = new FbaOrdersController;	 
						
							
							$days_of_month = 30; $total_sale = 0;
							$order_items = array();
							echo '<div class="col-sm-1">SKU</div>';
							echo '<div class="col-sm-1">Seller SKU</div>';
							
								if(isset($_REQUEST["report_month"])){
									 $days_of_month = cal_days_in_month(CAL_GREGORIAN,$_REQUEST["report_month"],$_REQUEST["report_year"]);
								}else{												 
									$days_of_month = cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y'));
								}
								
								for ($i = 1; $i <= $days_of_month; $i++)
								{													
									 
									$qty = '-';	
									$sale  = 0;
									if(isset($_REQUEST["report_month"])){
										$date = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-'.str_pad($i, 2, "0", STR_PAD_LEFT); 
									  }	else{												 
										$date =   date('Y-m-').str_pad($i, 2, "0", STR_PAD_LEFT); 
									} 	 
										 
								$sale = $obj->getDateSale($date );    
								 	
								$total_sale += $sale;
								$mon  = date("d M y",strtotime($date));
								echo '<div class="cdiv">'. $mon.'<br><span class="qtbr">'.$sale.'</span></div>';			
							}
									
				 
			
						?>
						<div class="cdiv">Total Sale <?php echo date("M", strtotime($date));?><br><span class="qtbr"><?php echo $total_sale; ?></span></div>  	
					</div>	 
					<div class="body" id="results" >
						 <?php  		
						//pr($all_orders);
						
						 if(count($all_orders) > 0) {	 
							foreach($all_orders  as $item ) {
								$sku_t_sale = 0;
								echo '<div class="row sku">
									<div class="col-lg-12">';
								echo '<div class="col-sm-1"><small>'. $item['FbaOrderItem']['master_sku'].'</small></div>';
								echo '<div class="col-sm-1"><small>'. $item['FbaOrderItem']['seller_sku'].'</small></div>';
								for ($i = 1; $i <= $days_of_month; $i++)
								{					
									if(isset($_REQUEST["report_month"])){
										$date = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-'.str_pad($i, 2, "0", STR_PAD_LEFT); 
									  }	else{												 
										$date =   date('Y-m-').str_pad($i, 2, "0", STR_PAD_LEFT); 
									} 
									
									$sku_sale = $obj->getSkuSale($item['FbaOrderItem']['seller_sku'],$date ); 
									 if($sku_sale > 0){
										 echo '<div class="cdiv"><small>'. $sku_sale. '</small></div>';
									 }else{
										 echo '<div class="cdiv"><small>-</small></div>';
									 }												 
									 $sku_t_sale += $sku_sale;											 
								}	 	 
								
								echo '<div class="cdiv">'. $sku_t_sale. '</div>';  
								echo '</div>												
									</div>';
							 }  										 		 
								
							 
						 	} else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	
function loadajax(){

	document.getElementById("salefrm").submit();		 

}
</script>
