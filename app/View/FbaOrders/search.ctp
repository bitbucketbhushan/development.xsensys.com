<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Serach Result</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						<form method="get" name="searchfrm" action="<?php echo Router::url('/', true) ?>FbaOrders/search" enctype="multipart/form-data">	
						<div class="col-sm-8 no-padding ">
							<div class="col-lg-7 no-padding">
									<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Enter Order Id Or SKU." name="searchkey" class="form-control searchString" />
								</div>
								
								<div class="col-lg-5"> 
									<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
									&nbsp;<button type="button" class="btn btn-warning" onclick="window.location.href='<?php echo Router::url('/', true) ?>FbaOrders'">&nbsp;Go Back</button>
								</div>
						</div>
						</form>
						<div class="col-sm-6 text-right">
						<?php /*?><ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul><?php */?>  
						</div>	
						</div>	
					<div class="panel-body no-padding-top bg-white">
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-sm-2">Order Id</div>		
								<div class="col-sm-7">
									<div class="col-sm-12">
										<div class="col-sm-2">Sku</div>											
										<div class="col-sm-7">Title</div>			
										<div class="col-sm-1">Qty</div>																				
										<div class="col-sm-1">Item Price</div>
										<div class="col-sm-1">Shipping Price</div>	
									</div>
								</div>
								
								<div class="col-sm-2"><center>Purchase Date</center></div>	
								<div class="col-sm-1">Status</div> 
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						 if(count($all_orders) > 0) {	 ?>
							<?php foreach( $all_orders as $items ) {   ?>
								<div class="row sku" id="r_<?php echo $items['FbaOrder']['id']; ?>">
									<div class="col-lg-12">	
										<div class="col-sm-2"><small><?php echo $items['FbaOrder']['amazon_order_id']; ?></small></div>	
										<div class="col-sm-7">
										
										<?php foreach($items['FbaOrderItem'] as $item ) {
											echo '<div class="col-sm-12">';
												echo '<div class="col-sm-2"><small>'. $item['seller_sku'].'</small></div>';
												echo '<div class="col-sm-7"><small>'. $item['title'].'</small></div>';
												echo '<div class="col-sm-1"><small>'. $item['quantity_ordered'].'</small></div>';
												echo '<div class="col-sm-1"><small>'. $item['item_price'].'</small></div>';
												echo '<div class="col-sm-1"><small>'. $item['shipping_price'].'</small></div>';
											echo '</div>';
										 
										 } ?>
										</div>										
										<div class="col-sm-2"><small><center><?php echo $items['FbaOrder']['purchase_date']; ?></center></small></div>	
										<div class="col-sm-1"><small><?php echo $items['FbaOrder']['order_status']; ?></small></div>
										 
									</div>												
								</div>
								 
							<?php }?>
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	

$('#filter_shipment').click(function(){  
	document.getElementById("shipmentFrm").submit();		 
});
	
function updateStatus(t,v){	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaOrders/updateStatus',
		type: "POST",				
		cache: false,			
		data : {status:$(t).find("option:selected").val(),id:v },
		success: function(data, textStatus, jqXHR)
		{	
			 	
			if(data.error){
				alert(data.msg);
			} 							
		}		
 	});  		 
}
</script>
