<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.head div div div{ padding:0px ;  }
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.col-65 { width: 65%; float: left;}
	.col-60 { width: 60%; float: left;}
	.col-55 { width: 55%; float: left;}	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Suppliers</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
  						<div class="row" id="suppliers_data">
						<div class="table-responsive">	
								
						<table class="table table-striped">
						  <thead>
							<tr>
								<th width="10%">Supplier</th>
								<th>Supplier Code</th>
								<th width="15%">Email / Phone</th>
								<th>Lead Time</th>
								<th>Credit Limit</th>	
 								<th>Min Order Amount</th>	
								<th>Credit Period<br>(In days)</th>	
								<th>Currency </th>	
							   	<th width="20%">Address </th>	
								<th>SKU </th>	
								<th>See All PO / All Invoices</th>							
								<th align="center">Action</th>
							</tr>
 						  </thead>
						  <tbody  >
								<?php if(count($SupplierDetail) > 0){ 
								foreach($SupplierDetail as $val){
								?> 								
								<tr>
									<td><?php echo $val['SupplierDetail']['supplier_name']?></td>
									<td><?php echo $val['SupplierDetail']['supplier_code']?></td>
									<td><?php echo $val['SupplierDetail']['email']?> <br /><?php echo $val['SupplierDetail']['phone_number']?></td>	
									<td><?php echo $val['SupplierDetail']['lead_time']?></td>
									<td><?php echo $val['SupplierDetail']['credit_limit']?></td>	
									<td><?php echo $val['SupplierDetail']['min_order_amount']?></td>	
									<td><?php echo $val['SupplierDetail']['credit_period']?></td>
									<td><?php echo $val['SupplierDetail']['currency']?></td>	
									<td><?php echo $val['SupplierDetail']['address']?></td>									
									<td align="center"><a href="<?php echo Router::url('/', true).'SupplierDetails/SupplierAllSku/'.$val['SupplierDetail']['id']?>" style="font-size:11px; color:#0033CC; text-decoration:underline" >See All SKU</a> </td>
									<td align="center"><a href="<?php echo Router::url('/', true).'SupplierDetails/SupplierAllPo/'.$val['SupplierDetail']['id']?>" style="font-size:11px; color:#0033CC;text-decoration:underline" >See POs</a> <br /> 
									<a href="<?php echo Router::url('/', true).'SupplierDetails/SupplierAllInvoice'.$val['SupplierDetail']['id']?>" style="font-size:11px; color:#0033CC;text-decoration:underline" >See Invoices</a> 
									</td>
									<td align="center"><a href="<?php echo Router::url('/', true).'SupplierDetails/AddSupplier/'.$val['SupplierDetail']['id']?>" class="btn btn-info btn-sm" role="button">Edit</a><button type="button" class="btn btn-danger btn-sm" onclick="delSupplier(<?php echo $val['SupplierDetail']['id']?>)" style="margin-top:0px; margin-left:1px; margin-bottom:1px;">Delete</button>
									
									
									<a href="<?php echo Router::url('/', true).'SupplierDetails/SupplierGeneratePo/'.$val['SupplierDetail']['id']?>" class="btn btn-warning btn-sm" >Generate new PO</a>
									
									
									</td>
									
									
									
								</tr>
							<?php	}	};?>
						  </tbody>
						</table>
						<div class="" style="margin:0 auto; width:350px">
										 <ul class="pagination">
										  <?php
											   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
											   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
											   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										  ?>
										 </ul>
									</div
									
						</div>
						</div>	
					
						<div class="panel-body no-padding-top bg-white">
 							<br />
							<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
						</div>			
				  </div>
			  </div> 
			</div> 
		</div> 
	 	</div>  
</div> 		 
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999; al " src="<?php echo $this->webroot; ?>img/482.gif" />
</div> 
<script>	
$("#supplier_grid" ).load( "<?php echo Router::url('/', true) ?>SupplierDetails/ajaxGetSuppliers",{"supplier_code":$("#sec_supplier_code option:selected").val()}); 

function addData(){
	
	if($('#supplier_name').val() == ''){
		alert('Enter supplier name.');
	}else if($('#supplier_code').val() == ''){
		alert('Enter supplier code.');
	}else{
		
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>SupplierDetails/ajaxAddSupplier',
			type: "POST",
			beforeSend: function() {
 				$(".outerOpac").show(); 
			},
			data : {id:$('#id').val(),supplier_name:$('#supplier_name').val(),supplier_code:$('#supplier_code').val(),credit_period:$('#credit_period').val(),credit_limit:$('#credit_limit').val(),credit_limit_available:$('#credit_limit_available').val(),currency:$('#currency').val(),address:$('#address').val(),category_id:$("#category_id option:selected").val()},
			success: function(data, textStatus, jqXHR)
			{	
				$(".outerOpac").hide(); 	
 				alert(data.msg);	
				if(data.error !='yes'){
					$("#supplier_grid" ).load( "<?php echo Router::url('/', true) ?>Suppliers/ajaxGetSuppliers");	
				}				
				//$('#sup_message').delay(5000).fadeOut('slow');						
			},
			complete: function() {
				$(".outerOpac").hide(); 
			}		
		});
	}
}

function updateData(){
	
	if($('#supplier_name').val() == ''){
		alert('Enter supplier name.');
	}else if($('#supplier_code').val() == ''){
		alert('Enter supplier code.');
	}else{
		
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>SupplierDetails/ajaxUpdateSupplier',
			type: "POST",
			beforeSend: function() {
 				$(".outerOpac").show(); 
			},
			data : {id:$('#id').val(),supplier_name:$('#supplier_name').val(),supplier_code:$('#supplier_code').val(),credit_period:$('#credit_period').val(),credit_limit:$('#credit_limit').val(),credit_limit_available:$('#credit_limit_available').val(),currency:$('#currency').val(),address:$('#address').val(),category_id:$("#category_id option:selected").val()},
			success: function(data, textStatus, jqXHR)
			{	
				//$(".outerOpac").hide(); 	
 				alert(data.msg);	
				if(data.error !='yes'){
					$("#supplier_code").removeAttr("disabled"); 	
					$('#id').val('');
					$('#supplier_name').val(''); 
					$('#supplier_code').val('');				
					$('#credit_period').val('');
					$('#credit_limit').val('');
					$('#credit_limit_available').val('');
					$('#address').val('');
					$('#btn-add').show();
					$('#btn-update').hide(); 
 					$("#supplier_grid" ).load( "<?php echo Router::url('/', true) ?>Suppliers/ajaxGetSuppliers");	
				}				
								
			},
			complete: function() {
				$(".outerOpac").hide(); 
			}		
		});
	}
}

function editSupplier(id){
	 
		$("#loading-mask").show(); 
		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>SupplierDetails/ajaxGetSupplier',
		type: "POST",
		beforeSend: function() {
 				$(".outerOpac").show(); 
		},
		data : {id:id},
		success: function(data, textStatus, jqXHR)
		{	
 				$('#id').val(data.id);
				$('#supplier_name').val(data.supplier_name); 
				$('#supplier_code').val(data.supplier_code);
 				$("#supplier_code").attr("disabled", "disabled"); 
				$('#credit_period').val(data.credit_period);
				$('#credit_limit').val(data.credit_limit);
				$('#credit_limit_available').val(data.credit_limit_available);
				$('#currency').val(data.currency);
				$('#address').val(data.address);
				$('#btn-add').hide();
				$('#btn-update').show(); 
 						
 		},
		complete: function() {
				$(".outerOpac").hide(); 
			}	
	});
   
}

function delSupplier(id){
	if (confirm("Are you sure to delete this Supplier? It will delete all items and record of this supplier.")) {
		$("#loading-mask").show(); 
		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>SupplierDetails/ajaxDelSuppliers',
		type: "POST",
		beforeSend: function() {
 				$(".outerOpac").show(); 
		},
		data : {id:id},
		success: function(data, textStatus, jqXHR)
		{	
 			alert(data.msg);	
			if(data.error !='yes'){
				$("#supplier_grid" ).load( "<?php echo Router::url('/', true) ?>SupplierDetails/ajaxGetSuppliers");	
			}			
 		},
		complete: function() {
				$(".outerOpac").hide(); 
			}	
	});
  } 
}
</script>
