<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.head div div div{ padding:0px ;  }
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.col-65 { width: 65%; float: left;}
	.col-60 { width: 60%; float: left;}
	.col-55 { width: 55%; float: left;}	
	.bg{padding:10px; background-color:#CCCCCC; border-radius:5px;} 
	.not_added{border:2px dashed #FF0000!important;}
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<div class="col-lg-6"><h4 style="padding-right: 10px;float:left;">Supplier SKUs(<?php echo $this->params['pass'][0].' : '.$supplier_code;?>)</h4>
				<a href="<?php echo Router::url('/', true).'SupplierMappings/DownloadSku/'.$this->params['pass'][0]?>" style="float:left;" class="btn btn-info btn-sm" role="button">Download SKU</a></div>
				<div class="col-lg-6 text-right"><a href="<?php echo Router::url('/', true).'SupplierMappings/AddSupplierSku/'.$this->params['pass'][0]?>" class="btn btn-info btn-sm" role="button">Add/Map SKU</a></div>
			</div>
    	  <div class="container-fluid" style="padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto; position:static">							
			<div class="row " style="margin-left:0px;">	
				  <div class="col-sm-12 bg">
				  	<form method="post" name="supplier_data" action="<?php echo Router::url('/', true) ?>SupplierMappings/UploadFile" id="supplier_data" enctype="multipart/form-data">	 
						<div class="col-sm-3">
							<div class="input-group">
							<span class="input-group-btn">
								<span class="btn btn-primary btn-file">
									Browse <input type="file" size="60" name="supplierfile" id="supplierfile">	
								</span>
							</span>
							<input type="text" placeholder="No file selected" readonly="" class="form-control">
							<?php  print $this->form->input( 'sup_inc_id', array( 'type'=>'hidden' ,'value'=>$this->params['pass'][0]) );?>
							
							</div>
							<small><a href="<?php echo Router::url('/', true) ?>sample_file/supplier_sku_mapping.csv" style="text-decoration:underline">Download Sample File</a></small>
						</div>
					 
					 
					 <div class="col-sm-2"><button type="submit" class="btn btn-primary btn-sm">Upload</button></div>
					  </form>
					 <form method="get" name="search" id="search" action="<?php echo Router::url('/', true) ?>SupplierMappings/index/<?php echo $this->params['pass'][0]?>"> 
					 	<div class="col-sm-3"><input type="search" autocomplete="off" class="form-control" placeholder="Search By Our SKU or Supplier SKU" name="search_key" value="<?php if(isset($_REQUEST['search_key'])){ echo $_REQUEST['search_key'];}?>"> </div>
						<div class="col-sm-1"><button type="submit" class="btn btn-success btn-sm">Search</button></div>
					   <div class="col-lg-2 text-right">
					   <select class="form-control" name="filter" onchange="document.search.submit();">
						   <option value="" <?php if(isset($_REQUEST['filter']) && $_REQUEST['filter'] == ''){ echo 'selected="selected"' ;}?>>All</option>
						   <option value="added" <?php if(isset($_REQUEST['filter']) && $_REQUEST['filter'] == 'added'){ echo 'selected="selected"' ;}?>>Added</option>
						   <option value="not_added"<?php if(isset($_REQUEST['filter']) && $_REQUEST['filter'] == 'not_added'){ echo 'selected="selected"' ;}?>>Not Added</option>
					   </select>
					   </div>
					  </form>
					  
					  
					  
					   <div class="col-lg-1 text-right"><a href="<?php echo Router::url('/', true)?>SupplierDetails" class="btn btn-warning">Back</a> </div>
					</div> 
				 </div> 
				
				</div>
							  
				<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border " style="position:absolute">
						 
 						<div class="table-responsive">	
								
						<table class="table table-striped">
						  <thead>
					<!--	  System SKU	Supplier SKU	Barcode	Our Title	Supplier Title	Case Barcode	Case Quantity	Unit of sale	Weight	dimension	Moq	 	 	 	  	Edit	Action	Added /Modifed By
-->

							<tr>
 								<th>System SKU / Supplier Sku</th>
								<th>Barcode / Case Barcode</th>
								<th>Our Title / Supplier Title</th>	
 								<th>Case Quantity</th>
								<th>Price / Case Price</th>
 								<th>Minimum Order Quantity </th>	
								<th>End Of Sale Life Days </th>	
							   	<th>Type Of SKU -<small>(SlowMoving, Medium Moving, Fast Moving)</small> </th>	
								<th>Product type</th>
								<th>Unit of sale</th>
								<th>Weight / Dimension</th>
 								<th>In Transit</th>
								<th>Last PO Date</th>
								<th>Lead time </th>
  								<th>Alternative SKU</th>
								<!--th>Stock Min Level</th>
								<th>Life of item </th>-->
								<th align="center">Status </th>
  								<th>Sku Added By</th>
								<th align="center">Action</th>
							</tr>
 						  </thead>
						  <tbody  >
								<?php if(count($supplier_mapping) > 0){ 
								foreach($supplier_mapping as $val){
								?> 								
								<tr class="<?php echo $val['SupplierMapping']['status'];?>">
  									<td>
									SYS: <?php echo $val['SupplierMapping']['master_sku']?> <br />
									SUP: <?php echo $val['SupplierMapping']['supplier_sku']?></td>	
 									<td>
									B: <?php echo $val['SupplierMapping']['barcode']?><br />
									CB: <?php echo $val['SupplierMapping']['case_barcode']?>	</td>
									<td>
									SYS: <?php echo $val['SupplierMapping']['title']?><br />
									SUP: <?php echo $val['SupplierMapping']['sup_title']?>	</td>
									
 									<td><?php echo $val['SupplierMapping']['case_quantity']?></td>
									<td><?php echo $val['SupplierMapping']['price']?><br />
									 <?php echo $val['SupplierMapping']['case_price']?></td>
									 
 									<td><?php echo $val['SupplierMapping']['min_order_qty']?></td>	
									<td><?php echo $val['SupplierMapping']['end_sale_life']?></td>	
									 
 									<td><?php echo $val['SupplierMapping']['type_of_sku']?></td>	
									<td><?php echo $val['SupplierMapping']['product_type']?></td>
									<td><?php echo $val['SupplierMapping']['unit_of_sale']?></td>
 									<td>
									 <?php echo $val['SupplierMapping']['weight']?><br />
									 <?php echo $val['SupplierMapping']['dimension']?>	</td>
								
									<td>In Transit</td>
									<td>Last PO Date</td>
									<td><?php echo $val['SupplierMapping']['lead_time']?></td>
 									<td><?php echo $val['SupplierMapping']['alternative_sku']?></td>  
									<!--<td>Stock Min Level</td>
									<td><?php echo $val['SupplierMapping']['life_of_item']?></td>-->
									<td><div style="margin-top:0px; margin-bottom:2px; text-align:center">
									<?php if($val['SupplierMapping']['status'] == 'not_added'){?>
									<a href="<?php echo Router::url('/', true).'SupplierMappings/MoveToProduct/'.$val['SupplierMapping']['id']?>" class="btn btn-warning btn-xs" role="button">Add to Product</a>
									<?php }else{
										echo '<button type="button" class="btn btn-info btn-xs">Added in Product</button>';
									}?>
									</div>
									<div style="margin-top:0px; margin-bottom:2px; text-align:center">
									<?php if($val['SupplierMapping']['istatus'] == 1){?>
									<a title="Make Active" href="<?php echo Router::url('/', true).'SupplierMappings/ChangeStatus/'.$val['SupplierMapping']['id'].'/0'?>" class="btn btn-danger btn-xs" role="button" >InActive</a>
									<?php }else{ ?>
									<a title="Make InActive" href="<?php echo Router::url('/', true).'SupplierMappings/ChangeStatus/'.$val['SupplierMapping']['id'].'/1'?>" class="btn btn-success btn-xs" role="button" >Active</a>
									<?php }?>
									</div>
									</td>	 
									<td><?php echo $val['SupplierMapping']['username']?></td> 
									<td align="center"><a href="<?php echo Router::url('/', true).'SupplierMappings/AddSupplierSku/'.$this->params['pass'][0].'/'.$val['SupplierMapping']['id']?>" class="btn btn-info btn-xs" role="button"  style="margin-top:0px; margin-bottom:2px;">Edit</a><a href="<?php echo Router::url('/', true).'SupplierMappings/SupplierSkuDelete/'.$this->params['pass'][0].'/'.$val['SupplierMapping']['id']?>" class="btn btn-danger btn-xs" role="button"  style="margin-top:0px; margin-bottom:2px;">Delete</a></td>
									
									
									
								</tr>
							<?php	}	};?>
						  </tbody>
						</table>
						<div class="" style="margin:20px auto; width:350px">
								 <ul class="pagination">
								  <?php
									   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
									   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								  ?>
								 </ul>
							</div
									
						 
					
						><div class="panel-body no-padding-top bg-white">
 							<br />
							<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
						</div>			
				  </div>
			  </div> 
			</div> 
		</div> 
	 	</div>  
</div> 		 
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999; al " src="<?php echo $this->webroot; ?>img/482.gif" />
</div> 
<script>	
$("#supplier_grid" ).load( "<?php echo Router::url('/', true) ?>SupplierDetails/ajaxGetSuppliers",{"supplier_code":$("#sec_supplier_code option:selected").val()}); 

function addData(){
	
	if($('#supplier_name').val() == ''){
		alert('Enter supplier name.');
	}else if($('#supplier_code').val() == ''){
		alert('Enter supplier code.');
	}else{
		
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>SupplierDetails/ajaxAddSupplier',
			type: "POST",
			beforeSend: function() {
 				$(".outerOpac").show(); 
			},
			data : {id:$('#id').val(),supplier_name:$('#supplier_name').val(),supplier_code:$('#supplier_code').val(),credit_period:$('#credit_period').val(),credit_limit:$('#credit_limit').val(),credit_limit_available:$('#credit_limit_available').val(),currency:$('#currency').val(),address:$('#address').val(),category_id:$("#category_id option:selected").val()},
			success: function(data, textStatus, jqXHR)
			{	
				$(".outerOpac").hide(); 	
 				alert(data.msg);	
				if(data.error !='yes'){
					$("#supplier_grid" ).load( "<?php echo Router::url('/', true) ?>Suppliers/ajaxGetSuppliers");	
				}				
				//$('#sup_message').delay(5000).fadeOut('slow');						
			},
			complete: function() {
				$(".outerOpac").hide(); 
			}		
		});
	}
}

function updateData(){
	
	if($('#supplier_name').val() == ''){
		alert('Enter supplier name.');
	}else if($('#supplier_code').val() == ''){
		alert('Enter supplier code.');
	}else{
		
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>SupplierDetails/ajaxUpdateSupplier',
			type: "POST",
			beforeSend: function() {
 				$(".outerOpac").show(); 
			},
			data : {id:$('#id').val(),supplier_name:$('#supplier_name').val(),supplier_code:$('#supplier_code').val(),credit_period:$('#credit_period').val(),credit_limit:$('#credit_limit').val(),credit_limit_available:$('#credit_limit_available').val(),currency:$('#currency').val(),address:$('#address').val(),category_id:$("#category_id option:selected").val()},
			success: function(data, textStatus, jqXHR)
			{	
				//$(".outerOpac").hide(); 	
 				alert(data.msg);	
				if(data.error !='yes'){
					$("#supplier_code").removeAttr("disabled"); 	
					$('#id').val('');
					$('#supplier_name').val(''); 
					$('#supplier_code').val('');				
					$('#credit_period').val('');
					$('#credit_limit').val('');
					$('#credit_limit_available').val('');
					$('#address').val('');
					$('#btn-add').show();
					$('#btn-update').hide(); 
 					$("#supplier_grid" ).load( "<?php echo Router::url('/', true) ?>Suppliers/ajaxGetSuppliers");	
				}				
								
			},
			complete: function() {
				$(".outerOpac").hide(); 
			}		
		});
	}
}

function editSupplier(id){
	 
		$("#loading-mask").show(); 
		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>SupplierDetails/ajaxGetSupplier',
		type: "POST",
		beforeSend: function() {
 				$(".outerOpac").show(); 
		},
		data : {id:id},
		success: function(data, textStatus, jqXHR)
		{	
 				$('#id').val(data.id);
				$('#supplier_name').val(data.supplier_name); 
				$('#supplier_code').val(data.supplier_code);
 				$("#supplier_code").attr("disabled", "disabled"); 
				$('#credit_period').val(data.credit_period);
				$('#credit_limit').val(data.credit_limit);
				$('#credit_limit_available').val(data.credit_limit_available);
				$('#currency').val(data.currency);
				$('#address').val(data.address);
				$('#btn-add').hide();
				$('#btn-update').show(); 
 						
 		},
		complete: function() {
				$(".outerOpac").hide(); 
			}	
	});
   
}

function delSupplier(id){
	if (confirm("Are you sure to delete this Supplier? It will delete all items and record of this supplier.")) {
		$("#loading-mask").show(); 
		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>SupplierDetails/ajaxDelSuppliers',
		type: "POST",
		beforeSend: function() {
 				$(".outerOpac").show(); 
		},
		data : {id:id},
		success: function(data, textStatus, jqXHR)
		{	
 			alert(data.msg);	
			if(data.error !='yes'){
				$("#supplier_grid" ).load( "<?php echo Router::url('/', true) ?>SupplierDetails/ajaxGetSuppliers");	
			}			
 		},
		complete: function() {
				$(".outerOpac").hide(); 
			}	
	});
  } 
}
</script>
