<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Enter SKU Details</h1>
		<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <?php  
                                print $this->form->create( 'SupplierMapping', array( 'action'=>'SaveSupplierSku','class'=>'form-horizontal', 'type'=>'post','id'=>'savesupplier','enctype'=>'multipart/form-data' ));
                            
                                 print $this->form->input( 'SupplierMapping.id', array( 'type'=>'hidden' ) );
								 print $this->form->input( 'SupplierMapping.sup_inc_id', array( 'type'=>'hidden' ,'value'=>$this->params['pass'][0]) );
								 print $this->form->input( 'SupplierMapping.sup_code', array( 'type'=>'hidden' ) );
                            ?>
                                <div class="panel-body padding-bottom-40">
                                      <div class="form-group">
                                        <label for="SupplierDetailSupplierName" class="control-label col-lg-3">System SKU*:</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                 print $this->form->input( 'SupplierMapping.master_sku', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>true  ) );
                                            ?>
                                        </div>
                                      </div>
                                      
                                      <div class="form-group">
                                        <label for="SupplierDetailSupplierCode" class="control-label col-lg-3">Supplier SKU*:</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'SupplierMapping.supplier_sku', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>true  ) );
                                            ?>
                                        </div>
                                      </div>
                                      
                                      <div class="form-group">
                                        <label for="SupplierDetailEmail" class="control-label col-lg-3">System Title:</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'SupplierMapping.title', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>
                                        </div>
                                      </div>
									  <div class="form-group">
                                        <label for="SupplierDetailEmail" class="control-label col-lg-3">Supplier Title:</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'SupplierMapping.sup_title', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>
                                        </div>
                                      </div>
									  
									  
									   <div class="form-group">
                                        <label for="SupplierDetailEmail" class="control-label col-lg-3">Barcode:</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'SupplierMapping.barcode', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>
                                        </div>
                                      </div>
									  <div class="form-group">
                                        <label for="SupplierDetailEmail" class="control-label col-lg-3">Case Barcode:</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'SupplierMapping.case_barcode', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>
                                        </div>
                                      </div>
									  
									  <div class="form-group">
                                        <label for="SupplierDetailEmail" class="control-label col-lg-3">Price*:</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'SupplierMapping.price', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>true ) );
                                             ?>
                                        </div>
                                      </div>
									  <div class="form-group">
                                        <label for="SupplierDetailEmail" class="control-label col-lg-3">Case Price*:</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'SupplierMapping.case_price', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>true ) );
                                             ?>
                                        </div>
                                      </div>
									  
									  <?php /*?> <div class="form-group">
                                        <label for="SupplierDetailEmail" class="control-label col-lg-3">Pack Size*:</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'SupplierMapping.pack_size', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>true ) );
                                             ?>
                                        </div>
                                      </div><?php */?>
									  
                                     <div class="form-group">
										<label for="SupplierDetailPhoneNumber" class="control-label col-lg-3">Case Quantity*:</label>
										<div class="col-lg-7">
											 <?php
												print $this->form->input( 'SupplierMapping.case_quantity', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>true) );
												?>
 										</div>
									</div>
									 
                                     <div class="form-group">
										<label for="SupplierDetailPhoneNumber" class="control-label col-lg-3">Unit of sale*:</label>
										<div class="col-lg-7">
											 <?php
												print $this->form->input( 'SupplierMapping.unit_of_sale', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>true) );
												?>
 										</div>
									</div>
									
									 <div class="form-group">
										<label for="SupplierDetailPhoneNumber" class="control-label col-lg-3">Weight*:</label>
										<div class="col-lg-7">
											 <?php
												print $this->form->input( 'SupplierMapping.weight', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>true) );
												?>
 										</div>
									</div>
									
									 <div class="form-group">
										<label for="SupplierDetailPhoneNumber" class="control-label col-lg-3">Dimension*:</label>
										<div class="col-lg-7">
											 <?php
												print $this->form->input( 'SupplierMapping.dimension', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>true) );
												?>
 										</div>
									</div>
									
									<div class="form-group">
										<label for="SupplierDetailPhoneNumber" class="control-label col-lg-3">Product type*:</label>
										<div class="col-lg-7">
											 <?php
												print $this->form->input( 'SupplierMapping.product_type', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>true) );
												?>
 										</div>
									</div>
                                      
									 <div class="form-group">
									  <div class="col-lg-3"> </div>
									  
									   <div class="col-lg-4">  
									   <?php  
										   $btn = 'Add Supplier SKU';
										   if(isset($this->params['pass']) && count($this->params['pass']) > 1){
											 $btn = 'Update Supplier SKU';
										   }
 											echo $this->Form->button($btn, array(
												'type' => 'submit',
												'escape' => true,
												'class'=>'btn btn-info'
										         ));	
										?></div>
										 <div class="col-lg-3 text-right"><a href="<?php echo Router::url('/', true).'SupplierMappings/index/'.$this->params['pass'][0]?>" class="btn btn-warning" role="button">Back</a>
 </div>
                               		  </div> 
									   </div> 
									  
								 </div> 
										  
                            
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>
<style>
.help-block{margin-bottom:0px!important; font-size:10px;margin-top: 0px;}
</style>
 
<script type="text/javascript">
function getcode()
{
	var nstr = $.trim($('#SupplierDetailSupplierName').val().toUpperCase());
	var str = nstr.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');//replace(/ /g, '_');
	
	var fstr = str.substring(0,3);
	<?php if(count($this->params['pass']) == 0){?>
	$('#SupplierDetailSupplierCode').val(fstr);
	<?php }?>
}
 
</script>
</div> 



