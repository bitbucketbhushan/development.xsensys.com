<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.head div div div{ padding:0px ;  }
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.col-65 { width: 65%; float: left;}
	.col-60 { width: 60%; float: left;}
	.col-55 { width: 55%; float: left;}	 
	.editable-submit, .editable-cancel{padding: 3px 8px;}
	.icon-remove, .icon-ok {position: relative;top: 1px;display: inline-block;font-family: 'Glyphicons Halflings';-webkit-font-smoothing: antialiased;font-style: normal;font-weight: normal; line-height: 1;}
	.icon-ok:before {content: "\e013";}
	.icon-remove:before {    content: "\e014";}
	.paginate-responsive {    text-align: center;	}
	.paginate-responsive .total_pages {    float: left;    width: auto;    display: inline-block;    margin: 0px;    padding: 5px 0 0;	}
	.paginate-responsive .paginate {    float: left;    text-align: center;    display: inline-block;    width: 68%;}
	.paginate-responsive .pagination {    display: inline-block;    padding-left: 0;    margin: 10px 0;    border-radius: 4px;}
	.paginate-responsive .pagination > li {    display: inline;}
	.paginate-responsive .total_rows {    float: right;    width: auto;    display: inline-block;    margin: 0px;    padding: 5px 0 0;}

 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Supplier Sku Mapping</h4>
			</div>
    	  <div class="container-fluid   bg-white">							
			<div class="row">
			  			
				<div class="col-lg-12 ">
					  
						   <div class="row">	
							<form method="post" name="supplier_data" action="<?php echo Router::url('/', true) ?>Suppliers/UploadFile" id="supplier_data" enctype="multipart/form-data">	 
							  <div class="col-sm-12 bg">
								   <div class="col-sm-2"><strong>Add data using csv sheet:</strong></div>
									<div class="col-sm-5">
										<div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												Browse <input type="file" size="60" name="supplierfile" id="supplierfile">	
											</span>
										</span>
										<input type="text" placeholder="No file selected" readonly="" class="form-control">
										</div>
								 </div>
								 <div class="col-sm-2"><a href="<?php echo Router::url('/', true) ?>sample_file/supplier_sku_mapping.csv" style="text-decoration:underline">Download Sample File</a></div>
							 </div> 
							 </form>
							  </div>
			  
			  
			    <div class="row" style="padding-top: 10px;">	 
			 	<div class="col-sm-12">							
					<div class="col-sm-3"><input type="text" class="form-control" placeholder="Enter Master SKU" id="master_sku" name="master_sku" value=""></div>
					<div class="col-sm-3"><input type="text" class="form-control" placeholder="Enter Supplier SKU/Product code" id="supplier_sku" name="supplier_sku" value=""></div>	
					<div class="col-sm-3"> 
					<?php
								 					
								echo '<select class="form-control" name="supplier_code" id="supplier_code"  >';
								  $i=0;
								  foreach($suppliers as $val){ 
									if($i<1){
										echo '<option value="'.$val['SupplierDetail']['supplier_code'].'" selected="selected">'. $val['SupplierDetail']['supplier_name'].'</option>';
									}else{
										echo '<option value="'.$val['SupplierDetail']['supplier_code'].'">'. $val['SupplierDetail']['supplier_name'].'</option>';
									}
									$i++;						
								  } 			
								echo '</select>';
						
						?>
					</div>
					<div class="col-sm-3"><button type="button" class="btn save btn-success" name="save" onClick="addData()";>Save</button></div>
					</div>
				 </div>	
			 
			 
					<div class="row" style="padding-top: 8px;background-color: #ccc;padding-bottom: 8px;margin-top: 5px;border-radius: 10px;">	 
						<div class="col-sm-12">	
							 
							<div class="col-sm-3"> <input type="search" autocomplete="off" class="form-control" placeholder="Search By Master SKU" id="search_master_sku" name="sku"></div>
							<div class="col-sm-3"> <input type="search" autocomplete="off" class="form-control" placeholder="Search By Supplier SKU" id="search_supplier_sku" name="title"></div>	
							
							<div class="col-sm-1"> <button id="Filter" type="submit" title="Filter" class="button btn btn-info" onclick="loadajax()">Search</button></div>	
							<div class="col-sm-1"> <button id="Filter" type="submit" title="Filter" class="button btn btn-info" onclick="ResetSearch()">Reset</button></div>				
						</div>
					</div>
						
 						<div class="row" id="suppliers_data">
						<div class="table-responsive">	
								
						<table class="table table-striped">
						  <thead>
							<tr>
								<th>SKU</th>
								<?php								
  								  foreach($suppliers as $val){ 
									echo  '<th width="9%">'.$val['SupplierDetail']['supplier_name'].'</th>';
								 					
								  } 			
 							?>	
							</tr>
							   
 					  </thead>
					  <tbody id="results">
						<?php /*?><tr>
						<?php 
						foreach($supplier_mapping as $val){
							echo  '<td width="9%">'.$val['SupplierMapping']['master_sku'].'</td>';
							 foreach($suppliers as $s){
							 	if($val['SupplierMapping']['supplier_code'] == $s['SupplierDetail']['supplier_code']){
									//echo  '<td width="9% class="xedit" data-name="supplier_sku" supplier-code="'.$s['SupplierDetail']['supplier_code'].'" pk="'.$val['SupplierMapping']['master_sku'].'" action="update" data-value="'.$s['SupplierDetail']['supplier_sku'].'">'.$val['SupplierMapping']['master_sku'].'</td>';
								}else{
									echo  '<td width="9%">-</td>';
								}
							 }
						} 
						?>
						
						</tr><?php */?>
							  
					  </tbody>
						</table>
						</div>
						</div>	
					<div class="row">
			<div class="paginate-responsive col-sm-12"> </div>	
		</div>
						<div class="panel-body no-padding-top bg-white">
 							<br />
							<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
						</div>			
				  </div>
			    
			 
		</div> 
	 	</div>  
 
</div> 		 
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999; al " src="<?php echo $this->webroot; ?>img/482.gif" />
</div> 
 
<?php /*?><script src="<?php echo Router::url('/', true) ?>js/bootstrap.min.js"></script>
<script src="<?php echo Router::url('/', true) ?>js/bootstrap-editable.js"></script>
 <?php */?>
  <!-- bootstrap -->
   
 <link href="<?php echo Router::url('/', true) ?>css/bootstrap-editable.css" rel="stylesheet"/>   
 
 
<script>
 
$("#supplierfile").change(function() {
		$("#supplier_data").submit();	
});
	
function addData(){
	if($('#master_sku').val() == ''){
		alert('Enter master sku.');
	}else if($('#supplier_sku').val() == ''){
		alert('Enter supplier sku/product code.');
	}else{
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Suppliers/ajaxMapSupplier',
			type: "POST",
			data : {id:$('#id').val(),supplier_sku:$('#supplier_sku').val(),master_sku:$('#master_sku').val(),supplier_code:$('#supplier_code').val()},
			success: function(data, textStatus, jqXHR)
			{		
						
				alert(data.msg);	
				if(data.error !='yes'){
					loadajax();
				}				
			 
			}		
		});
	}
}
function ResetSearch(){ 
	$("#search_master_sku").val('');
	$("#search_supplier_sku").val('');
	loadajax();
}		
function loadajax(val){ 
	var page = $(val).attr("data-page"); 
	var name = $(val).attr("data-name"); 
	var value = $(val).attr("data-value");	
	$(".outerOpac").show();
	$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Suppliers/ajaxGetSupplierMapping',
			type: "POST",
			data : {page:page,name:name,value:value ,master_sku:$("#search_master_sku").val(),supplier_sku:$("#search_supplier_sku").val()},
			success: function(txt, textStatus, jqXHR)
			{	
			    $(".outerOpac").hide();
				$('#message').show();		
				$(".paginate-responsive").html(txt.paginate);														
				$("#results").html(txt.data);								
				
			}		
		});
}
$(document).ready(function()
{
		$.fn.editable.defaults.mode = 'popup';  
		$(document).on('click','.editable-submit',function(){
			var x = $(this).closest('section').children('span').attr('pk');			
			var y = $('.editable-input .input-medium').val();  
			var z = $(this).closest('section').children('span'); 
			var act = $(this).closest('section').children('span').attr('action');
			var fname = $(this).closest('section').children('span').attr('data-name');
			var supplierCode = $(this).closest('section').children('span').attr('supplier-code');		 
			$.ajax({
				dataType: 'json',
				url: "<?php echo Router::url('/', true) ?>Suppliers/ajaxGridEitable",
				type: "POST",
				data : {action:act,value:y,pk:x,name:fname,supplier_code:supplierCode},
				success: function(s){ 
					if(s.status == 'ok'){
					$(z).html(y);}
					if(s.status == 'error') {
						alert(s.msg);
					}
				},
				error: function(e){
					alert('Error Processing your Request!!');
				}
			});
		});	
		loadajax();
		
		
		$(".paginate-responsive").on( "click", ".pagination a", function (e){
			e.preventDefault();			
			loadajax(this);			
		});
 	 
});
</script>

