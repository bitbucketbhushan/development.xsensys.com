<table class="" id="example1" aria-describedby="example1_info">
			<thead>
				<tr role="row">
					<th style="width: 2%;" >Id</th>
					<th style="width: 20%;" >Product Name</th>
					<th style="width: 2%;" >Product Sku</th>
					<th style="width: 8%;">Barcode</th>
					<th style="width: 9%;">Brand</th>
					<th style="width: 5%;">Packaging Class</th>
					<th style="width: 5%;">Category</th>
					<th style="width: 15%;">Avb. stock</th>
					<th style="width: 11%;">Open Ord.</th>
					<th style="width: 11%;">Intransit</th>
					<th style="width: 8%;">Status</th>
					<th style="width: 8%;">Comment</th>
					<th style="width: 8%;">FreZile Product</th>
					<th style="width: 15%;">Action</th>
				</tr>
			</thead>
			<tbody  >
				<?php 
					App::Import('Controller', 'Products');
					$obj = new ProductsController; 
					
					foreach($productdeatil as $productdeatilIndex => $productdeatilValue ) {  
						$alertClass = ( $productdeatilValue['Product']['is_deleted'] == 1 ) ? "alert-danger" : "";
						$status 	= ( $productdeatilValue['Product']['product_status'] == 0 ) ? "active" : "deactive";
						
 						$skumaps = $obj->getSkuMap($productdeatilValue['Product']['product_sku']);
						$active_channels = [];
						foreach($skumaps as $skm){
							if($skm['Skumapping']['listing_status'] == 'active'){
								$active_channels[] = $skm['Skumapping']['channel_name'];
							}
						}
				?>
				<tr class="odd <?php echo $alertClass; ?>">
								

					<td class="  sorting_1"><div id="pmyModal_<?php echo $productdeatilValue['Product']['id']; ?>" class="modal fade" role="dialog">
					  <div class="modal-dialog modal-lg">
					
						<!-- Modal content-->
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title"><?php echo $productdeatilValue['Product']['product_name'].'(SKU='. $productdeatilValue['Product']['product_sku'].')'; ?></h4>
						  </div>
						  <div class="modal-body" style="min-height:460px; padding:10px;">
						  <?php foreach($channels as $ch){?>
							<div class="col-sm-4">
								<div class="col-sm-2">
								 
										 <input type="checkbox" name="<?php echo $ch['ProChannel']['channel_title'];?>" id="<?php echo $ch['ProChannel']['channel_title'];?>_<?php echo $productdeatilValue['Product']['id']; ?>" <?php if(in_array($ch['ProChannel']['channel_title'],$active_channels)){echo'checked="checked"';}?>  onchange="saveChannel(this,'<?php echo $productdeatilValue['Product']['product_sku'];?>','<?php echo $ch['ProChannel']['channel_title'];?>');" />
								</div>
								<label for="<?php echo $ch['ProChannel']['channel_title'];?>_<?php echo $productdeatilValue['Product']['id']; ?>" class="col-sm-10 col-form-label"><?php echo $ch['ProChannel']['channel_title'];?></label>
							</div>
						  <?php }?> 
						  <br /><br /><br />
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						  </div>
						</div>
					
					  </div>
					</div><?php echo $productdeatilValue['Product']['id']; ?></td>
					<td class="  sorting_1"><?php echo $productdeatilValue['Product']['product_name']; ?><br />
					<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#pmyModal_<?php echo $productdeatilValue['Product']['id']; ?>"><i class="glyphicon glyphicon-plus"></i>&nbsp;SUBSOURCES</button></td>                                                
					<td class="  sorting_1"><?php echo $productdeatilValue['Product']['product_sku']; ?><br /><a href="<?php echo Router::url('/', true); ?>Reports/getReport/<?php echo $productdeatilValue['Product']['product_sku']; ?>" target="_blank" class="btn btn-warning btn-xs">View</a></td>
					<td class="  sorting_1">L:<?php echo $this->Common->getLatestBarcode($productdeatilValue['ProductDesc']['barcode']).'<br/>G:'.$productdeatilValue['ProductDesc']['barcode']; ?><br /><a href="<?php echo Router::url('/', true); ?>Reports/getReport/<?php echo $this->Common->getLatestBarcode($productdeatilValue['ProductDesc']['barcode']); ?>" target="_blank" class="btn btn-warning btn-xs">View</a></td>
					<td class="  sorting_1"><?php echo $productdeatilValue['ProductDesc']['brand']; ?></td>
					<td class="  sorting_1"><?php echo $productdeatilValue['ProductDesc']['variant_envelope_name']; ?></td>
					<td class="  sorting_1"><?php echo $productdeatilValue['Product']['category_name']; ?></td>
				<?php /*?>	<td class="  sorting_1"><?php $currentStock = $productdeatilValue['Product']['current_stock_level']; print $currentStock; ?></td><?php */?>
					<td class="  sorting_1">Cur Stock:<?php $currentStock = $productdeatilValue['Product']['current_stock_level']; print $currentStock; ?>
					<?php 
						$BinLocation	=	$obj->getBinLocation( $productdeatilValue['ProductDesc']['barcode'] );
						if(isset($BinLocation[$productdeatilValue['ProductDesc']['barcode']])){
							$blArray = $BinLocation[$productdeatilValue['ProductDesc']['barcode']];
							foreach($blArray  as $val){
								echo '<div style="font-size:10px; border-top:dotted #999999 1px;">'.$val['bin_location'] .'<br>Qty:'.$val['stock_by_location'].'</div>';
							}
						}
					/*if(isset($BinLocation[$productdeatilValue['ProductDesc']['barcode']])){
						$blArray = $BinLocation[$productdeatilValue['ProductDesc']['barcode']];
						foreach($blArray  as $val){
							echo '<div style="font-size:10px; border-top:dotted #999999 1px;">'.$val['bin_location'] .'<br>Qty:'.$val['stock_by_location'].'</div>';
						}
					}*/
					?>
					</td>
					<td class="  sorting_1">
						<div style="font-size:11px;">Open:<strong><?php echo $q = $obj->getOpenOrderSkuQty($productdeatilValue['Product']['product_sku'] ); ?></strong></div>
						
						<?php 
							if(isset($unp_data[$productdeatilValue['Product']['product_sku']])){
								$unpArray = $unp_data[$productdeatilValue['Product']['product_sku']];
								$qq = 0;											
								foreach($unpArray  as $val){
									$qq += $val['quantity'];
								}
								if($qq >0){
									$t =(int)$q + (int)$qq ;
									echo '<div style="font-size:11px; border-top:dotted #999999 1px;">UnP:<strong>'.$qq .'</strong></div>';
									echo '<div style="font-size:12px; border-top:dotted #999999 1px;">Total:<strong>'. $t.'</strong></div>';
								}
							}													
						?>
					</td>
					<td class="  sorting_1 intransitsku intransitsku_<?php echo $productdeatilValue['Product']['product_sku']; ?>" for="<?php echo $productdeatilValue['Product']['product_sku']; ?>" >
						<?php echo $obj->getIntransitSku( $productdeatilValue['Product']['product_sku'] ); ?>
					</td>
					<td class="  sorting_1"><?php echo $status; ?> </td>
					<td class="  sorting_1 comment_status_<?php echo $productdeatilValue['Product']['id']; ?>" ><?php echo ($productdeatilValue['Product']['comment'] != '') ? $productdeatilValue['Product']['comment'] : '---'; ?></td>
					<td class="  sorting_1">
						<?php 
												if($productdeatilValue['ProductDesc']['frezile_mark']==1){
													echo '<p style="color: white;background: red;text-align: center;">Yes</p>';} else {echo '<p style="color: white;background: red;text-align: center;">No</p>';} ?></td>
					<td>
						<ul id="icons" class="iconButtons">
							<a href="/Product/edit/<?php echo $productdeatilValue['Product']['id']; ?>" class="btn btn-success btn-xs margin-right-10"><i class="fa fa-pencil"></i></a>
							<?php if($productdeatilValue['Product']['product_status'] == 0) { ?>	
							<a href="Product/lock/UnlockPR/<?php echo $productdeatilValue['Product']['id']; ?>/<?php echo $status; ?>" alt="Unlock" class="btn btn-warning btn-xs margin-right-10"><i class="fa fa-unlock"></i></a>
							<?php } else { ?>
							<a href="Product/lock/UnlockPR/<?php echo $productdeatilValue['Product']['id']; ?>/<?php echo $status; ?>" alt="Lock" class="btn btn-warning btn-xs margin-right-10"><i class="fa fa-lock"></i></a>
							<?php } if($productdeatilValue['Product']['is_deleted'] == 0) { ?>
							<a href="Product/delete/PR/<?php echo $productdeatilValue['Product']['id']; ?>/<?php echo $productdeatilValue['Product']['is_deleted']; ?>" alt="Retrieve" class="btn btn-danger btn-xs"><i class="ion-plus-circled"></i></a>
							<?php } else { ?>
							<a href="Product/delete/PR/<?php echo $productdeatilValue['Product']['id']; ?>/<?php echo $productdeatilValue['Product']['is_deleted']; ?>" alt="Delete" class="btn btn-danger btn-xs"><i class="ion-minus-circled"></i></a>
							<?php }  ?>
							<a href="javascript:void(0);" for="<?php echo $productdeatilValue['Product']['id']; ?>" class="create_barcode_popup_search bg-blue-400 btn-xs margin-right-10">Barcode</a>
							<a href="javascript:void(0);" for="<?php echo $productdeatilValue['Product']['id']; ?>" class="create_qrcode_popup_search btn btn-success btn-xs margin-right-10">QR</a>
							<a href="javascript:void(0);" for="<?php echo $productdeatilValue['Product']['id']; ?>" class="create_comment_popup btn btn-success btn-xs margin-right-10">Comment</a>
							<!--Apply code for Global Barcode-->
							<a href="/Products/addBarcode/<?php echo str_replace('#','_h_',$productdeatilValue['Product']['product_sku']); ?>" for="<?php echo $productAllDesc['Product']['id']; ?>" class="btn btn-success btn-xs margin-right-10">Add Barcode</a>
						</ul>
					</td>	
				</tr>
				<?php } ?>
			</tbody>
</table>
<script type="text/javascript">
 
	//maniac.loaddatatables('table');

	$('body').on( 'click', '.create_barcode_popup_search', function(){
		
		var productId	=	$( this ).attr('for');
		$.ajax({
					url     	: '/Virtuals/getBarcodePopup',
					type    	: 'POST',
					data    	: {  productId : productId },  			  
					success 	:	function( popUpdata  )
					{
						$('.showPopupForAssignSlip').html( popUpdata );
					}                
				});
		});
		
	$('body').on( 'click', '.create_qrcode_popup_search', function(){
		
		var productId	=	$(this).attr('for');
		$.ajax({
					url     	: '/Virtuals/getqrcodePopup',
					type    	: 'POST',
					data    	: {  productId : productId },  			  
					success 	:	function( popUpdata  )
					{
						
						$('.showPopupForAssignSlip').html( popUpdata );
						
					},
					events: {
								show: function() {
									$('#QrcodeGenerate', this).focus();
								}
							}                
				});
				
		});
	
</script>
