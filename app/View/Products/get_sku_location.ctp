<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Sku By Location</h1>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12 col-lg-offset-0">
                             <div class="form-horizontal panel-body padding-bottom-40 padding-top-40">
								   <div class="form-group">
										<label class="col-sm-3 control-label">Location</label>
										<div class="col-sm-8">                                               
											<?php
												print $this->form->input( 'Product.location', array( 'type'=>'text','class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
											?>
										</div>
									</div>
                                 <div class="form-horizontal panel-body" >
									<table class="sku_date col-sm-12" >
									</table>
							    </div>  
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>
<script>
	
$(document).keypress(function(e)
	{
		if(e.which == 13) {
			
			getLocationValue();
		}
	});


	function getLocationValue()
	{
		var location  = $('#ProductLocation').val();
		var str ='';
		$.ajax({
						url     	: '/Products/getproductSku',
						type    	: 'POST',
						data    	: { location : location },  			  
						success 	:	function( msgArray  )
						{
							var json 		= 	JSON.parse(msgArray);
							var getStatus 	= 	json.status;
							if( getStatus == 1 )
							{
								var getData 	= 	json.data;
								str	+=	'<tr style="font-weight: bold; border-bottom: 2px solid #ccc;"><td style="width: 250px;">Product Detail</td><td colspan="6"></td></tr>';
								for (var i = 0, max = getData.length; i < max; i++)
								{
									
									var locationLan	=	getData[i].alllocation.loc
									str +='<tr style="border-bottom: 2px solid #ccc;" ><td>';
									str += '<div><div><strong>'+getData[i].Product.product_sku+'</strong></div>';
									//str +='<div> G:'+getData[i].ProductDesc.Barcode+'</div>';
									str +='<div>'+getData[i].ProductDesc.Barcodelo+'</div>';
									str +='<div>'+getData[i].Product.product_name+'</div>';
									str +='</div><td>';
									str += '<td class="col-sm-0" style=""><strong style="color:#e32d18; text-align: left;">Open Order '+getData[i].allorders.opun+'</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong style="color:#e32d18; float: right;" class="current_'+getData[i].ProductDesc.Barcode+'">Cur Stock: '+getData[i].Product.current_stock_level+'</strong>';
									
									str += '<table style="width: 100%;">';
									str	+= '<tr style="font-weight: bold; border-bottom: 1px solid #ddd;"><td></td><td>Picked Quantity</td><td>Unpicked Quantity</td><td>Pending Order Quantity </td><td>On Shelf Quantity</td><td>Action</td></tr>';
									for (var j=0; j < locationLan.length; j++)
									{
										var identifire = locationLan[j].BinLocation.bin_location+getData[i].ProductDesc.Barcode;
										
										str += '<tr style="border-bottom: 1px dashed;" id="updated_'+getData[i].ProductDesc.Barcode+'">';
										str += '<td>'+locationLan[j].BinLocation.bin_location+ ' <strong class="qty_'+identifire+'">Qty: ' +locationLan[j].BinLocation.stock_by_location+'</td>';
										str += '<td class="col-sm-0">'+locationLan[j].BinLocation.pickorder+'</td>';
										str += '<td class="unpick_'+identifire+'">'+locationLan[j].BinLocation.unpickorder+'</td>';
										str += '<td>'+locationLan[j].BinLocation.unpaidorder+'</td>';
										str += '<td class="col-sm-0 onself_'+identifire+'">'+locationLan[j].BinLocation.onself+'</td>';
										if( locationLan[j].BinLocation.bin_location == location)
										{
										str += '<td class="col-sm-0"><input type="text" id="stock_qty_'+getData[i].ProductDesc.Barcode+'" style="width: 30px; margin-right:10px;" value=""/><button class="update_stock btn bg-green-500 color-white btn-dark" id="'+getData[i].Product.product_sku+'" for="'+getData[i].ProductDesc.Barcode+'" data="Add" title="Add" group="'+identifire+'" style="margin-right:10px;"><span class="glyphicon glyphicon-plus-sign"></span></button><button class="update_stock btn bg-red-500 color-white btn-dark" id="'+getData[i].Product.product_sku+'" for="'+getData[i].ProductDesc.Barcode+'" data="Subtraction" title="Substraction" group="'+identifire+'"><span class="glyphicon glyphicon-minus-sign"></span></button></td>';
										}
										str += '</tr>';
									}
									str += '</table>';
									str += '</td>';
									str += '</tr>';
								}
								
									$('.sku_date').html('');
									$('.sku_date').append( str );
							}
							else
							{
								$('.sku_date').html('');
								alert('There is no location');
							}
						}                
				});	
	}
	
	
	$('body').on('click','.update_stock',function(){
		var barcode		=	$( this ).attr( 'for' );
		var sku			=	$( this ).attr( 'id' );
		var type		=	$( this ).attr('data');
		var identifire	=	$( this ).attr('group');
		var qty			=	$('#stock_qty_'+barcode).val();
		var unpick		=	$('.unpick_'+identifire).text();
		var current		=	$('.qty_'+identifire).text();
		
	
		if( qty == '' )
		{
			alert( 'Please enter Qunatity' );
			return false;
		}
		
		var location	=	$('#ProductLocation').val();
		$.ajax({
						url     	: '/Products/updateinventerybuylocationsearch',
						type    	: 'POST',
						data    	: { barcode : barcode, sku : sku, qty : qty, location : location, type : type },  			  
						success 	:	function( msgArray  )
						{
							var json 		= 	JSON.parse(msgArray);
							var getStatus 	= 	json.status;
							
							if( getStatus == 1 )
							{
								
								swal( 'Quantity updated successfully.' );
								//alert(json.locationupdate);
								//alert(unpick);
								$('.current_'+barcode).text( 'Cur Stock: '+json.stockUpdate );
								$('.onself_'+identifire).text( parseInt(json.locationupdate) + parseInt(unpick) );
								$('.qty_'+identifire).text( 'Qty:'+json.locationupdate );
								if(type == 'Add')
								{
									$('.onself_'+barcode).css("background-color", "green");
									$('.onself_'+barcode).css("color", "white");
									$('.current_'+barcode).css("background-color", "green");
									$('.current_'+barcode).css("color", "white");
									$('#updated_'+barcode).css("background-color", "green");
									$('#updated_'+barcode).css("color", "white");
									
								} else {
									$('.onself_'+barcode).css("background-color", "red");
									$('.onself_'+barcode).css("color", "white");
									$('.current_'+barcode).css("background-color", "red");
									$('.current_'+barcode).css("color", "white");
									$('#updated_'+barcode).css("background-color", "red");
									$('#updated_'+barcode).css("color", "white");
								}
								$('#stock_qty_'+barcode).val('');
								$('#stock_qty_'+barcode).css("color", "black");
								
							}
							else
							{
								alert( 'There is an error. Please check' );
							}
						}                
				});	
	
	});

</script>
