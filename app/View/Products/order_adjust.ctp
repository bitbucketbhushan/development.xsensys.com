<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Order Adjustment</h1>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="form-horizontal panel-body padding-bottom-40 padding-top-40">

                                <div class="form-group">
                                             <label class="col-sm-3 control-label">Order Id</label>
                                             <div class="col-sm-7">                                               
                                                <?php
                                                    print $this->form->input( 'Product.orderid', array( 'type'=>'text','class'=>'form-control orderid','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
                                                ?>  
                                             </div>
                                     </div>
                                       
                                      <!-- bin assosiation -->
                                      
                                      <div class='image1' ></div>
                                      
                                    <div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100">                                                                            
                                    
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>

<script>
	
	$(document).keypress(function(e)
	{
	   if(e.which == 13) {
		   
		   if($('.binLocation').val() != '')
		   {
			  
		   }
		   else
		   {
			   
		   }
		  
		   if($('.orderid').val() != '')
			{
				submitSanningValue();
			}
			else
			{
				//submitTrackingSanningValue();
			}
			
		}
	});

	
	function submitSanningValue()
	{
		
		var orderid	=	$('.orderid').val();			
		
		$.ajax({
			'url'            : '/Products/removeOrderById',
			'type'           : 'POST',			
			'data'           : {  orderid : orderid },
			'success' 		 : function( msgArray )
						{
                                                    if( msgArray == 1 )
                                                    {
                                                        swal( "Order deleted" );
                                                        return false;   
                                                    }       
                                                }
									
			});
	}	
</script>
