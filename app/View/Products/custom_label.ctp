<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Custom Label</h1>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="form-horizontal panel-body padding-bottom-40 padding-top-40">

									<div class="form-group">
										 <label class="col-sm-3 control-label">Order Id</label>
										 <div class="col-sm-7">                                               
											<?php
												print $this->form->input( 'Product.orderid', array( 'type'=>'text','class'=>'form-control orderid','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
											?>  
										 </div>
								     </div>
								     <div class="form-group">
										 <label class="col-sm-3 control-label"></label>
										 <div class="col-sm-7">
											 <a href="javascript:void(0);" class="printLabelSlip btn bg-green-500 color-white btn-dark padding-left-20 padding-right-20" onclick = "submitSanningValue();">Print Label / Slip</a>
											 <a href="javascript:void(0);" class="varifyLabel btn bg-green-500 color-white btn-dark padding-left-20 padding-right-20">Varify Label </a>
											 <a href="javascript:void(0);" class="varifySlip btn bg-green-500 color-white btn-dark padding-left-20 padding-right-20">Varify Slip</a>
										 </div>
								     </div>
                                      <!-- bin assosiation -->
                                     <div class='image1' ></div>
									 <div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100">                                                                            
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

<script>
	
	$(document).keypress(function(e)
	{
	   if(e.which == 13) {
		   
		   if($('.binLocation').val() != '')
		   {
			  
		   }
		   else
		   {
			   
		   }
		  
		   if($('.orderid').val() != '')
			{
				submitSanningValue();
			}
			else
			{
				//submitTrackingSanningValue();
			}
			
		}
	});

	
	// function submitSanningValue()
	// {
	// 	var orderid	=	$('.orderid').val();			
		
	// 	$.ajax({
	// 		'url'            : '/Customprints/customLabelPrint',
	// 		'type'           : 'POST',			
	// 		'data'           : {  orderid : orderid }
	// 		,
	// 		'beforeSend'	 : function() {
	// 							$( ".outerOpac" ).attr( 'style' , 'display:block');
	// 		},
	// 		'success' 		 : function( msgArray )
	// 							{
	// 								$( ".outerOpac" ).attr( 'style' , 'display:none');
	// 								$('.orderid').val('');
	// 								swal( "Label Generated" );									
	// 							}
									
	// 		});
			

	// 	$.ajax({
	// 		'url'            : '/Customprints/customSlipPrint',
	// 		'type'           : 'POST',			
	// 		'data'           : {  orderid : orderid }
	// 		,
	// 		'beforeSend'	 : function() {
	// 							$( ".outerOpac" ).attr( 'style' , 'display:block');
	// 		},
	// 		'success' 		 : function( msgArray )
	// 							{
	// 								$( ".outerOpac" ).attr( 'style' , 'display:none');
	// 								$('.orderid').val('');
	// 								swal( "Slip Generated" );									
	// 							}
									
	// 		});
	// }
	


	$( 'body' ).on( 'click', '.varifyLabel', function(){
		
		var orderid	=	$('.orderid').val();
		
		$.ajax({
			'url'            : '/Customprints/varifyLabelPrint',
			'type'           : 'POST',			
			'data'           : {  orderid : orderid },
			'success' 		 : function( msgArray )
								{
									window.open( msgArray,'_blank');
									//alert(msgArray);
								}
									
			});
		
		});

	
	$( 'body' ).on( 'click', '.varifySlip', function(){
		
		var orderid	=	$('.orderid').val();
		
		$.ajax({
			'url'            : '/Customprints/varifyLabelSlip',
			'type'           : 'POST',			
			'data'           : {  orderid : orderid },
			'success' 		 : function( msgArray )
								{
									window.open( msgArray,'_blank');
								}
									
			});
		
		});
		
</script>
