<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Custom Order Process</h1>
        <br />
        <h6>Order will process without adding into sorting and manifest!</h6>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="form-horizontal panel-body padding-bottom-40 padding-top-40">
							<?php							
								$staffOptions = Configure::read( "coreOption" );
							?>
							
							<!-- Staff option and customer coption for inventory manage -->
								
								<div class="form-group">
                                    <label for="username" class="control-label col-lg-3">Choose Process</label>                                        
                                    <div class="col-lg-7">                                            
                                        <?php
												if( count( $staffOptions ) > 0 )
												{
													$option = $staffOptions;
													print $this->form->input( 'CustomProcess.processName', array( 'type'=>'select','empty'=>'Choose Process Now', 'options' => $option, 'default'=>'Customer' , 'div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
												}
                                            ?>  
                                    </div>
                                  </div>
								
                                <div class="form-group">
                                             <label class="col-sm-3 control-label">Order Id</label>
                                             <div class="col-sm-7">                                               
                                                <?php
                                                    print $this->form->input( 'Product.custom_process_order', array( 'type'=>'text','class'=>'form-control custom_process_order','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
                                                ?>  
                                             </div>
                                     </div>
                                       
                                      <!-- bin assosiation -->
                                      
                                      <div class='image1' ></div>
                                      
                                    <div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100">                                                                            
                                    
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>

<div class="outerOpac" style="display:none">
	<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
	</span>
	<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

<!-- show cancel order popup -->    
<div class="showPopupForcancelOrder"></div> 

<script>
	
	$('.custom_process_order').focus();
	
	$(document).keypress(function(e)
	{
	   if(e.which == 13) {
		   
		   if($('.binLocation').val() != '')
		   {
			  
		   }
		   else
		   {
			   
		   }
		  
		   if($('.custom_process_order').val() != '')
			{
				submitSanningValue();
			}
			else
			{
				//submitTrackingSanningValue();
			}
			
		}
	});
	
	function submitSanningValue()
	{
		
		var orderid    = $('.custom_process_order').val();          
		$.ajax(
		{
			'url'            : '/Verify/Order/=OTPQ',
			'type'           : 'POST',
			'data'           : { orderid : orderid },                                    
			'beforeSend'     : function()
			{
							//$('.loading-image').show();
			},
			'success'        : function( msgArray ){ $( 'div.showPopupForcancelOrder' ).html( msgArray ); }
		});
	}
	
	$(document).ready(function()
	{
		
		$( 'body' ).on( 'click', '.customOrderProcess', function()
		{
			
			var orderid = $(this).attr('for');
			$.ajax(
			{
				'url'            : '/Verify/Custom/Order/Process/=91TqOZ',
				'type'           : 'POST',
				'data'           : { orderid : orderid },                                    
				'beforeSend'     : function()
				{
								$( ".outerOpac" ).attr( 'style' , 'display:block');
				},
				'success'        : function( msgArray )
									{
										$( ".outerOpac" ).attr( 'style' , 'display:none');
										if( msgArray == "1" )
										{
											$('.custom_process_order').val('');
											swal( "Complete order has been marked processed." );
											location.reload();
										}
										else if( msgArray == "2" )
										{
											$('.custom_process_order').val('');
											swal( "Sub-order has been marked processed." );
											location.reload();
										}
									}
			});
			
			
		});
		
	});
	
</script>
