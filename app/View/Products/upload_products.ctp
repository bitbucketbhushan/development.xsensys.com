<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Upload Products</h1>		
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>	
      </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-body check_image_extension padding-bottom-40 padding-top-40">
                                	   <?php							
										print $this->form->create( 'Product', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/Uploads/UploadsCsvFilesForInventery', 'type'=>'post','id'=>'brand','onsubmit'=>'return checkfrm();' ) );
										?>
										
										<div class="form-group">
 											 
											<label for="template" class="control-label col-lg-2">Select Template</label>
											<div class="col-lg-2 text-right">
											<select name="template" id="template"  class="form-control">
												<option value="">Select Template</option>
												<option value="CoreUploadFields">Core Upload Fields</option>
												<option value="DetailedProductInformation">Detailed Product Information</option>
												<option value="ChannelSKUMapping">Channel SKU Mapping</option>
												<option value="ChannelSKUManagement">Channel SKU Management</option>
												<option value="BundleSku">Bundle SKU</option>
												<option value="DeleteSku">Delete SKU</option>
												<option value="DeleteChannelSKUMapping">Delete Channel SKU Mapping</option>
												
											</select>
											
											</div> 
 											<div class="col-lg-5">
												<div class="input-group">
													<span class="input-group-btn">
														<span class="btn btn-primary btn-file">
															Browse Template <?php
																		print $this->form->input( 'Product.Import_file', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'', 'required'=>false) );
																	  ?>
														</span>
													</span>
															<input type="text" placeholder="No file selected" readonly="" class="form-control">
															<span class="input-group-addon no-padding-top no-padding-bottom">
													<div class="radio radio-theme min-height-auto no-margin no-padding">
														<?php
														echo $this->Form->button('Upload', array(
														'type' => 'submit',
														'escape' => true,
														'id' => 'btn-upload',
														'class'=>'btn bg-green-500 color-white btn-dark padding-left-10 padding-right-10'
														 ));	
													?>
													</div>
													</span>
											</div>
											
                                        </div>
										
											<div class="col-lg-2 text-right">
												<button type="button" class="btn btn-info" id="download-sample">Download Sample</button>
											</div>
                                       </div>
								      </form>
								      
 								    
								    <div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100">&nbsp;</div>
									
									  <?php							
										print $this->form->create( 'Product', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/Uploads/GetAsinStatus', 'type'=>'post' ) );
										?>
										<?php /*?><div class="form-group">
											<label for="ProductImportFile" class="control-label col-lg-2">Get ASIN Status</label>                                        
											<div class="col-lg-6">
												<div class="input-group">
													<span class="input-group-btn">
														<span class="btn btn-primary btn-file">
															Browse…  <?php
																		print $this->form->input( 'Product.Import_file', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'', 'required'=>false) );
																	  ?>
														</span>
													</span>
															<input type="text" placeholder="No file selected" readonly="" class="form-control">
															<span class="input-group-addon no-padding-top no-padding-bottom">
													<div class="radio radio-theme min-height-auto no-margin no-padding">
														<?php
														echo $this->Form->button('Upload', array(
														'type' => 'submit',
														'escape' => true,
														'class'=>'btn bg-yellow-800 color-white btn-dark padding-left-40 padding-right-40'
														 ));	
													?>
													</div>
													</span>
											</div>
											
                                        </div>
										
											<div class="col-lg-2 text-right"><a href="<?php echo Router::url('/', true) ?>sample_files/get_asin_status.csv" class="btn btn-primary" role="button">Download Sample</a></div>
                                       </div><?php */?>
								      </form>
									  
                                </div>
                                
						</div>
					</div>        
				</div>
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999; al " src="<?php echo $this->webroot; ?>img/482.gif" />
</div> 
<script>
function checkfrm(){
 
	if($("#template option:selected").val() == '' ){
		alert("Please select template!");
		$( "#template" ).focus();
		document.getElementById("template").focus();
		return false;
	}
	
}

$("#download-sample").click(function() {

	if($("#template option:selected").val() == '' ){
		alert("Please select template!");
		$( "#template" ).focus();
		document.getElementById("template").focus();
		return false;
	}else{
		//alert($("#template option:selected").val());
		if($("#template option:selected").val() == 'CoreUploadFields' || $("#template option:selected").val() == 'BundleSku' ){
 			window.location.href = '<?php echo Router::url('/', true); ?>sample_files/'+$("#template option:selected").val()+'.xlsx'; 
		}else{
			window.location.href = '<?php echo Router::url('/', true); ?>sample_files/'+$("#template option:selected").val()+'.csv'; 
		}
	}
   
 });
</script>