<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php print $role;?></h1>
        <div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
							<li>
								<a class="downloadStockFile_prevent btn btn-success btn-xs margin-right-10 color-white" href="/img/stockUpdate/stockUpdate.xls" ><i class="fa fa-download"></i> Download CSV</a>
							</li>
							<li>
								<a class="downloadBinLocation btn btn-success btn-xs margin-right-10 color-white" href="javascript:void(0);" ><i class="fa fa-download"></i> Bin Location</a>
							</li>
							<li>
								<a class="downloadSkuReff btn btn-success btn-xs margin-right-10 color-white" href="javascript:void(0);" ><i class="fa fa-download"></i> Sku Detail</a>
							</li>
							<li>
								<a class="btn btn-warning" href="<?php echo Router::url('/', true).'Products/getCsvData'?>"><i class="fa fa-download"></i> All Data</a>
							</li>
						</ul>
					</div>
				</div>
		
			<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		
    </div>
    <?php
		//pr($productAllDescs); exit;
    ?>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">
									<?php
										print $this->form->input( 'Product.searchtext', array( 'placeholder'=>'Search Product by Product Name or SKU or Barcode or Location','type'=>'text','class'=>'form-control searchtext selectpicker bg-white','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
									?> 										
									<div class="panel-tools">
									
									<!--<a class="downloadStockFile btn btn-success btn-xs margin-right-10 color-white"><i class="fa fa-download"></i> Download CSV</a>-->
									</div>
								</div>
                                <div class="panel-body no-padding-top bg-white">											
											<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
													<thead>
											<tr role="row">
												<th style="width: 2%;" >Id</th>
												<th style="width: 20%;" >Product Name</th>
												<th style="width: 2%;" >Product Sku</th>
												<th style="width: 8%;">Barcode</th>
												<th style="width: 9%;">Brand</th>
												<th style="width: 5%;">Packaging Class</th>
												<th style="width: 5%;">Category</th>
												<th style="width: 15%;">Avb. stock</th>
												<th style="width: 11%;">Open Ord.</th>
												<th style="width: 11%;">Intransit</th>
												<th style="width: 8%;">Status</th>
												<th style="width: 8%;">Comment</th>
												<th style="width: 8%;">FreZile Product</th>
												<th style="width: 15%;">Action</th>
											</tr>
										</thead>
									<tbody role="alert" aria-live="polite" aria-relevant="all">
												<?php 
												echo "                      ";
												foreach($productAllDescs as $productAllDesc) { 
													// echo "<pre>";
												 // print_r($productAllDesc);
												 // echo "<pre>";
												$alertClass = ( $productAllDesc['Product']['is_deleted'] == 1 ) ? "alert-danger" : "";
												$status 	= ( $productAllDesc['Product']['product_status'] == 0 ) ? "active" : "deactive";
												
												App::import('Controller', 'Products');
												$ProductObj = new ProductsController();
												
												?>
                                            <tr class="odd <?php echo $alertClass; ?>">
												<td class="  sorting_1"><?php echo $productAllDesc['Product']['id']; ?></td>
												<td class="  sorting_1"><?php echo $productAllDesc['Product']['product_name']; ?></td>                                                
												<td class="  sorting_1"><?php echo $productAllDesc['Product']['product_sku']; ?><br />
												<a href="<?php echo Router::url('/', true); ?>Reports/getReport/<?php echo $productAllDesc['Product']['product_sku']; ?>" target="_blank" class="btn btn-warning btn-xs">View</a></td>
												<td class="  sorting_1">L:
												<?php echo $this->Common->getLatestBarcode($productAllDesc['ProductDesc']['barcode']).'<br /> G:'.$productAllDesc['ProductDesc']['barcode']; ?><br />
												<a href="<?php echo Router::url('/', true); ?>Reports/getReport/<?php echo $productAllDesc['ProductDesc']['barcode']; ?>" target="_blank" class="btn btn-warning btn-xs">View</a></td>
												<td class="  sorting_1"><?php echo $productAllDesc['ProductDesc']['brand']; ?></td>
												<td class="  sorting_1"><?php echo $productAllDesc['ProductDesc']['variant_envelope_name']; ?></td>
												
												<td class="  sorting_1"><?php echo $productAllDesc['Product']['category_name']; ?></td>
												
												<td class="  sorting_1">Cur Stock:<?php $currentStock = $productAllDesc['Product']['current_stock_level']; print $currentStock; ?>
												
												<?php 
												
												$BinLocation	=	$ProductObj->getBinLocation( $productAllDesc['ProductDesc']['barcode'] );
												if(isset($BinLocation[$productAllDesc['ProductDesc']['barcode']])){
													$blArray = $BinLocation[$productAllDesc['ProductDesc']['barcode']];
													//print_r($blArray );
													foreach($blArray  as $val){
													echo '<div style="font-size:10px; border-top:dotted #999999 1px;">'.$val['bin_location'] .'<br>Qty:'.$val['stock_by_location'].'</div>';
													}
												}
												
												/*if(isset($BinLocation[$productAllDesc['ProductDesc']['barcode']])){
													$blArray = $BinLocation[$productAllDesc['ProductDesc']['barcode']];
													//print_r($blArray );
													foreach($blArray  as $val){
													echo '<div style="font-size:10px; border-top:dotted #999999 1px;">'.$val['bin_location'] .'<br>Qty:'.$val['stock_by_location'].'</div>';
													}
												
												}*/
												?>
												</td>
														
												<td  class="sorting_1">												 
													<?php 
													$q = 0;
													if(isset($openorders[$productAllDesc['Product']['product_sku']])){
														  $q = $openorders[$productAllDesc['Product']['product_sku']];
													
													}
													echo '<div style="font-size:11px;">Open:<strong>'.$q.'</strong></div>';
													if(isset($unp_data[$productAllDesc['Product']['product_sku']])){
														$unpArray = $unp_data[$productAllDesc['Product']['product_sku']];
														$qq = 0;											
														foreach($unpArray  as $val){
															$qq += $val['quantity'];
														}
														if($qq >0){
															$t =(int)$q + (int)$qq ;
															echo '<div style="font-size:11px; border-top:dotted #999999 1px;">UnP:<strong>'.$qq .'</strong></div>';
															echo '<div style="font-size:12px; border-top:dotted #999999 1px;">Total:<strong>'. $t.'</strong></div>';
														}
													}													
												?>
												</td>
												<td class="  sorting_1 intransitsku intransitsku_<?php echo $productAllDesc['Product']['product_sku']; ?>" for="<?php echo $productAllDesc['Product']['product_sku']; ?>" ></td>
												
												<td class="  sorting_1"><?php echo $status; ?></td>
												
												<td class="  sorting_1 comment_status_<?php echo $productAllDesc['Product']['id']; ?>" ><?php echo ($productAllDesc['Product']['comment'] != '') ? $productAllDesc['Product']['comment'] : '---'; ?></td>
												<td class="  sorting_1"><?php 
												if($productAllDesc['ProductDesc']['frezile_mark']==1){
													echo '<p style="color: white;background: red;text-align: center;">Yes</p>';}else{echo '<p style="color: white;background: red;text-align: center;">No</p>';} ?></td>
                                                <td>
                                                <ul id="icons" class="iconButtons">
													<a href="/Product/edit/<?php echo $productAllDesc['Product']['id']; ?>" class="btn btn-success btn-xs margin-right-10"><i class="fa fa-pencil"></i></a>
													<?php if($productAllDesc['Product']['product_status'] == 0) { ?>	
													<a href="Product/lock/UnlockPR/<?php echo $productAllDesc['Product']['id']; ?>/<?php echo $status; ?>" alt="Unlock" class="btn btn-warning btn-xs margin-right-10"><i class="fa fa-unlock"></i></a>
													<?php } else { ?>
													<a href="Product/lock/UnlockPR/<?php echo $productAllDesc['Product']['id']; ?>/<?php echo $status; ?>" alt="Lock" class="btn btn-warning btn-xs margin-right-10"><i class="fa fa-lock"></i></a>
													<?php } if($productAllDesc['Product']['is_deleted'] == 0) { ?>
													<a href="Product/delete/PR/<?php echo $productAllDesc['Product']['id']; ?>/<?php echo $productAllDesc['Product']['is_deleted']; ?>" alt="Retrieve" class="btn btn-danger btn-xs"><i class="ion-plus-circled"></i></a>
													<?php } else { ?>
													<a href="Product/delete/PR/<?php echo $productAllDesc['Product']['id']; ?>/<?php echo $productAllDesc['Product']['is_deleted']; ?>" alt="Delete" class="btn btn-danger btn-xs"><i class="ion-minus-circled"></i></a>
													<?php }  ?>
													<a href="javascript:void(0);" for="<?php echo $productAllDesc['Product']['id']; ?>" class="create_qrcode_popup btn btn-success btn-xs margin-right-10">QR</a>
													<a href="javascript:void(0);" for="<?php echo $productAllDesc['Product']['id']; ?>" class="create_barcode_popup bg-blue-400 btn-xs margin-right-10">Barcode</a>
													<a href="javascript:void(0);" for="<?php echo $productAllDesc['Product']['id']; ?>" class="create_comment_popup btn btn-success btn-xs margin-right-10">Comment</a>
													<!--Apply code for Global Barcode-->
													<a href="/Products/addBarcode/<?php echo $productAllDesc['Product']['product_sku']; ?>" for="<?php echo $productAllDesc['Product']['id']; ?>" class="btn btn-success btn-xs margin-right-10">Add Barcode</a>
												</ul>
                                                </td>												
                                            </tr>
											<?php } ?>
										</tbody>
									</table>
									
									<div class="" style="margin:0 auto; width:350px">
										 <ul class="pagination">
										  <?php
											   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
											   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
											   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										  ?>
										 </ul>
									</div
									
											
								></div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
				<!-- BEGIN FOOTER -->
				<footer class="bg-white">
					<div class="pull-left">
						<span class="pull-left margin-right-15">&copy; 2015 WMS by JijGroup.</span>
						<ul class="list-inline pull-left">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
						</ul>
					</div>
				</footer>
				<!-- END FOOTER -->
            </div>
    </div>
</div>
<div class="showPopupForAssignSlip"></div> 
<div class="showPopupForcomment"></div> 

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

<script>
	
	$(document).ready(function() {
		
					/*$('.openorder').each(function() {
						var currentSku	= $(this).attr('for');
						$.ajax({
								url     	: '/Products/getOpenOrderSkuQty',
								type    	: 'POST',
								data    	: { currentSku : currentSku },  			  
								success 	:	function( data  )
								{
									$('.openorderqty_'+currentSku).text( data );
								}                
						});
					});*/
					
					$('.intransitsku').each(function() {
						var currentSku	= $(this).attr('for');
						$.ajax({
								url     	: '/Products/getIntransitSku',
								type    	: 'POST',
								data    	: { currentSku : currentSku },  			  
								success 	:	function( data  )
								{
									$('.intransitsku_'+currentSku).text( data );
								}                
						});
					});
	});
	
	
	$(function()
	{
		//Download Stock file now
		$( 'body' ).on( 'click', '.downloadStockFile', function()
		{
			//Check If input rack name exists in table or not
			$.ajax({
					url     	: '/Products/prepareExcel',
					type    	: 'POST',
					data    	: {}, 
					success 	:	function( data  )
					{
						 window.open(data,'_self' );
					}                
				});				
		});
		
		$( 'body' ).on( 'click', '.downloadBinLocation', function()
		{
			//Check If input rack name exists in table or not
			$.ajax({
					url     	: '/Outers/getStock_ActualReport',
					type    	: 'POST',
					data    	: {},  			 
					beforeSend 	: function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },  
					success 	:	function( data  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						 window.open(data,'_self' );
					}                
				});
		});	
		
		$( 'body' ).on( 'click', '.downloadSkuReff', function()
		{
			//Check If input rack name exists in table or not
			$.ajax({
					url     	: '/Outers/getStock_ActualReport',
					type    	: 'POST',
					data    	: {},  			 
					beforeSend 	: function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },  
					success 	:	function( data  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						 window.open(data,'_self' );
					}                
				});
		});	
		
	});
	
	$('#ProductSearchtext').bind('keypress', function(e)
	{
	   if(e.which == 13) {
		   if($('#ProductSearchtext').val() != '')
			{
				submitSanningValue();
			}
			else
			{
				
			}
			
		}
	});
	
	function submitSanningValue()
	{
		var searchKey	=	$('#ProductSearchtext').val();
		$.ajax({
					url     	: '/Products/getSearchResult',
					type    	: 'POST',
					data    	: {  searchKey : searchKey },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					},   			  
					success 	:	function( data  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.panel-body').html( data );
					}                
				});
	}
	
	$('body').on( 'click', '.create_barcode_popup', function(){
		
		var productId	=	$( this ).attr('for');
		$.ajax({
					url     	: '/Virtuals/getBarcodePopup',
					type    	: 'POST',
					data    	: {  productId : productId },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					},  			  
					success 	:	function( popUpdata  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.showPopupForAssignSlip').html( popUpdata );
					}                
				});
		});
		
	$('body').on( 'click', '.create_qrcode_popup', function(){
		
		var productId	=	$(this).attr('for');
		$.ajax({
					url     	: '/Virtuals/getqrcodePopup',
					type    	: 'POST',
					data    	: {  productId : productId },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					},  			  
					success 	:	function( popUpdata  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.showPopupForAssignSlip').html( popUpdata );
						
					}              
				});
				
		});
	
	$('body').on( 'click', '.create_comment_popup', function(){
		
		var productId	=	$(this).attr('for');
		$.ajax({
					url     	: '/Virtuals/getcommentPopup',
					type    	: 'POST',
					data    	: {  productId : productId },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					},  			  
					success 	:	function( popUpdata  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.showPopupForcomment').html( popUpdata );
						
					}              
				});
				
		});
	
</script>
