<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-top:1px solid #666666; padding:0px;} 
	.body div.sku div{padding:4px 3px; font-size:12px;} 
	/*.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.pl-2{padding-left:4px;}
	.col-65{width:65%; float:left;}
	.fa-spin-custom, .glyphicon-spin {
    -webkit-animation: spin 1000ms infinite linear;
    animation: spin 1000ms infinite linear;
}@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
@keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
	</style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title">Product Upload Log Files</h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?>
					
				</div>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
 							 
							<div class="row">	
							
								<form method="get" name="searchfrm" id="searchfrm" action="<?php echo Router::url('/', true) ?>Products/FilesLog" onsubmit="return searchFrm();">				 		
	
									<div class="col-lg-3">
										<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Search By templete OR User" name="searchkey" class="form-control searchString" />
									</div>
									
									<!--<div class="col-lg-3"> 
										<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
									</div>-->
							 								
									<div class="col-lg-5">
 										 <div class="col-70">
										 <div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600  border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
										 
										<input name="start" type="hidden" id="start">
										<input name="end" type="hidden" id="end">
										 </div>								 
										<div class="col-20">
											<!-- <a href="javascript:void(0);" class="btn btn-info filter" role="button" title="Filter Data"><i class="fa fa-filter"></i></a>--><button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
										 </div>
										 <!--  <div class="col-15">									 
											 <a href="javascript:void(0);" class="btn btn-primary downloadfeed" role="button" title="Download Feed" ><i class="glyphicon glyphicon-download"></i></a>									
										 </div>		-->						
								   </div>
								   
								   <div class="col-lg-4" style="text-align:right;">
									 
									 <div class="col-65">
									 
									 <select name="filter" id="filter"  class="form-control selectpicker" data-live-search="true">
											<optgroup label="Filter By Template">
												<option value="">All Template</option>
												<option by="template" value="CoreUploadFields" <?php if(isset($_REQUEST['filter']) && ($_REQUEST['filter'] == 'CoreUploadFields')) echo 'selected=selected'?>>Core Upload Fields</option>
												<option by="template" value="DetailedProductInformation" <?php if(isset($_REQUEST['filter']) && ($_REQUEST['filter'] == 'DetailedProductInformation')) echo 'selected=selected'?>>Detailed Product Information</option>
												<option by="template" value="ChannelSKUMapping" <?php if(isset($_REQUEST['filter']) && ($_REQUEST['filter'] == 'ChannelSKUMapping')) echo 'selected=selected'?>>Channel SKU Mapping</option>
												<option by="template" value="ChannelSKUManagement" <?php if(isset($_REQUEST['filter']) && ($_REQUEST['filter'] == 'ChannelSKUManagement')) echo 'selected=selected'?>>Channel SKU Management</option>
											</optgroup>
											<optgroup label="Filter By User">
												<option value="">All Users</option>
												<?php foreach($users as $v){
												$selected = '';
												if(isset($_REQUEST['filter']) && ($_REQUEST['filter'] == $v)) $selected ='selected=selected';
												?>
												<option by="user" value="<?php echo $v?>" <?php echo $selected?>><?php echo $v?></option>
												<?php }?>
											 
											</optgroup>
										</select>												
 									 
									 </div>									 
									<div class="col-15">
										 <a href="javascript:void(0);" class="btn btn-info filter" role="button" title="Filter Data"><i class="fa fa-filter"></i></a>
									 </div>
									  <div class="col-20">	
									  <?php if(isset($_REQUEST['filter']) || isset($_REQUEST['searchkey'])){?>
										 <a href="<?php echo Router::url('/', true) ?>Products/FilesLog" class="btn btn-primary" role="button" title="Go Back">Back</a>
										 <?php }?>									
									 </div>	 						
								</div>
							</div>
								
								</form>
							</div>	
  						</div>	
 								
							
					<div class="panel-body no-padding-top bg-white">
					 
						<div class="panel-tools" style="margin-bottom: 10px;clear: right;text-align: center; float: right;">
							<ul class="pagination">
							<?php
							   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
							   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							?>
							</ul>
						</div> 
 						
					
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-25">Template</div>		
								<div class="col-25">Uploaded File</div>		
 								<div class="col-25">Uploaded By</div>	
								<div class="col-12">Added Date</div>	 
  							</div>								
						</div>	
							
						<div class="body" id="results">
						<div id="p_1"></div>
						<?php if(count($all_files) > 0){
  						foreach( $all_files as $file ) {  
  						?>
 						<div class="row sku"> 
							<div class="col-lg-12">
								<div class="col-25"><?php echo $file['ProductFilesLog']['template']; ?></div>
								<div class="col-25"><a href="<?php echo Router::url('/', true) ?>Products/ProductFilesLog/<?php echo $file['ProductFilesLog']['id']; ?>" style="color:#0000CC"><?php echo $file['ProductFilesLog']['uploaded_file']; ?></a></div>
								<div class="col-25"><?php echo $file['ProductFilesLog']['uploaded_by']; ?></div>
								<div class="col-12"><?php echo $file['ProductFilesLog']['added_date']; ?></div>
 					 
								
								<?php /*?><div class="col-10"><a href="<?php echo Router::url('/', true) ?>ProductFilesLogs/DeleteMap/<?php echo $file['ProductFilesLog']['id']; ?>" type="button" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete this?')">Delete</a></div>	<?php */?>
							</div>
							 						
						</div>
						
						<?php }?>
						<?php }else{?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php } ?>
						</div>
						<br />

						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
						
			</div>			
		
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>
 
<script>	
 						
 function searchFrm(){
 
	var start = $('div.daterangepicker_start_input input:text').val();
	var end  = $('div.daterangepicker_end_input input:text').val();
	$( '#start' ).val(start);
	$( '#end' ).val(end);	 
	
	document.searchfrm.submit();
 }
 
 
$(".filter").click(function(){

var start = $('div.daterangepicker_start_input input:text').val();
	var end  = $('div.daterangepicker_end_input input:text').val();
	$( '#start' ).val(start);
	$( '#end' ).val(end);	 
	
	//document.searchfrm.submit();
 	window.location.href = '<?php echo Router::url('/', true) ?>Products/FilesLog/?filter='+$("#filter option:selected").val()+'&by='+$("#filter option:selected").attr('by');
 });

$(".downloadfeed").click(function(){	
	if($("#template option:selected").val() == ''){
		alert("Please Select Template");	
	}else{ 	
		window.location.href = '<?php echo Router::url('/', true) ?>Skumappings/getChannelSkus/'+$("#template option:selected").val();
	}
		 
});

 
 
 
</script>
