<div class="rightside bg-grey-100">
   <!-- BEGIN PAGE HEADING -->
   <div class="page-head bg-grey-100">
      <h1 class="page-title"><?php print $title;?></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
   </div>
   <!-- END PAGE HEADING -->
   <div class="container-fluid">
      <div class="panel padding-top-40">
         <div class="row">
            <div class="col-lg-12 ">
               <div class="col-lg-9">
                  <!-- Tab panes -->
                  <div class="tab-content">
                     <div class="tab-pane active" id="general">
                        <div class="panel">
                           <div class="panel-title">
                              <div class="panel-head">General</div>
                           </div>
                           <?php							
                              print $this->form->create( 'Product', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/products/save_editproduct', 'type'=>'post','id'=>'savebarcode' ) );
                              print $this->form->input( 'Product.id', array( 'type'=>'hidden') );
                              print $this->form->input( 'ProductDesc.id', array( 'type'=>'hidden') );
                            ?>
                           <div class="panel-body ">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Name</label>                                        
                                       <div class="col-lg-10">                                            
                                          <?php
                                             //pr( $this->request->data );
                                             print $this->form->input( 'Product.product_name', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>  
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Short Description	</label>                                        
                                       <div class="col-lg-10">
                                          <?php
                                             print $this->form->input( 'ProductDesc.short_description', array( 'type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control bs-texteditor', 'required'=>false ) );
                                             ?>  
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Long Description	</label>                                        
                                       <div class="col-lg-10">
                                          <?php
                                             print $this->form->input( 'ProductDesc.long_description', array( 'type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control bs-texteditor', 'required'=>false ) );
                                             ?>  
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">SKU</label>                                        
                                       <div class="col-lg-10">                                            
                                          <?php
                                             print $this->form->input( 'Product.product_sku', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>  
                                       </div>
                                    </div>
                                    <!--<div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Weight</label>                                        
                                       <div class="col-lg-10">                                            
                                          <?php
                                            // print $this->form->input( 'Product.weight', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>  
                                       </div>
                                    </div>-->
                                    <!--<div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Status</label>                                        
                                       <div class="col-lg-10">                                            
                                          <?php
                                             $statusArray = Configure::read( 'status_key' );
                                             if( count( $statusArray ) > 0 )
                                               //  print $this->form->input( 'Product.product_status', array( 'type'=>'select', 'empty'=>'Choose status','options'=>$statusArray,'class'=>'form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
                                             ?>  
                                       </div>
                                    </div>-->
                                    <!--<div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Country of Manufacture</label>                                        
                                       <div class="col-lg-10">                                            
                                          <?php
                                             $statusArray = Configure::read( 'status_key' );
                                             if( count( $statusArray ) > 0 )
                                                // print $this->form->input( 'Product.Country', array( 'type'=>'select', 'empty'=>'Choose','options'=>$statusArray,'class'=>'form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
                                             ?>  
                                       </div>
                                    </div>-->
                                    <!--<div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Brands</label>                                        
                                       <div class="col-lg-10">                                            
                                          <?php
                                             $statusArray = Configure::read( 'status_key' );
                                             if( count( $statusArray ) > 0 )
                                                 //print $this->form->input( 'Product.Brands', array( 'type'=>'select', 'empty'=>'Choose','options'=>$statusArray,'class'=>'form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
                                             ?>  
                                       </div>
                                    </div>-->
                                    <!--<div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Barcode</label>                                        
                                       <div class="col-lg-10">                                            
                                          <?php
                                           //  print $this->form->input( 'ProductDesc.barcode', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                           ?> 
                                       </div>
                                    </div>-->
                                    
                                    <!--<div class="form-group" >
										<label for="username" class="control-label col-lg-2">Store</label>                                        
										<div class="col-lg-10">                                            
										   <?php
												//if( count( $storeList ) > 0 )
												//print $this->form->input( 'Product.store_name', array( 'type'=>'select', 'multiple' => 'multiple', 'empty'=>'Choose Country','options'=>$storeList,'class'=>'form-control', 'div'=>false, 'label'=>false, 'required'=>false, 'style' => 'height:150px;') );
											?>
										</div>
									</div>-->
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                    <!--<div class="tab-pane" id="prices">
                        <div class="panel">
                           <div class="panel-title">
                              <div class="panel-head">Prices</div>
                           </div>
                           <div class="panel-body ">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Price</label>                                        
                                       <div class="col-lg-10">                                            
                                          <?php
                                             print $this->form->input( 'Product.Price', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>  
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Group Price</label>                                        
                                       <div class="col-lg-10">
                                          <?php
                                             print $this->form->input( 'Product.group_price', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>   
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Special Price</label>                                        
                                       <div class="col-lg-10">
                                          <?php
                                             print $this->form->input( 'Product.special_price', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>  
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Tax Class</label>                                        
                                       <div class="col-lg-10">                                            
                                          <?php
                                             $statusArray = Configure::read( 'status_key' );
                                             if( count( $statusArray ) > 0 )
                                                 print $this->form->input( 'Product.tax_class', array( 'type'=>'select', 'empty'=>'Choose status','options'=>$statusArray,'class'=>'form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
                                             ?>  
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>-->
                     <!--<div class="tab-pane" id="meta">
                        <div class="panel">
                           <div class="panel-title">
                              <div class="panel-head">Meta Information</div>
                           </div>
                           <div class="panel-body ">
                              <div class="row">
                                 <div class="col-lg-12">
							        
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Meta Title</label>                                        
                                       <div class="col-lg-10">                                            
                                          <?php
                                             print $this->form->input( 'Product.meta_title', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>  
                                       </div>
                                    </div>
                                    
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Meta Keywords</label>                                        
                                       <div class="col-lg-10">
                                          <?php
                                             print $this->form->input( 'Product.meta_keywords', array( 'type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>  
                                       </div>
                                    </div>
                                    
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Meta Description</label>                                        
                                       <div class="col-lg-10">
                                          <?php
                                             print $this->form->input( 'Product.meta_description', array( 'type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>  
                                       </div>
                                    </div>
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>-->
                     <!--<div class="tab-pane" id="images">
                        <div class="panel">
                           <div class="panel-title">
                              <div class="panel-head">Images</div>
                           </div>
                           <div class="panel-body ">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="form-group">
                                       <div class="col-lg-12">   
										   <?php
											print $this->form->input( 'Product.product_image', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'file', 'multiple data-preview-file-type' => 'any', 'data-upload-url' => '#','data-preview-file-icon' => '','required'=>false, 'name' => 'data[Product][product_image][]' ));
										   ?>                                         
                                          <!--<input id="file-5" class="file" type="file" multiple data-preview-file-type="any" data-upload-url="#" data-preview-file-icon="">-->  
                                       <!--</div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>-->
                     <!--<div class="tab-pane" id="inventory">
                        <div class="panel">
                           <div class="panel-title">
                              <div class="panel-head">Inventory</div>
                           </div>
                           <div class="panel-body ">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Qty</label>                                        
                                       <div class="col-lg-10">
                                          <?php
                                             print $this->form->input( 'Product.quantity', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>  
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="username" class="control-label col-lg-2">Stock Availability</label>                                        
                                       <div class="col-lg-10">                                            
                                          <?php
                                             $statusArray = Configure::read( 'status_key' );
                                             if( count( $statusArray ) > 0 )
                                                 print $this->form->input( 'Product.stock_availability', array( 'type'=>'select', 'empty'=>'Choose status','options'=>$statusArray,'class'=>'form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
                                             ?>  
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>-->
                     <div class="tab-pane" id="categories">
                        <div class="panel">
                           <div class="panel-title">
                              <div class="panel-head">Categories</div>
                           </div>
                           <?php							
                              //print $this->form->create( 'Brand', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/manageBrand', 'type'=>'post','id'=>'brand' ) );
                              ?>
                           <div class="panel-body ">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="input" class="form-control" id="input-check-node" placeholder="Search Category..." value="">
                                    <ul class="list-group">
                                     <?php 
										foreach($getCategories as $getCategory) { 
										$haveChild = (count($getCategory['Children']) > 0 ) ? "glyphicon-plus" : "";
									 ?>
										<div id="treeview-checkable1" class="">
											<li class="list-group-item node-treeview-checkable" id="<?php echo $getCategory['Category']['id']; ?>" data-nodeid="0" style="color:undefined;background-color:undefined;">
												<span class="icon expand-icon glyphicon <?php echo $haveChild; ?>"></span><input type="checkbox" class="l-tcb" id="ext-gen15" name ="data[Product][category_id]" value="<?php echo $getCategory['Category']['id']; ?>">
												<?php 
													//print $this->form->input( 'Product.category_id', array('type'=>'checkbox', 'label'=>'Label', 'checked'=>'') );
												?>
												<?php echo $getCategory['Category']['category_name']; ?>
											</li>
										</div>
									<?php } ?>
									
							        </ul>
							        <!--<div id="tree1" data-url="/wms/products/getntchild" >asd</div>
							        <div id="treeview12" data-url="/wms/products/getntchild" ></div>-->
                                 </div>
                              </div>
                        </div>
                     </div>
                  </div>
               </div>
                <div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100">
					<button class="btn bg-orange-500 color-white btn-dark margin-right-10 padding-left-40 padding-right-40" type="submit" formaction="/showAllProduct" >Go Back</button>
						<?php
							echo $this->Form->button('Edit Product', array(
								'type' => 'submit',
								'escape' => true,
								'class'=>'add_brand btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
								 ));	
						?>
                 </div>
		   </div>
		   </form>
               <div class="col-lg-3">
                  <ul class="nav nav-tabs tabs-right">
                     <li class="active"><a href="#general" data-toggle="tab">General</a></li>
                     <!--<li><a href="#prices" data-toggle="tab">Prices</a></li>
                     <!--<li><a href="#meta" data-toggle="tab">Meta Information</a></li>-->
                     <!--<li><a href="#images" data-toggle="tab">Images</a></li>-->
                     <!--<li><a href="#inventory" data-toggle="tab">Inventory</a></li>
                     <li ><a href="#categories" data-toggle="tab">Categories</a></li>
                     <!--<li><a href="#general" data-toggle="tab">Related Products</a></li>
                     <li><a href="#general" data-toggle="tab">Up-sells</a></li>
                     <li><a href="#general" data-toggle="tab">Cross-sells</a></li>
                     <li><a href="#general" data-toggle="tab">Custom Options</a></li>-->
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
$(document).keypress(function(e) {
   if(e.which == 13) {
	   $('#savebarcode').submit();
	    return false;
    }
});
</script>


<script>
	/*
var data = [
    {
        "label": "Saurischia",
        "id": 1,
        "load_on_demand": true
    },
    {
        "label": "Ornithischians",
        "id": 23,
        "load_on_demand": true
    }
];

$(function() {
    $('#tree1').tree({
    data: data,
    autoOpen: true,
    dragAndDrop: true
});
});
*/



</script>
<script>

$('body').on('click','.node-treeview-checkable', function(){
	
	var id = $(this).attr('id');
	var spanClass 	= 	$('li#'+id).find('span').attr('class');
	var nextClass	=	$( 'li#'+id ).next('ul').attr('class');
	
	//alert(nextClass);
	var hasCheckedClass = $('li#'+id).find('span').hasClass('glyphicon-plus');
	
		if(hasCheckedClass == false)
		{
			return false;
		}
		
		if(nextClass == 'child')
		{
			return false;
		}
		
		    $.ajax(
            {
                url     : getUrl() + '/products/getNthChild',
                type    : 'POST',
                data    : { id : id },
                success : function( msgArray  )
                {
					$('#'+id).after( msgArray );
					$('li#'+id).find('span').removeClass('glyphicon-plus').addClass('glyphicon-minus');
                }                
            }); 
	});




</script>

<?php
   if(!empty($popupArray))
   {	
   	$objectComponent = $this->Common->getcomponent( "UploadComponent" );	
   	echo $objectComponent->Openpopup($popupArray);		
   } 
   ?>
<script type="text/javascript">
   $(function() {
   
       var defaultData = [
         {
           text: 'Parent 1',
           href: '#parent1',
           tags: ['4'],
           nodes: [
             {
               text: 'Child 1',
               href: '#child1',
               tags: ['2'],
               nodes: [
                 {
                   text: 'Grandchild 1',
                   href: '#grandchild1',
                   tags: ['0']
                 },
                 {
                   text: 'Grandchild 2',
                   href: '#grandchild2',
                   tags: ['0']
                 }
               ]
             },
             {
               text: 'Child 2',
               href: '#child2',
               tags: ['0']
             }
           ]
         },
         {
           text: 'Parent 2',
           href: '#parent2',
           tags: ['0']
         },
         {
           text: 'Parent 3',
           href: '#parent3',
            tags: ['0']
         },
         {
           text: 'Parent 4',
           href: '#parent4',
           tags: ['0']
         },
         {
           text: 'Parent 5',
           href: '#parent5'  ,
           tags: ['0']
         }
       ];
   
       var alternateData = [
         {
           text: 'Parent 1',
           tags: ['2'],
           nodes: [
             {
               text: 'Child 1',
               tags: ['3'],
               nodes: [
                 {
                   text: 'Grandchild 1',
                   tags: ['6']
                 },
                 {
                   text: 'Grandchild 2',
                   tags: ['3']
                 }
               ]
             },
             {
               text: 'Child 2',
               tags: ['3']
             }
           ]
         },
         {
           text: 'Parent 2',
           tags: ['7']
         },
         {
           text: 'Parent 3',
           icon: 'glyphicon glyphicon-earphone',
           href: '#demo',
           tags: ['11']
         },
         {
           text: 'Parent 4',
           icon: 'glyphicon glyphicon-cloud-download',
           href: '/demo.html',
           tags: ['19'],
           selected: true
         },
         {
           text: 'Parent 5',
           icon: 'glyphicon glyphicon-certificate',
           color: 'pink',
           backColor: 'red',
           href: 'http://www.tesco.com',
           tags: ['available','0']
         }
       ];
   
       var json = '[' +
         '{' +
           '"text": "Parent 1",' +
           '"nodes": [' +
             '{' +
               '"text": "Child 1",' +
               '"nodes": [' +
                 '{' +
                   '"text": "Grandchild 1"' +
                 '},' +
                 '{' +
                   '"text": "Grandchild 2"' +
                 '}' +
               ']' +
             '},' +
             '{' +
               '"text": "Child 2"' +
             '}' +
           ']' +
         '},' +
         '{' +
           '"text": "Parent 2"' +
         '},' +
         '{' +
           '"text": "Parent 3"' +
         '},' +
         '{' +
           '"text": "Parent 4"' +
         '},' +
         '{' +
           '"text": "Parent 5"' +
         '}' +
       ']';
   
       var initSelectableTree = function() {
         return $('#treeview-selectable').treeview({
           data: defaultData,
           multiSelect: $('#chk-select-multi').is(':checked'),
           onNodeSelected: function(event, node) {
             $('#selectable-output').prepend('<p>' + node.text + ' was selected</p>');
           },
           onNodeUnselected: function (event, node) {
             $('#selectable-output').prepend('<p>' + node.text + ' was unselected</p>');
           }
         });
       };
       var $selectableTree = initSelectableTree();
   
       var findSelectableNodes = function() {
         return $selectableTree.treeview('search', [ $('#input-select-node').val(), { ignoreCase: false, exactMatch: false } ]);
       };
       var selectableNodes = findSelectableNodes();
   
       $('#chk-select-multi:checkbox').on('change', function () {
         console.log('multi-select change');
         $selectableTree = initSelectableTree();
         selectableNodes = findSelectableNodes();          
       });
   
       // Select/unselect/toggle nodes
       $('#input-select-node').on('keyup', function (e) {
         selectableNodes = findSelectableNodes();
         $('.select-node').prop('disabled', !(selectableNodes.length >= 1));
       });
   
       $('#btn-select-node.select-node').on('click', function (e) {
         $selectableTree.treeview('selectNode', [ selectableNodes, { silent: $('#chk-select-silent').is(':checked') }]);
       });
   
       $('#btn-unselect-node.select-node').on('click', function (e) {
         $selectableTree.treeview('unselectNode', [ selectableNodes, { silent: $('#chk-select-silent').is(':checked') }]);
       });
   
       $('#btn-toggle-selected.select-node').on('click', function (e) {
         $selectableTree.treeview('toggleNodeSelected', [ selectableNodes, { silent: $('#chk-select-silent').is(':checked') }]);
       });
   
   
   
       var $expandibleTree = $('#treeview-expandible').treeview({
         data: defaultData,
         onNodeCollapsed: function(event, node) {
           $('#expandible-output').prepend('<p>' + node.text + ' was collapsed</p>');
         },
         onNodeExpanded: function (event, node) {
           $('#expandible-output').prepend('<p>' + node.text + ' was expanded</p>');
         }
       });
   
       var findExpandibleNodess = function() {
         return $expandibleTree.treeview('search', [ $('#input-expand-node').val(), { ignoreCase: false, exactMatch: false } ]);
       };
       var expandibleNodes = findExpandibleNodess();
   
       // Expand/collapse/toggle nodes
       $('#input-expand-node').on('keyup', function (e) {
         expandibleNodes = findExpandibleNodess();
         $('.expand-node').prop('disabled', !(expandibleNodes.length >= 1));
       });
   
       $('#btn-expand-node.expand-node').on('click', function (e) {
         var levels = $('#select-expand-node-levels').val();
         $expandibleTree.treeview('expandNode', [ expandibleNodes, { levels: levels, silent: $('#chk-expand-silent').is(':checked') }]);
       });
   
       $('#btn-collapse-node.expand-node').on('click', function (e) {
         $expandibleTree.treeview('collapseNode', [ expandibleNodes, { silent: $('#chk-expand-silent').is(':checked') }]);
       });
   
       $('#btn-toggle-expanded.expand-node').on('click', function (e) {
         $expandibleTree.treeview('toggleNodeExpanded', [ expandibleNodes, { silent: $('#chk-expand-silent').is(':checked') }]);
       });
   
       // Expand/collapse all
       $('#btn-expand-all').on('click', function (e) {
         var levels = $('#select-expand-all-levels').val();
         $expandibleTree.treeview('expandAll', { levels: levels, silent: $('#chk-expand-silent').is(':checked') });
       });
   
       $('#btn-collapse-all').on('click', function (e) {
         $expandibleTree.treeview('collapseAll', { silent: $('#chk-expand-silent').is(':checked') });
       });
   
   
   
       var $checkableTree = $('#treeview-checkable').treeview({
         data: defaultData,
         showIcon: false,
         showCheckbox: true,
         onNodeChecked: function(event, node) {
           $('#checkable-output').prepend('<p>' + node.text + ' was checked</p>');
         },
         onNodeUnchecked: function (event, node) {
           $('#checkable-output').prepend('<p>' + node.text + ' was unchecked</p>');
         }
       });
   
       var findCheckableNodess = function() {
         return $checkableTree.treeview('search', [ $('#input-check-node').val(), { ignoreCase: false, exactMatch: false } ]);
       };
       var checkableNodes = findCheckableNodess();
   
       // Check/uncheck/toggle nodes
       $('#input-check-node').on('keyup', function (e) {
         checkableNodes = findCheckableNodess();
         $('.check-node').prop('disabled', !(checkableNodes.length >= 1));
       });
   
       $('#btn-check-node.check-node').on('click', function (e) {
         $checkableTree.treeview('checkNode', [ checkableNodes, { silent: $('#chk-check-silent').is(':checked') }]);
       });
   
       $('#btn-uncheck-node.check-node').on('click', function (e) {
         $checkableTree.treeview('uncheckNode', [ checkableNodes, { silent: $('#chk-check-silent').is(':checked') }]);
       });
   
       $('#btn-toggle-checked.check-node').on('click', function (e) {
         $checkableTree.treeview('toggleNodeChecked', [ checkableNodes, { silent: $('#chk-check-silent').is(':checked') }]);
       });
   
       // Check/uncheck all
       $('#btn-check-all').on('click', function (e) {
         $checkableTree.treeview('checkAll', { silent: $('#chk-check-silent').is(':checked') });
       });
   
       $('#btn-uncheck-all').on('click', function (e) {
         $checkableTree.treeview('uncheckAll', { silent: $('#chk-check-silent').is(':checked') });
       });
   
   
   
       var $disabledTree = $('#treeview-disabled').treeview({
         data: defaultData,
         onNodeDisabled: function(event, node) {
           $('#disabled-output').prepend('<p>' + node.text + ' was disabled</p>');
         },
         onNodeEnabled: function (event, node) {
           $('#disabled-output').prepend('<p>' + node.text + ' was enabled</p>');
         },
         onNodeCollapsed: function(event, node) {
           $('#disabled-output').prepend('<p>' + node.text + ' was collapsed</p>');
         },
         onNodeUnchecked: function (event, node) {
           $('#disabled-output').prepend('<p>' + node.text + ' was unchecked</p>');
         },
         onNodeUnselected: function (event, node) {
           $('#disabled-output').prepend('<p>' + node.text + ' was unselected</p>');
         }
       });
   
       var findDisabledNodes = function() {
         return $disabledTree.treeview('search', [ $('#input-disable-node').val(), { ignoreCase: false, exactMatch: false } ]);
       };
       var disabledNodes = findDisabledNodes();
   
       // Expand/collapse/toggle nodes
       $('#input-disable-node').on('keyup', function (e) {
         disabledNodes = findDisabledNodes();
         $('.disable-node').prop('disabled', !(disabledNodes.length >= 1));
       });
   
       $('#btn-disable-node.disable-node').on('click', function (e) {
         $disabledTree.treeview('disableNode', [ disabledNodes, { silent: $('#chk-disable-silent').is(':checked') }]);
       });
   
       $('#btn-enable-node.disable-node').on('click', function (e) {
         $disabledTree.treeview('enableNode', [ disabledNodes, { silent: $('#chk-disable-silent').is(':checked') }]);
       });
   
       $('#btn-toggle-disabled.disable-node').on('click', function (e) {
         $disabledTree.treeview('toggleNodeDisabled', [ disabledNodes, { silent: $('#chk-disable-silent').is(':checked') }]);
       });
   
       // Expand/collapse all
       $('#btn-disable-all').on('click', function (e) {
         $disabledTree.treeview('disableAll', { silent: $('#chk-disable-silent').is(':checked') });
       });
   
       $('#btn-enable-all').on('click', function (e) {
         $disabledTree.treeview('enableAll', { silent: $('#chk-disable-silent').is(':checked') });
       });
   
   
   
       var $tree = $('#treeview12').treeview({
         data: json
       });
   });
</script>
<script>
   $(document).ready(function() {
       $("#file-5").fileinput({
           'allowedFileExtensions' : ['jpg', 'png','gif'],
   'msgImageWidthSmall' : '100px'
       });
   });
</script>
<script type="text/javascript">
   function close_popup(url)
   	{
   		$('#ModalForm').css('display','none');
   		window.location=url;
   	}
</script>
</div>

