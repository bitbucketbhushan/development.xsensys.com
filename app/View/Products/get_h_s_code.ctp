<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Product HSCode & Tax Rate</h1>
		<div class="panel-head"><?php print $this->Session->flash(); ?></div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel"> 
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2"  style="background-color:#f5f5f5; border:1px solid #CCCCCC; margin-top:10px;">
					<form name="uploadfrm" id="uploadfrm" action="<?php echo Router::url('/', true) ?>Products/uploadHSCode" method="post" enctype="multipart/form-data" >
						<div class="form-horizontal panel-body padding-bottom-20 padding-top-20">
							<div class="form-group">
							<label class="col-sm-3 control-label">Upload File:</label>
							 <div class="col-sm-4"><input type="file"  class="form-control" name="hsfile" ></div>
							 <div class="col-lg-2"><button type="submit" class="btn btn-info">Upload</button>(<a href="<?php echo Router::url('/', true) ?>img/hscode-sample.csv" target="_blank">Sample file</a>)</div>
                             <div class="col-lg-2"><?php echo $this->Html->link('<i class="fa fa-download"></i>	hsn code',[
	'controller' => 'products', 
	'action' => 'dwonloadhsncode'],['class'=>'btn btn-info','escape' => false]);?>
                             </div>
							</div>
						</div>
					</form>
					</div> 
				</div>       
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2"  style="background-color:#f5f5f5; border:1px solid #CCCCCC; margin-top:10px;">
					<form name="taxratefrm" id="taxratefrm" action="<?php echo Router::url('/', true) ?>Products/SaveTaxRate" method="post" enctype="multipart/form-data" >
						<div class="form-horizontal padding-top-10">
						  <div style="text-align:center; font-weight:bold;border-bottom:1px solid #FC8C8C;padding-bottom: 5px;
margin-bottom: 10px;color:#666">Default TaxRate of Countries</div>
						  <div class="form-group">                                                                           
							<div class="col-lg-2">Germany(DE):</div>
							<div class="col-lg-2"> <input name="taxrate[de]" class="form-control" value="<?php echo $data['def_taxrate_de']?>" type="text" id="def_taxrate_de" placeholder="TaxRate"></div>
							<div class="col-lg-2">Spain(ES):</div>                                  
							<div class="col-lg-2"> <input name="taxrate[es]" class="form-control" value="<?php echo $data['def_taxrate_es']?>" type="text" id="def_taxrate_es" placeholder="TaxRate"></div> 
							<div class="col-lg-2">France(FR)</div>                                       
							<div class="col-lg-2"> <input name="taxrate[fr]" class="form-control" value="<?php echo $data['def_taxrate_fr']?>" type="text" id="def_taxrate_fr" placeholder="TaxRate"></div>
						  </div>
						 
						   <div class="form-group">                                                                        
							<div class="col-lg-2">Italy(IT):</div>                                     
							<div class="col-lg-2"> <input name="taxrate[it]" class="form-control" value="<?php echo $data['def_taxrate_it']?>" type="text" id="def_taxrate_it" placeholder="TaxRate"></div>
							<div class="col-lg-2">United Kingdom(GB):</div>                                       
							<div class="col-lg-2"> <input name="taxrate[gb]" class="form-control" value="<?php echo $data['def_taxrate_gb']?>" type="text" id="def_taxrate_gb" placeholder="TaxRate"></div>
							<div class="col-lg-2">Canada(CA):</div>									 
							<div class="col-lg-2"> <input name="taxrate[ca]" class="form-control" value="<?php echo $data['def_taxrate_ca']?>" type="text" id="def_taxrate_ca" placeholder="TaxRate"></div>
						   </div>
						   
							<div class="form-group">                                                                           
							<div class="col-lg-2">United States(US)</div>                                       
							<div class="col-lg-2"> <input name="taxrate[us]" class="form-control" value="<?php echo $data['def_taxrate_us']?>" type="text" id="def_taxrate_us" placeholder="TaxRate"></div>
							<div class="col-lg-2">Australia(AU)</div>                                       
							<div class="col-lg-2"> <input name="taxrate[au]" class="form-control" value="<?php echo $data['def_taxrate_au']?>" type="text" id="def_taxrate_au" placeholder="TaxRate"></div>
							<div class="col-lg-2"><button type="button" class="btn btn-warning btn-sm" onclick="SaveTaxRate()">Save Default TaxRate</button></div>
						   </div>
					</div>
					</form>
					</div> 
				</div>                                    
                <div class="row">
					<div class="col-lg-8 col-lg-offset-2">
					<form name="productfrm" id="productfrm" action="" method="post">
							
							<div class="form-horizontal panel-body padding-bottom-40 padding-top-20">
	
								   <div class="form-group">
										<label class="col-sm-3 control-label">SKU</label> 
										<div class="col-sm-6"><input name="sku" class="form-control"  type="text" id="sku"></div>
										<div class="col-lg-2"><button type="button" class="btn btn-success btn-sm" onclick="Search()">Search</button></div>
										
									</div>
									
								   
								   <div class="form-group">
									<div class="col-lg-3"><strong>Country</strong></div>                                        
									<div class="col-lg-3"><strong>HSCode</strong></div>                                     
									<div class="col-lg-3"><strong>TaxRate</strong></div>                                 
								   </div>
								   
								  <div class="form-group">                                                                           
									<div class="col-lg-3">Germany(DE)</div>                                       
									<div class="col-lg-3"> <input name="hscode[de]" class="form-control" type="text" id="hscode_de"></div>
									<div class="col-lg-3"> <input name="taxrate[de]" class="form-control" type="text" id="taxrate_de"></div>
								  </div>
								  
								  <div class="form-group">                                                                           
									<div class="col-lg-3">Spain(ES)</div>                                       
									<div class="col-lg-3"> <input name="hscode[es]" class="form-control" type="text" id="hscode_es"></div>
									<div class="col-lg-3"> <input name="taxrate[es]" class="form-control" type="text" id="taxrate_es"></div>
								  </div>
								  
								  <div class="form-group">                                                                           
									<div class="col-lg-3">France(FR)</div>                                       
									<div class="col-lg-3"> <input name="hscode[fr]" class="form-control" type="text" id="hscode_fr"></div>
									<div class="col-lg-3"> <input name="taxrate[fr]" class="form-control" type="text" id="taxrate_fr"></div>
								  </div>
								  
								  <div class="form-group">                                                                           
									<div class="col-lg-3">Italy(IT)</div>                                       
									<div class="col-lg-3"> <input name="hscode[it]" class="form-control" type="text" id="hscode_it"></div>
									<div class="col-lg-3"> <input name="taxrate[it]" class="form-control" type="text" id="taxrate_it"></div>
								  </div>
								  
								   <div class="form-group">                                                                           
									<div class="col-lg-3">United Kingdom(GB)</div>                                       
									<div class="col-lg-3"> <input name="hscode[gb]" class="form-control" type="text" id="hscode_gb"></div>
									<div class="col-lg-3"> <input name="taxrate[gb]" class="form-control" type="text" id="taxrate_gb"></div>
								   </div>
								  
								   <div class="form-group">                                                                           
									<div class="col-lg-3">Canada(CA)</div>                                       
									<div class="col-lg-3"> <input name="hscode[ca]" class="form-control" type="text" id="hscode_ca"></div>
									<div class="col-lg-3"> <input name="taxrate[ca]" class="form-control" type="text" id="taxrate_ca"></div>
								   </div>
									<div class="form-group">                                                                           
									<div class="col-lg-3">United States(US)</div>                                       
									<div class="col-lg-3"> <input name="hscode[us]" class="form-control" type="text" id="hscode_us"></div>
									<div class="col-lg-3"> <input name="taxrate[us]" class="form-control" type="text" id="taxrate_us"></div>
								   </div>
								  
									<div class="form-group">                                                                           
									<div class="col-lg-3">Australia(AU)</div>                                       
									<div class="col-lg-3"> <input name="hscode[au]" class="form-control" type="text" id="hscode_au"></div>
									<div class="col-lg-3"> <input name="taxrate[au]" class="form-control" type="text" id="taxrate_au"></div>
								   </div>
								   
								   <div class="form-group">                                                                           
									<div class="col-lg-3"><button type="button" class="btn btn-info" onclick="saveValues()">Save</button></div>
								   </div>
							   
								 
							</div>
						</form>
					</div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>

<script> 

function SaveTaxRate()
{
	var formData = new FormData($('#taxratefrm')[0]);

		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Products/SaveTaxRate/',
		type: "POST",
		async: false,		
		cache: false,
		contentType: false,
		processData: false,
		data : formData,
		success: function(data, textStatus, jqXHR)
		{		
			alert(data.msg);
			 //$("#sp-orders").html(data.html);					 						
			//$('#message').delay(20000).fadeOut('slow');							
		}		
	});	
} 
function saveValues()
{
	var formData = new FormData($('#productfrm')[0]);

		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Products/addHSCode/',
		type: "POST",
		async: false,		
		cache: false,
		contentType: false,
		processData: false,
		data : formData,
		success: function(data, textStatus, jqXHR)
		{		
			alert(data.msg);
			 //$("#sp-orders").html(data.html);					 						
			//$('#message').delay(20000).fadeOut('slow');							
		}		
	});	
} 

function Search()
{
	    $.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Products/searchHSCode/',
		type: "POST",		 
		data :{ sku:$("#sku").val()},
		success: function(data, textStatus, jqXHR)
		{	 
			 if(data.msg != ''){
				
				$("#hscode_de").val(''); 
				$("#taxrate_de").val('');
				
				$("#hscode_es").val('');
				$("#taxrate_es").val('');
				
				$("#hscode_fr").val('');
				$("#taxrate_fr").val('');
				
				$("#hscode_it").val('');
				$("#taxrate_it").val('');
				
				$("#hscode_gb").val('');
				$("#taxrate_gb").val('');
				
				$("#hscode_us").val('');
				$("#taxrate_us").val('');
				
				$("#hscode_au").val('');
				$("#taxrate_au").val('');
				
				$("#hscode_ca").val('');
				$("#taxrate_ca").val('');
				 alert(data.msg);
			 }else{
				$("#hscode_de").val(data.hscode_de); 
				$("#taxrate_de").val(data.taxrate_de);
				
				$("#hscode_es").val(data.hscode_es);
				$("#taxrate_es").val(data.taxrate_es);
				
				$("#hscode_fr").val(data.hscode_fr);
				$("#taxrate_fr").val(data.taxrate_fr);
				
				$("#hscode_it").val(data.hscode_it);
				$("#taxrate_it").val(data.taxrate_it);
				
				$("#hscode_gb").val(data.hscode_gb);
				$("#taxrate_gb").val(data.taxrate_gb);
				
				$("#hscode_us").val(data.hscode_us);
				$("#taxrate_us").val(data.taxrate_us);
				
				$("#hscode_au").val(data.hscode_au);
				$("#taxrate_au").val(data.taxrate_au);
				
				$("#hscode_ca").val(data.hscode_ca);
				$("#taxrate_ca").val(data.taxrate_ca);
			}
			 
			
			 //$("#sp-orders").html(data.html);					 						
			//$('#message').delay(20000).fadeOut('slow');							
		}		
	});	
}	
</script>