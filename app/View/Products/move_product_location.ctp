<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Move Product Location</h1>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12 col-lg-offset-0">
                             <div class="form-horizontal panel-body padding-bottom-40 padding-top-40">
								   <div class="form-group">
										<label class="col-sm-3 control-label">Product Sku/Barcode</label>
										<div class="col-sm-5">                                               
											<?php
												print $this->form->input( 'Product.Sku', array( 'type'=>'text','class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false,'Placeholder' => 'Please Enter Product Sku/Barcode' ) );
											?>
										</div>
									</div>
                                 <div class="form-horizontal panel-body" >
									<table class="sku_data col-sm-12" >
									</table>
							    </div>  
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>

<script>
	
$(document).keypress(function(e)
	{
		if(e.which == 13) {
			
			getSkuLocationValue();
		}
	});

	function getSkuLocationValue()
	{
		var sku  = $('#ProductSku').val();
		var str =''; $('.sku_data').html('');
		$.ajax({
						url     	: '<?php echo  Router::url('/', true);?>Products/GetProductLocation',
						type    	: 'POST',
						data    	: { sku : sku },  			  
						success 	:	function( msgArray  )
						{
							var json 		= 	JSON.parse(msgArray);
							var getStatus 	= 	json.status;
							var getData 	= 	json.data;
							str	+=	'<tr style="font-weight: bold; border-bottom: 2px solid #ccc;"><td width=15%;>SKU</td><td width=15%;>Location</td><td style="width: 15%;">Barcode</td><td style="width: 5%;">Stock</td><td style="width: 20%;">Location</td><td style="width: 15%;">Move Qty</td><td style="width: 10%;">Action</td></tr>';
  							for (var i = 0, max = getData.length; i < max; i++)
								{
									var id = i+1;
									var barcode			=	getData[i].BinLocation.barcode;
									var location		=	getData[i].BinLocation.bin_location;
									var locationstock	=	getData[i].BinLocation.stock_by_location;
									var timestamp		=	getData[i].BinLocation.timestamp;
									str +='<tr style="border-bottom: 2px solid #ccc;">';
									str +='<td id="'+id+'" class="sku">'+json.product_sku+'</td>';
									str +='<td id="loc_'+id+'">'+location+'</td>';
									str +='<td id="barcode_'+id+'">'+barcode+'</td>';
									str +='<td id="stock_'+id+'" >'+locationstock+'</td>';
									str +='<td><input type="text" id="new_loc_'+id+'"></td>';
									str +='<td><input type="text" id="move_stock_'+id+'" class="countQty" style="width: 40px;" value="'+locationstock+'" /></td>';
									str +='<td><button class="savevalue btn bg-green-500 color-white btn-dark btn-sm" onclick="moveQty()"for="'+id+'" ><span class="glyphicon glyphicon-plus-sign"></span>Move</button></td>';
									str +='</tr>';
								 
								}
								$('.sku_data').append( str );
						}                
				});	
	}
	
	function moveQty()
	{ 
		var itemQty = [];
		$('.sku_data .sku').each(function() {
			var id = $(this).attr('id'); 
			
 			itemQty.push({
				 sku : $(this).text(), 
				 location : $("#loc_"+id).text(),
				 barcode  : $("#barcode_"+id).text(),
				 stock    : $("#stock_"+id).text(),
				 new_loc  : $("#new_loc_"+id).val(),
				 move_stock : $("#move_stock_"+id).val()
			});
		}); 
	
	
	
		$.ajax({
			dataType: 'json',
			url : '<?php echo  Router::url('/', true);?>Products/SaveProductLocation',
			type: "POST",
			data : {loc_data:itemQty},
			success: function(data, textStatus, jqXHR)
			{
 				 
 				alert(data.msg);//$(".sku_data").html(data.msg);	
				 
				
			}		
		});	
	}
	 
	
	
	

</script>
