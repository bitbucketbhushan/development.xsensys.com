<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Bulk type skus</h1>
		<div class="panel-head"><?php print $this->Session->flash(); ?></div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel"> 
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2"  style="background-color:#f5f5f5; border:1px solid #CCCCCC; margin-top:10px;">
					<form name="uploadfrm" id="uploadfrm" action="<?php echo Router::url('/', true) ?>Products/uploadBulkSkus" method="post" enctype="multipart/form-data" >
						<div class="form-horizontal panel-body padding-bottom-20 padding-top-20">
							<div class="form-group">
							<label class="col-sm-4 control-label">Upload File:</label>
							 <div class="col-sm-4"><input type="file"  class="form-control" name="bulkskus" ></div>
							 <div class="col-lg-2"><button type="submit" class="btn btn-info">Upload</button></div>							  
							</div>
							<div class="form-group" style="border-top:1px dashed #CCCCCC; padding-top: 10px;" >
							 	 
								 <div class="col-lg-3"> <a href="<?php echo Router::url('/', true) ?>Products/allBulkSkus">Download  All Bulk Skus</a></div>
								  <div class="col-lg-4"> <a href="<?php echo Router::url('/', true) ?>img/bulkskus-marksample.csv" target="_blank">Download Sample file(Mark Bulk Sku)</a></div>
								  
								  <div class="col-lg-4"> <a href="<?php echo Router::url('/', true) ?>img/bulkskus-unmarksample.csv" target="_blank">Download Sample file(UnMark Bulk Sku)</a></div>
 							</div>
							
						</div>
					</form>
					</div> 
				</div>       
				    <div class="row"> &nbsp;</div>
					<div class="row"> &nbsp;</div>                                
                
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>

<script> 

function SaveTaxRate()
{
	var formData = new FormData($('#taxratefrm')[0]);

		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Products/SaveTaxRate/',
		type: "POST",
		async: false,		
		cache: false,
		contentType: false,
		processData: false,
		data : formData,
		success: function(data, textStatus, jqXHR)
		{		
			alert(data.msg);
			 //$("#sp-orders").html(data.html);					 						
			//$('#message').delay(20000).fadeOut('slow');							
		}		
	});	
} 
function saveValues()
{
	var formData = new FormData($('#productfrm')[0]);

		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Products/addHSCode/',
		type: "POST",
		async: false,		
		cache: false,
		contentType: false,
		processData: false,
		data : formData,
		success: function(data, textStatus, jqXHR)
		{		
			alert(data.msg);
			 //$("#sp-orders").html(data.html);					 						
			//$('#message').delay(20000).fadeOut('slow');							
		}		
	});	
} 

function Search()
{
	    $.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Products/searchHSCode/',
		type: "POST",		 
		data :{ sku:$("#sku").val()},
		success: function(data, textStatus, jqXHR)
		{	 
			 if(data.msg != ''){
				
				$("#hscode_de").val(''); 
				$("#taxrate_de").val('');
				
				$("#hscode_es").val('');
				$("#taxrate_es").val('');
				
				$("#hscode_fr").val('');
				$("#taxrate_fr").val('');
				
				$("#hscode_it").val('');
				$("#taxrate_it").val('');
				
				$("#hscode_gb").val('');
				$("#taxrate_gb").val('');
				
				$("#hscode_us").val('');
				$("#taxrate_us").val('');
				
				$("#hscode_au").val('');
				$("#taxrate_au").val('');
				
				$("#hscode_ca").val('');
				$("#taxrate_ca").val('');
				 alert(data.msg);
			 }else{
				$("#hscode_de").val(data.hscode_de); 
				$("#taxrate_de").val(data.taxrate_de);
				
				$("#hscode_es").val(data.hscode_es);
				$("#taxrate_es").val(data.taxrate_es);
				
				$("#hscode_fr").val(data.hscode_fr);
				$("#taxrate_fr").val(data.taxrate_fr);
				
				$("#hscode_it").val(data.hscode_it);
				$("#taxrate_it").val(data.taxrate_it);
				
				$("#hscode_gb").val(data.hscode_gb);
				$("#taxrate_gb").val(data.taxrate_gb);
				
				$("#hscode_us").val(data.hscode_us);
				$("#taxrate_us").val(data.taxrate_us);
				
				$("#hscode_au").val(data.hscode_au);
				$("#taxrate_au").val(data.taxrate_au);
				
				$("#hscode_ca").val(data.hscode_ca);
				$("#taxrate_ca").val(data.taxrate_ca);
			}
			 
			
			 //$("#sp-orders").html(data.html);					 						
			//$('#message').delay(20000).fadeOut('slow');							
		}		
	});	
}	
</script>
