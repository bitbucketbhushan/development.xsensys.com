<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Add Barcode</h1>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12 col-lg-offset-0">
                             <div class="form-horizontal panel-body padding-bottom-40 padding-top-40">
								   
								   <div class="form-group">
										<label class="col-sm-2 control-label">Barcode</label>
										<div class="col-sm-6">                                               
											<?php
												print $this->form->input( 'ProductBarcode.barcode', array( 'type'=>'text','class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'placeholder' => 'Enter Barcode') );
											?>
										</div>
										<div class="col-sm-2">
										<a href="javascript:void(0);" aria-hidden="true" class="add_barcode btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center; width: 100px;">Add</a>
										</div>
										<div class="col-sm-2">
										<a href="/showAllProduct" aria-hidden="true" class="add_barcode btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center; width: 100px;">Back</a>
										</div>
									</div>
								   <div class="form-group">
								   		<div class="col-sm-12">                                               
											<table class="table table-bordered table-striped dataTable">
											<tr>
												<th bgcolor="#cccccc" style="border: 2px solid gray;" width="33%">SKU : <?php echo $getproductdetail['Product']['product_sku']; ?></th>
												<th bgcolor="#cccccc" style="border: 2px solid gray;" width="33%">TITLE : <?php echo $getproductdetail['Product']['product_name']; ?></th>
												<th bgcolor="#cccccc" style="border: 2px solid gray;" width="33%">GLOBAL BARCODE : <?php echo $getproductdetail['ProductDesc']['barcode']; ?></th>
											</tr>
										</div>
										<input type="hidden" class="marter_barcode" value="<?php echo $getproductdetail['ProductDesc']['barcode']; ?>">
									</div>
									
								   <div class="form-group">
								   		<div class="col-sm-12">                                               
											<table class="table table-bordered table-striped dataTable">
											<tr>
												<th width="33%">Barcode</th>
												<th width="33%">Added By</th>
												<th width="33%">Date</th>
											</tr>
											<?php foreach($getAllBarcodes as $getAllBarcode) { ?>
											<tr>
												<!--<td><?php echo $getAllBarcode['ProductBarcode']['global_barcode']; ?></td>-->
												<td><?php echo $getAllBarcode['ProductBarcode']['barcode']; ?></td>
												<td><?php echo $getAllBarcode['ProductBarcode']['username']; ?></td>
												<td><?php echo $getAllBarcode['ProductBarcode']['added_date']; ?></td>
											</tr>
											<?php } ?>
										</div>
									</div>
                                 <div class="form-horizontal panel-body" >
									<table class="sku_date col-sm-12" >
									</table>
							    </div>  
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>

<script>
$(function() {
  $("#ProductBarcodeBarcode").focus();
});

$('body').on('click','.add_barcode',function(){
	var barcode			=	$("#ProductBarcodeBarcode").val();
	var masterbarcode	=	$(".marter_barcode").val();
	if(barcode == '')
	{
		alert( 'Please enter the barcode.' );
		return false;
	}
	$.ajax({
				'url'            : getUrl() + '/savenewbarcode',
				'type'           : 'POST',
				'data'           : { barcode : barcode, masterbarcode : masterbarcode },
				'success' 		 : function( msgArray )
												{
													var json 		= 	JSON.parse(msgArray);
													if( json.status == 0 )
													{
														alert(json.msg);
													}
													if( json.status == 1 )
													{
														alert(json.msg);
														location.reload();
														
													}
													//location.reload();
												}  
			}); 
	
	});



</script>

