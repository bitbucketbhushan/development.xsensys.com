<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Product Country Of Origin</h1>
		<div class="panel-head"><?php print $this->Session->flash(); ?></div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel"> 
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2"  style="background-color:#f5f5f5; border:1px solid #CCCCCC; margin-top:10px;">
					<form name="uploadfrm" id="uploadfrm" action="<?php echo Router::url('/', true) ?>Products/uploadCountryOfOrigin" method="post" enctype="multipart/form-data" >
						<div class="form-horizontal panel-body padding-bottom-20 padding-top-20">
							<div class="form-group">
							<label class="col-sm-3 control-label">Upload File:</label>
							 <div class="col-sm-4"><input type="file"  class="form-control" name="coofile" ></div>
							 <div class="col-lg-3"><button type="submit" class="btn btn-info">Upload</button>(<a href="<?php echo Router::url('/', true) ?>img/country_of_origin.csv" target="_blank">Sample file</a>)</div>
							<div class="col-lg-2">
							<?php echo $this->Html->link('<i class="fa fa-download"></i> Export',['controller' => 'products','action' => 'exportlocation'],['class'=>'btn btn-info','escape' => false]);?>
                             </div>
                            </div>
						</div>
					</form>
					</div>
				</div>                                           
                    <div class="row"> 
                        <div class="col-lg-8 col-lg-offset-2">
                        <form name="productfrm" id="productfrm" action="" method="post">
								
								<div class="form-horizontal panel-body padding-bottom-40 padding-top-20">

									   <div class="form-group">
										<label class="col-sm-3 control-label">SKU</label> 
										<div class="col-sm-6"><input name="sku" class="form-control"  type="text" id="sku"></div>
										<div class="col-lg-2"><button type="button" class="btn btn-success btn-sm" onclick="Search()">Search</button></div>
										
									</div>
                                       
 									   <div class="form-group">
										<label class="col-sm-3 control-label">Country Of Origin</label> 
										<div class="col-sm-6"><input name="country_of_origin" class="form-control"  type="text" id="country_of_origin"></div>
										 
									</div>
									   <div class="form-group">                                                                           
  									    <div class="col-lg-3">&nbsp;</div>
										<div class="col-lg-3"><button type="button" class="btn btn-info" onclick="saveValues()">Save</button></div>
                                       </div>
                                   
                                     
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>

<script> 

function saveValues()
{
	var formData = new FormData($('#productfrm')[0]);

		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Products/SaveCountryOfOrigin/',
		type: "POST",
		async: false,		
		cache: false,
		contentType: false,
		processData: false,
		data : formData,
		success: function(data, textStatus, jqXHR)
		{		
			alert(data.msg);
			 //$("#sp-orders").html(data.html);					 						
			//$('#message').delay(20000).fadeOut('slow');							
		}		
	});	
} 

function Search()
{
	    $.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Products/SearchCountryOfOrigin/',
		type: "POST",		 
		data :{ sku:$("#sku").val()},
		success: function(data, textStatus, jqXHR)
		{	 
			$("#country_of_origin").val(''); 
			if(data.country_of_origin){ 
				$("#country_of_origin").val(data.country_of_origin); 
			} 
			if(data.msg){ 
				alert(data.msg);
			}
			
			 //$("#sp-orders").html(data.html);					 						
			//$('#message').delay(20000).fadeOut('slow');							
		}		
	});	
}	
</script>
