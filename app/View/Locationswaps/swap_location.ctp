<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Swap Location</h1>
		<div class="submenu">
			<div class="navbar-header">
				<ul class="nav navbar-nav pull-left">
					<li>
						<a class="btn btn-success btn-xs margin-right-10 color-white" href="" >Quantity Swap</a>
					</li>
					<li>
						<a class="btn btn-success btn-xs margin-right-10 color-white" href="/Locationswaps/swapLocation" >Location Swap</a>
					</li>
				</ul>
			</div>
		</div>
    </div>
    <div class="container-fluid">
        <div class="row">
		<div class="col-lg-6">
		  <div class="panel">                            
				<div class="row">
					<div class="col-lg-12 col-lg-offset-0">
						 <div class="form-horizontal panel-body padding-bottom-40 padding-top-40">
							   <div class="form-group">
									<label class="col-sm-3 control-label">Product Sku</label>
									<div class="col-sm-8">                                               
										<?php
											print $this->form->input( 'Product.Sku', array( 'type'=>'text','class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false,'Placeholder' => 'Please Enter Product Sku' ) );
										?>
									</div>
								</div>
							 <div class="form-horizontal panel-body" >
								<table class="sku_data col-sm-12" >
								</table>
							</div>  
						 </div>
					</div>
				</div>
			</div>
		 </div>
			
		 <div class="col-lg-6">
			<div class="panel">                            
				<div class="row">
					<div class="col-lg-12 col-lg-offset-0">
						<div class="form-horizontal panel-body padding-bottom-40 padding-top-40">
						   <div class="form-group">
							<label class="col-sm-3 control-label">Location</label>
								<div class="col-sm-8">                                               
									<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose locations" name="bin_location" id="bin_location"  > 
									  <?php foreach($getalllocations as $bin){	
											 echo '<option data-tokens="'.$bin.'" value="'.$bin.'">'.$bin.'</option>';
									  } ?>							
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>        
      </div>
   </div>        
</div>

<script>
	
$(document).keypress(function(e)
	{
		if(e.which == 13) {
			
			getSkuLocationValue();
		}
	});


	function getSkuLocationValue()
	{
		var sku  = $('#ProductSku').val();
		var str ='';
		$.ajax({
						url     	: '/Locationswaps/getskulocation',
						type    	: 'POST',
						data    	: { sku : sku },  			  
						success 	:	function( msgArray  )
						{
							var json 		= 	JSON.parse(msgArray);
							var getStatus 	= 	json.status;
							var getData 	= 	json.data;
							str	+=	'<tr style="font-weight: bold; border-bottom: 2px solid #ccc;"><td width=33%;>Location</td><td style="width: 50%;">Barcode</td><td style="width: 20%;">Stock</td></tr>';
							var total = 0;
							var locationlist = '<option value="">Choose Location</option>';
							for (var i = 0, max = getData.length; i < max; i++)
								{
									var barcode			=	getData[i].BinLocation.barcode;
									var location		=	getData[i].BinLocation.bin_location;
									var locationstock	=	getData[i].BinLocation.stock_by_location;
									var timestamp		=	getData[i].BinLocation.timestamp;
									str +='<tr style="border-bottom: 2px solid #ccc;">';
									str +='<td id="loc_'+i+'">'+location+'</td>';
									str +='<td id="barco_'+i+'">'+barcode+'</td>';
									str +='<td id="locationsto_'+i+'">'+locationstock+'</td>';
									str +='</tr>';
									total = parseInt(total) + parseInt(locationstock);
								}
									str +='<tr style="border-bottom: 2px solid #ccc;">';
									str +='<td style="width: 25%;"><b>Total Quantity</b></td>';
									str +='<td style="width: 25%;"></td>';
									str +='<td style="width: 25%;"><b>'+total+'</b></td>';
									str +='<td style="width: 25%;"></td>';
									str +='</tr>';
									$('.sku_data').html('');
									$('.sku_data').append( str );
						}                
				});	
	}

</script>
