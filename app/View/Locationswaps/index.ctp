<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Swap Location Quantity</h1>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12 col-lg-offset-0">
                             <div class="form-horizontal panel-body padding-bottom-40 padding-top-40">
								   <div class="form-group">
										<label class="col-sm-3 control-label">Product Sku</label>
										<div class="col-sm-8">                                               
											<?php
												print $this->form->input( 'Product.Sku', array( 'type'=>'text','class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false,'Placeholder' => 'Please Enter Product Sku' ) );
											?>
										</div>
									</div>
                                 <div class="form-horizontal panel-body" >
									<table class="sku_data col-sm-12" >
									</table>
							    </div>  
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>

<script>
	
$(document).keypress(function(e)
	{
		if(e.which == 13) {
			
			getSkuLocationValue();
		}
	});


	function getSkuLocationValue()
	{
		var sku  = $('#ProductSku').val();
		var str ='';
		$.ajax({
						url     	: '/Locationswaps/getskulocation',
						type    	: 'POST',
						data    	: { sku : sku },  			  
						success 	:	function( msgArray  )
						{
							var json 		= 	JSON.parse(msgArray);
							var getStatus 	= 	json.status;
							var getData 	= 	json.data;
							str	+=	'<tr style="font-weight: bold; border-bottom: 2px solid #ccc;"><td width=15%;>Location</td><td style="width: 15%;">Barcode</td><td style="width: 5%;">Stock</td><td style="width: 20%;">Location</td><td style="width: 15%;">Swap Qty</td><td style="width: 10%;">Action</td></tr>';
							var total = 0;
							var locationlist = '<option value="">Choose Location</option>';
							for (var i = 0, max = getData.length; i < max; i++)
								{
									var barcode			=	getData[i].BinLocation.barcode;
									var location		=	getData[i].BinLocation.bin_location;
									var locationstock	=	getData[i].BinLocation.stock_by_location;
									var timestamp		=	getData[i].BinLocation.timestamp;
									str +='<tr style="border-bottom: 2px solid #ccc;">';
									str +='<td id="loc_'+i+'">'+location+'</td>';
									str +='<td id="barco_'+i+'">'+barcode+'</td>';
									str +='<td id="locationsto_'+i+'">'+locationstock+'</td>';
									str +='<td><select class='+location+' id="selelo_'+i+'" >';
									str += '<option value='+i+'></option>';
									for (var j = 0, max = getData.length; j < max; j++)
										{
											if(getData[i].BinLocation.bin_location != getData[j].BinLocation.bin_location) {
												str += '<option value='+i+'>'+getData[j].BinLocation.bin_location+'</option>';
											}
										}
									str +='</select>';
									str +='</td>';
									str +='<td><input type="text" id="swapqtyval_'+i+'" class="countQty" style="width: 40px;" value="" /></td>';
									str +='<td><button class="savevalue btn bg-green-500 color-white btn-dark" onclick="saveswapvalue('+i+')"for="'+i+'" ><span class="glyphicon glyphicon-plus-sign"></span></button></td>';
									str +='</tr>';
									total = parseInt(total) + parseInt(locationstock);
								}
									str +='<tr style="border-bottom: 2px solid #ccc;">';
									str +='<td style="width: 25%;"><b>Total Quantity</b></td>';
									str +='<td style="width: 25%;"></td>';
									str +='<td style="width: 25%;"><b>'+total+'</b></td>';
									str +='<td style="width: 25%;"></td>';
									str +='</tr>';
									$('.sku_data').html('');
									$('.sku_data').append( str );
						}                
				});	
	}
	function saveswapvalue( id )
	{
		if( $('#selelo_'+id).find(":selected").text() == '' )
		{
			swal("Please select location");
			return false;
		}
		else if( parseInt($('#swapqtyval_'+id).val()) > parseInt($('#locationsto_'+id).text()) )
		{
			swal("Swap value should be less then or equal to location value");
			return false;
		}
		
		var str ='';
		$.ajax({
						url     	: '/Locationswaps/swaplocationqty',
						type    	: 'POST',
						data    	: { location : $('#loc_'+id).text(), barcode : $('#barco_'+id).text(), locationstock : $('#locationsto_'+id).text(), selectedloc : $('#selelo_'+id).find(":selected").text(), swapqty : $('#swapqtyval_'+id).val() },
						success 	:	function( msgArray  )
						{
							var json 		= 	JSON.parse(msgArray);
							var getStatus 	= 	json.status;
							var getData 	= 	json.data;
								if( getStatus == 1 )
								{
									str	+=	'<tr style="font-weight: bold; border-bottom: 2px solid #ccc;"><td width=15%;>Location</td><td style="width: 15%;">Barcode</td><td style="width: 5%;">Stock</td><td style="width: 20%;">Location</td><td style="width: 15%;">Swap Qty</td><td style="width: 10%;">Action</td></tr>';
									var total = 0;
									var locationlist = '<option value="">Choose Location</option>';
									for (var i = 0, max = getData.length; i < max; i++)
										{
											var barcode			=	getData[i].BinLocation.barcode;
											var location		=	getData[i].BinLocation.bin_location;
											var locationstock	=	getData[i].BinLocation.stock_by_location;
											var timestamp		=	getData[i].BinLocation.timestamp;
											str +='<tr style="border-bottom: 2px solid #ccc;">';
											str +='<td id="loc_'+i+'">'+location+'</td>';
											str +='<td id="barco_'+i+'">'+barcode+'</td>';
											str +='<td id="locationsto_'+i+'">'+locationstock+'</td>';
											str +='<td><select class='+location+' id="selelo_'+i+'" >';
											str += '<option value='+i+'></option>';
											for (var j = 0, max = getData.length; j < max; j++)
												{
													if(getData[i].BinLocation.bin_location != getData[j].BinLocation.bin_location) {
														str += '<option value='+i+'>'+getData[j].BinLocation.bin_location+'</option>';
													}
												}
											str +='</select>';
											str +='</td>';
											str +='<td><input type="text" id="swapqtyval_'+i+'" class="countQty" style="width: 40px;" value="" /></td>';
											str +='<td><button class="savevalue btn bg-green-500 color-white btn-dark" onclick="saveswapvalue('+i+')"for="'+i+'" ><span class="glyphicon glyphicon-plus-sign"></span></button></td>';
											str +='</tr>';
											total = parseInt(total) + parseInt(locationstock);
										}
										str +='<tr style="border-bottom: 2px solid #ccc;">';
										str +='<td style="width: 25%;"><b>Total Quantity</b></td>';
										str +='<td style="width: 25%;"></td>';
										str +='<td style="width: 25%;"><b>'+total+'</b></td>';
										str +='<td style="width: 25%;"></td>';
										str +='</tr>';
										$('.sku_data').html('');
										$('.sku_data').append( str );
								} else {
									swal('Location value should be greater the or equal to swap value.')
								}
						}			
				})
		
	}
	
	
	

</script>
