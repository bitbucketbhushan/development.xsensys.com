<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-top:1px solid #666666; padding:0px;} 
	.body div.sku div{padding:4px 3px; font-size:12px;} 
	/*.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.pl-2{padding-left:4px;}
	.fa-spin-custom, .glyphicon-spin {
    -webkit-animation: spin 1000ms infinite linear;
    animation: spin 1000ms infinite linear;
}@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
@keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
	</style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title">Channel Management</h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?>
					
				</div>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">	 
						</div>	
						
								
							
					<div class="panel-body no-padding-top bg-white">
 						<form method="get" name="search" id="searchfrm" action="<?php echo Router::url('/', true) ?>Channels">				 		
							<div class="col-lg-3" style="margin-left:-15px">
								<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Enter Search Keyword..." name="searchkey" class="form-control searchString" />
							</div>
							
							<div class="col-lg-2"> 
								<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
							</div>
						</form>
						
						<div  class="col-lg-2">
 								<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus"></i>&nbsp;ADD CHANNEL</button></div>
						<div class="panel-tools" style="margin-bottom: 10px;clear: right;text-align: center; float: right;">
							<ul class="pagination">
							<?php
							   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
							   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							?>
							</ul>
						</div> 
 						<div id="myModal" class="modal fade" role="dialog">
					  <div class="modal-dialog modal-lg">
					
						<!-- Modal content-->
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Add Channel</h4>
						  </div>
						  <div class="modal-body">
						  
 							<div class="form-group row">
								<label for="source" class="col-sm-3 col-form-label">Source</label>
								<div class="col-sm-7">
										 <select class="form-control" name="source" id="source">
											<option value="amazon">Amazon</option>
											<option value="ebay">eBay</option>
											<option value="realde">Real DE</option>
											<option value="CDISCOUNT">CDISCOUNT</option>
											 <option value="Onbuy">Onbuy</option>
										</select>
								</div>
							</div>						  
							
							<div class="form-group row">
								<label for="channel_type" class="col-sm-3 col-form-label">Channel Type</label>
								<div class="col-sm-7">
										<select class="form-control" name="channel_type" id="channel_type">
											<option value="fba">FBA</option>
											<option value="merchant">Merchant</option>
										</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="channel_title" class="col-sm-3 col-form-label">Channel Title(Name)</label>
								<div class="col-sm-7">
									<input name="channel_title" title="channel_title" placeholder="Enter channel title" type="text" class="form-control txt">
								</div>
							</div>
							
						<div class="form-group row">
						  <label for="country"  class="col-sm-3 col-form-label">Country </label>
						  <div class="col-sm-7"> <select class="form-control" id="country" name="country">
								<option selected="" value="GB">GB:United Kingdom</option>
									<option value="DE">DE:Germany</option>
									<option value="ES">ES:Spain</option>
									<option value="FR">FR:France</option>
									<option value="IT">IT:Italy</option>
									<option value="US">US:United States</option>
									<option value="CA">CA:Canada</option>
									<option value="AU">AU:Australia</option>
									<option value="SE">SE:Sweden</option>
									<option value="NL">NL:Netherlands</option>
									<option value="PL">PL:Poland</option>
 								</select>
					  	 </div>
					   </div>
					  
							<div class="form-group row">
								<label for="channel_sku" class="col-sm-3 col-form-label">Currency</label>
								<div class="col-sm-7">
 							   <select class="form-control" id="currency" name="currency">
									<option value="GBP">GBP</option>
									<option value="EUR">EUR</option>
									<option value="USD">USD</option>
									<option value="CAD">CAD</option>
									<option value="AUD">AUD</option>
									<option value="SEK">SEK</option>
								</select>
					  
								</div>
							</div>
							
							<div class="form-group row">
								<label for="weight_unit" class="col-sm-3 col-form-label">Weight Unit</label>
								<div class="col-sm-7">
 								   <select class="form-control" id="weight_unit" name="weight_unit">
										<option value="GRAMS">GRAMS</option>
										<option value="KG">Kilogram(KG)</option>
										<option value="POUND">POUND</option>
										</select>
								</div>
							</div>
  
							<div class="form-group row">
								<label for="dim_unit" class="col-sm-3 col-form-label">Dimension Unit</label>
								<div class="col-sm-7">
									<select class="form-control" id="dim_unit" name="dim_unit">
									<option value="MM">Millimeter(MM)</option>
									<option value="CM">Centimeter(CM)</option>
									<option value="FT">Foot(FT)</option>
									<option value="MT">Meter(MT)</option>
								</select>
							</div>
							</div>
							<div class="form-group row">
								<label for="prefix" class="col-sm-3 col-form-label">Prefix</label>
								<div class="col-sm-7">
									<input name="prefix" id="prefix" placeholder="Enter Prefix" type="text" class="form-control txt"></div>
							</div>
							<div class="form-group row">
								<label for="profit_margin" class="col-sm-3 col-form-label">Profit Margin(%)</label>
								<div class="col-sm-7">
									<input id="profit_margin" name="profit_margin" title="Profit Margin(%)" placeholder="Enter Profit Margin(%)" type="text" class="form-control txt">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="min_margin" class="col-sm-3 col-form-label">Min Margin</label>
								<div class="col-sm-7">
									<input name="min_margin" title="Min Margin" placeholder="Enter Min Margin" type="text" class="form-control txt"></div>
							</div>
							
							<div class="form-group row">
								<label for="max_margin" class="col-sm-3 col-form-label">Max Margin</label>
								<div class="col-sm-7">
									<input name="max_margin" title="Max Margin" placeholder="Enter Max Margin" type="text" class="form-control txt"></div>
							</div>
							<div class="form-group row">
								<label for="margin_diff" class="col-sm-3 col-form-label">Margin Diff</label>
								<div class="col-sm-7">
									<input name="margin_diff" title="Margin Diff" placeholder="Enter Margin Diff" type="text" class="form-control txt"></div>
							</div>
							 
							 
							 <div class="form-group row">
								<label for="increment_id" class="col-sm-3 col-form-label">SKU Increment ID(ex-1001) :</label>
								<div class="col-sm-7">
									<input name="increment_id" placeholder="Enter SKU Increment ID(ex-1001)" type="text" class="form-control txt"></div>
							</div>
							<div class="form-group row">
								<label for="extra_referral_fee" class="col-sm-3 col-form-label">Extra Referral fee</label>
								<div class="col-sm-7">
									<input name="extra_referral_fee" placeholder="Enter Extra Referral fee" type="text" class="form-control txt"></div>
							</div>
							<div class="form-group row">
								<label for="note" class="col-sm-3 col-form-label">Note</label>
								<div class="col-sm-7">
									<input name="note" id="note" placeholder="Enter Note" type="text" class="form-control txt"></div>
							</div>
							<div class="form-group row">
								<label for="channel_code" class="col-sm-3 col-form-label">AppEagle Code</label>
								<div class="col-sm-7">
									<input name="channel_code" id="channel_code" placeholder="Enter AppEagle Code" type="text" class="form-control txt"></div>
							</div>
							
							<div class="form-group row">
								<label for="margin" class="col-sm-2 col-form-label">&nbsp;</label>
								<div class="col-sm-2"><button type="button" class="btn btn-warning" onclick="saveData();"><i class="glyphicon glyphicon-check"></i>&nbsp;Save</button></div>
								<div class="col-sm-7 save-msg"></div>
							</div>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						  </div>
						</div>
					
					  </div>
					</div>
					
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-11">Channel (SubSource)</div>		
								<div class="col-5">Country</div>		
 								<div class="col-5">Channel Type</div>	
								<div class="col-7">AppEagle Code</div>	
								<div class="col-8">Note</div>	
								<div class="col-5">Currency</div>
								<div class="col-5">Weight Unit</div>
								<div class="col-5">Dim Unit</div>
								<div class="col-7">Prefix</div>
								<div class="col-6">SKU Increment ID(ex-1001)</div>
								<div class="col-5">Profit Margin(%)</div>
								<div class="col-5">Extra Referral fee(%)</div>
								<div class="col-5">Min Margin</div>
								<div class="col-5">Max Margin </div>								
								<div class="col-5">Margin Diff(%) (This is used for SKU margin)</div>	
								<div class="col-5">Status</div>
								<div class="col-5">Action</div>							
 							</div>								
						</div>	
							
						<div class="body" id="results">
						<div id="p_1"></div>
						<?php if(count($channels) > 0){?>
							<?php foreach( $channels as $channel ) { ?> 
						<div class="row sku"> 
							<div class="col-lg-12">
								<div class="col-11"><span title="Channel Title"><?php echo trim($channel['ProChannel']['channel_title']); ?></span></div> 
								<div class="col-5"><span title="country"><?php echo trim($channel['ProChannel']['country']); ?></span></div>	
								<div class="col-5"><section><span class="xedit channel_type" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="channel_type" data-title="Channel Type" data-type="select" data-value="<?php echo $channel['ProChannel']['channel_type']; ?>"></span></section></div>
								<div class="col-7"><?php echo trim($channel['ProChannel']['channel_code']); ?></div>
									
 								<div class="col-8"><section><span class="xedit" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="note" data-title="note" data-value="<?php echo $channel['ProChannel']['note']; ?>"><?php echo $channel['ProChannel']['note']; ?></span></section></div>		
								
								<div class="col-5"><section><span class="xedit currency" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="currency" data-title="currency" data-type="select" data-value="<?php echo $channel['ProChannel']['currency']; ?>"><?php echo $channel['ProChannel']['currency']; ?></span></section></div>
								<div class="col-5"><section><span class="xedit weight_unit" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="weight_unit" data-title="weight_unit" data-type="select" data-value="<?php echo $channel['ProChannel']['weight_unit']; ?>"><?php echo $channel['ProChannel']['weight_unit']; ?></span></section></div>
								<div class="col-5"><section><span class="xedit dim_unit" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="dim_unit" data-title="dim_unit" data-type="select" data-value="<?php echo $channel['ProChannel']['dim_unit']; ?>"><?php echo $channel['ProChannel']['dim_unit']; ?></span></section></div>
 								<div class="col-7"><section><span class="xedit" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="prefix" data-title="prefix"  data-value="<?php echo $channel['ProChannel']['prefix']; ?>"><?php echo $channel['ProChannel']['prefix']; ?></span></section> </div> 								
 								
								<div class="col-6"><section><span class="xedit" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="increment_id" data-title="increment_id" data-value="<?php echo $channel['ProChannel']['increment_id']; ?>"><?php echo trim($channel['ProChannel']['increment_id']); ?></span></section></div>
								
								<div class="col-5"><section><span class="xedit" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="profit_margin" data-title="profit_margin" data-value="<?php echo $channel['ProChannel']['profit_margin']; ?>"><?php echo trim($channel['ProChannel']['profit_margin']); ?></span></section></div>
								
								<div class="col-5"><section><span class="xedit" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="extra_referral_fee" data-title="extra_referral_fee" data-value="<?php echo $channel['ProChannel']['extra_referral_fee']; ?>"><?php echo trim($channel['ProChannel']['extra_referral_fee']); ?></span></section></div>
  								<div class="col-5"><section><span class="xedit" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="min_margin" data-title="min_margin" data-value="<?php echo $channel['ProChannel']['min_margin']; ?>"><?php echo trim($channel['ProChannel']['min_margin']); ?></span></section></div>
								
								<div class="col-5"><section><span class="xedit" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="max_margin " data-title="max_margin" data-value="<?php echo $channel['ProChannel']['max_margin']; ?>"><?php echo $channel['ProChannel']['max_margin']; ?></span></section></div>
								
								<div class="col-5"><section><span class="xedit" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="margin_diff" data-title="margin_diff"  data-value="<?php echo $channel['ProChannel']['margin_diff']; ?>"><?php echo $channel['ProChannel']['margin_diff']; ?></span></section></div>
								
								<div class="col-5"><section><span class="xedit status" pk="<?php echo $channel['ProChannel']['ch_id']; ?>" data-name="status" data-title="status" data-type="select" data-value="<?php echo $channel['ProChannel']['status']; ?>"><?php echo $channel['ProChannel']['status']; ?></span></section></div>
								
								
								<div class="col-5"><a href="<?php echo Router::url('/', true) ?>Channels/Delete/<?php echo $channel['ProChannel']['ch_id']; ?>" type="button" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete this?')">Delete</a>
</div>	
							</div>
							 						
						</div>
						
						<?php }?>
						<?php }else{?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php } ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
						
			</div>			
		
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>
 
<script>	
 jQuery('.status').editable({ 
		value:"active",
		source:[{value:"active", text:"active"},
			{value:"inactive", text:"inactive"} 
		],
		validate: function(value) {if($.trim(value) == "") return "This field is required"; } }
);
$(".currency").editable({inputclass:"",
		value:"GBP",
		source:[{value:"GBP", text:"GBP"},
			{value:"EUR", text:"EUR"},
			{value:"USD", text:"USD"},
			{value:"CAD", text:"CAD"},
			{value:"AUD", text:"AUD"},
			{value:"SEK", text:"SEK"}
		],
		validate: function(value) {if($.trim(value) == "") return "This field is required"; } }
);
				
jQuery('.weight_unit').editable({ 
		value:"GRAMS",
		source:[{value:"GRAMS", text:"GBP"},
			{value:"KG", text:"KG"},
			{value:"POUND", text:"POUND"}  
 		],
		validate: function(value) {if($.trim(value) == "") return "This field is required"; } }
);
jQuery('.dim_unit').editable({ 
		value:"MM",
		source:[{value:"MM", text:"Millimeter(MM)"},
			{value:"CM", text:"Centimeter(CM)"},
			{value:"FT", text:"Foot(FT)"} ,
			{value:"MT", text:"Meter(MT) "} 
 		],
		validate: function(value) {if($.trim(value) == "") return "This field is required"; } }
);
jQuery('.source').editable({ 
		value:"amazon",
		source:[{value:"amazon", text:"Amazon"},
			{value:"ebay", text:"eBay"},
			{value:"realde", text:"Real DE"},
			{value:"CDISCOUNT", text:"CDISCOUNT"},
			{value:"Onbuy", text:"Onbuy"}
		],
		validate: function(value) {if($.trim(value) == "") return "This field is required"; } }
);
 
jQuery('.channel_type').editable({ 
		value:"Merchant",
		source:[{value:"fba", text:"FBA"},
			{value:"merchant", text:"Merchant"} 
 		],
		validate: function(value) {if($.trim(value) == "") return "This field is required"; } }
);
jQuery(document).ready(function() {  
       // $.fn.editable.defaults.mode = 'popup';
        //jQuery('.xedit').editable();		
		$(document).on('click','.editable-submit',function(){
			var x = $(this).closest('section').children('span').attr('pk');			
			var y = $('.input-sm').val();
			var z = $(this).closest('section').children('span'); 
			var fname = $(this).closest('section').children('span').attr('data-name');
						
			$.ajax({
				url: "<?php echo Router::url('/', true) ?>Channels/Update",
				type: "POST",
				data : {value:y,pk:x,name:fname},
				success: function(s){
					if(s == 'status'){
					$(z).html(y);}
					if(s == 'error') {
					alert('Error Processing your Request!');}
				},
				error: function(e){
					alert('Error Processing your Request!!');
				}
			});
		});
		
		
});

jQuery('.xedit').editable();

$(".filter").click(function(){
 	window.location.href = '<?php echo Router::url('/', true) ?>Skumappings/maps?ch='+$("#ch option:selected").val();
 });


$( ".searchString" ).on( "keydown", function(event) {
	
	 if(event.which == 13) {
	 	if($(".searchString").val()){
			 ajaxSearch();
		 }else{
			 alert('Enter Search Keyword.');
			 event.preventDefault();
		 }
		 event.preventDefault();
	}
	
	 
	
});

$(".searchKey").click(function(){	
	if($(".searchString").val()){
		ajaxSearch();
	}else{
		alert('Enter Search Keyword.');
		event.preventDefault();
	}
		 
});

 function delCh(key){
 	$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Skumappings/Delete',
			type: "POST",				
			cache: false,			
			data : {id:key},
			success: function(data, textStatus, jqXHR)
			{	//alert(data.data);	
				$(".outerOpac").hide();		
				jQuery(".msg").html(data.msg);
				jQuery("#r_"+key).remove();	
				//$('#message').delay(20000).fadeOut('slow');							
		}		
	 });
 }
 
 

function saveData() {	
	
	if($("#sub_source option:selected").val() == 'all' ){
		alert("Please select sub source!");
		$( "#sub_source" ).focus();
		document.getElementById("sub_source").focus();
		return false;
	}else{
		var inputValues = [];
		jQuery('.modal-body .txt').each(function() {           
			inputValues.push($(this).attr('name')+'='+$(this).val());
		})	
 		jQuery.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>Channels/SavesChannel',
				type: "POST",
				data : {values:inputValues,source:$("#source option:selected").val(),channel_type:$("#channel_type option:selected").val(),country:$("#country option:selected").val(),currency:$("#currency option:selected").val(),weight_unit:$("#weight_unit option:selected").val(),dim_unit:$("#dim_unit option:selected").val()},
				success: function(data, textStatus, jqXHR)
				{		
					jQuery(".save-msg").show();				
 					jQuery(".save-msg").html(data.msg);
					 
				}		
			});
		}
	
}
</script>
