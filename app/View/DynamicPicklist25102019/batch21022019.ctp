<?php
$userID = $this->session->read('Auth.User.id');
$users_array = array('avadhesh.kumar@jijgroup.com','amit.gaur@jijgroup.com','mmarecky82@gmail.com','shashi.b.kumar@jijgroup.com','jake.shaw@euracogroup.co.uk','lalit.prasad@jijgroup.com','aakash.kushwah@jijgroup.com');
App::import('Controller', 'DynamicPicklist');
$dyna_pick 	=	new DynamicPicklistController;
?>
<style>
.selectpicker {border:1px solid #c6c6c6;}
.selectpicker:hover  {border:1px solid #666666;}
.completed{background-color:#c3f6c3 !important;}
.preview h3{color:#666;}
.preview th, .preview td{padding:5px; color:#424242;}
.btn-pad{margin-bottom:4px;}
.btn-pad-top{margin-top:4px;}
center{color:#FF0000;}
.c_total{color:#17C225;}
.c_count{ border-bottom: 1px dotted grey; }
</style> 
<div class="bottom_panel rightside bg-grey-100">
    <div class="container-fluid" >
       		
             <div class="panel">
			 <div class="panel-body">
			 <div class="col-lg-12"><?php print $this->Session->flash(); ?></div>  
				<div class="row">	<?php //pr($o_data); ?>
						
							<?php $total = 0; foreach( $countries as $countrie ) { 
								  $total = $total + $countrie['o_count'];
								  }
							?>
						 <div class="col-lg-3">
							<div class="panel bg-blue-400">
									<div class="panel-body padding-15-20">
										<div class="clearfix">
											<div class="pull-left">
												<div class="display-block color-blue-50 font-weight-600 font-size-13">Total Available Open Orders :</div>
												<div class="color-white font-size-14 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="343" data-speed="500" data-refresh-interval="10">
													Open Orders:<?php echo $o_data['op_ord_count'] ?><br />
													Orders after spilt:<?php echo $o_data['reanige_ord_count'] ?>
												</div>
												
											</div>
											 
										</div>
									</div>
								</div>
						</div>	
						<div class="col-lg-3">
								<div class="panel bg-teal-400">
									<div class="panel-body padding-15-20">
										<div class="clearfix">
											<div class="pull-left">
												<div class="display-block color-blue-50 font-weight-600 font-size-13">Orders after split available for new batch:</div>
												<div class="color-white font-size-20 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="343" data-speed="500" data-refresh-interval="10">													 
													<?php echo $o_data['readyforbatch'] ?>
												</div>
												
											</div>											 
										</div>
									</div>
								</div>							
							 
						</div>
						<div class="col-lg-4">
						 	<div class="col-md-6"><input type="text" class="form-control" id="fill_order" value="<?php echo $o_data['readyforbatch']; ?>"></div>
							<div class="col-md-6"> <button type="button"  class="btn btn-info create_batch" id="create_batch" >Create Batch</button></div>
						</div>
					</div>
				 </div>
			</div>
			</div>
			
			<div class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12 ">
						<table class="table table-bordered table-striped dataTable">
							<tr>
 								<th>Batch Name</th>
								<th>Country</th> 
								<th>Service</th>
								<th>Total Orders</th>
								<!--<th>Batch Status</th>-->
								<th>Creater Name</th>	
								<th>Creation Date</th>
								<th>Action</th>
							</tr>
 							<?php 
							App::import('Controller', 'DynamicPicklist');
							$dyna_pick 	=	new DynamicPicklistController;
							$t_b_o = 0;
							foreach($b_name_arr as $val) { ?>
							<tr <?php if($val['status'] == 2){?> class="completed"<?php }?>>
								<td><?php  echo $val['batch_name']; ?></td>
								<td><?php foreach( $val['country'] as $v ){ 
										echo '<div><b>'.$v['country'].'[ '.$v['o_count'].' ]</b></div>';
									} ?>
								</td>
								<td>
									<?php foreach( $val['service'] as $v ){ 
										if($v['service_provider'] != null){
											echo '<div><b>'.$v['service_provider'].'[ '.$v['s_count'].' ]</b></div>';
										} else {
											echo '<div><b>No Service'.'[ '.$v['s_count'].' ]</b></div>';
										}
									} ?>
								</td>
								<td><?php 
								
											$batchOrders = $dyna_pick->getBatchOrderStatus( $val['batch_name'] );
										 	echo '<div class="to_pro_'.$val['batch_name'].'" for="'.count($batchOrders).'"><strong>Total Batch Orders: '.count($batchOrders).'</strong></div>';
										 	echo "----------------------------------------<br>";
										 
											$locked  = [];
											$canceled = [];											 
											$open = 0;
											$processed = 0;
											$sorted = 0;
											$avilable_for_picklist = 0;
											foreach($batchOrders as $s){
												if($s['MergeUpdate']['status'] == 3){
													$locked[] = $s['MergeUpdate']['product_order_id_identify'];
												}else if($s['MergeUpdate']['status'] == 2){
													$canceled[] = $s['MergeUpdate']['product_order_id_identify'];
												}else if($s['MergeUpdate']['status'] == 1){
													$processed++;
												}else if($s['MergeUpdate']['status'] == 0){
													$open++;
												}
												if($s['MergeUpdate']['status'] == 0 && $s['MergeUpdate']['pick_list_status'] == 0){
													$avilable_for_picklist++;
												}											   
												if($s['MergeUpdate']['scan_date'] != ''){
													$sorted++;
												}
											}
											if($processed > 0){
												echo "<div style='color:#990066' class='pro_".$val['batch_name']."' for='".$processed."'><strong>Processed Orders: ".$processed."</strong></div>";
												echo "----------------------------------------<br>";
											}
										
										
										
											if($open > 0){
												echo "<div style='color:#006600'><strong>Open Orders: ". $open."</strong></div>";
												echo "----------------------------------------<br>";
											}
																			 
											if($sorted > 0){
												echo "<div style='color:#BF1705'><strong>Sorted Orders: ". $sorted."</strong></div>";
												echo "----------------------------------------<br>";
											}
											
 											
											echo '<div style="color:#006600"><strong>Avilable For Picklist : '.$avilable_for_picklist.'</strong></div>';
											echo "----------------------------------------<br>";
											
											
											if(count($locked) > 0){												
												echo "<div style='color:#000099'><strong>Locked Orders: ".count($locked)."</strong></div>";
												echo "----------------------------------------<br>";
 											}
											
 											if(count($canceled) > 0 ){												 
												echo "<div style='color:#FF0000' class='can_".$val['batch_name']."' for='".count($canceled)."'><strong>Canceled Orders: ".count($canceled)."</strong></div>"; 
											}
											
								
											//$qty			=	$dyna_pick->getOrderStatusQty( $val['batch_name'] );
											//$r_o_qty		=	$dyna_pick->getRemaningOrderQty( $val['batch_name'] );
								 		//	echo '<b>Open Orders:  </b>'.$qty['open_order']."<br>";
											//echo '<div style="color:#006600" class="pro_'.$val['batch_name'].'" for="'.$qty['proc_order'].'"><strong>Processed Orders : '.$qty['proc_order'].'</strong></div>';
											//echo '<b>Processed Orders:  </b>'.$qty['proc_order']."<br>";
											//echo '<b>Canceled Orders:  </b>'.$qty['canc_order']."<br>";
											//echo '<div style="color:#FF0000" class="can_'.$val['batch_name'].'" for="'.$qty['canc_order'].'" ><strong>Canceled Orders : '.$qty['canc_order'].'</strong></div>';
											//echo '<div style="color:#000099"><strong>Locked Orders : '.$qty['lock_order'].'</strong></div>';
											//echo '<div style="color:#000099" ><strong>For Picklist : '.$qty['remaning_order'].'</strong></div>';
											//echo '<b>Locked Orders:  </b>'.$qty['lock_order']."<br>";//color:#FF0000
									?>
								</td>
								<?php /*?><td><?php  
										//$bat_ststus		=	$dyna_pick->getbatchQty( $val['batch_name'] ); 
										echo '<div style="color:#006600" class="to_pro_'.count($batchOrders).'" for="'.count($batchOrders).'"><strong>Order to Process : '.count($batchOrders).'</strong></div>';
										//echo '<b>Order to Process:  </b>'.$bat_ststus['b_orders']."<br>";
										//echo '<b>Processed Orders:  </b>'.$bat_ststus['p_b_orders']."<br>";
									?>
								</td><?php */?>
								<td><?php  echo $val['creater_name'];?></td>
								<td><?php  echo $val['created_date'];?></td>
								<td><center><a href="<?php echo Router::url('/', true). 'DynamicPicklist/index/'.$val['batch_name']; ?>" class="btn btn-info">Go To Picklist</a></center>
								<br /> 
								<center>
								<?php if($val['status'] < 2){?>
								<a href="javascript:void(0);"  for="<?php echo $val['batch_name']; ?>" class="mark_complete btn btn-success">Mark Completed</a>
								<?php }else{?>
								<div style="color:#006600"><strong>Completed</strong></div>
								<?php }?>
								</center>
								</td>
							</tr>
							<input type="hidden" name="batchname" value="<?php echo $val['status'].'####'.$val['batch_name']; ?>" class="batchname" />
							<?php 
							
							$t_b_o = $t_b_o + $val['order_count'];
							} 
							?> 								
						</table>
						<input type="hidden" id="total" name="total" value="<?php echo $t_b_o; ?>" />
							 
						<ul class="pagination">
							  <?php
  								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>
				</div>
			</div>
			</div>        
		</div>
	</div>        
</div>   
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="<?php echo Router::url('/', true); ?>img/ajax-loader.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>
<script>
 
$(".mark_complete").click(function(){
			
			var batnum		=	$( this ).attr( 'for' ); 
			var can_order	=	'0';
				if($('.can_'+batnum).attr('for') > 0){
					can_order 	=   $('.can_'+batnum).attr('for');
				}
			var pro_order	=	'0';
				pro_order	=	$('.pro_'+batnum).attr('for');
			var to_pro		=	'0';
				to_pro		=	$('.to_pro_'+batnum).attr('for');
				
			var tot_order = parseInt(can_order) + parseInt(pro_order);
			 
			if(tot_order != to_pro)
			{
				alert('Barch Order is remaning for process.');
				return false;
			}else{
				 if(confirm("Are you sure want to marke it completed?")){
					$.ajax({
							url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/markcompletebatch',
							dataType	: 'json',
							type    	: 'POST',
							beforeSend  :  function() {$('.outerOpac').show()},
							data    	: {batnum:batnum},
							success 	:	function( data  )
							{			
									 //$(".preview").html(data.preview ) ;  
									 $('.outerOpac').hide();
									  location.reload(true);     
							}                
						});
					}
			}
});

$("#create_batch").click(function(){
	var o_f_batch	=	$('#fill_order').val();
 
			$.ajax({
						url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/createBatch',
						dataType	: 'json',
						type    	: 'POST',
						beforeSend  :  function() {$('.outerOpac').show()},
						data    	: {o_f_batch:o_f_batch},
						success 	:	function( data  )
						{			
								 $(".preview").html(data.preview ) ;  
								 $('.outerOpac').hide();
								  location.reload(true);     
						}                
					});	
			
		 
	
	
});

$( document ).ready(function() {
    var total	=	$('#total').val();
	if(total > 0){
		$('.b_total').text( total );
	} else {
		$('.b_total').text( '---' );
	}
	//var afba	=	<?php echo $total ?> - total;
	//$('#fill_order').val( afba );
});
 
</script>