<?php
$userID = $this->session->read('Auth.User.id');
$users_array = array('avadhesh.kumar@jijgroup.com','amit.gaur@jijgroup.com','mmarecky82@gmail.com','shashi.b.kumar@jijgroup.com','jake.shaw@euracogroup.co.uk','lalit.prasad@jijgroup.com','aakash.kushwah@jijgroup.com');
App::import('Controller', 'DynamicPicklist');
$dyna_pick = new DynamicPicklistController;
?>
<style>
.selectpicker {border:1px solid #c6c6c6;}
.selectpicker:hover  {border:1px solid #666666;}
.completed{background-color:#c3f6c3 !important;}
.preview h3{color:#666;}
.preview th, .preview td{padding:5px; color:#424242;}
.btn-pad{margin-bottom:4px;}
.btn-pad-top{margin-top:4px;}
center{color:#FF0000;}
.c_total{color:#17C225;}
.c_count{ border-bottom: 1px dotted grey; }
.dv{border-bottom:1px dashed #666666;width: 100%;clear: both; float:left;}
.dvs{border-bottom:1px dashed #666666;width: 100%;clear: both; float:left; padding:5px 0;}
.delevery_country{float:left; padding:3px 0; width:40%;}
.service{float:right; text-align:left;  padding:3px 0;  width:60%;}
.div-service{float:left; clear:both; width:99%; margin-bottom:2px;}
.spn{color:#009900; font-size:18px; font-weight:bold; cursor:pointer; float:right;} 
.gpl{ float:right;}
.div-spn{display:none;} 
.pick{ padding-bottom:3px;padding-left:8px; }
.pick a{color:#3333CC; font-size:11px; text-decoration:underline; }

</style> 
<div class="bottom_panel rightside bg-grey-100">
    <div class="container-fluid" >
       		
             <div class="panel">
			 <div class="panel-body">
				<div class="row">	 <div class="col-lg-12" style="margin-bottom:5px; margin-top:-5px;" ><?php print $this->Session->flash(); ?></div>  </div>
				<div class="row">	<?php //pr($o_data); ?>
						
							 
						 <div class="col-lg-3">
							<div class="panel bg-blue-400">
									<div class="panel-body padding-15-20">
										<div class="clearfix">
											<div class="pull-left">
												<div class="display-block color-blue-50 font-weight-600 font-size-13">Total Available Open Orders :</div>
												<div class="color-white font-size-14 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="343" data-speed="500" data-refresh-interval="10">
													Open Orders:<?php echo $o_data['op_ord_count'] ?><br />
													Orders after spilt:<?php echo $o_data['open_orders_merge'] ?>
												</div>
												
											</div>
											 
										</div>
									</div>
								</div>
						</div>	
						<div class="col-lg-3">
								<div class="panel bg-teal-400">
									<div class="panel-body padding-15-20">
										<div class="clearfix">
											<div class="pull-left">
												<div class="display-block color-blue-50 font-weight-600 font-size-13">Orders after split available for new batch:</div>
												<div class="color-white font-size-20 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="343" data-speed="500" data-refresh-interval="10">													 
													<?php echo $o_data['readyforbatch'] ?>
												</div>
												
											</div>											 
										</div>
									</div>
								</div>						
							 
						</div>
						<div class="col-lg-4">
						 	<div class="col-md-6"><input type="text" class="form-control" id="fill_order" value="<?php echo $o_data['readyforbatch']; ?>"></div>
							<div class="col-md-6"> <button type="button"  class="btn btn-info create_batch" id="create_batch" >Create Batch</button></div>
						</div>
					</div>
				 </div>
			</div>
			</div>
			
			<div class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12 ">
						<table class="table table-bordered table-striped dataTable">
							<tr>
 								<th>Batch Name</th>
								<th width="35%">Country & Service</th> 
								<th>Orders</th>
								<th>Creater Name</th>	
								<th>Creation Date</th>
								<th>Action</th>
							</tr>
							<?php
							$div_count = 0;
							
							App::import('Controller', 'DynamicPicklist');
							$dpObj 	=	new DynamicPicklistController;
							
							foreach($dynamic_batches as $val){
							
							$orders = $dpObj->batchOrders($val['DynamicBatche']['batch_name']);
							
							$locked  = [];
							$canceled = [];											 
							$open = 0;
							$processed = 0;
							$sorted = 0;
							$avilable_ord_for_picklist = 0;
							$avilable_qty_for_picklist = 0;
							$total_qty = 0;				
							$order_data =[];	
										
							foreach($orders as $order){
								//$order_data[$order['MergeUpdate']['order_id']] = $order['MergeUpdate']['order_id'];
								
								$service_name = 'service_not_found';
								$delevery_country = 'country_not_found';
								$q = 0;
								$sk = explode(",",$order['MergeUpdate']['sku']);
								if(count($sk)>1){
									foreach($sk as $s){
										$t = explode("XS-",$s);
										if(count($t)>1){
											$q += $t[0];											
										}
									}									
								}else{
										$t = explode("XS-",$order['MergeUpdate']['sku']);
										if(count($t)>1){
											$q += $t[0];
										} 
								}
								
								$total_qty += $q;
								
								if($order['MergeUpdate']['service_name']){
									$service_name = $order['MergeUpdate']['service_name'];
								}
								if($order['MergeUpdate']['delevery_country'] != 'null'){
									$delevery_country = $order['MergeUpdate']['delevery_country'];
								}
								$order_data[$delevery_country][$service_name][] = $order['MergeUpdate']['product_order_id_identify'];
								
								if($order['MergeUpdate']['status'] == 3){
									$locked[] = $order['MergeUpdate']['product_order_id_identify'];
								}else if($order['MergeUpdate']['status'] == 2){
									$canceled[] = $order['MergeUpdate']['product_order_id_identify'];
								}else if($order['MergeUpdate']['status'] == 1){
									$processed++;
									//$pp[] = $order['MergeUpdate']['product_order_id_identify'];
								}else if($order['MergeUpdate']['status'] == 0){
									$open++;
								}
								if($order['MergeUpdate']['status'] == 0 && $order['MergeUpdate']['pick_list_status'] == 0){
									$avilable_ord_for_picklist++;
									$avilable_qty_for_picklist += $q;
								}											   
								if($order['MergeUpdate']['scan_date'] != ''){
									$sorted++;
								}
							}
						//	pr($order_data);
							?>
							<tr>
 								<td ><?php echo $val['DynamicBatche']['batch_name'];?></td>
								<td><?php 
								 
								foreach($order_data as $delevery_country => $data){ 
									echo '<div class="dv">';
										echo '<div class="delevery_country"><strong>'.$delevery_country .'</strong></div>'; 
										echo '<div class="service">';
										foreach(array_keys($data) as $v){  
											$div_count++;
											
											echo '<div class="div-service">'. $v. '('. count($data[$v]) .')'; 
											
												 
													 if($v == 'Over Weight'){
  														echo '<span class="spn" data-toggle="collapse" data-target="#oow_'.$div_count.'">+</span>';
														echo '<div id="oow_'.$div_count.'" class="collapse">';
														echo implode("<br>",$data[$v]);
														echo '</div>';
													 }else if($v == 'service_not_found'){
 														echo '<span class="spn" data-toggle="collapse" data-target="#osn_'.$div_count.'">+</span>';
														echo '<div id="osn_'.$div_count.'" class="collapse">';
														echo implode("<br>",$data[$v]);
														echo '</div>';
 													 } 	else {
													 
														$pl = $dpObj->batchPicklist($val['DynamicBatche']['batch_name'],$delevery_country,$v);
													
														if(count($pl) > 0){
															foreach($pl as $pick){
 																echo '<div class="pick" id="gpl_'.$div_count.'"> <a href="'.Router::url('/', true).'img/printPickList/'.$pick['DynamicPicklist']['picklist_name'].'" target="_blank" >'.$pick['DynamicPicklist']['picklist_name'].'</a></div>';
															}
														}else{
															echo '<div class="gpl" id="gpl_'.$div_count.'"> <button type="button" class="btn btn-warning btn-xs gen-pl" batch="'.$val['DynamicBatche']['batch_name'].'" country="'.$delevery_country.'" service="'.$v.'" orders="'. implode(",",$data[$v]).'"  div_count="gpl_'.$div_count.'">G. PickList</button></div>';
														}
														
 													 } 											
												 
											echo "</div>";
										}		
										echo "</div>";	
									echo "</div>";							
								}
								
								
								?></td> 
							 	 
								<td><?php  //pr($pp);
								
											 
										 	echo '<div class="dvs"><strong>Total Batch Orders: '.count($orders).'  Qty: '.$total_qty.'</strong></div>'; 
											  
 											if($processed > 0){
												echo "<div class='dvs' style='color:#990066'><strong>Processed Orders: ".$processed."</strong></div>"; 
											}
										
 											if($open > 0){
												echo "<div class='dvs' style='color:#006600'><strong>Open Orders: ". $open."</strong></div>"; 
											}
											
  											if($sorted > 0){
												echo "<div class='dvs' style='color:#BF1705'><strong>Sorted Orders: ". $sorted."</strong></div>"; 
											}
											
  											if($avilable_ord_for_picklist > 0){
												echo '<div class="dvs" style="color:#006600"><strong>Available Orders For Picklist : '.$avilable_ord_for_picklist.' Qty:'.$avilable_qty_for_picklist.'</strong></div>';
												 
											}
 											
											if(count($locked) > 0){												
												echo "<div class='dvs' style='color:#000099'><strong>Locked Orders: ".count($locked)."</strong></div>";
												 
 											}
											
 											if(count($canceled) > 0 ){												 
												echo "<div style='color:#FF0000'><strong>Canceled Orders: ".count($canceled)."</strong></div>"; 
											}
											 
									?></td> 
								<td><?php  echo $val['DynamicBatche']['creater_name'];?></td>
								<td><?php  echo $val['DynamicBatche']['created_date'];?></td>
								<td><center><a href="<?php echo Router::url('/', true). 'DynamicPicklist/index/'.$val['DynamicBatche']['batch_name']; ?>" class="btn btn-info btn-xs">Go To Picklist</a></center>
								<br /> 
								<center>
								<?php if($val['DynamicBatche']['status'] < 2){?>
								<a href="javascript:void(0);"  for="<?php echo $val['DynamicBatche']['batch_name']; ?>" class="mark_complete btn btn-success btn-xs">Mark Completed</a>
								<?php }else{?>
								<div style="color:#006600"><strong>Completed</strong></div>
								<?php }?>
								</center>
								</td>
							 
							 
							</tr>
 							<?php 
							}
 							?> 								
						</table>
						 	 
						<ul class="pagination">
							  <?php
  								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>
				</div>
			</div>
			</div>        
		</div>
	</div>        
</div>   
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="<?php echo Router::url('/', true); ?>img/ajax-loader.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>
<script>


$(".gen-pl").click(function(){
	var batch		=	$( this ).attr( 'batch' ); 	 
	var orders		=	$( this ).attr( 'orders' );
	var country		=	$( this ).attr( 'country' );
	var service		=	$( this ).attr( 'service' );
	var div_count	=	$( this ).attr( 'div_count' );
	 
 	if(confirm("Are you sure want to generate pick list?")){
	$.ajax({
			url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/generateCountryPicklist', 
			dataType	: 'json',
			type    	: 'POST',
			beforeSend  : function() {$('.outerOpac').show()},
			data    	: {batch:batch,country:country,service:service,orders:orders},
			success 	: function( data  )
			{ 						  
					 $('.outerOpac').hide();
					 
					 if(data.single != undefined){
						 if(data.single.status == 'ok' && data.multi.status == 'ok'){
							  $("#"+div_count).html('<div class="pick"><a href="<?php echo Router::url('/', true); ?>img/printPickList/'+ data.single.pl_name  +'">'+ data.single.pl_name  +'</a></div><div class="pick"><a href="<?php echo Router::url('/', true); ?>img/printPickList/'+ data.multi.pl_name  +'">'+ data.multi.pl_name  +'</a></div>' ) ;  
						 }else if(data.single.status == 'ok' && data.multi.status == undefined){
							  $("#"+div_count).html('<div class="pick"><a href="<?php echo Router::url('/', true); ?>img/printPickList/'+ data.single.pl_name  +'">'+ data.single.pl_name  +'</a></div>' ) ;  
						 }else if(data.single.status == undefined && data.multi.status == 'ok'){
							  $("#"+div_count).html('<div class="pick"><a href="<?php echo Router::url('/', true); ?>img/printPickList/'+ data.single.pl_name  +'">'+ data.single.pl_name  +'</a></div>' ) ;  
						 }
					 
					 }else{
						 alert(data.msg); 
					 }   
			}                
		});
	}

			 
});

 function ShowDiv(div_id){
	 if($( "#"+div_id ).css('display') == 'none'){
		 $( "#"+div_id ).show();
	 }else if($( "#"+div_id ).css('display') == 'block'){
		 $( "#"+div_id ).hide();
	 }
 }
 
$(".mark_complete").click(function(){
 var batnum		=	$( this ).attr( 'for' ); 	 
 if(confirm("Are you sure want to marke it completed?")){
	$.ajax({
			url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/markcompletebatch',
			dataType	: 'json',
			type    	: 'POST',
			beforeSend  :  function() {$('.outerOpac').show()},
			data    	: {batnum:batnum},
			success 	:	function( data  )
			{ 						  
					 $('.outerOpac').hide();
					 
					 if(data.status == 'ok'){
						  location.reload(true);  
					 }else{
						 alert(data.status );
					 }   
			}                
		});
	}
			 
});

$("#create_batch").click(function(){
	var o_f_batch	=	$('#fill_order').val();
	
	$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/createBatch',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  :  function() {$('.outerOpac').show()},
				data    	: {o_f_batch:o_f_batch},
				success 	:	function( data  )
				{			
						 $(".preview").html(data.preview ) ;  
						 $('.outerOpac').hide();
						  location.reload(true);     
				}                
			});	 
	
});
  
</script>