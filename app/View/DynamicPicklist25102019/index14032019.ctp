<?php
$userID = $this->session->read('Auth.User.id');
/*$users_array = array('avadhesh.kumar@jijgroup.com','amit.gaur@jijgroup.com','mmarecky82@gmail.com','shashi.b.kumar@jijgroup.com','jake.shaw@euracogroup.co.uk','lalit.prasad@jijgroup.com','aakash.kushwah@jijgroup.com','anna.kedziora@onet.eu');*/
$users_array = array('avadhesh.kumar@jijgroup.com','amit.gaur@jijgroup.com','mmarecky82@gmail.com','anna.kedziora@onet.eu');
?>
<style>
.selectpicker {border:1px solid #c6c6c6;}
.selectpicker:hover  {border:1px solid #666666;}
.completed{background-color:#c3f6c3 !important;}
.preview h3{color:#666;}
.preview th, .preview td{padding:5px; color:#424242;}
.btn-pad{margin-bottom:4px;}
.btn-pad-top{margin-top:4px;}
center{color:#FF0000;}
.dvs{border-bottom:1px dashed #666666;width: 100%;clear: both; float:left; padding:5px 0;}
</style> 
<div class="bottom_panel rightside bg-grey-100">
    <div class="container-fluid" >
       		<?php //pr($this->request->pass[0]); 
			//echo substr("20181224010523",0,10)."<br>";
			//echo substr("20181224010523",6,2).'-'.substr("20181224010523",4,2).'-'.substr("20181224010523",0,4);
			//echo substr("20181224010523",8,2).':'.substr("20181224010523",10,2).':'.substr("20181224010523",12,2);
			?>
             <div class="panel">
			 <div class="panel-body">
			 <div class="col-lg-12"><?php print $this->Session->flash(); ?></div>  
				<div class="row">	
					<div class="col-lg-12">
					
						<div class="col-md-3 col-lg-3"> 	 
					 <?php $batch = ''; if(isset($this->request->pass[0])){ $batch =  $this->request->pass[0]; } ?>
 							<div class="col-md-4 col-lg-4" style="padding-left:0px;"><strong>Batch No:</strong> </div>
							<div class="col-md-8 col-lg-8" style="padding-left:0px;"><input type="text" name="batch" id="batch" class="form-control " placeholder="Batch"  disabled="disabled" value="<?php  echo $batch;   ?>" /></div>
  						</div>	
						 
 						
						<div class="col-md-3 col-lg-3"> 
 							<select class="form-control" title="Choose OrderType" name="sku_type" id="sku_type"  onchange="refreshConditions();" > 
							 <option value="">SKU in Type</option>	
							 <option value="single">Single SKU</option>	
							 <option value="multiple">Multiple SKU</option> 						
							</select>
						</div>	
						 
						
						<div class="col-md-3 col-lg-3" id="pc" style="display:none"> 
						 
							<select class="form-control" title="Choose Country" name="postal_country" id="postal_country" onchange="getPoatalServices()" > 
  							 <option value="">Choose Country</option>							 
							</select>
						</div>	
						<div class="col-md-3 col-lg-3" id="ps" style="display:none"> 
							<select class="form-control" title="Choose Service" name="postal_services" id="postal_services" onchange="getBinLocations();"> 
							   		<option value="">Choose Service</option>			
							</select>
						</div>
						
						</div>
						</div>
						
						<div class="row" style="padding-top:5px;">	
						<div class="col-lg-12">
						
						<?php /*?><div class="col-md-3 col-lg-3"><input type="text" class="form-control" placeholder="Bin" 
							value=""  onclick="showDiv('pps')">						
							<ul class="list-group list-group-flush" id="pps" style="display:none"></ul>
						</div><?php */?>
						
						<div class="col-md-3 col-lg-3"  id="bl" style="display:none"> 
							<select class="selectpicker btn form-control"  multiple data-live-search="true" title="Choose Bin location" name="bin_locations" id="bin_locations" onchange="getSkuList(this);"> 
							   		<option value="">Choose Bin location</option>			
							</select> 
						</div>	
						
						<div class="col-md-3 col-lg-3" id="sk" style="display:none"> 
							<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose SKU" name="sku" id="sku"> 
							  <option value="">Select SKU</option>   
							</select>
						</div>	
 						 
						
						 <div class="col-md-2" id="pp" style="display:none"> <button type="button"  class="btn btn-info btn-sm pick_preview">Picklist Preview</button></div>
						 <div class="col-md-1"> <a href="<?php  echo Router::url('/', true) ."DynamicPicklist/batch"?>" class="btn btn-warning btn-sm">Go Back</a></div>
						 
						<div class="col-lg-12 preview">&nbsp;</div>  
						
						 
						</div> 						
 						
				 </div>
			 
			</div>
			</div>
			
			<div class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12 ">
					
							<table class="table table-bordered table-striped dataTable">
							<tr>
 								<th>PickList Name</th> 
								<!--<th>Pc Name</th>-->
								<th>Totals</th>
								<th width="11%">Order Status</th> 
								<th>Assign User</th>
								<th>Assign Time</th>
								<th>Completed Time</th>
								<th>Time Taken</th>
								<th>Created By</th>	
								<th>Creation Date</th>
								<th>Action</th>
							</tr>
 							<?php 
							
								App::Import('Controller', 'DynamicPicklist'); 
								$obj = new DynamicPicklistController;	 
  								$userdata = array();
								$useroptions = '';
								foreach($users as $u){
									$userdata[$u['User']['email']] = $u['User']['first_name'].'&nbsp;'.$u['User']['last_name'];
									$useroptions .= '<option  data-tokens="'.$u['User']['email'].'" value="'.$u['User']['email'].'">'.$u['User']['first_name'].'&nbsp;'.$u['User']['last_name'].'</option>';
										 
								} 
								$user_email = strtolower($this->session->read('Auth.User.email'));
						 		foreach($DynamicPicklist as   $val){
 							?>
							<tr class="<?php if($val['DynamicPicklist']['completed_mark_user'] !=''){ echo 'completed';}?>">
								<td> 
								
								<a href="<?php echo Router::url('/', true). 'img/printPickList/'.$val['DynamicPicklist']['picklist_name']; ?>" target="_blank" style="text-decoration:underline"><?php  echo $val['DynamicPicklist']['picklist_name'];?></a>
								
								
								<br />
								
								<a href="<?php echo Router::url('/', true); ?>DynamicPicklist/getPicklistOrders/<?php  echo $val['DynamicPicklist']['id'];?>/?batch=<?php echo $batch;?>"><button type="button" class="btn btn-info btn-xs btn-pad-top">View Orders</button></a></td>
								
								<!--<td><?php  //echo $val['DynamicPicklist']['pc_name'];?></td>-->
								<td><?php  $allOrdStatus = $obj->getAllOrderStatus($val['DynamicPicklist']['id']);							
								echo "Orders:". count($allOrdStatus);
								echo "<br>Qty:".$val['DynamicPicklist']['sku_count'];
								
								?>
								
								</td>
								<td>
								<?php 
 									$locked = '';
									$canceled = '';
									$open = 0;
									$processed = 0;
									$sorted = 0;
									$label_status = 0;
									foreach($allOrdStatus as $s){
										if($s['MergeUpdate']['status'] == 3 && $s['MergeUpdate']['label_status'] == 1){
											$locked .= $s['MergeUpdate']['product_order_id_identify']. '<br>';
										}else if($s['MergeUpdate']['status'] == 2){
											$canceled .= $s['MergeUpdate']['product_order_id_identify']. '<br>';
										}else if($s['MergeUpdate']['status'] == 1){
											$processed++;
										}else if($s['MergeUpdate']['status'] == 0){
											$open++;
										}
										
									   if($s['MergeUpdate']['label_status'] == 2){
											$label_status++;
										}
										
										if($s['MergeUpdate']['scan_date'] != ''){
											$sorted++;
										}
 									}
									
									if($open > 0){
										echo "<div class='dvs' style='color:#006600'><strong>Open : ". $open."</strong></div>";										
									}
									/*if($label_status > 0){
										echo "<div style='color:#660000'><strong>Label Printed : ". $label_status."</strong></div>";
										echo "----------------------<br>";
									}*/
									
 									echo "<div class='dvs' style='color:#990066'><strong>Processed : ".$processed."</strong></div>";
									 
									echo "<div class='dvs' style='color:#006600'><strong>Sorted : ". $sorted."</strong></div>";
									
									
									if($locked !=''){										 
										echo "<div class='dvs' style='color:#000099'><strong>-----Locked-----</strong>". $locked."</div>";										
									}
									
									if($canceled !=''){
										echo "<div class='dvs' style='color:#FF0000'><strong>-----Canceled-----</strong>". $canceled."</div>";	
									}
									
									?>
								 </td> 
								<td>	 
								 
								<?php 
							  	 if(in_array($user_email, $users_array)){?>
								
								<select class="selectpicker form-control" data-live-search="true" title="Assign User" name="assign_user" id="assign_user_<?php  echo $val['DynamicPicklist']['id'];?>" onchange="assignUser(<?php  echo $val['DynamicPicklist']['id'];?>);">
									<option value="">-Assign User-</option>
									<?php echo $useroptions; ?>
									
								</select>
								
								<?php }?>
								<div id="user_<?php  echo $val['DynamicPicklist']['id'];?>"><?php  if($val['DynamicPicklist']['assign_user_email']){echo 'Assign To: <strong>'.$userdata[$val['DynamicPicklist']['assign_user_email']].'</strong>';}?></div>
								</td>
								<td id="time_<?php  echo $val['DynamicPicklist']['id'];?>"><?php  echo $val['DynamicPicklist']['assign_time'];?></td>
								<td id="ctime_<?php  echo $val['DynamicPicklist']['id'];?>"><?php  echo $val['DynamicPicklist']['completed_date'];?></td>
								<td><?php  
								if(strtotime($val['DynamicPicklist']['completed_date']) > 0){
								
									$interval = (strtotime($val['DynamicPicklist']['completed_date'])) - (strtotime($val['DynamicPicklist']['assign_time']) );
									$days = floor($interval / 86400); // seconds in one day
									$interval = $interval % 86400;
									$hours = floor($interval / 3600);
									$interval = $interval % 3600;
									$minutes = floor($interval / 60);
									$interval = $interval % 60;
									$seconds = $interval;
									
									if($days > 0) echo  $days .' Days <br>';
									if($hours > 0) echo  $hours .' Hours<br>';
									if($minutes > 0) echo  $minutes .' Minutes';
									if($minutes == 0) echo  $seconds .' Seconds';
									 
								}else{
									echo '-';
								}
								
								?></td>
 								
								<td><?php  echo $val['DynamicPicklist']['created_username'];?></td>
								<td><?php  echo $val['DynamicPicklist']['created_date'];?></td>
								<td align="center">
								<?php 
								
								  if($val['DynamicPicklist']['completed_mark_user'] == ''){  
								
								     if(in_array($user_email,$users_array)){
										echo '<button type="button" class="btn btn-success btn-xs btn-pad" onclick="markCompleted(\''. $val['DynamicPicklist']['id'].'\',\''. $val['DynamicPicklist']['assign_user_email'].'\')">Mark Completed</button><br />';
 									 }
 									 if($val['DynamicPicklist']['picklist_type'] == 'single'){  
									
										echo '<button type="button" class="btn btn-info btn-xs btn-pad" onclick="generateLnS(\''. $val['DynamicPicklist']['id'].'\',\''. $val['DynamicPicklist']['assign_user_email'].'\')">Print Label</button><br />';
										echo '<button type="button" class="btn btn-warning btn-xs btn-pad" onclick="markProcessed(\''. $val['DynamicPicklist']['id'].'\',\''. $val['DynamicPicklist']['assign_user_email'].'\')">Process</button><br />';
										echo '<button type="button" class="btn btn-primary btn-xs" onclick="markSorted(\''. $val['DynamicPicklist']['id'].'\',\''. $val['DynamicPicklist']['assign_user_email'].'\')">Sort</button><br />';
									 } 
									  
								  }else{      
									echo '<span style="color:#FF0000"><strong>Completed</strong></span>';
								  }?>
								
								</td>
 								</tr>
								<?php  
									}
								?> 								
						</table>
						
							 
						<ul class="pagination">
							  <?php
  								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>

				</div>
			</div>
			</div>        
		</div>
		
	</div>        
</div>   
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="<?php echo Router::url('/', true); ?>img/ajax-loader.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>

<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
 <script>
 function showDiv(){
	 $("#pps").show();
 }
 
  $(function() {
  $('input[name="picklist_before_date"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
	format: 'YYYY-MM-DD',
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
     getDeleveryCountry();
  });
});


function refreshConditions( ){
 var  error = 0;
	var section = [];
	/*$.each($("#sections option:selected"), function(){            
		section.push($(this).val());
	});*/
	
	/*if(section.length < 1){	
	 error++;
	}*/
 	if($("#sku_type option:selected").val() == ''){	
	 error++;
	}  
	var select = $('#bin_locations');
		select.empty();
		select.append("<option value=''>---Select bin locations---</option>"); 
		$('.selectpicker').selectpicker('refresh');
	
	var select = $('#postal_services');
		select.empty();
		select.append("<option value=''>---Select Postal Services---</option>");
					
	$(".preview").html('');
	
	if($("#sku_type option:selected").val() !=''){
		$('#pc').show();
	}else{
		$('#pc').hide();
	}			
	$('#ps').hide(); 
	$('#bl').hide(); 
	$('#sk').hide();
	$('#pp').hide();
	 
		
	if(error == 0){
		getDeleveryCountry();		 
	}
}

 function generatePickList( picklist_id ){
	 
 		if(confirm('Are you sure want to generate PickList!')){            
		  
 		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/generatePicklist',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  : function() {$('.outerOpac').show()},
				data    	: {  picklist_name:picklist_id},
				success 	:	function( data  )
				{			
 					   $('.outerOpac').hide();   
					   if(data.status == 'ok'){
					  	 window.location.href = '<?php echo Router::url('/', true); ?>DynamicPicklist/index/<?php if(isset($this->request->pass[0])){ echo $this->request->pass[0]; } ?>';   
					   }    
				}                
			});	
		}
	 
 }
 
 function markCompleted( picklist_id, assign_user ){
	
	var users = ["mmarecky82@gmail.com", "anna.kedziora@onet.eu","avadhesh.kumar@jijgroup.com","amit.gaur@jijgroup.com"];
	var user = users.indexOf("<?php echo $user_email?>");  	  
	if(assign_user == ''){
		alert('User is not assigned to this picklist. Please assign an user to it.');
	}else if(user < 0){
		alert('You are not authorised to mark it completed. Only Michal Or Pati can mark it completed .');
	}else{
	
		if(confirm('Are you sure want to Mark it Completed!')){            
		  
 		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/markCompleted',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  : function() {$('.outerOpac').show()},
				data    	: {  picklist_inc_id:picklist_id},
				success 	:	function( data  )
				{			
					$('.outerOpac').hide(); 
					console.log('status:'+ data.status);
					console.log('open orders:'+ data.open_orders);
					console.log('processed orders:'+ data.processed);
					console.log('locked orders:'+ data.locked);
					console.log('canceled orders:'+ data.canceled);
				    if(data.status == 'incomplete'){
						alert(data.open_orders + ' orders are open. Please process these orders before mark complete.'); 
				    }else{
						alert('Picklist is marked as completed.');  
					 	location.reload(true); 
					} 
					               
				}                
			});	
		}
	}
 }

 function getBinLocations(){
 	
	var  error = 0;
	var select = $('#sku');
		select.empty();
		select.append("<option value=''>---Select sku---</option>");
	
	$(".preview").html('');
 	 	
	if($("#batch").val() == ''){	
	 alert('Please select batch.');
	 error++;
	}
	
	if($("#sku_type option:selected").val() == ''){	
	 alert('Please select sku type.');
	 error++;
	}  
	
	if(error == 0){
	
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/getBinLocations',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  :  function() {$('.outerOpac').show()},
				data    	: {sku_type:$("#sku_type option:selected").val(),postal_country:$("#postal_country option:selected").val(),postal_service:$("#postal_services option:selected").val(), batch:$("#batch").val() },
				success 	:	function( data  )
				{			// alert(data);alert(data.length);
						
						if( $("#sku_type option:selected").val() == 'single' && $("#postal_country option:selected") != 'Italy'){
							$('#bl').show();
						}else{
							$('#bl').hide();
						}
					  
					  var select = $('#bin_locations');
						select.empty();
						select.append("<option value=''>---Select bin locations---</option>");
					  
						for (var j = 0; j < data.length; j++){ 
							
							console.log(data[j]);
							$("#bin_locations").append("<option data-tokens='"+data[j]+"' value='" +data[j]+ "'>" +data[j]+ "</option>");
							
							 
 							/*$("#pps").append('<li class="list-group-item borderless"><input type="checkbox" value="'+ data[j] + '" id="'+ data[j] +'"/><label class="checkbox" for="'+ data[j] +'">'+ data[j] +'</label></li>');
							*/
							 
     
							
						}
 					   $('.selectpicker').selectpicker('refresh');
					   $('.outerOpac').hide();
				}                
			});	
	}
 }
 function getSkuList(_this){
 	
	var  error = 0;
	var bin_locations = [];
	//alert(  $(_this).val());
	  
 	var select = $('#sku');
		select.empty();
		select.append("<option value=''>---Select sku---</option>");
	
	$(".preview").html('');
 	 	
	 
	$.each($("#bin_locations option:selected"), function(){            
		bin_locations.push($(this).val());
	});
	
	if(bin_locations.length < 1){	
	 alert('Please select bin locations.');
	 error++;
	}
 	 	
	if($("#batch").val() == ''){	
	 alert('Please select batch.');
	 error++;
	}
	
	if($("#sku_type option:selected").val() == ''){	
	 alert('Please select sku type.');
	 error++;
	}  
	
	if(error == 0){
	
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/getSkuList',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  :  function() {$('.outerOpac').show()},
				data    	: {bin_locations:bin_locations, sku_type:$("#sku_type option:selected").val(),postal_country:$("#postal_country option:selected").val(),postal_service:$("#postal_services option:selected").val(), batch:$("#batch").val() },
				success 	:	function( result  )
				{			 
						$(".preview").html('');
						data = result.data;
						
						if($("#sku_type option:selected").val() == 'single'){
							$('#sk').show();
							$('#pp').show();
						}else{
							$('#sk').hide();
							$('#pp').hide();
						}
						
						var select = $('#sku');
						select.empty();
						select.append("<option value=''>---Select sku---</option>");
						
						
						if(result.error == ''){
							for (var j = 0; j < data.length; j++){ 
								console.log(data[j]);
								$("#sku").append("<option data-tokens='"+data[j]+"' value='" +data[j]+ "'>" +data[j]+ "</option>");
							}
						}else{	
						
							if(confirm('Are you sure want to select different floors bin locations?')){ 
							
							}			  
  							 
						}
					  $('.selectpicker').selectpicker('refresh');
					  $('.outerOpac').hide();
				}                
			});	
	}
 }

 function getDeleveryCountry(){
 	
	var error = 0;
	var select = $('#bin_locations');
					select.empty();
					select.append("<option value=''>---Select bin locations---</option>"); 
	$('.selectpicker').selectpicker('refresh');
 	
	if($("#batch").val() == ''){	
	 alert('Please select batch.');
	 error++;
	}
	
	if($("#sku_type option:selected").val() == ''){	
	 alert('Please select sku type.');
	 error++;
	}  
	
	if(error == 0){
	
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/getDeleveryCountry',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  :  function() {$('.outerOpac').show()},
				data    	: {sku_type:$("#sku_type option:selected").val(),batch:$("#batch").val() },
				success 	:	function( data  )
				{ 						 					   
					   var select = $('#postal_country');
						select.empty();
						select.append("<option value=''>---Select Country---</option>");
					
						for (var j = 0; j < data.length; j++){
							console.log(data[j]);
							$("#postal_country").append("<option data-tokens='"+data[j]+"' value='" +data[j]+ "'>" +data[j]+ "</option>");
						}
						
					 $('.outerOpac').hide();
				}                
			});	
	}
}

function getPoatalServices(){

	var italy = false;
	/*var postal_country = [];
	$.each($("#postal_country option:selected"), function(){            
		postal_country.push($(this).val());
		
	});*/
	
	if($("#postal_country option:selected").val() ==  'Italy'){
		  italy = true;
	}
	 
	if( $("#sku_type option:selected").val() == 'single' && italy == false){ 
					 
	  $('#ps').show();
	  $('#pp').hide();
	  
		var select = $('#bin_locations');
			select.empty();
			select.append("<option value=''>---Select bin locations---</option>");  			
			$('.selectpicker').selectpicker('refresh');
		 
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/getPoatalServices',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  :  function() {$('.outerOpac').show()},
				data    	: {  postal_country:$("#postal_country option:selected").val(),sku_type:$("#sku_type option:selected").val(),batch:$("#batch").val()},
				success 	:	function( data  )
				{			
	  
						  var select = $('#postal_services');
							select.empty();
							select.append("<option value=''>---Select Postal Services---</option>");
						
							for (var j = 0; j < data.length; j++){
								console.log(data[j]);
								$("#postal_services").append("<option data-tokens='"+data[j]+"' value='" +data[j]+ "'>" +data[j]+ "</option>");
							}
						
						
					  $('.outerOpac').hide();
					 
				}                
			});	
			
		}else{
			$('#ps').hide();
			$('#pp').show();
		}
}

function changeSections(){

				var select = $('#postal_services');
					select.empty();
					select.append("<option value=''>---Select Postal Services---</option>");
}  
 
$(".pick_preview").click(function(){
	
	    var error = 0;
	 
		 var bin_locations = [];
		$.each($("#bin_locations option:selected"), function(){            
			bin_locations.push($(this).val());
		});
		
		var italy = false;
		/*var postal_country = [];
		$.each($("#postal_country option:selected"), function(){            
			postal_country.push($(this).val());
			if($(this).val() ==  'Italy'){
			  italy = true;
			}
		});*/
		
		if($("#postal_country option:selected").val() ==  'Italy'){
		  italy = true;
		}
		 
		if( $("#sku_type option:selected").val() == 'single' && italy == false){
			
			if(bin_locations.length < 1){	
			 alert('Please select bin locations.');
			 error++;
			}
			
			if($("#postal_services option:selected").val() == ''){	
			 alert('Please select postal service.');
			 error++;
			}
		
		 		
		}
		 
		var skus = [];
		$.each($("#sku option:selected"), function(){            
			skus.push($(this).val());
		});
 		
 		if($("#sku_type option:selected").val() == ''){	
		 alert('Please select sku type.');
		 error++;
		}
		
		if(postal_country.length < 1){	
		 alert('Please select country.');
		 error++;
		}
		
		 	
		if(error == 0){ 
		
		 
			$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/picklistPreview',
					dataType	: 'json',
					type    	: 'POST',
					beforeSend  :  function() {$('.outerOpac').show()},
					data    	: {bin_locations:bin_locations, sku_type:$("#sku_type option:selected").val(),postal_country:$("#postal_country option:selected").val(),postal_services:$("#postal_services option:selected").val(),skus:skus,batch:$("#batch").val()},
					success 	:	function( data  )
					{			
						   $(".preview").html(data.preview ) ;  
						   if(data.error != ''){
						    	alert(data.error);
								$(".preview").html(data.error ) ;  
						   } 
						    
						   $('.outerOpac').hide();   
					}                
				});	
			 
  	   } 
   
	
});

 function assignUser(picklist_id){

 	if($("#assign_user_"+picklist_id+" option:selected").val()==''){            
		  alert('Please select user');
	}else{
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/assignUser',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  :  function() {$('.outerOpac').show()},
				data    	: {  picklist_inc_id:picklist_id, assign_user:$("#assign_user_"+picklist_id+" option:selected").val()},
				success 	:	function( data  )
				{			
 					$("#user_"+picklist_id).html('Assign To:<strong>'+data.user+'</strong>');    
					$("#time_"+picklist_id).html('<?php echo date('Y-m-d H:i:s')?>');  
					$('.outerOpac').hide();          
				}                
			});	
		}
}
 

function generateLnS( picklist_id, assign_user){

	var users = ["mmarecky82@gmail.com", "anna.kedziora@onet.eu","avadhesh.kumar@jijgroup.com",assign_user];
	var user = users.indexOf("<?php echo $user_email?>"); 
	   
	if(user < 0){
		alert('You are not authorised to print label & slip of these order. Only assigned user can print.');
	}else{	
		if(confirm('Are you sure want to generate label and Slip!')){            
			$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/batchPicklistSlipLabel',
					dataType	: 'json',
					type    	: 'POST',
					beforeSend  :  function() {$('.outerOpac').show()},
					data    	: {  picklist_inc_id:picklist_id},
					success 	:	function( data  )
					{			
						$('.outerOpac').hide();   
						alert('Label and Slips are Generated/Printed.');					   
					    	
					}                
				});	
			}
	} 
}

function markProcessed( picklist_id, assign_user ){
	
	var users = ["mmarecky82@gmail.com", "anna.kedziora@onet.eu","avadhesh.kumar@jijgroup.com",assign_user];
	var user = users.indexOf("<?php echo $user_email?>"); 
  
	if(user < 0){
		 alert('You are not authorised to process these order. Only assigned user can process.');
	}else{
	 
		 if(confirm('Are you sure want to process orders!')){            
			$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/batckPicklistBulkOrderProcess',
					dataType	: 'json',
					type    	: 'POST',
					beforeSend  :  function() {$('.outerOpac').show()},
					data    	: {  picklist_inc_id:picklist_id},
					success 	:	function( data  )
					{			
						
						$('.outerOpac').hide();   
						
						if(data.status == 'ok'){
							window.location.href = '<?php echo Router::url('/', true); ?>DynamicPicklist/index/<?php if(isset($this->request->pass[0])){ echo $this->request->pass[0]; } ?>';   
						} else{
							alert(data.msg);
						}	
					   	
					}                
				});	
			}
		} 
}

function markSorted( picklist_id,assign_user ){

	var users = ["mmarecky82@gmail.com", "anna.kedziora@onet.eu","avadhesh.kumar@jijgroup.com",assign_user];
	var user = users.indexOf("<?php echo $user_email?>"); 
  
	if(user < 0){
		 alert('You are not authorised to sort these order. Only assigned user can sort.');
	}else{
	
		if(confirm('Are you sure want to sort all orders!')){            
			$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/sortBatchAllOrders',
					dataType	: 'json',
					type    	: 'POST',
					beforeSend  :  function() {$('.outerOpac').show()},
					data    	: {  picklist_inc_id:picklist_id},
					success 	:	function( data  )
					{			
  						$('.outerOpac').hide();   
						if(data.status == 'ok'){
							location.reload(true);  	
						}else{
							alert(data.msg);
						}				   
 					   			
					}                
				});	
		}
	}	
	 
}
 

 
</script>