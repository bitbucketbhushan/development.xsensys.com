<?php
App::import('Controller','Virtuals');
$virObj = new VirtualsController();
$margin = $virObj->getmargen();
//$getTotalSold = $virObj->getTotalSold();
?>

<script>
$(document).ready(function() {
(function () {
    var demo, fixedTable;
    fixedTable = function (el) {
        var $body, $header, $sidebar;
        $body = $(el).find('.fixedTable-body');
        $sidebar = $(el).find('.fixedTable-sidebar table');
        $header = $(el).find('.fixedTable-header table');
        return $($body).scroll(function () {
            $($sidebar).css('margin-top', -$($body).scrollTop());
            return $($header).css('margin-left', -$($body).scrollLeft());
        });
    };
    demo = new fixedTable($('#demo'));
}.call(this));
});
</script> 


<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
						<li>
						<div class="col-lg-12">						
								
						<label class="col-lg-3">Margin</label>
								<div class="col-lg-5"><input type="text" value="<?php echo $margin; ?>" class="form-control setmarginValue" /></div>
								<div class="col-lg-4"><a class="marginValue btn btn-success" href="#" data-toggle="modal" for="" data-target=""><i class=""></i> Save </a></div>
								</div>
						</li>
							<li>
								<a class="switch btn btn-success createFeed" href="" data-toggle="modal" data-target=""><i class=""></i> Create Feed </a>
							</li>
							<li>
							<div class="btn-group">
									  <button type="button"  href="javascript:void(0);" class="downloadPickList btn btn-success no-margin">Store Feeds</button>
									  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									  </button>
									<?php if(count($files) > 0 ) { ?>
									  <ul class="dropdown-menu scrollable-menu" role="menu">
									  <?php 
										$pdfPath = '/img/storesFeed/'; 
										foreach( $files as $file ) {
											$fileName = explode('.', $file);
										?>
										<li><a href="<?php echo $pdfPath.$file; ?>" target ="_blank" ><?php echo $fileName[0]?></a></li>
										<?php } ?>
										<li role="separator"></li>
									  </ul>
									  <?php } ?>
							</div>
							</li>
							<li>
								<a class="switch btn btn-success saveAllDetail" href="javascript:void(0);" data-toggle="modal" data-target=""><i class=""></i> Save </a>
							</li>
							<li>
								<a class="switch btn btn-success" href="/Virtuals/exportDataFeedDetail" data-toggle="modal" data-target="">Export</a>
							</li>
							<li>
								<a class="switch btn btn-success" href="/Virtuals/importDataFeedDetail" data-toggle="modal" data-target="">Import</a>
							</li>
							<li>
								<a class="switch btn btn-success" href="/Virtuals/saveStockFeed" data-toggle="modal" data-target="">Save New Sku</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="panel-head"><?php print $this->Session->flash(); ?>
					
				</div>
			</div>
    <div class="container-fluid">
			
							
		<div class="row">
			            <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">
									<div class="setSearchresult"></div>	
							<div class="col-lg-8">
								
								<div class="" style="margin:0 auto; width:250px;">
						 <ul class="pagination">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>
					</div>	
								
							</div>
							<div class="col-lg-4">
								
								<label class="col-lg-3"></label>
								<div class="col-lg-6"><input type="text" value="" class="form-control searchString" /></div>
								<div class="col-lg-3"><a class="searchKey btn btn-success" href="#" data-toggle="modal" for="" data-target=""><i class=""></i> Search </a></div>
							</div>
							
							
							<div class="panel-tools"></div>
							
								</div>
										
									<div class="panel-body no-padding-top bg-white">
									<div class="scrollableArea" ></div>
									<div id="demo" class="fixedTable">
  <header class="fixedTable-header">
    <table style="table-layout:fixed" class="table table-bordered">
      <thead>
        <tr>
															<th width="30px">SN</th>
															<th width="150px">SKU</th>
															<th width="40px">T. Sold</th>
															<th width="30px">New Sku</th>
															<th width="35px">Sold (30D)</th>
															<th width="30px">Stk</th>
															<th width="80px">
																<input type="text" class="form-control " id="bufferValueId" style="width: 40px;"/>
																<a data-target="" id='bufferValue' for="" data-toggle="modal" href="#" class="btn btn-success"><i class=""></i> Go </a>
																Buffer
															</th>
															<?php foreach( $getStores as $getStore ) { ?>
															<th width="120px">
																<input type="text" class="form-control updateSubSource_value_<?php echo str_replace('.','_',str_replace('://','_',str_replace(' ','_',$getStore['Store']['store_name']))); ?>" style="width: 70px;"/>
																<a data-target="" id='updateValue' for="<?php echo str_replace('.','_',str_replace('://','_',str_replace(' ','_',$getStore['Store']['store_name']))); ?>" data-toggle="modal" href="#" class="btn btn-success"><i class=""></i> Go </a>
																
																<?php echo $getStore['Store']['store_name']; ?>
															</th>
															<?php } ?>
															<th width="80px">Qty Report</th>
															<th width="80px">Action</th>
														</tr>
      </thead>
    </table>
  </header>
  <aside class="fixedTable-sidebar">
    <table style="table-layout:fixed" class="table table-bordered">
      <tbody >
														<?php 
															
															$j = 0;
															$i = 1;
															$skuString = ''; 
															foreach( $getskusDetail as $getskuDetailValue) { 
																$skuString .= $getskuDetailValue['Product']['product_sku'].'____'; 
																?>
														<tr class="sku2_<?php echo $getskuDetailValue['Product']['product_sku']; ?>" >
															<td width="30px"><?php echo $i; ?></td>
															<td width="150px" class="title_<?php echo $getskuDetailValue['Product']['product_sku']; ?>" ><?php echo $getskuDetailValue['Product']['product_sku']; ?></td>
															<td width="40px" class="total_sold_<?php echo $getskuDetailValue['Product']['product_sku']; ?>" ></td>
															<td width="30px" class="new_sku_<?php echo $getskuDetailValue['Product']['product_sku']; ?>" >
															
															<?php echo $this->Form->checkbox('', array('checked' => '', 'name'=> $getskuDetailValue['Product']['product_sku']."___sku_status_selected", 'class' => array('newsku_'.$getskuDetailValue['Product']['product_sku']))); ?>
															
															</td>
															<td width="35px" class="sold_one_month_<?php echo $getskuDetailValue['Product']['product_sku']; ?>" ></td>
															<td width="30px" class="current_stock_<?php echo $getskuDetailValue['Product']['product_sku']; ?>"><?php echo $getskuDetailValue['Product']['current_stock_level']; ?></td>
															<td width="80px">
																<div class="input-group">
																	<input 
																		type="input" 
																		name = "<?php echo $getskuDetailValue['Product']['product_sku']; ?>___buffer" 
																		for="<?php echo $getskuDetailValue['Product']['product_sku'] ?>" 
																		class="form-control subSourceBuffer1_<?php echo str_replace('#','',str_replace('.','',$getskuDetailValue['Product']['product_sku'])); ?>" 
																		value="" />
																</div>
															</td>
															<?php  foreach( $getStores as $getStore ) { 
																   $getStoreId	=	explode( ',', $getskuDetailValue['Product']['store_name'] );
															?>
															<td width="120px" class="store_name_<?php echo $getStore['Store']['store_name']; ?>" title="<?php echo $getStore['Store']['store_name']; ?>" >
																<div class="input-group">
																	<?php if( in_array( $getStore['Store']['id'], $getStoreId )  ) { ?>	
																	<input  type="text" 
																		value="" 
																		class="form-control setvalue_<?php echo str_replace('.','_',str_replace('://','_',str_replace(' ','_',$getStore['Store']['store_name']))); ?>  
																		subSource_<?php echo $getStore['Store']['store_name'].'####'.$getskuDetailValue['Product']['product_sku']; ?>
																		setmargen_<?php echo str_replace('.','_',str_replace('://','_',str_replace(' ','_',$getStore['Store']['store_name'].'__'.$getskuDetailValue['Product']['product_sku']))); ?>
																		setajaxData_<?php echo str_replace('.','_',str_replace('://','_',str_replace(' ','_',$getStore['Store']['store_name']))).$getskuDetailValue['Product']['product_sku']; ?>
																		commonText_<?php echo $j ?>
																		commonText"
																		for="subSource__<?php echo $getStore['Store']['store_name'].'####'.$getskuDetailValue['Product']['product_sku']; ?>" 
																		style=""
																		name ="<?php echo $getskuDetailValue['Product']['product_sku'].'___'.$getStore['Store']['store_name']; ?>"
																		data-area = "commonParent_<?php echo $j; ?>"
																	/>
																	<div>
																	&nbsp;<label style="margin-bottom:0px; color: #0288d1" title="Assinged Qty" class="assign_qty2_<?php echo $getskuDetailValue['Product']['product_sku'].'___'.$getStore['Store']['store_name']; ?>" ><?php echo '[0]'; ?></label>
																	&nbsp;<label style="margin-bottom:0px; color: #33691e;" title="Selected Day Qty" class="selected_day_qty2_<?php echo $getskuDetailValue['Product']['product_sku'].'___'.$getStore['Store']['store_name']; ?>" ><?php echo '[0]'; ?></label>
																	&nbsp;<label style="margin-bottom:0px; color: #d84315;" title="Open Order Qty" class="open_order_qty2_<?php echo $getskuDetailValue['Product']['product_sku'].'___'.$getStore['Store']['store_name']; ?>" >
																	<?php
																		$objectStoreval 	=  $virObj->getSoldByStoreOpenOrder( $getskuDetailValue['Product']['product_sku'] , $getStore['Store']['store_name'] );
																		$objectStoreval = ( $objectStoreval != '' ) ? $objectStoreval : '0';
																		echo '['.$objectStoreval.']'; ?>
																	
																	</label>
																	&nbsp;<label style="margin-bottom:0px; color: #ff6f00" title="OpenOrder + Assign Qty" class="openorder_assing_qty_<?php echo $getskuDetailValue['Product']['product_sku'].'___'.$getStore['Store']['store_name']; ?>"><?php echo '[0]'; ?></label>
																	<?php } else { ?>
																		<label>N/A</label>
																	<?php } ?>
																	</div>
																</div>
															</td>
															<?php  } ?>
															<td class="report_sku_<?php echo $getskuDetailValue['Product']['product_sku']; ?> customTotal_<?php echo $j; ?>" ></td>
															<td >
																<a data-target="" id='updateSingleValue' for="<?php echo $getskuDetailValue['Product']['product_sku'].'####'.$getStore['Store']['store_name'] ?>" data-toggle="modal" href="#" class="updateremove2_<?php echo $getskuDetailValue['Product']['product_sku'] ?> btn btn-success " >Update</a>
															</td>
														</tr>
														<?php $i++; $j++; } ?>
													</tbody>
    </table>
  </aside>
<form class="virtual_feed_form">
  <div class="fixedTable-body">
    <table style="table-layout:fixed" class="table table-bordered" >
      <tbody >
														<?php 
															
															$j = 0;
															$i = 1;
															$skuString = ''; 
															foreach( $getskusDetail as $getskuDetailValue) { 
																$skuString .= $getskuDetailValue['Product']['product_sku'].'____'; 
																?>
														<tr class="sku_<?php echo $getskuDetailValue['Product']['product_sku']; ?>" >
															<td width="40px" class="total_sold_<?php echo $getskuDetailValue['Product']['product_sku']; ?>" ></td>
															<td width="30px" class="new_sku_<?php echo $getskuDetailValue['Product']['product_sku']; ?>" >
															
															<?php echo $this->Form->checkbox('', array('checked' => '', 'name'=> $getskuDetailValue['Product']['product_sku']."___sku_status_selected", 'class' => array('newsku_'.$getskuDetailValue['Product']['product_sku']))); ?>
															
															</td>
															<td width="35px" class="sold_one_month_<?php echo $getskuDetailValue['Product']['product_sku']; ?>" ></td>
															<td width="30px" class="current_stock1_<?php echo $getskuDetailValue['Product']['product_sku']; ?>"><?php echo $getskuDetailValue['Product']['current_stock_level']; ?></td>
															<td width="80px">
																<div class="input-group">
																	<input 
																		type="input" 
																		name = "<?php echo $getskuDetailValue['Product']['product_sku']; ?>___buffer" 
																		for="<?php echo $getskuDetailValue['Product']['product_sku'] ?>" 
																		class="form-control subSourceBuffer_<?php echo str_replace('#','',str_replace('.','',$getskuDetailValue['Product']['product_sku'])); ?>" 
																		value="" />
																</div>
															</td>
															<?php  foreach( $getStores as $getStore ) { 
																   $getStoreId	=	explode( ',', $getskuDetailValue['Product']['store_name'] );
															?>
															<td width="120px" class="store_name_<?php echo $getStore['Store']['store_name']; ?>" title="<?php echo $getStore['Store']['store_name']; ?>" >
																<div class="input-group">
																	<?php if( in_array( $getStore['Store']['id'], $getStoreId )  ) { ?>	
																	<input  type="text" 
																		value="" 
																		class="form-control setvalue_<?php echo str_replace('.','_',str_replace('://','_',str_replace(' ','_',$getStore['Store']['store_name']))); ?>  
																		subSource_<?php echo $getStore['Store']['store_name'].'####'.$getskuDetailValue['Product']['product_sku']; ?>
																		setmargen_<?php echo str_replace('.','_',str_replace('://','_',str_replace(' ','_',$getStore['Store']['store_name'].'__'.$getskuDetailValue['Product']['product_sku']))); ?>
																		setajaxData_<?php echo str_replace('.','_',str_replace('://','_',str_replace(' ','_',$getStore['Store']['store_name']))).$getskuDetailValue['Product']['product_sku']; ?>
																		commonText_<?php echo $j ?>
																		commonText"
																		for="subSource__<?php echo $getStore['Store']['store_name'].'####'.$getskuDetailValue['Product']['product_sku']; ?>" 
																		style=""
																		name ="<?php echo $getskuDetailValue['Product']['product_sku'].'___'.$getStore['Store']['store_name']; ?>"
																		data-area = "commonParent_<?php echo $j; ?>"
																	/>
																	<div>
																	&nbsp;<label style="margin-bottom:0px; color: #0288d1" title="Assinged Qty" class="assign_qty_<?php echo $getskuDetailValue['Product']['product_sku'].'___'.$getStore['Store']['store_name']; ?>" ><?php echo '[0]'; ?></label>
																	&nbsp;<label style="margin-bottom:0px; color: #33691e;" title="Selected Day Qty" class="selected_day_qty_<?php echo $getskuDetailValue['Product']['product_sku'].'___'.$getStore['Store']['store_name']; ?>" ><?php echo '[0]'; ?></label>
																	&nbsp;<label style="margin-bottom:0px; color: #d84315;" title="Open Order Qty" class="open_order_qty_<?php echo $getskuDetailValue['Product']['product_sku'].'___'.$getStore['Store']['store_name']; ?>" >
																	<?php
																		$objectStoreval 	=  $virObj->getSoldByStoreOpenOrder( $getskuDetailValue['Product']['product_sku'] , $getStore['Store']['store_name'] );
																		$objectStoreval = ( $objectStoreval != '' ) ? $objectStoreval : '0';
																		echo '['.$objectStoreval.']'; ?>
																	
																	</label>
																	&nbsp;<label style="margin-bottom:0px; color: #ff6f00" title="OpenOrder + Assign Qty" class="openorder_assing_qty_<?php echo $getskuDetailValue['Product']['product_sku'].'___'.$getStore['Store']['store_name']; ?>"><?php echo '[0]'; ?></label>
																	<?php } else { ?>
																		<label>N/A</label>
																	<?php } ?>
																	</div>
																</div>
															</td>
															<?php  } ?>
															<td width="80px" class="report_sku_<?php echo $getskuDetailValue['Product']['product_sku']; ?> customTotal_<?php echo $j; ?>" ></td>
															<td width="80px" >
																<a data-target="" id='updateSingleValue' for="<?php echo $getskuDetailValue['Product']['product_sku'].'####'.$getStore['Store']['store_name'] ?>" data-toggle="modal" href="#" class="updateremove_<?php echo $getskuDetailValue['Product']['product_sku'] ?> btn btn-success " >Update</a>
															</td>
														</tr>
														<?php $i++; $j++; } ?>
													</tbody>
    </table>
  </div>
</div>
									
									
										<div style="display:none" id="makeMeScrollable" class="table-responsive">
											
												<input type="hidden" value="<?php echo $skuString; ?>" class="allSkuRecords" >
										</div>
									</div>
									
								</div>
							</div>
						</div>
						</form>	
					<?php echo $this->element('footer'); ?>
				</div>
			</div>
		</div>
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

		<script>	
			
			
$(document).ready(function() {
	
			var	skuString	=	$('.allSkuRecords').val();
			$.ajax({
						'url'            : '/Virtuals/getAllSkuDetail',
						'type'           : 'POST',
						'beforeSend' 	 : function(){ 
														$( ".outerOpac" ).attr( 'style' , 'display:block'); 
													 },
						'data'           : { 
												skuString : skuString
											},                                    
						'success'        : function( msgArray )
												{ 
													var json = JSON.parse(msgArray);
                                                    var getStatus = json.status;
                                                    var getData = json.data;
                                                    var perTotal = 0;
													for (var i = 0, max = getData.length; i < max; i++)
                                                        {
															var storeName 			= getData[i].SkuPercentRecord.store_name;
															var storeAlias 			= getData[i].SkuPercentRecord.store_alies;
															var fixedDaySold 		= getData[i].SkuPercentRecord.fixed_day_sold;
															var selectDaySold 		= getData[i].SkuPercentRecord.select_day_sold;
															var totlaSoldStock 		= getData[i].SkuPercentRecord.sold_stock;
															var percentage 			= getData[i].SkuPercentRecord.percentage;
															var buffer 				= getData[i].SkuPercentRecord.buffer;
															var sku 				= getData[i].SkuPercentRecord.sku;
															var customMark 			= getData[i].SkuPercentRecord.custom_mark;
															var marginValue 		= getData[i].SkuPercentRecord.margin_value;
															var skuStatus 			= getData[i].SkuPercentRecord.sku_status;
															var storeId 			= getData[i].SkuPercentRecord.store_id;
															var title	 			= getData[i].SkuPercentRecord.product_title;
															
															storeName = storeName.replace('.', '_').replace('://', '_').replace(' ', '_');
															storeName = storeName.replace('.', '_').replace('://', '_').replace(' ', '_');
															storeName = storeName.replace('.', '_').replace('://', '_').replace(' ', '_');
															var mergeStoreAndSku = storeName+sku;
															var getCurrentStock = $('.current_stock_'+sku).text();
															var getopenorderQty = $('.open_order_qty_'+sku+'___'+storeName).text().replace(/[\])}[{(]/g, '');
															var afterBuffer = parseInt(getCurrentStock) - parseInt(buffer);
															var assignQty = Math.floor((parseInt(afterBuffer) * parseInt(percentage))/100);
															assignQty = (assignQty < 0) ? '0' : assignQty;
															var openAndAssign = (parseInt(getopenorderQty) + parseInt(assignQty));
															
															
															$('.setajaxData_'+mergeStoreAndSku).val( percentage );
															$('.subSourceBuffer_'+sku).val( buffer );
															$('.sold_one_month_'+sku).text( fixedDaySold );
															$('.total_sold_'+sku).text( totlaSoldStock );
															$('.selected_day_qty_'+sku+'___'+storeName).text( '['+selectDaySold+']' );
															$('.assign_qty_'+sku+'___'+storeName).text( '['+assignQty+']' );
															$('.openorder_assing_qty_'+sku+'___'+storeName).text( '['+openAndAssign+']' );
															$( '.title_'+sku ).attr("title", title);
															
															if( marginValue > 0 )
															{
																$('.setmargen_'+storeName+'__'+sku).parent().attr('style','background-color:#26c6da');
															}
															if( skuStatus == 1 )
															{
																$( '.newsku_'+sku).parent('div').addClass('checked');
																$( '.newsku_'+sku).val('1');
																$( '.newsku_'+sku).addClass( 'selected' );
																$( '.newsku_'+sku).attr( 'checked', 'checked' );
															}
															
															perTotal  = parseInt(perTotal) + parseInt(percentage);
															$('.report_sku_'+sku).text( perTotal );
															
															
															
														}
														$( ".outerOpac" ).attr( 'style' , 'display:none');
												}
					});
	
	/*$("#makeMeScrollable").smoothDivScroll({
			});
	$('#example3').dataTable({ 
		"iDisplayLength": 200,
		"aLengthMenu": [[200, 500, 1000, 2000, -1], [200, 500, 1000, 2000, "All"]]    
	});*/
/*	
$('.table > tbody  > tr.sku_'+sku).each(function() {
													if( $( this ).find('input:text').val() !='' )
													{
														countPercentage = countPercentage + $( this ).find('input:text').val();
													}
													$('.report_sku_'+sku).text( countPercentage );
											});*/
$('.table > tbody  > tr').each(function() {

var skuName = $( this ).attr('class');
var skuArray	=	skuName.split('_');
var countPercentage = 0; 

$('.sku_'+skuArray[1]+' > td').each(function() {
	
												if( $( this ).find('input:text').val() != undefined )
												{
													countPercentage = parseInt(countPercentage) + parseInt($( this ).find('input:text').val());
												}
													
											});			
									$('.report_sku_'+skuArray[1]).text( countPercentage );
							});

	
$('body').on( 'click', '#bufferValue', function(){
	
	$('.table > tbody  > tr').each(function() {
							
							var bufferValue	=	$('#bufferValueId').val();
							var getSku	=	$( this ).find('td:nth-child(2)').text();
							var updateValue =  $(this).find('#updateSingleValue').attr('for');
							var markArray	=	updateValue.split('####');
							
							if( markArray[1] != 1 )
							{
								getSku = getSku.replace('#', '');
								//$( this ).find( '.subSourceBuffer_'+getSku ).val( bufferValue );
								$( '.subSourceBuffer_'+getSku ).val( bufferValue );
							}
					});
		});

$('body').on('click', '#updateValue', function(){
	
	var storeName	=	$( this ).attr('for');
	var storeValue	=	$('.updateSubSource_value_'+storeName).val();
	
	$('.table > tbody  > tr ').each(function() {
							
							var updateValue =  $(this).find('#updateSingleValue').attr('for');
							var markArray	=	updateValue.split('####');
							if( markArray[1] != 1 )
							{
								$( this ).find( '.setvalue_'+storeName ).val( storeValue );
							}
					});
	});

$('body').on( 'click', '#updateSingleValue', function(){
	
	var getskuAndCustomMark	=	$( this ).attr('for');
	var getsku	=	getskuAndCustomMark.split('####');
	var bufferValue = $('.subSourceBuffer_'+getsku).val();
	var skuStoredetail = [];
	var count = 7;
	var textString	=	$( this ).text();
	
	$('.table > tbody  > tr.sku_'+getsku[0]+' > td').each(function() {
				
					skuStoredetail.push( $( this ).find('input:text').val() + '####' + $( this ).find('input:text').attr('for') );
					count++;	
				});
				var newskuStatus	=	$('.newsku_'+getsku).is(':checked');	
				$.ajax({
						'url'            : '/Virtuals/saveStoreSkuPercentage',
						'type'           : 'POST',
						'beforeSend' 	 : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },
						'data'           : { 
												skuStoredetail		:	skuStoredetail,
												bufferValue 		: 	bufferValue,
												newskuStatus 		: 	newskuStatus,
												textString			:	textString
											},                                    
						'success'        : function( msgArray )
												{ 
													if( textString == "Update" )
													{
														/*$( '.updateremove_'+getsku[0] ).text('Remove');
														$('.table > tbody  > tr.sku_'+getsku[0]+' > td').each(function() {
																$( this ).find('input:text').attr( 'style' , 'border-color:red;');
																count++;	
															});*/
														
													}
													else
													{
														/*$( '.updateremove_'+getsku[0] ).text('Update');
														$('.table > tbody  > tr.sku_'+getsku[0]+' > td').each(function() {
																$( this ).find('input:text').attr( 'style' , 'border-color:#c3c3c3;');
																count++;	
															});*/
													}
													$( ".outerOpac" ).attr( 'style' , 'display:none');
												}
					});
	});
	
	$( 'body' ).on('click', '.saveAllDetail', function(){
		
		    var setData	=	$(".virtual_feed_form").serialize();
		    $.ajax({
						'url'            : '/Virtuals/saveStoreSkuDetail',
						'type'           : 'POST',
						'beforeSend' 	 : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },
						'data'           : { setData },                                    
						'success'        : function( msgArray )
												{ 
													$( ".outerOpac" ).attr( 'style' , 'display:none');
												}
					});
		});
		
		$('body').on('click', '.marginValue', function(){
			
			 var marginValue = $('.setmarginValue').val();
			 $.ajax({
						'url'            : '/Virtuals/saveMarginValue',
						'type'           : 'POST',
						'beforeSend' 	 : function(){ 
							$( ".outerOpac" ).attr( 'style' , 'display:block'); 
							},
						'data'           : { marginValue },                                    
						'success'        : function( data )
												{ 
													var json = JSON.parse(data);
                                                    var getData = json.data;
                                                    for (var i = 0, max = getData.length; i < max; i++)
                                                             {
																 $('table > tbody  > tr').each(function() {
																		var result= $(this).find('td:nth-child(2)').html();
																		if( result == getData[i].SkuPercentRecord.sku )
																		{
																 
																		$('.setmargen_'+getData[i].SkuPercentRecord.store_alies+'__'+getData[i].SkuPercentRecord.sku).parent().attr('style','background-color:#26c6da');
																	}
																 });
																 $( ".outerOpac" ).attr( 'style' , 'display:none');
															 }
												}
					});
			});
			
			$('body').on('click', '#removeSingleValue', function(){
			
			var remove = $(this).attr('for');
			$.ajax({
						'url'            : '/Virtuals/removeCustomMark',
						'type'           : 'POST',
						'beforeSend' 	 : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },
						'data'           : { remove },                                    
						'success'        : function( data )
												{ 
													$( ".outerOpac" ).attr( 'style' , 'display:none');
												}
					});
			});
			
		

});

/*
function delayClass()
	{
		$('.dataTable > tbody  > tr > td').each(function()
		{
				if( $( this ).find('input:hidden').val() == 1 )
				{
					$( this ).find('input:checkbox').parent().addClass('checked');
					$( this ).find('input:checkbox').attr('checked', 'checked');
					$( ".outerOpac" ).attr( 'style' , 'display:none');
				}
		});

	}*/
	
	$('body').on( 'click','.createFeed', function(){
		
			$.ajax({
					'url'            : '/Virtuals/creatFeedSheet',
					'type'           : 'POST',
					'beforeSend' 	 : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },
					'data'           : { },                                    
					'success'        : function( data )
											{ 
												$( ".outerOpac" ).attr( 'style' , 'display:none');
											}
						});
		
		});
		
		$( ".commonText" ).keyup(function() {
			var data = $( this ).attr('data-area');
			var splitData = data.split('_');
			var count = 0;
			$('.commonText_'+splitData[1]).each(function(index,item)
			{
				count = count + parseInt( $(item).val() == '' ? 0 : $(item).val() );
			});
			$('.customTotal_'+splitData[1]).text( count );
		});
	
		
	  $('body').on('click', '.searchKey', function(){
				var searchString =  $('.searchString').val();
				$.ajax({
					'url'            : '/Virtuals/searchFeedBySku',
					'type'           : 'POST',
					'beforeSend' 	 : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },
					'data'           : { searchString : searchString },                                    
					'success'        : function( data )
											{ 
												$('.fixedTable').hide();
												$('.scrollableArea').html( data );
												$( ".outerOpac" ).attr( 'style' , 'display:none');
												
											}
						});
		  });

</script>
