<?php
App::import('Controller','Virtuals');
$virObj = new VirtualsController();
$margin = $virObj->getmargen();
//$getTotalSold = $virObj->getTotalSold();
?>

<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
							<li>
								<a class="switch btn btn-success createFeed" href="" data-toggle="modal" data-target=""><i class=""></i> Create Feed </a>
							</li>
							<li>
								<a class="switch btn btn-success saveAllDetail" href="javascript:void(0);" data-toggle="modal" data-target=""><i class=""></i> Save </a>
							</li>
							<!--<li>
								<a class="switch btn btn-success" href="#" data-toggle="modal" data-target=""><i class=""></i> Export </a>
							</li>
							<li>
								<a class="switch btn btn-success" href="#" data-toggle="modal" data-target=""><i class=""></i> Import </a>
							</li>-->
						</ul>
					</div>
				</div>
				<div class="panel-head"><?php print $this->Session->flash(); ?>
					<div class="" style="margin:0 auto; width:350px; padding-top:10px">
						 <ul class="pagination">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>
					</div>	
				</div>
			</div>
    <div class="container-fluid">
			
							
		<div class="row">
			
			
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">	
<div class="col-lg-6 col-lg-offset-3">
				<label class="col-lg-3">Margin</label>
				<div class="col-lg-6"><input type="text" value="<?php echo $margin; ?>" class="form-control setmarginValue" /></div>
				<div class="col-lg-3"><a class="marginValue btn btn-success" href="#" data-toggle="modal" for="" data-target=""><i class=""></i> Save </a></div>
				
			</div>
			<div class="btn-group">
					  <button type="button"  href="javascript:void(0);" class="downloadPickList btn btn-success no-margin">Store Feeds</button>
					  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					  </button>
			<?php if(count($files) > 0 ) { ?>
					  <ul class="dropdown-menu scrollable-menu" role="menu">
					  <?php 
						$pdfPath = '/img/storesFeed/'; 
						foreach( $files as $file ) {
							$fileName = explode('.', $file);
						?>
						<li><a href="<?php echo $pdfPath.$file; ?>" target ="_blank" ><?php echo $fileName[0]?></a></li>
						<?php } ?>
						<li role="separator"></li>
					  </ul>
					  <?php } ?>
			</div>
								
									<div class="panel-tools">																	
									</div>
								</div>
									<form class="virtual_feed_form" >	
									<div class="panel-body no-padding-top bg-white">
										<div id="makeMeScrollable">
											<table style="table-layout:fixed" class="table table-bordered table-striped dataTable" id="example3">
													<thead>
														<tr>
															<th width="30px">SN</th>
															<th width="150px">SKU</th>
															<th width="30px">T. Sold</th>
															<th width="30px">New Sku</th>
															<th width="30px">Sold (30D)</th>
															<th width="30px">Stk</th>
															<th width="80px">
																<input type="text" class="form-control " id="bufferValueId" style="width: 40px;"/>
																<a data-target="" id='bufferValue' for="" data-toggle="modal" href="#" class="btn btn-success"><i class=""></i> Go </a>
																Buffer
															</th>
															<?php $j = 1; foreach( $getStores as $getStoreValue) { 
																?>
															<th width="120px">
																<input type="text" class="form-control updateSubSource_value_<?php echo str_replace('.','_',str_replace('://','_',str_replace(' ','_',$getStoreValue['Store']['store_name']))); ?>" style="width: 70px;"/>
																<a data-target="" id='updateValue' for="<?php echo str_replace('.','_',str_replace('://','_',str_replace(' ','_',$getStoreValue['Store']['store_name']))); ?>" data-toggle="modal" href="#" class="btn btn-success"><i class=""></i> Go </a>
																
																<?php echo $getStoreValue['Store']['store_name']; ?>
															</th>
															<?php $j++; } ?>
															<th width="80px">Qty Report</th>
															<th width="80px">Action</th>
														</tr>
													</thead>
													<tbody >
													
													
														<?php
														App::import('Component','Store');
														$storeCMP = new StoreComponent( new ComponentCollection() );
														$j = 1;
														$i = 1;foreach( $getskuDetail as $getAllSkuValue) { 
															if( $getAllSkuValue['SkuPercentRecord']['sku_status'] == '1' )
															{
																$checkClass = 'selected';
															}
															else
															{
																$checkClass = '';
															}
															if( $getAllSkuValue['SkuPercentRecord']['custom_mark'] == '1' )
															{
																$customMark 		= 'border-color:red';
																
															}
															else
															{
																$customMarkValue 	= '1';
																$customMark = '';
															}
															
															if( $getAllSkuValue['SkuPercentRecord']['sku_status'] == '1' )
															{
																$checked 	= 'checked';
															}
															else
															{
																$checked 	= '';
															}
															
															?>
														<tr class="sku_<?php echo $getAllSkuValue['SkuPercentRecord']['sku']; ?> reportQty commonParent_<?php echo $j; ?>" for="<?php echo $getAllSkuValue['SkuPercentRecord']['sku']; ?>" >
															<td><?php echo $i ?></td>
															<td><?php echo $getAllSkuValue['SkuPercentRecord']['sku']; ?></td>
															<td><?php 
																echo $getAllSkuValue['SkuPercentRecord']['sold_stock']; ?></td>                                                
															<td>
																<?php echo $this->Form->checkbox('', array('checked' => $checked, 'name'=> $getAllSkuValue['SkuPercentRecord']['sku']."___sku_status_selected", 'class' => array($checkClass,'newsku_'.$getAllSkuValue['SkuPercentRecord']['sku']))); ?>
																<!--<input 
																	type = "checkbox" 
																	name = "<?php echo $getAllSkuValue['SkuPercentRecord']['sku']; ?>___sku_status" 
																	class= "<?php echo $checkClass; ?>newsku_<?php echo $getAllSkuValue['SkuPercentRecord']['sku']; ?>" 
																/>
																<input 
																	type="hidden" 
																	value = "<?php echo $customMarkValue; ?>" 
																	class ="checkboxhidden" 
																	name="<?php echo $getAllSkuValue['SkuPercentRecord']['sku']; ?>___sku_status_check"
																/>-->
															</td>
															<td><?php echo $getAllSkuValue['SkuPercentRecord']['sold_stock']; ?></td>
															<td><?php echo $getAllSkuValue['SkuPercentRecord']['current_stock']; ?></td>
															<td><div class="input-group"><input type="input" name = "<?php echo $getAllSkuValue['SkuPercentRecord']['sku'] ?>___buffer" for="<?php echo $getAllSkuValue['SkuPercentRecord']['sku'] ?>" class="form-control subSourceBuffer_<?php echo str_replace('#','',str_replace('.','',$getAllSkuValue['SkuPercentRecord']['sku'])); ?>" value="<?php echo $getAllSkuValue['SkuPercentRecord']['buffer']; ?>" /></div></td>
															<?php 
																$storeNameBysku = '';
																$count = 0;
																$storePer = 0;
																foreach( $getStores as $storeValue  ) { 
																	
																	
																	$storename = $storeValue['Store']['store_name'];
																	$objectStoreval 	=  $storeCMP->getSoldByStore( $getAllSkuValue['SkuPercentRecord']['sku'] , $storeValue['Store']['store_name'] );
																	$objectStore = explode('____', $objectStoreval);
																	if( !empty($objectStore[1]) && $objectStore[1] > 0 )
																	{
																		$margin = "background-color:#26c6da";
																	}
																	else
																	{
																		$margin = "";
																	}
																	
															?>
															<td width="120px" title="<?php echo $storeValue['Store']['store_name']?>" style="<?php echo $margin; ?>">
																<div class="input-group">
																	<?php if( $objectStore[0] != 'Not Assign' ) { 
																		$count 		= 	$count + 1;
																		$storePer 	= 	$storePer + $objectStore[0];
																		?>
																		<input  type="text" 
																				value="<?php echo $objectStore[0]; ?>" 
																				class="form-control setvalue_<?php echo str_replace('.','_',str_replace('://','_',str_replace(' ','_',$storeValue['Store']['store_name']))); ?>  
																				subSource_<?php echo $storeValue['Store']['store_name'].'####'.$getAllSkuValue['SkuPercentRecord']['sku']; ?>
																				setmargen_<?php echo str_replace('.','_',str_replace('://','_',str_replace(' ','_',$storeValue['Store']['store_name'].'__'.$getAllSkuValue['SkuPercentRecord']['sku']))); ?>
																				commonText_<?php echo $j ?>
																				commonText"
																				for="subSource__<?php echo $storeValue['Store']['store_name'].'####'.$getAllSkuValue['SkuPercentRecord']['sku']; ?>" 
																				style=" <?php echo $customMark; ?>"
																				name ="<?php echo $getAllSkuValue['SkuPercentRecord']['sku'].'___'.$storename; ?>"
																				data-area = "commonParent_<?php echo $j; ?>"
																		/>
																		<?php 
																		$assignQty = floor(($getAllSkuValue['SkuPercentRecord']['current_stock'] - $getAllSkuValue['SkuPercentRecord']['buffer']) * $objectStore[0]/100);
																		?>
																		&nbsp;<label title="Assign Qty" ><?php echo '['.$assignQty.']'; ?></label>
																		&nbsp;<label title="OpenOrder Qty" ><?php echo '['.$getAllSkuValue['SkuPercentRecord']['open_order_qty'].']'; ?></label>
																		&nbsp;<label title="Selected Day Qty" ><?php echo '['.$getAllSkuValue['SkuPercentRecord']['select_day_sold'].']'; ?></label>
																		&nbsp;<label title="Assign Qty + OpenOrder Qty" ><?php echo '['.$getAllSkuValue['SkuPercentRecord']['open_order_qty'] + $assignQty .']'; ?></label>
																	<?php } else { ?>
																		<label><?php echo 'N/A'; //$objectStore[0]; ?></label>
																	<?php } ?>
																</div>
															</td>
															<?php unset($objectStore);  } ?>
															<td class="customTotal_<?php echo $j; ?>" ><?php echo $storePer; ?></td>
															<td >
																<?php if( $getAllSkuValue['SkuPercentRecord']['custom_mark'] == 0 ) { ?>
																<a data-target="" id='updateSingleValue' for="<?php echo $getAllSkuValue['SkuPercentRecord']['sku'].'####'.$getAllSkuValue['SkuPercentRecord']['custom_mark']; ?>" data-toggle="modal" href="#" class="btn btn-success " ><i class=""></i> Update </a>
																<?php } else { ?>
																<a data-target="" id='removeSingleValue' for="<?php echo $getAllSkuValue['SkuPercentRecord']['sku'].'####'.$getAllSkuValue['SkuPercentRecord']['custom_mark']; ?>" data-toggle="modal" href="#" class="btn btn-success " ><i class=""></i> Remove </a>
																<?php } ?>
															</td>
														</tr>
														<?php $i++; $j++; $customMarkValue = '';} ?>
													</tbody>
												</table>
												
										</div>
									</div>
									</form>	
								</div>
							</div>
						</div>
					<?php echo $this->element('footer'); ?>
				</div>
			</div>
		</div>
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<style type="text/css">

		#makeMeScrollable
		{
			width:100%;
			position: relative;
		}
		
		/* Replace the last selector for the type of element you have in
		   your scroller. If you have div's use #makeMeScrollable div.scrollableArea div,
		   if you have links use #makeMeScrollable div.scrollableArea a and so on. */
		#makeMeScrollable div.scrollableArea img
		{
			position: relative;
			float: left;
			margin: 0;
			padding: 0;
			/* If you don't want the images in the scroller to be selectable, try the following
			   block of code. It's just a nice feature that prevent the images from
			   accidentally becoming selected/inverted when the user interacts with the scroller. */
			-webkit-user-select: none;
			-khtml-user-select: none;
			-moz-user-select: none;
			-o-user-select: none;
			user-select: none;
		}
	</style>
		<script>	
			
			
$(document).ready(function() {
		$("#makeMeScrollable").smoothDivScroll({
			});
   $('#example3').dataTable({ 
		"iDisplayLength": 200,
		"aLengthMenu": [[200, 500, 1000, 2000, -1], [200, 500, 1000, 2000, "All"]]    
	});

	
$('body').on( 'click', '#bufferValue', function(){
	
	$('.dataTable > tbody  > tr').each(function() {
							
							var bufferValue	=	$('#bufferValueId').val();
							var getSku	=	$( this ).find('td:nth-child(2)').text();
							var updateValue =  $(this).find('#updateSingleValue').attr('for');
							var markArray	=	updateValue.split('####');
							if( markArray[1] != 1 )
							{
								getSku = getSku.replace('#', '');
								$( this ).find( '.subSourceBuffer_'+getSku ).val( bufferValue );
							}
					});
		});

$('body').on('click', '#updateValue', function(){
	
	var storeName	=	$( this ).attr('for');
	var storeValue	=	$('.updateSubSource_value_'+storeName).val();
	
	$('.dataTable > tbody  > tr ').each(function() {
							
							var updateValue =  $(this).find('#updateSingleValue').attr('for');
							var markArray	=	updateValue.split('####');
							if( markArray[1] != 1 )
							{
								$( this ).find( '.setvalue_'+storeName ).val( storeValue );
							}
					});
	});

$('body').on( 'click', '#updateSingleValue', function(){
	
	var getskuAndCustomMark	=	$( this ).attr('for');
	var getsku	=	getskuAndCustomMark.split('####');
	var bufferValue = $('.subSourceBuffer_'+getsku).val();
	var skuStoredetail = [];
	var count = 7;
	$('.dataTable > tbody  > tr.sku_'+getsku[0]+' > td').each(function() {
				
					skuStoredetail.push( $( this ).find('input:text').val() + '####' + $( this ).find('input:text').attr('for') );
					count++;	
				});
				var newskuStatus	=	$('.newsku_'+getsku).is(':checked');	
				$.ajax({
						'url'            : '/Virtuals/saveStoreSkuPercentage',
						'type'           : 'POST',
						'beforeSend' 	 : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },
						'data'           : { 
												skuStoredetail		:	skuStoredetail,
												bufferValue 		: 	bufferValue,
												newskuStatus 		: 	newskuStatus
											},                                    
						'success'        : function( msgArray )
												{ 
													$( ".outerOpac" ).attr( 'style' , 'display:none');
												}
					});
	});
	
	$( 'body' ).on('click', '.saveAllDetail', function(){
		
		    var setData	=	$(".virtual_feed_form").serialize();
		    $.ajax({
						'url'            : '/Virtuals/saveStoreSkuDetail',
						'type'           : 'POST',
						'beforeSend' 	 : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },
						'data'           : { setData },                                    
						'success'        : function( msgArray )
												{ 
													$( ".outerOpac" ).attr( 'style' , 'display:none');
												}
					});
		});
		
		$('body').on('click', '.marginValue', function(){
			
			 var marginValue = $('.setmarginValue').val();
			 $.ajax({
						'url'            : '/Virtuals/saveMarginValue',
						'type'           : 'POST',
						'beforeSend' 	 : function(){ 
							$( ".outerOpac" ).attr( 'style' , 'display:block'); 
							},
						'data'           : { marginValue },                                    
						'success'        : function( data )
												{ 
													$( ".outerOpac" ).attr( 'style' , 'display:none');
													var json = JSON.parse(data);
                                                    var getData = json.data;
                                                    for (var i = 0, max = getData.length; i < max; i++)
                                                             {
																 $('.dataTable > tbody  > tr').each(function() {
																		var result= $(this).find('td:nth-child(2)').html();
																		if( result == getData[i].SkuPercentRecord.sku )
																		{
																 
																		$('.setmargen_'+getData[i].SkuPercentRecord.store_alies+'__'+getData[i].SkuPercentRecord.sku).parent().attr('style','background-color:#26c6da');
																	}
																 });
																 
															 }
												}
					});
			});
			
			$('body').on('click', '#removeSingleValue', function(){
			
			var remove = $(this).attr('for');
			$.ajax({
						'url'            : '/Virtuals/removeCustomMark',
						'type'           : 'POST',
						'beforeSend' 	 : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },
						'data'           : { remove },                                    
						'success'        : function( data )
												{ 
													$( ".outerOpac" ).attr( 'style' , 'display:none');
												}
					});
			});
			
		

});

/*
function delayClass()
	{
		$('.dataTable > tbody  > tr > td').each(function()
		{
				if( $( this ).find('input:hidden').val() == 1 )
				{
					$( this ).find('input:checkbox').parent().addClass('checked');
					$( this ).find('input:checkbox').attr('checked', 'checked');
					$( ".outerOpac" ).attr( 'style' , 'display:none');
				}
		});

	}*/
	
	$('body').on( 'click','.createFeed', function(){
		
			$.ajax({
					'url'            : '/Virtuals/creatFeedSheet',
					'type'           : 'POST',
					'beforeSend' 	 : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },
					'data'           : { },                                    
					'success'        : function( data )
											{ 
												$( ".outerOpac" ).attr( 'style' , 'display:none');
											}
						});
		
		});
		
		$( ".commonText" ).keyup(function() {
			var data = $( this ).attr('data-area');
			var splitData = data.split('_');
			var count = 0;
			$('.commonText_'+splitData[1]).each(function(index,item)
			{
				count = count + parseInt( $(item).val() == '' ? 0 : $(item).val() );
			});
			$('.customTotal_'+splitData[1]).text( count );
		});
		
	  

</script>
