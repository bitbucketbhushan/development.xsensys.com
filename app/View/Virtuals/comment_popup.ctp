<div class="modal fade  qrCodeGlobel tabindex="-1" role="dialog" id="pop-up-qrcode" aria-labelledby="myLargeModalLabel-4" aria-hidden="true" >
	<div class="modal-dialog modal-dialog_qrcode modal-lg">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Comment</h4>
         </div>
         <div class="modal-body bg-grey-100">
            <div class="panel no-margin" style="clear:both">
     <div class="panel-body padding-top-20" >
     <div class="row">
		   <div class="col-lg-8 col-lg-offset-2">
					<div class="panel-body padding-bottom-40">
						        <div class="col-lg-3">
						        <label>Comment</label>
						        </div>
							<div class="col-lg-7">                                                                                         
								<?php
									$comment	=	($productBarcode['Product']['comment'] != '') ? $productBarcode['Product']['comment'] : '';											
									print $this->form->input( 'Product.comment', array( 'type'=>'textarea','class'=>'chooseStore form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'value'=> $comment));	
								?>
							</div>                                      
		
						</div>
					</div>
				</div>
		   <div class="row">
	     <div class="col-lg-12" style="text-align:center; padding-bottom: 7px;"> 
		<a class="save_comment btn btn-success" for="<?php print $productBarcode['Product']['id']; ?>" href="javascript:void(0);"><i class=""></i> Save </a>
         </div>
        </div>
       </div>
      </div> 
     </div>
    </div>
   </div>
  </div>
 </div>


<script>
	
	 $(document).ready(function()
	   {
		   $('#pop-up-qrcode').modal('show');
		   $('.qrCodeGlobel').on('shown.bs.modal', function ()
				   {
					  $('#QrcodeGenerate').focus()
				   })
	   });
		
	
	
	
	$('body').on( 'click', '.save_comment', function(){
		
		var productId		=	$(this).attr('for');
		var productComment	=	$('#ProductComment').val();
		
		$.ajax({
					url     	: '/Virtuals/setProductComment',
					type    	: 'POST',
					data    	: {  
									productId : productId,
									productComment : productComment
								   },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					},  			  
					success 	:	function( popUpdata  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.close').click();
						$('.comment_status_'+productId).text( productComment );
						if( popUpdata == 1 )
						{
							swal('Comment Save successfully.');	
						}
						else
						{
							swal('Opps there is an error.');
						}
					}              
				});
				
		});
							
		
	
</script>




