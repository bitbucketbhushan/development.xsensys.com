<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Generate Barcode</h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                                <div class="panel-body padding-bottom-40">
										<div class="form-group">                                        
                                        <div class="col-lg-7">                                                                                         
											<?php												
												print $this->form->input( 'Barcode.generate', array( 'type'=>'text','class'=>'chooseStore form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );	
											?>
                                        </div>                                      
                                      </div>
									</div>
								</div>
							</div>
						</div>
                    <div class="add_get_sku">
                </div>
            </div>
        </div>
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>


<script>
	
	$('#BarcodeGenerate').focus();
	
	$(document).keypress(function(e)
	{
	   if(e.which == 13) {
		   
		   submitBarcodeNumber();
			
		}
	});
	
	function submitBarcodeNumber()
	{
		
		var barcodeNumber    = $('#BarcodeGenerate').val();          
		$.ajax(
		{
			'url'            : '/Virtuals/generateBarcode',
			'type'           : 'POST',
			'data'           : { barcodeNumber : barcodeNumber },                                    
			'beforeSend'     : function()
			{
							//$('.loading-image').show();
			},
			'success'        : function( msgArray ){ swal('Barcode generated Successfully.'); }
		});
	}
	
</script>




