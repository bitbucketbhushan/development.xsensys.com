
<div class="modal fade barcodeCodeGlobel tabindex="-1" role="dialog" id="pop-up-barcode" aria-labelledby="myLargeModalLabel-4" aria-hidden="true" >
	<div class="modal-dialog modal-dialog_barcode modal-lg">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Barcode Generater</h4>
         </div>
         <div class="modal-body bg-grey-100">
            <div class="panel no-margin" style="clear:both">
     <div class="panel-body padding-top-20" >
     <div class="row">
		   <div class="col-lg-8 col-lg-offset-2">
					
							   <div class="row">
								<label class="col-lg-3"> Barcode : </label>
								<div class="col-lg-4">  
									<label class="label_barcode"><?php echo $this->Common->getLatestBarcode($productComment[0]['ProductDesc']['barcode']); ?></label>
								</div>
								</div>                                                                         
					

						  
						  <div class="row">                         
							  <label class="col-lg-3"> Quantity : </label>               
							<div class="col-lg-4">                                                                                         
								<div class="form-group">  
								<?php												
									print $this->form->input( 'Barcode.generate', array( 'type'=>'hidden','value'=> $productBarcode[0]['PurchaseOrder']['purchase_barcode']) );	
									print $this->form->input( 'Barcode.qty', array( 'type'=>'text','class'=>'chooseStore form-control', 'data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );	
									//print $this->form->input( 'Barcode.productdescid', array( 'type'=>'hidden', 'value' =>$productBarcode['ProductDesc']['id'] ) );
								?>
								</div>
							</div>                                      
						  </div>
						</div>
					</div>
				</div>
		   <div class="row">
         <div class="col-lg-12" style="text-align:center; padding-bottom: 7px;"> 
         </div>
        </div>
       </div>
      </div> 
     </div>
    </div>
   </div>
  </div>
 </div>


<script>
	
	 $(document).ready(function()
	   {
		   var poItemID = '<?php echo $productBarcode[0]['PurchaseOrder']['id']; ?>';
		   $('#pop-up-barcode').modal('show');
		   $('.barcodeCodeGlobel').on('shown.bs.modal', function ()
				   {
					  $('#BarcodeQty').focus()
				   })
				   var barcodeQty	=	$.trim($('.po_warehouse_qty_'+poItemID).text());
				   $('#BarcodeQty').val(barcodeQty);
				   
				   
	   });
	
	//$('#BarcodeGenerate').focus();
	
	 $('#BarcodeQty').bind('keypress', function(e)
	{
	   if(e.which == 13) {
		   if( $('#BarcodeQty').val()  != '' )
		   {
			 submitBarcodeNumber();
		   }
		   else
		   {
			   swal("Cancelled", "Please fill the quantity :)", "error");
		   }
			
		}
	});
	
	function submitBarcodeNumber()
	{
		var labelBarcode	=	$('.label_barcode').text();
		var barcodeNumber    	= 	$('#BarcodeGenerate').val();
		var quantity    		= 	$('#BarcodeQty').val();  
		var productdescId    	= 	'';  
		
		if( labelBarcode != barcodeNumber )
		{
			/*if( labelBarcode != '' )
			{
				swal("Please enter the same barcode.", "" , "error");
				return false;
			}*/
		}
		
		$.ajax(
		{
			'url'            : '/Virtuals/checkBarcode',
			'type'           : 'POST',
			'data'           : { barcodeNumber : barcodeNumber },                                    
			'success'        : function( msgArray )
									{ 	
										var arr = msgArray.split('##$##');
										$('#BarcodeGenerate').val( arr[1] );
										if( arr[0] == 1 )
										{
											swal({
													title: "Are you sure?",
													text: "You want to generate again !",
													type: "warning",
													showCancelButton: true,
													confirmButtonColor: "#DD6B55",
													confirmButtonText: "Yes, Generate it!",
													cancelButtonText: "No, cancel!",
													closeOnConfirm: false,
													closeOnCancel: false
												},
												function(isConfirm)
												{
													if (isConfirm)
													{
														$.ajax({
															
																'url' 					: 	'/Virtuals/generateBarcode',
																'type'					: 	'POST',
																'data' 					: 	{ barcodeNumber : barcodeNumber, quantity : quantity, productdescId : productdescId },
																'success' 				  : function( msgArray )
																	{
																		swal("Barcode generated successfully.", "" , "success");
																		$( ".close" ).click();
																	} 
																
															});
													}
													else
													{
														swal("Cancelled", "Barcode generated Successfully :)", "error");
														$('.barcodeCodeGlobel').on('shown.bs.modal', function ()
														   {
															  $('#BarcodeGenerate').focus();
														   })
														
													}
											});
										}
										else
										{
											$.ajax({
															
																'url' 					: 	'/Virtuals/generateBarcode',
																'type'					: 	'POST',
																'data' 					: 	{ barcodeNumber : barcodeNumber, quantity: quantity, productdescId: productdescId },
																'success' 				  : function( msgArray )
																	{
																		swal("Barcode generated successfully.", "" , "success");
																		$( ".close" ).click();
																	} 
																
															});
										}
									}
		});
	}
	
</script>




