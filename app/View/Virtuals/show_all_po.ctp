<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Show All Un-Locked Po</h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
				<div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
							<?php if($this->Session->read('Auth.User.role_type') == 2) { ?>
							<li>
								<a class="switch btn btn-success" href="/Virtuals/uploadPoView" data-toggle="modal" data-target=""><i class=""></i> Upload PO </a>
							</li>
							<?php } ?>
							<li>
								<a class="switch btn btn-success" href="/Virtuals/showAllLockedPo" data-toggle="modal" data-target=""><i class=""></i> Show Locked Po </a>
							</li>
							<li>
								<a class="switch btn btn-success" href="/Virtuals/showAllPo" data-toggle="modal" data-target=""><i class=""></i> Show Un-Locked Po </a>
							</li>
							<li>
							<a class="switch btn btn-success generateDemoPofile" href="/Virtuals/showAllRecivedPo" data-toggle="modal" data-target=""><i class=""></i> Recived Po</a>
							</li>
							<li>
								<a class="switch btn btn-success generateDemoPofile" href="/Virtuals/prepareDemoCsv" data-toggle="modal" data-target=""><i class=""></i> Sample Po</a>
							</li>
						</ul>
					</div>
				</div>	
			</div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
					         <div class="col-lg-12">
                                  <div class="panel-body">
								  <div class="row">
								  <div class="col-lg-8 col-lg-offset-2">
									<div class="form-group">
											                                           
												<?php
													print $this->Form->create('ScanInvoice', array('type' => 'file','url' => '/Virtuals/UploadScanningFilesForPo'));
													if( count( $getpoList ) > 0 )
													print $this->form->input( 'Po.choosePo', array( 'type'=>'select', 'empty'=>'Choose Po','options'=>$getpoList,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
												?> 
										 </div>
								  </div>
								  </div>
								  <div class="row">
								  <div class="col-lg-6 col-lg-offset-3">
									<div class="form-group col-lg-12 invoicenoSave">
											 <label class="col-lg-3" >Invoice No.</label>
											<div class="col-lg-7">                                            
												<?php
													print $this->form->input( 'Po.invoiceNo', array( 'type'=>'text', 'class'=>'form-control', 'div'=>false, 'label'=>false, 'required'=>false ) );
												?>  
											</div>
											<div class="col-lg-2">
											<a class="saveInvoiceNumber switch btn btn-success padding-left-20 padding-right-20" href="javascript:void(0);" data-toggle="modal" data-target=""><i class=""></i> Save </a>
											</div>
										 </div>
								  </div>
								  </div>
								  
								  <div class="row">
								  <div class="col-lg-6 col-lg-offset-3">
									<?php 
									echo $this->Form->create('ScanInvoice', array('type' => 'file','url' => '/Virtuals/UploadScanningFilesForPo'));
									?>
									<div class="upload_multy_file col-lg-12 form-group">
									<div class="col-lg-10">
									<div class="input-group">
													<span class="input-group-btn">
														<span class="btn btn-primary btn-file">
															Browse…  <?php
																		echo $this->Form->input('files.', array('type' => 'file', 'multiple','div'=>false,'label'=>false,'class'=>'', 'required'=>false));
																	  ?>
														</span>
													</span>
														<input type="text" placeholder="No file selected" readonly="" class="form-control">
													
											</div>
											</div>
									<div class="col-lg-2"><?php
																				
										echo $this->Form->button('Upload', array(
														'type' => 'submit',
														'escape' => true,
														'class'=>'add_brand btn btn-success btn-dark padding-left-15 padding-right-15'
														 ));
									?></div>
									</div>
								  
								  </div>
								  </div>
										 
									</div>
									
								</div>
							</div>
							
							
							
							
						</div>
						
						<div class="row">
								  <div class="col-lg-12">
									<div class="success col-lg-12 padding-left-lg" style="display:none;" ></div>
									<div class = "set_search_po" ></div>
								  
								  </div>
								  </div>
						
                </div>
            </div>
        </div>
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
						
	
<script>
	
	$(document).ready(function()
	{
		$( '.upload_multy_file' ).hide();
		$('.invoicenoSave').hide();
		
		$("body").on("change", "#PoChoosePo", function ()
		{
			
			var poNameold				=	$( '#PoChoosePo' ).find(":selected").text();
			var poName = poNameold.split("("); 
		
			$.ajax(
			{
				url     	: '/Virtuals/getSelectedPo',
				type    	: 'POST',
				data    	: { poName : poName[0] },
				beforeSend 	: function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },  
				success 	: function( msgArray  )
					{
						$('.set_search_po').html( msgArray );
						$( ".outerOpac" ).attr( 'style' , 'display:none');
					}                
			}); 
		});	
	});
	
	$( 'body' ).on( 'click', '.lockUnlockPo', function(){
		
		var poNameold				=	$( '#PoChoosePo' ).find(":selected").text();
		var poName 					= 	poNameold.split("(");
		
		var status				=	$( '.lockUnlockPo' ).attr("for");
			if( poName == '' )
			{
				swal( 'Please select store' );
				return false;
			}
			$.ajax({
				url     	: '/Virtuals/markLoclUnlock',
				type    	: 'POST',
				data    	: { poName : poName[0], status : status },  			  
				success 	:	function( data  )
				{
					//location.reload();
				}                
			});	
		
		});
		
		$( 'body' ).on('click', '.saveInvoiceNumber', function(){
			
			var invoiceNo		=	$( '#PoInvoiceNo' ).val();
			var poId			=	$( '#PoChoosePo' ).find(":selected").val();
		
			if( invoiceNo == '' || poId == '')
			{
				swal( 'Please fill invoice no and select po.' );
				return false;
			}
			$.ajax({
				url     	: '/Virtuals/saveInvoiceNumber',
				type    	: 'POST',
				data    	: { poId : poId, invoiceNo : invoiceNo },  			  
				success 	:	function(  data  )
				{
					if( data == 1 )
					{
						//swal( 'Invoice no. save successfully.' );
						$('.invoicenoSave').hide();
						location.reload();
					}
					else if( data == 2 )
					{
						swal( 'Invoice no. already exist.' );
					}
					else
					{
						swal( 'error in save.' );
					}
				  	
				}              
			});
			
			});
	
	
</script>
