<div class="row">
<div class="col-lg-6 col-lg-offset-3">
<div class="level2menu">
	<div class="navbar-header">
		<ul class="nav navbar-nav pull-left">
			<li>
				<a data-target="" data-toggle="modal" href="#" class="deletePo btn btn-info"><i class=""></i>Delete Po</a>
			</li>
			<li>
				<a data-target="" data-toggle="modal" href="#" class="addSkuInPo btn btn-info"><i class=""></i>Add SKU</a>
			</li>
			<li>
				<?php if( isset( $getPo['Po']['status'] ) && $getPo['Po']['status'] == 1 ) {
							if($getPo['Po']['invoice_no'] != '' ) {
				 ?>
				<a data-target="" data-toggle="modal" for="0" href="#" class="lockUnlockPo btn btn-info"><i class=""></i>Lock</a>
				<?php } } else { ?>
				<a data-target="" data-toggle="modal" for="1" href="#" class="lockUnlockPo btn btn-danger"><i class=""></i>Un-Lock</a>
				<?php } ?>
			</li>
			<li>
				<a data-target="" data-toggle="modal" href="#" class="downloadPoFile btn btn-info"><i class=""></i>Generate CSV</a>
			</li>
			
			<div class="btn-group">
				  <button type="button"  href="javascript:void(0);" class="btn btn-info no-margin">Invoice</button>
				  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="caret"></span>
					<span class="sr-only">Toggle Dropdown</span>
				  </button>
				
				  <ul class="dropdown-menu scrollable-menu" role="menu">
					  <?php 
							if( count($scanInvoiceData) > 0 ) {
								foreach( $scanInvoiceData as $value ) { 
									$pdfPath = '/img/scanInvoice/'; 
									$file = $value['ScanInvoice']['file_name'];
									$date = $value['ScanInvoice']['date'];
									$date = date('d-M-Y', strtotime($date));
					  ?>
						<li><a href="<?php echo $pdfPath.$file; ?>" target ="_blank" ><?php echo $value['ScanInvoice']['file_name'].'  ( '. $date .' )'; ?></a></li>
					  <?php } } else { ?>
						<li><a>No Data</a></li>
					  <?php } ?>
				  </ul>
			</div>
		</ul>
	</div>
</div>

</div>
</div>

<table class="table table-bordered table-striped dataTable" >
	<tr>
		<th width=1%>S.No</th>
		<th width=10%>PO Name</th>
		<th width=10%>SKU</th>
		<th width=10%>Barcode</th>
		<th width=8%>Code</th>
		<th width=20%>Desc</th>
		<th width=10%>loc</th>
		<th width=3%>Qty</th>
		<th width=3%>Count</th>
		<th width=3%>WH In</th>
		<th width=7%>Comment</th>
		<th width=17%>Action</th>
	</tr class="sku_detail" >
		
	<?php 
		$i = 1; 
		foreach($getPodetail as $getPodetailValue ) {
		$id = $getPodetailValue['PurchaseOrder']['id'];
	?>
		 <tr class="row_<?php echo $id; ?>" >
			<td><?php echo $i; ?></td>
			<td class="po_name_<?php echo $id; ?>" ><?php echo $getPodetailValue['PurchaseOrder']['po_name']; ?></td>
			<td class="poSku po_sku_<?php echo $id; ?>" for="<?php echo $getPodetailValue['PurchaseOrder']['purchase_sku']; ?>" id="<?php echo $id; ?>" ><?php echo $getPodetailValue['PurchaseOrder']['purchase_sku']; ?></td>
			<td class="po_barcode_<?php echo $id; ?>">
				<?php echo $this->Common->getLatestBarcode($getPodetailValue['PurchaseOrder']['purchase_barcode']); ?>
			</td>
			<td class="po_pur_code_<?php echo $id; ?>" ><?php echo $getPodetailValue['PurchaseOrder']['purchase_code']; ?></td>
			<td class="po_desc_<?php echo $id; ?>" title="<?php echo $getPodetailValue['PurchaseOrder']['purchase_description'] ?>" ><?php echo substr($getPodetailValue['PurchaseOrder']['purchase_description'], 0, 30); ?></td>
			<!--<td id="po_desc_<?php echo $id; ?>" class="po_desc_<?php echo $getPodetailValue['PurchaseOrder']['purchase_sku']; ?>" title="<?php echo $getPodetailValue['PurchaseOrder']['purchase_description'] ?>" ></td>-->
			<td class="" title="" ><?php if(isset($locbar[$getPodetailValue['PurchaseOrder']['purchase_barcode']]['location'])){ echo str_replace(',','<br>',$locbar[$getPodetailValue['PurchaseOrder']['purchase_barcode']]['location']); } else { echo 'No Location Found'; } ?></td>
			<td class="po_qty_<?php echo $id; ?>" ><?php echo $getPodetailValue['PurchaseOrder']['purchase_qty']; ?></td>
			<td class="po_count_<?php echo $id; ?>" >
				 <input type="text" value="<?php echo $getPodetailValue['PurchaseOrder']['purchase_count']; ?>" class="countQty countQty_<?php echo $id; ?> form-control border-green-300 bg-green-50 color-green-800">
				 <a href="javascript:void(0);" title="Edit" for="<?php echo $id; ?>" class="saveCountQty btn btn-xs hidden"></a>
			</td>
			<td class="po_warehouse_qty_<?php echo $id; ?>">
				<?php 
				//App::import( 'Controller' , 'Virtuals' );
				//$getMathod = new VirtualsController();
				//echo $getMathod->getWerehouseFillQty( $getPodetailValue['PurchaseOrder']['po_name'], $getPodetailValue['PurchaseOrder']['purchase_sku'] );
				if(isset($locbar[$getPodetailValue['PurchaseOrder']['purchase_barcode']]['qty'])){ echo $locbar[$getPodetailValue['PurchaseOrder']['purchase_barcode']]['qty']; };
				 ?>
			</td>
			
			<td>
			<p class="po_comment_<?php echo $id; ?>" >
				<?php echo $getPodetailValue['PurchaseOrder']['comment']; ?>
			</p>
			<p class="seller_comment_<?php echo $id; ?>" >
				<?php echo $getPodetailValue['PurchaseOrder']['seller_comment']; ?>
			</p>
			</td>
			
			<td>
				<a href="javascript:void(0);" title="Generate Barcode" for="<?php echo $id; ?>" class="generateBarcode btn btn-success btn-xs"><i class="fa fa-print"></i></a>
				<a href="javascript:void(0);" title="Edit" for="<?php echo $id; ?>" class="editPurchaseOrderSku btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>
				<a href="javascript:void(0);" title="Delete" for="<?php echo $id; ?>" class="deletePurchaseOrderSku btn btn-danger btn-xs"><i class="ion-minus-circled"></i></a>
				<a href="javascript:void(0);" title="Seller Comment" for="<?php echo $id; ?>" class="sellerCommetAboutSku btn btn-success btn-xs"><i class="fa fa-comment"></i></a>
				<div class="btn-group">
					  <!--<button type="button"  href="javascript:void(0);" class="downloadPickList btn btn-success no-margin">Comments</button>-->
					  <button type="button" class="btn btn-success dropdown-toggle no-padding width-25" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					  </button>
					  <ul class="dropdown-menu scrollable-menu po-dropdownr-right" role="menu" for="<?php echo $id; ?>" >
						<?php foreach( $getComment as $index => $value ) { ?>
							<li value="<?php echo $value['PoComment']['id']; ?>" ><a href="javascript:void(0)"><?php echo $value['PoComment']['comment']; ?></a></li>
						<?php } ?>
						<li role="separator"></li>
					  </ul>
				</div>
			</td>
		</tr>
	<?php $i++; } ?>


</table>
<div class="showPopupForEditSku"></div>
<div class="showPopupForAddSku"></div>
<div class="showPopupForSellerComment"></div>
<div class="showPopupForBarcode"></div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

<script>
	
				/*$('.poSku').each(function() {
						var currentSku	= $(this).attr('for');
						var id	= $(this).attr('id');
						//alert(id);
						$.ajax({
								url     	: '/Virtuals/getLocationSkuQty',
								type    	: 'POST',
								dataType	: 'json',
								data    	: { currentSku : currentSku },  			  
								success 	:	function( dataMessage  )
								{
									var count = dataMessage.message.length;
									var str = '';
									for( var i = 0; i < count; i++ )
									{
										str += dataMessage.message[i].BinLocation.bin_location+"<br>";
										
									}
									$('#po_desc_'+id).html( str );
									
								}                
						});
					});*/
	
	
	$( 'body' ).on( 'click', '.downloadPoFile', function()
	{
		var poName				=	$( '#PoChoosePo' ).find(":selected").text();
		if( poName == '' )
		{
			swal( 'Please select store' );
			return false;
		}
		$.ajax({
				url     	: '/Virtuals/prepareCsv',
				type    	: 'POST',
				data    	: { poName : poName },  			  
				success 	:	function( data  )
				{
					 window.open(data,'_self' );
				}                
			});				
	});
	
	$(document).ready(function()
	{
		var invoNo = '<?php echo $getPo['Po']['invoice_no']; ?>';
			if( invoNo != '' )
			{
				$('.invoicenoSave').hide();
				$( '.upload_multy_file' ).show();
			}
			else
			{
				$('.invoicenoSave').show();
				$( '.upload_multy_file' ).show();
			}
			$('.dataTable > tbody  > tr').each(function(){
				
					var uploadedQuantity	=	parseFloat($( this ).find('td:nth-child(8)').text());
					var inWarehouse			=	parseFloat($( this ).find('td:nth-child(10)').text());
					if( inWarehouse > uploadedQuantity)
					{
						$( this ).attr( 'style', 'background:#f8bbd0' );
					}
					else if( inWarehouse < uploadedQuantity)
					{
						$( this ).attr( 'style', 'background:#f0f4c3' );
					}
					else
					{
						$( this ).attr( 'style', 'background:#e8f5e9' );
					}
				});
				
				/*$('.dataTable > tbody  > tr').each(function(){
				
					var uploadedQuantity	=	parseFloat($( this ).find('td:nth-child(7)').text());
					var inWarehouse			=	parseFloat($( this ).find('td:nth-child(8)').text());
					if( inWarehouse > uploadedQuantity)
					{
						$( this ).attr( 'style', 'background:#f8bbd0' );
					}
					else if( inWarehouse < uploadedQuantity)
					{
						$( this ).attr( 'style', 'background:#f0f4c3' );
					}
					else
					{
						$( this ).attr( 'style', 'background:#e8f5e9' );
					}
				});*/
				
	});
	
	$( 'body' ).on( 'click', '.editPurchaseOrderSku', function(){
	
		var poSkuId = $( this ).attr('for')
		
		$.ajax({
					url     	: '/Virtuals/editPoSkuPopUp',
					type    	: 'POST',
					data    	: { poSkuId : poSkuId },  			  
					success 	:	function( data  )
					{
						$('.showPopupForEditSku').html( data );
					}                
			  });
		
		});
	$( 'body' ).on( 'click', '.editSkudetail', function(){
		
				var purchaseQty			=	$('.purchaseQty').val();
				var purchasePrice		=	$('.purchasePrice').val();
				var poskuID				=	$('.poskuID').val();
				if(purchasePrice == undefined){
					purchasePrice = '';
				}
				if(purchaseQty == ''){
					alert( 'Please enter quantity and price.' );
					return false;
				}
			$.ajax({
					url     	: '/Virtuals/editPoSku',
					type    	: 'POST',
					data    	: { 
									purchaseQty : purchaseQty,
									purchasePrice : purchasePrice,
									poskuID : poskuID
								  },  			  
					success 	:	function( msgData  )
					{
						var json = JSON.parse(msgData);
                        var getData = json.data;
						var purchaseQty 	=  getData.purchase_qty;
						var id 				=  getData.id;
						$('.po_qty_'+id).text( purchaseQty );
						$( ".close" ).click();
						setInterval(function(){
										$(".row_"+id).attr( 'style', 'background-color:#81c784;' );
									},1000)
						
					}                
			  });
		
		});
		
		$( 'body' ).on( 'click', '.deletePurchaseOrderSku', function(){
			
			var poSkuId		=	$( this ).attr( 'for' );
			var warehouseQty	=	$('.po_warehouse_qty_'+poSkuId).text();
			
				swal({
					title: "Are you sure?",
					text: "You want to delete po sku !",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						$.ajax({
									url     	: '/Virtuals/deletePoSkuPopUp',
									type    	: 'POST',
									data    	: { poSkuId : poSkuId },  			  
									success 	:	function( data  )
									{
										$( '.success' ).html('<p>Po sku deleted successfully.</p>')
										$( '.success' ).addClass('alert alert-success').fadeIn(1000).delay(4000).fadeOut(1000);
										$(".row_"+data).fadeOut(2000).remove();
									}                
							  });
						
						      
					}
					else
					{
						swal("Cancelled", "Your sku is safe :)", "error");
					}
				});
			});
			
		$( 'body' ).on( 'click', '.addSkuInPo', function(){
			
			var choosePo		=	$( '#PoChoosePo' ).find(":selected").text();
				$.ajax({
					url     	: '/Virtuals/addPoSkuPopUp',
					type    	: 'POST',
					data    	: { choosePo : choosePo },  			  
					success 	:	function( data  )
					{
						$('.showPopupForAddSku').html( data );
					}                
			  });
		});
		
	  $( 'body' ).on( 'click', '.addSkudetail', function(){
			
					var poName				=	$('.poName').val();
					var productSku			=	$('.productsku').val();
					var productCode			=	$('.purchaseCode').val();
					var productDesc			=	$('.productDesc').val();
					var purchaseQty			=	$('.purchaseQty').val();
				
					if( poName == 'Choose Po' )
					{
						swal( 'Please select po.' );
						return false;
					}
					if(productCode == '' ||  purchaseQty == '')
					{
						swal( 'Please fill po code & quantity.' );
						return false;
					}
					$.ajax({
								url     	: '/Virtuals/addPoSku',
								type    	: 'POST',
								data    	: { 
												poName : poName, 
												productSku : productSku, 
												productCode : productCode, 
												productDesc : productDesc, 
												purchaseQty : purchaseQty 
											  },  			  
								success 	:	function( data  )
								{
									if( data == 1 )
									{
										swal( 'Sku save .' );
										$( ".close" ).click();
									}
									else if( data == 2 )
									{
										swal( 'Sku already exist in po.' );
									}
									else if( data == 3 )
									{
										swal( 'Sku does not exist in our product.' );
									}
								}                
							});
				
		  
		  
		  });
		  
		  $('body').on( 'click', '.deletePo', function(){
			  
			    var choosePoId		=	$( '#PoChoosePo' ).val();
			    var choosePo		=	$( '#PoChoosePo' ).find(":selected").text();
				if( choosePo == 'Choose Po' )
					{
						swal( 'Please select po.' );
						return false;
					}
					
					
					swal({
					title: "Are you sure?",
					text: "You want to delete po !",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
							
							$.ajax({
									url     	: '/Virtuals/deletePoAllRecord',
									type    	: 'POST',
									data    	: { 
													choosePoId : choosePoId, 
													choosePo : choosePo 
												  },  			  
									success 	:	function( data  )
									{
										if( data == 1 )
										{
											location.reload();
										}
									}                
							  });
							
					}
					else
					{
						swal("Cancelled", "Your sku is safe :)", "error");
					}
				});
			});
		
		$(".countQty").focusout(function(){
			
			var poItemId	=	$( this ).next().attr('for');
			var poItemQty	=	$('.countQty_'+poItemId).val();
			$.ajax({
							url     	: '/Virtuals/saveSkuCount',
							type    	: 'POST',
							data    	: { 
											poItemId  : poItemId, 
											poItemQty : poItemQty 
										  },
							//beforeSend  : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },    			  
							success 	:	function( data  )
							{
								if( data == 1 )
								{
									$('.po_count_'+poItemId).css("background-color", "green");
									//$( ".outerOpac" ).attr( 'style' , 'display:none');
									
								}
								else
								{
									swal( 'Opps! error' );
								}
							}                
					  });
			
			
			
		});
		
		/*$( 'body' ).on('click', '.saveCountQty', function(){
			
				var poItemId	=	$( this ).attr('for');
				var poItemQty	=	$('.countQty_'+poItemId).val();
			
				$.ajax({
							url     	: '/Virtuals/saveSkuCount',
							type    	: 'POST',
							data    	: { 
											poItemId  : poItemId, 
											poItemQty : poItemQty 
										  },  			  
							success 	:	function( data  )
							{
								if( data == 1 )
								{
									swal( 'Quantity count successfully' );
								}
								else
								{
									swal( 'Opps! error' );
								}
							}                
					  });
			});*/
			
			$("body").on("click", ".dropdown-menu li", function ()
					{
						
						
						var commentId	=	$( this ).val();
						var comment		=	$( this ).text();
						var poSkuId 	=	$( this ).parent().attr('for');
						
						$.ajax({
							url     : '/Virtuals/saveSkuComment',
							type    : 'POST',
							data    : {
										commentId	:	commentId, 
										comment		:	comment,
										poSkuId		:	poSkuId
									  },
							'beforeSend' : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },  
							success :	function( msgArray  )
								{
									$( ".outerOpac" ).attr( 'style' , 'display:none');
									$('.po_comment_'+poSkuId).text( comment );
								}                
						}); 
						
					});	
		
		
		$( 'body' ).on( 'click', '.sellerCommetAboutSku', function(){
			
			var poSkuId		=	$( this ).attr( 'for' );
			var warehouseQty	=	$('.po_warehouse_qty_'+poSkuId).text();
			
				
						$.ajax({
									url     	: '/Virtuals/selletCommentPopup',
									type    	: 'POST',
									data    	: { poSkuId : poSkuId },  			  
									success 	:	function( data  )
									{
										$('.showPopupForSellerComment').html( data );
									}                
							  });
					});
					//sellerComment
					
					
			$( 'body' ).on( 'click', '.saveSellerComment', function(){
		
				var sellerComment		=	$('.sellerComment').val();
				var poskuID				=	$('.poskuID').val();
				$.ajax({
							url     	: '/Virtuals/saveSellerComment',
							type    	: 'POST',
							data    	: { 
											sellerComment : sellerComment,
											poskuID : poskuID
										  },  			  
							success 	:	function( msgData  )
							{
								$( '.seller_comment_'+poskuID ).html( '<p>'+sellerComment+'</p>' );
								$( '.close' ).click();
							}                
						});
					});
					
			$( 'body' ).on( 'click', '.generateBarcode', function(){
		
				var poSkuId		=	$( this ).attr( 'for' );
				$.ajax({
							url     	: '/Virtuals/brcodeGenerationPopup',
							type    	: 'POST',
							data    	: { 
											poskuID : poSkuId
										  },  			  
							success 	:	function( msgData  )
							{
								$('.showPopupForBarcode').html( msgData );
							}                
						});
					});
					
			$( 'body' ).on( 'click', '.printBarcode', function(){
		
				var poSkuId			=	$( '.poskuID' ).val();
				var purchaseQty		=	$( '.purchaseQty' ).val();
				var barcode			=	$( '.poskuBarcode' ).val();
				$.ajax({
							url     	: '/Virtuals/printSkuBarcode',
							type    	: 'POST',
							data    	: { 
											poskuID : poSkuId,
											purchaseQty : purchaseQty,
											barcode : barcode
										  },  			  
							success 	:	function( data  )
							{
								$( '.close' ).click();
							}                
						});
					});
					
					
	
</script>


