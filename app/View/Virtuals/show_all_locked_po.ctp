<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Show All Locked Po</h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
				<div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
							<li>
								<a class="switch btn btn-success" href="/Virtuals/uploadPoView" data-toggle="modal" data-target=""><i class=""></i> Upload PO </a>
							</li>
							<?php if($this->Session->read('Auth.User.role_type') == 2) { ?>
							<li>
								<a class="switch btn btn-success" href="/Virtuals/showAllLockedPo" data-toggle="modal" data-target=""><i class=""></i> Show Locked Po </a>
							</li>
							<?php } ?>
							<li>
								<a class="switch btn btn-success" href="/Virtuals/showAllPo" data-toggle="modal" data-target=""><i class=""></i> Show Un-Locked Po </a>
							</li>
							<li>
							<a class="switch btn btn-success generateDemoPofile" href="/Virtuals/showAllRecivedPo" data-toggle="modal" data-target=""><i class=""></i> Recived Po</a>
							</li>
							<li>
								<a class="switch btn btn-success generateDemoPofile" href="/Virtuals/prepareDemoCsv" data-toggle="modal" data-target=""><i class=""></i> Sample Po</a>
							</li>
						</ul>
					</div>
				</div>	
			</div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
					         <div class="col-lg-8 col-lg-offset-2">
                                  <div class="panel-body padding-bottom-40">
										<div class="form-group">
											<div class="col-lg-8">                                            
												<?php
													if( count( $getpoList ) > 0 )
													{
														print $this->form->input( 'Po.choosePo', array( 'type'=>'select', 'empty'=>'Choose Po','options'=>$getpoList,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
													}
													else
													{
														echo '<div class="col-lg-12 padding-left-lg"><p>There is no po.</p></div>';
													}
												?>  
											</div>
										 </div>
									</div>
								</div>
							</div>
						</div>
					<div class = "set_search_po" ></div>
                </div>
            </div>
        </div>
    </div>        
</div>



<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
						
	

<script>
	
	$(document).ready(function()
	{
		$("body").on("change", "#PoChoosePo", function ()
		{
			var poName				=	$( '#PoChoosePo' ).find(":selected").text();
		
			$.ajax(
			{
				url     : '/Virtuals/getSelectedPo',
				type    : 'POST',
				data    : { poName : poName },
				'beforeSend' : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },  
				success :	function( msgArray  )
					{
						$('.set_search_po').html( msgArray );
						$( ".outerOpac" ).attr( 'style' , 'display:none');
					}                
			}); 
		});	
	});
	
	$( 'body' ).on( 'click', '.lockUnlockPo', function(){
		
		var poName				=	$( '#PoChoosePo' ).find(":selected").text();
		var status				=	$( '.lockUnlockPo' ).attr("for");
			if( poName == '' )
			{
				swal( 'Please select store' );
				return false;
			}
			$.ajax({
				url     	: '/Virtuals/markLoclUnlock',
				type    	: 'POST',
				data    	: { poName : poName, status : status },  			  
				success 	:	function( data  )
				{
					location.reload();
					/*if( data == 0)
					{
						$( '.lockUnlockPo' ).removeClass('btn-danger').addClass('btn-success');
						$( '.lockUnlockPo' ).text('Lock');
						$( '.lockUnlockPo' ).attr('for', 0);
					}
					else
					{
						$( '.lockUnlockPo' ).removeClass('btn-success').addClass('btn-danger');
						$( '.lockUnlockPo' ).text('Un-Lock');
						$( '.lockUnlockPo' ).attr('for', 1);
					}*/
				}                
			});	
		
		});
	
	
</script>
