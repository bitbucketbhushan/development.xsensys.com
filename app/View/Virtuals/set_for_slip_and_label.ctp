<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php print 'Cancel - Delete Orders';?></h1>
		<?php print $this->Session->flash();  ?>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
       <div class="row">
				<div class="col-lg-12">
						<div class="panel no-border ">
							<div class="panel-title bg-white no-border">
							<div class="panel-body no-padding-top bg-white">																		
								<div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">
									<div class="row">
										<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Name: activate to sort column ascending">Ord-ID</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Type: activate to sort column ascending">SKU</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Type: activate to sort column ascending">Title</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Type: activate to sort column ascending">Barcode</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Type: activate to sort column ascending">Location</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Type: activate to sort column ascending">Qty</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Type: activate to sort column ascending">Type</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role Type: activate to sort column ascending">Del. Country</th>
												<th role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" title = "Edit Role Name" >Action</th>
										</thead>
											<tbody role="alert" aria-live="polite" aria-relevant="all">
												<?php 
												App::import('controller', 'Linnworksapis');
												$linnworkObj	=	new LinnworksapisController();
												if( count( $getOrderResult ) > 0 ) : 
														foreach( $getOrderResult as $index => $value ) :
														$title =	$linnworkObj->getTitle( $value['MergeUpdate']['sku'] );
														
												?>
													<tr class="odd">
														<td class="  sorting_1"><?php print $value['MergeUpdate']['order_id'] .'('.$value['MergeUpdate']['product_order_id_identify'].')'; ?></td>
														<td class="  sorting_1"><?php print $value['MergeUpdate']['sku']; ?></td>
														<td class="  sorting_1"><?php print $title[0]; ?></td>
														<td class="  sorting_1"><?php print $value['MergeUpdate']['barcode']; ?></td>
														<td class="  sorting_1"><?php print $value['MergeUpdate']['service_provider']; ?></td>
														<td class="  sorting_1"><?php print $value['MergeUpdate']['service_name']; ?></td>
														<td class="  sorting_1"><?php print $value['MergeUpdate']['provider_ref_code']; ?></td>
														<td class="  sorting_1"><?php print $value['MergeUpdate']['delevery_country']; ?></td>
														<td class="  sorting_1">
															<a style="margin:0 auto; text-align: center;" class="deleteOpenOrder btn bg-green-400 color-white btn-dark padding-left-40 padding-right-40" for="" href="/outers/verifyItem/<?php echo $value['MergeUpdate']['id']; ?>">OK</a>
														</td>
													</tr>
												<?php
													endforeach;
												?>
											<?php
												endif;
											?>
										</tbody>
									</table>
							</div>
						</div>
					</div>
			</div>
		</div> 
    </div>
		<?php echo $this->element('footer'); ?>
</div>

