<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Manage Virtual Stock</h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                                <div class="panel-body padding-bottom-40">
										<div class="form-group">                                        
                                        <div class="col-lg-7">                                                                                         
											<?php												
												App::import( 'Controller' , 'Uploads' );
												$uploadController = new UploadsController();
												$getAllStores = (array)$uploadController->getStores();
												print $this->form->input( 'chooseStore', array( 'type'=>'select', 'empty'=>'Choose store','options'=>$getAllStores,'class'=>'chooseStore form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );	
											?>
                                        </div>                                      
                                      </div>
									</div>
								</div>
							</div>
						</div>
                    <div class="add_get_sku">
                </div>
            </div>
        </div>
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>


<script>
	
	$(document).ready(function()
	{	
		$("body").on("change", ".chooseStore", function ()
		{
			
			var getStoreSelection = $( 'select.chooseStore' ).val();
			
			
			$.ajax(
			{
				url     : '/JijGroup/Product/CustomManage/getSku',
				type    : 'POST',
				data    : {getStoreSelection:getStoreSelection},
				'beforeSend' : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },  
				success :	function( msgArray  )
					{
						
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.add_get_sku').html(msgArray);
					}                
			}); 
			
		});	
		
	});

</script>




