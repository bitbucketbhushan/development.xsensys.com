<table class="table table-bordered table-striped dataTable" >
	<tr>
		<th width=1%>S.No</th>
		<th width=1%><a href="javascript:void(0);" class="check_all">Select All</a></th>
		<th width=15%>PO Name</th>
		<th width=10%>Invoice No</th>
		<th width=10%>Supplier Code</th>
		<th width=10%>Po date</th>
		<th width=10%>Uploaded date</th>
		<th width=8%>Status</th>
	</tr>
	 <?php $i = 1; foreach( $getAllPurchaseOrders as $getAllPurchaseOrder ) { ?>
	 <tr>
		<td><?php echo $i ?></td>
		<td><input type="checkbox" id="purchase_order_<?php echo $getAllPurchaseOrder['Po']['id']; ?>" name="<?php echo $getAllPurchaseOrder['Po']['id'].'####'.$getAllPurchaseOrder['Po']['po_name']; ?>"/></td>
		<td><?php echo $getAllPurchaseOrder['Po']['po_name']; ?></td>
		<td class="po_nam" ><?php echo ($getAllPurchaseOrder['Po']['invoice_no'] != '') ? $getAllPurchaseOrder['Po']['invoice_no'] : '-----'; ?></td>
		<td><?php echo ($getAllPurchaseOrder['Po']['supplier_code'] != '') ? $getAllPurchaseOrder['Po']['supplier_code'] : '-----'; ?></td>
		<td><?php echo ($getAllPurchaseOrder['Po']['po_date'] != '0000-00-00 00:00:00' ) ? date("d-m-Y", strtotime($getAllPurchaseOrder['Po']['po_date'])) : '-----'; ?></td>
		<td class="po_sku" ><?php echo ($getAllPurchaseOrder['Po']['date'] != '0000-00-00 00:00:00' ) ? date("Y-m-d", strtotime($getAllPurchaseOrder['Po']['date'])) : '-----'; ?></td>
		<td class="po_sku1" >
		<?php if($getAllPurchaseOrder['Po']['status'] == 1 ) { ?>
		<a class="btn bg-green-500 color-white btn-dark padding-left-5 padding-right-5" href="#" data-toggle="modal">Unlock</a>
		<a class="btn bg-orange-500 color-white btn-dark padding-left-5 padding-right-5" href="/Virtuals/showPoDetail/<?php echo $getAllPurchaseOrder['Po']['id']; ?>" data-toggle="modal">View</a>
		<?php } else { ?>
		<a class="btn bg-red-500 color-white btn-dark padding-left-5 padding-right-5" href="#" data-toggle="modal">Lock</a>
		<a class="btn bg-orange-500 color-white btn-dark padding-left-5 padding-right-5" href="/Virtuals/showPoDetail/<?php echo $getAllPurchaseOrder['Po']['id']; ?>" data-toggle="modal">View</a>
		<?php } ?>
		</td>
	</tr>
	<?php $i++;} ?>


</table>
