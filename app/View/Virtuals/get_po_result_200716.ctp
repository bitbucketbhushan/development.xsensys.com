<div class="submenu">
	<div class="navbar-header">
		<ul class="nav navbar-nav pull-left">
			<!--<li>
				<?php if( $getPo['Po']['status'] == 1 ) { ?>
				<a data-target="" data-toggle="modal" for="0" href="#" class="lockUnlockPo btn btn-success"><i class=""></i>Lock</a>
				<?php } else { ?>
				<a data-target="" data-toggle="modal" for="1" href="#" class="lockUnlockPo btn btn-danger"><i class=""></i>Un-Lock</a>
				<?php } ?>
			</li>
			<li>
				<a data-target="" data-toggle="modal" href="#" class="downloadPoFile btn btn-success"><i class=""></i> Generate CSV </a>
			</li>-->
			<li>
				<a data-target="" data-toggle="modal" href="#" class="deletePo btn btn-success"><i class=""></i>Delete Po</a>
			</li>
			<li>
				<a data-target="" data-toggle="modal" href="#" class="addSkuInPo btn btn-success"><i class=""></i>Add SKU</a>
			</li>
			<li>
				<?php if( isset( $getPo['Po']['status'] ) && $getPo['Po']['status'] == 1 ) { ?>
				<a data-target="" data-toggle="modal" for="0" href="#" class="lockUnlockPo btn btn-success"><i class=""></i>Lock</a>
				<?php } else { ?>
				<a data-target="" data-toggle="modal" for="1" href="#" class="lockUnlockPo btn btn-danger"><i class=""></i>Un-Lock</a>
				<?php } ?>
			</li>
			<li>
				<a data-target="" data-toggle="modal" href="#" class="downloadPoFile btn btn-success"><i class=""></i>Generate CSV</a>
			</li>
		</ul>
	</div>
</div>

<table class="table table-bordered table-striped dataTable" >
	<tr>
		<th width=5%><span class="col-lg-12">S.No</span></th>
		<th width=15%>PO Name</th>
		<th width=15%>SKU</th>
		<th width=15%>Barcode</th>
		<th width=15%><span class="col-lg-12">Code</span></th>
		<th width=30%><span class="col-lg-12">Desc</span></th>
		<th width=5%><span class="col-lg-12">Qty</span></th>
		<th width=5%><span class="col-lg-12">Warehouse In</span></th>
		<th width=5%><span class="col-lg-12">Action</span></th>
	</tr class="sku_detail" >
		
	<?php $i = 1; foreach($getPodetail as $getPodetailValue ) { 
			$id = $getPodetailValue['PurchaseOrder']['id'];
		?>
		 <tr class="row_<?php echo $id; ?>" >
			<td><?php echo $i; ?></td>
			<td class="po_name_<?php echo $id; ?>"  ><?php echo $getPodetailValue['PurchaseOrder']['po_name']; ?></td>
			<td class="po_sku_<?php echo $id; ?>" ><?php echo $getPodetailValue['PurchaseOrder']['purchase_sku']; ?></td>
			<td class="po_barcode_<?php echo $id; ?>" ><?php echo $getPodetailValue['PurchaseOrder']['purchase_barcode']; ?></td>
			<td class="po_pur_code_<?php echo $id; ?>" ><?php echo $getPodetailValue['PurchaseOrder']['purchase_code']; ?></td>
			<td class="po_desc_<?php echo $id; ?>" ><?php echo $getPodetailValue['PurchaseOrder']['purchase_description']; ?></td>
			<td class="po_qty_<?php echo $id; ?>" ><?php echo $getPodetailValue['PurchaseOrder']['purchase_qty']; ?></td>
			<td class="po_warehouse_qty_<?php echo $id; ?>" >
				<?php 
				App::import( 'Controller' , 'Virtuals' );
				$getMathod = new VirtualsController();
				echo $getMathod->getWerehouseFillQty( $getPodetailValue['PurchaseOrder']['po_name'], $getPodetailValue['PurchaseOrder']['purchase_sku'] );
				
				//echo $getPodetailValue['PurchaseOrder']['warehouse_fill_in']; ?>
			</td>
			<td>
				<a href="javascript:void(0);" title="Edit" for="<?php echo $id; ?>" class="editPurchaseOrderSku btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>
				<a href="javascript:void(0);" title="Delete" for="<?php echo $id; ?>" class="deletePurchaseOrderSku btn btn-danger btn-xs"><i class="ion-minus-circled"></i></a>
				<a href="javascript:void(0);" title="Print Barcode" for="<?php echo $id; ?>" class="create_barcode_popup bg-blue-400 btn-xs margin-right-10">Barcode</a>
			</td>
		</tr>
	<?php $i++; } ?>


</table>
<div class="showPopupForEditSku"></div>
<div class="showPopupForAddSku"></div>
<div class="showPopupForPrintBarcode"></div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>


<script>
	
	$( 'body' ).on( 'click', '.downloadPoFile', function()
	{
		var poName				=	$( '#PoChoosePo' ).find(":selected").text();
		if( poName == '' )
		{
			swal( 'Please select store' );
			return false;
		}
		$.ajax({
				url     	: '/Virtuals/prepareCsv',
				type    	: 'POST',
				data    	: { poName : poName },  			  
				success 	:	function( data  )
				{
					 window.open(data,'_self' );
				}                
			});				
	});
	
	$(document).ready(function()
	{
			$('.dataTable > tbody  > tr').each(function(){
				
					var uploadedQuantity	=	parseFloat($( this ).find('td:nth-child(7)').text());
					var inWarehouse			=	parseFloat($( this ).find('td:nth-child(8)').text());
					if( inWarehouse > uploadedQuantity)
					{
						$( this ).attr( 'style', 'background:#f8bbd0' );
					}
					else if( inWarehouse < uploadedQuantity)
					{
						$( this ).attr( 'style', 'background:#f0f4c3' );
					}
					else
					{
						$( this ).attr( 'style', 'background:#e8f5e9' );
					}
				});
	});
	
	$( 'body' ).on( 'click', '.editPurchaseOrderSku', function(){
	
		var poSkuId = $( this ).attr('for')
		
		$.ajax({
					url     	: '/Virtuals/editPoSkuPopUp',
					type    	: 'POST',
					data    	: { poSkuId : poSkuId },  			  
					success 	:	function( data  )
					{
						$('.showPopupForEditSku').html( data );
					}                
			  });
		
		});
	$( 'body' ).on( 'click', '.editSkudetail', function(){
		
				var purchaseQty			=	$('.purchaseQty').val();
				var poskuID				=	$('.poskuID').val();
			$.ajax({
					url     	: '/Virtuals/editPoSku',
					type    	: 'POST',
					data    	: { 
									purchaseQty : purchaseQty,
									poskuID : poskuID
								  },  			  
					success 	:	function( msgData  )
					{
						var json = JSON.parse(msgData);
                        var getData = json.data;
						var purchaseQty 	=  getData.purchase_qty;
						var id 				=  getData.id;
						$('.po_qty_'+id).text( purchaseQty );
						$( ".close" ).click();
						setInterval(function(){
										$(".row_"+id).attr( 'style', 'background-color:#81c784;' );
									},1000)
						
					}                
			  });
		
		});
		
		$( 'body' ).on( 'click', '.deletePurchaseOrderSku', function(){
			
			var poSkuId		=	$( this ).attr( 'for' );
			var warehouseQty	=	$('.po_warehouse_qty_'+poSkuId).text();
			
				swal({
					title: "Are you sure?",
					text: "You want to delete po sku !",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						$.ajax({
									url     	: '/Virtuals/deletePoSkuPopUp',
									type    	: 'POST',
									data    	: { poSkuId : poSkuId },  			  
									success 	:	function( data  )
									{
										$( '.success' ).html('<p>Po sku deleted successfully.</p>')
										$( '.success' ).addClass('alert alert-success').fadeIn(1000).delay(4000).fadeOut(1000);
										$(".row_"+data).fadeOut(2000).remove();
									}                
							  });
						
						      
					}
					else
					{
						swal("Cancelled", "Your sku is safe :)", "error");
					}
				});
			});
			
		$( 'body' ).on( 'click', '.addSkuInPo', function(){
			
			var choosePo		=	$( '#PoChoosePo' ).find(":selected").text();
				$.ajax({
					url     	: '/Virtuals/addPoSkuPopUp',
					type    	: 'POST',
					data    	: { choosePo : choosePo },  			  
					success 	:	function( data  )
					{
						$('.showPopupForAddSku').html( data );
					}                
			  });
		});
		
	  $( 'body' ).on( 'click', '.addSkudetail', function(){
			
					var poName				=	$('.poName').val();
					var productSku			=	$('.productsku').val();
					var productCode			=	$('.purchaseCode').val();
					var productDesc			=	$('.productDesc').val();
					var purchaseQty			=	$('.purchaseQty').val();
				
					if( poName == 'Choose Po' )
					{
						swal( 'Please select po.' );
						return false;
					}
					if(productCode == '' ||  purchaseQty == '')
					{
						swal( 'Please fill po code & quantity.' );
						return false;
					}
					$.ajax({
								url     	: '/Virtuals/addPoSku',
								type    	: 'POST',
								data    	: { 
												poName : poName, 
												productSku : productSku, 
												productCode : productCode, 
												productDesc : productDesc, 
												purchaseQty : purchaseQty 
											  },  			  
								success 	:	function( data  )
								{
									if( data == 1 )
									{
										swal( 'Sku save .' );
										$( ".close" ).click();
									}
									else if( data == 2 )
									{
										swal( 'Sku already exist in po.' );
									}
									else if( data == 3 )
									{
										swal( 'Sku does not exist in our product.' );
									}
								}                
							});
				
		  
		  
		  });
		  
		  $('body').on( 'click', '.deletePo', function(){
			  
			    var choosePoId		=	$( '#PoChoosePo' ).val();
			    var choosePo		=	$( '#PoChoosePo' ).find(":selected").text();
				if( choosePo == 'Choose Po' )
					{
						swal( 'Please select po.' );
						return false;
					}
					
					
					swal({
					title: "Are you sure?",
					text: "You want to delete po !",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
							
							$.ajax({
									url     	: '/Virtuals/deletePoAllRecord',
									type    	: 'POST',
									data    	: { 
													choosePoId : choosePoId, 
													choosePo : choosePo 
												  },  			  
									success 	:	function( data  )
									{
										if( data == 1 )
										{
											location.reload();
										}
									}                
							  });
							
					}
					else
					{
						swal("Cancelled", "Your sku is safe :)", "error");
					}
				});
			});
		
	$('body').on( 'click', '.create_barcode_popup', function(){
		
		var poSkuId	=	$( this ).attr('for');
		$.ajax({
					url     	: '/Virtuals/getBarcodePrintPopup',
					type    	: 'POST',
					data    	: {  poSkuId : poSkuId },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					},  			  
					success 	:	function( popUpdata  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.showPopupForPrintBarcode').html( popUpdata );
					}                
				});
		});
	
	
</script>


