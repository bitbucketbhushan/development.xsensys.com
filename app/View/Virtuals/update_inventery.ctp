<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Update Inventery</h1>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                             <div class="form-horizontal panel-body padding-bottom-40 padding-top-40">
									 <div class="form-group">
										<label class="col-sm-3 control-label">Search Text</label>
										<div class="col-sm-7">                                               
											<?php
												print $this->form->input( 'Product.barcode', array( 'type'=>'text','class'=>'form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
											?> 
										</div>
									  </div>
                                      
                                      <div class="form-group">
                                        <label for="username" class="control-label col-lg-3">Title</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'Product.title', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                            ?>
                                        </div>
                                      </div>
                                      
                                      <div class="form-group">
                                        <label for="username" class="control-label col-lg-3">SKU / Bin</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'Product.sku', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                            ?>
                                        </div>
                                      </div>
                                      
                                      <div class="form-group">
                                        <label for="username" id="stockLabel" class="control-label col-lg-3">Available Stock</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'stock', array( 'type'=>'text','div'=>false,'label'=>false, 'readonly' => 'readonly' ,'class'=>'form-control', 'required'=>false ) );
                                            ?>
                                        </div>
                                      </div>
                                      <!-- set the binlocation -->
                                      <div class="form-group brandSelect"></div>
                                      
                                      <div class="form-group count">
                                        <label for="username" class="control-label col-lg-3">Primary Bin Location</label>                                        
                                        <div class="col-lg-3">                                            
                                            <?php
                                                print $this->form->input( 'binLocation1', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control binLocation', 'required'=>false ) );
                                            ?>
                                        </div>
                                        <label for="username" class="control-label col-lg-1">Stock</label>                                        
                                        <div class="col-lg-3">                                            
                                            <?php
                                                print $this->form->input( 'stock1', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control stock', 'required'=>false ) );
                                            ?>
                                        </div>
                                      </div>
                                      
                                      <div class="form-group count">
                                        <label for="username" class="control-label col-lg-3">Secondary Bin Location</label>                                        
                                        <div class="col-lg-3">                                            
                                            <?php
                                                print $this->form->input( 'binLocation2', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control binLocation', 'required'=>false ) );
                                            ?>
                                        </div>
                                        <label for="username" class="control-label col-lg-1">Stock</label>                                        
                                        <div class="col-lg-3">                                            
                                            <?php
                                                print $this->form->input( 'stock2', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control stock', 'required'=>false ) );
                                            ?>
                                        </div>
                                      </div>
                                                                         
                                      
                                     
                                      
                                      <div class='image1' ></div>
                                      
                                    <div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100">                                                                            
                                    <?php
											echo $this->Form->button('Save', array(
												'type' => 'submit',												
												'escape' => true,
												'class'=>'resetPackage btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
										    ));	
										?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>


<?php 
						
	if($this->session->read('Auth.User.id') > 0)
	{	
		$getCommonHelper = $this->Common->getUserDataAfterLogin( $this->session->read('Auth.User.id') );
		$getName = $this->Common->getFirstLastName( $this->session->read('Auth.User.id') );
	} 
	$userId = $getCommonHelper['User']['id'];
	$pcName = $getCommonHelper['User']['pc_name'];
?> 


<script language="JavaScript">
	
	var userId = '<?php echo $userId ?>';
	var pcName = '<?php echo $pcName ?>';
	
function disableEnterKey(e)
{
     var key;      
     if(window.event)
          key = window.event.keyCode; //IE
     else
          key = e.which; //firefox      

     return (key != 13);
}

</script>


<script>
	
	$(document).keypress(function(e)
	{
	   if(e.which == 13) {
		   
		  
		   if($('.binLocation').val() != '')
		   {
			  return false;
		   }
		   else
		   {
			  
		   }	
		  
		   if($('#ProductBarcode').val() != '')
			{
				
				submitSanningValue();
			}
			else
			{
				//submitTrackingSanningValue();
			}
			
		}
	});

	$(function()
	{
		
		$('#ProductBarcode').focus();
		$( ".resetPackage" ).click( function()
		{	
			var barcode	=	$('#ProductBarcode').val();			
			var bin_sku	=	$('#ProductSku').val();			
			
			var i = 0;
			var getLocation = '';
		
			//Get Lacation and stock values
			$( "div.count" ).each(function()
			{
				if( i == 0 )
				{
					getLocation += $(this).find( '.binLocation' ).val();
				}
				else
				{
					getLocation += ','+$(this).find( '.binLocation' ).val();
				}				
			i++;  
			});
			
			var getStockByLocation = '';
			i = 0;
			//Get Lacation and stock values
			$( "div.count" ).each(function()
			{
				if( i == 0 )
				{
					getStockByLocation += $(this).find( '.stock' ).val();
				}
				else
				{
					getStockByLocation += ','+$(this).find( '.stock' ).val();
				}				
			i++;  
			});
						
			var productRefId = $( ".resetPackage" ).attr( 'for' ).split( '-' );
			var qtyBeforeUpdate = $('#stock').val();
			
			$.ajax({
				'url'            :  '/Virtuals/updatePackagingIdByBarcodeSearch',
				'type'           : 'POST',
				'dataType'		 : "JSON",
				'data'           : {  	barcode : barcode , 
										product_id : productRefId[0] , 
										id : productRefId[1] , 
										bin_sku : bin_sku,
										getLocation : getLocation,
										getStockByLocation : getStockByLocation,
										userId : userId,
										pcName:pcName,
										qtyBeforeUpdate: qtyBeforeUpdate
									},
				'success' 		 : function( msgArray )
								   {
										if( msgArray.data.status == "success" )
										{
											alert( "Update successful." );
											
											//Reset All feilds now
											$('#ProductBarcode').val('');
											$('#ProductTitle').val('');
											$('#ProductSku').val('');
											$('#binLocation1').val('');
											$('#binLocation2').val('');
											$('#stock1').val('');
											$('#stock2').val('');
											$('#stock').val('');
																						
											location.reload();
											
											return false;	
										}
								   }
				});
				
		});
		
	})
	
	function submitSanningValue()
	{
		
		var barcode	=	$('#ProductBarcode').val();			
		
		$.ajax({
			'url'            :  '/Products/getBarcodeSearch',
			'type'           : 'POST',
			'dataType'		 : "JSON",
			'data'           : {  barcode : barcode },
			'success' 		 : function( msgArray )
								{
									var brandName	=	msgArray.data.brand;	
									var getId_productId = msgArray.data.product_id + "-" + msgArray.data.id;											
									$( ".resetPackage" ).attr( 'for' , getId_productId );
									$('#ProductTitle').val( msgArray.data.name );
									$('#ProductShortDesc').val( msgArray.data.shortdescription );
									$('#ProductLognDesc').val( msgArray.data.longdescription );
									$('#ProductBrand').val( msgArray.data.brand );
									$('#ProductLength').val( msgArray.data.length );
									$('#ProductWidth').val( msgArray.data.width );
									$('#ProductHeight').val( msgArray.data.height );
									$('#ProductWeight').val( msgArray.data.weight );
									$('#ProductSku').val( msgArray.data.sku );											
									$('#packagingType').val( msgArray.data.variant_envelope_id );
									
									if( msgArray.data.stock === null )
									{
										$( '#stockLabel' ).addClass( 'color-red-800' );
										$('#stock').val( "N/A" );										
									}
									else
									{
										$('#stock').val( msgArray.data.stock );	
									}
									
									
									var str = '';
									var binLocate = msgArray.data.binLocationArray;	
									if( binLocate !== null )															
									{
										$.each(binLocate, function(idx, obj)
										{
											var index = idx;
											var idTable = obj.id;
											var bin_location = obj.bin_location;
											var stock_by_location = obj.stock_by_location;
											var barcode = obj.barcode;
	
											//Set AddMore Locations Html
											if( idx == 0 )
											{	
												str += '<div class="form-group count">';
													str += '<label class="control-label col-lg-3" for="username">Primary Bin Location</label>';                                        
													str += '<div class="col-lg-3">';                                            
														str += '<input type="text" id="binLocation1" class="form-control binLocation" value="'+bin_location+'" name="data[binLocation1]"></div>';
													str += '<label class="control-label col-lg-1" for="username">Stock</label>';                                        
													str += '<div class="col-lg-3">';                                            
														str += '<input type="text" id="stock1" class="form-control stock" value="'+stock_by_location+'" name="data[stock1]"></div>';
												  str += '</div>';
											}										
											else if( idx == 1 )
											{
												str += '<div class="form-group count">';
													str += '<label class="control-label col-lg-3" for="username">Secondary Bin Location</label>';                                        
													str += '<div class="col-lg-3">';                                            
														str += '<input type="text" id="binLocation2" class="form-control binLocation" value="'+bin_location+'" name="data[binLocation2]"></div>';
													str += '<label class="control-label col-lg-1" for="username">Stock</label>';                                        
													str += '<div class="col-lg-3">';                                            
														str += '<input type="text" id="stock2" class="form-control stock" value="'+stock_by_location+'" name="data[stock2]"></div>';
														str += '<div class="col-lg-2">';											
												str += '</div>';
												  str += '</div>';
											}
											else // for all
											{
												//Get Size
												var getCountLocationSize = parseInt($( "div.count" ).size()) + 1;	
														
												str += '<div class="form-group count">';
													str += '<label class="control-label col-lg-3" for="username">Secondary Bin Location</label>';                                        
													str += '<div class="col-lg-3">';                                            
														str += '<input type="text" id="binLocation'+getCountLocationSize+'" class="form-control binLocation" value="'+bin_location+'" name="data[binLocation'+getCountLocationSize+']"></div>';
													str += '<label class="control-label col-lg-1" for="username">Stock</label>';                                        
													str += '<div class="col-lg-3">';                                            
														str += '<input type="text" id="stock'+getCountLocationSize+'" class="form-control stock" value="'+stock_by_location+'" name="data[stock'+getCountLocationSize+']"></div>';
														str += '<div class="col-lg-2">';											
												str += '</div>';
												  str += '</div>';
											}
												
										});
										$( "div.count" ).remove();
									
										//Add more at run time after at last
										$( "div.brandSelect" ).after( str );
										
									}
									else
									{
										$('#binLocation1').val('');
										$('#binLocation2').val('');
										$('#stock1').val('');
										$('#stock2').val('');	
									}	
								}
			});
	}
	
</script>

