<div class="col-lg-12">
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Purchase Order Sku</h1>
    </div>
	<div style="text-align: center; font-size:20px;">
		<?php
			if($getPoName['Po']['reciver_name'] != ''){
			echo '<b>'.$getPoName['Po']['reciver_name'].' [ '.$getPoName['Po']['recived_date'].' ]</b>';
			}
		?>
	</div>
<a class="btn bg-orange-500 color-white btn-dark padding-left-5 padding-right-5"  style=" float: right; margin-bottom: 10px;" href="/Virtuals/showAllPurchaseOrder" data-toggle="modal">Go Back</a>
<div class="set_result">
<table class="table table-bordered table-striped dataTable" >
	<tr>
		<th width=1%>S.No</th>
		<th width=5%>Sup. Code</th>
		<th width=15%>PO Name</th>
		<th width=10%>Sku</th>
		<th width=10%>Code</th>
		<th width=5%>Qty</th>
		<th width=5%>Count</th>
		<th width=5%>Check in</th>
		<th width=5%>Price</th>
		<th width=5%>Sell Comm.</th>
		<th width=5%>Wh. Comm.</th>
		<th width=10%>Uploaded date</th>
		<th width=10%>po date</th>
	</tr>
		
	<?php $i = 1; foreach( $getPoItems as $getPoItem ) { ?>
				<?php 
				App::import( 'Controller' , 'Virtuals' );
				$getMathod = new VirtualsController();
				$checkin =  $getMathod->getWerehouseFillQty( $getPoItem['PurchaseOrder']['po_name'], $getPoItem['PurchaseOrder']['purchase_sku'] );
				 ?>
		 <tr>
		 	<td><?php echo $i ?></td>
			<td><?php echo $getPoItem['PurchaseOrder']['supplier_code']; ?></td>
			<td><?php echo $getPoItem['PurchaseOrder']['po_name']; ?></td>
			<td class="po_nam" ><?php echo ($getPoItem['PurchaseOrder']['purchase_sku'] != '') ? $getPoItem['PurchaseOrder']['purchase_sku'] : '-----'; ?></td>
			<td><?php echo ($getPoItem['PurchaseOrder']['purchase_code'] != '') ? $getPoItem['PurchaseOrder']['purchase_code'] : '-----'; ?></td>
			<td><?php echo ($getPoItem['PurchaseOrder']['purchase_qty'] != '' ) ? $getPoItem['PurchaseOrder']['purchase_qty'] : '-----'; ?></td>
			<td><?php echo ($getPoItem['PurchaseOrder']['purchase_qty'] != '' ) ? $getPoItem['PurchaseOrder']['purchase_count'] : '-----'; ?></td>
			<td><?php echo $checkin; ?></td>
			<td><?php echo ($getPoItem['PurchaseOrder']['price'] != '' ) ? $getPoItem['PurchaseOrder']['price'] : '-----'; ?></td>
			<td><?php echo ($getPoItem['PurchaseOrder']['comment'] != '' ) ? $getPoItem['PurchaseOrder']['comment'] : '-----'; ?></td>
			<td><?php echo ($getPoItem['PurchaseOrder']['seller_comment'] != '' ) ? $getPoItem['PurchaseOrder']['seller_comment'] : '-----'; ?></td>
			<td class="po_sku" ><?php echo ($getPoItem['PurchaseOrder']['date'] != '0000-00-00 00:00:00') ? date("d-m-Y", strtotime($getPoItem['PurchaseOrder']['date'])) : '-----'; ?></td>
			<td class="po_sku1" ><?php echo ($getPoItem['PurchaseOrder']['po_date'] != '0000-00-00 00:00:00') ? date("d-m-Y", strtotime($getPoItem['PurchaseOrder']['po_date'])) : '-----' ?></td>
		</tr>
	<?php $i++;} ?>


</table>

</div>
</div></div>
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>

$('body').on('click','.submit_search_query', function(){

	var poName			=	$('.po_name').val();
	var supplierCode	=	$('.supplier_code').val();
		$.ajax({
								url     	: '/Virtuals/getSearchedPo',
								type    	: 'POST',
								data    	: { poName : poName, supplierCode : supplierCode},
								beforeSend  : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },   			  
								success 	:	function( data  )
								{
								
									$('.set_result').html('');
									$('.set_result').append( data );
									$( ".outerOpac" ).attr( 'style' , 'display:none');
								}                
						});
	
	
});
</script>
