
<div class="modal fade pop-up-4" tabindex="-1" role="dialog" id="pop-up-5search" aria-labelledby="myLargeModalLabel-4" aria-hidden="true">
	<div class="modal-dialog " style="width:1000px">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1"><?php echo $getPoSkuDetail['PurchaseOrder']['po_name'] ?></h4>
         </div>
         <div class="modal-body bg-grey-100" >
           	<div class="no-margin" style="clear:both">
					<div class="panel-body no-padding-top" >
					
					<div class="row" style="padding:20px 0; border: 5px solid #c0c0c0; box-shadow: 0 0 30px -18px rgba(66, 66, 66, 1) inset;">
                        <div class="col-lg-12">
							<div class="panel no-margin-bottom">
                                <div class="panel-title no-border">
									<div class="panel-head">Sku Detail</div>
									<div class="panel-tools">
								</div>
								</div>
                                <div class="panel-body no-padding ">
									<?php 
										$id					=	$getPoSkuDetail['PurchaseOrder']['id'];
										$poName				=	$getPoSkuDetail['PurchaseOrder']['po_name'];
										$productSku			=	$getPoSkuDetail['PurchaseOrder']['purchase_sku'];
										$purchaseCode		=	$getPoSkuDetail['PurchaseOrder']['purchase_code'];
										$productDesc		=	$getPoSkuDetail['PurchaseOrder']['purchase_description'];
										$purchaseQty		=	$getPoSkuDetail['PurchaseOrder']['purchase_qty'];
										$warehouseFillIn	=	$getPoSkuDetail['PurchaseOrder']['warehouse_fill_in'];
										$productBarcode		=	$getPoSkuDetail['PurchaseOrder']['purchase_barcode'];
										$productTitle		=	$productComment['Product']['product_name'];
										$productsku			=	$productComment['Product']['product_sku'];
										$category			=	$productComment['Product']['category_name'];
										$shortDesc			=	$productComment['ProductDesc']['short_description'];
										$riskComment		=	$productComment['Product']['risk_comment'];
									?>
									<table class="table table-striped" cellpadding="5px;">									
										<tbody>
											<?php if($riskComment != '') { ?>
											<tr style="background-color: #f37e7e;color: white; font-size:16px;">
												<td class="vertical-middle">Risk Comment</td>
												<td class="vertical-middle"><?php echo $riskComment; ?></td>
											</tr>
											<?php } ?>
											<tr>
												<td class="vertical-middle">SKU</td>
												<td class="vertical-middle"><?php echo $productsku; ?></td>
											</tr>
											<tr>
												<td class="vertical-middle">Title</td>
												<td class="vertical-middle"><?php echo $productTitle; ?></td>
											</tr>
											<tr>
												<td class="vertical-middle">Barcode</td>
												<td class="vertical-middle"><?php echo $productBarcode; ?></td>
											</tr>
											<tr> 
												<td class="vertical-middle">Purchase Quantity</td>
												<td class="vertical-middle">
													<div class="col-lg-7 margin-left-5">
														<input type="hidden" value="<?php echo $id; ?>" class="poskuID form-control" >
														<input type="hidden" value="<?php echo $productBarcode; ?>" class="poskuBarcode form-control" >
														<input type="text" value="<?php echo $purchaseQty; ?>" class="purchaseQty form-control" >
													</div>
												</td>
											</tr>
										</tbody>
									</table>
							    </div>
					        </div>
                        </div>
                        <div style="text-align:center;">
                        <a style="margin:0 auto; text-align: center;" class="dismiss btn bg-orange-500 color-white btn-dark padding-left-20 padding-right-20 margin-top-20" href="javascript:void(0);">Cancel</a>
						<a style="margin:0 auto; text-align: center;" class="printBarcode btn bg-green-500 color-white btn-dark padding-left-20 padding-right-20 margin-top-20" href="javascript:void(0);">Print</a>
						</div>
					  </div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
   $(document).ready(function()
   {
       $('#pop-up-5search').modal('show');
	   
	   	$('.subOrderBlock>.panel-body').slimScroll({
        height: '275px',
		width:'100%',
		size:'3',
		alwaysVisible: true
    });
   });
   
   
   $("body").on("click", ".dismiss", function(){
					
						$( ".close" ).click();
						//location.reload();
						 
					
					});
</script>

