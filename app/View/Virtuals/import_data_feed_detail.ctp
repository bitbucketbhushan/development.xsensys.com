<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Upload Feed File</h1>		
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>		
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="panel-body check_image_extension padding-bottom-40 padding-top-40">
                                	   <?php							
											print $this->form->create( 'FeedUpload', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/Virtuals/importDataFeedDetail', 'type'=>'post','id'=>'brand' ) );
										?>
										
									 <div class="form-group">
                                        <label for="username" class="control-label col-lg-3">Type</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
												$getFeedDetailArray = array();
												$getFeedDetailArray["/Virtuals/UploadFeedDetail/storeMapSku"] = 'Sku mapping With Sku';
												$getFeedDetailArray["/Virtuals/UploadFeedDetail/bufferQuantity"] = 'SKU Status and Buffer update';
												$getFeedDetailArray["/Virtuals/UploadFeedDetail/channelPercentage"] = 'Link SKU with store Percentage';
												print $this->form->input( 'uploadFile', array( 'type'=>'select', 'empty'=>'Choose File','options'=>$getFeedDetailArray,'class'=>'uploadFile form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
                                            ?>  
                                        </div>
                                      </div>
									  <div class="form-group">
											<label for="username" class="control-label col-lg-3">CSV File</label>                                        
											<div class="col-lg-7">
												<div class="input-group">
													<span class="input-group-btn">
														<span class="btn btn-primary btn-file">
															Browse…  <?php
																		print $this->form->input( 'FeedUpload.Import_file', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'', 'required'=>false) );
																	  ?>
														</span>
													</span>
															<input type="text" placeholder="No file selected" readonly="" class="form-control">
															<span class="input-group-addon no-padding-top no-padding-bottom">
													<div class="radio radio-theme min-height-auto no-margin no-padding">
														<?php
														echo $this->Form->button('Fetch', array(
														'type' => 'submit',
														'escape' => true,
														'class'=>'add_upload_file btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
														 ));	
													?>
													</div>
													</span>
											</div>
								        </div>
                                      </div>
										<div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100">                                                                            
                                   </div>
                                </div>
                               </form>
						</div>
					</div>        
				</div>
				
				
				
				
				
						<?php if(!empty( $dataArray )) { ?>
                                <div class="panel-title bg-white no-border">
									<div class="panel-tools">
								</div>
								</div>
                                <div class="panel-body no-padding-top bg-white">
									<div class="table-responsive">
										 <?php							
											print $this->form->create( 'FeedUpload', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/Virtuals/UploadFeedDetail', 'type'=>'post','id'=>'brand' ) );
										 ?>
										<table class="table">
											<tbody>
												<?php if( !empty( $dataArray ) ) { foreach( $dataArray as $dataArrayValue ) { ?>
												<tr> 
													<td class="vertical-middle">
														<?php
															print $this->form->input('FeedUpload.upoadName', array( 'type'=> 'hidden', 'value' => $uploadfileName ));
															print $this->form->input('FeedUpload.uploadType', array( 'type'=> 'hidden', 'value' => $exFetchFile ));
														?>
														<?php echo $dataArrayValue; ?>
													</td>
													<td class="vertical-middle">
														<?php 
															if( !empty( $columnName ) )
															print $this->form->input( 'FeedUpload.upoadFile', array( 'type'=>'select', 'empty'=>'Choose Column Name','options'=>$columnName,'class'=>'form-control','name'=> 'data[FeedUpload][column][]','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
														?>
													</td>
												</tr>
												<?php } } ?>
												<tr>
													<td>
														<?php
															echo $this->Form->button('Upload', array(
															'type' => 'submit',
															'escape' => true,
															'class'=>'add_brand btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
															 ));	
														?>
														
														<!--<a href="javascript:void(0)" for = "" class="btn bg-green-500 color-white btn-dark padding-left-20 padding-right-20" >Upload</a></td>-->
													<td><a href="/Virtuals/importDataFeedDetail" for = "" class="btn bg-orange-500 color-white btn-dark padding-left-20 padding-right-20" >Cancel</a></td>
												</tr>
											</tbody>
										</table>	
										</form>
									</div>
                                </div>
                            </div>
						</div><!-- /.col --> 
					</div><!-- /.row -->
				</div>
				</div>
				<?php } ?>
			

