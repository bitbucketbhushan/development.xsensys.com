<div class="modal fade  qrCodeGlobel tabindex="-1" role="dialog" id="pop-up-qrcode" aria-labelledby="myLargeModalLabel-4" aria-hidden="true" >
	<div class="modal-dialog modal-dialog_qrcode modal-lg">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Qrcode</h4>
         </div>
         <div class="modal-body bg-grey-100">
            <div class="panel no-margin" style="clear:both">
     <div class="panel-body padding-top-20" >
     <div class="row">
		   <div class="col-lg-8 col-lg-offset-2">
					<div class="panel-body padding-bottom-40">
						        <div class="col-lg-3">
						        <label>QR Code</label>
						        </div>
							<div class="col-lg-7">                                                                                         
								<?php
									$qrCode	=	($productBarcode['ProductDesc']['qr_code'] != '') ? $productBarcode['ProductDesc']['qr_code'] : '';											
									print $this->form->input( 'Qrcode.generate', array( 'type'=>'textarea','class'=>'chooseStore form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'value'=> $qrCode));	
								?>
							</div>                                      
		
						</div>
					</div>
				</div>
		   <div class="row">
	     <div class="col-lg-12" style="text-align:center; padding-bottom: 7px;"> 
		<a href="javascript:void(0);" for="<?php print $productBarcode['ProductDesc']['id']; ?>" class="product_id btn color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;"></a>
         </div>
        </div>
       </div>
      </div> 
     </div>
    </div>
   </div>
  </div>
 </div>


<script>
	
	 $(document).ready(function()
	   {
		   $('#pop-up-qrcode').modal('show');
		   $('.qrCodeGlobel').on('shown.bs.modal', function ()
				   {
					  $('#QrcodeGenerate').focus()
				   })
	   });
		
	$('#QrcodeGenerate').bind('keypress', function(e)
	{
	   if(e.which == 13) {
		   
		   submitQrcode();
			
		}
	});
	
	function submitQrcode()
	{
		
		var qrcode    		= $('#QrcodeGenerate').val();          
		var productId    	= $( '.product_id' ).attr('for'); 
		 					$.ajax({
										
										'url' 					: 	'/Virtuals/setQrcode',
										'type'					: 	'POST',
										'data' 					: 	{ qrcode : qrcode, productId : productId },
										'success' 				:   function( msgArray )
											{
												swal("Qrcode save successfully.", "" , "success");
												$( ".close" ).click();
																							
											} 
										});
	}
							
		
	
</script>




