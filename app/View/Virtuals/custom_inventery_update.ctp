<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Custom Update Inventery</h1>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                             <div class="form-horizontal panel-body padding-bottom-40 padding-top-40">
									 <div class="form-group">
										<label class="col-sm-3 control-label">SKU / Bin</label>
										<div class="col-sm-7">                                               
											<?php
												print $this->form->input( 'Product.barcodeSku', array( 'type'=>'text','class'=>'form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
											?> 
										</div>
									  </div>
                                      
                                      <div class="form-group">
                                        <label for="username" class="control-label col-lg-3">Quantity</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'Product.quantity', array( 'value' => '1', 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                            ?>
                                        </div>
                                      </div>
                                      
                                      <div class="form-group">
									    <label for="username" class="control-label col-lg-3">Action</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
												$getAction = array( 'Increment', 'Decrement' );
                                                print $this->form->input( 'downloadFile', array( 'type'=>'select', 'default' => '0', 'options'=>$getAction,'class'=>'downloadFile form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );	
                                            ?>
                                        </div>
                                      </div>
                                      
                                      <div class="form-group">
                                        <label for="username" class="control-label col-lg-3">Comment</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'Product.comment', array( 'value' => '', 'type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                            ?>
                                        </div>
                                      </div>
                                  
                                      
                                    <div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100">                                                                            
                                    <a class="saveCoustemStock btn btn-success" for="" href="javascript:void(0);"><i class=""></i> Save </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

 <?php $userName	=	 $this->session->read('Auth.User.first_name').' '.$this->session->read('Auth.User.last_name'); ?>

<?php 
						
	if($this->session->read('Auth.User.id') > 0)
	{	
		$getCommonHelper = $this->Common->getUserDataAfterLogin( $this->session->read('Auth.User.id') );
		$getName = $this->Common->getFirstLastName( $this->session->read('Auth.User.id') );
	} 
	$userId = $getCommonHelper['User']['id'];
	$pcName = $getCommonHelper['User']['pc_name'];
?> 


<script language="JavaScript">
	
	var userId 		= '<?php echo $userId ?>';
	var pcName 		= '<?php echo $pcName ?>';
	var userName 	= '<?php echo $userName ?>';
	
	$('body').on( 'click', '.saveCoustemStock', function(){
		
		var barcodeSku		=	$('#ProductBarcodeSku').val();
		var quantity		=	$('#ProductQuantity').val();
		var comment			=	$('#ProductComment').val();
		var getStoreSelection = $( 'select.downloadFile' ).val();
		var str = '';
		if( comment == '' )
		{
			swal( "Please fill the comment." );
			return false;
		}
		if(barcodeSku == '')
		{
			swal( "Sku/Barcode field must be fill." );
			return false;
		}
		
		if(quantity == '')
		{
			swal( "Please input value greater then zero(0)." );
			return false;
		}
		else if(quantity <= 0)
		{
			swal( "Please input value greater then zero(0)." );
			return false;
		}
		if(getStoreSelection == 0)
		{
			str = "You want to up inventory?";
		}
		else
		{
			str = "You want to deduct inventory?";
		}
		swal({
					title: "Are you sure?",
					text: str,
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
		
						$.ajax({
									url     	: '/Virtuals/setCustomStockOnDemand',
									type    	: 'POST',
									data    	: {  
													barcodeSku : barcodeSku,
													quantity : quantity,
													getStoreSelection : getStoreSelection,
													comment : comment,
													userName : userName
												   },
									beforeSend	 : function() {
										$( ".outerOpac" ).attr( 'style' , 'display:block');
									},  			  
									success 	:	function( popUpdata  )
									{
										$( ".outerOpac" ).attr( 'style' , 'display:none');
										$('.close').click();
										swal( popUpdata );
										
									}              
								});
					}
					else
					{
						
					}
				});
			 });
			 
	
</script>

