<div class="col-lg-12">
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Purchase Order</h1>
    </div>
<table class="table table-bordered table-striped dataTable col-lg-12">
	<tr>
		<td width=25%><input type="text" class="po_name" placeholder="PO NAME" style="height: 36px; width: 269px; font-size: 18px;" /></td>
		<td width=25%><input type="text" class="supplier_code" placeholder="SUPPLIER CODE" style="height: 36px; width: 269px; font-size: 18px;"/></td>
		<td width=25%>
		<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
		</td>
		<td width=25%>
			<!--<a class="submit_search_query btn bg-green-500 color-white btn-dark padding-left-10 padding-right-10" href="javascript:void(0);" data-toggle="modal">Search</a>-->
			<a class="btn bg-green-500 color-white btn-dark padding-left-10 padding-right-10" href="/Virtuals/showAllPurchaseOrder" data-toggle="modal">Refresh</a>
			<a class="generate_selected_sku btn bg-green-500 color-white btn-dark padding-left-10 padding-right-10" href="javascript:void(0);" data-toggle="modal">Generate</a>
		</td>
	</tr>
</table>
<div class="set_result">
<table class="table table-bordered table-striped dataTable" >
	<tr>
		<th width=1%>S.No</th>
		<th width=1%><a href="javascript:void(0);" class="check_all">Select All</a></th>
		<th width=15%>PO Name</th>
		<th width=10%>Invoice No</th>
		<th width=10%>Supplier Code</th>
		<th width=6%>Po date</th>
		<th width=6%>Uploaded date</th>
		<th width=16%>Status( View | Lock | Unlocak | Received By)</th>
	</tr>		
		 <?php $i = 1; foreach( $getAllPurchaseOrders as $getAllPurchaseOrder ) { ?>
		 <tr>
		 	<td><?php echo $i ?></td>
			<td><input type="checkbox" id="purchase_order_<?php echo $getAllPurchaseOrder['Po']['id']; ?>" name="<?php echo $getAllPurchaseOrder['Po']['id'].'####'.$getAllPurchaseOrder['Po']['po_name']; ?>"/></td>
			<td><?php echo $getAllPurchaseOrder['Po']['po_name']; ?></td>
			<td class="po_nam" ><?php echo ($getAllPurchaseOrder['Po']['invoice_no'] != '') ? $getAllPurchaseOrder['Po']['invoice_no'] : '-----'; ?></td>
			<td><?php echo ($getAllPurchaseOrder['Po']['supplier_code'] != '') ? $getAllPurchaseOrder['Po']['supplier_code'] : '-----'; ?></td>
			<td><?php echo ($getAllPurchaseOrder['Po']['po_date'] != '0000-00-00 00:00:00' ) ? date("d-m-Y", strtotime($getAllPurchaseOrder['Po']['po_date'])) : '-----'; ?></td>
			<td class="po_sku" ><?php echo date("d-m-y", strtotime($getAllPurchaseOrder['Po']['date'])); ?></td>
			<td class="po_sku1" >
			<?php if($getAllPurchaseOrder['Po']['status'] == 1 ) { ?>
			<a class="btn bg-green-500 color-white btn-dark padding-left-5 padding-right-5" href="#" data-toggle="modal">Unlock</a>
			<a class="btn bg-orange-500 color-white btn-dark padding-left-5 padding-right-5" href="/Virtuals/showPoDetail/<?php echo $getAllPurchaseOrder['Po']['id']; ?>" data-toggle="modal">View</a>
				<?php if($getAllPurchaseOrder['Po']['recived_status'] == 0 ) { ?>
				<!--<a class="btn btn-info color-white btn-dark padding-left-5 padding-right-5" href="/Virtuals/recivedPo/<?php echo $getAllPurchaseOrder['Po']['id']; ?>" data-toggle="modal">Received</a>-->
				<a class="recived_po btn btn-info color-white btn-dark padding-left-5 padding-right-5" for ="<?php echo $getAllPurchaseOrder['Po']['id']; ?>" href="javascript:void(0);" data-toggle="modal">Recived</a>
				<?php } else { ?>
				<!--<strong class="btn-dark padding-left-2 padding-right-2" href="#" data-toggle="modal"><?php echo $getAllPurchaseOrder['Po']['reciver_name'].' [ '.$getAllPurchaseOrder['Po']['recived_date'].' ] '; ?></strong>-->
				<strong class="btn-dark padding-left-2 padding-right-2" href="#" data-toggle="modal"><?php echo $getAllPurchaseOrder['Po']['reciver_name'].' [ '.$getAllPurchaseOrder['Po']['recived_date'].' ] </br> Boxes : '.$getAllPurchaseOrder['Po']['recive_box'].'</br> Comment : '.$getAllPurchaseOrder['Po']['recive_comment']; ?></strong>
				<?php } ?>
			<?php } else { ?>
			<a class="btn bg-red-500 color-white btn-dark padding-left-5 padding-right-5" href="#" data-toggle="modal">Lock</a>
			<a class="btn bg-orange-500 color-white btn-dark padding-left-5 padding-right-5" href="/Virtuals/showPoDetail/<?php echo $getAllPurchaseOrder['Po']['id']; ?>" data-toggle="modal">View</a>
				<?php if($getAllPurchaseOrder['Po']['recived_status'] == 1 ) { ?>
				<strong class="btn-dark padding-left-2 padding-right-2" href="#" data-toggle="modal"><?php echo $getAllPurchaseOrder['Po']['reciver_name'].' [ '.$getAllPurchaseOrder['Po']['recived_date'].' ] '; ?></strong>
				<?php } ?>
			<?php } ?>
			</td>
		</tr>
		<?php $i++;} ?>
</table>

<div class="" style="margin:0 auto; width:350px">
	 <ul class="pagination">
	  <?php
		   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
		   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
		   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
	  ?>
	 </ul>
</div>
</div>
</div>
</div>
<div class="showPopupForRecivePo"></div>

<div class="outerOpac" style="display:none">
	<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; "></span>
	<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>

$('body').on('click', '.check_all', function(){
	$('input:checkbox').each(function() {
			$( this ).parent().addClass('checked');
			//$( this ).attr('checked', 'checked');
			this.checked = true;
		})
		$('.check_all').text('Deselect');
		$('.check_all').addClass('de_select');
		$('.check_all').removeClass('check_all');
});

$('body').on('click', '.de_select', function(){
	$('input:checkbox').each(function() {
			$( this ).parent().removeClass('checked');
			//$( this ).removeAttr('checked', 'checked');
			this.checked = false;
		})
		$('.de_select').text('Select All');
		$('.de_select').addClass('check_all');
		$('.de_select').removeClass('de_select');
		
});

$(document).keypress(function(e) {
   if(e.which == 13) {
   			var getFrom = $('div.daterangepicker_start_input input:text').val();
			var getEnd = $('div.daterangepicker_end_input input:text').val();
			
			var poName			=	$('.po_name').val();
			var supplierCode	=	$('.supplier_code').val();
				$.ajax({
										url     	: '/Virtuals/getSearchedPo',
										type    	: 'POST',
										data    	: { poName : poName, supplierCode : supplierCode,getFrom : getFrom,getEnd:getEnd },
										beforeSend  : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },   			  
										success 	:	function( data  )
										{
										
											$('.set_result').html('');
											$('.set_result').append( data );
											$( ".outerOpac" ).attr( 'style' , 'display:none');
										}                
								});				
    }
});

$('body').on('click', '.generate_selected_sku', function(){

		var selectedId = [];
		$('input:checked').each(function() {
			selectedId.push($(this).attr('name'));
		});
		
		if(selectedId == '')
		{
			swal('Please select any one po.')
			return false;
		}
		$.ajax({
								url     	: '/Virtuals/generatePurchaseSkuCsv',
								type    	: 'POST',
								data    	: { selectedId : selectedId},
								beforeSend  : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },   			  
								success 	:	function( data  )
								{
									$( ".outerOpac" ).attr( 'style' , 'display:none');
									window.open(data,'_blank');
								}                
						});
		
});

$('body').on('click','.recived_po', function(){
	var poid	=	$( this ).attr('for');
				$.ajax({
							url     	: '/Virtuals/getporecivepopup',
							type    	: 'POST',
							data    	: { poid : poid},
							beforeSend  : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },   			  
							success 	:	function( data  )
							{
								$( ".outerOpac" ).attr( 'style' , 'display:none');
								$('.showPopupForRecivePo').html( data );
							}                
						});
	
});
</script>
