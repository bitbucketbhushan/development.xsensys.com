<div class="modal fade barcodeCodeGlobel tabindex="-1" role="dialog" id="pop-up-barcode" aria-labelledby="myLargeModalLabel-4" aria-hidden="true" >
	<div class="modal-dialog modal-dialog_barcode modal-lg">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Barcode Generater</h4>
         </div>
         <div class="modal-body bg-grey-100">
            <div class="panel no-margin" style="clear:both">
     <div class="panel-body padding-top-20" >
     <div class="row">
		   <div class="col-lg-8 col-lg-offset-2">
								<?php //pr($getpodetail); ?>
							   <div class="row">
									<label class="col-lg-6"> Number of boxes recive : </label>
									<div class="col-lg-2"> 
										<input type="hidden" class="po_id" value="<?php echo $getpodetail['Po']['id']; ?>" /> 
										<input type="text" class="number_boxes" style="width: 271px;"/>
									</div>
							   </div>
							   <div class="row">
									<label class="col-lg-6"> Comment : </label>
									<div class="col-lg-2"> 
										<textarea rows="4" cols="50" class="recive_comment" ></textarea>
									</div>
							   </div>                                                                         
						  
						</div>
					</div>
				</div>
		   <div class="row">
         <div class="col-lg-12" style="text-align:center; padding-bottom: 7px;"> 
         <a href="javascript:void(0);" class="recivepos btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40" style="margin:0 auto; text-align: center;">OK</a>
         </div>
        </div>
       </div>
      </div> 
     </div>
    </div>
   </div>
  </div>
 </div>


<script>
	
	 $(document).ready(function()
	   {
		   $('#pop-up-barcode').modal('show');
		   $('.barcodeCodeGlobel').on('shown.bs.modal', function ()
				   {
					  $('#BarcodeQty').focus()
				   })
	   });
	
	//$('#BarcodeGenerate').focus();
	
	 $('#BarcodeQty').bind('keypress', function(e)
	{
	   if(e.which == 13) {
		   if( $('#BarcodeQty').val()  != '' )
		   {
			 submitBarcodeNumber();
		   }
		   else
		   {
			   swal("Cancelled", "Please fill the quantity :)", "error");
		   }
			
		}
	});
	
	$('body').on('click', '.recivepos', function(){
		
		var boxes			=	$('.number_boxes').val();
		var poid			=	$('.po_id').val();
		var comment 		= 	$('.recive_comment').val();
		if(boxes == ''){
			alert('Please fill box');
			return false;
		}
		if(comment == ''){
			alert('Please enter comment');
			return false;
		}
		
		$.ajax({
					url     	:  '/Virtuals/setrecivecomment',
					type    	:  'POST',
					data    	:  { poid : poid, boxes : boxes, comment:comment },
					//beforeSend  :  function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },   			  
					success 	:  function( data  )
					{
						location.reload();
					}                
				});
		
	
	});
	
	
	
</script>




