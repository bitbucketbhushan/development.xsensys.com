<?php $userID = $this->session->read('Auth.User.id'); ?>
<div class="col-lg-12">
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Purchase Orders</h1>
    </div>
    <div class="panel-head"><?php print $this->Session->flash(); ?></div>
 
<div class="set_result" style="padding-top:10px">
<table class="table table-bordered table-striped dataTable" >
	<tr>
		<th width=1%>S.No</th>
		<th width=1%><a href="javascript:void(0);" class="check_all">Select All</a></th>
		<th width=14%>PO Name</th>
		<th width=8%>Invoice No/ ASN NO</th>
		<th width=8%>Supplier Code</th>
		<th width=6%>Po date</th>
		<th width=6%>Uploaded date</th>
		<th width=19%>Status( View | Lock | Unlocak | Received By)</th>
	</tr>		
		 <?php $i = 1; foreach( $getAllPurchaseOrders as $getAllPurchaseOrder ) { 

		 	$status 	= ( $getAllPurchaseOrder['Po']['status'] == 0 ) ? "Lock" : "unLock";?>
		 <tr>
		 	<td><?php echo $i ?></td>
			<td><input type="checkbox" id="purchase_order_<?php echo $getAllPurchaseOrder['Po']['id']; ?>" name="<?php echo $getAllPurchaseOrder['Po']['id'].'####'.$getAllPurchaseOrder['Po']['po_name']; ?>"/></td>
			<td><?php echo $getAllPurchaseOrder['Po']['po_name']; ?></td>
			<td class="po_nam" ><?php echo ($getAllPurchaseOrder['Po']['invoice_no'] != '') ? 'Invoice:'.$getAllPurchaseOrder['Po']['invoice_no'] : ' '; ?>
			<?php $ty = 'po';  ?></td>
			<td><?php echo ($getAllPurchaseOrder['Po']['supplier_code'] != '') ? $getAllPurchaseOrder['Po']['supplier_code'] : '-----'; ?></td>
			<td><?php echo ($getAllPurchaseOrder['Po']['po_date'] != '0000-00-00 00:00:00' ) ? date("d-m-Y", strtotime($getAllPurchaseOrder['Po']['po_date'])) : '-----'; ?></td>
			<td class="po_sku" ><?php echo date("d-m-y", strtotime($getAllPurchaseOrder['Po']['date'])); ?></td>

			<td class="po_sku1" >
 			
			<?php if($getAllPurchaseOrder['Po']['status'] == 1) { ?>
 				<a class="btn bg-green-500 btn-sm color-white btn-dark padding-left-5 padding-right-5" data-toggle="modal" href="/Virtuals/LockUnclock/<?php echo $getAllPurchaseOrder['Po']['id']; ?>/<?php echo $status; ?>">Unlock</a> 
 			<?php } else { ?>
			<a class="btn bg-red-500 btn-sm color-white btn-dark padding-left-5 padding-right-5"  data-toggle="modal" href="/Virtuals/LockUnclock/<?php echo $getAllPurchaseOrder['Po']['id']; ?>/<?php echo $status; ?>">Lock</a> 
  			<?php } ?>
			<a class="btn bg-orange-500 btn-sm color-white btn-dark padding-left-5 padding-right-5" href="/SupplierDetails/showPoDetail/<?php echo $getAllPurchaseOrder['Po']['id'].'/'.$getAllPurchaseOrder['Po']['supplier_code']; ?>" data-toggle="modal">View</a>
			
			<a class="btn btn-primary btn-sm" href="/Virtuals/CheckinReport/<?php echo $getAllPurchaseOrder['Po']['po_name']; ?>" >CheckIn Report</a>
			
			<?php if($getAllPurchaseOrder['Po']['recived_status'] == 0 ) { ?>
			<a class="recived_po btn-sm btn btn-info color-white btn-dark padding-left-5 padding-right-5" for ="<?php echo $getAllPurchaseOrder['Po']['id']; ?>" href="javascript:void(0);" data-toggle="modal">Recived</a>
			<?php }  else { ?>
			<div style="clear:both; padding-top:2px;"><div class="btn-dark btn-sm padding-left-2 padding-right-2" href="#" data-toggle="modal">
			<?php echo $getAllPurchaseOrder['Po']['reciver_name'].' [ '.$getAllPurchaseOrder['Po']['recived_date'].' ] </br> Boxes : '.$getAllPurchaseOrder['Po']['recive_box'].'</br> Comment : '.$getAllPurchaseOrder['Po']['recive_comment']; ?></div></div>
			<?php } ?>
			
			</td>
		</tr>
		<?php $i++;} ?>
</table>

<div class="" style="margin:0 auto; width:350px">
	 <ul class="pagination">
	  <?php
		   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
		   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
		   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
	  ?>
	 </ul>
</div>
</div>
</div>
</div>
<div class="showPopupForRecivePo"></div>

<div class="outerOpac" style="display:none">
	<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; "></span>
	<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>

$('body').on('click', '.check_all', function(){
	$('input:checkbox').each(function() {
			$( this ).parent().addClass('checked');
			//$( this ).attr('checked', 'checked');
			this.checked = true;
		})
		$('.check_all').text('Deselect');
		$('.check_all').addClass('de_select');
		$('.check_all').removeClass('check_all');
});

$('body').on('click', '.de_select', function(){
	$('input:checkbox').each(function() {
			$( this ).parent().removeClass('checked');
			//$( this ).removeAttr('checked', 'checked');
			this.checked = false;
		})
		$('.de_select').text('Select All');
		$('.de_select').addClass('check_all');
		$('.de_select').removeClass('de_select');
		
});

$(document).keypress(function(e) {
   if(e.which == 13) {
   			var getFrom = $('div.daterangepicker_start_input input:text').val();
			var getEnd = $('div.daterangepicker_end_input input:text').val();
			
			var poName			=	$('.po_name').val();
			var supplierCode	=	$('.supplier_code').val();
				$.ajax({
										url     	: '/Virtuals/getSearchedPo',
										type    	: 'POST',
										data    	: { poName : poName, supplierCode : supplierCode,getFrom : getFrom,getEnd:getEnd },
										beforeSend  : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },   			  
										success 	:	function( data  )
										{
										
											$('.set_result').html('');
											$('.set_result').append( data );
											$( ".outerOpac" ).attr( 'style' , 'display:none');
										}                
								});				
    }
});

$('body').on('click', '.generate_selected_sku', function(){

		var selectedId = [];
		$('input:checked').each(function() {
			selectedId.push($(this).attr('name'));
		});
		
		if(selectedId == '')
		{
			swal('Please select any one po.')
			return false;
		}
		$.ajax({
								url     	: '/Virtuals/generatePurchaseSkuCsv',
								type    	: 'POST',
								data    	: { selectedId : selectedId},
								beforeSend  : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },   			  
								success 	:	function( data  )
								{
									$( ".outerOpac" ).attr( 'style' , 'display:none');
									window.open(data,'_blank');
								}                
						});
		
});

$('body').on('click','.recived_po', function(){
	var poid	=	$( this ).attr('for');
				$.ajax({
							url     	: '/Virtuals/getporecivepopup',
							type    	: 'POST',
							data    	: { poid : poid},
							beforeSend  : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },   			  
							success 	:	function( data  )
							{
								$( ".outerOpac" ).attr( 'style' , 'display:none');
								$('.showPopupForRecivePo').html( data );
							}                
						});
	
});
</script>
