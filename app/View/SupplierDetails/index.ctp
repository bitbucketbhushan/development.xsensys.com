<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.head div div div{ padding:0px ;  }
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.col-65 { width: 65%; float: left;}
	.col-60 { width: 60%; float: left;}
	.col-55 { width: 55%; float: left;}	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Suppliers</h4>
			</div>
    	  <div class="container-fluid">	
		  	<div class="panel no-border ">
				<div class="panel-title bg-white no-border">
 					<div class="row">	
				  <div class="col-sm-12"> 						  
					
					 <?php						
								print $this->form->create( 'upload', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/SupplierDetails/index', 'type'=>'get','id'=>'upload' ) );
						?>
						 
						 
						<div class="col-sm-3" ><input type="text" name="key" placeholder="Search By Supplier,Purchaser"  value="<?php if(isset($_GET['key'])){echo $_GET['key'];}?>" class="form-control">	</div>	
						<div class="col-sm-2" ><button type="submit" class="btn btn-info">Search</button></div> 
 						<?php if(isset($_GET['key']) && $_GET['key']!=''){ ?>
						<div class="col-sm-1" ><a href="<?php echo Router::url('/', true)?>SupplierDetails" class="btn btn-primary" role="button">Go Back</a></div>
						<?php }?>
						</form> 
						
						
						<div class="col-lg-3">
							 
							 <div class="col-60">
								<select class="form-control selectpicker" name="fpurchaser" id="fpurchaser" data-live-search="true"> 
										<option value="">All Purchasers</option>
										<?php foreach($users as $user){
										$selected = '';
										if($val['SupplierDetail']['purchaser'] == $user['User']['username']){ 
											$selected = ' selected="selected"';
										}
										echo '<option value="'.$user['User']['username'].'" '.$selected.'>'.$user['User']['first_name'].' '.$user['User']['last_name'].'</option>';
										}
										?> 												
									</select>
							 
							 </div>									 
							  <div class="col-20" style="padding-left: 4px;">
								 <a href="javascript:void(0);" class="btn btn-info filter" role="button" title="Filter Data"><i class="fa fa-filter"></i></a>
							 </div>
							 <div class="col-20">									 
								 <a href="javascript:void(0);" class="btn btn-primary downloadfeed" role="button" title="Download Feed" ><i class="glyphicon glyphicon-download"></i></a>									
							 </div>								
						</div>
					<!--<div class="col-sm-2"><a href="<?php echo Router::url('/', true)?>SupplierDetails/DownloadSuppliers" class="btn btn-warning" role="button">Download Suppliers</a></div>-->
					<div class="col-sm-2 text-right" style="float:right;"><a href="<?php echo Router::url('/', true)?>SupplierDetails/AddSupplier" class="btn btn-success" role="button">Add New Supplier</a></div>							 
				 </div> 			
				</div>
				</div>
			</div>	
									
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						 
 						<div class="table-responsive">	
								
						<table class="table table-striped">
						  <thead>
							<tr>
								<th width="15%">Supplier</th>
								<th>Supplier Code</th>
								<th>Purchaser</th>
								<th width="15%">Email / Phone</th>
								<th>Lead Time</th>
								<th>Payment Mode</th>
								<th>Credit Limit</th>	
 								<th>Min Order Amount</th>	
								<th>Credit Period<br>(In days)</th>	
								<th>Currency </th>	
							   <!--	<th width="20%">Address </th>-->	
								<th>SKU </th>	
								<th>See All PO / All Invoices</th>							
								<th align="center">Action</th>
							</tr>
 						  </thead>
						  <tbody>
								<?php if(count($SupplierDetail) > 0){ 
								
								App::Import('Controller', 'SupplierDetails'); 
								$obj = new SupplierDetailsController;	 
							
								foreach($SupplierDetail as $val){
								$inc_id = $val['SupplierDetail']['id'];
								?> 			
									<div id="myModal_<?php echo $inc_id?>" class="modal fade" role="dialog">
									  <div class="modal-dialog modal-lg">
									
										<!-- Modal content-->
										<div class="modal-content">
										  <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title"><?php echo $val['SupplierDetail']['supplier_name']?></h4>
										  </div>
										  <div class="modal-body"> 
 											<div class="form-group row">
												<label for="designation" class="col-sm-3 col-form-label">Designation</label>
												<div class="col-sm-8">
 													<input name="designation" id="designation_<?php echo $inc_id?>" placeholder="Enter Designation" type="text" class="form-control txt">
												</div>
											</div> 
											<div class="form-group row">
												<label for="department" class="col-sm-3 col-form-label">Department</label>
												<div class="col-sm-8">
													<input name="department" id="department_<?php echo $inc_id?>" placeholder="Enter Department" type="text" class="form-control txt">
												</div>
											</div>
				  
											<div class="form-group row">
												<label for="contact_person_name" class="col-sm-3 col-form-label">Contact Person Name</label>
												<div class="col-sm-8">
													<input name="contact_person_name" id="contact_person_name_<?php echo $inc_id?>" placeholder="Enter Contact Person Name" type="text" class="form-control txt"></div>
											</div>
											<div class="form-group row">
												<label for="email" class="col-sm-3 col-form-label">Email</label>
												<div class="col-sm-8">
													<input name="email" id="email_<?php echo $inc_id?>" placeholder="Enter Email" type="email" class="form-control txt"></div>
											</div>
											<div class="form-group row">
												<label for="phone" class="col-sm-3 col-form-label">Phone</label>
												<div class="col-sm-8">
													<input name="phone" id="phone_<?php echo $inc_id?>" placeholder="Enter Phone" type="phone" class="form-control txt">
												</div>
											</div>
											<div class="form-group row">
												<label for="margin" class="col-sm-3 col-form-label">&nbsp;</label>
												<div class="col-sm-2" id="button-<?php echo $inc_id?>">
													<button type="button" class="btn btn-warning" onclick="saveContactData('<?php echo $inc_id?>');"><i class="glyphicon glyphicon-check"></i>&nbsp;Save</button>
												</div>
												<div class="col-sm-7 save-msg"></div>
											</div>
											
											<div id="contact-div-<?php echo $inc_id?>"></div>
										  </div>
										  <div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										  </div>
										</div>
									
									  </div>
									</div>	
											
								<tr>
									<td><?php echo $val['SupplierDetail']['supplier_name']?></td>
									<td><?php echo $val['SupplierDetail']['supplier_code']; 
									$sup = $obj->getSupplierItems($val['SupplierDetail']['supplier_code']);
									echo '<br>Active:'.$sup['active'];
									echo ' InActive:'.$sup['inactive'];
									?>	
									</td>
									<td>
										<select class="form-control" name="purchaser" id="user_<?php echo $val['SupplierDetail']['id']?>" style="margin-top:5px;height: 30px;padding: 3px;" onchange="assignPurchaser('<?php echo $val['SupplierDetail']['id']?>')"> 
										<option value="">Purchaser</option>
										<?php foreach($users as $user){
										$selected = '';
										if($val['SupplierDetail']['purchaser'] == $user['User']['username']){ 
											$selected = ' selected="selected"';
										}
										echo '<option value="'.$user['User']['username'].'" '.$selected.'>'.$user['User']['first_name'].' '.$user['User']['last_name'].'</option>';
										}
										?> 												
									</select>
									</td>
									<td><?php echo $val['SupplierDetail']['email']?> <br /><?php echo $val['SupplierDetail']['phone_number']?></td>	
									<td><?php echo '<div style="border-bottom:1px dashed">ESL Warehouse:'.$val['SupplierDetail']['lead_time'].'</div>';
									echo '<div style="border-bottom:1px dashed">Direct FBA Europe:'.$val['SupplierDetail']['fba_lead_time'].'</div>';
									echo '<div style="">Direct FBA UK:'.$val['SupplierDetail']['fba_uk_lead_time'].'</div>';?></td>
									<td><?php echo $val['SupplierDetail']['payment_mode']?></td>
									<td><?php echo $val['SupplierDetail']['credit_limit']?></td>	
									<td><?php echo $val['SupplierDetail']['min_order_amount']?></td>	
									<td><?php echo $val['SupplierDetail']['credit_period']?></td>
									<td><?php echo $val['SupplierDetail']['currency']?></td>	
								<!--	<td><?php echo $val['SupplierDetail']['address']?></td>		-->							
									<td align="center"><a href="<?php echo Router::url('/', true).'SupplierMappings/index/'.$val['SupplierDetail']['id']?>" style="font-size:11px; color:#0033CC; text-decoration:underline" >See All SKU</a> </td>
									<td align="center"><a href="<?php echo Router::url('/', true).'SupplierDetails/SupplierPos/'.$val['SupplierDetail']['supplier_code']?>" style="font-size:11px; color:#0033CC;text-decoration:underline" >See POs</a> <br /> 
									<a href="<?php echo Router::url('/', true).'SupplierDetails/SupplierInvoices/'.$val['SupplierDetail']['id']?>" style="font-size:11px; color:#0033CC;text-decoration:underline" >See Invoices</a> 
									</td>
									<td align="center"><a href="<?php echo Router::url('/', true).'SupplierDetails/AddSupplier/'.$val['SupplierDetail']['id']?>" class="btn btn-info btn-sm" role="button"  style="margin-top:0px; margin-bottom:2px;">Edit</a><button type="button" class="btn btn-danger btn-sm" onclick="delSupplier(<?php echo $val['SupplierDetail']['id']?>)" style="margin-top:0px; margin-left:2px; margin-bottom:2px;">Delete</button>
 									
									<a href="<?php echo Router::url('/', true).'Reorder/Index/'.$val['SupplierDetail']['supplier_code'].'/'.$val['SupplierDetail']['lead_time']?>" class="btn btn-warning btn-sm"  style="margin-top:0px; margin-bottom:2px;">Generate new PO</a>
								<button type="button" class="btn btn-success  btn-sm" data-toggle="modal" data-target="#myModal_<?php echo $val['SupplierDetail']['id']?>" onclick="getContactData('<?php echo $val['SupplierDetail']['id']?>')">Contacts</button>		
									</td> 
								</tr>
							<?php	}	};?>
						  </tbody>
						</table>
						<div class="" style="margin:20px auto; width:350px">
								 <ul class="pagination">
								  <?php
									   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
									   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								  ?>
								 </ul>
							</div><div class="panel-body no-padding-top bg-white">
 							<br />
							<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
						</div>			
				  </div>
			  </div> 
			</div> 
		</div> 
	 	</div>  
</div> 		 
		  
 		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999; al " src="<?php echo $this->webroot; ?>img/482.gif" />
</div> 
<script>	
$("#supplier_grid" ).load( "<?php echo Router::url('/', true) ?>SupplierDetails/ajaxGetSuppliers",{"supplier_code":$("#sec_supplier_code option:selected").val()}); 

$(".filter").click(function(){
 	window.location.href = '<?php echo Router::url('/', true) ?>SupplierDetails/index/?purchaser='+$("#fpurchaser option:selected").val();
 });

$(".downloadfeed").click(function(){	
	window.location.href = '<?php echo Router::url('/', true)?>SupplierDetails/DownloadSuppliers/?purchaser='+$("#fpurchaser option:selected").val(); 
});

function edit(id,sup_id) { 
 	$(".outerOpac").show();	
	jQuery.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>SupplierDetails/getContact',
			type: "POST",
			data : {id:id},
			success: function(data, textStatus, jqXHR)
			{		
				$(".outerOpac").hide();	
				$('#designation_'+sup_id).val(data.designation);			
				$('#department_'+sup_id).val(data.department);	
				$('#contact_person_name_'+sup_id).val(data.contact_person_name);
				$('#email_'+sup_id).val(data.email);			
				$('#phone_'+sup_id).val(data.phone);
				$('#button-'+sup_id).html(data.button); 
  				 
			}		
		}); 
}
function UpdateContact(inc_id,id) {	
	
 	var error = 0;
	var inputValues = [];
	jQuery('#myModal_'+id+' .txt').each(function() {    
		if($(this).val() == ''){
			alert("Please enter "+$(this).attr('name')+"!");
			error++;
		}else{    
			inputValues.push($(this).attr('name')+'='+$(this).val());
		}
		if($(this).attr('name') == 'email'){
			if(ValidateEmail($(this).val()) == false){
				error++;
			}
		}
		if($(this).attr('name') == 'phone'){
			if(phonenumber($(this).val()) == false){
				//error++;
			}
		}
		
	})	;
	if(error == 0){
		$(".outerOpac").show();	
		jQuery.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>SupplierDetails/SaveSupplierContact',
				type: "POST",
				data : {values:inputValues,id:id,inc_id:inc_id},
				success: function(data, textStatus, jqXHR)
				{		
					$(".outerOpac").hide();	
					jQuery(".save-msg").show();				
					jQuery(".save-msg").html(data.msg);
					getContactData(id);
					 
				}		
			});
	}
}
function saveContactData(id) {	
	
 	var error = 0;
	var inputValues = [];
	jQuery('#myModal_'+id+' .txt').each(function() {    
		if($(this).val() == ''){
			alert("Please enter "+$(this).attr('name')+"!");
			error++;
		}else{    
			inputValues.push($(this).attr('name')+'='+$(this).val());
		}
		if($(this).attr('name') == 'email'){
			if(ValidateEmail($(this).val()) == false){
				error++;
			}
		}
		if($(this).attr('name') == 'phone'){
			if(phonenumber($(this).val()) == false){
				//error++;
			}
		}
		
	})	;
	if(error == 0){
		jQuery.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>SupplierDetails/SaveSupplierContact',
				type: "POST",
				data : {values:inputValues,id:id},
				success: function(data, textStatus, jqXHR)
				{		
					jQuery(".save-msg").show();				
					jQuery(".save-msg").html(data.msg);
					getContactData(id);
					 
				}		
			});
	}
}
function getContactData(id) {	
 	 
	jQuery.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>SupplierDetails/getSupplierContact',
			type: "POST",
			data : {id:id},
			success: function(data, textStatus, jqXHR)
			{		
				jQuery("#contact-div-"+id).show();				
				jQuery("#contact-div-"+id).html(data.html);
				 
			}		
		});
	 
}

function getDelete(inc_id,id) {		
 	 if(confirm("Are you sure want to delete?")){
		jQuery.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>SupplierDetails/getDelete',
				type: "POST",
				data : {id:inc_id},
				success: function(data, textStatus, jqXHR)
				{		
					jQuery(".save-msg").html(data.msg);
					getContactData(id);	
				}		
			});
		}
	 
}

function phonenumber(inputtxt)
{
   var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(inputtxt.match(phoneno))  {
      return true;
   }
  else{
	alert("Please enter valid phone no.");
	return false;
	}
}

function ValidateEmail(mail) 
{
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    return true;
  }
	alert("You have entered an invalid email address!");
	return false;
}

function assignPurchaser(id){

	if($("#user_"+id+" option:selected").val() == ''){
		alert('Select Purchaser.');
	}else{
		
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>SupplierDetails/AssignPurchaser',
			type: "POST",
			beforeSend: function() {
 				$(".outerOpac").show(); 
			},
			data : {id:id,purchaser:$("#user_"+id+" option:selected").val()},
			success: function(data, textStatus, jqXHR)
			{	
				$(".outerOpac").hide(); 	
 				alert(data.msg);	
 			},
			complete: function() {
				$(".outerOpac").hide(); 
			}		
		});
	}
}

function addData(){
	
	if($('#supplier_name').val() == ''){
		alert('Enter supplier name.');
	}else if($('#supplier_code').val() == ''){
		alert('Enter supplier code.');
	}else{
		
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>SupplierDetails/ajaxAddSupplier',
			type: "POST",
			beforeSend: function() {
 				$(".outerOpac").show(); 
			},
			data : {id:$('#id').val(),supplier_name:$('#supplier_name').val(),supplier_code:$('#supplier_code').val(),credit_period:$('#credit_period').val(),credit_limit:$('#credit_limit').val(),credit_limit_available:$('#credit_limit_available').val(),currency:$('#currency').val(),address:$('#address').val(),category_id:$("#category_id option:selected").val()},
			success: function(data, textStatus, jqXHR)
			{	
				$(".outerOpac").hide(); 	
 				alert(data.msg);	
				if(data.error !='yes'){
					$("#supplier_grid" ).load( "<?php echo Router::url('/', true) ?>Suppliers/ajaxGetSuppliers");	
				}				
				//$('#sup_message').delay(5000).fadeOut('slow');						
			},
			complete: function() {
				$(".outerOpac").hide(); 
			}		
		});
	}
}

function updateData(){
	
	if($('#supplier_name').val() == ''){
		alert('Enter supplier name.');
	}else if($('#supplier_code').val() == ''){
		alert('Enter supplier code.');
	}else{
		
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>SupplierDetails/ajaxUpdateSupplier',
			type: "POST",
			beforeSend: function() {
 				$(".outerOpac").show(); 
			},
			data : {id:$('#id').val(),supplier_name:$('#supplier_name').val(),supplier_code:$('#supplier_code').val(),credit_period:$('#credit_period').val(),credit_limit:$('#credit_limit').val(),credit_limit_available:$('#credit_limit_available').val(),currency:$('#currency').val(),address:$('#address').val(),category_id:$("#category_id option:selected").val()},
			success: function(data, textStatus, jqXHR)
			{	
				//$(".outerOpac").hide(); 	
 				alert(data.msg);	
				if(data.error !='yes'){
					$("#supplier_code").removeAttr("disabled"); 	
					$('#id').val('');
					$('#supplier_name').val(''); 
					$('#supplier_code').val('');				
					$('#credit_period').val('');
					$('#credit_limit').val('');
					$('#credit_limit_available').val('');
					$('#address').val('');
					$('#btn-add').show();
					$('#btn-update').hide(); 
 					$("#supplier_grid" ).load( "<?php echo Router::url('/', true) ?>Suppliers/ajaxGetSuppliers");	
				}				
								
			},
			complete: function() {
				$(".outerOpac").hide(); 
			}		
		});
	}
}

function editSupplier(id){
	 
		$("#loading-mask").show(); 
		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>SupplierDetails/ajaxGetSupplier',
		type: "POST",
		beforeSend: function() {
 				$(".outerOpac").show(); 
		},
		data : {id:id},
		success: function(data, textStatus, jqXHR)
		{	
 				$('#id').val(data.id);
				$('#supplier_name').val(data.supplier_name); 
				$('#supplier_code').val(data.supplier_code);
 				$("#supplier_code").attr("disabled", "disabled"); 
				$('#credit_period').val(data.credit_period);
				$('#credit_limit').val(data.credit_limit);
				$('#credit_limit_available').val(data.credit_limit_available);
				$('#currency').val(data.currency);
				$('#address').val(data.address);
				$('#btn-add').hide();
				$('#btn-update').show(); 
 						
 		},
		complete: function() {
				$(".outerOpac").hide(); 
			}	
	});
   
}

function delSupplier(id){
	if (confirm("Are you sure to delete this Supplier? It will delete all items and record of this supplier.")) {
		$("#loading-mask").show(); 
		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>SupplierDetails/ajaxDelSuppliers',
		type: "POST",
		beforeSend: function() {
 				$(".outerOpac").show(); 
		},
		data : {id:id},
		success: function(data, textStatus, jqXHR)
		{	
 			alert(data.msg);	
			if(data.error !='yes'){
				$("#supplier_grid" ).load( "<?php echo Router::url('/', true) ?>SupplierDetails/ajaxGetSuppliers");	
			}			
 		},
		complete: function() {
				$(".outerOpac").hide(); 
			}	
	});
  } 
}
</script>
