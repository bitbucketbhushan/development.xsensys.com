<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Enter Supplier Details</h1>
		<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <?php  
                                print $this->form->create( 'SupplierDetail', array( 'action'=>'SaveSupplier','class'=>'form-horizontal', 'type'=>'post','id'=>'savesupplier','enctype'=>'multipart/form-data' ));
                            
                                 print $this->form->input( 'SupplierDetail.id', array( 'type'=>'hidden' ) );
                            ?>
                                <div class="panel-body padding-bottom-40">
                                      <div class="form-group">
                                        <label for="SupplierDetailSupplierName" class="control-label col-lg-3">Supplier Name*:</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                 print $this->form->input( 'SupplierDetail.supplier_name', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>true ,'onKeyup' =>'getcode();' ) );
                                            ?>
                                        </div>
                                      </div>
                                      
                                      <div class="form-group">
                                        <label for="SupplierDetailSupplierCode" class="control-label col-lg-3">Supplier Code*:</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'SupplierDetail.supplier_code', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>true ,'onFocus' =>'getcode();' ) );
                                            ?>
                                        </div>
                                      </div>
                                      
                                      <div class="form-group">
                                        <label for="SupplierDetailEmail" class="control-label col-lg-3">Email:</label>                                        
                                        <div class="col-lg-7">                                            
                                            <?php
                                                print $this->form->input( 'SupplierDetail.email', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
                                             ?>
                                        </div>
                                      </div>
                                     <div class="form-group">
										<label for="SupplierDetailPhoneNumber" class="control-label col-lg-3">Phone:</label>
										<div class="col-lg-7">
											 <?php
												print $this->form->input( 'SupplierDetail.phone_number', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ,'data-inputmask' =>"'mask': '(999) 999-9999'") );
												?>
											<i class="help-block">(999) 999-9999</i>
										</div>
									</div>
                                      
									  <div class="form-group">
										<label for="SupplierDetailLeadTime" class="control-label col-lg-3">Lead time(In days):</label>
										<div class="col-lg-7">
											 <?php
												print $this->form->input( 'SupplierDetail.lead_time', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ,'data-inputmask' =>"'mask': '99'") );
												?>
											<i class="help-block">2 digit number. Ex:05</i>
										</div>
									</div>
									<div class="form-group">
										<label for="SupplierDetailLeadTime" class="control-label col-lg-3">FBA Lead time(In days):</label>
										<div class="col-lg-7">
											 <?php
												print $this->form->input( 'SupplierDetail.fba_lead_time', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ,'data-inputmask' =>"'mask': '99'") );
												?>
											<i class="help-block">2 digit number. Ex:05</i>
										</div>
									</div>
									<div class="form-group">
										<label for="SupplierDetailLeadTime" class="control-label col-lg-3">FBA UK Lead time(In days):</label>
										<div class="col-lg-7">
											 <?php
												print $this->form->input( 'SupplierDetail.fba_uk_lead_time', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ,'data-inputmask' =>"'mask': '99'") );
												?>
											<i class="help-block">2 digit number. Ex:05</i>
										</div>
									</div>
									
									<div class="form-group">
											<label for="SupplierDetailCreditLimit" class="control-label col-lg-3">Payment Mode:</label> 											<div class="col-lg-7">                                            
												<?php														
													print $this->form->input( 'SupplierDetail.payment_mode', array( 'type'=>'text', 'empty'=>'Payment Mode','class'=>'form-control','div'=>false, 'label'=>false, 'required'=>false) );
												?>  
											</div>
									  </div>
									 <div class="form-group">
										<label for="SupplierDetailMinOrderAmount" class="control-label col-lg-3">Min Order Amount:</label>
										<div class="col-lg-7">
											 <?php
												print $this->form->input( 'SupplierDetail.min_order_amount', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
												?>
 										</div>
									</div>
									
									<div class="form-group">
											<label for="SupplierDetailCreditLimit" class="control-label col-lg-3">Credit Limit:</label> 											<div class="col-lg-7">                                            
												<?php														
													print $this->form->input( 'SupplierDetail.credit_limit', array( 'type'=>'text', 'empty'=>'Credit Limit','class'=>'form-control','div'=>false, 'label'=>false, 'required'=>false) );
												?>  
											</div>
									  </div>
									  <div class="form-group">
											<label for="SupplierDetailCreditPeriod" class="control-label col-lg-3">Credit Period<small>(In days):</small></label>                                        
												<div class="col-lg-7">                                            
													<?php														
														print $this->form->input( 'SupplierDetail.credit_period', array( 'type'=>'text', 'empty'=>'Credit Period', 'class'=>'form-control','div'=>false, 'label'=>false, 'required'=>false) );
													?>  
												</div>
									  </div>
									  <div class="form-group">
											<label for="SupplierDetailCreditPeriod" class="control-label col-lg-3">Currency*:</small></label>                                        
												<div class="col-lg-7">                                            
													<?php														
														print $this->form->input( 'SupplierDetail.currency', array( 'type'=>'select', 'empty'=>'Currency', 'class'=>'form-control','options'=>['GBP'=>'GBP','EUR'=>'EUR'],'div'=>false, 'label'=>false, 'required'=>true) );
													?>  
												</div>
									  </div>
									  
									  <div class="form-group">
											<label for="purchaser" class="control-label col-lg-3">Purchaser*:</small></label>                                        
												<div class="col-lg-7">                                            
													<select class="form-control" name="purchaser" id="purchaser"> 
													<option value="">Select Purchaser</option>
													<?php foreach($users as $user){
 													echo '<option value="'.$user['User']['username'].'">'.$user['User']['first_name'].' '.$user['User']['last_name'].'</option>';
													}
													?> 												
												</select>
												</div>
									  </div>
									  
                                      <div class="form-group">
                                        <label for="SupplierDetailAddress" class="control-label col-lg-3">Address*:</label>                                        
                                        <div class="col-lg-7">                                            
                                        <?php
                                             print $this->form->input( 'SupplierDetail.address', array( 'type'=>'textarea', 'empty'=>'Address', 'class'=>'form-control', 'div'=>false, 'label'=>false, 'required'=>true) );
                                        ?>  
                                        </div>
                                      </div> 
									   <div class="form-group">
									  <div class="col-lg-3"> </div>
 									   <div class="col-lg-4">  
									   <?php  
										   $btn = 'Add Supplier';
										   if(isset($this->params['pass']) && count($this->params['pass']) > 0){
											 $btn = 'Update Supplier';
										   }
 											echo $this->Form->button($btn, array(
												'type' => 'submit',
												'escape' => true,
												'class'=>'btn btn-info'
										         ));	
										?></div>
										 <div class="col-lg-3 text-right"><a href="<?php echo Router::url('/', true).'SupplierDetails'?>" class="btn btn-warning" role="button">Back</a>
 </div>
                               		  </div> 
									   </div> 
									  
								 </div> 
										  
                            
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>
<style>
.help-block{margin-bottom:0px!important; font-size:10px;margin-top: 0px;}
</style>
 
<script type="text/javascript">
function getcode()
{
	var nstr = $.trim($('#SupplierDetailSupplierName').val().toUpperCase());
	var str = nstr.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');//replace(/ /g, '_');
	
	var fstr = str.substring(0,3);
	<?php if(count($this->params['pass']) == 0){?>
	$('#SupplierDetailSupplierCode').val(fstr);
	<?php }?>
}
 
</script>
</div> 



