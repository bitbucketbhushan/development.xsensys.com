<style> .greenbg{background-color:#00FF33!important;}.redbg{background-color:#FF6600!important;}.nobg{background-color:#FFFFFF!important;}</style>
<div class="col-lg-12">
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Supplier Invoices</h1>
    </div>
	 
<a class="btn bg-orange-500 color-white btn-dark padding-left-5 padding-right-5"  style=" float: right; margin-bottom: 10px;" href="/SupplierDetails" data-toggle="modal">Go Back</a>

 <div class="set_result">
<table class="table table-bordered table-striped dataTable" >
	<tr>
		<th width=1%>S.No</th>
		<th width=5%>Po name</th> 
		<th width=5%>Invoice Name</th> 
		<th width=5%>Download Invoice</th> 
		<th width=10%>Upload date</th>
	</tr> 	 	
	<?php $i = 1; 
		foreach( $ScanInvoice as $getPoItem ) {?>
				 
		 <tr>
		 	<td><?php echo $i ?></td>
			<td><?php echo $getPoItem['ScanInvoice']['po_name']; ?></td>
			<td><?php echo $getPoItem['ScanInvoice']['file_name']; ?></td>
			<td><a href="/img/scanInvoice/<?php echo $getPoItem['ScanInvoice']['file_name']?>" style="text-decoration:underline; color:#0000CC;">Download</td>
  			<td><?php echo $getPoItem['ScanInvoice']['date'];  ?></td>
		 
		</tr>
	<?php $i++;} ?>


</table>

</div>
</div></div>
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>

$('body').on('click','.submit_search_query', function(){

	var poName			=	$('.po_name').val();
	var supplierCode	=	$('.supplier_code').val();
		$.ajax({
								url     	: '/Virtuals/getSearchedPo',
								type    	: 'POST',
								data    	: { poName : poName, supplierCode : supplierCode},
								beforeSend  : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },   			  
								success 	:	function( data  )
								{
								
									$('.set_result').html('');
									$('.set_result').append( data );
									$( ".outerOpac" ).attr( 'style' , 'display:none');
								}                
						});
	
	
});
</script>
