<style> .greenbg{background-color:#00FF33!important;}.redbg{background-color:#FF6600!important;}.nobg{background-color:#FFFFFF!important;}</style>
<div class="col-lg-12">
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Purchase Order Sku</h1>
    </div>
	<div style="text-align: center; font-size:20px;">
		<?php
			echo $getPoName['Po']['po_name'];
			if($getPoName['Po']['reciver_name'] != ''){
			echo '<b>'.$getPoName['Po']['reciver_name'].' [ '.$getPoName['Po']['recived_date'].' ]</b>';
			}
			 
		?>
	</div>
<a class="btn bg-orange-500 color-white btn-dark padding-left-5 padding-right-5"  style=" float: right; margin-bottom: 10px;" href="/SupplierDetails/SupplierPos/<?php echo $this->params->pass[1];?>" data-toggle="modal">Go Back</a>

 <div class="set_result">
<table class="table table-bordered table-striped dataTable" >
	<tr>
		<th width=1%>S.No</th>
		<th width=5%>Sup. Code</th>
		<th width=15%>Stock Info</th>
		<th width=10%>Sku</th>
		<!--<th width=10%>Code</th>-->
		<th width=5%>Pack Size</th>
		<th width=5%><?php echo strtoupper($type); ?> Qty</th>
		<th width=5%>Order Case</th>
		<th width=5%>Total Qty</th>
		<th width=5%>Case Price</th>
		<th width=5%>Total Cost</th>
		<th width=5%>Count</th>
		<th width=5%>Check in</th>
		<th width=5%>Outstanding</th>
		<th width=5%>Price</th>
		<th width=5%>Sell Comm.</th>
		<th width=5%>Wh. Comm.</th>
		<th width=10%>Uploaded date</th>
		<th width=10%>po date</th>
	</tr> 	 	
	<?php $i = 1; 
		foreach( $getPoItems as $getPoItem ) {?>
				<?php 
				App::import( 'Controller' , 'Virtuals' );
				$getMathod = new VirtualsController();
				$checkin =  $getMathod->getWerehouseFillQty( $getPoItem['PurchaseOrder']['po_name'], $getPoItem['PurchaseOrder']['purchase_sku'] );
				$bg = 'nobg';
				if($checkin > 0){
					$out = ($getPoItem['PurchaseOrder']['total_qty'] - $checkin); 
					 if($out > 0){
					 	$bg = 'redbg';
					 }else if($out == 0){
						 $bg = 'greenbg';
					 }
				} 
				
				
				 ?>
		 <tr class="<?php echo $bg;?>">
		 	<td><?php echo $i ?></td>
			<td><?php echo $getPoItem['PurchaseOrder']['supplier_code']; ?></td>
			<td><?php //echo $getPoItem['PurchaseOrder']['po_name']; ?>
				<?php
 				if($getPoItem['PurchaseOrder']['checkin']){
					foreach ($getPoItem['PurchaseOrder']['checkin'] as  $value) {
					//print_r($value);
					echo '<div style="font-size:10px; border-top:dotted #999999 1px;"><b>Location:</b>'.$value['CheckIn']['bin_location'].'<br><b>LotNo:</b>'.$value['CheckIn']['lot_no'] .'<br><b>Expiry:</b>'.date("Y-m-d", strtotime($value['CheckIn']['expiry_date'])).' - '.$value['CheckIn']['qty_checkIn'].'</div>';
				}
			}else{
				echo "---";
			}
			
			/*if(count($getPoItem['PurchaseOrder']['bin_location']) > 0){
				foreach($getPoItem['PurchaseOrder']['bin_location'] as $v){
				echo '<div style="font-size:10px; border-top:dotted #999999 1px;">'.$v['BinLocation']['bin_location'].' Qty:'.$v['BinLocation']['stock_by_location'].'</div>';						
				}
			}
			$blArray = $getPoItem['PurchaseOrder']['checkin'];
			if(count($blArray) > 0){
				foreach($blArray  as $val){
					echo '<div style="font-size:10px; border-top:dotted #999999 1px;">LotNo:'.$val['CheckIn']['lot_no'] .'<br>Expiry:'.date("Y-m-d", strtotime($val['CheckIn']['expiry_date'])).' Qty: '.$val['CheckIn']['qty_checkIn'].'</div>';
				}
			}else{
				echo "---";
			}		*/	
			
						
						
			 ?>
			</td>
			<td class="po_nam" ><?php echo ($getPoItem['PurchaseOrder']['purchase_sku'] != '') ? $getPoItem['PurchaseOrder']['purchase_sku'] : '-----'; ?></td>
			<!--<td><?php echo ($getPoItem['PurchaseOrder']['purchase_code'] != '') ? $getPoItem['PurchaseOrder']['purchase_code'] : '-----'; ?></td>-->
			<td><?php echo ($getPoItem['PurchaseOrder']['pack_size']!= '' )? $getPoItem['PurchaseOrder']['pack_size']:'-----'; ?></td>
			<td><?php echo ($getPoItem['PurchaseOrder']['purchase_qty'] != '' ) ? $getPoItem['PurchaseOrder']['purchase_qty'] : '-----'; ?></td>

			<td><?php echo $getPoItem['PurchaseOrder']['order_case']; ?></td>
			<td><?php echo $getPoItem['PurchaseOrder']['total_qty']; ?></td>
			<td><?php echo $getPoItem['PurchaseOrder']['case_price']; ?></td>
			<td><?php echo $getPoItem['PurchaseOrder']['total_cost']; ?></td>
			<td><?php echo ($getPoItem['PurchaseOrder']['purchase_qty'] != '' ) ? $getPoItem['PurchaseOrder']['purchase_count'] : '-----'; ?></td>
			<td><?php echo $checkin; ?></td>
			<td><?php echo ($getPoItem['PurchaseOrder']['total_qty'] - $checkin); ?></td>

			<td><?php echo ($getPoItem['PurchaseOrder']['price'] != '' ) ? $getPoItem['PurchaseOrder']['price'] : '-----'; ?></td>
			<td><?php echo ($getPoItem['PurchaseOrder']['comment'] != '' ) ? $getPoItem['PurchaseOrder']['comment'] : '-----'; ?></td>
			<td><?php echo ($getPoItem['PurchaseOrder']['seller_comment'] != '' ) ? $getPoItem['PurchaseOrder']['seller_comment'] : '-----'; ?></td>
			<td class="po_sku" ><?php echo ($getPoItem['PurchaseOrder']['date'] != '0000-00-00 00:00:00') ? date("d-m-Y", strtotime($getPoItem['PurchaseOrder']['date'])) : '-----'; ?></td>
			<td class="po_sku1" ><?php echo ($getPoItem['PurchaseOrder']['po_date'] != '0000-00-00 00:00:00') ? date("d-m-Y", strtotime($getPoItem['PurchaseOrder']['po_date'])) : '-----' ?></td>
		</tr>
	<?php $i++;} ?>


</table>

</div>
</div></div>
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>

$('body').on('click','.submit_search_query', function(){

	var poName			=	$('.po_name').val();
	var supplierCode	=	$('.supplier_code').val();
		$.ajax({
								url     	: '/Virtuals/getSearchedPo',
								type    	: 'POST',
								data    	: { poName : poName, supplierCode : supplierCode},
								beforeSend  : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },   			  
								success 	:	function( data  )
								{
								
									$('.set_result').html('');
									$('.set_result').append( data );
									$( ".outerOpac" ).attr( 'style' , 'display:none');
								}                
						});
	
	
});
</script>
