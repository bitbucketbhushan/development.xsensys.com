<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">New Docket</h1>
		
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div>
		
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">
  

                                <div class="panel-body padding-bottom-40 padding-top-40">
									<table class="tbl_table docketTable" id="docket">
   <tbody>
      <tr>
         <td style="padding-bottom:10px;" colspan="11">
            <hr class="bannerLine">
         </td>
      </tr>
      <tr>
         <td class="docketHeaderItem" colspan="9">A UK</td>
         <td class="docketHeaderItem" colspan="2">New</td>
      </tr>
      <tr class="docketTableHeaderRow">
         <th class="docketTableHeader"></th>
         <th class="docketTableHeader"></th>
         <td class="docketTableItemSeparator"></td>
         <th class="docketTableHeader">Items</th>
         <th class="docketTableHeader">Bags</th>
         <th class="docketTableHeader">Item Rate</th>
         <th class="docketTableHeader">Weight</th>
         <th class="docketTableHeader">Kilo Rate</th>
         <th class="docketTableHeader">Item Amount</th>
         <th class="docketTableHeader">Kilo Amount</th>
         <th class="docketTableHeader">Total Amount</th>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="7" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SPR</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Small Packet - Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="24" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="24" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.6097" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.0674" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£14.63</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£14.63</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="394" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">LU1</td>
         <td style="text-align:left;color:#000000;background-color:#00FFFF;" class="docketTableItem">Packet L Blue - 750g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="235" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="35" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.8210" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£427.94</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£427.94</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="395" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">LU2</td>
         <td style="text-align:left;color:#000000;background-color:#00FFFF;" class="docketTableItem">Packet L Blue - 1000g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="350" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="35" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="2.0350" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£712.25</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£712.25</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="398" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">BU1</td>
         <td style="text-align:left;color:#FFFFFF;background-color:#0000FF;" class="docketTableItem">Packet Blue - 750g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="235" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="5" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.9693" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£462.79</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£462.79</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="399" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">BU2</td>
         <td style="text-align:left;color:#FFFFFF;background-color:#0000FF;" class="docketTableItem">Packet Blue - 1000g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="235" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="5" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="2.1857" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£513.64</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£513.64</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="402" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GU1</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 750g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="3" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="5" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="2.9512" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£8.85</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£8.85</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="403" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GU2</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 1000g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="50" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="5" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.1150" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£155.75</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£155.75</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="404" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GU3</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 1500g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="3535" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="325" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.2788" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£11,590.56</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£11,590.56</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="405" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GU4</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 2000g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="35" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="35" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.5518" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£124.31</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£124.31</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="406" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GU5</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 2500g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="35" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="35" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.5536" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£159.38</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£159.38</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="407" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GU6</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 3000g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.1094" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="408" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GU7</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 3500g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.6554" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="409" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GU8</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 4000g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="6.2014" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="410" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GU9</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 4500g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="6.7474" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="411" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">G10</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 5000g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="7.2934" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="412" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">G11</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 5500g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="7.8394" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="413" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">G12</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 6000g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="8.3854" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="414" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">G13</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 6500g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="8.9314" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="415" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">G14</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 7000g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="9.4774" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="416" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">G15</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 7500g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="10.0234" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="417" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">G16</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 8000g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="10.5694" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="418" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">G17</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 8500g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="11.1154" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="419" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">G18</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 9000g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="11.6614" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="420" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">G19</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 9500g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="12.2074" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="421" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">G20</td>
         <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 10000g Unsorted</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="12.7534" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="A" class="tbl_row docketTableItemRow  ">
         <td class="docketTableItemInvisible">
            <input value="A" name="docketid">
            <input value="26" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">RD3</td>
         <td style="text-align:left;color:#000000;background-color:#FF9900;" class="docketTableItem">Recorded Delivery</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="5757" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItem">&nbsp;<input type="hidden" value="0" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags"></td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.3100" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">n/a</td>
         <td class="docketTableItem">n/a<input type="hidden" value="0" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="kilorate"></td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£1,784.67</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£1,784.67</span>    
         </td>
      </tr>
      <tr data-tbltotal="A" class="docketTableTotalRow">
         <td colspan="2" class="docketTableTotal">TOTAL</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableTotal">&nbsp;</td>
         <td class="docketTableTotal">
            <span data-tbl="{&quot;name&quot;:&quot;bags&quot;,&quot;fmt&quot;:&quot;num&quot;}" class="calculatedField">509</span>
         </td>
         <td class="docketTableTotal">
         </td>
         <td class="docketTableTotal">&nbsp;</td>
         <td class="docketTableTotal">
         </td>
         <td class="docketTableTotal">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">£15,954.77</span>    
         </td>
         <td class="docketTableTotal">   
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
         </td>
         <td class="docketTableTotal">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">£15,954.77</span>
         </td>
      </tr>
      <tr>
         <td style="padding-top:10px;padding-bottom:10px;" colspan="11">
            <hr class="bannerLine">
         </td>
      </tr>
      <tr>
         <td class="docketHeaderItem" colspan="9">B International Blended</td>
         <td class="docketHeaderItem" colspan="2">New</td>
      </tr>
      <tr class="docketTableHeaderRow">
         <th class="docketTableHeader"></th>
         <th class="docketTableHeader"></th>
         <td class="docketTableItemSeparator"></td>
         <th class="docketTableHeader">Items</th>
         <th class="docketTableHeader">Bags</th>
         <th class="docketTableHeader">Item Rate</th>
         <th class="docketTableHeader">Weight</th>
         <th class="docketTableHeader">Kilo Rate</th>
         <th class="docketTableHeader">Item Amount</th>
         <th class="docketTableHeader">Kilo Amount</th>
         <th class="docketTableHeader">Total Amount</th>
      </tr>
      <tr data-tblblock="B" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="B" name="docketid">
            <input value="29" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">WW3</td>
         <td style="text-align:left;color:#000000;background-color:#FF0000;" class="docketTableItem">Packet</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.6900" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.2700" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="B" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="B" name="docketid">
            <input value="30" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">WW4</td>
         <td style="text-align:left;color:#FFFFFF;background-color:#0000FF;" class="docketTableItem">Large Packet</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.0900" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.0800" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tbltotal="B" class="docketTableTotalRow">
         <td colspan="2" class="docketTableTotal">TOTAL</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableTotal">&nbsp;</td>
         <td class="docketTableTotal">
            <span data-tbl="{&quot;name&quot;:&quot;bags&quot;,&quot;fmt&quot;:&quot;num&quot;}" class="calculatedField">0</span>
         </td>
         <td class="docketTableTotal">
         </td>
         <td class="docketTableTotal">&nbsp;</td>
         <td class="docketTableTotal">
         </td>
         <td class="docketTableTotal">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>    
         </td>
         <td class="docketTableTotal">   
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
         </td>
         <td class="docketTableTotal">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
         </td>
      </tr>
      <tr>
         <td style="padding-top:10px;padding-bottom:10px;" colspan="11">
            <hr class="bannerLine">
         </td>
      </tr>
      <tr>
         <td class="docketHeaderItem" colspan="9">C International Country Specific</td>
         <td class="docketHeaderItem" colspan="2">New</td>
      </tr>
      <tr class="docketTableHeaderRow">
         <th class="docketTableHeader"></th>
         <th class="docketTableHeader"></th>
         <td class="docketTableItemSeparator"></td>
         <th class="docketTableHeader">Items</th>
         <th class="docketTableHeader">Bags</th>
         <th class="docketTableHeader">Item Rate</th>
         <th class="docketTableHeader">Weight</th>
         <th class="docketTableHeader">Kilo Rate</th>
         <th class="docketTableHeader">Item Amount</th>
         <th class="docketTableHeader">Kilo Amount</th>
         <th class="docketTableHeader">Total Amount</th>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="710" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="37" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">PD1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Poland G &lt;20mm</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="12" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="24" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.3600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.4400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£4.32</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£4.32</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="711" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="37" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">PD2</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Poland E &gt;20mm</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.5600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.4400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="202" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="23" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">OZ1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Australia G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.4000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="7.9600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="204" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="23" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">OZ3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Australia E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.5600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="7.9600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="162" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="1" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">AU1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Austria G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.6100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.7600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="164" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="1" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">AU3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Austria E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.1900" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.5500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="142" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="2" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">BE1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Belgium G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.4900" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.1800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="144" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="2" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">BE3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Belgium E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.8900" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.8700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="231" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="25" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">CA1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Canada G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.4100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.6700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="233" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="25" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">CA3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Canada E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.5700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.6700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="190" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="16" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">DE1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Denmark G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.7800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="8.8600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="192" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="16" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">DE3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Denmark E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.6400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.7000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="146" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="17" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">FN1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Finland G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.5500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.8100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="148" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="17" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">FN3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Finland E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.0400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.8100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="182" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="9" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">FR1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">France G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.5200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.9400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="184" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="9" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">FR3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">France E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.7200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.7000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="178" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="3" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GE1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Germany G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.0200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.7500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="180" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="3" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GE3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Germany E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.6100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="2.4500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="261" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="35" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GR1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Greece G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.7400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.8700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="263" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="35" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GR3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Greece E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.2400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.8700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="174" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="6" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">HO1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Holland G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.3400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.9400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="176" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="6" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">HO3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Holland E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.8100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.5300" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="366" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="26" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">IC1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Iceland G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.5200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.3800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="367" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="26" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">IC3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Iceland E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.0400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.5400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="166" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="18" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">IR1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Ireland G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.6500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.2700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="168" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="18" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">IR3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Ireland E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.0200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.0800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="194" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="5" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">IT1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Italy G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.6300" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.9200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="196" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="5" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">IT3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Italy E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.9100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.4800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="368" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="27" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">JP1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Japan G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.4600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.8700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="369" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="27" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">JP3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Japan E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.6100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.8700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="343" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="28" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">LX1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Luxembourg G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.6800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.8900" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="370" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="28" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">LX3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Luxembourg E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.9300" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.7500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="222" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="24" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">ML1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Malta G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.7500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.4800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="224" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="24" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">ML3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Malta E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.9100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.4800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="227" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="29" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">NZ1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">New Zealand G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.4500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="7.7300" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="229" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="29" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">NZ3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">New Zealand E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.6000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="7.7300" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="154" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="19" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">NR1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Norway G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.7900" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="7.5500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="156" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="19" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">NR3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Norway E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.8600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="6.0200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="138" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="15" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">PO1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Portugal G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.4600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.0500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="140" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="15" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">PO3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Portugal E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.6400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.8900" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="374" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="30" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SA1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">South Africa G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="8.0200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="375" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="30" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SA3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">South Africa E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.2200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="8.0200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="198" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="4" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SP1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Spain G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.3700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.6100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="200" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="4" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SP3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Spain E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.5400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.4400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="186" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="7" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SW1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Sweden G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.4600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.2500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="188" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="7" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SW3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Sweden E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.6500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.2000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="150" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="20" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SZ1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Switzerland G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.7000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.8000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="152" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="20" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SZ3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Switzerland E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.1100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.4600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="158" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="22" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">US1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">USA G &lt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.3500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.1000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="C" name="docketid">
            <input value="160" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="22" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">US3</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">USA E &gt; 20mm (M)</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.5100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.1000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tbltotal="C" class="docketTableTotalRow">
         <td colspan="2" class="docketTableTotal">TOTAL</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableTotal">&nbsp;</td>
         <td class="docketTableTotal">
            <span data-tbl="{&quot;name&quot;:&quot;bags&quot;,&quot;fmt&quot;:&quot;num&quot;}" class="calculatedField">24</span>
         </td>
         <td class="docketTableTotal">
         </td>
         <td class="docketTableTotal">&nbsp;</td>
         <td class="docketTableTotal">
         </td>
         <td class="docketTableTotal">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">£4.32</span>    
         </td>
         <td class="docketTableTotal">   
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
         </td>
         <td class="docketTableTotal">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">£4.32</span>
         </td>
      </tr>
      <tr>
         <td style="padding-top:10px;padding-bottom:10px;" colspan="11">
            <hr class="bannerLine">
         </td>
      </tr>
      <tr>
         <td class="docketHeaderItem" colspan="9">E International Direct Mail</td>
         <td class="docketHeaderItem" colspan="2">New</td>
      </tr>
      <tr class="docketTableHeaderRow">
         <th class="docketTableHeader"></th>
         <th class="docketTableHeader"></th>
         <td class="docketTableItemSeparator"></td>
         <th class="docketTableHeader">Items</th>
         <th class="docketTableHeader">Bags</th>
         <th class="docketTableHeader">Item Rate</th>
         <th class="docketTableHeader">Weight</th>
         <th class="docketTableHeader">Kilo Rate</th>
         <th class="docketTableHeader">Item Amount</th>
         <th class="docketTableHeader">Kilo Amount</th>
         <th class="docketTableHeader">Total Amount</th>
      </tr>
      <tr data-tblblock="E" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="E" name="docketid">
            <input value="385" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="3" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">DP1</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Deutsche Post - Germany</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.8950" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.5997" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="E" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="E" name="docketid">
            <input value="799" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="3" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">DP2</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Deutsche Post - High Value</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="1.8535" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.5997" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="E" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="E" name="docketid">
            <input value="393" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="9" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">FR7</td>
         <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">La Poste - France</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.8610" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.2416" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tbltotal="E" class="docketTableTotalRow">
         <td colspan="2" class="docketTableTotal">TOTAL</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableTotal">&nbsp;</td>
         <td class="docketTableTotal">
            <span data-tbl="{&quot;name&quot;:&quot;bags&quot;,&quot;fmt&quot;:&quot;num&quot;}" class="calculatedField">0</span>
         </td>
         <td class="docketTableTotal">
         </td>
         <td class="docketTableTotal">&nbsp;</td>
         <td class="docketTableTotal">
         </td>
         <td class="docketTableTotal">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>    
         </td>
         <td class="docketTableTotal">   
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
         </td>
         <td class="docketTableTotal">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
         </td>
      </tr>
      <tr>
         <td style="padding-top:10px;padding-bottom:10px;" colspan="11">
            <hr class="bannerLine">
         </td>
      </tr>
      <tr>
         <td class="docketHeaderItem" colspan="9">J Premium Products</td>
         <td class="docketHeaderItem" colspan="2">New</td>
      </tr>
      <tr class="docketTableHeaderRow">
         <th class="docketTableHeader"></th>
         <th class="docketTableHeader"></th>
         <td class="docketTableItemSeparator"></td>
         <th class="docketTableHeader">Items</th>
         <th class="docketTableHeader">Bags</th>
         <th class="docketTableHeader">Item Rate</th>
         <th class="docketTableHeader">Weight</th>
         <th class="docketTableHeader">Kilo Rate</th>
         <th class="docketTableHeader">Item Amount</th>
         <th class="docketTableHeader">Kilo Amount</th>
         <th class="docketTableHeader">Total Amount</th>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="805" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">IR9</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Ireland Tracked Rate</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.5200" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.0800" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="797" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">FR9</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">France Tracked &amp; Signed</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.5000" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.7000" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="801" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">GE9</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Germany Tracked Service Bulk</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.7700" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="2.8600" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="796" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">IT9</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Italy Tracked &amp; Signed</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.4800" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.4800" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="798" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SP9</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Spain Tracked Bulk</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.2200" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.4400" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="802" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">US9</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">USA Tracked</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="3.3000" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.1000" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="381" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">AS3</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Bulk Airsure Europe</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.9500" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.4300" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="382" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">AS4</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Airsure International Bulk</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="5.9500" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="6.4900" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="793" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">AS5</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">International Signed Bulk</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="6.1700" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="6.3900" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="794" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">AS6</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">International Tracked &amp; Signed Bulk</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="6.1700" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="6.3900" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="383" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">ISB</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Bulk International Signed For</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="6.1100" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="6.3200" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="380" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SDJ</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Bulk Special Delivery</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="4.5400" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="2.4100" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="791" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SD2</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Special Delivery Bulk UK Max 15 kilos</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="33.4800" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
         <td class="docketTableItemInvisible">
            <input value="J" name="docketid">
            <input value="792" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
            <input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
         </td>
         <td style="text-align:left;color:Black;" class="docketTableItem">SD3</td>
         <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Special Delivery Bulk UK Max 20 kilos</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="6" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
         </td>
         <td class="docketTableItemEditable">
            <input type="text" value="0" maxlength="3" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="43.2600" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">    
            <label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
         </td>
         <td class="docketTableItem">
            <input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
         </td>
         <td class="docketTableItem">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
         </td>
         <td class="docketTableItem">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
         </td>
      </tr>
      <tr data-tbltotal="J" class="docketTableTotalRow">
         <td colspan="2" class="docketTableTotal">TOTAL</td>
         <td class="docketTableItemSeparator">
         </td>
         <td class="docketTableTotal">&nbsp;</td>
         <td class="docketTableTotal">
            <span data-tbl="{&quot;name&quot;:&quot;bags&quot;,&quot;fmt&quot;:&quot;num&quot;}" class="calculatedField">0</span>
         </td>
         <td class="docketTableTotal">
         </td>
         <td class="docketTableTotal">&nbsp;</td>
         <td class="docketTableTotal">
         </td>
         <td class="docketTableTotal">  
            <span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>    
         </td>
         <td class="docketTableTotal">   
            <span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
         </td>
         <td class="docketTableTotal">
            <span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
         </td>
      </tr>
      <tr>
         <td style="padding-top:10px;padding-bottom:10px;" colspan="11">
            <hr class="bannerLine">
         </td>
      </tr>
   </tbody>
</table>


<form action="/BpiShippingCorner/borderel12handling.do" method="post" name="ebpi.Borderel12HandlingForm">
   <div><input type="hidden" value="3b5919020fb2511ff406c26a704779f8" name="org.apache.struts.taglib.html.TOKEN"></div>
   <!-- the action to use when the user presses enter -->
   <input type="hidden" value="VolumailSortedSubmitGroups12Save" name="method">
   <span class="fieldLabel02Std">¹This column should contain the total weight (pieces × weight per piece) for the zone or destination.</span>
   <table class="inputBorderel">
      <tbody>
         <tr>
            <th rowspan="2">Zone1</th>
            <th colspan="2">P</th>
            <th colspan="2">G</th>
            <th colspan="2">E(bus)</th>
            <th colspan="2">E(bel)</th>
         </tr>
         <tr>
            <th>PCE</th>
            <th>PCE×KG¹</th>
            <th>PCE</th>
            <th>PCE×KG¹</th>
            <th>PCE</th>
            <th>PCE×KG¹</th>
            <th>PCE</th>
            <th>PCE×KG¹</th>
         </tr>
         <tr>
            <td><label accesskey="A" for="ItemQuantity_1_1_0_2_PCE_0"><span class="accesskey">A</span>ustria</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_1_0_2_PCE_0" name="ItemQuantity_1_1_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_1_0_1_KGS_0" name="ItemQuantity_1_1_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_1_1_2_PCE_0" name="ItemQuantity_1_1_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_1_1_1_KGS_0" name="ItemQuantity_1_1_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_1_2_2_PCE_0" name="ItemQuantity_1_1_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_1_2_1_KGS_0" name="ItemQuantity_1_1_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="B" for="ItemQuantity_1_2_0_2_PCE_0"><span class="accesskey">B</span>ulgaria</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_2_0_2_PCE_0" name="ItemQuantity_1_2_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_2_0_1_KGS_0" name="ItemQuantity_1_2_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_2_1_2_PCE_0" name="ItemQuantity_1_2_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_2_1_1_KGS_0" name="ItemQuantity_1_2_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_2_2_2_PCE_0" name="ItemQuantity_1_2_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_2_2_1_KGS_0" name="ItemQuantity_1_2_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="C" for="ItemQuantity_1_3_0_2_PCE_0"><span class="accesskey">C</span>roatia</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_3_0_2_PCE_0" name="ItemQuantity_1_3_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_3_0_1_KGS_0" name="ItemQuantity_1_3_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_3_1_2_PCE_0" name="ItemQuantity_1_3_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_3_1_1_KGS_0" name="ItemQuantity_1_3_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_3_2_2_PCE_0" name="ItemQuantity_1_3_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_3_2_1_KGS_0" name="ItemQuantity_1_3_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="C" for="ItemQuantity_1_4_0_2_PCE_0"><span class="accesskey">C</span>yprus</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_4_0_2_PCE_0" name="ItemQuantity_1_4_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_4_0_1_KGS_0" name="ItemQuantity_1_4_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_4_1_2_PCE_0" name="ItemQuantity_1_4_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_4_1_1_KGS_0" name="ItemQuantity_1_4_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_4_2_2_PCE_0" name="ItemQuantity_1_4_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_4_2_1_KGS_0" name="ItemQuantity_1_4_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="C" for="ItemQuantity_1_5_0_2_PCE_0"><span class="accesskey">C</span>zech Republic</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_5_0_2_PCE_0" name="ItemQuantity_1_5_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_5_0_1_KGS_0" name="ItemQuantity_1_5_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_5_1_2_PCE_0" name="ItemQuantity_1_5_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_5_1_1_KGS_0" name="ItemQuantity_1_5_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_5_2_2_PCE_0" name="ItemQuantity_1_5_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_5_2_1_KGS_0" name="ItemQuantity_1_5_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="D" for="ItemQuantity_1_6_0_2_PCE_0"><span class="accesskey">D</span>enmark</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_6_0_2_PCE_0" name="ItemQuantity_1_6_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_6_0_1_KGS_0" name="ItemQuantity_1_6_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_6_1_2_PCE_0" name="ItemQuantity_1_6_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_6_1_1_KGS_0" name="ItemQuantity_1_6_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_6_2_2_PCE_0" name="ItemQuantity_1_6_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_6_2_1_KGS_0" name="ItemQuantity_1_6_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="E" for="ItemQuantity_1_7_0_2_PCE_0"><span class="accesskey">E</span>stonia</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_7_0_2_PCE_0" name="ItemQuantity_1_7_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_7_0_1_KGS_0" name="ItemQuantity_1_7_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_7_1_2_PCE_0" name="ItemQuantity_1_7_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_7_1_1_KGS_0" name="ItemQuantity_1_7_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_7_2_2_PCE_0" name="ItemQuantity_1_7_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_7_2_1_KGS_0" name="ItemQuantity_1_7_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="F" for="ItemQuantity_1_8_0_2_PCE_0"><span class="accesskey">F</span>inland</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_8_0_2_PCE_0" name="ItemQuantity_1_8_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_8_0_1_KGS_0" name="ItemQuantity_1_8_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_8_1_2_PCE_0" name="ItemQuantity_1_8_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_8_1_1_KGS_0" name="ItemQuantity_1_8_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_8_2_2_PCE_0" name="ItemQuantity_1_8_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_8_2_1_KGS_0" name="ItemQuantity_1_8_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="F" for="ItemQuantity_1_9_0_2_PCE_0"><span class="accesskey">F</span>rance</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_9_0_2_PCE_0" name="ItemQuantity_1_9_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_9_0_1_KGS_0" name="ItemQuantity_1_9_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_9_1_2_PCE_0" name="ItemQuantity_1_9_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_9_1_1_KGS_0" name="ItemQuantity_1_9_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_9_2_2_PCE_0" name="ItemQuantity_1_9_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_9_2_1_KGS_0" name="ItemQuantity_1_9_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="G" for="ItemQuantity_1_10_0_2_PCE_0"><span class="accesskey">G</span>ermany</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_10_0_2_PCE_0" name="ItemQuantity_1_10_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_10_0_1_KGS_0" name="ItemQuantity_1_10_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_10_1_2_PCE_0" name="ItemQuantity_1_10_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_10_1_1_KGS_0" name="ItemQuantity_1_10_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_10_2_2_PCE_0" name="ItemQuantity_1_10_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_10_2_1_KGS_0" name="ItemQuantity_1_10_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="G" for="ItemQuantity_1_11_0_2_PCE_0"><span class="accesskey">G</span>reat Britain</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_11_0_2_PCE_0" name="ItemQuantity_1_11_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_11_0_1_KGS_0" name="ItemQuantity_1_11_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_11_1_2_PCE_0" name="ItemQuantity_1_11_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_11_1_1_KGS_0" name="ItemQuantity_1_11_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_11_2_2_PCE_0" name="ItemQuantity_1_11_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_11_2_1_KGS_0" name="ItemQuantity_1_11_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="G" for="ItemQuantity_1_12_0_2_PCE_0"><span class="accesskey">G</span>reece</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_12_0_2_PCE_0" name="ItemQuantity_1_12_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_12_0_1_KGS_0" name="ItemQuantity_1_12_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_12_1_2_PCE_0" name="ItemQuantity_1_12_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_12_1_1_KGS_0" name="ItemQuantity_1_12_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_12_2_2_PCE_0" name="ItemQuantity_1_12_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_12_2_1_KGS_0" name="ItemQuantity_1_12_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="H" for="ItemQuantity_1_13_0_2_PCE_0"><span class="accesskey">H</span>ungary</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_13_0_2_PCE_0" name="ItemQuantity_1_13_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_13_0_1_KGS_0" name="ItemQuantity_1_13_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_13_1_2_PCE_0" name="ItemQuantity_1_13_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_13_1_1_KGS_0" name="ItemQuantity_1_13_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_13_2_2_PCE_0" name="ItemQuantity_1_13_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_13_2_1_KGS_0" name="ItemQuantity_1_13_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="I" for="ItemQuantity_1_14_0_2_PCE_0"><span class="accesskey">I</span>reland</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_14_0_2_PCE_0" name="ItemQuantity_1_14_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_14_0_1_KGS_0" name="ItemQuantity_1_14_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_14_1_2_PCE_0" name="ItemQuantity_1_14_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_14_1_1_KGS_0" name="ItemQuantity_1_14_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_14_2_2_PCE_0" name="ItemQuantity_1_14_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_14_2_1_KGS_0" name="ItemQuantity_1_14_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="I" for="ItemQuantity_1_15_0_2_PCE_0"><span class="accesskey">I</span>taly</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_15_0_2_PCE_0" name="ItemQuantity_1_15_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_15_0_1_KGS_0" name="ItemQuantity_1_15_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_15_1_2_PCE_0" name="ItemQuantity_1_15_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_15_1_1_KGS_0" name="ItemQuantity_1_15_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_15_2_2_PCE_0" name="ItemQuantity_1_15_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_15_2_1_KGS_0" name="ItemQuantity_1_15_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="L" for="ItemQuantity_1_16_0_2_PCE_0"><span class="accesskey">L</span>atvia</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_16_0_2_PCE_0" name="ItemQuantity_1_16_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_16_0_1_KGS_0" name="ItemQuantity_1_16_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_16_1_2_PCE_0" name="ItemQuantity_1_16_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_16_1_1_KGS_0" name="ItemQuantity_1_16_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_16_2_2_PCE_0" name="ItemQuantity_1_16_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_16_2_1_KGS_0" name="ItemQuantity_1_16_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="L" for="ItemQuantity_1_17_0_2_PCE_0"><span class="accesskey">L</span>ithuania</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_17_0_2_PCE_0" name="ItemQuantity_1_17_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_17_0_1_KGS_0" name="ItemQuantity_1_17_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_17_1_2_PCE_0" name="ItemQuantity_1_17_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_17_1_1_KGS_0" name="ItemQuantity_1_17_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_17_2_2_PCE_0" name="ItemQuantity_1_17_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_17_2_1_KGS_0" name="ItemQuantity_1_17_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="L" for="ItemQuantity_1_18_0_2_PCE_0"><span class="accesskey">L</span>uxembourg</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_18_0_2_PCE_0" name="ItemQuantity_1_18_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_18_0_1_KGS_0" name="ItemQuantity_1_18_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_18_1_2_PCE_0" name="ItemQuantity_1_18_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_18_1_1_KGS_0" name="ItemQuantity_1_18_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_18_2_2_PCE_0" name="ItemQuantity_1_18_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_18_2_1_KGS_0" name="ItemQuantity_1_18_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="M" for="ItemQuantity_1_19_0_2_PCE_0"><span class="accesskey">M</span>alta</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_19_0_2_PCE_0" name="ItemQuantity_1_19_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_19_0_1_KGS_0" name="ItemQuantity_1_19_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_19_1_2_PCE_0" name="ItemQuantity_1_19_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_19_1_1_KGS_0" name="ItemQuantity_1_19_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_19_2_2_PCE_0" name="ItemQuantity_1_19_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_19_2_1_KGS_0" name="ItemQuantity_1_19_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="N" for="ItemQuantity_2_1_0_2_PCE_0"><span class="accesskey">N</span>etherlands</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_2_1_0_2_PCE_0" name="ItemQuantity_2_1_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_2_1_0_1_KGS_0" name="ItemQuantity_2_1_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_2_1_1_2_PCE_0" name="ItemQuantity_2_1_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_2_1_1_1_KGS_0" name="ItemQuantity_2_1_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_2_1_2_2_PCE_0" name="ItemQuantity_2_1_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_2_1_2_1_KGS_0" name="ItemQuantity_2_1_2_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_2_1_3_2_PCE_0" name="ItemQuantity_2_1_3_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_2_1_3_1_KGS_0" name="ItemQuantity_2_1_3_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="P" for="ItemQuantity_1_20_0_2_PCE_0"><span class="accesskey">P</span>oland</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_20_0_2_PCE_0" name="ItemQuantity_1_20_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_20_0_1_KGS_0" name="ItemQuantity_1_20_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_20_1_2_PCE_0" name="ItemQuantity_1_20_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_20_1_1_KGS_0" name="ItemQuantity_1_20_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_20_2_2_PCE_0" name="ItemQuantity_1_20_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_20_2_1_KGS_0" name="ItemQuantity_1_20_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="P" for="ItemQuantity_1_21_0_2_PCE_0"><span class="accesskey">P</span>ortugal</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_21_0_2_PCE_0" name="ItemQuantity_1_21_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_21_0_1_KGS_0" name="ItemQuantity_1_21_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_21_1_2_PCE_0" name="ItemQuantity_1_21_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_21_1_1_KGS_0" name="ItemQuantity_1_21_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_21_2_2_PCE_0" name="ItemQuantity_1_21_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_21_2_1_KGS_0" name="ItemQuantity_1_21_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="R" for="ItemQuantity_1_22_0_2_PCE_0"><span class="accesskey">R</span>omania</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_22_0_2_PCE_0" name="ItemQuantity_1_22_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_22_0_1_KGS_0" name="ItemQuantity_1_22_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_22_1_2_PCE_0" name="ItemQuantity_1_22_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_22_1_1_KGS_0" name="ItemQuantity_1_22_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_22_2_2_PCE_0" name="ItemQuantity_1_22_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_22_2_1_KGS_0" name="ItemQuantity_1_22_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="S" for="ItemQuantity_1_23_0_2_PCE_0"><span class="accesskey">S</span>lovakia</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_23_0_2_PCE_0" name="ItemQuantity_1_23_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_23_0_1_KGS_0" name="ItemQuantity_1_23_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_23_1_2_PCE_0" name="ItemQuantity_1_23_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_23_1_1_KGS_0" name="ItemQuantity_1_23_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_23_2_2_PCE_0" name="ItemQuantity_1_23_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_23_2_1_KGS_0" name="ItemQuantity_1_23_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="S" for="ItemQuantity_1_24_0_2_PCE_0"><span class="accesskey">S</span>lovenia</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_24_0_2_PCE_0" name="ItemQuantity_1_24_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_24_0_1_KGS_0" name="ItemQuantity_1_24_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_24_1_2_PCE_0" name="ItemQuantity_1_24_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_24_1_1_KGS_0" name="ItemQuantity_1_24_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_24_2_2_PCE_0" name="ItemQuantity_1_24_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_24_2_1_KGS_0" name="ItemQuantity_1_24_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="S" for="ItemQuantity_1_25_0_2_PCE_0"><span class="accesskey">S</span>pain</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_25_0_2_PCE_0" name="ItemQuantity_1_25_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_25_0_1_KGS_0" name="ItemQuantity_1_25_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_25_1_2_PCE_0" name="ItemQuantity_1_25_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_25_1_1_KGS_0" name="ItemQuantity_1_25_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_25_2_2_PCE_0" name="ItemQuantity_1_25_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_25_2_1_KGS_0" name="ItemQuantity_1_25_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="S" for="ItemQuantity_1_26_0_2_PCE_0"><span class="accesskey">S</span>weden</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_26_0_2_PCE_0" name="ItemQuantity_1_26_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_26_0_1_KGS_0" name="ItemQuantity_1_26_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_26_1_2_PCE_0" name="ItemQuantity_1_26_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_26_1_1_KGS_0" name="ItemQuantity_1_26_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_26_2_2_PCE_0" name="ItemQuantity_1_26_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_26_2_1_KGS_0" name="ItemQuantity_1_26_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><label accesskey="B" for="ItemQuantity_1_27_0_2_PCE_0"><span class="accesskey">B</span>elgium</label></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_27_0_2_PCE_0" name="ItemQuantity_1_27_0_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_27_0_1_KGS_0" name="ItemQuantity_1_27_0_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_27_1_2_PCE_0" name="ItemQuantity_1_27_1_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_27_1_1_KGS_0" name="ItemQuantity_1_27_1_1_KGS_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_27_2_2_PCE_0" name="ItemQuantity_1_27_2_2_PCE_0"></td>
            <td><input type="text" class="normal" value="" size="8" id="ItemQuantity_1_27_2_1_KGS_0" name="ItemQuantity_1_27_2_1_KGS_0"></td>
         </tr>
         <tr>
            <td><b>Totals</b></td>
            <td><b>0&nbsp;Pc</b></td>
            <td><b>0,000&nbsp;Kg</b></td>
            <td><b>0&nbsp;Pc</b></td>
            <td><b>0,000&nbsp;Kg</b></td>
            <td><b>0&nbsp;Pc</b></td>
            <td><b>0,000&nbsp;Kg</b></td>
            <td><b>0&nbsp;Pc</b></td>
            <td><b>0,000&nbsp;Kg</b></td>
         </tr>
      </tbody>
   </table>
   <table width="100%" cellspacing="0" cellpadding="5" border="0">
      <tbody>
         <tr>
            <td align="right" class="appSubTitleBar">
               <span class="appSubTitleBarTitle">
                  <table width="100%" cellspacing="0" cellpadding="5" border="0">
                     <tbody>
                        <tr>
                           <td align="left"><button onclick="javascript: this.form.method.value='delete'; return confirmSubmit('Please confirm the deletion.');" tabindex="5" value="4" name="inputDeposit_4" class="inputSubmit null" type="submit">Delete deposit</button>&nbsp;&nbsp;</td>
                           <td align="right">
                              <button onclick="javascript: this.form.method.value='VolumailSortedSubmitGroups12Save';" tabindex="1" value="0" name="inputDeposit_0" class="inputSubmit null" type="submit">Calculate totals</button>&nbsp;&nbsp;<button onclick="javascript: this.form.method.value='VolumailSortedGroups345AndSaveGroups12';" tabindex="2" value="1" name="inputDeposit_1" class="inputSubmit null" type="submit">Zones 3, 4 and 5</button>&nbsp;&nbsp;<button onclick="javascript: this.form.method.value='borderel12HandlingSubmitBack';" tabindex="3" value="2" name="inputDeposit_2" class="inputSubmit null" type="submit">Back</button>&nbsp;&nbsp;<button onclick="javascript: this.form.method.value='VolumailSortedSubmitGroups12Done';" tabindex="4" value="3" name="inputDeposit_3" class="inputSubmit null" type="submit">Next</button>&nbsp;&nbsp;
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </span>
            </td>
         </tr>
      </tbody>
   </table>
</form>
								</div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>
