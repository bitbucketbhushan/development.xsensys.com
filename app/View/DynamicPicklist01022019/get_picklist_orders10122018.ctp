<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Picklist Orders</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
				<!----------------------------------------Dispatch Console start-------------------------------------------------------->
			<div class="col-lg-12">
					<?php print $this->Session->flash(); ?>
              <div class="panel"> 
						<div class="panel-title">
							<div class="panel-head">Barcode Scan Section</div>
							<div class="panel-tools"><a href="/JijGroup/Generic/Order/GetOpenFilter"   class="btn bg-orange-500 color-white btn-dark">Go Back</a></div>
						</div>
					<div class="panel-body">
					    <div class="row">
			  				<div class="col-lg-4 col-lg-offset-1">
								<div class="input text"><label for="LinnworksapisBarcode">Barcode</label> <input type="text" id="LinnworksapisBarcode" index="0" value="" class="get_sku_string form-control" name="data[Linnworksapis][barcode]">
								<p class="help-block">Please scan an item to start processing order</p>
								</div>
							</div>
							<div class="col-lg-4 col-lg-offset-1"><img alt="" title="Scanner" src="/img/bar-code.png"></div>
							<div class="outer_bin_addMore"></div>	
						</div>
				</div>
			</div>
			
		</div>
				<!----------------------------------------Dispatch Console end---------------------------------------------------------->
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						<form method="get" name="searchfrm" action="<?php echo Router::url('/', true) ?>DynamicPicklist/search" enctype="multipart/form-data">	
						<div class="col-sm-6 no-padding">
							<div class="col-lg-7 no-padding">
									<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Enter Order Id OR SKU" name="searchkey" class="form-control searchString" />
								</div>
								
								<div class="col-lg-3"> 
									<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
								</div>
								<div class="col-lg-2"> 
									<a href="<?php echo Router::url('/', true); ?>DynamicPicklist/index" class="btn btn-warning btn-sm">Go Back</a>
								</div>
						</div>
						</form>
						<div class="col-sm-6 text-right">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						</div>	
					<div class="panel-body no-padding-top bg-white">
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-10">Split Order Id</div>		
								<div class="col-15">Sku</div>	 		
								<div class="col-5">Qty</div>																				
								<div class="col-8">Price</div>
								<div class="col-8">Postal Provider</div>	
								<div class="col-10">Service Name</div>	
								<div class="col-10">Delevery Country</div>	
								<div class="col-8">Order Date</div>
								<div class="col-8">Picked Date</div>	
								<div class="col-5">Status</div> 
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						 if(count($MergeUpdate) > 0) {	 ?>
							<?php foreach( $MergeUpdate as $items ) {   ?>
								<div class="row sku" id="r_<?php echo $items['MergeUpdate']['id']; ?>">
									<div class="col-lg-12">	
										<div class="col-10"><small><?php echo $items['MergeUpdate']['product_order_id_identify']; ?></small></div>	
										<div class="col-15"><small><?php echo $items['MergeUpdate']['sku']; ?></small></div>
										<div class="col-5"><small><?php echo $items['MergeUpdate']['quantity']; ?></small></div>
										<div class="col-8"><small><?php echo $items['MergeUpdate']['price']; ?></small></div> 
										<div class="col-8"><small><?php echo $items['MergeUpdate']['service_provider']; ?></small></div>
										<div class="col-10"><small><?php echo $items['MergeUpdate']['service_name']; ?></small></div>
  										<div class="col-10"><small><?php echo $items['MergeUpdate']['delevery_country']; ?></small></div>
										<div class="col-8"><small><?php echo $items['MergeUpdate']['order_date']; ?></small></div>	
										<div class="col-8"><small><?php echo $items['MergeUpdate']['picked_date']; ?></small></div>
										<div class="col-5"><small><?php echo $items['MergeUpdate']['status']; ?></small></div>
									</div>												
								</div>
								 
							<?php }?>
							
							<div class="col-sm-12 text-right">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	
$("body").on("change", ".invoice", function()
{
	$(".outerOpac").show();
	//$('.outerOpac span').css({'z-index':'10000'});
	
	//var id			=	$(this).attr("id");
	var orderid			=	$(this).attr("for");	
	var field_name  	=	$(this).attr("name");	
	var amazon_order_id =	$(this).attr("amz");
	var vals			=	$(this).val();
	
	$.ajax({
			dataType: 'json',
			url :   '<?php echo Router::url('/', true)?>Invoice/Address',
			type: "POST",
			data : {order_id:orderid,field:field_name,val:vals,amazon_order_id:amazon_order_id},
			success: function(txt, textStatus, jqXHR)
			{	 
				$(".outerOpac").hide();
				 
			}		
		});

}); 		
		
function updateStatus(t,v){	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>MergeUpdates/updateStatus',
		type: "POST",				
		cache: false,			
		data : {status:$(t).find("option:selected").val(),id:v },
		success: function(data, textStatus, jqXHR)
		{	
			 	
			if(data.error){
				alert(data.msg);
			} 							
		}		
 	});  		 
}

/********************Dispatch Console***********************/
$(function() {
  $("#LinnworksapisBarcode").focus();
});

$(document).keypress(function(e) {
    if(e.which == 13) {
        submitSanningValue();
    }
});
$(document).keypress(function(e) {
    if(e.which == 13) {
		var url = $('.btn-success').attr('href');
		//alert( url );
		//return false;
        window.open(url,'_self');
    }
});

function submitSanningValue()
{
	var Barcode	=	$('#LinnworksapisBarcode').val();
	$('.searchbarcode').text(Barcode);
	$.ajax({
				'url'            : getUrl() + '/getsearchlist',
				'type'           : 'POST',
				'data'           : { barcode : Barcode },
				'beforeSend'	 : function() {
													$('.loading-image').show();
												},
				'success' 		 : function( msgArray )
												{
													$('#LinnworksapisBarcode').val('');
													$('.outer_bin_addMore').html(msgArray);
													
												}  
				});
}

/********************Dispatch Console***********************/
</script>
