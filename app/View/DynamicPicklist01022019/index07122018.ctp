<?php

$userID = $this->session->read('Auth.User.id');
$users_array = array('avadhesh.kumar@jijgroup.com','amit.gaur@jijgroup.com','mmarecky82@gmail.com','shashi.b.kumar@jijgroup.com','jake.shaw@euracogroup.co.uk','lalit.prasad@jijgroup.com','aakash.kushwah@jijgroup.com');
?>
<style>
.selectpicker {border:1px solid #c6c6c6;}
.selectpicker:hover  {border:1px solid #666666;}
.completed{background-color:#c3f6c3 !important;}
</style>
<div class="bottom_panel rightside bg-grey-100">
    <div class="container-fluid" >
       		
             <div class="panel">
			 <div class="panel-body">
				<div class="row">	
					<div class="col-lg-12">
						 						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose Section" name="sections" id="sections" onchange="getCountry();"> 
							 <?php   
 									 foreach($sections as $v){	
										 $selected = '';
										 if(isset($this->request->query['sections'])){
											 $exp = explode(",", $this->request->query['sections']);
											 if(in_array($v,$exp)){
												$selected = 'selected="selected"';
											 }
										 }					  
									 	 echo ' <option data-tokens="'.$v.'" value="'.$v.'" '.$selected.'>Section '.$v.'</option>';
								  						  
							  }?>
							 							
							</select>
						</div>	
						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="form-control" title="Choose OrderType" name="sku_type" id="sku_type"  > 
							 <option value="">SKU in Order</option>	
							 <option value="single">Single SKU</option>	
							 <option value="multiple">Multiple SKU</option> 						
							</select>
						</div>	
						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="selectpicker btn form-control" data-live-search="true" title="Choose Country" name="postal_country" id="postal_country" onchange="getCountry()" > 
							 
							 
							  <?php foreach($countries as $v){	
							 		 $selected = '';
									 if(isset($this->request->query['postal_country'])){
										 $exp = explode(",", $this->request->query['postal_country']);
										 if(in_array($v,$exp)){
											$selected = 'selected="selected"';
										 }
									 }
									  									 							  
									 echo ' <option data-tokens="'.$v.'" value="'.$v.'" '.$selected.'>'.$v.'</option>';
								  						  
							  }?>
 
							</select>
						</div>	
						
						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="form-control" title="Choose Service" name="postal_services" id="postal_services"> 
							   		<option value="">Choose Service</option>			
							</select>
						</div>	
						</div>
						</div>
						<div class="row" style="padding-top:5px;">	
					<div class="col-lg-12">
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose SKU" name="sku" id="sku"> 
							  <option value="">Select SKU</option>	
							 
							  <?php foreach($skus as $v){	
							 		 $selected = '';
									 if(isset($this->request->query['postal_country'])){
										 $exp = explode(",", $this->request->query['postal_country']);
										 if(in_array($v,$exp)){
											$selected = 'selected="selected"';
										 }
									 }
									  									 							  
									 echo ' <option data-tokens="'.$v.'" value="'.$v.'" '.$selected.'>'.$v.'</option>';
								  						  
							  }?>
 
							</select>
						</div>	
						<div class="col-md-3 col-lg-3"> 
					 
							<div class="input-group date" data-provide="datepicker">
								<input type="text" name="picklist_before_date" id="picklist_before_date" class="form-control datepicker" placeholder="Before Date">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-th"></span>
								</div>
							</div>					
						</div>
						 
						
						 <div class="col-md-1"> <button type="button"  class="btn btn-success btnpick">GeneratePicklist</button></div>
						
						 
						</div>
						
						
						
						<div class="col-lg-12"><?php print $this->Session->flash(); ?></div>  
				 </div>
			 
			</div>
			</div>
			<div class="panel">
			        
		</div>
		
	</div>        
</div>   
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>
<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/plugins/datepicker/js/bootstrap-datepicker.js"></script>
<script>
$('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
     
});

function markCompleted( picklist_id ){
	if(confirm('Are you sure want to Mark it Completed!')){            
		  
 		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/markCompleted',
				dataType	: 'json',
				type    	: 'POST',
				data    	: {  picklist_inc_id:picklist_id},
				success 	:	function( data  )
				{			
 					            
				}                
			});	
		}
}

function getCountry(){

	var postal_country = [];
	$.each($("#postal_country option:selected"), function(){            
		postal_country.push($(this).val());
	});
	var section = [];
	$.each($("#sections option:selected"), function(){            
		section.push($(this).val());
	});
	
  	$.ajax({
			url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/getPoatalServices',
			dataType	: 'json',
			type    	: 'POST',
			data    	: {  sections:section, postal_countries:postal_country },
 			success 	:	function( data  )
			{			
				    				  
				  var select = $('#postal_services');
					select.empty();
					select.append("<option value=''>---Select Postal Services---</option>");
				
					for (var j = 0; j < data.length; j++){
						console.log(data[j]);
						$("#postal_services").append("<option data-tokens='"+data[j]+"' value='" +data[j]+ "'>" +data[j]+ "</option>");
					}
  				 
 			}                
		});	
}

function changeSections(){

var select = $('#postal_services');
					select.empty();
					select.append("<option value=''>---Select Postal Services---</option>");
} ;

$(".btnpick").click(function(){
	
	    var error = 0;
		var section = [];
		$.each($("#sections option:selected"), function(){            
			section.push($(this).val());
		});			
		/*var postal_country = [];
		$.each($("#postal_country option:selected"), function(){            
			postal_country.push($(this).val());
		});*/
		var skus = [];
		$.each($("#sku option:selected"), function(){            
			skus.push($(this).val());
		});
		
		
		if($("#sections option:selected").val() == ''){	
		 alert('Please select section');
		 error++;
		}
		if($("#sku_type option:selected").val() == ''){	
		 alert('Please select sku in order');
		 error++;
		}
		if($("#postal_country option:selected").val() == ''){	
		 alert('Please select country');
		 error++;
		}
		if($("#postal_services option:selected").val() == ''){	
		 alert('Please select postal service');
		 error++;
		}
		if($("#picklist_before_date").val() == ''){
		  alert('Please select date.');
		  error++;
		}	
		if(error == 0){ 
		
		if(confirm('Pick list will be generated of all orders before '+$("#picklist_before_date").val()+' date.')){
			$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/createPicklist',
					dataType	: 'json',
					type    	: 'POST',
					data    	: {sections:section, sku_type:$("#sku_type option:selected").val(),postal_country:$("#postal_country option:selected").val(),postal_services:$("#postal_services option:selected").val(),skus:skus,picklist_before_date:$("#picklist_before_date").val()},
					success 	:	function( data  )
					{			
						           
					}                
				});	
			}
		
			<?php /*?>
			  window.location.href = '<?php echo Router::url('/', true); ?>DynamicPicklist/createPicklist/?sections='+section+'&sku_type='+$("#sku_type option:selected").val()+'&postal_country='+postal_country+'&postal_services='+$("#postal_services option:selected").val()+'&skus='+skus+'&picklist_before_date='+$("#picklist_before_date").val() ; <?php */?>
			
	   } 
   
	
});

 function assignUser(picklist_id){

 	if($("#assign_user_"+picklist_id+" option:selected").val()==''){            
		  alert('Please select user');
	}else{
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/assignUser',
				dataType	: 'json',
				type    	: 'POST',
				data    	: {  picklist_inc_id:picklist_id, assign_user:$("#assign_user_"+picklist_id+" option:selected").val()},
				success 	:	function( data  )
				{			
 					$("#user_"+picklist_id).html('Assign To:<strong>'+data.user+'</strong>');    
					$("#time_"+picklist_id).html('<?php echo date('Y-m-d H:i:s')?>');            
				}                
			});	
		}
}
 



 
</script>