<?php
$userID = $this->session->read('Auth.User.id');
?>
<style>
.selectpicker {border:1px solid #c6c6c6;}
.selectpicker:hover  {border:1px solid #666666;}
</style>
<div class="bottom_panel rightside bg-grey-100">
    <div class="container-fluid" >
       		
             <div class="panel">
			 <div class="panel-body">
				<div class="row">	
					<div class="col-lg-12">
						 						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose Section" name="sections" id="sections" onchange="getCountry();"> 
							 <?php   
 									 foreach($sections as $v){	
										 $selected = '';
										 if(isset($this->request->query['sections'])){
											 $exp = explode(",", $this->request->query['sections']);
											 if(in_array($v,$exp)){
												$selected = 'selected="selected"';
											 }
										 }					  
									 	 echo ' <option data-tokens="'.$v.'" value="'.$v.'" '.$selected.'>Section '.$v.'</option>';
								  						  
							  }?>
							 							
							</select>
						</div>	
						<!--
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose OrderType" name="order_type" id="order_type"  > 
							 <option data-tokens="A" value="A" selected="selected">OrderType A</option>	
							 <option data-tokens="B" value="B">OrderType B</option> 						
							</select>
						</div>	-->
						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose Country" name="postal_country" id="postal_country" onchange="getCountry()" > 
							 
							 
							  <?php foreach($countries as $v){	
							 		 $selected = '';
									 if(isset($this->request->query['postal_country'])){
										 $exp = explode(",", $this->request->query['postal_country']);
										 if(in_array($v,$exp)){
											$selected = 'selected="selected"';
										 }
									 }
									  									 							  
									 echo ' <option data-tokens="'.$v.'" value="'.$v.'" '.$selected.'>'.$v.'</option>';
								  						  
							  }?>
 
							</select>
						</div>	
						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="form-control" title="Choose Service" name="postal_services" id="postal_services"> 
							   				
							</select>
						</div>	 
						
						 <div class="col-md-1"> <button type="button"  class="btn btn-success btnsearch">GeneratePicklist</button></div>
						
						 
						</div>
						
						
						
						<div class="col-lg-12"><?php print $this->Session->flash(); ?></div>  
				 </div>
			 
			</div>
			</div>
			<div class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12 ">
					
							<table class="table table-bordered table-striped dataTable">
							<tr>
 								<th>PickList Name</th>
								<!--<th>Pc Name</th>-->
								<th>Total Orders</th>
								<th>Processed Orders</th>	
								<th>Sorted Orders</th>
								<th>Assign User</th>
								<th>Assign Time</th>
								<th>Created By</th>	
								<th>Creation Date</th>
								<th>Action</th>
							</tr>
 							<?php
								$userdata = array();
								$useroptions = '';
								foreach($users as $u){
									$userdata[$u['User']['email']] = $u['User']['first_name'].'&nbsp;'.$u['User']['last_name'];
									$useroptions .= '<option  data-tokens="'.$u['User']['email'].'" value="'.$u['User']['email'].'">'.$u['User']['first_name'].'&nbsp;'.$u['User']['last_name'].'</option>';
										 
								} 
								$user_email = strtolower($this->session->read('Auth.User.email'));
						 		foreach($DynamicPicklist as   $val){
 							?>
							<tr>
								<td><?php  echo $val['DynamicPicklist']['picklist_name'];?></td>
								<!--<td><?php  //echo $val['DynamicPicklist']['pc_name'];?></td>-->
								<td><?php  echo $val['DynamicPicklist']['order_count'];?></td>
								<td><?php  echo $val['DynamicPicklist']['order_count'];?></td>
								<td><?php  echo $val['DynamicPicklist']['order_count'];?></td>
								<td>
								
								<?php 
							  	 if(in_array($user_email,array('avadhesh.kumar@jijgroup.com','amit.gaur@jijgroup.com','mmarecky82@gmail.com','shashi.b.kumar@jijgroup.com','jake.shaw@euracogroup.co.uk','lalit.prasad@jijgroup.com','aakash.kushwah@jijgroup.com'))){?>
								
								<select class="selectpicker form-control" data-live-search="true" title="Assign User" name="assign_user" id="assign_user_<?php  echo $val['DynamicPicklist']['id'];?>" onchange="assignUser(<?php  echo $val['DynamicPicklist']['id'];?>);">
									<option value="">-Assign User-</option>
									<?php echo $useroptions; ?>
									
								</select>
								<?php }?>
								<div id="user_<?php  echo $val['DynamicPicklist']['id'];?>"><?php  if($val['DynamicPicklist']['assign_user_email']){echo 'Assign To:<strong>'.$userdata[$val['DynamicPicklist']['assign_user_email']].'</strong>';}?></div>
								</td>
									<td id="time_<?php  echo $val['DynamicPicklist']['id'];?>"><?php  echo $val['DynamicPicklist']['assign_time'];?></td>
									<td><?php  echo $val['DynamicPicklist']['created_username'];?></td>
								<td><?php  echo $val['DynamicPicklist']['created_date'];?></td>
								<td><a href="<?php echo Router::url('/', true). '/img/printPickList/'.$val['DynamicPicklist']['picklist_name']; ?>" target="_blank" style="text-decoration:underline;">Download</a></td>
 								</tr>
								<?php  
									}
								?> 								
						</table>
						
							 
						<ul class="pagination">
							  <?php
  								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>

				</div>
			</div>
			</div>        
		</div>
		
	</div>        
</div>   
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>
<script>
function getCountry(){

	var postal_country = [];
	$.each($("#postal_country option:selected"), function(){            
		postal_country.push($(this).val());
	});
	var section = [];
	$.each($("#sections option:selected"), function(){            
		section.push($(this).val());
	});
	
  	$.ajax({
			url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/getPoatalServices',
			dataType	: 'json',
			type    	: 'POST',
			data    	: {  sections:section, postal_countries:postal_country },
 			success 	:	function( data  )
			{			
				    				  
				  var select = $('#postal_services');
					select.empty();
					select.append("<option value=''>---Select Postal Services---</option>");
				
					for (var j = 0; j < data.length; j++){
						console.log(data[j]);
						$("#postal_services").append("<option data-tokens='"+data[j]+"' value='" +data[j]+ "'>" +data[j]+ "</option>");
					}
  				 
 			}                
		});	
}

function changeSections(){

var select = $('#postal_services');
					select.empty();
					select.append("<option value=''>---Select Postal Services---</option>");
} ;

$(".btnsearch").click(function(){
   var section = [];
	$.each($("#sections option:selected"), function(){            
		section.push($(this).val());
	});
	var order_type = [];
	$.each($("#order_type option:selected"), function(){            
		order_type.push($(this).val());
	});	
	var postal_country = [];
	$.each($("#postal_country option:selected"), function(){            
		postal_country.push($(this).val());
	});
		
	if($("#postal_services option:selected").val()){	
 	  window.location.href = '<?php echo Router::url('/', true); ?>DynamicPicklist/createPicklist/?sections='+section+'&order_type='+order_type+'&postal_country='+postal_country+'&postal_services='+$("#postal_services option:selected").val(); 
   }else{
 	  alert('Please select postal service');
   }
	
});

 function assignUser(picklist_id){

 	if($("#assign_user_"+picklist_id+" option:selected").val()==''){            
		  alert('Please select user');
	}else{
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/assignUser',
				dataType	: 'json',
				type    	: 'POST',
				data    	: {  picklist_inc_id:picklist_id, assign_user:$("#assign_user_"+picklist_id+" option:selected").val()},
				success 	:	function( data  )
				{			
 					$("#user_"+picklist_id).html('Assign To:<strong>'+data.user+'</strong>');    
					$("#time_"+picklist_id).html('<?php echo date('Y-m-d H:i:s')?>');            
				}                
			});	
		}
}
 



 
</script>