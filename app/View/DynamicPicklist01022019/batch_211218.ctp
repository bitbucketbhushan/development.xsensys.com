<?php
$userID = $this->session->read('Auth.User.id');
$users_array = array('avadhesh.kumar@jijgroup.com','amit.gaur@jijgroup.com','mmarecky82@gmail.com','shashi.b.kumar@jijgroup.com','jake.shaw@euracogroup.co.uk','lalit.prasad@jijgroup.com','aakash.kushwah@jijgroup.com');
?>
<style>
.selectpicker {border:1px solid #c6c6c6;}
.selectpicker:hover  {border:1px solid #666666;}
.completed{background-color:#c3f6c3 !important;}
.preview h3{color:#666;}
.preview th, .preview td{padding:5px; color:#424242;}
.btn-pad{margin-bottom:4px;}
.btn-pad-top{margin-top:4px;}
center{color:#FF0000;}
.c_total{color:#17C225;}
.c_count{ border-bottom: 1px dotted grey; }
</style>
<div class="bottom_panel rightside bg-grey-100">
    <div class="container-fluid" >
       		
             <div class="panel">
			 <div class="panel-body">
			 <div class="col-lg-12"><?php print $this->Session->flash(); ?></div>  
				<div class="row">	
						<div class="col-lg-6">
						 	<?php $total = 0; foreach( $countries as $countrie ) { 
								  $total = $total + $countrie['o_count'];
							?>
							<div class="col-md-6 c_count"><div class="col-md-6"><?php echo $countrie['country'] ?></div><div class="col-md-6"><?php echo $countrie['o_count'] ?></div></div>
							<?php } ?>
							
							
							<div class="col-md-6 c_total"><strong class="col-md-6">Total</strong><strong class="col-md-6"><?php echo $total; ?></strong></div>
						</div>
						<div class="col-lg-6">
						 	<?php $s_total = 0; foreach( $providers as $provider ) {
								  if($provider['service_provider'] != '' ) {
								  $s_total = $s_total + $provider['s_count'];
							?>
							<div class="col-md-6 c_count"><div class="col-md-6"><?php echo $provider['service_provider'] ?></div><div class="col-md-6"><?php echo $provider['s_count'] ?></div></div>
							<?php } } ?>
							
							
							
						</div>
						<div class="col-md-6 c_total"><strong class="col-md-3">&nbsp;</strong><strong class="col-md-3">Total</strong><strong class="col-md-3"><?php echo $total; ?></strong></div>
							<!--<div class="col-md-3">
								<input type="text" class="form-control" id="usr">
							</div>	
							<div class="col-md-1"> <button type="button"  class="btn btn-info pick_preview">Create Batch</button></div>-->
						</div>
				  
				  
				  		<div class="row" style="margin-top: 50px;">	
							<div class="col-md-12">
								<div class="col-md-3">&nbsp;</div>
								<div class="col-md-3"><input type="text" class="form-control" id="fill_order" value="<?php echo $total; ?>"></div>
								<div class="col-md-3"> <button type="button"  class="btn btn-info create_batch">Create Batch</button></div>
								<div class="col-md-3">&nbsp;</div>
							</div>	
							
						</div>
				  
				  
						
				 </div>
			 
			</div>
			</div>
			
			<div class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12 ">
					
							<table class="table table-bordered table-striped dataTable">
							<tr>
 								<th>Batch Name</th> 
								<th>Total Orders</th>
								<th>Creater Name</th>	
								<th>Creation Date</th>
							</tr>
 							<?php foreach($b_name_arr as $val) { ?>
							<tr class="">
								<td><?php  echo $val['batch_name']; ?>
									<?php foreach( $val['country'] as $v ){ 
										echo '<div><b>'.$v['country'].'[ '.$v['o_count'].' ]</b></div>';
									} ?>
								
								</td>
								<td><?php  echo $val['order_count'];?></td>
								<td><?php  echo $val['creater_name'];?></td>
								<td><?php  echo $val['created_date'];?></td>
							</tr>
							<?php } ?> 								
						</table>
						
							 
						<ul class="pagination">
							  <?php
  								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>

				</div>
			</div>
			</div>        
		</div>
		
	</div>        
</div>   
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="<?php echo Router::url('/', true); ?>img/ajax-loader.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>

<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
 <script>
 
 
	
$(".pick_preview").click(function(){
	
	    var error = 0;
		var section = [];
		$.each($("#sections option:selected"), function(){            
			section.push($(this).val());
		});			
		/*var postal_country = [];
		$.each($("#postal_country option:selected"), function(){            
			postal_country.push($(this).val());
		});*/
		var skus = [];
		$.each($("#sku option:selected"), function(){            
			skus.push($(this).val());
		});
		
		
		if(section.length < 1){	
		 alert('Please select section.');
		 error++;
		}
		if($("#sku_type option:selected").val() == ''){	
		 alert('Please select sku type.');
		 error++;
		}
		if($("#postal_country option:selected").val() == ''){	
		 alert('Please select country.');
		 error++;
		}
		if($("#postal_services option:selected").val() == ''){	
		 alert('Please select postal service.');
		 error++;
		}
		if($("#picklist_before_date").val() == ''){
		  alert('Please select date.');
		  error++;
		}	
		if(error == 0){ 
		
		//if(confirm('Pick list will be generated of all orders till '+$("#picklist_before_date").val()+' date.')){
			$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/createPicklist',
					dataType	: 'json',
					type    	: 'POST',
					beforeSend  :  function() {$('.outerOpac').show()},
					data    	: {sections:section, sku_type:$("#sku_type option:selected").val(),till_date:$("#picklist_before_date").val(),postal_country:$("#postal_country option:selected").val(),postal_services:$("#postal_services option:selected").val(),skus:skus},
					success 	:	function( data  )
					{			
						     $(".preview").html(data.preview ) ;  
							 $('.outerOpac').hide(); 
					}                
				});	
			//}
  	   } 
   
	
});

$(".create_batch").click(function(){
	var o_f_batch	=	$('#fill_order').val();
	alert(o_f_batch);
	$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/createBatch',
					dataType	: 'json',
					type    	: 'POST',
					beforeSend  :  function() {$('.outerOpac').show()},
					data    	: {o_f_batch:o_f_batch},
					success 	:	function( data  )
					{			
						     $(".preview").html(data.preview ) ;  
							 $('.outerOpac').hide();
							  location.reload(true);     
					}                
				});	
});



 

 
</script>