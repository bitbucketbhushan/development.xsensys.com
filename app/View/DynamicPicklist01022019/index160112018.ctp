<?php
$userID = $this->session->read('Auth.User.id');
?>
<div class="bottom_panel rightside bg-grey-100">
    <div class="container-fluid" >
       		
             <div class="panel">
			 <div class="panel-body">
				<div class="row">	
					<div class="col-lg-12">
						 <?php /*?><div class="col-md-2 col-lg-2" style="padding-right:0px;"> 
						<div class="btn-group">
						 <button class="btn btn-success ">PickList Options</button>
						  <button class="btn btn-success  dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
						  <ul class="dropdown-menu">
							<li><a href="<?php echo Router::url('/', true).'Picklist/generate_picklist/all';?>" style="background-color:#99FFFF;">Generate Pick List Of All Open Ordders</a></li>
							<li><a href="<?php echo Router::url('/', true).'Picklist/generate_picklist/selsku';?>" style="background-color:#CC99FF;">Generate Pick List Of Selected SKU </a></li>
							<li><a href="<?php echo Router::url('/', true).'Picklist/generate_picklist/daterange'?>" style="background-color:#99FFFF;">Generate Pick List Of Date Range</a></li>
							<li><a href="javascript:void(0);" onclick="plByLocation();" style="background-color:#CC99FF;">Generate Pick List Of Selected Locations</a></li>
							<li><a href="<?php echo Router::url('/', true).'Picklist/generate_picklist/express'?>" style="background-color:#99FFFF;">Generate Pick List Of DHL orders</a></li>
							<?php if(count($files) > 0 ) {?>
								<li class="dropdown-submenu">  <a href="#" style="background-color:#FFFF66;">Download Picklist</a>  	 
								<ul class="dropdown-menu" style="background-color:#FFFF99;">
								<?php 
								$pdfPath = '/img/printPickList/'; 
								foreach( $files as $file ) {
									$time = explode('_', $file) ;
									
									$minute = explode('.', $time[7]) ;
									$picklistDate	=	 $time[3].'-'.$time[4].'-'.$time[5];
									
								if(strtotime(date($time[5].'-'.$time[4].'-'.$time[3])) >  strtotime('-7 day', strtotime(date('Y-m-d'))) ){
									
								?>
								<li><a href="<?php echo $pdfPath.$file; ?>" role="menuitem" target ="_blank" >Pick List <b>[ <?php echo $picklistDate.' '.$time[6].' : '.$minute[0] ?> ]</b></a></li> 
								<?php } }?>
								</ul>
							</li>
							<?php } ?>              
						  </ul>
						</div>		
								
						</div>	<?php */?>						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose Section" name="sections" id="sections"  > 
							 <?php   
 									 foreach($sections as $v){	
										 $selected = '';
										 if(isset($this->request->query['sections'])){
											 $exp = explode(",", $this->request->query['sections']);
											 if(in_array($v,$exp)){
												$selected = 'selected="selected"';
											 }
										 }					  
									 	 echo ' <option data-tokens="'.$v.'" value="'.$v.'" '.$selected.'>Section '.$v.'</option>';
								  						  
							  }?>
							 							
							</select>
						</div>	
						<!--
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose OrderType" name="order_type" id="order_type"  > 
							 <option data-tokens="A" value="A" selected="selected">OrderType A</option>	
							 <option data-tokens="B" value="B">OrderType B</option> 						
							</select>
						</div>	-->
						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose Country" name="postal_country" id="postal_country" onchange="getCountry()" > 
							 
							 
							  <?php foreach($countries as $v){	
							 		 $selected = '';
									 if(isset($this->request->query['postal_country'])){
										 $exp = explode(",", $this->request->query['postal_country']);
										 if(in_array($v,$exp)){
											$selected = 'selected="selected"';
										 }
									 }
									  									 							  
									 echo ' <option data-tokens="'.$v.'" value="'.$v.'" '.$selected.'>'.$v.'</option>';
								  						  
							  }?>
 
							</select>
						</div>	
						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="form-control" title="Choose Service" name="postal_services" id="postal_services"> 
							   				
							</select>
						</div>	 
						
						 <div class="col-md-1"> <button type="button"  class="btn btn-success btnsearch">GeneratePicklist</button></div>
						
						 
						</div>
						
						
						
						<div class="col-lg-12"><?php print $this->Session->flash(); ?></div>  
				 </div>
			 
			</div>
			</div>
			<div class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12 ">
					
							<table class="table table-bordered table-striped dataTable">
							<tr>
 								<th>PickList Name</th>
								<!--<th>Pc Name</th>-->
								<th>Total Orders</th>
								<th>Processed Orders</th>	
								<th>Sorted Orders</th>
								<th>Assign User</th>
								<th>Assign Time</th>
								<th>Created By</th>	
								<th>Creation Date</th>
								<th>Action</th>
							</tr>
 							<?php
								$userdata = array();
						 		foreach($DynamicPicklist as   $val){
 							?>
							<tr>
								<td><?php  echo $val['DynamicPicklist']['picklist_name'];?></td>
								<!--<td><?php  //echo $val['DynamicPicklist']['pc_name'];?></td>-->
								<td><?php  echo $val['DynamicPicklist']['order_count'];?></td>
								<td><?php  echo $val['DynamicPicklist']['order_count'];?></td>
								<td><?php  echo $val['DynamicPicklist']['order_count'];?></td>
								<td><select name="assign_user" id="assign_user_<?php  echo $val['DynamicPicklist']['id'];?>" onchange="assignUser(<?php  echo $val['DynamicPicklist']['id'];?>);" >
									<option value="">-User-</option>
									<?php  foreach($users as $u){
										$userdata[$u['User']['email']] = $u['User']['first_name'].'&nbsp;'.$u['User']['last_name'];
										echo '<option value="'.$u['User']['email'].'">'.$u['User']['first_name'].'&nbsp;'.$u['User']['last_name'].'</option>';
										 
									}?>
									
								</select>
								<?php  if($val['DynamicPicklist']['assign_user_email']){echo '<strong>Assign To:'.$userdata[$val['DynamicPicklist']['assign_user_email']].'</strong>';}?>
								</td>
									<td><?php  echo $val['DynamicPicklist']['assign_time'];?></td>
									<td><?php  echo $val['DynamicPicklist']['created_username'];?></td>
								<td><?php  echo $val['DynamicPicklist']['created_date'];?></td>
								<td><a href="<?php echo Router::url('/', true). '/img/printPickList/'.$val['DynamicPicklist']['picklist_name']; ?>" target="_blank" style="text-decoration:underline;">Download</a></td>
 								</tr>
								<?php  
									}
								?> 								
						</table>
						
							 
						<ul class="pagination">
							  <?php
  								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>

				</div>
			</div>
			</div>        
		</div>
		
	</div>        
</div>   
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>
<script>
function getCountry(){

	var postal_country = [];
	$.each($("#postal_country option:selected"), function(){            
		postal_country.push($(this).val());
	});
	
  	$.ajax({
			url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/getPoatalServices',
			dataType	: 'json',
			type    	: 'POST',
			data    	: {  postal_countries:postal_country },
 			success 	:	function( data  )
			{			
				    				  
				  var select = $('#postal_services');
					select.empty();
					select.append("<option value=''>---Select Postal Services---</option>");
				
					for (var j = 0; j < data.length; j++){
						console.log(data[j]);
						$("#postal_services").append("<option data-tokens='"+data[j]+"' value='" +data[j]+ "'>" +data[j]+ "</option>");
					}
  				 
 			}                
		});	
}

$(".btnsearch").click(function(){
   var section = [];
	$.each($("#sections option:selected"), function(){            
		section.push($(this).val());
	});
	var order_type = [];
	$.each($("#order_type option:selected"), function(){            
		order_type.push($(this).val());
	});	
	var postal_country = [];
	$.each($("#postal_country option:selected"), function(){            
		postal_country.push($(this).val());
	});
		
	if($("#postal_services option:selected").val()){	
 	  window.location.href = '<?php echo Router::url('/', true); ?>DynamicPicklist/createPicklist/?sections='+section+'&order_type='+order_type+'&postal_country='+postal_country+'&postal_services='+$("#postal_services option:selected").val(); 
   }else{
 	  alert('Please select postal service');
   }
	
});

 function assignUser(picklist_id){

	 
	if($("#assign_user_"+picklist_id+" option:selected").val()==''){            
		  alert('Please select user');
	}else{
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/assignUser',
				dataType	: 'json',
				type    	: 'POST',
				data    	: {  picklist_inc_id:picklist_id, assign_user:$("#assign_user_"+picklist_id+" option:selected").val()},
				success 	:	function( data  )
				{			
					 
					 
				}                
			});	
		}
}
	
function yyassignUser(picklist_id){
	var slecteditem = [];
	$.each($("#bin_location option:selected"), function(){            
		slecteditem.push($(this).val());
	});		
   window.location.href = '<?php echo Router::url('/', true); ?>Picklist/PicklistByLocation?loc='+slecteditem; 

}
$(function() {

  $('#datefilter').daterangepicker({     
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('#datefilter').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY')); 
	  document.dt.submit();
   });

  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});


 
function selectSku(id,sku,order_ids){


jQuery("#u"+ mid ).addClass("glyphicon-spin");
	$.ajax({
		dataType: 'json',
		url : 'ajax/manifest_ajax.php',
		type: "POST",
		data : {action:'update_postal',code:mcode,id:mid},
		success: function(data, textStatus, jqXHR)
		{				
			jQuery("#u"+ mid).removeClass("glyphicon-spin");
			$('#message').show();				
			$("#message").html(data.msg);	
			$("#loading-mask").hide();
			if(data.error != 'yes'){
				$("#tr_" + mid + " td .upd").removeClass('btn-primary');
				$("#tr_" + mid + " td .upd").addClass('btn-success');
			}
			//loadajax();							
		}		
	});
		 	
	$.ajax({
			url     	: '<?php echo Router::url('/', true); ?>Picklist/selectSku',
			dataType	: 'json',
			type    	: 'POST',
			data    	: {  sku:sku,order_ids : order_ids, action:$( "#"+id ).text() },
			beforeSend	: function() {
				//$( ".outerOpac" ).attr( 'style' , 'display:block');
				$( "#"+id ).text( 'Wait...');
			},   			  
			success 	:	function( data  )
			{			
				 if(data.msg == 'inserted'){
					  $( "#"+id ).addClass( 'selected');						 
					  $( "#"+id ).text( 'Selected');
				  }else{
					  $( "#"+id ).removeClass( 'selected');						 
					  $( "#"+id ).text( 'Select');
				  }
				 // $( ".outerOpac" ).attr( 'style' , 'display:none');
				 
			}                
		});	
	}



 
</script>