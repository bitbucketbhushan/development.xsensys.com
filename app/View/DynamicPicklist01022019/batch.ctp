<?php
$userID = $this->session->read('Auth.User.id');
$users_array = array('avadhesh.kumar@jijgroup.com','amit.gaur@jijgroup.com','mmarecky82@gmail.com','shashi.b.kumar@jijgroup.com','jake.shaw@euracogroup.co.uk','lalit.prasad@jijgroup.com','aakash.kushwah@jijgroup.com');
App::import('Controller', 'DynamicPicklist');
$dyna_pick 	=	new DynamicPicklistController;
?>
<style>
.selectpicker {border:1px solid #c6c6c6;}
.selectpicker:hover  {border:1px solid #666666;}
.completed{background-color:#c3f6c3 !important;}
.preview h3{color:#666;}
.preview th, .preview td{padding:5px; color:#424242;}
.btn-pad{margin-bottom:4px;}
.btn-pad-top{margin-top:4px;}
center{color:#FF0000;}
.c_total{color:#17C225;}
.c_count{ border-bottom: 1px dotted grey; }
</style> 
<div class="bottom_panel rightside bg-grey-100">
    <div class="container-fluid" >
       		
             <div class="panel">
			 <div class="panel-body">
			 <div class="col-lg-12"><?php print $this->Session->flash(); ?></div>  
				<div class="row">	
						<div class="col-lg-4">
							<?php $total = 0; foreach( $countries as $countrie ) { 
								  $total = $total + $countrie['o_count'];
								  }
							?>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="panel bg-blue-400">
									<div class="panel-body padding-15-20">
										<div class="clearfix">
											<div class="pull-left">
												<div class="display-block color-blue-50 font-weight-600"><i class="ion-plus-round"></i> Orders available for new batch </div>
												<div class="color-white font-size-26 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="343" data-speed="500" data-refresh-interval="10">
													<!--<?php echo $total; ?>-->
													<?php echo $o_data['reanige_ord_count'] ?>
												</div>
												
											</div>
											<div class="pull-right">
												<i class="font-size-36 color-blue-100 ion-ios-cart"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
						 	<div class="col-md-6"><input type="text" class="form-control" id="fill_order" value="<?php echo $o_data['reanige_ord_count']; ?>"></div>
							<div class="col-md-6"> <button type="button"  class="btn btn-info create_batch">Create Batch</button></div>
						</div>
					</div>
				 </div>
			</div>
			</div>
			
			<div class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12 ">
						<table class="table table-bordered table-striped dataTable">
							<tr>
 								<th>Batch Name</th>
								<th>Country</th> 
								<th>Service</th>
								<th>Total Orders</th>
								<th>Batch Status</th>
								<th>Creater Name</th>	
								<th>Creation Date</th>
								<th>Action</th>
							</tr>
 							<?php 
							App::import('Controller', 'DynamicPicklist');
							$dyna_pick 	=	new DynamicPicklistController;
							$t_b_o = 0;
							foreach($b_name_arr as $val) { ?>
							<tr class="">
								<td><?php  echo $val['batch_name']; ?></td>
								<td><?php foreach( $val['country'] as $v ){ 
										echo '<div><b>'.$v['country'].'[ '.$v['o_count'].' ]</b></div>';
									} ?>
								</td>
								<td>
									<?php foreach( $val['service'] as $v ){ 
										if($v['service_provider'] != null){
											echo '<div><b>'.$v['service_provider'].'[ '.$v['s_count'].' ]</b></div>';
										} else {
											echo '<div><b>No Service'.'[ '.$v['s_count'].' ]</b></div>';
										}
									} ?>
								</td>
								<td><?php 
											$qty		=	$dyna_pick->getOrderStatusQty( $val['batch_name'] );
								 			echo '<b>Open Orders:- </b>'.$qty['open_order']."<br>";
											echo '<b>Processed Orders:- </b>'.$qty['proc_order']."<br>";
											echo '<b>Canceled Orders:- </b>'.$qty['canc_order']."<br>";
											echo '<b>Locked Orders:- </b>'.$qty['lock_order']."<br>";
									?>
								</td>
								<td><?php  
										$bat_ststus		=	$dyna_pick->getbatchQty( $val['batch_name'] ); 
										echo '<b>Order to Process:- </b>'.$bat_ststus['b_orders']."<br>";
										echo '<b>Processed Orders:- </b>'.$bat_ststus['p_b_orders']."<br>";
									?>
								</td>
								<td><?php  echo $val['creater_name'];?></td>
								<td><?php  echo $val['created_date'];?></td>
								<td><a href="<?php echo Router::url('/', true). 'DynamicPicklist/index/'.$val['batch_name']; ?>" target="_blank" class="btn btn-info" style="text-decoration:underline">Go To Picklist</a>
								<a href="javascript:void(0);"  for="<?php echo $val['batch_name']; ?>" class="mark_complete btn btn-success" style="text-decoration:underline">Complete</a>
								</td>
							</tr>
							<input type="hidden" id="total1" name="batchname" value="<?php echo $val['status'].'####'.$val['batch_name']; ?>" class="batchname" />
							<?php 
							
							$t_b_o = $t_b_o + $val['order_count'];
							} 
							?> 								
						</table>
						<input type="hidden" id="total" name="total" value="<?php echo $t_b_o; ?>" />
							 
						<ul class="pagination">
							  <?php
  								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>
				</div>
			</div>
			</div>        
		</div>
	</div>        
</div>   
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="<?php echo Router::url('/', true); ?>img/ajax-loader.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>

<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
 <script>
 
 
	
$(".pick_preview").click(function(){
	
	    var error = 0;
		var section = [];
		$.each($("#sections option:selected"), function(){            
			section.push($(this).val());
		});			
		/*var postal_country = [];
		$.each($("#postal_country option:selected"), function(){            
			postal_country.push($(this).val());
		});*/
		var skus = [];
		$.each($("#sku option:selected"), function(){            
			skus.push($(this).val());
		});
		
		
		if(section.length < 1){	
		 alert('Please select section.');
		 error++;
		}
		if($("#sku_type option:selected").val() == ''){	
		 alert('Please select sku type.');
		 error++;
		}
		if($("#postal_country option:selected").val() == ''){	
		 alert('Please select country.');
		 error++;
		}
		if($("#postal_services option:selected").val() == ''){	
		 alert('Please select postal service.');
		 error++;
		}
		if($("#picklist_before_date").val() == ''){
		  alert('Please select date.');
		  error++;
		}	
		if(error == 0){ 
		
		//if(confirm('Pick list will be generated of all orders till '+$("#picklist_before_date").val()+' date.')){
			$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/createPicklist',
					dataType	: 'json',
					type    	: 'POST',
					beforeSend  :  function() {$('.outerOpac').show()},
					data    	: {sections:section, sku_type:$("#sku_type option:selected").val(),till_date:$("#picklist_before_date").val(),postal_country:$("#postal_country option:selected").val(),postal_services:$("#postal_services option:selected").val(),skus:skus},
					success 	:	function( data  )
					{			
						     $(".preview").html(data.preview ) ;  
							 $('.outerOpac').hide(); 
					}                
				});	
			//}
  	   } 
   
	
});

$(".mark_complete").click(function(){
			
			var batnum	=	$( this ).attr( 'for' );
			var batch_order 	= <?php echo $bat_ststus['b_orders']; ?>;
			var proc_b_order 	= <?php echo $bat_ststus['p_b_orders']; ?>;
			if(batch_order != proc_b_order)
			{
				alert('Barch Order is remaning for process.');
				return false;
			}
			$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/markcompletebatch',
					dataType	: 'json',
					type    	: 'POST',
					beforeSend  :  function() {$('.outerOpac').show()},
					data    	: {batnum:batnum},
					success 	:	function( data  )
					{			
							alert( data );
						     $(".preview").html(data.preview ) ;  
							 $('.outerOpac').hide();
							  location.reload(true);     
					}                
				});

});

$(".create_batch").click(function(){
	var o_f_batch	=	$('#fill_order').val();
	var remaning 	= <?php echo $o_data['reanige_ord_count'] ?>;
	var str='';
	
	var bnum = [];
	var count =0;
	$.each($(".batchname"), function(){
			str = $( this ).val();            
			var res = str.split("####");
			if(res[0] == 1){
				bnum.push(res[1]);
				count++;
			}
		});
	if(count > 0){
		if(confirm("Still we have below batch not complete, want create a new batch. [ "+bnum+' ]')){
				if( remaning < o_f_batch)
				{
					alert( 'Only a batch with '+remaning+' quantity can be created.' );
					return false;
				}
			$.ajax({
						url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/createBatch',
						dataType	: 'json',
						type    	: 'POST',
						beforeSend  :  function() {$('.outerOpac').show()},
						data    	: {o_f_batch:o_f_batch},
						success 	:	function( data  )
						{			
								 $(".preview").html(data.preview ) ;  
								 $('.outerOpac').hide();
								  location.reload(true);     
						}                
					});	
			
		}
	}
	
	
});

$( document ).ready(function() {
    var total	=	$('#total').val();
	if(total > 0){
		$('.b_total').text( total );
	} else {
		$('.b_total').text( '---' );
	}
	//var afba	=	<?php echo $total ?> - total;
	//$('#fill_order').val( afba );
});
 
</script>