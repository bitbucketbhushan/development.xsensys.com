<?php
$userID = $this->session->read('Auth.User.id');
$user_email = strtolower($this->session->read('Auth.User.email'));
 
$users_array = array('avadhesh.kumar@jijgroup.com','patrycja.swiatek@onet.eu','pappu.k@euracogroup.co.uk','zuzanna.swiatek001@gmail.com');
?>
<style>
.selectpicker {border:1px solid #c6c6c6;}
.selectpicker:hover  {border:1px solid #666666;}
.dvs{border-bottom:1px dashed #666666;width: 100%;clear: both; float:left; padding:5px 0;}
.dvs div{padding:5px 0;}
.pmsg{color:#FF0000; font-weight:bold;}
.collapse2{visibility: visible;position: absolute;z-index: +1;width: 99%;background: #fff;border: 1px solid #ddd;}
.checkbox input[type="checkbox"]{margin-left:5px;position: relative;}
.btninfo{width:99%; text-align:left;}  
.returned{background-color:#FF99FF !important;}	
</style> 

<div class="bottom_panel rightside bg-grey-100"> 
	<div class="page-head bg-grey-100">        
		<h1 class="page-title"><?php echo "Staff Orders"; ?></h1>
		<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		
     </div>
	
    <div class="container-fluid" >
  		<div class="panel"> 
			<div class="panel-body">
			
			<div class="row">
			 <div class="col-lg-12 form-inline">
					<div class="form-group col-35">
						<label class="col-lg-4" for="staff_person_email" style="padding-left:0px ;padding-right:0px">Select Staff Person:</label>
						<?php 
						$useroptions = '';  $userByEmail = []; $userById = [];
						foreach($users as $u){
							$userByEmail[$u['User']['email']] = ['first_name'=>$u['User']['first_name'],'last_name'=>$u['User']['last_name']];
							$userById[$u['User']['id']] = ['first_name'=>$u['User']['first_name'],'last_name'=>$u['User']['last_name']];
							
							if($u['User']['country'] == 3){
								$useroptions .= '<option  data-tokens="'.$u['User']['email'].'" value="'.$u['User']['email'].'">'.$u['User']['first_name'].'&nbsp;'.$u['User']['last_name'].'</option>';
							}
						} 
						?>
						<div class="col-lg-8" style="padding-left:0px"><select class="selectpicker form-control" data-live-search="true" title="Staff Person" name="staff_person_email" id="staff_person_email">
							<option value="">-Select Staff Person-</option>
							<?php echo $useroptions; ?>
						</select>
						</div>						
				  </div>
				  <div class="form-group col-32">
						<label for="product_barcode">&nbsp;Scan Product Barcode:</label>
						<input type="text" name="product_barcode" id="product_barcode" class="form-control" />
				  </div> 
				  
				  
				  <div class="form-group col-27 "><div id="bs-daterangepicker" class="btn bg-grey-50  no-border color-grey-600 border-radius-25 hidden-xs no-shadow"><i class="ion-calendar"></i> <span></span> <i class="ion-ios-arrow-down"></i></div></div>
				  
				  <div class="col-6">
						<button id="export" type="button" class="btn btn-primary">Export</button>
				</div>
				
				
				</div>	
					 
  			</div> 
			<div class="row">
				<div class="col-lg-12 ">
				<table class="table table-bordered table-striped dataTable" id="html-div">
				
				 </table>	 
				</div>		 
  			</div>	
			<form method="get" name="searchfrm"  id="searchfrm" action="<?php echo Router::url('/', true) ?>StaffOrders" enctype="multipart/form-data">	
			 	
			<div class="col-sm-12"  style="border-bottom:1px solid #bbb; border-top:1px solid #bbb; padding:5px;background:#ddd;">			 
				<div class="col-sm-3"><input type="text" name="search" id="search" class="form-control" value="<?php if(isset($_REQUEST['search']) && !empty($_REQUEST['search'])){ echo $_REQUEST['search'];}?>" /></div>
				<div class="col-sm-1"><button type="submit" class="btn btn-success">Search</button></div>
   			</div> 	
			</form>		
			<div class="row">
				<div class="col-lg-12 ">
				
						<table class="table table-bordered table-striped dataTable">
						<tr>
							<th>Order Id</th> 
							<th colspan="4"> <div class="col-sm-12" style="padding: 0px;"> 
								 <div class="col-20">SKU</div>
								 <div class="col-20">Barcode</div>
								 <div class="col-40">Product Title</div>
								 <div class="col-10">Qty</div>
								 <div class="col-10">Unit Price</div>
								 </div ></th>
 							<th>Order Total</th>	
							<th>Staff Person</th>	
							<th>Created By</th>	
							<th>Creation Date</th>
							<th>Status / Action</th>
						</tr>
						<?php 
							foreach($getStaffOrders as $pv =>  $val){ 
						?>
						<tr class="<?php echo $val['StaffOrder']['status'];?>ed">
							<td><?php  echo $val['StaffOrder']['order_id'];?></td>
							<td colspan="4"> <?php 
							$total = 0; $i = 0;
							foreach($staffOrders[$val['StaffOrder']['order_id']] as $item){
								$i++;
								$cls = '';
								if((count($staffOrders[$val['StaffOrder']['order_id']]) - $i) > 0){ $cls = 'dvs';}
							
								echo	'<div class="col-sm-12 '.$cls.'" style="padding: 0px;">';
								echo	'<div class="col-20">' . $item['StaffOrder']['sku'].'</div>';
								echo	'<div class="col-20">' . 	$item['StaffOrder']['barcode'].'</div>';
								echo	'<div class="col-40">' . 	$item['StaffOrder']['product_title'].'</div>';
								echo	'<div class="col-10">' . 	$item['StaffOrder']['quantity'].'</div>';
								echo	'<div class="col-10">' . 	$item['StaffOrder']['unit_price'].'</div>'; 
								echo	'</div >';
								
								$total += $item['StaffOrder']['quantity'] * $item['StaffOrder']['unit_price'];
							}
							
							?></td> 
							<td><?php  echo $total;?></td>
							<td><?php  echo $userByEmail[$val['StaffOrder']['staff_person_email']]['first_name'] .' '.$userByEmail[$val['StaffOrder']['staff_person_email']]['last_name'];?></td>
							<td><?php  echo $userById[$val['StaffOrder']['created_by_userid']]['first_name'].' '.$userById[$val['StaffOrder']['created_by_userid']]['last_name'];?></td>
							<td><?php  echo date('d M Y', strtotime($val['StaffOrder']['created_date']));?></td>
							<td><?php 
							if($val['StaffOrder']['status'] == 'return'){
 								$return_date = date('d M Y', strtotime($val['StaffOrder']['return_date']));
								echo '<strong title="Return Date:'.$return_date.'">RETURNED</strong>';
							}else{
							
								if(strtotime($val['StaffOrder']['created_date']) < strtotime("-7 days")){
									echo '<strong title="Staff Person can return with in 7 days.">ACTIVE</strong>';
								}else{
									echo '<button title="Staff Person can return with in 7 days." type="button" class="btn btn-success btn-sm" onclick="ReturnOrder('.$val['StaffOrder']['order_id'].');">Return</button>';
								}
							}
 							 
							 ?>
						 
							 </td>
							</tr>
							<?php  
								}
							?> 								
					</table>
						 
					<ul class="pagination">
						  <?php
							   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
							   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
						  ?>
					 </ul>

			</div>
		</div>
			</div>        
		</div>
	</div>        
</div>   
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="<?php echo Router::url('/', true); ?>img/ajax-loader.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>

<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>

$('body').on('click', '#export', function(){

	var getFrom 	= $('div.daterangepicker_start_input input:text').val();
	var getEnd 		= $('div.daterangepicker_end_input input:text').val();
	var dateArEnd 	= getEnd.split('/');
	var newDateEnd 	= dateArEnd[2] + '-' + dateArEnd[0].slice(-2) + '-' + dateArEnd[1];
	var dateArFrom 	= getFrom.split('/');
	var newDateFrom = dateArFrom[2] + '-' + dateArFrom[0].slice(-2) + '-' + dateArFrom[1];

	window.location = "<?php echo Router::url('/', true) ?>StaffOrders/downloadSheet?sdate="+newDateFrom+"&edate="+newDateEnd;
});

$(document).ready(function() {		
	removeItem();  	
});
 
$("#search" ).on( "keydown", function(event) {
	if(event.which == 13) {
		$( "#searchfrm" ).submit();
	}
});
 
 
$( "#product_barcode" ).on( "keydown", function(event) {
	 if(event.which == 13) {
		 checkProduct();
	}
});

function checkProduct(){
 	var error = 0; var barcode = $("#product_barcode").val();
 	if($("#staff_person_email option:selected").val() == ''){	
	 alert('Please select staff person.');
	 error++;
	} 
	if( ($("#product_barcode").val()).length < 10 ){	
	 alert('Please enter or scan correct barcode.');
	 error++;
	}
	if(error == 0){
		
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>StaffOrders/checkProduct',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  :  function() {/*$('.outerOpac').show()*/},
				data    	: {product_barcode:$("#product_barcode").val(),quantity:$("#quantity_"+barcode+" option:selected").val(),staff_person_email:$("#staff_person_email option:selected").val()},
				success 	:	function( data  )
				{ 						 					   
					  
					$("#product_barcode").val('');
					if(data.msg != undefined && data.msg != ''){
						alert(data.msg);
						$("#html-div" ).html(data.html);
					}else if(data.product_name == undefined){
						alert('Product not found!');
					}else{
						$("#html-div" ).html(data.html);
					}
 					$('.outerOpac').hide();
					
				}                
			});	
		 
	}
	 
}

function removeItem( barcode){  
	 
 	$.ajax({
			url     	: '<?php echo Router::url('/', true); ?>StaffOrders/removeItem',
			dataType	: 'json',
			type    	: 'POST',
			beforeSend  : function() {$('.outerOpac').show()},
			data    	: {barcode:barcode,current_stock:$("#current_stock_"+barcode).text()},
			success 	:	function( data  )
			{ 						 					   
				 $("#html-div" ).html(data.html );
 				 $('.outerOpac').hide();
			}                
		});	
		 
} 

function updateQty( barcode){
  		 
		$.ajax({
			url     	: '<?php echo Router::url('/', true); ?>StaffOrders/updateQty',
			dataType	: 'json',
			type    	: 'POST',
			beforeSend  : function() {$('.outerOpac').show()},
			data    	: {barcode:barcode,quantity:$("#quantity_"+barcode+" option:selected").val(),current_stock:$("#current_stock_"+barcode).text(),price:$("#price_"+barcode).text(),staff_person_email:$("#staff_person_email option:selected").val()},
			success 	:	function( data  )
			{ 						 					   
				 $("#html-div" ).html(data.html );
				 if(data.msg != ''){
				   alert(data.msg);
				 }
				 if(data.total != undefined){
					$("#total_"+barcode).text(data.total);  
  				 }
				 if(data.cart_total != undefined){
					$("#cart_total").text(data.cart_total);  
  				 }
				 $("#product_barcode").val('');
				 
				 
				
 				 $('.outerOpac').hide();
			}                
		});	
}

function createOrder(){
 	if($("#staff_person_email option:selected").val() == ''){	
 	 	alert('Please select staff person.');
  	} else{
 		if(confirm('Are you sure want to create order?')){
			$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>StaffOrders/createOrder',
					dataType	: 'json',
					type    	: 'POST',
					beforeSend  :  function() {/*$('.outerOpac').show()*/},
					data    	: {staff_person_email:$("#staff_person_email option:selected").val()},
					success 	:	function( data  )
					{ 
						$('.outerOpac').hide();
 						alert(data.msg);
						if(data.status != undefined && data.status == 2){
							location.reload(true); 
						}else{
							$("#html-div" ).html('');
						}
 					}                
				});	
		}	 
	}
}

function ReturnOrder(order_id){
 	if(confirm('Are you sure want to return this item?')){
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>StaffOrders/ReturnOrder',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  :  function() {/*$('.outerOpac').show()*/},
				data    	: {order_id:order_id},
				success 	:	function( data  )
				{ 
					 $('.outerOpac').hide();				 
					 alert(data.msg);
					 location.reload(true);  	
				}                
			});	
		 
	 }
} 
</script>
 