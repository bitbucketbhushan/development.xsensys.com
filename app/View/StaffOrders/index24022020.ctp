<?php
$userID = $this->session->read('Auth.User.id');
$user_email = strtolower($this->session->read('Auth.User.email'));

/*$users_array = array('avadhesh.kumar@jijgroup.com','amit.gaur@jijgroup.com','shashi.b.kumar@jijgroup.com','jake.shaw@euracogroup.co.uk','lalit.prasad@jijgroup.com','aakash.kushwah@jijgroup.com','patrycja.swiatek@onet.eu');*/
$users_array = array('avadhesh.kumar@jijgroup.com','patrycja.swiatek@onet.eu','pappu.k@euracogroup.co.uk','zuzanna.swiatek001@gmail.com');
?>
<style>
.selectpicker {border:1px solid #c6c6c6;}
.selectpicker:hover  {border:1px solid #666666;}
.dvs{border-bottom:1px dashed #666666;width: 100%;clear: both; float:left; padding:5px 0;}
.pmsg{color:#FF0000; font-weight:bold;}

.collapse2{visibility: visible;position: absolute;z-index: +1;width: 99%;background: #fff;border: 1px solid #ddd;}
.checkbox input[type="checkbox"]{margin-left:5px;position: relative;}
.btninfo{width:99%; text-align:left;}  
	
</style> 

<div class="bottom_panel rightside bg-grey-100"> 
	<div class="page-head bg-grey-100">        
		<h1 class="page-title"><?php echo "Staff Orders"; ?></h1>
		<div class="panel-head"><?php print $this->Session->flash(); ?></div>
     </div>
	
    <div class="container-fluid" >
  		<div class="panel"> 
			<div class="panel-body">
			
			<div class="row">
			 <div class="col-lg-12 form-inline">
					<div class="form-group col-lg-5">
 					<label class="col-lg-4" for="staff_person_email" style="padding-left:0px ;padding-right:0px">Select Staff Person:</label>
						<?php 
					$useroptions = ''; 
					foreach($users as $u){
					  $useroptions .= '<option  data-tokens="'.$u['User']['email'].'" value="'.$u['User']['email'].'">'.$u['User']['first_name'].'&nbsp;'.$u['User']['last_name'].'</option>';
					} 
 					?>
					<div class="col-lg-7" style="padding-left:0px"><select class="selectpicker form-control" data-live-search="true" title="Staff Person" name="staff_person_email" id="staff_person_email">
						<option value="">-Select Staff Person-</option>
						<?php echo $useroptions; ?>
					</select></div>
						<!--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
				  </div>
				  <div class="form-group">
						<label for="product_barcode">&nbsp;Scan Product Barcode:</label>
						<input type="text" name="product_barcode" id="product_barcode" class="form-control" onblur="checkProduct();" />
				  </div> 
				</div>		 
  			</div> 
			<div class="row">
				<div class="col-lg-12 ">
				<table class="table table-bordered table-striped dataTable" id="html-div">
				
				 </table>	 
				</div>		 
  			</div>			
			<div class="row">
				<div class="col-lg-12 ">
				
						<table class="table table-bordered table-striped dataTable">
						<tr>
							<th>PickList Name</th> 
							<th>Totals</th>
							<th width="11%">Order Status</th> 
							<th width="20%">Picking User Info</th> 
							<th width="20%">Packing User Info</th> 
							<th>Created By</th>	
							<th>Creation Date</th>
							<th>Action</th>
						</tr>
						<?php 
							foreach($getStaffOrders as $pv =>  $val){
						?>
						<tr>
							<td> </td>
							<td></td>
							<td></td> 
							<td></td>								
							<td></td>	
							<td><?php  echo $val['StaffOrder']['created_by_name'];?></td>
							<td><?php  echo $val['StaffOrder']['created_date'];?></td>
							</tr>
							<?php  
								}
							?> 								
					</table>
						 
					<ul class="pagination">
						  <?php
							   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
							   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
						  ?>
					 </ul>

			</div>
		</div>
			</div>        
		</div>
	</div>        
</div>   
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="<?php echo Router::url('/', true); ?>img/ajax-loader.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>

<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
function checkProduct(){
 	var error = 0; var product_error = 0;
 	if($("#staff_person_email option:selected").val() == ''){	
	 alert('Please select staff_person.');
	 error++;
	} 
	if( ($("#product_barcode").val()).length < 10 ){	
	 alert('Please enter or scan correct barcode.');
	 error++;
	}
	if(error == 0){
	
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>StaffOrders/checkProduct',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  :  function() {/*$('.outerOpac').show()*/},
				data    	: {product_barcode:$("#product_barcode").val()},
				success 	:	function( data  )
				{ 						 					   
					   if(data.product_name == undefined){
						   alert('Product not found!');
  					   }else{
						 $("#html-div" ).html(data.html);
					   }
					   
					  $('.outerOpac').hide();
				}                
			});	
		 
	}
	 
}

function removeItem( barcode){
	 
 	$.ajax({
			url     	: '<?php echo Router::url('/', true); ?>StaffOrders/removeItem',
			dataType	: 'json',
			type    	: 'POST',
			beforeSend  : function() {$('.outerOpac').show()},
			data    	: {barcode:barcode},
			success 	:	function( data  )
			{ 						 					   
				 $("#html-div" ).html(data.html );
 				 $('.outerOpac').hide();
			}                
		});	
		 
} 
function updateQty( barcode){
  	$("#total_"+barcode).text($("#price_"+barcode).text() * $("#quantity_"+barcode+" option:selected").val());		 
}

function createOrder(){
	
	 
 	var error = 0; var product_error = 0;
 	if($("#staff_person_email option:selected").val() == ''){	
	 alert('Please select staff person!');
	 error++;
	} 
	if( ($("#product_barcode").val()).length < 10 ){	
	 alert('Please enter or scan correct barcode!');
	 error++;
	}else{
		checkProduct();
	}
	if( $("#quantity").val() < 	1 ){	
	 alert('Please enter quantity greater than 0!');
	 error++;
	}
	if(error == 0){
	
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>StaffOrders/createOrder',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  :  function() {/*$('.outerOpac').show()*/},
				data    	: {staff_person_email:$("#staff_person_email option:selected").val(),product_barcode:$("#product_barcode").val(),price:$("#price").text(),quantity:$("#quantity").val()},
				success 	:	function( data  )
				{ 						 					   
					   if(data.product_name == undefined){
						   $("#pro").hide();
						   alert('Product not found!');
						   
 					   }else{
					   	 $("#pro").show(); 
					  	 $("#sku").html(data.product_sku); 
						 $("#product").html(data.product_name); 
						 $("#price").html(data.price);
						 
					   }
					   
					 $('.outerOpac').hide();
				}                
			});	
		 
	}
}
 
</script>
 