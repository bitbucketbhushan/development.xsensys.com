<?php
$userID = $this->session->read('Auth.User.id');
$users_array = array('avadhesh.kumar@jijgroup.com','amit.gaur@jijgroup.com','mmarecky82@gmail.com','shashi.b.kumar@jijgroup.com','jake.shaw@euracogroup.co.uk','lalit.prasad@jijgroup.com','aakash.kushwah@jijgroup.com');
?>
<style>
.selectpicker {border:1px solid #c6c6c6;}
.selectpicker:hover  {border:1px solid #666666;}
.completed{background-color:#c3f6c3 !important;}
.preview h3{color:#666;}
.preview th, .preview td{padding:5px; color:#424242;}
</style>
<div class="bottom_panel rightside bg-grey-100">
    <div class="container-fluid" >
       		
             <div class="panel">
			 <div class="panel-body">
				<div class="row">	
					<div class="col-lg-12">
						 						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose Section" name="sections" id="sections"> 
							 <?php   
 									 foreach($sections as $v){	
										 $selected = '';
										 if(isset($this->request->query['sections'])){
											 $exp = explode(",", $this->request->query['sections']);
											 if(in_array($v,$exp)){
												$selected = 'selected="selected"';
											 }
										 }					  
									 	 echo ' <option data-tokens="'.$v.'" value="'.$v.'" '.$selected.'>Section '.$v.'</option>';
								  						  
							  }?>
							 							
							</select>
						</div>	
						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="form-control" title="Choose OrderType" name="sku_type" id="sku_type"  > 
							 <option value="">SKU in Type</option>	
							 <option value="single">Single SKU</option>	
							 <option value="multiple">Multiple SKU</option> 						
							</select>
						</div>	
						<div class="col-md-3 col-lg-3"> 
					 
							<div class="input-group date" data-provide="datepicker">
								<input type="text" name="picklist_before_date" id="picklist_before_date" class="form-control datepicker" placeholder="Picklist Till Date">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-th"></span>
								</div>
							</div>					
						</div>
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="form-control" title="Choose Country" name="postal_country" id="postal_country" onchange="getPoatalServices()" > 
  							 <option value="">Choose Country</option>
							  <?php /*foreach($countries as $v){	
							 		 $selected = '';
									 if(isset($this->request->query['postal_country'])){
										 $exp = explode(",", $this->request->query['postal_country']);
										 if(in_array($v,$exp)){
											$selected = 'selected="selected"';
										 }
									 }
									  									 							  
									 echo ' <option data-tokens="'.$v.'" value="'.$v.'" '.$selected.'>'.$v.'</option>';
								  						  
							  }*/?> 
							</select>
						</div>	
						
						
						
						</div>
						</div>
						
						<div class="row" style="padding-top:5px;">	
						<div class="col-lg-12">
						<div class="col-md-3 col-lg-3"> 
							<select class="form-control" title="Choose Service" name="postal_services" id="postal_services" onchange="getSkuList();"> 
							   		<option value="">Choose Service</option>			
							</select>
						</div>	
						
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose SKU" name="sku" id="sku"> 
							  <option value="">Select SKU</option>	
							  <?php /*foreach($skus as $v){	
									 echo ' <option data-tokens="'.$v.'" value="'.$v.'">'.$v.'</option>';
 							  }*/?>
 
							</select>
						</div>	
						
						 
						
						 <div class="col-md-1"> <button type="button"  class="btn btn-info pick_preview">Picklist Preview</button></div>
						 
						<div class="col-lg-12 preview">&nbsp;</div>  
						 
						</div> 						
 						<div class="col-lg-12"><?php print $this->Session->flash(); ?></div>  
				 </div>
			 
			</div>
			</div>
			
			<div class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12 ">
					
							<table class="table table-bordered table-striped dataTable">
							<tr>
 								<th>PickList Name</th> 
								<!--<th>Pc Name</th>-->
								<th>Total Orders</th>
								<th>Processed Orders</th>	
								<th>Sorted Orders</th>
								<th>Assign User</th>
								<th>Assign Time</th>
								<th>Completed Time</th>
								<th>Time Taken</th>
								<th>Created By</th>	
								<th>Creation Date</th>
								<th>Action</th>
							</tr>
 							<?php
							
								App::Import('Controller', 'DynamicPicklist'); 
								$obj = new DynamicPicklistController;	 
  								$userdata = array();
								$useroptions = '';
								foreach($users as $u){
									$userdata[$u['User']['email']] = $u['User']['first_name'].'&nbsp;'.$u['User']['last_name'];
									$useroptions .= '<option  data-tokens="'.$u['User']['email'].'" value="'.$u['User']['email'].'">'.$u['User']['first_name'].'&nbsp;'.$u['User']['last_name'].'</option>';
										 
								} 
								$user_email = strtolower($this->session->read('Auth.User.email'));
						 		foreach($DynamicPicklist as   $val){
 							?>
							<tr class="<?php if($val['DynamicPicklist']['completed_mark_user'] !=''){ echo 'completed';}?>">
								<td><a href="<?php echo Router::url('/', true); ?>DynamicPicklist/getPicklistOrders/<?php  echo $val['DynamicPicklist']['id'];?>" style="text-decoration:underline;color:#000066;"><?php  echo $val['DynamicPicklist']['picklist_name'];?></a></td>
								<!--<td><?php  //echo $val['DynamicPicklist']['pc_name'];?></td>-->
								<td><?php  echo $val['DynamicPicklist']['order_count'];?></td>
								<td><?php  echo $obj->getProcessedOrders($val['DynamicPicklist']['id']);?></td>
								<td><?php  echo $obj->getSortedOrders($val['DynamicPicklist']['id']);?></td>
								<td>
								
								<?php 
							  	 if(in_array($user_email, $users_array)){?>
								
								<select class="selectpicker form-control" data-live-search="true" title="Assign User" name="assign_user" id="assign_user_<?php  echo $val['DynamicPicklist']['id'];?>" onchange="assignUser(<?php  echo $val['DynamicPicklist']['id'];?>);">
									<option value="">-Assign User-</option>
									<?php echo $useroptions; ?>
									
								</select>
								
								<?php }?>
								<div id="user_<?php  echo $val['DynamicPicklist']['id'];?>"><?php  if($val['DynamicPicklist']['assign_user_email']){echo 'Assign To:<strong>'.$userdata[$val['DynamicPicklist']['assign_user_email']].'</strong>';}?></div>
								</td>
								<td id="time_<?php  echo $val['DynamicPicklist']['id'];?>"><?php  echo $val['DynamicPicklist']['assign_time'];?></td>
								<td id="ctime_<?php  echo $val['DynamicPicklist']['id'];?>"><?php  echo $val['DynamicPicklist']['completed_date'];?></td>
								<td><?php  
								if(strtotime($val['DynamicPicklist']['completed_date']) > 0){
								
									$interval = (strtotime($val['DynamicPicklist']['completed_date'])) - (strtotime($val['DynamicPicklist']['assign_time']) );
									$days = floor($interval / 86400); // seconds in one day
									$interval = $interval % 86400;
									$hours = floor($interval / 3600);
									$interval = $interval % 3600;
									$minutes = floor($interval / 60);
									$interval = $interval % 60;
									$seconds = $interval;
									echo  $days .' days '.$hours  .' hours '. $minutes  .' minutes ' ;
								}else{
									echo '-';
								}
								
								?></td>
 								
								<td><?php  echo $val['DynamicPicklist']['created_username'];?></td>
								<td><?php  echo $val['DynamicPicklist']['created_date'];?></td>
								<td>
								<?php 
							  	 if(in_array($user_email,$users_array)){?>
								 
								 <?php if($val['DynamicPicklist']['completed_mark_user'] == ''){ ?>
								<button type="button" class="btn btn-warning btn-xs" onclick="markCompleted('<?php echo $val['DynamicPicklist']['id'];?>')">Mark Completed</button><br />
								<button type="button" class="btn bg-red-500 btn-xs" onclick="generateLnS('<?php echo $val['DynamicPicklist']['id'];?>')">Print Label</button><br />
								<button type="button" class="btn btn-info btn-xs" onclick="markProcessed('<?php echo $val['DynamicPicklist']['id'];?>')">Process</button><br />
								<button type="button" class="btn bg-yellow-500 btn-xs" onclick="markSorted('<?php echo $val['DynamicPicklist']['id'];?>')">Sort</button><br />
								<?php }else{ ?>
								<span style="color:#FF0000"><strong>Completed</strong></span>
								<?php }?>
								
								<?php
								}?>
								<a href="<?php echo Router::url('/', true). '/img/printPickList/'.$val['DynamicPicklist']['picklist_name']; ?>" target="_blank" style="text-decoration:underline;">Download</a>
								
								</td>
 								</tr>
								<?php  
									}
								?> 								
						</table>
						
							 
						<ul class="pagination">
							  <?php
  								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>

				</div>
			</div>
			</div>        
		</div>
		
	</div>        
</div>   
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>

<script type="text/javascript" src="<?php echo Router::url('/', true); ?>/js/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Router::url('/', true); ?>/js/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
 <script>
 
  $(function() {
  $('input[name="picklist_before_date"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
	format: 'YYYY-MM-DD',
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
     getDeleveryCountry();
  });
});

 function generatePickList( picklist_id ){
	 
		 
		if(confirm('Are you sure want to generate PickList!')){            
		  
 		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/generatePicklist',
				dataType	: 'json',
				type    	: 'POST',
				data    	: {  picklist_name:picklist_id},
				success 	:	function( data  )
				{			
 					            
				}                
			});	
		}
	 
 }
 
function markCompleted( picklist_id ){
	if(confirm('Are you sure want to Mark it Completed!')){            
		  
 		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/markCompleted',
				dataType	: 'json',
				type    	: 'POST',
				data    	: {  picklist_inc_id:picklist_id},
				success 	:	function( data  )
				{			
 					            
				}                
			});	
		}
		
}




function getSkuList(){
 	
	var  error = 0;
	var section = [];
	$.each($("#sections option:selected"), function(){            
		section.push($(this).val());
	});
	
	if(section.length < 1){	
	 alert('Please select section.');
	 error++;
	}
	
	if($("#sku_type option:selected").val() == ''){	
	 alert('Please select sku type.');
	 error++;
	}  
	
	if(error == 0){
	
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/getSkuList',
				dataType	: 'json',
				type    	: 'POST',
				data    	: {sections:section, sku_type:$("#sku_type option:selected").val(),till_date:$("#picklist_before_date").val(),postal_country:$("#postal_country option:selected").val(),postal_service:$("#postal_services option:selected").val() },
				success 	:	function( data  )
				{			 
										  
					  var select = $('#sku');
						select.empty();
						select.append("<option value=''>---Select sku---</option>");
					
						for (var j = 0; j < data.length; j++){ 
							console.log(data[j]);
							$("#sku").append("<option data-tokens='"+data[j]+"' value='" +data[j]+ "'>" +data[j]+ "</option>");
						}
					  $('.selectpicker').selectpicker('refresh');
				}                
			});	
	}
}


function getDeleveryCountry(){
 	
	var error = 0;
	var section = [];
	$.each($("#sections option:selected"), function(){            
		section.push($(this).val());
	});
	
	if(section.length < 1){	
	 alert('Please select section.');
	 error++;
	}
	
	if($("#sku_type option:selected").val() == ''){	
	 alert('Please select sku type.');
	 error++;
	}  
	
	if(error == 0){
	
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/getDeleveryCountry',
				dataType	: 'json',
				type    	: 'POST',
				data    	: {sections:section, sku_type:$("#sku_type option:selected").val(),till_date:$("#picklist_before_date").val() },
				success 	:	function( data  )
				{			
										  
					  var select = $('#postal_country');
						select.empty();
						select.append("<option value=''>---Select Country---</option>");
					
						for (var j = 0; j < data.length; j++){
							console.log(data[j]);
							$("#postal_country").append("<option data-tokens='"+data[j]+"' value='" +data[j]+ "'>" +data[j]+ "</option>");
						}
						
					 
				}                
			});	
	}
}

function getPoatalServices(){

	var postal_country = [];
	$.each($("#postal_country option:selected"), function(){            
		postal_country.push($(this).val());
	});
	var section = [];
	$.each($("#sections option:selected"), function(){            
		section.push($(this).val());
	});
	
  	$.ajax({
			url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/getPoatalServices',
			dataType	: 'json',
			type    	: 'POST',
			data    	: {  sections:section, postal_countries:postal_country },
 			success 	:	function( data  )
			{			
				    				  
				  var select = $('#postal_services');
					select.empty();
					select.append("<option value=''>---Select Postal Services---</option>");
				
					for (var j = 0; j < data.length; j++){
						console.log(data[j]);
						$("#postal_services").append("<option data-tokens='"+data[j]+"' value='" +data[j]+ "'>" +data[j]+ "</option>");
					}
  				 getSkuList();
 			}                
		});	
}

function changeSections(){

var select = $('#postal_services');
					select.empty();
					select.append("<option value=''>---Select Postal Services---</option>");
} ;



$(".pick_preview").click(function(){
	
	    var error = 0;
		var section = [];
		$.each($("#sections option:selected"), function(){            
			section.push($(this).val());
		});			
		/*var postal_country = [];
		$.each($("#postal_country option:selected"), function(){            
			postal_country.push($(this).val());
		});*/
		var skus = [];
		$.each($("#sku option:selected"), function(){            
			skus.push($(this).val());
		});
		
		
		if(section.length < 1){	
		 alert('Please select section.');
		 error++;
		}
		if($("#sku_type option:selected").val() == ''){	
		 alert('Please select sku type.');
		 error++;
		}
		if($("#postal_country option:selected").val() == ''){	
		 alert('Please select country.');
		 error++;
		}
		if($("#postal_services option:selected").val() == ''){	
		 alert('Please select postal service.');
		 error++;
		}
		if($("#picklist_before_date").val() == ''){
		  alert('Please select date.');
		  error++;
		}	
		if(error == 0){ 
		
		if(confirm('Pick list will be generated of all orders till '+$("#picklist_before_date").val()+' date.')){
			$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/createPicklist',
					dataType	: 'json',
					type    	: 'POST',
					data    	: {sections:section, sku_type:$("#sku_type option:selected").val(),till_date:$("#picklist_before_date").val(),postal_country:$("#postal_country option:selected").val(),postal_services:$("#postal_services option:selected").val(),skus:skus},
					success 	:	function( data  )
					{			
						     $(".preview").html(data.preview ) ;     
					}                
				});	
			}
  	   } 
   
	
});

 function assignUser(picklist_id){

 	if($("#assign_user_"+picklist_id+" option:selected").val()==''){            
		  alert('Please select user');
	}else{
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/assignUser',
				dataType	: 'json',
				type    	: 'POST',
				data    	: {  picklist_inc_id:picklist_id, assign_user:$("#assign_user_"+picklist_id+" option:selected").val()},
				success 	:	function( data  )
				{			
 					$("#user_"+picklist_id).html('Assign To:<strong>'+data.user+'</strong>');    
					$("#time_"+picklist_id).html('<?php echo date('Y-m-d H:i:s')?>');            
				}                
			});	
		}
}


function generateLnS( picklist_id ){
	if(confirm('Are you sure want to generate label and Slip!')){            
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>Dp/batchPicklistSlipLabel',
				dataType	: 'json',
				type    	: 'POST',
				data    	: {  picklist_inc_id:picklist_id},
				success 	:	function( data  )
				{			
 					            
				}                
			});	
		}
}

function markProcessed( picklist_id ){
	if(confirm('Are you sure want to process orders!')){            
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>Dp/batckPicklistBulkOrderProcess',
				dataType	: 'json',
				type    	: 'POST',
				data    	: {  picklist_inc_id:picklist_id},
				success 	:	function( data  )
				{			
 					            
				}                
			});	
		}
}

function markSorted( picklist_id ){
	if(confirm('Are you sure want to sort all orders!')){            
		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>Dp/sortBatchAllOrders',
				dataType	: 'json',
				type    	: 'POST',
				data    	: {  picklist_inc_id:picklist_id},
				success 	:	function( data  )
				{			
 					            
				}                
			});	
		}
}
 



 
</script>