<div class="rightside bg-grey-100">
   <!-- BEGIN PAGE HEADING -->
   <div class="page-head bg-grey-100">
      <h1 class="page-title">Customer Address</h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
   </div>
   <!-- END PAGE HEADING -->
   <div class="container-fluid">
      <div class="panel padding-top-0">
			<div class="panel-body ">
				<div class="row padding-bottom-10">
					                                       
					   <div class="col-lg-5">                                            
						  <?php
							 print $this->form->input( 'name', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
							 ?>  
					   </div>
					    <div class="col-lg-5">  
					   <?php
						echo $this->Form->button('Edit Product', array(
							'type' => 'submit',
							'escape' => true,
							'class'=>'add_brand btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
							 ));	
					?>
					  </div>
				</div>			
				<div class="row">
					<div class="col-lg-8">
							<?php							
							  print $this->form->create( 'customer', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/products/save_editproduct', 'type'=>'post','id'=>'savebarcode' ) );
							  print $this->form->input( 'customer.id', array( 'type'=>'hidden') );
							?>
						   
							<div class="form-group">
							   <label for="username" class="control-label col-lg-3">Name</label>                                        
							   <div class="col-lg-9">                                            
								  <?php
									 print $this->form->input( 'name', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
									 ?>  
							   </div>
							</div>
							<div class="form-group">
							   <label for="username" class="control-label col-lg-3">City	</label>                                        
							   <div class="col-lg-9">
								  <?php
									 print $this->form->input( 'short_description', array( 'type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control bs-texteditor', 'required'=>false ) );
									 ?>  
							   </div>
							</div>
							<div class="form-group">
							   <label for="username" class="control-label col-lg-3">Long Description	</label>                                        
							   <div class="col-lg-9">
								  <?php
									 print $this->form->input( 'ProductDesc.long_description', array( 'type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control bs-texteditor', 'required'=>false ) );
									 ?>  
							   </div>
							</div>
							<div class="form-group">
							   <label for="username" class="control-label col-lg-3">SKU</label>                                        
							   <div class="col-lg-9">                                            
								  <?php
									 print $this->form->input( 'Product.product_sku', array( 'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'required'=>false ) );
									 ?>  
							   </div>
							</div>
					 
						
							
							<div class="form-group" >
								<label for="username" class="control-label col-lg-3">Store</label>                                        
								<div class="col-lg-9">                                            
								   <?php
										if( count( $storeList ) > 0 )
										print $this->form->input( 'Product.store_name', array( 'type'=>'select', 'multiple' => 'multiple', 'empty'=>'Choose Country','options'=>$storeList,'class'=>'form-control', 'div'=>false, 'label'=>false, 'required'=>false, 'style' => 'height:150px;') );
									?>
								</div>
							</div>
							
							
							<div class="form-group" >
								<div class="col-lg-3">&nbsp;</div>
								<div class="col-lg-9"> 
								<button class="btn bg-orange-500 color-white btn-dark margin-right-10 padding-left-40 padding-right-40" type="submit" formaction="/showAllProduct" >Go Back</button>
					<?php
						echo $this->Form->button('Edit Product', array(
							'type' => 'submit',
							'escape' => true,
							'class'=>'add_brand btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
							 ));	
					?>
								</div>
							
							</div>
							
							
						  
					  
					
						
							</form> 
							</div>
					 </div>
			</div>
         </div>
      </div>
  </div>
 
 
<script type="text/javascript">
    
</script>
 
 
