<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php //print $role;?></h1>
        <div class="panel-title no-radius bg-green-500 color-white no-border"></div>
        	<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		</div>
		<div class="container-fluid">
				<div class="row">
				<?php
					print $this->form->create( 'ModulePermission', array( 'class'=>'form-horizontal', 'url' => '/Modules/addModulePermission', 'type'=>'post','id'=>'deleverymatrix' ) );
					print $this->form->input( 'ModulePermission.id', array( 'type'=>'hidden') );
				?>
            <div class="col-lg-12 warehouseDetails">
              <div class="panel">
						<div class="panel-title">
							<div class="panel-head">Edit Postal Provider</div>
						</div>
					<div class="panel-body">
                    <div class="row">
								<?php //pr( $this->request->data ); exit; ?>
									  <div class="form-group" >
										<label for="username" class="control-label col-lg-3">Module Name</label>                                        
										<div class="col-lg-7">                                            
										   <?php
												print $this->form->input( 'ModulePermission.module_name', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
										   ?> 
										</div>
									  </div>
									  
									   <div class="form-group" >
										<label for="username" class="control-label col-lg-3">Permission</label>                                        
										<div class="col-lg-7">                                            
										   <?php
										    if( count( $getUsers ) > 0 )
											print $this->form->input( 'ModulePermission.user_id', array( 'type'=>'select', 'multiple' => 'multiple', 'empty'=>'Choose User','options'=>$getUsers,'class'=>'form-control', 'div'=>false, 'label'=>false, 'required'=>false, 'style' => 'height:150px;') );
											
											?>
										</div>
								  </div>
							 
						</div>
						
						<div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100" style="clear:both">                                                                            
                            	<a class="btn bg-orange-500 color-white btn-dark margin-right-10 padding-left-40 padding-right-40" href="/Modules/showAllModule">Go Back</a>
							   	<?php
									echo $this->Form->button( 'Edit Permission' , array(
										'type' => 'submit',
										'escape' => true,
										'class'=>'add_attribute btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
										));	
								?>
						</div>
					</div>
				</div>	
			</form>
	    </div>
	</div>        
</div>
	


