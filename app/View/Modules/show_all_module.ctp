<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"><?php //print $role;?></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
    </div>
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">									
									<div class="panel-tools">																	
									</div>
								</div>
							    <div class="panel-body no-padding-top bg-white">											
											<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
													<thead>
											<tr role="row">
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Id: activate to sort column descending">S.No.</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Warehouse Name: activate to sort column descending">Provider Name</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="County Name: activate to sort column ascending">Action</th>
										</thead>
									<tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <?php $i = 1; foreach($getAllModules as $getAllModule) { ?>
                                            <tr class="odd">
												<td class="sorting_1"><?php print $i; ?></td>
												<td class="sorting_1"><?php print $getAllModule['ModulePermission']['module_name']; ?></td>                                                
											    <td class="sorting_1">
													<ul id="icons" class="iconButtons">
														<li>
															<a href="/Modules/editPermission/<?php print $getAllModule['ModulePermission']['id']; ?>" class="btn btn-success btn-xs margin-right-10" title="Edit Module" ><i class="fa fa-pencil"></i></a>
														</li>
													</ul>
                                                </td>
                                            </tr>
                                            <?php $i++; } ?>
                                    </tbody>
									</table>		
								</div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
				<!-- BEGIN FOOTER -->
				<?php echo $this->element('footer'); ?>
				<!-- END FOOTER -->
            </div>
    </div>
</div>




