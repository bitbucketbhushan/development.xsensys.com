<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.selected{ background-color: #66FFCC;}
	.col-5{ width:5%; float:left;}
	.col-6{ width:6%; float:left;}
	.col-7{ width:7%; float:left;}
	.col-8{ width:8%; float:left;}
	.col-9{ width:9%; float:left;}
	.col-10{ width:10%; float:left;}
	.col-11{ width:11%; float:left;}
	.col-12{ width:12%; float:left;}
	.col-14{ width:14%; float:left;}
	.col-15{ width:15%; float:left;}
	.col-16{ width:16%; float:left;}	
	.col-20{ width:20%; float:left;}
	.col-25{ width:25%; float:left;}
	.col-28{ width:28%; float:left;}
	.col-30{ width:30%; float:left;}
 </style>
 
 <div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php echo "FBA Export Console"; ?></h1>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
			<?php print $this->Session->flash(); ?>
              <div class="panel"> 
						<div class="panel-title">
							<div class="panel-head">Barcode Scan Section</div>
							<div class="panel-tools">
										<a href="<?php echo Router::url('/', true) ?>FbaShipments"   class="btn bg-orange-500 color-white btn-dark">Go Back</a>
									</div>
							
						</div>
					<div class="panel-body">

					<div class="row">

                        <div class="col-lg-4 col-lg-offset-1">
								<div class="input text"><label for="barcode">Barcode</label> <input type="text" id="barcode" index="0" value="" class="get_sku_string form-control" name="barcode">
								<p class="help-block">Please scan an item to start processing order</p>
								</div>
								
							</div>
							
							<div class="col-lg-4 col-lg-offset-1"><img alt="" title="Scanner" src="/img/bar-code.png"></div>
					</div>
					
					<label><span>Shipment containing barcode : <span><span class="searchbarcode"></span></label>
					<div class="row">						 
						<label class="col-lg-12">Remaning Quantity: <strong style="color:red;" class="remaning">----</strong></label>  
                    </div>
					<div class="outer_grid">
					
					</div>	
				</div>
			</div>
			
		</div>  

</div>						
                          
	</div>
<script>
     // document.getElementById("barcode").focus();
 	 // $("#barcode").focus();
$(function() {
  $("#barcode").focus();
});
 

$(document).keypress(function(e) {
    if(e.which == 13) {
		var id = $('.select').attr('id');
        if(typeof(id) == 'undefined'){
			 submitSanningValue();
		}else{
		   
		   var action = 'confirmBarcode/'+id;	
		    window.open('<?php echo Router::url('/', true) ?>FbaExportConsole/'+action,'_self');		
		}
		
		
    }
});
 
 
function submitSanningValue()
{
	  var Barcode = $('#barcode').val();
		$('.searchbarcode').text(Barcode); 
	 
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaExportConsole/getSearchList',
		type: "POST",				
		cache: false,			
		data : { barcode : Barcode },
		success: function(result, textStatus, jqXHR)
		{	
			 
			$('#barcode').val('');  
			 $('.remaning').text(result.rem_qty); 	 
			$('.outer_grid').html(result.data); 	
			if(result.error){
				alert(result.msg);
			} 							
		}		
 	});   				
}

</script>
