<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.selected{ background-color: #66FFCC;}
	.btn-disable{ cursor: not-allowed;opacity : .5;}
	.col-5{ width:5%; float:left;}
	.col-6{ width:6%; float:left;}
	.col-7{ width:7%; float:left;}
	.col-8{ width:8%; float:left;}
	.col-9{ width:9%; float:left;}
	.col-10{ width:10%; float:left;}
	.col-11{ width:11%; float:left;}
	.col-12{ width:12%; float:left;}
	.col-14{ width:14%; float:left;}
	.col-15{ width:15%; float:left;}
	.col-16{ width:16%; float:left;}	
	.col-20{ width:20%; float:left;}
	.col-25{ width:25%; float:left;}
	.col-28{ width:28%; float:left;}
	.col-30{ width:30%; float:left;}
 </style>
 
 <div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php echo "FBA Export Console"; ?></h1>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
			<?php print $this->Session->flash(); ?>
              <div class="panel"> 
						<div class="panel-title">
							<div class="panel-head">Barcode Scan Section</div>
							<div class="panel-tools">
								<a href="<?php echo Router::url('/', true) ?>FbaExportConsole" class="btn btn-warning btn-xs color-white">Skip</a>
								
								<button type="button" id="print_label" for="0" data-print="0" class="btn btn-warning btn-xs btn-disable"><i class="fa fa-print"></i>&nbsp;Print Label</button>
								<button type="button" id="print_slip" for="0" data-print="0" class="btn btn-warning btn-xs btn-disable"><i class="fa fa-print"></i>&nbsp;Print Slip</button>
								<?php /*?><button type="button" id="process" for="" class="btn btn-success btn-xs btn-disable">Process</button> <?php */?>
								<button type="button" id="boxing" for="" class="btn btn-success btn-xs btn-disable">Boxing</button> 								
								
							</div>
							
						</div>
					<div class="panel-body">

					<div class="row">

                        <div class="col-lg-4 col-lg-offset-1">
								<div class="input text"><label for="barcode">Barcode</label> <input type="text" id="barcode" index="0" value="" class="get_sku_string form-control" name="barcode">
								<p class="help-block">Please scan an item to start processing order</p>
								</div>
								
							</div>
							
							<div class="col-lg-4 col-lg-offset-1"><img alt="" title="Scanner" src="/img/bar-code.png"></div>
					</div>
					
					<label><span>Shipment containing barcode : </span><span class="searchbarcode"></span></label>
					<div class="row">						 
						<label class="col-lg-12">Remaning Quantity: <strong style="color:red;" class="remaning">----</strong></label>  
                    </div>
					<div class="outer_grid">
						<div class="row head" style="clear:both;">
							  <div class="col-sm-12">
								<div class="col-12">SellerSku</div>		
								<div class="col-15">MasterSku</div>	
								<div class="col-10">Barcode</div>			
								<div class="col-10">FNSKU</div>																				
								<div class="col-30">Title</div>	
								<div class="col-5"><center>ScanQty</center></div>	
								<div class="col-8"><center>Quantity</center></div>
								<div class="col-10">-</div>	
							</div>								
						</div>	
		
						<div class="body" id="results">
							<div id="r3">
							<?php  if(count($all_items) > 0) {	 pr($all_items);exit; ?>
							
								<?php $items = $all_items['FbaItemLocation'];   ?>
									<div class="row sku " >
									<?php $cls ='';
									 if($items['slip_print'] > 0 && $items['label_print'] > 0){ $cls = ' labelslipprint';}
										else if($items['slip_print'] > 0) $cls =  ' slipprint';
										else if($items['label_print'] > 0) $cls =  ' labelprint';?>
										
										<div class="col-lg-12 <?php echo $cls ?>" id="r_<?php echo $items['id']; ?>" for="<?php echo $cls ?>">	
											<?php  
												$master     = $obj->getSkuMapping($items['merchant_sku']);
												$master_sku = $master['sku'];
												$barcode 	= $obj->getSkuBarcode($master_sku);		
												$packaging  = $obj->getFbaPackaging($items['merchant_sku']);											
											?>
											<div class="col-sm-2"><?php echo $items['merchant_sku']; ?>	
											
												<div>												
												
													<?php foreach($envelope as $id => $pack){
															if($packaging != '' && $packaging == $pack){
																$sel = '';
															}else{
																$sel = 'selected="selected"';
															}
														}
														?>	
														
													<select name="packaging[<?php echo $items['merchant_sku'] ?>]" style="font-size:12px; padding:5px; border:1px solid #c6c6c6; width:100%;" onchange="updatePackaging(this,'<?php echo $items['merchant_sku'] ?>')">
													 
														<option value="" <?php echo $sel?>>--No Packaging--</option>
														<?php foreach($envelope as $id => $pack){
															if($packaging != '' && $packaging == $pack){
																echo '<option value="'.$pack.'" selected="selected">'.$pack.'</option>';
															}else{
																echo '<option value="'.$pack.'">'.$pack.'</option>';
															}
														}
														?>		
														<option value="add_new" style="font-weight:bold; color:#FF0000;">Add New Packaging</option>												 
													</select>
												</div>
												<div id="p_<?php echo $items['merchant_sku'] ?>" style="display:none;"><input type="text" id="pin_<?php echo $items['merchant_sku'] ?>"/>&nbsp;<button type="button" class="btn btn-success btn-xs" title="Save" onclick="addPackaging('<?php echo $items['merchant_sku'] ?>')"><i class="fa  fa-check-square"></i></button></div>
											</div>
											<div class="col-sm-2"><?php echo $master_sku.'<br>'.$barcode;  ?></div>
											
											<div class="col-sm-2">
												<div><a href="https://www.amazon.com/dp/<?php echo $items['asin']; ?>" target="_blank" style="color:#333399;"><?php echo $items['asin']; ?></a></div>
												<div style="border-top:1px dashed #bbb; width:55%;"><?php echo $items['fnsku'] ?></div>
											</div>	
											<div class="col-sm-3" style="font-size:13px;"><?php echo (strlen($items['title']) > 70) ? substr($items['title'],0,70).'...' : $items['title']; 	?> </div>	
											
											
																						
											<div class="col-sm-1"> 
											 <input type="hidden" name="poitems[<?php echo $items['fnsku']?>][merchant_sku]" id="merchant_sku_<?php echo $items['id']; ?>" value="<?php echo $items['merchant_sku'] ?>" />
											 <input type="hidden" name="poitems[<?php echo $items['fnsku']?>][master_sku]" id="master_sku_<?php echo $items['id'];?>"  value="<?php echo $master_sku ?>" />
											 <input type="hidden" name="poitems[<?php echo $items['fnsku']?>][prep_qty]" id="prep_qty_<?php echo $items['id']; ?>"   value="<?php echo $items['shipped'] ?>" />
											 <input type="hidden" name="poitems[<?php echo $items['fnsku']?>][items_inc_id]" id="items_inc_id_<?php echo $items['id']; ?>" value="<?php echo $items['id'] ?>" />
											 <input type="hidden" name="poitems[<?php echo $items['fnsku']?>][fnsku]" id="fnsku_<?php echo $items['id']; ?>" value="<?php echo $items['fnsku'] ?>" />
											 <input type="hidden" name="poitems[<?php echo $items['fnsku']?>][barcode]" id="barcode_<?php echo $items['id']; ?>" value="<?php echo $master['barcode']; ?>" />
											
											<?php 								
											 
												echo "1";
												$shipped_qty = '';
												if(isset($po_items[$items['fnsku'].'_'.$master_sku])){
													$shipped_qty = $po_items[$items['fnsku'].'_'.$master_sku]['shipped_qty'];
													$inc_id = $po_items[$items['fnsku'].'_'.$master_sku]['id'];
													echo '<input type="hidden" name="'.$items['fnsku'].'[shipments_items_po_inc_id]" value="'.$inc_id.'" />';
												} 
											  
											?>
											</div>
											
											<div class="col-sm-2">
											<div style="text-align:left; float:left;" id="l_<?php echo $items['id']; ?>">
											<?php 
											if(isset($po_items[$items['fnsku'].'_'.$master_sku])){
												echo   $po_items[$items['fnsku'].'_'.$master_sku]['prep_qty'];
											}else{
												echo $qt = $items['shipped'];
											} 
											if($shipped_qty == ''){
												$fqty = $qt;
											}else{
												$fqty = $shipped_qty;
											}
											?>
											</div>
											<div style="float:right">
											
												<div style="text-align:left; float:left;"><input type="number" name="poitems[<?php echo $items['fnsku']?>][qty]" id="input_<?php echo $items['id']?>" value="<?php echo $fqty?>" style="width:55px;"onchange="enterNumber('<?php echo $items['id'];?>')"/>
												<br /><center>
												<a href="javascript:void(0);" title="Update Quantity" class="btn btn-info btn-xs" id="upd_<?php echo $items['id']; ?>" onclick="updateQuantity('<?php echo $items['id']?>')"><small>Update Qty</small></a></center>
												</div>
												<div style="text-align:right; float:right;"><a href="javascript:void(0);" title="Print Label" class="btn btn-success btn-xs print_label" for="<?php echo $items['id']; ?>" id="label_<?php echo $items['id']; ?>" ><i class="fa fa-print print_label" for="<?php echo $items['id']; ?>"  onclick="printLabel(<?php echo $items['id']; ?>)"  ></i></a>
												
												&nbsp; <a href="javascript:void(0);" title="Print Slip" class="btn btn-warning btn-xs print_slip" for="<?php echo $items['id']; ?>" id="slip_<?php echo $items['id']; ?>"><i class="fa fa-print print_slip" for="<?php echo $items['id']; ?>"  onclick="printSlip(<?php echo $items['id']; ?>)" ></i></a></div>
											</div>
											</div>
											
										 
											
										</div>		
										
										 <?php 
											
											if($master_sku){
												if(isset($bundle_mapping[$master_sku])){
												
												?>
												<div class="col-sm-2">&nbsp;&nbsp;&nbsp;&nbsp;</div>
												
												<div class="col-sm-10"><div style=" margin-bottom:5px; background-color:#BAEEF9; font-size:11px; padding-left:0px !important;padding-right:0px !important;">
												<?php  
													 
												foreach($bundle_mapping[$master_sku] as $sku => $val){
													$barcode = $obj->getSkuBarcode($sku);												
												
													$shipped_qty = '';
													if(isset($po_items[$items['fnsku'].'_'.$sku])){
														$shipped_qty = $po_items[$items['fnsku'].'_'.$sku]['shipped_qty'];
														$inc_id = $po_items[$items['fnsku'].'_'.$sku]['id'];
														echo '<input type="hidden" name="poitems['.$items['fnsku'].'][shipments_items_po_inc_id]" value="'.$inc_id.'" />';
													}
													
													$id = $items['id'].'_'.$val['bid'];
													$name = $items['fnsku'].'[qty]['.$val['bid'].']';
													echo '<div class="col-sm-12" style="background-color:#BAEEF9; margin-left:0px !important;margin-right:0px !important; font-size:11px;border-top:1px dashed #bbb;">'; 
													echo '<div class="col-sm-2" style="font-size:11px;">'.$sku.'<br>'.$barcode.'</div>'; 												
													echo '<div class="col-sm-6" style="text-align:left;font-size:11px; padding-left:32px">'.$val['title'].'</div>';
													echo '<div class="col-sm-1 bundle_'.$items['id'].'" for="'.$id .'" style="text-align:center; font-size:11px;">'.$val['qty'].'</div>';
													echo '<div class="col-sm-1" style="text-align:right; padding-right:15px; font-size:11px;" id="l_'.$id.'">' . $val['qty'] * $items['shipped'] .'</div>';
													echo '<div class="col-sm-2" style="text-align:right;padding-right:68px;"><span name="'.$name.'" id="span_'.$id.'">-</span></div>';
													echo  '</div>';
													} 
												?></div>  
												</div>
												<?php
												  }
											//}
											?>	
									</div>
									 
								<?php }?>
							
								<div class="row" style="padding-top:10px;">
										<div class="col-lg-12">	
											<div class="col-sm-10">&nbsp;&nbsp;	</div>											
											<div class="col-sm-2 text-right">											
											<?php
											echo $this->Form->button('Save Values', array(
												'type' => 'submit',
												'id' => 'save_values',
												'name' => 'save_values',
												'value' => 'save_values',
												'escape' => true,
												'class'=>'btn btn-info btn-sm'
												 ));	
										?>
											
											 
											<?php if(count($shipment_po) > 0){	
											
											
											echo'<br /><br />';
											echo'<a href="'.Router::url('/', true) .'FbaShipments/ExportInvoice/'.$this->params['pass'][0].'" class="btn btn-primary btn-sm" style="float:right;">Export Invoice</a>';
											 
											 
											 echo'<br /><br />
											 <a href="'.Router::url('/', true).'FbaBoxing/index/'.$this->params['pass'][0].'" class="btn btn-warning btn-sm" style="float:right;">Next >></a>';
											 }?>
											 
											 
											 
											  </div>
										</div>
								</div>
							</div>	
							
						<div class="row sku"><div class="col-lg-12"></div></div>	
						
						</form>							
							
						<div class="row"><div class="col-sm-8"></div><div class="col-sm-4" id="msg"></div></div>	
							
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
							<?php }  ?>
						</div>
					</div>	
				</div>
			</div>
			
		</div>  

</div>						
                          
	</div>
<script>

$(function() {
  $("#barcode").focus();
});
 
$("#print_slip").click(function(){		
	if( $(this).attr('for') > 0){ 
		$("#print_slip i").removeClass('fa-print');	
		$("#print_slip i").addClass('fa-spin fa-refresh');	
		var v = $(this).attr('for');
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaExportConsole/printSlip',
			type: "POST",				
			cache: false,			
			data : {id:v},
			success: function(data, textStatus, jqXHR)
			{	
				$("#print_slip i").removeClass('fa-spin fa-refresh');	
				$("#print_slip i").addClass('fa-print');	
				$('#print_slip').attr('data-print',v);
				
				console.log(data.msg);		
				if(data.error){
					alert(data.msg);
				}else{
							
					if($('#print_label').attr('data-print') > 0){
						 $('#boxing').removeClass('btn-disable'); 	
						 $('#boxing').attr('for',v);
					}	
				}
											
			}		
		});  	
	}	 
});


$("#print_label").click(function(){	
	 
	if( $(this).attr('for') > 0){
			
			var v = $(this).attr('for');
		 
			$("#print_label i").removeClass('fa-print');	
			$("#print_label i").addClass('fa-spin fa-refresh');	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaExportConsole/printLabel',
				type: "POST",				
				cache: false,			
				data : {id:v},
				success: function(data, textStatus, jqXHR)
				{	
					$("#print_label i").removeClass('fa-spin fa-refresh');	
					$("#print_label i").addClass('fa-print');	
					$('#print_label').attr('data-print',v);
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					}else{
					 	
						if($('#print_slip').attr('data-print') > 0){
							 $('#boxing').removeClass('btn-disable'); 	
							 $('#boxing').attr('for',v);
						 }	
					}
						 						
				} 
			});
		} 	  
});

$("#boxing").click(function(){	
	 
	if( $(this).attr('for') > 0){
			
			var v = $(this).attr('for');
		    window.location.href = "<?php echo Router::url('/', true) ?>FbaExportConsole/completeBoxing/"+v;
 			
		}	
	  
});

$("#process").click(function(){	
	 
	if( $(this).attr('for') > 0){
			
			var v = $(this).attr('for');
		 
 			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaExportConsole/completeBoxing',
				type: "POST",				
				cache: false,			
				data : {id:v},
				success: function(data, textStatus, jqXHR)
				{	
				 
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					}else{
						window.location.href = "<?php echo Router::url('/', true) ?>FbaExportConsole";
					}
						 						
				} 
			});
		}	
	  
});

$(document).keypress(function(e) {
    if(e.which == 13) {
		completeBarcode(); 
    }
});
 
 

function completeBarcode()
{
	var Barcode = $('#barcode').val();
	$('.searchbarcode').text(Barcode); 
 
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaExportConsole/completeBarcode',
		type: "POST",				
		cache: false,			
		data : {  barcode : Barcode,  id : '<?php echo $this->params['pass'][0]; ?>'},
		success: function(result, textStatus, jqXHR)
		{	
		  	$('#barcode').val('');  
			if(result.status == 1){
				 $('#process_qty').text(result.qty); 	
			}else if(result.status == 2){
				 $('#process_qty').text(result.qty); 	
				 $('#print_label').removeClass('btn-disable'); 	
				 $('#print_label').attr('for','<?php echo $this->params['pass'][0]; ?>');
				 $('#print_slip').removeClass('btn-disable'); 
				 $('#print_slip').attr('for','<?php echo $this->params['pass'][0]; ?>');	
				
			}else if(result.status == 0){
				alert(result.userdetail);
			} 
			//$('.outer_grid').html(result.data); 	
			if(result.error){
				alert(result.msg);
			} 							
		}		
 	});   				
}

</script>
