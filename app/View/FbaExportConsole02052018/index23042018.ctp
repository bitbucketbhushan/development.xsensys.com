<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.selected{ background-color: #FFFF99;}
	.col-1{ width:1%; float:left;} .col-38{ width:38%; float:left;} .col-40{ width:40%; float:left;}
	.col-2{ width:2%; float:left;}
	.col-3{ width:3%; float:left;}
	.col-4{ width:4%; float:left;}
	.col-5{ width:5%; float:left;}
	.col-6{ width:6%; float:left;}
	.col-7{ width:7%; float:left;}
	.col-8{ width:8%; float:left;}
	.col-9{ width:9%; float:left;}
	.col-10{ width:10%; float:left;}
	.col-11{ width:11%; float:left;}
	.col-12{ width:12%; float:left;}
	.col-14{ width:14%; float:left;}
	.col-15{ width:15%; float:left;}
	.col-16{ width:16%; float:left;}
	.col-17{ width:17%; float:left;}
	.col-18{ width:18%; float:left;}
	.col-19{ width:19%; float:left;}	
	.col-20{ width:20%; float:left;}
	.col-25{ width:25%; float:left;}
	.col-28{ width:28%; float:left;}
	.col-30{ width:30%; float:left;}
	.col-32{ width:32%; float:left;}
	.col-34{ width:34%; float:left;}
	.col-36{ width:36%; float:left;}
 </style>
 
 <div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php echo "FBA Export Console"; ?></h1>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
			<?php print $this->Session->flash(); ?>
              <div class="panel"> 
						<div class="panel-title">
							<div class="panel-head">Barcode Scan Section</div>
							<div class="panel-tools">
										<a href="<?php echo Router::url('/', true) ?>FbaShipments"   class="btn bg-orange-500 color-white btn-dark">Go Back</a>
									</div>
							
						</div>
					<div class="panel-body">

					<div class="row">

                        <div class="col-lg-4 col-lg-offset-1">
								<div class="input text"><label for="barcode">Barcode</label> <input type="text" id="barcode" index="0" value="" class="get_sku_string form-control" name="barcode">
								<p class="help-block">Please scan an item to start processing order</p>
								</div>
								
							</div>
							
							<div class="col-lg-4 col-lg-offset-1"><img alt="" title="Scanner" src="/img/bar-code.png"></div>
					</div>
					
						<div class="row"><div class="col-lg-12"><label><span>Shipment containing barcode :<span class="searchbarcode"></span></label></div></div>
					 
					<div class="outer_grid">
					
					</div>	
				</div>
			</div>
			
		</div>  

</div>				 		
                          
	</div>
<script>
     // document.getElementById("barcode").focus();
 	 // $("#barcode").focus();
$(function() {
  $("#barcode").focus();
});
 

$(document).keypress(function(e) {
    if(e.which == 13) {
	
		if($("#barcode").val()){
			submitSanningValue();
		}else{
			alert('Please enter barcode.');
			$("#barcode").focus();
		}		
    }
});
 
 
function submitSanningValue()
{
	  var Barcode = $('#barcode').val();
		$('.searchbarcode').text(Barcode); 
	 
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaExportConsole/getSearchList',
		type: "POST",				
		cache: false,			
		data : { barcode : Barcode },
		success: function(result, textStatus, jqXHR)
		{	
			 
			$('#barcode').val('');  
			 $('.remaning').text(result.rem_qty); 	 
			$('.outer_grid').html(result.data); 	
			if(result.error){
				alert(result.msg);
			} 							
		}		
 	});   				
}
function ShowDiv(id,v){
	$("#div_"+id+"_"+v).hide();	
	$("#"+id+"_"+v).show();		 
}
function addPackHsOrigin(id,v){	 
	
	if(id == 'p'){		 
		var label = 'Packaging.'
		var field = 'packaging';	
	}else if(id == 'hs'){		 
		var label = 'Hs Code.'
		var field = 'hs_code';		 
	}else if(id == 'co'){		 
		var label = ' Country Of Origin.'
		var field = 'country_of_origin';		 
	}
	else if(id == 'ca'){	 
		var label = ' Category.'
		var field = 'category';		 
	}
	var input =  id + "in_" + v;
	
	if($("#"+input).val() == ''){	
		alert('Please enter ' +label);
	}else{
	
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaExportConsole/addPackHsOrigin',
			type: "POST",				
			cache: false,			
			data : {value:$("#"+input).val(),field:field,merchant_sku:v },
			success: function(data, textStatus, jqXHR)
			{			 	
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				}else{
				
					$("#div_"+id+"_"+v).show();	
					$("#div_"+id+"_"+v).html( $("#"+input).val());	
					$("#"+id+"_"+v).hide();		
	
				} 							
			}	
		});  
	}		 
}
function enterNumber(v){
  var q = $("#input_"+v).val();
  var id = 'input_' + v;
  var l_value = $('#l_' + v).text(); 
  var e = document.getElementById(id);
 
	if (!/^[0-9]+$/.test(e.value)) 
	{ 	
		alert("Please enter only number.");
	 	
	}
}    

function updateQuantity(v){	
		
} 

function printLabel(v){	
	var qty = $('#input_'+v).val();
	var printer = $('#printer_'+v).find("option:selected").val() ;
	var label_qty = parseInt($('#l_'+v).text());  
	if(qty < 1){
		alert('Please enter quantity.');
		$('#input_'+v).focus();
	}else{

		if(confirm("Are going to print #"+qty+" quantity?")){
			$("#label_"+v+" i").removeClass('fa-print');	
			$("#label_"+v+" i").addClass('fa-spin fa-refresh');	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaExportConsole/printLabel',
				type: "POST",				
				cache: false,			
				data : {id:v, qty:qty, printer:printer},
				success: function(data, textStatus, jqXHR)
				{	
					$("#label_"+v+" i").removeClass('fa-spin fa-refresh');	
					$("#label_"+v+" i").addClass('fa-print');	
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					}else{
						if($("#r_"+v).attr("for") == 'labelslipprint' || $("#r_"+v).attr("for") == 'slipprint'){
							$("#r_"+v).addClass('labelslipprint');
							$("#r_"+v).attr("for", "labelslipprint");
							$("#r_"+v).removeClass('slipprint');
						}else{
							$("#r_"+v).addClass('labelprint');
							$("#r_"+v).attr("for", "labelprint");
						}
						  	
					}	
						 						
				} 
			});
		}	
	 }	 
}

function printSlip(v){	
	var qty = $('#input_'+v).val();
	var printer = $('#printer_'+v).find("option:selected").val() ;
	
	$("#slip_"+v+" i").removeClass('fa-print');	
	$("#slip_"+v+" i").addClass('fa-spin fa-refresh');	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaExportConsole/printSlip',
		type: "POST",				
		cache: false,			
		data : {id:v,qty:qty,printer:printer},
		success: function(data, textStatus, jqXHR)
		{	
			$("#slip_"+v+" i").removeClass('fa-spin fa-refresh');	
			$("#slip_"+v+" i").addClass('fa-print');	
			console.log(data.msg);		
			if(data.error){
				alert(data.msg);
			}else{
			
				if($("#r_"+v).attr("for") == 'labelslipprint' || $("#r_"+v).attr("for") == 'labelprint'){
					$("#r_"+v).addClass('labelslipprint');
					$("#r_"+v).attr("for", "labelslipprint");
					$("#r_"+v).removeClass('labelprint');
				}else{
					$("#r_"+v).addClass('slipprint');
					$("#r_"+v).attr("for", "slipprint");
				}				
				  	
			} 								
		}		
 	});  		 
}

function addPackaging(v){	 
	
	if($("#pin_"+v).val() == ''){	
		alert('Please enter Packaging.');
	}else{
	
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaExportConsole/addPackaging',
			type: "POST",				
			cache: false,			
			data : {packaging:$("#pin_"+v).val(),merchant_sku:v },
			success: function(data, textStatus, jqXHR)
			{			 	
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				} 							
			}	
		});  
	}		 
}
function updatePackHsOrigin(t,v){	
	 
	var act = $(t).find("option:selected").val() ;  
	var id = $(t).attr('for');	
 	var field = $(t).attr('name');	;		 
	 
	 
	if(act == 'add_new'){
		$("#"+id+"_"+v).show();			 
	}else{
		$("#"+id+"_"+v).hide();
		
			
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaExportConsole/updatePackHsOrigin',
			type: "POST",				
			cache: false,			
			data : {value:act,field:field,merchant_sku:v },
			success: function(data, textStatus, jqXHR)
			{			 	
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				} 							
			}	
		});  
			
	}	 
}
function updatePackHsOrigin2(t,v){	
	 
	var val = $(t).find("option:selected").val() ; 
	
	var field = $(t).attr('name'); 
	
	var id = $(t).attr('id'); 
	 
	
	
	if(val == 'add_new'){
		$("#"+id+"_"+v).show();		
	}else{
		$("#"+id+"_"+v).hide();
		
		if(val !=''){	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaExportConsole/updatePackHsOrigin',
				type: "POST",				
				cache: false,			
				data : {field_value:val,merchant_sku:v,field:field },
				success: function(data, textStatus, jqXHR)
				{			 	
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					} 							
				}	
			});  
		}	
	}	 
}
</script>
