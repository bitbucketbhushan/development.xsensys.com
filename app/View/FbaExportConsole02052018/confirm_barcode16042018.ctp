<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.selected{ background-color: #66FFCC;}
	.btn-disable{ cursor: not-allowed;opacity : .5;}
	.col-5{ width:5%; float:left;}
	.col-6{ width:6%; float:left;}
	.col-7{ width:7%; float:left;}
	.col-8{ width:8%; float:left;}
	.col-9{ width:9%; float:left;}
	.col-10{ width:10%; float:left;}
	.col-11{ width:11%; float:left;}
	.col-12{ width:12%; float:left;}
	.col-14{ width:14%; float:left;}
	.col-15{ width:15%; float:left;}
	.col-16{ width:16%; float:left;}	
	.col-20{ width:20%; float:left;}
	.col-25{ width:25%; float:left;}
	.col-28{ width:28%; float:left;}
	.col-30{ width:30%; float:left;}
 </style>
 
 <div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php echo "FBA Export Console"; ?></h1>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
			<?php print $this->Session->flash(); ?>
              <div class="panel"> 
						<div class="panel-title">
							<div class="panel-head">Barcode Scan Section</div>
							<div class="panel-tools">
								<a href="<?php echo Router::url('/', true) ?>FbaExportConsole" class="btn btn-warning btn-xs color-white">Skip</a>
								
								<button type="button" id="print_label" for="0" data-print="0" class="btn btn-warning btn-xs btn-disable"><i class="fa fa-print"></i>&nbsp;Print Label</button>
								<button type="button" id="print_slip" for="0" data-print="0" class="btn btn-warning btn-xs btn-disable"><i class="fa fa-print"></i>&nbsp;Print Slip</button>
								<?php /*?><button type="button" id="process" for="" class="btn btn-success btn-xs btn-disable">Process</button> <?php */?>
								<button type="button" id="boxing" for="" class="btn btn-success btn-xs btn-disable">Boxing</button> 								
								
							</div>
							
						</div>
					<div class="panel-body">

					<div class="row">

                        <div class="col-lg-4 col-lg-offset-1">
								<div class="input text"><label for="barcode">Barcode</label> <input type="text" id="barcode" index="0" value="" class="get_sku_string form-control" name="barcode">
								<p class="help-block">Please scan an item to start processing order</p>
								</div>
								
							</div>
							
							<div class="col-lg-4 col-lg-offset-1"><img alt="" title="Scanner" src="/img/bar-code.png"></div>
					</div>
					
					<label><span>Shipment containing barcode : </span><span class="searchbarcode"></span></label>
					<div class="row">						 
						<label class="col-lg-12">Remaning Quantity: <strong style="color:red;" class="remaning">----</strong></label>  
                    </div>
					<div class="outer_grid">
						<div class="row head" style="clear:both;">
							  <div class="col-sm-12">
								<div class="col-12">SellerSku</div>		
								<div class="col-15">MasterSku</div>	
								<div class="col-10">Barcode</div>			
								<div class="col-10">FNSKU</div>																				
								<div class="col-30">Title</div>	
								<div class="col-5"><center>ScanQty</center></div>	
								<div class="col-8"><center>Quantity</center></div>
								<div class="col-10">-</div>	
							</div>								
						</div>	
		
						<div class="body" id="results">
						<?php
						foreach($all_orders as $count => $items){
							$cls = '';
							if($count == 0) { $cls = ' selected'; }
						?>	
							<div class="row sku" id="r_<?php echo $items['FbaItemLocation']['id']?>">
								<div class="col-lg-12 <?php echo $cls ?>">	
									<div class="col-12"><small><?php echo $items['FbaItemLocation']['merchant_sku'] ;?></small></div>	
									<div class="col-15"><small><?php echo $items['FbaItemLocation']['sku'] ;?></small></div>	
									<div class="col-10"><small><?php echo $items['FbaItemLocation']['barcode'] ;?></small></div> 		
									<div class="col-10"><small><?php echo $items['FbaItemLocation']['fnsku'] ;?></small></div>	
									<div class="col-30"><small><?php //echo $this->getProductTitle($items['FbaItemLocation']['sku']) ;?></small></div>	
									<div class="col-5"><small><center id="process_qty"><?php echo $items['FbaItemLocation']['process_qty'] ;?></center></small></div>
									<div class="col-8" <?php if( $items['FbaItemLocation']['quantity'] > 1) echo 'style="background-color:#00CC00;"';?> ><small><center><?php echo $items['FbaItemLocation']['quantity'] ;?></center></small></div>
									<div class="col-10">-</div>	
								</div>							 
							</div>												
						<?php } ?>
						 
						</div>
					</div>	
				</div>
			</div>
			
		</div>  

</div>						
                          
	</div>
<script>

$(function() {
  $("#barcode").focus();
});
 
$("#print_slip").click(function(){		
	if( $(this).attr('for') > 0){ 
		$("#print_slip i").removeClass('fa-print');	
		$("#print_slip i").addClass('fa-spin fa-refresh');	
		var v = $(this).attr('for');
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaExportConsole/printSlip',
			type: "POST",				
			cache: false,			
			data : {id:v},
			success: function(data, textStatus, jqXHR)
			{	
				$("#print_slip i").removeClass('fa-spin fa-refresh');	
				$("#print_slip i").addClass('fa-print');	
				$('#print_slip').attr('data-print',v);
				
				console.log(data.msg);		
				if(data.error){
					alert(data.msg);
				}else{
							
					if($('#print_label').attr('data-print') > 0){
						 $('#boxing').removeClass('btn-disable'); 	
						 $('#boxing').attr('for',v);
					}	
				}
											
			}		
		});  	
	}	 
});


$("#print_label").click(function(){	
	 
	if( $(this).attr('for') > 0){
			
			var v = $(this).attr('for');
		 
			$("#print_label i").removeClass('fa-print');	
			$("#print_label i").addClass('fa-spin fa-refresh');	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaExportConsole/printLabel',
				type: "POST",				
				cache: false,			
				data : {id:v},
				success: function(data, textStatus, jqXHR)
				{	
					$("#print_label i").removeClass('fa-spin fa-refresh');	
					$("#print_label i").addClass('fa-print');	
					$('#print_label').attr('data-print',v);
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					}else{
					 	
						if($('#print_slip').attr('data-print') > 0){
							 $('#boxing').removeClass('btn-disable'); 	
							 $('#boxing').attr('for',v);
						 }	
					}
						 						
				} 
			});
		} 	  
});

$("#boxing").click(function(){	
	 
	if( $(this).attr('for') > 0){
			
			var v = $(this).attr('for');
		    window.location.href = "<?php echo Router::url('/', true) ?>FbaExportConsole/completeBoxing/"+v;
 			
		}	
	  
});

$("#process").click(function(){	
	 
	if( $(this).attr('for') > 0){
			
			var v = $(this).attr('for');
		 
 			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaExportConsole/completeBoxing',
				type: "POST",				
				cache: false,			
				data : {id:v},
				success: function(data, textStatus, jqXHR)
				{	
				 
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					}else{
						window.location.href = "<?php echo Router::url('/', true) ?>FbaExportConsole";
					}
						 						
				} 
			});
		}	
	  
});

$(document).keypress(function(e) {
    if(e.which == 13) {
		completeBarcode(); 
    }
});
 
 

function completeBarcode()
{
	var Barcode = $('#barcode').val();
	$('.searchbarcode').text(Barcode); 
 
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaExportConsole/completeBarcode',
		type: "POST",				
		cache: false,			
		data : {  barcode : Barcode,  id : '<?php echo $this->params['pass'][0]; ?>'},
		success: function(result, textStatus, jqXHR)
		{	
		  	$('#barcode').val('');  
			if(result.status == 1){
				 $('#process_qty').text(result.qty); 	
			}else if(result.status == 2){
				 $('#process_qty').text(result.qty); 	
				 $('#print_label').removeClass('btn-disable'); 	
				 $('#print_label').attr('for','<?php echo $this->params['pass'][0]; ?>');
				 $('#print_slip').removeClass('btn-disable'); 
				 $('#print_slip').attr('for','<?php echo $this->params['pass'][0]; ?>');	
				
			}else if(result.status == 0){
				alert(result.userdetail);
			} 
			//$('.outer_grid').html(result.data); 	
			if(result.error){
				alert(result.msg);
			} 							
		}		
 	});   				
}

</script>
