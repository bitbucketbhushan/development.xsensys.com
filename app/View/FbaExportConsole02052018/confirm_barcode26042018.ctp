<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.selected{ background-color: #66FFCC;}
	.btn-disable{ cursor: not-allowed;opacity : .5;}
	.gr{padding:3px 0px;}
	.bt{border-top:1px solid #ccc;}
	.col-5{ width:5%; float:left;}
	.col-6{ width:6%; float:left;}
	.col-7{ width:7%; float:left;}
	.col-8{ width:8%; float:left;}
	.col-9{ width:9%; float:left;}
	.col-10{ width:10%; float:left;}
	.col-11{ width:11%; float:left;}
	.col-12{ width:12%; float:left;}
	.col-14{ width:14%; float:left;}
	.col-15{ width:15%; float:left;}
	.col-16{ width:16%; float:left;}.col-17{ width:17%; float:left;}
	.col-18{ width:18%; float:left;}.col-19{ width:19%; float:left;}	
	.col-20{ width:20%; float:left;}
	.col-25{ width:25%; float:left;}
	.col-28{ width:28%; float:left;}
	.col-30{ width:30%; float:left;}
	.col-90{ width:90%; float:left;}
 </style>
 
 <div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php echo "FBA Export Console"; ?></h1>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
			<?php print $this->Session->flash(); ?>
              <div class="panel"> 
						<div class="panel-title">
							<div class="panel-head">Complete Shipment </div>
							<div class="panel-tools">
								<a href="<?php echo Router::url('/', true) ?>FbaExportConsole" class="btn btn-warning btn-xs color-white">Skip</a>
								
								<!--<button type="button" id="print_label" for="0" data-print="0" class="btn btn-warning btn-xs btn-disable"><i class="fa fa-print"></i>&nbsp;Print Label</button>
								<button type="button" id="print_slip" for="0" data-print="0" class="btn btn-warning btn-xs btn-disable"><i class="fa fa-print"></i>&nbsp;Print Slip</button>-->
								<?php /*?><button type="button" id="process" for="" class="btn btn-success btn-xs btn-disable">Process</button> <?php */?>
								<button type="button" id="boxing" for="" class="btn btn-success btn-xs btn-disable">Boxing</button> 								
								
							</div>
							
						</div>
					<div class="panel-body">

						<div id="shipment"></div>
					
					
						<div class="outer_grid">
							<div class="body" id="results"><center><img style="text-align:center;" src="<?php echo $this->webroot; ?>img/482.gif" /></center></div> 
							 
							
							 <div id="ship_box"></div>
							 
 							<!-- Modal -->
							<div id="myModal" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							
								<!-- Modal content-->
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Add Box With Dimentions</h4>
								  </div>
								  <div class="modal-body">
								  
									<div class="row">
										<div class="col-lg-12"> 
										 <div class="col-8">Length:</div>
										 <div class="col-20"><input type="text" name="box_l" id="box_l" class="col-90" value=""/></div>
										 <div class="col-8">Height:</div>
										 <div class="col-20"><input type="text" name="box_h" id="box_h" class="col-90" value=""/></div> 
										 <div class="col-8">Width:</div>
										 <div class="col-20"><input type="text" name="box_w" id="box_w" class="col-90" value=""/></div> 
										 <div class="col-10"><button type="button" class="btn btn-success btn-xs" onclick="saveBox();">Save</button></div>
										</div>
									</div>  
								   
								  </div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								  </div>
								</div>
							
							  </div>
							</div>
							<!-- End Modal -->
						</div>
					</div>	
				</div>
			</div>
			
		</div>  
	</div>	                         
</div>
<script>
 
$( "#box_barcode" ).on( "keydown", function(event) {
	 if(event.which == 13) {	 
	
	 $.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaExportConsole/getShipmentBoxGrid/'+$('#shipment_id').val()+'/1',
				type: "POST",				
				cache: false,			
				data : {shipment_id:$('#shipment_id').val()},
				success: function(data, textStatus, jqXHR)
				{			
					 
					$('#ship_box').html(result.ship_box); 	
					 
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					}else{
						location.reload();
					}											
				} 
			}); 
	 
	 }
});

function saveShipmentBoxQty(box_no)
{
	if($("#qty_"+box_no).val() == ''){
		alert('Please enter box quantity.');
		$("#qty_"+box_no).focus();	 
	}else{
  		$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaExportConsole/saveShipmentBoxQty',
				type: "POST",				
				cache: false,			
				data : {shipment_id:$('#shipment_id').val(),box_inc_id:$("#box_inc_id_"+box_no).val(),box_number:$("#box_number_"+box_no).val(),qty:$("#qty_"+box_no).val(),fnsku:$('#fnsku').val(),merchant_sku:$('#merchant_sku').val()},
				success: function(data, textStatus, jqXHR)
				{			
					 
					$("#grid").html(data.data);
					 
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					}else{
						location.reload();
					}											
				} 
			}); 	
 	} 
}

function saveBox()
{
	if($("#box_l").val() == ''){
		alert('Please enter box Length.');
		$("#box_l").focus();
	}else if($("#box_h").val() == ''){
		alert('Please enter box Height.');
		$("#box_h_").focus();
	}else if($("#box_w").val() == ''){
		alert('Please enter box Width.');
		$("#box_w").focus();
	}else{
 
		 
		$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaExportConsole/shipmentBoxes',
				type: "POST",				
				cache: false,			
				data : {shipment_id:$('#shipment_id').val(),box_length:$('#box_l').val(),box_height:$('#box_h').val(),box_width:$('#box_w').val()},
				success: function(data, textStatus, jqXHR)
				{			
					 
					$("#grid").html(data.data);
					 
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					}else{
						location.reload();
					}											
				} 
			}); 	
	
	} 
} 
 
 loadAjax();
 
 function loadAjax()
{
	  
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaExportConsole/confirmBarcodeAjax/<?php echo $this->params['pass'][0]; ?>',
		type: "POST",				
		cache: false,			
		data : { id : ' <?php echo $this->params['pass'][0]; ?>'},
		success: function(result, textStatus, jqXHR)
		{	
  			$('#results').html(result.data); 	
			$('#shipment').html(result.shipment); 	
			$('#ship_box').html(result.ship_box); 	
			if(result.error){
				alert(result.msg);
			} 							
		}		
 	});   				
}

function printSlip(v){	
	var slip_status = $('#slip_status').val();
	
	if(slip_status > 0){
		if(!confirm("Slip for this is alredy printed. If you want to print again click on OK.")){
			return;
		}
	}
	
	var qty = $('#input_'+v).val();
	var printer = $('#printer_'+v).find("option:selected").val() ;
	
	$("#slip_"+v+" i").removeClass('fa-print');	
	$("#slip_"+v+" i").addClass('fa-spin fa-refresh');	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaExportConsole/printSlip',
		type: "POST",				
		cache: false,			
		data : {id:v,qty:qty,printer:printer},
		success: function(data, textStatus, jqXHR)
		{	
			$("#slip_"+v+" i").removeClass('fa-spin fa-refresh');	
			$("#slip_"+v+" i").addClass('fa-print');	
			console.log(data.msg);		
			if(data.error){
				alert(data.msg);
			}else{
				$("#slip_status").val('1');				  	
			} 								
		}		
 	});  		 
}
function printLabel(v){	

	var label_status = $('#label_status').val();
	
	if(label_status > 0){
		if(!confirm("Label for this is alredy printed. If you want to print again click on OK.")){
			return;
		}
	}

	var qty = $('#input_'+v).val();
	var printer = $('#printer_'+v).find("option:selected").val() ;
	var label_qty = parseInt($('#l_'+v).text());  
	if(qty < 1){
		alert('Please enter quantity.');
		$('#input_'+v).focus();
	}else{
		
			$("#label_"+v+" i").removeClass('fa-print');	
			$("#label_"+v+" i").addClass('fa-spin fa-refresh');	
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaExportConsole/printLabel',
				type: "POST",				
				cache: false,			
				data : {id:v, qty:qty, printer:printer},
				success: function(data, textStatus, jqXHR)
				{	
					$("#label_"+v+" i").removeClass('fa-spin fa-refresh');	
					$("#label_"+v+" i").addClass('fa-print');	
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					}else{
						$("#label_status").val('1');	
					}	
					
						 						
				} 
			});
			
	 }	 
}
function ShowDiv(id,v){
	$("#div_"+id+"_"+v).hide();	
	$("#"+id+"_"+v).show();		 
}
function addPackHsOrigin(id,v){	 
	
	if(id == 'p'){		 
		var label = 'Packaging.'
		var field = 'packaging';	
	}else if(id == 'hs'){		 
		var label = 'Hs Code.'
		var field = 'hs_code';		 
	}else if(id == 'co'){		 
		var label = ' Country Of Origin.'
		var field = 'country_of_origin';		 
	}
	else if(id == 'ca'){	 
		var label = ' Category.'
		var field = 'category';		 
	}
	var input =  id + "in_" + v;
	
	if($("#"+input).val() == ''){	
		alert('Please enter ' +label);
	}else{
	
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaExportConsole/addPackHsOrigin',
			type: "POST",				
			cache: false,			
			data : {value:$("#"+input).val(),field:field,merchant_sku:v },
			success: function(data, textStatus, jqXHR)
			{			 	
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				}else{
				
					$("#div_"+id+"_"+v).show();	
					$("#div_"+id+"_"+v).html( $("#"+input).val());	
					$("#"+id+"_"+v).hide();		
	
				} 							
			}	
		});  
	}		 
}
function enterNumber(v){
  var q = $("#input_"+v).val();
  var id = 'input_' + v;
  var l_value = $('#l_' + v).text(); 
  var e = document.getElementById(id);
 
	if (!/^[0-9]+$/.test(e.value)) 
	{ 	
		alert("Please enter only number.");
	 	
	}
}
function addPackaging(v){	 
	
	if($("#pin_"+v).val() == ''){	
		alert('Please enter Packaging.');
	}else{
	
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaExportConsole/addPackaging',
			type: "POST",				
			cache: false,			
			data : {packaging:$("#pin_"+v).val(),merchant_sku:v },
			success: function(data, textStatus, jqXHR)
			{			 	
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				} 							
			}	
		});  
	}		 
}

function updatePackHsOrigin(t,v){	
	 
	var act = $(t).find("option:selected").val() ;  
	var id = $(t).attr('for');	
 	var field = $(t).attr('name');	;		 
	 
	 
	if(act == 'add_new'){
		$("#"+id+"_"+v).show();			 
	}else{
		$("#"+id+"_"+v).hide();
		
			
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaExportConsole/updatePackHsOrigin',
			type: "POST",				
			cache: false,			
			data : {value:act,field:field,merchant_sku:v },
			success: function(data, textStatus, jqXHR)
			{			 	
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				} 							
			}	
		});  
			
	}	 
}

$("#boxing").click(function(){	
	 
	if( $(this).attr('for') > 0){
			
			var v = $(this).attr('for');
		    window.location.href = "<?php echo Router::url('/', true) ?>FbaExportConsole/completeBoxing/"+v;
 			
		}	
	  
});

$("#process").click(function(){	
	 
	if( $(this).attr('for') > 0){
			
			var v = $(this).attr('for');
		 
 			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>FbaExportConsole/completeBoxing',
				type: "POST",				
				cache: false,			
				data : {id:v},
				success: function(data, textStatus, jqXHR)
				{	
				 
					console.log(data.msg);	
					if(data.error){
						alert(data.msg);
					}else{
						window.location.href = "<?php echo Router::url('/', true) ?>FbaExportConsole";
					}
						 						
				} 
			});
		}	
	  
});

$(document).keypress(function(e) {
    if(e.which == 13) {
		completeBarcode(); 
    }
});
 
 

function completeBarcode()
{
	var Barcode = $('#barcode').val();
	$('.searchbarcode').text(Barcode); 
 
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaExportConsole/completeBarcode',
		type: "POST",				
		cache: false,			
		data : {  barcode : Barcode,  id : '<?php echo $this->params['pass'][0]; ?>'},
		success: function(result, textStatus, jqXHR)
		{	
		  	$('#barcode').val('');  
			if(result.status == 1){
				 $('#process_qty').text(result.qty); 	
			}else if(result.status == 2){
				 $('#process_qty').text(result.qty); 	
				 $('#print_label').removeClass('btn-disable'); 	
				 $('#print_label').attr('for','<?php echo $this->params['pass'][0]; ?>');
				 $('#print_slip').removeClass('btn-disable'); 
				 $('#print_slip').attr('for','<?php echo $this->params['pass'][0]; ?>');	
				
			}else if(result.status == 0){
				alert(result.userdetail);
			} 
			//$('.outer_grid').html(result.data); 	
			if(result.error){
				alert(result.msg);
			} 							
		}		
 	});   				
}

</script>
