<?php
class PackagetypesController extends AppController
{
    
    var $name = "Packagetypes";
    
    var $components = array('Session','Upload','Common','Auth');
    
    var $helpers = array('Html','Form','Common','Session');
    
    public function addPackage( $id = null )
    {
		$this->layout = "index";
		
		if(isset($this->request->data['Packagetype']['id']))
        	$id = $this->request->data['Packagetype']['id'];
		
		if( isset( $id ) & $id > 0 )
            $this->set( 'role','Edit PackageType' );
        else
            $this->set( 'role','Add PackageType' );
         
        if( $this->request->is('post') )
		{
			if( empty( $id ) && $id == '' )
			{ 
				//Add case				
				$this->Packagetype->saveAll( $this->request->data );
				
				$this->redirect( '/JijGroup/Packaging/Type/Show' );
				
				/* Redirect action after success */
				$this->Session->setflash( "Add successful" , 'flash_success');
			}
			else
			{
				//Edit case
			}
		}
	}
    
    public function showAllPackage()
    {
		$this->layout = "index";
		
		$this->set( 'role','ShowAll PackageTypes' );
		$this->set('packageType' , $this->Packagetype->find('all'));
		$this->render( 'show_all_package' );
	}
        
    public function addEnvelope( $id = null )
    {
		$this->layout = "index";
		$this->loadModel( 'PackageEnvelope' );
		
		if(isset($this->request->data['PackageEnvelope']['id']))
        	$id = $this->request->data['PackageEnvelope']['id'];
		
		if( isset( $id ) & $id > 0 )
            $this->set( 'role','Edit Envelope' );
        else
            $this->set( 'role','Add Envelope' );
        
        // Set Packagibg Type as list
        
        $this->set( 'packageList' , $this->Packagetype->find('list' , array( 'fields' => array( 'Packagetype.package_type_name' ) )) );
         
        if( $this->request->is('post') )
		{
			if( empty( $id ) && $id == '' )
			{ 
				
				//Add case				
				$this->PackageEnvelope->saveAll( $this->request->data );
				
				$this->redirect( '/JijGroup/Envelope/PackagingEnvelopes/Show' );
				
				/* Redirect action after success */
				$this->Session->setflash( "Add successful" , 'flash_success');
			}
			else
			{
				//Edit case
				echo "Edit case"; exit;
			}
		}
	}    
        
    public function showAllEnvelope()
    {
		$this->layout = "index";
		$this->loadModel( 'PackageEnvelope' );
		$this->set( 'role','ShowAll Variants' );
		$this->set('packageEnvelope' , $this->PackageEnvelope->find('all'));
		$this->render( 'show_all_envelope' );
	}
}

?>
