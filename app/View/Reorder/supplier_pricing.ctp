<link rel="stylesheet" href="/css/bootstrap-multiselect.css" type="text/css"> 
<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.head div div div{ padding:0px ;  }
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.col-65 { width: 65%; float: left;}
	.col-60 { width: 60%; float: left;}
	.col-55 { width: 55%; float: left;}	 
	.editable-submit, .editable-cancel{padding: 3px 8px;}
	.icon-remove, .icon-ok {position: relative;top: 1px;display: inline-block;font-family: 'Glyphicons Halflings';-webkit-font-smoothing: antialiased;font-style: normal;font-weight: normal; line-height: 1;}
	.icon-ok:before {content: "\e013";}
	.icon-remove:before {    content: "\e014";}
	.paginate-responsive {    text-align: center;	}
	.paginate-responsive .total_pages {    float: left;    width: auto;    display: inline-block;    margin: 0px;    padding: 5px 0 0;	}
	.paginate-responsive .paginate {    float: left;    text-align: center;    display: inline-block;    width: 68%;}
	.paginate-responsive .pagination {    display: inline-block;    padding-left: 0;    margin: 10px 0;    border-radius: 4px;}
	.paginate-responsive .pagination > li {    display: inline;}
	.paginate-responsive .total_rows {    float: right;    width: auto;    display: inline-block;    margin: 0px;    padding: 5px 0 0;}
	.cqty{padding:2px;text-align:center;}
	.multiselect-container {    height: 500px;    overflow: scroll;	}
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Supplier Pricing & Purchase order (<?php echo implode(",",$quote_supplier);//if(isset($this->params['pass'][0])){ echo $this->params['pass'][0];}?>)</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						
  						<div class="row">	
						  <div class="col-sm-12"> 						  
								
							 					
								<div class="col-sm-4" style="padding-left:0px; padding-right:0px;">	
								 <?php						
												print $this->form->create( 'upload', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/Reorder/UploadFile', 'type'=>'post','id'=>'upload' ) );
											?>
								 <div class="input-group">
									<span class="input-group-btn">
										<span class="btn btn-primary btn-file">
											Browse File <?php
														print $this->form->input( 'import_file', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'', 'required'=>false) );
													  ?>
										</span>
									</span>
									<input type="text" placeholder="No file selected" readonly="" class="form-control">
									<span class="input-group-addon no-padding-top no-padding-bottom">
									<div class="radio radio-theme min-height-auto no-margin no-padding">
										<?php
										echo $this->Form->button('Upload', array(
										'type' => 'submit',
										'escape' => true,
										'class'=>'btn bg-green-500 color-white btn-dark'
										 ));	
									?>
									</div>
									</span>
									</div>
									</form>
									<small style="display: inline-block;font-size: 75%; color:#FF0000;">(Upload Quotation<a href="<?php echo Router::url('/', true) ?>sample_file/update_reorder_lavel_qty.csv">Download Sample</a>)</small>
								 	 
							 
 							</div>
							 
							 
							 <div class="col-sm-2 supplier suppliercode"> 
 								 <select name="suppliercode" id="suppliercode" class="form-control" multiple="multiple" onchange="loadajax()">
 									<?php foreach($suppliers as $sup){?>
									<option value="<?php echo strtolower($sup['SupplierDetail']['supplier_code']);?>" <?php if(in_array(strtolower($sup['SupplierDetail']['supplier_code']),$quote_supplier)){?> selected="selected"<?php }?> ><?php echo strtolower($sup['SupplierDetail']['supplier_name']);?></option>
									<?php }?>
 								</select>			 
 								<small style="display: inline-block;font-size: 75%; color:#FF0000;">(Select Suppliers)</small>	
								
							 </div>
							   <div class="col-sm-2">
 								<button type="button" class="btn btn-warning btn-sm" onclick="loadajax()">Filter Data</button>

							 </div>
							
							 
						 
							<div class="col-15">				
							  <!--Info buttons with dropdown menu-->
								<div class="btn-group btnopen">
									<button type="button" class="btn btn-info btn-sm generate_po">Generate P.O.</button>
									<button type="button" data-toggle="dropdown" class="btn btn-info btn-sm  dropdown-toggle"><span class="caret"></span></button>
									<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu" id="po">												 
									
									</ul>
								</div>					
							</div>	
							
						
									 
						 </div> 			
						</div>
						 	
					</div>	
					
					<div class="panel-body no-padding-top bg-white">
				 
 						<div class="  table-responsive">	
			
						<table id="grid" class="table pog table-striped">
						  <thead>
							
							<tr>
								<th width="12%" class="msku">Master SKU
									<div class="dtq">-</div>
								</th>
								<th width="5%">					
								<span onClick="SortBy(this);" class="sort-by" data-name="price" order-by="ASC">
								<i class="glyphicon glyphicon-sort-by-attributes"></i> Exp. Price</span>
								<div class="dtq">-</div>					
								</th>	
 								<th width="5%">Max Exp. Price<div class="dtq"><input type="text" value="<?php echo $fee['Fee']['po_margin']?>" style="text-align: center;  width: 24px;" id="mar" onChange="SaveMargin('<?php echo $fee['Fee']['id']?>');">%</div></th>	
								<th width="5%">
								<span onClick="SortBy(this);" class="sort-by" data-name="qty" order-by="ASC">
								<i class="glyphicon glyphicon-sort-by-attributes"></i> Exp. Qty</span>
								<div class="dtq"><span class="tq" id="qty_exp"></span></div>
								</th>	
								<th width="15%"><span onClick="SortBy(this);" class="sort-by" data-name="Supplier_Count" order-by="ASC">
								<i class="glyphicon glyphicon-sort-by-attributes"></i>Available Qty(From All)</span><div class="dtq">-</div>	
								</th>	
							
							</tr>
							
							 
							<tr>							
								<th colspan="4"><input type="search" autocomplete="off" class="form-control" placeholder="Search By Master SKU" id="master_sku" name="sku"></th>															
								<th class="right"><button id="Filter" type="submit" title="Filter" class="button btn btn-info" onClick="resetFilter();">Reset Filter</button></th>				
							</tr>
								
						  </thead>
						  <tbody id="results">				
						  </tbody>
						  <div class="paginate-responsive"> </div>  		
						</table>
			 
          </div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
		 
		  
<div class="outerOpac" id="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999; al " src="<?php echo $this->webroot; ?>img/482.gif" />
</div> 
<script>	
  $(document).ready(function() {
     //for Reorder
		 
		//$('#stores').selectpicker();
		
		  $('#suppliercode').multiselect({
			numberDisplayed: 1, 
			includeSelectAllOption: false,
			nonSelectedText :'Select Suppliers',
      	    enableFiltering:true 
		});	
		///end	
		// getSuppliers();
  
   });
 

function getSuppliers(){ 
		$(".outerOpac").show();
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Reorder/getSuppliers',
			type: "POST",
			data : {					 
					buying_company:$("#buying_company option:selected").val()
				},
			success: function(data, textStatus, jqXHR)
			{		
				$(".outerOpac").hide();
				$('#suppliercode').empty();
				//$("#div-selling-stores").html(data.html);	
				for(var i=0; i< data.options.length; i++ ){ 
					$('#suppliercode').append('<option value="'+data.options[i].toLowerCase()+'">'+data.options[i].toLowerCase()+'</option>');
 				}
				$('#suppliercode').multiselect('rebuild');	
			}		
		});	
			
}

  $(".filter").click(function(){
	var suppliers  = $('.suppliercode li.active input[type="checkbox"]').map(function () {if(this.value!='multiselect-all'){return this.value; }}).get().join(",");
		if(suppliers == ''){
			alert('Please select Suppliers!'); 
		}else{
			 loadajax();
		}
 });
 
jQuery(document).ready(function() {  
        $.fn.editable.defaults.mode = 'popup';
        jQuery('.xedit').editable();		
		$(document).on('click','.editable-submit',function(){
			var x = $(this).closest('section').children('span').attr('pk');			
			var y = $('.input-sm').val();
			var z = $(this).closest('section').children('span'); 
			var act = $(this).closest('section').children('span').attr('action');
			var fname = $(this).closest('section').children('span').attr('data-name');
						
			$.ajax({
				url: "<?php echo Router::url('/', true) ?>Reorder/Update",
				type: "POST",
				data : {action:act,value:y,pk:x,name:fname},
				success: function(s){
					if(s == 'status'){
					$(z).html(y);}
					if(s == 'error') {
					alert('Error Processing your Request!');}
				},
				error: function(e){
					alert('Error Processing your Request!!');
				}
			});
		});
});



function SortBy(val){
	var name = $(val).attr("data-name"); 
	$(".pagination li a").attr("data-name",name);
	$(".sort-by").removeClass("active");	
	$(val).addClass("active");	
	$(".sort-by i").toggleClass('glyphicon-sort-by-attributes-alt glyphicon-sort-by-attributes');
	if($(val).attr("order-by") == 'ASC'){	
		var order = 'DESC';
		$(val).attr("order-by","DESC");	
		$(".pagination li a").attr("order-by","DESC");
	}else if($(val).attr("order-by") == 'DESC'){	 
		var order = 'ASC';
		$(val).attr("order-by","ASC");
		$(".pagination li a").attr("order-by","ASC");
		//$(".pagination li a").attr("data-value","ASC");
	}
	var suppliers  = $('.suppliercode li.active input[type="checkbox"]').map(function () {if(this.value!='multiselect-all'){return this.value; }}).get().join(",");

	$("#loading-mask").show();
	$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Reorder/getSupplierPricing',
			type: "POST",
			data : {name:name,orderby:order,supplier_code:suppliers},
			success: function(txt, textStatus, jqXHR)
			{					
				$(".suppliers").remove();
				$(".msku").after(txt.header);	
				
				$(".paginate-responsive").html(txt.paginate);					
				$("#results").html(txt.data);	
				$("#loading-mask").hide();	
			}		
		});
}

function loadajax(val){  
	var page = $(val).attr("data-page"); 
	var name = $(val).attr("data-name"); 
	var order = $(val).attr("order-by"); 
	var value = $(val).attr("data-value"); 
	var suppliers  = $('.suppliercode li.active input[type="checkbox"]').map(function () {if(this.value!='multiselect-all'){$("."+this.value).show();return this.value; }}).get().join(","); 
 	$("#loading-mask").show();
	
	/*$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Reorder/addSupplierPricing',
			type: "POST",
			data : {action:'filter_suppliers',supplier_code:suppliers},
			success: function(txt, textStatus, jqXHR)
			{	 
				$(".suppliers").remove();
				$(".msku").after(txt.data);	 
			}		
		});	*/
			
	$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Reorder/getSupplierPricing',
			type: "POST",
			data : {page:page,name:name,orderby:order,value:value,supplier_code:suppliers,master_sku:$("#master_sku").val(),qt:'<?php echo $this->params['pass'][0];?>'},
			success: function(txt, textStatus, jqXHR)
			{					 
				$('#message').show();	
				$("#loading-mask").hide();	
				
				$(".suppliers").remove();
				$(".msku").after(txt.header);	
				
 				$(".paginate-responsive").html(txt.paginate);					
				$("#results").html(txt.data);	
				var res = [];//txt.suppliers.split("|"); 
				
				//alert(res);				
				for(var i=0; i<res.length; i++){				
					var str = res[i];					
					var supl = str.split("="); 					
					$("#qty_"+supl[0]).html(supl[1]);					
				}				
				$("#qty_exp").html(txt.total_qty);
				
				//$('#message').delay(5000).fadeOut('slow');						
			}		
		});
}

function SaveMargin(vid){

	var val = $("#mar").val(); 

	$("#loading-mask").show();
		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Reorder/UpdateMargin',
		type: "POST",
		data : {action:'margin',id:vid,value:val},
		success: function(data, textStatus, jqXHR)
		{		
			$("#loading-mask").hide();
		}		
	});				
	loadajax(val);
}
$(".generate_po").click(function(){	
	if (confirm('Are you sure to Generate P.O.?')) {	
		var suppliers  = $('.suppliercode li.active input[type="checkbox"]').map(function () {if(this.value!='multiselect-all'){return this.value; }}).get().join(",");
	  
		$("#loading-mask").show();
		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Reorder/generatePo',
		type: "POST",
		data : {supplier_code:suppliers},
		success: function(data, textStatus, jqXHR)
		{		
			$("#loading-mask").hide();
			$(".btnopen").addClass('open');					
			//$("#message").html(data.msg);	
			$("#po").append(data.file_link);					
			//$('#message').delay(10000).fadeOut('slow');							
		}		
	});	
	}	 
});

function update(act){
	$("#loading-mask").show();
	var suppliers  = $('.suppliercode li.active input[type="checkbox"]').map(function () {if(this.value!='multiselect-all'){return this.value; }}).get().join(",");
		$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Reorder/Update',
		type: "POST",
		data : {action:act,supplier_code:suppliers},
		success: function(data, textStatus, jqXHR)
		{		
			$("#loading-mask").hide();	
			$("#message").html(data.msg);	
			loadajax(this);									
			//$('#message').delay(10000).fadeOut('slow');							
		}		
	});	
}

/*$(".Update_Price").click(function(){	
	update('Update_Price');
});*/
$(".Update_Qty").click(function(){	
	update('Update_Qty');
});
$(".flush").click(function(){	
	var suppliers  = $('.supplier_code li.active input[type="checkbox"]').map(function () {if(this.value!='multiselect-all'){return this.value; }}).get().join(",");
	if(suppliers == ''){
		alert('Please select Suppliers!'); 
	}else{
		update('flush');
	}
});


function resetFilter(){
	$("#master_sku").val('');			
	loadajax(this);		
}

$("#master_sku").on( "keydown", function(event) {
	 if(event.which == 13) {	loadajax(this);		}
});


$(document).ready(function()
{

	$("#loading-mask").hide();

	var options = { 
				beforeSend: function() 
				{				
					$("#loading-mask").show();					
					$("#message").html("");
				},
				uploadProgress: function(event, position, total, percentComplete) 
				{ 
					$("#bar").width(percentComplete+'%');
					$("#percent").html(percentComplete+'%');
				},
				success: function() 
				{
					$("#bar").width('100%');
					$("#percent").html('100%');				
				},
				complete: function(response) 
				{	$('#message').show(); 		
					var	 d = $.parseJSON(response.responseText);
						if(d.error=='yes'){
							$("#message").html(d.msg);
							$('#loading-mask').hide();	
							//$('#message').delay(5000).fadeOut('slow');
						}else{
							$("#message").html(d.msg);
							$('#loading-mask').hide();	
							//$('#message').delay(5000).fadeOut('slow');
							loadajax(this);	//$("#results" ).load( "<?php echo Router::url('/', true) ?>Reorder/getSupplierPricing",{"sku":$("#sec_sku option:selected").val()});
						}
					},
				error: function()
				{
				$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
				
				}
	
	}; 
	//$("#supplier_data").ajaxForm(options);
	$("#supplierfile").change(function() {
		$("#supplier_data").submit();	
	});

});

$(document).ready(function() {		
	//loadajax();
	update('Update_Price');
	$(".paginate-responsive").on( "click", ".pagination a", function (e){
			e.preventDefault();			
			loadajax(this);			
		});
	
});
</script>