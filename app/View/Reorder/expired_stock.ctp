<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.head div div div{ padding:0px ;  }
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.previewdata td, .previewdata th{padding:5px;
	.col-65 { width: 65%; float: left;}
	.col-60 { width: 60%; float: left;}
	.col-55 { width: 55%; float: left;}	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Expired Stock(<small><a href="<?php echo Router::url('/', true) ?>Stocks/downloadExpiredStock">Download</a></small>)</h4>
			</div>
    	  <div class="container-fluid">	
		  
		    <div class="panel">
			 <div class="panel-body">
		 	<?php /*?> <div class="row">	
					<div class="col-lg-12">
					  	 
						<div class="col-md-3 col-lg-2"> 
 								<input type="text" value="" placeHolder="Enter Quantity" id="quantity" name="quantity" class="form-control" />
						</div>	
 						 <div class="col-lg-3"> 
								<button type="submit" class="btn btn-warning pick_preview">Picklist Preview</button>
						</div>
						
						</div> 
						<div class="col-lg-12 preview">&nbsp;</div>  
					</div> 		
				</div>
			</div>		<?php */?>			
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						
 						<form method="get" name="searchfrm" action="<?php echo Router::url('/', true) ?>Stocks/expiredStock" enctype="multipart/form-data">	
						<div class="col-sm-5 no-padding">
							<div class="col-lg-4 no-padding">
									<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Search BY SKU,Barcode,Name,Location" name="searchkey" class="form-control searchString" />
								</div>
								
								<div class="col-lg-2"> 
									<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
								</div>
								<?php if(isset($_GET['searchkey'])){?>
								<div class="col-lg-2"> <a href="<?php echo Router::url('/', true) ?>Stocks/expiredStock" class="btn btn-info">Back</a></div> 
								<?php }?>
								<div class="col-lg-3" style="float: right;"> 
									<a href="<?php echo Router::url('/', true) ?>ExpiryPicklist/index" class="btn btn-warning">Generate Picklist</a>
								</div>
						</div>
						</form>
						<div class="col-sm-7 text-right">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						</div>	
					<div class="panel-body no-padding-top bg-white">
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-5">Id</div>		
 								<div class="col-10">Sku</div>	
								<div class="col-10">Barcode</div>											
								<div class="col-25">Title</div>			
								<div class="col-5">Stock</div>																				
								<div class="col-8">Min Acceptable Life</div>
								<div class="col-10">Picked Date</div>		
								<div class="col-18">Picklist Name</div>		
								<div class="col-8">Status</div>			 							 								 
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						 if(count($allproducts) > 0) {	 ?>
							<?php foreach( $allproducts as $items ) {   ?>
								<div class="row sku" id="r_<?php echo $items['Product']['id']; ?>">
									<div class="col-lg-12">	
										<div class="col-5"><small><?php echo $items['Product']['id']; ?></small></div>	
										<div class="col-10"><small><?php echo $items['Product']['product_sku']; ?></small></div>	
										<div class="col-10"><small><?php echo $items['Product']['barcode']; ?></small>	</div>										
										<div class="col-25"><small><?php echo $items['Product']['product_name']; ?></small></div>
										
										<div class="col-5"><small><a href="javascript:void(0);" style="color:#000099; text-decoration:underline" onclick="viewStock('<?php echo $items['Product']['product_sku'];?>');" ><?php echo $expiredStock[$items['Product']['product_sku']]; ?></a></small></div>	
										
										<div class="col-8"><small><?php echo $items['Product']['min_days_life']; ?></small></div> 
										
										<?php if(isset($expiredSku[$items['Product']['product_sku']])){?>
										
										<div class="col-10"><small>
										<?php 
											foreach($expiredSku[$items['Product']['product_sku']] as $t){ 
												echo date("d-m-Y",strtotime($t['created_date'])).'<br>';
											}
										 ?>
										 </small></div>		
										<div class="col-18"><small>
										<?php 
											foreach($expiredSku[$items['Product']['product_sku']] as $t){ 
												echo $t['picklist_name'].'<br>';
											}
										 ?> 
										 </div>	</small>	
										<div class="col-8"><small>
										<?php  $status = 'Open';
												foreach($expiredSku[$items['Product']['product_sku']] as $t){ 
													if($t['process_user'] != ''){
														 $status = 'Complete';
													}													 
												}
												 echo  $status;
										?></small>
										</div>
									
										<?php }else{?>
 										<div class="col-10">-</div>		
										<div class="col-18">-</div>		
										<div class="col-8">-</div>
									<?php }?>
								
										 
										 
									</div>												
								</div>
								 
							<?php }?>
							
							<div class="col-sm-12 text-right">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
		  
		<div class="modal fade" id="myModalStock" role="dialog">
		<div class="modal-dialog modal-lg">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Stock Details </h4>
			</div>
			<div class="modal-body">
			  
				<div class="row" id="Tablelist">
					 
 				</div>  

				<div class="row">
				  <div class="col-sm-12" style="text-align:right;">
 					<button type="button" class="btn btn-danger" id="continueclose" data-dismiss="modal" >Close</button>
				  </div>
				</div> 
			</div> 
		  </div>
		</div>
	  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999; al " src="<?php echo $this->webroot; ?>img/482.gif" />
</div> 
<script>	
$(".pick_preview").click(function(){
 	
		if( $("#quantity").val() == ''){
  			 alert('Please enter quantity.');
		 
   		}else{ 
		 
			$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/expiredPicklistPreview',
					dataType	: 'json',
					type    	: 'POST',
					beforeSend  :  function() {$('.outerOpac').show()},
					data    	: { quantity:$("#quantity").val() },
					success 	: function( data  )
					{			
						   $(".preview").html(data.preview ) ;  
						   if(data.error != ''){
						    	alert(data.error);
								$(".preview").html(data.error ) ;  
						   } 
						    
						   $('.outerOpac').hide();   
					}                
				});	
			 
  	   } 
   
	
}); 

 function generatePickList(picklist_id){
	 
 		if(confirm('Are you sure want to generate PickList!')){            
		  
 		$.ajax({
				url     	: '<?php echo Router::url('/', true); ?>DynamicPicklist/generateExpiredPickList',
				dataType	: 'json',
				type    	: 'POST',
				beforeSend  : function() {$('.outerOpac').show()},
				data    	: {  picklist_name:picklist_id },
				success 	:	function( data  )
				{			
 					   $('.outerOpac').hide();   
					   if(data.status == 'ok'){
					  	 window.location.href = '<?php echo Router::url('/', true); ?>DynamicPicklist/index/<?php if(isset($this->request->pass[0])){ echo $this->request->pass[0]; } ?>';   
					   }    
				}                
			});	
		}
	 
 }
function viewStock(sku){	
	//$(".outerOpac").show();
	$('#myModalStock').modal('show');
	$("#myModalStock #Tablelist").html('<center><img style="text-align:center;" src="<?php echo $this->webroot; ?>img/ajax-loader.gif"/></center>');
	
	$.ajax({ 
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>Stocks/viewExpiredStock',
		type: "POST",				
		cache: false,			
		data : {sku:sku },
		success: function(data, textStatus, jqXHR)
		{	
			//$(".outerOpac").hide();				 
			$("#myModalStock #Tablelist").html(data.html);			
			$("#myModalStock").modal({keyboard: true});						
		}		
 	});  		 
}
function ChangeExpiry(id){	
	if($("#expiry_"+id).val() == ''){
		alert('Please enter expiry date.');
		return false;
	}
	else {
		$(".outerOpac").show();
		 //alert(id);alert($("#expiry_"+id).val());//1iGmRt
		$.ajax({ 
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Stocks/ChangeExpiry',
			type: "POST",				
			cache: false,			
			data : {id: id,expiry:$("#expiry_"+id).val(),old_expiry:$("#old_expiry_"+id).text() },
			success: function(data, textStatus, jqXHR)
			{	
				 $(".outerOpac").hide();	
				 alert('Expiry updated successfully.');			 
								
			}		
		});  
	}		 
}

</script>
