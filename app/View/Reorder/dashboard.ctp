<style>td, th {
    padding: 5px;
	border: 1px solid #fff;
}</style>		<!-- BEGIN RIGHTSIDE -->
        <div class="rightside bg-grey-100">
			<!-- BEGIN PAGE HEADING -->
            <div class="page-head bg-grey-100">
				<h1 class="page-title">ReOrder Dashboard</h1>
				<!--<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>-->
			</div>
			<!-- END PAGE HEADING -->
			<!-- 'getPaidOrders' , 'getUnPaidOrders' , 'getProcessedOrders' , 'getCancelledOrders' -->
            <div class="container-fluid">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="panel bg-teal-500">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="display-block font-size-18  color-teal-50 font-weight-800" title="No of Sku not ordered in last 30 days and having no stock - this will contain the list of the sku which has not been ordered in last 30 days and out of stock. We must look for out of Stock SKU and No new PO for that Sku."><i class="ion-plus-round"></i> Non Selling Sku  		
										</div>
										<div class="color-white font-size-11 font-roboto font-weight-500" data-toggle="counter" data-start="0" data-from="0" data-to="1230" data-speed="500" data-refresh-interval="10">
										
									<div class="col-md-3">Select Days<select name="not_selling" id="not_selling" class="form-control" onchange="dashboardData();">
												<option value="3">3</option>
												<option value="5">5</option>
												<option value="7">7</option>
												<option value="10">10</option>
												<option value="15">15</option>
												<option value="20">20</option>
												<option value="30">30</option>
												<option value="40">40</option>
											</select>	 
											</div>
									<div class="col-md-3 font-size-18"> <span id="not_selling_sku">
									<?php
												if( isset( $final_not_sale )  )
												{
													print count($final_not_sale);
												}else{
													echo '0';
												}
												 
											?></span>
											 SKUs
											
									</div>		
											
									<div class="col-md-6"> <button type="button" class="btn btn-info" onclick="notSelling()">Downnload Report</button> 
									 </div>
										
									<div class="col-md-12">		
										<small>(Select from dropdown to see the list of sku having stock but no sales)</small>
									</div>
									
									</div> 
										  
																			
									</div>
										
									</div>
									 
								</div>
								 
							</div> 
					</div><!-- /.panel -->
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="panel bg-red-400">
								<div class="panel-body padding-15-20">
									<div class="clearfix">
										<div class="pull-left">
																	
											<div class="display-block font-size-18  color-teal-50 font-weight-800"><i class="ion-plus-round"></i>Sold Out Sku Report</div>
	
	
											<div class="color-white font-size-11 font-roboto font-weight-500" data-toggle="counter" data-start="0" data-from="0" data-to="343" data-speed="500" data-refresh-interval="10">
											
											<div class="col-md-3">Select Days
											<select name="sold_out" id="sold_out" class="form-control" onchange="dashboardData();">
												<option value="3">3</option>
												<option value="7">7</option>
												<option value="10">10</option>
											</select>
											</div>
											<div class="col-md-3 font-size-18"> 
												<span id="sold_out_sku">0</span> SKUs
											</div>	
											<div class="col-md-6"><button type="button" class="btn btn-primary"  onclick="soldOut()">Download Report</button> </div>
											<div class="col-md-12">	 <small>(Select from dropdown no of days to see the list of sku having no stock)</small></div>
											
											</div>
											
																				
												
										</div>
										 
									</div>
									 
								</div>
							</div> 
					</div>	
				</div><!-- /.col -->
				 
 				<div class="row">	
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="row">
								
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="panel bg-blue-grey-400">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="display-block font-size-18 color-teal-50 font-weight-800"><i class="ion-plus-round"></i>Over Stock </div>
										<div class="color-white font-size-11 font-roboto font-weight-500" data-toggle="counter" data-start="0" data-from="0" data-to="5613" data-speed="500" data-refresh-interval="10">
										
										<form name="" method="get" action="<?php echo Router::url('/', true) ?>Reports/OverStock">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
 											Runrate of days
											<select name="runrate_days" id="runrate_days" class="form-control">
												<option value="7">7</option>
												<option value="15">15</option>
 											</select> </div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											Report of days<select name="report_days" id="report_days" class="form-control">
												<option value="15">15</option>
												<option value="30">30</option>
												<option value="45">45</option>
												<option value="60">60</option>  
												</select>
 										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-top:15px;">
											<button type="submit" class="btn btn-warning">Download Report</button> 
										</div> 
										<div class="col-md-12"><small>(Select from runrate and no of days to get list of over stocking sku)</small></div>
										</form>
  									</div>
									</div>  
								</div>
								 
							</div>
						</div> 
					</div><!-- /.col -->
					
					</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="row">
								
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="panel bg-blue-400">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
										<div class="pull-left">
																	
											<div class="display-block font-size-18  color-teal-50 font-weight-800"><i class="ion-plus-round"></i>Today's Recommendation for P.O.</div>
	
	
											<div class="color-white font-size-11 font-roboto font-weight-500" data-toggle="counter" data-start="0" data-from="0" data-to="343" data-speed="500" data-refresh-interval="10">
											
 											<div class="col-md-3 font-size-18"> 
												<span id="recomendation">0</span> SKUs
											</div>	
											<div class="col-md-6">
  												<a class="btn btn-warning" href="<?php echo Router::url('/', true) ?>Reorder/viewRecomendation">View Recomendation</a> 
											
											</div>
											<div class="col-md-12">	 <small>(This will be the list of the SKU required to be ordered today. The PO will be generated Supplier wise so the recommendations will be displayed in summary for each supplier and on details page it will show the complete list supplier wise.
From there after reviewing the PO can be generated
)</small></div>
											
											</div>
											
																				
												
										</div>
										 
									</div>
								 
							</div>
						</div><!-- /.panel -->
					</div><!-- /.col -->
					
					</div>
					</div>
				</div>
				<div class="row">	
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="row">
								
					 <!-- /.col -->
					
					</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="row">			
					 <!-- /.col -->
								
					</div>
					</div>
				</div>
				</div>
				</div>
				
				</div><!-- /.rightside -->
    </div><!-- /.wrapper -->
	  
									  
<script>

 dashboardData();
 setInterval("dashboardData()",600000);
 
function dashboardData() {
        try { 
               var url = '<?php echo Router::url("/", true) ?>Reports/dashboardData/';

                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: "json",
                    cache: false,
					beforeSend  : function() {
						$('#not_selling_sku').html('<img src="<?php echo Router::url('/', true); ?>img/sm_loading.gif"/>'); 
						$('#sold_out_sku').html('<img src="<?php echo Router::url('/', true); ?>img/sm_loading.gif"/>'); 
						$('#recomendation').html('<img src="<?php echo Router::url('/', true); ?>img/sm_loading.gif"/>'); 
 					},
                    data: { days: $('#not_selling').val(), sold_out_days:$("#sold_out option:selected").val()},
                    success: function (data) {
                         $('#not_selling_sku').html(data.skus);
						 $('#sold_out_sku').html(data.sold_out_sku);
						 $('#recomendation').html(data.recomendation_count);
                    },
                    error: function (xhr, status, error) {
                        alert(error);
                    }

                });
            
        } catch (e) {
            alert(e.toString());
        }
 }

function notSelling(){ 
	window.location ='<?php echo Router::url('/', true) ?>Reports/notSellingReport?not_selling='+$("#not_selling").val();
}
function notCheckin(){ 
	window.location ='<?php echo Router::url('/', true) ?>Reports/notCheckInReport?not_checkin='+$("#not_checkin").val();
}
function soldOut(){ 
	window.location ='<?php echo Router::url('/', true) ?>Reports/soldOutReport?sold_out='+$("#sold_out").val();
}
function Recomendation(){ 
	window.location ='<?php echo Router::url('/', true) ?>Reports/recomendationReport';
}

</script>
