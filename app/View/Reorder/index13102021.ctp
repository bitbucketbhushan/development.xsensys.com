<link rel="stylesheet" href="/css/bootstrap-multiselect.css" type="text/css"> 
<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.head div div div{ padding:0px ;  }
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.col-65 { width: 65%; float: left;}
	.col-60 { width: 60%; float: left;}
	.col-55 { width: 55%; float: left;}	 
	.editable-submit, .editable-cancel{padding: 3px 8px;}
	.icon-remove, .icon-ok {position: relative;top: 1px;display: inline-block;font-family: 'Glyphicons Halflings';-webkit-font-smoothing: antialiased;font-style: normal;font-weight: normal; line-height: 1;}
	.icon-ok:before {content: "\e013";}
	.icon-remove:before {    content: "\e014";}
	.paginate-responsive {    text-align: center;	}
	.paginate-responsive .total_pages {    float: left;    width: auto;    display: inline-block;    margin: 0px;    padding: 5px 0 0;	}
	.paginate-responsive .paginate {    float: left;    text-align: center;    display: inline-block;    width: 68%;}
	.paginate-responsive .pagination {    display: inline-block;    padding-left: 0;    margin: 10px 0;    border-radius: 4px;}
	.paginate-responsive .pagination > li {    display: inline;}
	.paginate-responsive .total_rows {    float: right;    width: auto;    display: inline-block;    margin: 0px;    padding: 5px 0 0;}
	.cqty{padding:2px;text-align:center;}

 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Stock Re-Ordering</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						
  						<div class="row">	
						  <div class="col-sm-12"> 						  
							<div class="col-sm-3" style="padding-left:0px; padding-right:0px;">	
							 <?php						
											print $this->form->create( 'upload', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/Reorder/UploadFile', 'type'=>'post','id'=>'upload' ) );
										?>
							 <div class="input-group">
								<span class="input-group-btn">
									<span class="btn btn-primary btn-file">
										Browse File <?php
													print $this->form->input( 'import_file', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'', 'required'=>false) );
												  ?>
									</span>
								</span>
								<input type="text" placeholder="No file selected" readonly="" class="form-control">
								<span class="input-group-addon no-padding-top no-padding-bottom">
								<div class="radio radio-theme min-height-auto no-margin no-padding">
									<?php
									echo $this->Form->button('Upload', array(
									'type' => 'submit',
									'escape' => true,
									'class'=>'btn bg-green-500 color-white btn-dark'
									 ));	
								?>
								</div>
								</span>
								</div>
								</form>
								<small style="display: inline-block;font-size: 75%; color:#FF0000;">(<a href="<?php echo Router::url('/', true) ?>sample_file/re_ordering_custom_qty.csv">Download Sample</a>)</small>
							</div>			
							 <div class="col-sm-2">					
								<select name="nature_of_product" id="nature_of_product" class="form-control" onchange="loadajax()">
									<option value="ambient">Ambient</option>
									<option value="fresh">Fresh</option>
									<option value="frozen">Frozen</option>
								</select>			 
							 
								<small style="display: inline-block;font-size: 75%; color:#FF0000;">(Select any nature.)</small>						
							</div>
							 
							<?php /*?> <div class="col-sm-2"> 					 
								<form name="case_sizeFrm" method="post">
									<select class="form-control" id="case_size" name="case_size" onChange="loadajax()">
 										<option value="qty">Qty</option>
										<option value="case_size">Case Size</option>
									</select>	
								</form>
												
								<small style="display: inline-block;font-size: 75%; color:#FF0000;">(Qty or Case Size)</small>						
							</div><?php */?>
							
							 <div class="col-20">				 
							<div id="supp" class="supplier_code">
							<?php
								 					
								echo '<select class="form-control" name="supplier_code" id="supplier_code" onChange="loadajax()" multiple="multiple">';
								  $i=0;
								  foreach($suppliers as $val){ 
									if($i<1){
										echo '<option value="'.$val['SupplierDetail']['supplier_code'].'" selected="selected">'. $val['SupplierDetail']['supplier_name'].'</option>';
									}else{
										echo '<option value="'.$val['SupplierDetail']['supplier_code'].'">'. $val['SupplierDetail']['supplier_name'].'</option>';
									}
									$i++;						
								  } 			
								echo '</select>';
						
						?>	</div>			
							</div>
							<!--
 							<div class="col-sm-2 fresh_quotation">
								<input id="fresh_quotation" type="checkbox" value="1" name="fresh_quotation">
								<label for="fresh_quotation">Generate Fresh Quotation</label>
							</div> -->
						  
							<div class="col-17">				
							  <!--Info buttons with dropdown menu-->
								<div class="btn-group btnopen">
									<button type="button" class="btn btn-info btn-sm generate_quote">Generate Quotation/PO</button>
									<button type="button" data-toggle="dropdown" class="btn btn-info btn-sm  dropdown-toggle"><span class="caret"></span></button>
									<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu" id="po">												 
									
									</ul>
								</div>					
							</div>	
							
							<div class="col-20 pull-right"><a href="javascript:void(0);"  class="btn btn-warning pull-right btn-sm" onclick="RefreshStock();">Refresh Virtual & InTransit Stock</a></div>
									 
						 </div> 			
						</div>
					</div>	
					
					<div class="panel-body no-padding-top bg-white">
				 
 						<div class="  table-responsive">	
			
						<table id="grid" class="table pog table-striped">
						  <thead>
							
							<tr>
								<th width="17%">SKU
									<div class="dtq">-</div>
								</th>					
								
								<th>					
								<span onClick="SortBy(this);" class="sort-by" data-name="sales_qty" data-value="ASC">
								<i class="glyphicon glyphicon-sort-by-attributes"></i>Sale Of </span> 
								
 								<select name="avr_sale_w" id="avr_sale_w" onChange="loadajax()">
								<?php									 						
									for ($j = 1; $j <= 60; $j++)
									{						
										echo '<option value="'.$j.'">'.$j.'</option>';
									}
									?>
								</select> Days
								<div class="dtq">-</div>					
								</th>	
							
								<th>					
								<span onClick="SortBy(this);" class="sort-by" data-name="sales_qty" data-value="ASC">
								<i class="glyphicon glyphicon-sort-by-attributes"></i>Weekly Sales Average</span> 
								
 								<select name="weekly_sale" id="weekly_sale" onChange="loadajax()">
								<?php									 						
									for ($j = 1; $j <= 7; $j++)
									{						
										echo '<option value="'.$j.'">'.$j.'</option>';
									}
									?>
								</select>  
								 
								<div class="dtq">-</div>					
								</th>
								
 								<th>					
								Stock Required For
								<select name="stock_req" id="stock_req" onChange="loadajax()">
									 <?php 
									for ($i = 1; $i <= 30; $i++)
									{					
										$sel ='';
										if($i == 10){ $sel = 'selected="selected"'; }							
										echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
									}
									?>				
								</select> Days
								<div class="dtq">-</div>					
								</th>
								
								<th>					
								Safety Stock
								<div class="dtq">-</div>					
								</th>
								<th>					
								Total Stock Req
								<div class="dtq">-</div>					
								</th>
								<th>					
								Virtual Stock
 								<div><input type="checkbox" value="1" id="zero_vs" onClick="loadajax()" />&nbsp;<small style="color:#000099; font-size:10px;">0_Stock</small></div>	
								<div class="dtq">-</div>			
								</th>
								<th>					
								Quotation Stock In Transit
 								<div><input type="checkbox" value="1" id="quot_in_transit_excl" onClick="loadajax()" />&nbsp;<small style="color:#000099; font-size:10px;">Exclude</small></div>
								<div class="dtq">-</div>							
								</th>	
								<th>					
								Stock In Transit
 								<div><input type="checkbox" value="1" id="in_transit_excl" onClick="loadajax()" />&nbsp;<small style="color:#000099; font-size:10px;">Exclude</small></div>
								<div class="dtq">-</div>							
								</th>	
								
								<th>					
								
								<span onClick="SortBy(this);" class="sort-by" data-name="po_qty" data-value="ASC">
								<i class="glyphicon glyphicon-sort-by-attributes"></i>Required Qty For P.O.</span>
								
								<div class="dtq">-</div>					
								</th>	
							</tr>
							
							 
							<tr>							
								<th colspan="9"><input type="search" autocomplete="off" class="form-control" placeholder="Search By Master SKU" id="master_sku" name="sku"></th>															
								<th class="right"><button id="Filter" type="submit" title="Filter" class="button btn btn-info" onClick="resetFilter();">Reset Filter</button></th>				
							</tr>
								
						  </thead>
						  <tbody id="results">				
						  </tbody>
						  <div class="paginate-responsive"> </div>  		
						</table>
			 
          </div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
		  
		 
		  
<div class="outerOpac" id="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999; al " src="<?php echo $this->webroot; ?>img/482.gif" />
</div> 
<script>	
  $(document).ready(function() {
       
		  $('#supplier_code').multiselect({
			numberDisplayed: 1, 
			includeSelectAllOption: true,
			nonSelectedText :'Select Suppliers'
      	 // enableFiltering:true 
		});	
		
  });
  
function RefreshStock(){ 
		$(".outerOpac").show();
		$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Reorder/virtualInTransitStock',
			type: "POST",
			data : {},
			success: function(data, textStatus, jqXHR)
			{		
				$(".outerOpac").hide();
				$(".msg").html('<div class="alert alert-success">'+data.msg+'</div>');	
			}		
		});	
			
}
  
  	
function updateQty(sku){ 

			$(".outerOpac").show();
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>Reorder/updateRequiredStock',
				type: "POST",
				data : {stock_req:$("#"+sku).val(),sku:sku},
				success: function(data, textStatus, jqXHR)
				{		
					$(".outerOpac").hide();
 				}		
			});	
			
}
  
$(".generate_quote").click(function(){	

 	var suppliers  = $('.supplier_code li.active input[type="checkbox"]').map(function () {if(this.value!='multiselect-all'){return this.value; }}).get().join(",");
		
	if($("#nature_of_product").val() == '' &&  suppliers == ''){
		alert('Please select Nature of Product and Suppliers.');
	}
	else if($("#nature_of_product").val() == ''){
		alert('Please select Nature of Product.');
	}
	else if(suppliers == ''){
		alert('Please select Suppliers.');
	}
	else{
	
		if (confirm('Are you sure to Generate Quotation?')) {
		
			$(".outerOpac").show();
			$.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>Reorder/getQuotation',
				type: "POST",
				data : {action:'generate_quote',stock_req:$("#stock_req option:selected").val(),avr_sale_w:$("#avr_sale_w").val(),avr_sale_m:$("#avr_sale_m").val(),case_size:$("#case_size option:selected").val(),supplier_code:suppliers,nature_of_product:$("#nature_of_product").val(),fresh_quotation:$("input[name=fresh_quotation]:checked").val()},
				success: function(data, textStatus, jqXHR)
				{		
					 $(".outerOpac").hide();
					$(".btnopen").addClass('open');					
					//$("#message").html(data.msg);	
					$("#po").html('');
					$("#po").append(data.file_link);					
					//$('#message').delay(10000).fadeOut('slow');							
				}		
			});	
		}
	}
});
function resetFilter(){
	$("#master_sku").val('');			
	loadajax(this);		
}
$("#master_sku").on( "keydown", function(event) {
	 if(event.which == 13) {
		 loadajax(this);	
	}
}); 
function loadajax(val){   
	var page = $(val).attr("data-page"); 
	var name = $(val).attr("data-name"); 
	var order = $(val).attr("order-by"); 
	var value = $(val).attr("data-value"); 
	//$(".hid").hide();	
	$(".outerOpac").show();
 	var suppliers  = $('.supplier_code li.active input[type="checkbox"]').map(function () {if(this.value!='multiselect-all'){return this.value; }}).get().join(",");
	$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>Reorder/getQuotation',
			type: "POST",
			data : {page:page,name:name,orderby:order,value:value, nature_of_product:$("#nature_of_product").val(),previous_quotation:$("#previous_quotation option:selected").val(),master_sku:$("#master_sku").val(),stock_req:$("#stock_req option:selected").val(),avr_sale_w:$("#avr_sale_w").val(),avr_sale_m:$("#avr_sale_m").val(),case_size:$("#case_size option:selected").val(),supplier_code:suppliers, daterange:$("#daterange").val(), fresh_quotation:$("input[name=fresh_quotation]:checked").val(),weekly_sale:$("#weekly_sale option:selected").val(),zero_vs:$("#zero_vs:checked").val(),quot_in_transit_excl:$("#quot_in_transit_excl:checked").val(),in_transit_excl:$("#in_transit_excl:checked").val()},
			success: function(txt, textStatus, jqXHR)
			{	
			    $(".outerOpac").hide();
				$('#message').show();		
				$(".paginate-responsive").html(txt.paginate);														
				$("#results").html(txt.data);
				if(txt.file_link && (txt.file_link).length > 0 ){
					$(".hid").show();	
					$('#down').html('');					
					$("#down").append(txt.file_link);	
					$("#avr_sale_w").val(txt.avr_sale_w);
					$("#stock_req").val(txt.stock_req);	
				}									
				//$('#message').delay(5000).fadeOut('slow');
			},
			complete: function(textStatus)
			{
				$(".outerOpac").hide();	
			}			
		});
	
}
 
 
$(document).ready(function() {		
	loadajax();
	
	$(".paginate-responsive").on( "click", ".pagination a", function (e){
			e.preventDefault();			
			loadajax(this);			
		});
	
});
</script>
