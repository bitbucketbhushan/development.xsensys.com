<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px; }
	.highlight{border:1px #FF0000 solid;background:#FF3300;}
	.notequal{border:1px #993300 solid; background:#CCFF33;}
	.fl-pad{float:left; width:70px;}
	.pl{padding-left:5px !important;width:110px; text-align:center;}
	 body div.sku.bt{border-bottom:2px solid #bbb;border-top:2px solid #bbb;} 
	 .bgc{background-color:#66FFFF;}
	 .col-4{width:4%; float:left;}
	 .col-50{width:50%;}
	 .ub{margin-top:5px;}
 </style>
  
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>				
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>				
				<h4 style="float:left;"> Shipment Id : <?php echo  $this->params['pass'][0];?></h4>
				<div class="col-lg-3" style="float:right;">
					<div class="col-lg-6">
					
					</div>
					<div class="col-lg-6">
					<a href="<?php echo Router::url('/', true) ?>FbaShipments" class="btn btn-primary btn-sm" style="float:right;">Back</a>
					</div>					
				</div>
				<br /><br />
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							 
						
						<div class="body" id="results">
								
						<?php				
							if(count($shipment_boxes) > 0){
                            
							App::Import('Controller', 'FbaShipmentBoxes'); 
							$obj = new FbaShipmentBoxesController;	
							
                            foreach($shipment_boxes as $box){
									
								$boxItems = $obj->getShipmentBoxItems($box['FbaShipmentBox']['id']); 
								$id = $box['FbaShipmentBox']['id'];
								
								print $this->form->create( 'Shipment', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '#', 'type'=>'post','id'=>'shipment_frm_'.$id ) );
								
								echo  '<input type="hidden" value="'.$id.'"  name="box_id">';
								echo '<div class="row sku">';
								echo '<div class="col-sm-12">'; 											
									echo '<div class="col-sm-2">Box-'.$box['FbaShipmentBox']['box_number'].'</div>';
									echo '<div class="col-sm-2">Box Height:<input type="text" value="'.$box['FbaShipmentBox']['box_height'].'" id="height_'.$id.'" name="height" class="form-control"></div>';
									echo '<div class="col-sm-2">Box Width:<input type="text" value="'.$box['FbaShipmentBox']['box_width'].'" id="width_'.$id.'" name="width" class="form-control"></div>';
									echo '<div class="col-sm-2">Box Length:<input type="text" value="'.$box['FbaShipmentBox']['box_length'].'" id="length_'.$id.'" name="length" class="form-control"></div>';
									echo '<div class="col-sm-2">Box Weight:<input type="text" value="'.$box['FbaShipmentBox']['box_weight'].'" id="weight_'.$id.'" name="weight" class="form-control"></div>';
									
									echo '<div class="col-sm-2 text-right"><button type="button" class="btn btn-info"
									onclick=shippingBoxes(\'shipment_frm_'.$id.'\')>Update Box</button> <br>';
									
									echo' <button type="button" class="btn btn-warning ub" id="print_box_label_'.$box['FbaShipmentBox']['id'].'" onClick="printBoxLabel('.$box['FbaShipmentBox']['id'].');"><i class="fa fa-print"></i>&nbsp;PrintBoxLabel</button><br>';
									
									echo' <button type="button" class="btn btn-success ub" id="print_box_detail_'.$box['FbaShipmentBox']['id'].'" onClick="printBoxDetails('.$box['FbaShipmentBox']['id'].');"><i class="fa fa-print"></i>&nbsp;PrintBoxDetails</button></div> '; 
									 
								echo '</div>';
								
								foreach($boxItems as $bitem){
									$item_id = $bitem['FbaShipmentBoxItem']['id'];
									echo '<div class="col-sm-12">'; 
										echo '<div class="col-sm-2">&nbsp;</div>'; 
										echo '<div class="col-sm-10 bgc">'; 
											echo '<div class="col-sm-2">'.$bitem['FbaShipmentBoxItem']['fnsku'].'</div>';
											echo '<div class="col-sm-2">'.$bitem['FbaShipmentBoxItem']['merchant_sku'].'</div>';
											echo '<div class="col-4">Qty:</div><div class="col-sm-2"><input type="text" value="'.$bitem['FbaShipmentBoxItem']['qty'].'" id="qty_'.$id.'_'.$item_id.'" name="qty['.$item_id.']" class="form-control col-50"></div>';
										echo '</div>';
									echo '</div>';
								}
								echo '</div>';
								
								echo '</form>';
									
							 }
						}else{
							echo '<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>';
						}	  
						 ?> 
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script language="javascript">	

function shippingBoxes(frm_id){
$(".outerOpac").show();
var formData = new FormData($('#'+frm_id)[0]);
	$.ajax({
			dataType: 'json',
			url : '<?php echo Router::url('/', true) ?>FbaShipmentBoxes/UpdateBox',
			type: "POST",				
			cache: false,			
			type: "POST",
			async: false,		
			cache: false,
			contentType: false,
			processData: false,
			data : formData,
			success: function(data, textStatus, jqXHR)
			{	 
				$(".outerOpac").hide();
				console.log(data.msg);	
				if(data.error){
					alert(data.msg);
				}											
			} 
		}); 	
}

 function printBoxLabel(v){	
	  
	$("#print_box_label_"+v+" i").removeClass('fa-print');	
	$("#print_box_label_"+v+" i").addClass('fa-spin fa-refresh');	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaExportConsole/printBoxLabel',
		type: "POST",				
		cache: false,			
		data : {id:v},
		success: function(data, textStatus, jqXHR)
		{	
			$("#print_box_label_"+v+" i").removeClass('fa-spin fa-refresh');	
			$("#print_box_label_"+v+" i").addClass('fa-print');	
			console.log(data.msg);		
			if(data.error){
				alert(data.msg);
			} 								
		}		
 	});  		 
}
 function printBoxDetails(v){	
	  
	$("#print_box_detail_"+v+" i").removeClass('fa-print');	
	$("#print_box_detail_"+v+" i").addClass('fa-spin fa-refresh');	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaExportConsole/printBoxDetails',
		type: "POST",				
		cache: false,			
		data : {id:v},
		success: function(data, textStatus, jqXHR)
		{	
			$("#print_box_detail_"+v+" i").removeClass('fa-spin fa-refresh');	
			$("#print_box_detail_"+v+" i").addClass('fa-print');	
			console.log(data.msg);		
			if(data.error){
				alert(data.msg);
			} 								
		}		
 	});  		 
}

</script>
