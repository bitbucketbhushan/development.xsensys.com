<div class="col-lg-12">
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">SKU Age</h1>
    </div>
	<div class="col-lg-12">
		<div style="float: right;margin: 10px;"><a class="btn bg-green-500 color-white btn-dark padding-left-10 padding-right-10" onClick ="generateSkuAge();" href="javascript:void(0);" data-toggle="modal">Generate CSV</a></div>
	</div>
<div class="set_result">
<table class="table table-bordered table-striped dataTable" >
	<tr>
		<th width=15%>SKU</th>
		<th width=5%>Stock</th>
		<th width=5%>Price</th>
		<th width=15%>Po</th>
		<th width=15%>ChackIn Date</th>
		<th width=15%>< 30 days</th>
		<th width=15%>> 30 days</th>
		<th width=15%>Purchase User</th>
		<th width=15%>Status</th>
		
	</tr>		
		 <?php $i = 1;foreach( $skuage as $agek => $agev) { 
		 if( $agev['status'] == 'Sleep'){ $bccolor = '#FF9E9E'; } else { $bccolor = '#A2FCA6;'; }
		 ?>
		 <tr style="background-color: <?php echo $bccolor; ?>"; >
		 	<td><?php echo $agek; ?></td>
			<td><b><?php echo $agev['stock']; ?></b></td>
			<td><b><?php echo $agev['price']; ?></b></td>
			<td><?php echo (isset($agev['po']) && $agev['po'] != '') ? implode('<br>',$agev['po']) : '--'; ?></td>
			<td><?php echo date('d-m-Y',strtotime($agev['check_in_date'])); ?></td>
			<td><?php echo (isset($agev['age']) && $agev['age'] < '30') ? $agev['age'] : '-'; ?></td>
			<td><?php echo (isset($agev['age']) && $agev['age'] > '30') ? $agev['age'] : '-'; ?></td>
			<td><?php echo $agev['name']; ?></td>
			<td><?php echo $agev['status']; ?></td>
		</tr>
		<?php $i++; } ?>		 	
</table>
<div class="" style="margin:0 auto; width:350px; margin-bottom: 50px;">
	 <ul class="pagination">
	  <?php
		   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
		   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
		   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
	  ?>
	 </ul>
</div>
</div>
</div>
</div>
<div class="showPopupForRecivePo"></div>
<div class="outerOpac" style="display:none">
	<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; "></span>
	<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>
function generateSkuAge()
{
	window.location = "<?php echo Router::url('/', true) ?>Exportorders/generateSkuAgeFile";
}
</script>
