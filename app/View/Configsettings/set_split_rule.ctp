<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Split Rule</h1>
         <?php print $this->Session->flash();  ?>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="form-horizontal panel-body padding-bottom-40 padding-top-40">
									<div class="form-group">
										 <label class="col-sm-3 control-label">Max</label>
										 <div class="col-sm-7">                                               
											<?php
												print $this->form->create( 'SplitRule', array( 'class'=>'form-horizontal', 'type'=>'post','url'=>'/Setting/Configuration/Split/Rule/_Today_/=ERSQUV','id'=>'saveuser','enctype'=>'multipart/form-data' ));
												print $this->form->input( 'SplitRule.max_value', array( 'type'=>'text','class'=>'form-control orderid','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
												print $this->form->input( 'SplitRule.id', array( 'type'=>'hidden','class'=>'form-control orderid','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
											?>  
										 </div>
                                   </div>
                                   <div class="form-group">
										 <label class="col-sm-3 control-label">Min</label>
										 <div class="col-sm-7">   
											 <?php 
												if($this->session->read('Auth.User.id') > 0)
												{	
													$getCommonHelper = $this->Common->getUserDataAfterLogin( $this->session->read('Auth.User.id') );
													$getName = $this->Common->getFirstLastName( $this->session->read('Auth.User.id') );
												} 
											
												$userImage = $getCommonHelper['User']['user_image'];
												$userName = $getCommonHelper['User']['first_name'].' '.$getCommonHelper['User']['last_name'];
											
												print $this->form->input( 'SplitRule.min_value', array( 'type'=>'text','class'=>'form-control orderid','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
												print $this->form->input( 'SplitRule.user_name', array( 'type'=>'hidden', 'value' => $userName ) );
											?>  
										 </div>
                                   </div>
                                     
                                    <div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100">                                                                            
                                    <?php
										echo $this->Form->button('Save', array(
											'type' => 'submit',
											'escape' => true,
											'class'=>'btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
											 ));	
									?> 
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>
