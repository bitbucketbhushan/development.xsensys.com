<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">
		<?php
			$getOperatorInformation = $this->Session->read('Auth.User') ;	
			date_default_timezone_set('Europe/Jersey');			
		?>        
    <h1 class="page-title">New Docket <?php echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]'; ?></h1>
	<div class="submenu">
				<div class="navbar-header">
					<ul class="nav navbar-nav pull-left">
						<li>
							<div class="btn-group manifest_button">
							  <button type="button" id="GM" style="display:none;" class="manifest_show cut_off_manifest_dhl btn btn-success no-margin">Generate Manifest</button>
							</div>
							 
						</li>
					</ul>
				</div>
			</div>
	 </div>
    <div class="container-fluid">
        <div class="row">
           <div class="col-lg-12">
              <div class="panel">                            
                  <div class="row">
                      <div class="col-lg-12">
						<div class="panel-body padding-bottom-40 padding-top-10">
						   <a href="javascript:void(0);" class="espipper_correoc_detail btn bg-red-500 color-white btn-dark col-sm-12 margin-bottom-10">Generate eShipper</a> 
						   <table class="inputBorderel table table-bordered table-hover">
							  <tbody>
								 <tr>
									<th rowspan="2" style="background-color: #d1c5c5;">Zone1</th>
									<th colspan="2" style="background-color: #55c9ef;" >Detail</th>
								 </tr>
								 <tr>
									<th style="background-color: #55c9ef;">Count</th>
									<th style="background-color: #55c9ef;">Weight</th>
								 </tr>
								  
								<tr>
									<td style="background-color: #d1c5c5;"><label accesskey="A" >DHL</label></td>
									<td style="background-color: #a8e5f9;" ><input type="text" class="form-control normal" value="" id="qty" name="qty" size="8"></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal" value="" id="weight"  name="weight" size="8" ></td>
								 </tr>
								   
								</tbody>
							</table>
							   
								
						  </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>
	$('body').on('click','.espipper_correoc_detail', function(){
				var totalCount = 0;
				var totalWeight = 0;
				
				$.ajax({
				url            : 	'/Cronjobs/setInfoEshipperDHL',
				dataType		: 'json',
				type           : 	'POST',
				beforeSend	   :   function(){
												$('.outerOpac').show();
											 },
				success        : 	function( txt, textStatus, jqXHR )
								    {
										   $('#qty').val( txt.count );
										   $('#weight').val( txt.totla_weight );
										   $('.outerOpac').hide();
										    $('#GM').show();
								    }
				})
		});
		
	$('body').on('click','.cut_off_manifest_dhl', function(){
	
			var serviceProvider = 'DHL';
			$.ajax({
				url            : 	'/Cronjobs/createCutOffListDHL',
				type           : 	'POST',
				data           : 	{ serviceProvider : serviceProvider },
				success        : 	function( msgArray )
								    {
										 if( msgArray != 'blank' )
										   {
												window.open(msgArray,'_blank' );
												location.reload();
										   }
										   else
										   {
											   swal("No orders found for manifest!" , "" , "error");
										   }
								    }
				})
	});

</script>


