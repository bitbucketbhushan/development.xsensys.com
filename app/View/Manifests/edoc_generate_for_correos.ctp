<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">
		<?php
			$getOperatorInformation = $this->Session->read('Auth.User') ;	
			date_default_timezone_set('Europe/Jersey');			
		?>        
    <h1 class="page-title">New Docket <?php echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]'; ?></h1>
	<div class="submenu">
				<div class="navbar-header">
					<ul class="nav navbar-nav pull-left">
						<li>
							<div class="btn-group manifest_button">
							  <button type="button" id="PostNL" style="display:none;" class="manifest_show cut_off_manifest_correos btn btn-success no-margin">Generate Manifest</button>
							</div>
							<div class="btn-group manifest_button">
							  <button type="button" id="PostNL" class="delivery_note_correos btn btn-success no-margin">Delivery Note</button>
							</div>
						</li>
					</ul>
				</div>
			</div>
	 </div>
    <div class="container-fluid">
        <div class="row">
           <div class="col-lg-12">
              <div class="panel">                            
                  <div class="row">
                      <div class="col-lg-12">
						<div class="panel-body padding-bottom-40 padding-top-10">
						   <a href="javascript:void(0);" class="espipper_correoc_detail btn bg-red-500 color-white btn-dark col-sm-12 margin-bottom-10">Generate eShipper</a> 
						   <table class="inputBorderel table table-bordered table-hover">
							  <tbody>
								 <tr>
									<th rowspan="2" style="background-color: #d1c5c5;">Zone1</th>
									<th colspan="2" style="background-color: #55c9ef;" >Detail</th>
								 </tr>
								 <tr>
									<th style="background-color: #55c9ef;">Quantity</th>
									<th style="background-color: #55c9ef;">Weight</th>
								 </tr>
								  <?php $i = 0; foreach( $zones as $zone ) { ?>
								<tr>
									<td style="background-color: #d1c5c5;"><label accesskey="A" ><?php echo $zonesName[$i]; ?></label></td>
									<td style="background-color: #a8e5f9;" ><input type="text" class="form-control normal <?php echo str_replace(' ','_',$zone).'_qty'; ?>" value="" size="8"></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo str_replace(' ','_',$zone).'_weight'; ?>" value="" size="8" ></td>
								 </tr>
								   <?php $i++; } ?>
								</tbody>
							</table>
							   <div class="form-group">
									<label class="control-label col-lg-1 col-lg-offset-5" for="username">Count</label>                                        
									<div class="col-lg-2">                                            
										<input type="text" id="totalItems" class="form-control" name="data[Role][role_name]">                                       
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-1 col-lg-offset-5 padding-top-10" for="username">Weight</label>                                        
									<div class="col-lg-2 padding-top-10">                                            
										<input type="text" id="totalWeight" class="form-control" name="data[Role][role_name]">                                        
									</div>
							   </div>
						  </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>
	$('body').on('click','.espipper_correoc_detail', function(){
				var totalCount = 0;
				var totalWeight = 0;
				
				$.ajax({
				url            : 	'/Cronjobs/setInfoEshipperCorreos',
				datatype	   :	'json',
				type           : 	'POST',
				data           : 	{ },
				global		   : 	false,
				async          :	false,
				beforeSend	   :   function(){
												$('.outerOpac').show();
											 },
				success        : 	function( msgArray )
								    {
									 
									 	
											 var json = JSON.parse(msgArray);
											 var recordCount = json.data.length;
											
											 for( var j = 0; j < recordCount; j++)
												 {
													$('.'+json.data[j].postel_service_data.sevice_code+'_weight').val( json.data[j].postel_service_data.totla_weight );
													$('.'+json.data[j].postel_service_data.sevice_code+'_qty').val( json.data[j].postel_service_data.count );
													totalCount = parseInt(totalCount) + parseInt( json.data[j].postel_service_data.count );
													totalWeight = parseFloat(totalWeight) + parseFloat( json.data[j].postel_service_data.totla_weight );
												 }
											 $('#totalItems').val( totalCount );
											 $('#totalWeight').val( parseFloat(totalWeight).toFixed(3) );
											 $('.outerOpac').hide();
										
								    }
				})
		});
		
	$('body').on('click','.cut_off_manifest_correos', function(){
	
			var serviceProvider = 'Correos';
			$.ajax({
				url            : 	'/Cronjobs/createCutOffListCorreos',
				type           : 	'POST',
				data           : 	{ serviceProvider : serviceProvider },
				success        : 	function( msgArray )
								    {
										 if( msgArray != 'blank' )
										   {
												window.open(msgArray,'_blank' );
												location.reload();
										   }
										   else
										   {
											   swal("No orders found for manifest!" , "" , "error");
										   }
								    }
				})
	});

	$('body').on('click','.delivery_note_correos', function(){
			$.ajax({
					url            : 	'/Cronjobs/deliveryNoteCorreos',
					type           : 	'POST',
					data           : 	{},
					beforeSend	   :   function(){
												$('.outerOpac').show();
											 },
					success        : 	function( msgArray )
										{
											$('.manifest_show').show();
											$('.outerOpac').hide();
											window.open(msgArray,'_blank' );
										}
					})
	
	});
</script>


