<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">
		<?php
			$getOperatorInformation = $this->Session->read('Auth.User') ;	
			date_default_timezone_set('Europe/Jersey');			
		?>        
        <h1 class="page-title">Generate Manifest <?php echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]'; ?></h1>
        	<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div>
		
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">

                             <div class="panel-body padding-bottom-40 padding-top-10"> 
 									<a href="<?php echo  Router::url('/',true);?>Jerseypost/createManifest" class="btn btn-info" role="button">Create Manifest</a> 
 							</div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>

</script>

