<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">
		<?php
			$getOperatorInformation = $this->Session->read('Auth.User') ;	
			date_default_timezone_set('Europe/Jersey');			
		?>        
        <h1 class="page-title">New Docket <?php echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]'; ?></h1>
        <div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
						<li>
							<button type="button" id="Belgium Post" class="cut_off_manifest_back_up btn btn-success no-margin">Back Up</button>
						</li>
<li>
										<div class="btn-group manifest_button" style="display:none">
										
										  <button type="button" id="PostNL" class="cut_off_manifest btn btn-success no-margin">Generate Manifest</button>
										  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										  </button>
										  <?php if(count($files) > 0 ) { ?>
										  <ul class="dropdown-menu scrollable-menu" role="menu">
												<?php 
													$manifestPath = Router::url('/', true).'/img/cut_off/'.$folderName.'/';
													foreach( $files as $file )
													{
														$fileName = explode('-', $file) ;
														//File name
														$fname= $fileName[0];
														//get date
														$dateValue = explode( '_', $fileName[1] );
														$specificDate = $dateValue[0];
														//get time specific
														$minute = explode('.', $dateValue[2]) ;
														$timeValue = $dateValue[1] .':' . $minute[0];
												?>
												<?php 
													if( $fname == "PostNL" )
													{
												?>
															<li><a href="<?php echo $manifestPath.$file; ?>" target ="_blank" > <?php print $fname .'('.$timeValue.')' ; ?> </a></li>
												<?php
													} }
												?>
											
										  </ul>
										  <?php }?>
										</div>
							</li>

						
						</ul>
					</div>
				</div>
        
		
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div>
		
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">

                             <div class="panel-body padding-bottom-40 padding-top-10">
							 
							   
							<?php								
								echo $this->Form->button(
									'Generate eShipper', 
									array(
										'formaction' => Router::url(
											array('controller' => '/JijGroup/System/JP/eDoc/Create')
										 ),
										'escape' => true,
										'class'=>'btn cut_off bg-red-400 color-white btn-dark col-sm-12 margin-bottom-10',
										'id' => 'PostNL',
										'type' => 'submit'	
									)
								);
								echo $this->form->end();	
							?>
							 

								<form action="/BpiShippingCorner/borderel12handling.do" method="post" name="ebpi.Borderel12HandlingForm">
								   <div><input type="hidden" value="3b5919020fb2511ff406c26a704779f8" name="org.apache.struts.taglib.html.TOKEN"></div>
								   <!-- the action to use when the user presses enter -->
								   <input type="hidden" value="VolumailSortedSubmitGroups12Save" name="method">
								   <span class="fieldLabel02Std">¹This column should contain the total weight (pieces × weight per piece) for the zone or destination.</span>
								   <table class="inputBorderel table table-bordered table-hover">
									  <tbody>
										 <tr>
											<th rowspan="2" style="background-color: #d1c5c5;">Zone1</th>
											<th colspan="2" style="background-color: #55c9ef;" >G</th>
											<th colspan="2" style="background-color: #55c9ef;" >E</th>
											<th colspan="2" style="background-color: #55c9ef;" >Total</th>
											
											<th colspan="2" style="background-color: #f9f8a8;" >G REG</th>
											<th colspan="2" style="background-color: #f9f8a8;" >E REG</th>
											<th colspan="2" style="background-color: #f9f8a8;" >Total</th>
										 </tr>
										 <tr>
											<th style="background-color: #55c9ef;">PCE</th>
											<th style="background-color: #55c9ef;">PCE×KG¹</th>
											<th style="background-color: #55c9ef;">PCE</th>
											<th style="background-color: #55c9ef;">PCE×KG¹</th>
											<th style="background-color: #55c9ef;">Qty</th>
											<th style="background-color: #55c9ef;">Weight</th>
											
											<th style="background-color: #f9f8a8;">PCE</th>
											<th style="background-color: #f9f8a8;">PCE×KG¹</th>
											<th style="background-color: #f9f8a8;">PCE</th>
											<th style="background-color: #f9f8a8;">PCE×KG¹</th>
											<th style="background-color: #f9f8a8;">Qty</th>
											<th style="background-color: #f9f8a8;">Weight</th>
										 </tr>
										  <?php foreach( $locationDetail as $locationName ) { 
												$country	=	( $locationName['Location']['county_name'] == 'United Kingdom' )  ? 'Great Britain' : $locationName['Location']['county_name'];
												$countryID	=	( $country == 'Great Britain' || $country == 'Rest Of EU')  ? str_replace( ' ', '', $country ) : $locationName['Location']['county_name'];
										  ?>
								<tr>
									<td style="background-color: #d1c5c5;"><label accesskey="A" ><?php echo $country; ?></label></td>
									<td style="background-color: #a8e5f9;" ><input type="text" class="form-control normal <?php echo $countryID; ?>PCEG" value="" size="8"></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>KGSG" value="" size="8" ></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>PCEEBus" value="" size="8" ></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>KGSEBus" value="" size="8" ></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>QTYBus" value="" size="8" ></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>WEIGHTBus" value="" size="8" ></td>
									
									<td style="background-color: #f9f8ca;"><input type="text" class="form-control normal <?php echo $countryID; ?>PCEGREG" value="" size="8" ></td>
									<td style="background-color: #f9f8ca;"><input type="text" class="form-control normal <?php echo $countryID; ?>KGSGREG" value="" size="8" ></td>
									<td style="background-color: #f9f8ca;"><input type="text" class="form-control normal <?php echo $countryID; ?>PCEEREG" value="" size="8" ></td>
									<td style="background-color: #f9f8ca;"><input type="text" class="form-control normal <?php echo $countryID; ?>KGSEREG" value="" size="8" ></td>
									<td style="background-color: #f9f8ca;"><input type="text" class="form-control normal <?php echo $countryID; ?>QTYREG" value="" size="8" ></td>
									<td style="background-color: #f9f8ca;"><input type="text" class="form-control normal <?php echo $countryID; ?>WEIGHTREG" value="" size="8" ></td>
								 </tr>
										 <?php } ?>
									  </tbody>
								   </table>
								   
							   <div class="form-group">
									<label class="control-label col-lg-1 col-lg-offset-5" for="username">Count</label>                                        
									<div class="col-lg-2">                                            
										<input type="text" id="totalItems" class="form-control" name="data[Role][role_name]">                                       
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-1 col-lg-offset-5 padding-top-10" for="username">Weight</label>                                        
									<div class="col-lg-2 padding-top-10">                                            
										<input type="text" id="totalWeight" class="form-control" name="data[Role][role_name]">                                        
									</div>
							   </div>
							   
								</form>
								
								</div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>
	$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "eShipper is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
			
		 $( 'body' ).on( 'click', '.cut_off', function()
		 {
			
			console.log( "****************************" );                    
			console.log( "eShipper is fetching all information" );
			console.log( "****************************" );	
			
			var serviceProvider = $(this).attr('id');
			// Data fetching from this			
			var getEshipper = $.ajax(
			{
				'url'            : '/Cronjobs/setInfoEshipperPostnl',
				'type'           : 'POST',
				'data'           : { serviceProvider : serviceProvider },
				global: false,
				async:false,
				'success'        : function( msgArray )
								{	
								var json = JSON.parse(msgArray);
								var getData = json.data;
								var newCounter = 0;
								var newtotalWeight = 0;
								
									 var getEshipper = $.ajax(
									 {
											 'url'            : '/Cronjobs/getExactWeightPostNL',
											 'type'           : 'POST',
											 'data'           : {  },
											 global: false,
											 async:false,
											 'success'        : function( msgArray )
																 {
																		var json = JSON.parse(msgArray);
																		var getData = json.data;
																		var max2 = 0;
																		var getTotal = 0;
																		var getTotalWeight = 0;
																		 var groceWeight = 0;						
																	for (var i = 0, max = getData.length; i < max; i++)
																		 {
																			
																			 var delcountry 	= getData[i].delcountry;
																			 var service		= getData[i].postal_provider_code.length;
																			 var totalWeight 	= getData[i].totalWeight;
																			 var newCounter 	= getData[i].counter;
																			
																			 var counterreg = 0;
																			 var counter = 0;
																			 var weight = 0;
																			 var weightreg = 0;
																			 var countTotal = 0;
																			 var totlaWeightreg = 0;
																			 var totlaWeightNonreg = 0;
																			 var totalCountReg = 0;
																			 var totalCountNonReg = 0;
																			 var countersizeg = 0;
																			 var countersizegnon = 0;
																			 var weightsizeg = 0;
																			 var weightsizegnon =0;
																			 var totalcountersizeg =0;
																			
																										 
																			 for (var j = 0, max2 = service; j < max2; j++)
																			 {
																				var code = getData[i].postal_provider_code[j].split('###');
																				var serviceCode 	= code[0];
																				var pgweight 		= code[1];
																				var pgcounter 		= code[2];
																				//alert(serviceCode);
																				if( code[0] != "" || code[1] != "" )
																				{
																					if(serviceCode == 'Size E - Non Boxable')
																					{
																						serviceCode = 'sizee-nonboxable';
																					}
																					else if( serviceCode == 'Size G - Boxable')
																					{
																						serviceCode = 'sizeg-boxable';
																					}
																					if(delcountry != 'RestOfEU')
																					{
																					if(serviceCode == 'sizee-reg' || serviceCode == 'sizeg-reg')
																					{
																						if( serviceCode == 'sizee-reg')
																							{
																								var setSelectoritems = delcountry+'PCEEREG';
																								var setSelectorWeight = delcountry+'KGSEREG'
																							}
																							else
																							{
																								var setSelectoritems = delcountry+'PCEGREG';
																								var setSelectorWeight = delcountry+'KGSGREG'
																							}
																							totlaWeightreg = parseFloat(totlaWeightreg) + parseFloat(pgweight);	
																							totalCountReg = parseInt(totalCountReg) + parseInt(pgcounter);
																							$( '.'+setSelectoritems ).val( pgcounter );
																							$( '.'+setSelectorWeight ).val( parseFloat(pgweight).toFixed(3));
																					}
																					else
																					{
																					
																						if( serviceCode == 'sizeg-boxable')
																						{
																							var setSelectoritems = delcountry+'PCEG';
																							var setSelectorWeight = delcountry+'KGSG'
																							
																						}
																						else
																						{
																							var setSelectoritems = delcountry+'PCEEBus';
																							var setSelectorWeight = delcountry+'KGSEBus'
																						}
																						if( setSelectoritems == 'United KingdomPCEG' )
																						{
																							setSelectoritems = 'GreatBritainPCEG';
																							setSelectorWeight = 'GreatBritainKGSG';
																						}
																						else if( setSelectoritems == 'United KingdomPCEEBus' )
																						{
																							setSelectoritems = 'GreatBritainPCEEBus';
																							setSelectorWeight = 'GreatBritainKGSEBus';
																						}
																						else
																						{
																							setSelectoritems = setSelectoritems;
																							setSelectorWeight = setSelectorWeight;
																						}
																								
																						totlaWeightNonreg = parseFloat(totlaWeightNonreg) + parseFloat(pgweight);
																						totalCountNonReg = parseInt(totalCountNonReg) + parseInt(pgcounter);
																						$( '.'+setSelectoritems ).val( pgcounter );
																						$( '.'+setSelectorWeight ).val( parseFloat(pgweight).toFixed(3) );
																					}
																					}
																					else
																					{
																						if( serviceCode == 'sizeg-boxable')
																						{
																							var setSelectoritems = delcountry+'PCEG';
																							var setSelectorWeight = delcountry+'KGSG'
																							countersizeg = parseInt(countersizeg) + parseInt(pgcounter);
																							weightsizeg = parseFloat(weightsizeg) + parseFloat(pgweight);
																						}
																						else
																						{
																							var setSelectoritems = delcountry+'PCEEBus';
																							var setSelectorWeight = delcountry+'KGSEBus'
																							countersizegnon = parseInt(countersizegnon) + parseInt(pgcounter);
																							weightsizegnon = parseFloat(weightsizegnon) + parseFloat(pgweight);
																						}
																						totalcountersizeg = totalcountersizeg + pgcounter;
																					
																					}
																					 groceWeight =  parseFloat(groceWeight) + parseFloat(pgweight);
																				  }
																				}
																				
																				
																				//alert(countersizeg+'>>>>'+countersizegnon);
																				//alert(weightsizeg+'>>>>'+weightsizegnon);
																				
																			 
																			if( delcountry == 'United Kingdom')
																				{
																					 var setSelectorWightNonReg = 'GreatBritainWEIGHTBus';
																					 var setSelectorQtyNonReg =  'GreatBritainQTYBus';
																				}
																				else
																				{
																					 var setSelectorWightNonReg = delcountry+'WEIGHTBus';
																					 var setSelectorQtyNonReg = delcountry+'QTYBus';
																					 var setSelectorWightReg = delcountry+'WEIGHTREG';
																					 var setSelectorQtyReg = delcountry+'QTYREG';
																				}
																				
																			 
																			 totlaWeightreg = (totlaWeightreg != '') ? totlaWeightreg.toFixed(3) : '';
																			 totlaWeightNonreg = (totlaWeightNonreg != '') ? totlaWeightNonreg.toFixed(3) : '';
																			
																			 $( '.'+setSelectorWightReg ).val( totlaWeightreg );
																			 $( '.'+setSelectorWightNonReg ).val( totlaWeightNonreg );
																			 $( '.'+setSelectorQtyReg ).val( (totalCountReg != '') ? totalCountReg : '' );
																			 $( '.'+setSelectorQtyNonReg ).val( (totalCountNonReg != '')?totalCountNonReg:'' );
																			 if( delcountry == 'RestOfEU' )
																				{
																					$('.RestOfEUPCEG').val(countersizeg);
																					$('.RestOfEUPCEEBus').val(countersizegnon);
																					var totalsizeg 		= parseInt(countersizeg) + parseInt(countersizegnon);
																					var totalsizeweight = parseFloat(weightsizeg) + parseFloat(weightsizegnon)
																					
																					$('.RestOfEUKGSG').val(weightsizeg.toFixed(3));
																					$('.RestOfEUKGSEBus').val(weightsizegnon.toFixed(3));
																					$('.RestOfEUQTYBus').val( totalsizeg );
																					$('.RestOfEUWEIGHTBus').val( totalsizeweight.toFixed(3) );
																				}
																			 
																												/* if( serviceCode == 'Size G-REG' || serviceCode == 'Size E-REG' )
																												 {
																												 	 var totalWeightreg = getData[i].totalWeight;
																													 var newCounterreg 	= getData[i].counter;
																													 counterreg = counterreg + newCounterreg;
																													 alert( counterreg );
																												 }
																												 else
																												 {
			        																								   var totalWeight 	= getData[i].totalWeight;
																													   var newCounter 	= getData[i].counter;
																													    counter = counter + newCounter;
																														alert( counter + 'Non Reg' );
																												 }*/
																												
																												
																												/* for (var j = 0, max2 = service; j < max2; j++)
																												 {
																													var code = getData[i].postal_provider_code[j].split('###');
																													//check and restrict
																													if( code[0] == "" || code[1] == "" )
																													{
																														//yyswal( code[0] +'######'+ code[1]);
																													}
																													else
																													{
																														
																														var serviceCode 	= code[0];
																														var pgweight 		= code[1];
																														var pgcounter 		= code[2];
																														totalWeight = parseFloat(totalWeight).toFixed(3);
																														alert(pgweight);
																														//alert(delcountry+'========'+serviceCode +'====='+ pgweight +'====='+ pgcounter );
																														if(  serviceCode == 'sizeg-boxable' || serviceCode == 'sizee-nonboxable')
																														{
																														
																															$('.'+delcountry+'TOTALBus').val( totalWeight );
																															//counter = parseInt(counter) + parseInt(pgcounter);
																															//weight  = parseFloat(weight) + parseFloat(pgweight);
																															
																															 if( serviceCode == 'sizeg-boxable' )
																															 {
																																	 setSelectoritems = delcountry+'PCEG';
																																	 setSelectorWeight = delcountry+'KGSG';
																															 }
																															 else if( serviceCode == 'sizee-nonboxable' )
																															 {
																																	 setSelectoritems = delcountry+'PCEEBus';
																																	 setSelectorWeight = delcountry+'KGSEBus';
																															 }
																															//alert(counter +'=>>>>'+ weight +'>>>>>'+ setSelectoritems+'>>>>>>'+setSelectorWeight);
																														}
																														if(serviceCode == 'sizeg-reg' || serviceCode == 'sizee-reg' )
																														{
																															$('.'+delcountry+'TOTALREG').val( totalWeight );
																																	//counter = counterreg + pgcounter;
																																	//weight = weightreg + pgweight
																															 if( serviceCode == 'sizeg-reg' )
																															 {
																																	 setSelectoritems = delcountry+'PCEGREG';
																																	 setSelectorWeight = delcountry+'KGSGREG';
																																	 
																															 }
																															 else if( serviceCode == 'sizee-reg' )
																															 {
																																	 
																																	 setSelectoritems = delcountry+'PCEEREG';
																																	 setSelectorWeight = delcountry+'KGSEREG';
																															 }
																															 else{}
																														 }
																														
																			
																														if( setSelectoritems == 'United KingdomPCEP' )
																														{
																															setSelectoritems = 'GreatBritainPCEP';
																															setSelectorWeight = 'GreatBritainKGSP';
																														}
																														else if( setSelectoritems == 'United KingdomPCEG' )
																														{
																															setSelectoritems = 'GreatBritainPCEG';
																															setSelectorWeight = 'GreatBritainKGSG';
																														}
																														else if( setSelectoritems == 'United KingdomPCEEBus' )
																														{
																															setSelectoritems = 'GreatBritainPCEEBus';
																															setSelectorWeight = 'GreatBritainKGSEBus';
																														}
																														else
																														{
																															setSelectoritems = setSelectoritems;
																															setSelectorWeight = setSelectorWeight;
																														}
																														
																													
																														if( setSelectoritems == 'Czech RepublicPCEP' )
																														{
																															setSelectoritems = 'CzechRepublicPCEP';
																															setSelectorWeight = 'CzechRepublicPCEP';
																														}
																														else if( setSelectoritems == 'Czech RepublicPCEG' )
																														{
																															setSelectoritems = 'CzechRepublicPCEG';
																															setSelectorWeight = 'CzechRepublicKGSG';
																														}
																														else if( setSelectoritems == 'Czech RepublicPCEEBus' )
																														{
																															setSelectoritems = 'CzechRepublicPCEEBus';
																															setSelectorWeight = 'CzechRepublicKGSEBus';
																														}
																														else
																														{
																															setSelectoritems = setSelectoritems;
																															setSelectorWeight = setSelectorWeight;
																														}
																														pgweight = parseFloat(pgweight).toFixed(3);
																														
																														$( '.'+setSelectoritems ).val( pgcounter );
																														$( '.'+setSelectorWeight ).val( pgweight);
																														
																														
																													}
																														
																														
																													}*/
																													
																															
																											getTotal = parseInt(getTotal) + parseInt(newCounter);
																											getTotalWeight = parseFloat(getTotalWeight) + parseFloat(totalWeight);
																														
																											  }
																											 
																											 	$( '#totalItems' ).val( getTotal );
																												//$( '#totalWeight' ).val( getTotalWeight.toFixed(3) );
																												$( '#totalWeight' ).val( groceWeight.toFixed(3) );
																											  
																						         }																	
																						});
																					/*}*/	

																				}
			}).responseText;
			
			
			return false;
			
			console.log( "****************************" );                    
			console.log( "Now input" );
			console.log( "****************************" );	
		 });
		
		function getResult( data )
		{
			return data;
		}
	});
</script>

<script>
	$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "Euraco is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off_manifest', function()
		 {
			//Get time
			var getTime = $.ajax({				     
					url: "/Cronjobs/getClientTime",					
					dataType: 'text',					
					global: false,
					async:false,
					success: function(data) {
						return data;
					}
			}).responseText;

			var serviceProvider = $(this).attr('id');
			
			if( getTime == '0' )
			{
				swal({
					title: "Are you sure?",
					text: "You are generating Manifest After Cut off. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						var strAction = "update";
						/* Start here updating section */
						$.ajax(
						{
						'url'            : '/Cronjobs/createCutOffList',
						'type'           : 'POST',
						'data'           : { serviceProvider : serviceProvider },
						'success'        : function( data )
										   {										   
											   if( data != 'blank' )
											   {
													window.open(data,'_blank' );
													location.reload();
											   }
											   else
											   {
												   swal("No orders found for manifest!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						swal("Cancelled", "", "error");
					}
				});
			}
			else
			{
				$.ajax(
				{
				'url'            : '/Cronjobs/createCutOffList',
				'type'           : 'POST',
				'data'           : { serviceProvider : serviceProvider },
				'success'        : function( data )
								   {										   
									   if( data != 'blank' )
									   {
											window.open(data,'_blank' );
											location.reload();
									   }
									   else
									   {
										   swal("No orders found for manifest!" , "" , "error");										   
									   }
								   }
				});
			}
			 
		 });
		
		function getResult( data )
		{
			return data;
		}
		
		$( 'body' ).on( 'click', '.cut_off_manifest_back_up', function()
		 {
			 var userId = <?php echo $getOperatorInformation['id']; ?>;
			 var servicePost = $(this).attr('id');
			 
			 $.ajax(
				{
				'url'            : '/Uploads/serviceCounterBackUp',
				'type'           : 'POST',
				'data'           : { userId : userId , servicePost : servicePost },
				'beforeSend'	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
				  },
				'success'        : function( data )
								   {										   
									   if( data != 'blank' )
									   {
										    $( ".outerOpac" ).attr( 'style' , 'display:none');
										    $( ".manifest_button" ).attr( 'style' , 'display:inline-block');
										    $( ".cut_off_manifest_back_up" ).attr( 'style' , 'display:none');
									   }
									   else
									   {
										   $( ".outerOpac" ).attr( 'style' , 'display:none');	 
									   }
								   }
				});
			 
		 });
		
	});
</script>

