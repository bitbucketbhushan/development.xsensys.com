<div class=" container-fluid wrapper">	
   <div class="rightside bg-grey-100">
                
	<section class="content-header">
			 <div class="d-flex justify-content-between bg-light mb-3 mt-3" >
				<div style="float:left;"><h2>New Docket</h2></div>
			   <div style="float:right;"><button type="button" class="btn btn-success">eDoc Done</button> <button type="button" class="btn btn-success">Back up</button></div>
			</div>
    </section>
	
    
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">
                                <div class="panel-body padding-bottom-40 padding-top-40">
                                
									<button class="cut_off_edoc_jp btn btn-danger btn-sm btn-block p-2" id="Jersey Post" type="submit">Generate eDoc</button>    
									
									<table class="tbl_table docketTable table table-borderless" id="docket">
									   <tbody>
										  <tr>
											 <td style="padding-bottom:10px;" colspan="11">
												
											 </td>
										  </tr>
										  <tr>
											 <td class="docketHeaderItem" colspan="4">UK/Jersey</td>
											 
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader">Items</th>
											 <th class="docketTableHeader">Bags</th>
											 
										  </tr>
										  
										   <tr  data-tblblock="A">
												<td >UKF</td>
												<td>UK Packet - Signed For </td>
												<td> <input  type="text"  id="UKFItems"  maxlength="6" value="0"> </td>
												<td><input  type="text"   id="UKFBags" maxlength="6" value="0"></td>	
											</tr>
										  
										  
										  <tr>
											 
											    <td > UKE  </td>	
											     <td>UK Packet - Economy </td>
											     <td><input type="text" value="0" maxlength="6" id="UKEItems"></td>
												<td><input type="text" value="0" maxlength="6" id="UKEBags"> </td>									 
										  </tr>
										  
										  <tr>
											 <td>UKP </td>
											 <td>UK Packet - Priority </td>
											 <td><input type="text" value="0" maxlength="6" id="UKPItems"></td>
											 <td><input type="text" value="0" maxlength="6" id="UKPBags"></td>
											 </tr>	

										  
										  <tr>
											 <td >UKO  </td>
											 <td>UK Parcel - T&S Over 2kg </td>
											 <td><input type="text" value="0" maxlength="6" id="UKOItems"></td>
											  <td><input type="text" value="0" maxlength="6" id="UKOBags"></td>
										  </tr>
										  
										  
										  <tr>
											 <td class="docketHeaderItem" colspan="4">International Blended</td>											
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader">Items</th>
											 <th class="docketTableHeader">Bags</th>
											 
										  </tr>
										  
										  <tr>
											 <td >RWE  </td>
											  <td>RoW - Economy </td>
											   <td><input type="text" value="0" maxlength="6" id="RWEItems"></td>
											   <td><input type="text" value="0" maxlength="6" id="RWEBags"> </td>
											  </tr>
					
										  <tr>
											 <td > ERE </td>
											 <td>Europe - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="EREItems"></td>
											 <td><input type="text" value="0" maxlength="6" id="EREBags"> </td>
										</tr>	 
										  
										  <tr>
											 <td colspan="2">TOTAL</td>
											 <td>&nbsp;</td> 
											 <td><span>0</span></td>
										  </tr>
											
											
										
										  <tr>
											 <td style="padding-top:10px;padding-bottom:10px;" colspan="4">
												
											 </td>
										  </tr>
										  <tr>
											 <td class="docketHeaderItem" colspan="4"> International Country Specific</td>			

											 
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader">Items</th>
											 <th class="docketTableHeader">Bags</th>
											
										  </tr>
										  
								      <tr>
										<td> ESE  </td>
										    <td>Spain - Economy </td>
										   	  <td><input  type="text"   maxlength="6" id="ESEItems" value="0"></td>                       
											<td><input  type="text"   maxlength="6" id="ESEBags" value="0"></td>
									   </tr>	
  
                                 
					
										<tr>
											<td >DEE </td>
												<td >Germany - Economy </td>
													<td ><input  type="text"   maxlength="6" id="DEEItems" value="0"></td>
													<td><input  type="text"   maxlength="6" id="DEEBags" value="0"></td>
										
										</tr>
					
										<tr>
											<td> DKE </td>
											<td >Denmark - Economy </td>
											<td ><input  type="text"   maxlength="6" id="DKEItems" value="0"></td>
											<td><input  type="text"   maxlength="6" id="DKEBags" value="0"></td>       
										</tr>
                    <tr>
                            <td >FIE </td>
							<td >Finland - Economy </td>
							<td ><input  type="text"   maxlength="6" id="FIEItems" value="0"></td>

								<td>
									<input  type="text"   maxlength="6" id="FIEBags" value="0">
								</td> 
                    </tr>
					
                    <tr>
                        <td> GRE  </td>
						<td >Greece - Economy </td>
                        <td ><input  type="text"   maxlength="6" id="GREItems" value="0"></td>
						<td><input  type="text"  maxlength="6" id="GREBags" value="0"> </td>
                     </tr>   

                    <tr>
                        <td> ATE  </td>
						<td >Austria - Economy </td>
						<td> <input  type="text"   maxlength="6" id="ATEItems" value="0"></td>
						<td><input  type="text"   maxlength="6" id="ATEBags" value="0"> </td>
      
                    </tr>
										  
										  
										  
										  <tr>
											 <td>AUE </td>
											 <td>Australia - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="AUEItems"></td>
											<td>
												<input type="text" value="0" maxlength="6" id="AUEBags">
											 </td> 
										  </tr>
										  <tr>
											 <td>
												BEE 

											 </td>
											 <td>Belgium - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="BEEItems"></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="BEEBags">
											 </td>
											
										  </tr>
										  <tr  >
											 <td >
												FRE 

											 </td>
											 <td>France - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="FREItems"></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="FREBags">
											 </td>
											 
										  </tr>
										  <tr  >
											 <td >
												BRE 

											 </td>
											 <td>Brazil - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="BREItems"></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="BREBags">
											 </td>
											 
										  </tr>
										  <tr>
											 <td>
												CAE 

											 </td>
											 <td>Canada - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="CAEItems"></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="CAEBags">
											 </td>
											
										  </tr>
										  <tr  >
											 <td >
												CHE 

											 </td>
											 <td>Switzerland - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="CHEItems"></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="CHEBags">
											 </td>
											
										  </tr>
										  <tr  >
											 <td >
												CNE 

											 </td>
											 <td>China - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="CNEItems"></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="CNEBags">
											 </td>
											
										  </tr>
										  <tr  >
											 <td >
												SEE 

											 </td>
											 <td>Sweden - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="SEEItems" ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="SEEBags">
											 </td>
											
										  </tr>
										  <tr  >
											 <td >
												USE 

											 </td>
											 <td>United States of America - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="USEItems" /></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="USEBags" />
											 </td>
											 
										  </tr>
										  <tr>
											 <td>
												NOE 

											 </td>
											 <td>Norway - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="NOEItems"></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="NOEBags">
											 </td>
											
										  </tr>
										  <tr  >
											 <td >
												NZE 

											 </td>
											 <td>New Zealand - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="NZEItems"></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="NZEBags">
											 </td>
											 
										  </tr>
										  <tr  >
											 <td >
												PLE 

											 </td>
											 <td>Poland - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="PLEItems"></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="PLEBags">
											 </td>
											 
										  </tr>
										  <tr  >
											 <td >
												PTE 

											 </td>
											 <td>Portugal - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="PTEItems"> </td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="PTEBags">
											 </td>
											
										  </tr>
										  <tr  >
											 <td >
												IEE 

											 </td>
											 <td>Ireland - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="IEEItems" ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="IEEBags" >
											 </td>
											 
										  </tr>
										  <tr  >
											 <td >
												ISE 

											 </td>
											 <td>Iceland - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="ISEItems"></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="ISEBags">
											 </td>
											
										  </tr>
										  <tr  >
											 <td >
												ITE 

											 </td>
											 <td>Italy - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="ITEItems"></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="ITEBags">
											 </td>
											 
										  </tr>
										  <tr  >
											 <td >
												JPE 

											 </td>
											 <td>Japan - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="JPEItems"> </td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="JPEBags"   >
											 </td>
											 
										  </tr>
										  <tr  >
											 <td >
												LUE 

											 </td>
											 <td>Luxembourg - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="LUEItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="LUEBags"   >
											 </td>
											
										  </tr>
										  <tr  >
											 <td >
												MTE 

											 </td>
											 <td>Malta - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="MTEItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="MTEBags"   >
											 </td>
											 
										  </tr>
										  <tr  >
											 <td >
												MXE 

											 </td>
											 <td>Mexico - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="MXEItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="MXEBags"   >
											 </td>
											 
										  </tr>
										  <tr  >
											 <td >
												NLE 

											 </td>
											 <td>Netherlands - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="NLEItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="NLEBags"   >
											 </td>
											 
										  </tr>
										  <tr  >
											 <td >
												RUE 

											 </td>
											 <td>Russian Federation - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="RUEItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="RUEBags"   >
											 </td>
											 
										  </tr>
										  <tr  >
											 <td >
												ZAE 

											 </td>
											 <td>South Africa - Economy </td>
											 <td><input type="text" value="0" maxlength="6" id="ZAEItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="ZAEBags"   >
											 </td>
											 
											 
										  </tr>
										  
										  <tr data-tbltotal="C" class="docketTableTotalRow">
											 <td colspan="2" class="docketTableTotal">TOTAL</td>
											
											
											 <td class="docketTableTotal">
												<span  class="calculatedField">0</span>
											 </td>
											 <td class="docketTableTotal">
											 </td>
											 
										  </tr>
										  <tr>
											 <td style="padding-top:10px;padding-bottom:10px;" colspan="4">
												
											 </td>
										  </tr>
										  <tr>
											 <td class="docketHeaderItem" colspan="4"> International Direct Mail</td>
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader">Items</th>
											 <th class="docketTableHeader">Bags</th>
											 
										  </tr>
										  <tr data-tblblock="E" >
											 <td >
												FRU 

											 </td>
											 <td>France (Under 520g) Direct Entry </td>
											 <td><input type="text" value="0" maxlength="6" id="FRU Items"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="FRUBags"   >
											 </td>
											
										  </tr>
										  <tr data-tblblock="E" >
											 <td >
												GYE 

											 </td>
											 <td>Germany - Direct Entry </td>
											 <td><input type="text" value="0" maxlength="6" id="GYEItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="GYEBags"   >
											 </td>
											 
										  </tr>
										  
										  <tr data-tblblock="E" >
											 <td >
												FRO 

											 </td>
											 <td>France (Over 520g) Direct Entry </td>
											 <td><input type="text" value="0" maxlength="6" id="FROItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="FROBags"   >
											 </td>
											 
										  </tr>
								
										  <tr data-tbltotal="E" class="docketTableTotalRow">
											 <td colspan="2" class="docketTableTotal">TOTAL</td>
											
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
												<span  class="calculatedField">0</span>
											 </td>
 
										  </tr>
										  <tr>
											 <td style="padding-top:10px;padding-bottom:10px;" colspan="4">
												
											 </td>
										  </tr>
										  <tr>
											 <td class="docketHeaderItem" colspan="4"> Internatinal Signed & Tracked </td>
											 
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader">Items</th>
											 <th class="docketTableHeader">Bags</th>
											
										  </tr>
										  <tr >
											 <td >
												FRP 

											 </td>
											 <td>France - Priority </td>
											 <td><input type="text" value="0" maxlength="6" id="FRPItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="FRPBags"   >
											 </td>
											
										  </tr>
										  <tr >
											 <td >
												FRT 

											 </td>
											 <td>France - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="FRTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="FRTBags"   >
											 </td>
											
										  </tr>
										  <tr >
											 <td >
												FIP 

											 </td>
											 <td>Finland - Priority </td>
											 <td><input type="text" value="0" maxlength="6" id="FIPItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="FIPBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												FIT 

											 </td>
											 <td>Finland - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="FITItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="FITBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												GRS 

											 </td>
											 <td>Greece - Signed For </td>
											 <td><input type="text" value="0" maxlength="6" id="GRSItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="GRSBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												GRT 

											 </td>
											 <td>Greece - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="GRTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="GRTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												DKP 

											 </td>
											 <td>Denmark - Priority </td>
											 <td><input type="text" value="0" maxlength="6" id="DKPItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="DKPBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												DKT 

											 </td>
											 <td>Denmark - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="DKTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="DKTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												DEP 

											 </td>
											 <td>Germany - Priority </td>
											 <td><input type="text" value="0" maxlength="6" id="DEPItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="DEPBags"   >
											 </td>
											
										  </tr>
										  <tr >
											 <td >
												DET 

											 </td>
											 <td>Germany - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="DETItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="DETBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												ESS 

											 </td>
											 <td>Spain - Signed For </td>
											 
											 <td>
											 <input type="text" value="0" maxlength="6" id="ESSItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="ESSBags"   >
											 </td>
											
										  </tr>
										  <tr >
											 <td >
												EST 

											 </td>
											 <td>Spain - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="ESTItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="ESTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												CNT 

											 </td>
											 <td>China - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="CNTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="CNTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												CHP 

											 </td>
											 <td>Switzerland - Priority </td>
											 <td><input type="text" value="0" maxlength="6" id="CHPItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="CHPBags"   >
											 </td>
											 
										  </tr>
										  
										  	  <tr >
											 <td >
												CHT 

											 </td>
											 <td>Switzerland - Tracked  </td>
											 <td><input type="text" value="0" maxlength="6" id="CHTItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="CHTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												CAS 

											 </td>
											 <td>Canada - Signed For </td>
											 <td><input type="text" value="0" maxlength="6" id="CASItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="CASBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												CAT 

											 </td>
											 <td>Canada - Tracked  </td>
											 <td><input type="text" value="0" maxlength="6" id="CATItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="CATBags"   >
											 </td>
											 
										  </tr>
										  
										  	  <tr >
											 <td >
												BRS 

											 </td>
											 <td>Brazil - Signed For  </td>
											 <td><input type="text" value="0" maxlength="6" id="BRSItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="BRSBags"   >
											 </td>
											 
										  </tr>
										  <tr>
											 <td >
												BRT 

											 </td>
											 <td>Brazil - Tracked  </td>
											 <td><input type="text" value="0" maxlength="6" id="BRTItems" ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="BRTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												AUS 


											 </td>
											 <td>Australia - Signed For  </td>
											 <td><input type="text" value="0" maxlength="6" id="AUSItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="AUSBags"   >
											 </td>
											 
										  </tr>
										  
										  	  <tr >
											 <td >
												AUT  

											 </td>
											 <td>Australia - Tracked  </td>
											 <td><input type="text" value="0" maxlength="6" id="AUTItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="AUTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												BEP 

											 </td>
											 <td>Belgium - Priority  </td>
											 <td><input type="text" value="0" maxlength="6" id="BEPItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="BEPBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												BET 


											 </td>
											 <td>Belgium - Tracked  </td>
											 <td><input type="text" value="0" maxlength="6" id="BEPItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="BEPBags"   >
											 </td>
											 
										  </tr>
										  
										  
										   <tr >
											 <td >
												ATP 

											 </td>
											 <td>Austria - Priority   </td>
											 <td><input type="text" value="0" maxlength="6" id="ATPItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="ATPBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												ATT 
 

											 </td>
											 <td>Austria - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="ATTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="ATTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												NLS  

											 </td>
											 <td>Netherlands - Signed For  </td>
											 <td><input type="text" value="0" maxlength="6" id="NLSItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="NLSBags"   >
											 </td>
											 
										  </tr>
										  
										  	  <tr >
											 <td >
												NLT 
 

											 </td>
											 <td>Netherlands - Tracked  </td>
											 <td><input type="text" value="0" maxlength="6" id="NLTItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="NLTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												MXP 

											 </td>
											 <td>Mexico - Priority </td>
											 <td><input type="text" value="0" maxlength="6" id="MXPItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="MXPBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												MXT  


											 </td>
											 <td>Mexico - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="MXTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="MXTBags"   >
											 </td>
											 
										  </tr>
										  
										  	  <tr >
											 <td >
												MTP   

											 </td>
											 <td>Malta - Priority </td>
											 <td><input type="text" value="0" maxlength="6" id="MTPItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="MTPBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												LUP  

											 </td>
											 <td>Luxembourg - Priority  </td>
											 <td><input type="text" value="0" maxlength="6" id="LUPItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="LUPBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												LUT 


											 </td>
											 <td>Luxembourg - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="LUTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="LUTBags"   >
											 </td>
											 
										  </tr>
										  
										  
										  
										  
										  
										  
										  
										  
										  	  <tr >
											 <td >
												JPP 

											 </td>
											 <td>Japan - Priority </td>
											 <td><input type="text" value="0" maxlength="6" id="JPPItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="JPPBags"   >
											 </td>
											 
										  </tr>
										  
										  	  <tr >
											 <td >
												JPT 

											 </td>
											 <td>Japan - Tracked  </td>
											 <td><input type="text" value="0" maxlength="6" id="JPTItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="JPTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												ITP 

											 </td>
											 <td>Italy - Priority </td>
											 <td><input type="text" value="0" maxlength="6" id="ITPItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="ITPBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												ITT 

											 </td>
											 <td>Italy - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="ITTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="ITTBags"   >
											 </td>
											 
										  </tr>
										  
										  	  <tr >
											 <td >
												ISP  

											 </td>
											 <td>Iceland - Priority </td>
											 <td><input type="text" value="0" maxlength="6" id="ISPItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="ISPBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												IST 

											 </td>
											 <td>Iceland - Tracked   </td>
											 <td><input type="text" value="0" maxlength="6" id="ISTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="ISTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												IEP  


											 </td>
											 <td>Ireland - Priority </td>
											 <td><input type="text" value="0" maxlength="6" id="IEPItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="IEPBags"   >
											 </td>
											 
										  </tr>
										  
										  	  <tr >
											 <td >
												IET 
 

											 </td>
											 <td>Ireland - Tracked   </td>
											 <td><input type="text" value="0" maxlength="6" id="IETItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="IETBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												PTS 

											 </td>
											 <td>Portugal - Signed For  </td>
											 <td><input type="text" value="0" maxlength="6" id="PTSItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="PTSBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												PTT 


											 </td>
											 <td>Portugal - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="PTTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="PTTBags"   >
											 </td>
											 
										  </tr>
										  
										  
										   <tr >
											 <td >
												PLS 

											 </td>
											 <td>Poland - Signed For  </td>
											 <td><input type="text" value="0" maxlength="6" id="PLSItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="PLSBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												PLT  
 

											 </td>
											 <td>Poland - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="PLTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="PLTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												NZP  

											 </td>
											 <td>New Zealand - Priority   </td>
											 <td><input type="text" value="0" maxlength="6" id="NZPItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="NZPBags"   >
											 </td>
											 
										  </tr>
										  
										  	  <tr >
											 <td >
												NZT  
 

											 </td>
											 <td>New Zealand - Tracked   </td>
											 <td><input type="text" value="0" maxlength="6" id="NZTItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="NZTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												NOS  

											 </td>
											 <td>Norway - Signed For  </td>
											 <td><input type="text" value="0" maxlength="6" id="NOSItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="NOSBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												NOT   


											 </td>
											 <td>Norway - Tracked  </td>
											 <td><input type="text" value="0" maxlength="6" id="NOTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="NOTBags"   >
											 </td>
											 
										  </tr>
										  
										  	  <tr >
											 <td >
												USP 

											 </td>
											 <td>United States of America - Priority  </td>
											 <td><input type="text" value="0" maxlength="6" id="USPItems"   ></td>
											 
											 <td>
												<input type="text" value="0" maxlength="6" id="USPBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												UST   

											 </td>
											 <td>United States of America - Tracked   </td>
											 <td><input type="text" value="0" maxlength="6" id="USTItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="USTBags"   >
											 </td>
											 
										  </tr>
										  <tr >
											 <td >
												SEP 
 
											 </td>
											 <td>Sweden - Priority  </td>
											 <td><input type="text" value="0" maxlength="6" id="SEPItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="SEPBags"   >
											 </td>
											 
										  </tr>
							  
										  	  <tr >
											 <td >
												SET 
 
 
											 </td>
											 <td>Sweden - Tracked   </td>
											 <td><input type="text" value="0" maxlength="6" id="SETItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="SETBags"   >
											 </td>
											 
										  </tr>
										  	  <tr >
											 <td >

												ZAS 
 
											 </td>
											 <td>South Africa - Signed For </td>
											 <td><input type="text" value="0" maxlength="6" id="ZASItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="ZASBags"   >
											 </td>
											 
										  </tr>	  
										  <tr >
											 <td >
												ZAT  
 
											 </td>
											 <td>South Africa - Tracked </td>
											 <td><input type="text" value="0" maxlength="6" id="ZATItems"   ></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="ZATBags"   >
											 </td>
											 
										  </tr>	  <tr >
											 <td >
												RUP  
 
											 </td>
											 <td>Russian Federation - Priority  </td>
											 <td><input type="text" value="0" maxlength="6" id="RUPItems"></td>
											
											 <td>
												<input type="text" value="0" maxlength="6" id="RUPBags" >
											 </td>
											 
										  </tr>
										  
										  <tr>
											 <td colspan="2">TOTAL</td>
											 
											 <td>&nbsp;</td>
											 <td>
												<span>0</span>
											 </td>
											 
										  </tr>
										  
										  <tr>
											 <td colspan="2" >Jersey Post Total :</td>
											 
											 <td colspan="2"><input type="text" value="0" maxlength="6" id="jerseyPostTotal"   ></td>
											 
											 
										  </tr>
										  
										  
										  <tr>
											 <td style="padding-top:10px;padding-bottom:10px;" colspan="4">
												<hr class="bannerLine">
											 </td>
										  </tr>
									   </tbody>
									</table>
								</div>
							</div>
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>        
		</div>

</div>
</div>


<script>

$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "eShipper is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off_edoc_jp', function()
		 {
			
			console.log( "****************************" );                    
			console.log( "eShipper is fetching all information" );
			console.log( "****************************" );	
			
			var serviceProvider = $(this).attr('id');
			// Data fetching from this			
			var getEshipper = $.ajax(
			{
				'url'            : '/Manifests/setInfoJerseyEdoc',
				'type'           : 'POST',
				'beforeSend'	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
				  },
				'data'           : { serviceProvider : serviceProvider },
				global: false,
				async:false,
				'success'        : function( msgArray )
									{	
											var json = JSON.parse(msgArray);
											var getData = json.data;
											
											 // Get the value "long_name" from the long_name key
												 for (var i = 0, max = getData.length; i < max; i++)
												 {
														 var counter = getData[i].ServiceCounter.counter;
														 var bags = getData[i].ServiceCounter.bags;
														 var serviceCode = getData[i].ServiceCounter.service_code;
														
														 var esp3 = ''; 
														 var apair = '';
														 var esp4 = ''; 
														 var aproad = '';
														if(serviceCode == 'mad_air' || serviceCode == 'cat_air' || serviceCode == 'sp_air')
														{
															serviceCode = 'ES3';
															esp3 = $('#ES3Items').val();
															counter	=	parseInt(counter) + parseInt(esp3);
															$("#"+serviceCode+'Items').val(counter);
															 $("#"+serviceCode+'Bags').val(bags);
															 counter = 0;
															 bags = 0;
														}
														
														if(serviceCode == 'mad_road' || serviceCode == 'cat_road' || serviceCode == 'sp_road')
														{
															serviceCode = 'ES4';
															esp4 = $('#ES4Items').val();
															counter	=	parseInt(counter) + parseInt(esp4);
															$("#"+serviceCode+'Items').val(counter);
															$("#"+serviceCode+'Bags').val(bags);
															 
															 counter = 0;
															 bags = 0;
														}
													
														 if( $("#"+serviceCode+'Items').val().length == 0 )
														 {
															 $("#"+serviceCode+'Items').val(counter);
															 $("#"+serviceCode+'Bags').val(bags);
															 
															 counter = 0;
															 bags = 0;
														 }
														 else
														 {
														 	 counter = parseInt(counter);
															 bags = parseInt(bags);
															 $("#"+serviceCode+'Items').val(counter);
															 $("#"+serviceCode+'Bags').val(bags);
															 
															 counter = 0;
															 bags = 0;
														 }
														 
												 }
												 
												 $( ".outerOpac" ).attr( 'style' , 'display:none');	

									}
			}).responseText;
			
			
			return false;
			
			console.log( "****************************" );                    
			console.log( "Now input" );
			console.log( "****************************" );	
		 });
	});
	
</script>

<style>
	  .docketHeaderItem {
		font-size: medium;
		font-weight: bold;
		text-align: left;
		overflow: hidden;
		padding-left: 10px;
		background-color: #FFC0C0;
		color: Black;
	
	}
	.docketHeaderItem{text-align:center;}
	.content-header{margin:5px 15px; padding:5px 0px;}
	.docketTable input{border:none; background:none;}
	.docketTable td{font-weight:bold;}
	.docketTable th{font-weight:bold; text-align:center;}
	.wrapper{background:#f8f9fa;}
  </style>