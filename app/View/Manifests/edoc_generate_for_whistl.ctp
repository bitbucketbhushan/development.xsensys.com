<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">
		<?php
			$getOperatorInformation = $this->Session->read('Auth.User') ;	
			date_default_timezone_set('Europe/Jersey');			
		?> 
 		 
			<div class="col-lg-6"><h5 class="page-title">Whistl New Docket <?php echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]'; ?></h5></div>
			<div class="col-lg-6"><ul class="nav navbar-nav pull-right">
			 <li>
				<div class="btn-group manifest_button" >
				 <a type="button" href="<?php echo Router::url('/', true) ?>Whistl/manifest" class="btn btn-success no-margin">Generate Manifest</a>
				<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="caret"></span>
				<span class="sr-only">Toggle Dropdown</span>
				</button>
				 <?php  if(count($files) > 0 ) { ?>
						  <ul class="dropdown-menu scrollable-menu" role="menu">
								<?php 
									$manifestPath = Router::url('/', true).'/img/cut_off/'.$folderName.'/';
									
									foreach( $files as $file )
									{
										$fileName = explode('-', $file) ;
										//File name
									 	$fname = $fileName[0];
										if($fname == 'Whistl'){
											//get date
											$dateValue = explode( '_', $fileName[1] );
											$specificDate = $dateValue[0];
											//get time specific
											$minute = explode('.', $dateValue[2]) ; 
											$timeValue = $dateValue[1] .':' . $minute[0];
										}
								?>
								<?php 
									if( $fname == "Whistl" )
									{
								?>
											<li><a href="<?php echo $manifestPath.$file; ?>" target ="_blank" > <?php print $fname .'('.$timeValue.')' ; ?> </a></li>
								<?php
									} }
								?>
							
						  </ul>
						  <?php }?>
				</div>
			</li> 
			</ul>
			</div>     
      	  
        	<div class="panel-title no-radius bg-green-500 color-white no-border" style="clear:both">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div>
		
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">
 						<div class="panel-body padding-bottom-40 padding-top-10"> 
						
						
						<!-- the action to use when the user presses enter -->
						
						<span class="fieldLabel02Std">This column should contain the total weight (pieces × weight per piece) for the zone or destination.</span>
						<table class="inputBorderel table table-bordered table-hover">
						  <tbody>
							  <tr>											 
								<th style="background-color: #55c9ef;">Service</th>
								<th style="background-color: #55c9ef;">PCE </th>
								<th style="background-color: #55c9ef;">PCE×KG¹</th>
							 </tr>
							 
							  <tr>											 
								<th style="background-color: #FF9999;">Letter</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['L100'])){ echo count($service_name['L100']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['L100'])){ echo array_sum($packet_weight['L100']);}?>"></td>
							 </tr>
							 
							  <tr> 								
								<th style="background-color: #3399FF;">E-Com Large Letter(0-100)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['LL100'])){ echo count($service_name['LL100']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['LL100'])){ echo array_sum($packet_weight['LL100']);}?>"></td>
							 </tr>
							<tr> 								
								<th style="background-color: #3399FF;">E-Com Large Letter(101-250)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['LL250'])){ echo count($service_name['LL250']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['LL250'])){ echo array_sum($packet_weight['LL250']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #3399FF;">E-Com Large Letter(251-300)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['LL300'])){ echo count($service_name['LL300']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['LL300'])){ echo array_sum($packet_weight['LL300']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #3399FF;">E-Com Large Letter(301-350)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['LL350'])){ echo count($service_name['LL350']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['LL350'])){ echo array_sum($packet_weight['LL350']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #3399FF;">E-Com Large Letter(351-400)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['LL400'])){ echo count($service_name['LL400']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['LL400'])){ echo array_sum($packet_weight['LL400']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #3399FF;">E-Com Large Letter(401-450)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['LL450'])){ echo count($service_name['LL450']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['LL450'])){ echo array_sum($packet_weight['LL450']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #3399FF;">E-Com Large Letter(451-500)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['LL500'])){ echo count($service_name['LL500']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['LL500'])){ echo array_sum($packet_weight['LL500']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #3399FF;">E-Com Large Letter(501-600)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['LL600'])){ echo count($service_name['LL600']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['LL600'])){ echo array_sum($packet_weight['LL600']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #3399FF;">E-Com Large Letter(601-650)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['LL650'])){ echo count($service_name['LL650']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['LL650'])){ echo array_sum($packet_weight['LL650']);}?>"></td>
							 </tr>
							  <tr> 								
								<th style="background-color: #3399FF;">E-Com Large Letter(651-700)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['LL700'])){ echo count($service_name['LL700']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['LL700'])){ echo array_sum($packet_weight['LL700']);}?>"></td>
							 </tr>
							  <tr> 								
								<th style="background-color: #3399FF;">E-Com Large Letter(701-750)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['LL750'])){ echo count($service_name['LL750']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['LL750'])){ echo array_sum($packet_weight['LL750']);}?>"></td>
							 </tr>
							 
							 
							  <tr> 								
								<th style="background-color: #FFFF33;">Packet(0-100)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P100'])){ echo count($service_name['P100']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P100'])){ echo array_sum($packet_weight['P100']);}?>"></td>
							 </tr>
							<tr> 								
								<th style="background-color: #FFFF33;">Packet(101-250)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P250'])){ echo count($service_name['P250']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P250'])){ echo array_sum($packet_weight['P250']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #FFFF33;">Packet(251-300)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P300'])){ echo count($service_name['P300']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P300'])){ echo array_sum($packet_weight['P300']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #FFFF33;">Packet(301-350)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P350'])){ echo count($service_name['P350']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P350'])){ echo array_sum($packet_weight['P350']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #FFFF33;">Packet(351-400)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P400'])){ echo count($service_name['P400']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P400'])){ echo array_sum($packet_weight['P400']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #FFFF33;">Packet(401-450)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P450'])){ echo count($service_name['P450']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P450'])){ echo array_sum($packet_weight['P450']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #FFFF33;">Packet(451-500)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P500'])){ echo count($service_name['P500']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P500'])){ echo array_sum($packet_weight['P500']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #FFFF33;">Packet(501-600)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P600'])){ echo count($service_name['P600']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P600'])){ echo array_sum($packet_weight['P600']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #FFFF33;">Packet(601-650)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P650'])){ echo count($service_name['P650']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P650'])){ echo array_sum($packet_weight['P650']);}?>"></td>
							 </tr>
							  <tr> 								
								<th style="background-color: #FFFF33;">Packet(651-700)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P700'])){ echo count($service_name['P700']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P700'])){ echo array_sum($packet_weight['P700']);}?>"></td>
							 </tr>
							  <tr> 								
								<th style="background-color: #FFFF33;">Packet(701-750)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P750'])){ echo count($service_name['P750']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P750'])){ echo array_sum($packet_weight['P750']);}?>"></td>
							 </tr>
						  
						   <tr> 								
								<th style="background-color: #FFFF33;">Packet(751-800)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P800'])){ echo count($service_name['P800']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P800'])){ echo array_sum($packet_weight['P800']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #FFFF33;">Packet(801-850)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P850'])){ echo count($service_name['P850']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P850'])){ echo array_sum($packet_weight['P850']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #FFFF33;">Packet(851-900)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P900'])){ echo count($service_name['P900']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P900'])){ echo array_sum($packet_weight['P900']);}?>"></td>
							 </tr>
							  <tr> 								
								<th style="background-color: #FFFF33;">Packet(901-950)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P950'])){ echo count($service_name['P950']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P950'])){ echo array_sum($packet_weight['P950']);}?>"></td>
							 </tr>
							  <tr> 								
								<th style="background-color: #FFFF33;">Packet(951-1000)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P1000'])){ echo count($service_name['P1000']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P1000'])){ echo array_sum($packet_weight['P1000']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #FFFF33;">Packet(1001-1250)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P1250'])){ echo count($service_name['P1250']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P1250'])){ echo array_sum($packet_weight['P1250']);}?>"></td>
							 </tr>
							 
							 <tr> 								
								<th style="background-color: #FFFF33;">Packet(1251-1500)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P1500'])){ echo count($service_name['P1500']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P1500'])){ echo array_sum($packet_weight['P1500']);}?>"></td>
							 </tr>
							<tr> 								
								<th style="background-color: #FFFF33;">Packet(1501-1750)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P1750'])){ echo count($service_name['P1750']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P1750'])){ echo array_sum($packet_weight['P1750']);}?>"></td>
							 </tr>
							 <tr> 								
								<th style="background-color: #FFFF33;">Packet(1751-2000)</th>
								<td><input type="text" class="form-control" value="<?php if(isset($service_name['P2000'])){ echo count($service_name['P2000']);}?>"></td>
								<td><input type="text" class="form-control" value="<?php if(isset($packet_weight['P2000'])){ echo array_sum($packet_weight['P2000']);}?>"></td>
							 </tr>
							 
						  </tbody>
						</table>
						
						<div class="col-lg-12">
							<label class="control-label col-lg-2" for="latter_count">Latter Count:</label>                                        
							<div class="col-lg-2">                                            
								<input type="text" id="latter_count" class="form-control" value="<?php if(isset($all_service['letter'])){ echo count($all_service['letter']);}?>"> 							</div>
 							<label class="control-label col-lg-1" for="latter_weight">Weight(KG):</label>                                        
							<div class="col-lg-2">                                            
								<input type="text" id="latter_weight" class="form-control" value="<?php if(isset($all_service['letter'])){ echo array_sum($all_service['letter']);}?>">                                        
							</div>
						</div>
						
						<div class="col-lg-12" style="padding-top:10px;"><? //pr($all_service);?>
							<label class="control-label col-lg-2" for="ll_latter_count">E-Com L. Letter Count:</label>                                        
							<div class="col-lg-2">                                            
								<input type="text" id="ll_latter_count" class="form-control" value="<?php if(isset($all_service['large_letters'])){ echo count($all_service['large_letters']);}?>"> 							</div>
 							<label class="control-label col-lg-1" for="ll_latter_weight">Weight(KG):</label>                                        
							<div class="col-lg-2">                                            
								<input type="text" id="ll_latter_weight" class="form-control" value="<?php if(isset($all_service['large_letters'])){ echo array_sum($all_service['large_letters']);}?>">                                        
							</div>
						</div>
						
						<div class="col-lg-12">
							<label class="control-label col-lg-2" for="packets_count">Packets Count:</label>                                        
							<div class="col-lg-2">                                            
								<input type="text" id="packets_count" class="form-control" value="<?php if(isset($all_service['packets'])){ echo count($all_service['packets']);}?>"> 							</div>
 							<label class="control-label col-lg-1" for="packets_weight">Weight(KG):</label>                                        
							<div class="col-lg-2">                                            
								<input type="text" id="packets_weight" class="form-control" value="<?php if(isset($all_service['packets'])){ echo array_sum($all_service['packets']);}?>">                                        
							</div>
						</div>
						
						</form>
						
						</div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
 

