<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">
		<?php
			$getOperatorInformation = $this->Session->read('Auth.User') ;	
			date_default_timezone_set('Europe/Jersey');			
		?>        
    <h1 class="page-title">New Docket <?php echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]'; ?></h1>
	<div class="submenu">
				<div class="navbar-header">
					<ul class="nav navbar-nav pull-left">
						<li>
							<div class="btn-group manifest_button">
							  <button type="button" id="PostNL" class="manifest_show cut_off_manifest_correos btn btn-success no-margin">Generate Manifest</button>
							</div>
							
						</li>
					</ul>
				</div>
			</div>
	 </div>
    <div class="container-fluid">
        <div class="row">
           <div class="col-lg-12">
              <div class="panel">                            
                  <div class="row">
                      <div class="col-lg-12">
						<div class="panel-body padding-bottom-40 padding-top-10">
						   <div class="btn bg-red-500 color-white btn-dark col-sm-12 margin-bottom-10">Spain eShipper</div> 
						   <table class="inputBorderel table table-bordered table-hover">
							  <tbody>
								 <tr>
									<th rowspan="2" style="background-color: #d1c5c5;">Zone1</th>
									<th colspan="2" style="background-color: #55c9ef;" >Detail</th>
								 </tr>
								 <tr>
									<th style="background-color: #55c9ef;">Quantity</th>
									<th style="background-color: #55c9ef;">Weight</th>
								 </tr>
								  <?php $i = 0; foreach( $zones as $zone ) { ?>
								<tr>
									<td style="background-color: #d1c5c5;"><label accesskey="A" ><?php echo $zonesName[$i]; ?></label></td>
									<td style="background-color: #a8e5f9;" ><input type="text" class="form-control normal <?php echo str_replace(' ','_',$zone).'_qty'; ?>" value="" size="8"></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo str_replace(' ','_',$zone).'_weight'; ?>" value="" size="8" ></td>
								 </tr>
								   <?php $i++; } ?>
								</tbody>
							</table>
							   <div class="form-group">
									<label class="control-label col-lg-1 col-lg-offset-5" for="username">Count</label>                                        
									<div class="col-lg-2">                                            
										<input type="text" id="totalItems" class="form-control" name="data[Role][role_name]">                                       
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-1 col-lg-offset-5 padding-top-10" for="username">Weight</label>                                        
									<div class="col-lg-2 padding-top-10">                                            
										<input type="text" id="totalWeight" class="form-control" name="data[Role][role_name]">                                        
									</div>
							   </div>
						  </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>
	$(function()
	{         
	     
			console.log( "****************************" );                    
			console.log( "eShipper is fetching all information" );
			console.log( "****************************" );	
			var serviceProvider	= 'GLS';
			var totalCount = 0;
			var totalWeight = 0;
			var getEshipper = $.ajax(
			{
				url            : '/manifests/getExactWeightGls',
				type           : 	'POST',
				data           : 	{ },
				global		   : 	false,
				async          :	false,
				beforeSend	   : function() { $( ".outerOpac" ).attr( 'style' , 'display:block'); },
				data           : { },
				success        : function( msgArray )
									{		 
											var json = JSON.parse(msgArray);
											var getData = json.data;
											var max2 = 0;
											var getTotal = 0;
											var getTotalWeight = 0;
											var groceWeight = 0;	
											for (var i = 0, max = getData.length; i < max; i++)
											 {
												
												 var delcountry 	= getData[i].delcountry;
												 var service		= getData[i].postal_provider_code.length;
												 var totalWeight 	= getData[i].totalWeight;
												 var newCounter 	= getData[i].counter;
												 for (var j = 0, max2 = service; j < max2; j++)
													{
														var code = getData[i].postal_provider_code[j].split('###');
														var serviceCode 	= code[0];
														var pgweight 		= code[1];
														var pgcounter 		= code[2];
														$('.'+serviceCode+'_qty').val( pgcounter );
														$('.'+serviceCode+'_weight').val( parseFloat(pgweight).toFixed(3));
													}
											}
											$('#totalItems').val( newCounter );
											$('#totalWeight').val( parseFloat(totalWeight).toFixed(3) );
											$('.outerOpac').hide();
									}
			}).responseText;
			return false;
			console.log( "****************************" );                    
			console.log( "Now input" );
			console.log( "****************************" );	
	});

	$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "Euraco is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off_edoc', function()
		 {
			var serviceProvider = $(this).attr('id');
			//Get time
			var getTime = $.ajax({				     
					url: "/Cronjobs/getClientTime",
					data : { serviceProvider : serviceProvider },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					  },					
					dataType: 'text',					
					global: false,
					async:false,
					success: function(data) {
						return data;
					}
			}).responseText;

			if( getTime == '0' )
			{
				swal({
					title: "Are you sure?",
					text: "You are clearing data After Cut off. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						var strAction = "update";
						$.ajax(
						{
						'url'            : '/System/eDoc',
						'type'           : 'POST',
						'data'           : { serviceProvider : serviceProvider },
						'success'        : function( data )
										   {										   
											   if( data != 'blank' )
											   {
													$('input[type=text].countEditBox').val('0');
													$('input[type=text].countEditBox').val('0');
													$( ".outerOpac" ).attr( 'style' , 'display:none');
											   }
											   else
											   {
												   $( ".outerOpac" ).attr( 'style' , 'display:none');
												   swal("No orders found to flush!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						swal("Cancelled", "", "error");
					}
				});
			}
			else
			{
				
				swal({
					title: "Are you sure?",
					text: "have you filled eDoc information. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						var strAction = "update";
						$.ajax(
						{
						'url'            : '/System/eDoc',
						'type'           : 'POST',
						'data'           : { serviceProvider : serviceProvider },
						'success'        : function( data )
										   {										   
											   if( data != 'blank' )
											   {
													$('input[type=text].countEditBox').val('0');
													$('input[type=text].countEditBox').val('0');
													$( ".outerOpac" ).attr( 'style' , 'display:none');
											   }
											   else
											   {
												   $( ".outerOpac" ).attr( 'style' , 'display:none');
												   swal("No orders found to flush!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						swal("Cancelled", "", "error");
					}
				});
			}
			 
		 });
		
		function getResult( data )
		{
			return data;
		}
		
		
	});
</script>


