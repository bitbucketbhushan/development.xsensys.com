<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">
		<?php
			$getOperatorInformation = $this->Session->read('Auth.User') ;	
			date_default_timezone_set('Europe/Jersey');			
		?>        
        <h1 class="page-title">New Docket <?php echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]'; ?></h1>
        	<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div>
		
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">

                             <div class="panel-body padding-bottom-40 padding-top-10">
							 
							   
							<?php								
								echo $this->Form->button(
									'Generate eShipper', 
									array(
										'formaction' => Router::url(
											array('controller' => '/JijGroup/System/JP/eDoc/Create')
										 ),
										'escape' => true,
										'class'=>'btn cut_off bg-red-400 color-white btn-dark col-sm-12 margin-bottom-10',
										'id' => 'PostNL',
										'type' => 'submit'	
									)
								);
								echo $this->form->end();	
							?>
							 

								<form action="/BpiShippingCorner/borderel12handling.do" method="post" name="ebpi.Borderel12HandlingForm">
								   <div><input type="hidden" value="3b5919020fb2511ff406c26a704779f8" name="org.apache.struts.taglib.html.TOKEN"></div>
								   <!-- the action to use when the user presses enter -->
								   <input type="hidden" value="VolumailSortedSubmitGroups12Save" name="method">
								   <span class="fieldLabel02Std">¹This column should contain the total weight (pieces × weight per piece) for the zone or destination.</span>
								   <table class="inputBorderel table table-bordered table-hover">
									  <tbody>
										  <tr>
											<th rowspan="2" style="background-color: #d1c5c5;">Zone1</th>
											<th colspan="2" style="background-color: #55c9ef;">Royal Mail 24</th>
											<th colspan="2" style="background-color: #55c9ef;">Royal Mail 48</th>
											<th colspan="2" style="background-color: #55c9ef;">Royalmail LL 48</th>
											<th colspan="2" style="background-color: #55c9ef;">Total</th>
										 </tr>
										 <tr>
											<th style="background-color: #55c9ef;">PCE</th>
											<th style="background-color: #55c9ef;">PCE×KG¹</th>
											<th style="background-color: #55c9ef;">PCE</th>
											<th style="background-color: #55c9ef;">PCE×KG¹</th>
											<th style="background-color: #55c9ef;">PCE</th>
											<th style="background-color: #55c9ef;">PCE×KG¹</th>
											<th style="background-color: #55c9ef;">Qty</th>
											<th style="background-color: #55c9ef;">Weight</th>
										 </tr>
										 
										<?php
										
										/*App::import( 'Controller' , 'Manifests' );
										$obj = new ManifestsController();
										
										$data = $obj->getRoyalmailDocket();
										pr($data);*/
										
										?>
										
										  <?php foreach( $locationDetail as $locationName ) { 
												$country	=	( $locationName['Location']['county_name'] == 'United Kingdom' )  ? 'Great Britain' : $locationName['Location']['county_name'];
												$countryID	=	( $country == 'Great Britain' || $country == 'Rest Of EU')  ? str_replace( ' ', '', $country ) : $locationName['Location']['county_name'];
										  ?>
								<tr>
									<td style="background-color: #d1c5c5;"><label accesskey="A" ><?php echo $country; ?></label></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>QRM24" value="0" size="8"></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>WRM24" value="0.0" size="8" ></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>QRM48" value="0" size="8" ></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>WRM48" value="0.0" size="8" ></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>QRMLL48" value="0" size="8" ></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>WRMLL48" value="0.0" size="8" ></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>QTYBus" value="" size="8" ></td>
									<td style="background-color: #a8e5f9;"><input type="text" class="form-control normal <?php echo $countryID; ?>WEIGHTBus" value="" size="8" ></td>
									 </tr>
										 <?php } ?>
									  </tbody>
								   </table>
								   
							   <div class="form-group">
									<label class="control-label col-lg-1 col-lg-offset-5" for="username">Count</label>                                        
									<div class="col-lg-2">                                            
										<input type="text" id="totalItems" class="form-control" name="data[Role][role_name]">                                       
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-1 col-lg-offset-5 padding-top-10" for="username">Weight</label>                                        
									<div class="col-lg-2 padding-top-10">                                            
										<input type="text" id="totalWeight" class="form-control" name="data[Role][role_name]">                                        
									</div>
							   </div>
							   
								</form>
								
								</div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>
	$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "eShipper is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
			
		 $( 'body' ).on( 'click', '.cut_off', function()
		 {
			
			console.log( "****************************" );                    
			console.log( "eShipper is fetching all information" );
			console.log( "****************************" );	
			
			var serviceProvider = $(this).attr('id');
			// Data fetching from this			
			var getEshipper = $.ajax(
			{
				'url'            : '/Cronjobs/setInfoEshipperPostnl',
				'type'           : 'POST',
				'data'           : { serviceProvider : serviceProvider },
				global: false,
				async:false,
				'success'        : function( msgArray )
								{	
								var json = JSON.parse(msgArray);
								var getData = json.data;
								var newCounter = 0;
								var newtotalWeight = 0;
								
									 var getEshipper = $.ajax(
									 {
											 'url'            : '/manifests/getExactWeightRoyalmail',
											 'type'           : 'POST',
											 'data'           : {  },
											 global: false,
											 async:false,
											 'success'        : function( msgArray )
																 {
																		var json = JSON.parse(msgArray);
																		var getData = json.data;
																		var max2 = 0;
																		var getTotal = 0;
																		var getTotalWeight = 0;
																		$('.GreatBritainQRM24').val(0);
																		$('.GreatBritainWRM24').val(0.0);
																		
																		$('.GreatBritainQRM48').val(0);
																		$('.GreatBritainWRM48').val(0.0);
																		
																		$('.GreatBritainQRMLL48').val(0);
																		$('.GreatBritainWRMLL48').val(0.0);
																								
																	for (var i = 0, max = getData.length; i < max; i++)
																		 {
																			
																			 var delcountry 	= getData[i].delcountry;
																			 var service		= getData[i].postal_provider_code.length;
																			 var totalWeight 	= getData[i].totalWeight;
																			 var newCounter 	= getData[i].counter;
																			 var grossCountryWeight = 0;
																			 var grossCountryCount = 0;
																			 var counterreg = 0;
																			 var counter = 0;
																			 var weight = 0;
																			 var weightreg = 0;
																			 var countTotal = 0;
																			 var totlaWeightreg = 0;
																			 var totlaWeightNonreg = 0;
																			 var totalCountReg = 0;
																			 var totalCountNonReg = 0;
																			 var totalWeight = 0;
																			 var totalCount = 0;
																			 
																			 for (var j = 0; j < service; j++)
																			 {
																				var code = getData[i].postal_provider_code[j].split('###');
																				var serviceCode 	= code[0];
																				var pgweight 		= code[1];
																				var pgcounter 		= code[2];
																				var val24 = 0;
																				var wal24 = 0;
																				var val48 = 0;
																				var wal48 = 0;
																				var valll = 0;
																				var walll = 0;
																				var sername = code[3];
																				if( code[0] != "" || code[1] != "" )
																				{
																					if(delcountry == 'United Kingdom')
																					{
																						if(sername == 'royalmail_24')
																						{
																							val24 = $( '.GreatBritainQRM24' ).val();
																							val24  = parseInt(parseInt(val24) + parseInt(pgcounter));
																							$( '.GreatBritainQRM24' ).val( val24 );
																							wal24 = $( '.GreatBritainWRM24' ).val();
																							wal24  = parseFloat(wal24) + parseFloat(pgweight);
																							$( '.GreatBritainWRM24' ).val( parseFloat(wal24).toFixed(3));
																						}
																						else if(sername == 'royalmail_48')
																						{
																							val48 = $( '.GreatBritainQRM48' ).val();
																							val48  = parseInt(parseInt(val48) + parseInt(pgcounter));
																							$( '.GreatBritainQRM48' ).val( val48 );
																							wal48 = $( '.GreatBritainWRM48' ).val();
																							wal48  = parseFloat(wal48) + parseFloat(pgweight);
																							$( '.GreatBritainWRM48' ).val( parseFloat(wal48).toFixed(3));
																						}
																						else if(sername == 'royalmail_LL_48')
																						{
																							valll = $( '.GreatBritainQRMLL48' ).val();
																							valll  = parseInt(parseInt(valll) + parseInt(pgcounter));
																							$( '.GreatBritainQRMLL48' ).val( valll );
																							//$( '.GreatBritainQRMLL48' ).val( pgcounter );
																							walll 	= $( '.GreatBritainWRMLL48' ).val();
																							walll	= parseFloat(walll) + parseFloat(pgweight);
																							
																							$( '.GreatBritainWRMLL48' ).val( parseFloat(walll).toFixed(3));
																						}
																						totalWeight = parseFloat(totalWeight) + parseFloat(pgweight);	
																						totalCount = parseInt(totalCount) + parseInt(pgcounter);
																					}
																				 }
																				 	
																				    grossCountryWeight = (totalWeight != '') ? totalWeight.toFixed(3) : '';
																			 		grossCountryCount = (totalCount != '') ? totalCount : '';
																			 }
																			  $( '.GreatBritainWEIGHTBus' ).val( grossCountryWeight );
																			  $( '.GreatBritainQTYBus' ).val( (grossCountryCount != '') ? grossCountryCount:'' );
																			
																			  getTotal = parseInt(getTotal) + parseInt(newCounter);
																			  getTotalWeight = parseFloat(getTotalWeight) + parseFloat(totalWeight);
																			}
																				$( '#totalItems' ).val( getTotal );
																				$( '#totalWeight' ).val( getTotalWeight.toFixed(3) );
																		}																	
															});
													}
			}).responseText;
			
			
			return false;
			
			console.log( "****************************" );                    
			console.log( "Now input" );
			console.log( "****************************" );	
		 });
		
		function getResult( data )
		{
			return data;
		}
	});
</script>

