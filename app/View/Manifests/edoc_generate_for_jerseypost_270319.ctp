<div class=" container-fluid wrapper">	
   <div class="rightside bg-grey-100">
                
	<section class="content-header">
			 <div class="d-flex justify-content-between bg-light mb-3 mt-3" >
				<div style="float:left;"><h2>New Docket</h2></div>
			   <div style="float:right;"><button type="button" class="btn btn-success">eDoc Done</button> <button type="button" class="btn btn-success">Back up</button></div>
			</div>
    </section>
	
    
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">
                                <div class="panel-body padding-bottom-40 padding-top-40">
                                
									<button class="cut_off_edoc_jp btn btn-danger btn-sm btn-block p-2" id="Jersey Post" type="submit">Generate eDoc</button>    
									<table class="tbl_table docketTable table table-borderless" id="docket1" style="display:none;">
										<tr><td style="padding-bottom:10px;" colspan="11"></td></tr>
										  <tr>
											 <td class="docketHeaderItem" colspan="4">UK/Jersey</td>
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader" style="text-align: left;">Items</th>
											 <th class="docketTableHeader" style="text-align: left;">Bags</th>
										  </tr>
											<tr data-tblblock="A" id="UKFShow" style="display:none;" >
												<td >UKF</td>
												<td>UK Packet - Signed For </td>
												<td> <input  type="text"  id="UKFItems"  maxlength="6" value="0"> </td>
												<td><input  type="text"   id="UKFBags" maxlength="6" value="0"></td>	
											</tr>
											<tr id="UKEShow" style="display:none;">
												<td > UKE  </td>	
												<td>UK Packet - Economy </td>
												<td><input type="text" value="0" maxlength="6" id="UKEItems"></td>
												<td><input type="text" value="0" maxlength="6" id="UKEBags"> </td>									 
											</tr>
											<tr id="UKPShow" style="display:none;">
												<td>UKP </td>
												<td>UK Packet - Priority </td>
												<td><input type="text" value="0" maxlength="6" id="UKPItems"></td>
												<td><input type="text" value="0" maxlength="6" id="UKPBags"></td>
											</tr>	
											<tr id="UKOShow" style="display:none;">
												<td >UKO  </td>
												<td>UK Parcel - T&S Over 2kg </td>
												<td><input type="text" value="0" maxlength="6" id="UKOItems"></td>
												<td><input type="text" value="0" maxlength="6" id="UKOBags"></td>
											</tr>
									</table>
									<!-----------------------------International Blended Start ----------------------------------->
									<table class="tbl_table docketTable table table-borderless" id="docket2" style="display:none;">
										  <tr><td style="padding-bottom:10px;" colspan="11"></td></tr>
										  <tr><td class="docketHeaderItem" colspan="4">International Blended</td></tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader" style="text-align: left;" >Items</th>
											 <th class="docketTableHeader" style="text-align: left;" >Bags</th>
										  </tr>
											<tr id="RWEShow" style="display:none;">
												<td >RWE  </td>
												<td>RoW - Economy </td>
												<td><input type="text" value="0" maxlength="6" id="RWEItems"></td>
												<td><input type="text" value="0" maxlength="6" id="RWEBags"> </td>
											</tr>
											<tr id="EREShow" style="display:none;">
												<td >ERE </td>
												<td>Europe - Economy </td>
												<td><input type="text" value="0" maxlength="6" id="EREItems"></td>
												<td><input type="text" value="0" maxlength="6" id="EREBags"> </td>
											</tr>	 
										  	<tr>
												<td colspan="2">TOTAL</td>
												<td>&nbsp;</td> 
												<td><span>0</span></td>
											</tr>
									</table>
									<!-----------------------------International Blended End ----------------------------------->
									
									
									<table class="tbl_table docketTable table table-borderless" id="docket" style="display:none;">
										  <tr><td style="padding-bottom:10px;" colspan="11"></td></tr>
									 	  <tr><td class="docketHeaderItem" colspan="4"> International Country Specific</td></tr>
										  <tr class="docketTableHeaderRow" >
												 <th class="docketTableHeader"></th>
												 <th class="docketTableHeader"></th>
												 <th class="docketTableHeader" style="text-align: left;" >Items</th>
												 <th class="docketTableHeader" style="text-align: left;" >Bags</th>
										  </tr>
										  <tr id="ESEShow" style="display:none;" >
												<td> ESE  </td>
												<td>Spain - Economy </td>
												<td><input  type="text"   maxlength="6" id="ESEItems" value="0"></td>                       
												<td><input  type="text"   maxlength="6" id="ESEBags" value="0"></td>
										   </tr>	
											<tr id="DEEShow" style="display:none;">
												<td >DEE </td>
												<td >Germany - Economy </td>
												<td ><input  type="text"   maxlength="6" id="DEEItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="DEEBags" value="0"></td>
											</tr>
					
										<tr id="DKEShow" style="display:none;" >
											<td> DKE </td>
											<td >Denmark - Economy </td>
											<td ><input  type="text"   maxlength="6" id="DKEItems" value="0"></td>
											<td><input  type="text"   maxlength="6" id="DKEBags" value="0"></td>       
										</tr>
										<tr id="FIEShow" style="display:none;">
											<td >FIE </td>
											<td >Finland - Economy </td>
											<td ><input  type="text"   maxlength="6" id="FIEItems" value="0"></td>
											<td><input  type="text"   maxlength="6" id="FIEBags" value="0"></td> 
										</tr>
										<tr id="GREShow" style="display:none;">
											<td> GRE  </td>
											<td >Greece - Economy </td>
											<td ><input  type="text"   maxlength="6" id="GREItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="GREBags" value="0"> </td>
										</tr>   
										<tr id="ATEShow" style="display:none;" >
											<td> ATE  </td>
											<td >Austria - Economy </td>
											<td> <input  type="text"   maxlength="6" id="ATEItems" value="0"></td>
											<td><input  type="text"   maxlength="6" id="ATEBags" value="0"> </td>
										</tr>
										<tr id="AUEShow" style="display:none;" >
											<td>AUE </td>
											<td>Australia - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="AUEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="AUEBags"></td> 
										</tr>
										<tr id="BEEShow" style="display:none;" >
											<td>BEE </td>
											<td>Belgium - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="BEEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="BEEBags"></td>
										</tr>
										<tr id="FREShow" style="display:none;" >
											<td >FRE </td>
											<td>France - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="FREItems"></td>
											<td><input type="text" value="0" maxlength="6" id="FREBags"></td>
										</tr>
										<tr id="BREShow" style="display:none;" >
											<td>BRE</td>
											<td>Brazil - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="BREItems"></td>
											<td><input type="text" value="0" maxlength="6" id="BREBags"></td>
										</tr>
										<tr id="CAEShow" style="display:none;" >
											<td>CAE</td>
											<td>Canada - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="CAEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="CAEBags"></td>
										</tr>
										<tr id="CHEShow" style="display:none;" >
											<td>CHE</td>
											<td>Switzerland - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="CHEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="CHEBags"></td>
										</tr>
										<tr id="CNEShow" style="display:none;" >
											<td >CNE </td>
											<td>China - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="CNEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="CNEBags"></td>
										</tr>
										<tr id="SEEShow" style="display:none;" >
											<td>SEE</td>
											<td>Sweden - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="SEEItems" ></td>
											<td><input type="text" value="0" maxlength="6" id="SEEBags"></td>
										</tr>
										<tr id="USEShow" style="display:none;">
											<td >USE </td>
											<td>United States of America - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="USEItems" /></td>
											<td><input type="text" value="0" maxlength="6" id="USEBags" /></td>
										</tr>
										<tr id="NOEShow" style="display:none;" >
											<td>NOE /td>
											<td>Norway - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="NOEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="NOEBags"></td>
										</tr>
										<tr id="NOEShow" style="display:none;" >
											<td>NZE</td>
											<td>New Zealand - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="NZEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="NZEBags"></td>
										</tr>
										<tr id="PLEShow" style="display:none;" >
											<td>PLE</td>
											<td>Poland - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="PLEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="PLEBags"></td>
										</tr>
										<tr id="PTEShow" style="display:none;" >
											<td>PTE</td>
											<td>Portugal - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="PTEItems"> </td>
											<td><input type="text" value="0" maxlength="6" id="PTEBags"></td>
										</tr>
										<tr id="IEEShow" style="display:none;" >
											<td>IEE</td>
											<td>Ireland - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="IEEItems" ></td>
											<td><input type="text" value="0" maxlength="6" id="IEEBags" ></td>
										</tr>
										<tr id="ISEShow" style="display:none;" >
											<td>ISE</td>
											<td>Iceland - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="ISEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="ISEBags"></td>
										</tr>
										<tr id="ITEShow" style="display:none;" >
											<td>ITE</td>
											<td>Italy - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="ITEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="ITEBags"></td>
										</tr>
										<tr id="JPEShow" style="display:none;" >
											<td>JPE</td>
											<td>Japan - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="JPEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="JPEBags"></td>
										</tr>
										<tr id="LUEShow" style="display:none;" >
											<td>LUE</td>
											<td>Luxembourg - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="LUEItems" ></td>
											<td><input type="text" value="0" maxlength="6" id="LUEBags" ></td>
										</tr>
										<tr id="MTEShow" style="display:none;" >
											<td>MTE</td>
											<td>Malta - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="MTEItems"   ></td>
											<td><input type="text" value="0" maxlength="6" id="MTEBags"   ></td>
										</tr>
										<tr id="MXEShow" style="display:none;" >
											<td>MXE</td>
											<td>Mexico - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="MXEItems"   ></td>
											<td><input type="text" value="0" maxlength="6" id="MXEBags"   ></td>
										</tr>
										<tr id="NLEShow" style="display:none;" >
											<td>NLE</td>
											<td>Netherlands - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="NLEItems" ></td>
											<td><input type="text" value="0" maxlength="6" id="NLEBags" ></td>
										</tr>
										<tr id="RUEShow" style="display:none;" >
											<td>RUE</td>
											<td>Russian Federation - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="RUEItems"   ></td>
											<td><input type="text" value="0" maxlength="6" id="RUEBags"   ></td>
										</tr>
										<tr id="ZAEShow" style="display:none;" >
											<td>ZAE</td>
											<td>South Africa - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="ZAEItems"   ></td>
											<td><input type="text" value="0" maxlength="6" id="ZAEBags"   ></td>
										</tr>
										<!--<tr data-tbltotal="C" class="docketTableTotalRow">
											<td colspan="2" class="docketTableTotal">TOTAL</td>
											<td class="docketTableTotal"><span  class="calculatedField">0</span></td>
											<td class="docketTableTotal"></td>
										</tr>-->
										<tr>
											<td style="padding-top:10px;padding-bottom:10px;" colspan="4"></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>        
		</div>

</div>
</div>


<script>

$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "eShipper is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off_edoc_jp', function()
		 {
			
			console.log( "****************************" );                    
			console.log( "eShipper is fetching all information" );
			console.log( "****************************" );	
			
			var serviceProvider = $(this).attr('id');
			// Data fetching from this			
			var getEshipper = $.ajax(
			{
				'url'            : '/Manifests/setInfoJerseyEdoc',
				'type'           : 'POST',
				'beforeSend'	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
				  },
				'data'           : { serviceProvider : serviceProvider },
				global: false,
				async:false,
				'success'        : function( msgArray )
									{	
											var json = JSON.parse(msgArray);
											var getData = json.data;
											 // Get the value "long_name" from the long_name key
												 for (var i = 0, max = getData.length; i < max; i++)
												 {
												 		//alert(getData[i][0].counter)
														 var counter = getData[i].ServiceCounter.counter;
														 var bags = getData[i].ServiceCounter.bags;
														 var serviceCode = getData[i].ServiceCounter.service_code;
														 //alert( serviceCode );
														 var esp3 = ''; 
														 var apair = '';
														 var esp4 = ''; 
														 var aproad = '';
														if(serviceCode == 'mad_air' || serviceCode == 'cat_air' || serviceCode == 'sp_air')
														{
															serviceCode = 'ESE';
															esp3 = $('#ES3Items').val();
															counter	=	parseInt(counter) + parseInt(esp3);
															$("#"+serviceCode+'Items').val(counter);
															 $("#"+serviceCode+'Bags').val(bags);
															 counter = 0;
															 bags = 0;
														}
														
														if(serviceCode == 'mad_road' || serviceCode == 'cat_road' || serviceCode == 'sp_road')
														{
															serviceCode = 'ESE';
															esp4 = $('#ES4Items').val();
															counter	=	parseInt(counter) + parseInt(esp4);
															$("#"+serviceCode+'Items').val(counter);
															$("#"+serviceCode+'Bags').val(bags);
															 
															 counter = 0;
															 bags = 0;
														}
													
														 if( $("#"+serviceCode+'Items').val().length == 0 )
														 {
															 $("#"+serviceCode+'Items').val(counter);
															 $("#"+serviceCode+'Bags').val(bags);
															 
															 counter = 0;
															 bags = 0;
														 }
														 else
														 {
														 	 counter = parseInt(counter);
															 bags = parseInt(bags);
															 $("#"+serviceCode+'Items').val(counter);
															 $("#"+serviceCode+'Bags').val(bags);
															 $("#"+serviceCode+'Show').show();
															 $("#"+serviceCode+'Show').parent().parent().show();
															 counter = 0;
															 bags = 0;
														 }
														 
												 }
												 
												 $( ".outerOpac" ).attr( 'style' , 'display:none');	

									}
			}).responseText;
			
			
			return false;
			
			console.log( "****************************" );                    
			console.log( "Now input" );
			console.log( "****************************" );	
		 });
	});
	
</script>

<style>
	  .docketHeaderItem {
		font-size: medium;
		font-weight: bold;
		text-align: left;
		overflow: hidden;
		padding-left: 10px;
		background-color: #FFC0C0;
		color: Black;
	
	}
	.docketHeaderItem{text-align:center;}
	.content-header{margin:5px 15px; padding:5px 0px;}
	.docketTable input{border:none; background:none;}
	.docketTable td{font-weight:bold;}
	.docketTable th{font-weight:bold; text-align:center;}
	.wrapper{background:#f8f9fa;}
  </style>