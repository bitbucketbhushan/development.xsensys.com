<div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">
							<div class="panel-body padding-bottom-40 padding-top-10">
								<form action="/BpiShippingCorner/borderel12handling.do" method="post" name="ebpi.Borderel12HandlingForm">
								   <div><input type="hidden" value="3b5919020fb2511ff406c26a704779f8" name="org.apache.struts.taglib.html.TOKEN"></div>
								  
								   <input type="hidden" value="VolumailSortedSubmitGroups12Save" name="method">
								   <table class="inputBorderel table table-bordered table-hover">
									  <tbody>
										 <tr>
											<th>Category</th>
											<th>Count( Items )</th>
											<th>Weight( Kg. )</th>
										 </tr>
										 <?php foreach( $getResult as $getResultValue ) { ?>
										 <tr>
											<td><label accesskey="A" ><?php echo  $getResultValue['CategoryCountByEdoc']['category_name']; ?></label></td>
											<td><input type="text" value="<?php echo  $getResultValue[0]['QTYSKUSPLIT']; ?>" class="form-control normal PCEP" ></td>
											<td><input type="text" value="<?php echo  $getResultValue[0]['TOTALWEIGHT']; ?>" class="form-control normal KGSP" ></td>
										 </tr>
										 <?php } ?>
									  </tbody>
								   </table>
								</form>
							</div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>
