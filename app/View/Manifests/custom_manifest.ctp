<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">  
	<h4 class="modal-title" id="myLargeModalLabel-1">Generate Selected Date Manifest</h4>     
		<div class="row head" style="clear:both;">
		 		<div class="col-sm-12" >
					<div class="col-lg-12" style="background-color: #cfd8dc; padding: 15px; font-size: 14px;">
						<div class="col-sm-4">&nbsp;</div>
						<div class="col-sm-4" style="font-weight: bold;" >
							<select class="form-control input-sm date_select">
								<option>Select Manifest</option>
								<?php foreach( $getmanifestLists as $getmanifestList ) {  ?>
								<option value="<?php echo date('Y-m-d', strtotime($getmanifestList['ManifestEntrie']['manifest_date']));?>"> <?php echo $getmanifestList['ManifestEntrie']['manifest_name']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-sm-4">&nbsp;</div>
					</div>
					<div class="col-sm-12" style="background-color: #e9eaea; padding: 5px; font-size: 12px;">
						<div class="col-sm-4">&nbsp;</div>
						<div class="col-sm-4">&nbsp;</div>
						<div class="col-sm-4 download_link">
						<a href="/Manifests/downloadTextFile" class="download_file btn btn-success color-white btn-dark" style="display:none; margin-right:8px; auto; text-align: center;">Download Manifest</a>
						</div>
					</div>
			</div>
		</div>
    </div>
</div>
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>

$(document).ready(function() {

	$(".date_select").change(function(){
									var date = $( this ).val();
									$.ajax({
										  url : '/Manifests/createManifestFromEntry',
										  type: "POST",   
										  cache: false,
										  data : {date:date},
										  beforeSend : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },  
										  success: function(data)
														  {  
																$( ".outerOpac" ).attr( 'style' , 'display:none');
																if(data == 1)
																{
																	$('.download_file').show();
																}
																else
																{
																	$('.download_file').hide();
																	alert(data);
																}
														  }  
											});
								  });
							});


</script>