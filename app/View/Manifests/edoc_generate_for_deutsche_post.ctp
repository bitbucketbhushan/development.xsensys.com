<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
			<?php
				$getOperatorInformation = $this->Session->read('Auth.User') ;	
				date_default_timezone_set('Europe/Jersey');			
			?>        
        	<h1 class="page-title">New Docket <?php echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]'; ?></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div> 
		
    </div>
		<!-- Modal -->
		<!--<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog modal-lg">
				  <div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<center style="font-size: 16px;">Docket No :- <strong class="doc_num"></strong></center>
					  </div>
					  <div class="modal-body">
						<table width="100%" class="table table-striped">
							<tr>
								<th>Service Code</th>
								<th>Service Name</th>
								<th>Items</th>
								<th>Bags</th>
								<th>Delivery Country</th>
								
							</tr>
							
							<?php if(count($getserviceCount) > 0) { foreach( $getserviceCount as $getservic) { 
							if($getservic['ServiceCounter']['order_ids'] == ''){ 
								$orders =	'0';} else {
								$ids	=	explode(',',$getservic['ServiceCounter']['order_ids']);
								$orders	=	count($ids);
							}
							?>
							<tr>
								<td><?php echo $getservic['ServiceCounter']['service_code']; ?></td>
								<td><?php echo $getservic['ServiceCounter']['service_name']; ?></td>
								<td><?php echo $orders; ?></td>
								<td><?php echo $getservic['ServiceCounter']['bags']; ?></td>
								<td><?php echo $getservic['ServiceCounter']['destination']; ?></td>
							</tr>
							<?php } } ?>	
						</table>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="edocbutton btn cut_off_manifest btn btn-default" data-dismiss="modal" id = "Jersey Post">Want To Create Manifest</button>
					  </div>
					</div>
			  	</div>
			</div>-->
	<!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">
                                <div class="panel-body padding-bottom-40 padding-top-40">
                                	
									 <!--<label class="col-sm-2">Docket No.</label>
									 <input class="col-sm-2 margin-bottom-10" type="text" name="docket_no" id="docketno" >-->
									 <button type="button" id="germany" class="cut_off_manifest bg-red-400 color-white btn-dark col-sm-5 margin-bottom-10 btn" style="float:right;" >Germany Manifest</button>
									 <button type="button" id="france" class="cut_off_manifest bg-red-400 color-white btn-dark col-sm-5 margin-bottom-10 btn" style="float:left;" >France Manifest</button>
									<table class="tbl_table docketTable" id="docket">
									   <tbody>
										  <tr>
											 <td style="padding-bottom:10px;" colspan="11">
												<hr class="bannerLine">
											 </td>
										  </tr>
										 
										  <tr>
											 <td class="docketHeaderItem" colspan="9">E International Direct Mail</td>
											 <td class="docketHeaderItem" colspan="2">New</td>
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <td class="docketTableItemSeparator"></td>
											 <th class="docketTableHeader">Items</th>
											 <th class="docketTableHeader">Bags</th>
											 <th class="docketTableHeader">Item Rate</th>
											 <th class="docketTableHeader">Weight</th>
											 <th class="docketTableHeader">Kilo Rate</th>
											 <th class="docketTableHeader">Item Amount</th>
											 <th class="docketTableHeader">Kilo Amount</th>
											 <th class="docketTableHeader">Total Amount</th>
										  </tr>
										  <tr data-tblblock="E" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="E" name="docketid">
												<input value="385" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="3" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GYE</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Germany - Direct Entry</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GYEItems" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GYEBages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.8950" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.5997" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  
										  <tr data-tblblock="E" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="E" name="docketid">
												<input value="393" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="9" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">FRU</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">France Direct Entry</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="FRUItems" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="FRUBages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.8610" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.2416" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tbltotal="E" class="docketTableTotalRow">
											 <td colspan="2" class="docketTableTotal">TOTAL</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
												<span data-tbl="{&quot;name&quot;:&quot;bags&quot;,&quot;fmt&quot;:&quot;num&quot;}" class="calculatedField">0</span>
											 </td>
											 <td class="docketTableTotal">
											 </td>
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
											 </td>
											 <td class="docketTableTotal">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>    
											 </td>
											 <td class="docketTableTotal">   
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
											 </td>
											 <td class="docketTableTotal">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
											 </td>
										  </tr>
										  <tr>
											 <td style="padding-top:10px;padding-bottom:10px;" colspan="11">
												<hr class="bannerLine">
											 </td>
										  </tr>
										  
									   </tbody>
									</table>
								</div>
							</div>
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>        
		</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

<script>

$(function()
	{         
	     
			console.log( "****************************" );                    
			console.log( "eShipper is fetching all information" );
			console.log( "****************************" );	
			
			//var serviceProvider = $(this).attr('id');
			// Data fetching from this			
			var serviceProvider	= 'Jersey Post';
			var getEshipper = $.ajax(
			{
				'url'            : '/Directposts/setInfoEdoc',
				'type'           : 'POST',
				'beforeSend'	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
				  },
				'data'           : { serviceProvider : serviceProvider },
				global: false,
				async:false,
				'success'        : function( msgArray )
									{	
											var json = JSON.parse(msgArray);
											var getData = json.data;
												 for (var i = 0, max = getData.length; i < max; i++)
												 {
												 		 var counter = getData[i].ServiceCounter.counter;
														 var bags = getData[i].ServiceCounter.bags;
														 var serviceCode = getData[i].ServiceCounter.service_code;
														 if( $("#"+serviceCode+'Items').val().length == 0 )
														 {
														 	 $("#"+serviceCode+'Items').val(counter);
															 $("#"+serviceCode+'Bages').val(bags);
															
															 counter = 0;
															 bags = 0;
														 }
														 else
														 {
														 	 //counter = parseInt(counter) + parseInt($("#"+serviceCode+'Items').val());
															 //bags = parseInt(bags) + parseInt($("#"+serviceCode+'Bages').val());
															 $("#"+serviceCode+'Items').val(counter);
															 $("#"+serviceCode+'Bages').val(bags);
															
															 counter = 0;
															 bags = 0;
														 }
												 }
												 $( ".outerOpac" ).attr( 'style' , 'display:none');	
										}
			}).responseText;
			return false;
			console.log( "****************************" );                    
			console.log( "Now input" );
			console.log( "****************************" );	
	});
</script>
<script>

	/*function checkdocatno()
	{
		var docnum = $('#docketno').val();
		if(docnum != ''){
			$('.doc_num').text(docnum);
			$('#myModal').modal('show');
		} else {
			swal("Please fill edoc number." , "" , "error");
			return false;
		}
	}*/
	
	
	$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "Euraco is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off_edoc', function()
		 {
			var serviceProvider = $(this).attr('id');
			//Get time
			var getTime = $.ajax({				     
					url: "/Cronjobs/getClientTime",
					data : { serviceProvider : serviceProvider },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					  },					
					dataType: 'text',					
					global: false,
					async:false,
					success: function(data) {
						return data;
					}
			}).responseText;

			if( getTime == '0' )
			{
				swal({
					title: "Are you sure?",
					text: "You are clearing data After Cut off. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					if (isConfirm)
					{
						var strAction = "update";
						$.ajax(
						{
						'url'            : '/System/eDoc',
						'type'           : 'POST',
						'data'           : { serviceProvider : serviceProvider },
						'success'        : function( data )
										   {										   
											   if( data != 'blank' )
											   {
													$('input[type=text].countEditBox').val('0');
													$('input[type=text].countEditBox').val('0');
													$( ".outerOpac" ).attr( 'style' , 'display:none');
											   }
											   else
											   {
												   $( ".outerOpac" ).attr( 'style' , 'display:none');
												   swal("No orders found to flush!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						swal("Cancelled", "", "error");
					}
				});
			}
			else
			{
				
				swal({
					title: "Are you sure?",
					text: "have you filled eDoc information. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						var strAction = "update";
						/* Start here updating section */
						$.ajax(
						{
						'url'            : '/System/eDoc',
						'type'           : 'POST',
						'data'           : { serviceProvider : serviceProvider },
						'success'        : function( data )
										   {										   
											   if( data != 'blank' )
											   {
													//$('.autofocus').val('');
													//window.open(data,'_blank' );
													//location.reload();
													
													$('input[type=text].countEditBox').val('0');
													$('input[type=text].countEditBox').val('0');
													$( ".outerOpac" ).attr( 'style' , 'display:none');
													
											   }
											   else
											   {
												   $( ".outerOpac" ).attr( 'style' , 'display:none');
												   swal("No orders found to flush!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						swal("Cancelled", "", "error");
					}
				});
			}
			 
		 });
		
		function getResult( data )
		{
			return data;
		}
		
		
		 
		
		 
		 
		  $( 'body' ).on( 'click', '.cut_off_manifest', function()
		 {
			var country = $(this).attr('id');
			var getTime = $.ajax({				     
					url: "/Directposts/getClientTime",
					type : "Post",
					data : { country : country },					
					dataType: 'text',					
					global: false,
					async:false,
					success: function(data) {
						return data;
					}
			}).responseText;

			if( getTime == '0' )
			{
				swal({
					title: "Are you sure?",
					text: "You are clearing data After Cut off. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						var strAction = "update";
						/* Start here updating section */
						$.ajax(
						{
						'url'            : '/Directposts/getGermanyAndFranceManifest',
						'type'           : 'POST',
						'data'           : { country : country },
						'success'        : function( data )
										   {	
										   //alert(data);									   
											   if( data != 'blank' )
											   {
													window.open(data,'_blank' );
													location.reload();
													
											   }
											   else
											   {
												   swal("No orders found to flush!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						swal("Cancelled", "", "error");
					}
				});
			}
			else
			{
				
				swal({
					title: "Are you sure?",
					text: "have you filled eDoc information. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						var strAction = "update";
						/* Start here updating section */
						$.ajax(
						{
						'url'            : '/Directposts/getGermanyAndFranceManifest',
						'type'           : 'POST',
						'data'           : { country : country },
						'success'        : function( data )
										   {										   
											   if( data != 'blank' )
											   {
												   $('.autofocus').val('');
													location.reload( true  );
													window.open(data,'_blank' );
											   }
											   else
											   {
												   swal("No orders found to flush!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						swal("Cancelled", "", "error");
					}
				});
			}			 
		 });
		 
	});
</script>


