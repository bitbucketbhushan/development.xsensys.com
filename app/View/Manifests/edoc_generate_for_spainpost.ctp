<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <?php
			
			$getOperatorInformation = $this->Session->read('Auth.User') ;	
			date_default_timezone_set('Europe/Jersey');			
			
		?>        
        <h1 class="page-title">New Docket <?php echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]'; ?></h1>
			
        <div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
						<li>
							<button type="button" id="Spain Post" class="cut_off_manifest_back_up btn btn-success no-margin">Back Up</button>
						</li>
<li>
										<div class="btn-group manifest_button" style="display:none">
										
										  <button type="button" id="Spain Post" class="cut_off_spain_post btn btn-success no-margin">Generate Manifest</button>
										  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										  </button>
										  <?php if(count($files) > 0 ) { ?>
										  <ul class="dropdown-menu scrollable-menu" role="menu">
												<?php 
													$manifestPath = Router::url('/', true).'/img/cut_off/'.$folderName.'/';													
													foreach( $files as $file )
													{
														$fileName = explode('-', $file) ;
														//File name
														$fname= $fileName[0];
														//get date
														$dateValue = explode( '_', $fileName[1] );
														$specificDate = $dateValue[0];
														//get time specific
														$minute = explode('.', $dateValue[2]) ;
														$timeValue = $dateValue[1] .':' . $minute[0];
												?>
												<?php
												
													if( $fname == "SpainPost" )
													{
												?>
															<li><a href="<?php echo $manifestPath.$file; ?>" target ="_blank" > <?php print $fname .'('.$timeValue.')' ; ?> </a></li>
												<?php
													} }
												?>
											
										  </ul>
										  <?php }?>
										</div>
							</li>

						
						</ul>
					</div>
				</div>
        
		
		
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div>
		
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">
                                <div class="panel-body padding-bottom-40 padding-top-40">
                                
									<?php								
									 echo $this->Form->button(
										'Generate eDoc (Spain)', 
										array(
										'formaction' => Router::url(
											array('controller' => '/JijGroup/System/JP/eDoc/Create')
										 ),
										'escape' => true,
										'class'=>'btn cut_off bg-red-400 color-white btn-dark col-sm-12 margin-bottom-10',
										'id' => $servicePost,
										'type' => 'submit'	
										)
									);
									echo $this->form->end();	
									?>
                                
									<table class="tbl_table docketTable" id="docket">
									   <tbody>
										  <tr>
											 <td style="padding-bottom:10px;" colspan="11">
												<hr class="bannerLine">
											 </td>
										  </tr>
										  <tr>
											 <td class="docketHeaderItem" colspan="9">A UK</td>
											 <td class="docketHeaderItem" colspan="2">New</td>
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <td class="docketTableItemSeparator"></td>
											 <th class="docketTableHeader">Items</th>
											 <th class="docketTableHeader">Bags</th>
											 <th class="docketTableHeader">Item Rate</th>
											 <th class="docketTableHeader">Weight</th>
											 <th class="docketTableHeader">Kilo Rate</th>
											 <th class="docketTableHeader">Item Amount</th>
											 <th class="docketTableHeader">Kilo Amount</th>
											 <th class="docketTableHeader">Total Amount</th>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="7" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SPR</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Small Packet - Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id = "SPRItems" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id = "SPRBages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.6097" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.0674" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£14.63</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£14.63</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="394" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">LU1</td>
											 <td style="text-align:left;color:#000000;background-color:#00FFFF;" class="docketTableItem">Packet L Blue - 750g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="LU1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="LU1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.8210" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£427.94</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£427.94</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="395" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">LU2</td>
											 <td style="text-align:left;color:#000000;background-color:#00FFFF;" class="docketTableItem">Packet L Blue - 1000g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="LU2Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="LU2Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="2.0350" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£712.25</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£712.25</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="398" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">BU1</td>
											 <td style="text-align:left;color:#FFFFFF;background-color:#0000FF;" class="docketTableItem">Packet Blue - 750g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="BU1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="BU1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.9693" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£462.79</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£462.79</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="399" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">BU2</td>
											 <td style="text-align:left;color:#FFFFFF;background-color:#0000FF;" class="docketTableItem">Packet Blue - 1000g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="BU2Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="BU2Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="2.1857" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£513.64</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£513.64</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="402" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GU1</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 750g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="2.9512" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£8.85</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£8.85</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="403" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GU2</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 1000g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU2Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU2Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.1150" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£155.75</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£155.75</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="404" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GU3</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 1500g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.2788" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£11,590.56</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£11,590.56</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="405" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GU4</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 2000g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU4Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU4Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.5518" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£124.31</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£124.31</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="406" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GU5</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 2500g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU5Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU5Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.5536" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£159.38</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£159.38</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="407" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GU6</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 3000g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU6Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU6Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.1094" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="408" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GU7</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 3500g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU7Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU7Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.6554" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="409" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GU8</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 4000g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU8Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU8Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="6.2014" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="410" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GU9</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 4500g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU9Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU9bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="6.7474" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="411" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">G10</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 5000g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU10Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU10Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="7.2934" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="412" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">G11</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 5500g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU11Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU11bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="7.8394" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="413" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">G12</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 6000g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU12Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU12Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="8.3854" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="414" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">G13</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 6500g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU13Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU13Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="8.9314" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="415" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">G14</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 7000g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU14Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU14Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="9.4774" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="416" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">G15</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 7500g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU15Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU15Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="10.0234" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="417" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">G16</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 8000g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU16Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU16Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="10.5694" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="418" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">G17</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 8500g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU17Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU17Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="11.1154" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="419" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">G18</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 9000g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU18Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU18Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="11.6614" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="420" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">G19</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 9500g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU19Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU19Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="12.2074" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="421" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">G20</td>
											 <td style="text-align:left;color:#000000;background-color:#00FF00;" class="docketTableItem">Packet Green - 10000g Unsorted</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GU20Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GU21Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="12.7534" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="A" class="tbl_row docketTableItemRow  ">
											 <td class="docketTableItemInvisible">
												<input value="A" name="docketid">
												<input value="26" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">RD3</td>
											 <td style="text-align:left;color:#000000;background-color:#FF9900;" class="docketTableItem">Recorded Delivery</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="RD3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItem">&nbsp;<input type="hidden" value="0" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags"></td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.3100" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">n/a</td>
											 <td class="docketTableItem">n/a<input type="hidden" value="0" data-tbl="{&quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="kilorate"></td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£1,784.67</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;A&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£1,784.67</span>    
											 </td>
										  </tr>
										  <tr data-tbltotal="A" class="docketTableTotalRow">
											 <td colspan="2" class="docketTableTotal">TOTAL</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
												<span data-tbl="{&quot;name&quot;:&quot;bags&quot;,&quot;fmt&quot;:&quot;num&quot;}" class="calculatedField">0</span>
											 </td>
											 <td class="docketTableTotal">
											 </td>
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
											 </td>
											 <td class="docketTableTotal">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">£15,954.77</span>    
											 </td>
											 <td class="docketTableTotal">   
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
											 </td>
											 <td class="docketTableTotal">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">£15,954.77</span>
											 </td>
										  </tr>
										  <tr>
											 <td style="padding-top:10px;padding-bottom:10px;" colspan="11">
												<hr class="bannerLine">
											 </td>
										  </tr>
										  <tr>
											 <td class="docketHeaderItem" colspan="9">B International Blended</td>
											 <td class="docketHeaderItem" colspan="2">New</td>
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <td class="docketTableItemSeparator"></td>
											 <th class="docketTableHeader">Items</th>
											 <th class="docketTableHeader">Bags</th>
											 <th class="docketTableHeader">Item Rate</th>
											 <th class="docketTableHeader">Weight</th>
											 <th class="docketTableHeader">Kilo Rate</th>
											 <th class="docketTableHeader">Item Amount</th>
											 <th class="docketTableHeader">Kilo Amount</th>
											 <th class="docketTableHeader">Total Amount</th>
										  </tr>
										  <tr data-tblblock="B" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="B" name="docketid">
												<input value="29" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">WW3</td>
											 <td style="text-align:left;color:#000000;background-color:#FF0000;" class="docketTableItem">Packet</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="WW3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="WW3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.6900" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.2700" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="B" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="B" name="docketid">
												<input value="30" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">WW4</td>
											 <td style="text-align:left;color:#FFFFFF;background-color:#0000FF;" class="docketTableItem">Large Packet</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="WW4Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="WW4Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.0900" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.0800" data-tbl="{&quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;B&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tbltotal="B" class="docketTableTotalRow">
											 <td colspan="2" class="docketTableTotal">TOTAL</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
												<span data-tbl="{&quot;name&quot;:&quot;bags&quot;,&quot;fmt&quot;:&quot;num&quot;}" class="calculatedField">0</span>
											 </td>
											 <td class="docketTableTotal">
											 </td>
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
											 </td>
											 <td class="docketTableTotal">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>    
											 </td>
											 <td class="docketTableTotal">   
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
											 </td>
											 <td class="docketTableTotal">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
											 </td>
										  </tr>
										  <tr>
											 <td style="padding-top:10px;padding-bottom:10px;" colspan="11">
												<hr class="bannerLine">
											 </td>
										  </tr>
										  <tr>
											 <td class="docketHeaderItem" colspan="9">C International Country Specific</td>
											 <td class="docketHeaderItem" colspan="2">New</td>
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <td class="docketTableItemSeparator"></td>
											 <th class="docketTableHeader">Items</th>
											 <th class="docketTableHeader">Bags</th>
											 <th class="docketTableHeader">Item Rate</th>
											 <th class="docketTableHeader">Weight</th>
											 <th class="docketTableHeader">Kilo Rate</th>
											 <th class="docketTableHeader">Item Amount</th>
											 <th class="docketTableHeader">Kilo Amount</th>
											 <th class="docketTableHeader">Total Amount</th>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="710" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="37" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">PD1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Poland G &lt;20mm</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="PD1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="PD1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.3600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.4400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£4.32</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">£4.32</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="711" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="37" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">PD2</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Poland E &gt;20mm</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="PD2Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="PD2Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.5600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.4400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="202" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="23" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">OZ1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Australia G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="OZ1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="OZ2Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.4000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="7.9600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="204" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="23" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">OZ3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Australia E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="OZ3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="OZ3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.5600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="7.9600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="162" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="1" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">AU1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Austria G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="AU1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="AU2Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.6100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.7600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="164" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="1" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">AU3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Austria E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="AU3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="AU3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.1900" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.5500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="142" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="2" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">BE1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Belgium G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="BE1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="BE1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.4900" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.1800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="144" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="2" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">BE3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Belgium E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="BE3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="BE3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.8900" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.8700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="231" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="25" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">CA1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Canada G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="CA1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="CA1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.4100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.6700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="233" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="25" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">CA3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Canada E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="CA3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="CA3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.5700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.6700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="190" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="16" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">DE1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Denmark G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="DE1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="DE1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.7800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="8.8600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="192" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="16" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">DE3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Denmark E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="DE3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="DE3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.6400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.7000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="146" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="17" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">FN1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Finland G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="FN1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="FN1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.5500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.8100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="148" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="17" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">FN3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Finland E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="FN3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="FN3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.0400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.8100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="182" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="9" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">FR1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">France G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="FR1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="FR1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.5200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.9400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="184" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="9" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">FR3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">France E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="FR3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="FR3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.7200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.7000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="178" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="3" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GE1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Germany G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6"  id="GE1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3"  id="GE1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.0200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.7500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="180" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="3" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GE3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Germany E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GE3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GE3bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.6100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="2.4500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="261" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="35" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GR1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Greece G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GR1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GR1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.7400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.8700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="263" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="35" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GR3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Greece E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GR3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GR3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.2400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.8700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="174" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="6" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">HO1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Holland G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="HO1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="HO1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.3400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.9400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="176" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="6" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">HO3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Holland E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="HO3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="HO3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.8100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.5300" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="366" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="26" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">IC1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Iceland G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="IC1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="IC1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.5200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.3800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="367" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="26" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">IC3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Iceland E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="IC3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="IC3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.0400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.5400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="166" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="18" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">IR1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Ireland G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="IR1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="IR1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.6500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.2700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="168" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="18" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">IR3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Ireland E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="IR3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="IR3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.0200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.0800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="194" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="5" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">IT1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Italy G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="IT1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="IT1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.6300" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.9200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="196" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="5" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">IT3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Italy E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="IT3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="IT3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.9100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.4800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="368" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="27" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">JP1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Japan G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="JP1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="JP1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.4600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.8700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="369" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="27" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">JP3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Japan E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="JP3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="JP3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.6100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.8700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="343" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="28" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">LX1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Luxembourg G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="LX1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="LX1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.6800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.8900" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="370" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="28" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">LX3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Luxembourg E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="LX3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="LX3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.9300" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.7500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="222" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="24" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">ML1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Malta G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="ML1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="ML1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.7500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.4800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="224" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="24" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">ML3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Malta E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="ML3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="ML3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.9100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.4800" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="227" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="29" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">NZ1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">New Zealand G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="NZ1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="NZ1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.4500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="7.7300" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="229" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="29" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">NZ3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">New Zealand E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="NZ3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="NZ3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.6000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="7.7300" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="154" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="19" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">NR1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Norway G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="NR1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="NR1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.7900" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="7.5500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="156" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="19" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">NR3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Norway E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="NR3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="NR3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.8600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="6.0200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="138" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="15" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">PO1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Portugal G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="PO1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="PO1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.4600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.0500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="140" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="15" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">PO3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Portugal E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="PO3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="PO3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.6400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.8900" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="374" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="30" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SA1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">South Africa G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SA1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SA1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="8.0200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="375" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="30" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SA3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">South Africa E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SA3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SA3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.2200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="8.0200" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="198" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="4" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SP1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Spain G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SP1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SP1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.3700" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.6100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="200" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="4" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SP3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Spain E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SP3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SP3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.5400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.4400" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="186" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="7" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SW1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Sweden G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SW1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SW1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.4600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.2500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="188" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="7" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SW3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Sweden E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SW3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SW3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.6500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.2000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="150" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="20" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SZ1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Switzerland G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SZ1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SZ1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.7000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.8000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="152" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="20" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SZ3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Switzerland E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SZ3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SZ3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.1100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.4600" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="158" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="22" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">US1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">USA G &lt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="US1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="US1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.3500" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.1000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="C" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="C" name="docketid">
												<input value="160" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="22" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">US3</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">USA E &gt; 20mm (M)</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="US3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="US3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.5100" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.1000" data-tbl="{&quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;C&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tbltotal="C" class="docketTableTotalRow">
											 <td colspan="2" class="docketTableTotal">TOTAL</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
												<span data-tbl="{&quot;name&quot;:&quot;bags&quot;,&quot;fmt&quot;:&quot;num&quot;}" class="calculatedField">0</span>
											 </td>
											 <td class="docketTableTotal">
											 </td>
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
											 </td>
											 <td class="docketTableTotal">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">£4.32</span>    
											 </td>
											 <td class="docketTableTotal">   
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
											 </td>
											 <td class="docketTableTotal">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">£4.32</span>
											 </td>
										  </tr>
										  <tr>
											 <td style="padding-top:10px;padding-bottom:10px;" colspan="11">
												<hr class="bannerLine">
											 </td>
										  </tr>
										  <tr>
											 <td class="docketHeaderItem" colspan="9">E International Direct Mail</td>
											 <td class="docketHeaderItem" colspan="2">New</td>
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <td class="docketTableItemSeparator"></td>
											 <th class="docketTableHeader">Items</th>
											 <th class="docketTableHeader">Bags</th>
											 <th class="docketTableHeader">Item Rate</th>
											 <th class="docketTableHeader">Weight</th>
											 <th class="docketTableHeader">Kilo Rate</th>
											 <th class="docketTableHeader">Item Amount</th>
											 <th class="docketTableHeader">Kilo Amount</th>
											 <th class="docketTableHeader">Total Amount</th>
										  </tr>
										  <tr data-tblblock="E" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="E" name="docketid">
												<input value="385" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="3" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">DP1</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Deutsche Post - Germany</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="DP1Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="DP1Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.8950" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.5997" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="E" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="E" name="docketid">
												<input value="799" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="3" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">DP2</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Deutsche Post - High Value</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="DP2Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="DP2Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.8535" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.5997" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="E" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="E" name="docketid">
												<input value="393" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="9" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">FR7</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">La Poste - France</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="FR7Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="FR7Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.8610" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.2416" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  
										  <!-- -->
										  
										  <tr data-tblblock="E" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="E" name="docketid">
												<input value="393" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="9" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SP7</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Spain Direct Under 20mm</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SP7Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SP7Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.7795" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.6399" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  
										  <tr data-tblblock="E" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="E" name="docketid">
												<input value="393" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="9" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SP8</td>
											 <td style="text-align:left;color:#000000;background-color:#FFFFFF;" class="docketTableItem">Spain Direct Over 20mm</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SP8Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SP8Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.0095" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="1.6399" data-tbl="{&quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;E&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  
										  <!------>
										  
										  
										  
										  
										  <tr data-tbltotal="E" class="docketTableTotalRow">
											 <td colspan="2" class="docketTableTotal">TOTAL</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
												<span data-tbl="{&quot;name&quot;:&quot;bags&quot;,&quot;fmt&quot;:&quot;num&quot;}" class="calculatedField">0</span>
											 </td>
											 <td class="docketTableTotal">
											 </td>
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
											 </td>
											 <td class="docketTableTotal">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>    
											 </td>
											 <td class="docketTableTotal">   
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
											 </td>
											 <td class="docketTableTotal">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
											 </td>
										  </tr>
										  <tr>
											 <td style="padding-top:10px;padding-bottom:10px;" colspan="11">
												<hr class="bannerLine">
											 </td>
										  </tr>
										  <tr>
											 <td class="docketHeaderItem" colspan="9">J Premium Products</td>
											 <td class="docketHeaderItem" colspan="2">New</td>
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <td class="docketTableItemSeparator"></td>
											 <th class="docketTableHeader">Items</th>
											 <th class="docketTableHeader">Bags</th>
											 <th class="docketTableHeader">Item Rate</th>
											 <th class="docketTableHeader">Weight</th>
											 <th class="docketTableHeader">Kilo Rate</th>
											 <th class="docketTableHeader">Item Amount</th>
											 <th class="docketTableHeader">Kilo Amount</th>
											 <th class="docketTableHeader">Total Amount</th>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="805" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">IR9</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Ireland Tracked Rate</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="IR9Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="IR9Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.5200" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.0800" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="797" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">FR9</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">France Tracked &amp; Signed</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="FR9Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="FR9Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.5000" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.7000" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="801" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">GE9</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Germany Tracked Service Bulk</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="GE9Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="GE9Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.7700" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="2.8600" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="796" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">IT9</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Italy Tracked &amp; Signed</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="IT9Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="IT9Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.4800" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.4800" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="798" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SP9</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Spain Tracked Bulk</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SP9Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SP9Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.2200" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.4400" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="802" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">US9</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">USA Tracked</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="US9Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="US9Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="3.3000" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.1000" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="381" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">AS3</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Bulk Airsure Europe</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="AS3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="AS3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.9500" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.4300" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="382" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">AS4</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Airsure International Bulk</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="AS4Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="AS4Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="5.9500" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="6.4900" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="793" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">AS5</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">International Signed Bulk</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="AS5Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="AS5Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="6.1700" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="6.3900" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="794" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">AS6</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">International Tracked &amp; Signed Bulk</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="AS6Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="AS6Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="6.1700" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="6.3900" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="383" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">ISB</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Bulk International Signed For</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="ISBItems" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="ISBBages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="6.1100" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="6.3200" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="380" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SDJ</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Bulk Special Delivery</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SDJItems" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SDJBages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="4.5400" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="2.4100" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="791" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SD2</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Special Delivery Bulk UK Max 15 kilos</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SD2Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SD2Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="33.4800" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tblblock="J" class="tbl_row docketTableItemRow  nonSurcharge">
											 <td class="docketTableItemInvisible">
												<input value="J" name="docketid">
												<input value="792" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
												<input value="0" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
											 </td>
											 <td style="text-align:left;color:Black;" class="docketTableItem">SD3</td>
											 <td style="text-align:left;color:#000000;background-color:#FF8080;" class="docketTableItem">Special Delivery Bulk UK Max 20 kilos</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="6" id="SD3Items" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="items">
											 </td>
											 <td class="docketTableItemEditable">
												<input type="text" value="0" maxlength="3" id="SD3Bages" class="countEditBox autofocus recalculate" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;}" name="bags">
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="43.2600" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="itemrate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">    
												<label data-tbl="{&quot;name&quot;:&quot;weight&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;3&quot;}">0.000</label> 
											 </td>
											 <td class="docketTableItem">
												<input readonly="readonly" value="0.0000" data-tbl="{&quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;4&quot;}" name="kilorate" class="nonEditableInput">
											 </td>
											 <td class="docketTableItem">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>
											 </td>
											 <td class="docketTableItem">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;, &quot;total&quot;:&quot;J&quot;, &quot;fmt&quot;:&quot;num&quot;,&quot;dp&quot;:&quot;2&quot;,&quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}">-</span>    
											 </td>
										  </tr>
										  <tr data-tbltotal="J" class="docketTableTotalRow">
											 <td colspan="2" class="docketTableTotal">TOTAL</td>
											 <td class="docketTableItemSeparator">
											 </td>
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
												<span data-tbl="{&quot;name&quot;:&quot;bags&quot;,&quot;fmt&quot;:&quot;num&quot;}" class="calculatedField">0</span>
											 </td>
											 <td class="docketTableTotal">
											 </td>
											 <td class="docketTableTotal">&nbsp;</td>
											 <td class="docketTableTotal">
											 </td>
											 <td class="docketTableTotal">  
												<span data-tbl="{&quot;name&quot;:&quot;itemamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>    
											 </td>
											 <td class="docketTableTotal">   
												<span data-tbl="{&quot;name&quot;:&quot;kiloamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
											 </td>
											 <td class="docketTableTotal">
												<span data-tbl="{&quot;name&quot;:&quot;totalamount&quot;,&quot;fmt&quot;:&quot;num&quot;, &quot;dp&quot;:&quot;2&quot;, &quot;zero&quot;:&quot;-&quot;,&quot;ccy&quot;:&quot;£&quot;}" class="calculatedField">-</span>
											 </td>
										  </tr>
										  <tr>
											 <td style="padding-top:10px;padding-bottom:10px;" colspan="11">
												<hr class="bannerLine">
											 </td>
										  </tr>
									   </tbody>
									</table>
								</div>
							</div>
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>        
		</div>


<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>


<script>

$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "eShipper is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off', function()
		 {
			
			console.log( "****************************" );                    
			console.log( "eShipper is fetching all information" );
			console.log( "****************************" );	
			
			//First, will clear previous filled inputs
			$( '.countEditBox' ).val('0');
			
			var serviceProvider = $(this).attr('id');			
			// Data fetching from this			
			var getEshipper = $.ajax(
			{
				'url'            : '/Cronjobs/setInfoSpainEdoc',
				'type'           : 'POST',
				'data'           : { serviceProvider : serviceProvider },
				'beforeSend'	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
				  },
				global: false,
				async:false,
				'success'        : function( msgArray )
									{	
										
											 $( ".outerOpac" ).attr( 'style' , 'display:none');
											var json = JSON.parse(msgArray);
											var getData = json.data;
											
											 // Get the value "long_name" from the long_name key
												 for (var i = 0, max = getData.length; i < max; i++)
												 {
														 var counter = getData[i].ServiceCounter.counter;
														 var bags = getData[i].ServiceCounter.bags;
														 var serviceCode = $.trim(getData[i].ServiceCounter.service_code);
														 
														 if( $("#"+serviceCode+'Items').val().length == 0 )
														 {
															 $("#"+serviceCode+'Items').val(counter);
															 $("#"+serviceCode+'Bages').val(bags);
															 
															 counter = 0;
															 bags = 0;
														 }
														 else
														 {
															 counter = parseInt(counter) + parseInt($("#"+serviceCode+'Items').val());
															 bags = parseInt(bags) + parseInt($("#"+serviceCode+'Bages').val());
															 
															 $("#"+serviceCode+'Items').val(counter);
															 $("#"+serviceCode+'Bages').val(bags);
															 
															 counter = 0;
															 bags = 0;
														 }
														 
												 }	

									}
			}).responseText;
			
			
			return false;
			
			console.log( "****************************" );                    
			console.log( "Now input" );
			console.log( "****************************" );	
		 });
	});
</script>
<script>
	$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "Euraco is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off_spain_post', function()
		 {
			 
			 var serviceProvider = $(this).attr('id');				 		 			 
			//Get time
			var getTime = $.ajax({				     
					url: "/Cronjobs/getClientTime",
					type : "Post",
					data : { serviceProvider : serviceProvider },					
					dataType: 'text',					
					global: false,
					async:false,
					success: function(data) {
						return data;
					}
			}).responseText;

			if( getTime == '0' )
			{
				swal({
					title: "Are you sure?",
					text: "You are clearing data After Cut off. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						var strAction = "update";
						/* Start here updating section */
						$.ajax(
						{
						'url'            : '/System/eDoc/Spain',
						'type'           : 'POST',
						'data'           : { serviceProvider : serviceProvider },
						'success'        : function( data )
										   {										   
											   if( data != 'blank' )
											   {
													window.open(data,'_blank' );
													location.reload();
													
											   }
											   else
											   {
												   swal("No orders found to flush!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						swal("Cancelled", "", "error");
					}
				});
			}
			else
			{
				
				swal({
					title: "Are you sure?",
					text: "have you filled eDoc information. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						var strAction = "update";
						/* Start here updating section */
						$.ajax(
						{
						'url'            : '/System/eDoc/Spain',
						'type'           : 'POST',
						'data'           : { serviceProvider : serviceProvider },
						'success'        : function( data )
										   {										   
											   if( data != 'blank' )
											   {
												   $('.autofocus').val('');
													window.open(data,'_blank' );
													location.reload();
													
											   }
											   else
											   {
												   swal("No orders found to flush!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						swal("Cancelled", "", "error");
					}
				});
			}			 
		 });
		 
		 //Set Backup
		 $( 'body' ).on( 'click', '.cut_off_manifest_back_up', function()
		 {
			 var userId = <?php echo $getOperatorInformation['id']; ?>;
			 var servicePost = $(this).attr('id');
			 
			 $.ajax(
				{
				'url'            : '/Uploads/serviceCounterBackUp',
				'type'           : 'POST',
				'data'           : { userId : userId , servicePost : servicePost },
				'beforeSend'	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
				  },
				'success'        : function( data )
								   {										   
									   if( data != 'blank' )
									   {
										    $( ".outerOpac" ).attr( 'style' , 'display:none');
										    $( ".manifest_button" ).attr( 'style' , 'display:inline-block');
										    $( ".cut_off_manifest_back_up" ).attr( 'style' , 'display:none');
									   }
									   else
									   {
										   $( ".outerOpac" ).attr( 'style' , 'display:none');	 
									   }
								   }
				});			 
		 });	
		 
	});
</script>
