<div class=" container-fluid wrapper">	
   <div class="rightside bg-grey-100">
                
	<section class="content-header">
			 <div class="d-flex justify-content-between bg-light mb-3 mt-3" >
				<div style="float:left;"><h2>New Docket</h2></div>
			   <div style="float:right;"><button type="button" class="btn btn-success">eDoc Done</button> <button type="button" class="btn btn-success">Back up</button></div>
			</div>
    </section>
	
    
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">
                                <div class="panel-body padding-bottom-40 padding-top-40">
                                
									<button class="cut_off_edoc_jp btn btn-danger btn-sm btn-block p-2" id="Jersey Post" type="submit">Generate eDoc</button>    
									<table class="tbl_table docketTable table table-borderless" id="docket1" style="">
										<tr><td style="padding-bottom:10px;" colspan="11"></td></tr>
										  <tr>
											 <td class="docketHeaderItem" colspan="4">UK/Jersey</td>
										  </tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader" style="text-align: left;">Items</th>
											 <th class="docketTableHeader" style="text-align: left;">Bags</th>
										  </tr>
										  	<tr data-tblblock="A" id="UK5Show" style="" >
												<td >UK5</td>
												<td>Tracked &Signed (Comp up to &pound;500)</td>
												<td> <input  type="text"  id="UK5Items"  maxlength="6" value="0"> </td>
												<td><input  type="text"   id="UK5Bags" maxlength="6" value="0"></td>	
											</tr>
										  	<tr data-tblblock="A" id="UK1Show" style="" >
												<td >UK1</td>
												<td>Tracked & Signed (Comp upto &pound;1000)</td>
												<td> <input  type="text"  id="UK1Items"  maxlength="6" value="0"> </td>
												<td><input  type="text"   id="UK1Bags" maxlength="6" value="0"></td>	
											</tr>
										  	<tr data-tblblock="A" id="UK2Show" style="" >
												<td >UK2</td>
												<td>Tracked & Signed (Comp upto &pound;2500)</td>
												<td> <input  type="text"  id="UK2Items"  maxlength="6" value="0"> </td>
												<td><input  type="text"   id="UK2Bags" maxlength="6" value="0"></td>	
											</tr>
											<tr data-tblblock="A" id="UKFShow" style="" >
												<td >UKF</td>
												<td>UK Packet - Signed For </td>
												<td> <input  type="text"  id="UKFItems"  maxlength="6" value="0"> </td>
												<td><input  type="text"   id="UKFBags" maxlength="6" value="0"></td>	
											</tr>
											<tr id="UKEShow" style="">
												<td > UKE  </td>	
												<td>UK Packet - Economy </td>
												<td><input type="text" value="0" maxlength="6" id="UKEItems"></td>
												<td><input type="text" value="0" maxlength="6" id="UKEBags"> </td>									 
											</tr>
											<tr id="UKPShow" style="">
												<td>UKP </td>
												<td>UK Packet - Priority </td>
												<td><input type="text" value="0" maxlength="6" id="UKPItems"></td>
												<td><input type="text" value="0" maxlength="6" id="UKPBags"></td>
											</tr>	
											<tr id="UKOShow" style="">
												<td >UKO  </td>
												<td>UK Parcel - T&S Over 2kg </td>
												<td><input type="text" value="0" maxlength="6" id="UKOItems"></td>
												<td><input type="text" value="0" maxlength="6" id="UKOBags"></td>
											</tr>
									</table>
									<!-----------------------------International Blended Start ----------------------------------->
									<table class="tbl_table docketTable table table-borderless" id="docket2" style="">
										  <tr><td style="padding-bottom:10px;" colspan="11"></td></tr>
										  <tr><td class="docketHeaderItem" colspan="4">International Blended</td></tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader" style="text-align: left;" >Items</th>
											 <th class="docketTableHeader" style="text-align: left;" >Bags</th>
										  </tr>
											<tr id="RWEShow" style="">
												<td >RWE  </td>
												<td>RoW - Economy </td>
												<td><input type="text" value="0" maxlength="6" id="RWEItems"></td>
												<td><input type="text" value="0" maxlength="6" id="RWEBags"> </td>
											</tr>
											<tr id="EREShow" style="">
												<td >ERE </td>
												<td>Europe - Economy </td>
												<td><input type="text" value="0" maxlength="6" id="EREItems"></td>
												<td><input type="text" value="0" maxlength="6" id="EREBags"> </td>
											</tr>	 
									</table>
									
									<!--------------------------------------International Blended End------------------------------------------>
									
									<!-----------------------------C International Country Specific Start ----------------------------------->
									
									
									<table class="tbl_table docketTable table table-borderless" id="docket" style="">
										  <tr><td style="padding-bottom:10px;" colspan="11"></td></tr>
									 	  <tr><td class="docketHeaderItem" colspan="4"> C International Country Specific </td></tr>
										  <tr class="docketTableHeaderRow" >
												 <th class="docketTableHeader"></th>
												 <th class="docketTableHeader"></th>
												 <th class="docketTableHeader" style="text-align: left;" >Items</th>
												 <th class="docketTableHeader" style="text-align: left;" >Bags</th>
										  </tr>
										  <tr id="ESEShow" style="" >
												<td> ESE  </td>
												<td>Spain - Economy </td>
												<td><input  type="text"   maxlength="6" id="ESEItems" value="0"></td>                       
												<td><input  type="text"   maxlength="6" id="ESEBags" value="0"></td>
										   </tr>	
											<tr id="DEEShow" style="">
												<td >DEE </td>
												<td >Germany - Economy </td>
												<td ><input  type="text"   maxlength="6" id="DEEItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="DEEBags" value="0"></td>
											</tr>
					
										<tr id="DKEShow" style="" >
											<td> DKE </td>
											<td >Denmark - Economy </td>
											<td ><input  type="text"   maxlength="6" id="DKEItems" value="0"></td>
											<td><input  type="text"   maxlength="6" id="DKEBags" value="0"></td>       
										</tr>
										<tr id="FIEShow" style="">
											<td >FIE </td>
											<td >Finland - Economy </td>
											<td ><input  type="text"   maxlength="6" id="FIEItems" value="0"></td>
											<td><input  type="text"   maxlength="6" id="FIEBags" value="0"></td> 
										</tr>
										<tr id="GREShow" style="">
											<td> GRE  </td>
											<td >Greece - Economy </td>
											<td ><input  type="text"   maxlength="6" id="GREItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="GREBags" value="0"> </td>
										</tr>   
										<tr id="ATEShow" style="" >
											<td> ATE  </td>
											<td >Austria - Economy </td>
											<td> <input  type="text"   maxlength="6" id="ATEItems" value="0"></td>
											<td><input  type="text"   maxlength="6" id="ATEBags" value="0"> </td>
										</tr>
										<tr id="AUEShow" style="" >
											<td>AUE </td>
											<td>Australia - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="AUEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="AUEBags"></td> 
										</tr>
										<tr id="BEEShow" style="" >
											<td>BEE </td>
											<td>Belgium - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="BEEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="BEEBags"></td>
										</tr>
										<tr id="FREShow" style="" >
											<td >FRE </td>
											<td>France - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="FREItems"></td>
											<td><input type="text" value="0" maxlength="6" id="FREBags"></td>
										</tr>
										<tr id="BGEShow" style="" >
											<td>BGE</td>
											<td>Belgium - Economy</td>
											<td><input type="text" value="0" maxlength="6" id="BGEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="BGEBags"></td>
										</tr>
										<tr id="BREShow" style="" >
											<td>BRE</td>
											<td>Brazil - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="BREItems"></td>
											<td><input type="text" value="0" maxlength="6" id="BREBags"></td>
										</tr>
										<tr id="CAEShow" style="" >
											<td>CAE</td>
											<td>Canada - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="CAEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="CAEBags"></td>
										</tr>
										<tr id="CHEShow" style="" >
											<td>CHE</td>
											<td>Switzerland - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="CHEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="CHEBags"></td>
										</tr>
										<tr id="CNEShow" style="" >
											<td >CNE </td>
											<td>China - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="CNEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="CNEBags"></td>
										</tr>
										<tr id="SEEShow" style="" >
											<td>SEE</td>
											<td>Sweden - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="SEEItems" ></td>
											<td><input type="text" value="0" maxlength="6" id="SEEBags"></td>
										</tr>
										<tr id="USEShow" style="">
											<td >USE </td>
											<td>United States of America - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="USEItems" /></td>
											<td><input type="text" value="0" maxlength="6" id="USEBags" /></td>
										</tr>
										<tr id="NOEShow" style="" >
											<td>NOE </td>
											<td>Norway - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="NOEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="NOEBags"></td>
										</tr>
										<tr id="NOEShow" style="" >
											<td>NZE</td>
											<td>New Zealand - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="NZEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="NZEBags"></td>
										</tr>
										<tr id="PLEShow" style="" >
											<td>PLE</td>
											<td>Poland - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="PLEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="PLEBags"></td>
										</tr>
										<tr id="PTEShow" style="" >
											<td>PTE</td>
											<td>Portugal - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="PTEItems"> </td>
											<td><input type="text" value="0" maxlength="6" id="PTEBags"></td>
										</tr>
										<tr id="IEEShow" style="" >
											<td>IEE</td>
											<td>Ireland - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="IEEItems" ></td>
											<td><input type="text" value="0" maxlength="6" id="IEEBags" ></td>
										</tr>
										<tr id="ISEShow" style="" >
											<td>ISE</td>
											<td>Iceland - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="ISEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="ISEBags"></td>
										</tr>
										<tr id="ITEShow" style="" >
											<td>ITE</td>
											<td>Italy - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="ITEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="ITEBags"></td>
										</tr>
										<tr id="JPEShow" style="" >
											<td>JPE</td>
											<td>Japan - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="JPEItems"></td>
											<td><input type="text" value="0" maxlength="6" id="JPEBags"></td>
										</tr>
										<tr id="LUEShow" style="" >
											<td>LUE</td>
											<td>Luxembourg - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="LUEItems" ></td>
											<td><input type="text" value="0" maxlength="6" id="LUEBags" ></td>
										</tr>
										<tr id="MTEShow" style="" >
											<td>MTE</td>
											<td>Malta - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="MTEItems"   ></td>
											<td><input type="text" value="0" maxlength="6" id="MTEBags"   ></td>
										</tr>
										<tr id="MXEShow" style="" >
											<td>MXE</td>
											<td>Mexico - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="MXEItems"   ></td>
											<td><input type="text" value="0" maxlength="6" id="MXEBags"   ></td>
										</tr>
										<tr id="NLEShow" style="" >
											<td>NLE</td>
											<td>Netherlands - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="NLEItems" ></td>
											<td><input type="text" value="0" maxlength="6" id="NLEBags" ></td>
										</tr>
										<tr id="RUEShow" style="" >
											<td>RUE</td>
											<td>Russian Federation - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="RUEItems"   ></td>
											<td><input type="text" value="0" maxlength="6" id="RUEBags"   ></td>
										</tr>
										<tr id="ZAEShow" style="" >
											<td>ZAE</td>
											<td>South Africa - Economy </td>
											<td><input type="text" value="0" maxlength="6" id="ZAEItems"   ></td>
											<td><input type="text" value="0" maxlength="6" id="ZAEBags"   ></td>
										</tr>
										<!--<tr data-tbltotal="C" class="docketTableTotalRow">
											<td colspan="2" class="docketTableTotal">TOTAL</td>
											<td class="docketTableTotal"><span  class="calculatedField">0</span></td>
											<td class="docketTableTotal"></td>
										</tr>-->
										<tr>
											<td style="padding-top:10px;padding-bottom:10px;" colspan="4"></td>
										</tr>
									</table>
									<!---------------------------------C International Country Specific End-------------------------------------------------->
									
									<!-----------------------------E International Direct Mail Start ----------------------------------->
									<table class="tbl_table docketTable table table-borderless" id="docket4" style="">
										  <tr><td style="padding-bottom:10px;" colspan="11"></td></tr>
										  <tr><td class="docketHeaderItem" colspan="4">E International Direct Mail</td></tr>
										  <tr class="docketTableHeaderRow">
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader"></th>
											 <th class="docketTableHeader" style="text-align: left;" >Items</th>
											 <th class="docketTableHeader" style="text-align: left;" >Bags</th>
										  </tr>
											<tr id="FRUShow" style="">
												<td >FRU  </td>
												<td>France (Under 520g) Direct Entry</td>
												<td><input type="text" value="0" maxlength="6" id="FRUItems"></td>
												<td><input type="text" value="0" maxlength="6" id="FRUBags"> </td>
											</tr>
											<tr id="FROShow" style="">
												<td >FRO</td>
												<td>France (Over 520g) Direct Entry</td>
												<td><input type="text" value="0" maxlength="6" id="FROItems"></td>
												<td><input type="text" value="0" maxlength="6" id="FROBags"> </td>
											</tr>
											<tr id="GYEShow" style="">
												<td >GYE</td>
												<td>Germany - Direct Entry</td>
												<td><input type="text" value="0" maxlength="6" id="GYEItems"></td>
												<td><input type="text" value="0" maxlength="6" id="GYEBags"> </td>
											</tr>	 
									</table>
									<!-----------------------------E International Direct Mail End------------------------->
									<!-----------------------------H Int Priority over 2kg Start ----------------------------------->
									<table class="tbl_table docketTable table table-borderless" id="docket3" style="">
										  <tr><td style="padding-bottom:10px;" colspan="11"></td></tr>
									 	  <tr><td class="docketHeaderItem" colspan="4"> H Int Priority over 2kg </td></tr>
										  <tr class="docketTableHeaderRow" >
												 <th class="docketTableHeader"></th>
												 <th class="docketTableHeader"></th>
												 <th class="docketTableHeader" style="text-align: left;" >Items</th>
												 <th class="docketTableHeader" style="text-align: left;" >Bags</th>
										  </tr>
										  <tr id="EROShow" style="" >
												<td>ERO</td>
												<td>Blended Europe - T&S Over 2kg </td>
												<td><input  type="text"   maxlength="6" id="EROItems" value="0"></td>                       
												<td><input  type="text"   maxlength="6" id="EROBags" value="0"></td>
										   </tr>	
											<tr id="RWOShow" style="">
												<td >RWO </td>
												<td >Blended ROW -T&S Over 2kg </td>
												<td ><input  type="text"   maxlength="6" id="RWOItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="RWOBags" value="0"></td>
											</tr>
					
										<tr id="ATOShow" style="" >
											<td> ATO </td>
											<td >Austria - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="ATOItems" value="0"></td>
											<td><input  type="text"   maxlength="6" id="ATOBags" value="0"></td>       
										</tr>
										<tr id="AUOShow" style="">
											<td >AUO</td>
											<td >Australia - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="AUOItems" value="0"></td>
											<td><input  type="text"   maxlength="6" id="AUOBags" value="0"></td> 
										</tr>
										<tr id="BEOShow" style="">
											<td> BEO  </td>
											<td >Belgium - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="BEOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="BEOBags" value="0"> </td>
										</tr>  
										<tr id="BROShow" style="">
											<td> BRO  </td>
											<td >Brazil - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="BROItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="BROBags" value="0"> </td>
										</tr>
										<tr id="CAOShow" style="">
											<td> CAO  </td>
											<td >Canada - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="CAOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="CAOBags" value="0"> </td>
										</tr>  
										<tr id="CHOShow" style="">
											<td> CHO  </td>
											<td >Switzerland - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="CHOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="CHOBags" value="0"> </td>
										</tr>  
										<tr id="CNOShow" style="">
											<td> CNO  </td>
											<td >China - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="CNOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="CNOBags" value="0"> </td>
										</tr>  
										<tr id="DEOShow" style="">
											<td> DEO  </td>
											<td >Germany - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="DEOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="DEOBags" value="0"> </td>
										</tr>  
										<tr id="DKOShow" style="">
											<td> DKO  </td>
											<td >Denmark - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="DKOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="DKOBags" value="0"> </td>
										</tr>
										<tr id="ESOShow" style="">
											<td> ESO  </td>
											<td >Spain - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="ESOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="ESOBags" value="0"> </td>
										</tr>
										<tr id="FIOShow" style="">
											<td> FIO  </td>
											<td >Finland - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="FIOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="FIOBags" value="0"> </td>
										</tr>
										<tr id="FRSShow" style="">
											<td> FRS  </td>
											<td >France - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="FRSItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="FRSBags" value="0"> </td>
										</tr>
										<tr id="GROShow" style="">
											<td> GRO  </td>
											<td >Greece - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="GROItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="GROBags" value="0"> </td>
										</tr>
										<tr id="IEOShow" style="">
											<td> IEO  </td>
											<td >Ireland - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="IEOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="IEOBags" value="0"> </td>
										</tr>
										<tr id="ISOShow" style="">
											<td> ISO  </td>
											<td >Iceland - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="ISOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="ISOBags" value="0"> </td>
										</tr>
										<tr id="ITOShow" style="">
											<td> ITO  </td>
											<td >Italy - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="ITOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="ITOBags" value="0"> </td>
										</tr>
										<tr id="JPOShow" style="">
											<td> JPO  </td>
											<td >Japan - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="JPOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="JPOBags" value="0"> </td>
										</tr>
										<tr id="LUOShow" style="">
											<td> LUO  </td>
											<td >Luxembourg - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="LUOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="LUOBags" value="0"> </td>
										</tr>
										<tr id="MTOShow" style="">
											<td> MTO  </td>
											<td >Malta - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="MTOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="MTOBags" value="0"> </td>
										</tr>
										<tr id="MXOShow" style="">
											<td> MXO  </td>
											<td >Mexico - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="MXOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="MXOBags" value="0"> </td>
										</tr>
										<tr id="NLOShow" style="">
											<td> NLO  </td>
											<td >Netherlands - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="NLOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="NLOBags" value="0"> </td>
										</tr>
										<tr id="NOOShow" style="">
											<td> NOO  </td>
											<td >Norway - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="NOOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="NOOBags" value="0"> </td>
										</tr>
										<tr id="NZOShow" style="">
											<td> NZO  </td>
											<td >New Zealand - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="NZOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="NZOBags" value="0"> </td>
										</tr>
										<tr id="PLOShow" style="">
											<td> PLO  </td>
											<td >Poland - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="PLOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="PLOBags" value="0"> </td>
										</tr>
										<tr id="PTOShow" style="">
											<td> PTO  </td>
											<td >Portugal - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="PTOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="PTOBags" value="0"> </td>
										</tr>
										<tr id="RUOShow" style="">
											<td> RUO  </td>
											<td >Russian Federation - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="RUOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="RUOBags" value="0"> </td>
										</tr>
										<tr id="SEOShow" style="">
											<td> SEO  </td>
											<td >Sweden - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="SEOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="SEOBags" value="0"> </td>
										</tr>
										<tr id="ZAOShow" style="">
											<td> ZAO  </td>
											<td >South Africa - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="ZAOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="ZAOBags" value="0"> </td>
										</tr>
										<tr id="HROShow" style="">
											<td> HRO  </td>
											<td >Croatia - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="HROItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="HROBags" value="0"> </td>
										</tr>
										<tr id="HKOShow" style="">
											<td> HKO  </td>
											<td >Hong Kong - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="HKOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="HKOBags" value="0"> </td>
										</tr>
										<tr id="CZOShow" style="">
											<td> CZO  </td>
											<td >Czech - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="CZOItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="CZOBags" value="0"> </td>
										</tr>
										<tr id="USIShow" style="">
											<td> USI  </td>
											<td >Czech - T&S Over 2kg</td>
											<td ><input  type="text"   maxlength="6" id="USIItems" value="0"></td>
											<td><input  type="text"  maxlength="6" id="USIBags" value="0"> </td>
										</tr>
										<tr>
											<td style="padding-top:10px;padding-bottom:10px;" colspan="4"></td>
										</tr>
									</table>
									
									<!-------------------------------H Int Priority over 2kg END-------------------------------------------->
									
									
									<!-----------------------------J Int Signed & Tracked <2kg----------------------------------->
									<table class="tbl_table docketTable table table-borderless" id="docket3" style="">
										  <tr><td style="padding-bottom:10px;" colspan="11"></td></tr>
									 	  <tr><td class="docketHeaderItem" colspan="4"> J Int Signed & Tracked <2kg</td></tr>
										  <tr class="docketTableHeaderRow" >
												 <th class="docketTableHeader"></th>
												 <th class="docketTableHeader"></th>
												 <th class="docketTableHeader" style="text-align: left;" >Items</th>
												 <th class="docketTableHeader" style="text-align: left;" >Bags</th>
										  </tr>
										  	<tr id="ERSShow" style="" >
												<td>ERS</td>
												<td>Blended Europe - Signed For</td>
												<td><input  type="text"   maxlength="6" id="ERSItems" value="0"></td>                       
												<td><input  type="text"   maxlength="6" id="ERSBags" value="0"></td>
										    </tr>	
											<tr id="RWSShow" style="">
												<td >RWS </td>
												<td >Blended ROW - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="RWSItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="RWSBags" value="0"></td>
											</tr>
											<tr id="ERTShow" style="">
												<td >ERT</td>
												<td >Blended Europe - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="ERTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="ERTBags" value="0"></td>
											</tr>
											<tr id="RWTShow" style="">
												<td >RWT</td>
												<td >Blended ROW - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="RWTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="RWTBags" value="0"></td>
											</tr>
											<tr id="ERPShow" style="">
												<td >ERP</td>
												<td >Blended Europe - Priority</td>
												<td ><input  type="text"   maxlength="6" id="ERPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="ERPBags" value="0"></td>
											</tr>
											<tr id="RWPShow" style="">
												<td >RWP</td>
												<td >Blended ROW - Priority</td>
												<td ><input  type="text"   maxlength="6" id="RWPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="RWPBags" value="0"></td>
											</tr>
											<tr id="AUSShow" style="">
												<td >AUS</td>
												<td >Australia - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="AUSItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="AUSBags" value="0"></td>
											</tr>
											<tr id="BRSShow" style="">
												<td >BRS</td>
												<td >Brazil - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="BRSItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="BRSBags" value="0"></td>
											</tr>
											<tr id="CASShow" style="">
												<td >CAS</td>
												<td >Canada - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="CASItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="CASBags" value="0"></td>
											</tr>
											<tr id="ESSShow" style="">
												<td >ESS</td>
												<td >Spain - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="ESSItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="ESSBags" value="0"></td>
											</tr>
											<tr id="GRSShow" style="">
												<td >GRS</td>
												<td >Greece - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="GRSItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="GRSBags" value="0"></td>
											</tr>
											<tr id="NLSShow" style="">
												<td >NLS</td>
												<td >Netherlands - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="NLSItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="NLSBags" value="0"></td>
											</tr>
											<tr id="NOSShow" style="">
												<td >NOS</td>
												<td >Norway - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="NOSItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="NOSBags" value="0"></td>
											</tr>
											<tr id="PLSShow" style="">
												<td >PLS</td>
												<td >Poland - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="PLSItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="PLSBags" value="0"></td>
											</tr>
											<tr id="PTSShow" style="">
												<td >PTS</td>
												<td >Portugal - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="PTSItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="PTSBags" value="0"></td>
											</tr>
											<tr id="ZASShow" style="">
												<td >PTS</td>
												<td >South Africa - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="ZASItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="ZASBags" value="0"></td>
											</tr>
											<tr id="ATTShow" style="">
												<td >ATT</td>
												<td >South Africa - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="ATTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="ATTBags" value="0"></td>
											</tr>
											<tr id="AUTShow" style="">
												<td >AUT</td>
												<td >Australia - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="AUTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="AUTBags" value="0"></td>
											</tr>
											<tr id="BETShow" style="">
												<td >BET</td>
												<td >Belgium - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="BETItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="BETBags" value="0"></td>
											</tr>
											<tr id="BRTShow" style="">
												<td >BRT</td>
												<td >Brazil - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="BRTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="BRTBags" value="0"></td>
											</tr>
											<tr id="CATShow" style="">
												<td >CAT</td>
												<td >Canada - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="CATItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="CATBags" value="0"></td>
											</tr>
											<tr id="CHTShow" style="">
												<td >CHT</td>
												<td >Switzerland - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="CHTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="CHTBags" value="0"></td>
											</tr>
											<tr id="CNTShow" style="">
												<td >CNT</td>
												<td >China - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="CNTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="CNTBags" value="0"></td>
											</tr>
											<tr id="DETShow" style="">
												<td >DET</td>
												<td >Germany - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="DETItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="DETBags" value="0"></td>
											</tr>
											<tr id="DKTShow" style="">
												<td >DKT</td>
												<td >Denmark - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="DKTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="DKTBags" value="0"></td>
											</tr>
											<tr id="ESTShow" style="">
												<td >EST</td>
												<td >Spain - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="ESTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="ESTBags" value="0"></td>
											</tr>
											<tr id="FITShow" style="">
												<td >FIT</td>
												<td >Finland - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="FITItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="FITBags" value="0"></td>
											</tr>
											<tr id="FRTShow" style="">
												<td >FRT</td>
												<td >France - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="FRTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="FRTBags" value="0"></td>
											</tr>
											<tr id="GRTShow" style="">
												<td >GRT</td>
												<td >Greece - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="GRTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="GRTBags" value="0"></td>
											</tr>
											<tr id="ISTShow" style="">
												<td >IST</td>
												<td >Iceland - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="ISTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="ISTBags" value="0"></td>
											</tr>
											<tr id="IETShow" style="">
												<td >IET</td>
												<td >Ireland - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="IETItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="IETBags" value="0"></td>
											</tr>
											<tr id="ITTShow" style="">
												<td >ITT</td>
												<td >Italy - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="ITTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="ITTBags" value="0"></td>
											</tr>
											<tr id="JPTShow" style="">
												<td >JPT</td>
												<td >Japan - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="JPTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="JPTBags" value="0"></td>
											</tr>
											<tr id="LUTShow" style="">
												<td >LUT</td>
												<td >Luxembourg - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="LUTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="LUTBags" value="0"></td>
											</tr>
											<tr id="MXTShow" style="">
												<td >MXT</td>
												<td >Mexico - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="MXTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="MXTBags" value="0"></td>
											</tr>
											<tr id="NLTShow" style="">
												<td >NLT</td>
												<td >Netherlands - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="NLTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="NLTBags" value="0"></td>
											</tr>
											<tr id="NOTShow" style="">
												<td >NOT</td>
												<td >Norway - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="NOTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="NOTBags" value="0"></td>
											</tr>
											<tr id="NZTShow" style="">
												<td >NZT</td>
												<td >Norway - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="NZTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="NZTBags" value="0"></td>
											</tr>
											<tr id="NZTShow" style="">
												<td >NZT</td>
												<td >New Zealand - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="NZTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="NZTBags" value="0"></td>
											</tr>
											<tr id="PLTShow" style="">
												<td >PLT</td>
												<td >Poland - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="PLTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="PLTBags" value="0"></td>
											</tr>
											<tr id="PTTShow" style="">
												<td >PTT</td>
												<td >Poland - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="PTTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="PTTBags" value="0"></td>
											</tr>
											<tr id="SETShow" style="">
												<td >SET</td>
												<td >Sweden - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="SETItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="SETBags" value="0"></td>
											</tr>
											<tr id="USTShow" style="">
												<td >UST</td>
												<td >United States of America - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="USTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="USTBags" value="0"></td>
											</tr>
											<tr id="ZATShow" style="">
												<td >ZAT</td>
												<td >South Africa - Tracked</td>
												<td ><input  type="text"   maxlength="6" id="ZATItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="ZATBags" value="0"></td>
											</tr>
											<tr id="ATPShow" style="">
												<td >ZAT</td>
												<td >Austria - Priority</td>
												<td ><input  type="text"   maxlength="6" id="ATPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="ATPBags" value="0"></td>
											</tr>
											<tr id="BEPShow" style="">
												<td >BEP</td>
												<td >Belgium - Priority</td>
												<td ><input  type="text"   maxlength="6" id="BEPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="BEPBags" value="0"></td>
											</tr>
											<tr id="CHPShow" style="">
												<td >CHP</td>
												<td >Switzerland - Priority</td>
												<td ><input  type="text"   maxlength="6" id="CHPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="CHPBags" value="0"></td>
											</tr>
											<tr id="DKPShow" style="">
												<td >DKP</td>
												<td >Denmark - Priority</td>
												<td ><input  type="text"   maxlength="6" id="DKPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="DKPBags" value="0"></td>
											</tr>
											<tr id="FIPShow" style="">
												<td >FIP</td>
												<td >Finland - Priority</td>
												<td ><input  type="text"   maxlength="6" id="FIPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="FIPBags" value="0"></td>
											</tr>
											<tr id="DEPShow" style="">
												<td >DEP</td>
												<td >Germany - Priority</td>
												<td ><input  type="text"   maxlength="6" id="DEPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="DEPBags" value="0"></td>
											</tr>
											<tr id="FRPShow" style="">
												<td >FRP</td>
												<td >France - Priority</td>
												<td ><input  type="text"   maxlength="6" id="FRPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="FRPBags" value="0"></td>
											</tr>
											<tr id="ISPShow" style="">
												<td >ISP</td>
												<td >Iceland - Priority</td>
												<td ><input  type="text"   maxlength="6" id="ISPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="ISPBags" value="0"></td>
											</tr>
											<tr id="IEPShow" style="">
												<td >IEP</td>
												<td >Ireland - Priority</td>
												<td ><input  type="text"   maxlength="6" id="IEPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="IEPBags" value="0"></td>
											</tr>
											<tr id="ITPShow" style="">
												<td >ITP</td>
												<td >Italy - Priority</td>
												<td ><input  type="text"   maxlength="6" id="ITPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="ITPBags" value="0"></td>
											</tr>
											<tr id="JPPShow" style="">
												<td >JPP</td>
												<td >Japan - Priority</td>
												<td ><input  type="text"   maxlength="6" id="JPPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="JPPBags" value="0"></td>
											</tr>
											<tr id="LUPShow" style="">
												<td >LUP</td>
												<td >Japan - Priority</td>
												<td ><input  type="text"   maxlength="6" id="LUPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="LUPBags" value="0"></td>
											</tr>
											<tr id="LUPShow" style="">
												<td >LUP</td>
												<td >Luxembourg - Priority</td>
												<td ><input  type="text"   maxlength="6" id="LUPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="LUPBags" value="0"></td>
											</tr>
											<tr id="MTPShow" style="">
												<td >MTP</td>
												<td >Malta - Priority</td>
												<td ><input  type="text"   maxlength="6" id="MTPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="MTPBags" value="0"></td>
											</tr>
											<tr id="MXPShow" style="">
												<td >MXP</td>
												<td >Mexico - Priority</td>
												<td ><input  type="text"   maxlength="6" id="MXPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="MXPBags" value="0"></td>
											</tr>
											<tr id="NZPShow" style="">
												<td >NZP</td>
												<td >New Zealand - Priority</td>
												<td ><input  type="text"   maxlength="6" id="NZPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="NZPBags" value="0"></td>
											</tr>
											<tr id="RUPShow" style="">
												<td >RUP</td>
												<td >Russian Federation - Priority</td>
												<td ><input  type="text"   maxlength="6" id="RUPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="RUPBags" value="0"></td>
											</tr>
											<tr id="SEPShow" style="">
												<td >SEP</td>
												<td >Sweden - Priority</td>
												<td ><input  type="text"   maxlength="6" id="SEPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="SEPBags" value="0"></td>
											</tr>
											<tr id="USPShow" style="">
												<td >USP</td>
												<td >United States of America - Priority</td>
												<td ><input  type="text"   maxlength="6" id="USPItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="USPBags" value="0"></td>
											</tr>
											<tr id="HKSShow" style="">
												<td >HKS</td>
												<td >Hong Kong - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="HKSItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="HKSBags" value="0"></td>
											</tr>
											<tr id="HKTShow" style="">
												<td >HKT</td>
												<td >Hong Kong - Signed For</td>
												<td ><input  type="text"   maxlength="6" id="HKTItems" value="0"></td>
												<td><input  type="text"   maxlength="6" id="HKTBags" value="0"></td>
											</tr>
					
										
										<tr>
											<td style="padding-top:10px;padding-bottom:10px;" colspan="4"></td>
										</tr>
									</table>
									<!-------------------------------H Int Priority over 2kg END-------------------------------------------->
									
									
									
									
									
									
									
									
								</div>
							</div>
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>        
		</div>

</div>
</div>


<script>

$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "eShipper is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off_edoc_jp', function()
		 {
			
			console.log( "****************************" );                    
			console.log( "eShipper is fetching all information" );
			console.log( "****************************" );	
			
			var serviceProvider = $(this).attr('id');
			// Data fetching from this			
			var getEshipper = $.ajax(
			{
				'url'            : '/Manifests/setInfoJerseyEdoc',
				'type'           : 'POST',
				'beforeSend'	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
				  },
				'data'           : { serviceProvider : serviceProvider },
				global: false,
				async:false,
				'success'        : function( msgArray )
									{	
											var json = JSON.parse(msgArray);
											var getData = json.data;
											var counteres = 0;
											 // Get the value "long_name" from the long_name key
												 for (var i = 0, max = getData.length; i < max; i++)
												 {
												 		 var counter = getData[i][0].counter;
														 var bags = getData[i].ServiceCounter.bags;
														 var serviceCode = getData[i].ServiceCounter.service_code;
														 //alert( serviceCode );
														if(serviceCode == 'mad_air' || serviceCode == 'cat_air' || serviceCode == 'sp_air' || serviceCode == 'mad_road' || serviceCode == 'cat_road' || serviceCode == 'sp_road')
														{
															serviceCode = 'ESE';
															esp3 = $('#ESEItems').val();
															counteres	=	parseInt(counter) + parseInt(esp3);
															$("#ESEItems").val(counteres);
															$("#ESEBags").val(bags);
															//counter = 0;
															//bags = 0;
														}
													
														 if( $("#"+serviceCode+'Items').val().length == 0 )
														 {
															 $("#"+serviceCode+'Items').val(counter);
															 $("#"+serviceCode+'Bags').val(bags);
															 
															 counter = 0;
															 bags = 0;
														 }
														 else
														 {
														 	 counter = parseInt(counter);
															 bags = parseInt(bags);
															 $("#"+serviceCode+'Items').val(counter);
															 $("#"+serviceCode+'Bags').val(bags);
															 $("#"+serviceCode+'Show').show();
															 $("#"+serviceCode+'Show').parent().parent().show();
															 counter = 0;
															 bags = 0;
														 }
														 
												 }
												 
												 $( ".outerOpac" ).attr( 'style' , 'display:none');	

									}
			}).responseText;
			
			
			return false;
			
			console.log( "****************************" );                    
			console.log( "Now input" );
			console.log( "****************************" );	
		 });
	});
	
</script>

<style>
	  .docketHeaderItem {
		font-size: medium;
		font-weight: bold;
		text-align: left;
		overflow: hidden;
		padding-left: 10px;
		background-color: #FFC0C0;
		color: Black;
	
	}
	.docketHeaderItem{text-align:center;}
	.content-header{margin:5px 15px; padding:5px 0px;}
	.docketTable input{border:none; background:none;}
	.docketTable td{font-weight:bold;}
	.docketTable th{font-weight:bold; text-align:center;}
	.wrapper{background:#f8f9fa;}
  </style>