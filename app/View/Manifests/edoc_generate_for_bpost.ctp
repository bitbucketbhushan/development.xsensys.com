<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">
		<?php
			$getOperatorInformation = $this->Session->read('Auth.User') ;	
			date_default_timezone_set('Europe/Jersey');			
		?>        
        <h1 class="page-title">New Docket <?php echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]'; ?></h1>
        
        <div class="submenu">
					<div class="navbar-header">
						<ul class="nav navbar-nav pull-left">
<li>
							<button type="button" id="Belgium Post" class="cut_off_manifest_back_up btn btn-success no-margin">Back Up</button>
						</li>
<li>
										<div class="btn-group manifest_button" style="display:none">
										
										  <button type="button" id="Belgium Post" class="cut_off_manifest btn btn-success no-margin">Generate Manifest</button>
										  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										  </button>
										  <?php if(count($files) > 0 ) { ?>
										  <ul class="dropdown-menu scrollable-menu" role="menu">
												<?php 
													$manifestPath = Router::url('/', true).'/img/cut_off/'.$folderName.'/';
													foreach( $files as $file )
													{
														$fileName = explode('-', $file) ;
														//File name
														$fname= $fileName[0];
														//get date
														$dateValue = explode( '_', $fileName[1] );
														$specificDate = $dateValue[0];
														//get time specific
														$minute = explode('.', $dateValue[2]) ;
														$timeValue = $dateValue[1] .':' . $minute[0];
												?>
												<?php
													if( $fname == "BelgiumPost" )
													{
												?>
															<li><a href="<?php echo $manifestPath.$file; ?>" target ="_blank" > <?php print $fname .'('.$timeValue.')' ; ?> </a></li>
												<?php
													} }
												?>
											
										  </ul>
										  <?php }?>
										</div>
							</li>

						
						</ul>
					</div>
				</div>
        
		
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div>
		
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">

                             <div class="panel-body padding-bottom-40 padding-top-10">
							 
							   
							<?php								
								echo $this->Form->button(
									'Generate eShipper', 
									array(
										'formaction' => Router::url(
											array('controller' => '/JijGroup/System/JP/eDoc/Create')
										 ),
										'escape' => true,
										'class'=>'btn cut_off bg-red-400 color-white btn-dark col-sm-12 margin-bottom-10',
										'id' => 'Belgium Post',
										'type' => 'submit'	
									)
								);
								echo $this->form->end();	
							?>
							 

								<form action="/BpiShippingCorner/borderel12handling.do" method="post" name="ebpi.Borderel12HandlingForm">
								   <div><input type="hidden" value="3b5919020fb2511ff406c26a704779f8" name="org.apache.struts.taglib.html.TOKEN"></div>
								   <!-- the action to use when the user presses enter -->
								   <input type="hidden" value="VolumailSortedSubmitGroups12Save" name="method">
								   <span class="fieldLabel02Std">¹This column should contain the total weight (pieces × weight per piece) for the zone or destination.</span>
								   <table class="inputBorderel table table-bordered table-hover">
									  <tbody>
										 <tr>
											<th rowspan="2">Zone1</th>
											<th colspan="2">P</th>
											<th colspan="2">G</th>
											<th colspan="2">E(bus)</th>
											<th colspan="2">E(bel)</th>
										 </tr>
										 <tr>
											<th>PCE</th>
											<th>PCE×KG¹</th>
											<th>PCE</th>
											<th>PCE×KG¹</th>
											<th>PCE</th>
											<th>PCE×KG¹</th>
											<th>PCE</th>
											<th>PCE×KG¹</th>
										 </tr>
										 <tr>
											<td><label accesskey="A" ><span class="accesskey">A</span>ustria</label></td>
											<td><input type="text" class="form-control normal AustriaPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal AustriaKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal AustriaPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal AustriaKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal AustriaPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal AustriaKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="B" ><span class="accesskey">B</span>ulgaria</label></td>
											<td><input type="text" class="form-control normal BulgariaPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal BulgariaKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal BulgariaPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal BulgariaKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal BulgariaPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal BulgariaKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="C" for="ItemQuantity_1_3_0_2_PCE_0"><span class="accesskey">C</span>roatia</label></td>
											<td><input type="text" class="form-control normal CroatiaPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CroatiaKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CroatiaPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CroatiaKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CroatiaPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CroatiaKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="C" for="ItemQuantity_1_4_0_2_PCE_0"><span class="accesskey">C</span>yprus</label></td>
											<td><input type="text" class="form-control normal CyprusPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CyprusKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CyprusPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CyprusKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CyprusPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CyprusKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="C" for="ItemQuantity_1_5_0_2_PCE_0"><span class="accesskey">C</span>zech Republic</label></td>
											<td><input type="text" class="form-control normal CzechRepublicPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CzechRepublicKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CzechRepublicPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CzechRepublicKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CzechRepublicPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal CzechRepublicKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="D" for="ItemQuantity_1_6_0_2_PCE_0"><span class="accesskey">D</span>enmark</label></td>
											<td><input type="text" class="form-control normal DenmarkPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal DenmarkKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal DenmarkPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal DenmarkKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal DenmarkPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal DenmarkKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="E" for="ItemQuantity_1_7_0_2_PCE_0"><span class="accesskey">E</span>stonia</label></td>
											<td><input type="text" class="form-control normal EstoniaPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal EstoniaKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal EstoniaPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal EstoniaKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal EstoniaPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal EstoniaKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="F" for="ItemQuantity_1_8_0_2_PCE_0"><span class="accesskey">F</span>inland</label></td>
											<td><input type="text" class="form-control normal FinlandPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal FinlandKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal FinlandPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal FinlandKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal FinlandPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal FinlandKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="F" for="ItemQuantity_1_9_0_2_PCE_0"><span class="accesskey">F</span>rance</label></td>
											<td><input type="text" class="form-control normal FrancePCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal FranceKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal FrancePCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal FranceKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal FrancePCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal FranceKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="G" for="ItemQuantity_1_10_0_2_PCE_0"><span class="accesskey">G</span>ermany</label></td>
											<td><input type="text" class="form-control normal GermanyPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GermanyKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GermanyPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GermanyKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GermanyPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GermanyKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="G" for="ItemQuantity_1_11_0_2_PCE_0"><span class="accesskey">G</span>reat Britain</label></td>
											<td><input type="text" class="form-control normal GreatBritainPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GreatBritainKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GreatBritainPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GreatBritainKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GreatBritainPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GreatBritainKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="G" for="ItemQuantity_1_12_0_2_PCE_0"><span class="accesskey">G</span>reece</label></td>
											<td><input type="text" class="form-control normal GreecePCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GreeceKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GreecePCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GreeceKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GreecePCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal GreeceKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="H" for="ItemQuantity_1_13_0_2_PCE_0"><span class="accesskey">H</span>ungary</label></td>
											<td><input type="text" class="form-control normal HungaryPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal HungaryKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal HungaryPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal HungaryKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal HungaryPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal HungaryKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="I" for="ItemQuantity_1_14_0_2_PCE_0"><span class="accesskey">I</span>reland</label></td>
											<td><input type="text" class="form-control normal IrelandPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal IrelandKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal IrelandPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal IrelandKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal IrelandPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal IrelandKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="I" for="ItemQuantity_1_15_0_2_PCE_0"><span class="accesskey">I</span>taly</label></td>
											<td><input type="text" class="form-control normal ItalyPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal ItalyKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal ItalyPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal ItalyKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal ItalyPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal ItalyKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="L" for="ItemQuantity_1_16_0_2_PCE_0"><span class="accesskey">L</span>atvia</label></td>
											<td><input type="text" class="form-control normal LatviaPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LatviaKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LatviaPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LatviaKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LatviaPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LatviaKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="L" for="ItemQuantity_1_17_0_2_PCE_0"><span class="accesskey">L</span>ithuania</label></td>
											<td><input type="text" class="form-control normal LithuaniaPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LithuaniaKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LithuaniaPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LithuaniaKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LithuaniaPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LithuaniaKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="L" for="ItemQuantity_1_18_0_2_PCE_0"><span class="accesskey">L</span>uxembourg</label></td>
											<td><input type="text" class="form-control normal LuxembourgPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LuxembourgKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LuxembourgPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LuxembourgKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LuxembourgPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal LuxembourgKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="M" for="ItemQuantity_1_19_0_2_PCE_0"><span class="accesskey">M</span>alta</label></td>
											<td><input type="text" class="form-control normal MaltaPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal MaltaKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal MaltaPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal MaltaKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal MaltaPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal MaltaKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="N" for="ItemQuantity_2_1_0_2_PCE_0"><span class="accesskey">N</span>etherlands</label></td>
											<td><input type="text" class="form-control normal NetherlandsPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal NetherlandsKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal NetherlandsPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal NetherlandsKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal NetherlandsPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal NetherlandsKGSEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal NetherlandsPCEEBel" value="" size="8" ></td>
											<td><input type="text" class="form-control normal NetherlandsKGSEBel" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="P" for="ItemQuantity_1_20_0_2_PCE_0"><span class="accesskey">P</span>oland</label></td>
											<td><input type="text" class="form-control normal PolandPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal PolandKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal PolandPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal PolandKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal PolandPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal PolandKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="P" for="ItemQuantity_1_21_0_2_PCE_0"><span class="accesskey">P</span>ortugal</label></td>
											<td><input type="text" class="form-control normal PortugalPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal PortugalKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal PortugalPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal PortugalKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal PortugalPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal PortugalKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="R" for="ItemQuantity_1_22_0_2_PCE_0"><span class="accesskey">R</span>omania</label></td>
											<td><input type="text" class="form-control normal RomaniaPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal RomaniaKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal RomaniaPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal RomaniaKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal RomaniaPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal RomaniaKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="S" for="ItemQuantity_1_23_0_2_PCE_0"><span class="accesskey">S</span>lovakia</label></td>
											<td><input type="text" class="form-control normal SlovakiaPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SlovakiaKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SlovakiaPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SlovakiaKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SlovakiaPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SlovakiaKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="S" for="ItemQuantity_1_24_0_2_PCE_0"><span class="accesskey">S</span>lovenia</label></td>
											<td><input type="text" class="form-control normal SloveniaPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SloveniaKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SloveniaPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SloveniaKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SloveniaPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SloveniaKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="S" for="ItemQuantity_1_25_0_2_PCE_0"><span class="accesskey">S</span>pain</label></td>
											<td><input type="text" class="form-control normal SpainPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SpainKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SpainPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SpainKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SpainPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SpainKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="S" for="ItemQuantity_1_26_0_2_PCE_0"><span class="accesskey">S</span>weden</label></td>
											<td><input type="text" class="form-control normal SwedenPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SwedenKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SwedenPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SwedenKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SwedenPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal SwedenKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><label accesskey="B" for="ItemQuantity_1_27_0_2_PCE_0"><span class="accesskey">B</span>elgium</label></td>
											<td><input type="text" class="form-control normal BelgiumPCEP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal BelgiumKGSP" value="" size="8" ></td>
											<td><input type="text" class="form-control normal BelgiumPCEG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal BelgiumKGSG" value="" size="8" ></td>
											<td><input type="text" class="form-control normal BelgiumPCEEBus" value="" size="8" ></td>
											<td><input type="text" class="form-control normal BelgiumKGSEBus" value="" size="8" ></td>
										 </tr>
										 <tr>
											<td><b>Totals</b></td>
											<td><b>0&nbsp;Pc</b></td>
											<td><b>0,000&nbsp;Kg</b></td>
											<td><b>0&nbsp;Pc</b></td>
											<td><b>0,000&nbsp;Kg</b></td>
											<td><b>0&nbsp;Pc</b></td>
											<td><b>0,000&nbsp;Kg</b></td>
											<td><b>0&nbsp;Pc</b></td>
											<td><b>0,000&nbsp;Kg</b></td>
										 </tr>
									  </tbody>
								   </table>
								   
								</form>
								</div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

<script>
	$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "eShipper is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off', function()
		 {
			
			console.log( "****************************" );                    
			console.log( "eShipper is fetching all information" );
			console.log( "****************************" );	
			
			var serviceProvider = $(this).attr('id');
			// Data fetching from this			
			var getEshipper = $.ajax(
			{
				'url'            : '/Cronjobs/setInfoEshipper',
				'type'           : 'POST',
				'data'           : { serviceProvider : serviceProvider },
                                'beforeSend'	 : function() {
							$( ".outerOpac" ).attr( 'style' , 'display:block');
						  },
				global: false,
				async:false,
				'success'        : function( msgArray )
                                                {	
                                                        var json = JSON.parse(msgArray);
                                                        var getData = json.data;

                                                         // Get the value "long_name" from the long_name key
                                                             for (var i = 0, max = getData.length; i < max; i++)
                                                             {
                                                                     var country = getData[i].ServiceCounter.destination;
                                                                     var serviceCode = getData[i].ServiceCounter.service_code;
                                                                     var counter = getData[i].ServiceCounter.counter;
                                                                     var originalCounter = getData[i].ServiceCounter.original_counter;
                                                                     var orderIds = getData[i].ServiceCounter.order_ids;											
                                                                     var setSelectoritems = '';
                                                                     var setSelectorWeight = '';

                                                                     //alert( country +'=='+ serviceCode +'=='+ counter +'=='+ originalCounter +'=='+ orderIds );
                                                                     /*if( serviceCode == 'Size P - EU' )
                                                                     {
                                                                             setSelectoritems = country+'PCEP';
                                                                             setSelectorWeight = country+'PCEP';
                                                                     }
                                                                     else if( serviceCode == 'Size G - EU' )
                                                                     {
                                                                             setSelectoritems = country+'PCEG';
                                                                             setSelectorWeight = country+'PCEG';
                                                                     }
                                                                     else if( serviceCode == 'Size E(Bus) - EU' )
                                                                     {
                                                                             setSelectoritems = country+'PCEEBus';
                                                                             setSelectorWeight = country+'PCEEBus';
                                                                     }
                                                                     else{}
                                                                     */
                                                                     if( serviceCode == 'Size P - EU' )
                                                                     {
                                                                             setSelectoritems = country+'PCEP';
                                                                             setSelectorWeight = country+'KGSP';
                                                                     }
                                                                     else if( serviceCode == 'Size G - EU' )
                                                                     {
                                                                             setSelectoritems = country+'PCEG';
                                                                             setSelectorWeight = country+'KGSG';
                                                                     }
                                                                     else if( serviceCode == 'Size E(Bus) - EU' )
                                                                     {
                                                                             setSelectoritems = country+'PCEEBus';
                                                                             setSelectorWeight = country+'KGSEBus';
                                                                     }
                                                                     else{}

                                                                     //Checked exactly what service provider is coming then need to be input but 
                                                                     // we need to get exact weight of order with packagin weight
                                                                     //We need order merge update section
                                                                     //Get all weight combine with packagin weight 

																		//alert( counter );
																		//alert( setSelectoritems );//GreatBritainPCEP, CzechRepublicPCEP
																		
																		/* for United kingdom */
																		
																		if( setSelectoritems == 'United KingdomPCEP' )
																		{
																			setSelectoritems = 'GreatBritainPCEP';
																			setSelectorWeight = 'GreatBritainKGSP';
																		}
																		else if( setSelectoritems == 'United KingdomPCEG' )
																		{
																			setSelectoritems = 'GreatBritainPCEG';
																			setSelectorWeight = 'GreatBritainKGSG';
																		}
																		else if( setSelectoritems == 'United KingdomPCEEBus' )
																		{
																			setSelectoritems = 'GreatBritainPCEEBus';
																			setSelectorWeight = 'GreatBritainKGSEBus';
																		}
																		else
																		{
																			setSelectoritems = setSelectoritems;
																			setSelectorWeight = setSelectorWeight;
																		}
																		
																		/* for Czech Republic */
																		if( setSelectoritems == 'Czech RepublicPCEP' )
																		{
																			setSelectoritems = 'CzechRepublicPCEP';
																			setSelectorWeight = 'CzechRepublicPCEP';
																		}
																		else if( setSelectoritems == 'Czech RepublicPCEG' )
																		{
																			setSelectoritems = 'CzechRepublicPCEG';
																			setSelectorWeight = 'CzechRepublicKGSG';
																		}
																		else if( setSelectoritems == 'Czech RepublicPCEEBus' )
																		{
																			setSelectoritems = 'CzechRepublicPCEEBus';
																			setSelectorWeight = 'CzechRepublicKGSEBus';
																		}
																		else
																		{
																			setSelectoritems = setSelectoritems;
																			setSelectorWeight = setSelectorWeight;
																		}
																		
																		
																		/*else if( setSelectoritems == 'Czech RepublicPCEP' ) 
																		{
																			setSelectoritems = 'CzechRepublicPCEP';
																			setSelectorWeight = 'CzechRepublicPCEP';
																		}*/
																		
                                                                     $( '.'+setSelectoritems ).val( counter );
                                                                     //alert( $( '.'+setSelectoritems ).val( counter ) );
																     var getEshipper = $.ajax(
                                                                     {
                                                                             'url'            : '/Cronjobs/getExactWeight',
                                                                             'type'           : 'POST',
                                                                             'data'           : { orderId : orderIds },
                                                                             global: false,
                                                                             async:false,
                                                                             'success'        : function( msgArray )
                                                                                                 {
																									    var splitWeight = msgArray.split('==');
                                                                                                        var weight = parseFloat(splitWeight[0]);
                                                                                                        var envelopeWeight = parseFloat(splitWeight[1]);   
                                                                                                        
                                                                                                        var totalWeight = parseFloat(weight + envelopeWeight);
                                                                                                       
                                                                                                        $( '.'+setSelectoritems ).val( counter );
                                                                                                        $( '.'+setSelectorWeight ).val( totalWeight );

                                                                                                 }																	
                                                                     });



                                                             }	

                                                    $( ".outerOpac" ).attr( 'style' , 'display:none');

                                                }
			}).responseText;
			
			
			return false;
			
			console.log( "****************************" );                    
			console.log( "Now input" );
			console.log( "****************************" );	
		 });
		
		function getResult( data )
		{
			return data;
		}
	});
</script>

<script>
	$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "Euraco is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off_manifest', function()
		 {
			//Get time
			var getTime = $.ajax({				     
					url: "/Cronjobs/getClientTime",					
					dataType: 'text',					
					global: false,
					async:false,
					success: function(data) {
						return data;
					}
			}).responseText;

			var serviceProvider = $(this).attr('id');
			
			if( getTime == '0' )
			{
				swal({
					title: "Are you sure?",
					text: "You are generating Manifest After Cut off. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						var strAction = "update";
						/* Start here updating section */
						$.ajax(
						{
						'url'            : '/Cronjobs/createCutOffList',
						'type'           : 'POST',
						'data'           : { serviceProvider : serviceProvider },
                                                'beforeSend'	 : function() {
							$( ".outerOpac" ).attr( 'style' , 'display:block');
						  },
						'success'        : function( data )
										   {										   
											   if( data != 'blank' )
											   {
$( ".outerOpac" ).attr( 'style' , 'display:none');													
	window.open(data,'_blank' );
													location.reload();
											   }
											   else
											   {
$( ".outerOpac" ).attr( 'style' , 'display:none');	
												   swal("No orders found for manifest!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						swal("Cancelled", "", "error");
					}
				});
			}
			else
			{
				$.ajax(
				{
				'url'            : '/Cronjobs/createCutOffList',
				'type'           : 'POST',
				'data'           : { serviceProvider : serviceProvider },
                                'beforeSend'	 : function() {
							$( ".outerOpac" ).attr( 'style' , 'display:block');
						  },
				'success'        : function( data )
								   {										   
									   if( data != 'blank' )
									   {
$( ".outerOpac" ).attr( 'style' , 'display:none');	
											window.open(data,'_blank' );
											location.reload();
									   }
									   else
									   {
$( ".outerOpac" ).attr( 'style' , 'display:none');	
										   swal("No orders found for manifest!" , "" , "error");										   
									   }
								   }
				});
			}
			 
		 });
		
		function getResult( data )
		{
			return data;
		}


$( 'body' ).on( 'click', '.cut_off_manifest_back_up', function()
		 {
			 var userId = <?php echo $getOperatorInformation['id']; ?>;
			 var servicePost = $(this).attr('id');
			 
			 $.ajax(
				{
				'url'            : '/Uploads/serviceCounterBackUp',
				'type'           : 'POST',
				'data'           : { userId : userId , servicePost : servicePost  },
				'beforeSend'	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
				  },
				'success'        : function( data )
								   {										   
									   if( data != 'blank' )
									   {
										    $( ".outerOpac" ).attr( 'style' , 'display:none');
										    $( ".manifest_button" ).attr( 'style' , 'display:inline-block');
										    $( ".cut_off_manifest_back_up" ).attr( 'style' , 'display:none');
									   }
									   else
									   {
										   $( ".outerOpac" ).attr( 'style' , 'display:none');	 
									   }
								   }
				});
			 
		 });
	});
</script>

