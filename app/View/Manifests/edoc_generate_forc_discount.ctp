<?php 
App::import('Controller', 'Linnworksapis');
$checkproduct =	new LinnworksapisController;
$userID = $this->session->read('Auth.User.id');	
?>

<div class="rightside bg-grey-100">
<!-- BEGIN PAGE HEADING -->
<div class="page-head bg-grey-100">        
		<?php
			$getOperatorInformation = $this->Session->read('Auth.User') ;	
			date_default_timezone_set('Europe/Jersey');			
		?>        
    	<h1 class="page-title">New Docket <?php echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]'; ?></h1>
		<div class="panel-title no-radius bg-green-500 color-white no-border">
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		</div>		
</div>
	
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
          <div class="panel">                            
                <div class="row">
                    <div class="col-lg-12">
                            <div class="panel-body padding-bottom-40 padding-top-40">
                            	
								 <!--<label class="col-sm-2">Docket No.</label>
								 <input class="col-sm-2 margin-bottom-10" type="text" name="docket_no" id="docketno" >-->
								 <button type="button" id="germany" class="cut_off_manifest bg-red-400 color-white btn-dark col-sm-5 margin-bottom-10 btn" style="float:right;" >France-Direct Entry</button>
								 <!-- <button type="button" id="france" class="cut_off_manifest bg-red-400 color-white btn-dark col-sm-5 margin-bottom-10 btn" style="float:left;" >
								 	France Manifest</button> -->
								<table class="tbl_table docketTable" id="docket">
								   <tbody>
									  <tr>
										 <td style="padding-bottom:10px;" colspan="15">
											<hr class="bannerLine">
										 </td>
									  </tr>
									 
									  
									  <tr class="docketTableHeaderRow">
										 <th></th>
										 <th></th>
										 <th>Order Type</th>										 
										 <th>Order Status</th>
										 <th>Source(SubSource)</th>
										 
										 <th>SKU</th>
										 <th>Barcode</th>
										 <th>Price</th>
										 <th>Weight</th>
										 <th>Action</th>
									  </tr>

									  <br>
									   <?php foreach ($cDiscount as $key => $value) {
										   	$GeneralInf=unserialize($value['OpenOrder']['general_info']);
										   	$CustomerInfo=unserialize($value['OpenOrder']['customer_info']);
										   	$ShippingInfo=unserialize($value['OpenOrder']['shipping_info']);
										   	$TotalsInfo=unserialize($value['OpenOrder']['totals_info']);
									   

									$address = '';
									$address .= $CustomerInfo->Address->Address1.', ';
									$address .= $CustomerInfo->Address->Address2.', ';
									$address .= $CustomerInfo->Address->Address3.', ';
									$address .= $CustomerInfo->Address->Town.', ';
									$address .= $CustomerInfo->Address->PostCode.', ';
									$address .= $CustomerInfo->Address->Region.', ';
									$address .= $CustomerInfo->Address->Country;
											
									   		
									   	?>
									  <tr data-tblblock="E" class="tbl_row docketTableItemRow  nonSurcharge">
									  	<td class="docketTableItemInvisible">
											<input value="E" name="docketid">
											<input value="385" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
											<input value="3" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
										</td>

										<td class="docketTableItemInvisible">
											<input value="E" name="docketid">
											<input value="385" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="tariffid">
											<input value="3" data-tbl="{&quot;fmt&quot;:&quot;num&quot;}" name="destid">
										</td>
										

										 <td><?php  echo $value['MergeUpdate']['provider_ref_code'];?>
										 </td>
										 <td><?php  echo $value['MergeUpdate']['service_name'];?></td>

										  <td>
											<?php if( $value['MergeUpdate']['linn_fetch_orders'] == 0 ){print "Not Paid";}else if($value['MergeUpdate']['linn_fetch_orders'] == 1 ){ print "Paid";}else if($value['MergeUpdate']['linn_fetch_orders'] == 4 ){ print "Resend";}?></td>

										 </td>
										 <td><?php  echo $value['MergeUpdate']['parent_id'];?>
										 </td>
										 
										 <td><?php 	//changes by amit rawat
											if( ($value['MergeUpdate']['status'] == 1) && $value['MergeUpdate']['custom_marked'] == 0) {
											  echo "Processed";	
											} elseif($value['MergeUpdate']['status'] == 1 && $value['MergeUpdate']['custom_marked'] == 1) { 
											  echo "Custom Processed";		
											} elseif($value['MergeUpdate']['status'] == 4 && $value['MergeUpdate']['custom_marked'] == 1) { 
                                                                                                                                                                                                                                                            
											    echo "Custom Processed (Returned)";	
										                    			
											 } elseif($value['MergeUpdate']['status'] == 4 && $value['MergeUpdate']['custom_marked'] == 0) { 
												  
											    echo "Processed - Returned";	
										                      
											 }  else {
											   echo "----";
											 }
											if($value['MergeUpdate']['is_resend']==1) {
											 echo " (Refunded)";
											}
											if($value['MergeUpdate']['is_resend']==1) {
											 echo " (Resend)";
											}
																
                                             ?></td>
                                        
										<td> <?php  echo $value['OpenOrder']['sub_source'];?></td>
										 <td> <?php  echo $value['MergeUpdate']['sku'];?></td>
										 <td><?php  echo $value['MergeUpdate']['barcode'];?> </td>
										 <td>
											<?php echo $value['MergeUpdate']['price'];?>
										 </td>
										 <td>
											<?php echo $value['MergeUpdate']['packet_weight'];?>
										 </td>
										 <td><button class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?php echo $value['MergeUpdate']['id'];?>">View</button></td>
										 
									  </tr>

									  <br>
									  <div id="myModal<?php echo $value['MergeUpdate']['id'];?>" class="modal fade" role="dialog">
										  <div class="modal-dialog " style="width:980px;">

										    <!-- Modal content-->
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal">&times;</button>
										        <h4 class="modal-title" style="text-align: center;">Order Id --<?php  if($value['OpenOrder']['num_order_id']){
																	echo $value['OpenOrder']['num_order_id'];
																}else{
																	echo "--";
																}?></h4>
										      </div>
										      <div class="modal-body">
										      	<div class="container-fluid">
											      	<div class="row">
												       	<div class="col-md-6">
															<div class="heading-custom">Order Type</div>
															<div class="heading-custom">Parent Order Id:</div>
															<div class="heading-custom">Order Status:</div>
															<div class="heading-custom">Received Date:</div>
															<div class="heading-custom">Source(SubSource):</div>
															<div class="heading-custom">Process Date:</div>
															<div class="heading-custom">Reference No.:</div>
															<div class="heading-custom">Service:</div>
															<div class="heading-custom">Address:</div>
															<div class="heading-custom">FullName:</div>
															<div class="heading-custom">Subtotal:</div>
															<div class="heading-custom">Tax:</div>
															<div class="heading-custom">TotalCharge:</div>
															<div class="heading-custom"><br><br></div>
															<div class="heading-custom">Sub Order id</div>
															<div class="heading-custom">SKU</div>
															<div class="heading-custom">Barcode</div>
															<div class="heading-custom">Price</div>
															<div class="heading-custom">Order Status (Custom Process)</div>
															<div class="heading-custom">Packet Weight & Dimentions</div>
															<div class="heading-custom">Envelope Weight</div>
															<div class="heading-custom">Service Provider</div>
															<div class="heading-custom">Service Name & Code</div>
															<div class="heading-custom">Move Date</div>
															<div class="heading-custom">Processed By</div>
															<div class="heading-custom">Sorting Time</div>
															<div class="heading-custom">Order assigned</div>
															<div class="heading-custom">Batch Number</div>
															<div class="heading-custom">Dispatch Console</div>
	                        							</div>
												       <div class="col-md-6">
												       <div class="heading-record"><?php if( $value['MergeUpdate']['linn_fetch_orders'] == 0 ){print "Not Paid";}else if($value['MergeUpdate']['linn_fetch_orders'] == 1 ){ print "Paid";}else if($value['MergeUpdate']['linn_fetch_orders'] == 4 ){ print "Resend";}?>
												       </div>

														<div class="heading-record">
															<?php  if($value['MergeUpdate']['parent_id']){
																echo $value['MergeUpdate']['parent_id'];
															}else{
																echo "--";
																}?>
														</div>
														<div class="heading-record">
														<?php 	//changes by amit rawat
															if( ($value['MergeUpdate']['status'] == 1) && $value['MergeUpdate']['custom_marked'] == 0) {
															  echo "Processed";	
															} elseif($value['MergeUpdate']['status'] == 1 && $value['MergeUpdate']['custom_marked'] == 1) { 
															  echo "Custom Processed";		
															} elseif($value['MergeUpdate']['status'] == 4 && $value['MergeUpdate']['custom_marked'] == 1) { 
				                                                                                                                                                                                                                                                            
															    echo "Custom Processed (Returned)";	
														                    			
															 } elseif($value['MergeUpdate']['status'] == 4 && $value['MergeUpdate']['custom_marked'] == 0) { 
																  
															    echo "Processed - Returned";	
														                      
															 }  else {
															   echo "----";
															 }
															if($value['MergeUpdate']['is_resend']==1) {
															 echo " (Refunded)";
															}
															if($value['MergeUpdate']['is_resend']==1) {
															 echo " (Resend)";
															}
																				
				                                             ?>
				                                         </div>
															<div class="heading-record">
																<?php echo $GeneralInf->ReceivedDate;?>
															</div>
															<div class="heading-record">
																<?php if($value['OpenOrder']['sub_source']){
																	echo $value['OpenOrder']['sub_source'];
																}else{
																	echo "--";
																}?>
															</div>
															<div class="heading-record">
																<?php  if($value['MergeUpdate']['process_date']){
																echo $value['MergeUpdate']['process_date'];
															}else{
																echo "--";
																}?>
															</div>
															<div class="heading-record"> 
																<?php echo $GeneralInf->ReferenceNum;?>
															</div>
															<div class="heading-record">
																<?php echo $ShippingInfo->PostalServiceName;?>:
															</div>
															<div class="heading-record">
																<?php echo $address;?></div>
															<div class="heading-record">
																<?php echo $CustomerInfo->Address->FullName; ?>
															</div>
															<div class="heading-record">
																<?php echo $TotalsInfo->Subtotal .'&nbsp; ('. $TotalsInfo->Currency .')'; ?>
															</div>
															<div class="heading-record"><?php echo $TotalsInfo->Tax .'&nbsp; ('. $TotalsInfo->Currency .')' ; ?>
															</div>
															<div class="heading-record"><?php echo $TotalsInfo->TotalCharge .'&nbsp; ('. $TotalsInfo->Currency .')' ; ?></div>
															<div class="heading-record"><br><br></div>
															<div class="heading-record">
																<?php  if($value['OpenOrder']['num_order_id']){
																	echo $value['OpenOrder']['num_order_id'];
																}else{
																	echo "--";
																}?></div>
															<div class="heading-record">
																<?php  if($value['MergeUpdate']['sku']){
																	echo $value['MergeUpdate']['sku'];
																}else{
																	echo "--";
																}?>
															</div>
															<div class="heading-record">
															<?php if($value['MergeUpdate']['barcode']){
																	echo $value['MergeUpdate']['barcode'];
															}else{
																echo "--";
															}?>
															</div>
															<div class="heading-record">
															<?php if($value['MergeUpdate']['price']){
																echo $value['MergeUpdate']['price'];
															}else{
																echo '--';
															}?>
															</div>
															<div class="heading-record"><?php 	//changes by amit rawat
															if( ($value['MergeUpdate']['status'] == 1) && $value['MergeUpdate']['custom_marked'] == 0) {
															  echo "Processed";	
															} elseif($value['MergeUpdate']['status'] == 1 && $value['MergeUpdate']['custom_marked'] == 1) { 
															  echo "Custom Processed";		
															} elseif($value['MergeUpdate']['status'] == 4 && $value['MergeUpdate']['custom_marked'] == 1) { 
				                                                                                                                                                                                                                                                            
															    echo "Custom Processed (Returned)";	
														                    			
															 } elseif($value['MergeUpdate']['status'] == 4 && $value['MergeUpdate']['custom_marked'] == 0) { 
																  
															    echo "Processed - Returned";	
														                      
															 }  else {
															   echo "----";
															 }
															if($value['MergeUpdate']['is_resend']==1) {
															 echo " (Refunded)";
															}
															if($value['MergeUpdate']['is_resend']==1) {
															 echo " (Resend)";
															}
																				
				                                             ?></div>
															<div class="heading-record">
															<?php print $value['MergeUpdate']['packet_weight'] ." Kg  L-".  $value['MergeUpdate']['packet_length'] .' MM, W-'. $value['MergeUpdate']['packet_width'] .' MM, H-'. $value['MergeUpdate']['packet_height'] .' MM' ?>
															</div>
															<div class="heading-record">
																<?php if($value['MergeUpdate']['evelope_weight']){
																echo $value['MergeUpdate']['envelope_weight'];
																}else{
																echo "--";
																}?></div>
																<div class="heading-record">
																<?php if($value['MergeUpdate']['service_provider']){
																echo $value['MergeUpdate']['service_provider'];
																echo "(".$value['MergeUpdate']['service_provider'].")";
																}else{
																echo "--";
																}?>
																</div>
															<div class="heading-record">
																<?php if($value['MergeUpdate']['service_name']){
																echo $value['MergeUpdate']['service_name'];
																}else{
																echo "--";
																}?></div>
															<div class="heading-record">
																<?php if($value['MergeUpdate']['order_date']){
																echo $value['MergeUpdate']['order_date'];
																}else{
																echo "--";
																}?>
															</div>
															<div class="heading-record">
																<?php  
															echo $checkproduct->getUserDetail( $value['MergeUpdate']['user_id'] );
																?>
															</div>
															<div class="heading-record">
																<?php if($value['MergeUpdate']['scan_date']){
																echo $value['MergeUpdate']['scan_date'];
																}else{
																echo "--";
																}?>
															</div>
															<div class="heading-record"><?php
																if($value['MergeUpdate']['assign_user'] == '' && $value['MergeUpdate']['status'] == 0 ){
																		echo "----";
																	} else { 
																		echo $value['MergeUpdate']['assign_user'];
																	}
															?></div>
															<div class="heading-record">--</div>
															<div class="heading-record">--</div>
												       </div>
											   		</div>
										   		</div>
										      </div>
										      
										    </div>

										  </div>
										</div>
									  <?php } ?>
									  

									  <tr>
										 <td style="padding-top:10px;padding-bottom:10px;" colspan="15">
											<hr class="bannerLine">
										 </td>
									  </tr>
									  
								   </tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div>        
	</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

<script>

$(function()
{         
     
		console.log( "****************************" );                    
		console.log( "eShipper is fetching all information" );
		console.log( "****************************" );	
		
		//var serviceProvider = $(this).attr('id');
		// Data fetching from this			
		var serviceProvider	= 'Jersey Post';
		var getEshipper = $.ajax(
		{
			'url'            : '/Directposts/setInfoEdoc',
			'type'           : 'POST',
			'beforeSend'	 : function() {
				$( ".outerOpac" ).attr( 'style' , 'display:block');
			  },
			'data'           : { serviceProvider : serviceProvider },
			global: false,
			async:false,
			'success'        : function( msgArray )
								{	
										var json = JSON.parse(msgArray);
										var getData = json.data;
											 for (var i = 0, max = getData.length; i < max; i++)
											 {
											 		 var counter = getData[i].ServiceCounter.counter;
													 var bags = getData[i].ServiceCounter.bags;
													 var serviceCode = getData[i].ServiceCounter.service_code;
													 if( $("#"+serviceCode+'Items').val().length == 0 )
													 {
													 	 $("#"+serviceCode+'Items').val(counter);
														 $("#"+serviceCode+'Bages').val(bags);
														
														 counter = 0;
														 bags = 0;
													 }
													 else
													 {
													 	 //counter = parseInt(counter) + parseInt($("#"+serviceCode+'Items').val());
														 //bags = parseInt(bags) + parseInt($("#"+serviceCode+'Bages').val());
														 $("#"+serviceCode+'Items').val(counter);
														 $("#"+serviceCode+'Bages').val(bags);
														
														 counter = 0;
														 bags = 0;
													 }
											 }
											 $( ".outerOpac" ).attr( 'style' , 'display:none');	
									}
		}).responseText;
		return false;
		console.log( "****************************" );                    
		console.log( "Now input" );
		console.log( "****************************" );	
});
</script>
<script>

/*function checkdocatno()
{
	var docnum = $('#docketno').val();
	if(docnum != ''){
		$('.doc_num').text(docnum);
		$('#myModal').modal('show');
	} else {
		swal("Please fill edoc number." , "" , "error");
		return false;
	}
}*/


$(function()
{                     
	console.log( "****************************" );                    
	console.log( "Euraco is ready to processed" );
	console.log( "****************************" );					
	$("#LinnworksapisBarcode").focus();
	
	 $( 'body' ).on( 'click', '.cut_off_edoc', function()
	 {
		var serviceProvider = $(this).attr('id');
		//Get time
		var getTime = $.ajax({				     
				url: "/Cronjobs/getClientTime",
				data : { serviceProvider : serviceProvider },
				beforeSend	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
				  },					
				dataType: 'text',					
				global: false,
				async:false,
				success: function(data) {
					return data;
				}
		}).responseText;

		if( getTime == '0' )
		{
			swal({
				title: "Are you sure?",
				text: "You are clearing data After Cut off. Proceed?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Proceed",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm)
			{
				if (isConfirm)
				{
					var strAction = "update";
					$.ajax(
					{
					'url'            : '/System/eDoc',
					'type'           : 'POST',
					'data'           : { serviceProvider : serviceProvider },
					'success'        : function( data )
									   {										   
										   if( data != 'blank' )
										   {
												$('input[type=text].countEditBox').val('0');
												$('input[type=text].countEditBox').val('0');
												$( ".outerOpac" ).attr( 'style' , 'display:none');
										   }
										   else
										   {
											   $( ".outerOpac" ).attr( 'style' , 'display:none');
											   swal("No orders found to flush!" , "" , "error");
										   }
									   }
					});         
				}
				else
				{
					$( ".outerOpac" ).attr( 'style' , 'display:none');
					swal("Cancelled", "", "error");
				}
			});
		}
		else
		{
			
			swal({
				title: "Are you sure?",
				text: "have you filled eDoc information. Proceed?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Proceed",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm)
			{
				/* isConfirm tell us true or false */
				if (isConfirm)
				{
					var strAction = "update";
					/* Start here updating section */
					$.ajax(
					{
					'url'            : '/System/eDoc',
					'type'           : 'POST',
					'data'           : { serviceProvider : serviceProvider },
					'success'        : function( data )
									   {										   
										   if( data != 'blank' )
										   {
												//$('.autofocus').val('');
												//window.open(data,'_blank' );
												//location.reload();
												
												$('input[type=text].countEditBox').val('0');
												$('input[type=text].countEditBox').val('0');
												$( ".outerOpac" ).attr( 'style' , 'display:none');
												
										   }
										   else
										   {
											   $( ".outerOpac" ).attr( 'style' , 'display:none');
											   swal("No orders found to flush!" , "" , "error");
										   }
									   }
					});         
				}
				else
				{
					$( ".outerOpac" ).attr( 'style' , 'display:none');
					swal("Cancelled", "", "error");
				}
			});
		}
		 
	 });
	
	function getResult( data )
	{
		return data;
	}
	
	
	 
	
	 
	 
	  $( 'body' ).on( 'click', '.cut_off_manifest', function()
	 {
		var country = $(this).attr('id');
		var getTime = $.ajax({				     
				url: "/Directposts/getClientTime",
				type : "Post",
				data : { country : country },					
				dataType: 'text',					
				global: false,
				async:false,
				success: function(data) {
					return data;
				}
		}).responseText;

		if( getTime == '0' )
		{
			swal({
				title: "Are you sure?",
				text: "You are clearing data After Cut off. Proceed?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Proceed",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm)
			{
				/* isConfirm tell us true or false */
				if (isConfirm)
				{
					var strAction = "update";
					/* Start here updating section */
					$.ajax(
					{
					'url'            : '/Directposts/getGermanyAndFranceManifest',
					'type'           : 'POST',
					'data'           : { country : country },
					'success'        : function( data )
									   {										   
										   if( data != 'blank' )
										   {
												window.open(data,'_blank' );
												location.reload();
												
										   }
										   else
										   {
											   swal("No orders found to flush!" , "" , "error");
										   }
									   }
					});         
				}
				else
				{
					swal("Cancelled", "", "error");
				}
			});
		}
		else
		{
			
			swal({
				title: "Are you sure?",
				text: "have you filled eDoc information. Proceed?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Proceed",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm)
			{
				/* isConfirm tell us true or false */
				if (isConfirm)
				{
					var strAction = "update";
					/* Start here updating section */
					$.ajax(
					{
					'url'            : '/Directposts/getGermanyAndFranceManifest',
					'type'           : 'POST',
					'data'           : { country : country },
					'success'        : function( data )
									   {										   
										   if( data != 'blank' )
										   {
											   $('.autofocus').val('');
												location.reload( true  );
												window.open(data,'_blank' );
										   }
										   else
										   {
											   swal("No orders found to flush!" , "" , "error");
										   }
									   }
					});         
				}
				else
				{
					swal("Cancelled", "", "error");
				}
			});
		}			 
	 });
	 
});
</script>
<style type="text/css">
.heading-record {
    color: green;
    font-size: 13px;
    padding: 3px;
}
.heading-custom {
    color: red;
    font-size: 13px;
    padding: 3px;
}
</style>


