<?php if(isset($message['success']) && $message['success']!=''){ ?>
<?php /*?><div class="alert alert-success" timeout="10000" role="alert" style="margin-bottom:0px;margin-top:10px"> <?php echo $message['success']; ?></div>
<?php */?>

<div class="alert alert-success alert-dismissible"  style="margin-bottom:0px;margin-top:10px">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
 <?php echo $message['success']; ?>
</div>
<?php }?>
<?php if(isset($message['warning']) && $message['warning']!=''){ ?>
<div class="alert alert-warning  alert-dismissible" role="alert" style="margin-bottom:0px;margin-top:10px">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $message['warning']; ?>
</div>
<?php }?>
<?php if(isset($message['info']) && $message['info']!=''){ ?>
<div class="alert alert-info  alert-dismissible" role="alert" style="margin-bottom:0px;margin-top:10px">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<?php echo $message['info']; ?>
</div>
<?php }?>
<?php if(isset($message['danger']) && $message['danger']!=''){ ?>
<div class="alert alert-danger  alert-dismissible" role="alert" style="margin-bottom:0px;margin-top:10px">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<?php echo $message['danger']; ?>
</div>
<?php }?>



