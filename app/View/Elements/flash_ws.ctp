<?php if(isset($message['success'])){ ?>
<div class="alert alert-success" timeout="1000" role="alert" style="margin-bottom:0px;margin-top:10px"> <?php echo $message['success']; ?></div>
<?php }?>
<?php if(isset($message['warning'])){ ?>
<div class="alert alert-warning" timeout="1000" role="alert" style="margin-bottom:0px;margin-top:10px"><?php echo $message['warning']; ?></div>
<?php }?>
<?php if(isset($message['info'])){ ?>
<div class="alert alert-info" timeout="1000" role="alert" style="margin-bottom:0px;margin-top:10px"><?php echo $message['info']; ?></div>
<?php }?>
<?php if(isset($message['danger'])){ ?>
<div class="alert alert-danger" timeout="5000" role="alert" style="margin-bottom:0px;margin-top:10px"><?php echo $message['danger']; ?></div>
<?php }?>