<div class="leftside">
			<div class="sidebar">
				<!-- BEGIN RPOFILE -->
				<div class="nav-profile">
					<div class="thumb">
						<?php 
						
						if($this->session->read('Auth.User.id') > 0)
						{	
							$getCommonHelper = $this->Common->getUserDataAfterLogin( $this->session->read('Auth.User.id') );
							
							/* Set FirstName with LastName*/
							//$getName = $this->Common->getFirstLastName( $this->session->read('Auth.User.id') );
						} 
						?>
                        <?php
						    $userImage = $getCommonHelper['User']['user_image'];
							$userName = $getCommonHelper['User']['first_name'].' '.$getCommonHelper['User']['last_name'];
							if( isset( $userImage ) && $userImage != "" )
							{
								print $this->html->image( 'upload/'.$userImage, array( "class" => "img-circle", "title" => $userName ) );			
							}
                        ?>  
						<!--<span class="label label-danger label-rounded">3</span>-->
					</div>
					<div class="info">
						<a href="#"><?php print $getCommonHelper['User']['first_name'].'_'; ?></a>
					</div>
					<a href="<?php print Router::url(array('controller' => 'users', 'action' => 'logout')); ?>" class="button"><i class="ion-log-out"></i></a>
				</div>
				<!-- END RPOFILE -->
				<!-- BEGIN NAV -->
				<div class="title">Navigation</div>
					<ul class="nav-sidebar">
						<li class="active">
                            <a title="Dashboard" href="javascript::void(0);">
                                <i class="ion-home"></i> <span>Dashboard</span>
                            </a>
                        </li>
						
						<!-- Start here user section -->
						<li class="nav-dropdown">
                            <a title="Roles" href="#">
                                <i class="ion-wand"></i> <span>Roles</span>
                                <i class="ion-chevron-right pull-right"></i>
                            </a>
                            <ul>
									<li><a href="/showRoles">Show Roles</a></li>
									<li><a href="/managerole">Add New Roles</a></li>									
                            </ul>
                        </li>						
						<!-- End here role section -->
						
						<!-- Start here user section -->
						<li class="nav-dropdown">
                            <a title="Users" href="#">
                                <i class="fa fa-users"></i> <span>Users</span>
                                <i class="ion-chevron-right pull-right"></i>
                            </a>
                            <ul>
									<li><a href="/showList">Show All Users</a></li>
									<li><a href="/register">Register User</a></li>									
                            </ul>
                        </li>
						<!-- End here user section -->
						
						<!-- Start here state & city section -->
						<li class="nav-dropdown">
                            <a title="Region" href="#">
                                <i class="fa fa-map-marker"></i> <span>Region</span>
                                <i class="ion-chevron-right pull-right"></i>
                            </a>
                            <ul>
									<li class="nav-dropdown dropdown-submenu" ><a href="#"><i class="ion-ios-world"></i> Country</a>
									<ul class="dropdown-menu">
									<li><a href="/showallLocation">Show All Countries</a></li>
									<li><a href="/manageCounty">Add Country</a></li>
									</ul>
									</li>
									<li class="nav-dropdown dropdown-submenu" ><a href="#"><i class="ion-earth"></i> State</a>
									<ul class="dropdown-menu">
									<li><a href="/showallStates">Show All States</a></li>
									<li><a href="/manageState">Add State</a></li>
									</ul>
									</li>
									<li class="nav-dropdown dropdown-submenu" ><a href="#"><i class="ion-ios-navigate"></i> City</a>
									<ul class="dropdown-menu">
									<li><a href="/showallCities">Show All Cities</a></li>
									<li><a href="/manageCity">Add City</a></li>
									</ul>
									</li>
                            </ul>
                        </li>
						
						<!-- Start here warehouse section -->
						<li class="nav-dropdown">
                            <a title="Warehouse" href="#">
                                <i class="fa fa-cubes"></i> <span>Warehouse</span>
                                <i class="ion-chevron-right pull-right"></i>
                            </a>
                            <ul>
									<li><a href="/showallWarehouses">Show All Warehouse</a></li>
									<li><a href="/manageWarehouse">Add Physical Warehouse</a></li>
                            </ul>
                        </li>
						<!-- End here warehouse section -->
						
						<!-- Start here client section -->
						<li class="nav-dropdown">
                            <a title="Clients" href="#">
                                <i class="ion-ios-color-filter-outline"></i> <span>Clients</span>
                                <i class="ion-chevron-right pull-right"></i>
                            </a>
                            <ul>
									<li><a href="/showall/Client/List">Show All Client</a></li>
									<li><a href="/manage/client/new">Add Client</a></li>
                            </ul>
                        </li>
						<!-- End here client section -->
						<!-- Start here for Inventory section -->
						<li class="nav-dropdown">
                            <a title="Inventory Management" href="#">
                                <i class="ion-clipboard"></i> <span>Inventory Management</span>
                                <i class="ion-chevron-right pull-right"></i>
                            </a>
                            <ul>
									<li class="nav-dropdown dropdown-submenu" ><a title="Suppliers" href="#">
								
                                <i class="ion-android-person-add"></i> Suppliers
                                <i class="ion-chevron-right pull-right"></i>
                            </a>
                            <ul class="dropdown-menu">
									<li><a href="/showAllSupplier">Show Supplier</a></li>
									<li><a href="/manageSupplier">Add New Supplier</a></li>	
						    </ul>
									</li>
									<li class="nav-dropdown dropdown-submenu" ><a title="Categories" href="#">
                                <i class="ion-merge"></i> Categories
                                <i class="ion-chevron-right pull-right"></i>
								
                            </a>
                            <ul class="dropdown-menu">
									<li><a href="/manageCategory">Add New Category</a></li>	
									<li><a href="/showAllCategory">Show Category</a></li>	
						    </ul>
									</li>
									<li class="nav-dropdown dropdown-submenu" ><a title="Brands" href="#">

								<i class="fa fa-shirtsinbulk"></i> Brands
								<i class="ion-chevron-right pull-right"></i>
							</a>
							<ul class="dropdown-menu">
								<li><a href="/showAllBrand">Show Brands</a></li>
								<li><a href="/manageBrand">Add Brand</a></li> 
							</ul>
									</li>
									<li class="">
							<a title="Product Attributes" href="/attribute">
								<i class="fa fa-buysellads"></i> Product Attributes
							</a>
						</li>
                            </ul>
                        </li>
						<!-- End here Inventory section -->
						<!-- Start here for supplier section -->
						
						<!-- End here user for supplier section -->
						
						<!-- Start here for category section -->
						
						<!-- End here user for category section -->
						
						<!-- Start here for Brands section -->
						
						<!-- End here user for Brands section -->
						
						<!-- Start here for attribute section -->
						
						<!-- End here user for attribute section -->
						
						<!-- Start here for linnworks API -->
						<li class="nav-dropdown">
                            <a title="Linnworks API" href="#">
                                <i class="fa fa-plug"></i> <span>Linnworks API</span>
                                <i class="ion-chevron-right pull-right"></i>
                            </a>
                            <ul>
								<li>
								<a href="/JijGroup/Generic/Category"><i class="ion-merge"></i> Categories</a>
								</li>
								<li>
								<a href="/JijGroup/Generic/Store/Location"><i class="fa fa-map-marker"></i> Locations</a>
								</li>
								<li>
								<a href="/JijGroup/Generic/Services/Postal"><i class="ion-email"></i> Postal Services</a>
								</li>
								<li>
								<a href="/JijGroup/Generic/StockItem"><i class="fa fa-database"></i> Stock Item</a>
								</li>
								<li class="nav-dropdown dropdown-submenu"><a title="Brands" href="#">
								<i class="fa fa-gavel"></i> Order Management
								<i class="ion-chevron-right pull-right"></i>
							</a>
							<ul class="dropdown-menu">
								<li><a href="/JijGroup/Generic/Order/GetOrder">Orders</a></li>
								<li><a href="/JijGroup/Generic/Order/GetOpenFilter">Open Order</a></li> 	
								<li><a href="/JijGroup/Generic/Order/OrderFilter">Order Filter</a></li> 
								<li><a href="/JijGroup/Generic/Status/Order">Order Status</a></li>	
							</ul>
							</li>

                            </ul>
                        </li>
						<li class="nav-dropdown">
                            <a title="Linnworks API" href="#">
                                <i class="ion-grid"></i> <span>Delivery Matrix</span>
                                <i class="ion-chevron-right pull-right"></i>
                            </a>
                            <ul>
								<li class="nav-dropdown dropdown-submenu"><a title="Postal Service" href="#">
								<i class="ion-email"></i> Postal Service
								<i class="ion-chevron-right pull-right"></i>
							</a>
							<ul class="dropdown-menu">
								<li><a href="/JijGroup/showallmatrix">Show Postal Service</a></li>
								<li><a href="/JijGroup/DeleveryMatrix">Add Postal Service</a></li> 		
							</ul>
							</li>
							<li class="nav-dropdown dropdown-submenu"><a title="Postal Service" href="#">
								<i class="ion-cube"></i> Postal Provider
								<i class="ion-chevron-right pull-right"></i>
							</a>
							<ul class="dropdown-menu">
								<li><a href="/JijGroup/showallpostalprovider">Show Postal Provider</a></li>	
								<li><a href="/JijGroup/addPostalProvider">Add Postal Provider</a></li>		
							</ul>
							</li>

                            </ul>
                        </li>
						
                    </ul>
					<!-- END NAV -->				
			</div><!-- /.sidebar -->
        </div>
