<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4 style="float:left;">Fba Inventory</h4>				 
				<a href="<?php echo Router::url('/', true) ?>FbaListInventory" class="btn btn-primary" style="float:right;">Back</a>
				<br /><br />
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						<form method="get" name="searchfrm" action="<?php echo Router::url('/', true) ?>FbaListInventory/Search" enctype="multipart/form-data">	
						<div class="col-sm-6 no-padding">
							<div class="col-lg-8 no-padding">
									<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Enter FNSKU, SELLER SKU Or ASIN." name="searchkey" class="form-control searchString" />
								</div>
								
								<div class="col-lg-4"> 
									<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
								</div>
						</div>
						</form>
						<div class="col-sm-6 text-right">
						 
						</div>	
						</div>	
						
					<div class="panel-body no-padding-top bg-white">
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div style="float:left; width:17%">Seller SKU</div>		
								<div style="float:left; width:17%">FNSKU</div>	
								<div style="float:left; width:17%">ASIN</div>			
							 	<div style="float:left; width:12%">TotalSupplyQty</div>																	
								<div style="float:left; width:12%">InStockSupplyQty</div>	
								<div style="float:left; width:12%">EarliestAvailability</div>	
								 
								 
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						 if(count($all_inventory) > 0) {	 ?>
							<?php foreach( $all_inventory as $items ) {     ?>
								<div class="row sku" id="r_<?php echo $items['FbaInventory']['id']; ?>">
									<div class="col-lg-12">	
										<div style="float:left; width:17%"><small> <a href="<?php echo Router::url('/', true) ?>FbaListInventory/SupplyDetail/<?php echo $items['FbaInventory']['id']; ?>" title="View supply detail"  style="color:#000099; text-decoration:underline;"><?php echo $items['FbaInventory']['seller_sku']; ?></a></small></div>	
										<div style="float:left; width:17%"><small><?php echo $items['FbaInventory']['fnsku']; ?></small></div>	
										<div style="float:left; width:17%"><small><?php echo $items['FbaInventory']['asin'] ?></small></div> 		
										 								
										
										<div style="float:left; width:12%"><small><center><?php echo $items['FbaInventory']['total_supply_quantity'];  ?></center></small></div>
										<div style="float:left; width:12%"><small><center><?php echo $items['FbaInventory']['in_stock_supply_quantity']; ?></center></small></div>	
										<div style="float:left; width:12%">  <small><center><?php echo $items['FbaInventory']['earliest_availability']; ?></center></small></div>	
										
										 
									</div>												
								</div>
								 
							<?php }?>
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	

$('#filter_shipment').click(function(){  
	document.getElementById("shipmentFrm").submit();		 
});
	
function updateStatus(t,v){	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaInventorys/updateStatus',
		type: "POST",				
		cache: false,			
		data : {status:$(t).find("option:selected").val(),id:v },
		success: function(data, textStatus, jqXHR)
		{	
			 	
			if(data.error){
				alert(data.msg);
			} 							
		}		
 	});  		 
}
</script>
