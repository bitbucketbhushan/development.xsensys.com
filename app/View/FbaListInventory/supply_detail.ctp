<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4 style="float:left;">Supply Detail</h4>
				<a href="<?php echo Router::url('/', true) ?>FbaListInventory" class="btn btn-primary" style="float:right;">Back</a>
				<br /><br />
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			
			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">							
							  
						</div>	
						
					<div class="panel-body no-padding-top bg-white">
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div style="float:left; width:14%">Seller SKU</div>		
								<div style="float:left; width:12%"><center>Quantity</center></div>	
								<div style="float:left; width:14%">SupplyType</div>			
							 	<div style="float:left; width:14%">EarliestAvailability<br /><span style="font-size:9px;">(Pick Timepoint Type)</span></div>																	
								<div style="float:left; width:15%">EarliestAvailability<br /><span style="font-size:9px;">(Pick Date Time)</span></div>	
								<div style="float:left; width:15%">LatestAvailability<br /><span style="font-size:9px;">(Pick Timepoint Type)</span></div>	
								<div style="float:left; width:15%">LatestAvailability<br /><span style="font-size:9px;">(Pick Date Time)</span></div>	 
								 
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						 if(count($fba_inventory) > 0) {	 ?>
							<?php foreach( $fba_inventory as $items ) {     ?>
								<div class="row sku">
									<div class="col-lg-12">	
										<div style="float:left; width:14%"><small><?php echo $items['FbaInventorySupplyDetail']['seller_sku']; ?></small></div>	
										<div style="float:left; width:12%"><small><center><?php echo $items['FbaInventorySupplyDetail']['quantity']; ?></center></small></div>	
										<div style="float:left; width:14%"><small><?php echo $items['FbaInventorySupplyDetail']['supply_type'] ?></small></div> 		
										 								
										
										<div style="float:left; width:14%"><small><?php echo $items['FbaInventorySupplyDetail']['earliest_available_to_pick_timepoint_type'];  ?></small></div>
										<div style="float:left; width:15%"><small><?php echo $items['FbaInventorySupplyDetail']['earliest_available_to_pick_date_time']; ?></small></div>	
										<div style="float:left; width:15%">  <small><?php echo $items['FbaInventorySupplyDetail']['latest_available_to_pick_timepoint_type']; ?></small></div>	
										
										 <div style="float:left; width:12%">  <small><?php echo $items['FbaInventorySupplyDetail']['latest_available_to_pick_date_time']; ?></small></div>
									</div>												
								</div>
								 
							<?php }?>
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	

$('#filter_shipment').click(function(){  
	document.getElementById("shipmentFrm").submit();		 
});
	
function updateStatus(t,v){	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaInventorySupplyDetails/updateStatus',
		type: "POST",				
		cache: false,			
		data : {status:$(t).find("option:selected").val(),id:v },
		success: function(data, textStatus, jqXHR)
		{	
			 	
			if(data.error){
				alert(data.msg);
			} 							
		}		
 	});  		 
}
</script>
