<?php
$currentUri = $_SERVER["REQUEST_URI"];
$params = explode('/', $currentUri);

$user 		= isset($params[3]) ? $params[3] : '' ;
$startDate	= isset($params[4]) ? $params[4] : '' ;
$endDate 	= isset($params[5]) ? $params[5] : '' ;
?>
<div class="rightside bg-grey-100" style="margin-left: 0px;">
    <div class="page-head bg-grey-100">        
	        <h1 class="page-title">Profit Calculation</h1>
        	<!--head Start--->
				<div class="row head" style="clear:both;">
					<div class="col-sm-12" >
						<!--<div class="col-lg-12" style="background-color: #cfd8dc; padding: 15px; font-size: 14px;">
							<!--<div class="col-sm-4" style="font-weight: bold;" >Total :- <strong class="total_profit"></strong></div>-->
						<!--</div>-->
						<div class="col-sm-12" style="background-color: #e9eaea; padding: 5px; font-size: 12px;">
							<div class="col-sm-4">
								<select class="form-control input-sm user_select">
									<option>Select User</option>
									<option value="AllUser">All User</option>
									<?php foreach( $getUserName as $index => $value ) { 
										if($value != '----') {
									?>
									<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
									<?php } } ?>
								</select>
							</div>
							<div class="col-sm-4">
								<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
								</div>
							<div class="col-sm-4">
							<a href="javascript:void(0);" class="search_user_profit btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Search</a>
							<a href="/Profits/getAllUserProfit" class="btn btn-info color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Refresh</a>
							<a href="/Profits/currentUserGenerateFile/<?php echo $user.'/'.$startDate.'/'.$endDate; ?>" class="btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Generate xlsx</a>
							</div>
						</div>
					</div>
				</div>	
			<!--head End---->


    </div>
    <div class="container-fluid" style="padding:0px;">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
								<div class="panel-body no-padding-top bg-white tab-pane-container">
								<div class="table-responsive">	
			<table style="table-layout:fixed" class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info" data-page-length='3'>
				<thead>
					<tr>
						<th width="100px">Order ID</th>
						<!--<th width="100px">Order Date</th>-->
						<!--<th width="100px">Status</th>-->
						<th width="120px">Listing User</th>
						<!--<th width="130px">Purchase User</th>-->
						<th width="120px">Source Sku</th>
						<th width="50px">Qty</th>
						<th width="100px">Category</th>
						<th width="150px">Service Provider</th>
						<!--<th width="100px">Service</th>-->
						<th width="100px">Pick</th>
						<th width="100px">Pack</th>
						<th width="100px">Referral fee</th>
						<th width="100px">Postage cost</th>
						<th width="100px">Line Haul</th>
						<th width="100px">P.Cost</th>
						<th width="100px">Sell Price</th>
						<th width="100px">Postage Price</th>
						<th width="100px">Total Sell Price</th>
						<th width="100px">Profit/Loss</th>
					</tr>
				</thead>
				<tbody>
					   <?php if(count($getUserOrders) > 0){ foreach( $getUserOrders as $OrPr ) { ?>
					 <tr>
						<td>
							<?php 
								echo $OrPr['OrderProfit']['order_id'].'<br>'.$OrPr['OrderProfit']['order_date'].'<br>('.$OrPr['OrderProfit']['status'].')'; 
							?>
						</td>
						<!--<td><?php echo $OrPr['OrderProfit']['order_date']; ?></td>
						<td><?php echo $OrPr['OrderProfit']['status']; ?></td>-->
						<td>
							<?php 
								echo $OrPr['OrderProfit']['listing_user'].'<br>'.$OrPr['OrderProfit']['purchace_user']; 
							?>
						</td>
						<!--<td><?php echo $OrPr['OrderProfit']['purchace_user']; ?></td>-->
						<td><?php echo $OrPr['OrderProfit']['sku']; ?></td>
						<td><?php echo $OrPr['OrderProfit']['quantity']; ?></td>
						<td><?php echo $OrPr['OrderProfit']['category']; ?></td>
						<td>
							<?php 
								echo $OrPr['OrderProfit']['service_provider'].'<br> ( '.$OrPr['OrderProfit']['provider_code'].')'; 
							?>
						</td>
						<!--<td><?php echo $OrPr['OrderProfit']['provider_code']; ?></td>-->
						<td><?php echo $OrPr['OrderProfit']['quantity'].'x'.$OrPr['OrderProfit']['pick_rate_per'].'<b>='.$OrPr['OrderProfit']['pick_charge'].'</b>'; ?></td>
						<td><?php echo $OrPr['OrderProfit']['quantity'].'x'.$OrPr['OrderProfit']['pack_rate_per'].'<b>='.$OrPr['OrderProfit']['pack_charge']; ?></td>
						<td><?php echo $OrPr['OrderProfit']['referral_fee'].'<b>='.$OrPr['OrderProfit']['ref_charge']; ?></td>
						<td><?php echo '<div style="text-align:center;">'.$OrPr['OrderProfit']['per_item_rate'].'<b>='.$OrPr['OrderProfit']['per_item_cost'].'</b><br>'.
						$OrPr['OrderProfit']['per_kilo_rate'].'<b>='.$OrPr['OrderProfit']['per_kilo_cost'].'</div>';
						echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Per Item '.$OrPr['OrderProfit']['peritem_postage_cost'].'<br></strong></div>';
						 ?></td>
						<td><?php echo $OrPr['OrderProfit']['dhl_cost']; ?></td>
						<td><?php echo $OrPr['OrderProfit']['price'].'<b>=='.$OrPr['OrderProfit']['product_total_cost'].'</b>'; ?></td>
						<td><?php echo $OrPr['OrderProfit']['selling_price_per_pro']; ?></td>
						<td><?php echo $OrPr['OrderProfit']['quantity']*$OrPr['OrderProfit']['postage_charge']; ?></td>
						<td><?php echo $OrPr['OrderProfit']['total_charge']; ?></td>
						<?php $sty = '';
						if($OrPr['OrderProfit']['profit_lose'] < 0){
						$sty = 'background-color: red';
						} ?>
					
						<td style="<?php echo $sty; ?>" ><?php echo $OrPr['OrderProfit']['profit_lose']; ?></td>
					</tr>
					<?php } } ?>
					
					</tbody>
				</table>
                                </div>
								<div class="col-lg-12" style="line-height: 2pc; font-size: 13px; font-weight: bold; margin-top: 51px;">
									<div class="col-lg-6"></div>
									<div class="col-lg-6">
										<div class="col-sm-4">Summary</div>
										<div class="col-sm-4">Total Qty</div>
										<div class="col-sm-4"><?php echo $prof_info['totqty']; ?></div>
									</div>
									<div class="col-lg-6"></div>
									<div class="col-lg-6">
										<div class="col-sm-4"></div>
										<div class="col-sm-4">Total Purchase Price</div>
										<div class="col-sm-4"><?php echo $prof_info['total_product_price']; ?></div>
									</div>
									<div class="col-lg-6"></div>
									<div class="col-lg-6">
										<div class="col-sm-4"></div>
										<div class="col-sm-4">Warehouse/Postage Cost</div>
										<div class="col-sm-4"><?php echo $prof_info['ware_post']; ?></div>
									</div>
									<div class="col-lg-6"></div>
									<div class="col-lg-6">
										<div class="col-sm-4"></div>
										<div class="col-sm-4">Total Product Price</div>
										<div class="col-sm-4"><?php echo $prof_info['total_product_price'] + $prof_info['ware_post']; ?></div>
									</div>
									<div class="col-lg-6"></div>
									<div class="col-lg-6">
										<div class="col-sm-4"></div>
										<div class="col-sm-4">Total Selling</div>
										<div class="col-sm-4"><?php echo $prof_info['total_selling']; ?></div>
									</div>
									<div class="col-lg-6"></div>
									<div class="col-lg-6">
										<div class="col-sm-4"></div>
										<div class="col-sm-4">Profit/Loss</div>
										<div class="col-sm-4"><?php echo $prof_info['proloss']; ?></div>
									</div>
									<div class="col-lg-6"></div>
									<div class="col-lg-6">
										<div class="col-sm-4"></div>
										<div class="col-sm-4">Profit/Loss %</div>
										<div class="col-sm-4"><?php echo  round((($prof_info['proloss']/$prof_info['total_selling']) * 100), 2).' %'; ?></div>
									</div>
								</div>
                            </div>
							
					    </div><!-- /.col -->
						
						
                    </div><!-- /. row -->
				<!-- BEGIN FOOTER -->
				
				
				
				<footer class="bg-white">
					<div class="pull-left">
						<span class="pull-left margin-right-15">&copy; 2015 WMS by JijGroup.</span>
						<ul class="list-inline pull-left">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
						</ul>
					</div>
				</footer>
				<!-- END FOOTER -->
            </div>
    </div>
</div>
<div class="showPopupForAssignSlip"></div> 
<div class="showPopupForcomment"></div> 

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>



<script>

$(document).ready(function(){
				var total = 0;
				$('.totla_lose_profit').each(function() {
						var lopro	= $(this).attr('for');
						total = parseFloat(total) + parseFloat(lopro);
						
					});
					$('.total_profit').text( total.toFixed(3) );
					
	});
	

$('body').on('click', '.search_user_profit', function(){
  
  		var getFrom 	= $('div.daterangepicker_start_input input:text').val();
		var getEnd 		= $('div.daterangepicker_end_input input:text').val();
		var dateArEnd 	= getEnd.split('/');
		var newDateEnd 	= dateArEnd[2] + '-' + dateArEnd[0].slice(-2) + '-' + dateArEnd[1];
		var dateArFrom 	= getFrom.split('/');
		var newDateFrom = dateArFrom[2] + '-' + dateArFrom[0].slice(-2) + '-' + dateArFrom[1];
		
		var selectedUser = $('.user_select').val();
	
		if( selectedUser == 'Select User' ){
			alert( 'Please Select User.' );
			return false;
		}
		
		var selectedDate = newDateFrom;
		var startDate = newDateEnd;
		window.location = "<?php echo Router::url('/', true) ?>Profits/getAllUserProfit/"+selectedUser+"/"+selectedDate+"/"+startDate;
  });

</script>

