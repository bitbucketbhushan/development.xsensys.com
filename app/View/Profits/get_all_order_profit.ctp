<style>
div.dataTables_wrapper {
        width: 1230px;
        margin: 0 auto;
		height:300px;
		overflow:scroll;
    }
	.summary{border:1px solid #ddd; border-radius:5px;}
	.bottom{border-bottom:1px solid #ddd;}
</style>
<?php
$currentUri = 	$_SERVER["REQUEST_URI"];
$params 	= 	explode('/', $currentUri);
$start 		= 	isset($params[3]) ? $params[3] : '' ;
$end 		= 	isset($params[4]) ? $params[4] : '' ;
?>
<div class="rightside bg-grey-100" style="margin-left: 0px;">
    <div class="page-head bg-grey-100">        
	        <h1 class="page-title">Profit Calculation</h1>
        	<!--head Start--->
				<div class="row head" style="clear:both;">
					<div class="col-sm-12" >
						<div class="col-lg-12" style="background-color: #cfd8dc; padding: 15px; font-size: 14px;">
							<!--<div class="col-sm-4" style="font-weight: bold;" >Total :- <strong class="total_profit"></strong></div>-->
						</div>
						<div class="col-sm-12" style="background-color: #e9eaea; padding: 5px; font-size: 12px;">
							<!--<div class="col-sm-4">
								<select class="form-control input-sm channel_select">
									<option>Select Channel</option>
									<option value="All Channel">All Channel</option>
									<?php foreach( $getChannelName as $index => $value ) { 
										if($value != '----') {
									?>
									<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
									<?php } } ?>
								</select>
							</div>-->
							<div class="col-sm-4">
								<select class="form-control input-sm date_select" id="choose_order">
										<option value="select_type">Select Order Type</option>
										<option value="sub_order">Sub Order</option>
										<option value="main_order">Main Order</option>
								</select>
							</div>
							<div class="col-sm-4">
								<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
							</div>
							<div class="col-sm-4">
							<a href="javascript:void(0);" class="search_order_profit btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Search</a>
							<a href="/Profits/getAllOrderProfit" class="btn btn-info color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Refresh</a>
							<a href="/Profits/generateLossProfitSheet/<?php echo $start; ?>/<?php echo $end; ?>" class="btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Generate xlsx</a>
							</div>
						</div>
					</div>
				</div>	
			<!--head End---->


    </div>
    <div class="container-fluid" style="padding:5px;">
		<div class="row">
                        <div class="col-lg-12" style="padding:10px;">
							<div class="panel no-border ">
								<div class="panel-body bg-white tab-pane-container" style="padding-top:5px;">
								<div class="table-responsive">	
			<table style="table-layout:fixed;" class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info" data-page-length='1000'>
				<thead>
					<tr>
						<th width="100px">Order ID</th>
						<th width="100px">Split ID</th>
						<th width="100px">Order Date</th>
						<th width="100px">Source Sku</th>
						<th width="100px">Category</th>
						<th width="100px">Service Provider</th>
						<th width="100px">Service</th>
						<th width="100px">Pick</th>
						<th width="100px">Pack</th>
						<th width="100px">Referral fee</th>
						<th width="100px">Postage cost</th>
						<th width="100px">Line Haul</th>
						<th width="100px">P.Cost</th>
						<th width="100px">Sell Price</th>
						<th width="100px">Postage Price</th>
						<th width="100px">Total Sell Price</th>
						<th width="100px">Profit/Loss</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(count($splitOrders) > 0){
					foreach( $splitOrders as $splitOrder ) { ?>
					<tr>
						<td><?php echo $splitOrder[0]['OrderProfit']['order_id']; ?></td>
						<td>
						<?php foreach( $splitOrder as $detail) {  
									echo $detail['OrderProfit']['merge_update_id']."<br>"; 
									} ?>
						</td>
						<td><?php echo $splitOrder[0]['OrderProfit']['order_date']; ?></td>
						<td><?php foreach( $splitOrder as $detail) {  
								echo '<div style="border-bottom: 1px dashed #444;">'.$detail['OrderProfit']['quantity'] .'X'. $detail['OrderProfit']['sku']."</div>"; 
								}
								?>
						</td>
						<td>
						<?php foreach( $splitOrder as $detail) {  
								echo $detail['OrderProfit']['category']."</br>"; 
								} ?>
						</td>
						<td><?php echo $splitOrder[0]['OrderProfit']['service_provider']; ?></td>
						<td><?php echo $splitOrder[0]['OrderProfit']['service_name']; ?></td>
						<td><?php 
							$pictot = 0;
							foreach( $splitOrder as $detail) {  
							echo '<div style="text-align:center;">'.$detail['OrderProfit']['quantity'].'x'.$detail['OrderProfit']['pick_rate_per']."==<b>".$detail['OrderProfit']['pick_charge']."</b></div>"; 										$pictot  = $pictot + $detail['OrderProfit']['pick_charge'];
							} 
							echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$pictot ."</strong></div>";
							?> 
						</td>
						<td><?php 
						$pactot = 0;
						foreach( $splitOrder as $detail) {
							echo '<div style="text-align:center;">'.$detail['OrderProfit']['pack_charge']."</div>"; 
							$pactot  = $pactot + $detail['OrderProfit']['pack_charge'];	
						}
						echo '<div style="text-align:center; border-top: 1px dashed #444;"><strong>Total '.$pactot."</strong></div>";
						?></td>
						<td>
							<?php 		
							$sour_fee = 0;
							foreach( $splitOrder as $detail) { 
								$sour_fee	=	$sour_fee + $detail['OrderProfit']['ref_charge'];
								echo '<div style="text-align:center;">'.$detail['OrderProfit']['referral_fee']."===<b>".$detail['OrderProfit']['ref_charge']."</b></div>"; 									
								}
								echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$sour_fee.'</strong></div>';							
							 ?>
		 				</td>
						<td>
							<?php
								echo '<div style="text-align:center;">'.$splitOrder[0]['OrderProfit']['per_item_rate'].'==<b>'.$splitOrder[0]['OrderProfit']['per_item_cost'].'</b><br>'.$splitOrder[0]['OrderProfit']['per_kilo_rate'].'==<b>'.$splitOrder[0]['OrderProfit']['per_kilo_cost'].'</b></div>'; 
								$shippingCost = $splitOrder[0]['OrderProfit']['per_item_cost'] + $splitOrder[0]['OrderProfit']['per_kilo_cost'];
								echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$shippingCost.'</strong></div>';
								foreach( $splitOrder as $detail) { 
								echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Per Item '.$detail['OrderProfit']['peritem_postage_cost'].'<br></strong></div>';
								}
							?>
						</td>
						<td>
							<?php 
								$dhl_tot = 0;
								foreach( $splitOrder as $detail) { 
									$dhl_tot = $dhl_tot + $detail['OrderProfit']['dhl_cost'];
									echo '<div style="text-align:center;">'.$detail['OrderProfit']['dhl_cost'].'</div>'; 
								}
									echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$dhl_tot.'</strong></div>';	
							?>
						</td>
						<td>
							<?php 
							$totpropri = 0;
							foreach( $splitOrder as $detail) { 
								echo '<div style="text-align:center;">'.$detail['OrderProfit']['price'].'<strong>=='.$detail['OrderProfit']['product_total_cost'].'</strong></div>';																		//$totpropri = $totpropri + $splitOrder[0]['OrderProfit']['product_total_cost'];
								$totpropri = $totpropri + $detail['OrderProfit']['product_total_cost'];
								}
								echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$totpropri.'</strong></div>'; 
							?>
						</td>
						<td><?php
							$psp = 0;
							foreach( $splitOrder as $detail) { 
								$psp	=	$psp + $detail['OrderProfit']['selling_price_per_pro'];
								echo '<div style="text-align:center;">'.$detail['OrderProfit']['selling_price_per_pro']."</div>"; 									
								}
								echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$psp.'</strong></div>';	
								?>
						</td>
						<td><?php 
						$pch = 0;
						foreach( $splitOrder as $detail) { 
							$pch = $pch + $detail['OrderProfit']['postage_charge'];
						}
						echo $pch;	
						?></td>
						<td>
							<?php 
							$to_ch = 0;
							foreach( $splitOrder as $detail) { 
								//echo '<div style="text-align:center;"><strong>Sub T '.$splitOrder[0]['OrderProfit']['subtotal'].'</strong></div>';
								//echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Post '.$splitOrder[0]['OrderProfit']['postage_charge'].'</strong></div>';	
								echo '<div style="text-align:center;">'.$detail['OrderProfit']['total_charge']."</b></div>"; 
								$to_ch  = $to_ch + $detail['OrderProfit']['total_charge'];
								//echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$splitOrder[0]['OrderProfit']['total_charge'].'</strong></div>';	
								}
								echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$to_ch ."</strong></div>";
							?>
						</td>
						
						<?php 
						$sty = '';
						if($to_ch < $totpropri){
						$sty = 'background-color: red';
						} 
						 ?>
						<td style="<?php echo $sty; ?>">
						<div class="totla_lose_profit" for="<?php echo $detail['OrderProfit']['profit_lose']; ?>">
							
							<?php 	echo '<div style="text-align:center;">'.$to_ch.'-'.$totpropri.'</div>';
									echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$detail['OrderProfit']['profit_lose']."</strong></div>";
							?>
						</div>
						<!--<?php 
						$prototcost = '0';
						$prosellcost = '0';
						foreach( $splitOrder as $detail) { 
						$prototcost	=	$prototcost + $detail['OrderProfit']['product_total_cost'];
						$prosellcost =  $prosellcost + $detail['OrderProfit']['selling_price_per_pro'];
						}
						$proloss = $prosellcost - $prototcost;
						echo $proloss.'('. $splitOrder[0]['OrderProfit']['currency'] .')'; 
						
						?>-->
						</td>
						
					</tr>
					<?php } }?>
					</tbody>
				</table>
								<div class="" style="margin:0 auto; width:350px">
									 <ul class="pagination">
									  <?php
										   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
										   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									  ?>
									 </ul>
								</div>
								
                                </div>
								<div class="col-lg-12" style="line-height: 2pc; font-size: 13px; font-weight: bold; margin-top: 25px;">
									<div class="col-lg-8"></div>
									<div class="col-lg-4 summary">
									<div class="row">
										<div class="col-sm-12" style="font-weight:bold; border-bottom:1px solid #ddd; font-size:20px;">Summary</div>
									</div>
									
									<div class="row bottom">
									<?php if( !empty($this->params['pass']) && $this->params['pass'][2] == 'sub_order' ) { ?>
										<div class="col-sm-8">Total Split Order</div>
									<?php } else { ?>
									<div class="col-sm-8">Total Order</div>
									<?php } ?>
										<div class="col-sm-4"><?php echo $prof_info['total_sub_order']; ?></div>
									</div>
									
									<div class="row bottom">
										<div class="col-sm-8">Total Qty</div>
										<div class="col-sm-4"><?php echo $prof_info['totqty']; ?></div>
									</div>
									
									<div class="row bottom">
										
										<div class="col-sm-8">Total Purchase Price</div>
										<div class="col-sm-4"><?php echo $prof_info['total_product_price']; ?></div>
									</div>
									
									<div class="row bottom">
									
										<div class="col-sm-8">Warehouse/Postage Cost</div>
										<div class="col-sm-4"><?php echo $prof_info['ware_post']; ?></div>
										</div>
									
									 <div class="row bottom">
										
										<div class="col-sm-8">Total Product Price</div>
										<div class="col-sm-4"><?php echo $prof_info['total_product_price'] + $prof_info['ware_post']; ?></div>
									</div>
									 <div class="row bottom">
										
										<div class="col-sm-8">Total Selling</div>
										<div class="col-sm-4"><?php echo $prof_info['total_selling']; ?></div>
									</div>
									
									<div class="row bottom">
										
										<div class="col-sm-8">Profit/Loss</div>
										<div class="col-sm-4"><?php echo $prof_info['proloss']; ?></div>
									</div>
									
									<div class="row">
										
										<div class="col-sm-8">Profit/Loss %</div>
										<div class="col-sm-4"><?php echo  round((($prof_info['proloss']/$prof_info['total_selling']) * 100), 2).' %'; ?></div>
										</div>
									</div>
										
									</div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
				<!-- BEGIN FOOTER -->
				<footer class="bg-white">
					<div class="pull-left">
						<span class="pull-left margin-right-15">&copy; 2015 WMS by JijGroup.</span>
						<ul class="list-inline pull-left">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
						</ul>
					</div>
				</footer>
				<!-- END FOOTER -->
            </div>
    </div>
</div>
<div class="showPopupForAssignSlip"></div> 
<div class="showPopupForcomment"></div> 

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>



<script>

$(document).ready(function(){
				var total = 0;
				$('.totla_lose_profit').each(function() {
						var lopro	= $(this).attr('for');
						total = parseFloat(total) + parseFloat(lopro);
						
					});
					$('.total_profit').text( total.toFixed(3) );
					
	});
	

$('body').on('click', '.search_order_profit', function(){
  
  		var getFrom 	= $('div.daterangepicker_start_input input:text').val();
		var getEnd 		= $('div.daterangepicker_end_input input:text').val();
		var orderType	= $( '#choose_order' ).find(":selected").val();
		if( orderType == 'select_type' )
		{
			alert('Please select order type');
			return false;
		}
		var dateArEnd 	= getEnd.split('/');
		var newDateEnd 	= dateArEnd[2] + '-' + dateArEnd[0].slice(-2) + '-' + dateArEnd[1];
		var dateArFrom 	= getFrom.split('/');
		var newDateFrom = dateArFrom[2] + '-' + dateArFrom[0].slice(-2) + '-' + dateArFrom[1];
	
		var selectedDate = newDateFrom;
		var startDate = newDateEnd;
		window.location = "<?php echo Router::url('/', true) ?>Profits/getAllOrderProfit/"+selectedDate+"/"+startDate+"/"+orderType;
  });

</script>

