<div class="rightside bg-grey-100" style="margin-left: 0px;">
    <div class="page-head bg-grey-100">  
	<h4 class="margin-bottom-30 color-grey-700">Order summary</h4>
	<div class="col-sm-12 margin-bottom-10 padding-20" style="height:80px; padding-right: 0px; padding-left: 0px; background-color: #e0dddd; border: 1px solid #7e7575;">
	<div class="col-sm-8">
		<div class="col-sm-4"><strong style="font-size:16px;">&#65505;<?php echo number_format($t_summ['tsale'],2); ?></strong><br/>Sales</div>
		<div class="col-sm-4"><strong style="font-size:16px;"><?php echo $t_summ['torder']; ?></strong><br />Orders</div>
		<div class="col-sm-4"><strong style="font-size:16px;">&#65505;<?php echo number_format($t_summ['averg'],2); ?></strong><br />Average Order Value</div>
	</div>
		<div class="col-sm-4">
			<div class="col-sm-6">
				<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
			</div>
			<div class="col-sm-3">
				<a href="javascript:void(0);" class="search_company_profit btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Search</a>
			</div>
			<div class="col-sm-3">
				<a href="/Profits/getCompanyProfit" class="btn btn-info color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Today Sale</a>
			</div>
		</div>
		
	</div>
	<div class="clear:both"></div>
	
	<div class="col-sm-12" style="border: 2px solid #7accf2; padding-right: 0px; padding-left: 0px;padding-bottom: 50px;">
		<div class="col-sm-12" style="height:35px;font-size:16px;background-color:#7accf2; padding-right: 0px; padding-left: 0px;"><strong>Marketplace Sales Overview</strong></div>
		<?php foreach($f_data as $sk=> $sv) { ?>
		<div class="col-sm-12" style="height:35px;font-size: 12px;background-color: #069fe6; padding-right: 0px; padding-left: 0px;">
			<div class="col-sm-2"><strong><?php echo $sk; ?></strong></div>
			<div class="col-sm-3"><strong>Channel Account</strong></div>
			<div class="col-sm-2"><strong>Sales</strong></div>
			<div class="col-sm-2"><strong>Orders</strong></div>
			<div class="col-sm-2"><strong>Average Sales</strong></div>
		</div>
		<?php foreach($sv as $k => $v){ ?>
		<div class="col-sm-12">
			<div class="col-sm-2"><img src="/img/country/<?php echo $v['flag']; ?>.png"></div>
			<div class="col-sm-3"><?php echo $k;  ?></div>
			<div class="col-sm-2"><?php echo number_format($v['f_sale'], 2); ?></div>
			<div class="col-sm-2"><?php echo $v['o_count']; ?></div>
			<div class="col-sm-2"><?php echo number_format($v['f_sale']/$v['o_count'],2); ?></div>
		</div>
		<?php } } ?>
	</div>
	</div>
</div>
<div style="height: 200px;"></div>




<script>
	$('body').on('click', '.search_company_profit', function(){
  
  		var getFrom 	= $('div.daterangepicker_start_input input:text').val();
		var getEnd 		= $('div.daterangepicker_end_input input:text').val();
		var dateArEnd 	= getEnd.split('/');
		var newDateEnd 	= dateArEnd[2] + '-' + dateArEnd[0].slice(-2) + '-' + dateArEnd[1];
		var dateArFrom 	= getFrom.split('/');
		var newDateFrom = dateArFrom[2] + '-' + dateArFrom[0].slice(-2) + '-' + dateArFrom[1];
		var selectedDate = newDateFrom;
		var startDate = newDateEnd;
		window.location = "<?php echo Router::url('/', true) ?>Profits/getCompanyProfit/"+selectedDate+"/"+startDate;
  });
</script>




							
							