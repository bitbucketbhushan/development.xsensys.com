<div class="rightside bg-grey-100" style="margin-left: 0px;">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php //print $role;?></h1>
        	<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid" style="padding:0px;">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
								<div class="panel-body no-padding-top bg-white tab-pane-container">
									<h3 class="color-grey-700">Profit Calculation</h3>
													<table id="" class="table table-bordered table-hover">
														<thead>
															<tr>
																<th>Order ID</th>
																<th>Split ID</th>
																<th>Source Sku</th>
																<th>Category</th>
																<th>Service</th>
																<th>Pick</th>
																<th>Pack</th>
																<th>Referral fee</th>
																<th>Postage cost</th>
																<th>DHL</th>
																<th>P.T.Cost</th>
																<th>Sell Price</th>
																<th>Profit/Loss</th>
															</tr>
														</thead>
														<tbody>
															<?php foreach( $splitOrders as $splitOrder ) { ?>
															<tr>
																<td><?php echo $splitOrder[0]['OrderProfit']['merge_update_id']; ?>
																
																<a id="popoverData" class="moreDetails" href="#" data-content="<table class='popoverTable'><tr><td class='row'><div class='col-sm-6'><b>Order ID</b></div><div class='col-sm-6'><?php echo $splitOrder[0]['OrderProfit']['order_id']; ?></div></td><td class='row'><div class='col-sm-6'><b>Source Sku</b></div><div class='col-sm-6'><?php //echo $item->MergeUpdate->postal_service; ?></div></td><td class='row'><div class='col-sm-6'><b>Pick rate</b></div><div class='col-sm-6'><?php echo $splitOrder[0]['OrderProfit']['pick_rate_per']; ?></div></td><td class='row'><div class='col-sm-6'><b>Pack Rate</b></div><div class='col-sm-6'><?php echo $splitOrder[0]['OrderProfit']['pack_rate_per'] ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Referral Fee</b></div><div class='col-sm-6'><?php echo $splitOrder[0]['OrderProfit']['referral_fee']; ?></div></td><td class='row'><div class='col-sm-6'><b>DHL</b></div><div class='col-sm-6'><?php echo $splitOrder[0]['OrderProfit']['dhl_cost']; ?></div></td><td class='row'><div class='col-sm-6'><b>Per Item</b></div><div class='col-sm-6'><?php echo $splitOrder[0]['OrderProfit']['per_item_rate']; ?></div></td><td class='row'><div class='col-sm-6'><b>Per Kilo</b></div><div class='col-sm-6'><?php echo $splitOrder[0]['OrderProfit']['per_kilo_rate']; ?></div></td></tr><tr><td class='row'><div class='col-sm-6'><b>Service</b></div><div class='col-sm-6'><?php echo $splitOrder[0]['OrderProfit']['service_name']; ?></div></td><td class='row'><div class='col-sm-6'><b>Service ID</b></div><div class='col-sm-6'><?php echo $splitOrder[0]['OrderProfit']['service_id']; ?></div></td><td class='row'><div class='col-sm-6'><b>Delevery Country</b></div><div class='col-sm-6'><?php //echo $item->MergeUpdate->packet_width;?></div></td><td class='row'><div class='col-sm-6'><b>Currency</b></div><div class='col-sm-6'><?php echo $splitOrder[0]['OrderProfit']['currency']; ?></div></td></tr><tr><td colspan='4' class='row'><div class='col-sm-1'><b></b></div><div class='col-sm-11'><?php //echo $item->MergeUpdate->service_name; ?></div></td></tr><tr><td colspan='4' class='row'></td></tr></table>" rel="popover" data-placement="right" data-original-title="<b><?php //echo $item->MergeUpdate->order_id.' ( '.$item->MergeUpdate->product_order_id_identify.' ) '; ?></b>" data-trigger="hover">
												<i class="fa fa-eye"></i>
												</a>
																</td>
																<td>
																<?php foreach( $splitOrder as $detail) {  
																			echo $detail['OrderProfit']['merge_update_id']."<br>"; 
																			} ?>
																</td>
																<td><?php foreach( $splitOrder as $detail) {  
																		echo '<div style="border-bottom: 1px dashed #444;">'.$detail['OrderProfit']['quantity'] .'X'. $detail['OrderProfit']['sku']."</div>"; 
																		}
																		?>
																</td>
																<td>
																<?php foreach( $splitOrder as $detail) {  
																		echo $detail['OrderProfit']['category']."</br>"; 
																		} ?>
																</td>
																<td><?php echo $splitOrder[0]['OrderProfit']['service_name']; ?></td>
																<td><?php 
																	$pictot = 0;
																	foreach( $splitOrder as $detail) {  
																	echo '<div style="text-align:center;">'.$detail['OrderProfit']['quantity'].'x'.$detail['OrderProfit']['pick_rate_per']."==<b>".$detail['OrderProfit']['pick_charge']."</b></div>"; 										$pictot  = $pictot + $detail['OrderProfit']['pick_charge'];
																	} 
																	echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$pictot ."</strong></div>";
																	?> 
																</td>
																<td><?php 
																echo '<div style="text-align:center;"><strong>Total '.$splitOrder[0]['OrderProfit']['pack_rate_per']."</strong></div>";
																?></td>
																<td><?php 
												
												echo '<div style="text-align:center;">'.$splitOrder[0]['OrderProfit']['referral_fee']."==<b>".$splitOrder[0]['OrderProfit']['source_fee']."</b></div>"; 									
												echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$splitOrder[0]['OrderProfit']['source_fee'].'</strong></div>';								echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Per Item '.$splitOrder[0]['OrderProfit']['peritem_source_fee'].'</strong></div>';
												 ?></td>
																<td><?php
												echo '<div style="text-align:center;">'.$splitOrder[0]['OrderProfit']['per_item_rate'].'==<b>'.$splitOrder[0]['OrderProfit']['per_item_cost'].'</b><br>'.$splitOrder[0]['OrderProfit']['per_kilo_rate'].'==<b>'.$splitOrder[0]['OrderProfit']['per_kilo_cost'].'</b></div>'; 
												$shippingCost = $splitOrder[0]['OrderProfit']['per_item_cost'] + $splitOrder[0]['OrderProfit']['per_kilo_cost'];
												echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$shippingCost.'</strong></div>';
												echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Per Item '.$splitOrder[0]['OrderProfit']['peritem_postage_cost'].'</strong></div>';
												?></td>
																<td><?php echo '<div style="text-align:center;"><strong>Total '.$splitOrder[0]['OrderProfit']['dhl_cost'].'</strong></div>'; ?></td>
																<td><?php echo '<div style="text-align:center;"><strong>Total '.$splitOrder[0]['OrderProfit']['product_total_cost'].'</strong></div>'; ?></td>
																<td><?php 
												echo '<div style="text-align:center;"><strong>Sub T '.$splitOrder[0]['OrderProfit']['subtotal'].'</strong></div>';
												echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Post '.$splitOrder[0]['OrderProfit']['postage_charge'].'</strong></div>';	
												echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$splitOrder[0]['OrderProfit']['total_charge'].'</strong></div>';	
												?></td>
																<td><?php echo $splitOrder[0]['OrderProfit']['profit_lose']; ?></td>
															</tr>
															<?php } ?>
															
														</tbody>
													</table>
                                </div>
                                <div class="panel-title bg-white no-border">
									<div class="panel-tools">
									<!--<a class="downloadStockFile btn btn-success btn-xs margin-right-10 color-white"><i class="fa fa-download"></i> Download CSV</a>-->
									</div>
								</div>
								
								
								
                                <div class="panel-body no-padding-top bg-white ">
																	
											<table class="table table-bordered table-striped" id="example1" aria-describedby="example1_info">
													<thead>
											<tr role="row">
												<th style="width: 5%;" >Order ID</th>
												<th style="width: 5%;" >Split ID</th>
												<th style="width: 7%;" >Source Sku</th>
												<!--<th style="width: 7%;" >Sku</th>-->
												<th style="width: 7%;">Category</th>
												<th style="width: 7%;">Service</th>
												<!--<th style="width: 7%;" >Po</th>-->
												<th style="width: 7%;">Pick</th>
												<th style="width: 7%;">Pack</th>
												<th style="width: 7%;">Referral Fee</th>
												<th style="width: 7%;">Postage Cost</th>
												<th style="width: 7%;">DHL</th>
												<th style="width: 7%;">P. T. Cost</th>
												<th style="width: 20%;">Sell price</th>
												<th style="width: 7%;">Profit/Lose</th>
										</thead>
									<tbody role="alert" aria-live="polite" aria-relevant="all">
												
											<?php foreach( $splitOrders as $splitOrder ) { ?>
                                            <tr class="odd">
												<td class="sorting_1"><?php echo $splitOrder[0]['OrderProfit']['merge_update_id']; ?></td>
												<td class="sorting_1"><?php foreach( $splitOrder as $detail) {  
												echo $detail['OrderProfit']['merge_update_id']."<br>"; 
												} ?></td>
												
												<td class="sorting_1"><?php echo '<div><strong>'.$splitOrder[0]['OrderProfit']['source_sku'].'</strong></div>'; 
												foreach( $splitOrder as $detail) {  
												echo '<div style="border-top: 1px dashed #444;">'.$detail['OrderProfit']['quantity'] .'X'. $detail['OrderProfit']['sku']."</div>"; 
												}
												?></td>
												<!--<td class="sorting_1"><?php foreach( $splitOrder as $detail) {  
												echo $detail['OrderProfit']['quantity'] .'X'. $detail['OrderProfit']['sku']."<br>"; 
												} ?></td>-->            
												<td class="sorting_1"><?php foreach( $splitOrder as $detail) {  
												echo $detail['OrderProfit']['category']."</br>"; 
												} ?></td>                             
												<td class="sorting_1"><?php echo $splitOrder[0]['OrderProfit']['service_name']; ?></td>       
												<!--<td class="sorting_1"><?php foreach( $splitOrder as $detail) {  
												echo $detail['OrderProfit']['po_name']."<br>"; 
												} ?></td>-->
												<td class="sorting_1"><?php 
												$pictot = 0;
												foreach( $splitOrder as $detail) {  
												echo '<div style="text-align:center;">'.$detail['OrderProfit']['quantity'].'x'.$detail['OrderProfit']['pick_rate_per']."==<b>".$detail['OrderProfit']['pick_charge']."</b></div>"; 										$pictot  = $pictot + $detail['OrderProfit']['pick_charge'];
												} 
												echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$pictot ."</strong></div>";
												?> </td>
												<td class="sorting_1"><?php 
												echo '<div style="text-align:center;"><strong>Total '.$splitOrder[0]['OrderProfit']['pack_rate_per']."</strong></div>";
												?></td>
												<!--Start Referral fee-->
												<td class="sorting_1"><?php 
												
												echo '<div style="text-align:center;">'.$splitOrder[0]['OrderProfit']['referral_fee']."==<b>".$splitOrder[0]['OrderProfit']['source_fee']."</b></div>"; 									
												echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$splitOrder[0]['OrderProfit']['source_fee'].'</strong></div>';								echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Per Item '.$splitOrder[0]['OrderProfit']['peritem_source_fee'].'</strong></div>';
												 ?></td>
												<!--End Referral fee-->
												<td class="sorting_1">
												<?php
												echo '<div style="text-align:center;">'.$splitOrder[0]['OrderProfit']['per_item_rate'].'==<b>'.$splitOrder[0]['OrderProfit']['per_item_cost'].'</b><br>'.$splitOrder[0]['OrderProfit']['per_kilo_rate'].'==<b>'.$splitOrder[0]['OrderProfit']['per_kilo_cost'].'</b></div>'; 
												$shippingCost = $splitOrder[0]['OrderProfit']['per_item_cost'] + $splitOrder[0]['OrderProfit']['per_kilo_cost'];
												echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$shippingCost.'</strong></div>';
												echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Per Item '.$splitOrder[0]['OrderProfit']['peritem_postage_cost'].'</strong></div>';
												?>
												</td>
												<td class="sorting_1"><?php echo '<div style="text-align:center;"><strong>Total '.$splitOrder[0]['OrderProfit']['dhl_cost'].'</strong></div>'; ?></td>
												<td class="sorting_1"><?php echo '<div style="text-align:center;"><strong>Total '.$splitOrder[0]['OrderProfit']['product_total_cost'].'</strong></div>'; ?></td>
												<td class="sorting_1"><?php 
												echo '<div style="text-align:center;"><strong>Sub T '.$splitOrder[0]['OrderProfit']['subtotal'].'</strong></div>';
												echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Post '.$splitOrder[0]['OrderProfit']['postage_charge'].'</strong></div>';	
												echo '<div style="text-align:center;border-top: 1px dashed #444;"><strong>Total '.$splitOrder[0]['OrderProfit']['total_charge'].'</strong></div>';	
												?>
												</td>
												<td class="sorting_1"><?php echo $splitOrder[0]['OrderProfit']['profit_lose']; ?></td>
										    </tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
				<!-- BEGIN FOOTER -->
				<footer class="bg-white">
					<div class="pull-left">
						<span class="pull-left margin-right-15">&copy; 2015 WMS by JijGroup.</span>
						<ul class="list-inline pull-left">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
						</ul>
					</div>
				</footer>
				<!-- END FOOTER -->
            </div>
    </div>
</div>
<div class="showPopupForAssignSlip"></div> 
<div class="showPopupForcomment"></div> 

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>
	
	$(document).ready(function() {
			var $animationExamples;
        $animationExamples = $(".animated-tab").hashTabs();
		$("[rel='tooltip']").tooltip();
			$('.moreDetails').popover({html:true});
			$('.moreDetails i').mouseover(function() {
				//$('.rackDetails .tab-pane-container').addClass('overlay');
				$('div.overlay').addClass('on');
				//$('.table-striped > tbody > tr:nth-of-type(2n+1)').css('background-color','transparent');
				//$('.overlay .btn').css('display','none');
				$('.moreDetails i').css({"z-index": "9999999999", "position": "relative", "color": "#ffffff"});
			});
			$('.moreDetails i').mouseout(function() {
				$('div.overlay').removeClass('on');
				//$('.table-striped > tbody > tr:nth-of-type(2n+1)').css('background-color','#f9f9f9');
				//$('.overlay .btn').css('display','inline-block');
				$('.moreDetails i').css({"z-index": "9999999999", "position": "relative", "color": "#444444"});
			});
			
			
			//$('#popoverOption').popover({ trigger: "hover" });
					/*$('.openorder').each(function() {
						var currentSku	= $(this).attr('for');
						$.ajax({
								url     	: '/Products/getOpenOrderSkuQty',
								type    	: 'POST',
								data    	: { currentSku : currentSku },  			  
								success 	:	function( data  )
								{
									$('.openorderqty_'+currentSku).text( data );
								}                
						});
					});*/
					
					$('.intransitsku').each(function() {
						var currentSku	= $(this).attr('for');
						$.ajax({
								url     	: '/Products/getIntransitSku',
								type    	: 'POST',
								data    	: { currentSku : currentSku },  			  
								success 	:	function( data  )
								{
									$('.intransitsku_'+currentSku).text( data );
								}                
						});
					});
	});
	
	
	$(function()
	{
		//Download Stock file now
		$( 'body' ).on( 'click', '.downloadStockFile', function()
		{
			//Check If input rack name exists in table or not
			$.ajax({
					url     	: '/Products/prepareExcel',
					type    	: 'POST',
					data    	: {}, 
					success 	:	function( data  )
					{
						 window.open(data,'_self' );
					}                
				});				
		});
		
		$( 'body' ).on( 'click', '.downloadBinLocation', function()
		{
			//Check If input rack name exists in table or not
			$.ajax({
					url     	: '/Outers/getStock_ActualReport',
					type    	: 'POST',
					data    	: {},  			 
					beforeSend 	: function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },  
					success 	:	function( data  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						 window.open(data,'_self' );
					}                
				});
		});	
	});
	
	$('#ProductSearchtext').bind('keypress', function(e)
	{
	   if(e.which == 13) {
		   if($('#ProductSearchtext').val() != '')
			{
				submitSanningValue();
			}
			else
			{
				
			}
			
		}
	});
	
	function submitSanningValue()
	{
		var searchKey	=	$('#ProductSearchtext').val();
		$.ajax({
					url     	: '/Products/getSearchResult',
					type    	: 'POST',
					data    	: {  searchKey : searchKey },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					},   			  
					success 	:	function( data  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.panel-body').html( data );
					}                
				});
	}
	
	$('body').on( 'click', '.create_barcode_popup', function(){
		
		var productId	=	$( this ).attr('for');
		$.ajax({
					url     	: '/Virtuals/getBarcodePopup',
					type    	: 'POST',
					data    	: {  productId : productId },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					},  			  
					success 	:	function( popUpdata  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.showPopupForAssignSlip').html( popUpdata );
					}                
				});
		});
		
	$('body').on( 'click', '.create_qrcode_popup', function(){
		
		var productId	=	$(this).attr('for');
		$.ajax({
					url     	: '/Virtuals/getqrcodePopup',
					type    	: 'POST',
					data    	: {  productId : productId },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					},  			  
					success 	:	function( popUpdata  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.showPopupForAssignSlip').html( popUpdata );
						
					}              
				});
				
		});
	
	$('body').on( 'click', '.create_comment_popup', function(){
		
		var productId	=	$(this).attr('for');
		$.ajax({
					url     	: '/Virtuals/getcommentPopup',
					type    	: 'POST',
					data    	: {  productId : productId },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					},  			  
					success 	:	function( popUpdata  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.showPopupForcomment').html( popUpdata );
						
					}              
				});
				
		});
	
</script>
