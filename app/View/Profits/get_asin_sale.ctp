<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:600; font-size:13px;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.qtbr{min-height: 15px;border-bottom: 1px dotted #444;border-top: 1px dotted #444;font-size: 12px;text-align: center;color: #990000;}
	.cdiv{width:2.55%;text-align:center; float:left;}
	.ms{background-color:#CCFFFF!important;}
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<h4>Asin Sales Report</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			  	<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
					 	<form method="get" name="salefrm" id="salefrm" action="<?php echo Router::url('/', true) ?>Profits/getAsinSale">	
						<div class="col-sm-9 no-padding ">
							 <div class="col-sm-3">					  
								  <select name="report_month" id="report_month" class="form-control" onChange = "loadajax()" style="margin-left: -14px;">
								  		<?php 
											for ($i = 1; $i <= 12; $i++)
											{	$sel ='';
												$month_name = date('F', mktime(0, 0, 0, $i, 1, 2016));
												if(isset($_REQUEST["report_month"])){
													if($_REQUEST['report_month'] == $i){ $sel = 'selected="selected"'; }
												}else{
													if(date('n') == $i){ $sel = 'selected="selected"'; }
												}											
												echo '<option value="'.$i.'" '.$sel.'>'.$month_name.'</option>';
											}
										?>						
									</select>
									
								 </div>
								  <div class="col-sm-3">					  
								  <select name="report_year" id="report_year" class="form-control" onChange="loadajax()">
								 		<?php for ($i = 2017; $i <= date('Y'); $i++)
										{	
											$sel ='';
											if(isset($_REQUEST["report_year"])){
												if($_REQUEST['report_year'] == $i){ $sel = 'selected="selected"'; }
											}else{
												if(date('Y') == $i){ $sel = 'selected="selected"'; }
											}
											echo '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
										}?>						
									</select>					
						 </div>
						 <div class="col-sm-2">	
						 <?php
							if(isset($_REQUEST["report_year"]) && isset($_REQUEST["report_month"]) ){
								$report_year = $_REQUEST["report_year"];
								$report_month = str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT);
							}else{
								$report_year  = date('Y');
								$report_month = date('m'); 
							}
						 ?>				  
						<!--<a href="<?php echo Router::url('/', true) ?>FbaOrders/getSalesReport/<?php echo $report_year."/".$report_month ?>" class="btn btn-success">Get Report</a>-->							
						<a href="<?php echo Router::url('/', true) ?>Profits/getAsinSale" class="btn btn-success">Referesh</a>
						</div>
						<div class="col-sm-2">
						<!--<a href="<?php echo Router::url('/', true) ?>Profits/getAsinSalesReport/<?php echo $report_year."/".$report_month ?>" class="btn btn-info btn bg-info-500">Get Report</a>-->
						</div>	
						</div>
						<div class="col-sm-12 no-padding" style="margin-top: 10px;margin-bottom: 10px;">
							<input autocomplete="off" class="form-control" placeholder="Search By Master SKU" id="master_sku" name="sku" oninput="loadajax();" type="search">
						</div>
						 </form>
						 
					<div class="panel-body no-padding-top bg-white" style=" width:100%">
						
						 <div class="row head" style="clear:both;" >		 					   							
							<?php 
							App::Import('Controller', 'Profits'); 
							$obj = new ProfitsController;	 
							$days_of_month = 30; $total_sale = 0;
							$order_items = array();
							echo '<div class="col-sm-1">SKU</div>';
							echo '<div class="col-sm-1">Seller SKU</div>';
								if(isset($_REQUEST["report_month"])){
									 $days_of_month = cal_days_in_month(CAL_GREGORIAN,$_REQUEST["report_month"],$_REQUEST["report_year"]);
								}else{												 
									$days_of_month = cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y'));
								}
								
								for ($i = 1; $i <= $days_of_month; $i++)
								{													
									$qty = '-';	
									$sale  = 0;
									if(isset($_REQUEST["report_month"])){
										$date = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-'.str_pad($i, 2, "0", STR_PAD_LEFT); 
									  }	else{												 
										$date =   date('Y-m-').str_pad($i, 2, "0", STR_PAD_LEFT); 
									} 	 
										 
								$sale = $obj->getDateSale($date );    
								 	
								$total_sale += $sale;
								$mon  = date("d M y",strtotime($date));
								echo '<div class="cdiv">'. $mon.'<br><span class="qtbr">'.$sale.'</span></div>';			
							} ?>
						<div class="cdiv">Total Sale <?php echo date("M", strtotime($date));?><br><span class="qtbr"><?php echo $total_sale; ?></span></div>  	
					</div>	 
					<div class="body" id="results" >
						 <?php $i = 0; $j = 0;
						 if(!empty($skudata) && count($skudata) > 0) {
						    foreach($skudata  as $ksku => $vsku) {
									echo '<div class="row sku ms">';
										echo '<div class="col-lg-12">';
										
											echo '<div class="col-sm-1">'. $ksku.' <div class="main"><a href="#0" class="inner" for="'.$ksku.'" id="inner_'.$ksku.'">+</a></div><div id="content_'.$ksku.'" style="display: none;"></div></div>';
											echo '<div class="col-sm-1"><small><center>-</center></small></div>';
											$co = 0;
											for ($i = 1; $i <= $days_of_month; $i++)
											{ 
												if(isset($_REQUEST["report_month"])){
													$date = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-'.str_pad($i, 2, "0", STR_PAD_LEFT); 
												  }	else{												 
													$date =   date('Y-m-').str_pad($i, 2, "0", STR_PAD_LEFT); 
												}
												$sku_cou	=	$obj->getSkuCount( $date, $ksku );
												$co = $co + $sku_cou;
												 echo '<div class="cdiv"><small>'.$sku_cou.'</small></div>';						 										 
											}
											 echo '<div class="cdiv"><strong>'.$co.'</strong></div>';	 	 
										echo '</div>';
									echo '</div>';
									
									 
									 foreach($vsku  as $kasin => $vasin ) { 
									 	echo '<div class="row sku">';
										echo '<div class="col-lg-12">';
											
										echo '<div class="col-sm-1">&nbsp;</div>';
										echo '<div class="col-sm-1"><small>'. $kasin.'</small></div>';
										$tot = 0;
										for ($m = 1; $m <= $days_of_month; $m++)
											{
												if(isset($_REQUEST["report_month"])){
													$date = $_REQUEST["report_year"].'-'.str_pad($_REQUEST["report_month"], 2, "0", STR_PAD_LEFT).'-'.str_pad($m, 2, "0", STR_PAD_LEFT); 
												  }	else{												 
													$date =   date('Y-m-').str_pad($m, 2, "0", STR_PAD_LEFT); 
												} 
												$k = 0;
												foreach($vasin as $kdate => $vdate)
												{
													
													if($date == $kdate ){
														$sku_t_sale = 0; 
														foreach( $vdate as $kqty => $vqty )
														{	
															$k++;
															$sku_t_sale = $sku_t_sale + $vqty['qty'];
															$tot = $tot + $vqty['qty'];
														}
														echo '<div class="cdiv"><small>'.$sku_t_sale.'</small></div>';
													} 
												}
												if($k == 0){
													echo '<div class="cdiv"><small>-</small></div>';
												}
												
											}
										echo '<div class="cdiv">'.$tot.'</div>';
										echo '</div>';
										echo '</div>';
										
								}
								$j++;
								if($j == 5){ break; }	
							 } 	
						 } 
						 else {
							echo '<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>';
						  } 
						 ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/15.gif" />
</div>

<!--<div class="main">
  <a href="#0" id="inner">+</a>
</div> 
<div id="content" style="display: none;">
</div>-->
<style>
.main    
	{
			
	}

#content 
	{
		margin-top: 0px;  
		margin-bottom: 30px; 
		background: gray;
		width: auto; height: 50px;
	}
</style>
<script>
var rep_year	=	<?php echo $_REQUEST["report_year"]; ?>;
var rep_month	=	<?php echo $_REQUEST["report_month"]; ?>;

  $(".inner").click(function()
  {
  	
	//alert(rep_year);
  	var sku	=	$( this ).attr('for');
	$.ajax({
					url     	: '/Profits/getStoresale',
					type    	: 'POST',
					data    	: {  sku : sku , rep_year : rep_year, rep_month : rep_month },
					dataType	: 'json',
					/*beforeSend	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
					},  */ 			  
					success 	:	function( dataString  )
					{
							
							var obj = dataString.data;
							var result='<table>'; 
							for ( var p in obj) {
								if( obj.hasOwnProperty(p) ) {
									result += '<tr><td>&nbsp;</td><td style="width:400px;">'+p+'</td><td style="width:20px;">&nbsp;&nbsp;&nbsp;</td><td style="width:20px;">'+obj[p]+'</td></tr>';
									 }
								}
							result += '</table>';
							$('#content_'+sku).html(result);
					}                
				});
				
				$("#content_"+sku).slideToggle( "slow");
				
				  if ($(".inner").text() == "+")
				  {			
					$(".inner").html("-")
				  }
				  else 
				  {		
					$(".inner").text("+")
				  }
    
  });  


</script>

<script>	
function loadajax(){
	document.getElementById("salefrm").submit();		 
}

function filterItems(){
	document.getElementById("salefrm").submit();	
}

</script>
