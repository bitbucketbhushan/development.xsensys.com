<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.col-60 {    width: 60%;    float: left;}
	 
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Europe FBA Orders</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						
						  	   <?php							
											print $this->form->create( 'orders', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/FbaEuOrders/uploadSave', 'type'=>'post','id'=>'orders' ) );
										?>
										<div class="form-group" style="padding: 15px; background-color: #ddd; border-radius: 10px;">
											<label class="control-label col-lg-3">Amazon Order File</label>                                        
											<div class="col-lg-5">
												<div class="input-group">
													<span class="input-group-btn">
														<span class="btn btn-primary btn-file">
															Browse  <?php
																		print $this->form->input( 'orders.uploadfile', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'', 'required'=>false) );
																	  ?>
														</span>
													</span>
															<input type="text" placeholder="No file selected" readonly="" class="form-control">
															<span class="input-group-addon no-padding-top no-padding-bottom">
													
													</span>
											</div>
											
                                        </div>
										<div class="col-lg-2">
										<?php
											echo $this->Form->button('Upload', array(
											'type' => 'submit',
											'escape' => true,
											'class'=>'btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
											 ));	
										?>
										</div>
                                      </div>
								     
								       
								    
								   
                               </form>
							   
						<form method="get" name="searchfrm" action="<?php echo Router::url('/', true) ?>FbaEuOrders/search" enctype="multipart/form-data">	
						<div class="col-sm-5 no-padding">
							<div class="col-lg-8 no-padding">
									<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Enter Order Id, SKU Or ASIN." name="searchkey" class="form-control searchString" />
								</div>
								
								<div class="col-lg-4"> 
									<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
								</div>
						</div>
						</form>
						<div class="col-sm-7 text-right">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						</div>	
					<div class="panel-body no-padding-top bg-white">
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-12">Order Id</div>		
								<div class="col-60">
									<div class="col-sm-12">
										<div class="col-20">Sku</div>											
										<div class="col-50">Title</div>			
										<div class="col-10">Qty</div>																				
										<div class="col-10">Item Price</div>
										<div class="col-10">Shipping Price</div>	
									</div>
								</div>
								
								<div class="col-18"><center>Purchase Date</center></div>	
								<div class="col-10">Action</div>
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						 if(count($all_orders) > 0) {	 ?>
							<?php foreach( $all_orders as $items ) {   ?>
								<div class="row sku" id="r_<?php echo $items['FbaEuOrder']['id']; ?>">
									<div class="col-lg-12">	
										<div class="col-12"><small><?php echo $items['FbaEuOrder']['amazon_order_id']; ?></small></div>	
										<div class="col-60">
 										<?php echo '<div class="col-sm-12">'; 
											if(count($items['FbaEuOrderItem']) > 0){
											  foreach($items['FbaEuOrderItem'] as $item ) {
												
													echo '<div class="col-20"><small>'. $item['seller_sku'].'</small></div>';
													echo '<div class="col-50"><small>'. $item['title'].'</small></div>';
													echo '<div class="col-10"><small>'. $item['quantity_ordered'].'</small></div>';
													echo '<div class="col-10"><small>'. $item['item_price'].'</small></div>';
													echo '<div class="col-10"><small>'. $item['shipping_price'].'</small></div>';
												
 											 }
										 }else{
										 	echo '<div class="col-10"><small>-</small></div>';
											echo '<div class="col-50"><small>Still_items_not_feached</small></div>';
											echo '<div class="col-10"><small>-</small></div>';
											echo '<div class="col-10"><small>-</small></div>';
											echo '<div class="col-10"><small>-</small></div>';
										 }
										 echo '</div>';
										  ?>
										  
										</div>										
										<div class="col-18"><small><center><?php echo $items['FbaEuOrder']['purchase_date']; ?></center></small></div>	
										<div class="col-10">Invoice</div>
										 
									</div>												
								</div>
								 
							<?php }?>
							
							<div class="col-sm-12 text-right">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						<?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>


<script>	
$("body").on("change", ".invoice", function()
{
	$(".outerOpac").show();
	//$('.outerOpac span').css({'z-index':'10000'});
	
	//var id			=	$(this).attr("id");
	var orderid			=	$(this).attr("for");	
	var field_name  	=	$(this).attr("name");	
	var amazon_order_id =	$(this).attr("amz");
	var vals			=	$(this).val();
	
	$.ajax({
			dataType: 'json',
			url :   '<?php echo Router::url('/', true)?>Invoice/Address',
			type: "POST",
			data : {order_id:orderid,field:field_name,val:vals,amazon_order_id:amazon_order_id},
			success: function(txt, textStatus, jqXHR)
			{	 
				$(".outerOpac").hide();
				 
			}		
		});

}); 		
		
function updateStatus(t,v){	
	
	$.ajax({
		dataType: 'json',
		url : '<?php echo Router::url('/', true) ?>FbaEuOrders/updateStatus',
		type: "POST",				
		cache: false,			
		data : {status:$(t).find("option:selected").val(),id:v },
		success: function(data, textStatus, jqXHR)
		{	
			 	
			if(data.error){
				alert(data.msg);
			} 							
		}		
 	});  		 
}
</script>
