<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.separator{border-bottom: 1px solid #ccc; margin-bottom: 10px;}
	.normalbox{background-color:#ffffff;}
	/* .col-8 {width: 8%; float: left; }
	.col-10 {width: 10%; float: left;}
	.col-11 {width: 6%; float: left;}
	.col-12 { width: 12%; float: left; } */
	
	
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Returned Orders(Archive)</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row"> 
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						<form method="get" name="searchfrm" id="searchfrm" action="./showReturnOrders" enctype="multipart/form-data" onSubmit="return search()">	
						<div class="col-sm-5 no-padding">
						    <div class="col-lg-5">
									<select name="searchtype" id="searchtype" class="form-control">
									  <option value="">Select search type</option>
									  <option value="orderid" <?php echo ( isset($_REQUEST['searchtype']) && $_REQUEST['searchtype']=='orderid')?'selected':''; ?> >By Order ID</option>
										<option value="referenceid" <?php echo ( isset($_REQUEST['searchtype']) && $_REQUEST['searchtype']=='referenceid')?'selected':''; ?>>By Reference ID</option>
										<option value="customer" <?php echo ( isset($_REQUEST['searchtype']) && $_REQUEST['searchtype']=='customer')?'selected':''; ?>>By Customer Name</option>
									</select>
									<span class="text-danger hidden" id="searchtype-error">Select a type!</span>
								</div>
							  <div class="col-lg-3 ">
									<input type="text" value="<?php echo isset($_REQUEST['searchkey']) ? $_REQUEST['searchkey'] :''; ?>" placeHolder="Search" id="searchkey"  name="searchkey" class="form-control searchString" />
								  <span class="text-danger hidden" id="searchkey-error">Enter text search!</span>
								</div>
								<div class="col-lg-2 no-padding"> 
									<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
								</div>
								<div class="col-lg-2" style="z-index: 2;"> 
									<button type="button" class="btn btn-warning" id="clearsearch" style="margin-left: 4px;"><i class="glyphicon glyphicon-search"></i>&nbsp;Clear Search</button>
								</div>
						</div>
						</form>
						<div class="col-sm-7 text-right" style="z-index: 1;">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						</div>	
						</div>	
					<div class="panel-body no-padding-top bg-white">
						
						<div class="row head" style="clear:both;">
		 					  <div class="col-sm-12">
								<div class="col-sm-2">Order ID</div>					 
								<div class="col-sm-2">Sku Details</div> 
 								<!-- <div class="col-sm-1">Delevery country</div> -->
								<div class="col-sm-1">Order</div>	
								<div class="col-sm-1">Customer Details</div>
 								<!-- <div class="col-sm-1">Processed By</div>	-->
								<div class="col-sm-3">Status</div>
								<div class="col-sm-2">Grading Status</div> 
								<div class="col-sm-1">Action</div>	
								
							</div>								
						</div>	
						<div class="body" id="results">
						<?php  						
						 ini_set('memory_limit',-1);
						 if(count($Returnorders) > 0) {	 ?>
							<?php foreach( $Returnorders as $items ) {   
								$customerInfo = unserialize($items['OpenOrdersArchive']['customer_info']);
								$itemDetails  = unserialize($items['OpenOrdersArchive']['items']);
								$totalDetails = unserialize($items['OpenOrdersArchive']['totals_info']);  
								$generalInfo  = unserialize($items['OpenOrdersArchive']['general_info']);
								$titles = '';
								
							?>
								<div class="row sku" id="r_<?php echo $items['MergeUpdatesArchive']['id']; ?>">
									<div class="col-lg-12">	
										<div class="col-sm-2"> 
											<small>
												<span><b>O</b>: <?php echo $items['MergeUpdatesArchive']['product_order_id_identify']; ?></span><br/>
												<span><b>R</b>: <?php echo $items['OpenOrdersArchive']['amazon_order_id']; ?></span>
											</small> 
										</div>
										<div class="col-sm-2"> 
											<small>
												<span><b>I</b>: <?php echo $items['MergeUpdatesArchive']['sku']; ?></span><br/>
												<span><b>B</b>: 
												<?php
													$productBarcode = explode(',',$items['MergeUpdatesArchive']['barcode']); 
													foreach($productBarcode as $prokey=>$proval) { 
														echo $proval.'<br/>'; 
													}
													
												?>
												</span><br/>
												<span><b>T</b>: 
												<?php 
												 foreach($itemDetails as $itemDet) { 
													if(isset($itemDet->Title)) {
														$titles .= $itemDet->Title.', ';
													} 
												 }
												 echo trim($titles,", ");
												?></span>
											</small> 
										</div> 
										<!-- <div class="col-sm-1"><small><?php //echo $items['MergeUpdatesArchive']['delevery_country']; ?></small></div> -->
                    
										<div class="col-sm-1">
										  <small>  
												<span><b>OD</b>: <?php echo $items['MergeUpdatesArchive']['order_date']; ?></span><br/>
												<span><b>PD</b>: <?php echo $items['MergeUpdatesArchive']['process_date']; ?></span><br/> 
											</small> 
										</div>	
									
									  <div class="col-sm-1">
										  <small>  
												<span><b>Name</b>: <?php echo $customerInfo->Address->FullName; ?></span><br/>
												<span><b>Address</b>: <?php echo $customerInfo->Address->Address1.'<br/>'.$customerInfo->Address->Address2.'<br/>'.$customerInfo->Address->Address3; ?></span><br/> 
											</small> 
										</div> 
 	
										<!-- <div class="col-sm-1"><small><?php //echo $items['MergeUpdatesArchive']['assign_user']; ?></small></div>	  -->

										<div class="col-sm-3">
										  <small>  
												<span><b>Process</b>: 
													<?php 
													  $resend = '';
													  if($items['MergeUpdatesArchive']['is_resend']==1) {
															$resend = '& Resend';
														}
														if($items['MergeUpdatesArchive']['status']=='4') {
															echo 'Processed (Returned '.$resend.')';
														} else {
															echo 'Processed '.$resend;
														}  
														?> 
												</span><br/>
												<?php if($items['OpenOrdersArchive']['parent_id']!='' && $items['OpenOrdersArchive']['parent_id']!='0') {?><span><b>Parent Order ID</b>: <?php echo $items['OpenOrdersArchive']['parent_id']; ?></span><br/><?php } ?>
												<?php if(!empty($Orderrelation[$items['OpenOrdersArchive']['num_order_id']])) {?><span><b>Child Order ID</b>: <?php echo $Orderrelation[$items['OpenOrdersArchive']['num_order_id']]; ?></span><br/><?php } ?> 
												<?php if($items['MergeUpdatesArchive']['return_reason']!='') {?><span><b>Return Reason</b>: <?php echo $items['MergeUpdatesArchive']['return_reason']; ?></span><br/><?php } ?>
												<?php if($items['MergeUpdatesArchive']['comments']!='') {?><span><b>Return Comments</b>: <?php echo  $items['MergeUpdatesArchive']['comments']; ?></span><br/><?php } ?>
												<?php if($items['MergeUpdatesArchive']['return_by_user']!='') {?><span><b>Return By</b>: <?php echo $items['MergeUpdatesArchive']['return_by_user']; ?></span><br/><?php } ?>
												<?php if($items['MergeUpdatesArchive']['return_date']!='') {?><span><b>Return Date</b>: <?php echo  date('d-m-Y H:i:s',strtotime($items['MergeUpdatesArchive']['return_date'])); ?></span><br/><?php } ?>
												<!-- <?php //if($items['Refund']['refund_by_user']!='') {?><span><b>Refund By</b>: <?php //echo  $items['Refund']['refund_by_user']; ?></span><br/><?php //} ?>
												<?php //if($items['Refund']['created_at']!='') {?><span><b>Refund Date</b>: <?php //echo  date('d-m-Y H:i:s',strtotime($items['Refund']['created_at'])); ?></span><br/><?php //} ?> -->
												<!-- <?php //if(isset($items['Refund']['refund_amount'])) { ?>
												  <span style="color: #07f50b;"><b>Refunded</b>: <?php //echo $items['Refund']['refund_amount'].' '.$totalDetails->Currency; ?>(<?php //echo ucwords($items['Refund']['refund_option']); ?>)</span>  
											  <?php //} ?> -->
												<!-- <?php //if($items['OpenOrdersArchive']['resend_by_user']!='') {?><span><b>Resend By</b>: <?php //echo  $items['OpenOrdersArchive']['resend_by_user']; ?></span><br/><?php //} ?>
												<?php //if($items['OpenOrdersArchive']['resend_date']!='') {?><span><b>Resend Date</b>: <?php //echo date('d-m-Y H:i:s',strtotime($items['OpenOrdersArchive']['resend_date'])); ?></span><br/><?php //} ?> -->
											</small> 
										</div> 
										<div class="col-sm-2"> 
											<small>
												<?php if($items['MergeUpdatesArchive']['item_condition']!=''){?><span><b>Item Condition</b>:<?php echo $items['MergeUpdatesArchive']['item_condition']; ?></span><br/><?php } ?>
												<?php if($items['MergeUpdatesArchive']['warehouse_return_item_condition']!=''){?><span><b>Warehouse Item Condition</b>:<?php echo $items['MergeUpdatesArchive']['warehouse_return_item_condition']; ?></span><br/><?php } ?>
												<?php if($items['MergeUpdatesArchive']['warehouse_return_comments']!=''){?><span><b>Warehouse Comments</b>:<?php echo $items['MergeUpdatesArchive']['warehouse_return_comments']; ?></span><br/><?php } ?>
												<?php if($items['MergeUpdatesArchive']['warehouse_return_by_user']!=''){?><span><b>Warehouse Action By User</b>:<?php echo $items['MergeUpdatesArchive']['warehouse_return_by_user']; ?></span><br/><?php } ?>
											</small> 
										</div>
										<div class="col-sm-1">
											<?php 
												$totalCharge = '0';
												if(isset($totalDetails->TotalCharge)) { 
												  $totalCharge = $totalDetails->TotalCharge.' '.$totalDetails->Currency;
												}
											?>
									   	<a href="javascript:void(0);" onclick="moveToLocation('<?php echo $items['MergeUpdatesArchive']['product_order_id_identify']; ?>','<?php echo $items['MergeUpdatesArchive']['order_id']; ?>','<?php echo $generalInfo->Source; ?>','<?php echo $generalInfo->SubSource; ?>','<?php echo $items['MergeUpdatesArchive']['order_date']; ?>','<?php echo $customerInfo->Address->FullName; ?>','<?php echo $items['MergeUpdatesArchive']['process_date']; ?>','<?php echo $items['MergeUpdatesArchive']['postal_service']; ?>','<?php echo $totalCharge; ?>')" class="btn btn-warning btn-xs" role="button" title="View"><i class="glyphicon glyphicon-send"></i> View</a>
 										</div> 
									</div>												
								</div>
								 
							<?php }?>
						</div>	
						
						<div class="col-sm-12 text-right" style="margin-top:10px;margin-bottom:10px;">
						<ul class="pagination" style="display:inline-block">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
						 </ul>  
						 <?php } else {?>
							<div align="center"><div class="alert alert-danger fade in">No data found!</div></div>
						<?php }  ?>
						</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div> 

<!--move to location modal window -->
<div class="modal fade" id="myModalMoveWindow" role="dialog">
		<div class="modal-dialog modal-lg">
		  <div class="modal-content">
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Returned Order - Order ID: <span id="orderidtext"></span></h4>
			</div>
			<div class="modal-body">

				 <div style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;" class="msgalert alert-danger" id="form-error-popup-location-error"></div>
				 <div style="margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;padding: 15px;" class="msgalert alert-success" id="form-error-popup-location-success"></div>

				 <div class="row"> 
					<form class="form-horizontal" id="frmreturngrade">
						<div class="col-md-12 separator">
						   <label class="col-sm-4" style="padding: 0;"><b>Basic Details</b></label>
						</div>
					  <div class="col-md-6">
						
						<div class="form-group">
							<label class="col-sm-4" for="Source">Source:</label>
							<div class="col-sm-8">
								<input readonly type="email" class="form-control" id="source" placeholder="Source">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4" for="Sub source">Sub source:</label>
							<div class="col-sm-8"> 
								<input readonly type="text" class="form-control" id="sub_source" placeholder="Sub source">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4" for="Order Date">Order Date:</label>
							<div class="col-sm-8"> 
								<input readonly class="form-control textbox-n" type="text" id="order_date" placeholder="order date">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4" for="Customer">Customer:</label>
							<div class="col-sm-8"> 
								<input readonly type="text" class="form-control" id="full_name" placeholder="Customer">
							</div>
						</div>
						</div>
          
			    	<div class="col-md-6">
             
						<div class="form-group">
							<label class="col-sm-4" for="Processed on">Processed on:</label>
							<div class="col-sm-8">
								<input readonly type="email" class="form-control" id="processed_date" placeholder="Processed on">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4" for="Service:">Service:</label>
							<div class="col-sm-8"> 
								<input readonly type="text" class="form-control" id="service" placeholder="Service:">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4" for="Total">Total:</label>
							<div class="col-sm-8"> 
								<input readonly type="text" class="form-control" id="total" placeholder="Total">
							</div>
						</div> 	
          </div> 

					<div class="col-md-12 separator">
						<label class="col-sm-4" style="padding: 0;"><b>Sku Details</b></label>
					</div> 
				 	<div class='col-md-12' id="skulist">
					 
					</div>
         
					<div class="col-md-12 separator">
						<label class="col-sm-4" style="padding: 0;"><b>Grade Order Details</b></label>
						</div>
					  <div class="col-md-6">
						
						<div class="form-group">
							<label class="col-sm-4" for="Source">Item Condition*:</label>
							<div class="col-sm-8">
								<select name="itemcondition_warehouse" id="itemcondition_warehouse" class="form-control" >
									<option value="">Please select an item condition</option>
									<option value="New box intact - A++">New box intact - A++</option>
									<option value="New box open - B++">New box open - B++</option>
									<option value="New packaging damaged - A">New packaging damaged - A</option>
									<option value="Used no packaging - B">Used no packaging - B</option>
									<option value="Danger/ Faulty">Danger/ Faulty</option>
									<option value="Working/ Box open">Working/ Box open</option>
									<option value="Packed New">Packed New</option>   
							  </select>
								<span class="text-danger hidden" id="warehouse-condition-error">Item condition is a required field!</span>
							</div>
						</div>
						
					</div> 
					<div class="col-md-6">
					<div class="form-group">
							<label class="col-sm-4" for="Comments Warehouse">Comments:</label>
							<div class="col-sm-8"> 
							  <textarea name="comments_warehouse" id="comments_warehouse" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="col-md-12" id="movelocationid"></div>
<!-- 
          <div class="col-md-12">
					
					  <button type="button" class="btn btn-success pull-right" onclick="confirmGrading()">Save</button>
					</div> -->
           
					<input type="hidden" name="order_id" id="order_id" value="" />     
					<input type="hidden" name="islocation" id="islocation" value="" />     
					<input type="hidden" name="split_id" id="split_id" value="" />
					<!-- <input type="hidden" name="checkedlocation" id="checkedlocation" value="" />   -->

				  <div class="col-md-12 separator locationlist hidden">
						<label class="col-sm-4" style="padding: 0;"><b>Move Items To Inventory Details</b></label>
					</div> 
				 	<div class='col-md-12 locationlist hidden'>
					 <table class="table">
						<thead>
						<tr>  
							<th>Sku</th>
							<th>Location</th>
							<th>Stock</th>
							<td>&nbsp;</td>
						</tr>
						</thead>
						<tbody id="locationbody"></tbody>
					 </table> 
					</div> 

					<div class="col-sm-12">
						<button type="button" class="btn btn-info pull-right" onclick="confirmGrading()">SAVE</button>
					</div>
				</form>  
				</div>   
			</div> 
		  </div>
		</div>
	</div>
<script>	
    function isNumber(event) {
			if(event.which == 8 || event.which == 0){ 
				return true;
			}

			// prevent if not number/dot
			if(event.which < 46 || event.which > 59) {
				event.preventDefault();
			} 

			// prevent if already dot 
			if(event.which == 46 && event.target.value.indexOf('.') != -1) {
				event.preventDefault();
			} 
    }

    function search() {
			var searchFlag = true;
      if($("#searchtype").val()=='') {
        $("#searchtype-error").removeClass('hidden');
				searchFlag = false;
			}
			if($("#searchkey").val()=='') {
				$("#searchkey-error").removeClass('hidden');
				searchFlag = false;
			}   
			return searchFlag;
		}
 
  //  $("#movetolocation").click(function() {
	 $(document).delegate('#movetolocation','click',function() { 
		$(".locationlist").addClass('hidden'); 
		$("#islocation").val('0');
		if($(this).is(':checked')==true) {
			$("#islocation").val('1');
			$(".locationlist").removeClass('hidden'); 
			swal({
					title: "Confirm Move To Inventory",
					text: "Warning: If you move items to inventory it will available for selling. This action is irreversible.",
					type: "warning",
					confirmButtonText: "OK", 
				});
		}
	 }); 

   $("#clearsearch").click(function() {
		$("#searchtype").val('');
		$("#searchkey").val('');
		window.location.href = './showReturnOrders';
	 });

		$('#searchtype').on('change',function() {
			if($(this).val()!='') {
			 $("#searchtype-error").addClass('hidden');
			}
		});

		$('#searchkey').on('keyup',function() {
			if($(this).val()!='') {
			 $("#searchkey-error").addClass('hidden');
			}
		});

		$(document).delegate('.removebutton','click',function() {  
			$(this).parent('td').parent('tr').remove();
		});  
    
    function addMore() {
			var index = parseInt($('.locationlistcontainer').length) + 1;
			$("#locationbody").append('<tr class="locationlistcontainer"><td><input type="text" name="sku[]" class="requiredfield form-control normalbox" /></td><td><input type="text" name="location[]" class="requiredfield form-control normalbox qtybox" /></td><td><input onkeypress="return isNumber(event)" class="requiredfield pricebox form-control normalbox" type="text" value="" name="stock[]"></td><td><button type="button" class="btn btn-danger pull-right removebutton">-</button></td></tr>');
		}

		function isNumber(event) {
			if(event.which == 8 || event.which == 0){ 
				return true;
			}

			// prevent if not number/dot
			if(event.which < 46 || event.which > 59) {
				event.preventDefault();
			} 

			// prevent if already dot 
			if(event.which == 46 && event.target.value.indexOf('.') != -1) {
				event.preventDefault();
			} 
    }

		$('#itemcondition').on('change',function() {
			if($(this).val()!='') {
			 $("#condition-error").addClass('hidden');
			}
		});
  
		function moveToLocation(splitId='',orderId='',source='',subsource='',orderdate='',fullname='',processeddate='',service='',total='') {
			$(".msgalert").hide(); 
			$("#order_id").val(orderId); 
			$("#split_id").val(splitId);
			$("#orderidtext").html(splitId);
			$("#source").val(source);
			$("#sub_source").val(subsource);
			$("#order_date").val(orderdate);
			$("#full_name").val(fullname); 
			$('#processed_date').val(processeddate);  
			$('#total').val(total);
			$('#service').val(service);
			$('#itemcondition_warehouse').val('');
			$('#comments_warehouse').val('');
			$("#form-error-popup-location-error").html('').hide();
			$("#form-error-popup-location-success").html('').hide();
			$(".locationlist").addClass('hidden');
			 
      $.ajax({
				dataType: 'json',
				url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/getReturnDetails',
				type: "POST",				
				cache: false,			
				data : {orderid:$("#order_id").val(),splitid:$("#split_id").val()},
				success: function(data, textStatus, jqXHR)
				{	   
					$('#skulist').html(data.skuslist);  
					$('#locationbody').html(data.skuLocationlist);
					$('#itemcondition_warehouse').val(data.warehouseitemcondition);
					$('#comments_warehouse').val(data.warehousereturncomments);
					$('#movelocationid').html(data.movecheckbox);
					if(data.ismovedtolocation==1) {
						$('#movelocationid').html('Items already moved to inventory for selling.')
					}
					$("#myModalMoveWindow").modal('show'); 
				}	
			});
		}

		function confirmGrading() {
      //add reason
			var ajaxFlag=true;
			var reqfielderror=false;
      $('.reqerror').remove();
			$(".requiredfield").each(function() { 
				if($(this).val()=='' && $(this).is(':disabled')==false && $(this).is(':visible')) {
					$(this).parent('td').append('<br/><span class="text-danger reqerror" >Please enter a value!</span>');
					reqfielderror = true;
				} 
			});

			if($("#itemcondition_warehouse").val()=='') {
				$("#warehouse-condition-error").removeClass('hidden');
			} else {
				$("#warehouse-condition-error").addClass('hidden');
			}

			if($("#itemcondition_warehouse").val()=='' || reqfielderror==true) {
				ajaxFlag = false;
			}

			if(ajaxFlag==true) {
				$.ajax({
					dataType: 'json',
					url : '<?php echo Router::url('/', true) ?>ReturnProcessedOrders/confirmGrading',
					type: "POST",				
					cache: false,			
					data : $("#frmreturngrade").serialize(),
					success: function(data, textStatus, jqXHR)
					{	 
						if(data.status==1) {
							// $('#myModalReturnOrder').modal('hide');
							location.reload();
							$("#form-error-popup-location-success").html(data.msg).show(); 
							setTimeout(function(){ $(".msgalert").hide(); }, 3000);
						} else {
							$("#form-error-popup-location-error").html(data.msg).show();
						} 							
					}	
				});   		
			}
		}
 
</script>