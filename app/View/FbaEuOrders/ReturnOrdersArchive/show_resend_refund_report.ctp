<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.separator{border-bottom: 1px solid #ccc; margin-bottom: 10px;}
	.normalbox{background-color:#ffffff;}
	/* .col-8 {width: 8%; float: left; }
	.col-10 {width: 10%; float: left;}
	.col-11 {width: 6%; float: left;}
	.col-12 { width: 12%; float: left; } */
 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Download Return/ Resend Orders Report</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">  
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						<form method="post" name="downloadReport" id="downloadReport" action="./showResendRefundReport" enctype="multipart/form-data" onSubmit="return search()">	
						<div class="col-sm-12 no-padding"> 
 								  								
							    <input type='hidden' id='rangestart' name='rangestart' value='' />
								<input type='hidden' id='rangeend' name='rangeend' value='' />
								<input type="hidden" name="option" id="option" value="resend" />
								<div class="col-lg-4"> 
								<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
								</div>
								
								<div class="col-lg-2"> 
									<button onclick="downLoadReport('resend')" type="submit" class="btn btn-success">Generate Resend Report</button>
								</div>
								<div class="col-lg-2"> 
									<button onclick="downLoadReport('refund')" type="submit" class="btn btn-warning">Generate Refund Report</button>
								</div>
						</div>
						</form>
						 	
						</div>	
 
			</div>			
		  </div>
		  
<div class="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999;" src="<?php echo $this->webroot; ?>img/482.gif" />
</div>

 
<script>
 
	function downLoadReport( option )
	{ 
		var option   = $("#option").val(option);		 
		var getFrom  = $('div.daterangepicker_start_input input:text').val();
		var getEnd   = $('div.daterangepicker_end_input input:text').val();
		$("#rangestart").val(getFrom);
		$("#rangeend").val(getEnd);
		$("#downloadReport").submit();
		 	
	}
		
</script>