<?php $userID = $this->session->read('Auth.User.id'); ?>
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">  
	<h4 class="modal-title" id="myLargeModalLabel-1">Generate Selected Date Manifest</h4>     
		<div class="row head" style="clear:both;">
		 		<div class="col-sm-12" >
					<div class="col-lg-12" style="background-color: #cfd8dc; padding: 15px; font-size: 14px;">
						<div class="col-sm-4">&nbsp;</div>
						<div class="col-sm-4" style="font-weight: bold;" ></div>
						<div class="col-sm-4">&nbsp;</div>
					</div>
					<div class="col-sm-12" style="background-color: #e9eaea; padding: 5px; font-size: 12px;">
						
						<div class="col-sm-4">
						<?php  $userIds		=	explode(',', $getModuleRole['Processedorders']);
								if(in_array( $userID, $userIds)) { ?>
						<a href="/img/Sku_report/user_report/user_report.csv" class="download_file btn btn-success color-white btn-dark" style="display:none; margin-right:8px; auto; text-align: center;">Download</a>
						<?php } ?>
						</div>
						
							
						<div class="col-sm-4">
							<div id="bs-daterangepicker" class="date_select btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
						</div>
						
						<div class="col-sm-4 download_link">&nbsp;
						<a href="javascript:void(0);" class="date_string btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Click To Download</a>
						
						</div>
						
					</div>
			</div>
		</div>
    </div>
</div>
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>

$(document).ready(function() {

	$(".date_string").click(function(){
									var getFrom = $('div.daterangepicker_start_input input:text').val();
									var getEnd = $('div.daterangepicker_end_input input:text').val();
									$.ajax({
										  url : '/Skureports/getOperaterDetail',
										  type: "POST",   
										  cache: false,
										  data : {getFrom:getFrom, getEnd:getEnd},
										  beforeSend : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },  
										  success: function(data)
														  {  
																$( ".outerOpac" ).attr( 'style' , 'display:none');
																if(data == 1)
																{
																	$('.download_file').show();
																}
																else
																{
																	$('.download_file').hide();
																	alert(data);
																}
														  }  
											});
								  });
							});


</script>