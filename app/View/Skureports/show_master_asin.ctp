<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">    
		<!--<h4 class="modal-title" id="myLargeModalLabel-1">Check Title And Asin</h4>-->
		<div class="row head" style="clear:both;">
		 	<div class="col-sm-12" >
				<div class="col-lg-12" style="background-color: #cfd8dc; padding: 5px; font-size: 14px;">
					<div class="col-sm-4" style="font-weight: bold; font-size:23px" >Check Title And Asin</div>		
					<div class="col-sm-3" style="font-weight: bold;" >&nbsp;</div>	
					<div class="col-sm-2" style="font-weight: bold;" >&nbsp;</div>	
					<div class="col-sm-3" style="font-weight: bold;" >
						<a href="/Skureports/showMasterAsin" class="btn btn-info color-white btn-dark" style="margin-right:8px; auto; text-align: center; float: right;">Refresh</a>
					</div>		
				</div>
				<div class="col-sm-12" style="background-color: #e9eaea; padding: 5px; font-size: 12px;">
					<input type="text" class="col-sm-12 form-control input-lg" style="border-radius: 10px;" id="sku_name" placeholder = "Search By Master SKU, Asin, Channel Sku" />
				</div>
			</div>
		</div>       
    </div>
    <!-- END PAGE HEADING -->
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
		  <div class="panel">                            
			<div class="row">
				<div class="col-lg-12">
					<div class="" style="margin:0 auto; width:350px">
						 <ul class="pagination">
						  <?php
							   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
							   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
						  ?>
						 </ul>
					</div>
				   <div class="panel-body padding-bottom-40 padding-top-10">
				   		<div class="text-left border-top-1 border-grey-100">   
						<table id="example" class="col-lg-12 table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Channel</th>
									<th>Sku</th>
									<th>Asin</th>
									<th>Channel Sku</th>
									<th>Title</th>
									<th style="width: 113px;">Status</th>
								</tr>
							</thead>
							<tbody>
								
								<?php  
								$i=1;
								if(isset($getAllSkuAsins)) { 
								foreach( $getAllSkuAsins as $getAllSkuAsin ) { 
								?>
								<tr class="sku_asin_csku_<?php echo $getAllSkuAsin['MasterSku']['id']; ?>" >
									<td class="channel_<?php echo $getAllSkuAsin['MasterSku']['id']; ?>" ><?php echo $getAllSkuAsin['MasterSku']['channel']; ?></td>
									<td class="sku_<?php echo $getAllSkuAsin['MasterSku']['id']; ?>" ><?php echo $getAllSkuAsin['MasterSku']['sku']; ?></td>
									<td class="asin_<?php echo $getAllSkuAsin['MasterSku']['id']; ?>" ><?php echo $getAllSkuAsin['MasterSku']['asin']; ?></td>
									<td class="channelsku_<?php echo $getAllSkuAsin['MasterSku']['id']; ?>" ><?php echo $getAllSkuAsin['MasterSku']['channelsku']; ?></td>
									<td class="skutitle_<?php echo $getAllSkuAsin['MasterSku']['id']; ?>" ><?php echo $getAllSkuAsin['MasterSku']['title']; ?></td>
									<td>
									<a class="edit_sku btn btn-success btn-sm" for="<?php echo $getAllSkuAsin['MasterSku']['id']; ?>" data-title="Edit;"><span class="fa fa-pencil"></span></a>
									<a class="delete_sku btn btn-danger btn-sm" for="<?php echo $getAllSkuAsin['MasterSku']['id']; ?> " data-title="Delete"><span class="glyphicon glyphicon-trash"></span></a>
									
									</td>
								</tr>
								<?php $i++; } } ?>
								
							</tbody>
						</table>                             
						</div>
					</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>
<div class="showPopupForEditSkuAsin"></div>
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<script>
$(document).ready(function() {
		
	  $(document).keypress(function(e) {
		if(e.which == 13) {
				$('.outerOpac').show();
				var skustr = $.trim($('#sku_name').val());
				var sku	=	skustr.replace(/#/gi, '_h_');
				window.location = "<?php echo Router::url('/', true) ?>Skureports/showMasterAsin/"+sku;
			}
		});
		
	  $('.delete_sku').click(function(){
		var id 				= 	$( this ).attr('for');
	  	$.ajax({
				  url : '/Skureports/deleteSkuAsin',
				  type: "POST",   
				  cache: false,
				  data    	: { id : id },
				  success: function(data)
								  {  
										 swal("Sku delete successfully.", "" , "success");	
										 $('.sku_asin_csku_'+id).hide('500');
								  }  
			  });
	  });
	  
	  $('.edit_sku').click(function(){
		var id 				= 	$( this ).attr('for');
	  	$.ajax({
				  url : '/Skureports/editSkuAsinPopup',
				  type: "POST",   
				  cache: false,
				  data    	: { id : id },
				  success: function(data)
								  {  
										$('.showPopupForEditSkuAsin').html( data );
								  }  
			  });
	  });
	
    $('#example').DataTable({
	 aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1
	});	
});
</script>
