<?php
App::import('Controller', 'Skureports');
$getQtyAndPrice =	new SkureportsController;
?>
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"></h1>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-12">
                               <div class="panel-body padding-bottom-40 padding-top-10">
                                    <div class="text-left border-top-1 border-grey-100">   
									<table id="example" class="col-lg-12 table table-striped table-bordered" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>SKU</th>
												<th>Title</th>
												<th>Listing User</th>
												<th>Purchase User</th>
											</tr>
										</thead>
										<tbody>
											
											<?php foreach( $getAllProducts as $getAllProduct ) { 
											$productDescID = $getAllProduct['ProductDesc']['product_id'];
											$userID = $getQtyAndPrice->getUserName($getAllProduct['ProductDesc']['user_id']);
											$purchaseUserID = $getQtyAndPrice->getUserName($getAllProduct['ProductDesc']['purchase_user_id']);
											?>
											<tr class="row_<?php echo $productDescID; ?>" >
												<td><?php echo $getAllProduct['Product']['product_sku']; ?></td>
												<td><?php echo $getAllProduct['Product']['product_name']; ?></td>
												<td>
												<div class="btn-group">
													  <button type="button" class="btn btn-success dropdown-toggle no-padding width-25" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<span class="caret"></span>
														<span class="sr-only">Toggle Dropdown</span>
													  </button>
													  <ul class="operater_user dropdown-menu scrollable-menu po-dropdownr-right" role="menu" for="<?php echo $productDescID; ?>" >
														<?php foreach( $getAllUsers as $index => $value ) { ?>
															<li value="<?php echo $index; ?>" ><a href="javascript:void(0)"><?php echo $value; ?></a></li>
														<?php } ?>
														<li role="separator"></li>
													  </ul>
												</div>
												<div class="user_name_<?php echo $productDescID; ?>"><?php echo $userID; ?></div>
												</td>
												<td>
													<div class="btn-group">
													  <button type="button" class="btn btn-success dropdown-toggle no-padding width-25" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<span class="caret"></span>
														<span class="sr-only">Toggle Dropdown</span>
													  </button>
													  <ul class="purchase_users dropdown-menu scrollable-menu po-dropdownr-right" role="menu" for="<?php echo $productDescID; ?>" >
														<?php foreach( $getAllUsers as $index => $value ) { ?>
															<li value="<?php echo $index; ?>" ><a href="javascript:void(0)"><?php echo $value; ?></a></li>
														<?php } ?>
														<li role="separator"></li>
													  </ul>
												</div>
												<div class="purchase_user_name_<?php echo $productDescID; ?>"><?php echo $purchaseUserID; ?></div>
												</td>
											</tr>
											<?php } ?>
											
										</tbody>
									</table>                             
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable({
	
	 aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1
	});
	
} );


$("body").on("click", ".operater_user li", function ()
					{
						
						
						var userID		=	$( this ).val();
						var userName	=	$( this ).text();
						var productID 	=	$( this ).parent().attr('for');
						$.ajax({
							url     : '/Skureports/saveUserWithSku',
							type    : 'POST',
							data    : {
										userID			:	userID, 
										userName		:	userName,
										productID		:	productID
									  },
							success :	function( msgArray  )
								{
									$('.user_name_'+productID).text( userName );
									//$('.row_'+productID).remove();
								}                
						}); 
						
					});
$("body").on("click", ".purchase_users li", function ()
					{
						var userID		=	$( this ).val();
						var userName	=	$( this ).text();
						var productID 	=	$( this ).parent().attr('for');
						$.ajax({
							url     : '/Skureports/savePurchaseUserWithSku',
							type    : 'POST',
							data    : {
										userID			:	userID, 
										userName		:	userName,
										productID		:	productID
									  },
							success :	function( msgArray  )
								{
									$('.purchase_user_name_'+productID).text( userName );
									//$('.row_'+productID).remove();
								}                
						}); 
						
					});		


</script>
