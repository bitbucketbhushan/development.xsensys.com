<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
		<div class="row head" style="clear:both;">
		 		<div class="col-sm-12" >
					<div class="col-lg-12" style="background-color: #cfd8dc; padding: 15px; font-size: 14px;">
						<div class="col-sm-4" style="font-weight: bold;" ></div>		
					</div>
					<div class="col-sm-12" style="background-color: #e9eaea; padding: 5px; font-size: 12px;">
						<div class="col-sm-4">
						<form name="uploadfile" id="uploadfrm" enctype="multipart/form-data">
							<input type="file"  name="upload_file" class="filestyle" data-buttonText="Find file" id="upload_file"/>
						</form>
						</div>
						<div class="col-sm-4">
							<select class="form-control input-sm user_select">
								<option>Select Channel</option>
								<option>All Channel</option>
								<?php foreach( $getChannelName as $index => $value ) { ?>
								<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-sm-4">
							<a href="#" class="user_aku btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Search</a>
							<a href="#" class="btn bg-orange-500 color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Go Back</a>
							<a href="#" class="btn btn-info color-white btn-dark" style="margin:0 auto; text-align: center;">Refresh</a>
						</div>
				</div>
				<div class="col-sm-12 insert_message" style="display:none;">asasas</div>
			</div>
		</div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
	   <div class="row">
		    <div class="col-lg-12">
			  <div class="panel">                            
                    <div class="row">
					    <div class="col-lg-12">
							   <div class="panel-body padding-bottom-40 padding-top-10">
							   <!--<div class="" style="margin:0 auto; width:350px">
									 <ul class="pagination">
									  <?php
										   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
										   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									  ?>
									 </ul>
								</div>-->
							        <div class="text-left border-top-1 border-grey-100">  
									<table id="example" class="col-lg-12 table table-striped table-bordered" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>Sku</th>
												<th>Title</th>
												<th>Qty</th>
												<th>Country</th>
												<th>Channel Name</th>
											</tr>
										</thead>
										<tbody class="table_content">
										<?php $i = 1;  foreach( $getAllRecords as $getAllRecord ) { ?>
											<tr id='<?php echo $i; ?>'>
												<td class='channel_sku' ><?php echo $getAllRecord['AmazonRecord']['sku']; ?></td>
												<td class='channel_sku' ><?php echo $getAllRecord['AmazonRecord']['product_name']; ?></td>
												<td class='channel_name' ><?php echo $getAllRecord['AmazonRecord']['quantity_purchased']; ?></td>
												<td class='count' ><?php echo $getAllRecord['AmazonRecord']['ship_country']; ?></td>
												<td><?php echo $getAllRecord['AmazonRecord']['sales_channel']; ?></td>
											</tr>
										<?php $i++; } ?>
										</tbody>
									</table>                             
                                 </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>

<script>

$(document).ready(function() {

	  $('body').on('change', '#upload_file', function(){
	  var formData = new FormData($('#uploadfrm')[0]);

	  $.ajax({
				  url : '/Skureports/uploadAmazonFile',
				  type: "POST",   
				  cache: false,
				  contentType: false,
				  processData: false,
				  data : formData,
				  success: function( data)
				  {  
				  	alert( data );
					location.reload();
				  }  
			});
	

	});
	
  

	$('#example').DataTable({
	 aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1
	});	
});

</script>
