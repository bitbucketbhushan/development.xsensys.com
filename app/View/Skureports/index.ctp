<?php
App::import('Controller', 'Skureports');
$skurepobj =	new SkureportsController;
?>
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
   <div class="page-head bg-grey-100">        
		<div class="row head" style="clear:both;">
		 	<div class="col-sm-12" >
				<div class="col-lg-12" style="background-color: #cfd8dc; padding: 5px; font-size: 14px;">
					<div class="col-sm-4" style="font-weight: bold; font-size:23px" >Show Sku, Asin, Channel Sku</div>		
					<div class="col-sm-3" style="font-weight: bold;" >&nbsp;</div>	
					<div class="col-sm-2" style="font-weight: bold;" >&nbsp;</div>	
					<div class="col-sm-3" style="font-weight: bold;" >
						<a href="/Skureports" class="btn btn-info color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Refresh</a>
						<a href="/Skureports/generateUserReport" class="btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Generate CSV</a>
					</div>			
				</div>
				<div class="col-sm-12" style="background-color: #e9eaea; padding: 5px; font-size: 12px;">
					<input type="text" class="col-sm-12 form-control input-lg" style="border-radius: 10px;" oninput="searchSkuUser()" id="sku_name" placeholder = "Search By Master SKU" />
				</div>
			</div>
		</div>
    </div>
    <!-- END PAGE HEADING -->
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
		  <div class="panel">                            
			<div class="row">
				<div class="col-lg-12">
				   <div class="panel-body padding-bottom-40 padding-top-10">
				   		<div class="" style="margin:0 auto; width:350px">
							 <ul class="pagination">
							  <?php
								   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							  ?>
							 </ul>
						</div>
						<div class="text-left border-top-1 border-grey-100">   
						<table id="example" class="col-lg-12 table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SKU</th>
									<th>Category</th>
									<th>Brand</th>
									<th>Listing User</th>
									<th>Purchase User</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								
								<?php foreach( $getAllProducts as $getAllProduct ) { ?>
								<tr>
									<td><?php echo $getAllProduct['Product']['product_sku']; ?></td>
									<td><?php echo $getAllProduct['Product']['category_name']; ?></td>
									<td><?php echo $getAllProduct['ProductDesc']['brand']; ?></td>
									<td><?php echo $skurepobj->getUserName($getAllProduct['ProductDesc']['user_id']); ?></td>
									<td><?php echo $skurepobj->getUserName($getAllProduct['ProductDesc']['purchase_user_id']); ?></td>
									<td>
									<a href="/Skureports/getMappedSkuAsin/<?php echo str_replace('#', '_h_',$getAllProduct['Product']['product_sku']); ?>" title="Show Sku Detail"><span class="glyphicon btn btn-success">&#xe127;</span></a>
									<a href="/Skureports/generateSkuCsv/<?php echo str_replace('#', '_h_',$getAllProduct['Product']['product_sku']); ?>" title="Generate CSV" ><span class="glyphicon glyphicon-download-alt btn btn-success"></span>
									<a href="/Skureports/skuSellGraph/<?php echo str_replace('#', '_h_',$getAllProduct['Product']['product_sku']); ?>" title="Generate CSV" ><span class="glyphicon btn btn-success"><i class="fa fa-bar-chart"></i></span>
									</td>
								</tr>
								<?php } ?>
								
							</tbody>
						</table>                             
						</div>
					</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>
<script>

function searchSkuUser()
{
	var skustr = $('#sku_name').val();
	var sku	=	skustr.replace('#', '_h_');
	window.location = "<?php echo Router::url('/', true) ?>Skureports/index/"+sku;
}

$(document).ready(function() {
    $('#example').DataTable({
	
	 aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1
	});
	
} );

$('.fetch_asin').on('click', function(){

	var sku = $( this ).attr( 'for' );
	$.ajax({
					'url'            : '/Skureports/getMappedAsin',
					'type'           : 'POST',
					'beforeSend' 	 : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },
					'data'           : { sku : sku },                                    
					'success'        : function( data )
											{ 
												alert(data);
											}
						});
	
});

$('.get_mapped_sku').on('click', function(){

	var sku = $( this ).attr( 'for' );
	$.ajax({
				'url'            : '/Skureports/getMappedSkuAsin',
				'type'           : 'POST',
				'beforeSend' 	 : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },
				'data'           : { sku : sku },                                    
				'success'        : function( data )
										{ 
											alert(data);
										}
					});

});
</script>
