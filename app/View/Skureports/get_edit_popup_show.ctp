
<div class="modal fade pop-up-4" tabindex="-1" role="dialog" id="pop-up-4search1" aria-labelledby="myLargeModalLabel-4" aria-hidden="true">
	<div class="modal-dialog " style="width:1000px">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Add/Edit Master Sku And Asin </h4>
         </div>
         <div class="modal-body bg-grey-100" >
           	<div class="no-margin" style="clear:both">
					<div class="panel-body no-padding-top" >
					
					<div class="row" style="padding:20px 0; border: 5px solid #c0c0c0; box-shadow: 0 0 30px -18px rgba(66, 66, 66, 1) inset;">
                        <div class="col-lg-12">
							<div class="panel no-margin-bottom">
                                <div class="panel-title no-border">
									<div class="panel-head">Sku Detail</div>
									<div class="panel-tools">
								</div>
								</div>
                                <div class="panel-body no-padding ">
									<table class="table table-striped" cellpadding="5px;">									
										<tbody>
											<tr> 
												<td class="vertical-middle">Sku</td>
												<td class="vertical-middle">
													<div class="col-lg-7 margin-left-5">
														<input type="text" class="form-control" id="channel" >
													</div>
												</td>
											</tr>
											<tr> 
												<td class="vertical-middle">asin</td>
												<td class="vertical-middle">
													<div class="col-lg-7 margin-left-5">
														<input type="text" class="form-control" id="asin" >
													</div>
												</td>
											</tr>
											<tr> 
												<td class="vertical-middle">Channel</td>
												<td class="vertical-middle">
													<div class="col-lg-7 margin-left-5">
														<input type="text" class="form-control" id="channel" >
													</div>
												</td>
											</tr>
											<tr> 
												<td class="vertical-middle">Channel Sku</td>
												<td class="vertical-middle">
													<div class="col-lg-7 margin-left-5">
														<input type="text" value="<?php echo $data['channelSku']; ?>" class="form-control" id="channel_sku" >
													</div>
												</td>
											</tr>
											<tr> 
												<td class="vertical-middle">Title</td>
												<td class="vertical-middle">
													<div class="col-lg-7 margin-left-5">
														<input type="text" value="<?php echo $data['channelTitle']; ?>" class="form-control" id="title" >
													</div>
												</td>
											</tr>
											
										</tbody>
									</table>
							    </div>
					        </div>
                        </div>
                        <div style="text-align:center;">
                        <a style="margin:0 auto; text-align: center;" class="dismiss1 btn bg-orange-500 color-white btn-dark padding-left-20 padding-right-20 margin-top-20" href="javascript:void(0);">Cancel</a>
						<a style="margin:0 auto; text-align: center;" class="editSkudetail btn bg-green-500 color-white btn-dark padding-left-20 padding-right-20 margin-top-20" href="javascript:void(0);">Add</a>
						</div>
					  </div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
   $(document).ready(function()
   {
       $('#pop-up-4search1').modal('show');
	   
	   	$('.subOrderBlock>.panel-body').slimScroll({
        height: '275px',
		width:'100%',
		size:'3',
		alwaysVisible: true
    });
   });
   
   
   $("body").on("click", ".dismiss1", function(){
						$( ".close" ).click();
					});
	
	$("body").on("click", ".editSkudetail", function(){
	
		var channelSku 	= 	$('#channel_sku').val();
		var title 		= 	$('#title').val();
		var channel		=	$('#channel').val();//asin
		var asin		=	$('#asin').val();
		$.ajax({
				url     	: '/Skureports/saveAddMasterSku',
				type    	: 'POST',
				data    	: { channelSku : channelSku, title : title, channel:channel, asin:asin },  			  
				success 	:	function( data  )
				{
					alert( data );
				}                
			  });
	});
	
</script>

