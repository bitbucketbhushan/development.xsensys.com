<?php
App::import('Controller', 'Skureports');
$getQtyAndPrice =	new SkureportsController;

$currentUri = $_SERVER["REQUEST_URI"];
$params = explode('/', $currentUri);
$sku 		= isset($params[3]) ? $params[3] : '' ;
$startDate	= isset($params[4]) ? $params[4] : '' ;
$endDate 	= isset($params[5]) ? $params[5] : '' ;
	


?>
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
		<div class="row head" style="clear:both;">
		 	<div class="col-sm-12" >
				<div class="col-lg-12" style="background-color: #cfd8dc; padding: 15px; font-size: 14px;">
					<div class="col-sm-2" style="font-weight: bold;" >SKU :- <?php echo $getProduct['Product']['product_sku']; ?>
					<input type="hidden" value="<?php echo str_replace('#', '_h_',$getProduct['Product']['product_sku']); ?>" id="current_sku" />
					</div>		
					<div class="col-sm-7" style="font-weight: bold;" >Title :- <?php echo $getProduct['Product']['product_name']; ?></div>	
					<div class="col-sm-3" style="font-weight: bold;" >Stock :- <?php echo $getProduct['Product']['current_stock_level']; ?></div>		
				</div>
				<div class="col-sm-12" style="background-color: #e9eaea; padding: 5px; font-size: 12px;">
					<div class="col-sm-3">Barcode</div>
					<div class="col-sm-3">Sold Qty</div>
					<!--<div class="col-sm-3">Amount</div>-->
					<div class="col-sm-3">Po Detail</div>
					<div class="col-sm-3">
						<a href="/Skureports/currentDataCsv/<?php echo $sku.'/'.$startDate.'/'.$endDate; ?>" class="btn btn-success color-white btn-dark pull-left" style="margin:0 auto; text-align: center;"><i class="fa fa-download"></i>CSV</a>
						<a href="/Skureports" class="btn btn-success color-white btn-dark pull-right" style="margin:0 auto; text-align: center;">Go Back</a>
					</div>
				</div>
				<div class="col-sm-12" style="background-color: #e9eaea; padding: 5px; font-size: 12px;">
					<div class="col-sm-3"><?php echo $getProduct['ProductDesc']['barcode']; ?></div>
					<div class="col-sm-3 total_sell">&nbsp;</div>
					<!--<div class="col-sm-3 amount">&nbsp;</div>-->
					<div class="col-sm-3"><?php echo $getQtyAndPrice->getPoRate( $getProduct['Product']['product_sku'] ); ?></div>
					<div class="col-sm-3">
						<select class="form-control input-sm date_select">
								<option>Select</option>
								<option value="<?php echo date('Y-m-d'); ?>">Today</option>
								<option value="<?php echo date('Y-m-d', strtotime('-1 day' )); ?>">Yesterday</option>
								<option value="<?php echo date('Y-m-d', strtotime('-7 days' )); ?>">Week</option>
								<option value="<?php echo date('Y-m-d', strtotime('-15 days' )); ?>">Two Week</option>
								<option value="<?php echo date('Y-m-d', strtotime('-30 days' )); ?>">Month</option>
						</select>
					</div>
					
				</div>
				<!--<div class="col-sm-12" style="padding: 5px;">
					<div class="col-sm-4"><div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-left border-radius-25 no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div></div>
					<div class="col-sm-4 pull-right">
					<!--<a href="javascript:void(0);" class="search_result btn btn-success color-white btn-dark padding-left-10 padding-right-10  col-lg-2" style="margin-right:10px; text-align: center;">Find</a>-->
					<!--<a href="javascript:void(0);" class="btn btn-success color-white btn-dark padding-left-10 padding-right-10  col-lg-2" style="margin-right:10px; text-align: center;">Generate</a>
					<a href="/Skureports" class="btn btn-success color-white btn-dark padding-left-10 padding-right-10  col-lg-2" style="margin:0 auto; text-align: center;">Go Back</a></div>
					
					<div class="col-sm-4">
						<select class="form-control input-sm date_select">
							<option value="<?php echo date('Y-m-d'); ?>">Today</option>
							<option value="<?php echo date('Y-m-d', strtotime('-1 day' )); ?>">Yesterday</option>
							<option value="<?php echo date('Y-m-d', strtotime('-7 days' )); ?>">Week</option>
							<option value="<?php echo date('Y-m-d', strtotime('-15 days' )); ?>">Two Week</option>
							<option value="<?php echo date('Y-m-d', strtotime('-30 days' )); ?>">Month</option>
						</select>
					</div>
					
				</div>-->
			</div>
		</div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
	   <div class="row">
		    <div class="col-lg-12">
			  <div class="panel">                            
                    <div class="row">
					    <div class="col-lg-12">
							   <div class="panel-body padding-bottom-40 padding-top-10">
							        <div class="text-left border-top-1 border-grey-100">  
									<table id="example" class="col-lg-12 table table-striped table-bordered" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>Channel Sku</th>
												<th>Channel Name</th>
												<th>Quantity</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody class="table_content">
										<?php $i = 1;  
											foreach( $getAllOrders as $getAllOrder ) { 
										?>
											<tr id='<?php echo $i; ?>'>
												<td class='channel_sku' ><?php echo $getAllOrder['ChannelDetail']['channelSKU']; ?></td>
												<td class='channel_name' ><?php echo $getAllOrder['ChannelDetail']['sub_source']; ?></td>
												<td class='count' > <?php echo $getAllOrder[0]['Qty']; ?> </td>
												<td><?php echo $getAllOrder['ChannelDetail']['order_date']; ?></td>
											</tr>
										<?php $i++; } ?>
										</tbody>
									</table>                             
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>
<script>
$(document).ready(function() {
  
  $(".date_select").change(function(){
  	var startDate = '<?php echo date('Y-m-d') ?>';
	var endDate = $( this ).val();
	window.location = "<?php echo Router::url('/', true) ?>Skureports/getMappedSkuAsin/"+$("#current_sku").val()+"/"+endDate+"/"+startDate;
  	
  });
  
  $(".search_result").click(function() {
  
  var st = $("input[name='daterangepicker_start']").val(); 
  var end = $("input[name='daterangepicker_end']").val(); 

  var std = st.split("/");
  var enddate = end.split("/");
  window.location = "<?php echo Router::url('/', true) ?>Skureports/getMappedSkuAsin/"+$("#current_sku").val()+"/?startd="+std[0]+"-"+std[1]+"-"+std[2]+"&endd="+enddate[0]+"-"+enddate[1]+"-"+enddate[2];
  });
  
    
	$('#example').DataTable({
	
	 aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1
	});
	
	var total 		= 		0;
	var amount  	= 		0;
	$('.table_content  > tr').each(function() {
	
		var id 			= 		$( this ).attr('id');
		var sku 		= 		$("#"+id+" .sku").text();
		var asin 		= 		$("#"+id+" .asin").text();
		var channelSku 	= 		$("#"+id+" .channel_sku").text();
		var channelName = 		$("#"+id+" .channel_name").text();
		var count 		= 		$("#"+id+" .count").text();
		var totalCharge = 		$("#"+id+" .total_charge").text();
		
		total	=	parseInt(total) + parseInt(count);
		amount	=	parseFloat(amount) + parseFloat(totalCharge);
		amount = amount.toFixed(2);
		
		$('.total_sell').text( total );
		$('.amount').text( amount );
		
		
	});
	
	
} );



</script>
