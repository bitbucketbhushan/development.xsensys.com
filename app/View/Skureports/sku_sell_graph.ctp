<?php
$currentUri = $_SERVER["REQUEST_URI"];
$params = explode('/', $currentUri);
$sku 		= isset($params[3]) ? $params[3] : '' ;
?>

<?php print $this->html->script(array('screenfull')); ?>
		<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});	
		});
		</script>

<?php print $this->html->css(array('morris')); ?>
<div class="rightside bg-grey-100">
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
		  <div class="panel">                            
			<div class="row">
				<div class="col-lg-12">
				   <div class="panel-body padding-bottom-40 padding-top-10">
				   		<div class="text-left border-top-1 border-grey-100">   
						    <div class="col-md-6 agile-grid-right">
								<div class="w3l-chart1">
									<h3><?php 
									$sku 		= isset($params[3]) ? $params[3] : '' ; 
									echo str_replace('_h_','#',$sku);
									?>
									</h3>
									<div id="graph2"></div>
								</div>
							</div>                   
						</div>
					</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>
	<script>
			$(function($){
			
			var sku  = '<?php echo $sku 		= isset($params[3]) ? $params[3] : '' ; ?>';
			
				$.ajax({
				  url : '/Skureports/getGraphData',
				  type: "POST",   
				  cache: false,
				  data    	: { sku : sku },
				  dataType: 'json',
				  success: function(messageData)
								  {  
										  
								         var setquantity	=	messageData.quantity;
										 var orderdate		=	messageData.orderdate;
										
				$('#graph2').graphify({
					start: 'linear',
					obj: {
						id: 'lol',
						legend: false,
						showPoints: true,
						width: 1100,
						legendX: 450,
						pieSize: 200,
						shadow: true,
						height: 400,
						animations: true,
						//x: [20,22,23,24,25,26,27,28,29,30,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,05],
						//points: [339,4,8,530,703,695,510,376,481,502,112,637,405,21,28,33,31,52,38,51,51,31,58,67,119,192,316,255,38,2],
						x: orderdate,
						points: [setquantity],
						xDist: 30,
						scale: 100,
						yDist: 35,
						grid: false,
						xName: 'Year',
						dataNames: ['Amount'],
						design: {
							lineColor: 'red',
							tooltipFontSize: '20px',
							pointColor: 'red',
							barColor: 'blue',
							areaColor: 'orange'
						}
					}
				});
				}  
								   
			  	});
			})(jQuery);
		</script>
<?php print $this->html->script(array('graph')); ?>

