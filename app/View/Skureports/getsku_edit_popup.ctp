
<div class="modal fade pop-up-4" tabindex="-1" role="dialog" id="pop-up-4asinsku" aria-labelledby="myLargeModalLabel-4" aria-hidden="true">
	<div class="modal-dialog " style="width:1000px">
		<div class="modal-content">
         <div class="modal-header" style="text-align: center;" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel-1">Edit Master Sku And Asin </h4>
         </div>
         <div class="modal-body bg-grey-100" >
           	<div class="no-margin" style="clear:both">
					<div class="panel-body no-padding-top" >
					
					<div class="row" style="padding:20px 0; border: 5px solid #c0c0c0; box-shadow: 0 0 30px -18px rgba(66, 66, 66, 1) inset;">
                        <div class="col-lg-12">
							<div class="panel no-margin-bottom">
                                <div class="panel-title no-border">
									<div class="panel-head">Sku Detail</div>
									<div class="panel-tools">
								</div>
								</div>
                                <div class="panel-body no-padding ">
									<table class="table table-striped" cellpadding="5px;">									
										<tbody>
											<tr> 
												<td class="vertical-middle">Channel</td>
												<td class="vertical-middle">
													<div class="col-lg-7 margin-left-5">
														<input type="text" class="form-control channel_<?php echo $getMasterSku['MasterSku']['id']; ?>" id="channel" value="<?php echo $getMasterSku['MasterSku']['channel']; ?>" >
														<input type="hidden" class="form-control" id="mid" value="<?php echo $getMasterSku['MasterSku']['id']; ?>" >
													</div>
												</td>
											</tr>
											<tr> 
												<td class="vertical-middle">Sku</td>
												<td class="vertical-middle">
													<div class="col-lg-7 margin-left-5">
														<input type="text" class="form-control sku_<?php echo $getMasterSku['MasterSku']['id']; ?>" id="sku" value="<?php echo $getMasterSku['MasterSku']['sku']; ?>" >
													</div>
												</td>
											</tr>
											<tr> 
												<td class="vertical-middle">Channel Sku</td>
												<td class="vertical-middle">
													<div class="col-lg-7 margin-left-5">
														<input type="text" class="form-control channel_sku_<?php echo $getMasterSku['MasterSku']['id']; ?>" id="channel_sku" value="<?php echo $getMasterSku['MasterSku']['channelsku']; ?>" >
													</div>
												</td>
											</tr>
											<tr> 
												<td class="vertical-middle">Asin</td>
												<td class="vertical-middle">
													<div class="col-lg-7 margin-left-5">
														<input type="text" class="form-control asin_<?php echo $getMasterSku['MasterSku']['id']; ?>" id="asin" value="<?php echo $getMasterSku['MasterSku']['asin']; ?>" >
													</div>
												</td>
											</tr>
											<tr> 
												<td class="vertical-middle">Title</td>
												<td class="vertical-middle">
													<div class="col-lg-7 margin-left-5">
														<input type="text" class="form-control title_<?php echo $getMasterSku['MasterSku']['id']; ?>" id="title" value="<?php echo $getMasterSku['MasterSku']['title']; ?>">
													</div>
												</td>
											</tr>
										</tbody>
									</table>
							    </div>
					        </div>
                        </div>
                        <div style="text-align:center;">
						<a style="margin:0 auto; text-align: center;" class="editSkudetail btn bg-green-500 color-white btn-dark padding-left-20 padding-right-20 margin-top-20" href="javascript:void(0);">Add</a>
						</div>
					  </div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
   $(document).ready(function()
   {
       $('#pop-up-4asinsku').modal('show');
	   
	   	$('.subOrderBlock>.panel-body').slimScroll({
        height: '275px',
		width:'100%',
		size:'3',
		alwaysVisible: true
    });
   });
   
   
   $("body").on("click", ".dismiss", function(){
					
						$( ".close" ).click();
					});
	$('.editSkudetail').click(function(){
	
		var sku			=	$('#sku').val();
		var channel		=	$('#channel').val();
		var channelSku	=	$('#channel_sku').val();
		var asin		=	$('#asin').val();
		var title		=	$('#title').val();
		var mid			=	$('#mid').val();
		if( sku == '' || channel == '' || channelSku == '' || asin == '' || title == '')
		{
			alert('Please fill all fields.');
			return false;
		}
		$.ajax({
					url     	: '/Skureports/editMasterSkuAsin',
					type    	: 'POST',
					data    	: { id : mid, sku : sku, channel : channel, channelSku : channelSku, asin : asin, title : title },  			  
					success 	:	function( data  )
					{	
						$( ".close" ).click();
						$('.sku_'+data).text( sku );
						$('.channel_'+data).text( channel );
						$('.channelsku_'+data).text( channelSku );
						$('.asin_'+data).text( asin );
						$('.skutitle_'+data).text( title );
						$(".sku_asin_csku_"+data).animate({backgroundColor: "#006400" }, 1500).animate({backgroundColor: "#ffffff" }, 500)
					}                
			  });
	
	});
	
	
</script>

