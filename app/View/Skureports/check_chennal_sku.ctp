<?php
$currentUri = $_SERVER["REQUEST_URI"];
$params = explode('/', $currentUri);
$channel 		= isset($params[3]) ? $params[3] : '' ;
?>
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">    
		<h4 class="modal-title" id="myLargeModalLabel-1">Check Title And Asin</h4>       
        <div class="row head" style="clear:both;">
		 		<div class="col-sm-12" >
					<div class="col-lg-12" style="background-color: #cfd8dc; padding: 15px; font-size: 14px;">
						<div class="col-sm-4" style="font-weight: bold;" ></div>		
					</div>
					<div class="col-sm-12" style="background-color: #e9eaea; padding: 5px; font-size: 12px;">
						<div class="col-sm-4">
						<form name="uploadfile" id="uploadmasterSkufrm" enctype="multipart/form-data">
							<input type="file"  name="upload_sku_file" class="filestyle" data-buttonText="Find file" id="upload_sku_file"/>
						</form>
						</div>
						<div class="col-sm-3">
							<select class="form-control input-sm channel_select">
								<option>Select Channel</option>
								<option value="All Channel">All Channel</option>
								<?php foreach( $getChannelName as $index => $value ) { ?>
								<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-sm-5">
							<a href="/Skureports/checkChennalSku" class="btn btn-info color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Refresh</a>
							<a href="/img/Sku_report/master_sku/master_sku_sample.xls" class="btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Sample xls</a>
							<a href="/Skureports/openOrderStatusfile/<?php echo $channel; ?>" class="btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Generate xlsx</a>
							<a href="javascript:void(0);" class="add_master_sku btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Add</a>
						</div>
				</div>
			</div>
		</div>
    </div>
    <!-- END PAGE HEADING -->
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
		  <div class="panel">                            
			<div class="row">
				<div class="col-lg-12">
				   <div class="panel-body padding-bottom-40 padding-top-10">
				   		<div class="text-left border-top-1 border-grey-100">   
						<table id="example" class="col-lg-12 table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Channel</th>
									<th>Sku</th>
									<th>Asin</th>
									<th>Channel Sku</th>
									<th>Title</th>
									<th style="width: 113px;">Status</th>
								</tr>
							</thead>
							<tbody>
								
								<?php  
								$i=0;
								if(isset($getOpenOrders)) { 
								
								foreach( $getOpenOrders as $getOpenOrder ) {  
								$class = '';
								similar_text($getOpenOrder['open_order_product_title'], $getOpenOrder['martersku_title'], $percent);
								if($percent == 100){
									$class = 'background-color: #bff6bf';
									$status = 'Matched';
								} else { 
									$class = 'background-color: #fcb2b2';
									$status = 'Not Matched';
								}
								$i++;
								?>
								<tr>
									<td class="sku" style="<?php echo $class; ?>">
										<div class="open_order_channel" style="border-bottom: 2px dotted #c3c3c3;" ><b><?php echo $getOpenOrder['sales_channel']; ?></b></div>
										<div class="master_channel"><?php echo $getOpenOrder['channel']; ?></div>
									</td>
									<td class="sku" style="<?php echo $class; ?>">
										<div class="master_sku"><?php echo $getOpenOrder['martersku_sku']; ?></div>
									</td>
									<td class="asin" style="<?php echo $class; ?>">
										<div class="master_asin"><?php echo $getOpenOrder['asin']; ?></div>
									</td>
									<td class="channelsku" style="<?php echo $class; ?>" >
										<div class="open_order_channelsku" style="border-bottom: 2px dotted #c3c3c3;"><b id="chsku_<?php echo $i?>"><?php echo $getOpenOrder['open_order_channelsku']; ?></b></div>
										<div class="mastersku_channelsku"><?php echo $getOpenOrder['martersku_channelsku']; ?></div>
									</td>
									<td class="skutitle" style="<?php echo $class; ?>" >
										<div class="open_order_product_title" style="border-bottom: 2px dotted #c3c3c3;"><b id="chtitle_<?php echo $i?>"><?php echo $getOpenOrder['open_order_product_title'] ?></b></div> 
										<div class="martersku_title"><?php echo $getOpenOrder['martersku_title']; ?></div> 
									</td>
									<td style="<?php echo $class; ?>" ><?php echo '<b>'.$status.' [ '.number_format( $percent, 2 ).' ]<b>'; ?>
									<!--<a href="javascript:void(0);" title="Edit" for="<?php echo $i; ?>"  class="addChannelinMaster btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>-->
									</td>
								</tr>
								<?php } } ?>
								
							</tbody>
						</table>                             
						</div>
					</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>
<div class="showPopupForAddSku"></div>
<div class="showPopupForEditSku"></div>
<script>
$(document).ready(function() {
		
	  $('.addChannelinMaster').click(function(){
		var id 				= 	$( this ).attr('for');
		var channelSku		=	$('#chsku_'+id).text();
	  	var channelTitle	=   $('#chtitle_'+id).text();
	  	$.ajax({
				  url : '/Skureports/getEditPopup',
				  type: "POST",   
				  cache: false,
				  data    	: { channelSku : channelSku, channelTitle : channelTitle },
				  success: function(data)
								  {  
								         $('.showPopupForEditSku').html( data );
								  }  
			  });
	  });
		
	  $('.add_master_sku').click(function(){
	  	$.ajax({
				url     	: '/Skureports/showPopupForAddSku',
				type    	: 'POST',
				data    	: { },  			  
				success 	:	function( data  )
				{
					$('.showPopupForAddSku').html( data );
				}                
			  });
	  });
			
	  $(".channel_select").change(function(){
				var channel = $( this ).val();
				window.location = "<?php echo Router::url('/', true) ?>Skureports/checkChennalSku/"+channel;
  		});
		
	  $('#upload_sku_file').change(function(){
		var formData = new FormData($('#uploadmasterSkufrm')[0]);
		
		$.ajax({
				  url 			: '/Skureports/uploadMasterSkuAndChannelSku',
				  type			: "POST",   
				  cache			: false,
				  contentType	: false,
				  processData	: false,
				  data 			: formData,
				  success		: function(data)
								  {  
								         alert( data );
										 location.reload();
								  }  
				 	});
				
  		});

    $('#example').DataTable({
	 aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1
	});	
});
</script>
