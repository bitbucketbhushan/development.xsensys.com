<?php
App::import('Controller', 'Skureports');
$skurepobj =	new SkureportsController;

$currentUri = $_SERVER["REQUEST_URI"];
$params = explode('/', $currentUri);

$user 		= isset($params[3]) ? $params[3] : '' ;
$startDate	= isset($params[4]) ? $params[4] : '' ;
$endDate 	= isset($params[5]) ? $params[5] : '' ;
?>
<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
		<div class="row head" style="clear:both;">
		 	<div class="col-sm-12" >
				<div class="col-lg-12" style="background-color: #cfd8dc; padding: 15px; font-size: 14px;">
					<div class="col-sm-4" style="font-weight: bold;" >Selected Date Sold :-  <b class="total_sell"></b></div>		
					<!--<div class="col-sm-4" style="font-weight: bold;" >Total Sold :- <?php echo $getAllSkuCount[0][0]['Qty']; ?></div>-->	
				</div>
				<div class="col-sm-12" style="background-color: #e9eaea; padding: 5px; font-size: 12px;">
					<div class="col-sm-4">
					<select class="form-control input-sm user_select">
						<option>Select User</option>
						<option>All Users</option>
						<?php foreach( $getAllUsers as $index => $value ) { ?>
						<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
						<?php } ?>
					</select>
					</div>
					<div class="col-sm-4">
					<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
					<!--<select class="form-control input-sm date_select">
						<option>Select</option>
						<option value="<?php echo date('Y-m-d'); ?>">Today</option>
						<option value="Yesterday">Yesterday</option>
						<option value="<?php echo date('Y-m-d', strtotime('-7 days' )); ?>">Week</option>
						<option value="<?php echo date('Y-m-d', strtotime('-15 days' )); ?>">Two Week</option>
						<option value="<?php echo date('Y-m-d', strtotime('-30 days' )); ?>">Month</option>
					</select>-->
					</div>
					<div class="col-sm-4">
						<a href="javascript:void(0);" class="user_aku btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Search</a>
						<a href="/Skureports/currentDataCsvAllUser/<?php echo $user.'/'.$startDate.'/'.$endDate; ?>" class="btn btn-success color-white btn-dark" style="margin-right:8px; auto; text-align: center;"><i class="fa fa-download"></i>CSV</a>
						<a href="/Skureports" class="btn bg-orange-500 color-white btn-dark" style="margin-right:8px; auto; text-align: center;">Go Back</a>
						<a href="/Skureports/getAllChannelSkuOrders" class="btn btn-info color-white btn-dark" style="margin:0 auto; text-align: center;">Refresh</a>
					</div>
				</div>
			</div>
		</div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
	   <div class="row">
		    <div class="col-lg-12">
			  <div class="panel">                            
                    <div class="row">
					    <div class="col-lg-12">
							   <div class="panel-body padding-bottom-40 padding-top-10">
							   <div class="" style="margin:0 auto; width:350px">
									 <ul class="pagination">
									  <?php
										   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
										   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									  ?>
									 </ul>
								</div>
							        <div class="text-left border-top-1 border-grey-100">  
									<table id="example" class="col-lg-12 table table-striped table-bordered" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>Sku</th>
												<th>Channel Sku</th>
												<th>Channel Name</th>
												<th>Quantity</th>
												<th>Listing User</th>
												<th>Purchase User</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody class="table_content">
										<?php $i = 1;  foreach( $getAllOrders as $getAllOrder ) { ?>
											<tr id='<?php echo $i; ?>'>
												<td class='channel_sku' ><?php echo $getAllOrder['ChannelDetail']['sku']; ?></td>
												<td class='channel_sku' ><?php echo $getAllOrder['ChannelDetail']['channelSKU']; ?></td>
												<td class='channel_name' ><?php echo $getAllOrder['ChannelDetail']['sub_source']; ?></td>
												<td class='count' > <?php echo $getAllOrder[0]['quantity']; ?> </td>
												<td><?php echo $skurepobj->getUserName( $getAllOrder['ChannelDetail']['listing_user'] ) ?></td>
												<td><?php echo $skurepobj->getUserName( $getAllOrder['ChannelDetail']['purchase_user']); ?></td>
												<td><?php echo $getAllOrder['ChannelDetail']['order_date']; ?></td>
											</tr>
										<?php $i++; } ?>
										</tbody>
									</table>                             
                                 </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>
<script>
$(document).ready(function() {
  
  $('body').on('click', '.user_aku', function(){
  
  		var getFrom 	= $('div.daterangepicker_start_input input:text').val();
		var getEnd 		= $('div.daterangepicker_end_input input:text').val();
		var dateArEnd 	= getEnd.split('/');
		var newDateEnd 	= dateArEnd[2] + '-' + dateArEnd[0].slice(-2) + '-' + dateArEnd[1];
		var dateArFrom 	= getFrom.split('/');
		var newDateFrom = dateArFrom[2] + '-' + dateArFrom[0].slice(-2) + '-' + dateArFrom[1];
		
	  	var selectedUser = $('.user_select').val();
	
		if( selectedUser == 'Select User' ){
			alert( 'Please Select User.' );
			return false;
		}
		
		var selectedDate = newDateFrom;
		var startDate = newDateEnd;
		window.location = "<?php echo Router::url('/', true) ?>Skureports/getAllChannelSkuOrders/"+selectedUser+"/"+selectedDate+"/"+startDate;
  });

	$('#example').DataTable({
	 aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1
	});
	
	var total 		= 		0;
	var amount  	= 		0;
	$('.table_content  > tr').each(function() {
	
		var id 			= 		$( this ).attr('id');
		var sku 		= 		$("#"+id+" .sku").text();
		var asin 		= 		$("#"+id+" .asin").text();
		var channelSku 	= 		$("#"+id+" .channel_sku").text();
		var channelName = 		$("#"+id+" .channel_name").text();
		var count 		= 		$("#"+id+" .count").text();
		var totalCharge = 		$("#"+id+" .total_charge").text();
		
		total	=	parseInt(total) + parseInt(count);
		amount	=	parseFloat(amount) + parseFloat(totalCharge);
		amount = amount.toFixed(2);
		
		$('.total_sell').text( total );
		$('.amount').text( amount );
	});	
});

</script>
