<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Downloads</h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                                <div class="panel-body padding-bottom-40">
                                     <ul class="">
											<li>
												<a href="/Orders/download/getProcessedOrder"><i class=""></i> <span>Download Processed Orders</span></a>
											</li>
											<li>
												<a href="/Orders/download/getOpenOrder"><i class=""></i> <span>Download Open Orders</span></a>
											</li>
											<li>
												<a href="/Orders/download/getUnprepareOrder"><i class=""></i> <span>Download Unprepare Orders</span></a>
											</li>
											<li>
												<a href="/Orders/download/getCancelOrder"><i class=""></i> <span>Download Cancel Orders</span></a>
											</li>
										</ul>
                                    </div>
                           </div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>
