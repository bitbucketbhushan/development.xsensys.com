<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Downloads</h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
				<div class="panel-head"><?php print $this->Session->flash(); ?></div>
			</div>
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                                <div class="panel-body padding-bottom-40">
									<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
										
										<!--<ul class="">
											<li>
												<a href="/Orders/download/getProcessedOrder"><i class=""></i> <span>Download Processed Orders</span></a>
											</li>
											<li>
												<a href="/Orders/download/getOpenOrder"><i class=""></i> <span>Download Open Orders</span></a>
											</li>
											<li>
												<a href="/Orders/download/getUnprepareOrder"><i class=""></i> <span>Download Unprepare Orders</span></a>
											</li>
											<li>
												<a href="/Orders/download/getCancelOrder"><i class=""></i> <span>Download Cancel Orders</span></a>
											</li>
										</ul>-->
										
									<div class="form-group">                                        
                                        <div class="col-lg-7">                                                                                         
											<?php												
												App::import( 'Controller' , 'Uploads' );
												$uploadController = new UploadsController();
												$getAllStores = (array)$uploadController->getStores();
												
												print $this->form->input( 'chooseStore', array( 'type'=>'select', 'empty'=>'Choose store','options'=>$getAllStores,'class'=>'chooseStore form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );	
											?>
                                        </div>                                      
                                      </div>
										
										<br>
										<br>
										
										<div class="form-group">                                        
                                        <div class="col-lg-7">                                                                                         
											<?php
												$getLocationArray = array();
												$getLocationArray["/Orders/download/getProcessedOrder"] = 'Download Processed Orders';
												$getLocationArray["/Orders/download/getOpenOrder"] = 'Download Open Orders';
												$getLocationArray["/Orders/download/getUnprepareOrder"] = 'Download Unprepare Orders';
												$getLocationArray["/Orders/download/getCancelOrder"] = 'Download Cancel Orders';
												$getLocationArray["/Orders/download/getSellReport"] = 'Download Sell Report';
												print $this->form->input( 'downloadFile', array( 'type'=>'select', 'empty'=>'Choose download file','options'=>$getLocationArray,'class'=>'downloadFile form-control selectpicker','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );	
											?>
                                        </div>                                      
                                      </div>
                                      <!--<button class="downloadFile btn bg-orange-500 color-white btn-dark margin-right-10 padding-left-40 padding-right-40" type="submit">Proceed</button>-->
                                      
                                    </div>
                            </div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>


<script>
	
	$(document).ready(function()
	{	
		$("body").on("change", ".downloadFile", function ()
		{
			
			var getUrlForSet = $(this).val();
			var getStoreSelection = $( 'select.chooseStore' ).val();
			var getFrom = $('div.daterangepicker_start_input input:text').val();
			var getEnd = $('div.daterangepicker_end_input input:text').val();
			
			//Ajax Perform with cache false
			$.ajax(
			{
				url     : getUrlForSet,
				type    : 'POST',
				data    : {getFrom:getFrom, getEnd:getEnd, getStoreSelection:getStoreSelection},
				'beforeSend' : function(){ $( ".outerOpac" ).attr( 'style' , 'display:block'); },  
				success :	function( msgArray  )
					{
						
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						window.open( msgArray );
						
					}                
			}); 
			
		});	
		
	});

</script>
