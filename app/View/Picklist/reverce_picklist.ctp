<?php
$userID = $this->session->read('Auth.User.id');
?>
<div class="bottom_panel rightside bg-grey-100">
    <div class="container-fluid" >
       		
             <div class="panel">
			 <div class="panel-body">
				<div class="row">	
					<div class="col-lg-12">
						<div class="col-md-2 no-padding">
								<a href="<?php echo Router::url('/', true).'Picklist/generate_reverce_picklist'?>" class="btn btn-success btnsearch">Reverce Picklist</a>
						</div>
						<div class="col-md-2 no-padding">
								<div class="btn-group">
								  <button type="button"  href="javascript:void(0);" class="downloadPickList btn btn-success no-margin">Held Orders</button>
								  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="caret"></span>
									<span class="sr-only">Toggle Dropdown</span>
								  </button>
								  <?php if(count($files) > 0 ) { ?>
								  <ul class="dropdown-menu scrollable-menu" role="menu">
								  <?php 
									$pdfPath = '/img/HeldOrders/'; 
									foreach( $files as $file ) { ?>
									<li><a href="<?php echo $pdfPath.$file; ?>" target ="_blank" ><b>[ <?php echo $file; ?> ]</b></a></li>
									<?php } ?>
									<li role="separator"></li>
								  </ul>
								  <?php } ?>
								</div>
						</div>
						<div class="col-md-2 no-padding"></div>
						<div class="col-md-2 no-padding"></div>
						<div class="col-md-2 no-padding"></div>
				 </div>
			 
			</div>
			</div>
			
			<div class="panel">
			 <div class="panel-body">
				<div class="row">	
					<div class="col-lg-12">
						<table class="table table-bordered table-striped dataTable">
							<tr>
								<th width="50%">Reverce Picklist Name</th>
								<th width="40%">User</th>	
								<th width="10%">Action</th>	
							</tr>
							<?php if(!empty($rpicklists)) { foreach( $rpicklists as $rpicklist ) { ?>
							<tr id="row_<?php echo $rpicklist['RevercePicklist']['id']; ?>">
								<td><?php echo $rpicklist['RevercePicklist']['file_name']; ?></td>
								<td><?php echo $rpicklist['RevercePicklist']['user_name']; ?></td>	
								<td>
								<?php if($rpicklist['RevercePicklist']['status'] == '0') { ?>
									<button type="button"  for ="<?php echo $rpicklist['RevercePicklist']['id']; ?>" class="done_reverce_picklist btn-sm btn btn-success btnsearch">Done</button>
								<?php } ?>	
								</td>	
							</tr> 
							<?php } } ?>
						</table>
				 </div>
			 </div>
			</div>
			</div>
			
			<div class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12 ">
					
							<table class="table table-bordered table-striped dataTable">
							<tr>
								<th width="15%">SKU</th>
								<th width="3%">Qty</th>	
								<th width="3%">Order Count</th>	
								<th >Order Details</th>
								<th width="10%">Action</th>
							</tr>
							<?php
							//pr($selected_skus);
							if(!empty($data)) {
							foreach($data as   $val){
								$t = '';   $o = '';  $q = ''; $sku = ''; 
								$p = array();
								
								foreach($val as $i => $d){
									 if(is_numeric($i)){
										 $t[] = $d['order_id']; 									
										 $q[] = $d['quantity']; 
										 $o[] = $d['order_id'];	
										 $p[$d['bin_location']]['oid'][] = $d['order_id'];
										 $p[$d['bin_location']]['qty'][] = $d['quantity'];
									 }										  
								}									
								$sku = $val['sku'];
								$barcode = $val[0]['barcode']
								
								?>
								<tr id="row_<?php echo $barcode;?>" class="rowc_<?php echo $barcode;?>" >
									<td id="sku_<?php echo $sku;?>" class="skuc_<?php echo $barcode;?>" ><?php echo $sku;?></td>
									<td id="sum_<?php echo $sku;?>" class="sumc_<?php echo $barcode;?>" ><?php echo array_sum($q) ; ?> </td>
									<td id="count_<?php echo $sku;?>" class="countc_<?php echo $barcode;?>" ><?php echo $val['order_count'] ; ?> </td>
									<td id="det_<?php echo $sku;?>" class="detc_<?php echo $barcode;?>" ><?php // echo implode(", ",$t); 
									foreach($p as $loc => $vArr){
										echo "<div><label style='width: 200px;font-size: 12px; margin-bottom: 0px;'>".$loc." [ ". array_sum($p[$loc]['qty']) ." ] </label><label style='width: 200px;font-size: 12px; margin-bottom: 0px;' class='orderids'>". implode(" : ",$p[$loc]['oid'])."</label></div>";
									}
									
									?> 
									<input type="hidden" value="<?php  echo implode(", ",$t);  ?>" class="orderallids" for="<?php echo $barcode;?>" />
									</td>
									<td><a href="javascript:void(0);" for="<?php echo implode(",",$o)?>" id="<?php echo $sku;?>" data = "<?php echo $barcode; ?>" class="putback_onself">Put On Self<a></td>
								</tr>
								<?php  
									} }
								?> 								
						</table>
				</div>
			</div>
			</div>        
		</div>
		
	</div>        
</div>   
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>
<script>
	$(document).ready(function() {
		$('.orderallids').each(function() {
		var ids	=	$( this ).val();
		var sku =	$( this ).attr('for');
			$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>Picklist/getrevercepickliststatus',
					dataType	: 'json',
					type    	: 'POST',
					data    	: {  ids : ids },
					success 	:	function( data  )
					{			
						if(data == 2)
						{
							$('#row_'+sku).css("background-color", "#ffe0b2 ");
						}
					}                
				});	
		});
	});
	
	
	$('body').on('click','.putback_onself',function(){
	
		var ids		=	$( this ).attr( 'for' );
		var sku 	= 	$( this ).attr( 'id' );
		var barcode = 	$( this ).attr( 'data' );
		
		var qty 	=	$('.sumc_'+barcode).text();
		var count 	=	$('.countc_'+barcode).text();
		var det		=	$('.detc_'+barcode).text();
		$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>Picklist/putbacklocation',
					dataType	: 'json',
					type    	: 'POST',
					data    	: {  orderid : ids, sku : sku, qty : qty, count : count, det : det },
					success 	:	function( data  )
					{			
						if(data == 1)
						{
							//$('#'+merid).remove();
							$('.rowc_'+barcode).fadeOut();
							swal('Item successfully put back on the location.');
						}
					}                
				});	
	});

	$('body').on('click', '.done_reverce_picklist', function(){
	
		var rpid = $( this ).attr( 'for' );
		$.ajax({
					url     	: '<?php echo Router::url('/', true); ?>Picklist/revercepickdone',
					dataType	: 'json',
					type    	: 'POST',
					data    	: {  rpid : rpid },
					success 	:	function( data  )
					{			
						if(data == 1)
						{
							$('#row_'+rpid).fadeOut();
							location.reload();
						}
					}                
				});	
	});

</script>