<?php
$userID = $this->session->read('Auth.User.id');
?>
<div class="bottom_panel rightside bg-grey-100">
    <div class="container-fluid" >
       <div class="panel">
			 <div class="panel-body">
				
					<div class="row"> 
						<div class="col-lg-12">
						<div class="col-md-2 col-lg-2" style="padding-right:0px;"> 
						<div class="btn-group">
						  <button class="btn btn-success ">PickList Options</button>
						  <button class="btn btn-success  dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
						  <ul class="dropdown-menu">
							<li><a href="<?php echo Router::url('/', true).'Picklist/generate_picklist/all';?>" style="background-color:#99FFFF;">Generate Pick List Of All Open Ordders</a></li>
							<li><a href="<?php echo Router::url('/', true).'Picklist/generate_picklist/selsku';?>" style="background-color:#CC99FF;">Generate Pick List Of Selected SKU </a></li>
							<li><a href="<?php echo Router::url('/', true).'Picklist/generate_picklist/daterange?datefilter='.$_REQUEST['datefilter'].'&keyword='.$_REQUEST['keyword']?>" style="background-color:#99FFFF;">Generate Pick List Of Date Range</a></li>
							<li><a href="javascript:void(0);" onclick="plByLocation();" style="background-color:#CC99FF;">Generate Pick List Of Selected Locations</a></li> 
							<li><a href="<?php echo Router::url('/', true).'Picklist/generate_picklist/express'?>" style="background-color:#99FFFF;">Generate Pick List Of DHL orders</a></li>
							
							<?php if(count($files) > 0 ) {?>
								<li class="dropdown-submenu">  <a href="#" style="background-color:#FFFF66;">Download Picklist</a>  	 
								<ul class="dropdown-menu" style="background-color:#FFFF99;">
								<?php 
								$pdfPath = '/img/printPickList/'; 
								foreach( $files as $file ) {
									$time = explode('_', $file) ;
									
									$minute = explode('.', $time[7]) ;
									$picklistDate	=	 $time[3].'-'.$time[4].'-'.$time[5];
									
								if(strtotime(date($time[5].'-'.$time[4].'-'.$time[3])) >  strtotime('-7 day', strtotime(date('Y-m-d'))) ){
									
								?>
								<li><a href="<?php echo $pdfPath.$file; ?>" role="menuitem" target ="_blank" >Pick List <b>[ <?php echo $picklistDate.' '.$time[6].' : '.$minute[0] ?> ]</b></a></li> 
								<?php } }?>
								</ul>
							</li>
							<?php } ?>              
						  </ul>
						</div>		
								
						</div>							
						<div class="col-md-3 col-lg-3"> 
						 
							<select class="selectpicker btn form-control" multiple data-live-search="true" title="Choose locations" name="bin_location" id="bin_location"  > 
							  <?php foreach($bin_location as $bin){								  
									 echo ' <option data-tokens="'.$bin.'" value="'.$bin.'">'.$bin.'</option>';
								  						  
							  }?>							
							</select>
						</div>	  
						<form action="<?php echo Router::url('/', true).'Picklist/search/';?>" method="get" name="dt" id="dt">
						<div class="col-md-3  col-lg-3">
						
						<input type="text" id="datefilter" name="datefilter" value="<?php print isset($_REQUEST['datefilter'])?$_REQUEST['datefilter']:''?>" placeholder="Pick Date Range" class="form-control" value="" />
						
						</div>	
											
						<div class="col-md-3 col-lg-3">
					<?php /*?>	<form action="<?php echo Router::url('/', true).'Picklist/search/';?>" method="get" name="searchfrm" id="searchfrm"><?php */?>
							<input type="text" class="form-control" name="keyword" id="keyword" value="<?php print isset($_REQUEST['keyword'])?$_REQUEST['keyword']:''?>" placeholder="Search here..." />
						 
						</div>
						</form>
						<div class="col-md-1 no-padding"><button type="button"  class="btn btn-success btnsearch">Search</button></div>
						
						</div>
						<div class="col-lg-12"><?php print $this->Session->flash(); ?></div>  
				 </div>
			 
			</div>
			</div>
		<div class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12 ">
							<table class="table table-bordered table-striped dataTable">
								<tr>
									<th width="8%">Action</th>
									<th width="10%">SKU</th>
									<th width="3%">Qty</th>	
									<th width="5%">Order Count</th>	
									<th>Order Details</th>
								</tr>
								<?php								
								if(count($data) > 0){
								
								App::import('Controller', 'SortMdArray');
								$sort =	new SortMdArrayController;
								
								if(isset($_REQUEST['page'])){
									$page_number = $_REQUEST['page']; 				
								}else{
									$page_number = 1; 
								}
								$url = Router::url('/', true).'Picklist/search/?datefilter='.$_REQUEST['datefilter'].'&keyword='.$_REQUEST['keyword'].'&page=';
								$item_per_page = 10;
								$sort_by 	   = 'order_count';								
								$order_by 	   = $orderby = 'DESC';
								//$sort = new SortMdArray;
								$sort->sort_order = strtolower($orderby); 		
								$sort->sort_key = $sort_by;//'price';
								$sort->sortByKey($data);
			
			
								$total_rows    = count($data); 		
								$total_pages   = ceil($total_rows/$item_per_page);
								$page_position = (($page_number-1) * $item_per_page);
								$data 	  	   = array_slice( $data, $page_position,  $item_per_page );
								 		
					 			$id = 0; 
								//pr($selected_skus);
								foreach($data as   $val){
									$t= '';   $o= '';  $q= ''; $sku = ''; $id++;
									
									
									foreach($val as $i => $d){
										 if(is_numeric($i)){
											 $t[] = $d['order_id']." : ".$d['bin_location']." : ".date('Y-m-d H:i:s', strtotime($d['move_date'])); 									
											 $q[] = $d['quantity']; 
											 $o[] = $d['order_id'];										
										 }										  
									}									
									$sku = $val['sku'];
									
								?><tr>
									<td> 									
									<?php if(in_array($sku,$selected_skus)){?>
									<a href="javascript:void(0);" onclick="selectSku('<?php echo $id?>','<?php echo $sku?>','<?php echo implode(",",$o) ?>');" id="<?php echo $id?>" class="selected">Selected</a>
									<?php }else{?>
									<a href="javascript:void(0);" onclick="selectSku('<?php echo $id?>','<?php echo $sku?>','<?php echo implode(",",$o) ?>');" id="<?php echo $id?>">Select</a>
									
									<?php  }?>
									</td>
									<td><?php  echo $sku;?></td>
									<td><?php echo array_sum($q) ; ?> </td>
									<td><?php echo $val['order_count'] ; ?> </td>
									<td><?php  echo implode(", ",$t); ?> </td>
									
									</tr>
									<?php  
										}
									?> 	
								<?php }else{?>	
								<tr><td colspan="5">No Data Found!</td></tr>		
								<?php }?>				
							</table>							
						 
							 <ul class="pagination">
									  <?php
									 	if(count($data) > 0){
										echo $sort->getPaginate($item_per_page, $page_number, $total_rows, $total_pages,$sort_by,$order_by,$orderby,$url);
									  
									  }
										  /* echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
										   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));*/
									  ?>
								 </ul>
					 </div>											 
				</div>
            </div>
	    </div>

	</div>        
</div>


<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>
<script>

$(".btnsearch").click(function(){
    if($("#keyword").val())
    document.dt.submit();
});
$('#keyword').keypress(function(e){
        if(e.which == 13){//Enter key pressed
            document.dt.submit();
        }
    });
function plByLocation(){
	var slecteditem = [];
	$.each($("#bin_location option:selected"), function(){            
		slecteditem.push($(this).val());
	});		
   window.location.href = '<?php echo Router::url('/', true); ?>Picklist/PicklistByLocation?loc='+slecteditem; 

}
$(function() {

  $('#datefilter').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('#datefilter').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY')); 
	
	
	 document.dt.submit();
 /*
	  $.ajax(
			{
				url     : '<?php echo Router::url('/', true); ?>Picklist/search',
				type    : 'POST',
				data    : {date_range:$('#datefilter').val() },
				beforeSend : function(){  },  
				success :	function( msgArray  )
					{
						
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						 
						
					}                
			}); */
			
  });

  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});

function selectSku(id,sku,order_ids){
	 	
	$.ajax({
			url     	: '<?php echo Router::url('/', true); ?>Picklist/selectSku',
			dataType	: 'json',
			type    	: 'POST',
			data    	: {  sku:sku,order_ids : order_ids, action:$( "#"+id ).text() },
			beforeSend	: function() {
				//$( ".outerOpac" ).attr( 'style' , 'display:block');
				$( "#"+id ).text( 'Wait...');
			},   			  
			success 	:	function( data  )
			{
			
				 if(data.msg == 'inserted'){
					  $( "#"+id ).addClass( 'selected');						 
					  $( "#"+id ).text( 'Selected');
				  }else{
					  $( "#"+id ).removeClass( 'selected');						 
					  $( "#"+id ).text( 'Select');
				  }
				 // $( ".outerOpac" ).attr( 'style' , 'display:none');
				 
			}                
		});
	
}



 
</script>