<?php
$userID = $this->session->read('Auth.User.id');
?>
<div class="bottom_panel rightside bg-grey-100">
    <div class="container-fluid" >
        <div class="row">
		
		    <div class="col-lg-12">
				
              <div class="panel">
			  	
				<div class="row">	
				<div class="col-lg-12"><?php print $this->Session->flash(); ?></div>
				<div class="col-lg-12">
					<div class="navbar-header" style="margin:0 auto; width:65%;padding-top:10px;float:left;">
						
							<div class="col-lg-4  "> 
										<div class="btn-group">
										 <button type="button"  href="javascript:void(0);" class="downloadPickList btn btn-success no-margin">Generate Pick List</button>
										  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										  </button>
										  <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">												  												<li><a class="btn btn-warning" href="<?php echo Router::url('/', true).'Picklist/generate_picklist/all';?>"><i class=""></i>Pick List Of All Open Ordders</a></li>											
												<li><a class="btn btn-warning" href="<?php echo Router::url('/', true).'Picklist/generate_picklist/selsku';?>"><i class=""></i>Pick List Of Selected SKU </a></li>
												<li><a class="btn btn-warning" href="<?php echo Router::url('/', true).'Picklist/generate_picklist/daterange?datefilter=';?>"><i class=""></i>Pick List Of Date Range </a></li>										  
									
										</ul>
								  </div>
										    
							</div>							
							
							<div class="col-lg-3 no-padding">
								<a class="btn btn-warning" href="<?php echo Router::url('/', true).'Picklist/flushskus/';?>"><i class=""></i>Flush Old Data </a>
							</div>	 
							
							<div class="col-lg-5 no-padding"><form action="<?php echo Router::url('/', true).'Picklist/search/';?>" method="get" name="dt" id="dt">
							<input type="text" id="datefilter" name="datefilter" class="form-control" value="" /></form></div>	 
					</div>				
					<div style="margin:0 auto;  width:30%; padding-top:10px;float:right; margin-right:10px;">
						<form action="<?php echo Router::url('/', true).'Picklist/search/';?>" method="get">						
						<div class="col-md-9">
						<input type="text" class="form-control" name="keyword" placeholder="Search here..." />
						</div>
						<div class="col-md-3 no-padding"><button type="button"  class="btn btn-success">Search</button></div>
						</form>
					</div>
					 <div class="search-wrapper">
						  <div class="input-holder"></div>               
					 </div>
             </div>
			</div>
			<div class="panel-body">
			<div class="row">
			<div class="col-lg-12 ">
			<section id="blur-in">
         	 <div class="animated-tab">
            
              
              <div class="tab-pane-container">
                <section id="group1">
						
						<div class="overlay">&nbsp;</div>
							<table class="table table-bordered table-striped dataTable">
								<tr>
									<th width="8%">Action</th>
									<th width="10%">SKU</th>
									<th width="3%">Qty</th>	
									<th width="5%">Order Count</th>	
									<th >Order Details</th>
								</tr>
								
								
								<?php
								class SortMdArray {
									public $sort_order = 'asc'; // default
									public $sort_key = 'position'; // default
									 
									public function sortByKey(&$array) {       
										usort($array, array(__CLASS__, 'sortByKeyCallback'));     
									}
									 
									function sortByKeyCallback($a, $b) {
									$return='';
										if($this->sort_order == 'asc') {
											$return = $a[$this->sort_key] - $b[$this->sort_key];
										} else if($this->sort_order == 'desc') {
											$return = $b[$this->sort_key] - $a[$this->sort_key];
										}
										return $return;
									} 
								} 	
			
								App::import('Component', 'Common');
								$obj =	new CommonComponent;
								
								if(isset($page)){
									$page_number = $page; 				
								}else{
									$page_number = 1; 
								}
								$url = Router::url('/', true).'Picklist/orderSkus/';
								$item_per_page = 10;
								$sort_by 	   = 'order_count';								
								$order_by 	   = $orderby = 'DESC';
								$sort = new SortMdArray;
								$sort->sort_order = strtolower($orderby); 		
								$sort->sort_key = $sort_by;//'price';
								$sort->sortByKey($data);
			
			
								$total_rows    = count($data); 		
								$total_pages   = ceil($total_rows/$item_per_page);
								$page_position = (($page_number-1) * $item_per_page);
								$data 	  	   = array_slice( $data, $page_position,  $item_per_page );
								 		
					 			$id = 0; 
								//pr($selected_skus);
								foreach($data as   $val){
									$t= '';   $o= '';  $q= ''; $sku = ''; $id++;
									
									
									foreach($val as $i => $d){
										 if(is_numeric($i)){
											 $t[] = $d['order_id']." : ".$d['bin_location']." : ".date('Y-m-d H:i:s', strtotime($d['move_date'])); 									
											 $q[] = $d['quantity']; 
											 $o[] = $d['order_id'];										
										 }										  
									}									
									$sku = $val['sku'];
									
								?><tr>
									<td> 
									
									<?php   if(in_array($sku,$selected_skus)){?>
									<a href="javascript:void(0);" onclick="selectSku('<?php echo $id?>','<?php echo $sku?>','<?php echo implode(",",$o) ?>');" id="<?php echo $id?>" class="selected">Selected</a>
									<?php }else{?>
									<a href="javascript:void(0);" onclick="selectSku('<?php echo $id?>','<?php echo $sku?>','<?php echo implode(",",$o) ?>');" id="<?php echo $id?>">Select</a>
									
									<?php  }?>
									</td>
									<td><?php  echo $sku;?></td>
									<td><?php echo array_sum($q) ; ?> </td>
									<td><?php echo $val['order_count'] ; ?> </td>
									<td><?php  echo implode(", ",$t); ?> </td>
									
									</tr>
									<?php  
										}
									?> 								
							</table>
							<br> 
							</section>
							
							<div class="" style="margin:0 auto; width:350px">
								 <ul class="pagination">
									  <?php
									 	echo $obj->getPaginate($item_per_page, $page_number, $total_rows, $total_pages,$sort_by,$order_by,$orderby,$url);
									  
									  
										  /* echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
										   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));*/
									  ?>
								 </ul>
							  </div>
						</div>
					 
				</div>
            </div>
	    </div>

	</div>        
</div>
 

 
<script type="text/javascript">
$(function() {

  $('#datefilter').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('#datefilter').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY')); 
	
	
	 document.dt.submit();
 /*
	  $.ajax(
			{
				url     : '<?php echo Router::url('/', true); ?>Picklist/search',
				type    : 'POST',
				data    : {date_range:$('#datefilter').val() },
				beforeSend : function(){  },  
				success :	function( msgArray  )
					{
						
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						 
						
					}                
			}); */
			
  });

  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});
</script>
<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
<!---------------------------------------------- End for arrange order ---------------------------------------->
<style>
.selected{color:#009900; font-weight:bold;}
</style>
<script>
function selectSku(id,sku,order_ids){
	 	
	$.ajax({
			url     	: '<?php echo Router::url('/', true); ?>Picklist/selectSku',
			dataType	: 'json',
			type    	: 'POST',
			data    	: {  sku:sku,order_ids : order_ids, action:$( "#"+id ).text() },
			beforeSend	: function() {
				//$( ".outerOpac" ).attr( 'style' , 'display:block');
				$( "#"+id ).text( 'Wait...');
			},   			  
			success 	:	function( data  )
			{			
				 if(data.msg == 'inserted'){
					  $( "#"+id ).addClass( 'selected');						 
					  $( "#"+id ).text( 'Selected');
				  }else{
					  $( "#"+id ).removeClass( 'selected');						 
					  $( "#"+id ).text( 'Select');
				  }
				 // $( ".outerOpac" ).attr( 'style' , 'display:none');
				 
			}                
		});	
	}



 
</script>