		<!-- BEGIN RIGHTSIDE -->
        <div class="rightside bg-grey-100">
			<!-- BEGIN PAGE HEADING -->
            <div class="page-head bg-grey-100">
				<h1 class="page-title">Dashboard<small>Welcome to administration</small></h1>
				<!--<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>-->
				
				<div class="panel-title no-radius color-white no-border">
					<div class="panel-head"><?php print $this->Session->flash(); ?></div>
				</div>
				
			</div>
			<!-- END PAGE HEADING -->
			<!-- 'getPaidOrders' , 'getUnPaidOrders' , 'getProcessedOrders' , 'getCancelledOrders' -->
            <div class="container-fluid">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="panel bg-teal-500">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="color-white font-size-26 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="1230" data-speed="500" data-refresh-interval="10">
											<?php
												if( isset( $getProcessedOrders ) && $getProcessedOrders > 0 )
												{
													print $getProcessedOrders;
												}
											?>
										</div>
										<div class="display-block color-teal-50 font-weight-600"><i class="ion-plus-round"></i> Processed Orders</div>
									</div>
									<div class="pull-right">
										<i class="font-size-36 color-teal-100 ion-person-add"></i>
									</div>
								</div>
								<!--<div class="progress progress-animation progress-xs margin-top-25 margin-bottom-5">
									<div class="progress-bar bg-teal-100" role="progressbar" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									</div>
								</div>
								<div class="font-size-11 clearfix color-teal-50 font-weight-600">
									<div class="pull-left">PROGRESS</div>
									<div class="pull-right">72%</div>
								</div>-->
							</div>
						</div><!-- /.panel -->
					</div><!-- /.col -->
								
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="panel bg-red-400">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="color-white font-size-26 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="5613" data-speed="500" data-refresh-interval="10">
										<?php
												if( isset( $getUnPrepareOrders ) && $getUnPrepareOrders > 0 )
												{
													print $getUnPrepareOrders;
												}
												else
												{
													print "--";
												}
												
											?>
										</div>
										<div class="display-block color-red-50 font-weight-600"><i class="ion-plus-round"></i> Unprepared Orders</div>
									</div>
									<div class="pull-right">
										<i class="font-size-36 color-red-100 ion-email-unread"></i>
									</div>
								</div>
								<!--<div class="progress progress-animation progress-xs margin-top-25 margin-bottom-5">
									<div class="progress-bar bg-red-100" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									</div>
								</div>
								<div class="font-size-11 clearfix color-red-50 font-weight-600">
									<div class="pull-left">UNREAD</div>
									<div class="pull-right">80%</div>
								</div>-->
							</div>
						</div><!-- /.panel -->
					</div><!-- /.col -->
								
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="panel bg-blue-400">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="color-white font-size-26 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="343" data-speed="500" data-refresh-interval="10">
										    <?php 
										    
												if( isset( $getPaidOrders ) && $getPaidOrders > 0 )
												{
													echo trim($getPaidOrders);
													echo '  ';
													//print $getPaidOrders;
												}
												else
												{
													print "--";
												}
												
												print " / " ;
												
												if( isset( $getUnPaidOrders ) && $getUnPaidOrders > 0 )
												{
													print $getUnPaidOrders;
												}
												else
												{
													print "--";
												}

											?>
										</div>
										<div class="display-block color-blue-50 font-weight-600"><i class="ion-plus-round"></i> Paid / Unpaid Orders</div>
									</div>
									<div class="pull-right">
										<i class="font-size-36 color-blue-100 ion-ios-cart"></i>
									</div>
								</div>
								<!--<div class="progress progress-animation progress-xs margin-top-25 margin-bottom-5">
									<div class="progress-bar bg-blue-100" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									</div>
								</div>
								<div class="font-size-11 clearfix color-blue-50 font-weight-600">
									<div class="pull-left">UNREAD</div>
									<div class="pull-right">45%</div>
								</div>-->
							</div>
						</div><!-- /.panel -->
					</div><!-- /.col -->
								
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="panel bg-blue-grey-400">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="color-white font-size-26 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="152" data-speed="500" data-refresh-interval="10">
										<?php
												if( isset( $getCancelledOrders ) && $getCancelledOrders > 0 )
												{
													print $getCancelledOrders;
												}
												else
												{
													print "---- / ----";
												}
											?>
										</div>
										<div class="display-block color-blue-grey-50 font-weight-600"><i class="ion-plus-round"></i> Cancel Orders</div>
									</div>
									<div class="pull-right">
										<i class="font-size-36 color-blue-grey-100 ion-social-rss"></i>
									</div>
								</div>
								<!--<div class="progress progress-animation progress-xs margin-top-25 margin-bottom-5">
									<div class="progress-bar bg-blue-grey-100" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
										<span class="sr-only">60% Complete</span>
									</div>
								</div>
								<div class="font-size-11 clearfix color-blue-grey-50 font-weight-600">
									<div class="pull-left">UNREAD</div>
									<div class="pull-right">80%</div>
								</div>-->
							</div>
						</div><!-- /.panel -->
					</div><!-- /.col -->
				</div><!-- /.row -->
				
				<?php
				
				App::Import('Controller', 'Reports'); 
				$obj = new ReportsController;	
				//$delay_24 = $obj->DelayOrdersCount(24); 
							
				?>
				<div class="row">
				
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="panel bg-blue-300">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="color-white font-size-16 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="343" data-speed="500" data-refresh-interval="10">Held Orders</div>
										<div class="display-block color-blue-50 font-weight-500">
										More than 24 Hours => <?php echo	$obj->DelayOrdersCount(24) ?> 
										<small>(<a href="<?php echo Router::url('/', true) ?>Reports/DelayOrders">Download</a>)</small>	 
										</div>
									</div>									
								</div>
							 
							</div>
						</div><!-- /.panel -->
					</div>
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="panel bg-red-300">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="color-white font-size-16 font-roboto font-weight-500" data-toggle="counter" data-start="0" data-from="0" data-to="343" data-speed="500" data-refresh-interval="10">
										   LateDispatchRate(30 Days)
										</div>
										<?php $data = $obj->LateDispatchRate(30);  ?>
										<div class="display-block color-blue-50 font-weight-500">
											(<?php echo $data['date_range'][0];?> <strong style="color:#000;">to</strong> <?php echo $data['date_range'][1];?>)<br />											
											 Late Percentage =><?php echo number_format((($data['late_orders'] * 100) / $data['total_orders']),2); ?>%   <br />
											 Total Orders =><?php echo $data['total_orders'] ; ?> <small>(<a href="<?php echo Router::url('/', true) ?>Reports/TotalOrders">Download</a>)</small>	<br />
										     Late Count =><?php echo $data['late_orders']; ?> <small>(<a href="<?php echo Router::url('/', true) ?>Reports/LateDispatch">Download</a>)</small>	
										</div>
									</div>									
								</div>
							 
							</div>
						</div> 
					</div> 
					<!----------->
					<?php /*?><div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="panel bg-blue-400">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="color-white font-size-26 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="343" data-speed="500" data-refresh-interval="10">
										    <?php echo	$obj->DelayOrdersCount(72) ?>
										</div>
										<div class="display-block color-blue-50 font-weight-600"><i class="ion-plus-round"></i> Held Orders(72 Hours)</div>
									</div>									
								</div>
							 
							</div>
						</div> 
					</div><?php */?>
					
				</div>
				
				<div class="row">
						
				<div class="rightside bg-grey-100" style="margin-left: -15px; margin-right: -15px; margin-top: -69px;">
    <div class="page-head bg-grey-100">  
	
	<div class="col-sm-12 margin-bottom-10 padding-20" style="height:80px; padding-right: 0px; padding-left: 0px; background-color: #e0dddd; border: 1px solid #999;">
	<div class="col-sm-6">
		<div class="col-sm-4"><p style="font-size:16px; text-align:center; font-weight:bold; margin-bottom:0px;">&#65505;<?php echo number_format($t_summ['tsale'],2); ?></p>
		<p style="font-size:14px; text-align:center; margin-top:0px; ">Sales</p></div>
		
		<div class="col-sm-4"><p style="font-size:16px; text-align:center; font-weight:bold; margin-bottom:0px;"><?php echo $t_summ['torder']; ?></p>
		<p style="font-size:14px; text-align:center; margin-top:0px;">Orders</p></div>
		
		<div class="col-sm-4"><p style="font-size:16px; text-align:center; font-weight:bold; margin-bottom:0px;">&#65505;<?php echo number_format($t_summ['averg'],2); ?></p>
		<p style="font-size:14px; text-align:center; margin-top:0px;">Average Order Value</p></div>
		
	</div>
	
		<div class="col-sm-6">
		 <div class="col-sm-3">
				<a href="/Users" class="btn btn-info color-white btn-dark" style=" text-align: center; margin-top:3px;">Today Sale</a>
			</div>
		
			<div class="col-sm-6">
				<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
			</div>
			<div class="col-sm-3">
				<a href="javascript:void(0);" class="search_company_profit btn btn-success color-white btn-dark" style="margin-right:0px; text-align: center; margin-top:3px;">Search</a>
			</div>
			
		</div>
		
	</div>
	<div class="clear:both"></div>
	
	<div class="col-sm-12" style="border: 1px solid #ddd; padding-right: 0px; padding-left: 0px;padding-bottom: 50px;">
		<div class="col-sm-12" style="height:35px;font-size:16px;background-color:#313842; padding-right: 0px; padding-left: 15px; padding-top:5px; color:#fff; font-weight:bold; letter-spacing:0.7px; ">Marketplace Sales Overview</div>
		<?php if($f_data){?>
		<?php foreach($f_data as $sk=> $sv) { ?>
		<div class="col-sm-12" style="height:35px;font-size: 12px;background-color: #ddd; padding-right: 0px; padding-left: 0px; padding-top:8px;">
			<div class="col-sm-2"><strong><?php echo $sk; ?></strong></div>
			<div class="col-sm-3"><strong>Channel Account</strong></div>
			<div class="col-sm-2"><strong>Sales</strong></div>
			<div class="col-sm-2"><strong>Orders</strong></div>
			<div class="col-sm-2"><strong>Average Sales</strong></div>
		</div>
		<?php }?>
		<?php if($sv){?>
		<?php foreach($sv as $k => $v){ ?>
		<div class="col-sm-12" style="height:35px;font-size: 12px;background-color: #fff; padding-right: 0px; padding-left: 0px; padding-top:8px; border-bottom:1px solid #ddd;">
			<div class="col-sm-2"><img src="/img/country/<?php echo $v['flag']; ?>.png"></div>
			<div class="col-sm-3"><?php echo $k;  ?></div>
			<div class="col-sm-2"><?php echo number_format($v['f_sale'], 2); ?></div>
			<div class="col-sm-2"><?php echo $v['o_count']; ?></div>
			<div class="col-sm-2"><?php echo number_format($v['f_sale']/$v['o_count'],2); ?></div>
		</div>
		<?php }?>
		<?php } } ?>
	</div>
	</div>
</div>
</div>


				
				
				
                <div class="row">
					<div class="col-lg-12"> 
						<div class="panel">
							<div class="panel-title no-border bg-white">
								<!--<div class="panel-head"><i class="ion-arrow-graph-up-right"></i> Top 10 Skus</div>
								<div class="panel-tools">
									<a href="#" data-toggle="dropdown"><i class="ion-gear-a"></i></a>  
									<ul class="dropdown-menu pull-right margin-right-10">
										<li>
											<a href="#"><i class="ion-gear-a"></i> Settings </a>
										</li>
										<li>
											<a href="#"><i class="ion-ios-printer"></i> Print </a>
										</li>
										<li>
											<a href="#"><i class="ion-refresh"></i> Refresh </a>
										</li>
										<li class="divider"></li>
										<li>
											<a href="#" class="clearfix"><span class="pull-left">New Visitors</span> <span class="label bg-teal-500 pull-right">3</span></a>
										</li>
										<li>
											<a href="#" class="clearfix"><span class="pull-left">Total</span> <span class="label bg-red-500 pull-right">2</span></a>
										</li>
                                    </ul>
									<a href="#" class="panel-refresh"><i class="ion-refresh"></i></a>
									<a href="#" class="panel-close"><i class="ion-close"></i></a>
								</div>
							</div>-->
							<!--<div class="panel-body padding-top-5">
								<div class="row">
									<div class="col-lg-12">
										<div id="placeholder" class="flot-placeholder height-440">
										</div>
									</div>
								</div>
                            </div>-->
                        </div><!-- /.panel -->
					</div><!-- /.col -->
						
					<!--<div class="col-lg-6">
						<div class="panel">
                            <div class="panel-title no-border bg-white">
								<div class="panel-head"><i class="ion-ios-location"></i> Vector Map</div>
								<div class="panel-tools">
									<a href="#" class="panel-collapse"><i class="ion-arrow-up-b"></i></a>
									<a href="#" class="panel-refresh"><i class="ion-refresh"></i></a>
									<a href="#" class="panel-close"><i class="ion-close"></i></a>
								</div>
							</div>
                            <div class="panel-body no-padding">	
								<div id="map" class="height-460"></div>
                            </div>
						</div>
					</div>--><!-- /.col -->
				</div><!-- /.row -->
					
				<!-- BEGIN FOOTER -->
				<footer class="bg-white">
					<div class="pull-left">
						<span class="pull-left margin-right-15">&copy; 2015 WMS by JijGroup.</span>
						<ul class="list-inline pull-left">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
						</ul>
					</div>
				</footer>
				<!-- END FOOTER -->
            </div><!-- /.container-fluid -->
        </div><!-- /.rightside -->
    </div><!-- /.wrapper -->
	
<script>
	$('body').on('click', '.search_company_profit', function(){
  
  		var getFrom 	= $('div.daterangepicker_start_input input:text').val();
		var getEnd 		= $('div.daterangepicker_end_input input:text').val();
		var dateArEnd 	= getEnd.split('/');
		var newDateEnd 	= dateArEnd[2] + '-' + dateArEnd[0].slice(-2) + '-' + dateArEnd[1];
		var dateArFrom 	= getFrom.split('/');
		var newDateFrom = dateArFrom[2] + '-' + dateArFrom[0].slice(-2) + '-' + dateArFrom[1];
		var selectedDate = newDateFrom;
		var startDate = newDateEnd;
		window.location = "<?php echo Router::url('/', true) ?>Users/index/"+selectedDate+"/"+startDate;
  });
</script>
