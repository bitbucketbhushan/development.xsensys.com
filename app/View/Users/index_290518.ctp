		<!-- BEGIN RIGHTSIDE -->
        <div class="rightside bg-grey-100">
			<!-- BEGIN PAGE HEADING -->
            <div class="page-head bg-grey-100">
				<h1 class="page-title">Dashboard<small>welcome to administration</small></h1>
				<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600 pull-right border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
			</div>
			<!-- END PAGE HEADING -->
			<!-- 'getPaidOrders' , 'getUnPaidOrders' , 'getProcessedOrders' , 'getCancelledOrders' -->
            <div class="container-fluid">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="panel bg-teal-500">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="color-white font-size-26 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="1230" data-speed="500" data-refresh-interval="10">
											<?php
												if( isset( $getProcessedOrders ) && $getProcessedOrders > 0 )
												{
													print $getProcessedOrders;
												}
											?>
										</div>
										<div class="display-block color-teal-50 font-weight-600"><i class="ion-plus-round"></i> Processed Orders</div>
									</div>
									<div class="pull-right">
										<i class="font-size-36 color-teal-100 ion-person-add"></i>
									</div>
								</div>
								<div class="progress progress-animation progress-xs margin-top-25 margin-bottom-5">
									<div class="progress-bar bg-teal-100" role="progressbar" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									</div>
								</div>
								<div class="font-size-11 clearfix color-teal-50 font-weight-600">
									<div class="pull-left">PROGRESS</div>
									<div class="pull-right">72%</div>
								</div>
							</div>
						</div><!-- /.panel -->
					</div><!-- /.col -->
								
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="panel bg-red-400">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="color-white font-size-26 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="5613" data-speed="500" data-refresh-interval="10">
										<?php
												if( isset( $getUnPrepareOrders ) && $getUnPrepareOrders > 0 )
												{
													print $getUnPrepareOrders;
												}
												else
												{
													print "--";
												}
												
											?>
										</div>
										<div class="display-block color-red-50 font-weight-600"><i class="ion-plus-round"></i> UNPREPARED AMAZON ORDERS</div>
									</div>
									<div class="pull-right">
										<i class="font-size-36 color-red-100 ion-email-unread"></i>
									</div>
								</div>
								<div class="progress progress-animation progress-xs margin-top-25 margin-bottom-5">
									<div class="progress-bar bg-red-100" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									</div>
								</div>
								<div class="font-size-11 clearfix color-red-50 font-weight-600">
									<div class="pull-left">UNREAD</div>
									<div class="pull-right">80%</div>
								</div>
							</div>
						</div><!-- /.panel -->
					</div><!-- /.col -->
								
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="panel bg-blue-400">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="color-white font-size-26 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="343" data-speed="500" data-refresh-interval="10">
										    <?php 
										    
												if( isset( $getPaidOrders ) && $getPaidOrders > 0 )
												{
													echo trim($getPaidOrders);
													echo '  ';
													//print $getPaidOrders;
												}
												else
												{
													print "--";
												}
												
												print " / " ;
												
												if( isset( $getUnPaidOrders ) && $getUnPaidOrders > 0 )
												{
													print $getUnPaidOrders;
												}
												else
												{
													print "--";
												}

											?>
										</div>
										<div class="display-block color-blue-50 font-weight-600"><i class="ion-plus-round"></i> PAID / UNPAID ORDERS</div>
									</div>
									<div class="pull-right">
										<i class="font-size-36 color-blue-100 ion-ios-cart"></i>
									</div>
								</div>
								<div class="progress progress-animation progress-xs margin-top-25 margin-bottom-5">
									<div class="progress-bar bg-blue-100" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									</div>
								</div>
								<div class="font-size-11 clearfix color-blue-50 font-weight-600">
									<div class="pull-left">UNREAD</div>
									<div class="pull-right">45%</div>
								</div>
							</div>
						</div><!-- /.panel -->
					</div><!-- /.col -->
								
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="panel bg-blue-grey-400">
							<div class="panel-body padding-15-20">
								<div class="clearfix">
									<div class="pull-left">
										<div class="color-white font-size-26 font-roboto font-weight-600" data-toggle="counter" data-start="0" data-from="0" data-to="152" data-speed="500" data-refresh-interval="10">
										<?php
												if( isset( $getCancelledOrders ) && $getCancelledOrders > 0 )
												{
													print $getCancelledOrders;
												}
												else
												{
													print "---- / ----";
												}
											?>
										</div>
										<div class="display-block color-blue-grey-50 font-weight-600"><i class="ion-plus-round"></i> CANCEL ORDERS</div>
									</div>
									<div class="pull-right">
										<i class="font-size-36 color-blue-grey-100 ion-social-rss"></i>
									</div>
								</div>
								<div class="progress progress-animation progress-xs margin-top-25 margin-bottom-5">
									<div class="progress-bar bg-blue-grey-100" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
										<span class="sr-only">60% Complete</span>
									</div>
								</div>
								<div class="font-size-11 clearfix color-blue-grey-50 font-weight-600">
									<div class="pull-left">UNREAD</div>
									<div class="pull-right">80%</div>
								</div>
							</div>
						</div><!-- /.panel -->
					</div><!-- /.col -->
				</div><!-- /.row -->

                <div class="row">
					<div class="col-lg-6"> 
						<div class="panel">
							<div class="panel-title no-border bg-white">
								<div class="panel-head"><i class="ion-arrow-graph-up-right"></i> Top 10 Skus</div>
								<div class="panel-tools">
									<a href="#" data-toggle="dropdown"><i class="ion-gear-a"></i></a>  
									<ul class="dropdown-menu pull-right margin-right-10">
										<li>
											<a href="#"><i class="ion-gear-a"></i> Settings </a>
										</li>
										<li>
											<a href="#"><i class="ion-ios-printer"></i> Print </a>
										</li>
										<li>
											<a href="#"><i class="ion-refresh"></i> Refresh </a>
										</li>
										<li class="divider"></li>
										<li>
											<a href="#" class="clearfix"><span class="pull-left">New Visitors</span> <span class="label bg-teal-500 pull-right">3</span></a>
										</li>
										<li>
											<a href="#" class="clearfix"><span class="pull-left">Total</span> <span class="label bg-red-500 pull-right">2</span></a>
										</li>
                                    </ul>
									<a href="#" class="panel-refresh"><i class="ion-refresh"></i></a>
									<a href="#" class="panel-close"><i class="ion-close"></i></a>
								</div>
							</div>
							<div class="panel-body padding-top-5">
								<div class="row">
									<div class="col-lg-12">
										<div id="placeholder" class="flot-placeholder height-440">
										</div>
									</div>
								</div>
                            </div>
                        </div><!-- /.panel -->
					</div><!-- /.col -->
						
					<div class="col-lg-6">
						<div class="panel">
                            <div class="panel-title no-border bg-white">
								<div class="panel-head"><i class="ion-ios-location"></i> Vector Map</div>
								<div class="panel-tools">
									<a href="#" class="panel-collapse"><i class="ion-arrow-up-b"></i></a>
									<a href="#" class="panel-refresh"><i class="ion-refresh"></i></a>
									<a href="#" class="panel-close"><i class="ion-close"></i></a>
								</div>
							</div>
                            <div class="panel-body no-padding">	
								<div id="map" class="height-460"></div>
                            </div>
						</div><!-- /.panel -->
					</div><!-- /.col -->
				</div><!-- /.row -->
					
				<!-- BEGIN FOOTER -->
				<footer class="bg-white">
					<div class="pull-left">
						<span class="pull-left margin-right-15">&copy; 2015 WMS by JijGroup.</span>
						<ul class="list-inline pull-left">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
						</ul>
					</div>
				</footer>
				<!-- END FOOTER -->
            </div><!-- /.container-fluid -->
        </div><!-- /.rightside -->
    </div><!-- /.wrapper -->
