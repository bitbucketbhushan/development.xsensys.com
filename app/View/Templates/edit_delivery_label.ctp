<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Template( Delivery label )</h1>
		<div class="panel-title no-radius bg-green-500 color-white no-border">
			
			<div class="submenu">
				<div class="navbar-header">
					<ul class="nav navbar-nav pull-left">
						<li>
							<a class="switch btn btn-success" href="/JijGroup/showalldeliverylabel" data-toggle="modal" data-target=""><i class=""></i> Show Delivery Label </a>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		</div>
    </div>
    <div class="message_notification"></div>
    <div class="container-fluid">
        <div class="row">
				<?php 
					print $this->form->create( 'Template', array( 'class'=>'form-horizontal', 'url' => '/templates/AddTemplate', 'type'=>'post','id'=>'warehouse' ) );
					print $this->form->input( 'Template.id', array( 'type'=>'hidden', ) );
				?>
         		<div class="col-lg-6 rackDetails">
					<div class="panel rackdetail">
						<div class="panel-title">
							<div class="panel-head">Store Detail</div>
								<div class="panel-tools">
								</div>
							</div>
							
							<div class="panel-body services">
							
									<div class="form-group">
										<div class="col-lg-12">                                            
											<?php
												if( count( $courier ) > 0 )
												print $this->form->input( 'Template.provider_id', array( 'type'=>'select', 'empty'=>'Choose Courier','options'=>$courier,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
											?>  
										</div>
									 </div>
									 
									 <div class="form-group">
										<div class="col-lg-12">                                            
											<?php
												if( count( $locationList ) > 0 )
												print $this->form->input( 'Template.location_id', array( 'type'=>'select', 'empty'=>'Choose Country','options'=>$locationList,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
											?>  
										</div>
									 </div>
									 
									 <div class="form-group">
										<div class="col-lg-12">                                            
											<?php
												if( count( $companyList ) > 0 )
												print $this->form->input( 'Template.company_id', array( 'type'=>'select', 'empty'=>'Choose Company','options'=>$companyList,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'onChange'=>'select_store();' ) );
											?>  
										</div>
									 </div>
									 
									 <div class="form-group addstore">
										<div class="col-lg-12">                                            
											<?php
												if( count( $storeList ) > 0 )
												print $this->form->input( 'Template.store_id', array( 'type'=>'select', 'empty'=>'Choose Store','options'=>$storeList,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
											?>  
										</div>
									 </div>
									 
									 <div class="form-group">
										<div class="col-lg-12">                                            
											<?php
												$paperMode = array('landscape' => 'Landscape', 'portrait' => 'Portrait');
												if( count( $paperMode ) > 0 )
												print $this->form->input( 'Template.paper_mode', array( 'type'=>'select', 'empty'=>'Paper Mode','options'=>$paperMode,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
											?>  
										</div>
									 </div>
									 
									  <div class="form-group">
										<div class="col-lg-12">                                            
											<?php
											   print $this->form->input( 'Template.paper_width', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'placeholder' => 'Paper Width' ) );
											?>  
										</div>
									 </div>
									 
									 <div class="form-group">
										<div class="col-lg-12">                                            
											<?php
											   print $this->form->input( 'Template.paper_height', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'placeholder' => 'Paper Height' ) );
											?>  
										</div>
									 </div>
									 
									  <div class="form-group">
										<div class="col-lg-12">                                            
											<?php
											   print $this->form->input( 'Template.barcode_width', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'placeholder' => 'Barcode Width' ) );
											?>  
										</div>
									 </div>
									 
									  <div class="form-group">
										<div class="col-lg-12">                                            
											<?php
											   print $this->form->input( 'Template.barcode_height', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'placeholder' => 'Barcode Height' ) );
											?>  
										</div>
									 </div>
									 
							</div>
						</div>
					</div>
		
				<div class="col-lg-6 warehouseDetails">
				  <div class="panel">
							<div class="panel-title">
								<div class="panel-head">Html</div>
							</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									  <div class="form-group">
									  <label class="col-sm-2 control-label">Html</label>
										<div class="col-lg-10">                                            
											<?php
												print $this->form->input( 'Template.html', array( 'type'=>'textarea','class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
											?>  
										</div>
									  </div>
								</div>
							</div>
							
							<div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100" style="clear:both">                                                                            
								<?php
									echo $this->Form->button('Add Template', array(
									'type' => 'button',
									'escape' => true,
									'class'=>'btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40',
									'onclick' => 'submitTemplateValue();'
									 ));	
								?>
							</div>
							
						</div>
					</div>
				</div>
		
				<!--<div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100" style="clear:both">                                                                            
					<?php
						echo $this->Form->button('Add Template', array(
						'type' => 'button',
						'escape' => true,
						'class'=>'btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40',
						'onclick' => 'submitTemplateValue();'
						 ));	
					?>
				</div>-->
			</form>
		</div>        
	</div>
	
<div class="outerOpac" style="display:none">
	<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; "></span>
	<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
	
	<script>
	
	function select_courier()
	{
		var id = $('#TemplateProviderId').val();
		
		$.ajax({
			'url'            : '/templates/getselectedservice',
			'type'           : 'POST',
			'data'           : { id : id },
			'success' 		 : function( msgArray )
								{
									if(msgArray != '')
									{
										$('.services').html( msgArray );
									}
									else
									{
										$('.services').html( '<p>There is now any postal service. </p>' );
									}
								}  
                              
            }); 
	}
	
	function select_store()
	{
		var id = $('#TemplateCompanyId').val();
		$.ajax({
			'url'            : '/templates/getselectedstore',
			'type'           : 'POST',
			'data'           : { id : id },
			'success' 		 : function( msgArray )
								{
									if(msgArray != '')
									{
										$('.addstore').html( msgArray );
									}
									else
									{
										$('.addstore').html( 'No Store.' );
									}
								}  
                              
            }); 
	}
	
	function submitTemplateValue()
	{
		var TemplateId					=	$( '#TemplateId' ).val();
		var TemplateProviderId			=	$( '#TemplateProviderId' ).val();
		var TemplateLocationId  		=   $( '#TemplateLocationId' ).val();
		var TemplateCompanyId 			=	$( '#TemplateCompanyId' ).val();
		/* for paper  */
		var TemplatePaperMode 			=	$( '#TemplatePaperMode' ).val();
		var TemplatePaperHeight 		=	$( '#TemplatePaperHeight' ).val();
		var TemplatePaperWidth 			=	$( '#TemplatePaperWidth' ).val();
		var TemplateBarcodeWidth 		=	$( '#TemplateBarcodeWidth' ).val();
		var TemplateBarcodeHeight 		=	$( '#TemplateBarcodeHeight' ).val();
		
		var TemplateStoreId				=	$( '#TemplateStoreId' ).val();
		
		var TemplateProviderText		=	$( '#TemplateProviderId' ).find(":selected").text();
		var TemplateLocationText		=	$( '#TemplateLocationId' ).find(":selected").text();
		var TemplateCompanyText			=	$( '#TemplateCompanyId' ).find(":selected").text();
		var TemplateStoreText			=	$( '#TemplateStoreId' ).find(":selected").text();
		
		var TemplateHtmlText			=	$( '#TemplateHtml' ).val();
		
		if( TemplateProviderId == '' )
		{
			$('<p style="margin-bottom: -15px; color:red;">Please select service provider.</p>').insertAfter( '#TemplateProviderId' ).delay(2000).fadeOut();
		}
		if( TemplateLocationId == '' )
		{
			$('<p style="margin-bottom: -15px; color:red;">Please select location.</p>').insertAfter( '#TemplateLocationId' ).delay(2000).fadeOut();
		}
		if( TemplateCompanyId == '' )
		{
			$('<p style="margin-bottom: -15px; color:red;">Please select company.</p>').insertAfter( '#TemplateCompanyId' ).delay(2000).fadeOut();
		}
		if( TemplateStoreId == '' )
		{
			$('<p style="margin-bottom: -15px; color:red;">Please select store.</p>').insertAfter( '#TemplateStoreId' ).delay(2000).fadeOut();
		}
		if( TemplateHtmlText == '' )
		{
			$('<p style="margin-bottom: -15px; color:red;">Please put content.</p>').insertAfter( '#TemplateHtml' ).delay(2000).fadeOut();
		}
		if( TemplatePaperMode == '' )
		{
			$('<p style="margin-bottom: -15px; color:red;">Please put content.</p>').insertAfter( '#TemplatePaperMode' ).delay(2000).fadeOut();
		}
		if( TemplatePaperHeight == '' )
		{
			$('<p style="margin-bottom: -15px; color:red;">Please put content.</p>').insertAfter( '#TemplatePaperHeight' ).delay(2000).fadeOut();
		}
		if( TemplatePaperWidth == '' )
		{
			$('<p style="margin-bottom: -15px; color:red;">Please put content.</p>').insertAfter( '#TemplatePaperWidth' ).delay(2000).fadeOut();
		}
		if( TemplateBarcodeWidth == '' )
		{
			$('<p style="margin-bottom: -15px; color:red;">Please put content.</p>').insertAfter( '#TemplateBarcodeWidth' ).delay(2000).fadeOut();
		}
		if( TemplateBarcodeHeight == '' )
		{
			$('<p style="margin-bottom: -15px; color:red;">Please put content.</p>').insertAfter( '#TemplateBarcodeHeight' ).delay(2000).fadeOut();
		}
		if( TemplateProviderId == '' ||  TemplateLocationId == ''  || TemplateCompanyId == '' || TemplateStoreId == '' || TemplateHtmlText == '' || TemplatePaperMode == '' || TemplatePaperHeight == '' || TemplatePaperWidth == '' || TemplateBarcodeWidth == '' || TemplateBarcodeHeight == '' )
		{
			return false;
		}
		
		$.ajax({
			'url'            : '/templates/AddTemplate',
			'type'           : 'POST',
			'data'           : { 
									id   		  : TemplateId,
									provider_id   : TemplateProviderId,
									provider_name : TemplateProviderText,
									location_id   : TemplateLocationId,
									location_name : TemplateLocationText,
									company_id    : TemplateCompanyId,
									company_name  : TemplateCompanyText,
									store_id      : TemplateStoreId,
									store_name    : TemplateStoreText,
									paper_mode    : TemplatePaperMode,
									paper_height  : TemplatePaperHeight,
									paper_width   : TemplatePaperWidth,
									barcode_width : TemplateBarcodeWidth,
									barcode_height: TemplateBarcodeHeight,
									html 		  : TemplateHtmlText
								},
			'beforeSend'	 : function() 
											{
												$( ".outerOpac" ).attr( 'style' , 'display:block');
											}, 	
			'success' 		 : function( msgArray )
								{
									if(msgArray == '1')
									{
										$( ".outerOpac" ).attr( 'style' , 'display:none');
										$( '#TemplateProviderId' ).val('');
										$( '#TemplateLocationId' ).val('');
										$( '#TemplateCompanyId' ).val('');
										$( '#TemplateStoreId' ).val('');
										$( '#TemplateHtml' ).val('');
										
										$( '#TemplatePaperMode' ).val('');
										$( '#TemplatePaperHeight' ).val('');
										$( '#TemplatePaperWidth' ).val('');
										$( '#TemplateBarcodeWidth' ).val('');
										$( '#TemplateBarcodeHeight' ).val('');
										//location.reload();
										var url = $('.switch').attr('href');
										window.open(url,'_self');
									}
									else
									{
										$( ".outerOpac" ).attr( 'style' , 'display:none');
										location.reload();
									}
									
								}  
                              
            }); 
		return false;
		
	}
	
	</script>
