<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php print 'Delivery Label' ;?></h1>
		
			<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		
    </div>
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">									
									<div class="panel-tools">																	
									</div>
								</div>
                                <div class="panel-body no-padding-top bg-white">											
									<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 177px;" aria-sort="ascending" aria-label="Id: activate to sort column descending">S.No.</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 177px;" aria-sort="ascending" aria-label="Client: activate to sort column descending">Name</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 177px;" aria-sort="ascending" aria-label="Client: activate to sort column descending">Location</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 177px;" aria-sort="ascending" aria-label="Client: activate to sort column descending">Company</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 177px;" aria-sort="ascending" aria-label="Client: activate to sort column descending">Store</th>
												<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 177px;" aria-sort="ascending" aria-label="Client: activate to sort column descending">Action</th>
											</tr>
										</thead>
										<tbody role="alert" aria-live="polite" aria-relevant="all">
											<?php $i = 1; foreach( $getDeleveryDetails as $getDeleveryDetail ) { 
													$status = ($getDeleveryDetail['Template']['status'] == 0 ) ? 'alert-danger' : '';
												 ?>
												<tr class="odd <?php echo $status; ?> ">
													<td class="  sorting_1"><?php echo $i; ?></td>
													<td class="  sorting_1"><?php echo $getDeleveryDetail['Template']['label_name']; ?></td>                                                
													<td class="  sorting_1"><?php echo $getDeleveryDetail['Template']['location_name']; ?></td>                                                
													<td class="  sorting_1"><?php echo $getDeleveryDetail['Template']['company_name']; ?></td>                                                
													<td class="  sorting_1"><?php echo $getDeleveryDetail['Template']['store_name']; ?></td>                                                
													<td class="  sorting_1">
														
													<ul id="icons" class="iconButtons">
														<li>
															<a href="/templates/editDeliveryLabel/<?php print $getDeleveryDetail['Template']['id']; ?>" class="btn btn-success btn-xs margin-right-10"><i class="fa fa-pencil"></i></a>
														</li>
														<li>
															<?php if( $getDeleveryDetail['Template']['status'] == 1 ) { ?>
																<a href="javascript:void(0);" for="<?php print $getDeleveryDetail['Template']['id']; ?>/active" class="deleteDeliveryLabel btn btn-info btn-xs margin-right-10" title="" ><i class="fa fa-unlock"></i></a>
															<?php } else { ?>
																<a href="javascript:void(0);" for="<?php print $getDeleveryDetail['Template']['id']; ?>/deactive" class="deleteDeliveryLabel btn btn-info btn-xs margin-right-10" title="" ><i class="fa fa-lock"></i></a>
															<?php } ?>
														</li>
													</ul>
													</td>                                                
												</tr>
											<?php $i++;	} ?>
										</tbody>
									</table>		
								</div>
                            </div>
                        </div>
                    </div>
					<?php echo $this->element('footer'); ?>
				</div>
			</div>
		</div>
<div class="outerOpac" style="display:none">
	<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; "></span>
	<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>


<script>
$( "body" ).on( "click", ".deleteDeliveryLabel", function(){
	
				var attValue = $( this ).attr( 'for' )
				var id = attValue.split("/"); 
				$.ajax(
				{
					'url'            : '/templates/activeDeactiveDeleveryLabel',
					'type'           : 'POST',
					'data'           : { id : id[0], status : id[1] },
					'beforeSend'	 : function() 
												{
													$( ".outerOpac" ).attr( 'style' , 'display:block');
												}, 	
					'success' 		 : function( msgArray )
										{
											if(msgArray == 1)
											{
												$( ".outerOpac" ).attr( 'style' , 'display:none');
												location.reload();
											}
											if(msgArray == 2)
											{
												$( ".outerOpac" ).attr( 'style' , 'display:none');
												location.reload();
											}
										}
						});  
								  
				});

</script>

