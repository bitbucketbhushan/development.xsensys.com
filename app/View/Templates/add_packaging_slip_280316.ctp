<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Template( Packaging Slip )</h1>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
    </div>
    <div class="container-fluid">
        <div class="row">
				<?php
					print $this->form->create( 'Template', array( 'class'=>'form-horizontal', 'url' => '/templates/AddPackagingSlip', 'type'=>'post','id'=>'warehouse' ) );
				?>
				<div class="col-lg-12 warehouseDetails">
				  <div class="panel">
							<div class="panel-title">
								<div class="panel-head">Html</div>
							</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 col-lg-offset-2">
									  <div class="form-group">
										<div class="col-lg-10">                                            
											<?php
												print $this->form->input( 'PackagingSlip.html', array( 'type'=>'textarea','class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
											?>  
										</div>
									  </div>
								</div>
							</div>
						</div>
					</div>
				</div>
		
				<div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100" style="clear:both">                                                                            
					<?php
						echo $this->Form->button('Add Template', array(
						'type' => 'submit',
						'escape' => true,
						'class'=>'btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
						 ));	
					?>
				</div>
			</form>
		</div>        
	</div>
