<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Add Packaging Slip </h1>
		<div class="panel-title no-radius bg-green-500 color-white no-border">
			
			<div class="submenu">
				<div class="navbar-header">
					<ul class="nav navbar-nav pull-left">
						<li>
							<a class="switch btn btn-success" href="/JijGroup/showallpackagingSlip" data-toggle="modal" data-target=""><i class=""></i> Show Packaging Slip </a>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		</div>
    </div>
    <div class="container-fluid">
        <div class="row">
				<?php
					print $this->form->create( 'PackagingSlip', array( 'class'=>'form-horizontal', 'url' => '/templates/AddPackagingSlip', 'type'=>'post','id'=>'warehouse' ) );
					print $this->form->input( 'PackagingSlip.id', array( 'type'=>'hidden') );
				?>
          
				<div class="col-lg-6 rackDetails">
					<div class="panel rackdetail">
						<div class="panel-title">
							<div class="panel-head">Store Detail</div>
								<div class="panel-tools">
								</div>
							</div>
							
							<div class="panel-body services">
							<?php //pr( $locationList ); ?>
									<!--<div class="form-group">
										<div class="col-lg-12">                                            
											<?php
												if( count( $courier ) > 0 )
												print $this->form->input( 'PackagingSlip.provider_id', array( 'type'=>'select', 'empty'=>'Choose Courier','options'=>$courier,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
											?>  
										</div>
									 </div>
									 
									 <div class="form-group">
										<div class="col-lg-12">                                            
											<?php
												if( count( $locationList ) > 0 )
												print $this->form->input( 'PackagingSlip.location_id', array( 'type'=>'select', 'empty'=>'Choose Country','options'=>$locationList,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
											?>  
										</div>
									 </div>-->
									 
									 <div class="form-group">
										<div class="col-lg-12">                                            
											<?php
												if( count( $companyList ) > 0 )
												print $this->form->input( 'PackagingSlip.company_id', array( 'type'=>'select', 'empty'=>'Choose Company','options'=>$companyList,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'onChange'=>'select_store();' ) );
											?>  
										</div>
									 </div>
									 
									 
									 <div class="form-group addstore">
										 <div class="col-lg-12">                                            
												<?php
												
													if( count( $storeList ) > 0 )
													print $this->form->input( 'PackagingSlip.store_id', array( 'type'=>'select', 'empty'=>'Choose Store','options'=>$storeList,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
												?>  
											</div>
									 </div>
									 
									 <div class="form-group">
										    <div class="col-lg-12">                                            
												<?php
												    $paperMode = array('landscape' => 'Landscape', 'portrait' => 'Portrait');
													if( count( $paperMode ) > 0 )
													print $this->form->input( 'PackagingSlip.paper_mode', array( 'type'=>'select', 'empty'=>'Paper Mode','options'=>$paperMode,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
												?>  
											</div>
									 </div>
									 
									 
									  <div class="form-group">
											<div class="col-lg-12">                                            
												<?php
												   print $this->form->input( 'PackagingSlip.paper_width', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'placeholder' => 'Paper Width' ) );
												?>  
											</div>
									 </div>
									 
									 <div class="form-group">
											<div class="col-lg-12">                                            
												<?php
												   print $this->form->input( 'PackagingSlip.paper_height', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'placeholder' => 'Paper Height' ) );
												?>  
											</div>
									 </div>
									 
									  <div class="form-group">
											<div class="col-lg-12">                                            
												<?php
												   print $this->form->input( 'PackagingSlip.barcode_width', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'placeholder' => 'Barcode Width' ) );
												?>  
											</div>
									 </div>
									 
									  <div class="form-group">
											<div class="col-lg-12">                                            
												<?php
												   print $this->form->input( 'PackagingSlip.barcode_height', array( 'type'=>'text', 'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'placeholder' => 'Barcode Height' ) );
												?>  
											</div>
									 </div>
									 
							</div>
						</div>
					</div>
		
				<div class="col-lg-6 warehouseDetails">
				  <div class="panel">
							<div class="panel-title">
								<div class="panel-head">Html</div>
							</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									 
									  <div class="form-group">
									  <label class="col-sm-2 control-label">Html</label>
										<div class="col-lg-10">                                            
											<?php
												print $this->form->input( 'PackagingSlip.html', array( 'type'=>'textarea','class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
											?>  
										</div>
									  </div>
								</div>
							</div>
							
							<div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100" style="clear:both">                                                                            
								<?php
									echo $this->Form->button('Add Template', array(
									'type' => 'button',
									'escape' => true,
									'class'=>'btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40',
									'onclick' => 'submitPackagingValue();'
									 ));	
								?>
							</div>
							
						</div>
					</div>
				</div>
		
				<!--<div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100" style="clear:both">                                                                            
					<?php
						echo $this->Form->button('Add Template', array(
						'type' => 'button',
						'escape' => true,
						'class'=>'btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40',
						'onclick' => 'submitPackagingValue();'
						 ));	
					?>
				</div>-->
			</form>
		</div>        
	</div>
<div class="outerOpac" style="display:none">
	<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; "></span>
	<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>

<script>

	function select_store()
		{
			var id = $('#PackagingSlipCompanyId').val();
			$.ajax({
				'url'            : '/templates/selectedstore',
				'type'           : 'POST',
				'data'           : { id : id },
				'success' 		 : function( msgArray )
									{
										if(msgArray != '')
										{
											$('.addstore').html( msgArray );
										}
										else
										{
											$('.addstore').html( 'No Store.' );
										}
									}  
								  
				}); 
		}

	function submitPackagingValue()
		{
			var PackagingSlipId					=	$( '#PackagingSlipId' ).val();
			//var PackagingSlipProviderId			=	$( '#PackagingSlipProviderId' ).val();
			//var PackagingSlipLocationId  		=   $( '#PackagingSlipLocationId' ).val();
			var PackagingSlipCompanyId 			=	$( '#PackagingSlipCompanyId' ).val();
			var PackagingSlipPaperMode 			=	$( '#PackagingSlipPaperMode' ).val(); /*  */
			var PackagingSlipStoreId			=	$( '#PackagingSlipStoreId' ).val();
			var PackagingSlipPaperWidth			=	$( '#PackagingSlipPaperWidth' ).val();
			var PackagingSlipPaperHeight		=	$( '#PackagingSlipPaperHeight' ).val();
			var PackagingSlipBarcodeWidth		=	$( '#PackagingSlipBarcodeWidth' ).val();
			var PackagingSlipBarcodeHeight		=	$( '#PackagingSlipBarcodeHeight' ).val();
			
			//var PackagingSlipProvideText		=	$( '#PackagingSlipProviderId' ).find(":selected").text();
			//var PackagingSlipLocationText		=	$( '#PackagingSlipLocationId' ).find(":selected").text();
			var PackagingSlipCompanyText		=	$( '#PackagingSlipCompanyId' ).find(":selected").text();
			var PackagingSlipPaperModeText		=	$( '#PackagingSlipPaperMode' ).find(":selected").text();
			var PackagingSlipStoreIdText		=	$( '#PackagingSlipStoreId' ).find(":selected").text();
			
			var PackagingSlipHtml			    =	$( '#PackagingSlipHtml' ).val();
			
			/*if( PackagingSlipProviderId == '' )
			{
				$('<p style="margin-bottom: -15px; color:red;">Please select service provider.</p>').insertAfter( '#PackagingSlipProviderId' ).delay(2000).fadeOut();
			}
			if(  PackagingSlipLocationId == '' )
			{
				$('<p style="margin-bottom: -15px; color:red;">Please select location.</p>').insertAfter( '#PackagingSlipLocationId' ).delay(2000).fadeOut();
			}*/
			if( PackagingSlipCompanyId == '' )
			{
				$('<p style="margin-bottom: -15px; color:red;">Please select company.</p>').insertAfter( '#PackagingSlipCompanyId' ).delay(2000).fadeOut();
			}
			if( PackagingSlipStoreId == '' )
			{
				$('<p style="margin-bottom: -15px; color:red;">Please select store.</p>').insertAfter( '#PackagingSlipStoreId' ).delay(2000).fadeOut();
			}
			if( PackagingSlipHtml == '' )
			{
				$('<p style="margin-bottom: -15px; color:red;">Please put content.</p>').insertAfter( '#PackagingSlipHtml' ).delay(2000).fadeOut();
			}
			/* for page and barcode size */
			if( PackagingSlipPaperMode == '' )
			{
				$('<p style="margin-bottom: -15px; color:red;">Please slelect paper mode.</p>').insertAfter( '#PackagingSlipPaperMode' ).delay(2000).fadeOut();
			}
			if( PackagingSlipPaperWidth == '' )
			{
				$('<p style="margin-bottom: -15px; color:red;">Please fill paper width.</p>').insertAfter( '#PackagingSlipPaperWidth' ).delay(2000).fadeOut();
			}
			if( PackagingSlipPaperHeight == '' )
			{
				$('<p style="margin-bottom: -15px; color:red;">Please fill paper height.</p>').insertAfter( '#PackagingSlipPaperHeight' ).delay(2000).fadeOut();
			}
			if( PackagingSlipBarcodeWidth == '' )
			{
				$('<p style="margin-bottom: -15px; color:red;">Please fill barcode width.</p>').insertAfter( '#PackagingSlipBarcodeWidth' ).delay(2000).fadeOut();
			}
			if( PackagingSlipBarcodeHeight == '' )
			{
				$('<p style="margin-bottom: -15px; color:red;">Please fill barcode height.</p>').insertAfter( '#PackagingSlipBarcodeHeight' ).delay(2000).fadeOut();
			}
			
			if( PackagingSlipCompanyId == '' || PackagingSlipStoreId == '' || PackagingSlipHtml == '' || PackagingSlipPaperMode == '' || PackagingSlipPaperWidth == '' || PackagingSlipPaperHeight == '' || PackagingSlipBarcodeWidth == '' || PackagingSlipBarcodeHeight == '')
			{
				return false;
			}
			
			$.ajax({
				'url'            : '/templates/AddPackagingSlip',
				'type'           : 'POST',
				'data'           : { 
										id    		   : PackagingSlipId,
										//provider_id    : PackagingSlipProviderId,
										//provider_name  : PackagingSlipProvideText,
										//location_id    : PackagingSlipLocationId,
										//location_name  : PackagingSlipLocationText,
										company_id     : PackagingSlipCompanyId,
										company_name   : PackagingSlipCompanyText,
										paper_mode	   : PackagingSlipPaperMode,
										paper_width	   : PackagingSlipPaperWidth,
										paper_height   : PackagingSlipPaperHeight,
										barcode_width  : PackagingSlipBarcodeWidth,
										barcode_height : PackagingSlipBarcodeHeight,
										store_id       : PackagingSlipStoreId,
										store_name     : PackagingSlipStoreIdText,
										html           : PackagingSlipHtml
									},
				'beforeSend'	 : function() 
												{
													$( ".outerOpac" ).attr( 'style' , 'display:block');
												}, 	
				'success' 		 : function( msgArray )
									{
										if(msgArray == '1')
										{
											$( ".outerOpac" ).attr( 'style' , 'display:none');
											$( '#PackagingSlipProviderId' ).val('');
											$( '#PackagingSlipLocationId' ).val('');
											$( '#PackagingSlipCompanyId' ).val('');
											$( '#PackagingSlipHtml' ).val('');
											var url = $('.switch').attr('href');
											window.open(url,'_self');
										}
										else
										{
											$( ".outerOpac" ).attr( 'style' , 'display:none');
											location.reload();
										}
									}  
								  
				}); 
					return false;
		}	
	
</script>
