<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Template</h1>
		<div class="panel-title no-radius bg-green-500 color-white no-border">
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		</div>
    </div>
    <div class="container-fluid">
        <div class="row">
				<?php
					print $this->form->create( 'Template', array( 'class'=>'form-horizontal', 'url' => '/templates/AddTemplate', 'type'=>'post','id'=>'warehouse' ) );
				?>
          
				<div class="col-lg-6 rackDetails">
					<div class="panel rackdetail">
						<div class="panel-title">
							<div class="panel-head">Details</div>
								<div class="panel-tools">
									<div class="form-group">
										<div class="col-lg-12">                                            
											<?php
												if( count( $courier ) > 0 )
												print $this->form->input( 'Template.provider_id', array( 'type'=>'select', 'empty'=>'Choose Courier','options'=>$courier,'class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false, 'onChange'=>'select_courier();') );
											?>  
										</div>
									 </div>  
								</div>
							</div>
							<div class="panel-body services"></div>
						</div>
					</div>
		
				<div class="col-lg-6 warehouseDetails">
				  <div class="panel">
							<div class="panel-title">
								<div class="panel-head">Html</div>
							</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12">
									  <div class="form-group">
									  <label class="col-sm-2 control-label">Name</label>
									 	<div class="col-lg-10">                                            
											<?php
												print $this->form->input( 'Template.name', array( 'type'=>'text','class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
											?>  
										</div>
									  </div>
									  
									  <div class="form-group">
									  <label class="col-sm-2 control-label">Html</label>
										<div class="col-lg-10">                                            
											<?php
												print $this->form->input( 'Template.html', array( 'type'=>'textarea','class'=>'form-control','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false) );
											?>  
										</div>
									  </div>
								</div>
							</div>
						</div>
					</div>
				</div>
		
				<div class="text-center margin-top-20 padding-top-20 border-top-1 border-grey-100" style="clear:both">                                                                            
					<?php
						echo $this->Form->button('Add Template', array(
						'type' => 'submit',
						'escape' => true,
						'class'=>'btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
						 ));	
					?>
				</div>
			</form>
		</div>        
	</div>
	
	<script>
	
	function select_courier()
	{
		var id = $('#TemplateProviderId').val();
		
		$.ajax({
			'url'            : getUrl() + '/templates/getselectedservice',
			'type'           : 'POST',
			'data'           : { id : id },
			'success' 		 : function( msgArray )
								{
									if(msgArray != '')
									{
										$('.services').html( msgArray );
									}
									else
									{
										$('.services').html( '<p>There is now any postal service. </p>' );
									}
								}  
                              
            }); 
	}
	
	</script>
