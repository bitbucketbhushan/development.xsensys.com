<link rel="stylesheet" href="/css/bootstrap-multiselect.css" type="text/css"> 
<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.head div div div{ padding:0px ;  }
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.col-65 { width: 65%; float: left;}
	.col-60 { width: 60%; float: left;}
	.col-55 { width: 55%; float: left;}	 
	.editable-submit, .editable-cancel{padding: 3px 8px;}
	.icon-remove, .icon-ok {position: relative;top: 1px;display: inline-block;font-family: 'Glyphicons Halflings';-webkit-font-smoothing: antialiased;font-style: normal;font-weight: normal; line-height: 1;}
	.icon-ok:before {content: "\e013";}
	.icon-remove:before {    content: "\e014";}
	.paginate-responsive {    text-align: center;	}
	.paginate-responsive .total_pages {    float: left;    width: auto;    display: inline-block;    margin: 0px;    padding: 5px 0 0;	}
	.paginate-responsive .paginate {    float: left;    text-align: center;    display: inline-block;    width: 68%;}
	.paginate-responsive .pagination {    display: inline-block;    padding-left: 0;    margin: 10px 0;    border-radius: 4px;}
	.paginate-responsive .pagination > li {    display: inline;}
	.paginate-responsive .total_rows {    float: right;    width: auto;    display: inline-block;    margin: 0px;    padding: 5px 0 0;}
	.cqty{padding:2px;text-align:center;}

 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Sku Report</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						
  						 
						 
					</div>	
					
					<div class="panel-body no-padding-top bg-white">
				 
 						<div class="  table-responsive">	
			
						<table id="grid" class="table pog table-striped">
						
						  <thead>
							<tr>
								<th>SKU</th>					
								<th>Stock</th>
								<th>Purchaser</th> 
								<th>Status</th>  
     							</tr>
						  </thead>
						  <tbody id="results">	
						  <?php foreach( $all_data as $v){ ?>
						  <?php		
						  
								$normal_stock = json_decode($v['StockEod']['normal_stock'],1);
								 
								foreach($normal_stock as $sku => $st){
									if(in_array($sku,$purchaser_skus)){
										$data[$sku] = $st;
									}
								}
								
								$nb_elem_per_page = 100;      
								$page = isset($_GET['seite'])?intval($_GET['seite']-1):0;
								//$data = $normal_stock;
								$number_of_pages = intval(count($data)/$nb_elem_per_page)+2;
								$page_no = $_REQUEST['seite'];

								foreach (array_slice($data, $page*$nb_elem_per_page, $nb_elem_per_page) as $sku => $stock) 
								{?>
								<tr>
									<td><?php echo $sku?></td>								
									<td><?php echo $stock;?></td>								
									<td><?php echo $supplier_purchaser[$supplier_skus[$sku]];?></td>
									<td><?php if(isset($active_products[$sku])){echo 'Active';}else{echo 'InActive';	}?></td>
 								</tr>
								<?php	 
								} 
								} ?>
							</tbody>		
						  </tbody>
						  <div class="paginate-responsive">
						  </div>  		
						</table>
			 
			  </div>
		  		<div class="" style="margin:20px auto; width:350px">
 						<ul id='paginator' class="pagination">
												
						<li class="page-item <?php echo $page_no == 1 ? 'active disabled' : '' ; ?>">
         <a class="page-link" href="<?php echo '?seite=1' ;?> " >First</a>
      </li>
						<?php
// Loop through page numbers
for ($page_in_loop = 2; $page_in_loop <= ($number_of_pages-1); $page_in_loop++) {
   if ($number_of_pages > 3) {

      // 5 pages to, 5 page to the left, 5 pages to the right
      if ( ($page_in_loop >= $page_no - 5 && $page_in_loop <= $page_no )  || ( $page_in_loop <= $page_no + 5 && $page_in_loop >= $page_no) ) {  ?>
  
      <li class="page-item <?php echo $page_in_loop == $page_no ? 'active disabled' : '' ; ?>">
         <a class="page-link" href="<?php
            // The URL
            echo '?seite=' . $page_in_loop ;?> " ><?php                     echo $page_in_loop;            ?>
         </a>
      </li>

      <?php }
   }
   // if the total pages doesn't look ugly, we can display all of them
   else { ?>

      <li class="page-item <?php echo $page_in_loop == $page_no ? 'active disabled' : '' ; ?>">
         <a class="page-link" href="<?php echo '?seite=' . $page_in_loop; ?> " ><?php echo $page_in_loop; ?></a>
     </li>
<?php } 
}// end for loop
?>
<li class="page-item <?php echo $page_no == $number_of_pages ? 'active disabled' : '' ; ?>">
         <a class="page-link" href="<?php echo '?seite='.$number_of_pages ;?> " >Last</a>
      </li>
</ul> 						
							</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
		 
		  
<div class="outerOpac" id="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999; al " src="<?php echo $this->webroot; ?>img/482.gif" />
</div> 
<script>	
function getSuppliers() {
        try { 
               var url = '<?php echo Router::url("/", true) ?>Purchaser/getSuppliers/';

                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: "json",
                    cache: false,
					beforeSend  : function() {
						//$('#not_selling_sku').html('<img src="<?php echo Router::url('/', true); ?>img/sm_loading.gif"/>'); 
						//$('#sold_out_sku').html('<img src="<?php echo Router::url('/', true); ?>img/sm_loading.gif"/>'); 
						//$('#recomendation').html('<img src="<?php echo Router::url('/', true); ?>img/sm_loading.gif"/>'); 
						 $('#outerOpac').show();
 					},
                    data: { user:$("#user option:selected").val() },
                    success: function (data) {
                        $('#supplier-div').html(data.supplier); 
						// $('#sold_out_sku').html(data.sold_out_sku);
						// $('#recomendation').html(data.recomendation_count);
						 $('#outerOpac').hide();
                    },
                    error: function (xhr, status, error) {
                        alert(error);
                    }

                });
            
        } catch (e) {
            alert(e.toString());
        }
 }

function checkUser(){ 

	if($("#user option:selected").val() == 'all'){
		alert('Please select user.');
		return false;
	}else{
 		window.location	= '<?php echo Router::url('/', true).'Purchaser/DownloadSkus/';?>'+$("#user option:selected").val();
 	} 
	return true;
}
function Checks(){ 

	var start = $('div.daterangepicker_start_input input:text').val();
	var end  = $('div.daterangepicker_end_input input:text').val();
	$( '#start' ).val(start);
	$( '#end' ).val(end);
	$( '#download' ).val(0);
	document.frm.submit();
 }
function Download(){ 
  	var start = $('div.daterangepicker_start_input input:text').val();
	var end  = $('div.daterangepicker_end_input input:text').val();
	$( '#start' ).val(start);
	$( '#end' ).val(end);
	$( '#download' ).val('1');
	document.frm.submit();
 } 
 
</script>
