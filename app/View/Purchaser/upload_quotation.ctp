<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Upload Quotation</h1>		
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>	
			<div class="submenu">
				<div class="navbar-header">
					<!--<ul class="nav navbar-nav pull-left">
						<li>
							<a class="switch btn btn-success" href="/Virtuals/showAllPo" data-toggle="modal" data-target=""><i class=""></i> Show All Po </a>
						</li>
						<li>
							<a class="switch btn btn-success generateDemoPofile" href="#" data-toggle="modal" data-target=""><i class=""></i> Sample Po </a>
						</li>
					</ul>-->
					 
				</div>
			</div>	
    </div>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="panel">                            
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="panel-body check_image_extension padding-bottom-40 padding-top-40">
                                	   <?php							
											print $this->form->create( 'quotation', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/Purchaser/UploadQuotationFile', 'type'=>'post','id'=>'productPo' ) );
										?>
										<div class="form-group">
											<label for="username" class="control-label col-lg-3">CSV File</label>                                        
											<div class="col-lg-7">
												<div class="input-group">
													<span class="input-group-btn">
														<span class="btn btn-primary btn-file">
															Browse...<?php
																		print $this->form->input( 'Import_file', array( 'type'=>'file','div'=>false,'label'=>false,'class'=>'', 'required'=>false) );
																	  ?>
														</span>
													</span>
															<input type="text" placeholder="No file selected" readonly="" class="form-control">
															<span class="input-group-addon no-padding-top no-padding-bottom">
													<div class="radio radio-theme min-height-auto no-margin no-padding">
														<input type="hidden" name="qt_id" value="<?php echo $this->params['pass'][0]?>">
														<?php
														echo $this->Form->button('Upload', array(
														'type' => 'submit',
														'escape' => true,
														'class'=>'add_brand btn bg-green-500 color-white btn-dark padding-left-40 padding-right-40'
														 ));	
													?>
													</div>
												</span>
											</div>
									    </div>
                                     </div>
								  </form>
							</div>
						</div>        
					</div>
<script>
	
	 
	
</script>
