<style>
.attrib{float: none;clear: both;width: 100%;padding-top: 2px;}
.showedit{border: 1px solid; padding:3px;}
.edit:hover{cursor:pointer}
</style><div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php print $role;?></h1>
        <div class="submenu">
					<div class="navbar-header">
						 
					</div>
					</div>
		
			<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		<h4>Purchaser Data </h4>
    </div>
    <?php
		//pr($productAllDescs); exit;
    ?>
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title bg-white no-border">
									<div class="<?php if(isset($_GET['search_key'])){echo 'col-md-10';}else{echo 'col-md-12';}?>" id="search_div">

								<form method="get" name="searchfrm" id="searchfrm" action="<?php echo Router::url('/', true);?>Purchaser" enctype="multipart/form-data">	
 									<input name="search_key" placeholder="Search by Supplier code or Date" class="form-control searchtext bg-white" data-style="btn-dropdown" type="text" value="<?php if(isset($_GET['search_key'])){echo $_GET['search_key'];}?>">
 								</form>	 
									</div>	
									
									<?php if(isset($_GET['search_key'])){ ?>
									<div class="col-md-2" ><a href="<?php echo Router::url('/', true) ?>Purchaser" class="btn btn-warning">GO BACK</a></div>
									 <?php }?>
									<div class="panel-tools">
									
									<!--<a class="downloadStockFile btn btn-success btn-xs margin-right-10 color-white"><i class="fa fa-download"></i> Download CSV</a>-->
									</div>
								</div>
                                <div class="panel-body no-padding-top bg-white">											
										<table class="table table-bordered table-striped">
										<thead>
										<tr role="row">
											<th style="width: 10%;">Supplier Code</th>
											<th>Quotation</th>
											<th>Quotation number</th>
											<th>Date</th>
											<th style="width: 10%;">PO Status</th>
											<th style="width: 12%;">Created BY</th> 
											<th style="width: 20%;">Action</th>
										</tr>
									</thead>
								<tbody role="alert" aria-live="polite" aria-relevant="all">
									<?php 
									foreach($quotations as $quote) {  
									?>
										<tr class="">
											<td class="sorting_1"><a href="<?php echo Router::url('/', true) ?>Purchaser/QuotationItems/<?php echo $quote['Quotation']['id']; ?>" style="color:#0000CC; text-decoration:underline;"> <?php echo $quote['Quotation']['supplier_code']; ?></a> </td>
											<td class="sorting_1">
											<?php if(!empty($quote['Quotation']['uploaded_file']) && file_exists(WWW_ROOT.'quotation/'.$quote['Quotation']['uploaded_file']) ){?><a href="<?php echo Router::url('/', true) ?>Reorder/SupplierPricing/<?php echo $quote['Quotation']['quotation_id']; ?>" style="margin-top:2px; color:#0000CC; text-decoration:underline;" title="Generate P.O(Supplier Pricing)"><?php echo $quote['Quotation']['quotation_id'];?></a> <?php }else{ echo $quote['Quotation']['quotation_id']; }?>
											</td>
											
											<td class="sorting_1">											
											<?php if(!empty($quote['Quotation']['file_name']) && file_exists(WWW_ROOT.'quotation/'.$quote['Quotation']['file_name']) ){?><a href="<?php echo Router::url('/', true) ?>quotation/<?php echo $quote['Quotation']['file_name']; ?>" style="margin-top:2px; color:#0000CC; text-decoration:underline;" title="Download Generated Quotation"><?php echo $quote['Quotation']['quotation_number']; ?></a> <?php }?></td>
											
											
											<td class="sorting_1"><?php echo $quote['Quotation']['added_date']; ?></td>
											<td class="sorting_1"><?php  if($quote['Quotation']['po_status'] > 0){echo 'Uploaded';}else{echo 'Not Uploaded';} ?></td> 												<td class="sorting_1"><?php echo $quote['Quotation']['username']; ?></td> 
											<td class="sorting_1">
											<?php  if($quote['Quotation']['po_status'] == 0){ ?>
											  <a href="<?php echo Router::url('/', true) ?>Purchaser/deleteQuotation/<?php echo $quote['Quotation']['id']; ?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete this Quotation?')"style="margin-top:2px;">Delete</a>
											| <a href="<?php echo Router::url('/', true) ?>Purchaser/uploadQuotation/<?php echo $quote['Quotation']['id']; ?>" class="btn btn-info btn-xs" style="margin-top:2px;" title="Upload Quotation">Upload Quote</a>
											<br /><?php if(!empty($quote['Quotation']['uploaded_file']) && file_exists(WWW_ROOT.'quotation/'.$quote['Quotation']['uploaded_file']) ){?><a href="<?php echo Router::url('/', true) ?>quotation/<?php echo $quote['Quotation']['uploaded_file']; ?>" style="margin-top:2px; color:#0000CC; text-decoration:underline;" title="Download Uploaded Quotation">Download Uploaded Quotation</a> <?php }?>
											
											<?php }?>
												
												 </td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
								
										<div class="" style="margin:0 auto; width:350px">
									 <ul class="pagination">
									  <?php
										   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
										   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									  ?>
									 </ul>
								</div>
								
 									<?php if(count($pos) > 0){ ?>
									  
 						
									<table class="table table-bordered table-striped">
									
									  <thead>
										<tr>
											<th>Supplier Code</th>
											<th>PO Name</th>											
											<th>PO Status</th>
											<th>PO Date</th>
										</tr>
									  </thead>
									  <tbody id="results">	
									  <?php  foreach( $pos as $v){ ?>
										<tr>
											<td class="sorting_1"><?php echo $v['Po']['supplier_code']?></td>
											<td><a href="<?php echo Router::url('/', true) ?>Virtuals/showPoDetail/<?php echo $v['Po']['id']?>" style="color:#0033CC; text-decoration:underline"><?php echo $v['Po']['po_name']?></a></td>					
																						
											<td><?php if($v['Po']['status'] > 0){echo 'Uploaded';}else{ echo 'Not Uploaded';}?></td>
											<td><?php echo $v['Po']['po_date']?></td>	
										</tr>
										<?php }?>
										</tbody>		
									  </tbody>
 									</table>
						 
 								  <div class="" style="margin:20px auto; width:350px">
											 <ul class="pagination">
											  <?php
												   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
												   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
												   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
											  ?>
											 </ul>
										</div>
									
									<?php }?><br />
								</div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
				<!-- BEGIN FOOTER -->
				<footer class="bg-white">
					<div class="pull-left">
						<span class="pull-left margin-right-15">&copy; 2015 WMS by JijGroup.</span>
						<ul class="list-inline pull-left">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
						</ul>
					</div>
				</footer>
				<!-- END FOOTER -->
            </div>
    </div>
</div>

 
		 
<div class="showPopupForAssignSlip"></div> 
<div class="showPopupForcomment"></div> 

<div class="outerOpac" style="display:none">
<span style="opacity:0.7;   background: #000;   width:      100%;  height:     100%;   z-index:    9999;  top:        0;   left:       0;   position:   fixed; ">
</span>
<img style="bottom: 0;    left: 0;    margin: auto;    opacity: 1;    position: fixed;    right: 0;   top: 0; z-index:    99999; " src="/app/webroot/img/482.gif" />
</div>
 