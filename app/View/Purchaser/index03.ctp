<link rel="stylesheet" href="/css/bootstrap-multiselect.css" type="text/css"> 
<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.head div div div{ padding:0px ;  }
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.col-65 { width: 65%; float: left;}
	.col-60 { width: 60%; float: left;}
	.col-55 { width: 55%; float: left;}	 
	.editable-submit, .editable-cancel{padding: 3px 8px;}
	.icon-remove, .icon-ok {position: relative;top: 1px;display: inline-block;font-family: 'Glyphicons Halflings';-webkit-font-smoothing: antialiased;font-style: normal;font-weight: normal; line-height: 1;}
	.icon-ok:before {content: "\e013";}
	.icon-remove:before {    content: "\e014";}
	.paginate-responsive {    text-align: center;	}
	.paginate-responsive .total_pages {    float: left;    width: auto;    display: inline-block;    margin: 0px;    padding: 5px 0 0;	}
	.paginate-responsive .paginate {    float: left;    text-align: center;    display: inline-block;    width: 68%;}
	.paginate-responsive .pagination {    display: inline-block;    padding-left: 0;    margin: 10px 0;    border-radius: 4px;}
	.paginate-responsive .pagination > li {    display: inline;}
	.paginate-responsive .total_rows {    float: right;    width: auto;    display: inline-block;    margin: 0px;    padding: 5px 0 0;}
	.cqty{padding:2px;text-align:center;}

 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Purchaser Data </h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						
  						<div class="row">	
						  <div class="col-sm-12"> 	
						 <?php $purchasers = [];				
								print $this->form->create( 'upload', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/Purchaser/index', 'type'=>'get','id'=>'upload','name'=>'upload' ) );
							?>
							 
 								 	
						   <div class="col-sm-3"><input type="text" name="searchkey"  class="form-control" value="<?php if(isset($_GET['searchkey'])) echo $_GET['searchkey']?>" /> 	</div>
						  <div class="col-sm-2"><button class="btn btn-info" role="button">Search</button></div>
						  </form>
						  
						  <?php if(isset($_GET['searchkey'])){?>
						   <div class="col-sm-1"><a href="<?php echo Router::url('/', true).'Purchaser';?>" class="btn btn-warning">Back</a></div>
						   <?php }?>
									
 									 
						 </div> 			
						</div>
						 
					</div>	
					
					<div class="panel-body no-padding-top bg-white">
				 
 						<div class="  table-responsive">	
			
						<table id="grid" class="table pog table-striped">
						
						  <thead>
							<tr>
								<th>Quotation No</th>					
								<th>File</th>
								<th>PO Status</th>
								<th>Added Date</th>
							</tr>
						  </thead>
						  <tbody id="results">	
						  <?php  foreach( $quotations as $v){ ?>
							<tr>
								<td><a href="<?php echo Router::url('/', true) ?>Purchaser/QuotationItems/<?php echo $v['Quotation']['id']?>" style="color:#0033CC; text-decoration:underline"><?php echo $v['Quotation']['quotation_number']?></a></td>					
								<td><?php echo $v['Quotation']['file_name']?></td>
								<td><?php if($v['Quotation']['po_status'] > 0){echo 'Uploaded';}else{ echo 'Not Uploaded';}?></td>
								<td><?php echo $v['Quotation']['added_date']?></td>	
							</tr>
							<?php }?>
							</tbody>		
						  </tbody>
						  <div class="paginate-responsive"> 
 						  </div>  		
						</table>
			 
					  </div>  
						<div class="" style="margin:20px auto; width:350px">
								 <ul class="pagination">
								  <?php
									   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
									   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								  ?>
								 </ul>
							</div>
						 
						<?php if(count($pos) > 0){ ?>
 						<div class="  table-responsive">	
			
						<table id="grid" class="table pog table-striped">
						
						  <thead>
							<tr>
								<th>PO Name</th>					
								<th>supplier_code</th>
								<th>PO Status</th>
								<th>PO Date</th>
							</tr>
						  </thead>
						  <tbody id="results">	
						  <?php  foreach( $pos as $v){ ?>
							<tr>
								<td><a href="<?php echo Router::url('/', true) ?>Virtuals/showPoDetail/<?php echo $v['Po']['id']?>" style="color:#0033CC; text-decoration:underline"><?php echo $v['Po']['po_name']?></a></td>					
								<td><?php echo $v['Po']['supplier_code']?></td>
								<td><?php if($v['Po']['status'] > 0){echo 'Uploaded';}else{ echo 'Not Uploaded';}?></td>
								<td><?php echo $v['Po']['po_date']?></td>	
							</tr>
							<?php }?>
							</tbody>		
						  </tbody>
						  <div class="paginate-responsive"> 
 						  </div>  		
						</table>
			 
					  </div>  
						<div class="" style="margin:20px auto; width:350px">
								 <ul class="pagination">
								  <?php
									   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
									   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								  ?>
								 </ul>
							</div>
						
						<?php }?><br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
		 
		  
<div class="outerOpac" id="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999; al " src="<?php echo $this->webroot; ?>img/482.gif" />
</div> 
<script>	
 
function checkUser(){ 

	if($("#user option:selected").val() == 'all'){
		alert('Please select Purchaser.');
		return false;
	}else{alert('<?php echo Router::url('/', true).'Purchaser/DownloadSkus/';?>'+$("#user option:selected").val());
 		window.location	= '<?php echo Router::url('/', true).'Purchaser/DownloadSkus/';?>'+$("#user option:selected").val();
 	} 
	return true;
}
 
</script>
