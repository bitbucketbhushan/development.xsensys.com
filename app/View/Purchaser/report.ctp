<link rel="stylesheet" href="/css/bootstrap-multiselect.css" type="text/css"> 
<style>
	.head{ background:#ddd;}
	.head div{ padding:5px ; font-weight:bold;}
	.head div div div{ padding:0px ;  }
	.body div{ padding:5px ;}
	.body div div{ padding:1px ;}
	.url{overflow:scroll;}
	.body div.sku{ border-bottom:1px solid #C6C6C6; padding:0px;} 
	.body div.sku div{padding:2px 3px; font-size:14px;} 
	.body .sku:nth-child(even) { background-color: #f9f9f9;	}*/
	.skudiv{ background-color: #eee; width:100%; border-bottom:#999999 dashed 1px;}
	.txt{float:left;}
	.xedit{cursor: pointer;}
	.delete{color:#CC0000; cursor:pointer; font-size:12px;}
	.col-65 { width: 65%; float: left;}
	.col-60 { width: 60%; float: left;}
	.col-55 { width: 55%; float: left;}	 
	.editable-submit, .editable-cancel{padding: 3px 8px;}
	.icon-remove, .icon-ok {position: relative;top: 1px;display: inline-block;font-family: 'Glyphicons Halflings';-webkit-font-smoothing: antialiased;font-style: normal;font-weight: normal; line-height: 1;}
	.icon-ok:before {content: "\e013";}
	.icon-remove:before {    content: "\e014";}
	.paginate-responsive {    text-align: center;	}
	.paginate-responsive .total_pages {    float: left;    width: auto;    display: inline-block;    margin: 0px;    padding: 5px 0 0;	}
	.paginate-responsive .paginate {    float: left;    text-align: center;    display: inline-block;    width: 68%;}
	.paginate-responsive .pagination {    display: inline-block;    padding-left: 0;    margin: 10px 0;    border-radius: 4px;}
	.paginate-responsive .pagination > li {    display: inline;}
	.paginate-responsive .total_rows {    float: right;    width: auto;    display: inline-block;    margin: 0px;    padding: 5px 0 0;}
	.cqty{padding:2px;text-align:center;}

 </style>
<div class="rightside bg-grey-100">
    <div class="page-head bg-grey-100">        
			<h1 class="page-title"></h1>
			<div class="panel-title no-radius bg-green-500 color-white no-border"></div>
				<div class="submenu">
					<div class="navbar-header">
					</div>
				</div>
				<div class="panel-head msg"><?php print $this->Session->flash(); ?> </div>
				<h4>Report</h4>
			</div>
    	  <div class="container-fluid">							
			<div class="row">
			  			
				<div class="col-lg-12">
					<div class="panel no-border ">
						<div class="panel-title bg-white no-border">
						
  						<div class="row">	
						  <div class="col-sm-12"> 	
						 <?php						
								print $this->form->create( 'upload', array( 'enctype' => 'multipart/form-data','class'=>'form-horizontal', 'url' => '/Purchaser/Report', 'type'=>'get','name'=>'frm','id'=>'frm' ) );
										?>
							  <div class="col-sm-2">
								<select class="form-control" name="user" id="user" onchange="getSuppliers();"> 
									<option value="all">All Purchaser</option>
									<?php foreach($users as $user){
									$selected = '';
									if(isset($_GET['user']) && $_GET['user'] == $user['User']['username']){ 
										$selected = ' selected="selected"';
									}
									echo '<option value="'.$user['User']['username'].'" '.$selected.'>'.$user['User']['first_name'].' '.$user['User']['last_name'].'</option>';
									}
									?> 												
								</select>
								</div>						  
							<div class="col-sm-3" id="supplier-div">	
  							  <select class="form-control" name="supplier" id="supplier"> 
									<option value="all">All Suppliers</option>
									<?php foreach($suppliers as $su){
									$selected = '';
									if(isset($_GET['supplier']) && $_GET['supplier'] == $su['SupplierDetail']['supplier_code']){ 
										$selected = ' selected="selected"';
									}
									echo '<option value="'.$su['SupplierDetail']['supplier_code'].'" '.$selected.'>'.$su['SupplierDetail']['supplier_name'].'</option>';
									}
									?> 												
								</select>
 							</div>	
							 	
 							  <div class="col-lg-4"> 
								<div id="bs-daterangepicker" class="btn bg-grey-50 padding-10-20 no-border color-grey-600  border-radius-25 hidden-xs no-shadow"><i class="ion-calendar margin-right-10"></i> <span></span> <i class="ion-ios-arrow-down margin-left-5"></i></div>
 									 
									<input name="start" type="hidden" id="start">
									<input name="end" type="hidden" id="end">
								 
							</div> 
							
							<div class="col-10">                                                      
 								<button class="btn btn-primary btn-warning" onclick="Checks();">Show Result</button>
							</div>
							<input name="download" value="0" type="hidden" id="download">
							
							<div class="col-lg-1">                                                      
 								<button class="btn btn-primary btn-success" onclick="Download();"><span class="glyphicon glyphicon-save"></span> Download</button>                             </div>			  	
 							</form>			 
						 </div> 			
						</div>
						 
					</div>	
					
					<div class="panel-body no-padding-top bg-white">
				 
 						<div class="  table-responsive">	
			
						<table id="grid" class="table pog table-striped">
						
						  <thead>
							<tr>
								<th>Date</th>					
								<th>Total sku</th>
								<th>Out of stock</th> 								
								<th>In stock SKU</th>
								<th>Out of stock %</th> 
								<!--<th>Sku status</th>-->
								<th>Action</th>
  							</tr>
						  </thead>
						  <tbody id="results">	
						  <?php foreach( $all_data as $v){ ?>
							<tr>
								<td><a href="javascript:void(0);" style="text-decoration:underline; color:#0000CC;" onclick="ReportSkus('<?php echo base64_encode($v['StockEod']['stock_date'])?>')"><?php echo $v['StockEod']['stock_date']?></a></td>					
									<?php $normal_stock = json_decode($v['StockEod']['normal_stock'],1); 
									$out_stock_count = 0; $in_stock_count = 0 ; $total = 0; $out_stock_per = 0;
									 
									foreach($normal_stock as $sku => $st){
										if(in_array($sku,$purchaser_skus)){
											 if($st < 1){
												$out_stock_count++;
											 }else{
												$in_stock_count++;
											 }
											 $total++;
										}
 									}
									$out_stock_per = number_format(((100 * $out_stock_count)/$total),2);
									//echo ' out_stock_count'.$out_stock_count;
									//echo ' = total'. $total;
 									?>
								<td><?php echo $total;?></td>
								<td><?php echo $out_stock_count;?></td>
								<td><?php echo $in_stock_count;?></td>
								<td><?php echo $out_stock_per;?>%</td>
								<!-- <td><?php echo $in_stock_count;?>status</td>-->								
								<td><a href="javascript:void(0);" onclick="downloadEod('<?php echo $v['StockEod']['stock_date']?>');">Download</a></td>
								
							</tr>
							<?php } ?>
							</tbody>		
						  </tbody>
						  <div class="paginate-responsive">
 						  </div>  		
						</table>
			 
          </div><div class="" style="margin:20px auto; width:350px">
								 <ul class="pagination">
								  <?php
									   echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									   echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
									   echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								  ?>
								 </ul>
							</div>
						<br />
						<div style="position:absolute; top:100%; right:0; left:0;"><?php echo $this->element('footer'); ?></div>
			</div>			
		  </div>
		  
		 
		  
<div class="outerOpac" id="outerOpac" style="display:none;">
<span style="opacity:0.7; background: #000; width:100%; height:100%; z-index:9999;  top:0; left:0; position:fixed;"></span>
<img style="bottom: 0; left: 0; margin: auto; opacity: 1; position: fixed; right: 0; top: 0; z-index: 99999; al " src="<?php echo $this->webroot; ?>img/482.gif" />
</div> 
<script>	
function getSuppliers() {
        try { 
               var url = '<?php echo Router::url("/", true) ?>Purchaser/getSuppliers/';

                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: "json",
                    cache: false,
					beforeSend  : function() {
						//$('#not_selling_sku').html('<img src="<?php echo Router::url('/', true); ?>img/sm_loading.gif"/>'); 
						//$('#sold_out_sku').html('<img src="<?php echo Router::url('/', true); ?>img/sm_loading.gif"/>'); 
						//$('#recomendation').html('<img src="<?php echo Router::url('/', true); ?>img/sm_loading.gif"/>'); 
						 $('#outerOpac').show();
 					},
                    data: { user:$("#user option:selected").val() },
                    success: function (data) {
                        $('#supplier-div').html(data.supplier); 
						// $('#sold_out_sku').html(data.sold_out_sku);
						// $('#recomendation').html(data.recomendation_count);
						 $('#outerOpac').hide();
                    },
                    error: function (xhr, status, error) {
                        alert(error);
                    }

                });
            
        } catch (e) {
            alert(e.toString());
        }
 }
 function ReportSkus(date){ 

  	window.location	= '<?php echo Router::url('/', true).'Purchaser/ReportSkus/';?>'+date+'/'+$("#user option:selected").val()+'/'+$("#supplier option:selected").val(); 
  }
  function downloadEod(date){ 

  	window.location	= '<?php echo Router::url('/', true).'Purchaser/downloadEod/';?>'+date+'/'+$("#user option:selected").val()+'/'+$("#supplier option:selected").val(); 
  }

function checkUser(){ 

	if($("#user option:selected").val() == 'all'){
		alert('Please select user.');
		return false;
	}else{
 		window.location	= '<?php echo Router::url('/', true).'Purchaser/DownloadSkus/';?>'+$("#user option:selected").val();
 	} 
	return true;
}
function Checks(){ 

	var start = $('div.daterangepicker_start_input input:text').val();
	var end  = $('div.daterangepicker_end_input input:text').val();
	$( '#start' ).val(start);
	$( '#end' ).val(end);
	$( '#download' ).val(0);
	document.frm.submit();
 }
function Download(){ 
  	var start = $('div.daterangepicker_start_input input:text').val();
	var end  = $('div.daterangepicker_end_input input:text').val();
	$( '#start' ).val(start);
	$( '#end' ).val(end);
	$( '#download' ).val('1');
	document.frm.submit();
 } 
 
</script>
