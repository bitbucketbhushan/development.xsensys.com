<div class="searchComp">
	<ul>
		<?php
			$productdeatil = json_decode(json_encode($productdeatil),0);
			foreach( $productdeatil as $productInfo => $productDetail ) :
		
		?>
		<li class="listSelect" id="sku_<?php printf( "%s", $productDetail->Product->id ); ?>"><div class="rTable">
			<div class="rTableRow">
				<div class="rTableCell one"><?php printf( "%s", $productDetail->Product->product_name ); ?></div>
				<div class="rTableCell two"><?php printf( "%s", $productDetail->Product->product_sku ); ?></div>
				<div class="rTableCell three"><?php printf( "%s", $productDetail->ProductDesc->barcode ); ?></div>
			</div>
		</div>
		</li>
		<?php
		
			endforeach;
		
		?>
		
	</ul>
</div>
<script>

	$( function()
	{
		
		$('body').on( 'click', 'li.listSelect', function(event)
		{
			
			event.stopImmediatePropagation(); 
						
			var title = $(this).find( 'div.one' ).text();
			var sku = $(this).find( 'div.two' ).text();
			var barcode = $(this).find( 'div.three' ).text();
			var id = $(this).attr('id').split('_')[1];			
			var li_id = $(this).attr('id');
			
			$.ajax({
				url     	: '/Bundles/mergeListRow',
				type    	: 'POST',
				data    	: {  id : id , title : title , sku : sku , barcode : barcode },	
				beforeSend	 : function() {
					$( ".outerOpac" ).attr( 'style' , 'display:block');
				},   			  
				async 		: false,		
				success 	:	function( data  )
				{
					
					var outrerBool = false;
					$( ".outerOpac" ).attr( 'style' , 'display:none');
					
					//barricate once
					if( $( "tr.alert-danger" ).length > 0 )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						alert( "OOPS, You must remove duplicate sku from list...!" );
						return false;
					}
					
					//newly row added
					if( $('.addNewRow tr' ).length == 0 )
					{
						$('.addNewRow').html( data );
						$( '.createBundle' ).attr( 'style' , 'display:block' );
						
						//remove row from list
						$( "#"+li_id ).remove();
						
					}
					else
					{
						//add once
						$('.addNewRow tr:last').after( data );							
						
						//get specific barcode
						var barcodeText = $('.addNewRow tr:last td.barcode').text();
						
						//count multipel same / duplicate rows
						var textCount = $('.barcode_'+barcodeText).length;
						
						if( textCount > 1 )
						{
							
							//now check once it has already added in list or not
							if( $('.addNewRow tr' ).length > 1 )
							{
								
								//get last and pick id
								var tr_Last_Id = $('.addNewRow tr:last' ).attr( 'id' );
								
								//get last and pick id
								var counetr = $('.addNewRow tr#'+tr_Last_Id ).length;
								
								//find once
								var newStr = ("td[id='" + tr_Last_Id + "']").length;
								
								$('.addNewRow tr:last').addClass( 'alert-danger' );
								
								if( $( "tr.alert-danger" ).length > 0 )
								{
									$( '.createBundle' ).attr( 'style' , 'display:none' );
									alert( "OOPS, You must remove duplicate sku from list...!" );
									return false;
								}
								
							}
							
						}
						else
						{
							
							$( '.createBundle' ).attr( 'style' , 'display:block' );
							//remove row from list
							$( "#"+li_id ).remove();
						
							
						}
						
					}
					
				}
				
			});
			
		});
		
	});

</script>
