<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title"><?php print $role;?></h1>
		<div class="submenu">
					
					</div>
			<div class="panel-title no-radius bg-green-500 color-white no-border">
			</div>
			<div class="panel-head"><?php print $this->Session->flash(); ?></div>
		
    </div>    
    <!-- END PAGE HEADING -->
    
    <div class="container-fluid">
		<div class="row">
                        <div class="col-lg-12">
							<div class="panel no-border ">
                                <div class="panel-title no-border">
										<?php
											print $this->form->input( 'Product.searchtext', array( 'placeholder'=>'Create bundle by serach Title > Sku > Barcode','type'=>'text','class'=>'form-control searchtext selectpicker bg-white','data-style'=>'btn-dropdown', 'div'=>false, 'label'=>false, 'required'=>false ) );
										?>
										<button style="display:none;" type="submit" class="clearList btn bg-orange-500 color-white btn-dark margin-right-10 padding-left-15 padding-right-15" formaction="/showList">X</button>
										
										<!-- Multiple search result for bundle creation -->
										<div class="multipleResult" style="display:none;"></div>
										
										<!-- hidden for sec only -->
										<div class="hiddenResult" style="display:none;"></div>
										
									<div class="panel-tools">
										
									</div>
								</div>
								
                                <div class="panel-body">											
											<table class="generatedTable table table-bordered table-striped="example1" aria-describedby="example1_info">
													<thead>
											<tr role="row">
												<!--<th style="width: 2%;" >Id</th>-->
												<th style="width: 2%;" >Sku</th>
												<th style="width: 20%;" >Title</th>												
												<th style="width: 8%;">Barcode</th>
												<th style="width: 8%;">Qty</th>
												<th style="width: 8%;">Remove</th>
										</thead>
										<tbody role="alert" aria-live="polite" aria-relevant="all" class="addNewRow">
										
										</tbody>
										
									</table>
									
									<input placeHolder="Enter Bundle Name" type="text" id="bundleTitle" value="" class="form-control" name="bundleTitle">
									<br>
									<input placeHolder="Enter Bundle Sku Alias" type="text" id="bundleAlias" value="" class="form-control" name="bundleAlias">
									<br>
									<input placeHolder="Enter Bundle Price" type="text" id="bundlePrice" value="" class="form-control" name="bundlePrice">
									<br>
									<button class="createBundle btn bg-orange-500 color-white btn-dark margin-right-10 padding-left-40 padding-right-40" type="submit" style="display:none">Create Bundle</button>
										
								</div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
				<!-- BEGIN FOOTER -->
				<footer class="bg-white">
					<div class="pull-left">
						<span class="pull-left margin-right-15">&copy; 2015 WMS by JijGroup.</span>
						<ul class="list-inline pull-left">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
						</ul>
					</div>
				</footer>
				<!-- END FOOTER -->
            </div>
    </div>
</div>
<div class="showPopupForAssignSlip"></div> 

<script>
	$(function()
	{
		
		//Download Stock file now
		$( 'body' ).on( 'click', '.downloadStockFile', function()
		{
			//Check If input rack name exists in table or not
			$.ajax({
					url     	: '/Products/prepareExcel',
					type    	: 'POST',
					data    	: {},  			  
					success 	:	function( data  )
					{
						 window.open(data,'_self' );
					}                
				});				
		});	
	});
	
	$('#ProductSearchtext').bind('keypress', function(e)
	{
	   if(e.which == 13) {
		   if($('#ProductSearchtext').val() != '')
			{
				submitSanningValue();
			}
			else
			{
				
			}
			
		}
	});
	
	function submitSanningValue()
	{
		
		//Not should be appear duplicate
		if( $( "tr.alert-danger" ).length > 0 )
		{
			alert( "OOPS, You must remove duplicate sku from list...!" );
			return false;
		}
		
		var searchKey	=	$('#ProductSearchtext').val();
		var outerTextResult = null;

		//ajax for to check if single / nil / multiple
		$.ajax({
			url     	: '/Bundles/getSearchResultOnce',
			type    	: 'POST',
			data    	: {  searchKey : searchKey },
			'dataType'	: "JSON",
			beforeSend	 : function() {
				$( ".outerOpac" ).attr( 'style' , 'display:block');
			},   			  
			success 	:	function( data )
			{
				
				var outerStatus = data.status;
				
				//section based on response				
				callAjaxByType(  searchKey , outerStatus );
				
			}
					
		});
		
	}
	
	function callAjaxByType( searchKey , outerStatus )
	{
		
		$.ajax({
			url     	: '/Bundles/getSearchResult',
			type    	: 'POST',
			data    	: {  searchKey : searchKey },			
			success 	:	function( data  )
			{
				
				$( ".outerOpac" ).attr( 'style' , 'display:none');
				
				if( outerStatus == "single" )
				{
					
					//newly row added
					if( $('.addNewRow tr' ).length == 0 )
					{
						$('.addNewRow').html( data );
						$( '.createBundle' ).attr( 'style' , 'display:block' );
					}
					else
					{
						//add once
						$('.addNewRow tr:last').after( data );							
						
						//get specific barcode
						var barcodeText = $('.addNewRow tr:last td.barcode').text();
						
						//count multipel same / duplicate rows
						var textCount = $('.barcode_'+barcodeText).length;
						
						if( textCount > 1 )
						{
							
							//now check once it has already added in list or not
							if( $('.addNewRow tr' ).length > 1 )
							{
								
								//get last and pick id
								var tr_Last_Id = $('.addNewRow tr:last' ).attr( 'id' );
								
								//get last and pick id
								var counetr = $('.addNewRow tr#'+tr_Last_Id ).length;
								
								//find once
								var newStr = ("td[id='" + tr_Last_Id + "']").length;
								
								$('.addNewRow tr:last').addClass( 'alert-danger' );
								
								if( $( "tr.alert-danger" ).length > 0 )
								{
									
									$( '.createBundle' ).attr( 'style' , 'display:none' );
									alert( "OOPS, You must remove duplicate sku from list...!" );
									return false;
									
								}
								
							}
							
						}
						else
						{
							$( '.createBundle' ).attr( 'style' , 'display:block' );
						}
						
					}
					
				}
				else if( outerStatus == "multiple" )
				{
					
					$('.multipleResult').attr( 'style' , 'display:block' );
					$('.multipleResult').html( data );
					
					$( '.clearList' ).attr( 'style' , 'display:block' );
					
				}
				else if( outerStatus == "error" )
				{
					$( '.createBundle' ).attr( 'style' , 'display:block' );
					swal( "Oops, no found!. Please try with correct keyword." );
					return false;
				}
										
			}   
						 
		});
			
	}
	
	//clear list
	$('body').on( 'click', '.createBundle', function()
	{
		
		if( $( '#bundleTitle' ).val() == "" )
		{
			swal( "Title Required." );
			return false;
		}
		
		if( $( '#bundlePrice' ).val() == "" || $( '#bundlePrice' ).val() == 0 )
		{
			swal( "Price Required." );
			return false;
		}
		
		//count skus
		var getCounter = $('.addNewRow tr' ).length;
		var createBundleSku;
		var i = 0;
		var status = false;
		
		//set identifier
		if( getCounter == 1 )
		{
			
			//Bundle with single
			status = "Single";
			
		}
		else if( getCounter > 1 )
		{
			
			//Bundle with multiple	
			status = "Multiple";
			
		}
		
		//now creation and merge skus
		$('.generatedTable').find("tbody.addNewRow tr td.sku").each(function(index)
		{
			
			if( i == 0 )
			{
				createBundleSku = $(this).html();
			}
			else
			{
				createBundleSku += ',' + $(this).html();
			}	
		
		i++;	
		});
		
		var qtyBundle = 0;
		i = 0;
		//now creation and merge quanitities
		$('.generatedTable').find("tbody.addNewRow tr td.qty").each(function(index)
		{
			
			if( i == 0 )
			{
				qtyBundle = $(this).find( 'input.inputQty' ).val();
			}
			else
			{
				qtyBundle += ',' + $(this).find( 'input.inputQty' ).val();
			}	
		
		i++;	
		});
		
		var barcodeBundle = 0;
		i = 0;
		//now creation and merge quanitities
		$('.generatedTable').find("tbody.addNewRow tr td.barcode").each(function(index){
			
			if( i == 0 )
			{
				barcodeBundle = $(this).text();
			}
			else
			{
				barcodeBundle += ',' + $(this).text();
			}	
		
		i++;	
		});
		
		//get title
		var bundleCreateTitle = $( '#bundleTitle' ).val();
		
		//get title alias
		var bundleCreateAlias = $( '#bundleAlias' ).val();
		
		//get price
		var bundleCreatePrice = $( '#bundlePrice' ).val()
		
		//now set the data to send to create bundle with customize quantity		
		$.ajax({
			url     	: '/Bundles/createBundle',
			type    	: 'POST',
			data    	: {  bundleCreateAlias : bundleCreateAlias , createBundleSku : createBundleSku , qtyBundle : qtyBundle , barcodeBundle : barcodeBundle , bundleCreateTitle : bundleCreateTitle , bundleCreatePrice : bundleCreatePrice , status : status },
			'dataType'	: "JSON",
			beforeSend	 : function() {
				//$( ".outerOpac" ).attr( 'style' , 'display:block');
			},   			  
			success 	:	function( data )
			{
				if( data.status == "sucess" )
				{
					$( ".addNewRow" ).remove();								
					$( "#bundleTitle" ).val('');
					$( "#bundlePrice" ).val('');
					$( "#ProductSearchtext" ).val('');
					$( "#bundleAlias" ).val('');
					swal( "[ " + data.bundle + " ] is created successsful." );						
					location.reload();					
				}
				else
				{
					swal( "Oops, somethign went wrong with your new bundle." );									
					return false;
				}
			}
					
		});
		
	});
	
	$('body').on( 'click', '.clearList', function()
	{
		
		//step 1: remove list
		//step 2: invisible clear button
		
		$('.multipleResult').html();
		$('.multipleResult').attr( 'style' , 'display:none' );
		
		$( '.clearList' ).attr( 'style' , 'display:none' );		
		
	});
	
	
	//reverify
	function reVerifyAgain()
	{
		$( "tr.alert-danger" ).length;		
	}
	
	$('body').on( 'click', '.create_barcode_popup', function(){
		
		var productId	=	$( this ).attr('for');
		$.ajax({
					url     	: '/Virtuals/getBarcodePopup',
					type    	: 'POST',
					data    	: {  productId : productId },
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					},  			  
					success 	:	function( popUpdata  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.showPopupForAssignSlip').html( popUpdata );
					}                
				});
		});
		
	$('body').on( 'click', '.create_qrcode_popup', function(){
		
		var productId	=	$(this).attr('for');
		$.ajax({
					url     	: '/Virtuals/getqrcodePopup',
					type    	: 'POST',
					data    	: {  productId : productId }, 
					beforeSend	 : function() {
						$( ".outerOpac" ).attr( 'style' , 'display:block');
					}, 			  
					success 	:	function( popUpdata  )
					{
						$( ".outerOpac" ).attr( 'style' , 'display:none');
						$('.showPopupForAssignSlip').html( popUpdata );
						
					}               
				});
				
		});
	
</script>
<script>

	$(function()
	{
		
		$('body').on( 'click', '.removeIt', function()
		{
			
			var id = $(this).parent().attr( 'id' );
			$(this).parent().remove();
			
		});
		
		
	});

</script>
