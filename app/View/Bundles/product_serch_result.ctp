<?php 
	foreach($productdeatil as $productdeatilIndex => $productdeatilValue ) {  			
?>
<tr class="odd" id="remove_<?php echo $productdeatilValue['Product']['id']; ?>">	
	<td class="  sorting_1 sku"><?php echo $productdeatilValue['Product']['product_sku']; ?></td>
	<td class="  sorting_1 titleSku"><?php echo $productdeatilValue['Product']['product_name']; ?></td>                                                	
	<td class="  sorting_1 barcode barcode_<?php echo $productdeatilValue['ProductDesc']['barcode']; ?>"><?php echo $productdeatilValue['ProductDesc']['barcode']; ?></td>
	<td class="  sorting_1 qty">
		<input type="text" id="inputQty_<?php echo $productdeatilValue['Product']['id']; ?>" data-style="btn-dropdown" class="form-control inputQty bg-white" name="inputQty" value="1">
	</td>					
	<td class="  sorting_1 removeIt">Remove</td>
</tr>
<?php } ?>
<script type="text/javascript">
	
	maniac.loaddatatables('table');
		
</script>

<script>

	$(function()
	{
		
		$('body').on( 'click', '.removeIt', function()
		{
			
			var id = $(this).parent().attr( 'id' );
			$(this).parent().remove();
			
			if( $( "tr.alert-danger" ).length > 0 )
			{
				$( '.createBundle' ).attr( 'style' , 'display:none' );
			}
			
			if( $('.addNewRow tr' ).length > 0 )
			{
				if( $( "tr.alert-danger" ).length > 0 )
				{
					$( '.createBundle' ).attr( 'style' , 'display:none' );
				}
				else
				{
					$( '.createBundle' ).attr( 'style' , 'display:block' );
				}
			}
			else
			{
				$( '.createBundle' ).attr( 'style' , 'display:none' );
			}
		});
		
		
	});

</script>
