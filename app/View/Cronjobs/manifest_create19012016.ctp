<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Manifest
			<?php              
				$getOperatorInformation = $this->Session->read('Auth.User') ;				
			?>
			
        </h1>
    </div>
    
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">                         
			<div class="col-lg-12">
	<div class="panel sortingSerivesLeft">
		  <div class="panel-title bg-gray-100 no-border">
				<div class="panel-head">Postal Provider Service List</div>                                                         
					<div class="panel-tools"></div>
				</div>
		  <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 100%; height: 430px;">
			  <div class="panel-body bg-grey-200" style="overflow: hidden; width: 100%; height: 430px;">
						  
		<!-- Now, Service would be render -->    
		<div class="row">
		<?php
			foreach( $getPostalNames as $postalIndex => $potalvalue )
			{
		?> 	
				<div class="col-sm-2 sortingBox">
					<div class=" panel margin-bottom-15 bg-white">
						<!-- Box Creation -->				
						<div class="panel-body padding-5">
							<div class="clearfix ">
								<div class="">
									<?php										
										if( $potalvalue['PostalProvider']['provider_name'] == "Belgium Post" )
										{
											$timeScene = '10PM';
										}
										else if( $potalvalue['PostalProvider']['provider_name'] == "Jersey Post" )
										{
											$timeScene = '11PM';
										}
									?>
									<p class="help-block no-margin font-size-18" ><span style="display:none;"><?php echo $potalvalue['PostalProvider']['provider_name']; ?></span><?php echo $potalvalue['PostalProvider']['provider_name'] . '( ' .$timeScene .' )'; ?></p>																											
									
									
										
										<!-- Manifest -->
										
										<div class="btn-group">
											<?php
											if( $potalvalue['PostalProvider']['provider_name'] == "Belgium Post" )
											{
												if( time() > strtotime('10 pm') )
												{		
													$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));											
													
											?>
												<button type="submit" id="<?php echo $potalvalue['PostalProvider']['provider_name']; ?>" class="cut_off btn btn-success no-margin">Cut-Off List</button>
												<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<?php
												}
												else
												{
													$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));	
											?>
													<button type="submit" id="<?php echo $potalvalue['PostalProvider']['provider_name']; ?>" class="cut_off btn btn-success no-margin">Cut-Off List</button>
													<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<?php
												}
											?>												
											<?php	
											}
											else if( $potalvalue['PostalProvider']['provider_name'] == "Jersey Post" )
											{
												if( time() > strtotime('11 pm') )
												{
													$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));	
											?>	
													<button type="submit" id="<?php echo $potalvalue['PostalProvider']['provider_name']; ?>" class="cut_off btn btn-success no-margin">Cut-Off List</button>
													<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<?php
												}
												else
												{	
													$time_in_12_hour_format  = date("g:i a", strtotime(date("H:i",$_SERVER['REQUEST_TIME'])));	
												?>
													<button type="submit" id="<?php echo $potalvalue['PostalProvider']['provider_name']; ?>" class="cut_off btn btn-success no-margin">Cut-Off List</button>
													<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<?php
												}	
												?>
											<?php
											}
											?>
												
											
												<span class="caret"></span>
												<span class="sr-only">Toggle Dropdown</span>
											</button>
											<?php
											if( $potalvalue['PostalProvider']['provider_name'] == "Belgium Post" )
											{
											?>
												<?php if(count($files) > 0 ) { ?>
													<ul class="dropdown-menu scrollable-menu" role="menu">
												<?php 
													$manifestPath = Router::url('/', true).'/img/cut_off/'.$folderName.'/';
													foreach( $files as $file )
													{
														$fileName = explode('-', $file) ;
														
														//File name
														$fname= $fileName[0];
														
														//get date
														$dateValue = explode( '_', $fileName[1] );
														$specificDate = $dateValue[0];
														
														//get time specific
														$minute = explode('.', $dateValue[2]) ;
														
														$timeValue = $dateValue[1] .':' . $minute[0];
												?>
														<?php
														if( $fname == "BelgiumPost" )
														{
														?>
																<li><a href="<?php echo $manifestPath.$file; ?>" target ="_blank" > <?php print $fname .'('.$timeValue.')' ; ?> </a></li>
														<?php
														}
													?>	
												<?php
													}
												?>
														<li role="separator"></li>
													</ul>
											<?php
												}
											}
											else if( $potalvalue['PostalProvider']['provider_name'] == "Jersey Post" )
											{
											?>
												<?php if(count($files) > 0 ) { ?>
													<ul class="dropdown-menu scrollable-menu" role="menu">
												<?php 
													$manifestPath = Router::url('/', true).'/img/cut_off/'.$folderName.'/';
													foreach( $files as $file )
													{
														$fileName = explode('-', $file) ;
														
														//File name
														$fname= $fileName[0];
														
														//get date
														$dateValue = explode( '_', $fileName[1] );
														$specificDate = $dateValue[0];
														
														//get time specific
														$minute = explode('.', $dateValue[2]) ;
														
														$timeValue = $dateValue[1] .':' . $minute[0];
												?>
													<?php
														if( $fname == "JerseyPost" )
														{
													?>
															<li><a href="<?php echo $manifestPath.$file; ?>" target ="_blank" > <?php print $fname .'('.$timeValue.')' ; ?> </a></li>
													<?php
														}
													?>	
												<?php
													}
												?>
														<li role="separator"></li>
													</ul>	
											<?php
												}	
											}
											?>
									  </div>
										
										<!-- Manifest -->										
								</div>
							</div>
						</div><!-- /Box -->	
					</div><!-- /.panel -->
				</div>	
		<?php
			}
		?>
	</div>
	  
	  </div><div class="slimScrollBar" style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 430px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div></div>
	  </div>
	  </div>
        </div>   
	</div><!-- /.panel -->
</div>
<!-- For popup -->    
<div class="showPopup"></div>    

<script>
	$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "Euraco is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off', function()
		 {
				var serviceProvider = $(this).attr('id');				
				$.ajax(
				{
					'url'            : '/Cronjobs/createCutOffList',
					'type'           : 'POST',
					'data'           : { serviceProvider : serviceProvider },
					'success'        : function( data )
									   {										   
										   if( data != 'blank' )
										   {
												window.open(data,'_blank' );
												location.reload();
										   }
										   else
										   {
											   alert( "OOPS, No orders found for manifest!" );											   
										   }
									   }
				});
			 
		 });
	});
</script>

