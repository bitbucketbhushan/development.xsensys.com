<div class="rightside bg-grey-100">
    <!-- BEGIN PAGE HEADING -->
    <div class="page-head bg-grey-100">        
        <h1 class="page-title">Manifest
			<?php              
				$getOperatorInformation = $this->Session->read('Auth.User') ;	
				date_default_timezone_set('Europe/Jersey');
				echo '[ '.date("g:i A", strtotime(date("H:i",$_SERVER['REQUEST_TIME']))) .' ]';
			?>
			
        </h1>
    </div>
    
    <!-- END PAGE HEADING -->
    <div class="container-fluid">
        <div class="row">                         
			<div class="col-lg-12">
		<?php   
                        //pr($getPostalNames);
			foreach( $getPostalNames as $postalIndex => $potalvalue )
			{
		?> 	
				<div class="col-sm-2">
					<div class=" panel margin-bottom-15 bg-white">
						<!-- Box Creation -->	
						<div class="panel-title padding-5"><?php echo $potalvalue['PostalProvider']['provider_name']; ?></div>						
						<div class="panel-body padding-5">
							<div class="clearfix ">
								<div class="">
									<?php										
										if( $potalvalue['PostalProvider']['provider_name'] == "Belgium Post" )
										{
											$timeScene = '01:00PM';
										}
										else if( $potalvalue['PostalProvider']['provider_name'] == "Jersey Post" )
										{
											$timeScene = '03:00PM';
										}
                                                                                else{}
									?>
									<p class="padding-top-10 padding-bottom-10 font-size-18 text-uppercase" ><span style="display:none;"><?php echo $potalvalue['PostalProvider']['provider_name']; ?></span>
									<?php										
										if( $potalvalue['PostalProvider']['provider_name'] == "Belgium Post" )
										{?>
											<img class="img-responsive" src="/img/bpost_logo.jpg">
										<?php }
										else if( $potalvalue['PostalProvider']['provider_name'] == "Jersey Post" )
										{?>
											<img class="img-responsive" src="/img/jpost_logo.jpg">
										<?php }
										else if( $potalvalue['PostalProvider']['provider_name'] == "PostNL" )
										{?>
											<img class="img-responsive" src="/img/postnl_logo.jpg">
										<?php }										
										else if( $potalvalue['PostalProvider']['provider_name'] == "Spain Post" )
										{?>
											<img class="img-responsive" src="/img/spainPost_logo.jpg">
										<?php }
										else if( $potalvalue['PostalProvider']['provider_name'] == "Correos" )//LogoCorreos.jpg
										{?>
											<img class="img-responsive" src="/img/LogoCorreos.jpg" style="height: 61px;">
										<?php }
										else{
										?>
																														
											<img class="img-responsive" src="/img/dhl_logo.jpg">
										<?php
										}
										?>
									</p>
									<div class="text-center">
									 <div class="bg-grey-900 color-white col-sm-4 padding-5 text-uppercase bold" style="">Before</div>
									 <div class="color-white col-sm-8 bg-red-400 padding-5 text-uppercase bold"><?php echo $timeScene;?></div>
									</div>																											
									
									
										
										<!-- Manifest -->
										<div class="row">
										
											<?php
											if( $potalvalue['PostalProvider']['provider_name'] == "Belgium Post" )
											{
												?>
												<div class="padding-top-5 col-sm-12"> 
                                                                                                <?php
													echo $this->form->create( 'createeDoc', array('url' => array('controller'=> 'Manifests', 'action' => '/JijGroup/System/BP/eShipper/Create')) );
													echo $this->Form->button(
														'Generate eShipper', 
														array(
															'formaction' => Router::url(
																array('controller' => '/JijGroup/System/BP/eShipper/Create')
															 ),
															'escape' => true,
															'class'=>'btn bg-red-400 color-white btn-dark col-sm-12'	
														)
													);
													echo $this->form->end();	
												?>
												</div>
												
												<div class="padding-top-5 col-sm-12"> 
                                                                                                <?php
													echo $this->form->create( 'createeDoc', array('url' => array('controller'=> 'Manifests', 'action' => '/JijGroup/System/Export/Label')) );
													echo $this->Form->button(
														'Generate Export Label', 
														array(
															'formaction' => Router::url(
																array('controller' => '/JijGroup/System/Export/Label')
															 ),
															'escape' => true,
															'class'=>'btn bg-blue-400 color-white btn-dark col-sm-12'	
														)
													);
													echo $this->form->end();	
												?>
												</div> 
																							
											<?php	
											}
											else if( $potalvalue['PostalProvider']['provider_name'] == "Jersey Post" )
											{
												?>
												<div class="padding-top-5 col-sm-12">
												
												<?php
													echo $this->form->create( 'createeDoc', array('url' => array('controller'=> 'Manifests', 'action' => '/JijGroup/System/JP/eDoc/Create')) );
													echo $this->Form->button(
														'Generate eDoc', 
														array(
															'formaction' => Router::url(
																array('controller' => '/JijGroup/System/JP/eDoc/Create')
															 ),
															'escape' => true,
															'class'=>'btn bg-red-400 color-white btn-dark col-sm-12'	
														)
													);
													echo $this->form->end();	
												?>
												</div>
											<?php
											}
											else if( $potalvalue['PostalProvider']['provider_name'] == "PostNL" )
											{
											?>
                                                <div class="padding-top-5 col-sm-12"> 
                                                                                                <?php
													echo $this->form->create( 'createeDoc', array('url' => array('controller'=> 'Manifests', 'action' => '/JijGroup/System/BP/eShipper/Create')) );
													echo $this->Form->button(
														'Generate eShipper', 
														array(
															'formaction' => Router::url(
																array('controller' => '/JijGroup/System/PNL/eShipper/Create')
															 ),
															'escape' => true,
															'class'=>'btn bg-red-400 color-white btn-dark col-sm-12'	
														)
													);
													echo $this->form->end();	
												?>
												</div>
												
												<div class="padding-top-5 col-sm-12"> 
                                                                                                <?php
													echo $this->form->create( 'createeDoc', array('url' => array('controller'=> 'Manifests', 'action' => '/JijGroup/System/Export/Label')) );
													echo $this->Form->button(
														'Generate Export Label', 
														array(
															'formaction' => Router::url(
																array('controller' => '/JijGroup/System/Export/Label/Postnl')
															 ),
															'escape' => true,
															'class'=>'btn bg-blue-400 color-white btn-dark col-sm-12'	
														)
													);
													echo $this->form->end();	
												?>
												</div>
												<div class="padding-top-5 col-sm-12"> 
                                                                                                <?php
													echo $this->form->create( 'createeDoc', array('url' => array('controller'=> 'Manifests', 'action' => '')) );
													echo $this->Form->button(
														'Brt FNVAB00R', 
														array(
															'formaction' => Router::url(
																array('controller' => '/Brt/getBrtSheetManifest')
															 ),
															'escape' => true,
															'class'=>'btn bg-green-400 color-white btn-dark col-sm-12'	
														)
													);
													echo $this->form->end();	
												?>
												</div>
												
												<div class="padding-top-5 col-sm-12"> 
                                                                                                <?php
													echo $this->form->create( 'createeDoc', array('url' => array('controller'=> 'Manifests', 'action' => '')) );
													echo $this->Form->button(
														'Brt FNVATOOR', 
														array(
															'formaction' => Router::url(
																array('controller' => '/Brt/getPhoneNumbreList')
															 ),
															'escape' => true,
															'class'=>'btn bg-red-400 color-white btn-dark col-sm-12'	
														)
													);
													echo $this->form->end();	
												?>
												</div>
												
												<div class="padding-top-5 col-sm-12"> 
                                                                                                <?php
													echo $this->form->create( 'createeDoc', array('url' => array('controller'=> 'Manifests', 'action' => '')) );
													echo $this->Form->button(
														'Brt Export', 
														array(
															'formaction' => Router::url(
																array('controller' => '/Brt/getDataForBrtPalletPrintPdf')
															 ),
															'escape' => true,
															'class'=>'btn bg-blue-400 color-white btn-dark col-sm-12'	
														)
													);
													echo $this->form->end();	
												?>
												</div>
												
												
<?php /*?>
<div class="padding-top-5 col-sm-12"> 
                                                                                                <?php
													echo $this->form->create( 'createeDoc', array('url' => array('controller'=> 'Manifests', 'action' => '/JijGroup/System/BP/eShipper/Create')) );
													echo $this->Form->button(
														'Generate eShipper', 
														array(
															'formaction' => Router::url(
																array('controller' => '/JijGroup/System/BP/eShipper/Create')
															 ),
															'escape' => true,
															'class'=>'btn bg-red-400 color-white btn-dark col-sm-12'	
														)
													);
													echo $this->form->end();	
												?>
												</div><?php */?>
												
												<?php /*?><div class="padding-top-5 col-sm-12"> 
                                                                                                <?php
													echo $this->form->create( 'createeDoc', array('url' => array('controller'=> 'Manifests', 'action' => '/JijGroup/System/Export/Label')) );
													echo $this->Form->button(
														'Generate Export Label', 
														array(
															'formaction' => Router::url(
																array('controller' => '/JijGroup/System/Export/Label/Postnl')
															 ),
															'escape' => true,
															'class'=>'btn bg-blue-400 color-white btn-dark col-sm-12'	
														)
													);
													echo $this->form->end();	
												?>
												</div><?php */?>												
											<?php
											}
											else if( $potalvalue['PostalProvider']['provider_name'] == "Spain Post" )
											{
												?>
												<div class="padding-top-5 col-sm-12">
												
												<?php
													echo $this->form->create( 'createeDocSpain', array('url' => array('controller'=> 'Manifests', 'action' => '/JijGroup/System/JP/Spain/Post/eDoc/Create')) );
													echo $this->Form->button(
														'Spain Manifest', 
														array(
															'formaction' => Router::url(
																array('controller' => '/JijGroup/System/JP/Spain/Post/eDoc/Create')
															 ),
															'escape' => true,
															'class'=>'cut_off_spain_post_outer btn bg-red-400 color-white btn-dark col-sm-12',
															'id' => $potalvalue['PostalProvider']['provider_name']
														)
													);
													echo $this->form->end();	
												?>
												</div>
											<?php
											}
											else if( $potalvalue['PostalProvider']['provider_name'] == "Correos" )
											{
												?>
												<div class="padding-top-5 col-sm-12">
												
												<?php
													echo $this->form->create( 'createeDocSpain', array('url' => array('controller'=> 'Manifests', 'action' => '/JijGroup/System/JP/Spain/Post/eDoc/Create')) );
													echo $this->Form->button(
														'Generate eShipper', 
														array(
															'formaction' => Router::url(
																array('controller' => 'manifests/edocGenerateForCorreos')
															 ),
															'escape' => true,
															'class'=>'cut_off_spain_post_outer btn bg-red-400 color-white btn-dark col-sm-12',
															'id' => $potalvalue['PostalProvider']['provider_name']
														)
													);
													echo $this->form->end();	
												?>
												</div>
												<div class="padding-top-5 col-sm-12"> 
                                                <?php
													echo $this->form->create( 'createeDoc', array('url' => array('controller'=> 'Manifests', 'action' => '/JijGroup/System/Export/Label')) );
													echo $this->Form->button(
														'Generate Export Label', 
														array(
															'formaction' => Router::url(
																array('controller' => '/JijGroup/System/Export/Label/Postnl')
															 ),
															'escape' => true,
															'class'=>'btn bg-blue-400 color-white btn-dark col-sm-12'	
														)
													);
													echo $this->form->end();	
												?>
												</div>		
											<?php
											}else if( $potalvalue['PostalProvider']['provider_name'] == "DHL" )
											{
												?>
												<div class="padding-top-5 col-sm-12">
												
												<?php
													echo $this->form->create( 'createeDocSpain', array('url' => array('controller'=> 'Manifests', 'action' => '#')) );
													echo $this->Form->button(
														'Generate eShipper', 
														array(
															'formaction' => Router::url(
																array('controller' => 'manifests/edocGenerateForDHL')
															 ),
															'escape' => true,
															'class'=>'cut_off_spain_post_outer btn bg-red-400 color-white btn-dark col-sm-12',
															'id' => $potalvalue['PostalProvider']['provider_name']
														)
													);
													echo $this->form->end();	
												?>
												</div>
												<div class="padding-top-5 col-sm-12"> 
                                                <?php
													echo $this->form->create( 'createeDoc', array('url' => array('controller'=> 'Manifests', 'action' => '/JijGroup/System/Export/DHL')) );
													echo $this->Form->button(
														'Generate Export Label', 
														array(
															'formaction' => Router::url(
																array('controller' => '/JijGroup/System/Export/Label/DHL')
															 ),
															'escape' => true,
															'class'=>'btn bg-blue-400 color-white btn-dark col-sm-12'	
														)
													);
													echo $this->form->end();	
												?>
												</div>		
											<?php
											}
											else
											{
											 
											}
											
											?>
										<!-- Manifest -->										
								</div>
							</div>
						</div><!-- /Box -->	
						</div>
					</div><!-- /.panel -->
				</div>	
		<?php
			}
		?>
	</div>
	  
	  </div>


	  </div>

</div>
<!-- For popup -->    
<div class="showPopup"></div>    

<script>
	$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "Euraco is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off', function()
		 {
			//Get time
			var getTime = $.ajax({				     
					url: "/Cronjobs/getClientTime",					
					dataType: 'text',					
					global: false,
					async:false,
					success: function(data) {
						return data;
					}
			}).responseText;

			var serviceProvider = $(this).attr('id');
			
			if( getTime == '0' )
			{
				swal({
					title: "Are you sure?",
					text: "You are generating Manifest After Cut off. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						var strAction = "update";
						/* Start here updating section */
						$.ajax(
						{
						'url'            : '/Cronjobs/createCutOffList',
						'type'           : 'POST',
						'data'           : { serviceProvider : serviceProvider },
						'success'        : function( data )
										   {										   
											   if( data != 'blank' )
											   {
													window.open(data,'_blank' );
													location.reload();
											   }
											   else
											   {
												   swal("No orders found for manifest!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						swal("Cancelled", "", "error");
					}
				});
			}
			else
			{
				$.ajax(
				{
				'url'            : '/Cronjobs/createCutOffList',
				'type'           : 'POST',
				'data'           : { serviceProvider : serviceProvider },
				'success'        : function( data )
								   {										   
									   if( data != 'blank' )
									   {
											window.open(data,'_blank' );
											location.reload();
									   }
									   else
									   {
										   swal("No orders found for manifest!" , "" , "error");										   
									   }
								   }
				});
			}
			 
		 });
		
		function getResult( data )
		{
			return data;
		}
	});
</script>

<script>
	$(function()
	{                     
		console.log( "****************************" );                    
		console.log( "Euraco is ready to processed" );
		console.log( "****************************" );					
		$("#LinnworksapisBarcode").focus();
		
		 $( 'body' ).on( 'click', '.cut_off_spain_post', function()
		 {
			 
			 var serviceProvider = $(this).attr('id');				 		 			 
			//Get time
			var getTime = $.ajax({				     
					url: "/Cronjobs/getClientTime",
					type : "Post",
					data : { serviceProvider : serviceProvider },					
					dataType: 'text',					
					global: false,
					async:false,
					success: function(data) {
						return data;
					}
			}).responseText;

			if( getTime == '0' )
			{
				swal({
					title: "Are you sure?",
					text: "You are clearing data After Cut off. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						var strAction = "update";
						/* Start here updating section */
						$.ajax(
						{
						'url'            : '/System/eDoc/Spain',
						'type'           : 'POST',
						'data'           : { serviceProvider : serviceProvider },
						'success'        : function( data )
										   {										   
											   if( data != 'blank' )
											   {
													window.open(data,'_blank' );
													location.reload();
													
											   }
											   else
											   {
												   swal("No orders found to flush!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						swal("Cancelled", "", "error");
					}
				});
			}
			else
			{
				
				swal({
					title: "Are you sure?",
					text: "have you filled eDoc information. Proceed?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Proceed",
					cancelButtonText: "No, cancel!",
					closeOnConfirm: false,
					closeOnCancel: false
				},
				function(isConfirm)
				{
					/* isConfirm tell us true or false */
					if (isConfirm)
					{
						var strAction = "update";
						/* Start here updating section */
						$.ajax(
						{
						'url'            : '/System/eDoc/Spain',
						'type'           : 'POST',
						'data'           : { serviceProvider : serviceProvider },
						'success'        : function( data )
										   {										   
											   if( data != 'blank' )
											   {
												   $('.autofocus').val('');
													window.open(data,'_blank' );
													location.reload();
													
											   }
											   else
											   {
												   swal("No orders found to flush!" , "" , "error");
											   }
										   }
						});         
					}
					else
					{
						swal("Cancelled", "", "error");
					}
				});
			}			 
		 });		
	}); 
</script>
